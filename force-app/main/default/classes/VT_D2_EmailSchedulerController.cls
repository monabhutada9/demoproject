/**
 * Created by Michael Salamakha on 12.02.2019.
 */

public class VT_D2_EmailSchedulerController {  //global with sharing
    public List<VTD1_Actual_Visit__c> actualVisits = new List<VTD1_Actual_Visit__c>();

    public  Id       relatedToVisitMember            {get; set;}
    private Id       visitMemberId;
    private Boolean  isCaregiver=false;
    private String   recipientName;

    public String communityPatientUrl {get {
        if (VTD1_RTId__c.getInstance().VTD2_Patient_Community_URL__c != null) return VTD1_RTId__c.getInstance().VTD2_Patient_Community_URL__c;
        else return null;
    }set;}

    public VT_D2_EmailSchedulerController() {
    }
    public class VisitInfo{
        public String   visitContactFirstName           {get; set;}
        public String   visitName                       {get; set;}
        public String   visitStudyNickname              {get; set;}
        public String   visitScheduledDay               {get; set;}
        public String   visitScheduledDate              {get; set;}
        public String   visitNumericTimeZone            {get; set;}
        public String   visitStudyPhoneNumber           {get; set;}
        public String   visitTime                       {get; set;}
        public String   visitTimeZone                   {get; set;}
        public DateTime visitPatientScheduledVisitDate  {get; set;}
    }
    public VisitInfo getVisitInfo(){
        VisitInfo vI = new VisitInfo();
        if(this.relatedToVisitMember != null){
            List<Visit_Member__c> visitMembersList = [select id,VTD1_Participant_User__r.FirstName, VTD1_Actual_Visit__c, VTD1_Participant_User__r.VTD1_Profile_Name__c from Visit_Member__c where id=:relatedToVisitMember limit 1];
            if(!visitMembersList.isEmpty()) {
                recipientName = visitMembersList.get(0).VTD1_Participant_User__r.FirstName;
                this.actualVisits = [
                        select id, VTD1_Contact__r.FirstName, VTD1_Visit_Name__c,
							   	VTD2_Scheduled_Visit_Day__c, VTD1_Scheduled_Date_Time__c,
							   	VTD2_Scheduled_Visit_Date_Formula_Email__c, VTD1_Actual_Visit__c.VTD2_Patient_s_Numeric_timezone__c,
							   	VTD2_UserTimezoneOffset__c, VTD1_Protocol_Nickname__c,
							   	VTD2_Caregiver_s_TimeZone__c, VTD2_Caregiver_s_Scheduled_Time__c,
							   	VTD2_Patient_s_Scheduled_Visit_Date__c, VTD2_Scheduled_Date_Time_Formula__c,
							   	VTD2_Study_Phone_Number__c, VTD2_Scheduled_Time_Formula__c,
								VTD1_Case__r.VTD1_Patient_User__r.LanguageLocaleKey
                        from VTD1_Actual_Visit__c
                        where id = :visitMembersList.get(0).VTD1_Actual_Visit__c
                        limit 1
                ];
                if(visitMembersList.get(0).VTD1_Participant_User__r.VTD1_Profile_Name__c == 'Caregiver') isCaregiver=true;
            }
        }
        if (!this.actualVisits.isEmpty()) { //Visit Member
            VTD1_Actual_Visit__c av = this.actualVisits.get(0);
            vI.visitContactFirstName          = recipientName;//av.VTD1_Contact__r.FirstName;
            vI.visitName                      = av.VTD1_Visit_Name__c;
            vI.visitStudyNickname             = av.VTD1_Protocol_Nickname__c;
            vI.visitScheduledDay              = VT_D1_TranslateHelper.translate(av.VTD2_Scheduled_Visit_Day__c, av.VTD1_Case__r.VTD1_Patient_User__r.LanguageLocaleKey);
            vI.visitPatientScheduledVisitDate = av.VTD1_Scheduled_Date_Time__c;
            vI.visitNumericTimeZone           = av.VTD2_Patient_s_Numeric_timezone__c;
            vI.visitScheduledDate             = av.VTD2_Scheduled_Visit_Date_Formula_Email__c;
            vI.visitStudyPhoneNumber          = av.VTD2_Study_Phone_Number__c;
            if(isCaregiver) {vI.visitTime     = av.VTD2_Caregiver_s_Scheduled_Time__c;}
            else            {vI.visitTime     = av.VTD2_Scheduled_Time_Formula__c;}
            if(isCaregiver) {vI.visitTimeZone = av.VTD2_Caregiver_s_TimeZone__c;}
            else            {vI.visitTimeZone = av.VTD2_Patient_s_Numeric_timezone__c;}  //VTD2_Patient_s_Numeric_timezone__c,
        }
        return vI;
    }
}