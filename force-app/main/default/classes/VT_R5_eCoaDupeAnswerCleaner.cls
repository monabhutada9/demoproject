/**
* @author: Carl Judge
* @date: 08-Dec-20
* @description: 
**/

public class VT_R5_eCoaDupeAnswerCleaner implements Database.Batchable<SObject> {
    public String query = 'SELECT Id FROM VTD1_Survey__c WHERE CreatedBy.Name = \'Mulesoft\' AND CreatedDate > 2020-12-06T00:00:00.000Z AND VTD1_CSM__r.VTD1_Study__c = \'a095I000000HaGC\'';
    public Object param1;
    public Object param2;
    public Object param3;
    public Object param4;
    public Object param5;

    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<VTD1_Survey__c> scope) {
        List<VTD1_Survey_Answer__c> answersToDelete = new List<VTD1_Survey_Answer__c>();
        Set<String> compositeKeys = new Set<String>();
        for (VTD1_Survey_Answer__c answer : [
            SELECT Id, VTD1_Survey__r.VTR5_Response_GUID__c, VT_R5_External_Widget_Data_Key__c
            FROM VTD1_Survey_Answer__c
            WHERE VTD1_Survey__c IN :scope
            ORDER BY VTD1_Survey__c
            LIMIT 10000
        ]) {
            String compositeKey = answer.VTD1_Survey__r.VTR5_Response_GUID__c + answer.VT_R5_External_Widget_Data_Key__c;
            if (!compositeKeys.contains(compositeKey)) {
                compositeKeys.add(compositeKey);
            } else {
                answersToDelete.add(answer);
            }
        }

        if (!answersToDelete.isEmpty()) {
            List<Database.DeleteResult> results = Database.delete(answersToDelete, false);
            for (Integer i = 0; i < results.size(); i++) {
                if (!results[i].isSuccess()) {
                    System.debug('Delete failed on record ' + answersToDelete[i].Id + ' - ' + results[i].errors[0].getMessage());
                }
            }
        }
    }

    public void finish(Database.BatchableContext BC) {

    }
}