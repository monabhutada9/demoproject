/**
 * Created by Alexey Mezentsev on 10/13/2020.
 */

public without sharing class VT_R5_ActualVisitScheduledActionsHandler extends Handler {
    private static final String TO_BE_SCHEDULED = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
    private static final String TO_BE_RESCHEDULED = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED;
    private static final String SCHEDULED = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED;
    private static final String REQUESTED = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_REQUESTED;
    private static final String COMPLETED = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED;
    private static final String TYPE_STUDY_TEAM = VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_STUDY_TEAM;
    private static final String TYPE_LABS = VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_LABS;
    private static final Boolean EXTERNAL_REMINDERS_IS_ACTIVE = VTR4_ExternalScheduler__c.getInstance().VTR4_RemindersIsActive__c;

    protected override void onAfterUpdate(Handler.TriggerContext context) {
        Datetime now = System.now();
        List<VT_R5_ActualVisitScheduledActions.ScheduledActionData> scheduledActions = new List<VT_R5_ActualVisitScheduledActions.ScheduledActionData>();
        for (VTD1_Actual_Visit__c aVisit : (List<VTD1_Actual_Visit__c>) context.newList) {
            VTD1_Actual_Visit__c oldVisit = (VTD1_Actual_Visit__c) context.oldMap.get(aVisit.Id);
            if (isTimeSlotChosen(aVisit, oldVisit)) {
                scheduledActions.add(new VT_R5_ActualVisitScheduledActions.ScheduledActionData(
                        aVisit.VTD2_Schedule_Request_Submitted__c.addDays(1), aVisit.Id, 'TimeSlotIsChosen')
                );
            }

            // SH-14809 PB Name: Pop Up Notifications for scheduled visits 6 diamond
            if (isConsentOrReconsentVisit(aVisit, oldVisit)) {
                scheduledActions.add(new VT_R5_ActualVisitScheduledActions.ScheduledActionData(
                        aVisit.VTD1_Scheduled_Date_Time__c, aVisit.Id, 'ConsentReconsentVisitScheduled'));
            }
            // SH-14809 PB Name: Pop Up Notifications for scheduled visits 9 diamond
            if (isConsentScheduled(aVisit, oldVisit)) {
                scheduledActions.add(new VT_R5_ActualVisitScheduledActions.ScheduledActionData(
                        aVisit.VTD1_Scheduled_Date_Time__c, aVisit.Id, 'ConsentScheduled')
                );
            }

            if (!EXTERNAL_REMINDERS_IS_ACTIVE && isNeedReminder(aVisit, oldVisit)) {
                if (aVisit.VTD1_Scheduled_Date_Time__c > now.addDays(2)) {
                    scheduledActions.add(new VT_R5_ActualVisitScheduledActions.ScheduledActionData(
                            aVisit.VTD1_Scheduled_Date_Time__c.addHours(-1), aVisit.Id, 'SendReminderBefore1Hour')
                    );
                    scheduledActions.add(new VT_R5_ActualVisitScheduledActions.ScheduledActionData(
                            aVisit.VTD1_Scheduled_Date_Time__c.addHours(-2), aVisit.Id, 'SendReminderBefore2Hours')
                    );
                    scheduledActions.add(new VT_R5_ActualVisitScheduledActions.ScheduledActionData(
                            aVisit.VTD1_Scheduled_Date_Time__c.addDays(-2), aVisit.Id, 'SendReminderBefore2Days')
                    );
                } else if (aVisit.VTD1_Scheduled_Date_Time__c <= now.addDays(2) && aVisit.VTD1_Scheduled_Date_Time__c > now.addHours(2)) {
                    scheduledActions.add(new VT_R5_ActualVisitScheduledActions.ScheduledActionData(
                            aVisit.VTD1_Scheduled_Date_Time__c.addHours(-1), aVisit.Id, 'SendReminderBefore1Hour')
                    );
                    scheduledActions.add(new VT_R5_ActualVisitScheduledActions.ScheduledActionData(
                            aVisit.VTD1_Scheduled_Date_Time__c.addHours(-2), aVisit.Id, 'SendReminderBefore2Hours')
                    );
                } else if (aVisit.VTD1_Scheduled_Date_Time__c <= now.addHours(2) && aVisit.VTD1_Scheduled_Date_Time__c > now.addHours(1)) {
                    scheduledActions.add(new VT_R5_ActualVisitScheduledActions.ScheduledActionData(
                            aVisit.VTD1_Scheduled_Date_Time__c.addHours(-1), aVisit.Id, 'SendReminderBefore1Hour')
                    );
                }
            }


            if (isStatusToBeScheduledOrRescheduled(aVisit, oldVisit)) {
                scheduledActions.add(new VT_R5_ActualVisitScheduledActions.ScheduledActionData(
                        aVisit.To_Be_Scheduled_Date__c.addDays(-5), aVisit.Id, 'SubTypeToBeScheduled')
                );
            }


            if (isLabsPSCCompleted(aVisit, oldVisit)) {
                scheduledActions.add(new VT_R5_ActualVisitScheduledActions.ScheduledActionData(
                        now.addDays(1), aVisit.Id, 'LabsPSCCompleted')
                );
            }
        }
//        if (!scheduledActions.isEmpty() && Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()) {
//            System.enqueueJob(new VT_R5_ActualVisitScheduledActions(scheduledActions));
//        }
        if (!scheduledActions.isEmpty()) {
            new VT_R5_ActualVisitScheduledActionsEvent(scheduledActions).publish();
        }

    }

    // PB Name: Schedule Study Team Visit Task
    // PB Description: Schedule Study Team visit task for Patient
    private static Boolean isTimeSlotChosen(VTD1_Actual_Visit__c aVisit, VTD1_Actual_Visit__c oldVisit) {
        return aVisit.VTD1_Status__c == REQUESTED
                && (aVisit.VTD1_Visit_Type__c == TYPE_STUDY_TEAM || aVisit.VTD1_Unscheduled_Visit_Type__c == TYPE_STUDY_TEAM)
                && aVisit.VTD2_Schedule_Request_Submitted__c != null
                && (aVisit.VTD2_Schedule_Request_Submitted__c != oldVisit.VTD2_Schedule_Request_Submitted__c
                || aVisit.VTD1_Status__c != oldVisit.VTD1_Status__c);
    }

    // PB Name: Pop Up Notifications for scheduled visits (9 diamond)
    // PB Description:

    private static Boolean isConsentScheduled(VTD1_Actual_Visit__c aVisit, VTD1_Actual_Visit__c oldVisit) {
        return aVisit.VTD1_Status__c == SCHEDULED
                && aVisit.VTD1_Onboarding_Type__c == 'Consent'
                && aVisit.VTD1_Status__c != oldVisit.VTD1_Status__c;
    }


    // PB Name: Pop Up Notifications for scheduled visits (6 diamond)
    private static Boolean isConsentOrReconsentVisit(VTD1_Actual_Visit__c aVisit, VTD1_Actual_Visit__c oldVisit) {
        return !EXTERNAL_REMINDERS_IS_ACTIVE && aVisit.VTD1_Scheduled_Date_Time__c != oldVisit.VTD1_Scheduled_Date_Time__c
                && (aVisit.VTD1_Onboarding_Type__c == 'Consent' || aVisit.VTD1_Unscheduled_Visit_Type__c == 'Consent'
                || aVisit.VTD1_Unscheduled_Visit_Type__c == 'Re-Consent') && aVisit.VTD1_Status__c == 'Scheduled';
    }

    // PB Name: Visit Alerts Common
    // PB Description: Common logic for all visit reminders
    private static Boolean isNeedReminder(VTD1_Actual_Visit__c aVisit, VTD1_Actual_Visit__c oldVisit) {
        return aVisit.VTD1_Status__c == SCHEDULED
                && aVisit.VTD1_Scheduled_Date_Time__c != null
                && (aVisit.VTD1_Status__c != oldVisit.VTD1_Status__c
                || aVisit.VTD1_Scheduled_Date_Time__c != oldVisit.VTD1_Scheduled_Date_Time__c);
    }

    // PB Name: "Visit Processes" Diamond: "Status to be Scheduled/Rescheduled"


    private static Boolean isStatusToBeScheduledOrRescheduled(VTD1_Actual_Visit__c aVisit, VTD1_Actual_Visit__c oldVisit) {
        return aVisit.VTD1_Status__c != oldVisit.VTD1_Status__c
                && (aVisit.VTD1_Status__c == TO_BE_SCHEDULED || aVisit.VTD1_Status__c == TO_BE_RESCHEDULED)
                && (aVisit.Sub_Type__c == 'Home Health Nurse' || aVisit.Sub_Type__c == 'Phlebotomist')
                && aVisit.To_Be_Scheduled_Date__c >= System.today().addDays(5);
    }



    // PB Name: "Visit Processes" Diamond: "Labs PSC Completed"
    private static Boolean isLabsPSCCompleted(VTD1_Actual_Visit__c aVisit, VTD1_Actual_Visit__c oldVisit) {
        return aVisit.VTD1_Status__c != oldVisit.VTD1_Status__c
                && aVisit.VTD1_Status__c == COMPLETED
                && aVisit.VTD2_Common_Visit_Type__c == TYPE_LABS
                && aVisit.Sub_Type__c == 'PSC';
    }
}