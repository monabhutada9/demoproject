@isTest(seeAllData = false)
public class VT_R5_BatchUpdateScPrimaryLanguageTest {
    
    @testSetup
    static void testDataSetup(){
        Id adminId = [Select id from Profile where Name = 'System Administrator'].id;
        User systemAdmin = new User(ProfileId = adminId, username = 'testUserNameR5@test.com', email = 'testUserNameR5@test.com',
                                   lastName = 'testLastName', isActive = true, CommunityNickname = 'testUsernickName1',
                                   Alias = 'testName', TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', 
                                   EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert systemAdmin;
    }
    
    @isTest
    static void testBatchPositive(){
        
        Id scId = [Select id from Profile where Name = 'Study Concierge'].id;
        
        
        
        User systemAdmin = [Select id, Name, username from User where username = 'testUserNameR5@test.com' limit 1];


        User studyConcierge = new User(ProfileId = scId, username = 'testScUserNameR5@test.com', email = 'testScUserNameR5@test.com',
                                   lastName = 'testSCLastName', isActive = true, CommunityNickname = 'testSCUsernickName1',
                                   Alias = 'testSC', TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', 
                                   EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');

        VT_D1_UserTriggerHandler.byPassUserTrigger();
        insert studyConcierge;
        Test.startTest();
        System.runAs(systemAdmin){
                VTR5_BatchUpdateScPrimaryLanguage.runBatch(100);
        }
        Test.stopTest();
        System.assertEquals('en_US',[Select id, VT_R5_Primary_Preferred_Language__c from User where username
                                  = 'testScUserNameR5@test.com'].VT_R5_Primary_Preferred_Language__c);

        
    }

}