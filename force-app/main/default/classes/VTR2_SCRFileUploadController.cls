/**
 * Created by Alona Riabchenko on 19/03/25.
 */

public with sharing class VTR2_SCRFileUploadController {

    @AuraEnabled
    public static Id saveTheChunk(Id docId, Id parentId, String fileName, String base64Data, String fileId) {
        return new FileManager().saveTheChunk(docId, parentId, fileName, base64Data, fileId);
    }
    @AuraEnabled
    public static String getUserId() {
        return UserInfo.getUserId();
    }
    @AuraEnabled
    public static String convertToNewVersion(Id docId, Id contentDocId) {
        List<ContentDocumentLink> cdlList = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: docId];
        ContentVersion uploadedVersion = [Select Id, Title, PathOnClient, VersionData, ContentDocumentId, ContentDocument.Id from ContentVersion where ContentDocumentId =: contentDocId limit 1];
        Id finalDocumentId = !cdlList.isEmpty() ? cdlList.get(0).ContentDocumentId : null;
        ContentVersion finalVersion = new ContentVersion(
                            Title = uploadedVersion.Title,
                            PathOnClient = uploadedVersion.PathOnClient,
                            VersionData = uploadedVersion.VersionData,
                            IsMajorVersion = true,
                            ContentDocumentId = finalDocumentId
                        );
        insert finalVersion;
        delete uploadedVersion.ContentDocument;

        if (cdlList.isEmpty()) {
            if(finalDocumentId == null){
                finalDocumentId = [Select ContentDocumentId from ContentVersion where Id =: finalVersion.Id].ContentDocumentId;
            }
            ContentDocumentLink cdl = new ContentDocumentLink(LinkedEntityId = docId,
                    ContentDocumentId = finalDocumentId,
                    ShareType = 'V',
                    Visibility = 'AllUsers');
            insert cdl;

        }

        return finalDocumentId;
    }
    @AuraEnabled
    public static DocumentData getDocumentById(Id documentId) {
        return new DocumentData(documentId);
    }

    private class FileManager {

        public FileManager() {}

        public Id saveTheChunk(Id docId, Id parentId, String fileName, String base64Data, String fileId) {
            if (fileId == '') {
                fileId = saveTheFile(docId, parentId, fileName, base64Data);
            } else {
                appendToFile(fileId, base64Data);
            }
            return Id.valueOf(fileId);
        }

        private Id saveTheFile(Id docId, Id parentId, String fileName, String base64Data) {
            List<ContentDocumentLink> cdlList = [SELECT id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: docId];
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
            ContentVersion cv;
            if (cdlList.size() > 0) {
                ContentDocumentLink cdl = cdlList[0];
                cv = new ContentVersion(
                        Title = fileName,
                        PathOnClient = '/' + fileName,
                        VersionData = EncodingUtil.base64Decode(base64Data),
                        IsMajorVersion = true,
                        ContentDocumentId = cdl.ContentDocumentId
                );
                insert cv;
                return cv.Id;
            } else {
                cv = new ContentVersion(
                        Title = fileName,
                        PathOnClient = '/' + fileName,
                        VersionData = EncodingUtil.base64Decode(base64Data),
                        IsMajorVersion = true
                );
                insert cv;
                ContentDocumentLink cdl = new ContentDocumentLink(LinkedEntityId = docId,
                        ContentDocumentId = [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId=:cv.Id].Id,
                        ShareType = 'V',
                        Visibility = 'AllUsers');
                insert cdl;
                return cv.Id;
            }
//            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
//            ContentVersion cv = new ContentVersion();
//            cv.VersionData = EncodingUtil.base64Decode(base64Data);
//            cv.Title = fileName;
//            cv.PathOnClient = '/' + fileName;
//            if (parentId == null) {
//                cv.FirstPublishLocationId = docId;
//            } else {
//                cv.ContentDocumentId = parentId;
//            }
//            insert cv;
//            return cv.Id;

        }

        private void appendToFile(Id fileId, String base64Data) {
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
            ContentVersion contentVersion = [
                    SELECT Id
                            , VersionData
                    FROM ContentVersion
                    WHERE Id = :fileId
            ];
            String existingBody = EncodingUtil.base64Encode(contentVersion.VersionData);
            contentVersion.VersionData = EncodingUtil.base64Decode(existingBody + base64Data);
            update contentVersion;
        }
    }

    public class DocumentData {

        @AuraEnabled
        public VTD1_Document__c document;

        @AuraEnabled
        public String pdfData;

        @AuraEnabled
        public String contentDocumentId;

        @AuraEnabled
        public String documentDownloadUrl;
        private final Integer maximumSizeForPreview = 25000000; //Apex heapsize is 6mb but it allows to pass about 20mb ???
        //private transient Set<String> supportedFormats = new Set<String>{'pdf', 'txt'};
        private transient Set<String> supportedFormats = new Set<String>{'pdf'};

        public DocumentData(Id docId) {
            this.document = this.getDocument(docId);
            ContentDocumentLink contentDocumentLink = this.getContentDocumentLink(docId);
            if(contentDocumentLink != null) {
                String fileExtension = contentDocumentLink.ContentDocument.LatestPublishedVersion.FileExtension;
                Integer fileSize = contentDocumentLink.ContentDocument.LatestPublishedVersion.ContentSize;
                this.contentDocumentId = contentDocumentLink.ContentDocumentId;
                this.documentDownloadUrl = this.createDocumentDownloadLink(contentDocumentLink);
                if (this.supportedFormats.contains(fileExtension) && fileSize < maximumSizeForPreview) {
                    this.pdfData = this.getContentAsPDF(contentDocumentLink);
                } else {
                    this.pdfData = this.getUnsupportedFormatWarningAsBase64Pdf();
                }
            }
        }
        private VTD1_Document__c getDocument(Id docId) {
            if (docId == null) return null;
            List<VTD1_Document__c> document = [
                    SELECT
                            Id,
                            VTD1_Does_file_needs_deletion__c,
                            VTD1_Deletion_Reason__c,
                            VTD1_Why_file_is_irrelevant__c,
                            VTD1_Eligibility_Assessment_Status__c,
                            VTD2_Previous_PI_Decision__c,
                            VTD2_Final_Eligibility_Decision__c,
                            VTD2_TMA_Eligibility_Decision__c,
                            VTD2_TMA_Comments__c,
                            VTD1_Clinical_Study_Membership__r.Contact.Name,
                            VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c,
                            VTD1_Study__r.Name,
                            VTD1_Name__c,
                            VTD1_FileNames__c,
                            VTD1_Version__c,
                            VTD1_Nickname__c,
                            VTD1_Status__c,
                            VTD1_Comments__c,
                            VTD2_Patient__r.VTD1_Patient_Name__c,
                            VTD2_Patient__r.Name,
                            RecordTypeId,
                            RecordType.Name,
                            VTD1_Regulatory_Document_Type__c,
                            VTD1_Document_Type__c
                    FROM VTD1_Document__c
                    WHERE Id = :docId
            ];
            if (!document.isEmpty()) {
                return document[0];
            }
            return null;
        }

        private String getContentAsPDF(ContentDocumentLink contentDocumentLink) {
            String sandboxPrefix = Url.getCurrentRequestUrl().getPath().toLowerCase().split('/')[1];
            if (sandboxPrefix == 'pi' || sandboxPrefix == 'scr'){
                sandboxPrefix = '/' + sandboxPrefix;
            }
            else{
                sandboxPrefix = '';
            }
            PageReference pageRef = new PageReference(getSandboxPrefix() + '/sfc/servlet.shepherd/document/download/' + contentDocumentLink.ContentDocumentId);
            String data;
            if (!Test.isRunningTest()) {
                data = EncodingUtil.base64Encode(pageRef.getContentAsPDF());
            } else {
                data = 'TestData';
            }
            return data;
        }

        private ContentDocumentLink getContentDocumentLink(Id docId){
            if (docId == null) return null;
            List<ContentDocumentLink> contentDocumentLinks = [
                    SELECT
                            ContentDocumentId,
                            LinkedEntityId,
                            ContentDocument.LatestPublishedVersion.FileExtension,
                            ContentDocument.LatestPublishedVersion.ContentSize 
                    FROM ContentDocumentLink
                    WHERE LinkedEntityId = :docId
            ];
            if (!contentDocumentLinks.isEmpty()) {
                return contentDocumentLinks[0];
            } else {
                return null;
            }
        }

        private String getUnsupportedFormatWarningAsBase64Pdf() {
            return 'JVBERi0xLjUNCiW1tbW1DQoxIDAgb2JqDQo8PC9UeXBlL0NhdGFsb2cvUGFnZXMgMiAwIFIvTGFuZyhlbi1VUykgL1N0cnVjdFRyZWVSb290IDEyIDAgUi9NYXJrSW5mbzw8L01hcmtlZCB0cnVlPj4+Pg0KZW5kb2JqDQoyIDAgb2JqDQo8PC9UeXBlL1BhZ2VzL0NvdW50IDEvS2lkc1sgMyAwIFJdID4+DQplbmRvYmoNCjMgMCBvYmoNCjw8L1R5cGUvUGFnZS9QYXJlbnQgMiAwIFIvUmVzb3VyY2VzPDwvRm9udDw8L0YxIDUgMCBSL0YyIDkgMCBSPj4vRXh0R1N0YXRlPDwvR1M3IDcgMCBSL0dTOCA4IDAgUj4+L1Byb2NTZXRbL1BERi9UZXh0L0ltYWdlQi9JbWFnZUMvSW1hZ2VJXSA+Pi9NZWRpYUJveFsgMCAwIDU5NC45NiA4NDIuMDRdIC9Db250ZW50cyA0IDAgUi9Hcm91cDw8L1R5cGUvR3JvdXAvUy9UcmFuc3BhcmVuY3kvQ1MvRGV2aWNlUkdCPj4vVGFicy9TL1N0cnVjdFBhcmVudHMgMD4+DQplbmRvYmoNCjQgMCBvYmoNCjw8L0ZpbHRlci9GbGF0ZURlY29kZS9MZW5ndGggMzYwPj4NCnN0cmVhbQ0KeJytkl9LwzAUxd8L/Q7nMRk2y78mKQwfnHMoCIoFH8SHsWY62Nq5dfr1TbYxNhUUWZ7Sm5N7zu+m6N6h1+ve9q8vwc/PcXHZx1uacMbjcs4YcOSFZoWB05JxjaVPk8cO6jS5KNOkeyWgNvVykiYiqDlCxcDq0ESinAfN8MHiZRXb6kLjZVNxB5VhmiCLcitQjp8I9os+o7xJk0Fwuk+TEyQTMtzQh+n2ofZZQoBjXwxu+8DBqMRuVCLeE7xguYPleeysbM5sgWCj3SbRpPNDA/m/WUtI/o1oZy9EtN8SyS2P3NKUr9MVqmZMDVnPPc1J3WLSLOfhe9QinNVNi9U6HCyoIouGZposW19RFwZhSJB+mYdQijkJYwsWQitpmVa/EasTEu/sXR7tfyJeLGkmiX+f+g9qCcP1JICh9lQTX/nqLGItZn608nFXNR81zRSZBW0zqn55e306EG0Ek+YY5G8/4yfdeMKFDQplbmRzdHJlYW0NCmVuZG9iag0KNSAwIG9iag0KPDwvVHlwZS9Gb250L1N1YnR5cGUvVHJ1ZVR5cGUvTmFtZS9GMS9CYXNlRm9udC9BcmlhbE1UL0VuY29kaW5nL1dpbkFuc2lFbmNvZGluZy9Gb250RGVzY3JpcHRvciA2IDAgUi9GaXJzdENoYXIgMzIvTGFzdENoYXIgMzIvV2lkdGhzIDIwIDAgUj4+DQplbmRvYmoNCjYgMCBvYmoNCjw8L1R5cGUvRm9udERlc2NyaXB0b3IvRm9udE5hbWUvQXJpYWxNVC9GbGFncyAzMi9JdGFsaWNBbmdsZSAwL0FzY2VudCA5MDUvRGVzY2VudCAtMjEwL0NhcEhlaWdodCA3MjgvQXZnV2lkdGggNDQxL01heFdpZHRoIDI2NjUvRm9udFdlaWdodCA0MDAvWEhlaWdodCAyNTAvTGVhZGluZyAzMy9TdGVtViA0NC9Gb250QkJveFsgLTY2NSAtMjEwIDIwMDAgNzI4XSA+Pg0KZW5kb2JqDQo3IDAgb2JqDQo8PC9UeXBlL0V4dEdTdGF0ZS9CTS9Ob3JtYWwvY2EgMT4+DQplbmRvYmoNCjggMCBvYmoNCjw8L1R5cGUvRXh0R1N0YXRlL0JNL05vcm1hbC9DQSAxPj4NCmVuZG9iag0KOSAwIG9iag0KPDwvVHlwZS9Gb250L1N1YnR5cGUvVHJ1ZVR5cGUvTmFtZS9GMi9CYXNlRm9udC9BQkNERUUrUHJveGltYU5vdmEtU2VtaWJvbGQvRW5jb2RpbmcvV2luQW5zaUVuY29kaW5nL0ZvbnREZXNjcmlwdG9yIDEwIDAgUi9GaXJzdENoYXIgMzIvTGFzdENoYXIgMTE5L1dpZHRocyAyMSAwIFI+Pg0KZW5kb2JqDQoxMCAwIG9iag0KPDwvVHlwZS9Gb250RGVzY3JpcHRvci9Gb250TmFtZS9BQkNERUUrUHJveGltYU5vdmEtU2VtaWJvbGQvRmxhZ3MgMzIvSXRhbGljQW5nbGUgMC9Bc2NlbnQgOTIwL0Rlc2NlbnQgLTIxMC9DYXBIZWlnaHQgNzkwL0F2Z1dpZHRoIDQ0Ni9NYXhXaWR0aCAxMjkxL0ZvbnRXZWlnaHQgNjAwL1hIZWlnaHQgMjUwL1N0ZW1WIDQ0L0ZvbnRCQm94WyAtMTcyIC0yMTAgMTExOSA3OTBdIC9Gb250RmlsZTIgMjIgMCBSPj4NCmVuZG9iag0KMTEgMCBvYmoNCjw8L0F1dGhvcih1c2VyKSAvQ3JlYXRvcij+/wBNAGkAYwByAG8AcwBvAGYAdACuACAAVwBvAHIAZAAgADIAMAAxADYpIC9DcmVhdGlvbkRhdGUoRDoyMDE5MDkwOTA4MTk0MyswMycwMCcpIC9Nb2REYXRlKEQ6MjAxOTA5MDkwODE5NDMrMDMnMDAnKSAvUHJvZHVjZXIo/v8ATQBpAGMAcgBvAHMAbwBmAHQArgAgAFcAbwByAGQAIAAyADAAMQA2KSA+Pg0KZW5kb2JqDQoxOCAwIG9iag0KPDwvVHlwZS9PYmpTdG0vTiA3L0ZpcnN0IDQ2L0ZpbHRlci9GbGF0ZURlY29kZS9MZW5ndGggMzA0Pj4NCnN0cmVhbQ0KeJyNUsFqwkAQvQv+w/zBZKPRCiKUqrSIIonQg3hY4zQGk11ZN6B/3xkTMRQPvezOe/vey8wQNYAA1AgiBSoCFbyBCkENuOxBGA1A9aE/YmbIigjGY1yLLoAYE1zj5nYmTLyrUj8rqMTFFoId4DqDnmgmk27nHxYFIev7L43hw6idf+mV/mOZgK8dNMaWcOOIYms9xragpT7LYBLJgWTurzKjMJIW1TGt1xVd/YJuoJroOWcZ6wlXcszM4Qk2LN3bKyaUevwkfSBX1+J51F+myA0lRy0dCvFuOEH73JoGO5//aC7u6Nu6097aE05tWpXc0525HIl8vZOlTp1t4Y8jny08zXVhsxaRFPmBWtr6OyzLnC5xnmeVo2bWVVVetvJ/DJ/b/bPyl9cOup1fSH6yGQ0KZW5kc3RyZWFtDQplbmRvYmoNCjIwIDAgb2JqDQpbIDI3OF0gDQplbmRvYmoNCjIxIDAgb2JqDQpbIDI1NyAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMjQ1IDAgMjQ1IDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAyNTkgMCAwIDAgMCAwIDAgMCAwIDAgMCA1NzcgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgNTM2IDAgNDk3IDU4MSA1NTcgMzA4IDAgNTY4IDI0MSAwIDAgMjQxIDgzMyA1NjcgNTczIDU3OCAwIDM0NyA0NzIgMzE5IDU2NyA1MDMgNzU0XSANCmVuZG9iag0KMjIgMCBvYmoNCjw8L0ZpbHRlci9GbGF0ZURlY29kZS9MZW5ndGggMTAxNjAvTGVuZ3RoMSAzMjE0OD4+DQpzdHJlYW0NCnic7T0JfFTF+d8387I5SELuzUWyuQ9yHyQEEkIg4Qg5OEWOxgABAtkkJuEU5RCVgmiLClhpK3iLWqRglVq1LWrhb9GKIlKwGqxYRRQPoCK7/2/mvd3sbrIk4bDt78d8zJt5733XfPPNzDcv+x6AANAXVgCH5rKyCRWzp8StA7h5MV0NHzG8tCzsvsjhANmv0PmUEdVV45f9ZviXADnhlCNGjJ9YMjnIuBpg7mCAwIKq8elZzek/3QmAgv6GmcbaZn3dt18B+J4EYIfm1LY2m80kCbKfp/tucxqWzH610asKIGAv4eyYW1c76zv3t1bSvR8oD5hLF1wrdT8nfjl0HjvX2Lb4VvDYT+eHAXwCGppm1s6pnN8GEE/ZrdpYu7hZMUIh3Z9M+IbGWmPd5+aYRQBpxF/5W3NTa5v5Bggg+ZvFfRBtZ/Bq6tM/WVbTd/B30Id/SldgX+hrWZbSPMF0QqnhdxKuG+Gqiej4ZtMh4nmreYJ5glIjOdkkflxcURKgABR5gUkqgVWsoTB8iU0GF6q8yNbS+Z1qiccgCysEgYuOi0J5ENiX1fDheQtvY0NrK+nudV7hQeY0oQkmU1N+LRU7zh6QLVPwCxghr1RoKgvZ30Eu3gQZ0JP0HeTYlT9CQq7qbEl8mP25bWJDIby3/FkwJEg5pyGelVJOhXjyLC95Lx+Gw2nIsyKfhuF8KAxneZQLCbeE7gvaqTCcvC8Uh4C7uIdnzSb2tejHy094C8RcFv19EH0l9LiWrqVrSY6nAf9pHa6la+laupauRsKN1+a3a+la6nG6D0JldkyboJ/MWsItTuLw72EA7ceC4APaSzom2ptdikpstFaOpD2KkfYqnPYk36l7FbaaSq0u9XqL7n9MeSXlDYQfruFlEo0ZgvFD0LNldI948r7azvW/JLF68GJxauZT1NwJh/Z0osR+HW3+sRNv0PaT40jH2zvr+B9OXMvh6hML9KQzlOcKfEVlvnh2AMFUi4YUSINRUAEToBZmQz00wyJ4EvawHYblZ9rPfHrm8zPfnI06W3t29tn6s81nF51XzGYpw2BHORPmQgO0WCk/OnOCKE9Lypln555tONtyHogSzR93CX82/9m0td3Ds02Dcs8QC7jtcnzu0oukw/2y/LATD7Q+5+kuqZQK7f51Xdz1oex3SbqlamWm3dV8p/illEfASLK5mA3GgPrMpxrGwjgYT70AMAmug8lwPUyBqZek0ZVPRrj58pnw3fw5EE/JRpgfxUfZV+TNrgAuUb5R3DfKFx81bcUag+nDKXz0D+9w7x++lri55jPwCnuAcGl05sXwbBYdn5szICsoMODxqf4rYvOTkvKT38A3L0xNCu+XlNQvnGgy8A4sZ4+AO/gD+McI/Gwi0Ftrt2bERIcEx6xKF0U03vG2b3BMTPBxtRCqIuSYb4LXYKXwi7wAnWtCEebpYqLjJf1r2f3TQ5Jd3N1CoxPDDWnxWVMjEvoOCOEx+UmZKu138ArRUuuCBElWUMDG/OTk/OQ+SQVJSQWSv3hW9qJsF2TH+Ga/uGCBeBgor7MfqOoJEBXjy2O4L90Nw2z2w4K/Zr2x4JHdzz2CX5p8RWYPmEbh74gmnGjiiMZV0sTkRgXG4NQF+5n7fpYzZw5IOyaYv8V38EuaLaIFVq5ojLSGPjcnPibaNUEahmfl5dKZLiAI36nPn1Uyu27YnNyi7LFF/cctub75aFVsbFVqQUFO8cTyiuqikckZhaMqJ0w1/ZA1KiujMl22K57kvE9yAqhnLRJcVZbZWXl6HRoa77yzcUlxaeKIkurqktLk0mLPu5sa755UPPD6u6YMLJ5EuhIPhtIGwaquapfrc7N51oC83DSU7BjWD35p8eKXBtd/Elfm51cWVzR6dEnlmgVta6vOX3guNvyV8Lgpd10PGj930slfRAMWrYL6YVQEZhNDvc5fsnTFmMa7182bf6tpnW5cSVx5SMYoF1Y6tKhM8Vx/47x77m0NGTgmPKS0DOOyKiuyqa3kkTiP/CxQ9mFMbrZNc/thdmDMa/XV1ePLypOS9FGRsTNm4B3TwiqvTx4amJQYNo3oad3Db6mdfrLfRDfoXEV3BxaRXoH4bX12elxJYH192NTcLZiRmxofZvoNe+B7Q9r9ap+K558vsAfJz0kTf805A3T+xGJxaEJCaFh8fGh9PYuIHxBP/y6043HVt8V6+3SH7z1NOA9o19kFslOEqo+mTo6dWnqpG7tQXxVV5FtfbSj0q6/3uz6rPkw/JXsdDrouPtT0jChIVTxeEpZ9fWzuBo03fkO8/ex4qzyDAvGb+tr4kgBq6pTciipMm6XRG9Kqy9W2ij7UE70HRWSiD9WBHKBLyBogHVgXSD4b0bxmTfONa9bcOLS8fGhxebnnPYc2bJjfsGH69OHDpk8fNny6ymu4eSSLIF5eECJ4JQQKRVTbETu1HwIjEEfeMKi+vqC2ZtGiW8i3imKFl3lWnWcPkJt99tPp5F3xYS+HxVtsiqPxlOiJbK1dgZIRWTd8Qqq3f1S5Dx4fEZs2lyeEEH4ojZOdpEMsgD46IU96Ym5OGtdcSDpkYECEQk3816qKjLBpqWmZgydNGjx7clNFin5MSlJm/6Fjhvaf51k+Jjwxtl9oP9+Q4uyiicNHhxqiQgNCPAPDh6SWTFD7251k5bM5wk/15P3SU7MDyT8DtUH5buGQ6ur6GTOCgwx9Q6I8iwejftrq1dMuhEb6VkVqPkM8viEH8rXMHrm+2b6CfECebzZ13/A5D9RnpYs+LB5zP04yvZWbGhuG4wWt2USHfxOtTtByMZthzo2vNq9ffyMeN23EeYK/+NvKF4Qj570+qKKJee+6xubmxo3G2a3TWmcbcYbpV0RDC4XIoi9jsAhHsDWgF/MaqvNap2nNVZvWhI+MqIgbljJsWMrw2CRDXmJk/rjCio2ZwcGZ4XFxWBaVlDcjNzEtzNA/Kyd/yLaoFENEcEiEkBNNclpJjrqmWMdavHQ/wXl/+fTp5RXTpi0cl5ExLh2Lau6soX9D8tNS8/JS06QNB2AZ1rBz0FdqSmPA1eJ5gVhTOSQtqqIipWwelv1pSMIR3Lktq95Ks6ZrmoohGZGVlSmlRPPKUKIpSJY0oeYIWGR+TYw1fQC5kli8XC3zfdCiAl0w845AxnzDwvwDwm4cmeTiz9wMXhjq5x8WIehxIizCC4Le30If4+qMHl9zYCDb2s88D5aSDmINDNDJVXBpWHx4ePzE+LCw+DAVB7fCUpJDOH52ONiuIal2ryObeYu5G+3WrWyxWmkDJQjrKhKLhowdO6QooXJLbmifVH1CRgaWJWVOriifkrXNdGvsHX0DB6alD1RtCifJpn1EdCCNGZhAI3V1ZWXqIH1QpgeZvCAlpwz1fdW2BJEO/QnfIHpeG6sdQ1V1rAhGI3XbhPwov8ER/aISBw5MLB08LjUyanBYZOHIwpFYlJsREBYUG9jHJ5lYpKbFBfn6B7inJacMVGW4YREcZofEGPWPLkSbIUojNAL1uvsm5+ZWDh0aEeDBvFggFk3cnT9pUv7jsR5u0e6BoMVKFHeweJrXvYkLxQ7+AZq5soJ2+0zwXSFCpOT8JBYvRp0pUMZLSUmg/sWTdqdE60ozLPhn+8ckRLnGxCW4Hl99fGmyyZR0E95eW8viLxyZM+e1r74Cy1rPclmWzYxsnbiC1IUVq1Y88cSKTVVTgkanVM+bV50yJmBKpefWH7auTI3p3zB+nLF/dMpKdU6mwwiS76bOML62s3t/9MWII+/f8dC2NZNn1Ewxsvj7b1q6uWZ2RdVs02Zt7n2XaKk/k9A3KlADfNfUhD83HcUIUzvGsvia5TVrrXO1wHeXc7WG/azRgisxLethsco3SkZV5CtRudms2Gj6wGjEaONnn7H4Y8esa+qz0vZy/n/WSFpaZD1DVRepWzbNlc+YNhovUBcMtNCxB+l+p3jvQePjmU8YN2x54B5cblopMtH8EmdqNIWq/lExKAiIMytsMh03tmGwEZeZVhPuXdiirZvzqY/ctYgsx7rOqcsmjZxRK7dtW7li27YV1TNnVo+dOdPzoXMP0b8bjOPGNzQ82gDW/lFIpoeYFYTVZP/0xzyxYPbHQDKfMnnWjCmb1iWnpSWvY/F1o8vralJHpmztoPcgei/h4VExCTGBFh6uCRoL5nHLshpic11dZUFxcnpG0rrXX2fxs8rLJrtN1BghBNM61E7tibFfNxXruikHIye+pnvH5UTW5g4cmDlq4qjMRdNum5AbOWVy3vAbaocv9JwwwdA/2RAa3Te4X0lGWU1VZURUfERoZEDk6ILKnwg5ejrsk+OhI6aON27DnQ/h6zU11jWR5ROOjxpVygWRtBGlDotuWt446vZRjSs8l9ZjlWmPCEmwzLRz3lLQ9qJyvNmsiXNWH1/2xPal1HVj8TcCR6E9Ux+Lb9iuiXUtDQ0tjyyZ33Zda8MSvM10E9HMx/tM83CjsDXFIOxXMi7t7ywyxR7FqzkXiV9NS+zDWXWHhl9IuZGif7qWnGcXpwqJE0qzo/Q+oULCJx1hq+BeNdm/so/gvskmhJXtw4+knJCLtM+RN4Y5MDUtkb+/UAQ/Hm3Ve6DW491o3rUF8Xt7oSc6GnRhR2drdtVAUz97w0q78iCtvenOtMMe64Th3SgjDHOhzqF7Lfsyill7ti8bNK/TvmyYqTE2/OXwuOvFvkyNKaPVva7eGk/SqvHKsvrgoEgRiS5j/S98qUWhst95orRD4kX63XEf5eAGbIbdtsq+3Y67LJJJbZ4kZUY7lWmru6PTpVnaYi/JaNsyOXbM3/J1mpx8p31sKymuZ52dblHgwrSLdbmNQp6OPR9khQwY1wU0E6y6JLhLg8ftYI9TOAWn0AfzcRauJngYX5HwBn7PQmmlnkewij3PjrBz3IPH9wCmdAPLrsH/BGy8BtfgR4Qd1+C/Et7iX1tAUZQgCcU2MFlZqzxlhf0XgY+Vj108XBJdil0mEMxzWSjhdpcHXZ5zOeLyvcv3ugjdON0s3SrdvbodVwhOuhpcR7pudj1qATeFIPyyYBLBWrfD/z1A8UQ4TrL+HS/PshuiY186U+sM3GC8VueQDFOtf70LhDKt7kLx4G1aXUe72w1a3RVS4VGtLp6Me2v1PhAGB7W6p03dGwZRXKHWfeiOhdZX6sABFdpnQyMmaXWECNyk1Rn44JtancME/IdWVyAZd2t1Fwhl2VpdB4msQqu7whS2QKu7QyiWavU+MIBZeHra1L1hIXfR6j4QbKX1FToMa2pe0lI/Z26bIXFmkqGitmW+YXy9samxtakxxZCVkZGbZhja0GCQKK2GlrrWupaFdbPSqluaFtcbaw2VTQtrDWPa7E7H1xnrZzQ1zBLMLLwKDHYopYvbWmoFToGUMamupbW+qdGQlZaRka0hCrxUCys74vpWQ62B6GfVGYW6TbPt1U6b29bWXJCevmjRojSB0Gq5PrPJaHurNbW1bcGs+iZxPb2sqbFtfG1DXWu6sbW1oX5mXWNrXe2clro6Y11jG3E0NsAwaKJwdQm0QD3MgbnQBgbaTsyEJCoroJauz6faeLprJMxGaJXHFLqWReFvBuRCGtWHQgOBwYZLqzyro7KOyoV0nEWY1VRvgsWSWy1hVNLZQlkbQzTO744neiNdn0FXGoiTRTNHvQoI1zmXUrraRndrrXwKbNoxSWraSnSCk7ieJu9kO3C08EvtpJVzyfXSHqKmyp8lKS3WbYLZF7V2mrRoG/VTAW1402GRhDQrh9ZO+DOpNDqlaiXdW+nOAtJDtNaCn06TiaBvIw1qqUWi99Ilfiud1RNWnZRRR3fnyN4VraiTFKqORmhQ5yiZzNPFU7YuE5rNNFugnHuA9kxijutD57dQXg4r6WwVo5mHPcaeAsaeZm9Q/a/sANXf5L8F5Lv488D4C/wo1Y/xj6jersSrz4i0X0AI3h4QQa6KDbVtjTR7yd8EiV9riLvz61oa5S+Z5BndY5LSAwSXIImLpBtjp8CDnTYfEG/AsOeAZewX9wyDE1fBCDIsmLeaT5pPm4+Zd1PtGNWPQV+qnTG3m182HzQfoOMZun/A/I35FJUnBYb5oBOr9DCRrHbJ66B5B+WT5na6tpfyafMWknSA7p+hs8MOVAfpqsflSZZ8zjicn5YtbHeKf9qRotcSO9lLtI3aLVtoPnF53J3IPN/VNfMF88kurp6x1rqg6oVMaSlbHuQ9pwkOOtPIua49lnnSqr3WMvMJ6aWHL0LRTl4utD3hvNed0crjYcnDpt+kRx8w75D1LnhKK5wWFGIs9U6mU10u0yt7JevlHuLtpVF6WHrBAaHh5fi2pQe7ayfZ/YQqR7Nwr72pswQxEsSscFGq8/ZlL2WecKTTZoMzl86zW5mduFJvnZdz/tWQdtC8T6vtI59w8Hq5prSTv5yUHrOXYI95j8196zxFGh6Qa1R7T0arZa4VfWedEU6SteXaZYvRmcbuWg9tYr6FWneMVswNVNtNbRFr2STZv1uoRY+abxVSqTxDWMcIdx9h7u6Cj8WbuvE6DWuPikW8tpPlhMz18nwT2WqreY2sb+lZCy41qRKtZ85n9y20yssWkxVELx6W+p7sSTsvWbfdVj+4hNlWs63j+Dwje+dLx3tdz0/ktSoXNbrpbg6z8iMJnXhrx5Ny1Ih5yY6bE/mXbF0xajtHCVcm0ajQfF+OyRPm7Q62PCyv7pY1AXvU/iO/3kKxhJjdt8r5ZCt5/T7yfeHv2ynTuOtqVGlcu5w3pD0vMs/K0SxnARmbHr6cqLen9lTnOVnrZWRyEZ6Hu5Nv8ahLiSgs40tYkrTf6nD3pOyzA1qMfUadH+Wu44DWs4+KuVbOiyeoh/fS8aDWN05Hbtet6S7WkDGaugK0996+tvjUqk66yfWmXR7PyzVVepbczajR78taq45pvn3CEqtcZOfRZYu6bedB25Wut/OAGsNq9cNijre3tpwdHpUrt4Dtao+b15B1ZYwo+3OvvHLQvJ5Wv91y5B5T7ziR2fX47K6dWy1RhYgbOqKN3qfubUS9uPdSuXfL+4qNdKcSDnfa824he+0xb7g60qy1vbZySd5hMVurd66G5M6c5Wqxp6s7V0Hymh7ibSK7bO0e7yIcDnTqz00yhu6BBp2imqsS+2u8r9gO1TIrkQ+J/cL2i+IeU9eOS+vvLvZlu+na4Z7ugK9M0ua1qyiTWtQRc6qRqphP9/VkTrg6u9OrJDMPhlrrQyEEEgmGEgzuljKR1s9Ep8/EEqHMGWHnlUt7PuR0BexFyuoOweI1JO88Sb3oTCMjBLFaO42de5tkLLe+ezzoctd9SRL3yT3DHid3u4gitKdFPfAoNaIT8494smVzdSvFNNvlrl5KVmeI7iOQLmQmyqcEV+UZsNP0YE8RZey2+/JWq4twt4ksyVd7/LyCemOLWna6c4mxlIiQqA978KyAev2SdtYiKne4Iv72cbBntr1SMy6187T69Ptqpas69/Vci8Pi+cvFIwBtj/TylbKG3Gef6UbmaU07p3+duCh15+e3h+XzoJ60c1/v22k7Hrr8q4b2pIK0EM829zpbRXoz01/antcB+yo9vXIq78edvVWZV2xOVv9+0vsI4GrOIr1Ijf9B2SE/vsgr9dfDXiYP+QsCnWKwfrUolmrDqBTvy2ZDJuWO72uqaai80l+8tWKTGCjyiw3il1EihYC3/FqD+NWXn/zlVyDo6WoY9IMiiIQoKcWSYiABkqkUX7lIl19sTJcRcYy8k0BneZAPA6EABlGEXwKFVLOlT+5Ra8U7/ZkO1xy/xNn5Cy06mS0g6iEaRMrzGBtIIU1SNLCkVK0NKgCIL5iqUEj1QsnBkhWit806TT5Ifva5D1nFNnfw6ZxUj06nPski8AHQ+kZNar1DC9X+OtLOkoW8AOoDSw6k3rDNeuobS1blFUiZaqnmftRzkdRe0fcF1pwmW5ggMYQUUbdwFKlnX9PSQaS1rpfHKM1y3jY46rdD9LLfijSbRJJci/cIDQYBSNsL/AKyFUqfA63nkikHy34cBKHql0+t/So4i18+dthbSA+TJSPQa1eYVtpqpdDdMKmVY7t0Wq9wkltA9smimtAzS/OhBJmT6a7wphKrb6HMCfK+8JdgwlG/dpKufTU3S44iPd1hmv7usjeEPh19IHrNkm19pGe5s9faZ0srE3qVbb3KMUfKEZUpx/kAgmybUuzus61Sne7z/+eTs9nQ9gtOpfKImi+4aOAq3qQENwmi9NTAS351KVQrxdetfQFk9pLgqoGaLO8hBUsItQJABBggmsBLzt6WDDbf6crQIOkS2i1+CxykfScszgkO0vgR7XQDD61dobI1aiv8ZRuCpNbhpO0QqS/agBtRu2ngZW19hwUEWNproLqB+KU56NBHm/fSSYM+GhR0AmdJrC0JNLocnxilSwA7bW1B/PrRzQE8bHpYJFen4K/1Z6gDREgQ3/12UaJo1Fm+2hQna1kyGoiR+opx2NHPA7SVN9X6ZSbxvWcDcXCXo9dSemhn6rnInjR3etGa1ZdiFV+tty22EF6TLMFfGwXqeBcelazlFGpJILVETxFMiPblbgu9s5Fj/y0qMccndMIx2IG7VW9bcLeC+qt3FXw13W0h2QqWpOrfcTWZ9FBBXWkN8sfRananu7Y50yaLZHveGbcjSnIsRfKQcaHlqiN2JmlpyarlO/PvwPCk3rDN3tQ3liySeA/dMfelnvOl9oq+z7Bmf81GAsMgrZts5di5Bzv3nSX52pyLY7TkZYvjrpXehOtOPuil0SVbVzRhY/GBqhQNX3hfpPQ5i4eJHnSlK8nUDj/rtUBZ85Y0npqtDdqVWBsNDPLc0053d+3oTXfcJba3pmVHSpI+JGKeZPL9FFkXmllGR4bUKkTezSAriyuu8pii3QV5VNuVLSlFCtE4dSR/a83SB6LXLNnWU3uSQfN359mSusOzz115lyWrfqBGdiIKi7GW4lfZ/Wzi7Dy4UuliPmq4JI7+Nj3R+ySspMaMA7vBjNSAa5Am17c0a2l/HbQyQsPowLXFS5N/0xGgrkB+VhDrWIBc7QRllk3uSGEadP6yaPdJ6JEo11J1H9w5hVNb06x6qmVHa7iWE6XW4i2pKKlvpB1wK74tcBuwtFe8rSCyvxXU5C7XObEeceu6ktwJnCX1nj1HkNxUiHQCXWlt23Pd2TbRoTdVUOORALLWcm3XI2ITpLnDm8abD2G40N0AwhG96k4jMJIw4oibl5yvfOVrMf40Kw2h2aYURtAIHQVjqP8qoYowxkINzUTi1ZBBsJhgMCwlKIRVsIZ2XWsJSuEegjK4D+4n6gdgK1E/BM/AePgtvEDUv4e/wWw4SHAzHCK4BQ4TLIcjBCvgKMFK+AA+IY6fEqyDzwjuhO8I1sM5+AHuAhPBPaigAveiK7rCfeiO7rAR+6AnbEJvDID7MQiT4UFMxXTYjZlYAs/jcBwB+3EUVsEBHIfj4F2sxVo4hHU4G97DeqyH93E+NsARbMRGOIrN2AzHsAVb4QNchIvgQ1yCS+AjvAmXQzuuwtVwAtfgGvgM1+Ja+BzvxPVwEu/Ge+EUbsSN8C1uxgfhO3wIHwETPoZPIuLTuBNdcBf+jnTdg7/HvvgSgS/+icAP9+I+9McDBCH4Fr6NofgOQT88hEcxAj/AbzEOz+A5HIzn8QIOQTNzwRLmxtxxFPNk6VjOMlkmNrFslo3NLJfl4o0sj+VhCxvIBmIrG8QGYRsrZIW4gA1hQ3AhG87m4yJmZI34a9bMWnAra2ML8WG2hC3Fx9hythyfYDvZTnySnWQncTs7x87hU8zETPg0R87xGe7CPfBZ7sm98XfchwfhHh7MQ/GPvB+PwL3cwA34Go/mifg6T+F5eIAP5EV4hBfzYvyQl/AS/IiX8lJs56N5JR7nc/l8/JQ38kY8yZfytfgFv5PvRBPfxXexOP4if4nF81f4AZbE/87/wfL5Cf4vVsg/52dZMf+3omMVirsSx8R3RNX3g2bjFuv7QT7AKqrGGGD4uKoK8UKaeFdID+o3Vbn8UqobjQcPirQ95XuUfYnCl8aLv3zmFiR3+Or7RTqtpDV+ZlbrTNg8s7a1Dn41q7HJCA/PbqmdCU821M+phecbGhcY4aWGppkNsFce99OFFnirSVw/1NQyi/ysWdTbW2sb2uDT1vrG2XCq1TizGb5tbc3IhO/pmIVAx2zU0TEHPek4AP3omIfBrQtmtGJE64LmVoxtIz6YvLSupQkzZKu85FH99pjYs7hoX4N11d6LCgLtyz/WN1IRLF+YRRljoYyNUM79KKN1lPsQlNGQeINVHH3k0Vce/eTRXx4D5NFDHt3lMUQexZxfDCOhGibDDTCXxvwa+BlshgfhcdgBH9KY/wrOkTJu6IPBaMBEzMB8LMaRWI2T8Qacq+pJrVTLIq0co5UTtHKa+hwVZ9HI3IVHmcJS2AS2jD2s3me75H1kL2rnX2vlGbXk+Vq5VOIFkvd9oICSrExQbleeV9pd3FzSXKpdGl02u7zk8rFOp4vVVeradL/Q7VfpLKX7DrX08tbKZq08oJXn1NI7TStHauVP1LfUvD9Vz/tO1spn1NInUSsbtXKXVppUOl8f9dz3Y+pRT+A4Biu4eGbFMQqjeaqTq+Td7Lh8dy9AfjVcnL/vcG57X/1qcwD5SLTmSf2kJwVKT/Jny+VZUA/xgp3i6XuIZ89P7xQvqId49vzCe6ifc7yQHuKFOeB1trt61U+jihD32Fn5aiajca7GIuJdSxTzOCjyXUsX9gX5dx/JrUFy9ifsRPDlk9kdbA2fxKfwaWwFLIMFsJCtZKvYrWw1u43dzidIrurzLDE/oowluNRGJ+MBH43Tdexedh+fyK/nU9lato7dydazu9jd7Gfs52wDu4eP/1E5cekttnYSrS51sGRnjPF2GEw+hbLMaR1cUNJ1jzm+S8xw7W1UFG+xWnEn24021QOR1isRFYbLb59b5m4LhZj5kM/pRCdkBEscP8qx2vXrbLgGahKFDUIlNUrtLZyvc8DqunWTpfzZNlKDpG/6WNeVkdYxp2J0ZXELTtdaj7PjoOI709oWd6RTraU89lMbqV1rre+B1nonWk+y43BxrW1xu9Ga11mldvBKdvAhJuN88QRFnVFUH+qaa4cH2VMxXAwv8AaKyJZQPHYTX8Zv5rfw5XwFX8lX8Vv5an4bv53fwdfwn/K1fB1Fa+v5Xfxu/jP+c76B38Pv5ffxjXwT38zv57/gD/At/Jf8V/zX/EG+lW/jD/GH+SP8Uf4Yf5w/wZ/k2/lT/Gn+DP8N38Gf5UxEmBRfunI3ruPu3IN78z7ci6JMT+7HfXkA9+eBPIiH8RCu56G8kCLPcBF3UtQZyaMolozmsTyG9+UJPI7H83qexPvzZIpD03gqn8ezeCbP4Pk8l2fzPJ7DB1BsWsB38kF8MJ+v+ChhXOGJPF0J5mf5Odps6JQgRa+EKLFKtBJDMedJ/gX/kn/Fv+an+Gn+Df+Wf8fP8H/z7/l5/gO/wE3cTPGCq+JGMamH0kfxVLwUb6Wv4qv4Kf5KgBKohCrhSj8lQolUDEqUEsf/qDDFReGKQpZ/gWY2D9p7xdEckUCzYZL2rClV/mVXPPXLoj1aDuTCALu/6RbSLuxWWA23we1wB0VWP6Ud2TraP62nfdPdFGf9HDbQ7uxe2p1thE0Udd1PO7Qt8Ev4FfyaIrCt8Gd4FV6HffB/8Fd4k3ZqB+FdeA/eh7/DMfgHfATH4Z9wAv4Fn8MX8CWchm9oX3YW/k37MpMYTrQr01Hk5oFetLfxpd1MIOppNxNGu5hIijBiaAeTgEnYX+zMaF+WjbmYhwNxEO1piijGG45lFOeNpqikkuI1He1tPGhn4818mB8LYEEsmIWycBbBDCyaxbJ4lsiSKapLYxksi+WwASyfFbDBrIgVsxLa25SxkWw0G8MqWTUbR7HfJDaZTWHT2E/YDWwGm8Vms7lsHsV1f+J/5nv5q+Ttu+C9H93Pd/ba01dpnr76Mn1dejq/087Xb/3RvH0p2XQD2Q3JcpvJgoxscD/ZUJV1Sko7TfIssn5BOna0d4cYq0ocjZT3aB0us/4fE+Xy/5gQzyo6/o+JiXb/x8Q0mA4/gYfgYXgEHoXHaNfxBDwJ2+EpeBqegd/QHuRZ2Am/hV2wG56D38HzNBZ/Dy/CH+AleBlegT/Cn2AvvAZ/gf3wBhyAt+BteAcOwWE4AkfhA9rBtMPH8AntYz6Dk3CKdjNfw7dwBs7B93AeLoAZETm6oCu6Yx/0pl2OHwZgEO11QjGc9vkGjMZYjKd9TzKmYBrtfrIwBwfQHqgAC2nXPxRLsBRH4CgsxwqswmrmwlyZO+vDvFhf5sv8WSDTsxAWxvqxSBbFYlgcS2BJrD9LZeksk2WzXJbHBrJBrJANYUPZMFbKRrBRrJxVsCo2lo1nE9l17Ho2lU1nNayWzWR1bA6rZ/P5H/jLtPt+jb/O/8L38f38//gb/K/8AH+Tv8X/xt/mB/k7/F1+iL/HD/P3+RHaox/l7fw4/5j/k39Ce/VP+b/4Z7TCDIBgjdd+4vYX4rCP+Pxd7OaJ5+vEtYPqU35UiScqsfaWUp9Od+i5V6y90WHZArLNO2w+ScoBPY3vPzjIUWXspXH/Ko184k/zrkLzrgv/o1xV+5MkeymqBG3+E9xpVPrRqAzgK+SK6kFSNH52vDyIl0ZlR2EALydaHZXfOfHqWgMhmaj9QEfWs9oIxBOJ6o72i+80gQtZV9jzU2k7F7IbWQoLpFVGQ7S0ysV6wGKdTj1h175oaauL9YplzejUOzb2QGgBlH8lF7ugZPmbjGIa02No/E6msTqLf0SRy2jS93lRQjX/nSwr2F9lWcmPUavK+IfACOcfVK/iz9Gxmu+mYwXfJdosvi5DmB/QUUY7RMegBCLYTeDO3uZj2UH2DnuXHWLvscPsfXaEbWS/xmHgCrNhLu0k3GyeAoWQnmEUK/YTv9Zib7I3acv1d3aSIigjN4I/b+KL5RNYwf1f3Mj+Tlea+Y28hbfyNr6AL+SL+GJ2kn3GPmefEv0n7AQ7yj5g/2Afso9YOzvOPmb/ZMdIxwj5tMa9S9l1QjMcxm4SmrK3HfXnY0mDLAjqsm1H2bFO8v7G98hfn8kdhGyDB3+BvwAB/Pf89yRfchPnjpp01ToIJBphnb+xI2Sdo+yfxG0s6RTA95AcwS0Z/HgRH8JH82I+lJfwYXw4r+JlvIKP4KW8nI/hlXwkHyUjak/5190hMBRqtM8rNcJSuAVWiWeXbAlbxm4mjt6g8BdEO0i2+OtSAFnlKG+i+gEaAT4wAxbDNprP3Yhjh6eVkG88QX7kD+GipL1yOHtMnutFSed6+X+Qi7/cr5DnQbSvFufBoqRz8csfHXizxzWqxzUqRX4/nCSxJzX+T6r8aVeu0q+0o9+uYW3XsNSrT2lXn9KuitGseoP4FaC0Ixh5DY6lvczNvI7P5nNol3+Kfcm+YqfZ1+wb9i37Tj7V8qOxOlKOqvHqisjOsnPs3+x7dp79wC4wEzNrn3aSTwTfib1f/tXG9v8c/3+Cfu5tDQplbmRzdHJlYW0NCmVuZG9iag0KMjMgMCBvYmoNCjw8L1R5cGUvWFJlZi9TaXplIDIzL1dbIDEgNCAyXSAvUm9vdCAxIDAgUi9JbmZvIDExIDAgUi9JRFs8MzdDN0UyREIwRTM2MDg0Q0JBNUZGNDVDNzE0OEY4MTY+PDM3QzdFMkRCMEUzNjA4NENCQTVGRjQ1QzcxNDhGODE2Pl0gL0ZpbHRlci9GbGF0ZURlY29kZS9MZW5ndGggOTI+Pg0Kc3RyZWFtDQp4nGNgAIL//xmBpCADA4iqhVBbwRTjGTDFXAemWOTAFCsDhDKFUFlgik0BTLHLMjAwAeWFGJghFAuEYoVQTBAKqoQNpOE7jMcI5HF2g03hXAamuGYzMAAAS+sJQA0KZW5kc3RyZWFtDQplbmRvYmoNCnhyZWYNCjAgMjQNCjAwMDAwMDAwMTIgNjU1MzUgZg0KMDAwMDAwMDAxNyAwMDAwMCBuDQowMDAwMDAwMTI1IDAwMDAwIG4NCjAwMDAwMDAxODEgMDAwMDAgbg0KMDAwMDAwMDQ2MCAwMDAwMCBuDQowMDAwMDAwODk0IDAwMDAwIG4NCjAwMDAwMDEwNTQgMDAwMDAgbg0KMDAwMDAwMTI4MCAwMDAwMCBuDQowMDAwMDAxMzMzIDAwMDAwIG4NCjAwMDAwMDEzODYgMDAwMDAgbg0KMDAwMDAwMTU2OCAwMDAwMCBuDQowMDAwMDAxODIxIDAwMDAwIG4NCjAwMDAwMDAwMTMgNjU1MzUgZg0KMDAwMDAwMDAxNCA2NTUzNSBmDQowMDAwMDAwMDE1IDY1NTM1IGYNCjAwMDAwMDAwMTYgNjU1MzUgZg0KMDAwMDAwMDAxNyA2NTUzNSBmDQowMDAwMDAwMDE4IDY1NTM1IGYNCjAwMDAwMDAwMTkgNjU1MzUgZg0KMDAwMDAwMDAwMCA2NTUzNSBmDQowMDAwMDAyNDQzIDAwMDAwIG4NCjAwMDAwMDI0NzAgMDAwMDAgbg0KMDAwMDAwMjcxNSAwMDAwMCBuDQowMDAwMDEyOTY2IDAwMDAwIG4NCnRyYWlsZXINCjw8L1NpemUgMjQvUm9vdCAxIDAgUi9JbmZvIDExIDAgUi9JRFs8MzdDN0UyREIwRTM2MDg0Q0JBNUZGNDVDNzE0OEY4MTY+PDM3QzdFMkRCMEUzNjA4NENCQTVGRjQ1QzcxNDhGODE2Pl0gPj4NCnN0YXJ0eHJlZg0KMTMyNTgNCiUlRU9GDQp4cmVmDQowIDANCnRyYWlsZXINCjw8L1NpemUgMjQvUm9vdCAxIDAgUi9JbmZvIDExIDAgUi9JRFs8MzdDN0UyREIwRTM2MDg0Q0JBNUZGNDVDNzE0OEY4MTY+PDM3QzdFMkRCMEUzNjA4NENCQTVGRjQ1QzcxNDhGODE2Pl0gL1ByZXYgMTMyNTgvWFJlZlN0bSAxMjk2Nj4+DQpzdGFydHhyZWYNCjEzODk1DQolJUVPRg==';
            //return 'JVBERi0xLjQKJeLjz9MKMiAwIG9iago8PC9MZW5ndGggOTQvRmlsdGVyL0ZsYXRlRGVjb2RlPj5zdHJlYW0KeJwr5HIK4TI2U7AwMFMISeEyUNA1sQAx9N0MFYyNFELSuAz0TCxNDI0U0OmidC4NBThwyy/KTSxRyCxWyMsvUSguLSjILypJTdEMyQKamQ7EBiBTXUO4ArkAqOkavgplbmRzdHJlYW0KZW5kb2JqCjQgMCBvYmoKPDwvVHlwZS9QYWdlL01lZGlhQm94WzAgMCA1OTUgODQyXS9SZXNvdXJjZXM8PC9Gb250PDwvRjEgMSAwIFI+Pj4+L0NvbnRlbnRzIDIgMCBSL1BhcmVudCAzIDAgUj4+CmVuZG9iagoxIDAgb2JqCjw8L1R5cGUvRm9udC9TdWJ0eXBlL1R5cGUxL0Jhc2VGb250L0hlbHZldGljYS9FbmNvZGluZy9XaW5BbnNpRW5jb2Rpbmc+PgplbmRvYmoKMyAwIG9iago8PC9UeXBlL1BhZ2VzL0NvdW50IDEvS2lkc1s0IDAgUl0+PgplbmRvYmoKNSAwIG9iago8PC9UeXBlL0NhdGFsb2cvUGFnZXMgMyAwIFI+PgplbmRvYmoKNiAwIG9iago8PC9Qcm9kdWNlcihQREZkby5jb20gIDUuNS42LjApL0NyZWF0aW9uRGF0ZShEOjIwMTkwNTIxMjIxNDMzKzA4JzAwJykvTW9kRGF0ZShEOjIwMTkwNTIxMjIxNDMzKzA4JzAwJyk+PgplbmRvYmoKeHJlZgowIDcKMDAwMDAwMDAwMCA2NTUzNSBmIAowMDAwMDAwMjg3IDAwMDAwIG4gCjAwMDAwMDAwMTUgMDAwMDAgbiAKMDAwMDAwMDM3NSAwMDAwMCBuIAowMDAwMDAwMTc1IDAwMDAwIG4gCjAwMDAwMDA0MjYgMDAwMDAgbiAKMDAwMDAwMDQ3MSAwMDAwMCBuIAp0cmFpbGVyCjw8L1NpemUgNy9Sb290IDUgMCBSL0luZm8gNiAwIFIvSUQgWzw4ZTc0OWEzYmQyYmU4YWUwYzYzNTVhOTU0ZjViMzBhZT48OGU3NDlhM2JkMmJlOGFlMGM2MzU1YTk1NGY1YjMwYWU+XT4+CiVpVGV4dC01LjUuNgpzdGFydHhyZWYKNTkxCiUlRU9GCg==';
        }

        private String createDocumentDownloadLink(ContentDocumentLink contentDocumentLink) {
            return Url.getSalesforceBaseUrl().toExternalForm()
                    + getSandboxPrefix()
                    + '/sfc/servlet.shepherd/document/download/'
                    + contentDocumentLink.ContentDocumentId;
        }

        private String getSandboxPrefix(){
            String sandboxPrefix = Url.getCurrentRequestUrl().getPath().toLowerCase().split('/')[1];
            if (sandboxPrefix == 'pi' || sandboxPrefix == 'scr' || sandboxPrefix == 'patient'){
                sandboxPrefix = '/' + sandboxPrefix;
            }
            else{
                sandboxPrefix = '';
            }
            return sandboxPrefix;
        }
    }
}