/**
* @author: Carl Judge
* @date: 28-Sep-20
* @description: Classes that extend this can be queued using VT_R5_BatchQueueService
**/

public abstract class VT_R5_QueueableBatch extends VT_R5_TypeSerializable implements Database.Batchable<Object> {
    private Id queueRecordId;

    public void setId(Id queueRecordId) {
        this.queueRecordId = queueRecordId;
    }

    public abstract String getQueueName();

    public virtual void finish(Database.BatchableContext BC) {
        try {
            delete [SELECT Id FROM VTR5_QueueBatch__c WHERE Id = :queueRecordId];
        } catch (Exception e) {
            System.debug('Failed to delete queue record: ' + e.getMessage());
        }
        launchNext();
    }

    private void launchNext() {
        List<VTR4_Semaphore__c> semaphores;
        try {
            semaphores = [SELECT Id FROM VTR4_Semaphore__c WHERE Name = 'BatchQueueSemaphore' FOR UPDATE];
        } catch (Exception e) {}

        if (semaphores != null) {
            try {
                VTR5_QueueBatch__c nextBatchRecord;
                for (VTR5_QueueBatch__c nextBatchRec : [
                    SELECT Id, VTR5_BatchClass__c, VTR5_BatchJSON__c, VTR5_ScopeSize__c
                    FROM VTR5_QueueBatch__c
                    WHERE VTR5_QueueName__c = :getQueueName()
                    ORDER BY CreatedDate
                    LIMIT 1
                ]) {
                    nextBatchRecord = nextBatchRec;
                }
                if (nextBatchRecord != null) {
                    VT_R5_QueueableBatch nextBatch = (VT_R5_QueueableBatch) JSON.deserialize(nextBatchRecord.VTR5_BatchJSON__c, Type.forName(nextBatchRecord.VTR5_BatchClass__c));
                    nextBatch.actionsAfterDeserialize();
                    nextBatch.setId(nextBatchRecord.Id);
                    Database.executeBatch(nextBatch, nextBatchRecord.VTR5_ScopeSize__c.intValue());
                }
            } catch (Exception e) {
                System.debug('Failed to enqueue next job: ' + e.getMessage());
            }
        }
    }
}