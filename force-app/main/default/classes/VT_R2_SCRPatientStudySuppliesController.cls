/**
 * Created by user on 01.04.2019.
 */

public without sharing class VT_R2_SCRPatientStudySuppliesController {
    /*@AuraEnabled
    public static String getOptions() {
        List<List<String>> options = new List<List<String>>();
        options.add(new List<String>{'Kit Type', 'Connected Devices', 'IMP', 'Lab'});
        options.add(new List<String>{'Delivery Status', 'Pending Shipment', 'Shipped', 'Delivered'});
        return JSON.serialize(options);
    }*/

    class Option {
        String label;
        Boolean selected;
        Option(String label, Boolean selected) {
            this.label = label;
            this.selected = selected;
        }
    }

    class StudySupplyItem {
        String kitName;
        String kitType;
        String kitId;
        String deliveryStatus;
        String statusDate;
        String statusDateInSeconds;
    }

    @AuraEnabled
    public static String getOptions() {
        List<List<Option>> options = new List<List<Option>>();
        List<Option> kitTypes = new List<Option>{new Option('Kit Type', false)};
        /*Schema.DescribeFieldResult fieldResult = VTD1_Patient_Kit__c.VTD1_Kit_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry s : ple){
            kitTypes.add(new Option(s.getLabel(), false));
        }*/
        kitTypes.add(new Option(Label.VTD2_ConnectedDevices, false));
        kitTypes.add(new Option(Label.VTD2_IMP, false));
        kitTypes.add(new Option(Label.VTD2_Lab, false));
        kitTypes.add(new Option(Label.VTD2_StudyHubTablet, false));
        kitTypes.add(new Option(Label.VTD2_WelcometoStudyPackage, false));
        options.add(kitTypes);

        List<Option> deliveryStatuses = new List<Option>{new Option('Delivery Status', false)};
        Schema.DescribeFieldResult fieldResult = VTD1_Order__c.VTD1_Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry s : ple){
            deliveryStatuses.add(new Option(s.getLabel(), false));
        }
        options.add(deliveryStatuses);
        return JSON.serialize(options);
    }

    @AuraEnabled
    public static String getStudySupplies(String caseId) {
        List <VTD1_Patient_Kit__c> kits = [select Id, Name, VTD1_Kit_Type__c,
                VTD1_Patient_Delivery__r.VTD1_Status__c, LastModifiedDate from VTD1_Patient_Kit__c where VTD1_Case__c = :caseId];
        List <StudySupplyItem> items = new List<StudySupplyItem>();
        for (VTD1_Patient_Kit__c kit : kits) {
            StudySupplyItem item = new StudySupplyItem();
            item.kitId = kit.Id;
            item.kitName = kit.Name;
            item.kitType = kit.VTD1_Kit_Type__c;
            item.deliveryStatus = kit.VTD1_Patient_Delivery__r.VTD1_Status__c;
            item.statusDate = kit.LastModifiedDate.format('dd-MMM-YYYY');
            item.statusDateInSeconds = '' + kit.LastModifiedDate.getTime();
            items.add(item);
        }
        return JSON.serialize(items);
    }
}