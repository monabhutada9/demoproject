/**
 * Created by Yuliya Yakushenkova on 12/10/2020.
 */

public with sharing class VT_R5_PISCRMyPatientsService {

    private static VT_R5_PISCRMyPatientsService instance = new VT_R5_PISCRMyPatientsService();
    private static final Integer RECORDS_PER_REQUEST = 2;

    public static VT_R5_PISCRMyPatientsService getInstance() {
        return instance;
    }

    public void getMyPatients(Id userId, String search, Integer offset) {
        VT_R5_SearchService.SearchResponse myPatientSearchResponse = getMyPatientsSearchResult(userId, search, offset);

    }

    private VT_R5_SearchService.SearchResponse getMyPatientsSearchResult(Id userId, String search, Integer offset) {
        VT_R5_SearchService.SearchFilter patientsSearchFilter = new VT_R5_MyPatientsSearchFilter(userId, search);
        VT_R5_SearchService.PaginationData paginationData = new VT_R5_SearchService.PaginationData(RECORDS_PER_REQUEST);
        paginationData.currentPage = offset;
        return VT_R5_SearchService.search(patientsSearchFilter, paginationData);
    }
}