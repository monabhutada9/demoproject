/**
 * Created by Alexander Komarov on 17.04.2019.
 */

@RestResource(UrlMapping='/Patient/Calendar/Cancel/*')
global with sharing class VT_R3_RestPatientCalendarCancel {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();

    @HttpPost
    global static String cancelRequest(String visitId, String reason) {
        RestResponse response = RestContext.response;

        if (String.isBlank(visitId) || !helper.isIdCorrectType(visitId, 'VTD1_Actual_Visit__c')) {
            response.statusCode = 400;
            return helper.forAnswerForIncorrectInput();
        }

        try {
            VT_D1_PatientCalendar.cancelVisitRemote(visitId, reason);
        } catch (Exception e) {
            response.statusCode = 500;
            cr.buildResponse(
                    'Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName(),
                    System.Label.VT_D1_ErrorCancellingVisit
            );
            return JSON.serialize(cr, true);
        }
        VTD1_Actual_Visit__c actualVisit = [SELECT VTD1_Formula_Name__c FROM VTD1_Actual_Visit__c WHERE Id = :visitId LIMIT 1];
        String visitName = actualVisit.VTD1_Formula_Name__c;
        cr.buildResponse('SUCCESS', System.Label.VTR3_Submit_Request_To_Cancel.replace('#1', visitName));
        return JSON.serialize(cr, true);
    }
}