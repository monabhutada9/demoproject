public without sharing class VT_D1_FeedItemProcessMentions extends VT_D1_ProcessMentions {

    private void getRecsWithAtSign() {
        for (sObject obj : this.records) {
            FeedItem item = (FeedItem)obj;
            if (item.Body != null && (item.Body.contains('@') || item.Body.contains('{!'))) {
                this.recsWithAtSign.add(obj);
            }
        }
    }

    private void updateRecords() {
        Map<Id, VT_D1_Mention> toUpdMap = VT_D1_MentionHelper.getFinalUpdateMap(this.oooMentions);

        if (!toUpdMap.isEmpty()) {
            List<ConnectApi.BatchResult> batchResponse = ConnectApi.ChatterFeeds.getFeedElementBatch(Network.getNetworkId(), new List<Id>(toUpdMap.keySet()));
            System.debug('VT_D1_MentionHelper.getBatchResults(batchResponse).size()' + VT_D1_MentionHelper.getBatchResults(batchResponse).size());
            for (Object result : VT_D1_MentionHelper.getBatchResults(batchResponse)) {
                ConnectApi.FeedElement existingRec = (ConnectApi.FeedElement)result;

                ConnectApi.FeedItemInput updatedRec = new ConnectApi.FeedItemInput();
                updatedRec.body = VT_D1_MentionHelper.getMessageBodyWithAppendedBackups(existingRec.Id, existingRec.body.messageSegments, toUpdMap);
                ConnectApi.ChatterFeeds.updateFeedElement(Network.getNetworkId(), existingRec.Id, (ConnectApi.FeedElementInput) updatedRec);
            }
        }
    }


}