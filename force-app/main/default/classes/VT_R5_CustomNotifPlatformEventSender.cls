/**
 * Created by Olya Baranova
 * date: 22-May-20
 * @description: Custom Notification Platform Event Sender for PG (N574)
 */

public without sharing class VT_R5_CustomNotifPlatformEventSender extends VT_R3_AbstractPublishedAction {

    private String jsonString;

    public VT_R5_CustomNotifPlatformEventSender(String jsonString) {
        this.jsonString = jsonString;
    }

    public override Type getType() {
        return VT_R5_CustomNotifPlatformEventSender.class;
    }


    public override void execute() {
        VT_D2_TNCatalogNotifications.sendCustomNotificationsFuture(jsonString);
    }
}