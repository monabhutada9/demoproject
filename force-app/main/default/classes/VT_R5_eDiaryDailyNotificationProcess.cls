/**
 * Created by Dmitry Kovalev on 06.08.2020.
 */

public with sharing class VT_R5_eDiaryDailyNotificationProcess
        extends VT_R5_HerokuScheduler.AbstractScheduler implements Schedulable {

    private static final String NOTIFICATION_RECORD_TYPE = 'Daily Alert';

    private static final Integer MAX_SPAN_LEFT = -11 * 60;
    private static final Integer MAX_SPAN_RIGHT = 14 * 60;
    private static final Integer DAY_SPAN = 24 * 60;

    public static final String CRON_EXP = '0 55 * * * ?'; // 1 time per 1 hour (24 times, 00:55 - 23:55)
    public static final String JOB_NAME = 'eDiary (eCOA) Daily Notification Process ' + Crypto.getRandomInteger();

    /**
     * Set of possible timezone offsets in minutes.
     */
    public static final Set<Integer> timezoneOffsets {
        get {
            if (timezoneOffsets == null) {
                timezoneOffsets = new Set<Integer>();
                for(Schema.PicklistEntry pickListVal : User.fields.TimeZoneSidKey.getDescribe().getPicklistValues()){
                    Integer offsetInMinutes = TimeZone.getTimeZone(pickListVal.getValue()).getOffset(Datetime.now()) / 60000;
                    timezoneOffsets.add(offsetInMinutes);
                }
            }
            return timezoneOffsets;
        }
        private set;
    }

    /**
     * @return Id of scheduled job
     */
    public static Id runJob() {
        return (Id) System.schedule(JOB_NAME, CRON_EXP, new VT_R5_eDiaryDailyNotificationProcess());
    }

    /**
     * Method implementation of Schedulable interface.
     * @param context SchedulableContext
     */
    public void execute(SchedulableContext context) {
        executeFuture(); // future due to callout
    }

    private static Datetime getDatetimeNow() {
        return (Datetime) new VT_Stubber.ResultStub(
            'eDiaryProcess_Datetime',
            Datetime.now()
        ).getResult();
    }

    @Future(Callout=true)
    public static void executeFuture() {
        Datetime now = getDatetimeNow();
        String yesterday = now.addDays(-1).format('EEEE');
        String today = now.format('EEEE');
        String tomorrow = now.addDays(1).format('EEEE');
        Set<String> weekDays = new Set<String> { today };

        // DK NOTE: scheduling for different week days
        //   The time between 10AM and 11AM the only time, when we need to query builders
        // for three weekdays: the current week day, the previous one and the next one.
        if (now.hourGmt() < 11) {
            weekDays.add(yesterday);
        }
        if (now.hourGmt() >= 10) {
            weekDays.add(tomorrow);
        }

        /* For each builder, create payload if needed */
        List<VT_R5_HerokuScheduler.HerokuSchedulerPayload> notifications = new List<VT_R5_HerokuScheduler.HerokuSchedulerPayload>();
        List<VTR5_eDiaryNotificationBuilder__c> builders = getNotificationBuilders(weekDays);

        Integer nowTime = new VT_TimeParser(now).toMinutes();

        for (VTR5_eDiaryNotificationBuilder__c builder : builders) {
            Integer alertTime = new VT_TimeParser(builder.VTR5_AlertTime__c).toMinutes();

            /**
             * DK NOTE: timezones calculations
             *
             *   ------|--|-------------|-------------|------------>
             *        ti  a1            t             a2          time axis
             *
             *   a1, a2 - timestamps, alert time for different notification builders;
             * t - timestamp, current time in GMT+0 (Datetime.now());
             * ti - timezone offset in minutes (Set<Integer>).
             *   All the time converted to minutes since midnight for the calculations convenience.
             *
             *   s1 = a1 - t < 0; s2 = a2 - t > 0;   <-- here we calculate time spans (s1, s2) in minutes;
             * d = s1 - ti; 0 < d < 60   => the number _d_ is how many minutes after the current time when
             * the notification should be shown.
             *
             *   _s1_ is a negative number. It means, that on the axis timestamp a1 less than _t_,
             * _d_ is a positive number in a range between 0 and 59 minutes. We can use this knowledge
             * in further calculations.
             *
             *   For example, we need to understand when to show our notification =>
             *   We take all of the possible timezone offsets. In a loop for each offset we calculate
             * difference between this span and a timezone offset. The resulting number should be inside
             * the interval between 0 and 59 minutes, because our process (schedule) is run one time per hour.
             * If the resulting number inside a 60-minute interval, it means that we on the right way,
             * now we need to take this resulting number, add it as minutes to now timestamp (it'll be
             * the time in UTC, when we need to show our notification) and to schedule it by Heroku
             * with timezone offset obtained from the list of timezones.
             *
             *   If I would leave previous implementation with only positive spans (absolute values),
             * this solutions would not be possible to implement by this "ease" way.
             */

            // DK NOTE: scheduling for different week days (continue)
            //   Since the whole time range is 25 hours, our configuration (Notification Builder)
            // can fit two different days of the week at the same time.
            String possibleDays = builder.VTR5_DaysOfWeek__c;
            Integer span = alertTime - nowTime;

            if (possibleDays.contains(yesterday) && span - DAY_SPAN > MAX_SPAN_LEFT) {
                //System.debug('~~~~~~~~ weekday: ' + yesterday);
                addNotificationToList(notifications, builder, now, span - DAY_SPAN);
            }

            if (possibleDays.contains(today) && span > MAX_SPAN_LEFT && span < MAX_SPAN_RIGHT) {
                //System.debug('~~~~~~~~ weekday: ' + today);
                addNotificationToList(notifications, builder, now, span);
            }

            if (possibleDays.contains(tomorrow) && span + DAY_SPAN < MAX_SPAN_RIGHT) {
                //System.debug('~~~~~~~~ weekday: ' + tomorrow);
                addNotificationToList(notifications, builder, now, span + DAY_SPAN);
            }
        }

        if (notifications.size() > 0) {
            (new VT_R5_eDiaryDailyNotificationProcess()).schedule(notifications);
        }

        //System.debug('-------- notifications list length: ' + notifications.size());
        //System.debug('-------- notifications: ' + notifications);
    }

    private static  List<VTR5_eDiaryNotificationBuilder__c> getNotificationBuilders(Set<String> weekDays) {
        String weekDaysStr = String.join(new List<String>(weekDays), '\',\'');
        return (List<VTR5_eDiaryNotificationBuilder__c>) new VT_Stubber.ResultStub(
            'eDiaryProcess_builders',
                Database.query(
                        'SELECT Id, VTR5_DiaryStudyKey__c, VTR5_eDiaryName__c, ' +
                                'VTR5_DaysOfWeek__c, VTR5_AlertTime__c, ' +
                                'VTR5_NotificationRecipient__c, VTR5_RecipientType__c, ' +
                                'VTR5_Notification_Title__c, VTR5_NotificationMessage__c, VTR5_Active__c ' +
                        'FROM VTR5_eDiaryNotificationBuilder__c ' +
                        'WHERE VTR5_DaysOfWeek__c INCLUDES (\'' + weekDaysStr + '\') ' +
                            'AND VTR5_Active__c = TRUE ' +
                            'AND RecordType.Name = \'Daily Alert\''
                )
        ).getResult();
    }

    private static void addNotificationToList(List<VT_R5_HerokuScheduler.HerokuSchedulerPayload> notifications,
            VTR5_eDiaryNotificationBuilder__c builder,
            Datetime now, Integer span) {
        Boolean eDiaryBased = String.isNotEmpty(builder.VTR5_eDiaryName__c);
        //System.debug('~~~~~~~~ builder: ' + builder.Id + ' for eDiary: ' + builder.VTR5_eDiaryName__c);
        //System.debug('~~~~~~~~ alert time: ' + builder.VTR5_AlertTime__c + ' days: ' + builder.VTR5_DaysOfWeek__c);
        for (Integer offset : timezoneOffsets) {
            Integer d = span - offset;
            //System.debug('---> d = ' + d);
            if (d >= 0 && d < 60) {
                Datetime notificationTime = now.addMinutes(d).addSeconds(-now.second() -1); // t + d - s(t)
                //System.debug('-------> [' + offset + '] in minutes: ' + d);
                //System.debug('-------> [' + offset + '] time GMT+0: ' + notificationTime);
                //System.debug('-------> [' + offset + '] local time: ' + notificationTime.addMinutes(offset));
                VT_R5_eDiaryAlertsService.RecipientType recipient = builder.VTR5_RecipientType__c == 'Site Staff'
                        ? VT_R5_eDiaryAlertsService.RecipientType.SITE_STAFF
                        : VT_R5_eDiaryAlertsService.RecipientType.PT_CG;
                VT_R5_HerokuScheduler.HerokuSchedulerPayload notification = new VT_R5_HerokuScheduler.HerokuSchedulerPayload(
                    notificationTime,
                    'VT_R5_eDiaryAlertsService',
                    JSON.serialize(new VT_R5_eDiaryAlertsService.AlertRequestInfo(builder.Id, recipient, offset, eDiaryBased))
                );
                notifications.add(notification);
            }
        }
    }
}