@IsTest
public class VT_UtilitiesTest {
    public static void doTest(){
        System.assert(VT_Utilities.isAccessible('Account','Name'));
        System.assert(VT_Utilities.isAccessibleAndUpdatable(Account.Name));

        System.assert(VT_Utilities.isAccessible('Account'));
        System.assert(VT_Utilities.isAccessible(Account.SObjectType));
        System.assert(VT_Utilities.isAccessible(Account.Name));

        System.assert(VT_Utilities.isCreateable('Account'));
        System.assert(VT_Utilities.isCreateable(Account.SObjectType));
        System.assert(VT_Utilities.isCreateable(Account.Name));

        System.assert(VT_Utilities.isUpdateable('Account'));
        System.assert(VT_Utilities.isUpdateable(Account.SObjectType));
        System.assert(VT_Utilities.isUpdateable(Account.Name));

        System.assert(VT_Utilities.isDeletable('Account'));
        System.assert(VT_Utilities.isDeletable(Account.SObjectType));
    }
}