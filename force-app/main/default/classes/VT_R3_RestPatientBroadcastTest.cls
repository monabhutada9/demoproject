@isTest
public with sharing class VT_R3_RestPatientBroadcastTest {

	@TestSetup
	static void setup(){
		Test.startTest();
		HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
		HealthCloudGA__CandidatePatient__c candidatePatient = VT_D1_TestUtils.createPatientCandidate('Ter', 'kin', 'terkin@test.com', 'terkin@test.com', 'terkin', 'Converted', study.Id, 'PN', '11-11-11', 'US', 'NY');
		//VT_D1_TestUtils.createTestPatientCandidate(1);
		Test.stopTest();
	}

	@isTest
	public static void getBroadcastsTest() {
		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();
		request.requestURI = '/Patient/Broadcasts/';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response  = response;
		String responsString = VT_R3_RestPatientBroadcast.getBroadcasts();
		System.debug('responsString'+responsString);
		System.assertEquals(200, response.statusCode);

		VTD1_Chat__c chat = new VTD1_Chat__c();
		insert chat;

		VTD1_Chat_Member__c member = new VTD1_Chat_Member__c(
				VTD1_User__c = UserInfo.getUserId(),
				VTD1_Chat__c = chat.Id
		);
		insert member;
		List<VTD1_Chat__c> chats = [
				SELECT Id, Name
				FROM VTD1_Chat__c
				WHERE Id IN (
						SELECT VTD1_Chat__c
						FROM VTD1_Chat_Member__c
						WHERE VTD1_User__c = :UserInfo.getUserId()
				)
				ORDER BY CreatedDate DESC
		];
		responsString = VT_R3_RestPatientBroadcast.getBroadcasts();
        
        // As Broadcast can be responded back by Patient, hence to make a immediate fix we are sending blank array.- for test coverage
        VT_R3_RestPatientBroadcast.PatientBroadcast patientBrodcastWrapper = new VT_R3_RestPatientBroadcast.PatientBroadcast('test','test','test', System.now(), 'test',true,'test');
	}

	@isTest
	public static void getBCTest() {
		User user = [SELECT Id FROM User WHERE Username = 'terkin@test.com'];
		//User user = [SELECT Id FROM User WHERE Username = 'PIUser1@test.com_user'];
		Case pcf = new Case(RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VTD1_PCF').getRecordTypeId());
		insert pcf;
		VTD1_PCF_Chat_Member__c pcfMember = new VTD1_PCF_Chat_Member__c(
				VTD1_PCF__c = pcf.Id,
				VTD1_User_Id__c = user.Id
		);
		insert pcfMember;
		VTD1_Chat__c chat = new VTD1_Chat__c();
		insert chat;
		VTD1_Chat_Member__c chatMember = new VTD1_Chat_Member__c(
				VTD1_Chat__c = chat.Id,
				VTD1_User__c = user.Id
		);
		insert chatMember;

		FeedItem caseFeedItem = new FeedItem(
				Body = 'Test',
				ParentId = pcfMember.VTD1_PCF__c
		);
		FeedItem chatFeedItem = new FeedItem(
				Body = 'Test',
				ParentId = chat.Id
		);
		insert new List<FeedItem>{caseFeedItem, chatFeedItem};


		FeedComment caseFeedComment = new FeedComment(
				CommentBody = 'Test',
				FeedItemId = caseFeedItem.Id
		);
		FeedComment chatFeedComment = new FeedComment(
				CommentBody = 'Test',
				FeedItemId = chatFeedItem.Id
		);
		insert new List<FeedComment>{caseFeedComment, chatFeedComment};

		VTD2_Feed_Post_on_Chat__c feedPostOnChat = new VTD2_Feed_Post_on_Chat__c(
				VTD2_Feed_Element__c = caseFeedItem.Id,
				VTD2_Chat_Member__c = chatMember.Id
		);
		insert feedPostOnChat;

		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();
		request.requestURI = '/Patient/Messages';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response  = response;
		String responseString = VT_R3_RestPatientBroadcast.getBroadcasts();

		System.runAs(user){
			responseString = VT_R3_RestPatientBroadcast.getBroadcasts();
//			ConnectApi.BatchResult[] feedElementsBatchResult = ConnectApi.ChatterFeeds.getFeedElementBatch(null, new List<String>(feedPostsMap.keySet()));

		}
	}
}