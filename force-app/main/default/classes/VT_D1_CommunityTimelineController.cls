/**
* @author: Carl Judge
* @date: 06-Jun-18
* @description: Controller for Community Timeline component
**/

public class VT_D1_CommunityTimelineController {
    private static final String VISIT_TYPE = Label.VTD2_VisitType;
    private static final Map<String,String> VISIT_ICONS = new Map<String,String>{
            'At Home' => 'single-neutral-home',
            'At Location' => 'car-retro-1'
    };
    private static final Boolean VISIT_SHOW_POPUP = true;

    private static final String ORDER_TYPE = Label.VTD2_DeliveryType;
    private static final String ORDER_ICON = 'delivery-truck-2';

    private static final String MILESTONE_STATUS = Label.VTD2_MilestoneCompleted;
    private static final String MILESTONE_LINE_STATUS = 'complete';
    private static final String MILESTONE_TYPE = Label.VTD2_MilestoneType;
    private static final Boolean MILESTONE_SHOW_POPUP = false;


    private static final String DOSE_TYPE = Label.VTR5_Intervention;
    private static final String DOSE_STATUS = Label.VTR5_Delivered;
    private static final String DOSE_ICON = 'icon-dose';
    private static final Boolean DOSE_SHOW_POPUP = false;


    /*private static final Map<String, String> MILESTONE_ICONS = new Map<String, String>{
            'Pre-Consent' => '264PersonComment',
            'Consented' => '261PersonCheck',
            'Active/Randomized' => '261PersonCheck',
            'Screened' => '223Stethoscope',
            'Completed' => '261PersonCheck'
    };*/ // Temporary commented on 14.08
    private static final String MILESTONE_ICON = 'flag-triangle';

    private static final Map<String, Integer> LINE_STATUS_ORDER = new Map<String, Integer>{
            'complete' => 1,
            'inProgress' => 2,
            'inactive' => 3
    };

    private static final Map<String, String> VISIT_STATUS_MAP = new Map<String, String>{
            'Future Visit' => 'inactive',
            'To Be Scheduled' => 'inactive',
            'To Be Rescheduled' => 'inProgress',
            'Scheduled' => 'inProgress',
            'Completed' => 'complete',
            'Cancelled' => 'inactive',
            'Requested' => 'inactive'
    };

    private static final Map<String, String> ORDER_STATUS_MAP = new Map<String, String>{
            'Cancelled' => 'inactive',
            'Pending Shipment' => 'inactive',
            'Shipped' => 'inProgress',
            'Pending Delivery' => 'inProgress',
            'Confirm Delivery' => 'inProgress',
            'Received' => 'complete',
            'Delivered' => 'complete',
            'Not Started' => 'inactive',
            'Pending Return' => 'inProgress',
            'Reconciliation' => 'inProgress',
            'Return Complete' => 'complete',
            'Return Incomplete' => 'inProgress'
    };

    @AuraEnabled(Cacheable=true)
    public static Case getCase(Id caseId) {
        CommunityTimelineFactory factory = new CommunityTimelineFactory();
        factory.addAlgorithm(new PiPatientDocumentsSelector());
        factory.addAlgorithm(new ScrPatientDocumentsSelector());
        factory.addAlgorithm(new PatientDocumentsSelector());
        return factory.getCase(caseId);
    }

    @AuraEnabled
    public static List<TimelineItem> getTimeLineItems(Id caseId) {
        CommunityTimelineFactory factory = new CommunityTimelineFactory();
        factory.addAlgorithm(new PiPatientDocumentsSelector());
        factory.addAlgorithm(new ScrPatientDocumentsSelector());
        factory.addAlgorithm(new PatientDocumentsSelector());
        return factory.getTimeLineItems(caseId);
    }

    interface CommunityTimelineSelector {

        Boolean isSuitableProfile();

        Case getCase(Id caseId);

        List<TimelineItem> getTimeLineItems(Id caseId);
    }

    public abstract class ProfileCommunityTimelineSelector implements CommunityTimelineSelector {

        protected abstract Id getProfileId();

        protected abstract Case getCase(Id caseId);

        protected abstract List<TimelineItem> getTimeLineItems(Id caseId);

        protected Boolean isPatientProfile = false;
        public Boolean isSuitableProfile() {
            this.isPatientProfile = VT_D1_HelperClass.getProfileNameOfCurrentUser() == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME;
            return UserInfo.getProfileId() == this.getProfileId();
        }

        public List<TimelineItem> getDoses(Case c) {
            List<TimelineItem> items = new List<TimelineItem>();
            if (c.VTR5_Day_1_Dose__c != null) {
                items.add(new TimelineItem(c.VTR5_Day_1_Dose__c, Label.VTR5_Day1Dose, c.Id));
            }
            if (c.VTR5_Day_57_Dose__c != null) {
                items.add(new TimelineItem(c.VTR5_Day_57_Dose__c, Label.VTR5_Day57Dose, c.Id));
            }
            return items;
        }
    }

    public class CommunityTimelineFactory {

        public List<CommunityTimelineSelector> algorithms;

        public CommunityTimelineFactory() {
            this.algorithms = new List<CommunityTimelineSelector>();
        }

        public Case getCase(Id caseId) {
            Case result;
            for (CommunityTimelineSelector algorithm : this.algorithms) {
                if (algorithm.isSuitableProfile()) {
                    result = algorithm.getCase(caseId);
                    break;
                }
            }
            return result;
        }

        public List<TimelineItem> getTimeLineItems(Id caseId) {
            List<TimelineItem> result = new List<TimelineItem>();
            for (CommunityTimelineSelector algorithm : this.algorithms) {
                if (algorithm.isSuitableProfile()) {
                    result = algorithm.getTimeLineItems(caseId);
                    break;
                }
            }
            return result;
        }

        public void addAlgorithm(CommunityTimelineSelector communityTimelineSelector) {
            this.algorithms.add(communityTimelineSelector);
        }
    }

    public without sharing class PiPatientDocumentsSelector extends ProfileCommunityTimelineSelector {

        protected override Id getProfileId() {
            return getPiProfileId();
        }

        public override Case getCase(Id caseId) {
            return getCaseById(caseId);
        }

        public override List<TimelineItem> getTimeLineItems(Id caseId) {
            List<TimelineItem> items = new List<TimelineItem>();
            Case caseRecord = caseId != null ? getCaseById(caseId) : VT_D1_CaseByCurrentUser.getCase().caseObj;

            if (caseRecord != null && caseRecord.Id != null) {
                caseId = caseRecord.Id;

                items.addAll(this.getVisits(caseId));
                items.addAll(this.getMilestones(caseId));
                items.addAll(this.getDoses(caseRecord));
                //items.addAll(this.getOrders(caseId));
                items.sort();
            }
            return items;
        }

        private List<TimelineItem> getVisits(Id caseId) {
            List<VTD1_Actual_Visit__c> visits = getActualVisits(caseId);
            VT_D1_TranslateHelper.translate(visits); // the only thing we need to translate here is VTD1_Protocol_Visit__r.VTD1_EDC_Name__c

            List<TimelineItem> items = new List<TimelineItem>();
            for (VTD1_Actual_Visit__c rec : visits) {
                items.add(new TimelineItem(rec, caseId));
            }
            return items;
        }

        /* SH-5884 3. Kits/deliveries will be removed from the timeline completely.
        private List<TimelineItem> getOrders(Id caseId) {
            List<VTD1_Order__c> orders = [
                    SELECT Id, Name, VTD1_Status__c, toLabel(VTD1_Status__c) translatedStatus, VTD1_Date_for_Timeline__c,
                            VTD1_Protocol_Delivery__r.Name, VTD1_Expected_Shipment_Date__c, VTD1_Delivery_Order__c, VTD1_Description__c, VTD1_Protocol_Delivery__r.VTD1_Description__c
                    FROM VTD1_Order__c
                    WHERE VTD1_Case__c = :caseId
                    AND VTD1_Status__c != :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED
            ];
            VT_D1_TranslateHelper.translate(orders); // the only thing we need to translate here is VTD1_Protocol_Delivery__r.Name

            List<TimelineItem> items = new List<TimelineItem>();
            for (VTD1_Order__c rec : orders) {
                items.add(new TimelineItem(rec, caseId));
            }
            return items;
        }*/

        private List<TimelineItem> getMilestones(Id caseId) {
            List<TimelineItem> items = new List<TimelineItem>();
            List<VTD1_Case_Status_History__c> caseStatusHistories = getCaseStatusHistories(caseId);
            VT_D1_TranslateHelper.translate(caseStatusHistories);
            for (VTD1_Case_Status_History__c rec : caseStatusHistories) {
                items.add(new TimelineItem(rec, caseId));
            }
            return items;
        }

        private List<VTD1_Case_Status_History__c> getCaseStatusHistories(Id caseId) {
            return [
                    SELECT
                            CreatedDate,
                            VTD1_Status__c,
                            toLabel(VTD1_Status__c) translatedStatus
                    FROM VTD1_Case_Status_History__c
                    WHERE VTD1_Case__c = :caseId
                    AND VTD1_Is_Active__c = TRUE
            ];
        }

        private List<VTD1_Actual_Visit__c> getActualVisits(Id caseId) {
            return [
                    SELECT
                            Id,
                            VTD1_Status__c,
                            toLabel(VTD1_Status__c) translatedStatus,
                            VTD1_Date_for_Timeline__c,
                            VTD1_Visit_Type__c,
                            VTD1_Protocol_Visit__c,
                            VTD1_Protocol_Visit__r.VTD1_EDC_Name__c,
                            VTD1_Protocol_Visit__r.VTD1_Visit_Description__c,
                            VTD1_Unscheduled_Visit_Type__c,
                            Name,
                            VTD1_Protocol_Visit__r.VTD1_VisitNumber__c,
                            To_Be_Scheduled_Date__c,
                            VTD1_Visit_Completion_Date__c,
                            VTD1_Visit_Description__c,
                            VTD1_Scheduled_Date_Time__c,
                            VTR2_Modality__c,
                            toLabel(VTR2_Modality__c) translatedModality,
                            VTD2_Schedule_Request_Submitted__c,
                            VTR2_Status_changed_date__c,
                            VTR3_Previous_Schedule_Date__c
                    FROM VTD1_Actual_Visit__c
                    WHERE (VTD1_Case__c = :caseId OR Unscheduled_Visits__c = :caseId)
                    AND VTD1_Status__c != :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED
                    AND VTR3_LTFUStudyVisit__c = true
                    AND (VTD1_Protocol_Visit__r.VTD1_isArchivedVersion__c = FALSE OR VTD1_Status__c = :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED)
                    ORDER BY VTD1_Protocol_Visit__r.VTD1_VisitNumber__c
            ];
        }

        private Case getCaseById(Id caseId) {
            List<Case> cases = [
                    SELECT
                            Contact.Name,
                            VTD1_Study__r.Name,
                            VTR3_LTFU__c,
                            VTD1_Study__r.VTR5_IRTSystem__c,
                            VTR5_Day_1_Dose__c,
                            VTR5_Day_57_Dose__c
                    FROM Case
                    WHERE Id = :caseId
            ];
            if (!cases.isEmpty()) {
                return cases[0];
            } else {
                return new Case();
            }
        }

        private Id getPiProfileId() {
            return [SELECT Id FROM Profile WHERE Name = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME][0].Id;
        }
    }

    public without sharing class ScrPatientDocumentsSelector extends ProfileCommunityTimelineSelector {

        protected override Id getProfileId() {
            return getScrProfileId();
        }

        public override Case getCase(Id caseId) {
            return getCaseById(caseId);
        }

        public override List<TimelineItem> getTimeLineItems(Id caseId) {
            List<TimelineItem> items = new List<TimelineItem>();

            Case caseRecord = caseId != null ? getCaseById(caseId) : VT_D1_CaseByCurrentUser.getCase().caseObj;

            if (caseRecord != null && caseRecord.Id != null) {
                caseId = caseRecord.Id;

                items.addAll(this.getVisits(caseId));
                items.addAll(this.getMilestones(caseId));
                items.addAll(this.getDoses(caseRecord));
                //items.addAll(this.getOrders(caseId));
                items.sort();
            }
            return items;
        }

        private List<TimelineItem> getVisits(Id caseId) {
            List<VTD1_Actual_Visit__c> visits = getActualVisits(caseId);
            VT_D1_TranslateHelper.translate(visits); // the only thing we need to translate here is VTD1_Protocol_Visit__r.VTD1_EDC_Name__c

            List<TimelineItem> items = new List<TimelineItem>();
            for (VTD1_Actual_Visit__c rec : visits) {
                items.add(new TimelineItem(rec, caseId));
            }
            return items;
        }

        /*SH-5884 3. Kits/deliveries will be removed from the timeline completely.
        private List<TimelineItem> getOrders(Id caseId) {
            List<VTD1_Order__c> orders = [
                    SELECT Id, Name, VTD1_Status__c, toLabel(VTD1_Status__c) translatedStatus, VTD1_Date_for_Timeline__c,
                            VTD1_Protocol_Delivery__r.Name, VTD1_Expected_Shipment_Date__c, VTD1_Delivery_Order__c, VTD1_Description__c, VTD1_Protocol_Delivery__r.VTD1_Description__c
                    FROM VTD1_Order__c
                    WHERE VTD1_Case__c = :caseId
                    AND VTD1_Status__c != :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED
            ];
            VT_D1_TranslateHelper.translate(orders); // the only thing we need to translate here is VTD1_Protocol_Delivery__r.Name

            List<TimelineItem> items = new List<TimelineItem>();
            for (VTD1_Order__c rec : orders) {
                items.add(new TimelineItem(rec, caseId));
            }
            return items;
        }*/

        private List<TimelineItem> getMilestones(Id caseId) {
            List<TimelineItem> items = new List<TimelineItem>();
            for (VTD1_Case_Status_History__c rec : getCaseStatusHistories(caseId)) {
                items.add(new TimelineItem(rec, caseId));
            }
            return items;
        }

        private List<VTD1_Case_Status_History__c> getCaseStatusHistories(Id caseId) {
            return [
                    SELECT
                            CreatedDate,
                            VTD1_Status__c,
                            toLabel(VTD1_Status__c) translatedStatus
                    FROM VTD1_Case_Status_History__c
                    WHERE VTD1_Case__c = :caseId
                    AND VTD1_Is_Active__c = TRUE
            ];
        }

        private List<VTD1_Actual_Visit__c> getActualVisits(Id caseId) {
            return [
                    SELECT Id, VTD1_Status__c, toLabel(VTD1_Status__c) translatedStatus, VTD1_Date_for_Timeline__c, VTD1_Visit_Type__c,
                            VTD1_Protocol_Visit__c, VTD1_Protocol_Visit__r.VTD1_EDC_Name__c, VTD1_Unscheduled_Visit_Type__c, Name, VTD1_Protocol_Visit__r.VTD1_VisitNumber__c,
                            To_Be_Scheduled_Date__c, VTD1_Visit_Completion_Date__c, VTD1_Visit_Description__c, VTD1_Protocol_Visit__r.VTD1_Visit_Description__c, VTD1_Scheduled_Date_Time__c, VTR2_Modality__c,
                            toLabel(VTR2_Modality__c) translatedModality, VTD2_Schedule_Request_Submitted__c,
                            VTR2_Status_changed_date__c, VTR3_Previous_Schedule_Date__c
                    FROM VTD1_Actual_Visit__c
                    WHERE (VTD1_Case__c = :caseId OR Unscheduled_Visits__c = :caseId)
//                    AND VTD1_Status__c != :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED
                    AND VTR3_LTFUStudyVisit__c = true
                    AND (VTD1_Protocol_Visit__r.VTD1_isArchivedVersion__c = FALSE OR VTD1_Status__c = :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED)
                    ORDER BY VTD1_Protocol_Visit__r.VTD1_VisitNumber__c
            ];
        }

        private Case getCaseById(Id caseId) {
            List<Case> cases = [
                    SELECT
                            Contact.Name,
                            VTD1_Study__r.Name,
                            VTR3_LTFU__c,
                            VTD1_Study__r.VTR5_IRTSystem__c,
                            VTR5_Day_1_Dose__c,
                            VTR5_Day_57_Dose__c
                    FROM Case
                    WHERE Id = :caseId
            ];
            if (!cases.isEmpty()) {
                return cases[0];
            } else {
                return new Case();
            }
        }

        private Id getScrProfileId() {
            return [SELECT Id FROM Profile WHERE Name = :VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME][0].Id;
        }
    }

    // for any other Profile
    public without sharing class PatientDocumentsSelector extends ProfileCommunityTimelineSelector {

        protected override Id getProfileId() {
            return UserInfo.getProfileId();
        }

        public override Case getCase(Id caseId) {
            return getCaseById(caseId);
        }

        public override List<TimelineItem> getTimeLineItems(Id caseId) {
            List<TimelineItem> items = new List<TimelineItem>();
            Case caseRecord = caseId != null ? getCaseById(caseId) : VT_D1_CaseByCurrentUser.getCase().caseObj;

            if (caseRecord != null && caseRecord.Id != null) {
                caseId = caseRecord.Id;
                items.addAll(this.getVisits(caseId));
                items.addAll(this.getMilestones(caseId));
                items.addAll(this.getDoses(caseRecord));
                //items.addAll(this.getOrders(caseId));
                items.sort();
            }
            return items;
        }

        private List<TimelineItem> getVisits(Id caseId) {
            List<VTD1_Actual_Visit__c> visits = getActualVisits(caseId);
            VT_D1_TranslateHelper.translate(visits); // the only thing we need to translate here is VTD1_Protocol_Visit__r.VTD1_EDC_Name__c

            List<TimelineItem> items = new List<TimelineItem>();
            for (VTD1_Actual_Visit__c rec : visits) {
                if (!rec.Visit_Members__r.isEmpty()) {
                    items.add(new TimelineItem(rec, caseId,  this.isPatientProfile));
                }
            }
            return items;
        }

        /* SH-5884 3. Kits/deliveries will be removed from the timeline completely.
        private List<TimelineItem> getOrders(Id caseId) {
            List<VTD1_Order__c> orders = [
                    SELECT Id, Name, VTD1_Status__c, toLabel(VTD1_Status__c) translatedStatus, VTD1_Date_for_Timeline__c,
                            VTD1_Protocol_Delivery__r.Name, VTD1_Expected_Shipment_Date__c, VTD1_Delivery_Order__c, VTD1_Description__c, VTD1_Protocol_Delivery__r.VTD1_Description__c
                    FROM VTD1_Order__c
                    WHERE VTD1_Case__c = :caseId
                    AND VTD1_Status__c != :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED
            ];
            VT_D1_TranslateHelper.translate(orders); // the only thing we need to translate here is VTD1_Protocol_Delivery__r.Name

            List<TimelineItem> items = new List<TimelineItem>();
            for (VTD1_Order__c rec : orders) {
                items.add(new TimelineItem(rec, caseId));
            }
            return items;
        }*/

        private List<TimelineItem> getMilestones(Id caseId) {
            List<TimelineItem> items = new List<TimelineItem>();
            for (VTD1_Case_Status_History__c rec : getCaseStatusHistories(caseId)) {
                items.add(new TimelineItem(rec, caseId));
            }
            return items;
        }

        private List<VTD1_Case_Status_History__c> getCaseStatusHistories(Id caseId) {
            return [
                    SELECT
                            CreatedDate,
                            VTD1_Status__c,
                            toLabel(VTD1_Status__c) translatedStatus
                    FROM VTD1_Case_Status_History__c
                    WHERE VTD1_Case__c = :caseId
                    AND VTD1_Is_Active__c = TRUE
            ];
        }

        private List<VTD1_Actual_Visit__c> getActualVisits(Id caseId) {
            return [
                    SELECT
                            Id,
                            VTD1_Status__c,
                            toLabel(VTD1_Status__c) translatedStatus,
                            VTD1_Date_for_Timeline__c,
                            VTD1_Visit_Type__c,
                            VTD1_Protocol_Visit__r.VTD1_EDC_Name__c,
                            VTD1_Unscheduled_Visit_Type__c,
                            Name,
                            VTD1_Protocol_Visit__r.VTD1_VisitNumber__c,
                            To_Be_Scheduled_Date__c,
                            VTD1_Visit_Completion_Date__c,
                            VTD1_Visit_Description__c,
                            VTD1_Protocol_Visit__r.VTD1_Visit_Description__c,
                            VTD1_Scheduled_Date_Time__c,
                            VTR2_Modality__c,
                            toLabel(VTR2_Modality__c) translatedModality,
                            VTD2_Schedule_Request_Submitted__c,
//                            OwnerId,
                            VTD1_Visit_Name__c,
                            VTD1_Visit_Number__c,
                            VTD1_Contact__r.Name,
                            VTD1_Case__r.VTD1_Study__r.VTD1_Primary_Sponsor__r.Name,
                            VTD1_Case__r.VTD1_Study__r.VTD1_Protocol_Nickname__c,
                            VTD1_Case__r.VTD1_Study__r.VTD1_Indication__c,
                            VTD1_Protocol_Visit__c,
                            VTD1_Protocol_Visit__r.VTD1_Study__r.VTD1_Primary_Sponsor__r.Name,
                            VTD1_Protocol_Visit__r.VTD1_Study__r.VTD1_Protocol_Nickname__c,
                            VTD1_Protocol_Visit__r.VTD1_Study__r.VTD1_Indication__c,
                            VTD1_Protocol_Visit__r.VTD1_Range__c,
                            VTD1_Protocol_Visit__r.RecordTypeId,
                            VTD1_Case__r.ContactId,
                            VTD1_Case__r.Contact.Phone,
                            VTD1_Case__r.Status,
                            VTD1_Case__c,
                            VTD1_Case__r.VTD1_Subject_ID__c,
                            Unscheduled_Visits__r.VTD1_Study__r.VTD1_Primary_Sponsor__r.Name,
                            Unscheduled_Visits__r.VTD1_Study__r.VTD1_Protocol_Nickname__c,
                            Unscheduled_Visits__r.VTD1_Study__r.VTD1_Indication__c,
                            Unscheduled_Visits__r.ContactId,
                            Unscheduled_Visits__r.Contact.Phone,
                            Unscheduled_Visits__r.VTD1_Subject_ID__c,
                            VTD1_Visit_Duration__c,
                            VTD1_VisitType_SubType__c,
                            VTD1_Proposed_Time_Range_From__c,
                            VTD1_Proposed_Time_Range_To__c,
                            VTD1_Onboarding_Type__c,
                            CreatedDate,
                            VTD1_Case__r.VTD1_PI_user__r.TimeZoneSidKey,
                            Unscheduled_Visits__r.VTD1_PI_user__r.TimeZoneSidKey,
                            RecordTypeId,
                            RecordType.DeveloperName,
                            VTR2_Visit_location__c,
                            VTD1_Status_To_Be_Scheduled_Set_Date__c,
                            Patient_Name_Formula__c,
                            VTR2_Status_changed_date__c,
                            VTR3_Previous_Schedule_Date__c, (
                            SELECT Id
                            FROM Visit_Members__r
                            WHERE VTD1_Participant_User__r.Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
                            OR VTD1_Participant_User__r.Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME
                    )
                    FROM VTD1_Actual_Visit__c
                    WHERE (VTD1_Case__c = :caseId OR Unscheduled_Visits__c = :caseId)
                    // SH-5474
                    //AND VTD1_Status__c != :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED
                    AND (VTD1_Protocol_Visit__r.VTD1_isArchivedVersion__c = FALSE OR VTD1_Status__c = :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED)
                    AND VTR3_LTFUStudyVisit__c = true
                    ORDER BY VTD1_Protocol_Visit__r.VTD1_VisitNumber__c
            ];
        }

        private Case getCaseById(Id caseId) {
            List<Case> cases = [
                    SELECT
                            Contact.Name,
                            VTD1_Study__r.Name,
                            VTR3_LTFU__c,
                            VTD1_Study__r.VTR5_IRTSystem__c,
                            VTR5_Day_1_Dose__c,
                            VTR5_Day_57_Dose__c
                    FROM Case
                    WHERE Id = :caseId
            ];
            if (cases.isEmpty()) {
                return null;
            } else {
                return cases[0];
            }
        }
    }

    private class TieBreakerForVisit {
        Integer visitNumber;
        Datetime dateTimeForSorting;
        Datetime dateTimeForSorting2;
    }

    public class TimelineItem implements Comparable {
        @AuraEnabled public String label;
        @AuraEnabled public String labelVisitNamePTandCG;
        @AuraEnabled public String subLabel;
        @AuraEnabled public String statusValue;
        @AuraEnabled public String status;
        @AuraEnabled public String lineStatus; // complete/inProgress/inactive = green/blue/grey on the timeline
        @AuraEnabled public String itemType;
        @AuraEnabled public String link;
//        @AuraEnabled public Date itemDate;
        @AuraEnabled public String itemDate;
        @AuraEnabled public String icon;
        @AuraEnabled public String prevStatus;
        @AuraEnabled public String nextStatus;
        @AuraEnabled public Datetime itemDateTime;
        @AuraEnabled public String caseId;
        @AuraEnabled public String id;
        @AuraEnabled public String visitType;
        @AuraEnabled public String description;
        @AuraEnabled public Boolean noDetails;
        @AuraEnabled public String modality;
        public String translatedModality;
        @AuraEnabled public String visitWindow;
        @AuraEnabled public Boolean showPopup;
        public List<Datetime> visitWindowRaw;
        public String sortTieBreaker;
        public String sortStatusBreaker;
        public TieBreakerForVisit tieBreakerForVisit;

        public TimelineItem() {
        }

        public TimelineItem(SObject rec, Id caseId, Boolean isPatient) {
            VTD1_Actual_Visit__c actualVisit = (VTD1_Actual_Visit__c) rec;
            if (isPatient && (actualVisit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED
                    || actualVisit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED
                    || actualVisit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_REQUESTED
                    || actualVisit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT)) {
                VT_D1_ActualVisitsHelper.actualVisitInfo = actualVisit;
                if (actualVisit.VTD1_Case__c != null) {
                    VT_D1_ActualVisitsHelper.PIuserTimeZone = actualVisit.VTD1_Case__r.VTD1_PI_user__r.TimeZoneSidKey;
                } else {
                    VT_D1_ActualVisitsHelper.PIuserTimeZone = actualVisit.Unscheduled_Visits__r.VTD1_PI_user__r.TimeZoneSidKey;
                }
                this.visitWindow = new VT_D1_ActualVisitsHelper().getVisitWindowTranslated(actualVisit.Id);
                this.visitWindowRaw = new VT_D1_ActualVisitsHelper().getVisitWindowRaw(actualVisit.Id);
            }
            init(rec, caseId);
        }

        public TimelineItem(SObject rec, Id caseId) {
            init(rec, caseId);
        }

        public void init(SObject rec, Id caseId) {
            if (VTD1_Actual_Visit__c.getSObjectType() == rec.getSObjectType()) {
                VTD1_Actual_Visit__c actualVisit = (VTD1_Actual_Visit__c) rec;
                this.id = actualVisit.Id;
                this.tieBreakerForVisit = getTieBreakerForVisit(actualVisit);
                this.status = (String) actualVisit.get('translatedStatus');
                this.statusValue = actualVisit.VTD1_Status__c;
                this.label = VT_D1_HelperClass.getActualVisitName(actualVisit);
                this.sortTieBreaker = null;
                this.lineStatus = VISIT_STATUS_MAP.get(actualVisit.VTD1_Status__c);
                this.itemType = VISIT_TYPE;
                String dateForTimeline;
                if (actualVisit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_REQUESTED) {
                    this.itemDate = actualVisit.VTD2_Schedule_Request_Submitted__c != null ?
//                            actualVisit.VTD2_Schedule_Request_Submitted__c.date() : null;
                            VT_D1_TranslateHelper.translate(actualVisit.VTD2_Schedule_Request_Submitted__c.format('dd-MMM-yyyy')) : null;
                    dateForTimeline = this.visitWindow;
                } else if (actualVisit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED
                        || actualVisit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED
                        || actualVisit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT) {
                    dateForTimeline = this.visitWindow;
                } else {
                    if (actualVisit.VTD1_Date_for_Timeline__c != null) {
//                        this.itemDate = actualVisit.VTD1_Date_for_Timeline__c.date();
                        this.itemDate = VT_D1_TranslateHelper.translate(actualVisit.VTD1_Date_for_Timeline__c.format('dd-MMM-yyyy'));
                        dateForTimeline = VT_D1_TranslateHelper.translate(actualVisit.VTD1_Date_for_Timeline__c.format('dd-MMM-yyyy'));
                    }
                }

                this.subLabel = (String) actualVisit.get('translatedStatus') + (dateForTimeline!=null? ' | ' +  dateForTimeline : '');
                this.icon = VISIT_ICONS.get(actualVisit.VTR2_Modality__c);
                this.itemDateTime = this.tieBreakerForVisit.dateTimeForSorting;
                this.visitType = actualVisit.VTD1_Unscheduled_Visit_Type__c == null ? actualVisit.VTD1_Visit_Type__c : actualVisit.VTD1_Unscheduled_Visit_Type__c;
                this.description = actualVisit.VTD1_Protocol_Visit__r.VTD1_Visit_Description__c;
                if (actualVisit.VTD1_Scheduled_Date_Time__c == null || actualVisit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED) {
                    this.noDetails = true;
                }
                this.caseId = caseId;
                this.modality = actualVisit.VTR2_Modality__c;
                this.translatedModality = (String)actualVisit.get('translatedModality');
                this.showPopup = VISIT_SHOW_POPUP;

            } else if (VTD1_Case_Status_History__c.getSObjectType() == rec.getSObjectType()) {
                VTD1_Case_Status_History__c caseStatusHistory = (VTD1_Case_Status_History__c) rec;
                this.status = MILESTONE_STATUS;
                this.label = (String) caseStatusHistory.get('translatedStatus');
                this.sortTieBreaker = (String) caseStatusHistory.get('translatedStatus');
                this.lineStatus = MILESTONE_LINE_STATUS;
                this.itemType = MILESTONE_TYPE;
//                this.itemDate = caseStatusHistory.CreatedDate.date();
                this.itemDate = VT_D1_TranslateHelper.translate(caseStatusHistory.CreatedDate.format('dd-MMM-yyyy'));
                VT_D1_TranslateHelper.translate(caseStatusHistory);
                this.subLabel = VT_D1_TranslateHelper.translate(caseStatusHistory.CreatedDate.format('dd-MMM-yyyy'));
                this.icon = MILESTONE_ICON;//MILESTONE_ICONS.get(caseStatusHistory.VTD1_Status__c);
                this.itemDateTime = caseStatusHistory.CreatedDate;
                this.caseId = caseId;
                this.showPopup = MILESTONE_SHOW_POPUP;
            }
        }

        // for Day1Dose and Day57Dose
        public TimelineItem(Date dateDose, String label, Id caseId) {
            Datetime dt = Datetime.newInstance(dateDose, Time.newInstance(0,0,0,0));

            this.itemType = DOSE_TYPE;
            this.label = label;
            this.sortTieBreaker = label;
            this.itemDate = VT_D1_TranslateHelper.translate(dt.format('dd-MMM-yyyy'));
            this.subLabel = VT_D1_TranslateHelper.translate(dt.format('dd-MMM-yyyy'));
            this.icon = DOSE_ICON;
            this.itemDateTime = dateDose;
            this.caseId = caseId;
            this.showPopup = DOSE_SHOW_POPUP;

            if(dateDose <= Date.today()){
                this.lineStatus = 'complete';
                this.status = DOSE_STATUS;
            } else {
                this.lineStatus = 'inactive';
            }
        }

        public Integer compareTo(Object compareTo) {
            TimelineItem compareToItem = (TimelineItem) compareTo;


            if (itemDateTime != compareToItem.itemDateTime) {
                if (itemDateTime == null) {
                    return 1;
                }
                if (compareToItem.itemDateTime == null) {
                    return -1;
                }
                return itemDateTime < compareToItem.itemDateTime ? -1 : 1;
            }
            if (tieBreakerForVisit != null && compareToItem.tieBreakerForVisit != null) {
                if (tieBreakerForVisit.visitNumber != null && compareToItem.tieBreakerForVisit.visitNumber != null) {
                    return tieBreakerForVisit.visitNumber < compareToItem.tieBreakerForVisit.visitNumber ? -1 : 1;
                }
            }
            return 0;
        }

        /*public Integer compareTo1(Object compareTo) {
            TimelineItem compareToItem = (TimelineItem) compareTo;
            if (lineStatus != compareToItem.lineStatus) {
                return LINE_STATUS_ORDER.get(lineStatus) < LINE_STATUS_ORDER.get(compareToItem.lineStatus) ? -1 : 1;
            } else if (itemDateTime != compareToItem.itemDateTime) {
                if (itemDateTime == null) {
                    return 1;
                }
                if (compareToItem.itemDateTime == null) {
                    return -1;
                }
                return itemDateTime < compareToItem.itemDateTime ? -1 : 1;
            } else if (sortTieBreaker != compareToItem.sortTieBreaker) {
                if (sortTieBreaker == null) {
                    return 1;
                }
                if (compareToItem.sortTieBreaker == null) {
                    return -1;
                }
                return sortTieBreaker < compareToItem.sortTieBreaker ? -1 : 1;
            } else if (sortStatusBreaker != VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT
                    && compareToItem.sortStatusBreaker != VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT
                    && label != compareToItem.label) {
                return label < compareToItem.label ? -1 : 1;
            }
            return 0;
        }*/
    }

    public static TieBreakerForVisit getTieBreakerForVisit(VTD1_Actual_Visit__c av) {
        TieBreakerForVisit stb = new TieBreakerForVisit();
        Set<String> visitStatusSet1 = new Set<String> {VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED,
                VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED,
                VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_REQUESTED,
                VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT};

        Set<String> visitStatusSet2 = new Set<String> {VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED,
                VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_MISSED};

        String visitNumberStr = av.VTD1_Protocol_Visit__r.VTD1_VisitNumber__c;
        Integer visitNumber;
        if (visitNumberStr != null) {
            if (visitNumberStr.contains('o')) {
                visitNumber = Integer.valueOf(visitNumberStr.remove('o')) - 100;
            } else {
                visitNumber = Integer.valueOf(visitNumberStr.remove('v'));
            }
        }
        stb.visitNumber = visitNumber;
        if (av.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED
                || visitStatusSet2.contains(av.VTD1_Status__c)) {
            stb.dateTimeForSorting = av.VTD1_Scheduled_Date_Time__c;
        } else if (visitStatusSet1.contains(av.VTD1_Status__c)) {
            stb.dateTimeForSorting = av.To_Be_Scheduled_Date__c;
            stb.dateTimeForSorting2 = av.VTR2_Status_changed_date__c;
        } else if (av.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED) {
            stb.dateTimeForSorting = av.VTD1_Visit_Completion_Date__c;
        }
        return stb;
    }
}