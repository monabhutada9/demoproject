/**
* @author: Carl Judge
* @date: 16-Aug-19
**/

public without sharing class VT_R3_GlobalSharingLogic_Account extends VT_R3_GlobalSharingLogic_PatientRelated {
    public override Type getType() {
        return VT_R3_GlobalSharingLogic_Account.class;
    }

    public override VTR3_GlobalSharingConfig__mdt getConfig() {
        return getConfigForObjectType(Account.getSObjectType().getDescribe().getName());
    }

    protected override Set<Id> getRecordIdsFromCarePlanIds(Set<Id> caseIds) {
        Set<Id> accIds = new Set<Id>();
        for (Case cas : [
            SELECT Id, Contact.AccountId
            FROM Case
            WHERE Id IN :caseIds
            AND Contact.AccountId != NULL
        ]) {
            accIds.add(cas.Contact.AccountId);
        }
        return accIds;
    }

    protected override void fillRecordToCasesIdMap() {
        for (Case cas : [
            SELECT Id, VTD1_Patient_User__r.Contact.AccountId
            FROM Case
            WHERE Contact.AccountId IN :recordIds
        ]) {
            addToRecordToCaseIdsMap(cas.VTD1_Patient_User__r.Contact.AccountId, cas.Id);
        }
    }

    protected override String getUserQuery() {
        String query =
            'SELECT Id, VTD1_Patient_User__r.Contact.AccountId, VTD1_Study__c' +
                ' FROM Case' +
                ' WHERE (VTD1_Patient_User__c IN :patientIds OR VTD1_Patient_User__c IN :cgPatientIds)';
        String scopeCondition = getScopeCondition();
        if (scopeCondition != null) { query += ' AND ' + scopeCondition; }
        System.debug(query);
        return query;
    }

    protected override String getRemoveQuery() {
        return 'SELECT Id FROM AccountShare WHERE Account.HealthCloudGA__CarePlan__r.'
            + getScopeCondition()
            + ' AND UserOrGroupId IN :includedUserIds';
    }

    private String getScopeCondition() {
        String scopeCondition;
        if (scopedCaseIds != null) {
            scopeCondition = 'Id IN :scopedCaseIds';
        } else if (scopedSiteIds != null) {
            scopeCondition = 'VTD1_Virtual_Site__c IN :scopedSiteIds';
        } else if (scopedStudyIds != null) {
            scopeCondition = 'VTD1_Study__c IN :scopedStudyIds';
        }
        return scopeCondition;
    }

    protected override void createUserShareDetailsFromRecords(List<SObject> records) {
        for (SObject rec : records) {
            Case cas = (Case)rec;
            addUserShareDetailsForCase(cas.VTD1_Patient_User__r.Contact.AccountId, cas);
        }
    }
}