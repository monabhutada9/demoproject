/**
 * Created by Yuliya Yakushenkova on 10/13/2020.
 */

public interface VT_R5_RandomizationHomepage {
    
    VT_R5_RandomizationHomepage getPage(Case patientCase);

    VT_R5_RandomizationHomepage setTeammateSection(List<VT_R5_RandomizationHomepageService.Teammate> teammates);

    VT_R5_RandomizationHomepage setStudyInformation(VT_R5_RandomizationHomepageService.StudyInformation studyInformation);

    VT_R5_RandomizationHomepage addNewsFeed(VT_R5_RandomizationHomepageService.NewsFeedSection newsFeedSection);

}