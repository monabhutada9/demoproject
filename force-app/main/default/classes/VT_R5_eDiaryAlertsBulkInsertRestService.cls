/**
 * Created by dmitry on 30.10.2020.
 */

@RestResource(UrlMapping='/ediary-bulk-alerts')
global with sharing class VT_R5_eDiaryAlertsBulkInsertRestService {

    public class RequestPayload {
        public String emailTemplateName { get; private set; }
        public Set<Id> usersForEmails { get; private set; }
        public Set<Id> usersForPush { get; private set; }
        public List<Task> tasks { get; private set; }
        public List<VTD1_NotificationC__c> communityNotifications { get; private set; }

        public RequestPayload(String emailTemplateName, Set<Id> usersForEmails, List<VTD1_NotificationC__c> communityNotifications, List<Task> tasks) {
            this.emailTemplateName = emailTemplateName;
            this.usersForEmails = usersForEmails;
            this.communityNotifications = communityNotifications;
            this.tasks = tasks;
            this.usersForPush = new Set<Id>();
        }

        public RequestPayload(String emailTemplateName, Set<Id> usersForEmails, List<VTD1_NotificationC__c> communityNotifications, Set<Id> usersForPush) {
            this.emailTemplateName = emailTemplateName;
            this.usersForEmails = usersForEmails;
            this.communityNotifications = communityNotifications;
            this.usersForPush = usersForPush;
            this.tasks = new List<Task>();
        }
    }

    @HttpPost
    global static void doPost() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        try {
            if (!Test.isRunningTest()) {   // Modified in R5.7 for epic# SH-20625 as during test class execution the call directly comes from a batch context and future methods cannot be called from a batch 
            insertAlerts(request.requestBody.toString());
            response.statusCode = 200;
            }
            else{
                RequestPayload alerts = (RequestPayload) JSON.deserialize(request.requestBody.toString(), RequestPayload.class);
        		insertCommunityNotifications(alerts.communityNotifications);
        		insertTasks(alerts.tasks);
        		sendOtherAlerts(alerts);
            }
        } catch (Exception e) {
            response.responseBody = Blob.valueOf(e.getMessage());
            response.statusCode = 500;
        }
    }

    @Future(Callout = true)
    private static void insertAlerts(String requestBody) {
        RequestPayload alerts = (RequestPayload) JSON.deserialize(requestBody, RequestPayload.class);

        insertCommunityNotifications(alerts.communityNotifications);
        insertTasks(alerts.tasks);
        sendOtherAlerts(alerts);

    }

    private static void insertCommunityNotifications(List<VTD1_NotificationC__c> communityNotifications) {

        if (communityNotifications.isEmpty()) { return; }

        VT_R2_NotificationCHandler.eCoaBulkAlerts = true;
        insert communityNotifications;
        VT_R2_NotificationCHandler.sendToNotificationsService(communityNotifications);
    }

    private static void insertTasks(List<Task> tasks) {

        if (tasks.isEmpty()) { return; }

        VT_D2_TaskHandler.eCoaBulkAlerts = true;
        insert tasks;
    }

    private static void sendOtherAlerts(RequestPayload alerts) {

        if (alerts.usersForPush.isEmpty() && alerts.usersForEmails.isEmpty()) { return; }

        PushAndEmailQueueableWrapper queueableWrapper = new PushAndEmailQueueableWrapper(alerts);
        System.enqueueJob(queueableWrapper);
    }

    private class PushAndEmailQueueableWrapper implements Queueable {
        public RequestPayload alerts { get; private set; }

        public PushAndEmailQueueableWrapper(RequestPayload alerts) {
            this.alerts = alerts;
        }

        public void execute(QueueableContext param1) {
            List<VT_R3_EmailsSender.ParamsHolder> emailsParamsHolder = new List<VT_R3_EmailsSender.ParamsHolder>();
            Map<Id, VTD1_NotificationC__c> mapNotificationsToSend = new Map<Id, VTD1_NotificationC__c>();
            Map<Id, User> notificationWithUser = new Map<Id, User>();
            Map<Id, User> usersMap = getUsersMap(this.alerts.usersForPush);

            for (VTD1_NotificationC__c n : this.alerts.communityNotifications) {
                if (this.alerts.usersForEmails.contains(n.OwnerId)) {
                    VT_R3_EmailsSender.ParamsHolder ph = new VT_R3_EmailsSender.ParamsHolder(n.OwnerId, n.Id, this.alerts.emailTemplateName);
                    ph.subjectIsStatic = true;
                    emailsParamsHolder.add(ph);
                }
                if (this.alerts.usersForPush.contains(n.OwnerId)) {
                    mapNotificationsToSend.put(n.Id, n);
                    notificationWithUser.put(n.Id, usersMap.get(n.OwnerId));
                }
            }

            if (!emailsParamsHolder.isEmpty()) {
                VT_R3_EmailsSender.sendEmails(emailsParamsHolder);
            }
            new VT_R5_MobilePushNotifications(notificationWithUser, mapNotificationsToSend)
                    .execute();
        }

        private Map<Id, User> getUsersMap(Set<Id> usersForPush) {
            if (usersForPush == null || usersForPush.isEmpty()) {
                return new Map<Id, User>();
            }

            return new Map<Id, User>([
                    SELECT Id, (SELECT VTR5_ConnectionToken__c, VTR5_ServiceType__c, VTR5_User__c FROM MobilePushRegistrations__r ORDER BY CreatedDate DESC LIMIT 5)
                    FROM User
                    WHERE Id IN :usersForPush
            ]);
        }
    }

}