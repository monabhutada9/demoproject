public without sharing class VT_D1_ContentVersionProcessHandler {
    private static Set <Id> lastContentVersionIds = new Set<Id>();
    private static Set <Id> approvedContentDocIds = new Set<Id>();
    public static Map<Id, VTD1_Document__c> certDocsWithSignatureDateMap = new Map<Id, VTD1_Document__c>();
 
    class LockException extends Exception {

    }
 
    public static void uploadedByGuestProcess(List<ContentVersion> cvList) {
        if (UserInfo.getUserType() != 'Guest') {
            for (ContentVersion cv : cvList) {
                cv.VTD1_Uploaded_by_Guest__c = false;
            }
        }
    }

    public static void processReview(List<ContentVersion> newList, Map<Id, ContentVersion> oldMap) {
        for (ContentVersion cv : newList) {
            if (cv.VTR4_ReviewCompleted__c != oldMap.get(cv.Id).VTR4_ReviewCompleted__c) {
                if (cv.VTR4_ReviewCompleted__c) {
                    cv.VTR4_DateReviewed__c = Datetime.now();
                    cv.VTR4_ReviewedBy__c = UserInfo.getUserId();
                } else {
                    cv.VTR4_DateReviewed__c = null;
                    cv.VTR4_ReviewedBy__c = null;
                }
            }
        }
    }

    /*public static void createContainers(List<ContentVersion> newContentVersionList, Map<Id, ContentVersion> newContentVersionMap) {
        Map<Id, Id> contentDocId_contentVerId = new Map<Id, Id>();
        Map<Id, ContentVersion> contentDocId_contentVer = new Map<Id, ContentVersion>();

        for (ContentVersion newCV : newContentVersionList) {
            contentDocId_contentVerId.put(newCV.ContentDocumentId, newCV.Id);
            contentDocId_contentVer.put(newCV.ContentDocumentId, newContentVersionMap.get(newCV.Id));
        }
        //comment !
        Set<Id> contentDocIdSet = new Set<Id>();
        contentDocIdSet = contentDocId_contentVerId.keySet();

        List<ContentDocumentLink> contentDocLinkList = [
                SELECT Id, ContentDocumentId, LinkedEntityId, ShareType, Visibility
                FROM ContentDocumentLink
                WHERE ContentDocumentId IN:contentDocIdSet
        ];

        System.debug('contentDocLinkList=' + contentDocLinkList);
        if (contentDocLinkList.isEmpty()) return;

        //find study content document is connected with
        Map<Id, Id> contentDocId_studyId = new Map<Id, Id>();
        Map<Id, ContentDocumentLink> contentDocId_study = new Map<Id, ContentDocumentLink>();

        for (ContentDocumentLink ctlItem : contentDocLinkList) {
            //check if study
            if (String.valueOf(ctlItem.LinkedEntityId).indexOf('a093') == 0) {
                contentDocId_studyId.put(ctlItem.ContentDocumentId, ctlItem.LinkedEntityId);
                contentDocId_study.put(ctlItem.ContentDocumentId, ctlItem);
                System.debug('contentDocId=' + ctlItem.ContentDocumentId + ', LinkedEntityId=' + ctlItem.LinkedEntityId);
            }
        }

        if (contentDocId_studyId.keySet().isEmpty()) return;

        //create container records - one VTD1_Document__c record for each content document record
        List<VTD1_Document__c> docList = new List<VTD1_Document__c>();
        Map<Id, VTD1_Document__c> contentDocId_doc = new Map<Id, VTD1_Document__c>();//one doc for each content document
        Map<Id, Id> contentDocId_contentVerIdByStudy = new Map<Id, Id>();

        //specify record type for VTD1_Document__c record
        Id recTypeId;
        List<RecordType> recTypeList = [SELECT Id FROM RecordType WHERE DeveloperName = 'VTD1_Approval_Letter' AND SobjectType = 'VTD1_Document__c'];
        if (!recTypeList.isEmpty()) recTypeId = recTypeList[0].Id;

        for (Id contentDocIdItem : contentDocId_studyId.keySet()) {
            Id contentVerId = contentDocId_contentVerId.get(contentDocIdItem);
            VTD1_Document__c vtd1_doc = new VTD1_Document__c();
            vtd1_doc.VTD1_Study__c = contentDocId_studyId.get(contentDocIdItem); //studyId
            if (recTypeId != null) {
                vtd1_doc.RecordTypeId = recTypeId;
            }
            docList.add(vtd1_doc);
            contentDocId_doc.put(contentDocIdItem, vtd1_doc);
            contentDocId_contentVerIdByStudy.put(contentDocIdItem, contentDocId_contentVerId.get(contentDocIdItem));
            System.debug('VTD1_Document=' + vtd1_doc);
        }
        if (!docList.isEmpty()) insert docList;
        System.debug('Added docs: ' + docList.size());

        //create Content Document Link
        List<ContentDocumentLink> cdlForVTD1DocumentList = new List<ContentDocumentLink>();
        for (Id contentDocIdItem : contentDocId_doc.keySet()) {
            ContentDocumentLink cdl = contentDocId_study.get(contentDocIdItem);
            ContentDocumentLink cdlCopy = cdl.clone(false, true, false, false);
            cdlCopy.LinkedEntityId = contentDocId_doc.get(contentDocIdItem).Id; //VTD1_Document.Id
            cdlForVTD1DocumentList.add(cdlCopy);
            System.debug('!!! cdlCopy=' + cdlCopy);
            //ContentDocumentLink contentDocLink = new ContentDocumentLink();
            //contentDocLink.LinkedEntityId
            //contentDocLink.ContentDocumentId
        }
        if (!cdlForVTD1DocumentList.isEmpty()) insert cdlForVTD1DocumentList;

        *//*
        //link ContentVersion with VTD1_Document__c
        List<ContentVersion> cvStudyList = [select Id, ContentDocumentId, VTD1_DocumentId__c from ContentVersion where Id in:contentDocId_contentVerIdByStudy.values()];//new List<ContentVersion>();
        map<Id, ContentVersion> contentDocId_contentVerByStudy = new map<Id, ContentVersion>();
        for(ContentVersion cvItem:cvStudyList){
            contentDocId_contentVerByStudy.put(cvItem.ContentDocumentId, cvItem);
        }

        List<ContentVersion> cvList = new List<ContentVersion>();
        for(Id contentDocIdItem:contentDocId_doc.keySet()){
            Id docId = contentDocId_doc.get(contentDocIdItem).Id;

            contentDocId_contentVerByStudy.get(contentDocIdItem).VTD1_DocumentId__c = docId;
            //cv.VTD1_DocumentId__c = docId;
            //cvList.add(cv);
        }
        system.debug('link ContentVersion with VTD1_Document__c');
        update contentDocId_contentVerByStudy.values();

        //if(!cvList.isEmpty()) update cvList;
        *//*
    }*/

    private static void processVersionContent(Map <Id, Id> entityIdToContentDocIdMap) {
        Id Document_Regulatory_Document = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT;
        Id Document_EligibilityAssessmentForm = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE;
        List <VTD1_Document__c> documents = [
                SELECT Name, VTD1_Version__c, VTD1_Status__c, VTD1_Current_Workflow__c, VTD1_Document_Locked__c,
                        VTD1_Study_RSU__c, VTD1_Study_IRB_Flag__c, VTD1_Site_RSU__c, VTD1_Site_IRB_Flag__c, VTD1_ISF__c, VTD1_TMF__c,
                        VTD2_PI_Signature_required__c, VTD1_Signature_Date__c, VTD1_Regulatory_Document_Type__c, RecordTypeId,
                        VTD1_Site__c, VTD1_Site__r.VTD1_EDP_Done__c, VTD1_Site__r.VTD1_EDP_In_Progress__c, VTR4_EDP_Completed__c,
                        VTD1_Protocol_Amendment__c, VTD2_EAFStatus__c, VTR4_Signature_Type__c
                FROM VTD1_Document__c
                WHERE Id IN :entityIdToContentDocIdMap.keySet() AND
                RecordTypeId IN (:Document_Regulatory_Document, :Document_EligibilityAssessmentForm)
        ];
        if (documents.isEmpty()) {
            return;
        }
        Map <Id, ContentVersion> contentIdToContentVersionMap = new Map<Id, ContentVersion>();
        List <ContentVersion> contentVersions = [
                SELECT Id, ContentDocumentId, VTD1_CompoundVersionNumber__c, VTD1_FullVersion__c, VTD1_Lifecycle_State__c, VTD1_Current_Version__c
                FROM ContentVersion
                WHERE ContentDocumentId IN :entityIdToContentDocIdMap.values() AND IsLatest = TRUE
        ];
        for (ContentVersion contentVersion : contentVersions) {
            contentIdToContentVersionMap.put(contentVersion.ContentDocumentId, contentVersion);
        }
        if (contentIdToContentVersionMap.isEmpty()) {
            return;
        }
        List<ContentVersion> contentVersionForUpdate = new List<ContentVersion>();
        certDocsWithSignatureDateMap = getCertDocsWithSignatureDate(documents);
        for (VTD1_Document__c document : documents) {
            ContentVersion contentVersion = contentIdToContentVersionMap.get(entityIdToContentDocIdMap.get(document.Id));
            document.VTD1_Version__c = contentVersion.VTD1_CompoundVersionNumber__c;
            if (document.RecordTypeId == Document_Regulatory_Document) {
                contentVersion.VTD1_CompoundVersionNumber__c = '0.1';
                contentVersion.VTD1_Current_Version__c = true;
                document.VTD1_Lifecycle_State__c = contentVersion.VTD1_Lifecycle_State__c;
                if (validateSignature(document)) {
                    handleFlags(document);
                }
                List <String> parts = new List<String>();
                parts.add(contentVersion.VTD1_CompoundVersionNumber__c);
                parts.add('Current');
                parts.add(contentVersion.VTD1_Lifecycle_State__c);
                parts.add(document.VTD1_Current_Workflow__c);
                contentVersion.VTD1_FullVersion__c = String.join(parts, ',');
                contentVersionForUpdate.add(contentVersion);
            }
        }
        if (!contentVersionForUpdate.isEmpty()) {
            update contentVersionForUpdate; //contentVersions;
        }
        update documents;
    }

    /*public static void onDCLInsertBefore(List <ContentDocumentLink> contentDocumentLinks) {
        System.debug('onDCLInsertBefore ' + contentDocumentLinks);
        Map <Id, ContentDocumentLink> entityIdToLinkMap = new Map<Id, ContentDocumentLink>();
        for (ContentDocumentLink contentDocumentLink : contentDocumentLinks) {
            contentDocumentLink.Visibility = 'AllUsers'; //Sharing for PI
            entityIdToLinkMap.put(contentDocumentLink.LinkedEntityId, contentDocumentLink);
        }
        List <VTD1_Document__c> documents = [SELECT Id FROM VTD1_Document__c WHERE Id IN :entityIdToLinkMap.keySet()];
        System.debug('onDCLInsertBefore docs = ' + documents);
        for (VTD1_Document__c document : documents) {
            System.debug('!!! entityIdToLinkMap.get(document.Id).ShareType=' + entityIdToLinkMap.get(document.Id).ShareType);
            System.debug('!!! LinkedEntityId=' + entityIdToLinkMap.get(document.Id).LinkedEntityId);
            if (entityIdToLinkMap.get(document.Id).ShareType != 'I') {
                entityIdToLinkMap.get(document.Id).ShareType = 'I';
            }
        }
        List<VTR4_TemplateContainer__c> tcList = [SELECT Id FROM VTR4_TemplateContainer__c WHERE Id IN :entityIdToLinkMap.keySet()];
        for (VTR4_TemplateContainer__c tc : tcList) {
            if (entityIdToLinkMap.get(tc.Id).ShareType != 'I') {
                entityIdToLinkMap.get(tc.Id).ShareType = 'I';
            }
        }
    }*/

//    @Future
//    public static void onDCLSetSharingFuture(Set <Id> contentDocumentLinkIds) {
//        onDCLSetSharing(contentDocumentLinkIds);
//    }

    public static void onDCLSetSharing(List<ContentDocumentLink> contentDocumentLinks) {
        Set<SObjectType> sobjectTypesToShare = new Set<SObjectType>{
                VTD1_Document__c.SObjectType,
                VTR4_TemplateContainer__c.SObjectType,
                Virtual_Site__c.SObjectType
        };
        for (ContentDocumentLink cdl : contentDocumentLinks) {
            if (sobjectTypesToShare.contains(cdl.LinkedEntityId.getSobjectType())) {
                cdl.ShareType = 'I';
                cdl.Visibility = 'AllUsers';
            }
        }
    }

    public static void onDCLInsertAfter(List <ContentDocumentLink> contentDocumentLinks) {
        Map <Id, Id> entityIdToContentDocIdMap = new Map<Id, Id>();
        for (ContentDocumentLink contentDocumentLink : contentDocumentLinks) {
            if ((contentDocumentLink.LinkedEntityId).getSobjectType() == dsfs__DocuSign_Status__c.SObjectType) {
                entityIdToContentDocIdMap.put(contentDocumentLink.LinkedEntityId, contentDocumentLink.ContentDocumentId);
            }
        }


        if (entityIdToContentDocIdMap.isEmpty()) {


            return;
        }
        List <dsfs__DocuSign_Status__c> docuSignStatuses = [
                SELECT Id, dsfs__Envelope_Status__c, VTD1_Document__c, dsfs__Completed_Date_Time__c
                FROM dsfs__DocuSign_Status__c
                WHERE Id IN :entityIdToContentDocIdMap.keySet() AND dsfs__Envelope_Status__c = 'Completed'
        ];
        if (docuSignStatuses.isEmpty()) {
            processVersionContent(entityIdToContentDocIdMap);
            return;
        }




        Set <Id> docuSignContentDocIds = new Set<Id>();
        Map <Id, Id> docuSignToDocIdMap = new Map<Id, Id>();
        for (dsfs__DocuSign_Status__c docuSignStatus : docuSignStatuses) {
            if (docuSignStatus.VTD1_Document__c != null) {
            docuSignToDocIdMap.put(docuSignStatus.Id, docuSignStatus.VTD1_Document__c);


            }
            docuSignContentDocIds.add(entityIdToContentDocIdMap.get(docuSignStatus.Id));
        }
        if (docuSignToDocIdMap.isEmpty()) {
            return;


        }
        Map <Id, VTD1_Document__c> documentMap = new Map<Id, VTD1_Document__c>([
                SELECT Id, VTD1_Signature_Date__c, RecordTypeId, VTD1_Regulatory_Document_Type__c, VTD1_Protocol_Amendment__c, VTD1_Site__c
                FROM VTD1_Document__c
                WHERE Id IN :docuSignToDocIdMap.values()
        ]);
        for (dsfs__DocuSign_Status__c docuSignStatus : docuSignStatuses) {
            if (docuSignStatus.dsfs__Completed_Date_Time__c != null && documentMap != null && documentMap.get(docuSignStatus.VTD1_Document__c) != null) {
                VTD1_Document__c document = documentMap.get(docuSignStatus.VTD1_Document__c);
                document.VTD1_Signature_Date__c = docuSignStatus.dsfs__Completed_Date_Time__c.date();
                if (VT_D1_DocumentCHandler.isPASignaturePage(document)) {
                    document.VTD2_EAFStatus__c = VT_R4_ConstantsHelper_Documents.DOCUMENT_EAF_STATUS_COMPLETED;
                }
            }
            docuSigncontentDocIds.add(entityIdToContentDocIdMap.get(docuSignStatus.Id));
        }
        update documentMap.values();
        List <ContentVersion> attachments = [
                SELECT ContentDocumentId, Title, PathOnClient, VersionData
                FROM ContentVersion
                WHERE ContentDocumentId IN :docuSigncontentDocIds
                AND IsLatest = TRUE
        ];
        Map <Id, ContentVersion> contentDocIdToContentVersionMap = new Map<Id, ContentVersion>();
        for (ContentVersion contentVersion : attachments) {
            contentDocIdToContentVersionMap.put(contentVersion.ContentDocumentId, contentVersion);
        }


        Map <Id, Id> docIdToContentDocIdMap = new Map<Id, Id>();
        for (ContentDocumentLink cdl : [

                SELECT ContentDocumentId, LinkedEntityId
                FROM ContentDocumentLink
                WHERE LinkedEntityId IN :docuSignToDocIdMap.values()
        ]) {



            docIdToContentDocIdMap.put(cdl.LinkedEntityId, cdl.ContentDocumentId);
        }
        List <ContentVersion> contentVersions = new List<ContentVersion>();
        for (Id id : docuSignToDocIdMap.keySet()) {
            Id attachmentContentId = entityIdToContentDocIdMap.get(id);
            ContentVersion attachment = attachmentContentId != null ? contentDocIdToContentVersionMap.get(attachmentContentId) : null;
            Id documentId = docuSignToDocIdMap.get(id);
            Id contentDocumentId = docIdToContentDocIdMap.get(documentId);
            if (attachment != null) {
                ContentVersion contentVersion = new ContentVersion(
                        ContentDocumentId = contentDocumentId,
                        Title = attachment.Title,
                        PathOnClient = attachment.PathOnClient,
                        VersionData = attachment.VersionData,
                        IsMajorVersion = true);
                contentVersions.add(contentVersion);
            }
        }
        if (!contentVersions.isEmpty()) {
            insert contentVersions;
        }
    }

    public static void setCompoundVersionNumberAfter(List<ContentVersion> newContentVersionList) {
        List <Id> contentVersionIds = new List<Id>();
        List <Id> contentDocIds = new List<Id>();
        Map <Id, ContentVersion> originalContentVersionsByIds = new Map<Id, ContentVersion>();
        for (ContentVersion contentVersion : newContentVersionList) {
            contentVersionIds.add(contentVersion.Id);
            contentDocIds.add(contentVersion.ContentDocumentId);
            originalContentVersionsByIds.put(contentVersion.Id, contentVersion);
        }
        List <ContentVersion> contentVersionsToUpdate = [

                SELECT Id, ContentDocumentId, VTD1_Lifecycle_State__c, PathOnClient,

                        VTD1_Current_Version__c, VTD1_CompoundVersionNumber__c, VTD1_FullVersion__c
                FROM ContentVersion
                WHERE Id IN :contentVersionIds
        ];
        List <ContentVersion> lastContentVersions = [
                SELECT VTD1_CompoundVersionNumber__c, ContentDocumentId
                FROM ContentVersion
                WHERE Id IN :lastContentVersionIds
        ];

        Map <Id, ContentVersion> contentDocIdToLastVersionMap = new Map<Id, ContentVersion>();
        Map <Id, String> contentDocIdToVersion = new Map<Id, String>();
        Map <Id, String> contentDocIdToLifeCycleState = new Map<Id, String>();
        for (ContentVersion contentVersion : lastContentVersions) {
            contentDocIdToLastVersionMap.put(contentVersion.ContentDocumentId, contentVersion);
        }
        for (ContentVersion contentVersion : contentVersionsToUpdate) {
            ContentVersion lastContentVersion = contentDocIdToLastVersionMap.get(contentVersion.ContentDocumentId);
            if (lastContentVersion != null) {
                List <String> parts = lastContentVersion.VTD1_CompoundVersionNumber__c.split('\\.');
                contentVersion.VTD1_CompoundVersionNumber__c = parts[0] + '.' + (Integer.valueOf(parts[1]) + 1);
            } else {
                contentVersion.VTD1_CompoundVersionNumber__c = '0.1';
            }
            contentVersion.VTD1_Current_Version__c = true;
            System.debug('tested ContentDocumentId ' + contentVersion.ContentDocumentId + ' ' + contentVersion.VTD1_CompoundVersionNumber__c);
            System.debug('tested ContentDocumentId ' + approvedContentDocIds);
            if (approvedContentDocIds.contains(contentVersion.ContentDocumentId)) {
                contentVersion.VTD1_Lifecycle_State__c = 'Draft';
            }
            contentDocIdToVersion.put(contentVersion.ContentDocumentId, contentVersion.VTD1_CompoundVersionNumber__c);
            contentDocIdToLifeCycleState.put(contentVersion.ContentDocumentId, contentVersion.VTD1_Lifecycle_State__c);
        }
        List <ContentDocumentLink> cdls = [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE ContentDocumentId IN :contentDocIds];
        Map <Id, Id> docIdToContentDocIdMap = new Map<Id, Id>();
        Map <Id, ContentVersion> contentIdToContentVersionMap = new Map<Id, ContentVersion>();
        for (ContentVersion contentVersion : contentVersionsToUpdate) {
            contentIdToContentVersionMap.put(contentVersion.ContentDocumentId, contentVersion);
        }
        for (ContentDocumentLink cdl : cdls) {
            System.debug('cdl = ' + cdl);
            docIdToContentDocIdMap.put(cdl.LinkedEntityId, cdl.ContentDocumentId);
        }
        List <VTD1_Document__c> documents = [
                SELECT Name, VTD1_Version__c, VTD1_Status__c, VTD1_Lifecycle_State__c, VTD1_Current_Workflow__c, VTD1_Document_Locked__c,
                        VTD1_Study_RSU__c, VTD1_Study_IRB_Flag__c, VTD1_Site_RSU__c, VTD1_Site_IRB_Flag__c, VTD1_ISF__c, VTD1_TMF__c,
                        VTD1_ISF_Complete__c, VTD1_TMF_Complete__c, VTD1_Site_RSU_Complete__c, VTD1_Study_RSU_Complete__c,
                        VTD2_PI_Signature_required__c, VTD1_Signature_Date__c, VTD1_Regulatory_Document_Type__c, RecordTypeId,
                        VTD1_Site__c, VTD1_Site__r.VTD1_EDP_Done__c, VTD1_Site__r.VTD1_EDP_In_Progress__c, VTR4_EDP_Completed__c,
                        VTD1_Protocol_Amendment__c, VTD2_EAFStatus__c, VTR4_Signature_Type__c

                FROM VTD1_Document__c
                WHERE Id IN :docIdToContentDocIdMap.keySet()
        ];
        System.debug('documents = ' + documents);

        certDocsWithSignatureDateMap = getCertDocsWithSignatureDate(documents);
        for (VTD1_Document__c document : documents) {
            ContentVersion contentVersion = contentIdToContentVersionMap.get(docIdToContentDocIdMap.get(document.Id));
            if (document.VTD1_Document_Locked__c) {
                originalContentVersionsByIds.get(contentVersion.Id).addError(Label.VT_D1DocumentLocked);
            }
            System.debug(document.VTD1_Version__c + ' ---Version--- ' + contentVersion.VTD1_CompoundVersionNumber__c);
            System.debug(document.VTD1_Lifecycle_State__c + ' ---LifecycleState--- ' + contentVersion.VTD1_Lifecycle_State__c);
            document.VTD1_Version__c = contentVersion.VTD1_CompoundVersionNumber__c;
            document.VTD1_Lifecycle_State__c = contentVersion.VTD1_Lifecycle_State__c;
            if (contentVersion.VTD1_CompoundVersionNumber__c != null && !contentVersion.VTD1_CompoundVersionNumber__c.equals('0.1')) {
                document.VTD1_FileNames__c =
                        (contentVersion.PathOnClient.substring(0, 1) == '/') ?
                                contentVersion.PathOnClient.substring(1, contentVersion.PathOnClient.length()) :
                                contentVersion.PathOnClient;
            }

            if (//document.RecordType.Name == 'Regulatory Document'
                    document.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT
                            && document.VTR4_Signature_Type__c == 'Wet') {

                document.VTD2_Certification__c = null;
            }

            if (VT_D1_HelperClass.getProfileNameOfCurrentUser() == 'Study Concierge') {
                document.VTR3_Upload_File_Completed__c = true;
            }
            if (validateSignature(document)) handleFlags(document);
            System.debug('cv = ' + contentVersion + ' ' + document.VTD1_Current_Workflow__c);
            List <String> parts = new List<String>();
            parts.add(contentVersion.VTD1_CompoundVersionNumber__c);
            parts.add('Current');
            parts.add(contentVersion.VTD1_Lifecycle_State__c);
            parts.add(document.VTD1_Current_Workflow__c);
            contentVersion.VTD1_FullVersion__c = String.join(parts, ',');
        }
        update contentVersionsToUpdate;
        update documents;
    }

    public static void setCompoundVersionNumberBefore(List<ContentVersion> newContentVersionList) {
        List <Id> contentDocumentIds = new List<Id>();
        for (ContentVersion contentVersion : newContentVersionList) {
            contentDocumentIds.add(contentVersion.ContentDocumentId);
        }
        if (contentDocumentIds.isEmpty()) {
            return;
        }
        List <ContentVersion> lastContentVersions = [
                SELECT VTD1_Current_Version__c, VTD1_Lifecycle_State__c, ContentDocumentId, VTD1_FullVersion__c
                FROM ContentVersion
                WHERE ContentDocumentId IN :contentDocumentIds AND VTD1_Current_Version__c = TRUE
        ];
        for (ContentVersion contentVersion : lastContentVersions) {
            contentVersion.VTD1_Current_Version__c = false;
            if (contentVersion.VTD1_FullVersion__c != null) {
                contentVersion.VTD1_FullVersion__c = contentVersion.VTD1_FullVersion__c.replace(',Current', '');
            }
            lastContentVersionIds.add(contentVersion.Id);
            if (contentVersion.VTD1_Lifecycle_State__c == 'Approved') {
                contentVersion.VTD1_Lifecycle_State__c = 'Superseded';
                System.debug('added ContentDocumentId ' + contentVersion.ContentDocumentId);
                approvedContentDocIds.add(contentVersion.ContentDocumentId);
            }
        }
        if (!lastContentVersions.isEmpty()) {
            update lastContentVersions;
        }
    }

    public static void onStatusChanged(List <ContentVersion> contentVersions, Map <Id, ContentVersion> oldMap) {
        Set <Id> contentDocIds = new Set<Id>();
        Map <Id, String> contentDocIdToVersion = new Map<Id, String>();
        Map <Id, String> contentDocIdToLifeCycleState = new Map<Id, String>();
        Map <Id, ContentVersion> contentVersionMap = new Map<Id, ContentVersion>();
        for (ContentVersion contentVersion : contentVersions) {
            if (contentVersion.IsLatest && contentVersion.VTD1_Lifecycle_State__c == 'Approved' && contentVersion.VTD1_Lifecycle_State__c != oldMap.get(contentVersion.Id).VTD1_Lifecycle_State__c) {
                List <String> parts = contentVersion.VTD1_CompoundVersionNumber__c.split('\\.');
                contentVersion.VTD1_CompoundVersionNumber__c = (Integer.valueOf(parts[0]) + 1) + '.0';
                contentDocIds.add(contentVersion.ContentDocumentId);
                contentDocIdToVersion.put(contentVersion.ContentDocumentId, contentVersion.VTD1_CompoundVersionNumber__c);
                contentDocIdToLifeCycleState.put(contentVersion.ContentDocumentId, contentVersion.VTD1_Lifecycle_State__c);
                contentVersionMap.put(contentVersion.ContentDocumentId, contentVersion);
            }
        }
        if (contentDocIds.isEmpty()) {
            return;
        }

        System.debug('onStatusChanged, contentDocIds = ' + contentDocIds);
        Map <Id, Id> entityIdToContentDocIdMap = new Map<Id, Id>();
        List <ContentDocumentLink> contentDocumentLinks = [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE ContentDocumentId IN :contentDocIds];
        for (ContentDocumentLink cdl : contentDocumentLinks) {
            entityIdToContentDocIdMap.put(cdl.LinkedEntityId, cdl.ContentDocumentId);
        }
        List <VTD1_Document__c> documents = [SELECT Id, VTD1_Version__c, VTD1_Current_Workflow__c FROM VTD1_Document__c WHERE Id IN :entityIdToContentDocIdMap.keySet()];
        for (VTD1_Document__c document : documents) {
            Id contentDocumentId = entityIdToContentDocIdMap.get(document.Id);
            document.VTD1_Version__c = contentDocIdToVersion.get(contentDocumentId);
            document.VTD1_Lifecycle_State__c = contentDocIdToLifeCycleState.get(contentDocumentId);
            System.debug('set doc version = ' + document.VTD1_Version__c);
            ContentVersion contentVersion = contentVersionMap.get(entityIdToContentDocIdMap.get(document.Id));
            System.debug('cv = ' + contentVersion);
            List <String> parts = new List<String>();
            parts.add(contentVersion.VTD1_CompoundVersionNumber__c);
            parts.add('Current');
            parts.add(contentVersion.VTD1_Lifecycle_State__c);
            parts.add(document.VTD1_Current_Workflow__c);
            contentVersion.VTD1_FullVersion__c = String.join(parts, ',');
        }
        update documents;
    }

    public static Map<Id, VTD1_Document__c> getCertDocsWithSignatureDate(List<VTD1_Document__c> documents) {
        Set<Id> documentIds = new Set<Id>();
        for (VTD1_Document__c document : documents) {
            if (document.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT
                    && document.VTD2_PI_Signature_required__c
                    && document.VTR4_Signature_Type__c == 'Wet'
                    ) {
                documentIds.add(document.Id);
            }
        }
        System.debug('getCertDocsWithSignatureDate documentIds ' + documentIds);

        Map<Id, VTD1_Document__c> certificationMap = new Map<Id, VTD1_Document__c>();
        for (VTD1_Document__c certification : [
                SELECT Id, VTD1_Signature_Date__c, VTD2_Associated_Medical_Record_id__c
                FROM VTD1_Document__c
                WHERE RecordTypeId = :VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON
                AND VTD2_Associated_Medical_Record_id__c IN :documentIds AND VTD1_Signature_Date__c <> NULL
        ]) {
            certificationMap.put(certification.VTD2_Associated_Medical_Record_id__c, certification);
        }
        System.debug('certificationMap ' + certificationMap);
        return certificationMap;
    }

    public static Boolean validateSignature(VTD1_Document__c document) {
        System.debug('validateSignature ' + document);
        if (document.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON
                || (document.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT
                && document.VTD2_PI_Signature_required__c
                && (document.VTD1_Signature_Date__c == null
                || (document.VTR4_Signature_Type__c == 'Wet'
                && certDocsWithSignatureDateMap.get(document.Id) == null)))
                ) {
            System.debug('validateSignature false ');
            return false;
        }
        System.debug('validateSignature true');
        return true;
    }

    public static void handleFlags(VTD1_Document__c document) {
        System.debug('Study RSU = ' + document.VTD1_Study_RSU__c);
        System.debug('Study IRB = ' + document.VTD1_Study_IRB_Flag__c);
        System.debug('Site RSU = ' + document.VTD1_Site_RSU__c);
        System.debug('Site IRB = ' + document.VTD1_Site_IRB_Flag__c);
        System.debug('ISF = ' + document.VTD1_ISF__c);
        System.debug('TMF = ' + document.VTD1_TMF__c);
        System.debug('workflow = ' + document.VTD1_Current_Workflow__c);
        System.debug('-+-+document = ' + document);
        if (document.VTD1_Lifecycle_State__c != 'Draft' || document.VTD1_Status__c == 'TMF Rejected') {
            return;
        }

        Boolean clearCompleteFlags = false;
        Boolean isPA = VT_D1_DocumentCHandler.isPASignaturePage(document);
        if (isPA && document.VTD2_PI_Signature_required__c
                && document.VTD2_EAFStatus__c != 'Completed') {
            clearCompleteFlags = true;
        } else if (document.VTD1_Study_RSU__c && !document .VTD1_Study_IRB_Flag__c) {
            document.VTD1_Status__c = 'Study RSU Started';
            document.VTD1_Current_Workflow__c = 'Study RSU No IRB';
            clearCompleteFlags = true;
        } else if (document.VTD1_Study_RSU__c && document.VTD1_Study_IRB_Flag__c) {
            document.VTD1_Status__c = 'Study RSU Started';
            document.VTD1_Current_Workflow__c = 'Study RSU With IRB';
            clearCompleteFlags = true;
        } else if ((!isPA || (!document.VTR4_EDP_Completed__c))
                && document.VTD1_Site_RSU__c && !document.VTD1_Site_IRB_Flag__c) {
            document.VTD1_Status__c = 'Site RSU Not Started';
            document.VTD1_Current_Workflow__c = 'Site RSU No IRB';
            clearCompleteFlags = true;
            System.debug('clearCompleteFlags =-=-= true');
        } else if ((!isPA || (!document.VTR4_EDP_Completed__c))
                && document.VTD1_Site_RSU__c && document.VTD1_Site_IRB_Flag__c) {
            document.VTD1_Status__c = 'Site RSU Not Started';
            document.VTD1_Current_Workflow__c = 'Site RSU With IRB';
            clearCompleteFlags = true;
        } else if (document.VTD1_ISF__c && (!document.VTD1_Site_RSU__c || isPA)
                && document.RecordTypeId != VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE
//                && (document.VTD2_PI_Signature_required__c == false ||
//                        (document.VTD2_PI_Signature_required__c == true &&
//                                (document.VTD1_Regulatory_Document_Type__c == 'Subject Identification Log' ||
//                                        document.VTD1_Regulatory_Document_Type__c == 'Subject Screening Log' ||
//                                        document.VTD1_Regulatory_Document_Type__c == 'Source Note of Transfer' ||
//                                        document.VTD1_Regulatory_Document_Type__c == 'Target Note of Transfer' ||
//                                        document.VTD1_Regulatory_Document_Type__c == 'Note to File Step Down' ||
//                                        document.VTD1_Regulatory_Document_Type__c == VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_DOA_LOG) &&
//                                document.VTD1_Signature_Date__c != null))
                ) {
            document.VTD1_Status__c = 'ISF Started';
            document.VTD1_Current_Workflow__c = 'ISF';
            clearCompleteFlags = true;
        } else if (document.VTD1_TMF__c && !document.VTD1_ISF__c && !document.VTD1_Study_RSU__c && !document.VTD1_Site_RSU__c) {
            document.VTD1_Status__c = 'TMF Started';
            document.VTD1_Current_Workflow__c = 'TMF';
            clearCompleteFlags = true;
        }
        if (clearCompleteFlags) {
            System.debug('clearCompleteFlags true');
            document.VTD1_Study_RSU_Complete__c = false;
            document.VTD1_Site_RSU_Complete__c = false;
            document.VTD1_ISF_Complete__c = false;
            document.VTD1_TMF_Complete__c = false;
        }
    }

    public static void onDeleteBefore(List <ContentDocument> contentDocuments) {
        Set <Id> contentDocIds = new Set<Id>();
        Map <Id, ContentDocument> contentDocumentMap = new Map<Id, ContentDocument>();
        for (ContentDocument contentDocument : contentDocuments) {
            contentDocIds.add(contentDocument.Id);
            contentDocumentMap.put(contentDocument.Id, contentDocument);
        }
        List <ContentDocumentLink> contentDocumentLinks = [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE ContentDocumentId IN :contentDocIds];
        System.debug('onDeleteBefore = ' + contentDocumentLinks);
        Map <Id, Id> entityIdToContentDocId = new Map<Id, Id>();
        for (ContentDocumentLink contentDocumentLink : contentDocumentLinks) {
            entityIdToContentDocId.put(contentDocumentLink.LinkedEntityId, contentDocumentLink.ContentDocumentId);
        }
        List <EmailMessage> messages = [SELECT Id FROM EmailMessage WHERE Id IN :entityIdToContentDocId.keySet()];
        System.debug('messages = ' + messages);
        for (EmailMessage message : messages) {
            ContentDocument contentDocument = contentDocumentMap.get(entityIdToContentDocId.get(message.Id));
            contentDocument.addError(Label.VT_D1OperationForbidden);
        }
    }

    public static void updateContentDocumentVersion(List<ContentVersion> contentVersions) {
        System.debug('VT_D1_ContentVersionProcessHandler::updateContentDocumentVersion::659 ');
        Set<Id> contentDocumentIds = new Set<Id>();
        for (ContentVersion currentContentVersion : contentVersions) {
            contentDocumentIds.add(currentContentVersion.ContentDocumentId);
        }
        List<ContentDocumentLink> contentDocumentLinks = [
                SELECT ContentDocumentId,
                        LinkedEntityId,
                        ContentDocument.LastModifiedDate
                FROM ContentDocumentLink
                WHERE ContentDocumentId IN:contentDocumentIds
        ];
        VT_D1_ContentDocumentLinkProcessHandler.recalculateLastFileUploadedDate(contentDocumentLinks);
    }

    //Uncommented for SH-18740
    public static void blockUploadForSignMonVisitTask(List<ContentVersion> newCVList) {
        Map<Id, List<ContentVersion>> mvToCVMap = new Map<Id, List<ContentVersion>>();
        for (ContentVersion newCV : newCVList) {
            if (newCV.FirstPublishLocationId != null && newCV.FirstPublishLocationId.getSobjectType() == VTD1_Monitoring_Visit__c.getSObjectType()) {
                List<ContentVersion> exactCVList = mvToCVMap.get(newCV.FirstPublishLocationId);
                if (exactCVList == null) exactCVList = new List<ContentVersion>();
                exactCVList.add(newCV);
                mvToCVMap.put(newCV.FirstPublishLocationId, exactCVList);
            } else {
                return;
            }
        }

        List<VTD1_Monitoring_Visit__c> blockedMVList = [
                SELECT Id,
                        Follow_Up_Letter_Status__c,
                        VTR5_SignMVReportTaskSent__c,
                        VTR5_FollowUpLetterSubmitted__c
                FROM VTD1_Monitoring_Visit__c
                WHERE Id
                        IN :mvToCVMap.keySet()
        ];

        List<ContentVersion> wrongCVList = new List<ContentVersion>();
        for (VTD1_Monitoring_Visit__c blockedMV : blockedMVList) {
            if (blockedMV.VTR5_SignMVReportTaskSent__c == true || blockedMV.VTR5_FollowUpLetterSubmitted__c == true && blockedMV.Follow_Up_Letter_Status__c != 'Approved') {
                wrongCVList.addAll(mvToCVMap.get(blockedMV.Id));
            }
        }

        for (ContentVersion wrongCV : wrongCVList) {
            wrongCV.addError('A file already exists for this task.');
        }
    }

}