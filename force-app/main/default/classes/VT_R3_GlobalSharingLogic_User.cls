/**
* @author: Carl Judge
* @date: 16-Aug-19
* @description:
**/

public without sharing class VT_R3_GlobalSharingLogic_User extends VT_R3_GlobalSharingLogic_PatientRelated {
    public override Type getType() {
        return VT_R3_GlobalSharingLogic_User.class;
    }

    public override VTR3_GlobalSharingConfig__mdt getConfig() {
        return getConfigForObjectType(User.getSObjectType().getDescribe().getName());
    }

    protected override Set<Id> getRecordIdsFromCarePlanIds(Set<Id> caseIds) {
        return new Map<Id, User>([
            SELECT Id
            FROM User
            WHERE Contact.VTD1_Clinical_Study_Membership__c IN :caseIds
            LIMIT :Limits.getLimitQueryRows() - Limits.getQueryRows()
        ]).keySet();
    }

    protected override void fillRecordToCasesIdMap() {
        Map<Id, Set<Id>> accToUserIds = new Map<Id, Set<Id>>();
        for (User item : [SELECT Id, Contact.AccountId FROM User WHERE Contact.AccountId != null AND Id IN :recordIds]) {
            if (! accToUserIds.containsKey(item.Contact.AccountId)) {
                accToUserIds.put(item.Contact.AccountId, new Set<Id>());
            }
            accToUserIds.get(item.Contact.AccountId).add(item.Id);
        }
        for (Case cas : [
            SELECT Id, VTD1_Patient_User__r.Contact.AccountId
            FROM Case
            WHERE VTD1_Patient_User__r.Contact.AccountId IN :accToUserIds.keySet()
        ]) {
            for (Id userId : accToUserIds.get(cas.VTD1_Patient_User__r.Contact.AccountId)) {
                addToRecordToCaseIdsMap(userId, cas.Id);
            }
        }
    }

    protected override String getUserQuery() {
        String query =
            'SELECT Id,' +
                'Contact.VTD1_Clinical_Study_Membership__r.Id,' +
                'Contact.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c,' +
                'Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c' +
            ' FROM User' +
            ' WHERE (Contact.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c IN :patientIds' +
                ' OR Contact.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c IN :cgPatientIds' +
            ')';
        String scopeCondition = getScopeCondition();
        if (scopeCondition != null) { query += ' AND ' + scopeCondition; }
        System.debug(query);

        return (String) new VT_Stubber.ResultStub(
            'VT_R3_GlobalSharingLogic_User.getUserQuery',
            query
        ).getResult();
    }

    protected override String getRemoveQuery() {
        return 'SELECT Id FROM UserShare WHERE User.'
            + getScopeCondition()
            + ' AND UserOrGroupId IN :includedUserIds';
    }

    private String getScopeCondition() {
        String scopeCondition;
        if (scopedCaseIds != null) {
            scopeCondition = 'Contact.VTD1_Clinical_Study_Membership__r.Id IN :scopedCaseIds';
        } else if (scopedSiteIds != null) {
            scopeCondition = 'Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__c IN :scopedSiteIds';
        } else if (scopedStudyIds != null) {
            scopeCondition = 'Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c IN :scopedStudyIds';
        }
        return scopeCondition;
    }

    protected override void createUserShareDetailsFromRecords(List<SObject> records) {
        for (SObject rec : records) {
            User usr = (User)rec;
            Case cas = usr.Contact.VTD1_Clinical_Study_Membership__r;
            addUserShareDetailsForCase(usr.Id, cas);
        }
    }
}