/**
 * @author: Alexander Komarov
 * @date: 26.11.2020
 * @description:
 */

@IsTest
public with sharing class VT_R5_MobilePISCRCalendarTest {
    static User u;
    static {
        u = [SELECT Id FROM User WHERE Username =:VT_R5_AllTestsMobile.PI_USERNAME];
    }
    public static void firstTest() {
        System.runAs(u) {
            new VT_R5_MobilePISCRCalendar().getMinimumVersions();
            VT_R5_AllTestsMobile.setRestContext('/mobile/v1/calendar', 'GET');
            VT_R5_MobileRouter.doGET();
        }
    }
    public static void secondTest() {
        System.runAs(u) {
            VT_R5_AllTestsMobile.setRestContext('/mobile/v1/calendar', 'POST');
            VT_R5_MobileRouter.doPOST();
        }
    }
    public static void thirdTest() {
        u = [SELECT Id FROM User WHERE Username =:VT_R5_AllTestsMobile.PT_USERNAME];
        System.runAs(u) {
            Id visitId = [SELECT Id FROM VTD1_Actual_Visit__c LIMIT 1].Id;
            VT_R5_AllTestsMobile.setRestContext('/mobile/v1/visit/'+visitId, 'GET');
            VT_R5_MobileRouter.doGET();
        }
    }
    public static void fourthTest() {
        u = [SELECT Id FROM User WHERE Username =:VT_R5_AllTestsMobile.PT_USERNAME];
        System.runAs(u) {
            Id visitId = [SELECT Id FROM VTD1_Actual_Visit__c LIMIT 1].Id;
            VT_R5_AllTestsMobile.setRestContext('/mobile/v1/visit/'+visitId+'/timeslots', 'GET');
            VT_R5_MobileRouter.doGET();
        }
    }
    public static void fifthTest() {
        u = [SELECT Id FROM User WHERE Username =:VT_R5_AllTestsMobile.PT_USERNAME];
        System.runAs(u) {
            Id visitId = [SELECT Id FROM VTD1_Actual_Visit__c LIMIT 1].Id;
            VT_R5_AllTestsMobile.setRestContext('/mobile/v1/visit/'+visitId+'/participants', 'GET');
            VT_R5_MobileRouter.doGET();
            VT_R5_AllTestsMobile.setRestContext('/patient/v1/timeslots/'+visitId, 'GET');
            VT_R5_MobileRestRouter.doGET();
        }
    }

    public static void sixthTest() {
        u = [SELECT Id FROM User WHERE Username =:VT_R5_AllTestsMobile.PT_USERNAME];
        System.runAs(u) {
            Id visitId = [SELECT Id FROM VTD1_Actual_Visit__c LIMIT 1].Id;
            VT_R5_AllTestsMobile.setRestContext('/mobile/v1/visit/'+visitId+'/cancel', 'POST');
            RestContext.request.requestBody = Blob.valueOf('{}');
            VT_R5_MobileRouter.doPOST();
        }
    }
    public static void seventhTest() {
        u = [SELECT Id FROM User WHERE Username =:VT_R5_AllTestsMobile.PT_USERNAME];
        System.runAs(u) {
            Id visitId = [SELECT Id FROM VTD1_Actual_Visit__c LIMIT 1].Id;
            VT_R5_AllTestsMobile.setRestContext('/mobile/v1/visit/'+visitId+'/reschedule', 'POST');
            RestContext.request.requestBody = Blob.valueOf('{}');
            VT_R5_MobileRouter.doPOST();
        }
    }
}