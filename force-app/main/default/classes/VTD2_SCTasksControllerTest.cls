@IsTest
private class VTD2_SCTasksControllerTest {

    @TestSetup
    static void dataSetup() {
        VTD1_SC_Task__c task = new VTD1_SC_Task__c(OwnerId = UserInfo.getUserId());
        insert task;
    }

    @IsTest
    static void getSCTasksFromPoolUserTest() {
        Test.startTest();
            List<VTD1_SC_Task__c> sCTasks = VTD2_SCTasksController.getSCTasksFromPool();
        Test.stopTest();

        System.assertEquals(0, sCTasks.size(), 'Incorrect task list size');
    }

    @IsTest
    static void claimTasksRemoteTest() {
        VTD1_SC_Task__c sCTask = [SELECT Id FROM VTD1_SC_Task__c LIMIT 1];

        Test.startTest();
            List<VTD1_SC_Task__c> sCTasks = VTD2_SCTasksController.claimTasksRemote(new List<Id>{sCTask.Id});
        Test.stopTest();
    }
}