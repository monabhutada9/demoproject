/**
 * Created by IhorSemenko on 07.04.2020.
 */

@IsTest
private class VT_R4_GeneralVisitTriggerHandlerTest {

    @TestSetup
    static void setup(){
        VTR4_General_Visit__c generalVisit1 = new VTR4_General_Visit__c(Name = 'test1', VTR4_Status__c = 'Scheduled', VTR4_Duration__c = 1, VTR4_Scheduled_Date_Time__c = Datetime.now());
        insert generalVisit1;
        VTR4_General_Visit_Member__c generalVisitMember1 = new VTR4_General_Visit_Member__c(VTR4_Participant_User__c = UserInfo.getUserId(), VTR4_General_Visit__c = generalVisit1.Id
                , VTR4_External_Member_Name__c = 'tN', VTR4_External_Participant_Email__c = 't1@test.com');
        insert generalVisitMember1;

        VTR4_General_Visit__c generalVisit2 = new VTR4_General_Visit__c(Name = 'test2', VTR4_Duration__c = 1, VTR4_Status__c = 'Scheduled', VTR4_Scheduled_Date_Time__c = Datetime.now() + 200);
        insert generalVisit2;
        VTR4_General_Visit_Member__c generalVisitMember2 = new VTR4_General_Visit_Member__c(VTR4_Participant_User__c = UserInfo.getUserId(), VTR4_General_Visit__c = generalVisit2.Id
                , VTR4_External_Member_Name__c = 'tN', VTR4_External_Participant_Email__c = 't2@test.com');
        insert generalVisitMember2;
    }

    @IsTest static void addEventForGeneralVisitSuccess() {
        VTR4_General_Visit__c generalVisit = new VTR4_General_Visit__c(VTR4_Duration__c = 1, VTR4_Scheduled_Date_Time__c = Datetime.now() + 300);
        insert generalVisit;
        VTR4_General_Visit_Member__c generalVisitMember = new VTR4_General_Visit_Member__c(VTR4_Participant_User__c = UserInfo.getUserId(), VTR4_General_Visit__c = generalVisit.Id
                , VTR4_External_Member_Name__c = 'tN', VTR4_External_Participant_Email__c = 't@test.com');
        insert generalVisitMember;
        System.assertEquals(3, [SELECT Id FROM Event].size());
    }

    @IsTest static void deleteAllRelatedEventsToGeneralVisitSuccess() {
        VTR4_General_Visit__c generalVisit = new VTR4_General_Visit__c(VTR4_Duration__c = 1, VTR4_Scheduled_Date_Time__c = Datetime.now() + 400);
        insert generalVisit;
        VTR4_General_Visit_Member__c generalVisitMember = new VTR4_General_Visit_Member__c(VTR4_Participant_User__c = UserInfo.getUserId(), VTR4_General_Visit__c = generalVisit.Id
                , VTR4_External_Member_Name__c = 'tN', VTR4_External_Participant_Email__c = 't@test.com');
        insert generalVisitMember;
        delete generalVisit;
        System.assertEquals(2, [SELECT Id FROM Event].size());
    }

    @IsTest
    static void changeTimeVisitTriggerHandlerTest(){
        VTR4_General_Visit__c generalVisit1 = new VTR4_General_Visit__c(VTR4_Duration__c = 1, VTR4_Scheduled_Date_Time__c = Datetime.now() + 600);
        insert generalVisit1;
        VTR4_General_Visit_Member__c generalVisitMember = new VTR4_General_Visit_Member__c(VTR4_Participant_User__c = UserInfo.getUserId(), VTR4_General_Visit__c = generalVisit1.Id
                , VTR4_External_Member_Name__c = 'tN', VTR4_External_Participant_Email__c = 't@test.com');
        insert generalVisitMember;
        generalVisit1.VTR4_Status__c = 'Canceled';
        System.assertEquals(3, [SELECT Id FROM Event].size());
        update generalVisit1;
        System.assertEquals(2, [SELECT Id FROM Event].size());
        VTR4_General_Visit__c visit = [SELECT Id, VTR4_Scheduled_Date_Time__c, VTR4_Duration__c, VTR4_Status__c FROM VTR4_General_Visit__c WHERE Name = 'test2'];
        visit.VTR4_Scheduled_Date_Time__c = System.now() + 1100;
        visit.VTR4_Duration__c = 1000;
        update visit;
        System.assertEquals(1000, [SELECT VTR4_Duration__c FROM VTR4_General_Visit__c WHERE Name = 'test2'].VTR4_Duration__c);
    }

    @IsTest
    static void changeTimeVisitTriggerHandlerEventUpdateTest(){
        VTR4_General_Visit__c visit1 = [SELECT Id, VTR4_Status__c FROM VTR4_General_Visit__c WHERE Name = 'test1'];
        VTR4_General_Visit_Member__c member = [SELECT Id FROM VTR4_General_Visit_Member__c WHERE VTR4_General_Visit__c != :visit1.Id];
        System.assertEquals(2, [SELECT Id FROM Event].size());
        delete visit1;
        System.assertEquals(1, [SELECT Id FROM Event].size());
        delete member;
        System.assertEquals(0, [SELECT Id FROM Event].size());
    }

    @IsTest static void createAndChangeVideoConferenceForGeneralVisitSuccess() {
        VTR4_General_Visit__c generalVisit = new VTR4_General_Visit__c(VTR4_Duration__c = 1, VTR4_Scheduled_Date_Time__c = Datetime.now() + 300, VTR4_Visit_Method__c = 'Televisit');
        insert generalVisit;
        System.assertEquals(1, [SELECT Id FROM Video_Conference__c WHERE VTR4_General_Visit__c = :generalVisit.Id].size());
        generalVisit.VTR4_Duration__c = 2;
        update generalVisit;
        System.assertEquals('2', [SELECT Duration__c FROM Video_Conference__c WHERE VTR4_General_Visit__c = :generalVisit.Id][0].Duration__c);
    }

    @IsTest static void addVideoConferenceMemberForGeneralVisitMemberSuccess() {
        VTR4_General_Visit__c generalVisit = new VTR4_General_Visit__c(VTR4_Duration__c = 1, VTR4_Scheduled_Date_Time__c = Datetime.now() + 300, VTR4_Visit_Method__c = 'Televisit');
        insert generalVisit;
        System.assertEquals(1, [SELECT Id FROM Video_Conference__c WHERE VTR4_General_Visit__c = :generalVisit.Id].size());
        VTR4_General_Visit_Member__c generalVisitMember = new VTR4_General_Visit_Member__c(VTR4_Participant_User__c = UserInfo.getUserId(), VTR4_General_Visit__c = generalVisit.Id
                , VTR4_External_Member_Name__c = 'tN', VTR4_External_Participant_Email__c = 't2@test.com');
        insert generalVisitMember;
        System.assertEquals(3, [SELECT Id FROM VTD1_Conference_Member__c].size());
        delete generalVisitMember;
        System.assertEquals(2, [SELECT Id FROM VTD1_Conference_Member__c].size());
    }

    @IsTest static void dmlAllowanceValidationSuccess() {
        Profile pr = [SELECT Name FROM Profile WHERE Name = 'Standard User'];
        String scrUserName = 'scr_testName';
        User user = new User(
                ProfileId = pr.Id,
                Email = scrUserName + '@test.com',
                Username = scrUserName + '@test.com' + '_user',
                LastName = 'Test',
                Alias = 'Test',
                CommunityNickname = 'Test',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US'
        );
        insert user;
        System.runAs(user) {
            VTR4_General_Visit__c generalVisit = new VTR4_General_Visit__c(VTR4_Duration__c = 1, VTR4_Scheduled_Date_Time__c = Datetime.now() + 300, VTR4_Visit_Method__c = 'Televisit');
            insert generalVisit;
        }
        System.assertEquals(3, [SELECT COUNT() FROM VTR4_General_Visit__c]);
    }
}