global class VT_D1_scheduler_01 implements Schedulable {
    global void execute(SchedulableContext ctx) {
        //FR–PD–251 (R1,R2) Study Hub will send daily reminder notifications to the Project Lead 
        //until the Notify Sponsor/TMA task has been marked as completed    
        String query =  'SELECT Subject, Status, CreatedDate, WhatId, OwnerId ' +
                        'FROM Task ' +
                        'WHERE Status = \'Open\' ' +
                        'AND WhatId IN (SELECT Id FROM VTD1_Protocol_Deviation__c) '+
                        'AND Owner.Profile.Name = \'Project Lead\'';
        Database.executeBatch(new VT_D1_NotifySponsorTMATaskReset(query));
    }
}