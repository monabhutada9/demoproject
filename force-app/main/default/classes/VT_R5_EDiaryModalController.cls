/**
 * Created by Maksim Fedarenka on 8/12/2020.
 */

public with sharing class VT_R5_EDiaryModalController {
    @AuraEnabled
    public static AnswersWrapper getSurveyAnswers(final VTD1_Survey__c eDiary) {
        List<VTD1_Survey_Answer__c> answers = [
                SELECT Id, VTR2_Question_content__c, VTD1_Answer__c
                FROM VTD1_Survey_Answer__c
                WHERE VTD1_Survey__c = :eDiary.Id
                ORDER BY VTR5_External_Question_Number__c ASC
        ];

        return new AnswersWrapper().setAnswers(answers);
    }

    @AuraEnabled
    public static void updateEDairy(final VTD1_Survey__c eDiary) {
        update eDiary;
    }

    public class AnswersWrapper {
        @AuraEnabled
        public List<VTD1_Survey_Answer__c> answers;

        public AnswersWrapper setAnswers(final List<VTD1_Survey_Answer__c> answers) {
            this.answers = answers;
            return this;
        }
    }
}