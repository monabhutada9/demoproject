/**
* @author: Carl Judge
* @date: 15-Sep-20
**/

@IsTest
private class VT_R5_SiteParticipantSharingHandlerTest {
    @TestSetup
    private static void testSetup() {
        Test.startTest();
        VT_R3_GlobalSharing.disableForTest = false;
        DomainObjects.Study_t study = new DomainObjects.Study_t()
            .addAccount(new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor'));
        DomainObjects.VirtualSite_t site = new DomainObjects.VirtualSite_t()
            .addStudy(study);
        site.persist();
        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t();
        DomainObjects.User_t userPatient = new DomainObjects.User_t()
            .addContact(patientContact)
            .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        userPatient.persist();
        DomainObjects.Case_t casePatient = new DomainObjects.Case_t()
            .setRecordTypeByName('CarePlan')
            .addStudy(study)
            .addUser(userPatient)
            .addVirtualSite(site)
            .addContact(patientContact);
        casePatient.persist();
        VT_R5_PatientConversionSharing.createConversionShares(new Set<Id>{casePatient.id});
        DomainObjects.User_t userPI = new DomainObjects.User_t()
            .addContact(new DomainObjects.Contact_t())
            .setProfile('Primary Investigator');
        DomainObjects.StudyTeamMember_t stmPI = new DomainObjects.StudyTeamMember_t()
            .addStudy(study)
            .addUser(userPI)
            .setRecordTypeByName('PI');
        stmPI.persist();
        VTD1_Document__c document = (VTD1_Document__c) new DomainObjects.VTD1_Document_t()
            .addVirtualSite(site)
            .setRecordType('VTD1_Regulatory_Document')
            .persist();
        DomainObjects.VTD1_Protocol_Deviation_t protocolDeviationT= new DomainObjects.VTD1_Protocol_Deviation_t();
        VTD1_Protocol_Deviation__c pd = (VTD1_Protocol_Deviation__c) protocolDeviationT.persist();
        pd.VTD1_Virtual_Site__c = site.id;
        update pd;
        VT_R3_GlobalSharing.doSharing(pd);
        Test.stopTest();
    }

    @IsTest
    private static void testAddBackupPI() {
        Case cas = [SELECT Id, ContactId, Contact.AccountId, VTD1_Patient_User__c, VTD1_Study__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];
        VTD1_Document__c doc = [SELECT Id FROM VTD1_Document__c ORDER BY CreatedDate DESC LIMIT 1];
        VTD1_Protocol_Deviation__c pd = [SELECT Id FROM VTD1_Protocol_Deviation__c ORDER BY CreatedDate DESC LIMIT 1];
        Virtual_Site__c site = [SELECT Id,VTD1_Study_Team_Member__c, VTR5_EditShareGroupId__c FROM Virtual_Site__c ORDER BY CreatedDate DESC LIMIT 1];
        Study_Team_Member__c backupPISTM = [SELECT Id, User__c FROM Study_Team_Member__c WHERE User__r.Profile.Name = 'Primary Investigator' AND User__r.IsActive = TRUE AND Id != :site.VTD1_Study_Team_Member__c ORDER BY CreatedDate DESC LIMIT 1];

        System.assert(![SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE RecordId = :cas.Id AND UserId = :backupPISTM.User__c].HasEditAccess);
        System.assert(![SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE RecordId = :cas.Contact.AccountId AND UserId = :backupPISTM.User__c].HasEditAccess);
        System.assert(![SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE RecordId = :pd.Id AND UserId = :backupPISTM.User__c].HasEditAccess);
        System.assert([SELECT Id, UserOrGroupId, AccessLevel, RowCause FROM VTD1_Document__Share WHERE ParentId = :doc.Id AND UserOrGroupId = :backupPISTM.User__c].isEmpty());

        VT_R3_GlobalSharing.disableForTest = true;
        insert new VTD1_Study_Sharing_Configuration__c(VTD1_User__c = backupPISTM.User__c, VTD1_Study__c = cas.VTD1_Study__c, VTD1_Access_Level__c = 'Edit');
        VT_R3_GlobalSharing.disableForTest = false;
        // add PI to site
        Test.startTest();
        site.VTR2_Backup_PI_STM__c = backupPISTM.Id;
        update site;
        Test.stopTest();

        System.assert([SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE RecordId = :cas.Id AND UserId = :backupPISTM.User__c].HasEditAccess);
        System.assert([SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE RecordId = :cas.Contact.AccountId AND UserId = :backupPISTM.User__c].HasEditAccess);
        System.assert([SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE RecordId = :pd.Id AND UserId = :backupPISTM.User__c].HasEditAccess);
        System.assert(![SELECT Id, UserOrGroupId, AccessLevel, RowCause FROM VTD1_Document__Share WHERE ParentId = :doc.Id AND UserOrGroupId = :backupPISTM.User__c].isEmpty());
    }

    @IsTest
    private static void testRemoveBackupPI() {
        Case cas = [SELECT Id, ContactId, Contact.AccountId, VTD1_Patient_User__c, VTD1_Study__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];
        Virtual_Site__c site = [SELECT Id,VTD1_Study_Team_Member__c, VTR5_EditShareGroupId__c FROM Virtual_Site__c ORDER BY CreatedDate DESC LIMIT 1];
        Study_Team_Member__c backupPISTM = [SELECT Id, User__c FROM Study_Team_Member__c WHERE User__r.Profile.Name = 'Primary Investigator' AND User__r.IsActive = TRUE AND Id != :site.VTD1_Study_Team_Member__c ORDER BY CreatedDate DESC LIMIT 1];

        VT_R3_GlobalSharing.disableForTest = true;
        insert new VTD1_Study_Sharing_Configuration__c(VTD1_User__c = backupPISTM.User__c, VTD1_Study__c = cas.VTD1_Study__c, VTD1_Access_Level__c = 'Edit');
        VT_R3_GlobalSharing.disableForTest = false;
        // add PI to site
        site.VTR2_Backup_PI_STM__c = backupPISTM.Id;
        update site;

        System.assert([SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE RecordId = :cas.Id AND UserId = :backupPISTM.User__c].HasEditAccess);
        System.assert([SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE RecordId = :cas.Contact.AccountId AND UserId = :backupPISTM.User__c].HasEditAccess);

        Test.startTest();
        // remove PI from site
        site.VTR2_Backup_PI_STM__c = null;
        update site;
        Test.stopTest();

        // now should have no access
        System.assert(![SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE RecordId = :cas.Id AND UserId = :backupPISTM.User__c].HasEditAccess);
        System.assert(![SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE RecordId = :cas.Contact.AccountId AND UserId = :backupPISTM.User__c].HasEditAccess);
    }
}