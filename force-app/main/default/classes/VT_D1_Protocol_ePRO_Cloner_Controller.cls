/**
* @author: Carl Judge
* @date: 27-Aug-18
* @description:
* @test: VT_D1_ePRO_Test.testClone()
**/

public without sharing class VT_D1_Protocol_ePRO_Cloner_Controller {

    @AuraEnabled
    public static String go(Id recId, Id paId) {
        String query = getMainQueryString();
        VTD1_Protocol_ePRO__c currentEpro = Database.query(query);

        // clone protocol eDiary
        VTD1_Protocol_ePRO__c newEpro = currentEpro.clone(false, true, false, false);
        if (paId != null) { // if we have a PA Id, it's a new version of the same protocol
            newEpro.VTD1_Previous_Version__c = currentEpro.Id;
            newEpro.VTR4_RootProtocolId__c = currentEpro.VTR4_RootProtocolId__c != null ? currentEpro.VTR4_RootProtocolId__c : currentEpro.Id;
            newEpro.VTD1_Protocol_Amendment__c = paId;
        } else {
            newEpro.VTD1_Previous_Version__c = null;
            newEpro.VTR4_RootProtocolId__c = null;
            newEpro.VTD1_Protocol_Amendment__c = null;
            newEpro.VTD1_Version__c = 1;
        }
        insert newEpro;

        // clone scales
        Map<Id, VTR2_Protocol_eDiary_Scale__c> newScaleByOldId = new Map<Id, VTR2_Protocol_eDiary_Scale__c>();
        for (VTR2_Protocol_eDiary_Scale__c currentScale : currentEpro.Protocol_eDiary_Scales__r) {
            VTR2_Protocol_eDiary_Scale__c newScale = currentScale.clone(false, true, false, false);
            newScale.VTR2_Protocol_eDiary__c = newEpro.Id;

            newScaleByOldId.put(currentScale.Id,newScale);
        }
        insert newScaleByOldId.values();

        // clone ScaleResponses (VTR2_ProtocoleDiaryScaleResponse__c)
        String scaleResponseQueryString = getScaleResponseQueryString();//
        List<Id> scaleIds = new List<Id>(newScaleByOldId.keySet());//for deploy by US
        List<VTR2_ProtocoleDiaryScaleResponse__c> newScaleResponses = new List<VTR2_ProtocoleDiaryScaleResponse__c>();//
        for (VTR2_ProtocoleDiaryScaleResponse__c currentScaleResponse : Database.query(scaleResponseQueryString)) {//
            VTR2_ProtocoleDiaryScaleResponse__c newScaleResponse = currentScaleResponse.clone(false, true, false, false);
            newScaleResponse.VTR2_Protocol_eDiary_Scale__c = newScaleByOldId.get(currentScaleResponse.VTR2_Protocol_eDiary_Scale__c).Id;
            newScaleResponses.add(newScaleResponse);
        }
        if (! newScaleResponses.isEmpty()) { insert newScaleResponses; }

        // clone questions
        Map<Id, VTD1_Protocol_ePro_Question__c> newQuestionsByOldId = new Map<Id, VTD1_Protocol_ePro_Question__c>();
        for (VTD1_Protocol_ePro_Question__c currentQuestion : currentEpro.Protocol_ePro_Questions__r) {
            VTD1_Protocol_ePro_Question__c newQuestion = currentQuestion.clone(false, true, false, false);
            newQuestion.VTD1_Protocol_ePRO__c = newEpro.Id;
            newQuestion.VTR4_Branch_Parent__c = null;
            newQuestion.VTR4_Response_Trigger__c = null;
            newQuestion.VTR4_Contains_Branching_Logic__c = false;
            if(currentQuestion.VTR2_Protocol_eDiary_Scale__c != null){
                newQuestion.VTR2_Protocol_eDiary_Scale__c = newScaleByOldId.get(currentQuestion.VTR2_Protocol_eDiary_Scale__c).Id;
            }
            newQuestionsByOldId.put(currentQuestion.Id,newQuestion);
        }
        insert newQuestionsByOldId.values();

        // link new questions to parents
        Map<Id, VTD1_Protocol_ePro_Question__c> oldQuestionsById = new Map<Id, VTD1_Protocol_ePro_Question__c>(currentEpro.Protocol_ePro_Questions__r);
        List<VTD1_Protocol_ePro_Question__c> newQuestionsToUpdate = new List<VTD1_Protocol_ePro_Question__c>();
        for (Id oldQuestionId : newQuestionsByOldId.keySet()) {
            VTD1_Protocol_ePro_Question__c oldQ = oldQuestionsById.get(oldQuestionId);
            VTD1_Protocol_ePro_Question__c newQ = newQuestionsByOldId.get(oldQuestionId);

            if (oldQ.VTR4_Branch_Parent__c != null && newQuestionsByOldId.containsKey(oldQ.VTR4_Branch_Parent__c)) {
                newQ.VTR4_Branch_Parent__c = newQuestionsByOldId.get(oldQ.VTR4_Branch_Parent__c).Id;
                newQ.VTR4_Response_Trigger__c = oldQ.VTR4_Response_Trigger__c;
                newQuestionsToUpdate.add(newQ);
            }
        }
        if (!newQuestionsToUpdate.isEmpty()) { update newQuestionsToUpdate; }

        // clone groups
        Map<Id, VTR2_Protocol_eDiary_Group__c> newGroupsByOldId = new Map<Id, VTR2_Protocol_eDiary_Group__c>();
        for (VTR2_Protocol_eDiary_Group__c currentGroup : currentEpro.Protocol_eDiary_Groups1__r) {
            VTR2_Protocol_eDiary_Group__c newGroup = currentGroup.clone(false, true, false, false);
            newGroup.VTR2_Protocol_eDiary__c = newEpro.Id;
            newGroupsByOldId.put(currentGroup.Id,newGroup);
        }
        insert newGroupsByOldId.values();

        // clone question groups
        String groupQuestionQueryString = getGroupQuestionQueryString();
        List<Id> questionIds = new List<Id>(newQuestionsByOldId.keySet()); //for deploy by US
        List<Id> groupIds = new List<Id>(newGroupsByOldId.keySet()); // for deploy by US
        List<VTR2_Protocol_eDiary_Group_Question__c> newGroupQuestions = new List<VTR2_Protocol_eDiary_Group_Question__c>();
        for (VTR2_Protocol_eDiary_Group_Question__c currentGroupQuestion : Database.query(groupQuestionQueryString)) {
            VTR2_Protocol_eDiary_Group_Question__c newGroupQuestion = currentGroupQuestion.clone(false, true, false, false);
            newGroupQuestion.VTR2_Protocol_eDiary_Question__c = newQuestionsByOldId.get(currentGroupQuestion.VTR2_Protocol_eDiary_Question__c).Id;
            newGroupQuestion.VTR2_Protocol_eDiary_Group__c = newGroupsByOldId.get(currentGroupQuestion.VTR2_Protocol_eDiary_Group__c).Id;
            newGroupQuestions.add(newGroupQuestion);
        }
        if (! newGroupQuestions.isEmpty()) { insert newGroupQuestions; }

        //clone VTR2_eDiaryInstructions__c
        List<VTR2_eDiaryInstructions__c> newInstructionList = new List<VTR2_eDiaryInstructions__c>();
        for (VTR2_eDiaryInstructions__c currentInstructions : currentEpro.Protocol_eDiary_Instructions__r) {
            VTR2_eDiaryInstructions__c newInstructions = currentInstructions.clone(false, true, false, false);
            newInstructions.VTR2_Protocol_eDiary__c = newEpro.Id;
            newInstructionList.add(newInstructions);
        }
        insert newInstructionList;

        return newEpro.Id;
    }

    public static String getMainQueryString() {
        return String.format(
            'SELECT {0},(SELECT {1} FROM Protocol_ePro_Questions__r), (SELECT {2} FROM Protocol_eDiary_Groups1__r), (SELECT {3} FROM Protocol_eDiary_Scales__r), (SELECT {4} FROM Protocol_eDiary_Instructions__r)' +
                ' FROM VTD1_Protocol_ePRO__c WHERE Id = :recId',
            new List<String>{
                String.join(getAllFields('VTD1_Protocol_ePRO__c'),','),
                String.join(getAllFields('VTD1_Protocol_ePro_Question__c'),','),
                String.join(getAllFields('VTR2_Protocol_eDiary_Group__c'),','),
                String.join(getAllFields('VTR2_Protocol_eDiary_Scale__c'),','),
                String.join(getAllFields('VTR2_eDiaryInstructions__c'),',')
            }
        );
    }

    public static String getGroupQuestionQueryString() {
        return String.format(
            'SELECT {0} FROM VTR2_Protocol_eDiary_Group_Question__c' +
                ' WHERE VTR2_Protocol_eDiary_Question__c IN :questionIds' +
                ' AND VTR2_Protocol_eDiary_Group__c IN :groupIds',
            new List<String>{
                String.join(getAllFields('VTR2_Protocol_eDiary_Group_Question__c'),',')
            }
        );
    }

    public static String getScaleResponseQueryString() {
        return String.format(
            'SELECT {0} FROM VTR2_ProtocoleDiaryScaleResponse__c' +//
                ' WHERE VTR2_Protocol_eDiary_Scale__c IN :scaleIds',//
            new List<String>{
                String.join(getAllFields('VTR2_ProtocoleDiaryScaleResponse__c'),',')
            }
        );
    }

    public static List<String> getAllFields(String SObjectType) {
        List<String> fields = new List<String>();
        for (Schema.SObjectField field : Schema.getGlobalDescribe().get(SObjectType).getDescribe().fields.getMap().values()) {
            fields.add(field.getDescribe().getName());
        }
        return fields;
    }
}