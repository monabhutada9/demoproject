/**
 * Created by User on 19/05/16.
 */
@isTest
public with sharing class VT_D1_ResetVideoConfSignalTest {

    public static void doTest(){
        User user = (User) new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5))
                .persist();
        Test.startTest();
        VT_D1_ResetVideoConfSignal.resetSignalToNotConnectedParticipants(new List<Id>{user.Id});
        Test.stopTest();
    }
}