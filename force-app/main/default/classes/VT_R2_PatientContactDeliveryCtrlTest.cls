@IsTest
public with sharing class VT_R2_PatientContactDeliveryCtrlTest {

    public static void testGetPatientContactUser() {
        User user = (User) new DomainObjects.User_t().persist();

        Test.startTest();
        User actualUser;
        System.runAs(user) {
            actualUser = VT_D1_PatientContactDeliveryController.getPatientContactUser();
        }
        Test.stopTest();

        System.assertEquals(user.Id, actualUser.Id);
    }
}