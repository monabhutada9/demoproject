public with sharing class VT_FilesUploaderController {

    @AuraEnabled
    public static void deleteFiles(List<String> fileIds) {
        List<ContentDocument> contentDocuments = [SELECT Id FROM ContentDocument WHERE Id IN :fileIds];
        delete contentDocuments;
    }
}