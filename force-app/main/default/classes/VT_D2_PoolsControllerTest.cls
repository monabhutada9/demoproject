/**
 * Created by shume on 21.01.2019.
 */

@IsTest
private class VT_D2_PoolsControllerTest {
    /*

    VT_D2_SCTasksController
    VT_D2_TMATasksController
    VT_D1_ChangeTaskOwnerController
    VTR5_SCTaskPoolController
    */

    @testSetup
    private static void setupMethod() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        //User patient = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Patient' AND ContactId != null AND IsActive = true LIMIT 1];
        //User PI = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Primary Investigator' AND ContactId != null AND IsActive = true LIMIT 1];
        //System.debug('patient:'+patient);
        //System.debug('PI:'+PI);
        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();

        User tmaUser = VT_D1_TestUtils.createUserByProfile(VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME, 'tmaUser');
        VT_D1_TestUtils.persistUsers();
        Test.stopTest();
        List<Group> nGroup = [SELECT Id FROM Group WHERE DeveloperName = 'VTD2_TMA_Pool'];
        // GroupMember gm = new GroupMember(GroupId = nGroup.Id,  UserOrGroupId = tmaUser.Id);
        //  insert gm;


        VTR5_Pool_task__c task = new VTR5_Pool_task__c(VTR5_Subject__c = 'asd');
        insert task;
        VTD1_SC_Task__c sc_task = new VTD1_SC_Task__c(OwnerId = UserInfo.getUserId());
        insert sc_task;
        VTD2_TMA_Task__c tma_task = new VTD2_TMA_Task__c(OwnerId = nGroup[0].Id);
        insert tma_task;

        new DomainObjects.User_t()
                .setActive(true)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME)
                .persist();

    }

    @IsTest
    static void testBehavior() {

        String jsonParams = '{"limit": "1000","offset": "0","sObjectName" : "VTD1_SC_Task__c"}';
        VT_D2_SCTasksController.getSCTasksFromPool(null, jsonParams);
        VT_D2_TMATasksController.getTMATasksFromPool();

        VTD1_SC_Task__c sc_task = [SELECT Id FROM VTD1_SC_Task__c LIMIT 1];
        VT_D1_ChangeTaskOwnerController.changeOwnerMethod(sc_task.Id);

        VTD2_TMA_Task__c tma_task = [SELECT Id FROM VTD2_TMA_Task__c LIMIT 1];
        User tmaUser = [SELECT Id FROM User WHERE IsActive = TRUE AND Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME LIMIT 1];
        System.runAs(tmaUser) {
            VT_D1_ChangeTaskOwnerController.changeTMAOwner(tma_task.Id);
        }
        List<Id> scTaskIds = new List<Id>();
        for (VTD1_SC_Task__c t : [SELECT Id FROM VTD1_SC_Task__c]) {
            scTaskIds.add(t.Id);
        }
        List<Id> tmaTaskIds = new List<Id>();
        for (VTD2_TMA_Task__c t : [SELECT Id, OwnerId FROM VTD2_TMA_Task__c]) {
            tmaTaskIds.add(t.Id);
        }
        VT_D2_SCTasksController.claimTasksRemote(scTaskIds);
        VT_D2_TMATasksController.claimTasksRemote(tmaTaskIds);
    }
    @IsTest
    static void testForVTR5_PoolTaskController() {
        List<User> uIds = [SELECT Id FROM User WHERE profile.name = :VT_R4_ConstantsHelper_ProfilesSTM.MRR_PROFILE_NAME AND IsActive = True];
        List<String> ids = new List<String>();
        for (User u : uIds) {
            ids.add(u.id);
        }
        GroupMember gm = [SELECT Group.Type, UserOrGroupId FROM GroupMember WHERE UserOrGroupId IN :ids AND Group.Type = 'Queue' LIMIT 1];
        User uuUsr = [SELECT userName FROM User WHERE id = :gm.userOrGroupId LIMIT 1];
        List<Id> task = new List<Id>{
                [SELECT Id FROM VTR5_Pool_task__c WHERE VTR5_Subject__c = 'asd' LIMIT 1].Id
        };
        System.runAs(uuUsr) {
            VT_R5_TaskPoolController.getcmpHeader();
            VT_R5_TaskPoolController.getTaskPool(null);
            VT_R5_TaskPoolController.claimTasksRemote(task);
        }
    }
}