/**
 * Created by user on 24.09.2018.
 */

public with sharing class VT_D1_TestDataPostConversion implements /*Queueable*/ Schedulable {
    String firstName;
    String lastName;
    Integer attempt = 0;
    String epicName;
    private final long startTime;
    public VT_D1_TestDataPostConversion(String firstName, String lastName, Integer attempt, String epicName, long startTime) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.attempt = attempt;
        this.epicName = epicName;
        this.startTime = startTime;
    }
    /*public void run(long startTimeLocal) {
        long current = System.now().getTime();
        System.debug('execute VT_D1_TestDataPostConversion ' + current);
        if (current - startTime < 30000) {
            VT_D1_TestDataPostConversion testDataPostConversion = new VT_D1_TestDataPostConversion(firstName, lastName, attempt, epicName, startTime);
            System.enqueueJob(testDataPostConversion);
        } else {
            System.debug('VT_D1_TestDataPostConversion exceed time limit duration');
        }
    }*/
    public void run() {
        Datetime dt = System.now();
        Integer seconds = dt.second();
        Integer minutes = dt.minute();
        Integer hours = dt.hour();

        seconds += 30;
        if (seconds >= 60) {
            seconds -= 30;
            minutes += 1;
        }

        if (minutes >= 60) {
            minutes -= 60;
            hours += 1;
        }

        VT_D1_TestDataPostConversion testDataPostConversion = new VT_D1_TestDataPostConversion(firstName, lastName, attempt, epicName, startTime);
        System.schedule('CG_' + firstName, seconds + ' ' + minutes + ' ' + hours + ' * * ?', testDataPostConversion);
    }
    /*public void execute(QueueableContext context) {
        System.debug('execute ' + firstName + ' ' + lastName);
        String userName = firstName + ' ' + lastName;
        System.debug('username = ' + userName);
        String cgUserName = 'CG_' + firstName;
        List <Account> accounts = [select Id from Account where Name = :userName];
        if (accounts.isEmpty()) {
            run(startTime);
        } else if (!accounts.isEmpty()) {
            Id accountId = accounts[0].Id;
            Case caseObj = [select Id, VTD1_Study__c, ContactEmail, VTD1_Eligibility_Status__c, VTD2_Baseline_Visit_Complete__c, VTD1_End_of_study_visit_completed__c, VTD1_Drug_Reconciliation_Process__c from Case where AccountId = : accountId and RecordType.Name = 'CarePlan'];
            if (epicName == 'Virtual Study Closeout' || epicName == 'PreScreening') {
                caseObj.VTD1_Eligibility_Status__c = 'Eligible';
                caseObj.VTD2_Baseline_Visit_Complete__c = true;
                caseObj.VTD1_End_of_study_visit_completed__c = true;
                caseObj.VTD1_Drug_Reconciliation_Process__c = 'Complete';
                caseObj.VTD1_Screening_Complete__c = true;
                update caseObj;
            }
            Id rtId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId();
            List <Contact> contacts = [select Id from Contact where AccountId = : accountId and RecordTypeId = : rtId];
            if (contacts.isEmpty()) {
                System.debug('creating contact for account = ' + accountId);
                String patientEmail = '' + caseObj.ContactEmail;
                String cgEmail = patientEmail.substring(0, patientEmail.length() - 10) + '0' + patientEmail.substring(patientEmail.length() - 10);
                Contact contact = new Contact(FirstName = cgUserName, VTD1_EmailNotificationsFromCourier__c = true,
                        LastName = 'L', Email = cgEmail, RecordTypeId = rtId, AccountId = accountId, VTD1_Clinical_Study_Membership__c = caseObj.Id, VTD1_Primary_CG__c = true);
                insert contact;
                rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Patient Contact Form').getRecordTypeId();
                Case CasePCF = new Case(RecordTypeId = rtId, ParentId = caseObj.Id, AccountId = accountId, VTD1_Clinical_Study_Membership__c = caseObj.Id);
                insert CasePCF;
                createCareGiverUser(contact.Id, firstName, cgEmail);
                run(startTime);
            } else {
                List <User> cgUsers = [select Id from User where FirstName = : cgUserName and isActive = true];
                if (!cgUsers.isEmpty()) {
                    VTD1_Study_Sharing_Configuration__c configuration = new VTD1_Study_Sharing_Configuration__c();
                    configuration.VTD1_Study__c = caseObj.VTD1_Study__c;
                    configuration.VTD1_User__c = cgUsers[0].Id;
                    configuration.VTD1_Access_Level__c = 'Edit';
                    insert configuration;
                    System.debug('grant access caregiver to study = ' + accountId);
                } else {
                    run(startTime);
                }
            }
        }
    }*/
    public void execute(SchedulableContext sc) {
        System.debug('execute ' + firstName + ' ' + lastName);
        String jobName = 'CG_' + firstName;
        List<CronTrigger> jobsToAbort = [select Id from CronTrigger where CronJobDetail.Name = :jobName];
        System.debug('to abort = ' + jobsToAbort);
        for (CronTrigger ct : jobsToAbort) {
            System.abortJob(ct.id);
        }
        String userName = firstName + ' ' + lastName;
        System.debug('username = ' + userName);
        String cgUserName = 'CG_' + firstName;
        List <Account> accounts = [select Id from Account where Name = :userName];
        if (accounts.isEmpty() && attempt < 5) {
            System.debug('no account yet ' + attempt);
            attempt++;
            run();
        } else if (!accounts.isEmpty()) {
            Id accountId = accounts[0].Id;
            Case caseObj = [select Id, VTD1_Study__c, ContactEmail, VTD1_Eligibility_Status__c, VTD2_Baseline_Visit_Complete__c, VTD1_End_of_study_visit_completed__c, VTD1_Drug_Reconciliation_Process__c from Case where AccountId = : accountId and RecordType.Name = 'CarePlan'];
            if (epicName == 'Virtual Study Closeout' || epicName == 'PreScreening') {
                caseObj.VTD1_Eligibility_Status__c = 'Eligible';
                caseObj.VTD2_Baseline_Visit_Complete__c = true;
                caseObj.VTD1_End_of_study_visit_completed__c = true;
                caseObj.VTD1_Drug_Reconciliation_Process__c = 'Complete';
                caseObj.VTD1_Screening_Complete__c = true;
                update caseObj;
            }
            Id rtId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId();
            List <Contact> contacts = [select Id from Contact where AccountId = : accountId and RecordTypeId = : rtId];
            if (contacts.isEmpty()) {
                System.debug('creating contact for account = ' + accountId);
                String patientEmail = '' + caseObj.ContactEmail;
                String cgEmail = patientEmail.substring(0, patientEmail.length() - 10) + '0' + patientEmail.substring(patientEmail.length() - 10);
                Contact contact = new Contact(FirstName = cgUserName, VTD1_EmailNotificationsFromCourier__c = true,
                        LastName = 'L', Email = cgEmail, RecordTypeId = rtId, AccountId = accountId, VTD1_Clinical_Study_Membership__c = caseObj.Id, VTD1_Primary_CG__c = true);
                insert contact;
                //rtId = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF;
                //Case CasePCF = new Case(RecordTypeId = rtId, ParentId = caseObj.Id, AccountId = accountId, VTD1_Clinical_Study_Membership__c = caseObj.Id);
                //insert CasePCF;
                createCareGiverUser(contact.Id, firstName, cgEmail);
                run();
            } else {
                List <User> cgUsers = [select Id from User where FirstName = : cgUserName and isActive = true];
                if (!cgUsers.isEmpty()) {
                    VTD1_Study_Sharing_Configuration__c configuration = new VTD1_Study_Sharing_Configuration__c();
                    configuration.VTD1_Study__c = caseObj.VTD1_Study__c;
                    configuration.VTD1_User__c = cgUsers[0].Id;
                    configuration.VTD1_Access_Level__c = 'Edit';
                    insert configuration;
                    System.debug('grant access caregiver to study = ' + accountId);
                } else {
                    run();
                }
            }
        }
    }
    @future
    public static void createCareGiverUser(Id contactId, String firstName, String cgEmail) {
        Profile profile = [select Id from Profile where Name = 'Caregiver'];
        String cgUserName = 'CG_' + firstName;
        User user = new User(
                ProfileId = profile.Id,
                FirstName = cgUserName,
                LastName = 'L',
                Email = cgEmail,
                Username = cgUserName + '@test.com' + '_user',
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/New_York',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                IsActive = true,
                ContactId = contactId
        );
        insert user;
        System.debug('user = ' + user);
    }

    @future
    public static void completeSomeVisits(Id caseId) {
        List <VTD1_Actual_Visit__c> visits = [select Id, VTD1_Status__c from VTD1_Actual_Visit__c where VTD1_Case__c = : caseId];
        System.debug('visits = ' + visits);
        for (VTD1_Actual_Visit__c visit : visits) {
            visit.VTD1_Status__c = 'Completed';
        }
        update visits;
    }
}