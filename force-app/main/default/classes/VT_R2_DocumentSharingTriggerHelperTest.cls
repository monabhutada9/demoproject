/**
 * Created by User on 19/06/03.
 */
@isTest
private with sharing class VT_R2_DocumentSharingTriggerHelperTest {
    @TestSetup
    public static void setup(){
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setName('Study Name')
               /* .addPMA(new DomainObjects.User_t()
                        .addContact(new DomainObjects.Contact_t())
                        .setProfile('Site Coordinator'))*/
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));
        HealthCloudGA__CarePlanTemplate__c studyObj = (HealthCloudGA__CarePlanTemplate__c) study.persist();
        new DomainObjects.VTD1_Document_t().persist();
        new DomainObjects.VTD1_Document_t().persist();
        VTD1_Regulatory_Document__c documentPAsignature = new VTD1_Regulatory_Document__c(
                VTD1_Document_Type__c = 'PA Signature Page',
                Name = 'Test Reg Doc',
                Level__c = 'Site',
                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI'
        );
        insert documentPAsignature;
        VTD1_Regulatory_Document__c documentPA = new VTD1_Regulatory_Document__c(
                VTD1_Document_Type__c = 'Protocol Amendment',
                Name = 'Test Reg Doc',
                Level__c = 'Site',
                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI'
        );
        insert documentPA;
        VTD1_Regulatory_Binder__c binderPAsignature = new VTD1_Regulatory_Binder__c(
                VTD1_Care_Plan_Template__c = studyObj.Id,
                VTD1_Regulatory_Document__c = documentPAsignature.Id
        );
        insert binderPAsignature;
        VTD1_Regulatory_Binder__c binderPA = new VTD1_Regulatory_Binder__c(
                VTD1_Care_Plan_Template__c = studyObj.Id,
                VTD1_Regulatory_Document__c = documentPA.Id
        );
        insert binderPA;
    }
    @isTest
    private static void processStudiesTest(){
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setName('StudyName')
                /*.addPMA(new DomainObjects.User_t()
                        .addContact(new DomainObjects.Contact_t())
                                            .setProfile('Site Coordinator'))*/
                .addAccount(new DomainObjects.Account_t()
                                    .setRecordTypeByName('Sponsor'))
                .setVTD2_TMA_Blinded_for_Study(true);
        study.persist();
        HealthCloudGA__CarePlanTemplate__c studyObj = (HealthCloudGA__CarePlanTemplate__c)study.toObject();
        User newUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Site Coordinator').persist();
        studyObj.VTD2_TMA_Blinded_for_Study__c = false;
        List<HealthCloudGA__CarePlanTemplate__c> studyList = [
                SELECT Id
                        ,VTD1_Project_Lead__c
                        ,VTD1_Virtual_Trial_Study_Lead__c
                        ,VTD1_Regulatory_Specialist__c
                        ,VTD1_Remote_CRA__c
                        ,VTD1_Virtual_Trial_head_of_Operations__c
                        ,VTD2_TMA_Blinded_for_Study__c
                        ,VTD1_PMA__c
                        ,VTD1_Monitoring_Report_Reviewer__c
                FROM HealthCloudGA__CarePlanTemplate__c
                WHERE Id = :studyObj.Id
        ];
       Test.startTest();
        VT_R2_DocumentSharingTriggerHelper.processStudies(new List<HealthCloudGA__CarePlanTemplate__c>{studyObj},
                                                            new Map<Id,HealthCloudGA__CarePlanTemplate__c>{studyList[0].Id => studyList[0]});
        Test.stopTest();
    }
    @isTest
    private static void processCasesTest(){
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setName('StudyName')
               /* .addPMA(new DomainObjects.User_t()
                        .addContact(new DomainObjects.Contact_t())
                                            .setProfile('Site Coordinator'))*/
                .addAccount(new DomainObjects.Account_t()
                                    .setRecordTypeByName('Sponsor'))
                .setVTD2_TMA_Blinded_for_Study(true);
        study.persist();
        DomainObjects.User_t user =  new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Site Coordinator');
        DomainObjects.Case_t cas = new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .addStudy(study)
                .addUser(user);
        cas.persist();
        Case newCase = (Case) cas.toObject();
        DomainObjects.User_t patientUser =  new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Patient');
        patientUser.persist();
        newCase.VTD1_Patient_User__c = patientUser.Id;
        List<Case> caseList = [
                SELECT Id
                        ,VTD1_Patient_User__c
                        ,VTD1_PI_user__c
                        ,VTD1_Backup_PI_User__c
                        ,VTD1_Primary_PG__c
                        ,VTD1_Secondary_PG__c
                FROM Case
                WHERE Id = :cas.Id
        ];
       Test.startTest();
        VT_R2_DocumentSharingTriggerHelper.processCases(new List<Case>{newCase},
                                                            new Map<Id,Case>{caseList[0].Id => caseList[0]});
        Test.stopTest();
    }
    @isTest
    private static void processDocumentsTest(){
        DomainObjects.VTD1_Document_t docT = new DomainObjects.VTD1_Document_t();
        docT.persist();
        VTD1_Document__c doc = (VTD1_Document__c) docT.toObject();
        doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_ARCHIVED;
        List<VTD1_Document__c> docList = [
                SELECT Id
                        ,VTD1_Clinical_Study_Membership__c
                        ,RecordTypeId
                        ,VTD1_Status__c
                        ,VTD2_Redacted__c
                        ,VTD1_Study__c
                FROM VTD1_Document__c
                WHERE Id = :docT.Id
        ];
        Test.startTest();
        docList[0].RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_ARCHIVED;
        VT_R2_DocumentSharingTriggerHelper.processDocuments(new List<VTD1_Document__c>{doc},
                                                            new Map<Id,VTD1_Document__c>{docList[0].Id => docList[0]});
        docList[0].RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON;
        VT_R2_DocumentSharingTriggerHelper.processDocuments(new List<VTD1_Document__c>{doc},
                                                            new Map<Id,VTD1_Document__c>{docList[0].Id => docList[0]});
        Test.stopTest();
    }
    @isTest
    private static void processVirtualSitesTest(){
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setName('Study Name');
        DomainObjects.StudyTeamMember_t stmOnsiteCra = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA')
                )
                .addStudy(study);
        stmOnsiteCra.persist();
        DomainObjects.StudyTeamMember_t stmRemoteCra = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA'))
                .addStudy(study);
        Test.startTest();
        stmRemoteCra.persist();
        DomainObjects.StudyTeamMember_t stm = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('SCR')
                .addUser(new DomainObjects.User_t()
                        .addContact(new DomainObjects.Contact_t())
                        .setProfile('Site Coordinator'))
            //RAJESH :SH-17440 removed the CRA fields mapping from the virtual site.
               // .setOnsiteCraId(stmOnsiteCra.id)  
               //.setRemoteCraId(stmRemoteCra.id)
                //END:Sh-17440
                .addStudy(study);
        Study_Team_Member__c stmNew = (Study_Team_Member__c) new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('SCR')
                .addUser(new DomainObjects.User_t()
                        .addContact(new DomainObjects.Contact_t())
                        .setProfile('Site Coordinator'))
            //RAJESH :SH-17440 removed the CRA fields mapping from the virtual site.
                //.setOnsiteCraId(stmOnsiteCra.id)  
                //.setRemoteCraId(stmRemoteCra.id)
                //END:Sh-17440
                .addStudy(study)
                .persist();
        Virtual_Site__c virtSite = (Virtual_Site__c) new DomainObjects.VirtualSite_t()
                 .addStudyTeamMember(stm)
                 .addStudy(study)
                .setStudySiteNumber('12345')
                 .persist();
        //
        List<Virtual_Site__c> virtSiteList = [
                SELECT Id ,VTD1_Study_Team_Member__c, VTD1_Study__c, VTR2_Backup_PI_STM__c,
            //START RAJESH :SH-17440 removed the CRA fields mapping from the virtual site.
            /* VTD1_Remote_CRA__c, VTR2_Onsite_CRA__c,*/ 
            //END:SH-17440
            VTR2_Primary_SCR__c
                FROM Virtual_Site__c
                WHERE Id = :virtSite.Id
        ];
        virtSiteList[0].VTD1_Study_Team_Member__c = stmNew.Id;
        VT_R2_DocumentSharingTriggerHelper.processVirtualSites(new List<Virtual_Site__c>{virtSite},
                                                            new Map<Id,Virtual_Site__c>{virtSiteList[0].Id => virtSiteList[0]});
        VT_R2_DocumentSharingTriggerHelper.processStudyTeamMembers(new List<Study_Team_Member__c>{stmNew});
        Test.stopTest();
    }
    @isTest
    private static void processProtocolAmendmentsOnDeleteTest(){
        HealthCloudGA__CarePlanTemplate__c studyObj = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        VTD1_Document__c doc = [SELECT Id FROM VTD1_Document__c LIMIT 1];
        VTD1_Protocol_Amendment__c amendment = new VTD1_Protocol_Amendment__c(VTD1_PA_Document_is_Signed__c = false,VTD1_Study_ID__c = studyObj.Id, VTD1_Protocol_Amendment_Document_Link__c = doc.Id);
        insert amendment;
        Test.startTest();
        VT_R2_DocumentSharingTriggerHelper.processProtocolAmendmentsOnDelete(new List<VTD1_Protocol_Amendment__c>{amendment});
        Test.stopTest();
    }
    @isTest
    private static void processProtocolAmendmentsOnUpdateTest(){
        HealthCloudGA__CarePlanTemplate__c studyObj = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        List<VTD1_Document__c> doc = [SELECT Id FROM VTD1_Document__c LIMIT 2];
        VTD1_Protocol_Amendment__c amendment = new VTD1_Protocol_Amendment__c(VTD1_PA_Document_is_Signed__c = false,VTD1_Study_ID__c = studyObj.Id, VTD1_Protocol_Amendment_Document_Link__c = doc[0].Id);
        insert amendment;
        List<VTD1_Protocol_Amendment__c> virtSiteList = [
                SELECT Id
                        ,VTD1_Study_ID__c
                        ,VTD1_Protocol_Amendment_Document_Link__c
                FROM VTD1_Protocol_Amendment__c
                WHERE Id = :amendment.Id
        ];
        amendment.VTD1_Protocol_Amendment_Document_Link__c = doc[1].Id;
        Test.startTest();
        VT_R2_DocumentSharingTriggerHelper.processProtocolAmendmentsOnUpdate(new List<VTD1_Protocol_Amendment__c>{amendment}
                , new Map<Id,VTD1_Protocol_Amendment__c>{virtSiteList[0].Id => virtSiteList[0]});
        Test.stopTest();
    }
}