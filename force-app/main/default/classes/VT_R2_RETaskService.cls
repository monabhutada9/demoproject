global class VT_R2_RETaskService extends Handler implements Database.Batchable<SObject> {
    // Task notifications batch class for SHA-1062
    // Notifies RE about users not logged in agreed time
    private final RecruitmentTaskCreationService creationService;
    public static final String TN_CATALOG_CODE_FOR_TASK = 'T550';
    public static final Id ID_CARE_PLAN_CASE = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN;
    private static final Id ID_RE_TASK = Task.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('VTR2_RE_Task').getRecordTypeId();
    private static final Id ID_RE_STM = Study_Team_Member__c.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('RE').getRecordTypeId();

    public VT_R2_RETaskService() {
        super();
        this.creationService = new RecruitmentTaskCreationService();
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return new QueryBuilder(Case.class)
                .addField(Case.VTD1_StudyMaximumDaysWithoutLoggingIn__c)
                .addField(Case.VTD_1_Not_Logged_In_For__c)


                .addConditions()
                .add(new QueryBuilder.CompareCondition(Case.RecordTypeId).eq(ID_CARE_PLAN_CASE))
                .add(new QueryBuilder.CompareCondition(Case.VTD_1_Not_Logged_In_For__c).gt(0))
                .add(new QueryBuilder.CompareCondition(Case.VTD1_StudyMaximumDaysWithoutLoggingIn__c).gt(0))
                .add(new QueryBuilder.NullCondition(Case.VTD1_Patient_User__c).notNull())
                .add(new QueryBuilder.NullCondition(Case.VTD1_Study__c).notNull())
                .add(new QueryBuilder.CompareCondition('Case.VTD1_Study__r.VTR2_Caregiver__c').eq(false))
                .setConditionOrder('1 AND 2 AND 3 AND 4 AND 5 AND 6')
                .endConditions()
                .toQueryLocator();

    }

    global void execute(Database.BatchableContext bc, List<Case> scope) {
        this.creationService.createTasks(scope);
    }

    global void finish(Database.BatchableContext bc) {

    }


    protected override void onAfterUpdate(Handler.TriggerContext context) {
        RecruitmentTaskClosingService service = new RecruitmentTaskClosingService();
        List<User> users = service.getFirstLoginUsers(context);
        service.closeTasks(users);
    }


    public class RecruitmentTaskCreationService {

        public RecruitmentTaskCreationService() {
        }

        public void createTasks(List<Case> cases) {
            Set<Id> casesToProcessIds = this.getCasesToProcess(cases);
            if (casesToProcessIds.isEmpty()) {
                return;
            }
            List<HealthCloudGA__CarePlanTemplate__c> studies = this.getStudies(casesToProcessIds);
            Map<Id, Set<Id>> caseUsersWithReTaskMap = this.getOpenReTasksByCaseId(casesToProcessIds);

            List<Database.SaveResult> sr = Database.insert(this.generateTask(studies, caseUsersWithReTaskMap));
        }

        private Set<Id> getCasesToProcess(List<Case> cases) {
            Set<Id> casesToProcessSTMsIds = new Set<Id>();
            for (Case cs : cases) {
                if (cs.VTD_1_Not_Logged_In_For__c > cs.VTD1_StudyMaximumDaysWithoutLoggingIn__c) {
                    casesToProcessSTMsIds.add(cs.Id);
                }
            }
            return casesToProcessSTMsIds;
        }

        private List<HealthCloudGA__CarePlanTemplate__c> getStudies(Set<Id> cases) {
            return [
                    SELECT
                    (
                            SELECT
                                    User__c
                            FROM Study_Team_Members__r
                            WHERE RecordTypeId =: ID_RE_STM
                    )
                            , (
                            SELECT
                                    Id
                            FROM Cases__r
                            WHERE Id IN : cases
                    )
                    FROM HealthCloudGA__CarePlanTemplate__c
                    WHERE Id IN (SELECT VTD1_Study__c FROM Case WHERE Id IN :cases)
            ];
        }
        /**
         * Edited by Olga Baranova on 25.12.2019.
         * Edited as part of SH-6991


         * Move unconverted Tasks to TN Catalog
         *
         * @lastModified: 17-November-20 by Andrey Pivovarov
         * @issue: SH-19434
         * @return List of Tasks for Re


         **/
        private List<Task> generateTask(List<HealthCloudGA__CarePlanTemplate__c> studies, Map<Id, Set<Id>> caseUsersWithReTaskMap) {
            Map<String, String> assignedToMap = new Map<String, String>();
            List <String> tnCatalogCodes = new List<String>();
            List <Id> soursIds = new List<Id>();
            for (HealthCloudGA__CarePlanTemplate__c study : studies) {
                if (study.Study_Team_Members__r == null || study.Study_Team_Members__r.isEmpty()) {
                    continue;
                }
                Set<Id> usersAssignedIds = new Set<Id>();
                for (Study_Team_Member__c studyTeamMember : study.Study_Team_Members__r) {
                    usersAssignedIds.add(studyTeamMember.User__c);
                }
                String allUsersAssignedString = String.join(new List<Id>(usersAssignedIds), ',');
                for (Case cs : study.Cases__r) {
                    Id csId = cs.Id;
                    String usersAssignedString = allUsersAssignedString;
                    if (caseUsersWithReTaskMap.containsKey(csId)) {
                        Set<Id> caseUsersAssignedIds = usersAssignedIds;

                        caseUsersAssignedIds.removeAll(caseUsersWithReTaskMap.get(csId));

                        usersAssignedString = String.join(new List<Id>(caseUsersAssignedIds), ',');
                    }
                    if (usersAssignedString != '') {
                        assignedToMap.put(TN_CATALOG_CODE_FOR_TASK + csId, usersAssignedString);
                        tnCatalogCodes.add(TN_CATALOG_CODE_FOR_TASK);
                        soursIds.add(csId);
                    }
                }
            }
            List<Task> taskList = new List<Task>();
            if (!tnCatalogCodes.isEmpty()) {
                taskList = VT_D2_TNCatalogTasks.generateTasks(tnCatalogCodes, soursIds, null, assignedToMap, false);
            }
            return taskList;
        }

        private Map<Id, Set<Id>> getOpenReTasksByCaseId(Set<Id> caseIds) {
            Map<Id, Set<Id>> caseUsersWithReTaskMap = new Map<Id, Set<Id>>();

            for (Task task : [
                    SELECT
                            Id
                            , OwnerId
                            , WhatId
                    FROM Task
                    WHERE RecordTypeId =: ID_RE_TASK
                    AND VTD1_Case_lookup__c IN : caseIds


                    AND Status = 'Open'
            ]) {
                Id caseId = task.WhatId;
                Id studyTeamMemberREId = task.OwnerId;
                if (!caseUsersWithReTaskMap.containsKey(caseId)) {
                    caseUsersWithReTaskMap.put(caseId, new Set<Id>{studyTeamMemberREId});
                } else {
                    caseUsersWithReTaskMap.get(caseId).add(studyTeamMemberREId);
                }
            }

            return caseUsersWithReTaskMap;
        }
    }

    public without sharing class RecruitmentTaskClosingService {

        public RecruitmentTaskClosingService() {

        }

        public List<User> getFirstLoginUsers(Handler.TriggerContext context) {
            List<User> usersToProcess = new List<User>();
            List<User> newUsers = context.newList;
            Map<Id, User> oldUsers = (Map<Id, User>) context.oldMap;

            for (User newUser : newUsers) {
                User oldUser = oldUsers.get(newUser.Id);
                if (oldUser.First_Log_In__c == null && newUser.First_Log_In__c != null) {
                    usersToProcess.add(newUser);
                }
            }

            return usersToProcess;
        }

        public void closeTasks(List<User> users) {
            List<Task> tasksToUpdate = new List<Task>();
            for (Task task : [
                    SELECT
                            Id
                    FROM Task
                    WHERE RecordTypeId =: ID_RE_TASK

                    AND VTD1_Case_lookup__r.VTD1_Patient_User__c IN :users
                    AND Status = 'Open'
            ]) {
                task.Status = 'Completed';
                tasksToUpdate.add(task);
            }

            update tasksToUpdate;
        }

    }
}