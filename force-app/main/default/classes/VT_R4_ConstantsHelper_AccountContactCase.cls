public with sharing class VT_R4_ConstantsHelper_AccountContactCase {
    public static final string SOBJECT_CASE = 'Case';

    public static final String CASE_STUDY_FIELD_NAME = 'VTD1_Study__c';
    public static final String CASE_PG_FIELD_NAME = 'VTD1_Primary_PG__c';
    public static final String CASE_PI_FIELD_NAME = 'VTD1_PI_user__c';
    public static final String CASE_BACKUP_PG_FIELD_NAME = 'VTD1_Secondary_PG__c';
    public static final String CASE_BACKUP_PI_FIELD_NAME = 'VTD1_Backup_PI_User__c';

   /* public static final String RECORD_TYPE_ID_CASE_CAREPLAN = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CarePlan').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CASE_PCF_READONLY = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VTD1_PCF_Read_Only').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CASE_PCF = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VTD1_PCF').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CONTACT_PCP = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('PCP').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CONTACT_PI = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('PI').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CONTACT_SPONSOR = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Sponsor_Contact').getRecordTypeId();
*/
    //STATUSES - ПЕРЕДЕЛАТЬ!!!
    public static final String CASE_PRE_CONSENT = 'Pre-Consent';
    public static final String CASE_WASHOUT_RUN_IN = 'Washout/Run-In';
    public static final String CASE_WASHOUT_RUN_IN_FAILURE = 'Washout/Run-In Failure';
    public static final String CASE_ACTIVE_RANDOMIZED = 'Active/Randomized';
    public static final String CASE_RANDOMIZED = 'Randomized';
    public static final String CASE_SCREENED = 'Screened';
    public static final String CASE_CONSENTED = 'Consented';
    public static final String CASE_SCREEN_FAILURE = 'Screen Failure';
    public static final String CASE_LOST_TO_FOLLOW_UP = 'Lost to Follow-Up';    // Obsolete in R1.1?
    public static final String CASE_DROPPED = 'Dropped';
    public static final String CASE_COMPLETED = 'Completed';
    public static final String CASE_CLOSED = 'Closed';
    public static final String CASE_OPEN = 'Open'; 
    public static final String CASE_DOES_NOT_QUALIFY = 'Does Not Qualify';
    public static final String CASE_DROPPED_IN_FOLLOW_UP = 'Dropped Treatment,In F/Up';
    public static final String CASE_DROPPED_COMPL_FOLLOW_UP = 'Dropped Treatment,Compl F/Up';
    public static final String CASE_COMPL_IN_FOLLOW_UP = 'Compl Treatment,In F/Up';
    public static final String CASE_COMPL_DROPPED_FOLLOW_UP = 'Compl Treatment,Dropped F/Up';
    public static final Set<String> SEND_SUBJECT_CASE_STATUSES = new Set<String>{   // For Cenduit / CSM only
            CASE_CONSENTED,
            //CASE_DOES_NOT_QUALIFY,
            CASE_SCREENED,
            CASE_SCREEN_FAILURE,
            CASE_WASHOUT_RUN_IN,
            CASE_WASHOUT_RUN_IN_FAILURE,
            CASE_ACTIVE_RANDOMIZED,
            CASE_DROPPED,
            CASE_DROPPED_IN_FOLLOW_UP,
            CASE_DROPPED_COMPL_FOLLOW_UP,
            CASE_COMPLETED,
            CASE_COMPL_IN_FOLLOW_UP,
            CASE_COMPL_DROPPED_FOLLOW_UP
    };

    public static final Set<String> SEND_SUBJECT_CASE_STATUSES_RH = new Set<String>{    // For RR only
            CASE_PRE_CONSENT,
            CASE_CONSENTED,
            CASE_DOES_NOT_QUALIFY,
            CASE_SCREENED,
            CASE_SCREEN_FAILURE,
            CASE_WASHOUT_RUN_IN,
            CASE_WASHOUT_RUN_IN_FAILURE,
            CASE_ACTIVE_RANDOMIZED,
            CASE_DROPPED//,
            //CASE_DROPPED_IN_FOLLOW_UP,
            //CASE_DROPPED_COMPL_FOLLOW_UP,
            //CASE_COMPLETED,
            //CASE_COMPL_IN_FOLLOW_UP,
            //CASE_COMPL_DROPPED_FOLLOW_UP

    };

    public static final String RECORD_TYPE_ID_ACCOUNT_PATIENT = VT_D1_HelperClass.getRTMap().get('Account').get('Patient');
    public static final String RECORD_TYPE_ID_ACCOUNT_SPONSOR = VT_D1_HelperClass.getRTMap().get('Account').get('Sponsor');

    public static final String RECORD_TYPE_ID_CONTACT_PATIENT = VT_D1_HelperClass.getRTMap().get('Contact').get('Patient');
    public static final String RECORD_TYPE_ID_CONTACT_CAREGIVER = VT_D1_HelperClass.getRTMap().get('Contact').get('Caregiver');

    public static final String RECORD_TYPE_ID_CASE_CAREPLAN = VT_D1_HelperClass.getRTMap().get('Case').get('CarePlan'); //VT_R4_ConstantsHelper_AccountContactCase
    public static final String RECORD_TYPE_ID_CASE_PCF_READONLY = VT_D1_HelperClass.getRTMap().get('Case').get('VTD1_PCF_Read_Only'); //VT_R4_ConstantsHelper_AccountContactCase
    public static final String RECORD_TYPE_ID_CASE_PCF = VT_D1_HelperClass.getRTMap().get('Case').get('VTD1_PCF'); //VT_R4_ConstantsHelper_AccountContactCase
    public static final String RECORD_TYPE_ID_CASE_GCF = VT_D1_HelperClass.getRTMap().get('Case').get('VT_R2_General_Contact_Form'); //VT_R4_ConstantsHelper_AccountContactCase
    public static final String RECORD_TYPE_ID_CASE_SPF = VT_D1_HelperClass.getRTMap().get('Case').get('Sponsor_Contact_Form'); //VT_R4_ConstantsHelper_AccountContactCase
    public static final String RECORD_TYPE_ID_CONTACT_PCP = VT_D1_HelperClass.getRTMap().get('Contact').get('PCP'); //VT_R4_ConstantsHelper_AccountContactCase
    public static final String RECORD_TYPE_ID_CONTACT_PI = VT_D1_HelperClass.getRTMap().get('Contact').get('PI'); //VT_R4_ConstantsHelper_AccountContactCase
    public static final String RECORD_TYPE_ID_CONTACT_SPONSOR = VT_D1_HelperClass.getRTMap().get('Contact').get('Sponsor_Contact'); //VT_R4_ConstantsHelper_AccountContactCase


    public static final String ASSIGNMENT_HISTORY_CURRENT = 'Current';
    public static final String ASSIGNMENT_HISTORY_TRANSFERRED = 'Transferred';
    public static final String CANDIDATE_PATIENT_CONVERSION_CONVERTED = 'Converted';
    public static final String CANDIDATE_PATIENT_CONVERSION_FINISHED = 'Finished';
    public static final String CANDIDATE_PATIENT_CONVERSION_FAILED = 'Failed';
    public static final String CANDIDATE_PATIENT_CONVERSION_PENDING = 'Pending';


    public static final String STUDY_MILESTONE_PARENT_FIELD = 'VTD1_Study__c';
    public static final String SITE_MILESTONE_PARENT_FIELD = 'VTD1_Site__c';
    public static final String MILESTONE_DESCRIPTION_FIELD_NAME = 'VTD1_Milestone_Description__c';

    public static final String RT_CAREPLAN = 'CarePlan';
    public static final String RT_PCF = 'VTD1_PCF';
    public static final String RT_GCF = 'VT_R2_General_Contact_Form';
    public static final String RT_PCF_READONLY = 'VTD1_PCF_Read_Only';


    public static final String SITE_ENROLLMENT_OPEN = 'Enrollment Open';
    public static final String SITE_CLOSED = 'Closed';
    public static final String SITE_ENROLLMENT_CLOSED = 'Enrollment Closed';
    public static final String SITE_PREMATURELY_CLOSED = 'Prematurely Closed';


   public static final String STUDY_FIRST_SUBJECT_ENROLLED = 'First Subject Enrolled';
   public static final String STUDY_FIRST_SUBJECT_RANDOMIZED = 'First Subject Randomized';
   public static final String STUDY_FIRST_SUBJECT_SCREENED = 'First Subject Screened';
   public static final String STUDY_FIRST_VIRTUAL_SITE_ACTIVATED = 'First Virtual Site Activated';
   public static final String STUDY_FIRST_VIRTUAL_SITE_CLOSED = 'First Virtual Site Closed';
   public static final String STUDY_FIRST_VIRTUAL_SITE_INITIATED = 'First Virtual Site Initiated';
   public static final String STUDY_FIRST_VIRTUAL_SITE_SELECTED = 'First Virtual Site Selected';
   public static final String LAST_SUBJECT_ENROLLED = 'Last Subject Enrolled';
   public static final String LAST_SUBJECT_RANDOMIZED = 'Last Subject Randomized';
   public static final String LAST_SUBJECT_SCREENED = 'Last Subject Screened';
   public static final String LAST_SUBJECT_OUT = 'Last Subject Out LPLV';
   public static final String SITE_VIRTUAL_SITE_INITIATED = 'Virtual Site Initiated';
   public static final String SITE_VIRTUAL_SITE_SELECTED = 'Virtual Site Selected';
   public static final String SITE_VIRTUAL_SITE_STARTUP_COMPLETION = 'Virtual Site Start-Up Completion';
}