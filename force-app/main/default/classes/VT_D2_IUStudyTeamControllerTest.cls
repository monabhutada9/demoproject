@IsTest
public with sharing class VT_D2_IUStudyTeamControllerTest {

    public static void getPatientStudyTeamTest() {
        Test.startTest();

        List<Case> casesList = [SELECT Id, VTD1_Primary_PG__c FROM Case];
        System.assert(casesList.size() > 0);
        if(casesList.size() > 0){
            Case cas = casesList.get(0);
            String userId = cas.VTD1_Primary_PG__c;
            User u = [SELECT Id FROM User WHERE Id =: userId];
            insert new CaseShare(CaseId = cas.Id, UserOrGroupId = userId, CaseAccessLevel = 'Edit', RowCause = 'Manual');

            Datetime currentDateTime = Datetime.now();
            Event ev = new Event();
            ev.RecordTypeId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_EVENT_OUTOFOFFICE;
            ev.StartDateTime = currentDateTime;
            ev.EndDateTime = currentDateTime.addMinutes(1);
            ev.OwnerId = userId;
            insert ev;

            System.runAs(u) {
                String patientStudyTeamString = VT_D2_IUStudyTeamController.getPatientStudyTeam(cas.Id);
                VT_D2_IUStudyTeamController.PatientStudyTeam patientStudyTeam = (VT_D2_IUStudyTeamController.PatientStudyTeam) JSON.deserialize(patientStudyTeamString, VT_D2_IUStudyTeamController.PatientStudyTeam.class);

                System.assertNotEquals(null, patientStudyTeam.primaryPIUser);
                System.assertNotEquals(null, patientStudyTeam.primaryPGUser);
                System.assertNotEquals(null, patientStudyTeam.backupPIUser);
                System.assertNotEquals(null, patientStudyTeam.backupPGUser);
                System.assertEquals('Acting', patientStudyTeam.primaryPIStatus);
                System.assertEquals('Out of office', patientStudyTeam.primaryPGStatus);
                System.assertEquals('Secondary', patientStudyTeam.backupPIStatus);
                System.assertEquals('Acting', patientStudyTeam.backupPGStatus);
            }
        }
        Test.stopTest();
    }
}