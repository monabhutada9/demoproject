public class VT_D1_eProHomeController {
	private static Map<String, String> STATUS_LABELS = new Map<String, String> { // map status API name to custom label for some statues
		'Not Started' => Label.VTR2_Future,
		'Review Required' => Label.VTR2_Completed,
		'Reviewed' => Label.VTR2_Completed,
		'Reviewed (Partially Completed)' => Label.VTR2_Completed
	};
	private static Set<String> PRE_SUBMISSION_STATUSES = new Set<String>{
		VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_READY_TO_START,
		VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_IN_PROGRESS
	};

	private static  String statusApiName;
	private static String statusLbl;
	@AuraEnabled(Cacheable=true)
	public static String getProtocolEdiaryTool() {
		return new QueryWithoutSharing().getEcoaConfigData(); //SH-20580: some patients have not read access to their study, added it as a quick workaround.
		
	}
    @AuraEnabled
	public static String getEcoaUri() {
		return eCOA_IntegrationDetails__c.getInstance().eCoa_Diary_Url__c + VT_R5_eCoaSSO.getToken(UserInfo.getUserId());
	}
    @AuraEnabled
    public static VTD1_General_Settings__mdt getScribeGoogleAppStores() {
        return [
                SELECT VTR5_IQVIAScribeAppleAppStore__c, VTR5_IQVIAScribeAndroidGooglePlay__c
                FROM VTD1_General_Settings__mdt LIMIT 1
        ];
    }
	@AuraEnabled
	public static Boolean getIsUserCaregiver() {
		return ![SELECT Id FROM User WHERE Id = :UserInfo.getUserId() AND Profile.Name = 'Caregiver'].isEmpty();
	}
	@AuraEnabled
	public static List<SurveyWrapper> performSearch(String searchString, List<SurveyWrapper> listForSearch) {
		List<SurveyWrapper> foundItemsList = new List<SurveyWrapper>();
		Integer i;
		try {
			for (i = 0; i < listForSearch.size(); i++) {
				if ((listForSearch.get(i).sv.Name != null)
						&& (listForSearch.get(i).sv.Name.contains(searchString))
						&& (listForSearch.get(i).sv.Name != '')) {
					foundItemsList.add(listForSearch.get(i));
				}
			}
		} catch (NullPointerException e) {
			return foundItemsList;
		}
		return null;
	}
	@AuraEnabled
	public static SurveysWrapper getSurveys() {
		List<VTD1_Survey__c> surveys = getSurveysRemote();
		System.debug(surveys);
		SurveysWrapper sw = new SurveysWrapper();
		sw.surveys =surveys;
		Long offset = (DateTime.newInstance(DateTime.now().date(), DateTime.now().time()).getTime() - DateTime.newInstance(DateTime.now().dateGmt(), DateTime.now().timeGmt()).getTime()) / (60 * 60 * 1000);
		sw.timeZoneOffset = offset;
		return sw;
	}
	@AuraEnabled
	public static SurveysWrapperForPatient getSurveysForPatient() {
		SurveysWrapper surveysWrapper = getSurveys();
		List<VTD1_Survey__c> surveys = surveysWrapper.surveys;
		Map <Id, VTD1_Protocol_ePRO__c> protocolMap = getEdairyInstructions(surveys);
		for (VTD1_Protocol_ePRO__c item : protocolMap.values()) {
			system.debug(item.Id + ' - ' + item.Name);
		}
		System.debug(surveys);
		for (VTD1_Survey__c survey : surveys) {
			survey.VTD1_Protocol_ePRO__r = protocolMap.get(survey.VTD1_Protocol_ePRO__c);
			system.debug(survey.VTD1_Protocol_ePRO__c);
			system.debug(protocolMap.get(survey.VTD1_Protocol_ePRO__c));
		}
		List<SurveyWrapper> surveysSurveyWrappers =  translateSurveysAndInstrustions(surveys);
		SurveysWrapperForPatient sw = new SurveysWrapperForPatient();
		sw.surveys = surveysSurveyWrappers;
		Long offset = (DateTime.newInstance(DateTime.now().date(), DateTime.now().time()).getTime() - DateTime.newInstance(DateTime.now().dateGmt(), DateTime.now().timeGmt()).getTime()) / (60 * 60 * 1000);
		sw.timeZoneOffset = offset;
		return sw;
	}
	public static Map <Id, VTD1_Protocol_ePRO__c> getEdairyInstructions(List<VTD1_Survey__c> surveys) {
		System.debug('getEdairyInstructions ' + surveys);
		Set <Id> ids = new Set<Id>();
		for (VTD1_Survey__c survey : surveys) {
			ids.add(survey.VTD1_Protocol_ePRO__c);
		}
		Map <Id, VTD1_Protocol_ePRO__c> protocolMap = new Map <Id, VTD1_Protocol_ePRO__c>([select Id, Name,VTD1_Subject__c, VTR4_Is_Branching_eDiary__c, (select Id, VTR2_Content__c, VTR2_Group_number__c from Protocol_eDiary_Instructions__r)
		from VTD1_Protocol_ePRO__c where Id in : ids]);
		List <VTR2_eDiaryInstructions__c> instructions = new List<VTR2_eDiaryInstructions__c>();
		for (VTD1_Protocol_ePRO__c protocol : protocolMap.values()) {
			for (VTR2_eDiaryInstructions__c instruction : protocol.Protocol_eDiary_Instructions__r) {
				System.debug('instruction = ' + instruction);
				instructions.add(instruction);
			}
		}
		return protocolMap;
	}
	public static List<VTD1_Survey__c> getSurveysRemote() {
		Id currentUserId = UserInfo.getUserId();
		Id patientId;
		String profileName = VT_D1_PatientCaregiverBound.getUserProfileName(currentUserId);
		String profileCG = VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME;
		if (profileName == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
			patientId = currentUserId;
		} else if (profileName == profileCG) {
			system.debug('IS CAREGIVER');
			patientId = VT_D1_PatientCaregiverBound.findPatientOfCaregiver(currentUserId);
			system.debug(patientId);
		} else {
			return new List<VTD1_Survey__c>();
		}
		Id currentCaseId = [
				SELECT Id, VTD1_Clinical_Study_Membership__c
				FROM Contact
				WHERE VTD1_UserId__c = :patientId
				ORDER BY RecordType.DeveloperName DESC
				LIMIT 1
		].VTD1_Clinical_Study_Membership__c;
		DateTime dt = DateTime.now();
		String queryStr = 'SELECT Id, Name, VTD1_Protocol_ePRO__r.Name, ' +
			'VTD1_Protocol_ePRO__r.VTR4_Is_Branching_eDiary__c, ' +
			'VTR4_Percentage_Completed__c, ' +
			'VTD1_Status__c, ' +
			'VTD1_Completion_Time__c, ' +
			'toLabel(VTD1_Status__c) lblStatus, ' +
			'VTD1_Protocol_ePRO__c, ' +
			'VTD1_Due_Date__c, ' +
			'VTD1_Trigger__c, ' +
			'VTD1_Protocol_ePRO__r.VTD1_Subject__c, ' +
			'VTD1_Start_Time__c,' +
			'VTR2_DocLanguage__c,' +
			'VTR2_DateOpened__c,' +
			'VTD1_CSM__r.VTD1_Study__r.VTR5_Display_Completed_Missed_Ediary__c,'+
			'(SELECT Id, VTR2_Question_content__c, VTD1_Answer__c FROM Survey_Answers__r)' +
			' FROM VTD1_Survey__c WHERE VTD1_CSM__c = :currentCaseId AND VTD1_Date_Available__c<:dt AND RecordType.Name != \'Source Form\'';
		system.debug('!!!!' + queryStr);
		Set<String> preSubmissionStatuses = PRE_SUBMISSION_STATUSES;
		if (profileName == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
			queryStr = queryStr +
				' AND VTD1_Protocol_ePRO__r.VTD1_Subject__c !=:profileCG';
		} else {
			queryStr = queryStr + ' AND ((VTD1_Caregiver_User_Id__c != NULL)' +
				' OR VTD1_Protocol_ePRO__r.VTD1_Subject__c =:profileCG)';
		}
		queryStr = queryStr + ' AND VTD1_Status__c !=\'' +
			+ VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_NOT_STARTED + '\'' + '  ORDER BY VTD1_Due_Date__c';
		system.debug('!!!!' + queryStr);
		List<VTD1_Survey__c> surveys = Database.query(queryStr);
		system.debug(surveys);
		//if (profileName != VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME && profileName != VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME) {
		VT_D1_TranslateHelper.translate(surveys);
		for (VTD1_Survey__c s : surveys) {
			s.Name = s.VTD1_Protocol_ePRO__r.Name;
		}
		//}
		return surveys;
	}
	public static List<SurveyWrapper> translateSurveysAndInstrustions (List<VTD1_Survey__c> surveys) {
		Map<String, List<VTD1_Survey__c>> langSurveyMap = new Map<String, List<VTD1_Survey__c>>();
		Map<String, List<VTR2_eDiaryInstructions__c>> langProtocolInstractionMap = new Map<String, List<VTR2_eDiaryInstructions__c>>();
		String docLangContact =  [SELECT Contact.VTR2_Doc_Language__c
		FROM User WHERE Id=:UserInfo.getUserId()].Contact.VTR2_Doc_Language__c;
		String language;
		Map<Id, String> engInstrContent = new Map<Id, String>();
		for (VTD1_Survey__c survey : surveys) {
			language = getDocLang(survey.Id, survey, docLangContact==null?UserInfo.getLanguage():docLangContact);
			System.debug('language = ' + language + ' ' + survey.Id);
			List<VTD1_Survey__c> surveysByLang = langSurveyMap.get(language);
			if (surveysByLang == null) {
				surveysByLang = new List<VTD1_Survey__c>();
			}
			surveysByLang.add(survey);
			langSurveyMap.put(language, surveysByLang);
			// instructions
			List<VTR2_eDiaryInstructions__c> instrByLang = langProtocolInstractionMap.get(language + ';' + survey.Id);
			if (instrByLang == null) {
				instrByLang = new List<VTR2_eDiaryInstructions__c>();
			}
			instrByLang.addAll(survey.VTD1_Protocol_ePRO__r.Protocol_eDiary_Instructions__r);
			for (VTR2_eDiaryInstructions__c instr : survey.VTD1_Protocol_ePRO__r.Protocol_eDiary_Instructions__r) {
				engInstrContent.put(instr.Id, instr.VTR2_Content__c);
			}
			langProtocolInstractionMap.put(language + ';' + survey.Id, instrByLang);
		}
		List<VTD1_Survey__c> surveysResult = new List<VTD1_Survey__c>();
		for (String langString : langSurveyMap.keySet()) {
			List<VTD1_Survey__c> svToTranslate = langSurveyMap.get(langString);
			VT_D1_TranslateHelper.translate(svToTranslate, langString);
			for (VTD1_Survey__c sv: svToTranslate) {
				sv.Name = sv.VTD1_Protocol_ePRO__r.Name;
			}
			surveysResult.addAll(langSurveyMap.get(langString));
		}
		for (String langString : langProtocolInstractionMap.keySet()) {
			List<VTR2_eDiaryInstructions__c> instrByLang = langProtocolInstractionMap.get(langString);
			for (VTR2_eDiaryInstructions__c instr : instrByLang) {
				instr.VTR2_Content__c = engInstrContent.get(instr.Id);
			}
			System.debug('instruction translate ' + instrByLang + ' ' + langString.split(';')[0]);
			VT_D1_TranslateHelper.translate(instrByLang, langString.split(';')[0]);
			List<VTR2_eDiaryInstructions__c> transInstructions = new List<VTR2_eDiaryInstructions__c>();
			for (VTR2_eDiaryInstructions__c instruction :  instrByLang) {
				System.debug('instruction.VTR2_Content__c = ' + instruction.VTR2_Content__c);
				transInstructions.add(new VTR2_eDiaryInstructions__c(Id = instruction.Id,
					VTR2_Group_number__c = instruction.VTR2_Group_number__c,
					VTR2_Content__c = instruction.VTR2_Content__c));
			}
			langProtocolInstractionMap.put(langString, transInstructions);
		}
		List<SurveyWrapper> surveyWrappers = new List<SurveyWrapper>();
		for (VTD1_Survey__c survey : surveysResult) {
			SurveyWrapper surveyWrapper = new SurveyWrapper();
			language = getDocLang(survey.Id, survey, docLangContact==null?UserInfo.getLanguage():docLangContact);
			surveyWrapper.sv = survey;
			//surveyWrapper.sv.Name = survey.VTD1_Protocol_ePRO__r.Name;
			surveyWrapper.instructions = langProtocolInstractionMap.get(language +';' + survey.Id);
			if (surveyWrapper.instructions != null) {
				System.debug(' surveyWrapper.instructions ' +  surveyWrapper.instructions.size());
			}
			surveyWrappers.add(surveyWrapper);
		}
		return surveyWrappers;
	}
	@AuraEnabled
	public static List<VTD1_Protocol_ePro_Question__c> getQuestionsAnswers(Id surveyId, Id protocolId) {
		return getQuestionsAnswers(surveyId, protocolId, null);
	}

	@AuraEnabled
	public static List<VTD1_Protocol_ePro_Question__c> getQuestionsAnswers(Id surveyId, Id protocolId, String surveyStatus) {
		if (surveyStatus == 'Ready to Start') {
			createEmptyAnswers(surveyId, protocolId);
		}
		List<VTD1_Protocol_ePro_Question__c> questions = [
				SELECT 	Id,
						VTR2_QuestionContentLong__c,
						VTR4_QuestionContentRichText__c,
						VTD1_Type__c,
						VTD1_Options__c,
						VTR4_Number__c,
						VTD1_Number__c,
						VTD1_Group_number__c,
						VTR4_Branch_Parent__c,
						VTR4_Response_Trigger__c,
						VTD1_Protocol_ePRO__r.VTR4_Is_Branching_eDiary__c,
						VTR4_Contains_Branching_Logic__c,
				(
						SELECT 	Id, VTD1_Answer__c
						FROM 	Survey_Answers__r
						WHERE 	VTD1_Survey__c =:surveyId
						LIMIT 1
				)
				FROM 	VTD1_Protocol_ePro_Question__c
				WHERE 	VTD1_Protocol_ePRO__c =:protocolId
				ORDER BY VTD1_Group_number__c, VTR4_Number__c
		];
		return questions;
	}

	private static void createEmptyAnswers(Id surveyId, Id protocolId) {
		List<VTD1_Protocol_ePro_Question__c> questions = [
				SELECT 	Id,
				(
						SELECT 	Id, VTD1_Answer__c
						FROM 	Survey_Answers__r
						WHERE 	VTD1_Survey__c =:surveyId
						LIMIT 1
				)
				FROM 	VTD1_Protocol_ePro_Question__c
				WHERE 	VTD1_Protocol_ePRO__c =:protocolId
				ORDER BY VTD1_Group_number__c, VTR4_Number__c
		];
		List<VTD1_Survey_Answer__c> newAnswerForInsert = new List<VTD1_Survey_Answer__c>();
		for (VTD1_Protocol_ePro_Question__c question : questions) {
			if (question.Survey_Answers__r.isEmpty()) {
				newAnswerForInsert.add(new VTD1_Survey_Answer__c(
						VTD1_Survey__c = surveyId,
						VTD1_Question__c = question.Id));
			}
		}
		if (!newAnswerForInsert.isEmpty()) {
			insert newAnswerForInsert;
		}
	}
	@AuraEnabled
	public static QAWrapper getQuestionsAnswersForPTCG(Id surveyId, Id protocolId, String surveyStatus) {
		try {
			QAWrapper qaWrapper = new QAWrapper();
			List<VTD1_Protocol_ePro_Question__c>  questions = getQuestionsAnswers(surveyId, protocolId, surveyStatus);
			String languageString = getDocLang(surveyId, null, null);
			VT_D1_TranslateHelper.translate(questions, languageString);
			qaWrapper.qas = questions;
			qaWrapper.lblsForSurvey = getTranslatedLabels(languageString);
			return qaWrapper;
		} catch (Exception e ) {
			throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
		}
	}
	@AuraEnabled
	public static QAWrapper getQuestionsAnswersForPatient(Id surveyId, Id protocolId, String surveyStatus) {
		return getQuestionsAnswersForPTCG(surveyId, protocolId, surveyStatus);
	}
	public static SavedResultsWrapper saveAndRemoveAnswers(String answers, String answersToDelete, Id surveyId, Integer percentage, Boolean isMobileContext) {
		removeAnswers(answersToDelete);
		update new VTD1_Survey__c(Id = surveyId, VTR4_Percentage_Completed__c = percentage);
		//return null;
		return saveAnswers(answers, surveyId, false, isMobileContext);
	}
	@AuraEnabled
	public static SavedResultsWrapper saveAndRemoveAnswers(String answers, String answersToDelete, Id surveyId, Integer percentage) {
		removeAnswers(answersToDelete);
		update new VTD1_Survey__c(Id = surveyId, VTR4_Percentage_Completed__c = percentage);
		//return null;
		return saveAnswers(answers, surveyId, false);
	}
	private static void removeAnswers(String answersToDelete) {
		if(String.isNotBlank(answersToDelete)) {
			Set<String> answIds = (Set<String>) JSON.deserialize(answersToDelete, Set<String>.class);
			List<VTD1_Survey_Answer__c> answToDeleteList = new List<VTD1_Survey_Answer__c>();
			for(String answId : answIds) {
				answToDeleteList.add(new VTD1_Survey_Answer__c(Id = answId));
			}
			if(!answToDeleteList.isEmpty()) {
				delete answToDeleteList;
			}
		}
	}
	@AuraEnabled
	public static SavedResultsWrapper saveAnswer(String answer, Id surveyId, Boolean submit, Integer percentage) {
		update new VTD1_Survey__c(Id = surveyId, VTR4_Percentage_Completed__c = percentage);
		return saveAnswers(answer, surveyId, submit);
	}
	@AuraEnabled
	public static SavedResultsWrapper saveAnswers(String answers, Id surveyId, Boolean submit) {
		return saveAnswers(answers, surveyId, submit, false);
	}
	@AuraEnabled
	public static SavedResultsWrapper saveAnswers(String answers, Id surveyId, Boolean submit, Boolean isMobileContext) {
		List<AnswerWrapper> answersList = (List<AnswerWrapper>) JSON.deserialize(answers, List<AnswerWrapper>.class);
		if(answersList == null && !isMobileContext){
			throw new AuraHandledException(System.Label.VT_D1_ImpossibleToSave);
		}
		SavedResultsWrapper resultsWrapper = new SavedResultsWrapper();
		setStatusInfo(surveyId);
		String systemLangString = [Select LanguageLocaleKey from Organization][0].LanguageLocaleKey;
		List<VTD1_Protocol_ePro_Question__c>  eProOptionsOriginal;
		Map<Id, VTD1_Protocol_ePro_Question__c>  eProOptionsTranslatedMap = new Map<Id, VTD1_Protocol_ePro_Question__c>();
		Map<Id, VTD1_Protocol_ePro_Question__c>  eProOptionsMap = new Map<Id, VTD1_Protocol_ePro_Question__c>();
		String docLang = getDocLang(surveyId, null, null);
		if (systemLangString != docLang) {
			List<Id> listQuestionIds = new List<Id>();
			for (AnswerWrapper an : answersList) {
				if (an.answer != null && an.answer.length() != 0
					&& an.answerType != 'Long Text (Essay)' && an.answerType != 'Short Text') {
					listQuestionIds.add(an.questionId);
				}
			}
			if (listQuestionIds.size() != 0) {
				eProOptionsOriginal = [SELECT Id, VTD1_Options__c
				FROM VTD1_Protocol_ePro_Question__c
				WHERE Id IN :listQuestionIds];
				for (VTD1_Protocol_ePro_Question__c q : eProOptionsOriginal) {
					eProOptionsMap.put(q.Id, new VTD1_Protocol_ePro_Question__c(Id=q.Id, VTD1_Options__c = q.VTD1_Options__c));
				}
				System.debug('docLang ' + docLang);
				VT_D1_TranslateHelper.translate(eProOptionsOriginal, docLang);
				for (VTD1_Protocol_ePro_Question__c q : eProOptionsOriginal) {
					eProOptionsTranslatedMap.put(q.Id, q);
				}
			}
		}
		if (statusApiName != null) {
			resultsWrapper.sStatus = statusApiName;
			resultsWrapper.lblStatus = statusLbl;
			if (!PRE_SUBMISSION_STATUSES.contains(statusApiName)) {
				resultsWrapper.errorMsg = System.Label.VT_D1_CantSubmit;
				return resultsWrapper;
			}
		}
		system.debug('answers  size' + answersList.size());
		List<VTD1_Survey_Answer__c> anss = new List<VTD1_Survey_Answer__c>();
		for (AnswerWrapper an : answersList) {
			system.debug('an.answerId ' + an.answerId + ' na.VTD1_Question__c ' + an.questionId + ' an.answer '  + an.answer);
			VTD1_Survey_Answer__c na = new VTD1_Survey_Answer__c();
			na.Id = an.answerId;
			na.VTD1_Question__c = an.questionId;
			na.VTD1_Answer__c =  an.answer;
			na.VTR2_English_Answer__c =  an.answer;
			if (eProOptionsTranslatedMap.size()!=0 && an.answer != null && an.answer.length() != 0) {
				if (eProOptionsTranslatedMap.get(an.questionId) != null
					&& eProOptionsMap.get(an.questionId) != null) {
					String translatedOptions = eProOptionsTranslatedMap.get(an.questionId).VTD1_Options__c;
					String options = eProOptionsMap.get(an.questionId).VTD1_Options__c;
					if (translatedOptions != null && options != null) {
						List<String> translatedOptionsList = translatedOptions.split(';');
						for (Integer i = 0 ; i < translatedOptionsList.size(); i++) {
							translatedOptionsList[i] = translatedOptionsList[i].trim();
						}
						List<String> optionsList = options.split(';');
						if (an.answerType == 'Multi Select Drop Down' || an.answerType == 'Checkbox') {
							List<String> optionsAnsweredTranslated = an.answer.split(';');
							String optionsAnswered = '';
							for (String oa : optionsAnsweredTranslated) {
								Integer optionIndex = translatedOptionsList.indexOf(oa.trim());
								if (optionIndex >= 0) {
									if (optionsList.get(optionIndex) != null) {
										optionsAnswered+= optionsList.get(optionIndex) + ';';
									}
								}
							}
							if (optionsAnswered.length() != 0) {
								na.VTR2_English_Answer__c =  optionsAnswered.removeEnd(';');
							}
						} else if (an.answerType != 'Long Text (Essay)' && an.answerType != 'Short Text') {
							Integer optionIndex = translatedOptionsList.indexOf(an.answer.trim());
							if (optionIndex >= 0) {
								if (optionsList.get(optionIndex) != null) {
									na.VTR2_English_Answer__c = optionsList.get(optionIndex);
								}
							}
						}
					}
				}
			}
			na.VTR2_Is_Answered__c = String.isNotBlank(na.VTR2_English_Answer__c);
			if (an.answerId == null) {
				na.VTD1_Survey__c = surveyId;
			}
			anss.add(na);
		}
		List<VTD1_Survey_Answer__c> newAnswers = new List<VTD1_Survey_Answer__c>();
		try {
			Database.UpsertResult[] results = Database.upsert(anss);
			for(Integer index = 0, size = results.size(); index < size; index++) {
				if(results[index].isSuccess()) {
					if(results[index].isCreated()) {
						VTD1_Survey_Answer__c na = new VTD1_Survey_Answer__c();
						na.VTD1_Question__c = anss[index].VTD1_Question__c;
						na.VTD1_Answer__c = results[index].getId();
						newAnswers.add(na);
					}
				}
			}
		} catch (DmlException e) {
			if (isMobileContext) {
				throw e;
			}
			system.debug('!!! Error while updaing answers!');
			system.debug(e.getStackTraceString() + e.getMessage());
		}
		if (submit) {
			VTD1_Survey__c sv = new VTD1_Survey__c();
			sv.Id = surveyId;
			sv.VTD1_Completion_Time__c = Datetime.now();
			sv.VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED;
			sv.VTR5_CompletedBy__c = UserInfo.getUserId();
			try {
				update sv;
				setStatusInfo(surveyId);
				resultsWrapper.sStatus = statusApiName;
				resultsWrapper.lblStatus = statusLbl;
			} catch (DmlException e) {
				newAnswers = null;
				system.debug('!!! Error while submitting survey!');
				if (isMobileContext) {
					throw e;
				}
				throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
			}
		}
		resultsWrapper.answers = newAnswers;
		system.debug('!!! ' +resultsWrapper.answers);
		return resultsWrapper;
	}

	@AuraEnabled
	public static SurveyWrapper updateDateTime(Id surveyId, String status) {
		SurveyWrapper surveyWrapper = new SurveyWrapper();
		VTD1_Survey__c s = setStatusInfo(surveyId);
		surveyWrapper.sv = s;
		//VTD1_Survey__c s = new VTD1_Survey__c();
		if (!PRE_SUBMISSION_STATUSES.contains(statusApiName) && s.VTR2_DateOpened__c != null) {
			surveyWrapper.errorMsg = System.Label.VT_D1_CantSubmit;
			s.VTD1_Status__c = statusApiName;
			surveyWrapper.lblStatus = statusLbl;
			return surveyWrapper;
		}
		//s.Id = surveyId;
		if (s.VTD1_Start_Time__c == null) {
			s.VTD1_Start_Time__c = Datetime.now();
		}
		if (s.VTR2_DateOpened__c == null) {
			s.VTR2_DateOpened__c = Datetime.now();
		}
		try {
			update s;
			setStatusInfo(surveyId);
			s.VTD1_Status__c = statusApiName;
			surveyWrapper.lblStatus = statusLbl;
		} catch (DmlException e) {
			system.debug('!!! Error while updating date of survey start!');
		}
		surveyWrapper.sv = s;
		return surveyWrapper;
	}
	private static VTD1_Survey__c setStatusInfo (Id surveyId) {
		String status;
		List<VTD1_Survey__c> svInDB = [SELECT Id, VTD1_Status__c, VTD1_Start_Time__c, VTR2_DateOpened__c FROM VTD1_Survey__c
		WHERE Id=:surveyId];
		if (svInDB.size() != 0) {
			statusApiName = (String) svInDB[0].get('VTD1_Status__c');
			if (STATUS_LABELS.containsKey(statusApiName)) {
				statusLbl = STATUS_LABELS.get(statusApiName);
			} else {
				list<Schema.PicklistEntry> PicklistEntries = VTD1_Survey__c.VTD1_Status__c.getDescribe().getPicklistValues();
				map<String,String> ApiToLabel = new map<String,String>();
				for (Schema.PicklistEntry pe : PicklistEntries){
					ApiToLabel.put(pe.getValue(),pe.getLabel());
				}
				statusLbl =  ApiToLabel.get(statusApiName);
			}
			return svInDB[0];
		}
		return null;
	}
	private static String getDocLang(Id surveyId, VTD1_Survey__c sv, String docLanguageContact) {
		String languageString = UserInfo.getLanguage();
		if (sv == null) {
			sv = [SELECT VTR2_DocLanguage__c FROM VTD1_Survey__c WHERE Id=:surveyId];
		}
		if (sv.VTR2_DocLanguage__c != null) {
			languageString = sv.VTR2_DocLanguage__c;
		} else {
			if (docLanguageContact == null) {
				docLanguageContact = [SELECT Contact.VTR2_Doc_Language__c
				FROM User WHERE Id=:UserInfo.getUserId()].Contact.VTR2_Doc_Language__c;
			}
			if (docLanguageContact != null) {
				languageString = docLanguageContact;
			}
		}
		System.debug('languageString ' + languageString);
		return  languageString;
	}
	public static LabelsForSurvey getTranslatedLabels(String languageString) {
		System.debug('languageString ' + languageString);
		LabelsForSurvey labelsForSurvey = new LabelsForSurvey();
		labelsForSurvey.eProCompleteLbl = VT_D1_TranslateHelper.getLabelValue('VT_D1_eProComplete', languageString);
		labelsForSurvey.eProSaveLbl = VT_D1_TranslateHelper.getLabelValue('VT_D1_eProSave', languageString);
		labelsForSurvey.eProCancelLbl = VT_D1_TranslateHelper.getLabelValue('VT_D1_eProCancel', languageString);
		labelsForSurvey.eProPagesLbl = VT_D1_TranslateHelper.getLabelValue('VT_D1_eProPages', languageString);
		labelsForSurvey.ePro_SelectOptionLbl = VT_D1_TranslateHelper.getLabelValue('VT_D1_ePro_SelectOption', languageString);
		labelsForSurvey.eProSeeHighlightedInRedLbl = VT_D1_TranslateHelper.getLabelValue('VT_D1_eProSeeHighlightedInRed', languageString);
		labelsForSurvey.selectYourAnswerLbl = VT_D1_TranslateHelper.getLabelValue('VT_D1_ePro_SelectYourAnswer', languageString);
		labelsForSurvey.submittedAnswerLbl = VT_D1_TranslateHelper.getLabelValue('VT_D1_ePro_SubmittedAnswer', languageString);
		labelsForSurvey.errorOccurredSavingLbl = VT_D1_TranslateHelper.getLabelValue('VT_D2_ErrorOccurredSaving', languageString);
		labelsForSurvey.showMoreLbl = VT_D1_TranslateHelper.getLabelValue('VTR2_ShowMore', languageString);
		labelsForSurvey.showLessLbl = VT_D1_TranslateHelper.getLabelValue('VTR2_ShowLess', languageString);
		labelsForSurvey.somethingSubmittedLbl = VT_D1_TranslateHelper.getLabelValue('VTR3_SomethingSubmitted', languageString);
		labelsForSurvey.savedSuccessLbl = VT_D1_TranslateHelper.getLabelValue('VT_D2_SavedSuccess', languageString);
		labelsForSurvey.errorOccurredSubmittingLbl = VT_D1_TranslateHelper.getLabelValue('VT_D2_ErrorOccurredSubmitting', languageString);
		return labelsForSurvey;
	}
	public with sharing class AnswerWrapper {
		public String questionId;
		public String answerId;
		public String answerType;
		public String answer;
	}
	public class SurveysWrapper {
		@AuraEnabled public List<VTD1_Survey__c> surveys;
		@AuraEnabled public Long timeZoneOffset;
	}
	public class SurveysWrapperForPatient {
		@AuraEnabled public List<SurveyWrapper> surveys;
		@AuraEnabled public Long timeZoneOffset;
	}
	public class SavedResultsWrapper {
		@AuraEnabled public List<VTD1_Survey_Answer__c> answers;
		@AuraEnabled public String sStatus;
		@AuraEnabled public String errorMsg;
		@AuraEnabled public String lblStatus;
	}
	public class SurveyWrapper {
		@AuraEnabled public VTD1_Survey__c sv;
		@AuraEnabled public List<Case> cas;
		@AuraEnabled public String errorMsg;
		@AuraEnabled public String lblStatus;
		@AuraEnabled public List<VTR2_eDiaryInstructions__c> instructions;
	}
	public class QAWrapper {
		@AuraEnabled public List<VTD1_Protocol_ePro_Question__c> qas;
		@AuraEnabled public LabelsForSurvey lblsForSurvey;
	}
	public class LabelsForSurvey {
		@AuraEnabled public String      eProCompleteLbl;
		@AuraEnabled public String      eProSaveLbl;
		@AuraEnabled public String      eProCancelLbl;
		@AuraEnabled public String      eProPagesLbl;
		@AuraEnabled public String      ePro_SelectOptionLbl;
		@AuraEnabled public String      eProSeeHighlightedInRedLbl;
		@AuraEnabled public String      selectYourAnswerLbl;
		@AuraEnabled public String      submittedAnswerLbl;
		@AuraEnabled public String      errorOccurredSavingLbl;
		@AuraEnabled public String      showMoreLbl;
		@AuraEnabled public String      showLessLbl;
		@AuraEnabled public String      somethingSubmittedLbl;
		@AuraEnabled public String      savedSuccessLbl;
		@AuraEnabled public String      errorOccurredSubmittingLbl;
	}
	public without sharing class QueryWithoutSharing{
		public String getEcoaConfigData(){
			List<Case> caseNeeded = [
				SELECT VTD1_Study__r.VTR5_eDiaryTool__c
				FROM Case
				WHERE Id IN (SELECT VTD1_Clinical_Study_Membership__c
							FROM Contact
							WHERE VTD1_UserId__c =: UserInfo.getUserId())];
        	return !caseNeeded.isEmpty() ? caseNeeded[0].VTD1_Study__r.VTR5_eDiaryTool__c : null;
        	
		}
	}
}