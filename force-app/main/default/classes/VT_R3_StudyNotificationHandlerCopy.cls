public class VT_R3_StudyNotificationHandlerCopy {
    private Map<Id, List<Case>> getCasesForRecurringNotificationsByUserId(Set<Id> userIdsForRecurring) {
        Map<Id, List<Case>> casesForRecurringNotificationsByUserId = new Map<Id, List<Case>>();
        for(Case c : [SELECT Id
                      , VTD1_Patient_User__c
                      , Status
                      , VTD1_Study__c
                      , Contact.Account.VTD2_Caregiver_s_ID__c
                      FROM Case
                      WHERE VTD1_Patient_User__c != NULL
                      AND RecordType.Name = :VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN
                      AND ( VTD1_Patient_User__c IN :userIdsForRecurring OR Contact.Account.VTD2_Caregiver_s_ID__c IN :userIdsForRecurring)
                     ])
        {
            if(c.VTD1_Patient_User__c != null){
                if (!casesForRecurringNotificationsByUserId.containsKey(c.VTD1_Patient_User__c)) {
                    casesForRecurringNotificationsByUserId.put(c.VTD1_Patient_User__c, new List<Case>());
                }
                casesForRecurringNotificationsByUserId.get(c.VTD1_Patient_User__c).add(c);
            }
            if(c.Contact.Account.VTD2_Caregiver_s_ID__c != null){
                if (!casesForRecurringNotificationsByUserId.containsKey(c.Contact.Account.VTD2_Caregiver_s_ID__c)) {
                    casesForRecurringNotificationsByUserId.put(c.Contact.Account.VTD2_Caregiver_s_ID__c, new List<Case>());
                }
                casesForRecurringNotificationsByUserId.get(c.Contact.Account.VTD2_Caregiver_s_ID__c).add(c);
            }
        }
        return casesForRecurringNotificationsByUserId;
    }
    
    
}