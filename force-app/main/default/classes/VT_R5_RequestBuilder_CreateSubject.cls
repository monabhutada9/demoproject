/**
* @author: Carl Judge
* @date: 29-Apr-20
* @description: JSON body for creating eCOA subject
**/
public class VT_R5_RequestBuilder_CreateSubject implements VT_D1_RequestBuilder {
    public String username;
    public String email;
    public String studyGuid;
    public String timezone;
    public String ident;
    public String siteGuid;
    public Map<String, String> metadata = new Map<String, String>();
    public String randomizationId;
    public String studySpecificId;
    public String subjectLocale;
    public String stateClient;
    public String stateClientOverwrite;
    public String stateServerMerge;
    public String eproRuleSetGuid;
    public String clinroRuleSetGuid;
    public String orgGuid = eCOA_IntegrationDetails__c.getInstance().eCOA_OrgGuid__c;
    public String loginType;
    public String loginSsoId;
    public String loginSsoIssuerGuid;

    public static final String issuerGuid = eCOA_IntegrationDetails__c.getInstance().VTR5_eCoa_IssuerGuid__c;

    /**
    * @modified by N.Arbatskiy
    * @date: 27-May-20
    * @description: Builds a body for POST method (VT_R5_eCoaCreateSubject)
    */
    public VT_R5_RequestBuilder_CreateSubject(User u) {
        //username and sso details should be sent only if diary tool is ecoa
        if (u.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eDiaryTool__c == 'eCOA') {
            username = u.Id;
            loginType = VT_R4_ConstantsHelper_Integrations.ECOA_LOGIN_TYPE;
            loginSsoId = u.Id;
            loginSsoIssuerGuid = issuerGuid;
        }
        studyGuid = u.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eCOA_Guid__c;
        timezone = u.TimeZoneSidKey;
        ident = u.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c != null
            ? u.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c
            : u.Id;
        siteGuid = u.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_eCOA_Guid__c;
        subjectLocale = VT_R5_eCoaLocaleAdaptor.adaptLocale(u);

        eproRuleSetGuid = u.VTR5_ePRO_GUID__c;
        clinroRuleSetGuid = u.VTR5_ClinRO_GUID__c;
    }

    public String buildRequestBody() {
        return JSON.serialize(this);
    }
}