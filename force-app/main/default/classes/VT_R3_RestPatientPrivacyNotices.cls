/**
 * Created by Alexander Komarov on 11.06.2019.
 */

@RestResource(UrlMapping='/Patient/PrivacyNotices/')
global with sharing class VT_R3_RestPatientPrivacyNotices {
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();

    @HttpGet
    global static String getPrivacyNotices() {
        RestResponse response = RestContext.response;

        PrivacyNotices pn;
        try {
            User currentUser = [SELECT Id, Contact.Account.Candidate_Patient__r.Study__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
            Knowledge__kav article;
            String userLang = UserInfo.getLanguage();

            for (Knowledge__kav item : [
                    SELECT VTD1_Content__c, Language, Title,Summary
                    FROM Knowledge__kav
                    WHERE IsLatestVersion = TRUE
                    AND PublishStatus = 'Online'
                    AND VTD2_Type__c = 'Privacy_Notice'
                    AND VTD2_Study__c = :currentUser.Contact.Account.Candidate_Patient__r.Study__c
            ]) {
                if (item.Language == userLang) {
                    article = item;
                    break;
                }
            }
            pn = new PrivacyNotices(article.Id, article.Summary, article.Title, article.VTD1_Content__c);


        } catch (Exception e) {
            System.debug('Exception *** ' + e);
            response.statusCode = 500;
            cr.buildResponse('Something went wrong.');
            return JSON.serialize(cr, true);
        }
        cr.buildResponse(pn);
        return JSON.serialize(cr, true);
    }

    private class PrivacyNotices {
        public String id;
        public String summary;
        public String title;
        public String content;

        public PrivacyNotices(String i, String s, String t, String c) {
            this.id = i;
            this.summary = s;
            this.title = t;
            this.content = c;

        }
    }
}