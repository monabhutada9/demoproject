@IsTest
public with sharing class VT_D2_StudyTeamMemberTriggerHandlerTest {
    @TestSetup
    static void setupMethod() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        study.Name = 'TestStudy';
        update study;
    }

    @IsTest
    static void deleteUserFromGroupsOnUpdateAndDeleteTest() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id, Name FROM HealthCloudGA__CarePlanTemplate__c WHERE Name = 'TestStudy'];

        User user1 = (User) new DomainObjects.User_t().persist();
        DomainObjects.StudyTeamMember_t stmPI = new DomainObjects.StudyTeamMember_t()
                .setStudyId(study.Id)
                .setUserId(user1.Id)
                .setRecordTypeByName('PI');
        stmPI.persist();
        Study_Team_Member__c stm = [SELECT Id, Name, User__c FROM Study_Team_Member__c WHERE User__c = :user1.Id];

        User user2 = (User) new DomainObjects.User_t().persist();
        stm.User__c = user2.Id;

        update stm;
    }

    @IsTest
    static void deleteUserFromGroupsOnDeleteTest() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id, Name FROM HealthCloudGA__CarePlanTemplate__c WHERE Name = 'TestStudy'];

        User user = (User) new DomainObjects.User_t().persist();

        CollaborationGroup collaborationGroup = new CollaborationGroup(Name = 'Test PG Chatter Group', CollaborationType = 'Private');
        insert collaborationGroup;

        study.VTD1_PG_Study_Group_Id__c = collaborationGroup.Id;
        update study;

        CollaborationGroupMember groupMember = new CollaborationGroupMember();
        groupMember.MemberId = user.Id;
        groupMember.CollaborationGroupId = collaborationGroup.Id;
        insert groupMember;

        DomainObjects.StudyTeamMember_t stmPG = new DomainObjects.StudyTeamMember_t()
                .setStudyId(study.Id)
                .setUserId(user.Id)
                .setRecordTypeByName('PG');
        stmPG.persist();
        Study_Team_Member__c stm = [SELECT Id, Name, User__c FROM Study_Team_Member__c WHERE User__c = :user.Id];
        VT_R3_GlobalSharing.disableForTest = true;

        delete stm;
    }
}