/**
 * @author: Alexander Komarov
 * @date: 20.10.2020
 * @description:
 */

@IsTest
public with sharing class VT_R5_MobilePatientMessagingTest {

    @TestSetup
    static void setup() {

        User patientUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME)
                .setUsername('VT_R5_MobilePatientMessagingTest@test.com')
                .setVTR3_UnreadFeeds('something;')
                .persist();

        Case pcf = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName(VT_R4_ConstantsHelper_AccountContactCase.RT_PCF)
                .setSubject('PCF_SUBJECT')
                .persist();

        Case pcf2 = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName(VT_R4_ConstantsHelper_AccountContactCase.RT_PCF)
                .setSubject('PCF_SUBJECT2')
                .persist();

        VTD1_PCF_Chat_Member__c pfcUserChatMember = (VTD1_PCF_Chat_Member__c) new DomainObjects.VTD1_PCF_Chat_Member_t()
                .setPCFId(pcf.Id)
                .setUserId(patientUser.Id)
                .persist();

        VTD1_PCF_Chat_Member__c pfcUserChatMember2 = (VTD1_PCF_Chat_Member__c) new DomainObjects.VTD1_PCF_Chat_Member_t()
                .setPCFId(pcf2.Id)
                .setUserId(patientUser.Id)
                .persist();

        VTD1_Chat__c chat = (VTD1_Chat__c) new DomainObjects.VTD1_Chat_t().persist();

        VTD1_Chat__c chat2 = (VTD1_Chat__c) new DomainObjects.VTD1_Chat_t().persist();

        VTD1_Chat_Member__c chatMember = (VTD1_Chat_Member__c) new DomainObjects.VTD1_Chat_Member_t()
                .setChat(chat.Id)
                .setUser(pfcUserChatMember.VTD1_User_Id__c)
                .persist();

        VTD1_Chat_Member__c chatMember2 = (VTD1_Chat_Member__c) new DomainObjects.VTD1_Chat_Member_t()
                .setChat(chat2.Id)
                .setUser(pfcUserChatMember.VTD1_User_Id__c)
                .persist();

        FeedItem caseFeedItem = (FeedItem) new DomainObjects.FeedItem_t()
                .setBodyFeedItem('Test')
                .setParentId(pfcUserChatMember.VTD1_PCF__c)
                .persist();

        FeedItem caseFeedItem2 = (FeedItem) new DomainObjects.FeedItem_t()
                .setBodyFeedItem('Test')
                .setParentId(pfcUserChatMember2.VTD1_PCF__c)
                .persist();

        FeedItem chatFeedItem = (FeedItem) new DomainObjects.FeedItem_t()
                .setBodyFeedItem('Test')
                .setParentId(chat.Id)
                .persist();

        FeedItem chatFeedItem2 = (FeedItem) new DomainObjects.FeedItem_t()
                .setBodyFeedItem('Test')
                .setParentId(chat2.Id)
                .persist();

        new DomainObjects.FeedComment_t()
                .setCommentBody('Test')
                .setFeedItemId(caseFeedItem.Id);

        new DomainObjects.FeedComment_t()
                .setCommentBody('Test')
                .setFeedItemId(chatFeedItem.Id)
                .persist();

        insert new List<VTD2_Feed_Post_on_PCF__c>{
                new VTD2_Feed_Post_on_PCF__c(VTD2_PCF_Chat_Member__c = pfcUserChatMember.Id, VTD2_Feed_Element__c = caseFeedItem.Id),
                new VTD2_Feed_Post_on_PCF__c(VTD2_PCF_Chat_Member__c = pfcUserChatMember2.Id, VTD2_Feed_Element__c = caseFeedItem2.Id)
        };
        insert new List<VTD2_Feed_Post_on_Chat__c>{
                new VTD2_Feed_Post_on_Chat__c(VTD2_Chat_Member__c = chatMember.Id, VTD2_Feed_Element__c = chatFeedItem.Id),
                new VTD2_Feed_Post_on_Chat__c(VTD2_Chat_Member__c = chatMember2.Id, VTD2_Feed_Element__c = chatFeedItem2.Id)
        };

    }

    @IsTest
    public static void firstTest() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_MobilePatientMessagingTest@test.com' LIMIT 1];
        System.runAs(toRun) {
            setRestContext('/patient/v1/messages', 'GET');
            Test.startTest();
            VT_R5_MobileRestRouter.doGET();
            Test.stopTest();
        }
    }
    @IsTest
    public static void secondTest() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_MobilePatientMessagingTest@test.com' LIMIT 1];
        System.runAs(toRun) {
            setRestContext('/patient/v1/announcements', 'GET');
            Test.startTest();
            VT_R5_MobileRestRouter.doGET();
            Test.stopTest();
        }
    }
    @IsTest
    public static void thirdTest() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_MobilePatientMessagingTest@test.com' LIMIT 1];
        System.runAs(toRun) {
            setRestContext('/patient/v0/messages', 'GET');
            Test.startTest();
            VT_R5_MobileRestRouter.doGET();
            Test.stopTest();
        }
    }
    @IsTest
    public static void fourthTest() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_MobilePatientMessagingTest@test.com' LIMIT 1];
        List<VTD2_Feed_Post_on_Chat__c> feedPostOnChats = [SELECT Id, VTD2_Feed_Element__c FROM VTD2_Feed_Post_on_Chat__c];
        System.runAs(toRun) {
            setRestContext('/patient/v1/messages/'+feedPostOnChats[0].VTD2_Feed_Element__c, 'GET');
            Test.startTest();
            VT_R5_MobileRestRouter.doGET();
            Test.stopTest();
        }
    }
    @IsTest
    public static void fifthTest() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_MobilePatientMessagingTest@test.com' LIMIT 1];
        List<VTD2_Feed_Post_on_Chat__c> feedPostOnChats = [SELECT Id, VTD2_Feed_Element__c FROM VTD2_Feed_Post_on_Chat__c];
        System.runAs(toRun) {
            setRestContext('/patient/v1/messages/'+feedPostOnChats[0].VTD2_Feed_Element__c, 'POST');
            RestContext.request.requestBody = Blob.valueOf('{"message":"message","fileName":"file.txt","based64FileData":"=dsadakn/c=="}');
            Test.startTest();
            VT_R5_MobileRestRouter.doPOST();
            Test.stopTest();
        }
    }
    @IsTest
    public static void sixthTest() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_MobilePatientMessagingTest@test.com' LIMIT 1];
        List<VTD2_Feed_Post_on_PCF__c> feedPostOnPCFS = [SELECT Id, VTD2_Feed_Element__c FROM VTD2_Feed_Post_on_PCF__c];
        System.runAs(toRun) {
            setRestContext('/patient/v1/messages/'+feedPostOnPCFS[0].VTD2_Feed_Element__c, 'POST');
            RestContext.request.requestBody = Blob.valueOf('{"message":"message","fileName":"file.txt","based64FileData":"=dsadakn/c=="}');
            Test.startTest();
            VT_R5_MobileRestRouter.doPOST();
            Test.stopTest();
        }
    }
    @IsTest
    public static void seventhTest() {
        User toRun = [SELECT Id, VTR3_UnreadFeeds__c FROM User WHERE Username = 'VT_R5_MobilePatientMessagingTest@test.com' LIMIT 1];
        List<VTD2_Feed_Post_on_PCF__c> feedPostOnPCFS = [SELECT Id, VTD2_Feed_Element__c FROM VTD2_Feed_Post_on_PCF__c];
        List<VTD2_Feed_Post_on_Chat__c> feedPostOnChats = [SELECT Id, VTD2_Feed_Element__c FROM VTD2_Feed_Post_on_Chat__c];

        System.runAs(toRun) {
            setRestContext('/patient/v1/messages/'+feedPostOnChats[0].VTD2_Feed_Element__c, 'GET');
            Test.startTest();
            String newUnreadFeeds = toRun.VTR3_UnreadFeeds__c + feedPostOnChats[0].Id + ';' + feedPostOnPCFS[0].Id + ';';
            toRun.VTR3_UnreadFeeds__c = newUnreadFeeds;
            update toRun;
            VT_R5_MobileRestRouter.doGET();
            Test.stopTest();
        }
    }

    @IsTest
    public static void eighthTest() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_MobilePatientMessagingTest@test.com' LIMIT 1];
        List<VTD2_Feed_Post_on_PCF__c> feedPostOnPCFS = [SELECT Id, VTD2_Feed_Element__c FROM VTD2_Feed_Post_on_PCF__c];
        System.runAs(toRun) {
            setRestContext('/patient/v1/messages/'+feedPostOnPCFS[0].VTD2_Feed_Element__c, 'POST');
            RestContext.request.requestBody = Blob.valueOf('{"message":"message"}');
            Test.startTest();
            VT_R5_MobileRestRouter.doPOST();
            Test.stopTest();
        }
    }
    public static void setRestContext(String URI, String method) {
        RestContext.request = new RestRequest();
        RestContext.response = new RestResponse();
        RestContext.request.requestURI = URI;
        RestContext.request.httpMethod = method;
    }
}