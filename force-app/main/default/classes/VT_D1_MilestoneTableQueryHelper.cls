public with sharing class VT_D1_MilestoneTableQueryHelper {

    Id parentId;
    Map<String, VTD1_Milestone_Component_Config__mdt> mdtMap;
    Schema.sObjectType milestoneType;
    String parentFieldName;
    Map<String, sObject> milestonesByDescription = new Map<String, sObject>();
    List<sObject> sortedMilestones = new List<sObject>();

    public List<sObject> getMilestones(Id parentId, Map<String, VTD1_Milestone_Component_Config__mdt> mdtMap) {
        this.parentId = parentId;
        this.mdtMap = mdtMap;

        getMilestoneType();
        getParentFieldName();

        getMilestonesByDescription();
        addMissingMilestonesFromMetadata();
        getSortedMilestones();

        return this.sortedMilestones;
    }

    private void getMilestoneType() {
        this.milestoneType = this.parentId.getSObjectType() == HealthCloudGA__CarePlanTemplate__c.getSObjectType() ?
            VTD1_Study_Milestone__c.getSObjectType() : VTD1_Site_Milestone__c.getSObjectType();
    }

    private void getParentFieldName() {
        this.parentFieldName = this.milestoneType == VTD1_Study_Milestone__c.getSObjectType() ?
            VT_R4_ConstantsHelper_AccountContactCase.STUDY_MILESTONE_PARENT_FIELD : VT_R4_ConstantsHelper_AccountContactCase.SITE_MILESTONE_PARENT_FIELD;
    }

    private void getMilestonesByDescription() {
        Boolean isSysAdmin = [SELECT Name FROM Profile WHERE Id = :userInfo.getProfileId()].Name.equals('System Administrator');
        Set<String> descriptions = this.mdtMap.keySet();
        Id pId = this.parentId;

        String query = 'SELECT Id, VTD1_Actual_Date__c, VTD1_Milestone_Description__c, VTD1_Projected_Date__c,'
            + (milestoneType == VTD1_Study_Milestone__c.getSObjectType() ? 'VTD1_Contracted_Date__c,' : '')
            + this.parentFieldName
            + ' FROM ' + milestoneType.getDescribe().getName()
            + ' WHERE ' + this.parentFieldName + ' = :pId'
            + (!isSysAdmin ? ' AND VTD1_Milestone_Description__c IN :descriptions' : '')
        ;

        for (sObject item : Database.query(query)) {
            this.milestonesByDescription.put((String)item.get(VT_R4_ConstantsHelper_AccountContactCase.MILESTONE_DESCRIPTION_FIELD_NAME), item);
        }
    }

    private void addMissingMilestonesFromMetadata() {
        for (String milestoneDesc : this.mdtMap.keySet()) {
            if (! this.milestonesByDescription.containsKey(milestoneDesc)) {
                sObject newMilestone = this.milestoneType.newSObject();
                newMilestone.put(VT_R4_ConstantsHelper_AccountContactCase.MILESTONE_DESCRIPTION_FIELD_NAME, milestoneDesc);
                newMilestone.put(this.parentFieldName, this.parentId);
                this.milestonesByDescription.put(milestoneDesc, newMilestone);
            }
        }
    }

    private void getSortedMilestones() {
        List<String> sortedDescriptions = new List<String>(this.milestonesByDescription.keySet());
        sortedDescriptions.sort();

        for (String description : sortedDescriptions) {
            this.sortedMilestones.add(this.milestonesByDescription.get(description));
        }
    }
}