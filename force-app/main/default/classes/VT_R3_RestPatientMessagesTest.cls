@isTest
public with sharing class VT_R3_RestPatientMessagesTest {
	
	public static void getPostsTest() {
		User user = [SELECT Id FROM User WHERE Profile.Name = 'Patient' AND IsActive = true AND Name = 'CP1 CP1L' LIMIT 1];
		Case pcf = new Case(RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VTD1_PCF').getRecordTypeId());
		insert pcf;
		VTD1_PCF_Chat_Member__c pcfMember = new VTD1_PCF_Chat_Member__c(
				VTD1_PCF__c = pcf.Id,
				VTD1_User_Id__c = user.Id
		);
		insert pcfMember;
		VTD1_Chat__c chat = new VTD1_Chat__c();
		insert chat;
		VTD1_Chat_Member__c chatMember = new VTD1_Chat_Member__c(
				VTD1_Chat__c = chat.Id,
				VTD1_User__c = pcfMember.VTD1_User_Id__c
		);
		insert chatMember;

		FeedItem caseFeedItem = new FeedItem(
				Body = 'Test',
				ParentId = pcfMember.VTD1_PCF__c
		);
		FeedItem chatFeedItem = new FeedItem(
				Body = 'Test',
				ParentId = chat.Id
		);
		insert new List<FeedItem>{caseFeedItem, chatFeedItem};


		FeedComment caseFeedComment = new FeedComment(
				CommentBody = 'Test',
				FeedItemId = caseFeedItem.Id
		);
		FeedComment chatFeedComment = new FeedComment(
				CommentBody = 'Test',
				FeedItemId = chatFeedItem.Id
		);
		insert new List<FeedComment>{caseFeedComment, chatFeedComment};

		VTD2_Feed_Post_on_PCF__c feedPostOnPCF = new VTD2_Feed_Post_on_PCF__c(
				VTD2_Feed_Element__c = caseFeedItem.Id,
				VTD2_PCF_Chat_Member__c = pcfMember.Id
		);
		insert feedPostOnPCF;

//		ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
//		List<ConnectApi.FeedItem> testItemList = new List<ConnectApi.FeedItem>();
//		testItemList.add(new ConnectApi.FeedItem());
//		testItemList.add(new ConnectApi.FeedItem());
//		testPage.elements = testItemList;
//
//		// Set the test data
//		ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(null,
//				null, 'dummyId', testPage);


		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();
		request.requestURI = '/Patient/Messages';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response  = response;
		String responseString = VT_R3_RestPatientMessages.getPosts();
		System.debug(responseString);
		System.runAs(user){
			responseString = VT_R3_RestPatientMessages.getPosts();
//			ConnectApi.BatchResult[] feedElementsBatchResult = ConnectApi.ChatterFeeds.getFeedElementBatch(null, new List<String>(feedPostsMap.keySet()));

		}
	}
}