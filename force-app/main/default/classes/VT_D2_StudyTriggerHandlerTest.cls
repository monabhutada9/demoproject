/**
 * Created by User on 19/06/06.
 */


    @IsTest
private with sharing class VT_D2_StudyTriggerHandlerTest {
    @TestSetup
    static void setup() {
        DomainObjects.User_t gPL = new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PL_PROFILE_NAME);
        DomainObjects.User_t gVTSL = new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.VTSL_PROFILE_NAME);

        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setName('Study Name')


                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'))
                .setVTD2_Certification_form_Medical_Records('Title 1')
                .addVTD1_Project_Lead(gPL)
                .addVTD1_Virtual_Trial_Study_Lead(gVTSL);
        new DomainObjects.Task_t().addStudy(study).setStatus('Open').setCategory('GPL').addUser(gPL).persist();
        new DomainObjects.Task_t().addStudy(study).setStatus('Open').setCategory('GVTSL').addUser(gVTSL).persist();
    }

    @IsTest
    static void updateGlobalTaskOwnerTest() {
        HealthCloudGA__CarePlanTemplate__c study = [
                SELECT Id, VTD1_Project_Lead__c, VTD1_Virtual_Trial_Study_Lead__c
                FROM HealthCloudGA__CarePlanTemplate__c
                LIMIT 1
        ];
        Id oldGPL = study.VTD1_Project_Lead__c;
        Id oldGVTSL = study.VTD1_Virtual_Trial_Study_Lead__c;

        User newGPL = (User) new DomainObjects.User_t().persist();
        User newGVTSL = (User) new DomainObjects.User_t().persist();

        study.VTD1_Project_Lead__c = newGPL.Id;
        study.VTD1_Virtual_Trial_Study_Lead__c = newGVTSL.Id;
        update study;

        List<Task> changedTaskList = [
                SELECT Id, Category__c, OwnerId
                FROM Task
                WHERE HealthCloudGA__CarePlanTemplate__c = :study.Id
        ];

        for (Task task : changedTaskList) {
            if (task.Category__c == 'GPL') {
                System.assertNotEquals(study.VTD1_Project_Lead__c, oldGPL);
            }
            if (task.Category__c == 'GVTSL') {
                System.assertNotEquals(study.VTD1_Virtual_Trial_Study_Lead__c, oldGVTSL);
            }
        }
    }

    @IsTest
    static void updateSecondAddressChangeSiteFail() {
        HealthCloudGA__CarePlanTemplate__c study = [
                SELECT Id, VTD1_Project_Lead__c, VTD1_Virtual_Trial_Study_Lead__c
                FROM HealthCloudGA__CarePlanTemplate__c
                LIMIT 1
        ];
        DomainObjects.ContentVersion_t contVers = new DomainObjects.ContentVersion_t()
                .setTitle('Title');
        contVers.persist();
        ContentVersion contentVersion1 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contVers.Id];
        DomainObjects.ContentDocumentLink_t conDoc = new DomainObjects.ContentDocumentLink_t()
                .withLinkedEntityId(study.Id)
                .withContentDocumentId(contentVersion1.ContentDocumentId)
                .withVisibility('InternalUsers');
        conDoc.persist();
        
        Test.startTest();
        study.VTD2_Certification_form_Medical_Records__c = 'Title';
        update new List<HealthCloudGA__CarePlanTemplate__c>{study};
        Test.stopTest();
    }

    

    @isTest
    static void verifyValidateRecueStudyRequiredFields(){
        test.startTest();
                DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setName('StudyName62_2020')


                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'))
              ;
        study.persist();
        List<HealthCloudGA__CarePlanTemplate__c> lstStudies = [select id,VTD1_SCGroupId__c,VTD2_CM_Group_Id__c,VTD1_PG_Study_Group_Id__c,VTR2_REGroupId__c,VTR2_CRA_Group_Id__c,VTD1_Community_Id__c, VTR2_SCR_Group_Id__c,VTD1_PIGroupId__c,VTD1_Patient_Group_Id__c, VTD1_DeliveryTeamGroupId__c,VTD1_ZIP__c,City_for_return__c,State_or_return__c,VTR3_Countryforreturn__c,VTD1_Address_Line_1__c,VT_R4_RescueTelVisitStudy__c,VTD1_Carematix_Bulk_Upload__c,VTD1_Study_Uses_Connected_Device_Kits__c,VTD1_Name__c from HealthCloudGA__CarePlanTemplate__c where VTD1_Name__c = 'StudyName62_2020'];
        VT_D2_StudyTriggerHandler.validateRecueStudyRequiredFields(lstStudies, true);
        test.stopTest();
       
        
    }
        @IsTest
        static void updateStudyParticipant() {
            Test.startTest();
            HealthCloudGA__CarePlanTemplate__c study = [
                    SELECT Id,
                            VTD1_Project_Lead__c,
                            VTD1_Virtual_Trial_Study_Lead__c,
                            VTD1_Remote_CRA__c,
                            CreatedDate
                    FROM HealthCloudGA__CarePlanTemplate__c
                    LIMIT 1
            ];
//            VTR5_UserAudit__c uaForOldPl = [SELECT Id FROM VTR5_UserAudit__c WHERE User__c = :study.VTD1_Project_Lead__c];
//            System.debug('ua: uaForOldPl ' + uaForOldPl);
//            if (uaForOldPl == null) {
//                System.debug('here');
//                VTR5_UserAudit__c uaPl = new VTR5_UserAudit__c(
//                        User__c = study.VTD1_Project_Lead__c
//                );
//                insert uaPl;
//            }
            VTR5_UserAudit__c uaOldPl = [SELECT Id FROM VTR5_UserAudit__c WHERE User__c = :study.VTD1_Project_Lead__c];
            VTR5_SiteStudyAccessAudit__c ssaAuditForOldPl = new VTR5_SiteStudyAccessAudit__c(
                    User_Audit__c = uaOldPl.Id,
                    VTR5_StudyName__c = study.Id,
                    VTR5_StudyAccessStatus__c = true,
                    VTR5_AssignedDateStudy__c = study.CreatedDate

            );
            insert ssaAuditForOldPl;
            DomainObjects.User_t newPl = new DomainObjects.User_t()
                    .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PL_PROFILE_NAME);
            newPl.persist();
            DomainObjects.User_t newCra = new DomainObjects.User_t()
                    .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME);
            newCra.persist();
            System.debug('study: pl ' + study.VTD1_Project_Lead__c);
            User userPl = (User) newPl.toObject();
            User userCra = (User) newCra.toObject();
            System.debug('study: newPl ' + userPl.Id);
            study.VTD1_Project_Lead__c = userPl.Id;
            study.VTD1_Remote_CRA__c = userCra.Id;
            update study;
//            VTR5_UserAudit__c uaNewPl = [SELECT Id FROM VTR5_UserAudit__c WHERE User__c = :userPl.Id];
//            System.debug('ua: uaNewPl ' + uaNewPl);
//            if (uaNewPl == null) {
                System.debug('here');
                VTR5_UserAudit__c uaNewPl1 = new VTR5_UserAudit__c(
                        User__c = userPl.Id
                );
                insert uaNewPl1;
//            }
            System.debug('ua: studyTriggerHandlerTest ' + [SELECT Id FROM VTR5_UserAudit__c WHERE User__c = :userPl.Id]);
            Test.stopTest();
        }
}