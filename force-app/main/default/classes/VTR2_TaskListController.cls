public inherited sharing class VTR2_TaskListController {

    @AuraEnabled
    public static Tasks getTaskInfo(String type) {
        return new TaskManager().getScrOrPITasks(type);
    }

    @AuraEnabled
    public static void reassignTasks(List<Task> tasksToReassign, String siteCoordinatorOrPIId) {
        new TaskManager().reassignTasks(tasksToReassign, siteCoordinatorOrPIId);
    }

    @AuraEnabled
    public static List<SiteCoordinatorOrPISelectOption> getSelectOptions(List<Task> tasks) {
            return new ScrOrPiPicklist().getScrOrPiSelectOptions(tasks);
    }

    //Code to work with tasks
    public without sharing class TaskManager {

        @AuraEnabled
        public String userId;

        public TaskManager() {
            this.userId = UserInfo.getUserId();
        }

        public Tasks getScrOrPITasks(String type) {
            return new Tasks(this.getTasks(type, null));
        }

        public void reassignTasks(List<Task> tasksToReassign, String siteCoordinatorOrPIId) {
            for (Task task : tasksToReassign) {
                task.OwnerId = siteCoordinatorOrPIId;
            }
            update tasksToReassign;
        }

        private Set<String> getTaskCategories() {
            Set<String> result = new Set<String>();
            VTD2_TaskCategoriesByProfile__c settingForProfile = VTD2_TaskCategoriesByProfile__c.getInstance();
            if (settingForProfile == null || String.isBlank(settingForProfile.VTD2_Categories__c)) {
                return result;
            }
            for (String taskCategory : settingForProfile.VTD2_Categories__c.split(';')) {
                result.add(taskCategory.trim());
            }
            return result;
        }

        private List<Task> getTasks(String status, String category) {
            QueryBuilder taskQuery = new QueryBuilder(Task.class)
                    .addFields(new List<String>{
                            Task.OwnerId + '',
                            Task.Subject + '',
                            Task.VTD2_Subject_Parameters__c + '',
                            Task.Description + '',
                            Task.VTD2_Description_Parameters__c + '',
                            Task.Status + '',
                            Task.Priority + '',
                            Task.Category__c + '',
                            Task.ActivityDate + '',
                            Task.WhatId + '',
                            Task.Type + '',
                            Task.VTD1_Type_for_backend_logic__c + '',
                            'Id',
                            'What.Name',
                            Task.HealthCloudGA__CarePlanTemplate__c + '',
                            Task.VTD2_Study_Name__c + '',
                            Task.VTD2_My_Task_List_Redirect__c + '',
                            Task.VTD2_isRequestVisitRedirect__c + '',
                            Task.VTR5_AutocompleteWhenClicked__c + ''
                    })
                    .addOrderDesc(Task.CreatedDate)
                    .setLimit(1000)
                    .addConditions()
                    .add(new QueryBuilder.CompareCondition(Task.Status).eq(status))
                    .add(new QueryBuilder.CompareCondition(Task.OwnerId).eq(this.userId))
                    .endConditions();

            if (String.isNotEmpty(category)) {
                taskQuery.addConditions()
                        .add(new QueryBuilder.CompareCondition(Task.Category__c).eq(category))
                        .setConditionOrder('1 AND 2 AND 3')
                        .endConditions();
            } else {
                taskQuery.addConditions()
                        .setConditionOrder('1 AND 2')
                        .endConditions();
            }
            taskQuery.preview();
            return taskQuery.toList();
        }
    }

    public class Tasks {

        @AuraEnabled
        public Integer count;

        @AuraEnabled
        public List<Task> tasks;

        @AuraEnabled
        public List<TaskCategory> categories;

        public Tasks(List<Task> tasks) {
            this.tasks = tasks;
            this.count = count;
            this.categories = this.calculateCategories();
        }

        public Tasks(List<Task> tasks, List<TaskCategory> categories, Integer count) {
            this.tasks = tasks;
            this.count = count;
            this.categories = categories;
        }

        private List<TaskCategory> calculateCategories() {
            Map<String, TaskCategory> result = new Map<String, TaskCategory>();
            Set<String> taskCategories = this.getTaskCategories();
            if (taskCategories.isEmpty()) {
                return result.values();
            }
            for (String taskCategory : taskCategories) {
                result.put(taskCategory, new TaskCategory(taskCategory));
            }
            for (Task task : this.tasks) {
                if (String.isEmpty(task.Category__c) || !taskCategories.contains(task.Category__c)) {
                    continue;
                }
                result.get(task.Category__c).increment();
            }
            return result.values();
        }

        private Set<String> getTaskCategories() {
            Set<String> result = new Set<String>();
            VTD2_TaskCategoriesByProfile__c settingForProfile = VTD2_TaskCategoriesByProfile__c.getInstance();
            if (settingForProfile == null || String.isBlank(settingForProfile.VTD2_Categories__c)) {
                return result;
            }
            for (String taskCategory : settingForProfile.VTD2_Categories__c.split(';')) {
                result.add(taskCategory.trim());
            }
            return result;
        }
    }

    //Code to work with selecting Site Coordinators or PIs
    public class ScrOrPiPicklist {

        public final String SITE_COORDINATOR_OR_PI = VT_D1_HelperClass.getUserProfileName(UserInfo.getUserId());

        public ScrOrPiPicklist() {

        }

        public List<SiteCoordinatorOrPISelectOption> getScrOrPiSelectOptions(List<Task> tasks) {
            ScrOrPiSelectionPattern selectionPattern = new ScrOrPiSelectionPattern(tasks);
            selectionPattern.addSelector(new ScrOrPiGlobalSelector(this.SITE_COORDINATOR_OR_PI));
            return selectionPattern.getScrOrPiSelectOptions();
        }

    }

    public class TaskCategory {

        @AuraEnabled
        public String name;

        @AuraEnabled
        public Integer count;

        public TaskCategory(String categoryName) {
            this.name = categoryName;
            this.count = 0;
        }

        public TaskCategory(String categoryName, Integer count) {
            this.name = categoryName;
            this.count = count;
        }

        public void increment() {
            this.count++;
        }

    }

    interface Selector {

        List<User> getScrs(List<Task> tasks);

        List<User> getPIs(List<Task> tasks);
    }

    public without sharing class ScrOrPiGlobalSelector implements Selector {

        public final String siteCoordinatorOrPI;

        public ScrOrPiGlobalSelector(String siteCoordinatorOrPI) {
            this.siteCoordinatorOrPI = siteCoordinatorOrPI;
        }

        public List<User> getScrs(List<Task> tasks) {
            Set<Id> associatedPiIds = new Set<Id>();
            for (Study_Site_Team_Member__c stm : getSstmByStmByUser()) {
                if (stm.VTR2_Associated_PI__c != null) {
                    associatedPiIds.add(stm.VTR2_Associated_PI__c);
                }
            }

            Set<Id> siteCoordinatorsIds = new Set<Id>();
            for (Study_Site_Team_Member__c studySiteTeamMember : getSstmByStm(associatedPiIds)) {
                siteCoordinatorsIds.add(studySiteTeamMember.VTR2_Associated_SCr__r.User__c);
            }
            if (siteCoordinatorsIds.contains(UserInfo.getUserId())) {
                siteCoordinatorsIds.remove(UserInfo.getUserId());
            }
            return getUsersByProfileAndIds(siteCoordinatorsIds, VT_D1_HelperClass.getProfileIdByName(siteCoordinatorOrPI));
        }

        public List<User> getPIs(List<Task> tasks){
            Set<Id> primaryPIs = new Set<Id>();
            primaryPIs.add(UserInfo.getUserId());

            for(Study_Team_Member__c studyTeamMember : getStmByPi()){
                primaryPIs.add(studyTeamMember.Study_Team_Member__r.User__c);
            }

            for (Study_Site_Team_Member__c studySiteTeamMember : getSstmByPi()){
                primaryPIs.add(studySiteTeamMember.VTR2_Associated_PI3__c);
            }

            Set<Id> backupPIs = new Set<Id>();
            for (Study_Team_Member__c studyTeamMember : getStmByPrimaryPIs(primaryPIs)){
                backupPIs.add(studyTeamMember.User__c);
            }
            Set<Id> subPIs = new Set<Id>();
            for (Study_Site_Team_Member__c studySiteTeamMember : getSstmByPrimaryPIs(primaryPIs)){
                subPIs.add(studySiteTeamMember.VTR2_Associated_SubI__c);
            }

            primaryPIs.addAll(backupPIs);
            primaryPIs.addAll(subPIs);

            if (primaryPIs.contains(UserInfo.getUserId())) {
                primaryPIs.remove(UserInfo.getUserId());
            }
            return getUsersByProfileAndIds(primaryPIs, VT_D1_HelperClass.getProfileIdByName(siteCoordinatorOrPI));
        }

        private List<Study_Team_Member__c> getStmByPi(){
            return [
                    SELECT
                            Study_Team_Member__r.User__c
                    FROM Study_Team_Member__c
                    WHERE User__c = :UserInfo.getUserId()
                    AND VTD1_Backup_PI__c = true
            ];
        }

        private List<Study_Site_Team_Member__c> getSstmByPi(){
            return [
                    SELECT
                            VTR2_Associated_PI3__c
                    FROM Study_Site_Team_Member__c
                    WHERE VTR2_Associated_SubI__c = :UserInfo.getUserId()

            ];
        }

        private List<Study_Team_Member__c> getStmByPrimaryPIs(Set<Id> primaryPIs){
            return [
                    SELECT User__c
                    FROM Study_Team_Member__c
                    WHERE Study_Team_Member__R.User__c IN :primaryPIs
            ];
        }

        private List<Study_Site_Team_Member__c> getSstmByPrimaryPIs(Set<Id> primaryPIs){
            return [
                    SELECT VTR2_Associated_SubI__c
                    FROM Study_Site_Team_Member__c
                    WHERE VTR2_Associated_PI3__c IN :primaryPIs
            ];
        }

        private List<Study_Site_Team_Member__c> getSstmByStmByUser() {
            return [
                    SELECT
                            VTR2_Associated_PI__c
                    FROM Study_Site_Team_Member__c
                    WHERE VTR2_Associated_SCr__c IN (SELECT Id FROM Study_Team_Member__c WHERE User__c = :UserInfo.getUserId())
            ];
        }

        private List<Study_Site_Team_Member__c> getSstmByStm(Set<Id> associatedPiIds) {
            return [
                    SELECT
                            VTR2_Associated_SCr__r.User__c
                    FROM Study_Site_Team_Member__c
                    WHERE VTR2_Associated_PI__c IN :associatedPiIds
            ];
        }

        private List<User> getUsersByProfileAndIds(Set<Id> siteCoordinatorsIds, String profile) {
            return [
                    SELECT Id
                            , FullPhotoUrl
                            , Name
                    FROM User
                    WHERE ProfileId = :profile
                    AND Id IN :siteCoordinatorsIds
            ];
        }
    }

    public class ScrOrPiSelectionPattern {

        private final List<Task> tasks;
        private List<Selector> selectors;

        public ScrOrPiSelectionPattern(List<Task> tasks) {
            this.tasks = tasks;
            this.selectors = new List<Selector>();
        }

        public void addSelector(Selector selector) {
            this.selectors.add(selector);
        }

        public List<SiteCoordinatorOrPISelectOption> getScrOrPiSelectOptions() {
            for (Selector selector : this.selectors) {
                if (VT_D1_HelperClass.getUserProfileName(UserInfo.getUserId()) == 'Site Coordinator') {
                    return this.convertScrsOrPIsToSelectOptions(selector.getScrs(tasks));
                } else if (VT_D1_HelperClass.getUserProfileName(UserInfo.getUserId()) == 'Primary Investigator'){
                    return this.convertScrsOrPIsToSelectOptions(selector.getPIs(tasks));
                }
            }
            return new List<SiteCoordinatorOrPISelectOption>();
        }

        private List<SiteCoordinatorOrPISelectOption> convertScrsOrPIsToSelectOptions(List<User> Users) {
            final List<SiteCoordinatorOrPISelectOption> result = new List<SiteCoordinatorOrPISelectOption>();
            for (User User : Users) {
                result.add(new SiteCoordinatorOrPISelectOption(User));
            }
            return result;
        }
    }

    public class SiteCoordinatorOrPISelectOption {

        @AuraEnabled
        public Id key;

        @AuraEnabled
        public String avatar;

        @AuraEnabled
        public String label;

        public SiteCoordinatorOrPISelectOption(User User) {
            this.key = User.Id;
            this.avatar = User.FullPhotoUrl;
            this.label = User.Name;
        }

    }
}