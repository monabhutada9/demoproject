/**
 * @author: N.Arbatskiy
 * @date: 10-August-20
 * @description: Custom validation on PCF
 * @condition: If VTR5_Stop_eDiary_Schedule__c set to true and you'll try to set it to false
 * there will be an error.
 **/

public with sharing class VT_R5_ValidationPcfScheduleStopped {
    public static void checkForStoppedPCFs(List<Case> pcfCases, Map<Id, Case> oldMap) {
        for (Case pcf : pcfCases) {
            if (oldMap.get(pcf.Id).VTR5_Stop_eDiary_Schedule__c == true &&
                    pcf.VTR5_Stop_eDiary_Schedule__c == false) {
                pcf.VTR5_Stop_eDiary_Schedule__c.addError('eDiary schedule has already been stopped for Daily SIC ' +
                        'and Oxygen Level and Heart Rate and cannot be selected again.');
            }
        }
    }
}