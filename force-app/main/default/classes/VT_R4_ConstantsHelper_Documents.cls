public with sharing class VT_R4_ConstantsHelper_Documents {
    public static final String SOBJECT_DOC_CONTAINER = 'VTD1_Document__c';

    public static final String REGULATORY_DOCUMENT_TYPE_SOURCE_NOTE_OF_TRANSFER = 'Source Note of Transfer';
    public static final String REGULATORY_DOCUMENT_TYPE_TARGET_NOTE_OF_TRANSFER = 'Target Note of Transfer';
    public static final String REGULATORY_DOCUMENT_TYPE_DOA_LOG = 'DOA Log';
    public static final String REGULATORY_DOCUMENT_TYPE_NOTE_TO_FILE_STEP_DOWN = 'Note to File Step Down';
    public static final String REGULATORY_DOCUMENT_TYPE_SUBJECT_SCREENING_LOG = 'Subject Screening Log';
    public static final String REGULATORY_DOCUMENT_TYPE_SUBJECT_IDENTIFICATION_LOG = 'Subject Identification Log';
    public static final String REGULATORY_DOCUMENT_TYPE_1572 = '1572 IRB Acknowledgement Letter';
    public static final String REGULATORY_DOCUMENT_TYPE_ACKNOWLEDGEMENT_LETTER = 'Acknowledgement Letter';
    public static final String REGULATORY_DOCUMENT_TYPE_FOLLOW_UP_LETTER = 'Follow-Up Letter';
    public static final String REGULATORY_DOCUMENT_TYPE_PA_SIGNATURE_PAGE = 'PA Signature Page';
    public static final String REGULATORY_DOCUMENT_TYPE_PA_PROTOCOL_AMENDMENT = 'Protocol Amendment';
    public static final String REGULATORY_DOCUMENT_LEVEL_SITE = 'Site';
    public static final String REGULATORY_DOCUMENT_LEVEL_SUBJECT = 'Subject';

    public static final String DOCUMENT_EDPRC_NAME = 'EDPRC';

    public static final String DOCUMENT_APPROVED = 'Approved';
    public static final String DOCUMENT_REJECTED = 'Rejected';


    public static final String DOCUMENT_CERTIFIED = 'Certified';
    public static final String DOCUMENT_PENDING_CERTIFICATION = 'Pending Certification';



    public static final String RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_ARCHIVED = VT_D1_HelperClass.getRTId('VTD1_Document__c','Medical_Record_Archived');
    public static final String RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_REJECTED = VT_D1_HelperClass.getRTId('VTD1_Document__c','Medical_Record_Rejected');
    public static final String RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_RELEASE_FORM = VT_D1_HelperClass.getRTId('VTD1_Document__c','Medical_Record_Release_Form');
    public static final String RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD = VT_D1_HelperClass.getRTId('VTD1_Document__c','VTD1_Medical_Record');
    public static final String RECORD_TYPE_ID_DOCUMENT_OTHER = VT_D1_HelperClass.getRTId('VTD1_Document__c','VTD1_Other');
    public static final String RECORD_TYPE_ID_DOCUMENT_CERTIFICATON = VT_D1_HelperClass.getRTId('VTD1_Document__c','VTD2_Certification');
    public static final String RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE = VT_D1_HelperClass.getRTId('VTD1_Document__c','VTD1_Patient_eligibility_assessment_form');
    public static final String RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSR = VT_D1_HelperClass.getRTId('VTD1_Document__c','Patient_Eligibility_Assessment_Form_Rejected');
    public static final String RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT = VT_D1_HelperClass.getRTId('VTD1_Document__c','VTD1_Regulatory_Document');
    public static final String RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT_LOCKED = VT_D1_HelperClass.getRTId('VTD1_Document__c','VTD1_Regulatory_Document_Locked');
    public static final String RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT_REJECTED = VT_D1_HelperClass.getRTId('VTD1_Document__c','Regulatory_Document_Rejected');
    public static final String RECORD_TYPE_ID_DOCUMENT_PA_SIGNATURE_PAGE = VT_D1_HelperClass.getRTId('VTD1_Document__c','VTD2_PA_Signature_Page');
    public static final String RECORD_TYPE_ID_DOCUMENT_NOTE_OF_TRANSFER = VT_D1_HelperClass.getRTId('VTD1_Document__c','VTR2_Note_of_Transfer');
    public static final String RECORD_TYPE_ID_DOCUMENT_INFORMED_CONSENT_GUIDE = VT_D1_HelperClass.getRTId('VTD1_Document__c','VTR2_Informed_Consent_Guide');
    public static final String RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT = VT_D1_HelperClass.getRTId('VTD1_Document__c','VTR3_VisitDocument');
    public static final String RECORD_TYPE_ID_ORDER_FORM = VT_D1_HelperClass.getRTId('VTD1_Document__c','VTD2_Order_Form');


    public static final String RECORD_TYPE_ID_CONTENT_VERSION_CV = VT_D1_HelperClass.getRTId('ContentVersion', 'Content_Version');


    public static final String DOCUMENT_SITE_RSU_COMPLETE = 'Site RSU Complete';

    public static final String DOCUMENT_CATEGORY_UNCATEGORIZED = 'Uncategorized';

    public static final String DOCUMENT_EAF_STATUS_COMPLETED = 'Completed';

    public static final Set<String> REGULATORY_DOC_TYPES = new Set<String>{
            RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT,
            RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT_LOCKED,
            RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT_REJECTED
    };

}