/**
 * Created by Michael Salamakha on 08.02.2019.
 */

public class VT_D2_EmailWelcomeController {
private final List<User> currentUser;
    private final String firstName;
    private final String studyNickname;
    private final String studyPhoneNumber;

    public VT_D2_EmailWelcomeController(){
        currentUser = [select id, FirstName, Contact.Account.Candidate_Patient__r.Study__r.VTD1_Protocol_Nickname__c,  CountryCode, VTD1_StudyId__c from User order by CreatedDate desc limit 1];
        firstName = currentUser.get(0).FirstName;
        studyNickname = currentUser.get(0).Contact.Account.Candidate_Patient__r.Study__r.VTD1_Protocol_Nickname__c;
    }
    public String getFirstName(){
        return firstName;
    }
    public String getStudyNickname(){
        return studyNickname;
    }
    public String getStudyPhoneNumber(){
        return studyPhoneNumber;
    }
}