/**
 * @author: Alexander Komarov
 * @date: 19.10.2020
 * @description:
 */

@IsTest
public with sharing class VT_R5_MobilePatientStudySuppliesTest {
    public static void firstTest(){
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        System.debug('here!'+JSON.serialize([SELECT Id, (SELECT Id FROM Patient_Kits__r) FROM VTD1_Order__c]));
        System.runAs(toRun) {
            System.debug('here!'+JSON.serialize([SELECT Id, (SELECT Id FROM Patient_Kits__r) FROM VTD1_Order__c]));
            setRestContext('/patient/v1/study-supplies', 'GET');
            Test.startTest();
            VT_R5_MobileRestRouter.doGET();
            Test.stopTest();
        }
    }
    public static void secondTest(){
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        System.runAs(toRun) {
            List<VTD1_Order__c> orders = [SELECT Id FROM VTD1_Order__c];
            setRestContext('/patient/v1/study-supplies/'+orders[0].Id, 'GET');
            Test.startTest();
            VT_R5_MobileRestRouter.doGET();
            Test.stopTest();
        }
    }
    public static void thirdTest(){
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        System.runAs(toRun) {
            List<VTD1_Patient_Kit__c> kits = [SELECT Id FROM VTD1_Patient_Kit__c];
            setRestContext('/patient/v1/study-supplies/'+kits[0].Id, 'GET');
            Test.startTest();
            VT_R5_MobileRestRouter.doGET();
            Test.stopTest();
        }
    }
    public static void fourthTest(){
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        System.runAs(toRun) {
            List<VTD1_Order__c> orders = [SELECT Id FROM VTD1_Order__c WHERE VTD1_Status__c = 'Delivered'];
            setRestContext('/patient/v1/study-supplies/'+orders[0].Id+'/confirm-delivery', 'GET');
            Test.startTest();
            VT_R5_MobileRestRouter.doGET();
            Test.stopTest();
        }
    }
    public static void fifthTest(){
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        System.runAs(toRun) {
            List<VTD1_Order__c> orders = [SELECT Id FROM VTD1_Order__c WHERE VTD1_Status__c = 'Delivered'];
            List<VTD1_Patient_Kit__c> kits = [SELECT Id FROM VTD1_Patient_Kit__c];
            setRestContext('/patient/v1/study-supplies/'+orders[0].Id+'/confirm-delivery', 'POST');
            RestContext.request.requestBody = Blob.valueOf('{"deliveryDate":1602793834000,"comment":"commentttt","updateDelivery":false,"kitData":[{"kitId":"'+kits[0].Id+'","containsAll":true,"damaged":false,"notReceived":false}]}');
            Test.startTest();
            VT_R5_MobileRestRouter.doPOST();
            Test.stopTest();
        }
    }
    public static void sixthTest(){
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        System.runAs(toRun) {
            List<VTD1_Order__c> orders = [SELECT Id FROM VTD1_Order__c];
            List<VTD1_Patient_Kit__c> kits = [SELECT Id FROM VTD1_Patient_Kit__c];
            setRestContext('/patient/v1/study-supplies/'+orders[0].Id+'/confirm-drop', 'POST');
            RestContext.request.requestBody = Blob.valueOf('{"dateTimeUnix":1602793834000}');
            Test.startTest();
            VT_R5_MobileRestRouter.doPOST();
            Test.stopTest();
        }
    }
    public static void seventhTest(){
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        System.runAs(toRun) {
            List<VTD1_Order__c> orders = [SELECT Id FROM VTD1_Order__c];
            List<VTD1_Patient_Kit__c> kits = [SELECT Id FROM VTD1_Patient_Kit__c];
            setRestContext('/patient/v1/study-supplies/'+orders[0].Id+'/request-replacement', 'POST');
            RestContext.request.requestBody = Blob.valueOf('{"kitId":"'+kits[0].Id+'","isIMP":true,"reason":"reason","comment":"comment"}');
            Test.startTest();
            VT_R5_MobileRestRouter.doPOST();
            Test.stopTest();
        }
    }
    public static void eighthTest(){
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        System.runAs(toRun) {
            List<VTD1_Order__c> orders = [SELECT Id FROM VTD1_Order__c];
            setRestContext('/patient/v1/study-supplies/'+orders[0].Id+'/confirm-packaging-materials', 'POST');
            RestContext.request.requestBody = Blob.valueOf('{"needPackagingMaterials":true}');
            Test.startTest();
            VT_R5_MobileRestRouter.doPOST();
            Test.stopTest();
        }
    }
    public static void ninthTest(){
        setRestContext('/patient/v0/study-supplies/', 'GET');
        VT_R5_MobileRestRouter.doPOST();
    }


    public static void setRestContext(String URI, String method) {
        RestContext.request = new RestRequest();
        RestContext.response = new RestResponse();
        RestContext.request.requestURI = URI;
        RestContext.request.httpMethod = method;
    }
}