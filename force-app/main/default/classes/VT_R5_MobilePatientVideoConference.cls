/**
 * Created by Yuliya Yakushenkova on 10/6/2020.
 */

public class VT_R5_MobilePatientVideoConference extends VT_R5_MobileRestResponse implements VT_R5_MobileRestRouter.Routable {

    private transient RequestParams requestParams;

    private static final String TOKEN = 'token';
    private static final String INFO = 'info';
    private static final String SESSION_ID = 'sessionId';

    public List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/videoConference/{action}'
        };
    }

    public void versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        String requestAction = parameters.get('action');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        if (TOKEN.equals(requestAction)) {
                            get_v1_token();
                            return;
                        }
                        if (SESSION_ID.equals(requestAction)) {
                            get_v1_sessionId();
                            return;
                        }
                        if (INFO.equals(requestAction)) {
                            get_v1_info();
                            return;
                        }
                    }
                }
            }
        }
        VT_R5_MobileRestRouter.unsupportedVersionResponse();
        this.addError('unsupported version');
        this.sendResponse();
    }

    private void get_v1_token() {
        try {
            String token;
            if (requestParams.sessionId != null) {
                VT_R5_RemoteCall_VideoConfToken.Token videoConfToken = (VT_R5_RemoteCall_VideoConfToken.Token)
                        new VT_R5_RemoteCall_VideoConfToken(requestParams.sessionId).execute();
                token = videoConfToken.getMessage();
            }
            this.buildResponse(new Token(token));
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private void get_v1_sessionId() {
        try {
            String sessionId;
            if (requestParams.visitId != null) {
                sessionId = [
                        SELECT Id, SessionId__c, VTD1_Actual_Visit__c
                        FROM Video_Conference__c
                        WHERE VTD1_Actual_Visit__c = :requestParams.visitId
                        ORDER BY CreatedDate DESC
                ].SessionId__c;
            }
            this.buildResponse(new Info(sessionId));
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private void get_v1_info() {
        try {
            this.buildResponse(
                    new Info(
                            VideoPanelTreeController.getConferenceMembersByConferenceId(null),
                            VideoPanelTreeController.getChecklists(null)
                    ));
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    public void initService() {
        requestParams = new RequestParams();
        parseBody();
        parseParams();
    }

    private void parseBody() {}

    private void parseParams() {
        if (request.params.containsKey('visitId')) this.requestParams.setVisitId(request.params.get('visitId'));
        if (request.params.containsKey('sessionId')) this.requestParams.setSessionId(request.params.get('sessionId'));
    }

    private class RequestParams {
        private String sessionId;
        private String visitId;

        private void setVisitId(String visitId) {
            this.visitId = visitId;
        }

        private void setSessionId(String sessionId) {
            this.sessionId = sessionId;
        }
    }


    private class Token {
        private String message;

        private Token(String message) {
            this.message = message;
        }
    }

    private class Info {
        private List<Participant> participants;
        private String checklist;
        private String sessionId;

        private Info(List<VTD1_Conference_Member__c> members, String checklist) {
            this.participants = new List<Participant>();
            for (VTD1_Conference_Member__c member : members) {
                this.participants.add(new Participant(
                        member.VTD1_User__r.SmallPhotoUrl,
                        member.Participant_Name__c,
                        member.VTD1_Participant_Role__c
                ));
            }
            this.checklist = checklist;
        }

        private Info(String sessionId) {
            this.sessionId = sessionId;
        }
    }

    private class Participant {
        private String photoUrl;
        private String userName;
        private String profName;

        private Participant(String photoUrl, String userName, String profName) {
            this.photoUrl = photoUrl;
            this.userName = userName;
            this.profName = profName;
        }
    }
}