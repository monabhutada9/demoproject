/**
 * Created by Alexey Mezentsev on 10/29/2020.
 */

public without sharing class VT_R5_GenericScheduledActionsEvent extends VT_R3_AbstractPublishedAction {
    private List<VT_R5_GenericScheduledActionsService.ScheduledActionData> scheduledActions = new List<VT_R5_GenericScheduledActionsService.ScheduledActionData>();

    public VT_R5_GenericScheduledActionsEvent(List<VT_R5_GenericScheduledActionsService.ScheduledActionData> scheduledActions) {
        this.scheduledActions = scheduledActions;
    }

    public override Type getType() {
        return VT_R5_GenericScheduledActionsEvent.class;
    }

    public override void execute() {
        System.enqueueJob(new VT_R5_GenericScheduledActionsService(scheduledActions));
    }
}