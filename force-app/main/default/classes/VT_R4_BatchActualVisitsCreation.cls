/**
 * Created by Alexey Mezentsev on 2/28/2020.
 */

public with sharing class VT_R4_BatchActualVisitsCreation implements Database.Batchable<SObject>, Database.Stateful {
    private static final Map<String, String> PROTOCOL_TO_ACTUAL_VISIT_TYPES = new Map<String, String>{
            VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_LABS => VT_R4_ConstantsHelper_VisitsEvents.RT_ACTUAL_VISIT_LABS,
            VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_STUDY_TEAM => VT_R4_ConstantsHelper_VisitsEvents.RT_ACTUAL_VISIT_STUDY_TEAM,
            VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_HCP => VT_R4_ConstantsHelper_VisitsEvents.RT_ACTUAL_VISIT_HCP
    };
    private static final String DEFAULT_MODALITY = 'At Home';
    private Id cpId;
    private Case cas;
    private String statusVisits = VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_FINISHED;
    private Boolean sendSubject;

    public VT_R4_BatchActualVisitsCreation(Case cas, Id cpId, Boolean sendSubject) {
        this.cas = cas;
        this.cpId = cpId;
        this.sendSubject = sendSubject;
    }

    public Iterable<SObject> start(Database.BatchableContext bc) {
        List<VTD1_ProtocolVisit__c> pVisits = [
                SELECT Id, Name, VTD1_VisitType__c, VTD1_EDC_Name__c, VTD1_PreVisitInstructions__c, VTR2_SH_TelevisitNeeded__c,
                        VTD1_Independent_Rater_Flag__c, VTD1_Patient_Visit_Checklist__c, VTD1_Visit_Checklist__c, VTR2_Modality__c,
                        RecordTypeId, VTD1_VisitNumber__c, VTD1_Protocol_Amendment__c, VTR4_Original_Version__c, VTR4_Remove_from_Protocol__c
                FROM VTD1_ProtocolVisit__c
                WHERE VTD1_Study__c = :this.cas.VTD1_Study__c
                AND VTD1_VisitType__c IN ('Labs', 'Health Care Professional (HCP)', 'Study Team')
        ];
        List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrappedRecords = wrapProtocolVisits(pVisits);
        VT_R4_ProtocolAmendmentVersionHelper paVersionHelper = new VT_R4_ProtocolAmendmentVersionHelper(wrappedRecords);
        pVisits = paVersionHelper.getCorrectVersions(cas.VTD1_Virtual_Site__c, pVisits);
        return pVisits;
    }

    public void execute(Database.BatchableContext bc, List<VTD1_ProtocolVisit__c> pVisits) {
        Map<String, Schema.RecordTypeInfo> recTypeMap = Schema.SObjectType.VTD1_Actual_Visit__c.getRecordTypeInfosByDeveloperName();
        List<VTD1_Actual_Visit__c> aVisits = new List<VTD1_Actual_Visit__c>();
        for (VTD1_ProtocolVisit__c pVisit : pVisits) {
            VTD1_Actual_Visit__c newVisit = new VTD1_Actual_Visit__c(
                    Name = pVisit.VTD1_EDC_Name__c,
                    RecordTypeId = recTypeMap.get(PROTOCOL_TO_ACTUAL_VISIT_TYPES.get(pVisit.VTD1_VisitType__c)).getRecordTypeId(),
                    Sub_Type__c = pVisit.VTD1_VisitType__c == 'Labs' ? this.cas.Preferred_Lab_Visit__c : null,
                    VTD1_Case__c = this.cas.Id,
                    VTD1_Contact__c = this.cas.ContactId,
                    VTD1_Protocol_Visit__c = pVisit.Id,
                    VTD1_Pre_Visit_Instructions__c = pVisit.VTD1_PreVisitInstructions__c,
                    VTR2_Independent_Rater_Flag__c = pVisit.VTD1_Independent_Rater_Flag__c,
                    VTD1_Additional_Patient_Visit_Checklist__c = pVisit.VTD1_Patient_Visit_Checklist__c,
                    VTD1_Additional_Visit_Checklist__c = pVisit.VTD1_Visit_Checklist__c,
                    VTR2_Televisit__c = pVisit.VTR2_SH_TelevisitNeeded__c,
                    VTR2_Modality__c = pVisit.VTR2_Modality__c != null ? pVisit.VTR2_Modality__c : DEFAULT_MODALITY
            );
            aVisits.add(newVisit);
        }

        if (!aVisits.isEmpty()) {
            try {
                insert aVisits;
            } catch (Exception dbE) {
                insert ((SObject) new VTR4_Conversion_Log__c(VTR4_Candidate_Patient__c = cpId,
                        VTR4_Error_Message__c = dbE.getMessage() + ' ' + dbE.getStackTraceString()));
                statusVisits = VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_FAILED;
            }
        }
    }

    public void finish(Database.BatchableContext bc) {
        if (cpId != null) {
            HealthCloudGA__CandidatePatient__c cp = new HealthCloudGA__CandidatePatient__c(
                    Id = cpId,
                    VTR4_StatusVisits__c = statusVisits
            );
            update (SObject) cp;
        }
        if (sendSubject) {
            Database.executeBatch(new VT_D2_Batch_SendSubjectStatus(new Set<Id>{cas.Id}), 1);
        }
    }

    private List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrapProtocolVisits(List<VTD1_ProtocolVisit__c> protocolVisits) {
        List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrappedRecords = new List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper>();
        for (VTD1_ProtocolVisit__c protocol : protocolVisits) {
            wrappedRecords.add(new VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper(
                    protocol, protocol.VTR4_Original_Version__c, protocol.VTD1_Protocol_Amendment__c, protocol.VTR4_Remove_from_Protocol__c
            ));
        }
        return wrappedRecords;
    }

}