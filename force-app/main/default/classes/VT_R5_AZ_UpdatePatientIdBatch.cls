public class VT_R5_AZ_UpdatePatientIdBatch implements Database.Batchable<sObject> {
    
	public static Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('SELECT Id, VTR4_Cross_Reference_Subject_ID__c, VTD1_Subject_ID__c, VTR5_Internal_Subject_Id__c FROM Case WHERE RecordType.Name = \'CarePlan\' AND VTR4_Cross_Reference_Subject_ID__c != NULL');
    }
    
    public void execute(Database.BatchableContext BC, List<Case> scope) {
    	for (Case c : scope) {
    		c.VTR5_Internal_Subject_Id__c = c.VTD1_Subject_ID__c;
    		c.VTD1_Subject_ID__c = c.VTR4_Cross_Reference_Subject_ID__c;
		}
		List<Database.SaveResult> result = Database.update(scope, false);
		System.debug(JSON.serializePretty(result));
    }
    
    public void finish(Database.BatchableContext BC) {
        List<HealthCloudGA__CarePlanTemplate__c> studies = [SELECT Id, VTR2_PatientIDConfigurable__c FROM HealthCloudGA__CarePlanTemplate__c WHERE VTR2_PatientIDConfigurable__c = TRUE];
		for (HealthCloudGA__CarePlanTemplate__c s : studies) {
    		s.VTR5_Requires_External_Subject_Id__c = true;
    		s.VTR5_External_Subject_ID_Input_Type__c = 'Manual';
		}
		List<Database.SaveResult> result = Database.update(studies, false);
		System.debug(JSON.serializePretty(result));
    }
}