/**
 * Created by Leonid Bartenev
 */

@IsTest
private class VT_D1_Test_QueueAction {
    @TestSetup
    private static void setup() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);

        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();

        Case cas = [
                SELECT Id,
                        VTD1_Patient_User__c,
                        VTD1_Primary_PG__c,
                        VTD1_PI_user__c,
                        VTR2_SiteCoordinator__c,
                        VTD1_Study__r.VTD1_Study_Admin__c,
                        VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c,
                        //VTD1_Virtual_Site__r.VTR3_Study_Geography__c,
                        VTD1_Virtual_Site__r.VTR3_Study_Geography__r.VTR5_Primary_VTSL__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
        ];

        User patient = [SELECT Id, Name, ContactId, AccountId, Contact.AccountId FROM User WHERE Id = :cas.VTD1_Patient_User__c];
        User PI = [SELECT Id, ContactId FROM User WHERE Id = :cas.VTD1_PI_user__c];

        //   User CG = [SELECT Id, ContactId, AccountId FROM User WHERE Profile.Name = 'Caregiver' AND Contact.AccountId = :patient.Contact.AccountId AND IsActive = TRUE LIMIT 1];
        User CG = VT_D1_TestUtils.createUserByProfile('Caregiver', 'CaregiverTest');
        Contact cgCon = new Contact(FirstName = CG.FirstName, LastName = 'L', AccountId = patient.Contact.AccountId);
        insert cgCon;
        CG.ContactId = cgCon.Id;
        insert CG;
        CG = [SELECT Id, ContactId, AccountId, Name FROM User WHERE Id = :CG.Id];

        // insert TN-Catalog T562, T563, T564, N541, N542, N544, N545, T606
        List <VTD2_TN_Catalog_Code__c> tnCatalogCodes = new List<VTD2_TN_Catalog_Code__c>();
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'T562',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Task').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Case',
                VTD2_T_Patient__c = 'Case.Id',
                VTD2_T_Care_Plan_Template__c = 'Case.VTD1_Study__c',
                VTD2_T_Related_To_Id__c = 'Case.Id',
                VTD2_T_Task_Record_Type_ID__c = Task.getSObjectType().getDescribe().getRecordTypeInfosByName().get('SimpleTask').getRecordTypeId(),
                VTD2_T_Subject__c = 'test',
                VTD2_T_Status__c = 'Open',
                VTD2_T_Due_Date__c = 'TODAY()',
                VTD2_T_Description__c = 'test #getMessage'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'T563',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Task').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Case',
                VTD2_T_Patient__c = 'Case.Id',
                VTD2_T_Care_Plan_Template__c = 'Case.VTD1_Study__c',
                VTD2_T_Related_To_Id__c = 'Case.Id',
                VTD2_T_Task_Record_Type_ID__c = Task.getSObjectType().getDescribe().getRecordTypeInfosByName().get('SimpleTask').getRecordTypeId(),
                VTD2_T_Subject__c = 'test',
                VTD2_T_Status__c = 'Open',
                VTD2_T_Due_Date__c = 'TODAY()',
                VTD2_T_Description__c = 'test #getMessage'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'T564',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Task').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Case',
                VTD2_T_Patient__c = 'Case.Id',
                VTD2_T_Care_Plan_Template__c = 'Case.VTD1_Study__c',
                VTD2_T_Related_To_Id__c = 'Case.Id',
                VTD2_T_Task_Record_Type_ID__c = Task.getSObjectType().getDescribe().getRecordTypeInfosByName().get('SimpleTask').getRecordTypeId(),
                VTD2_T_Subject__c = 'test',
                VTD2_T_Status__c = 'Open',
                VTD2_T_Due_Date__c = 'TODAY()',
                VTD2_T_Description__c = 'test #getMessage'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'N541',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('CustomNotification').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Case',
                VTD2_T_Has_direct_link__c = true,
                VTD2_T_Link_to_related_event_or_object__c = 'Case.Id',
                VTR4_T_Number_of_unread_messages__c = 1,
                VTD2_T_Type__c = 'General',
                VTD2_T_Title__c = 'test',
                VTD2_T_Message__c = 'test'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'N544',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Notification').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Case',
                VTD2_T_Has_direct_link__c = true,
                VTD2_T_Link_to_related_event_or_object__c = '"patient-menu?caseId=" & Case.Id',
                VTR4_T_Number_of_unread_messages__c = 1,
                VTD2_T_Type__c = 'General',
                VTD2_T_Title__c = 'test',
                VTD2_T_Message__c = 'test'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'N542',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('CustomNotification').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Case',
                VTD2_T_Has_direct_link__c = true,
                VTD2_T_Link_to_related_event_or_object__c = 'Case.Id',
                VTR4_T_Number_of_unread_messages__c = 1,
                VTD2_T_Type__c = 'General',
                VTD2_T_Title__c = 'test',
                VTD2_T_Message__c = 'test'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'N545',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Notification').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Case',
                VTD2_T_Has_direct_link__c = true,
                VTD2_T_Link_to_related_event_or_object__c = '"patient-menu?caseId=" & Case.Id',
                VTR4_T_Number_of_unread_messages__c = 1,
                VTD2_T_Type__c = 'General',
                VTD2_T_Title__c = 'test',
                VTD2_T_Message__c = 'test'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'T606',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Task').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Case',
                VTD2_T_Patient__c = 'Case.Id',
                VTD2_T_Care_Plan_Template__c = 'Case.VTD1_Study__c',
                VTD2_T_Related_To_Id__c = 'Case.Id',
                VTD2_T_Task_Record_Type_ID__c = Task.getSObjectType().getDescribe().getRecordTypeInfosByName().get('SimpleTask').getRecordTypeId(),
                VTD2_T_Subject__c = 'test',
                VTD2_T_Status__c = 'Open',
                VTD2_T_Due_Date__c = 'TODAY()',
                VTD2_T_Description__c = 'test #getMessage'
        ));
        insert tnCatalogCodes;

        /*
        Case cas = VT_D1_TestUtils.createCase('CarePlan', patient.ContactId, PI.ContactId, null, null, null, null);
        cas.VTD1_Study__c = study.Id;
        cas.AccountId = patient.AccountId;
        //cas.VTD1_CreatedInCenduitCSM__c = true;
        //cas.Status = 'Screened';
        Test.startTest();
        insert cas;
        Test.stopTest();
        */

        List<Address__c> addresses = new List<Address__c>();
        Set<Id> contactIds = new Set<Id>();
        for (Address__c item : [
                SELECT Id, VTD1_Primary__c, VTD1_Contact__c
                FROM Address__c
                WHERE (VTD1_Contact__c = :patient.ContactId OR VTD1_Contact__c = :CG.ContactId)
                AND VTD1_Primary__c = TRUE
        ]) {
            contactIds.add(item.VTD1_Contact__c);
        }

        for (User u : new List<User>{
                patient, CG
        }) {
            addresses.add(new Address__c(
                    Name = 'Test address ' + u.Name,
                    VTD1_Contact__c = u.ContactId,
                    VTD1_Address2__c = '26109 Cherry Blossom Court',
                    City__c = 'New-York',
                    State__c = 'NY',
                    ZipCode__c = '100100',
                    VTD1_Primary__c = !contactIds.contains(u.ContactId),
                    Country__c = 'US'
            ));
            contactIds.add(u.ContactId);
        }

        List<VT_D1_Phone__c> caregiverPhones = new List<VT_D1_Phone__c>{
                new VT_D1_Phone__c(PhoneNumber__c = '12345', IsPrimaryForPhone__c = true, Type__c = 'Home', VTD1_Contact__c = CG.ContactId, Account__c = CG.AccountId),
                new VT_D1_Phone__c(PhoneNumber__c = '67890', IsPrimaryForPhone__c = false, Type__c = 'Home', VTD1_Contact__c = CG.ContactId, Account__c = CG.AccountId)
        };

        System.debug(JSON.serializePretty(addresses));

        insert addresses;
        insert caregiverPhones;
    }

    public class SendSubjectMock implements HttpCalloutMock {
        private Integer responseCode;
        public SendSubjectMock(Integer responseCode) {
            this.responseCode = responseCode;
        }
        public HttpResponse respond(HttpRequest req) {
            HttpResponse response = new HttpResponse();
            response.setStatusCode(responseCode);
            if (responseCode == null) {
                response = null;
            }
            return response;
        }
    }

    public class RandomizeMock implements HttpCalloutMock {
        private Integer responseCode;

        public RandomizeMock(Integer responseCode) {
            this.responseCode = responseCode;
        }
        public HttpResponse respond(HttpRequest req) {
            HttpResponse response = new HttpResponse();
            response.setStatusCode(responseCode);
            if (responseCode == 200) {
                VT_D1_SubjectIntegration.RandomizeSubjectResponseMessage subj = new VT_D1_SubjectIntegration.RandomizeSubjectResponseMessage();
                subj.protocolId = 'some';
                subj.randomizationId = 'someid';
                response.setBody(JSON.serialize(subj));
            } else if (responseCode == null) {
                response = null;
            }
            return response;
        }
    }

    static void sendSubjectTest(Integer statusCode) {
        Id caseId = getCaseId();
        Test.setMock(HttpCalloutMock.class, new SendSubjectMock(statusCode));
        Test.startTest();
        VT_D1_QAction_SendSubject sendSubjectAction = new VT_D1_QAction_SendSubject(caseId);
        System.assertNotEquals(null, sendSubjectAction.returnForMassAddToQueue());
        sendSubjectAction.execute();
        sendSubjectAction.getStatus();
        sendSubjectAction.getMessage();
        sendSubjectAction.getAttemptNumber();
        sendSubjectAction.executeFuture();
        Test.stopTest();
    }

    @IsTest
    static void sendSubjectisLastAttempt() {
        Id caseId = getCaseId();
        Test.setMock(HttpCalloutMock.class, new SendSubjectMock(400));
        Test.startTest();
        VT_D1_QAction_SendSubject sendSubjectAction = new VT_D1_QAction_SendSubject(caseId);
        System.assertNotEquals(null, sendSubjectAction.returnForMassAddToQueue());
        sendSubjectAction.attemptsCount = 1;
        sendSubjectAction.execute();
        sendSubjectAction.getStatus();
        sendSubjectAction.getMessage();
        sendSubjectAction.getAttemptNumber();
        sendSubjectAction.executeFuture();
        Test.stopTest();
    }

    @IsTest
    static void sendSubjectOk() {
        sendSubjectTest(200);
    }

    @IsTest
    static void sendSubjectError500() {
        sendSubjectTest(500);
    }

    @IsTest
    static void sendSubjectError400() {
        sendSubjectTest(400);
    }

    @IsTest
    static void sendSubjectErrorNull() {
        sendSubjectTest(null);
    }

    static void getRandomizeTest(Integer statusCode) {
        Id caseId = getCaseId();
        Test.setMock(HttpCalloutMock.class, new RandomizeMock(statusCode));
        Test.startTest();
        VT_D1_QAction_GetRandomize getRandomizeAction = new VT_D1_QAction_GetRandomize(caseId);
        //System.assertNotEquals(null, getRandomizeAction.returnForMassAddToQueue());
        getRandomizeAction.execute();
        getRandomizeAction.getStatus();
        getRandomizeAction.getMessage();
        getRandomizeAction.getAttemptNumber();
        getRandomizeAction.executeFuture();
        Test.stopTest();
    }

    @IsTest
    static void getRandomizeisLastAttempt() {
        Id caseId = getCaseId();
        Id studyId = getStudyId();
        Test.setMock(HttpCalloutMock.class, new RandomizeMock(400));
        Test.startTest();
        DomainObjects.User_t newVTSLUser = new DomainObjects.User_t().setProfile(VT_R4_ConstantsHelper_ProfilesSTM.VTSL_PROFILE_NAME);
        newVTSLUser.persist();

        Study_Team_Member__c newSTM = new Study_Team_Member__c();
        newSTM.Study__c = studyId;
        newSTM.User__c = newVTSLUser.Id;
        insert newSTM;

        VTD2_Study_Geography__c newSG = new VTD2_Study_Geography__c();
        newSG.VTD2_Study__c = studyId;
        insert newSG;

        Virtual_Site__c virtualSite = [SELECT VTR3_Study_Geography__c FROM Virtual_Site__c LIMIT 1];
        virtualSite.VTR3_Study_Geography__c = newSG.Id;
        update virtualSite;

        VTR3_Geography_STM__c newGeoSTM = new VTR3_Geography_STM__c();
        newGeoSTM.Study_Geography__c = newSG.Id;
        newGeoSTM.VTR3_STM__c = newSTM.Id;
        newGeoSTM.Is_Primary__c = true;
        insert newGeoSTM;

        newSG.VTR5_Primary_VTSL__c = newVTSLUser.Id;
        update newSG;

        VT_D1_QAction_GetRandomize getRandomizeAction = new VT_D1_QAction_GetRandomize(caseId);
        //System.assertNotEquals(null, getRandomizeAction.returnForMassAddToQueue());
        getRandomizeAction.attemptsCount = 1;
        getRandomizeAction.execute();
        getRandomizeAction.getStatus();
        getRandomizeAction.getMessage();
        getRandomizeAction.getAttemptNumber();
        getRandomizeAction.executeFuture();
        Test.stopTest();
    }

    @IsTest
    static void getRandomizeOk() {
        getRandomizeTest(201);
    }

    @IsTest
    static void getRandomizeError400() {
        getRandomizeTest(400);
    }

    @IsTest
    static void getRandomizeError500() {
        getRandomizeTest(500);
    }

    @IsTest
    static void getRandomizeErrorNull() {
        getRandomizeTest(null);
    }

    static String getCaseId() {
        return [SELECT Id FROM Case LIMIT 1].Id;
    }

    static String getStudyId() {
        return [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1].Id;
    }

}