@IsTest
private without sharing class VT_D1_LicenseTriggerTest {

    @testSetup
    static void setup(){
        User patientUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t()
                        .setFirstName('FNLicenseTriggerTest')
                        .setLastName('PInvestigator'))
                .setProfile('Primary Investigator')
                .persist();
        User caregiverUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t()
                        .setFirstName('FNLicenseTriggerTest')
                        .setLastName('PBInvestigator'))
                .setProfile('Primary Investigator')
                .persist();
    }

    @IsTest
    private static void onInsertTest(){
        User PIUser = [SELECT id, ContactId FROM User WHERE Contact.LastName = 'PInvestigator'];
        User PIBackupUser = [SELECT id, ContactId FROM User WHERE Contact.LastName = 'PBInvestigator'];
        Test.startTest();
        VT_D1_TestUtils.createLicenses(PIUser.ContactId, PIBackupUser.ContactId, 'US', 'LA');
        VT_D1_TestUtils.createLicenses(PIUser.ContactId, PIBackupUser.ContactId, 'US', 'NY');
        Test.stopTest();
        List<Licenses__c> licenses = [SELECT Id, State__c, Contact__c FROM Licenses__c];
        System.assert(!licenses.isEmpty());
        List<Contact> contacts = [SELECT VTD1_Licensed_In_States__c FROM Contact WHERE Id IN (:PIUser.ContactId, :PIBackupUser.ContactId) AND VTD1_Licensed_In_States__c = 'LA,NY'];
        System.assert(!contacts.isEmpty());
    }
    @IsTest
    private static void onUpdateTest(){
        User PIUser = [SELECT id, ContactId FROM User WHERE Contact.LastName = 'PInvestigator'];
        User PIBackupUser = [SELECT id, ContactId FROM User WHERE Contact.LastName = 'PBInvestigator'];
        VT_D1_TestUtils.createLicenses(PIUser.ContactId, PIBackupUser.ContactId, 'US', 'NY');
        List<Licenses__c> licenses = [SELECT Id,VTD1_Expiry_Date__c FROM Licenses__c];
        System.assertNotEquals(0,licenses.size());
        licenses[0].VTD1_Expiry_Date__c = Date.today().addYears(5);
        Test.startTest();
        update licenses;
        Test.stopTest();
        List<Licenses__c> licenses2 = [SELECT Id,VTD1_Expiry_Date__c FROM Licenses__c WHERE Id=:licenses[0].Id];
        System.assertEquals(Date.today().addYears(5),licenses2[0].VTD1_Expiry_Date__c);
    }
    @IsTest
    private static void onDeleteTest(){
        User PIUser = [SELECT id, ContactId FROM User WHERE Contact.LastName = 'PInvestigator'];
        User PIBackupUser = [SELECT id, ContactId FROM User WHERE Contact.LastName = 'PBInvestigator'];
        VT_D1_TestUtils.createLicenses(PIUser.ContactId, PIBackupUser.ContactId, 'US', 'NY');
        List<Licenses__c> licenses = [SELECT Id,VTD1_Expiry_Date__c FROM Licenses__c];
        System.assertNotEquals(0,licenses.size());
        Test.startTest();
        delete licenses;
        Test.stopTest();
        List<Licenses__c> licenses2 = [SELECT Id,VTD1_Expiry_Date__c FROM Licenses__c WHERE Id IN :licenses];
        System.assertEquals(0,licenses2.size());
    }
}