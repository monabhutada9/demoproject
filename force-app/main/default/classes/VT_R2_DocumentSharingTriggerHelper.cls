/**
* @author: Carl Judge
* @date: 19-Feb-19
* @description: Methods to be called from various triggers to invoke document sharing if needed
**/

public without sharing class VT_R2_DocumentSharingTriggerHelper {
    public static final List<String> STUDY_SHARING_FIELDS = new List<String>{
        'VTD1_Project_Lead__c',
        'VTD1_Virtual_Trial_Study_Lead__c',
        'VTD1_Regulatory_Specialist__c',
        'VTD1_Virtual_Trial_head_of_Operations__c',
        'VTD2_TMA_Blinded_for_Study__c'
    };

    public static final List<String> CASE_SHARING_FIELDS = new List<String>{
        'VTD1_Patient_User__c',
        'VTD1_PI_user__c',
        'VTD1_Backup_PI_User__c',
        'VTD1_Primary_PG__c',
        'VTD1_Secondary_PG__c'
    };

    public static final Set<Id> MEDICAL_RECORDTYPES = new Set<Id>{
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD,
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON,
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_ARCHIVED,
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_REJECTED,
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_RELEASE_FORM
    };

    public static void processStudies(List<HealthCloudGA__CarePlanTemplate__c> studies, Map<Id, HealthCloudGA__CarePlanTemplate__c> oldMap) {
        List<Id> userIds = new List<Id>();
        List<Id> changedTMABlindedStudyIds = new List<Id>();

        for (HealthCloudGA__CarePlanTemplate__c study : studies) {
            for (String field : STUDY_SHARING_FIELDS) {
                Object newVal = study.get(field);
                Object oldVal = oldMap.get(study.Id).get(field);

                if (oldVal != newVal) {
                    if (field == 'VTD2_TMA_Blinded_for_Study__c') {
                        changedTMABlindedStudyIds.add(study.Id);
                    } else {
                        if (newVal != null) { userIds.add((Id)newVal); }
                        if (oldVal != null) { userIds.add((Id)oldVal); }
                    }
                }
            }
        }

        if (! changedTMABlindedStudyIds.isEmpty() || ! userIds.isEmpty()) {
            recalculateSharingForSTMs([
                SELECT Study__c, User__c
                FROM Study_Team_Member__c
                WHERE (User__c IN :userIds AND Study__c IN :studies)
                OR (User__r.Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME AND Study__c IN :changedTMABlindedStudyIds)
            ]);
        }
    }

    public static void processCases(List<Case> cases, Map<Id, Case> oldMap) {
        List<Id> caseIds = new List<Id>();
        Set<Id> userIds = new Set<Id>();

        for (Case item : cases) {
            for (String field : CASE_SHARING_FIELDS) {
                Id newVal = (Id)item.get(field);
                Id oldVal = (Id)oldMap.get(item.Id).get(field);

                if (oldVal != newVal) {
                    caseIds.add(item.Id);
                    if (newVal != null) { userIds.add(newVal); }
                    if (oldVal != null) { userIds.add(oldVal); }
                }
            }
        }

        if (! caseIds.isEmpty()) {
            VT_R3_GlobalSharing.doSharing([
                SELECT Id, RecordTypeId
                FROM VTD1_Document__c
                WHERE VTD1_Clinical_Study_Membership__c IN :caseIds
            ], userIds);
        }
    }

    public static void processDocuments(List<VTD1_Document__c> recs, Map<Id, VTD1_Document__c> oldMap) {
        List<VTD1_Document__c> toRecalc = new List<VTD1_Document__c>();
        for (VTD1_Document__c item : recs) {
            VTD1_Document__c old = oldMap.get(item.Id);
            if (item.RecordTypeId != old.RecordTypeId ||
                item.VTD1_Clinical_Study_Membership__c != old.VTD1_Clinical_Study_Membership__c ||
                item.VTD1_Study__c != old.VTD1_Study__c)
            {
                toRecalc.add(item);
            } else if (MEDICAL_RECORDTYPES.contains(item.RecordTypeId)) {
                if ((item.VTD1_Status__c == VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED) != (old.VTD1_Status__c == VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED)) {
                    toRecalc.add(item);
                } else if (item.VTD2_Redacted__c != old.VTD2_Redacted__c) {
                    toRecalc.add(item);
                }
            } else if (item.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT) {
                if (item.VTR3_Category__c != VT_R4_ConstantsHelper_Documents.DOCUMENT_CATEGORY_UNCATEGORIZED && old.VTR3_Category__c == VT_R4_ConstantsHelper_Documents.DOCUMENT_CATEGORY_UNCATEGORIZED) {
                    toRecalc.add(item);
                }
            }
        }

        if (! toRecalc.isEmpty()) { VT_R3_GlobalSharing.doSharing(toRecalc); }
    }

    /* Deprecated, now in virtual site handler for all objects */
    public static void processVirtualSites(List<Virtual_Site__c> recs, Map<Id, Virtual_Site__c> oldMap) {
        Set<Id> changedStudyMemberSiteIds = new Set<Id>();
        Set<Id> stmIds = new Set<Id>();
        Set<String> virtualSiteFields = new Set<String>{
            'VTD1_Study_Team_Member__c',
            'VTR2_Backup_PI_STM__c',
            'VTR2_Primary_SCR__c'/*,//RAJESH :SH-17440 removed the CRA fields mapping from the virtual site.
            'VTR2_Onsite_CRA__c',
            'VTD1_Remote_CRA__c'*/
        };

        for (Virtual_Site__c item : recs) {
            for (String virtualSiteField : virtualSiteFields) {
                Object stmVal = item.get(virtualSiteField);
                Object oldVal = oldMap.get(item.Id).get(virtualSiteField);
                if (stmVal != oldVal) {
                    changedStudyMemberSiteIds.add(item.Id);
                    if (stmVal != null) {
                        stmIds.add((Id)item.VTD1_Study_Team_Member__c);
                    }
                    if (oldVal != null) {
                        stmIds.add((Id)oldMap.get(item.Id).VTD1_Study_Team_Member__c);
                    }
                }
            }
        }

        if (!changedStudyMemberSiteIds.isEmpty()) {
            Set<Id> userIds = new Set<Id>();
            for (Study_Team_Member__c member : [
                SELECT Id, User__c
                FROM Study_Team_Member__c
                WHERE Id IN :stmIds
            ]) {
                userIds.add(member.User__c);
            }

            VT_R3_GlobalSharing.doSharing([
                SELECT Id, RecordTypeId
                FROM VTD1_Document__c
                WHERE VTD1_Site__c IN :changedStudyMemberSiteIds
            ], userIds);
        }
    }

    public static void processStudyTeamMembers(List<Study_Team_Member__c> recs) {
        recalculateSharingForSTMs(recs);
    }

    public static void processProtocolAmendmentsOnDelete(List<VTD1_Protocol_Amendment__c> recs) {
        List<Id> docIds = new List<Id>();
        for (VTD1_Protocol_Amendment__c item : recs) {
            if (item.VTD1_Study_ID__c != null && item.VTD1_Protocol_Amendment_Document_Link__c != null) {
                docIds.add(item.VTD1_Protocol_Amendment_Document_Link__c);
            }
        }
        if (! docIds.isEmpty()) {
            VT_R3_GlobalSharing.doSharing(docIds);
        }
    }

    public static void processProtocolAmendmentsOnUpdate(List<VTD1_Protocol_Amendment__c> recs, Map<Id, VTD1_Protocol_Amendment__c> oldMap) {
        List<Id> docIds = new List<Id>();
        for (VTD1_Protocol_Amendment__c item : recs) {
            if (item.VTD1_Study_ID__c != null &&
                item.VTD1_Protocol_Amendment_Document_Link__c != oldMap.get(item.Id).VTD1_Protocol_Amendment_Document_Link__c)
            {
                if (item.VTD1_Protocol_Amendment_Document_Link__c != null) {
                    docIds.add(item.VTD1_Protocol_Amendment_Document_Link__c);
                }
                if (oldMap.get(item.Id).VTD1_Protocol_Amendment_Document_Link__c != null) {
                    docIds.add(oldMap.get(item.Id).VTD1_Protocol_Amendment_Document_Link__c);
                }
            }
        }
        if (! docIds.isEmpty()) {
            VT_R3_GlobalSharing.doSharing(docIds);
        }
    }

    public static void recalculateSharingForSTMs(List<Study_Team_Member__c> stms) {
        List<Id> studyIds = new List<Id>();
        Set<Id> userIds = new Set<Id>();
        for (Study_Team_Member__c stm : stms) {
            studyIds.add(stm.Study__c);
            userIds.add(stm.User__c);
        }

        List<Id> docIdsFromPAs = new List<Id>();
        for (VTD1_Protocol_Amendment__c item : [
            SELECT VTD1_Protocol_Amendment_Document_Link__c
            FROM VTD1_Protocol_Amendment__c
            WHERE VTD1_Study_ID__c IN :studyIds
            AND VTD1_Protocol_Amendment_Document_Link__c != null
        ]) {
            docIdsFromPAs.add(item.VTD1_Protocol_Amendment_Document_Link__c);
        }

        VT_R3_GlobalSharing.doSharing(new List<Id>(new Map<Id, VTD1_Document__c>([
            SELECT Id, RecordTypeId
            FROM VTD1_Document__c
            WHERE VTD1_Study__c IN :studyIds
            OR Id IN :docIdsFromPAs
            OR VTD1_Clinical_Study_Membership__r.VTD1_Study__c IN :studyIds
        ]).keySet()), userIds);
    }
}