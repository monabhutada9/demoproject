/**
 * Created by User on 19/05/14.
 */
@IsTest
private with sharing class VT_D2_PIEligibilityAssessmentFormRevTest {
    @IsTest
    private static void getEligibilityDataTest() {
        DomainObjects.User_t user = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5));
        System.debug('User*** ' + user);
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'));
        System.debug('study***' + study);
        DomainObjects.Case_t cas = new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .addUser(user)
                .addStudy(study);
        System.debug('case***' + cas);
        new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('RE')
                .addUser(user)
                .addStudy(study);

        DomainObjects.Task_t task = new DomainObjects.Task_t()
                .addUser(user)
                .addCase(cas)
                .addStudy(study)
                .setRecordTypeByName('VTR2_RE_Task')
                .setStatus('Open')
                .setVTD1_Type_for_backend_logic('Final Eligibility Decision Task')
                .addDocument(new DomainObjects.VTD1_Document_t());
        System.debug('task*** ' + task);
        task.persist();

        System.debug('task after persist' + task);
        try {
            System.assertNotEquals(null, VT_D2_PIEligibilityAssessmentFormReview.getEligibilityData(task.Id));
        } catch (Exception e) {
            System.debug('ee!' + e);
        }

    }

    @IsTest
    private static void submitEligibilityTest() {
        DomainObjects.User_t user = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5));

        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'));

        DomainObjects.Case_t cas = new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .addUser(user)
                .addStudy(study);

        new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('RE')
                .addUser(user)
                .addStudy(study);

        DomainObjects.Task_t task = new DomainObjects.Task_t()
                .addUser(user)
                .addCase(cas)
                .addStudy(study)
                .setRecordTypeByName('VTR2_RE_Task')
                .setStatus('Open')
                .setVTD1_Type_for_backend_logic('Final Eligibility Decision Task')
                .addDocument(new DomainObjects.VTD1_Document_t());
        task.persist();

        VTD1_Document__c doc = new VTD1_Document__c();
        doc.VTD2_Final_Eligibility_Decision__c = 'Eligible';
        doc.VTD2_Final_Review_Comments__c = 'test';
        insert doc;
        VT_D2_PIEligibilityAssessmentFormReview.submitEligibility(JSON.serialize(doc));
        VTD1_Document__c docNull = new VTD1_Document__c();
        docNull.VTD2_Final_Eligibility_Decision__c = null;
        docNull.VTD2_Final_Review_Comments__c = null;
        try {
            VT_D2_PIEligibilityAssessmentFormReview.submitEligibility(JSON.serialize(docNull));
        } catch (Exception e) {

        }

    }
    @IsTest
    private static void pdfNegativeTest() {

        ContentDocument contentDocument = new ContentDocument();
        ContentDocumentLink contentDocumentLink = new ContentDocumentLink(
                ContentDocumentId = contentDocument.Id,
                LinkedEntityId = null
        );
        System.debug('getPDF ' + VT_D2_PIEligibilityAssessmentFormReview.getContentAsPDF(contentDocumentLink.Id));

    }

}