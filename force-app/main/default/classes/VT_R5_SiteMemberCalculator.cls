/**
* @author: Carl Judge
* @date: 25-Sep-20
* @description: Find all users related to sites and vice versa
**/

public inherited sharing class VT_R5_SiteMemberCalculator {
    public static final Set<String> SITE_PROFILES = new Set<String> {
        VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME
    };

    private Set<Id> siteIds;
    private Set<Id> userIds;
    private Set<Id> studyIds;
    private Map<Id, Set<Id>> siteToUsersMap;
    // Conquerors
    // Abhay
    // Jira Ref: SH-17438
    private Map<Id, Set<Id>> siteToCraUsersMap;
    // End of code: Conquerors

    private Map<Id, Set<Id>> userToSitesMap;

    public void initForSites(Set<Id> siteIds) {
        System.debug('initForSites ' + siteIds);
        reset();
        this.siteIds = siteIds;
        if (siteIds != null && !siteIds.isEmpty()) {
            List<Virtual_Site__c> sites = getSites();
            addMembersFromSites(sites);
            addPiRelatedSSTMs(sites);
            addSiteRelatedSSTMs(sites);
            addGeographyMembersFromSites(sites);
            fillUsersToSiteMap();
            removeNulls();
        }
    }

    public void initForSSTMs(Set<Id> sstmIds) {
        reset();
        List<Study_Site_Team_Member__c> sstms = getSSTMs(sstmIds);
        addSSTMMembers(sstms);
    }

    public void initForUsers(Set<Id> userIds, Set<Id> studyIds, Set<Id> siteIds) {
        reset();
        if (studyIds == null || studyIds.isEmpty()) {
            throw new SiteMemberCalcException('Study IDs cannot be null or empty');
        }
        this.userIds = userIds;
        this.studyIds = studyIds;
        this.siteIds = siteIds;
        if (userIds != null && !userIds.isEmpty()) {
            addSiteMembersFromUsers();
            addSSTMMembers(getSSTMsForUsers());
            fillUsersToSiteMap();
            removeNulls();
        }
    }
    public void initForUsers(Set<Id> userIds, Set<Id> studyIds) {
        initForUsers(userIds, studyIds, null);
    }

    public Set<Id> getSiteUserIds(Id siteId) {
        return siteToUsersMap.get(siteId) != null ? siteToUsersMap.get(siteId) : new Set<Id>();
    }

    public Set<Id> getUserSiteIds(Id userId) {
        return userToSitesMap.get(userId) != null ? userToSitesMap.get(userId) : new Set<Id>();
    }

    public Boolean isSiteMember(Id siteId, Id userId) {
        return siteToUsersMap.containsKey(siteId) && siteToUsersMap.get(siteId).contains(userId);
    }

    public Boolean isSiteMember(SiteUser siteUser) {
        return siteToUsersMap.containsKey(siteUser.siteId) && siteToUsersMap.get(siteUser.siteId).contains(siteUser.userId);
    }

    public Set<Id> getSiteIds() {
        return siteToUsersMap.keySet();
    }

    public Set<Id> getUserIds() {
        return userToSitesMap.keySet();
    }

    public Map<Id, Set<Id>> getSiteToUsersMap() {
        return siteToUsersMap;
    }

    public Map<Id, Set<Id>> getUserToSitesMap() {
        return userToSitesMap;
    }

    // Conquerors
    // Abhay
    // Jira Ref: SH-17438 
    // Description: Getter to get all the CRAs associated to the site
    public Map<Id, Set<Id>> getSiteToCraUsersMap() {	
        return siteToCraUsersMap;	
    }
    // End of code: Conquerors 

    public List<SiteUser> getSiteUsers() {
        List<SiteUser> siteUsers = new List<SiteUser>();
        for (Id siteId : siteToUsersMap.keySet()) {
            siteUsers.addAll(getSiteUsers(siteId));
        }
        return siteUsers;
    }

    public List<SiteUser> getSiteUsers(Id siteId) {
        List<SiteUser> siteUsers = new List<SiteUser>();
        for (Id userId : siteToUsersMap.get(siteId)) {
            siteUsers.add(new SiteUser(siteId, userId));
        }
        return siteUsers;
    }

    // given a list of SiteUsers, return a list of any user who is NOT a member of the site
    public List<SiteUser> getNonMembers(List<SiteUser> siteUsers) {
        List<SiteUser> nonMembers = new List<SiteUser>();
        for (SiteUser sUser : siteUsers) {
            if (!isSiteMember(sUser)) {
                nonMembers.add(sUser); 
            }
        }
        return nonMembers;
    }

    private void reset() {
        siteIds = null;
        userIds = null;
        userIds = null;
        siteToUsersMap = new Map<Id, Set<Id>>();
        userToSitesMap = new Map<Id, Set<Id>>();

        // Conquerors
        // Abhay
        // Jira Ref: SH-17438 
        // Description: Getter to get all the CRAs associated to the site
        siteToCraUsersMap = new Map<Id, Set<Id>>();
        // End of code: Conquerors 
    }

    private List<Virtual_Site__c> getSites() {
        return [
            SELECT Id,
                VTD1_Study_Team_Member__c,
                VTD1_Study_Team_Member__r.VTD1_Active__c,
                VTD1_Study_Team_Member__r.User__r.Id,
                VTD1_Study_Team_Member__r.User__r.IsActive,
                VTD1_Study_Team_Member__r.User__r.Profile.Name,
                VTR2_Backup_PI_STM__r.User__r.Id,
                VTR2_Backup_PI_STM__r.User__r.IsActive,
                VTR2_Backup_PI_STM__r.User__r.Profile.Name,
                VTR2_Primary_SCR__r.Id,
                VTR2_Primary_SCR__r.IsActive,
                VTR2_Primary_SCR__r.Profile.Name,
                VTR3_Study_Geography__c
            FROM Virtual_Site__c
            WHERE Id IN :siteIds
        ];
    }

    private void addMembersFromSites(List<Virtual_Site__c> sites) {
        for (Virtual_Site__c site : sites) {
            addMember(site.Id, site.VTD1_Study_Team_Member__r.User__r);
            addMember(site.Id, site.VTR2_Backup_PI_STM__r.User__r);
            addMember(site.Id, site.VTR2_Primary_SCR__r);
        }
    }
    
    private void addPiRelatedSSTMs(List<Virtual_Site__c> sites) {
        Map<Id, List<User>> piToUsers = getSSTMMembersByPI(sites);
        for (Virtual_Site__c site : sites) {
            if (piToUsers.containsKey(site.VTD1_Study_Team_Member__c)) {
                for (User sstmUser : piToUsers.get(site.VTD1_Study_Team_Member__c)) {
                    addMember(site.Id, sstmUser);
                }
            }
        }
    }

    private void addSiteRelatedSSTMs(List<Virtual_Site__c> sites) {
        //START: RAjesh :SH-17440: Add Member from the Virtual Site related SSTM
        for(Study_Site_Team_Member__c sstm : [
            SELECT Id,VTD1_SiteID__c, VTR5_Associated_CRA__r.User__r.Id, VTR5_Associated_CRA__r.User__r.IsActive,
            VTR5_Associated_CRA__r.User__r.Profile.Name
            FROM Study_Site_Team_Member__c
            WHERE VTD1_SiteID__c IN:sites
            AND VTR5_Associated_CRA__c != NULL
        ]){
            addMember(sstm.VTD1_SiteID__c, sstm.VTR5_Associated_CRA__r.User__r);
        }
        //END SH-17440
    }

    private void addGeographyMembersFromSites(List<Virtual_Site__c> sites) {
        Map<Id, Set<Id>> geoToSites = new Map<Id, Set<Id>>();
        for (Virtual_Site__c site : sites) {
            if (site.VTR3_Study_Geography__c != null) {
                if (!geoToSites.containsKey(site.VTR3_Study_Geography__c)) {
                    geoToSites.put(site.VTR3_Study_Geography__c, new Set<Id>());
                }
                geoToSites.get(site.VTR3_Study_Geography__c).add(site.Id);
            }
        }

        if (!geoToSites.isEmpty()) {
            for (VTR3_Geography_STM__c geoStm : [
                SELECT VTR3_STM__r.User__r.Id, VTR3_STM__r.User__r.IsActive, Study_Geography__c,
                    VTR3_STM__r.User__r.Profile.Name
                FROM VTR3_Geography_STM__c
                WHERE Study_Geography__c IN :geoToSites.keySet()
                AND VTR3_STM__r.User__r.Profile.Name IN :SITE_PROFILES
            ]) {
                for (Id siteId : geoToSites.get(geoStm.Study_Geography__c)) {
                    addMember(siteId, geoStm.VTR3_STM__r.User__r);
                }
            }
        }
    }

    private Map<Id, List<User>> getSSTMMembersByPI(List<Virtual_Site__c> sites) {
        Map<Id, List<User>> piToUsers = new Map<Id, List<User>>();
        for (Virtual_Site__c site : sites) {
            if (site.VTD1_Study_Team_Member__c != null && site.VTD1_Study_Team_Member__r.VTD1_Active__c) {
                piToUsers.put(site.VTD1_Study_Team_Member__c, new List<User>());
            }
        }
        if (!piToUsers.isEmpty()) {
            for (Study_Site_Team_Member__c sstm : [
                SELECT
                    // PG fields
                    VTD1_Associated_PG__r.User__r.Id,
                    VTD1_Associated_PG__r.User__r.IsActive,
                    VTD1_Associated_PG__r.User__r.Profile.Name,
                    VTD1_Associated_PI__c,
                    // SCR fields
                    VTR2_Associated_SCr__r.User__r.Id,
                    VTR2_Associated_SCr__r.User__r.IsActive,
                    VTR2_Associated_SCr__r.User__r.Profile.Name,
                    VTR2_Associated_PI__c,
                    // SUB-I fields
                    VTR2_Associated_SubI__r.User__r.Id,
                    VTR2_Associated_SubI__r.User__r.IsActive,
                    VTR2_Associated_SubI__r.User__r.Profile.Name,
                    VTR2_Associated_PI3__c
                FROM Study_Site_Team_Member__c
                WHERE (VTD1_Associated_PI__c IN :piToUsers.keySet() AND VTD1_Associated_PG__r.User__r.IsActive = TRUE)
                OR (VTR2_Associated_PI__c IN :piToUsers.keySet() AND VTR2_Associated_SCr__r.User__r.IsActive = TRUE)
                OR (VTR2_Associated_PI3__c IN :piToUsers.keySet() AND VTR2_Associated_SubI__r.User__r.IsActive = TRUE)
            ]) {
                if (piToUsers.containsKey(sstm.VTD1_Associated_PI__c)) {
                    piToUsers.get(sstm.VTD1_Associated_PI__c).add(sstm.VTD1_Associated_PG__r.User__r);
                } else if (piToUsers.containsKey(sstm.VTR2_Associated_PI__c)) {
                    piToUsers.get(sstm.VTR2_Associated_PI__c).add(sstm.VTR2_Associated_SCr__r.User__r);
                } else {
                    piToUsers.get(sstm.VTR2_Associated_PI3__c).add(sstm.VTR2_Associated_SubI__r.User__r);
                }
            }
        }
        return piToUsers;
    }

    private void addSiteMembersFromUsers() {
        Map<Id, Set<User>> geographyToUserMap = getGeographyUsers();
        for (Virtual_Site__c site : [
            SELECT Id,
                VTD1_Study_Team_Member__c,
                VTD1_Study_Team_Member__r.VTD1_Active__c,
                VTD1_Study_Team_Member__r.User__r.Id,
                VTD1_Study_Team_Member__r.User__r.IsActive,
                VTD1_Study_Team_Member__r.User__r.Profile.Name,
                VTR2_Backup_PI_STM__r.User__r.Id,
                VTR2_Backup_PI_STM__r.User__r.IsActive,
                VTR2_Backup_PI_STM__r.User__r.Profile.Name,
                VTR2_Primary_SCR__r.Id,
                VTR2_Primary_SCR__r.IsActive,
                VTR2_Primary_SCR__r.Profile.Name,
                VTR3_Study_Geography__c
            FROM Virtual_Site__c
            WHERE VTD1_Study__c IN :studyIds
            AND (
                VTD1_Study_Team_Member__r.User__c IN :userIds
                OR VTR2_Backup_PI_STM__r.User__c IN :userIds
                OR VTR2_Primary_SCR__c IN :userIds
                OR VTR3_Study_Geography__c IN :geographyToUserMap.keySet())
        ]) {
            addMember(site.Id, site.VTD1_Study_Team_Member__r.User__r);
            addMember(site.Id, site.VTR2_Backup_PI_STM__r.User__r);
            addMember(site.Id, site.VTR2_Primary_SCR__r);
            if (geographyToUserMap.containsKey(site.VTR3_Study_Geography__c)) {
                for (User geoUsr : geographyToUserMap.get(site.VTR3_Study_Geography__c)) {
                    addMember(site.Id, geoUsr);
                }
            }
        }
    }

    private List<Study_Site_Team_Member__c> getSSTMs(Set<Id> sstmIds) {
        return [
            SELECT
            // PG fields
            VTD1_Associated_PG__r.User__r.Id,
            VTD1_Associated_PG__r.User__r.IsActive,
            VTD1_Associated_PI__c,
            // SCR fields
            VTR2_Associated_SCr__r.User__r.Id,
            VTR2_Associated_SCr__r.User__r.IsActive,
            VTR2_Associated_PI__c,
            // SUB-I fields
            VTR2_Associated_SubI__r.User__r.Id,
            VTR2_Associated_SubI__r.User__r.IsActive,
            VTR2_Associated_PI3__c,
            // Conquerors
            // Abhay
            // Jira Ref: SH-17438
            // Description: We are not adding enforce security in SOQL because we don't know the impact of the existing code
            VTR5_Associated_CRA__c,
            VTR5_Associated_CRA__r.User__r.Id,
            VTR5_Associated_CRA__r.User__r.IsActive,
            VTR5_Associated_CRA__r.User__r.Profile.Name,
            VTR2_Associated_SubI__r.User__r.Profile.Name,
            VTR2_Associated_SCr__r.User__r.Profile.Name,
            VTD1_Associated_PG__r.User__r.Profile.Name,
            VTD1_SiteID__c
            // End of code: Conquerors
            FROM Study_Site_Team_Member__c
            WHERE Id IN :sstmIds
        ];
    }

    private void addSSTMMembers(List<Study_Site_Team_Member__c> sstms) {
        Map<Id, Set<User>> piToUserMap = mapSSTMsByPI(sstms);
        if (!piToUserMap.isEmpty()) {
            addPiSSTMMembers(piToUserMap);
        }
        addSiteSSTMMembers(sstms);
    }

    private Map<Id, Set<User>> mapSSTMsByPI(List<Study_Site_Team_Member__c> sstms) {
        Map<Id, Set<User>> piToUserMap = new Map<Id, Set<User>>();
        for (Study_Site_Team_Member__c sstm : sstms) {
            Id piStmId;
            User usr;
            if (sstm.VTD1_Associated_PI__c != null) {
                piStmId = sstm.VTD1_Associated_PI__c;
                usr = sstm.VTD1_Associated_PG__r.User__r;
            } else if (sstm.VTR2_Associated_PI__c != null) {
                piStmId = sstm.VTR2_Associated_PI__c;
                usr = sstm.VTR2_Associated_SCr__r.User__r;
            } else if (sstm.VTR2_Associated_PI3__c != null) {
                piStmId = sstm.VTR2_Associated_PI3__c;
                usr = sstm.VTR2_Associated_SubI__r.User__r;
            }
            if (piStmId != null && usr != null) {
                if (!piToUserMap.containsKey(piStmId)) {
                    piToUserMap.put(piStmId, new Set<User>());
                }
                piToUserMap.get(piStmId).add(usr);
            }
        }
        return piToUserMap;
    }

    private void addPiSSTMMembers(Map<Id, Set<User>> piToUserMap) {
        Map<Id, Set<Id>> piToSiteMap = new Map<Id, Set<Id>>();
        for (Virtual_Site__c site : [
            SELECT Id, VTD1_Study_Team_Member__c
            FROM Virtual_Site__c
            WHERE VTD1_Study_Team_Member__c IN :piToUserMap.keySet()
        ]) {
            if (!piToSiteMap.containsKey(site.VTD1_Study_Team_Member__c)) {
                piToSiteMap.put(site.VTD1_Study_Team_Member__c, new Set<Id>());
            }
            piToSiteMap.get(site.VTD1_Study_Team_Member__c).add(site.Id);
        }
        for (Id piStmId : piToUserMap.keySet()) {
            if (piToSiteMap.containsKey(piStmId)) {
                for (Id siteId : piToSiteMap.get(piStmId)) {
                    for (User usr : piToUserMap.get(piStmId)) {
                        addMember(siteId, usr);
                    }
                }
            }
        }
    }

    private void addSiteSSTMMembers(List<Study_Site_Team_Member__c> sstms) {
        for (Study_Site_Team_Member__c sstm : sstms) {
            addMember(sstm.VTD1_SiteID__c, sstm.VTR5_Associated_CRA__r.User__r);
        }
    }

    private List<Study_Site_Team_Member__c> getSSTMsForUsers() {
        return [
            SELECT
                // PG fields
                VTD1_Associated_PG__r.User__r.Id,
                VTD1_Associated_PG__r.User__r.IsActive,
                VTD1_Associated_PG__r.User__r.Profile.Name,
                VTD1_Associated_PI__r.VTD1_VirtualSite__c,
                // SCR fields
                VTR2_Associated_SCr__r.User__r.Id,
                VTR2_Associated_SCr__r.User__r.IsActive,
                VTR2_Associated_SCr__r.User__r.Profile.Name,
                VTR2_Associated_PI__r.VTD1_VirtualSite__c,
                // SUB-I fields
                VTR2_Associated_SubI__r.User__r.Id,
                VTR2_Associated_SubI__r.User__r.IsActive,
                VTR2_Associated_SubI__r.User__r.Profile.Name,
                VTR2_Associated_PI3__r.VTD1_VirtualSite__c, 
                // CRA fields
                VTR5_Associated_CRA__c,
                VTR5_Associated_CRA__r.User__r.Id,
                VTR5_Associated_CRA__r.User__r.IsActive,
                VTR5_Associated_CRA__r.User__r.Profile.Name,
                VTD1_SiteID__c
            FROM Study_Site_Team_Member__c
            WHERE (VTD1_Associated_PG__r.User__c IN :userIds
                AND VTD1_Associated_PI__r.VTD1_Active__c = TRUE
                AND VTD1_Associated_PI__r.VTD1_VirtualSite__r.VTD1_Study__c IN :studyIds)
            OR (VTR2_Associated_SubI__r.User__c IN :userIds
                AND VTR2_Associated_PI3__r.VTD1_Active__c = TRUE
                AND VTR2_Associated_PI3__r.VTD1_VirtualSite__r.VTD1_Study__c IN :studyIds)
            OR (VTR2_Associated_SCr__r.User__c IN :userIds
                AND VTR2_Associated_PI__r.VTD1_Active__c = TRUE
                AND VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTD1_Study__c IN :studyIds)
            OR (VTR5_Associated_CRA__r.User__c IN :userIds AND VTD1_SiteID__r.VTD1_Study__c IN :studyIds)
        ];
    }

    private Map<Id, Set<User>> getGeographyUsers() {
        Map<Id, Set<User>> geographyToUserMap = new Map<Id, Set<User>>();
        for (VTR3_Geography_STM__c geoStm : [
            SELECT VTR3_STM__r.User__r.Id, VTR3_STM__r.User__r.IsActive, Study_Geography__c,VTR3_STM__r.User__r.Profile.Name
            FROM VTR3_Geography_STM__c
            WHERE VTR3_STM__r.User__c IN :userIds
            AND Study_Geography__r.VTD2_Study__c IN :studyIds
            AND VTR3_STM__r.User__r.Profile.Name IN :SITE_PROFILES
        ]){
            if (!geographyToUserMap.containsKey(geoStm.Study_Geography__c)) {
                geographyToUserMap.put(geoStm.Study_Geography__c, new Set<User>());
            }
            geographyToUserMap.get(geoStm.Study_Geography__c).add(geoStm.VTR3_STM__r.User__r);
        }
        return geographyToUserMap;
    }

    private void addMember(Id siteId, User usr) {
        if (siteId != null
            && usr != null
            && usr.IsActive
            && (userIds == null || userIds.contains(usr.Id))
            && (siteIds == null || siteIds.contains(siteId)))
        {
            if (!siteToUsersMap.containsKey(siteId)) {
                siteToUsersMap.put(siteId, new Set<Id>());
            }
            siteToUsersMap.get(siteId).add(usr.Id);

            // Conquerors
            // Abhay
            // Jira Ref: SH-17438 
            // Description: Creating a map only to associate the User who are CRA to the Virtual Site
            if (!siteToCraUsersMap.containsKey(siteId) 	
                && usr.Profile != null
                && usr.Profile.Name != null
                && usr.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME) {	
                siteToCraUsersMap.put(siteId, new Set<Id>());	
            }
            if(siteToCraUsersMap.containsKey(siteId)) {
                siteToCraUsersMap.get(siteId).add(usr.Id); 
            }	 
            // End of code: Conquerors
        }
    }

    private void fillUsersToSiteMap() {
        for (Id siteId : siteToUsersMap.keySet()) {
            for (Id userId : siteToUsersMap.get(siteId)) {
                if (!userToSitesMap.containsKey(userId)) {
                    userToSitesMap.put(userId, new Set<Id>());
                }
                userToSitesMap.get(userId).add(siteId);
            }
        }
    }

    private void removeNulls() {
        siteToUsersMap.remove(null);
        userToSitesMap.remove(null);
        for (Set<Id> ids : siteToUsersMap.values()) {
            ids.remove(null);
        }
        for (Set<Id> ids : userToSitesMap.values()) {
            ids.remove(null);
        }
    }

    public class SiteUser {
        public Id siteId;
        public Id userId;

        public SiteUser(Id siteId, Id userId) {
            this.siteId = siteId;
            this.userId = userId;
        }
    }

    public class SiteMemberCalcException extends Exception{}
}