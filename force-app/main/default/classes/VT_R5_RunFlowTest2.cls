@IsTest
private class VT_R5_RunFlowTest2 {
    @IsTest
    static void runAmendment_Protocol_Approved() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Amendment_Protocol_Approved(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
		Test.stopTest();
    }
    
    @IsTest//TODO
    static void runVTD1_Amendments_Protocol_Amendment_Implemented() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            //myMap.put('varContactId', '');
            new Flow.Interview.VTD1_Amendments_Protocol_Amendment_Implemented(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest
    static void runVTD1_Amendments_Task_Creation_AcknowledgeUrgentSafetyInformation() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTD1_Amendments_Task_Creation_AcknowledgeUrgentSafetyInformation(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest
    static void runCreate_Post_to_Chatter_or_Notification_for_PG_or_SCR_TMA_vs_PI_decisions() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Create_Post_to_Chatter_or_Notification_for_PG_or_SCR_TMA_vs_PI_decisions(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest
    static void runCreate_Task_for_PG_or_SCR_to_Follow_Up_with_PI() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Create_Task_for_PG_or_SCR_to_Follow_Up_with_PI(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest
    static void runVTR2_Create_Task_for_PG_or_SCR_to_Follow_up_with_PI_for_FED() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTR2_Create_Task_for_PG_or_SCR_to_Follow_up_with_PI_for_FED(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest
    static void runDelete_create_sharing_when_PG_or_PI_is_changed() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Delete_create_sharing_when_PG_or_PI_is_changed(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest//TODO
    static void runDisable_Caregivers() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Disable_Caregivers(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest
    static void runEMAIL_ALERTS() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.EMAIL_ALERTS(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest//TODO
    static void runVTD2_Email_Countersign_Packet_for_PI() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTD2_Email_Countersign_Packet_for_PI(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest
    static void runEmail_to_CRA_on_rejected_Monitoring_Report() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Email_to_CRA_on_rejected_Monitoring_Report(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest
    static void runKits_Ad_hoc_Returns() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Kits_Ad_hoc_Returns(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest
    static void runVTD1_KITS_Task_Support_Flow() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTD1_KITS_Task_Support_Flow(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest
    static void runVTD1_Kits_Return_Delivery_Return_Complete_Flow() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTD1_Kits_Return_Delivery_Return_Complete_Flow(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }
    
    @IsTest//TODO
    static void runVTR3_Mobile_Push_15_mins_before_Video_Conf() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTR3_Mobile_Push_15_mins_before_Video_Conf(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }
    
    @IsTest
    static void runMonitoring_Complete_Tasks_To_Schedule_Interim_Visit() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Monitoring_Complete_Tasks_To_Schedule_Interim_Visit(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }
    
    @IsTest
    static void runNotify_Patient_Guide_when_the_Patient_changes_State_Flow() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Notify_Patient_Guide_when_the_Patient_changes_State_Flow(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }
    
    @IsTest
    static void runPatient_Payment_Case_Compensation_Calculation_on_Study() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Patient_Payment_Case_Compensation_Calculation_on_Study(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest
    static void runPatient_Payment_Case_Reimbursement_Calculation_on_Study() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Patient_Payment_Case_Reimbursement_Calculation_on_Study(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest//TODO
    static void runVTD2_case_share_access_to_TMA() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTD2_case_share_access_to_TMA(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest//TODO
    static void runVTR3_PCF_SC_Reminders_Sender_Helper() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTR3_PCF_SC_Reminders_Sender_Helper(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest
    static void runPCF_SC_TMA_Task_Queue_Email() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.PCF_SC_TMA_Task_Queue_Email(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest//TODO
    static void runPD_Form_Subject_PD_Creation_notification() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.PD_Form_Subject_PD_Creation_notification(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest
    static void runVTR2_ReviewKitComments_SendSCRemail() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTR2_ReviewKitComments_SendSCRemail(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
        Test.stopTest();
    }
}