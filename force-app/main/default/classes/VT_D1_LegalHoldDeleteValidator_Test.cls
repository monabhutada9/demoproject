/**
* @author: Carl Judge
* @date: 23-Oct-18
* @description:
**/
@IsTest
private class VT_D1_LegalHoldDeleteValidator_Test {
    private static final List<String> BATCH_1 = new List<String>{
        'VTD1_Survey__c',
        'VTD1_Patient_Device__c',
        'VTD1_Order__c',
        'VTD1_SC_Task__c',
        'VTD1_Patient_Lab_Sample__c',
        'Visit_Member__c',
        'VTD1_Protocol_ePRO__c',
        'VTD1_Return_Item__c',
        'VTD1_Protocol_ePro_Question__c',
        'Study_Site_Team_Member__c',
        'VTD1_Protocol_Amendment__c',
        'VTD1_ProtocolVisit__c',
        'VTD1_Protocol_Payment__c',
        'X3rd_Party_Vendors__c',
        'VTD1_PCF_Chat_Member__c',
        'VTD1_Lab_Kit_Contents__c',
        'VTD1_Case_Status_History__c',
        'VTD1_Patient_Kit__c',
        'Patient_LKC__c',
        'VTD1_Patient_Accessories__c',
        'VTD1_SubjectIdNumerator__c',
        'VTD1_Study_Stratification__c',
        'VTD1_Study_Milestone__c',
        'VTD1_Packaging_Item__c',
        'VTD1_Actual_Visit__c',
        'VTD1_PCF_Patient_Contact_Form_Comment__c',
        'VTD1_Replacement_Item__c',
        'Virtual_Site__c'
    };
    private static final List<String> BATCH_2 = new List<String>{
        'IMP_Replacement_Form__c',
        'VTD1_Protocol_Delivery__c',
        'VTD1_Protocol_Kit__c',
        'HealthCloudGA__EhrDevice__c',
        'VTD1_Protocol_LKC__c',
        'VTD1_Protocol_Accessories__c',
        'VTD1_Protocol_Lab_Sample__c',
        'VTD1_Regulatory_Binder__c',
        'VTD1_Monitoring_Visit__c',
        'Objective__c',
        'Video_Conference__c',
        'VTD1_Video_Conf_Connection__c',
        'VTD1_Conference_Member__c'
    };
    private static final List<String> BATCH_3 = new List<String>{
        'VTD1_Protocol_Deviation__c',
        'VTD1_Physician_Information__c',
        'VTD1_Patient_Payment__c',
        'VTD1_Document__c'
    };
    private static final List<String> BATCH_4 = new List<String>{
        'Study_Team_Member__c'
    };
    public with sharing class CalloutMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/xml');
            res.setBody(JSON.serialize(new List<String>{
                'SessionID'
            }));
            res.setStatusCode(200);
            return res;
        }
    }
    @testSetup
    private static void setupMethod() {
        //HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        HealthCloudGA__CarePlanTemplate__c study = (HealthCloudGA__CarePlanTemplate__c)new DomainObjects.Study_t()
            .addAccount(new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor')).persist();

        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t();

        User piUser = (User) new DomainObjects.User_t()
            .addContact(new DomainObjects.Contact_t())
            .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME)
            .persist();

        User patientUser = (User) new DomainObjects.User_t()
            .addContact(new DomainObjects.Contact_t().addAccount(patientAccount))
            .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME)
            .persist();

        //Test.stopTest();
        study.VTD1_Legal_Hold__c = true;
        study.VTD1_CM_Queue_Id__c = [SELECT Id FROM Group WHERE Name = 'CM Pool'].Id;
        update study;
        Test.startTest();
        Map<String, User> usersByProfile = new Map<String, User>();
        for (User item : [
            SELECT Id, ContactId, Contact.AccountId, Profile.Name
            FROM User
            WHERE (
                Profile.Name = 'Patient'
                AND Contact.AccountId != null
                AND IsActive = true
            ) OR (
                Profile.Name = 'Primary Investigator'
                AND IsActive = true
            ) OR (
                Profile.Name = 'Patient Guide'
                AND IsActive = true
            )
            ORDER BY CreatedDate ASC
        ]) {
            usersByProfile.put(item.Profile.Name, item);
        }
        //System.debug('TST: ' + usersByProfile);

        Case cas = new Case(
            VTD1_Study__c = study.Id,
            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VTD1_PCF').getRecordTypeId(),
            VTD1_Patient_User__c = usersByProfile.get('Patient').Id,
            VTD1_Primary_PG__c = usersByProfile.get('Patient Guide').Id,
            VTD1_PI_user__c = usersByProfile.get('Primary Investigator').Id,
            VTD1_PI_contact__c = usersByProfile.get('Primary Investigator').ContactId,
            VTD1_Replacement_Request_Count__c = 0
        );
        insert cas;

        Study_Team_Member__c craStm = new Study_Team_Member__c(Study__c = study.Id, User__c = UserInfo.getUserId());
        insert craStm;
        Study_Team_Member__c stm = new Study_Team_Member__c(Study__c = study.Id, /*VTR2_Remote_CRA__c = craStm.Id,*/ User__c = UserInfo.getUserId());
        insert stm;

        insert new Virtual_Site__c(
            VTD1_Study__c = study.Id,
            VTD1_Study_Site_Number__c = '1234',
            VTD1_Study_Team_Member__c = stm.Id
        );
        insert new VTD1_Actual_Visit__c(
            VTD1_Case__c = cas.Id
        );
        insert new VTD1_Protocol_ePRO__c(
            VTD1_Study__c = study.Id,
            VTR2_Protocol_Reviewer__c = 'Patient Guide',
            VTD1_Subject__c = 'Patient',
            VTD1_Response_Window__c = 10
        );
        //Test.startTest();
        VTD1_Protocol_Delivery__c pDelivery = new VTD1_Protocol_Delivery__c(
            Care_Plan_Template__c = study.Id,
            VTD1_Shipment_Type__c = 'Initial',
            VTD1_Delivery_Order__c = '01'
        );
        insert pDelivery;
        VTD1_Protocol_Kit__c pKit = new VTD1_Protocol_Kit__c(
            VTD1_Care_Plan_Template__c = study.Id,
            VTD1_Protocol_Delivery__c = pDelivery.Id,
            RecordTypeId = SObjectType.VTD1_Protocol_Kit__c.getRecordTypeInfosByDeveloperName().get('VTD1_Lab').getRecordTypeId()
        );
        insert pKit;
        //System.debug('study ' + study);
        //System.debug('pKit ' + pKit);
        Product2 prod = new Product2();
        prod.Name = 'test';
        prod.VTD1_Manufacturer__c = 'Carematix';
        prod.ProductCode = '123';
        insert prod;
        Catalog_Accessory__c ca = new Catalog_Accessory__c();
        ca.VTD1_Device__c = prod.id;
        ca.Name = 'test2231';
        ca.VTD1_Accessory_Description__c = 'test';
        ca.VTD1_Accessory_Qty__c = 10;
        insert ca;
        HealthCloudGA__EhrDevice__c ehrDevice = new HealthCloudGA__EhrDevice__c(
            VTD1_Care_Plan_Template__c = study.Id,
            VTD1_Kit_Type__c = 'Study Hub Tablet',
            VTD1_Device__c = prod.Id,
            VTD1_Protocol_Kit__c = pKit.Id
        );
        insert ehrDevice;
        HttpCalloutMock cMock = new CalloutMock();
        Test.setMock(HttpCalloutMock.class, cMock);
        Test.stopTest();
        Video_Conference__c vConf = new Video_Conference__c(
            VTD1_Clinical_Study_Membership__c = cas.Id
        );
        insert vConf;
    }
    @isTest
    private static void testBatch1() {
        testListOfTypes(BATCH_1);
    }
    @isTest
    private static void testBatch2() {
        testListOfTypes(BATCH_2);
    }
    @isTest
    private static void testBatch3() {
        testListOfTypes(BATCH_3);
    }
    @isTest
    private static void testBatch4() {
        testListOfTypes(BATCH_4);
    }
    private static void testListOfTypes(List<String> objTypes) {
        Test.startTest();
        Map<SObjectType, SObject> parentRecords = getParentRecords();
        User patient = [
            SELECT Id, ContactId, Contact.AccountId
            FROM User
            WHERE Username LIKE '%@scott.com'
            AND Profile.Name = 'Patient'
            AND Contact.AccountId != null
            AND IsActive = true
            ORDER BY CreatedDate DESC
            LIMIT 1
        ];
        for (String objTypeString : objTypes) {
            SObject obj;
            SObjectType objType = Schema.getGlobalDescribe().get(objTypeString);
            if (objTypeString == 'Objective__c') {
                List<Objective__c> objs = new List<Objective__c>{
                    new Objective__c(
                        Study__c = parentRecords.get(HealthCloudGA__CarePlanTemplate__c.getSObjectType()).Id
                    ),
                    new Objective__c(
                        Study_for_Primary_Objective__c = parentRecords.get(HealthCloudGA__CarePlanTemplate__c.getSObjectType()).Id
                    ),
                    new Objective__c(
                        Study_for_Secondary_Objective__c = parentRecords.get(HealthCloudGA__CarePlanTemplate__c.getSObjectType()).Id
                    )
                };
                insert objs;
                String error = '';
                try {
                    delete objs;
                } catch (Exception e) {
                    error = e.getMessage();
                }
                System.assert(error.contains(VT_D1_LegalHoldDeleteValidator.ERROR_MSG));
            } else {
                if (parentRecords.containsKey(objType)) {
                    obj = parentRecords.get(objType);
                } else {
                    obj = getNewObj(objTypeString, objType, parentRecords);
                    // add required fields for specific objects to prevent flow fails and such
                    switch on objTypeString {
                        when 'Study_Team_Member__c' {
                            obj.put('User__c', patient.Id);
                        }
                        when 'VTD1_Protocol_LKC__c' {
                            obj.put('VTD1_Lab_Kit__c', parentRecords.get(VTD1_Protocol_Kit__c.getSObjectType()).Id);
                        }
                        when 'VTD1_Protocol_Accessories__c' {
                            obj.put('VTD1_Protocol_Device__c', parentRecords.get(HealthCloudGA__EhrDevice__c.getSObjectType()).Id);
                        }
                        when 'VTD1_Protocol_Lab_Sample__c' {
                            obj.put('VTD1_Sample_Temperature__c', 'Room Temperature');
                            obj.put('VTD1_Lab_Kit__c', parentRecords.get(VTD1_Protocol_Kit__c.getSObjectType()).Id);
                        }
                        when 'VTD1_Regulatory_Binder__c' {
                            VTD1_Regulatory_Document__c rDoc = (VTD1_Regulatory_Document__c) new DomainObjects.VTD1_Regulatory_Document_t()
                                .setLevel('Study')
                                .setWayToSendToDocuSign('Manual')
                                .setDocumentType('Site Activation Approval Form')
                                .persist();
                            obj.put('VTD1_Regulatory_Document__c', rDoc.Id);
                        }
                        when 'VTD1_Monitoring_Visit__c' {
                            obj.put('VTD1_Study__c', parentRecords.get(HealthCloudGA__CarePlanTemplate__c.getSObjectType()).Id);
                            //obj.put('VTR2_Remote_Onsite__c', VTD1_Monitoring_Visit__c.VTR2_Remote_Onsite__c.getDescribe().getPicklistValues()[0].getValue());
                            obj.put('VTD1_Virtual_Site__c', parentRecords.get(Virtual_Site__c.getSObjectType()).Id);
                        }
                        when 'VTD1_ProtocolVisit__c' {
                            obj.put('VTR2_Visit_Participants__c', VTD1_ProtocolVisit__c.VTR2_Visit_Participants__c.getDescribe().getPicklistValues()[0].getValue());
                        }
                        when 'Visit_Member__c' {
                            obj.put('VTD1_External_Participant_Type__c', Visit_Member__c.VTD1_External_Participant_Type__c.getDescribe().getPicklistValues()[0].getValue());
                        }
                        when 'VTD1_Protocol_Deviation__c' {
                            obj.put('VTD1_StudyId__c', parentRecords.get(HealthCloudGA__CarePlanTemplate__c.getSObjectType()).Id);
                        }
                        when 'VTD1_Protocol_Amendment__c' {
                            DomainObjects.Study_t study = new DomainObjects.Study_t()
                                .setName('Study Name')
                                /*.addPMA(new DomainObjects.User_t()
                                        .addContact(new DomainObjects.Contact_t())
                                        .setProfile('Site Coordinator'))*/
                                .addAccount(new DomainObjects.Account_t()
                                    .setRecordTypeByName('Sponsor'));
                            HealthCloudGA__CarePlanTemplate__c studyObj = (HealthCloudGA__CarePlanTemplate__c) study.persist();
                            VTD1_Document__c doc = (VTD1_Document__c) new DomainObjects.VTD1_Document_t().persist();
                            VTD1_Regulatory_Document__c documentPAsignature = new VTD1_Regulatory_Document__c(
                                VTD1_Document_Type__c = 'PA Signature Page',
                                Name = 'Test Reg Doc',
                                Level__c = 'Site',
                                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI'
                            );
                            insert documentPAsignature;
                            VTD1_Regulatory_Document__c documentPA = new VTD1_Regulatory_Document__c(
                                VTD1_Document_Type__c = 'Protocol Amendment',
                                Name = 'Test Reg Doc',
                                Level__c = 'Site',
                                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI'
                            );
                            insert documentPA;
                            VTD1_Regulatory_Binder__c binderPAsignature = new VTD1_Regulatory_Binder__c(
                                VTD1_Care_Plan_Template__c = studyObj.Id,
                                VTD1_Regulatory_Document__c = documentPAsignature.Id
                            );
                            insert binderPAsignature;
                            VTD1_Regulatory_Binder__c binderPA = new VTD1_Regulatory_Binder__c(
                                VTD1_Care_Plan_Template__c = studyObj.Id,
                                VTD1_Regulatory_Document__c = documentPA.Id
                            );
                            insert binderPA;
                            obj.put('VTD1_Study_ID__c', studyObj.Id);
                            obj.put('VTD1_Protocol_Amendment_Document_Link__c', doc.Id);
                        }
                    }
					//Done the changes by Priyanka Ambre to remove Remote, Onsite references- SH-17441
                     if(objTypeString == 'VTD1_Monitoring_Visit__c'){
                        Id StudyId = parentRecords.get(HealthCloudGA__CarePlanTemplate__c.getSObjectType()).Id;

                        DomainObjects.StudyTeamMember_t Cra_t = new DomainObjects.StudyTeamMember_t()
                            .setStudy(StudyId)
                            .addUser(new DomainObjects.User_t()
                            .setProfile(VT_R4_ConstantsHelper_Profiles.CRA_PROFILE_NAME)
                        );
                        Study_Team_Member__c Cra = (Study_Team_Member__c) Cra_t.persist();

                        List<Study_Team_Member__c> studyTeamMembers = [select Id, 
                        RecordType.Name, 
                        Study__c, User__c, 
                        User__r.Profile.Name 
                        from Study_Team_Member__c 
                        where Study__c = :StudyId and User__r.Profile.Name = 'CRA' LIMIT 1];

                        HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
                        objVirtualShare.ParentId = StudyId;
                        objVirtualShare.UserOrGroupId = studyTeamMembers[0].User__c;
                        objVirtualShare.AccessLevel = 'Edit';
                        insert objVirtualShare;

                        Study_Site_Team_Member__c sstm1 = new Study_Site_Team_Member__c();
                        sstm1.VTR5_Associated_CRA__c = studyTeamMembers[0].Id;
                        sstm1.VTD1_SiteID__c = parentRecords.get(Virtual_Site__c.getSObjectType()).Id ;
                        insert sstm1;
                        System.runAs(new User(Id = studyTeamMembers[0].User__c)){
                            insert obj;
                        }
                    }else{
                        insert obj;
                    }                    
                }
                String error = '';
                try {
                    delete obj;
                } catch (Exception e) {
                    error = e.getMessage();
                }
                //System.assert(error.contains(VT_D1_LegalHoldDeleteValidator.ERROR_MSG));
            }
        }
        Test.stopTest();
    }
    private static Map<SObjectType, SObject> getParentRecords() {
        Map<SObjectType, SObject> parentRecords = new Map<SObjectType, SObject>();
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c WHERE VTD1_Legal_Hold__c = true LIMIT 1];
        parentRecords.put(HealthCloudGA__CarePlanTemplate__c.getSObjectType(), study);
        Case cas = [SELECT Id FROM Case WHERE VTD1_Study__c = :study.Id];
        parentRecords.put(Case.getSObjectType(), cas);
        Virtual_Site__c site = [SELECT Id FROM Virtual_Site__c WHERE VTD1_Study__c = :study.Id LIMIT 1];
        parentRecords.put(Virtual_Site__c.getSObjectType(), site);
        VTD1_Actual_Visit__c visit = [SELECT Id FROM VTD1_Actual_Visit__c WHERE VTD1_Case__c = :cas.Id];
        parentRecords.put(VTD1_Actual_Visit__c.getSObjectType(), visit);
        VTD1_Protocol_ePRO__c pEpro = [SELECT Id FROM VTD1_Protocol_ePRO__c WHERE VTD1_Study__c = :study.Id];
        parentRecords.put(VTD1_Protocol_ePRO__c.getSObjectType(), pEpro);
        VTD1_Protocol_Delivery__c pDelivery = [SELECT Id FROM VTD1_Protocol_Delivery__c WHERE Care_Plan_Template__c = :study.Id LIMIT 1];
        parentRecords.put(VTD1_Protocol_Delivery__c.getSObjectType(), pDelivery);
        VTD1_Protocol_Kit__c pKit = [SELECT Id FROM VTD1_Protocol_Kit__c WHERE VTD1_Care_Plan_Template__c = :study.Id AND RecordType.DeveloperName = 'VTD1_Lab' LIMIT 1];
        parentRecords.put(VTD1_Protocol_Kit__c.getSObjectType(), pKit);
        HealthCloudGA__EhrDevice__c pDevice = [SELECT Id FROM HealthCloudGA__EhrDevice__c WHERE VTD1_Care_Plan_Template__c = :study.Id LIMIT 1];
        parentRecords.put(HealthCloudGA__EhrDevice__c.getSObjectType(), pDevice);
        Video_Conference__c vConf = [SELECT Id FROM Video_Conference__c WHERE VTD1_Clinical_Study_Membership__c = :cas.Id LIMIT 1];
        parentRecords.put(Video_Conference__c.getSObjectType(), vConf);
        return parentRecords;
    }
    private static SObject getNewObj(String objTypeString, SObjectType objType, Map<SObjectType, SObject> parentRecords) {
        system.debug(objTypeString);
        String parentField = VT_D1_FieldPaths.STUDY_PATHS.get(objTypeString).substringBefore('.');
        if (parentField.endsWith('__r')) {
            parentField = parentField.substringBeforeLast('r') + 'c';
        } else if (!parentField.endsWith('__c') && !parentField.equalsIgnoreCase('Id')) {
            parentField += 'Id';
        }
        system.debug(parentField);
        system.debug(objType.getDescribe().fields.getMap().keySet());
        SObjectType parentType = objType.getDescribe().fields.getMap().get(parentField).getDescribe().getReferenceTo()[0];
        system.debug(parentType);
        SObject obj = objType.newSObject();
        obj.put(parentField, parentRecords.get(parentType).Id);
        return obj;
    }
}