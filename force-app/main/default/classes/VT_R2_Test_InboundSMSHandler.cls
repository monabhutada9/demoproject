/**
 * Created by MPlatonov on 14.02.2019.
 */

@isTest
public with sharing class VT_R2_Test_InboundSMSHandler {
    private static final String VTR2_eDiarySMSOptOutCode = '333';
    private static final String VTR2_NewMessagesSMSOptOutCode = '444';
    private static final String VTR2_VisitRemindersSMSOptOutCode = '555';

    @testSetup
    static void setup() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();

        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        study = [select VTR2_Send_eDiary_Notifications__c, VTR2_Send_New_Messages__c, VDT2_Send_Visit_Reminders__c,
                VTR2_eDiarySMSOptOutCode__c, VTR2_NewMessagesSMSOptOutCode__c, VTR2_VisitRemindersSMSOptOutCode__c, Name from HealthCloudGA__CarePlanTemplate__c where Id = : study.Id];
        study.VTR2_Send_eDiary_Notifications__c = true;
        study.VTR2_Send_New_Messages__c = true;
        study.VDT2_Send_Visit_Reminders__c = true;
        study.VTR2_eDiarySMSOptOutCode__c = VTR2_eDiarySMSOptOutCode;
        study.VTR2_NewMessagesSMSOptOutCode__c = VTR2_NewMessagesSMSOptOutCode;
        study.VTR2_VisitRemindersSMSOptOutCode__c = VTR2_VisitRemindersSMSOptOutCode;
        update study;
        Test.stopTest();

        VT_D1_TestUtils.createTestPatientCandidate(1);
        //VT_D1_TestUtils.createTestPatientCandidate(2);
        // trigger on VTD1_Actual_Visit__c raises mass email exception, delete it
        List <VTD1_Actual_Visit__c> visits = [select Id from VTD1_Actual_Visit__c];
        delete visits;
    }
    @isTest
    public static void test() {
        VT_R3_GlobalSharing.disableForTest = true;
        setUpMCSettings();
        Test.startTest();
        Case caseObj = [select Id, VTD1_Patient_User__c, VTD1_PI_user__c, ContactEmail, VTR2_Receive_SMS_eDiary_Notifications__c,
        VTR2_Receive_SMS_Visit_Reminders__c, VTR2_Receive_SMS_New_Notifications__c from Case];
        caseObj.VTR2_Receive_SMS_eDiary_Notifications__c = true;
        caseObj.VTR2_Receive_SMS_Visit_Reminders__c = true;
        caseObj.VTR2_Receive_SMS_New_Notifications__c = true;
        update caseObj;

        List <Account> accounts = [select Id, Name from Account where Name = 'CP1 CP1L'];
        for (Account account : accounts) {
            System.debug('acc = ' + account);
        }
        Id accountId = accounts[0].Id;
        String cgUserName = 'CG_' + accounts[0].Name;

        Contact patientContact = [select Id, Name from Contact where AccountId = : accountId];
        System.debug('patientContact = ' + patientContact);

        Id rtId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId();

        System.debug('creating contact for account = ' + accountId);
        String patientEmail = '' + caseObj.ContactEmail;
        String cgEmail = patientEmail.substring(0, patientEmail.length() - 10) + '0' + patientEmail.substring(patientEmail.length() - 10);
        Contact contact = new Contact(FirstName = cgUserName, VTD1_EmailNotificationsFromCourier__c = true,
                LastName = 'L', Email = cgEmail, RecordTypeId = rtId, AccountId = accountId, VTD1_Clinical_Study_Membership__c = caseObj.Id, VTD1_Primary_CG__c = true);
        insert contact;
        System.debug('contact = ' + contact);

        Profile profile = [select Id from Profile where Name = 'Caregiver'];

        User cgUser = new User(
                ProfileId = profile.Id,
                FirstName = cgUserName,
                LastName = 'L',
                Email = cgEmail,
                Username = VT_D1_TestUtils.generateUniqueUserName(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/New_York',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                IsActive = true,
                ContactId = contact.Id
        );
        insert cgUser;

        Contact PIContact = [
                select VTR2_Receive_SMS_New_Message__c, VTR2_Receive_SMS_Visit_Reminders__c, VTR2_Receive_SMS_eDiary_Notifications__c, AccountId
                , RecordType.Name from Contact
                where Id in (select ContactId from User where Id = :caseObj.VTD1_PI_user__c)
        ];
        PIContact.VTR2_Receive_SMS_New_Message__c = true;
        PIContact.VTR2_Receive_SMS_Visit_Reminders__c = true;
        PIContact.VTR2_Receive_SMS_eDiary_Notifications__c = true;
        System.debug('PIContact = ' + PIContact);
        System.debug('PIContact type = ' + PIContact.RecordType.Name);
        update PIContact;


        List <VT_D1_Phone__c> phones = new List<VT_D1_Phone__c>();
        phones.add(new VT_D1_Phone__c(VTD1_Contact__c = contact.Id, Account__c = accountId, PhoneNumber__c = '(555) 789-3215', Type__c = 'Mobile'));
        phones.add(new VT_D1_Phone__c(VTD1_Contact__c = PIContact.Id, Account__c = PIContact.AccountId, PhoneNumber__c = '(555) 789-3216', Type__c = 'Mobile'));
        phones.add(new VT_D1_Phone__c(VTD1_Contact__c = PIContact.Id, Account__c = PIContact.AccountId, PhoneNumber__c = '15557893217', Type__c = 'Mobile'));
        phones.add(new VT_D1_Phone__c(VTD1_Contact__c = patientContact.Id, Account__c = accountId, PhoneNumber__c = '(555) 789-3218', Type__c = 'Mobile'));
        phones.add(new VT_D1_Phone__c(VTD1_Contact__c = patientContact.Id, Account__c = accountId, PhoneNumber__c = '15557893219', Type__c = 'Mobile'));
        insert phones;

        phones = [select Id, PhoneNumber__c, VTR2_OptInFlag__c, VTR2_SMS_Opt_In_Status__c from VT_D1_Phone__c];
        for (VT_D1_Phone__c phone : phones) {
            phone.VTR2_OptInFlag__c = true;
        }

        update phones;

        phones = [select Id, PhoneNumber__c, VTR2_OptInFlag__c, VTR2_SMS_Opt_In_Status__c from VT_D1_Phone__c];

        for (VT_D1_Phone__c phone : phones) {
            System.debug('phone before income message = ' + phone);
        }

        //PIContact.VTR2_Receive_SMS_Visit_Reminders__c = false;
        //update PIContact;

        List <VTR2_InboundSMS__c> inboundSMS = new List<VTR2_InboundSMS__c>();
        /*inboundSMS.add(new VTR2_InboundSMS__c(VTR2_MessageText__c = VTR2_eDiarySMSOptOutCode, VTR2_PhoneNumber__c = phones[1].PhoneNumber__c));
        inboundSMS.add(new VTR2_InboundSMS__c(VTR2_MessageText__c = VTR2_NewMessagesSMSOptOutCode, VTR2_PhoneNumber__c = phones[1].PhoneNumber__c));
        inboundSMS.add(new VTR2_InboundSMS__c(VTR2_MessageText__c = VTR2_VisitRemindersSMSOptOutCode, VTR2_PhoneNumber__c = phones[1].PhoneNumber__c));
        inboundSMS.add(new VTR2_InboundSMS__c(VTR2_MessageText__c = '1234', VTR2_PhoneNumber__c = phones[1].PhoneNumber__c));

        inboundSMS.add(new VTR2_InboundSMS__c(VTR2_MessageText__c = VTR2_VisitRemindersSMSOptOutCode, VTR2_PhoneNumber__c = phones[2].PhoneNumber__c));
        inboundSMS.add(new VTR2_InboundSMS__c(VTR2_MessageText__c = VTR2_eDiarySMSOptOutCode, VTR2_PhoneNumber__c = phones[0].PhoneNumber__c));
        inboundSMS.add(new VTR2_InboundSMS__c(VTR2_MessageText__c = '2345', VTR2_PhoneNumber__c = phones[0].PhoneNumber__c));
        */
        inboundSMS.add(new VTR2_InboundSMS__c(VTR2_MessageText__c = 'YES', VTR2_PhoneNumber__c = phones[2].PhoneNumber__c));

        insert inboundSMS;

        phones = [select Id, PhoneNumber__c, VTR2_OptInFlag__c, VTR2_SMS_Opt_In_Status__c from VT_D1_Phone__c];

        for (VT_D1_Phone__c phone : phones) {
            System.debug('phone after income message = ' + phone);
        }

        inboundSMS = new List<VTR2_InboundSMS__c>();
        inboundSMS.add(new VTR2_InboundSMS__c(VTR2_MessageText__c = 'STOP', VTR2_PhoneNumber__c = phones[2].PhoneNumber__c));
        inboundSMS.add(new VTR2_InboundSMS__c(VTR2_MessageText__c = 'XYZ', VTR2_PhoneNumber__c = phones[2].PhoneNumber__c));
        inboundSMS.add(new VTR2_InboundSMS__c(VTR2_MessageText__c = 'YES', VTR2_PhoneNumber__c = phones[2].PhoneNumber__c));
        inboundSMS.add(new VTR2_InboundSMS__c(VTR2_MessageText__c = 'YES', VTR2_PhoneNumber__c = phones[3].PhoneNumber__c));
        inboundSMS.add(new VTR2_InboundSMS__c(VTR2_MessageText__c = 'HELP', VTR2_PhoneNumber__c = phones[3].PhoneNumber__c));
        insert inboundSMS;

        phones = [select Id, PhoneNumber__c, VTR2_OptInFlag__c, VTR2_SMS_Opt_In_Status__c from VT_D1_Phone__c];

        for (VT_D1_Phone__c phone : phones) {
            System.debug('phone after income message = ' + phone);
        }
        Test.stopTest();

    }
    static void setUpMCSettings() {
        List<VTR2_McloudSettings__mdt> mcloudSettings = new List<VTR2_McloudSettings__mdt>();
        mcloudSettings.add(new VTR2_McloudSettings__mdt(VTR2_OutboundMessageKey_API_Triggered__c = 'NjU6Nzg6MA',
                VTR2_RestURI__c = 'https://mc2gwv28hdsr86gd-sldnjzvzzgm.rest.marketingcloudapis.com/',
                VTR2_ShortCode__c = '99808'));
        VT_R2_McloudBroadcaster.setMCsettings(mcloudSettings);
    }
}