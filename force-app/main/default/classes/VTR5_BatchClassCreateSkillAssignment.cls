/*
Created By - Yogesh More
Created Date - 17/01/2020
Version - 1.0
Story No. - SH-9363/SH-9180
Discription - This is the batch class to Create Service Resource and Service Resource skill based on user language.
*/

global class VTR5_BatchClassCreateSkillAssignment implements Database.Batchable<sObject>, Database.Stateful {

    public string query;
    public List<String> errorLogList = new List<String>();
    
    
    global VTR5_BatchClassCreateSkillAssignment(String queryParam){
        query = queryParam; // getting query as parameter
    }
        
    global Database.QueryLocator start(Database.BatchableContext BC){
            
        //List<String> profileList =Label.VT_R5_ProfileName.split(',');
        //String query = 'Select id,Name,IsActive,LanguageLocaleKey,VT_R5_Secondary_Preferred_Language__c,VT_R5_Tertiary_Preferred_Language__c from User where Profile.Name IN : profileList';
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<user> userList){
        
        // Service Resource and Service Resource skill based on user language.
        String result = VTR5_CreateSkillAssignment.CreateSkillAssignments(userList);
        System.debug('result==>'+result);
        if(result != 'Success'){
            errorLogList.add(result);
        }
    }
    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
        If(errorLogList != null && errorLogList.size()>0){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        string[] to = new string[] {UserInfo.getUserEmail()};
        email.setToAddresses(to);
        email.setSubject('Error - Service Resourse and skill creation failed');        
        email.setHtmlBody('Hello, <br/><br/>Batch job get failed while creation of records. Please check below details.<br/><br/>'+JSON.Serialize(errorLogList)+'<br/><br/>');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }
    }
}