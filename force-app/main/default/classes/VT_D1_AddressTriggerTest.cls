/**
 * Created by Riabchenko Alona on 19/04/07.
 */
@isTest
public without sharing class VT_D1_AddressTriggerTest {
    private static Id accountRecordTypeIdOfSponsor = Account.getSObjectType().getDescribe().getRecordTypeInfosByName().get('Sponsor').getRecordTypeId();
    private static Id caseRecordTypeIdOfPcf = Case.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('VTD1_PCF').getRecordTypeId();

    public static void updateAddressTest() {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeId(accountRecordTypeIdOfSponsor)
                );
        DomainObjects.Contact_t con = new DomainObjects.Contact_t();
        DomainObjects.User_t user = new DomainObjects.User_t()
                .addContact(con)
                .setProfile('Site Coordinator');
        DomainObjects.User_t userPI = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Primary Investigator');
        DomainObjects.VirtualSite_t virtSite = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .setStudySiteNumber('12345');
        DomainObjects.StudyTeamMember_t stmPI = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(userPI)
                .addVirtualSite(virtSite)
                .setRecordTypeByName('PI');
        DomainObjects.StudyTeamMember_t stmSCR = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(user)
                .setRecordTypeByName('SCR');
        DomainObjects.StudySiteTeamMember_t sstm = new DomainObjects.StudySiteTeamMember_t()
                .setAssociatedPI2(stmPI)
                .setAssociatedScr(stmSCR);
        sstm.persist();
        DomainObjects.Account_t acc = new DomainObjects.Account_t();
        DomainObjects.Case_t cas = new DomainObjects.Case_t()
                .setRecordTypeId(caseRecordTypeIdOfPcf)
                .addStudy(study)
                .addPIUser(userPI)
                .addVirtualSite(virtSite)
                .addContact(con)
                .addAccount(acc);
        cas.persist();
        DomainObjects.Address_t address = new DomainObjects.Address_t()
                .addContact(con)
                .setPrimary(true)
                .addVTD1_Patient(acc);
        address.persist();
        //System.debug('Before Address --- ' + address);

        Test.startTest();
        Address__c addressRec = (Address__c) address.toObject();
        addressRec.City__c = 'City';
        update addressRec;
        //System.debug('After Update Address --- ' + addressRec);
        Test.stopTest();

    }
}