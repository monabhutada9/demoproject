/**
 * Created by Alexey Mezentsev on 3/23/2020.
 */

@IsTest
private class VT_R4_BatchActualVisitsCreationTest {
    @TestSetup
    static void setupMethod() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Study__c = study.Id;
        protocolVisit.VTD1_Range__c = 4;
        protocolVisit.VTD1_VisitOffset__c = 2;
        protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisit.VTD1_VisitType__c = 'Labs';
        insert protocolVisit;
    }

    @IsTest
    static void batchInsert() {
        Case cas = [
                SELECT Id, ContactId, Preferred_Lab_Visit__c, VTD1_Study__c, VTD1_Study__r.VTD1_IMP_Return_Numbers__c,
                        VTD1_Study__r.VTD1_Lab_Return_Numbers__c,
                        VTD1_Study__r.VTD1_Return_Process__c, VTD1_Subject_ID__c,
                        VTD1_Study__r.VTD1_Lab_Return_Process__c, VTD1_Virtual_Site__c,
                        VTR4_CandidatePatient__c
                FROM Case
        ];
        Integer visitCount = [SELECT Id FROM VTD1_Actual_Visit__c WHERE VTD1_Case__c =: cas.Id].size();
        Test.startTest();
        Database.executeBatch(new VT_R4_BatchActualVisitsCreation(cas, cas.VTR4_CandidatePatient__c, true));
        Test.stopTest();
        System.assertEquals(++visitCount, [SELECT Id FROM VTD1_Actual_Visit__c WHERE VTD1_Case__c =: cas.Id].size());
    }
}