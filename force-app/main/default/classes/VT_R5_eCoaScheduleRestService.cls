/**
* @author: Carl Judge
* @date: 14-Aug-20
* @description: Service to receive post back from Mulesoft after requesting a schedule. Note that Mulesoft can also upsert schedules directly. This class is currently only used after STOP diary notifications
 * as if diaries have been removed from the schedule we will also need to do a delete as well as an upsert.
**/

@RestResource(UrlMapping='/eCoaScheduleService')
global without sharing class VT_R5_eCoaScheduleRestService {
    @HttpPost
    global static void processSchedule() {
        RestResponse response = RestContext.response;
        response.statusCode = 200;
        response.responseBody = Blob.valueOf('OK');

        RestRequest req = RestContext.request;
        String body = req.requestBody.toString();

        VT_R5_eCoaScheduleRestService.ScheduleResponse resp = (VT_R5_eCoaScheduleRestService.ScheduleResponse)JSON.deserialize(body, VT_R5_eCoaScheduleRestService.ScheduleResponse.class);
        if (!resp.subjectsScheduleStatus.isEmpty()) {
            List<VTD1_Survey__c> surveys = new List<VTD1_Survey__c>();
            Set<String> compositeKeys = new Set<String>();
            Id externalRecTypeId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('VTR5_External').getRecordTypeId();
            Set<Id> doDeletePatientIds = new Set<Id>(); // any user Ids in here will have uncompleted ecoa diaries deleted if they are not in the received schedule

            for (ScheduleItem item : resp.subjectsScheduleStatus) {
                String compositeKey = item.subjectGuid + item.ruleGuid + item.windowBegin;
                if (!compositeKeys.contains(compositeKey)) {
                    compositeKeys.add(compositeKey);
                    surveys.add(new VTD1_Survey__c(
                        VTD1_CSM__c = item.clinicalStudyMembership,
                        VTD1_Patient_User_Id__c = item.userId,
                        RecordTypeId = externalRecTypeId,
                        Name = item.diaryName,
                        VTR5_eCoa_ruleGuid__c = item.ruleGuid,
                        VTR5_eCoa_RuleName__c = item.ruleName,
                        VTD1_Date_Available__c = Datetime.newInstance(item.windowBegin),
                        VTD1_Due_Date__c = Datetime.newInstance(item.windowEnd + 1),
                        VTR5_eCoaCompositeKey__c = compositeKey
                    ));
                    if (item.stopDiary) {
                        doDeletePatientIds.add(item.userId);
                    }
                }
            }
            Database.upsert(surveys, VTD1_Survey__c.VTR5_eCoaCompositeKey__c, false);
//            upsert surveys VTR5_eCoaCompositeKey__c;

            if (!doDeletePatientIds.isEmpty()) {
                VT_D1_SurveyHandler.ignoreDeleteValidation = true;
                List<VTD1_Survey__c> surveyForDelete = [
                    SELECT Id
                    FROM VTD1_Survey__c
                    WHERE RecordTypeId = :externalRecTypeId
                    AND VTD1_Status__c != :VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_COMPLETED
                    AND VTR5_eCoaCompositeKey__c NOT IN :compositeKeys
                    AND VTD1_Patient_User_Id__c IN :doDeletePatientIds
                    AND VTD1_Due_Date__c >= :Datetime.now()
                    AND VTD1_Date_Available__c >= TODAY // temporary for SH-18788 so that multi-day diaries dont get deleted after their first day
                ];
                if(!surveyForDelete.isEmpty()) {
                    delete surveyForDelete;
                    Database.emptyRecycleBin(surveyForDelete);
            	}
        	}
    	}


    }


    public class ScheduleResponse {
        public List<ScheduleItem> subjectsScheduleStatus;
    }
    public class ScheduleItem {
        public Id userId;
        public String subjectGuid;
        public Id clinicalStudyMembership;
        public Boolean stopDiary;
        public String ruleGuid;
        public String ruleName;
        public String diaryName;
        public Long windowBegin;
        public Long windowEnd;
    }
}