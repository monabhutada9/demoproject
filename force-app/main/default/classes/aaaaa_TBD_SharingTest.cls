@IsTest
public with sharing class aaaaa_TBD_SharingTest {
    @TestSetup
    static void setup() {

        VT_R3_GlobalSharing.disableForTest = false;
        Test.startTest();
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'Primary Investigator'];
        Contact contact = new Contact(LastName = 'PI contact', Email = 'PI@email.com');
        insert contact;

        DomainObjects.Study_t study = new DomainObjects.Study_t() 
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );
        HealthCloudGA__CarePlanTemplate__c TestProt = new HealthCloudGA__CarePlanTemplate__c(Id = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1].Id);
        TestProt.Name = 'TestProt';
        update TestProt;

        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();

        User piUser = new User(
            ProfileId = prof.Id,
            ContactId = contact.Id,
            FirstName = 'PITestUserForSharing',
            LastName = 'PITestUserForSharing',
            Email = VT_D1_TestUtils.generateUniqueUserName(),
            Username = VT_D1_TestUtils.generateUniqueUserName(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            IsActive = true
        );
        System.runAs (new User(Id = UserInfo.getUserId())) {
            insert piUser;
        }
        insert new Study_Team_Member__c(Study__c = study.Id, User__c = piUser.Id);

        // DomainObjects.VirtualSite_t virtSite = new DomainObjects.VirtualSite_t()
        //         .addStudy(study)
        //         .setStudySiteNumber('12345')
        //         .addStudyTeamMember(stm);
        //     virtSite.persist();

        new DomainObjects.HealthCloudGA_CandidatePatient_t()
                .setCaregiverEmail(DomainObjects.RANDOM.getEmail())

                .setRr_firstName('Mike')
                .setRr_lastName('Birn')
                .setConversion('Converted')
                .addStudy(study)
                .persist();

         Test.stoptest(); 
    }  

    @IsTest
    public static void testSharing() {
        Case carePlan = [SELECT Id, VTD1_Study__c, VTD1_Patient_User__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];
        system.assertNotEquals(null, carePlan);
        User usr = [SELECT id FROM User WHERE userName = 'test15052020_1@email.com'];
        System.runAs(usr) {
            Case carePlanUsrContext = [SELECT Id, VTD1_Study__c, VTD1_Patient_User__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];
            system.assertNotEquals(null, carePlanUsrContext);
        }
    }
    
    @IsTest
    public static void ediaryFilterForPITest() {
        Case ptCase = [SELECT Id, VTD1_Study__c,VTD1_PI_user__c FROM Case LIMIT 1];
        System.debug('ptCase---'+ptCase);
        /*List<Study_Team_Member__c> studyTeamMembers = [select Id,
                                                              User__c, 
                                                       		  User__r.Profile.Name 
                                                       from Study_Team_Member__c 
                                                       where Study__c = :ptCase.VTD1_Study__c AND User__r.Profile.Name =: VT_R4_ConstantsHelper_Profiles.PI_PROFILE_NAME
                                                       AND VTD1_VirtualSite__c <> NULL LIMIT 1];*/
        VTR5_eDiary_Filter__c objeDiaryFilter = new VTR5_eDiary_Filter__c(
            VTR5_eDiary_Name__c = 'test2test2' ,
            VTR5_Label_Name__c = 'Test1',
           // VTR5_External_Widget_Key__c = 'Unwell?',
            VTR5_Study__c = ptCase.VTD1_Study__c,
            VTR5_Answer_Content__c = 'YES'
        );
        insert objeDiaryFilter;
        System.assertNotEquals(null, objeDiaryFilter.Id);


        List<VTD1_Survey_Answer__c> lstSurveyAnswers = [SELECT    Id,
                                                                  VTD1_Survey__c,
                                                                  VTD1_Answer__c
                                                        FROM VTD1_Survey_Answer__c
                                                       /* WHERE VT_R5_External_Widget_Data_Key__c  =: objeDiaryFilter.VTR5_External_Widget_Key__c
                                                        AND VTD1_Survey__r.Name = 'test2test2'*/ LIMIT 1];
        System.assertEquals(1, lstSurveyAnswers.size());
        //Need to create share records as VTD1_Survey_Answer__c records are not shared with PI in test Methods.
        //createShareRecord(studyTeamMembers[0].User__c, ptCase.VTD1_PI_user__c);

         Map<String, Object> tableParams = new Map<String, Object>{
                'limit' => (Object) '1000',
                'filterMap' => (Object) new Map<String, Object> {
                        'markReviewedFilter' => (Object) 'false',
                        'statusFilters' => null,
                        'eDiaryNameFilters' => (Object) new List<Object>(),
                        'studyId' => ptCase.VTD1_Study__c
                },
                'selectedResponse' => (Object) new Map<String, Object> {
                        'VTR5_eDiary_Name__c' => 'test2test2',
                        'VTR5_Label_Name__c' => '',
                        'VTR5_Study__c' => ptCase.VTD1_Study__c,
                        'VTR5_Answer_Content__c' => 'YES',
                        'VTR5_External_Widget_Key__c' => 'Unwell?'
                }
        };


        Test.startTest();
        System.runAs(new User(Id = ptCase.VTD1_PI_user__c)){
           System.debug('lstSurveyAnswers---'+ [SELECT    Id,
                                                                  VTD1_Survey__c,
                                                                  VTD1_Answer__c
                                                        FROM VTD1_Survey_Answer__c/*
                                                        WHERE VT_R5_External_Widget_Data_Key__c  =: objeDiaryFilter.VTR5_External_Widget_Key__c
                                                        AND VTD1_Survey__r.Name = 'test2test2'*/ LIMIT 1]);
            VT_R5_EDiariesTableController result = VT_R5_EDiariesTableController.getEdiaryRecords(null, JSON.serialize(tableParams), null, true);
            System.assertEquals(lstSurveyAnswers[0].VTD1_Survey__c, result.items[0].eDiary.Id);
        }
        Test.stopTest();
    }
    public static void createShareRecord(Id userId, Id caseId){
        CaseShare objVirtualShare = new CaseShare();
        objVirtualShare.CaseId = caseId;
        objVirtualShare.UserOrGroupId = userId;
        objVirtualShare.CaseAccessLevel = 'Edit';
        objVirtualShare.RowCause = 'Manual';
        insert objVirtualShare;
    }
}