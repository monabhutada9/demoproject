@isTest
public with sharing class VT_R3_RestPatientChatHistoryTest {

    
    public static void getChatHistoriesTest() {
        List<User> userList = [SELECT Id, ContactId, Name FROM User WHERE ContactId != null AND IsActive = true AND Name != null LIMIT 1];
        List<LiveChatVisitor> visitors = new List<LiveChatVisitor>();
        LiveChatVisitor visitor = new LiveChatVisitor();
//		LiveChatVisitor visitor1 = new LiveChatVisitor();
//		LiveChatVisitor visitor2 = new LiveChatVisitor();

        visitors.add(visitor);
//		visitors.add(visitor1);
//		visitors.add(visitor2);
        insert visitors;

        Case cs = VT_D1_TestUtils.createCase('VTD1_PCF', userList[0].ContactId, null, null, null, null, null);
//		Case cs1 = VT_D1_TestUtils.createCase('VTD1_PCF', null, null, null, null, null, null);
//		Case cs2 = VT_D1_TestUtils.createCase('VTD1_PCF', null, null, null, null, null, null);
        List<Case> cases = new List<Case>();
        cases.add(cs);
        insert cases;


        List<LiveChatTranscript> liveChatTranscriptList = new List<LiveChatTranscript>();
        LiveChatTranscript liveChatTranscript = new LiveChatTranscript();
        liveChatTranscript.LiveChatVisitorId = visitor.Id;
        liveChatTranscript.CaseId = cs.Id;
        liveChatTranscript.ContactId = userList[0].ContactId;
        liveChatTranscript.Body = '<p align="center">Chat Started: Wednesday, June 06, 2018, 10:56:58 (+0000)</p>'
                + '<p align="center">Chat Origin: Chat with Patient</p>'
                + '<p align="center">Agent Study Concierge</p>( 20s ) Nic: 11<br>( 26s ) Study Concierge: 22<br>( 1h 17m 45s ) '
                + userList[0].Name.split('(\\s|&nbsp;)') + ': qwerty<br>';
        liveChatTranscript.Status = 'completed';
        liveChatTranscript.OwnerId = userList[0].Id;
        liveChatTranscript.Case = cs;
        liveChatTranscript.EndTime = System.now();


        liveChatTranscriptList.add(liveChatTranscript);

//		LiveChatTranscript liveChatTranscript1 = new LiveChatTranscript();
//		liveChatTranscript1.LiveChatVisitorId = visitor1.Id;
//		liveChatTranscript1.CaseId = cs1.Id;
//		liveChatTranscript1.ContactId = userList[1].ContactId;
//		liveChatTranscript1.Status = 'InProgress';
//
//		liveChatTranscriptList.add(liveChatTranscript1);
//
//		LiveChatTranscript liveChatTranscript2 = new LiveChatTranscript();
//		liveChatTranscript2.LiveChatVisitorId = visitor2.Id;
//		liveChatTranscript2.CaseId = cs2.Id;
//		liveChatTranscript2.ContactId = userList[2].ContactId;
//		liveChatTranscript2.Status = 'missed';
//		liveChatTranscript2.Body = null;
//
//		liveChatTranscriptList.add(liveChatTranscript2);
        DomainObjects.Study_t st = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t();
        DomainObjects.User_t userPatient = new DomainObjects.User_t()
                .setEmail('RestPatientCalendarTestPatient@test.com')
                .addContact(patientContact)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        userPatient.persist();
        DomainObjects.Case_t casePatient = new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .addStudy(st)
                .addUser(userPatient)
                .addContact(patientContact)
                .setStatus('Screened');
        casePatient.persist();
        patientContact.addVTD1_Clinical_Study_Membership(casePatient);
        Contact pContact = (Contact) patientContact.record;
        Case caseCase = (Case) casePatient.record;
        pContact.VTD1_Clinical_Study_Membership__c = caseCase.Id;
        update pContact;
        User patient = (User) userPatient.record;

        insert liveChatTranscriptList;

        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/Patient/ChatHistories';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = response;
        System.runAs(patient) {
            System.debug('HEREEE ' + [
                    SELECT Id, Body, CaseId, Case.Subject, CreatedDate, OwnerId, Owner.Name
                    FROM LiveChatTranscript
                    WHERE CaseId != NULL AND Case.RecordType.Id IN :VT_D1_HelperClass.getRecordTypePCF()
                    AND ContactId IN (SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId())
                    ORDER BY CreatedDate DESC
            ]);
            String responsString = VT_R3_RestPatientChatHistory.getChatHistories();
        }
//		System.runAs(userList[0]){
//			String responsString = VT_R3_RestPatientChatHistory.getChatHistories();
//		}
//		System.runAs(userList[2]){
//			String responsString = VT_R3_RestPatientChatHistory.getChatHistories();
//		}

//		System.assertEquals(200, response.statusCode);
    }
}