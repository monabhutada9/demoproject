/**
 * Created by Yuliya Yakushenkova on 10/23/2020.
 */

public class VT_R5_MobilePISCRCalendar extends VT_R5_MobileRestResponse implements VT_R5_MobileLibrary.Routable {

    public List<String> getMapping() {
        return new List<String>{
                '/mobile/{version}/calendar'
        };
    }

    public VT_R5_MobileRestResponse versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        return get_v1();
                    }
                }
            }
        }
        return null;
    }

    public VT_R5_MobileRestResponse get_v1() {
        return this.buildResponse(
                VT_R5_PISCRCalendarService.getInstance().getPreviewVisitsAndMembersByUserId(UserInfo.getUserId())
        );
    }

    public void initService() {
    }

    public Map<String, String> getMinimumVersions() {
        return new Map<String, String>{
                'v1' => '5.5'
        };
    }
}