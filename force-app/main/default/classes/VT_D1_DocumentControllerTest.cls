@IsTest
public with sharing class VT_D1_DocumentControllerTest {

    private static final String QUERY_SETTINGS = '{"sortingParams":{"field":"ISF","order":"DESC"},"filterParams":[{"field":"RegulatoryDocumentType","filterValue":["option1", "option3"]},{"field":"DocuSignStatus","filterValue":["option1"]},{"field":"DocuSignStatus","filterValue":null}],"entriesOnPage":10,"currentPage":1}';
    private static final String QUERY_SETTINGS2 = '{"sortingParams":{"field":"ContentDocument.CreatedDate","order":"DESC"},"filterParams":[{"field":"","filterValue":null}],"entriesOnPage":10,"currentPage":1}';

    public static void getDocumentsTest() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);

        //User patient = [SELECT Id, ContactId FROM User WHERE Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME AND ContactId != null AND IsActive = true LIMIT 1];
        //User PI = [SELECT Id, ContactId FROM User WHERE Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME AND ContactId != null AND IsActive = true LIMIT 1];

        List<Study_Team_Member__c> studyTeamMembers = [SELECT User__c FROM Study_Team_Member__c WHERE Study__c = :study.Id AND User__r.Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME];
        User PI;
        if (!studyTeamMembers.isEmpty()) {
            PI = [SELECT Id, ContactId FROM User WHERE Id = :studyTeamMembers[0].User__c];
        }
        System.assertNotEquals(null, PI);
        //System.debug('testStudyTeamMembers '+ PI.Id +' '+ studyTeamMembers);

        //Case caseOpen = VT_D1_TestUtils.createCase('CarePlan', patient.ContactId, PI.ContactId, null, null, null, null);
        //caseOpen.VTD1_Study__c = study.Id;

        VTD1_Regulatory_Document__c rd = new VTD1_Regulatory_Document__c(
                Name = 'Test doc',
                VTR2_Way_to_Send_with_DocuSign__c = 'Manual',
                VTD1_Document_Type__c = 'Site Activation Approval Form',
                OwnerId = PI.Id
        );
        insert rd;
        VTD1_Regulatory_Binder__c rb = new VTD1_Regulatory_Binder__c(
                VTD1_Regulatory_Document__c = rd.Id,
                VTD1_Care_Plan_Template__c = study.Id
        );
        insert rb;

        List<VTD1_Document__c> documentList = new List<VTD1_Document__c>();
        for (Integer i = 0; i < 1; i++) {
            VTD1_Document__c document = new VTD1_Document__c(
                    VTD1_Regulatory_Binder__c = rb.Id,
                    VTD1_Study__c = study.Id,
                    RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT,
                    OwnerId = PI.Id
            );
            documentList.add(document);
        }
        insert documentList;
        Test.startTest();
        List<ContentVersion> cvList = new List<ContentVersion>();
        for (Integer i = 0; i < 3; i++) {
            ContentVersion version = new ContentVersion(
                    Title = 'ABC' + i,
                    PathOnClient = 'test',
                    VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body')
            );
            cvList.add(version);
        }
        insert cvList;

        //Test.startTest();
        //Test.stopTest();

        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        for (ContentVersion cv : [SELECT Id, ContentDocumentId, Title FROM ContentVersion WHERE Id IN :cvList]) {
            ContentDocumentLink cdl = new ContentDocumentLink(
                    ContentDocumentId = cv.ContentDocumentId,
                    LinkedEntityId = documentList[0].Id,
                    ShareType = 'I',
                    Visibility = 'AllUsers'
            );
            cdlList.add(cdl);
        }

        insert cdlList;

        List<ContentDocument> cdList = [SELECT Id, ParentId, Title FROM ContentDocument];

        System.debug('documentList ' + documentList);
        System.debug('cdList ' + cdList);
        System.debug('cvList ' + cvList);
        System.debug('cdlList ' + cdlList);

        List<VT_D1_DocumentController.DocumentWrapper> documents;
        System.debug('runAs: ' + PI.Id);

        System.runAs(PI) {

            try {

                    documents = VT_D1_DocumentController.getDocuments(study.Id);

            } catch (DmlException exc) {
                System.assertEquals(true, exc.getMessage().contains('INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY'));

                System.assertEquals(false, documents.isEmpty());
            }
        }
        Test.stopTest();
    }

    public static void getDocumentsSCRTest() {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'))
                .setOriginalName('tN')
                .setStudyAdminId(UserInfo.getUserId());
        DomainObjects.Contact_t scrCon = new DomainObjects.Contact_t()
                .setLastName('Con1');
        DomainObjects.User_t scrUser = new DomainObjects.User_t()
                .addContact(scrCon)
                .setProfile('Site Coordinator');
        DomainObjects.StudyTeamMember_t stmSCR = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(scrUser)
                .setRecordTypeByName('SCR');
        DomainObjects.VirtualSite_t virtualSite = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .setStudySiteNumber('12345')
                .addStudyTeamMember(stmSCR);

        virtualSite.persist();
        Study_Team_Member__c studyTeamMember = [SELECT Id, Study__c, User__c FROM Study_Team_Member__c LIMIT 1];
        try{
            insert new HealthCloudGA__CarePlanTemplate__share(AccessLevel = 'Edit', ParentId = studyTeamMember.Study__c, UserOrGroupId = studyTeamMember.User__c);
        }
        catch(Exception ex){
            system.debug('sharing exception ' + ex.getMessage());
        }
        Virtual_Site__c vs = [select id, VTD1_Study__c, VTD1_Study_Team_Member__r.User__c from Virtual_Site__c limit 1];
        Test.startTest();
        VTD1_Document__c document = (VTD1_Document__c) new DomainObjects.VTD1_Document_t()
                .addCase(new DomainObjects.Case_t())
                .setRecordTypeId(VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD)//RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT)
                .setVTD1_Current_Workflow('Medical Records flow')
                .setStatus('Approved')
                .addStudy(new DomainObjects.Study_t())
                .persist();
        ContentVersion contentVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();
        ContentVersion insertedContVersion = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        DomainObjects.ContentDocumentLink_t docLink = new DomainObjects.ContentDocumentLink_t()
                .withLinkedEntityId(document.Id)
                .withContentDocumentId(insertedContVersion.ContentDocumentId);
        ContentDocumentLink siteLink = (ContentDocumentLink) new DomainObjects.ContentDocumentLink_t()
                .withLinkedEntityId(vs.Id)
                .withContentDocumentId(insertedContVersion.ContentDocumentId)        
                .persist();
        try {
            System.runAs(new User(Id = studyTeamMember.User__c)){
                VT_D1_DocumentController.getContentDocumentLinks(studyTeamMember.Id, QUERY_SETTINGS2, true);
                VT_D1_DocumentController.getDocumentsSCR(studyTeamMember.Id, QUERY_SETTINGS);
            }
        } catch (Exception exc) {
            System.assertEquals('Script-thrown exception', exc.getMessage());
        }
        Test.stopTest();
    }

    public static void uploadFileTest() {
        VTD1_Document__c document = (VTD1_Document__c) new DomainObjects.VTD1_Document_t()
                .addCase(new DomainObjects.Case_t())
                .setRecordTypeId(VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT)
                .addStudy(new DomainObjects.Study_t())
                .persist();
        ContentVersion contentVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();
        ContentVersion insertedContVersion = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        new DomainObjects.ContentDocumentLink_t()
                .withLinkedEntityId(document.Id)
                .withContentDocumentId(insertedContVersion.ContentDocumentId)
                .persist();
        VTD1_Document__c documents = [SELECT Id FROM VTD1_Document__c LIMIT 1];

        Test.startTest();
        Id uploadedFileId = VT_D1_DocumentController.uploadFile(documents.Id, 'test', 'test');
        Test.stopTest();

        System.assert(String.isNotBlank(uploadedFileId), 'Incorrect file Id');
    }

    public static void uploadFileTest_NoContentDocumentLink() {
        VTD1_Document__c document = (VTD1_Document__c) new DomainObjects.VTD1_Document_t()
                .addCase(new DomainObjects.Case_t())
                .setRecordTypeId(VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT)
                .addStudy(new DomainObjects.Study_t())
                .persist();
        ContentVersion contentVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();

        VTD1_Document__c doc = [SELECT Id FROM VTD1_Document__c LIMIT 1];

        Test.startTest();
            Id uploadedFileId = VT_D1_DocumentController.uploadFile(doc.Id, 'test', 'test');
            ContentDocumentLink contentDocumentLink = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :doc.Id LIMIT 1];
        Test.stopTest();

        System.assertEquals(doc.Id, contentDocumentLink.LinkedEntityId, 'Incorrect LinkedEntityId');
        System.assert(String.isNotBlank(uploadedFileId), 'Incorrect file Id');
    }

    public static void standardUploadTest(){
        VTD1_Document__c document = (VTD1_Document__c) new DomainObjects.VTD1_Document_t()
                .addCase(new DomainObjects.Case_t())
                .setRecordTypeId(VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT)
                .addStudy(new DomainObjects.Study_t())
                .persist();
        ContentVersion firstVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();
        ContentVersion nextVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();

        VTD1_Document__c doc = [SELECT Id FROM VTD1_Document__c LIMIT 1];
        Id uploadedFirstDocId = [Select ContentDocumentId from ContentVersion where Id =: firstVersion.Id].ContentDocumentId;
        Id uploadedNextDocId = [Select ContentDocumentId from ContentVersion where Id =: nextVersion.Id].ContentDocumentId;
        Test.startTest();
            VT_D1_DocumentController.convertToNewVersion(doc.Id, uploadedFirstDocId);
            VT_D1_DocumentController.convertToNewVersion(doc.Id, uploadedNextDocId);
            ContentDocumentLink contentDocumentLink = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :doc.Id LIMIT 1];
        Test.stopTest();

        System.assertEquals(doc.Id, contentDocumentLink.LinkedEntityId, 'Incorrect LinkedEntityId');
    }
}