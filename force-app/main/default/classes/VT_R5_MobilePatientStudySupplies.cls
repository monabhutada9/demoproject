/**
 * @author: Alexander Komarov
 * @date: 15.10.2020
 * @description:
 */

public with sharing class VT_R5_MobilePatientStudySupplies extends VT_R5_MobileRestResponse implements VT_R5_MobileRestRouter.Routable {

    public List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/study-supplies',
                '/patient/{version}/study-supplies/{id}',
                '/patient/{version}/study-supplies/{id}/{action}'
        };
    }

    private static final String CONFIRM_DELIVERY = 'confirm-delivery';
    private static final String CONFIRM_DROP = 'confirm-drop';
    private static final String CONFIRM_PICKUP = 'confirm-pickup';
    private static final String CONFIRM_PACKAGING_MATERIALS = 'confirm-packaging-materials';
    private static final String REQUEST_REPLACEMENT = 'request-replacement';

    public void versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        String requestId = parameters.get('id');
        String requestAction = parameters.get('action');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        if (requestAction == null) {
                            get_v1(requestId);
                            return;
                        } else {
                            if (CONFIRM_DELIVERY.equals(requestAction)) {
                                get_v1_confirm(requestId);
                                return;
                            }
                        }

                    }
                }
            }
            when 'POST' {
                switch on requestVersion {
                    when 'v1' {
                        post_v1(requestId, requestAction);
                        return;
                    }
                }
            }
        }
        VT_R5_MobileRestRouter.unsupportedVersionResponse();
        this.addError('unsupported version');
        this.sendResponse();
    }

    private void post_v1(String requestId, String action) {
        try {
            RequestParams params = (RequestParams) JSON.deserialize(this.request.requestBody.toString(), RequestParams.class);
            String delName = [SELECT Id, Name FROM VTD1_Order__c WHERE Id = :requestId LIMIT 1].Name;
            if (action.equals(CONFIRM_PACKAGING_MATERIALS)) {
                VT_D1_MyStudySuppliesItemController.confPackMatHand(requestId, params.needPackagingMaterials);
                this.addBannerMessage(Label.VTR3_Confirm_Packaging_Materials_Request);
            } else if (action.equals(REQUEST_REPLACEMENT)) {
                Case c = VT_D1_PatientCaregiverBound.getCaseForUser();
                String patientName = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
                if (params.isIMP) {
                    VT_D1_MyStudySuppliesItemController.confReqReplDelivery(c.Id, params.kitId, delName, patientName, params.isIMP, params.reason, params.comment);
                } else {
                    VT_D1_MyStudySuppliesItemController.confReqReplDelivery(c.Id, params.kitId, delName, patientName, params.isIMP, null, null);
                }
                this.addBannerMessage(Label.VTR3_Kit_Replacement_Request.replace('#1', delName));
            } else if (action.equals(CONFIRM_PICKUP) || action.equals(CONFIRM_DROP)) {
                Datetime d = Datetime.newInstance(params.dateTimeUnix - timeOffset);
                if (action.equals(CONFIRM_DROP)) {
                    VT_D1_MyStudySuppliesItemController.confDropOff(requestId, d);
                } else {
                    VT_D1_MyStudySuppliesItemController.confPickup(requestId, d);
                }
                this.addBannerMessage(System.Label.VTR3_Success_Confirm_Return_Delivery.replace('#1', delName));
            } else if (action.equals(CONFIRM_DELIVERY)) {
                processConfirmDelivery(requestId, params);
                this.addBannerMessage(System.Label.VTR3_Success_Confirm_Delivery.replace('#1', delName));
            } else {
                this.sendResponse(500);
                this.addError('no such action');
            }
            this.showSuccessBanner();
            get_v1(null);
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private void processConfirmDelivery(String deliveryId, RequestParams requestParams) {
        VTD1_Order__c delivery = [
                SELECT Id,VTD1_CommentsNotes__c, VTD1_ExpectedDeliveryDateTime__c,
                        VTD1_Kit_Contains_All_Contents__c, VTD1_Kit_Contents_Damaged__c, VTD1_isPackageDelivery__c,
                        RecordTypeId, Name
                FROM VTD1_Order__c
                WHERE Id = :deliveryId
        ];
        delivery.VTD1_ExpectedDeliveryDateTime__c = Datetime.newInstance(requestParams.deliveryDate - timeOffset);
        delivery.VTD1_CommentsNotes__c = requestParams.comment;

        List<VTD1_Patient_Kit__c> kitsToUpdate = new List<VTD1_Patient_Kit__c>();
        if (requestParams.updateDelivery) {
            delivery.VTD1_Kit_Contains_All_Contents__c = requestParams.kitContainsAll;
            delivery.VTD1_Kit_Contents_Damaged__c = requestParams.kitContentsDamaged;
        } else {
            for (KitData kd : requestParams.kitData) {
                VTD1_Patient_Kit__c k = new VTD1_Patient_Kit__c();
                k.Id = kd.kitId;
                k.VTD1_Kit_Contains_All_Contents__c = kd.containsAll;
                k.VTD1_Kit_Contents_Damaged__c = kd.damaged;
                k.VTD1_Kit_Not_Received__c = kd.notReceived;
                kitsToUpdate.add(k);
            }
        }
        if (!kitsToUpdate.isEmpty()) update kitsToUpdate;
        update delivery;
        List<Task> tasks = [SELECT Status FROM Task WHERE OwnerId = :UserInfo.getUserId() AND WhatId = :delivery.Id AND Status != :VT_R4_ConstantsHelper_Tasks.TASK_STATUS_COMPLETED];
        if (!tasks.isEmpty()) {
            for (Task task : tasks) {
                task.Status = VT_R4_ConstantsHelper_Tasks.TASK_STATUS_COMPLETED;
            }
            update tasks;
        }
    }

    String deliveryId;
    Boolean isPackage;
    Boolean isAdhoc;
    List<KitsDetails> kits;
    private void get_v1_confirm(String requestId) {

        try {
            VTD1_Order__c delivery = [
                    SELECT Id, Name, VTD1_isPackageDelivery__c, RecordTypeId, (SELECT Id, Name FROM Patient_Kits__r)
                    FROM VTD1_Order__c
                    WHERE Id = :requestId
                    LIMIT 1
            ];
            String adHocDeliveryRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'VTD1_Ad_hoc_Packaging_Materials' AND SobjectType = 'VTD1_Order__c'].Id;
            deliveryId = delivery.Id;
            isPackage = delivery.VTD1_isPackageDelivery__c;
            isAdhoc = delivery.RecordTypeId.equals(adHocDeliveryRecordTypeId);

            if (!delivery.Patient_Kits__r.isEmpty()) {
                kits = getKitData(delivery);
            }

        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.buildResponse();
        this.sendResponse();
    }

    private List<KitsDetails> getKitData(VTD1_Order__c delivery) {
        List<KitsDetails> result = new List<KitsDetails>();
        Set<Id> kitsIds = new Set<Id>();
        for (VTD1_Patient_Kit__c kit : delivery.Patient_Kits__r) {
            kitsIds.add(kit.Id);
        }
        Map<Id, List<VTD1_Patient_Device__c>> kitToDevices = new Map<Id, List<VTD1_Patient_Device__c>>();
        for (VTD1_Patient_Device__c device : [
                SELECT Id, Name, VTD1_Model_Name__c, VTR5_DeviceKeyId__c,VTD1_Patient_Kit__c, (SELECT Name, VTD1_Accessory_Qty__c FROM Patient_Accessories__r LIMIT 100)
                FROM VTD1_Patient_Device__c
                WHERE VTD1_Patient_Kit__c IN :kitsIds
        ]) {
            if (!kitToDevices.containsKey(device.VTD1_Patient_Kit__c)) {
                kitToDevices.put(device.VTD1_Patient_Kit__c, new List<VTD1_Patient_Device__c>());
            }
            kitToDevices.get(device.VTD1_Patient_Kit__c).add(device);
        }
        for (VTD1_Patient_Kit__c kit : delivery.Patient_Kits__r) {
            result.add(new KitsDetails(kit, kitToDevices.get(kit.Id)));
        }
        return result;
    }

    private void get_v1(String requestId) {
        try {
            VT_D1_PatientMyStudySuppliesController.StudySupplies wrapperFromController = VT_D1_PatientMyStudySuppliesController.getStudySuppliesWrapper();
            this.deliveryStatuses = wrapperFromController.deliveryStatuses;
            this.replacementReasons = wrapperFromController.replacementReasons;
            this.kitTypes = wrapperFromController.kitTypes;

            items = new List<SupplyItem>();
            Id particularDeliveryId = null;
            Id particularKitId = null;

            if (requestId != null) {
                if (requestId.startsWith(VTD1_Patient_Kit__c.getSObjectType().getDescribe().getKeyPrefix())) {
                    particularKitId = Id.valueOf(requestId);
                } else {
                    particularDeliveryId = Id.valueOf(requestId);
                }
            }
            items.addAll(getSupplyItems(wrapperFromController.studySupplyList, particularDeliveryId, particularKitId));
        } catch (Exception e) {
            System.debug('exception?' + e.getStackTraceString() + e.getMessage() + e.getLineNumber());
            this.buildResponse(e);
        }
        this.buildResponse();
        this.sendResponse();
    }

    private List<SupplyItem> getSupplyItems(List<VT_D1_PatientMyStudySuppliesController.StudySupplyItem> items, Id deliveryId, Id kitId) {
        List<SupplyItem> result = new List<SupplyItem>();
        Boolean searchForDelivery = deliveryId != null;
        Boolean searchForKit = kitId != null;
        Boolean searchFinished = false;

        for (VT_D1_PatientMyStudySuppliesController.StudySupplyItem item : items) {
            if (searchForDelivery) {
                if (!item.delivery.Id.equals(deliveryId) || searchFinished) {
                    continue;
                } else {
                    searchFinished = true;
                }
            }
            if (item.kitsList != null && !item.kitsList.isEmpty()) {
                for (VTD1_Patient_Kit__c kit : item.kitsList) {
                    if (searchForKit) {
                        System.debug('income id = ' + kitId + ' in list id = ' + kit.Id + ' equals? = ' + kit.Id.equals(kitId));
                        if (!kit.Id.equals(kitId) || searchFinished) {
                            continue;
                        } else {
                            searchFinished = true;
                        }
                    }
                    result.add(new SupplyItem(item, item.delivery, kit));
                }
            } else {
                result.add(new SupplyItem(item, item.delivery, null));
            }
        }
        return result;
    }

    private List<SupplyItem> items;
    private List<VT_D1_PatientMyStudySuppliesController.Option> deliveryStatuses;
    private List<VT_D1_PatientMyStudySuppliesController.Option> replacementReasons;
    private List<VT_D1_PatientMyStudySuppliesController.Option> kitTypes;

    private static Integer timeOffset;
    private static Boolean formUpdated;
    private static Map<String, String> BUTTON_LABELS;
    public void initService() {
        BUTTON_LABELS = new Map<String, String>{
                'Confirm delivery' => System.Label.VTD1_ConfirmDelivery,
                'Confirm Packaging Materials In Hand' => System.Label.VTD1_ConfirmPackagingMaterialsInHand,
                'Request Replacement' => System.Label.VTD1_RequestReplacement,
                'Confirm Pickup' => System.Label.VTD1_ConfirmPickup,
                'Confirm DropOff' => System.Label.VTD1_ConfirmDropOff
        };
        formUpdated = [SELECT VTD1_Form_Updated__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].VTD1_Form_Updated__c;
        timeOffset = UserInfo.getTimeZone().getOffset(System.now());
    }

    private static String getKitName(VTD1_Patient_Kit__c kitItem) {
        if (kitItem.RecordType.Name == 'Packaging Materials') {
            return System.Label.VTR3_PackagingMaterials;
        } else if (kitItem.VTD1_Protocol_Kit__c != null) {
            return kitItem.VTD1_Protocol_Kit__r.Name;
        } else {
            return kitItem.VTD1_Kit_Name__c;
        }
    }
    private static String determinateButton(VT_D1_PatientMyStudySuppliesController.StudySupplyItem item, VTD1_Patient_Kit__c kitItem) {
        String result = '';
        if (item.delivery.VTD1_Status__c == 'Delivered'
                && (item.delivery.VTR2_Destination_Type__c == 'Site' ? item.delivery.VTR2_KitsGivenToPatient__c : true)
                && item.delivery.VTD1_ExpectedDeliveryDateTime__c == null) {
            result += CONFIRM_DELIVERY;
        } else if (item.delivery.VTD1_Status__c == 'Received' || item.confirmPackagingMaterialsInHand) {
            if (item.confirmPackagingMaterialsInHand) {
                result += CONFIRM_PACKAGING_MATERIALS;
            } else if (kitItem != null && kitItem.IMP_Replacement_Forms__r.isEmpty()) {
                result += REQUEST_REPLACEMENT;
            }
        } else if (item.confirmCourierPickup) {
            result += CONFIRM_PICKUP;
        } else if (item.confirmDropOff) {
            result += CONFIRM_DROP;
        }
        return result;
    }

    private static Boolean checkIfButtonNeedForDelivery(VT_D1_PatientMyStudySuppliesController.StudySupplyItem item) {
        return ((item.delivery.VTD1_Status__c == 'Delivered' && item.delivery.VTD1_ExpectedDeliveryDateTime__c == null)
                || (item.delivery.VTD1_Status__c == 'Pending Shipment' && !formUpdated && item.delivery.VTR2_Destination_Type__c != 'Site')
                || item.delivery.VTD1_Status__c == 'Received'
                || item.confirmPackagingMaterialsInHand
                || !item.confirmCourierPickup
                || !item.confirmDropOff);
    }
    private static Boolean isUpdateNeed(VT_D1_PatientMyStudySuppliesController.StudySupplyItem item) {
        return item.delivery.VTD1_Status__c == 'Pending Shipment' && !formUpdated && item.delivery.VTR2_Destination_Type__c != 'Site';
    }
    private static Datetime getExpectedDeliveryDate(Date expectedDeliveryDate) {
        if (expectedDeliveryDate != null) {
            return Datetime.newInstance(expectedDeliveryDate.year(), expectedDeliveryDate.month(), expectedDeliveryDate.day());
        } else return null;
    }

    private class SupplyItem {
        Boolean isDelivery;
        String deliveryId;
        String kitId = '';
        String title = '';
        String description = '';
        String kitType = '';
        String status;
        Boolean recalled;
        String button;
        String buttonLabel;
        Boolean updateNeed;
        Long dateLastModified;
        Boolean isPackageDelivery;
        String shipmentType;
        String courierName;
        String trackingId;
        String shipDate;
        String estDeliveryDate;
        Long expectedDeliveryDate;

        SupplyItem(VT_D1_PatientMyStudySuppliesController.StudySupplyItem controllerItem, VTD1_Order__c delivery, VTD1_Patient_Kit__c kit) {
            this.isDelivery = kit == null;
            this.deliveryId = delivery.Id;
            if (delivery.VTD1_Protocol_Delivery__c != null && delivery.VTD1_Protocol_Delivery__r.VTD1_Description__c != null) {
                this.description = delivery.VTD1_Protocol_Delivery__r.VTD1_Description__c;
            }
            String status = String.valueOf(delivery.get('translatedStatus'));
            this.status = status == null ? '' : status;
            this.dateLastModified = delivery.LastModifiedDate.getTime();// + timeOffset;
            this.isPackageDelivery = delivery.VTD1_isPackageDelivery__c;
            this.updateNeed = isUpdateNeed(controllerItem);
            this.button = checkIfButtonNeedForDelivery(controllerItem) ? determinateButton(controllerItem, kit) : '';
            this.buttonLabel = BUTTON_LABELS.get(this.button);
            this.shipmentType = delivery.VTD1_Shipment_Type_formula__c == null ? '' : delivery.VTD1_Shipment_Type_formula__c;
            this.courierName = delivery.VTD1_Carrier__c == null ? '' : delivery.VTD1_Carrier__c;
            this.trackingId = delivery.VTD1_TrackingNumber__c == null ? '' : delivery.VTD1_TrackingNumber__c;
            this.shipDate = controllerItem.shipmentDate == null ? '' : controllerItem.shipmentDate;
            this.estDeliveryDate = controllerItem.expectedDeliveryDate == null ? '' : controllerItem.expectedDeliveryDate;
            Datetime expectedDeliveryDate = getExpectedDeliveryDate(delivery.VTD1_ExpectedDeliveryDate__c);
            if (expectedDeliveryDate != null) this.expectedDeliveryDate = expectedDeliveryDate.getTime();

            if (this.isDelivery) {
                this.title = controllerItem.translatedDeliveryName;
                this.recalled = false;
            } else {
                this.title = getKitName(kit);
                this.kitId = kit.Id;
                this.kitType = kit.RecordType.Name == 'IMP' ? System.Label.VTD1_MedicationKit : kit.VTD1_Kit_Type__c;
                this.recalled = kit.VTD1_Recalled__c;
            }
        }
    }
    private class RequestParams {
        Boolean isIMP;
        String reason;
        String comment;
        Boolean needPackagingMaterials;
        Long dateTimeUnix;
        String kitId;

        Long deliveryDate;
        List<KitData> kitData;
        Boolean updateDelivery;
        Boolean kitContainsAll;
        Boolean kitContentsDamaged;
    }

    private class KitData {
        String kitId;
        Boolean containsAll;
        Boolean damaged;
        Boolean notReceived;
    }

    private class KitsDetails {
        String kitId;
        String kitName;
        List<DeviceItem> devices;

        KitsDetails(VTD1_Patient_Kit__c kit, List<VTD1_Patient_Device__c> devices) {
            this.kitId = kit.Id;
            this.kitName = kit.Name;

            if (devices != null && !devices.isEmpty()) {
                this.devices = new List<DeviceItem>();
                for (VTD1_Patient_Device__c device : devices) {
                    this.devices.add(new DeviceItem(device));
                }
            }
        }
    }

    private class DeviceItem {
        String id;
        String dName;
        String dModel;
        String serialNumber;
        String accessories;

        DeviceItem(VTD1_Patient_Device__c patientDevice) {
            this.id = patientDevice.Id;
            this.dName = patientDevice.Name;
            this.dModel = patientDevice.VTD1_Model_Name__c;
            this.serialNumber = patientDevice.VTR5_DeviceKeyId__c;

            String accessories = '';
            if (!patientDevice.Patient_Accessories__r.isEmpty()) {
                for (VTD1_Patient_Accessories__c patientAccessories : patientDevice.Patient_Accessories__r) {
                    accessories += patientAccessories.Name + ' ' + '(Qty: ' + patientAccessories.VTD1_Accessory_Qty__c + ')';
                }
            }
            this.accessories = accessories.equals('') ? null : accessories.removeEnd(', ');
        }
    }
}