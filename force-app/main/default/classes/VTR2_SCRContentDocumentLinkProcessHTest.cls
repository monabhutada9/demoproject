/**
 * Created by Alona Riabchenko on 19/03/29.
 */
@isTest
public with sharing class VTR2_SCRContentDocumentLinkProcessHTest {

    public static void checkIfNeedCreateContainersTest() {
        List<Profile> scrProfiles = [SELECT Id FROM Profile WHERE Name = 'Site Coordinator'];
        System.assertNotEquals(0, scrProfiles.size());
        DomainObjects.Contact_t conT = new DomainObjects.Contact_t();
        DomainObjects.User_t userT = new DomainObjects.User_t(scrProfiles[0])
                .setCreatedDate(Date.today().addDays(-5))
                .addContact(conT);
        User u = (User) userT.persist();
        System.runAs(u){
            Boolean isSCR = VT_D1_ContentDocumentLinkProcessHandler.checkIfNeedCreateContainers();
            System.assert(isSCR);
        }
    }
}