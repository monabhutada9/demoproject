/**
 * Created by Yuliya Yakushenkova on 7/17/2020.
 */

public without sharing class VT_R5_MobileECoaSSO extends VT_R5_MobileRestResponse implements VT_R5_MobileRestRouter.Routable {

    public List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/ecoa-link'
        };
    }

    public void versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');

        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        getECoaSSO_v1();
                        return;
                    }
                    when 'v2' {
                        getECoaSSO_v2();
                        return;
                    }
                }
            }
        }
        VT_R5_MobileRestRouter.unsupportedVersionResponse();
        this.addError('unsupported version');
        this.sendResponse();
    }

    private void getECoaSSO_v2() {
        try {
            User currentUser = [SELECT Id, VTR5_Create_Subject_in_eCOA__c FROM User WHERE Id = :UserInfo.getUserId()];
            String token = eCOA_IntegrationDetails__c.getInstance().eCoa_Diary_Url__c + VT_R5_eCoaSSO.getToken(UserInfo.getUserId());

            this.buildResponse(new MobileECoaResponse(token, currentUser.VTR5_Create_Subject_in_eCOA__c));
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    public void initService() {
    }

    public void getECoaSSO_v1() {
        try {
            String token = eCOA_IntegrationDetails__c.getInstance().eCoa_Diary_Url__c + VT_R5_eCoaSSO.getToken(UserInfo.getUserId());
            this.buildResponse(token);
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private class MobileECoaResponse {
        String ssoLink;
        Boolean isEcoaSubjectCreated;
        MobileECoaResponse(String link, Boolean isEcoaSubjectCreated) {
            this.ssoLink = link;
            this.isEcoaSubjectCreated = isEcoaSubjectCreated;
        }
    }
}