@isTest
public with sharing class VT_R3_RestPatientStudySuppliesTest {

    public static void firstTest() {
        Test.startTest();
        Case cas = [SELECT Id,VTD1_Patient_User__c FROM Case WHERE RecordType.Name = 'CarePlan' LIMIT 1];

        List<VTD1_Order__c> deliveryList = [SELECT Id, VTD1_Status__c FROM VTD1_Order__c WHERE VTD1_Case__c = :cas.Id];
        System.debug('deliveryList ' + deliveryList);
//        List<VTD1_Order__c> deliveryListToUpdate = new List<VTD1_Order__c>();
//
//        VTD1_Order__c delivery0 = deliveryList.get(0);
//        delivery0.VTD1_isReturnDelivery__c = false;
//        delivery0.VTD1_Status__c = 'Delivered';
//        deliveryListToUpdate.add(delivery0);
//
        VTD1_Order__c delivery1 = deliveryList.get(0);
        VTD1_Order__c delivery2 = deliveryList.get(1);
//        delivery1.VTD1_Status__c = 'Received';
//        delivery1.VTD1_ExpectedDeliveryDate__c = Date.today();
//        delivery1.VTD1_Shipment_Date__c = Date.today();
//        deliveryListToUpdate.add(delivery1);

        List<VTD1_Patient_Kit__c> kits1 = [SELECT Id, VTD1_Recalled__c FROM VTD1_Patient_Kit__c WHERE VTD1_Patient_Delivery__c = :delivery1.Id];
        for (VTD1_Patient_Kit__c kit : kits1) {
            kit.VTD1_Recalled__c = true;
        }
        update kits1;

//        List<VTD1_Patient_Kit__c> kits0 = [SELECT Id, VTD1_Recalled__c FROM VTD1_Patient_Kit__c WHERE VTD1_Patient_Delivery__c = :delivery0.Id];
//        for (VTD1_Patient_Kit__c kit : kits0) {
//            kit.VTD1_Recalled__c = true;
//        }
//        delete kits0;
//
//        List<VTD1_Patient_Kit__c> kits5 = [SELECT Id, VTD1_Recalled__c FROM VTD1_Patient_Kit__c WHERE VTD1_Patient_Delivery__c = :delivery5.Id];
//        for (VTD1_Patient_Kit__c kit : kits5) {
//            kit.VTD1_Recalled__c = true;
//        }
//        delete kits5;

//        VTD1_Order__c delivery2 = deliveryList.get(2);
//        delivery2.VTD1_isReturnDelivery__c = true;
//        delivery2.VTD1_Status__c = 'Pending Return';
//        delivery2.VTD1_Patient_Delivery__c = delivery0.Id;
//        deliveryListToUpdate.add(delivery2);
//
//        VTD1_Order__c delivery3 = deliveryList.get(3);
//        delivery3.VTD1_isReturnDelivery__c = true;
//        delivery3.VTD1_Status__c = 'Pending Return';
//        delivery3.VTD1_Patient_Delivery__c = delivery0.Id;
//        deliveryListToUpdate.add(delivery3);
//
//        VTD1_Order__c delivery4 = deliveryList.get(4);
//        delivery4.VTD1_isReturnDelivery__c = true;
//        delivery4.VTD1_Status__c = 'Pending Return';
//        delivery4.VTD1_Patient_Delivery__c = delivery0.Id;
//        deliveryListToUpdate.add(delivery4);
//
//        System.debug('deliveryListToUpdate ' + deliveryListToUpdate);
//        update deliveryListToUpdate;

        List<Task> tasksToInsert = new List<Task>();

        Task task1 = new Task();
        task1.OwnerId = cas.VTD1_Patient_User__c;
        task1.VTD1_Type_for_backend_logic__c = 'Confirm Packaging Materials in Hand';
        task1.Status = 'Open';
        task1.WhatId = delivery1.Id;
        tasksToInsert.add(task1);

        Task task2 = new Task();
        task2.OwnerId = cas.VTD1_Patient_User__c;
        task2.VTD1_Type_for_backend_logic__c = 'Confirm Courier Pickup';
        task2.Status = 'Open';
        task2.WhatId = delivery2.Id;
        tasksToInsert.add(task2);

        Task task3 = new Task();
        task3.OwnerId = cas.VTD1_Patient_User__c;
        task3.VTD1_Type_for_backend_logic__c = 'Confirm Drop Off of Study Medication';
        task3.Status = 'Open';
        task3.WhatId = delivery1.Id;
        tasksToInsert.add(task3);

        Task task4 = new Task();
        task4.OwnerId = cas.VTD1_Patient_User__c;
        task4.VTD1_Type_for_backend_logic__c = 'Confirm Courier Pickup';
        task4.Status = 'Open';
        task4.WhatId = delivery1.Id;
        tasksToInsert.add(task4);

        insert tasksToInsert;

        String responseString;
        Map<String, Object>responseMap = new Map<String, Object>();
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/StudySupplies/';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = response;


        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        System.runAs(toRun) {
            responseString = VT_R3_RestPatientStudySupplies.getSupplies();
            responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
            System.debug('#@#@#@#@ ' + responseMap.get('serviceResponse'));

            RestContext.request.requestURI = '/StudySupplies/' + kits1[0].Id;
            responseString = VT_R3_RestPatientStudySupplies.getSupplies();
            RestContext.request.requestURI = '/StudySupplies/' + deliveryList[0].Id;
            responseString = VT_R3_RestPatientStudySupplies.getSupplies();
        }
        Test.stopTest();
    }


    public static void secondTest() {
        Case cas = [SELECT Id FROM Case WHERE RecordType.Name = 'CarePlan' LIMIT 1];

        String responseString;
        Map<String, Object>responseMap = new Map<String, Object>();
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/StudySupplies/';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = response;


        List<VTD1_Order__c> deliveryList = [SELECT Id, VTD1_Status__c FROM VTD1_Order__c WHERE VTD1_Case__c = :cas.Id];
        List<VTD1_Patient_Kit__c> kitList = [SELECT Id, VTD1_Recalled__c FROM VTD1_Patient_Kit__c WHERE VTD1_Patient_Delivery__c = :deliveryList[0].Id];

        System.assert(kitList.size() > 0);
        System.assert(deliveryList.size() > 0);
        String delName = [SELECT Id, Name FROM VTD1_Order__c WHERE Id = :deliveryList[0].Id LIMIT 1].Name;

        Test.startTest();
        responseString = VT_R3_RestPatientStudySupplies.doAction('Confirm Packaging Materials In Hand', deliveryList[0].Id, null, null, null, true, null, null);
        responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
        System.assertEquals(System.Label.VTR3_Confirm_Packaging_Materials_Request.replace('#1', delName), responseMap.get('serviceMessage'));

        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];

        System.runAs(toRun) {
            List<VTD1_Order__c> orderList = [SELECT Id, Name FROM VTD1_Order__c];
            System.assert(orderList.size() > 0);
            delName = [SELECT Id, Name FROM VTD1_Order__c WHERE Id = :orderList[0].Id LIMIT 1].Name;

            responseString = VT_R3_RestPatientStudySupplies.doAction('Request Replacement', orderList[0].Id, false, 'reason', 'comment', true, null, kitList[0].Id);
            responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
            System.assertEquals(System.Label.VTR3_Kit_Replacement_Request.replace('#1', delName), responseMap.get('serviceMessage'));

            Datetime dateTimeUnix = DateTime.now();
            responseString = VT_R3_RestPatientStudySupplies.doAction('Confirm Pickup', orderList[0].Id, false, 'reason', 'comment', true, dateTimeUnix.getTime(), kitList[0].Id);
            responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
            System.assertEquals(System.Label.VTR3_Success_Confirm_Return_Delivery.replace('#1', delName), responseMap.get('serviceMessage'));

            responseString = VT_R3_RestPatientStudySupplies.doAction('Confirm delivery', orderList[0].Id, false, 'reason', 'comment', true, dateTimeUnix.getTime(), kitList[0].Id);
            System.assertEquals('Wrong destination. For confirm delivery use /services/apexrest/Patient/StudySupplies/Confirm/*', responseString);

            responseString = VT_R3_RestPatientStudySupplies.doAction('Confirm DropOff', orderList[0].Id, false, 'reason', 'comment', true, dateTimeUnix.getTime(), kitList[0].Id);
            responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
            System.assertEquals(System.Label.VTR3_Success_Confirm_Return_Delivery.replace('#1', delName), responseMap.get('serviceMessage'));

            responseString = VT_R3_RestPatientStudySupplies.doAction('Other action', orderList[0].Id, false, 'reason', 'comment', true, dateTimeUnix.getTime(), kitList[0].Id);
            System.assertEquals('No such action', responseString);
        }

        Test.stopTest();
    }
}