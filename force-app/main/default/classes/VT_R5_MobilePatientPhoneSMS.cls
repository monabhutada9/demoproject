/**
 * Created by Yuliya Yakushenkova on 10/19/2020.
 */

public class VT_R5_MobilePatientPhoneSMS extends VT_R5_MobileRestResponse implements VT_R5_MobileRestRouter.Routable {

    private static RequestParams requestParams;

    public List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/phone/sms' //?action={action_name}
        };
    }

    public void versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        switch on this.request.httpMethod {
            when 'POST' {
                switch on requestVersion {
                    when 'v1' {
                        post_v1();
                        return;
                    }
                }
            }
        }
        VT_R5_MobileRestRouter.unsupportedVersionResponse();
        this.addError('unsupported version');
        this.sendResponse();
    }

    public void initService() {
        requestParams = new RequestParams();
        parseBody();//first!
        parseParams();
    }

    private void parseBody() {
        if (request.requestBody.size() > 0) {
            requestParams = (RequestParams) JSON.deserialize(request.requestBody.toString(), RequestParams.class);
        }
    }

    private void parseParams() {
        if (request.params.containsKey('action')) requestParams.setAction(request.params.get('action'));
    }

    private void post_v1() {
        try {
            String message = getMessage();
            if (String.isBlank(message)) {
                VT_D1_Phone__c toReturn = [
                        SELECT Id, PhoneNumber__c, Type__c, VTR2_OptInFlag__c, VTR2_SMS_Opt_In_Status__c, IsPrimaryForPhone__c
                        FROM VT_D1_Phone__c WHERE Id = :requestParams.phoneId LIMIT 1];

                Phone result = new Phone();
                result.id = toReturn.Id;
                result.phoneNumber = toReturn.PhoneNumber__c;
                result.isPrimary = toReturn.IsPrimaryForPhone__c;
                result.optInStatus = toReturn.VTR2_SMS_Opt_In_Status__c;
                result.optInFlag = toReturn.VTR2_OptInFlag__c;
                result.phoneType = toReturn.Type__c;
                this.buildResponse(result);
            } else {
                this.buildResponse(message);
            }
            this.buildResponse();
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private String getMessage() {
        VT_D1_Phone__c phone = [
                SELECT Id, PhoneNumber__c, Type__c, VTR2_OptInFlag__c, VTR2_SMS_Opt_In_Status__c, IsPrimaryForPhone__c
                FROM VT_D1_Phone__c
                WHERE Id = :requestParams.phoneId
        ];

        if (requestParams.action.equalsIgnoreCase('optIn')) {
            if (!(phone.VTR2_SMS_Opt_In_Status__c == null)) {
                if (!(phone.VTR2_SMS_Opt_In_Status__c.equals('Inactive'))) {
                    return 'Phone should be in optIn-status "Inactive" or with null status';
                }
            }

            VT_D1_PatientFullProfileController.optInRemote(requestParams.phoneId);
        } else if (requestParams.action.equalsIgnoreCase('optOut')) {
            if (!phone.VTR2_SMS_Opt_In_Status__c.equals('Active')) {
                return 'Phone should be in optIn-status "Active"';
            }
            VT_D1_PatientFullProfileController.optOutRemote(requestParams.phoneId);
        } else if (requestParams.action.equalsIgnoreCase('resendOptIn')) {
            if (!(phone.VTR2_SMS_Opt_In_Status__c.equals('Pending'))) {
                return 'Phone should be in optIn-status "Pending"';
            }
            VT_D1_PatientFullProfileController.resendOptInRemote(requestParams.phoneId);
        } else if (requestParams.action.equalsIgnoreCase('sendContacts')) {
            if (!(phone.VTR2_SMS_Opt_In_Status__c.equals('Active'))) {
                return 'Phone should be in optIn-status "Active"';
            }
            VT_D1_PatientFullProfileController.sendContactsRemote(requestParams.phoneId);
        } else {
            if (requestParams.action != null) {
                return 'Please, specify one of these actions: optIn, optOut, resendOptIn, sendContacts. Got action: "' + requestParams.action + '"';
            } else {
                return 'Impossible to ' + requestParams.action + ' on phone with such status ' + phone.VTR2_SMS_Opt_In_Status__c + ' and ID - ' + requestParams.phoneId;
            }
        }
        return '';
    }

    private class Phone {
        public String id;
        public String phoneNumber;
        public String phoneType;
        public Boolean isPrimary;
        public Boolean optInFlag;
        public String optInStatus;
    }

    private class RequestParams {
        private String phoneId;
        private String action;

        private void setAction(String action) {
            this.action = action;
        }
    }
}