public without sharing class VT_D1_TaskListController {
    public static String userProfile = VT_D1_HelperClass.getUserProfileName(UserInfo.getUserId());
    //test 
    public class TaskInfo {
        @AuraEnabled public List<Task> tasks;
        @AuraEnabled public String userProfile;
        @AuraEnabled public String userId;
        @AuraEnabled public List<String> categories = new List<String>();
        @AuraEnabled public VT_R5_PagingQueryHelper.ResultWrapper result = new VT_R5_PagingQueryHelper.ResultWrapper();
        public void setCategories() {
            VTD2_TaskCategoriesByProfile__c catsForProfileSetting = VTD2_TaskCategoriesByProfile__c.getInstance();
            if (catsForProfileSetting != null && catsForProfileSetting.VTD2_Categories__c != null) {
                Set<String> picklistVals = new Set<String>();
                for (Schema.PicklistEntry picklistItem : Task.Category__c.getDescribe().getPicklistValues()) {
                    picklistVals.add(picklistItem.getValue());
                }

                for (String cat : catsForProfileSetting.VTD2_Categories__c.split(';')) {
                    if (! String.iSBlank(cat)) {
                        cat = cat.trim();
                        if (picklistVals.contains(cat)) { this.categories.add(cat); }
                    }
                }
            }
        }
    }

    @AuraEnabled
    public static TaskInfo getTasksInfo(String status, String category) {
        TaskInfo ti = new TaskInfo();
        ti.userProfile = userProfile;
        ti.userId = UserInfo.getUserId();
        ti.tasks = getTasks(status, category);
        return ti;
    }

    @AuraEnabled
    public static List<Task> getTasks(String status, String category) {
        List<Task> tasks;
        Id currentUserId = UserInfo.getUserId();
        String patientId;
        Case currentCase = VT_D1_CaseByCurrentUser.getCase().caseObj;
        Id currentCaseId = currentCase.Id;
        System.debug('currentCase - '+currentCase);
        String profileName = VT_D1_PatientCaregiverBound.getUserProfileName(currentUserId);
        if (profileName == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME) {
            patientId = VT_D1_PatientCaregiverBound.findPatientOfCaregiver(currentUserId);
        }

        List<String> taskFields = new List<String>{
                Task.OwnerId + '',
                Task.Subject + '',
                Task.VTD2_Subject_Parameters__c + '',
                Task.Description + '',
                Task.VTD2_Description_Parameters__c + '',
                Task.Status + '',
                Task.Priority + '',
                Task.Category__c + '',
                'toLabel(Category__c) CategoryLabel',
                Task.ActivityDate + '',
                Task.WhatId + '',
                Task.Type + '',
                Task.VTD1_Type_for_backend_logic__c + '',
                Task.VTD1_Caregiver_Clickable__c + '',
                'Id',
                'What.Name',
                Task.HealthCloudGA__CarePlanTemplate__c + '',
                Task.VTD2_Study_Name__c + '',
                Task.VTD2_My_Task_List_Redirect__c + '',
                Task.VTD2_isRequestVisitRedirect__c + '',
                Task.VTD1_Case_lookup__c+'',
                Task.VTR5_EndDate__c+'',
                Task.VTR5_AutocompleteWhenClicked__c + ''
        };

        Datetime now = Datetime.now();
        String queryStr = 'SELECT ' + String.join(taskFields, ',')
                + ' FROM Task WHERE '
                + '(OwnerId=:currentUserId' + (patientId != null ? ' OR OwnerId=:patientId' :'') + ')'
                + (status != null ? ' AND status=:status' : '')
                + (category != null ? ' AND Category__c=:category' : '')
                + (currentCaseId != null ? ' AND VTD1_Case_lookup__c=:currentCaseId' : '')
                + ' AND (VTR5_StartDate__c = null OR VTR5_StartDate__c <= :now)'
                + ' ORDER BY ActivityDate ASC, VTR5_EndDate__c ASC NULLS FIRST LIMIT 1000';

        tasks = Database.query(queryStr);

        VT_D1_TranslateHelper.translate(tasks);
        for (Task t : tasks) {
            if (t.WhatId != null
                && String.valueOf(t.WhatId.getSobjectType()).contains('VTD1_Protocol_Deviation')) {
                t.Type = 'PI acknowledgment';
            }
        }
        return tasks;
    }

    @AuraEnabled(cacheable=true)
    public static List <String> getCategories() {
        List<String> categories = new List<String>();

        VTD2_TaskCategoriesByProfile__c catsForProfileSetting = VTD2_TaskCategoriesByProfile__c.getInstance();
        if (catsForProfileSetting != null && catsForProfileSetting.VTD2_Categories__c != null) {
            Set<String> picklistVals = new Set<String>();
            for (Schema.PicklistEntry picklistItem : Task.Category__c.getDescribe().getPicklistValues()) {
                picklistVals.add(picklistItem.getValue());
            }

            for (String cat : catsForProfileSetting.VTD2_Categories__c.split(';')) {
                if (! String.iSBlank(cat)) {
                    cat = cat.trim();
                    if (picklistVals.contains(cat)) { categories.add(cat); }
                }
            }
        }

        return categories;
    }

    @AuraEnabled
    public static void closeTask(String taskId) {
        Task task = new Task(Id=taskId);//[SELECT Id, Status FROM Task WHERE Id =: taskId];
        task.Status = 'Completed';
        update task;
    }

    @AuraEnabled
    public static String getNetworkId() {
        return Network.getNetworkId();
    }

    @AuraEnabled
    public static String getTasksFromPool(String category, String status, String jsonParams) {
        TaskInfo taskInfo = new TaskInfo();
        if (category == null ) {
            taskInfo.setCategories();
        }
        if(status == 'Completed'){
            category = null;
        }
        taskInfo.result = getAllTasks(category, status, jsonParams);
        return JSON.serialize(taskInfo);
    }

    public static VT_R5_PagingQueryHelper.ResultWrapper getAllTasks(String category, String status, String jsonParams) {

        List<String> taskFields = new List<String>{
                Task.OwnerId + '',
                Task.Subject + '',
                Task.VTD2_Subject_Parameters__c + '',
                Task.Description + '',
                Task.VTD2_Description_Parameters__c + '',
                Task.Status + '',
                Task.Priority + '',
                Task.Category__c + '',
                'toLabel(Category__c) CategoryLabel',
                Task.ActivityDate + '',
                Task.WhatId + '',
                Task.Type + '',
                Task.VTD1_Type_for_backend_logic__c + '',
                Task.VTD1_Caregiver_Clickable__c + '',
                'Id',
                'What.Name',
                Task.HealthCloudGA__CarePlanTemplate__c + '',
                Task.VTD2_Study_Name__c + '',
                Task.VTD2_My_Task_List_Redirect__c + '',
                Task.VTD2_isRequestVisitRedirect__c + '',
                Task.VTD1_Case_lookup__c+'',
                Task.VTR5_EndDate__c+'',
                Task.VTR5_AutocompleteWhenClicked__c + ''
        };

        Id currentUserId = UserInfo.getUserId();
        String patientId;
        Case currentCase = VT_D1_CaseByCurrentUser.getCase().caseObj;
        Id currentCaseId = currentCase.Id;
        System.debug('currentCase - '+currentCase);
        String profileName = VT_D1_PatientCaregiverBound.getUserProfileName(currentUserId);
        if (profileName == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME) {
            patientId = VT_D1_PatientCaregiverBound.findPatientOfCaregiver(currentUserId);
        }

        String query = 'SELECT ' + String.join(taskFields, ',') +
                ' FROM Task ';
        String filter =  '(OwnerId=\'' + currentUserId + '\'' + (patientId != null ? ' OR OwnerId= \'' + patientId + '\'' :'') + ')' +
                 (status != null ? ' AND status= \'' + status + '\'' : '') +
                 (category != null ? ' AND Category__c IN (\'' + category + '\')' : '') +
                 (currentCaseId != null ? ' AND VTD1_Case_lookup__c= \'' + currentCaseId + '\'' : '') +
                 ' AND (VTR5_StartDate__c = null OR VTR5_StartDate__c <= :dateTimeNow)';
        //filter += String.format(' AND (VTR5_StartDate__c = null OR VTR5_StartDate__c <= {0})', new List<Object>{now});
        System.debug('filter= ' + filter );

        //String order = 'ActivityDate ASC, VTR5_EndDate__c ASC NULLS FIRST';
        String order = 'CreatedDate DESC';
        Map <String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(jsonParams);
        params.put('filter', filter);
        params.put('order', order);
        params.put('groupBy', 'Category__c');
        String sObjectName = (String)params.get('sObjectName');
        try {
            VT_R5_PagingQueryHelper.ResultWrapper wrapper = VT_R5_PagingQueryHelper.query(query, sObjectName, JSON.serialize(params));
            return wrapper;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
        }
    }
}