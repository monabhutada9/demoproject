/**
 * @author: Alexander Komarov
 * @date: 02.04.2020
 * @description:
 */

@IsTest
private class VT_R4_MobilePatientEdiaryTest {
    static Id eProRecTypeId = Schema.SObjectType.VTD1_Protocol_ePRO__c.getRecordTypeInfosByDeveloperName().get('ePRO').getRecordTypeId();
    static Id eProRecTypeId2 = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('ePRO').getRecordTypeId();

    @TestSetup
    static void setup() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();

        User user = [SELECT Id FROM User WHERE Profile.Name = 'Patient' AND IsActive = true AND Name = 'CP1 CP1L' LIMIT 1];

        insert new HealthCloudGA__CarePlanTemplate__Share(
                UserOrGroupId = user.Id,
                ParentId = study.Id,
                AccessLevel = 'Edit'
        );

        Case carePlan = [
                SELECT Id,VTD1_Study__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
        ];

        VTD1_Protocol_ePRO__c protocolEPRO = new VTD1_Protocol_ePRO__c(
                RecordTypeId = eProRecTypeId,
                VTD1_Study__c = carePlan.VTD1_Study__c,
                VTR2_Protocol_Reviewer__c = 'Patient Guide',
                VTD1_Subject__c = 'Patient',
                VTD1_Response_Window__c = 10,
                VTR4_Is_Branching_eDiary__c = true
        );
        insert protocolEPRO;

        List<VTD1_Protocol_ePro_Question__c> questionsToInsert = new List<VTD1_Protocol_ePro_Question__c>();
        List<VTD1_Protocol_ePro_Question__c> questionsToInsertChild = new List<VTD1_Protocol_ePro_Question__c>();

        questionsToInsert.add(new VTD1_Protocol_ePro_Question__c(VTD1_Protocol_ePRO__c = protocolEPRO.Id, VTR2_QuestionContentLong__c = 'questionContent?', VTD1_Type__c = 'Short Text', VTR4_Number__c = '1', VTD1_Group_number__c = 1));
        questionsToInsert.add(new VTD1_Protocol_ePro_Question__c(VTD1_Protocol_ePRO__c = protocolEPRO.Id, VTR2_QuestionContentLong__c = 'questionContent 2?', VTD1_Type__c = 'Drop Down', VTR4_Number__c = '2', VTD1_Group_number__c = 1, VTD1_Options__c = 'Yes;No', VTR4_Contains_Branching_Logic__c = true));
        questionsToInsert.add(new VTD1_Protocol_ePro_Question__c(VTD1_Protocol_ePRO__c = protocolEPRO.Id, VTR2_QuestionContentLong__c = 'questionContent 3?', VTD1_Type__c = 'Short Text', VTR4_Number__c = '3', VTD1_Group_number__c = 1));
        insert questionsToInsert;

        List<VTD1_Protocol_ePro_Question__c> parentQuestions = [SELECT Id FROM VTD1_Protocol_ePro_Question__c WHERE VTR4_Contains_Branching_Logic__c = TRUE];
        if (parentQuestions.size() > 0) {
            questionsToInsertChild.add(new VTD1_Protocol_ePro_Question__c(VTD1_Protocol_ePRO__c = protocolEPRO.Id, VTR2_QuestionContentLong__c = 'questionContent 4?', VTD1_Type__c = 'Long Text (Essay)', VTR4_Number__c = '2.1', VTD1_Group_number__c = 1, VTR4_Branch_Parent__c = parentQuestions[0].Id, VTR4_Response_Trigger__c = 'Yes'));
            questionsToInsertChild.add(new VTD1_Protocol_ePro_Question__c(VTD1_Protocol_ePRO__c = protocolEPRO.Id, VTR2_QuestionContentLong__c = 'questionContent 4?', VTD1_Type__c = 'Long Text (Essay)', VTR4_Number__c = '2.1', VTD1_Group_number__c = 1, VTR4_Branch_Parent__c = parentQuestions[0].Id, VTR4_Response_Trigger__c = 'No'));
            insert questionsToInsertChild;
        }

        VTD1_Survey__c survey = new VTD1_Survey__c(RecordTypeId = eProRecTypeId2, VTR2_DocLanguage__c = 'en_US', VTD1_Protocol_ePRO__c = protocolEPRO.Id, VTD1_CSM__c = carePlan.Id, VTD1_Status__c = 'In Progress', VTD1_Date_Available__c = Datetime.now() - 1, VTD1_Due_Date__c = Datetime.now() + 10, Name = 'samplename');
        insert survey;

        List<VTD1_Translation__c> translations = new List<VTD1_Translation__c>();
        translations.add(new VTD1_Translation__c(VTD1_Object_Name__c = 'VTD1_Protocol_ePro_Question__c',VTD1_Field_Name__c='VTD1_Options__c', VTD1_Record_Id__c = questionsToInsert[0].Id,VTD1_Value__c = 'value1', VTR2_Value__c = 'value2', VTD1_Language__c = 'en_US'));
        translations.add(new VTD1_Translation__c(VTD1_Object_Name__c = 'VTD1_Protocol_ePro_Question__c',VTD1_Field_Name__c='VTD1_Question_Content__c', VTD1_Record_Id__c = questionsToInsert[0].Id,VTD1_Value__c = 'value1', VTR2_Value__c = 'value2', VTD1_Language__c = 'en_US'));
        translations.add(new VTD1_Translation__c(VTD1_Object_Name__c = 'VTD1_Protocol_ePro_Question__c',VTD1_Field_Name__c='VTR2_QuestionContentLong__c', VTD1_Record_Id__c = questionsToInsert[0].Id,VTD1_Value__c = 'value1', VTD1_Language__c = 'en_US'));
        insert translations;
    }

    @IsTest
    static void testGetAndPut() {
        User user = [SELECT Id FROM User WHERE Profile.Name = 'Patient' AND IsActive = true AND Name = 'CP1 CP1L' LIMIT 1];
        System.runAs(user) {
            RestContext.request = new RestRequest();
            RestContext.response = new RestResponse();

            RestContext.request.requestURI = '/Patient/v1/Ediaries/';
            RestContext.request.httpMethod = 'GET';

            VT_R4_MobilePatientEdiary.getMethod();

            List<VTD1_Survey__c> surveys = [SELECT Id FROM VTD1_Survey__c];
            if (surveys.size() > 0) {
                RestContext.request.requestURI = '/Patient/v1/Ediaries/' + surveys[0].Id;
                VT_R4_MobilePatientEdiary.getMethod();
                RestContext.request.httpMethod = 'PUT';
                VT_R4_MobilePatientEdiary.putMethod();
                RestContext.request.requestURI = '/Patient/v1/Ediaries/Reset/' + surveys[0].Id;
                VT_R4_MobilePatientEdiary.putMethod();
                RestContext.request.requestURI = '/Patient/v1/Ediaries/' + fflib_IDGenerator.generate(VTD1_Survey__c.getSObjectType());
                VT_R4_MobilePatientEdiary.getMethod();
            }
        }
    }

    @IsTest
    static void testPost() {
        User user = [SELECT Id FROM User WHERE Profile.Name = 'Patient' AND IsActive = true AND Name = 'CP1 CP1L' LIMIT 1];

        System.runAs(user) {
            List<VTD1_Survey__c> surveys = [SELECT Id, VTR2_DocLanguage__c FROM VTD1_Survey__c];
            if (surveys.size() > 0) {
                RestContext.request = new RestRequest();
                RestContext.response = new RestResponse();

                RestContext.request.requestURI = '/Patient/v1/Ediaries/' + surveys[0].Id;
                RestContext.request.httpMethod = 'POST';

                RestContext.request.params.put('complete', 'false');
                VT_R4_MobilePatientEdiary.MobileEdiaryRequest request = formRequest(true);
                RestContext.request.requestBody = Blob.valueOf(JSON.serialize(request));
                VT_R4_MobilePatientEdiary.postMethod();
                surveys[0].VTR2_DocLanguage__c = null;
                update surveys[0];

                RestContext.request.params.put('complete', 'true');
                request = formRequest(false);
                RestContext.request.requestBody = Blob.valueOf(JSON.serialize(request));
                VT_R4_MobilePatientEdiary.postMethod();

                VT_R4_MobilePatientEdiary.postMethod();
            }
        }

    }

    static VT_R4_MobilePatientEdiary.MobileEdiaryRequest formRequest(Boolean first) {
        VT_R4_MobilePatientEdiary.MobileEdiaryRequest request = new VT_R4_MobilePatientEdiary.MobileEdiaryRequest();
        request.answers = new List<VT_R4_MobilePatientEdiary.CustomAnswerWrapper>();
        for (VTD1_Protocol_ePro_Question__c q : [SELECT Id, VTD1_Type__c, VTR4_Response_Trigger__c FROM VTD1_Protocol_ePro_Question__c]) {
            VT_R4_MobilePatientEdiary.CustomAnswerWrapper caw = new VT_R4_MobilePatientEdiary.CustomAnswerWrapper();
            if (q.VTD1_Type__c == 'Short Text') {
                caw.questionId = q.Id;
                caw.answer = 'some answer';
                caw.timestamp = System.now().getTime();
                request.answers.add(caw);
            } else if (q.VTD1_Type__c == 'Drop Down') {
                if (first) {
                    caw.questionId = q.Id;
                    caw.answer = 'Yes';
                    caw.timestamp = System.now().getTime();
                    request.answers.add(caw);
                } else {
                    caw.questionId = q.Id;
                    caw.answer = 'No';
                    caw.timestamp = System.now().getTime();
                    request.answers.add(caw);
                }
            } else {
                if (first && q.VTR4_Response_Trigger__c == 'Yes') {
                    caw.questionId = q.Id;
                    caw.answer = 'Long text';
                    caw.timestamp = System.now().getTime();
                    request.answers.add(caw);
                } else if (!first && q.VTR4_Response_Trigger__c == 'No') {
                    caw.questionId = q.Id;
                    caw.answer = 'Long text';
                    caw.timestamp = System.now().getTime();
                    request.answers.add(caw);
                }
            }
        }
        request.percentage = 100;
        return request;
    }
}