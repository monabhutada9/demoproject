/**
 * Created by Maksim Fedarenka on 12/3/2020.
 */

/**
 * SCRIPT:
// INPUTS:
Id caseId = '5001w000006RtbVAAS';
// MAIN
System.enqueueJob(new SH_21076_SurveyGenerator(50, caseId));

 * SOQL:
SELECT COUNT()
FROM VTD1_Survey__c
WHERE (RecordType.DeveloperName = 'VTR5_External' OR RecordType.DeveloperName = 'ePRO') AND VTD1_Status__c <> 'Not Started'
*/

public with sharing class SH_21076_SurveyGenerator implements Queueable {
    private final Id rtId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('VTR5_External').getRecordTypeId();
    private final Id caseId;
    private Integer numOfIterations;

    public SH_21076_SurveyGenerator(Integer numOfIterations, Id caseId) {
        this.numOfIterations = numOfIterations;
        this.caseId = caseId;
    }

    public void execute(QueueableContext qc) {
        List<VTD1_Survey__c> surveys = new List<VTD1_Survey__c>();
        for (Integer i = 0; i < 1000; i++) {
            surveys.add(new VTD1_Survey__c(VTD1_CSM__c = caseId, Name = 'TEST SURVEY', RecordTypeId = rtId));
        }
        insert surveys;

        numOfIterations--;

        if (numOfIterations > 0) {
            System.enqueueJob(new SH_21076_SurveyGenerator(numOfIterations, caseId));
        }
    }
}