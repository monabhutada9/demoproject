/**
* @author:      Carl Judge
* @date:        20-Jan-20
* @updater:     Rishat Shamratov
* @updated:     02.10.2020
* @description: Test class for VT_R4_ProtocolAmendmentVersionHelper
**/

@IsTest
private class VT_R4_ProtocolAmendmentVersionHelperTest {
    @TestSetup
    private static void setupMethod() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        DomainObjects.Study_t domainStudy = new DomainObjects.Study_t();
        domainStudy.persist();

        DomainObjects.User_t domainCRAUser = new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME)
                .setEmail(VT_D1_TestUtils.generateUniqueUserName());
        domainCRAUser.persist();

        DomainObjects.User_t domainPIUser = new DomainObjects.User_t()
                .setContact(new DomainObjects.Contact_t().setFirstName('Test').setLastName('PI').persist().Id)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME)
                .setEmail(VT_D1_TestUtils.generateUniqueUserName());
        domainPIUser.persist();

        DomainObjects.StudyTeamMember_t domainCRAStudyTeamMember = new DomainObjects.StudyTeamMember_t()
                .addStudy(domainStudy)
                .addUser(domainCRAUser);
        domainCRAStudyTeamMember.persist();

        DomainObjects.StudyTeamMember_t domainPIStudyTeamMember = new DomainObjects.StudyTeamMember_t()
                .addStudy(domainStudy)
                .addUser(domainPIUser);
            //START RAJESH SH-17440
            //    .setRemoteCraId(domainCRAStudyTeamMember.Id);
        //END:SH-17440

        domainPIStudyTeamMember.persist();

        DomainObjects.VirtualSite_t domainVirtualSite1 = new DomainObjects.VirtualSite_t()
                .addStudy(domainStudy)
                .addStudyTeamMember(domainPIStudyTeamMember)
                .setName('SiteTest1')
                .setStudySiteNumber('12345');
        domainVirtualSite1.persist();

        DomainObjects.VirtualSite_t domainVirtualSite2 = new DomainObjects.VirtualSite_t()
                .addStudy(domainStudy)
                .addStudyTeamMember(domainPIStudyTeamMember)
                .setName('SiteTest2')
                .setStudySiteNumber('54321');
        domainVirtualSite1.persist();
        Test.stopTest();

        List<VTD1_Regulatory_Document__c> regulatoryDocumentList = new List<VTD1_Regulatory_Document__c>{
                new VTD1_Regulatory_Document__c(
                        VTD1_Document_Type__c = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_SIGNATURE_PAGE,
                        VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI'
                ),
                new VTD1_Regulatory_Document__c(
                        VTD1_Document_Type__c = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_PROTOCOL_AMENDMENT,
                        VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI'
                )
        };
        insert regulatoryDocumentList;

        List<VTD1_Regulatory_Binder__c> regulatoryBinderList = new List<VTD1_Regulatory_Binder__c>{
                new VTD1_Regulatory_Binder__c(
                        VTD1_Care_Plan_Template__c = domainStudy.Id,
                        VTD1_Regulatory_Document__c = regulatoryDocumentList[0].Id
                ),
                new VTD1_Regulatory_Binder__c(
                        VTD1_Care_Plan_Template__c = domainStudy.Id,
                        VTD1_Regulatory_Document__c = regulatoryDocumentList[1].Id
                )
        };
        insert regulatoryBinderList;

        List<VTD1_Protocol_Amendment__c> protocolAmendmentList = new List<VTD1_Protocol_Amendment__c>{
                new VTD1_Protocol_Amendment__c(Name = 'Site1And2PA', VTD1_Study_ID__c = domainStudy.Id),
                new VTD1_Protocol_Amendment__c(Name = 'Site1PA', VTD1_Study_ID__c = domainStudy.Id),
                new VTD1_Protocol_Amendment__c(Name = 'NoSitePA', VTD1_Study_ID__c = domainStudy.Id)
        };
        insert protocolAmendmentList;

        Test.setCreatedDate(protocolAmendmentList[0].Id, System.now().addMinutes(-10));
        Test.setCreatedDate(protocolAmendmentList[1].Id, System.now().addMinutes(-5));

        insert new List<VTD1_Document__c>{
                getDoc(domainVirtualSite1.Id, protocolAmendmentList[0].Id, regulatoryBinderList[0].Id),
                getDoc(domainVirtualSite1.Id, protocolAmendmentList[1].Id, regulatoryBinderList[0].Id),
                getDoc(domainVirtualSite2.Id, protocolAmendmentList[0].Id, regulatoryBinderList[0].Id)
        };

        List<Account> accountList = new List<Account>();
        for (Integer i = 0; i < 7; i++) {
            accountList.add(new Account(Name = 'Acc' + i));
        }
        insert accountList;
    }

    private static VTD1_Document__c getDoc(Id siteId, Id paId, Id regBinderId) {
        return new VTD1_Document__c(
                VTD1_Site__c = siteId,
                VTD1_Protocol_Amendment__c = paId,
                RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT,
                VTD1_Regulatory_Binder__c = regBinderId,
                VTD2_EAFStatus__c = 'Completed',
                VTR4_IRB_Approved__c = true
        );
    }

    @IsTest
    private static void doTest() {
        Test.startTest();
        Map<String, Id> paIdMap = new Map<String, Id>();
        for (VTD1_Protocol_Amendment__c pa : [
                SELECT Id, Name
                FROM VTD1_Protocol_Amendment__c
                ORDER BY CreatedDate DESC
                LIMIT 3
        ]) {
            paIdMap.put(pa.Name, pa.Id);
        }

        Virtual_Site__c vs1 = [SELECT Id FROM Virtual_Site__c WHERE Name = 'SiteTest1' ORDER BY CreatedDate DESC LIMIT 1];
        Virtual_Site__c vs2 = [SELECT Id FROM Virtual_Site__c WHERE Name = 'SiteTest2' ORDER BY CreatedDate DESC LIMIT 1];

        List<Account> accountList = [SELECT Id FROM Account ORDER BY CreatedDate DESC LIMIT 7];
        Account branch1Ver1 = accountList[0];
        Account branch2Ver1 = accountList[1];
        Account branch2Ver2 = accountList[2];
        Account branch2Ver3 = accountList[3];
        Account branch3Ver1 = accountList[4];
        Account branch3Ver2 = accountList[5];
        Account branch3Ver3 = accountList[6];

        List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrappedRecs = new List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper>{
                new VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper(branch1Ver1, null, null, false),

                new VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper(branch2Ver1, null, null, false),
                new VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper(branch2Ver2, branch2Ver1.Id, paIdMap.get('Site1PA'), false),
                new VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper(branch2Ver3, branch2Ver1.Id, paIdMap.get('NoSitePA'), false),

                new VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper(branch3Ver1, null, null, false),
                new VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper(branch3Ver2, branch3Ver1.Id, paIdMap.get('Site1And2PA'), true),
                new VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper(branch3Ver3, branch3Ver1.Id, paIdMap.get('Site1PA'), false)
        };

        VT_R4_ProtocolAmendmentVersionHelper helper = new VT_R4_ProtocolAmendmentVersionHelper(wrappedRecs);

        Set<Id> site1RecIds = new Map<Id, SObject>(helper.getCorrectVersions(vs1.Id)).keySet();
        System.assert(site1RecIds.contains(branch1Ver1.Id));
        System.assert(!site1RecIds.contains(branch2Ver1.Id));
        System.assert(site1RecIds.contains(branch2Ver2.Id));
        System.assert(!site1RecIds.contains(branch2Ver3.Id));
        System.assert(!site1RecIds.contains(branch3Ver1.Id));
        System.assert(!site1RecIds.contains(branch3Ver2.Id));
        System.assert(site1RecIds.contains(branch3Ver3.Id));

        Set<Id> site2RecIds = new Map<Id, SObject>(helper.getCorrectVersions(vs2.Id)).keySet();
        System.assert(site2RecIds.contains(branch1Ver1.Id));
        System.assert(site2RecIds.contains(branch2Ver1.Id));
        System.assert(!site2RecIds.contains(branch2Ver2.Id));
        System.assert(!site2RecIds.contains(branch2Ver3.Id));
        System.assert(!site2RecIds.contains(branch3Ver1.Id));
        System.assert(!site2RecIds.contains(branch3Ver2.Id));
        System.assert(!site2RecIds.contains(branch3Ver3.Id));

        List<Account> subSet = new List<Account>{
                branch3Ver1, branch3Ver2, branch3Ver3
        };
        Set<Id> site1RecIdsSubset = new Map<Id, SObject>(helper.getCorrectVersions(vs1.Id, subSet)).keySet();
        System.assert(!site1RecIdsSubset.contains(branch1Ver1.Id));
        System.assert(!site1RecIdsSubset.contains(branch2Ver1.Id));
        System.assert(!site1RecIdsSubset.contains(branch2Ver2.Id));
        System.assert(!site1RecIdsSubset.contains(branch2Ver3.Id));
        System.assert(!site1RecIdsSubset.contains(branch3Ver1.Id));
        System.assert(!site1RecIdsSubset.contains(branch3Ver2.Id));
        System.assert(site1RecIdsSubset.contains(branch3Ver3.Id));

        helper.getCorrectVersions(new Set<Id>{
                vs1.Id, vs2.Id
        });
        helper.getCorrectVersions(vs1.Id, new Map<Id, Account>(subSet).keySet());

        helper.isPAApproved(paIdMap.values().get(1));
        Test.stopTest();
    }
}