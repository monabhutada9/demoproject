/**
* @author: Carl Judge
* @date: 09-Aug-19
**/

@IsTest
private class VT_R2_SstmSharingHandlerTest {
    @TestSetup
    static void testSetup() {
        VT_R3_GlobalSharing.disableForTest = false;
        Test.startTest();
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'Primary Investigator'];
        Contact contact = new Contact(LastName = 'PI contact', Email = 'PI@email.com');
        insert contact;
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();

        User piUser = new User(
            ProfileId = prof.Id,
            ContactId = contact.Id,
            FirstName = 'PITestUserForSharing',
            LastName = 'PITestUserForSharing',
            Email = VT_D1_TestUtils.generateUniqueUserName(),
            Username = VT_D1_TestUtils.generateUniqueUserName(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            IsActive = true
        );
        System.runAs (new User(Id = UserInfo.getUserId())) {
            insert piUser;
        }
        insert new Study_Team_Member__c(Study__c = study.Id, User__c = piUser.Id);
        VT_R5_PatientConversionSharing.createConversionShares(new Map<Id, Case>([SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1]).keySet());
    }

    @IsTest
    static void testInsert() {
        Case carePlan = [SELECT Id, VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c, VTD1_Virtual_Site__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];
        Study_Team_Member__c newPiStm = [SELECT Id, User__c, Study__c FROM Study_Team_Member__c WHERE User__r.FirstName = 'PITestUserForSharing'];

        Study_Site_Team_Member__c sstm = new Study_Site_Team_Member__c(
            VTR2_Associated_PI3__c = carePlan.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c,
            VTR2_Associated_SubI__c = newPiStm.Id
        );

        VT_R3_GlobalSharing.disableForTest = true;
        insert new VTD1_Study_Sharing_Configuration__c(VTD1_User__c = newPiStm.User__c, VTD1_Study__c = newPiStm.Study__c, VTD1_Access_Level__c = 'Edit');
        VT_R3_GlobalSharing.disableForTest = false;

        Test.startTest();
        insert sstm;
        Test.stopTest();
        System.assert([SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE RecordId = :carePlan.Id AND UserId = :newPiStm.User__c].HasEditAccess);
    }

    @IsTest
    static void testDelete() {
        Case carePlan = [SELECT Id, VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c, VTD1_Virtual_Site__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];
        Study_Team_Member__c newPiStm = [SELECT Id, User__c FROM Study_Team_Member__c WHERE User__r.FirstName = 'PITestUserForSharing'];
        Study_Site_Team_Member__c sstm = new Study_Site_Team_Member__c(
            VTR2_Associated_PI3__c = carePlan.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c,
            VTR2_Associated_SubI__c = newPiStm.Id
        );

        VT_R3_GlobalSharing.disableForTest = false;
        Test.startTest();
        insert sstm;
        System.assert([SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE RecordId = :carePlan.Id AND UserId = :newPiStm.User__c].HasEditAccess);
        delete sstm;
        Test.stopTest();
        System.assert(![SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE RecordId = :carePlan.Id AND UserId = :newPiStm.User__c].HasEditAccess);
    }
}