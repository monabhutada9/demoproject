/**
 * @author: Dmitry Yartsev
 * @date: 03.06.2020
 * @description: Test class for VT_R5_ConnectedDeviceUpdateFieldsBatch
 */

@IsTest
private with sharing class VT_R5_ConnDeviceUpdateFieldsBatchTest {
    private static final Datetime LAST_READING_DATETIME = Datetime.now().addHours(-1);
    private static final String NOT_CONTINOUS = 'NOT_CONTINOUS';
    private static final String NOT_CONTINOUS_READING_DUE = 'NOT_CONTINOUS_READING_DUE';
    private static final String WITHOUT_READINGS = 'WITHOUT_READINGS';
    
    private static Id caseId;

    @TestSetup
    private static void setup() {
        caseId = getCaseId();
        Id protocolDeviceId = getProtocolDeviceId();
        List<VTD1_Patient_Device__c> devices = new List<VTD1_Patient_Device__c>();
        devices.add(createDevice(WITHOUT_READINGS, protocolDeviceId));
        devices.add(createDevice(NOT_CONTINOUS, protocolDeviceId));
        devices.add(createDevice(NOT_CONTINOUS_READING_DUE, protocolDeviceId));
        insert devices;
    }

    @IsTest
    private static void testUpdateFields() {
        List<VTD1_Patient_Device__c> devices = new List<VTD1_Patient_Device__c>();
        for (VTD1_Patient_Device__c device : [SELECT VTR5_LastReadingDate__c, VTR5_Next_Reading_Date__c, VTR5_SyncStatus__c, VTD1_Model_Name__c FROM VTD1_Patient_Device__c]) {
            if (device.VTD1_Model_Name__c != WITHOUT_READINGS) {
                device.VTD1_Distribution_Vendor__c = 'testForReplace';
                devices.add(getDeviceWithReadings(device, device.VTD1_Model_Name__c == NOT_CONTINOUS_READING_DUE));
            } else {
                devices.add(device);
            }
        }
        Test.startTest();
        {
            new QueryBuilder()
                    .buildStub()
                    .addStubToList(devices)
                    .namedQueryStub('VT_R5_ConnectedDeviceUpdateFieldsBatch_QueryLocator')
                    .applyStub();
            System.schedule('Update fields on Patient Devices' + Crypto.getRandomInteger(), '0 0 * * * ?',  new VT_R5_ConnectedDeviceUpdateFieldsBatch());
            Database.executeBatch(new VT_R5_ConnectedDeviceUpdateFieldsBatch(), 500);
        }
        Test.stopTest();
        System.assertEquals(1, [SELECT COUNT() FROM VTD1_Patient_Device__c WHERE VTR5_SyncStatus__c = 'Up to Date']);
        System.assertEquals(2, [SELECT COUNT() FROM VTD1_Patient_Device__c WHERE VTR5_SyncStatus__c = 'Reading Due']);
    }

    private static VTD1_Patient_Device__c createDevice(String name, Id protocolDeviceId) {
        return new VTD1_Patient_Device__c(
                VTD1_Model_Name__c = name,
                VTR5_LastReadingDate__c = LAST_READING_DATETIME,
                VTR5_LastDataSync__c = null,
                VTR5_SyncStatus__c = null,
                VTD1_Protocol_Device__c = protocolDeviceId,
                VTR5_IMEI__c = Crypto.getRandomInteger() + name,
                VTD1_Case__c = caseId
        );
    }

    private static Id getCaseId() {
        Case cas = new Case();
        insert cas;
        return cas.Id;
    }

    private static Id getProtocolDeviceId() {
        Product2 product = new Product2(
                Name = 'TestProduct2',
                VTR5_IsContinous__c = false,
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('VTD1_Connected_Devices').getRecordTypeId()
        );
        insert product;


        VTD1_Protocol_Kit__c protocolKit = new VTD1_Protocol_Kit__c();
        insert protocolKit;


        HealthCloudGA__EhrDevice__c ehrDevice = new HealthCloudGA__EhrDevice__c(
                VTD1_Device__c = product.Id,
                VTR5_MeasurementFrequency__c = 0.0,
                VTR5_Measurement_Type__c = 'heart_rate',


                VTD1_Kit_Type__c = 'Connected Devices',
                VTD1_Protocol_Kit__c = protocolKit.Id


        );
        insert ehrDevice;
        return ehrDevice.Id;
    }

    private static VTD1_Patient_Device__c getDeviceWithReadings(VTD1_Patient_Device__c device, Boolean isReadingDue) {
        Map<String, Object> readingMap = new  Map<String, Object>{
                'totalSize' => 1,
                'done' => true,
                'records' => new List<VTR5_publicreadings__x>{
                        new VTR5_publicreadings__x(
                                VTR5_date__c = isReadingDue
                                        ? LAST_READING_DATETIME.addMinutes(-1)
                                        : Datetime.now()
                        )
                }
        };
        String deviceJSON = JSON.serialize(device).replace('VTD1_Distribution_Vendor__c', 'public_readings1__r').replace('"testForReplace"', JSON.serialize(readingMap));
        return (VTD1_Patient_Device__c)JSON.deserialize(deviceJSON, VTD1_Patient_Device__c.class);
    }
}