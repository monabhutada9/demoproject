/**
 * Created by Alexander Komarov on 19.03.2019.
 * @see VT_R2_PatientHistoryHandlerTest
 */

global without sharing class VT_R2_PatientHistoryScheduler implements Schedulable {
    Integer minutesPeriod = 15;
    public VT_R2_PatientHistoryScheduler() {}
    public VT_R2_PatientHistoryScheduler(Integer minutesPeriod) {
        this.minutesPeriod = minutesPeriod;
    }
    public void execute(SchedulableContext sc) {
        System.abortJob(sc.getTriggerId());
        Datetime now = System.now();
        Database.executeBatch(new VT_R5_PatientHistoryBatch(), 2000);
        Datetime nextRun = now.addMinutes(minutesPeriod);
        String day = String.valueOf(nextRun.day());
        String month = String.valueOf(nextRun.month());
        String hour = String.valueOf(nextRun.hour());
        String minute = String.valueOf(nextRun.minute());
        String year = String.valueOf(nextRun.year());
        String strJobName = 'Patient Field History (archives to Big Object)';
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
        if (!Test.isRunningTest()) {
            System.schedule(strJobName, strSchedule, new VT_R2_PatientHistoryScheduler(minutesPeriod));
        }
    }
}