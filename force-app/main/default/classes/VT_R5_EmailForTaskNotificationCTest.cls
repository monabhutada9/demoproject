/**
* Test Class for VT_R5_EmailGeneratorForTaskNotificationC.
*/
@isTest
public class VT_R5_EmailForTaskNotificationCTest {
    
    @testSetup static void setup(){
        VTD1_NotificationC__c notification1 = VT_D1_TestUtils.CreateNotificationC(Userinfo.getUserId(), Userinfo.getUserId(), 'Test Title', 'Test Message');
        Insert notification1;
        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.setupRecordTypes();
    }
    
    static testMethod void EmailTest1(){
        List<VTD1_NotificationC__c> notList = [SELECT Id,Title__c,OwnerId,VTD2_Title_Parameters__c,Link_to_related_event_or_object__c,CreatedDate,VTD2_New_Message_Added_DateTme__c,
                                              FORMAT(CreatedDate)CreatedDateF,FORMAT(VTD2_New_Message_Added_DateTme__c)VTD2_New_Message_Added_DateTme__cF,Message__c,VTD1_Read__c,
                                              Number_of_unread_messages__c,Type__c,HasDirectLink__c,isActualVisitLink__c,VTD1_Parameters__c,VTD1_CSM__c,VTD2_VisitID__c,
                                              VTR2_PatientNotificationId__c,VDT2_Unique_Code__c,VTR5_Email_Body__c,VTR5_Email_Body_Parameters__c,VTR5_Email_Subject__c,
                                              VTR5_Email_Subject_Parameters__c FROM VTD1_NotificationC__c];
        
        VT_R5_EmailGeneratorForTaskNotificationC.SendEmail(notList);
        
        for(VTD1_NotificationC__c nt : notList){
            nt.VTR5_Email_Body__c = '';
            nt.VTR5_Email_Subject__c = '';
        }
        
        VT_R5_EmailGeneratorForTaskNotificationC.sendEmailNotification(JSON.serialize(notList));
    }
}