/**
 * @author: Alexander Komarov
 * @date: 23.09.2020
 * @description: Test for VT_R5_MobilePushNotifications
 */

@IsTest
private class VT_R5_MobilePushNotificationsTest {
    @IsTest
    static void testBehavior() {

        VTD1_NotificationC__c n = new VTD1_NotificationC__c();
        n.Type__c = 'Messages';
        n.Title__c = 'Messages';
        n.VTD1_Receivers__c = UserInfo.getUserId();
        n.VTD2_New_Message_Added_DateTme__c = Datetime.now();
        n.Message__c = 'Some message';
        n.Number_of_unread_messages__c = 10.0;
        n.VTR3_Notification_Type__c = 'eCOA Notification';
        insert n;

        VTD1_NotificationC__c n2 = new VTD1_NotificationC__c();
        n2.Type__c = 'General';
        n2.VTD1_Receivers__c = UserInfo.getUserId();
        n2.VTD2_New_Message_Added_DateTme__c = Datetime.now();
        n2.Message__c = 'Some message';
        n2.Link_to_related_event_or_object__c = 'calendar';
        insert n2;

        List<VTR5_MobilePushRegistrations__c> toInsert = new List<VTR5_MobilePushRegistrations__c>();
        for (Integer i = 0; i<50; i++) {
            toInsert.add(new VTR5_MobilePushRegistrations__c(
                    VTR5_ConnectionToken__c = 'token'+i,
                    VTR5_DeviceIdentifier__c = 'devId'+i,
                    VTR5_ServiceType__c = 'Android',
                    VTR5_User__c = UserInfo.getUserId()
            ));
            toInsert.add(new VTR5_MobilePushRegistrations__c(
                    VTR5_ConnectionToken__c = 'token2'+i,
                    VTR5_DeviceIdentifier__c = 'devId2'+i,
                    VTR5_ServiceType__c = 'iOS',
                    VTR5_User__c = UserInfo.getUserId()
            ));
        }

        insert toInsert;

        Map<Id, VTD1_NotificationC__c> notificationsToSend = new Map<Id, VTD1_NotificationC__c>();

        User u = [SELECT Id, (SELECT Id, VTR5_DeviceIdentifier__c, VTR5_ServiceType__c, VTR5_ConnectionToken__c FROM MobilePushRegistrations__r) FROM User WHERE Id = :UserInfo.getUserId()];

        Map<Id, User> notificationWithUser = new Map<Id, User>{n.Id => u, n2.Id => u};

        notificationsToSend.put(n.Id, n);
        notificationsToSend.put(n2.Id, n2);

        VT_R5_MobilePushNotifications mobilePushNotifications = new VT_R5_MobilePushNotifications(notificationWithUser,notificationsToSend);
        mobilePushNotifications.execute();
    }
}