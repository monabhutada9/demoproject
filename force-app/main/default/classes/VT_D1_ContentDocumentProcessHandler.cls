public with sharing class VT_D1_ContentDocumentProcessHandler {
	public static Boolean skipSetFileNamesOnDoc = false;




	/**
	* @author Eugene Pavlovskiy
	* @date 28/05/2020
	* @see VT_D1_ContentDocumentLinkProcessHandler.recalculateLastFileUploadedDate()
	* documentToRecalculateList required for passing to VT_D1_ContentDocumentLinkProcessHandler.recalculateLastFileUploadedDate()
	* */
	private static List<ContentDocumentLink> documentToRecalculateList;




    public static void setFileNamesOnDocumentWhenFileIsTriggered(List<ContentDocument> newContentDocumentList) {
		set<Id> contentDocIdSet = new set<Id>();

    	for (ContentDocument newCD : newContentDocumentList) {
    		contentDocIdSet.add(newCD.Id);
    	}




    	List<ContentDocumentLink> contentDocLinkList = [select Id, ContentDocumentId, LinkedEntityId, ShareType, Visibility
    													FROM ContentDocumentLink where ContentDocumentId in:contentDocIdSet];


    	if(contentDocLinkList.isEmpty()) return;
    	
    	List<VTD1_Document__c> docContainersList = VT_D1_ContentDocumentLinkProcessHandler.getDocContainersList(contentDocLinkList);
    	if(docContainersList==null || docContainersList.isEmpty()) return;
    	
    	map<Id, VTD1_Document__c> docId_doc = new map<Id, VTD1_Document__c>(docContainersList); 
    	
    	setFileNamesOnDocumentFuture(docId_doc.keySet());
    }




	/**
	* 	Method prepared data for recalculate content document
	*	@author Eugene Pavlovskiy
	* 	@date 28/05/2020
	*	@see VT_D1_ContentDocumentProcess
	* */
	public static void preparedDataForRecalculateContentDocument(List<ContentDocument> contentDocumentList){
		/**  contentDocumentIdSet takes Id's deleted contentDocument's	 */
		Set<Id> contentDocumentIdSet = new Set<Id>();
		for (ContentDocument currentContentDocument : contentDocumentList) {
			contentDocumentIdSet.add(currentContentDocument.Id);
		}
		/** contentDocumentLinkList takes list ContentDocumentLink from all  ContentDocumentLink where ContentDocumentId include contentDocumentIdSet*/
		List<ContentDocumentLink> contentDocumentLinkList = [
				SELECT	Id,
						ContentDocumentId,
						LinkedEntityId,
						ShareType,
						Visibility
				FROM	ContentDocumentLink
				WHERE	ContentDocumentId IN:contentDocumentIdSet
		];
		/**contentDocumentLinksIds takes LinkedEntityId  */
		Set<Id> contentDocumentLinksIds = new Set<Id>();
		for (ContentDocumentLink contentDocumentLink : contentDocumentLinkList){
			contentDocumentLinksIds.add(contentDocumentLink.LinkedEntityId);
		}


		documentToRecalculateList = [
				SELECT	Id,
						ContentDocumentId,
						LinkedEntityId
				FROM	ContentDocumentLink
				WHERE	LinkedEntityId  =:contentDocumentLinksIds AND LinkedEntity.Type ='VTD1_Document__c'
		];


	}
	/**
	* executeRecalculateContentDocument used to recalculate the number of displayed documents on the page by updating the field VTR5_LastFileUploadedDate__c visible only to the system
	* @author Eugene Pavlovskiy
	* @date 28/05/2020
	* @see VT_D1_ContentDocumentLinkProcessHandler.recalculateLastFileUploadedDate()
	* */
	public static void executeRecalculateContentDocument(){
		VT_D1_ContentDocumentLinkProcessHandler.recalculateLastFileUploadedDate(documentToRecalculateList);
	} 



    @future
    public static void setFileNamesOnDocumentFuture(set<Id> docIds){
    	List<VTD1_Document__c> docContainersList = [select Id, VTD1_FileNames__c from VTD1_Document__c where Id in:docIds];
    	if(docContainersList==null || docContainersList.isEmpty()) return;
    	
    	map<Id, VTD1_Document__c> docId_doc = new map<Id, VTD1_Document__c>(docContainersList); 






		VT_D1_DocumentCHandlerWithoutSharing.setSharedFileNamesByVTD1Documents(docContainersList, docId_doc);

    	//VT_D1_ContentDocumentLinkProcessHandler.setFileNamesOnDocument(contentDocLinkList);
    }

	public static void sendPlatformEventToRefreshPage(List<ContentDocument> documents) {
        List<ContentDocumentLink> mvDocs = [
                SELECT ContentDocumentId, LinkedEntityId
                FROM ContentDocumentLink
                WHERE ContentDocumentId IN :documents AND LinkedEntityId IN (SELECT Id FROM VTD1_Monitoring_Visit__c)
        ];
        if (!mvDocs.isEmpty()) {
            List<VTR5_FilesChange__e> fileUploadEvents = new List<VTR5_FilesChange__e>();
            for (ContentDocumentLink contentDocumentLink : mvDocs) {
                fileUploadEvents.add(
                        new VTR5_FilesChange__e(
                                Id__c = contentDocumentLink.ContentDocumentId,
                                LinkedEntityId__c = contentDocumentLink.LinkedEntityId,
                                isDeleted__c = true
                        )
                );

	


            }
            EventBus.publish(fileUploadEvents);
        }
    }


	public static void checkReasonIsEmpty(List<ContentDocument> oldContentDocuments, Map<Id, ContentDocument> oldContentDocumentsByIds) {
		List<ContentDocumentLink> contentDocumentLinks = [
				SELECT Id, LinkedEntityId, ContentDocumentId
				FROM ContentDocumentLink
				WHERE ContentDocumentId IN : oldContentDocumentsByIds.keySet()];


		if (contentDocumentLinks.size() > 0) {
			List<String> linkedEntityIds = new List<String>();
			for (ContentDocumentLink each : contentDocumentLinks) {
				linkedEntityIds.add(each.LinkedEntityId);
			}
			List<VTD1_Document__c> currentDocumentsWithEmptyReason = [
					SELECT Id, Name, VTR4_DeletionReason__c
					FROM VTD1_Document__c
					WHERE Id IN :linkedEntityIds
					AND VTR4_DeletionReason__c = NULL
			];
			System.debug(currentDocumentsWithEmptyReason);
			if (currentDocumentsWithEmptyReason.size() > 0) {
				oldContentDocuments[0].addError('The field "Deletion Reason" must be filled');
			}
		}
	}
    



	public static void validateDocumentDeletionReasonOnDeleteContentDocument(Map<Id, ContentDocument> oldContentDocumentIdtoContentDocument) {
		Set<Id> documentIds = new Set<Id>();
		String documentSobjectName = VTD1_Document__c.getSObjectType().getDescribe().getName();
		for (ContentDocumentLink contentDocumentLink : [
				SELECT Id, LinkedEntityId, ContentDocumentId
				FROM ContentDocumentLink
				WHERE ContentDocumentId IN : oldContentDocumentIdtoContentDocument.keySet()
		]) {
			String LinkedEntitySobjectName = contentDocumentLink.LinkedEntityId.getSobjectType().getDescribe().getName();
			if (documentSobjectName == LinkedEntitySobjectName) {
				documentIds.add(contentDocumentLink.LinkedEntityId);
			}
		}



		if (!documentIds.isEmpty()) {
			Set<Id> contentDocumentIdsRelatedToDocumentWithEmptyDeletionReason = new Set<Id>();
			for (VTD1_Document__c document : [
					SELECT Id, (SELECT ContentDocumentId FROM ContentDocumentLinks)
					FROM VTD1_Document__c
					WHERE Id IN :documentIds
					AND VTR4_DeletionReason__c = NULL
					AND VTD1_Regulatory_Binder__c != NULL


			]) {
				for (ContentDocumentLink contentDocumentLink : document.ContentDocumentLinks) {
					contentDocumentIdsRelatedToDocumentWithEmptyDeletionReason.add(contentDocumentLink.ContentDocumentId);
				}
			}
			for (ContentDocument contentDocument : oldContentDocumentIdtoContentDocument.values()) {
				if (contentDocumentIdsRelatedToDocumentWithEmptyDeletionReason.contains(contentDocument.Id)) {
					contentDocument.addError(System.Label.VTR4_ErrorDeletionReasonEmpty);
				}
			}
		}
	}
}