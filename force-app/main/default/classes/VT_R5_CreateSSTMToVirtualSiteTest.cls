/********************************************************
 * @Class Name   : VT_R5_CreateSSTMToVirtualSiteTest
 * @Created Date : 15/10/2020 
 * @Version      : R5.5


 * @group-content: http://jira.quintiles.net/browse/SH-17314
 * @Author       : Rajesh Narode
 * @Purpose      : This is the test class for the class VT_R5_CreateSSTMToVirtualSite
 * *****************************************************/
@isTest
public with Sharing class VT_R5_CreateSSTMToVirtualSiteTest {
/********************************************************
 * @Method Name  : createVirtualSiteTest
 * @Params       : NA
 * @Created Date : 15/10/2020 
 * @Version      : R5.5
 * @Purpose      : Is i s used to test the functionality of the Batch class VT_R5_CreateSSTMToVirtualSite
 * *****************************************************/    
    @isTest
    private static void createVirtualSiteTest(){
        VT_R5_ShareGroupMembershipService.disableMembership = true;
        
            DomainObjects.VirtualSite_t virtualSite = createVirtualSite();
            virtualSite.persist();
        
        User objUser = [SELECT id from user where ID=:userInfo.getUserId()];
        System.runAs(objUser){
            Test.startTest();
            DomainObjects.User_t mrrUser = new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.MRR_PROFILE_NAME)
                .setEmail('test.mrr@test.mrr');
            mrrUser.persist();
            
            DomainObjects.User_t mrrUserOn = new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.MRR_PROFILE_NAME)
                .setEmail('test.mrrSec@test.mrr');
            mrrUserOn.persist();
            
            List<Virtual_Site__c> virtSiteList = [SELECT Id,
                                                  VTD1_Remote_CRA__c,
                                                  VTR2_Onsite_CRA__c,
                                                  VTD1_Study__c
                                                  FROM Virtual_Site__c
                                                  WHERE Id = :virtualSite.Id
                                                 ];
            virtSiteList[0].VTD1_Remote_CRA__c = mrrUser.Id;
            virtSiteList[0].VTR2_Onsite_CRA__c = mrrUserOn.Id;
            update virtSiteList;
            
            VT_R5_CreateSSTMToVirtualSite objBatch = New VT_R5_CreateSSTMToVirtualSite(new Set<ID>{virtSiteList[0].ID});
            Database.executeBatch(objBatch);
            
            Test.stopTest();
        }
        List<Virtual_Site__c> virtSiteListTest = [
            SELECT Id ,VTR5_CRA_Queue__c,VTD1_Study_Team_Member__r.VTR2_Remote_CRA__c,
            (Select Id,VTR5_Associated_CRA__c
                        from Study_Site_Team_Members__r Limit 10)
            FROM Virtual_Site__c
            WHERE Id = :virtualSite.Id
        ];
        List<Group> lstGroup = [Select id,Name from Group where type='Queue' AND Name=:virtSiteListTest[0].Id+'_CRAQueue'];
        System.assertNotEquals(Null, virtSiteListTest[0].Study_Site_Team_Members__r);
        System.assertEquals(1, lstGroup.size());
        System.assertEquals(virtSiteListTest[0].VtR5_CRA_Queue__c, lstGroup[0].Id);
        System.assertNotEquals(Null,virtSiteListTest[0].Study_Site_Team_Members__r[0].VTR5_Associated_CRA__c);
        
    }
  /********************************************************
 * @Method Name  : negativeCreateVirtualSiteTest
 * @Params       : NA
 * @Created Date : 06/11/2020 
 * @Version      : R5.5
 * @Purpose      : Is i s used to test the functionality of the Batch class VT_R5_CreateSSTMToVirtualSite
 * *****************************************************/    
@isTest
private static void negativeCreateVirtualSiteTest(){
    VT_R5_ShareGroupMembershipService.disableMembership = true;
    
        DomainObjects.VirtualSite_t virtualSite = createVirtualSite();
        virtualSite.persist();
    
    User objUser = [SELECT id from user where ID=:userInfo.getUserId()];
    System.runAs(objUser){
        Test.startTest();
        
        
        List<Virtual_Site__c> virtSiteList = [SELECT Id,
                                              VTD1_Remote_CRA__c,
                                              VTR2_Onsite_CRA__c,
                                              VTD1_Study__c
                                              FROM Virtual_Site__c
                                              WHERE Id = :virtualSite.Id
                                             ];

        List<Study_Team_Member__c> objSTM = [SELECT ID,
                                             Study__c,
                                             User__c,
                                             VTD1_Type__c 
                                             FROM  Study_Team_Member__c
                                             WHERE VTD1_Type__c = 'CRA'
                                             AND Study__c =: virtSiteList[0].VTD1_Study__c];
                                                                                   
        virtSiteList[0].VTD1_Remote_CRA__c = objSTM[0].User__c;
        virtSiteList[0].VTR2_Onsite_CRA__c = objSTM[1].User__c;
        virtSiteList[0].VTD1_Study_Team_Member__c = Null;
        update virtSiteList;
        
        VT_R5_CreateSSTMToVirtualSite objBatch = New VT_R5_CreateSSTMToVirtualSite(new Set<ID>{virtSiteList[0].ID});
        Database.executeBatch(objBatch);
        
        Test.stopTest();
    }
    List<Virtual_Site__c> virtSiteListTest = [
        SELECT Id ,VTR5_CRA_Queue__c,
        (Select Id
                    from Study_Site_Team_Members__r Limit 10)
        FROM Virtual_Site__c
        WHERE Id = :virtualSite.Id
    ];
    List<Group> lstGroup = [Select id,Name from Group where type='Queue' AND Name=:virtSiteListTest[0].Id+'_CRAQueue'];
    System.assertNotEquals(Null, virtSiteListTest[0].Study_Site_Team_Members__r);
    System.assertEquals(1, lstGroup.size());
    System.assertEquals(virtSiteListTest[0].VtR5_CRA_Queue__c, lstGroup[0].Id);
    
}  
 /********************************************************
 * @Method Name  : negativeSameCRATest
 * @Params       : NA
 * @Created Date : 06/11/2020 
 * @Version      : R5.5
 * @Purpose      : Is i s used to test the functionality of the Batch class VT_R5_CreateSSTMToVirtualSite
 * *****************************************************/    
@isTest
private static void negativeSameCRATest(){
    VT_R5_ShareGroupMembershipService.disableMembership = true;
    
        DomainObjects.VirtualSite_t virtualSite = createVirtualSite();
        virtualSite.persist();
    
    User objUser = [SELECT id from user where ID=:userInfo.getUserId()];
    System.runAs(objUser){
        Test.startTest();
        
        
        List<Virtual_Site__c> virtSiteList = [SELECT Id,
                                              VTD1_Remote_CRA__c,
                                              VTR2_Onsite_CRA__c,
                                              VTD1_Study__c
                                              FROM Virtual_Site__c
                                              WHERE Id = :virtualSite.Id
                                             ];

        List<Study_Team_Member__c> objSTM = [SELECT ID,
                                             Study__c,
                                             User__c,
                                             VTD1_Type__c 
                                             FROM  Study_Team_Member__c
                                             WHERE VTD1_Type__c = 'CRA'
                                             AND Study__c =: virtSiteList[0].VTD1_Study__c];
                                                                                   
        virtSiteList[0].VTD1_Remote_CRA__c = objSTM[0].User__c;
        virtSiteList[0].VTR2_Onsite_CRA__c = objSTM[0].User__c;
        virtSiteList[0].VTD1_Study_Team_Member__c = Null;
        update virtSiteList;
        
        VT_R5_CreateSSTMToVirtualSite objBatch = New VT_R5_CreateSSTMToVirtualSite(new Set<ID>{virtSiteList[0].ID});
        Database.executeBatch(objBatch);
        
        Test.stopTest();
    }
    List<Virtual_Site__c> virtSiteListTest = [
        SELECT Id ,VTR5_CRA_Queue__c,
        (Select Id
                    from Study_Site_Team_Members__r Limit 10)
        FROM Virtual_Site__c
        WHERE Id = :virtualSite.Id
    ];
    List<Group> lstGroup = [Select id,Name from Group where type='Queue' AND Name=:virtSiteListTest[0].Id+'_CRAQueue'];
    System.assertNotEquals(Null, virtSiteListTest[0].Study_Site_Team_Members__r);
    System.assertEquals(1, lstGroup.size());
    System.assertEquals(virtSiteListTest[0].VtR5_CRA_Queue__c, lstGroup[0].Id);
    
}      
/********************************************************
 * @Method Name  : createVirtualSite
 * @Params       : NA
 * @Created Date : 15/10/2020 
 * @Version      : R5.5
 * @Purpose      : Is i s used to create the Test virtual site data for the class
 * *****************************************************/    
     private static DomainObjects.VirtualSite_t createVirtualSite() {

   DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setName('Study Name')
                
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));
		study.persist();
        DomainObjects.StudyTeamMember_t stmOnsiteCra = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA')
                )
                .addStudy(study);
        stmOnsiteCra.persist();

        DomainObjects.StudyTeamMember_t stmRemoteCra = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA'))
                .addStudy(study);
        stmRemoteCra.persist();

        DomainObjects.StudyTeamMember_t stm = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('SCR')
                .addUser(new DomainObjects.User_t()
                        .addContact(new DomainObjects.Contact_t())
                        .setProfile('Site Coordinator'))
                .addStudy(study);
         
	     stm.persist();
         Study_Team_Member__c objSTM = [SELECT id,VTR2_Onsite_CRA__c,VTR2_Remote_CRA__c from  Study_Team_Member__c
                                        where Recordtype.Name='SCR' Limit 1];
         objSTM.VTR2_Onsite_CRA__c = stmOnsiteCra.ID;
         objSTM.VTR2_Remote_CRA__c = stmRemoteCra.id;
         update objSTM;
         return new DomainObjects.VirtualSite_t()
             .addStudyTeamMember(stm)
             .addStudy(study)
             .setStudySiteNumber('12345');
             
     }
}