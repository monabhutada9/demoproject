/**
* @author: Carl Judge
* @date: 14-Feb-19
* @description: Reassign PGs on Cases (probably after PI change)
**/

public without sharing class VT_R2_PGReassigner {
    private enum PGType {PRIMARY, BACKUP}
    private static final Map<PGType, String> PG_FIELDS = new Map<PGType, String> {
        PGType.PRIMARY => 'VTD1_Primary_PG__c',
        PGType.BACKUP => 'VTD1_Secondary_PG__c'
    };
    private static final Map<PGType, String> TIMESTAMP_FIELDS = new Map<PGType, String> {
        PGType.PRIMARY => 'VTD1_Last_Assigned_Patient__c',
        PGType.BACKUP => 'VTD1_Secondary_Last_Assigned_Patient__c'
    };

    private List<Case> cases;
    private Map<PGType, Map<Id, List<Study_Site_Team_Member__c>>> sstmMap = new Map<PGType, Map<Id, List<Study_Site_Team_Member__c>>>(); // PGType => Site ID => SSTMs
    private Map<PGType, Map<Id, Set<Id>>> roundRobinUserIdMap = new Map<PGType, Map<Id, Set<Id>>>(); // PGType => Site ID => PGIds
    private Map<Id, Study_Team_Member__c> studyMembersToUpdate = new Map<Id, Study_Team_Member__c>();

    private VT_R2_PGReassigner(List<Case> cases) {
        this.cases = cases;
    }

    public static void reassignPGs(List<Case> cases) {
        new VT_R2_PGReassigner(cases).execute();
    }

    private void execute() {
        getMaps();
        for (Case cas : this.cases) {
            setPG(cas, PGType.PRIMARY);
            setPG(cas, PGType.BACKUP);
        }
        update studyMembersToUpdate.values();
    }

    private void getMaps() {
        List<Id> siteIds = new List<Id>();
        for (Case item : this.cases) {
            if (item.VTD1_Virtual_Site__c != null) { siteIds.add(item.VTD1_Virtual_Site__c); }
        }

        Map<Id, List<Study_Site_Team_Member__c>> priSstmMap = getRoundRobinMap(siteIds, PGType.PRIMARY);
        Map<Id, List<Study_Site_Team_Member__c>> secSstmMap = getRoundRobinMap(siteIds, PGType.BACKUP);
        Map<Id, Set<Id>> priUserIdMap = getUserIdMap(priSstmMap);
        Map<Id, Set<Id>> secUserIdMap = getUserIdMap(secSstmMap);

        this.sstmMap.put(PGType.PRIMARY, priSstmMap);
        this.sstmMap.put(PGType.BACKUP, secSstmMap);
        this.roundRobinUserIdMap.put(PGType.PRIMARY, priUserIdMap);
        this.roundRobinUserIdMap.put(PGType.BACKUP, secUserIdMap);
    }

    private Map<Id, List<Study_Site_Team_Member__c>> getRoundRobinMap(List<Id> siteIds, PGType pgTyp) {
        Map<Id, List<Study_Site_Team_Member__c>> roundRobinMap = new Map<Id, List<Study_Site_Team_Member__c>>();

        String query =
            'SELECT VTD1_Associated_PI__r.VTD1_VirtualSite__c, VTD1_Associated_PG__c, VTD1_Associated_PG__r.User__c' +
                ' FROM Study_Site_Team_Member__c' +
                ' WHERE VTD1_Associated_PI__r.VTD1_VirtualSite__c IN :siteIds' +
                ' AND VTD1_Ready_For_Assignment__c = true' +
                ' ORDER BY ' + TIMESTAMP_FIELDS.get(pgTyp) + ' ASC NULLS FIRST';

        for (Study_Site_Team_Member__c item : Database.query(query)) {
            if (! roundRobinMap.containsKey(item.VTD1_Associated_PI__r.VTD1_VirtualSite__c)) {
                roundRobinMap.put(item.VTD1_Associated_PI__r.VTD1_VirtualSite__c, new List<Study_Site_Team_Member__c>());
            }
            roundRobinMap.get(item.VTD1_Associated_PI__r.VTD1_VirtualSite__c).add(item);
        }

        return roundRobinMap;
    }

    // these maps are just to check if a Patient already has a PG in the round robin list, no need to change in this case
    private Map<Id, Set<Id>> getUserIdMap(Map<Id, List<Study_Site_Team_Member__c>> sstmMap) {
        Map<Id, Set<Id>> userIdMap = new Map<Id, Set<Id>>();
        for (Id siteId : sstmMap.keySet()) {
            userIdMap.put(siteId, new Set<Id>());
            for (Study_Site_Team_Member__c sstm : sstmMap.get(siteId)) {
                userIdMap.get(siteId).add(sstm.VTD1_Associated_PG__r.User__c);
            }
        }
        return userIdMap;
    }

    private void setPG(Case cas, PGType pgTyp) {
        List<Study_Site_Team_Member__c> siteMemberList = this.sstmMap.get(pgTyp).get(cas.VTD1_Virtual_Site__c);
        Set<Id> roundRobinUserIds = this.roundRobinUserIdMap.get(pgTyp).get(cas.VTD1_Virtual_Site__c);
        String pgField = PG_FIELDS.get(pgTyp);
        String timeTrackingField = TIMESTAMP_FIELDS.get(pgTyp);
        Integer index = 0;
        if (pgTyp == PGType.BACKUP) { // for backup PI, if pri PG is the same as first queued PG, choose idx 1 for backup PG so they are different
            index = siteMemberList != null && siteMemberList[0].VTD1_Associated_PG__r.User__c == cas.VTD1_Primary_PG__c ? 1 : 0;
        }
        id currentPgId = cas.get(pgField) != null ? (Id)cas.get(pgField) : null;

        if (currentPgId == null || (roundRobinUserIds != null && !roundRobinUserIds.contains(currentPgId))) { // only proceed with PG update if we don't already have a PG from the round robin list
            if (siteMemberList != null && siteMemberList.size() > index) {
                Study_Site_Team_Member__c siteMember = siteMemberList.remove(index);
                siteMemberList.add(siteMember);
                cas.put(pgField, siteMember.VTD1_Associated_PG__r.User__c);

                addSstmToUpdate(siteMember.VTD1_Associated_PG__c, timeTrackingField);
            } else {
                cas.put(pgField, null);
            }
        }
    }

    private void addSstmToUpdate(Id pgId, String timeTrackingField) {
        Study_Team_Member__c studyMember = this.studyMembersToUpdate.containsKey(pgId) ?
            this.studyMembersToUpdate.get(pgId) : new Study_Team_Member__c(Id = pgId);

        studyMember.put(timeTrackingField, Datetime.now());
        this.studyMembersToUpdate.put(studyMember.Id, studyMember);
    }
}