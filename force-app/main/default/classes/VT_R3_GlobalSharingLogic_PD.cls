/**
* @author: Carl Judge
* @date: 08-Sep-20
* @description: Sharing logic for Protocol Deviation records
**/

public without sharing class VT_R3_GlobalSharingLogic_PD extends VT_R3_AbstractGlobalSharingLogic {
    public static final Set<String> PROFILES_TO_SHARE_WITH = new Set<String>{
        VT_R4_ConstantsHelper_ProfilesSTM.MRR_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.PL_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.VTSL_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.PMA_PROFILE_NAME
    };

    public override Type getType() {
        return VT_R3_GlobalSharingLogic_PD.class;
    }

    public override VTR3_GlobalSharingConfig__mdt getConfig() {
        return getConfigForObjectType(VTD1_Protocol_Deviation__c.getSObjectType().getDescribe().getName());
    }

    protected override void addShareDetailsForRecords() {
        List<VTD1_Protocol_Deviation__c> pds = [
            SELECT Id,
                VTD1_StudyId__c,
                VTD1_Virtual_Site__c,
                VTD1_Virtual_Site__r.VTD1_Study__c,
                VTD1_Subject_ID__r.VTD1_Virtual_Site__c,
                VTD1_Subject_ID__r.VTD1_Virtual_Site__r.VTD1_Study__c,
                VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c,
                VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c,
                VTD1_Subject_ID__r.VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c,
                VTD1_Subject_ID__r.VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c
            FROM VTD1_Protocol_Deviation__c
            WHERE Id IN :recordIds
        ];

        Set<Id> studyIds = new Set<Id>();
        for (VTD1_Protocol_Deviation__c pd : pds) {
            Id studyId = getStudyId(pd);
            if (studyId != null) {
                studyIds.add(studyId);
            }
        }
        Map<Id, Set<Id>> studyUserIds = new Map<Id, Set<Id>>();
        if (!studyIds.isEmpty()) { studyUserIds = getStudyUsers(studyIds); }

        for (VTD1_Protocol_Deviation__c pd : pds) {
            Id studyId = getStudyId(pd);
            if (studyUserIds.containsKey(studyId)) {
                addShares(
                    pd.Id,
                    studyUserIds.get(studyId),
                    studyId
                );
            }
            if (pd.VTD1_Virtual_Site__c != null) {
                addShareDetail(pd.Id, pd.VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c, pd.VTD1_Subject_ID__r.VTD1_Virtual_Site__r.VTD1_Study__c, 'Read');
                addShareDetail(pd.Id, pd.VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c, pd.VTD1_Subject_ID__r.VTD1_Virtual_Site__r.VTD1_Study__c, 'Edit');
            }
            if (pd.VTD1_Subject_ID__r.VTD1_Virtual_Site__c != null) {
                addShareDetail(pd.Id, pd.VTD1_Subject_ID__r.VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c, pd.VTD1_Subject_ID__r.VTD1_Virtual_Site__r.VTD1_Study__c, 'Read');
                addShareDetail(pd.Id, pd.VTD1_Subject_ID__r.VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c, pd.VTD1_Subject_ID__r.VTD1_Virtual_Site__r.VTD1_Study__c, 'Edit');
            }
        }
    }

    private Id getStudyId(VTD1_Protocol_Deviation__c pd) {
        return pd.VTD1_StudyId__c != null
            ? pd.VTD1_StudyId__c
            : pd.VTD1_Virtual_Site__r.VTD1_Study__c != null
                ? pd.VTD1_Virtual_Site__r.VTD1_Study__c
                : pd.VTD1_Subject_ID__r.VTD1_Virtual_Site__r.VTD1_Study__c;
    }

    protected override Iterable<Object> getUserBatchIterable() {
        return (Iterable<Object>) Database.getQueryLocator(getUserQuery());
    }

    protected override void doUserQueryPrep() {
        groupUserIdsByProfile(PROFILES_TO_SHARE_WITH);
        if (!profileToUserIds.isEmpty()) {
                  // if we have profiles that can get STM sharing, query to check what study or studies they belong to
            Set<Id> stmUserIds = new Set<Id>();
            for (String profileName : profileToUserIds.keySet()) {
                if (!VT_R5_SiteMemberCalculator.SITE_PROFILES.contains(profileName) && !VT_R5_ShareGroupMembershipService.STUDY_LEVEL_PROFILES.contains(profileName)) {
                    stmUserIds.addAll(profileToUserIds.get(profileName));
                }
            }
            if (! stmUserIds.isEmpty()) {
                getStmUserIdByStudy(stmUserIds);
            }
            studyFromStmIds = studyToStmUserIds.keySet();
        }
    }

    protected override List<SObject> executeUserQuery() {
        String query = getUserQuery() + ' LIMIT ' + getAdjustedQueryLimit();
        return Database.query(query);
    }

    private void addToMemberMap(Map<Id, Set<Id>> memberMap, Id parentId, User member) {
        if (parentId != null && member != null && member.IsActive) {
            if (!memberMap.containsKey(parentId)) {
                memberMap.put(parentId, new Set<Id>());
            }
            memberMap.get(parentId).add(member.Id);
        }
    }

    private Map<Id, Set<Id>> getStudyUsers(Set<Id> studyIds) {
        Map<Id, Set<Id>> studyUsers = new Map<Id, Set<Id>>();
        for (HealthCloudGA__CarePlanTemplate__c study : [
            SELECT Id,
                VTD1_Project_Lead__r.Id,
                VTD1_Project_Lead__r.IsActive,
                VTD1_Virtual_Trial_Study_Lead__r.Id,
                VTD1_Virtual_Trial_Study_Lead__r.IsActive,
                VTD1_Regulatory_Specialist__r.Id,
                VTD1_Regulatory_Specialist__r.IsActive,
                VTD1_Virtual_Trial_head_of_Operations__r.Id,
                VTD1_Virtual_Trial_head_of_Operations__r.IsActive,
            (
                SELECT User__r.Id, User__r.IsActive
                FROM Study_Team_Members__r
                WHERE User__r.IsActive = TRUE
                AND User__r.Profile.Name NOT IN :VT_R5_SiteMemberCalculator.SITE_PROFILES
                AND User__r.Profile.Name NOT IN :VT_R5_ShareGroupMembershipService.STUDY_LEVEL_PROFILES
            )
            FROM HealthCloudGA__CarePlanTemplate__c
            WHERE Id IN :studyIds
        ]) {
            addToMemberMap(studyUsers, study.Id, study.VTD1_Project_Lead__r);
            addToMemberMap(studyUsers, study.Id, study.VTD1_Virtual_Trial_Study_Lead__r);
            addToMemberMap(studyUsers, study.Id, study.VTD1_Regulatory_Specialist__r);
            addToMemberMap(studyUsers, study.Id, study.VTD1_Virtual_Trial_head_of_Operations__r);
            for (Study_Team_Member__c member : study.Study_Team_Members__r) {
                addToMemberMap(studyUsers, study.Id, member.User__r);
            }
        }
        return studyUsers;
    }

    private String getUserQuery() {
        String query =
            'SELECT Id,' +
                'VTD1_StudyId__c,' +
                'VTD1_Virtual_Site__c,' +
                'VTD1_Virtual_Site__r.VTD1_Study__c,' +
                'VTD1_Subject_ID__r.VTD1_Virtual_Site__c,' +
                'VTD1_Subject_ID__r.VTD1_Virtual_Site__r.VTD1_Study__c' +
            ' FROM VTD1_Protocol_Deviation__c' +
            ' WHERE (VTD1_StudyId__c IN :studyFromStmIds' +
                ' OR VTD1_Virtual_Site__r.VTD1_Study__c IN :studyFromStmIds' +
                ' OR VTD1_Subject_ID__r.VTD1_Virtual_Site__r.VTD1_Study__c IN :studyFromStmIds' +
            ')';
        String scopeCondition = getScopeCondition();
        if (scopeCondition != null) { query += ' AND ' + scopeCondition; }
        System.debug(query);
        return query;
    }

    private String getScopeCondition() {
        String scopeCondition;
        if (scopedStudyIds != null) {
            scopeCondition = '(VTD1_StudyId__c IN :scopedStudyIds OR VTD1_Virtual_Site__r.VTD1_Study__c IN :scopedStudyIds OR VTD1_Subject_ID__r.VTD1_Virtual_Site__r.VTD1_Study__c IN :scopedStudyIds)';
        }
        return scopeCondition;
    }

    protected override String getRemoveQuery() {
        String removeQuery =
            'SELECT Id ' +
                'FROM VTD1_Protocol_Deviation__Share ' +
                'WHERE (' +
                    'Parent.VTD1_StudyId__c IN :scopedStudyIds ' +
                    'OR Parent.VTD1_Virtual_Site__r.VTD1_Study__c IN :scopedStudyIds ' +
                    'OR Parent.VTD1_Subject_ID__r.VTD1_Virtual_Site__r.VTD1_Study__c IN :scopedStudyIds' +
                ') ' +
                'AND UserOrGroupId IN :includedUserIds';
        System.debug(removeQuery);
        return scopedStudyIds != null ? removeQuery : null; // if removing only site scoped users, no need to do anything
    }

    protected override void createUserShareDetailsFromRecords(List<SObject> records) {
        List<VTD1_Protocol_Deviation__c> pds = (List<VTD1_Protocol_Deviation__c>) records;
        for (VTD1_Protocol_Deviation__c pd : pds) {
            if (studyToStmUserIds.containsKey(pd.VTD1_StudyId__c)) {
                addShares(
                    pd.Id,
                    studyToStmUserIds.get(pd.VTD1_StudyId__c),
                    pd.VTD1_StudyId__c
                );
            }
        }
    }

    private void addShares(Id pdId, Set<Id> userIds, Id studyId) {
        for (Id userId : userIds) {
            addShareDetail(pdId, userId, studyId, 'Edit');
        }
    }
}