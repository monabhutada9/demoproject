/**
 * This Unit Test class is for Process Triggers and Handlers for ContentDocument, ContentDocumentLink and ContentVersion objects.
 */

@IsTest
private class VT_R5_ContentProcessHandlerTest {
    /*@TestSetup
    private static void testSetup() {
        DomainObjects.Study_t study = new DomainObjects.Study_t();
        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t();
        DomainObjects.User_t userPatient = new DomainObjects.User_t()
            .addContact(patientContact)
            .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        userPatient.persist();
        DomainObjects.VTD1_Patient_t patient = new DomainObjects.VTD1_Patient_t();
        patient.persist();
        DomainObjects.Case_t caseCaregiver = new DomainObjects.Case_t()
            .setRecordTypeByName('CarePlan')
            .addStudy(study)
            .addUser(userPatient)
            .addVTD1_Patient(patient)
            .addContact(patientContact);
        caseCaregiver.persist();
    }

    @IsTest
    private static void cdlInsertTest() {
        Case cas = [SELECT Id, VTD1_Study__c FROM Case LIMIT 1] ;

        VTD1_Regulatory_Document__c regDoc = new VTD1_Regulatory_Document__c();
        regDoc.VTD1_Document_Type__c = 'Eligibility Assesment Form With TMA';
        regDoc.VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI';
        insert regDoc;

        VTD1_Regulatory_Binder__c regBinder = new VTD1_Regulatory_Binder__c();
        regBinder.VTD1_Regulatory_Document__c = regDoc.Id;
        regBinder.VTD1_Care_Plan_Template__c = cas.VTD1_Study__c;
        insert regBinder;

        VTD1_Document__c doc = new VTD1_Document__c();
        doc.VTD1_Study__c = cas.VTD1_Study__c;
        doc.VTD1_Clinical_Study_Membership__c = cas.Id;
        doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE;
        doc.VTD1_Regulatory_Binder__c = regBinder.Id;
        insert doc;

        createFile('test', doc.Id, 'I', 'AllUsers');
    }

    @IsTest
    private static void cdlUpdateTest() {
        Case cas = [SELECT Id, VTD1_Study__c FROM Case LIMIT 1] ;

        VTD1_Regulatory_Document__c regDoc = new VTD1_Regulatory_Document__c();
        regDoc.VTD1_Document_Type__c = 'Eligibility Assesment Form With TMA';
        regDoc.VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI';
        insert regDoc;

        VTD1_Regulatory_Binder__c regBinder = new VTD1_Regulatory_Binder__c();
        regBinder.VTD1_Regulatory_Document__c = regDoc.Id;
        regBinder.VTD1_Care_Plan_Template__c = cas.VTD1_Study__c;
        insert regBinder;

        VTD1_Document__c doc = new VTD1_Document__c();
        doc.VTD1_Study__c = cas.VTD1_Study__c;
        doc.VTD1_Clinical_Study_Membership__c = cas.Id;
        doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE;
        doc.VTD1_Regulatory_Binder__c = regBinder.Id;
        insert doc;

        ContentVersion cv = createFile('test', doc.Id, 'I', 'AllUsers');

        List<ContentDocumentLink> cdlList = [
            SELECT Visibility
            FROM ContentDocumentLink
            WHERE ContentDocumentId = :cv.ContentDocumentId
        ];
        update cdlList;
    }

    @IsTest
    private static void cdlDeleteTest() {
        Case cas = [SELECT Id FROM Case LIMIT 1];

        ContentVersion cv = createFile('test file', cas.Id, 'I', 'AllUsers');
        List<ContentDocumentLink> cdlList = [
            SELECT Visibility
            FROM ContentDocumentLink
            WHERE ContentDocumentId = :cv.ContentDocumentId
        ];

        System.assertNotEquals(null, cdlList.get(0));

        delete cdlList.get(0);

        List<ContentDocumentLink> cdlListInDB = [
            SELECT Id
            FROM ContentDocumentLink
            WHERE Id = :cdlList.get(0).Id
        ];

        System.assertEquals(0, cdlListInDB.size());
    }

    @IsTest
    private static void shareVirtualSiteFilesToCustomersTest() {
        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c();
        insert study;
        Virtual_Site__c virtualSite = new Virtual_Site__c();
        virtualSite.VTD1_Study__c = study.Id;
        virtualSite.VTD1_Study_Site_Number__c = '12345';
        insert virtualSite;

        ContentVersion cvOfSite = createFile('Virtual Site\'s file', virtualSite.Id, 'V', 'InternalUsers');
        ContentDocumentLink cldForAssert;
        for (ContentDocumentLink cdl : [
            SELECT Visibility
            FROM ContentDocumentLink
            WHERE ContentDocumentId = :cvOfSite.ContentDocumentId
        ]) {
            if (cdl.Visibility == 'AllUsers') {
                cldForAssert = cdl;
            }
        }
        System.assertEquals('AllUsers', cldForAssert.Visibility);
    }

    private static ContentVersion createFile(String title, Id parentId, String shareType, String visibility) {
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.PathOnClient = 'test.txt';
        contentVersion.Title = title;
        contentVersion.VersionData = Blob.valueOf('Test Data');
        insert contentVersion;

        ContentVersion contentVersionCreated = [
            SELECT ContentDocumentId
            FROM ContentVersion
            WHERE Id = :contentVersion.Id
        ];

        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = parentId;
        contentDocumentLink.ContentDocumentId = contentVersionCreated.ContentDocumentId;
        contentDocumentLink.ShareType = shareType;
        contentDocumentLink.Visibility = visibility;
        insert contentDocumentLink;

        return contentVersionCreated;
    }*/
}