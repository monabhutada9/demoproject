/*   * Author: Shailesh B
     * Class : VTR5_CreateStudySkillQueueable
     * param : Set<Id> studySet
     * Description : This class is used to create chat nutton and skill by calling method
                    createStudySkillAndChatButtons and iterates itself with next batch records
     * return :  void
*/

public class VTR5_CreateStudySkillQueueable implements Queueable, Database.AllowsCallouts{ 
    Set<Id> lstMaster = new Set<Id>();
    public VTR5_CreateStudySkillQueueable(Set<Id> studySet ) {
        this.lstMaster= studySet;   
    }
     public void execute(QueueableContext context){
        List<HealthCloudGA__CarePlanTemplate__c> lstToBeUpdated = new List<HealthCloudGA__CarePlanTemplate__c>();

         VTR5_SkillAndChatButtonController.ResponseJSON  result;
         Id studyId;
         Set<Id> lstCurrent = new Set<Id>();
         System.debug('mastermap list before'+lstMaster);


         for(Id key : lstMaster){
            studyId = key;
            break;
         }
         lstCurrent.add(studyId);

        if(!lstMaster.isEmpty()){
            lstMaster.removeAll(lstCurrent);
        }

        System.debug('currentMap  after'+lstCurrent);
        System.debug('currentMap  size after '+lstCurrent.size());
         System.debug('masterMap list after'+lstMaster);
         System.debug('masterMap size after'+lstMaster.size());

       if(studyId != null) {
            try {
                result= VTR5_SkillAndChatButtonController.createStudySkillAndChatButtons(studyId);
                HealthCloudGA__CarePlanTemplate__c temp= new HealthCloudGA__CarePlanTemplate__c(Id=studyId);
                 Boolean isUpdateSuccess = VTR5_SkillAndChatButtonController.updateStudies(temp, result);
                System.debug('JSon Response '+result);
                 System.debug('Update result  '+isUpdateSuccess);
            } catch (Exception e) {
                System.debug('Error in operation '+ e.getMessage() );
                ErrorLogUtility.logException(e, ErrorLogUtility.ApplicationArea.APP_AREA_CHAT_AUTOMATION,
                         VTR5_CreateStudySkillQueueable.class.getName());
            }
        }

        if(!lstMaster.isEmpty()){
             System.debug('Next stuydID count '+lstMaster.size());
                VTR5_CreateStudySkillQueueable asyncCaller = new VTR5_CreateStudySkillQueueable(lstMaster);
                System.enqueueJob(asyncCaller);

        }
     }

}