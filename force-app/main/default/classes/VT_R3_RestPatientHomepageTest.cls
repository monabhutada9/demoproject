/**
 * Created by Alexander Komarov on 15.10.2019.
 */

@IsTest
public class VT_R3_RestPatientHomepageTest {
    private static void extendSetup(User u, Boolean fullHome) {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        insert new HealthCloudGA__CarePlanTemplate__Share(
                UserOrGroupId = u.Id,
                ParentId = study.Id,
                AccessLevel = 'Edit'
        );
        Case caseObj = [SELECT Id, Contact.Id, Contact.VTD1_FullHomePage__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];

        insert new Task(VTD2_My_Task_List_Redirect__c = '&banner=some-value', VTD1_Case_lookup__c = caseObj.Id, ActivityDate = System.now().addDays(-5).date(), OwnerId = u.Id, Subject = 'some subject', VTD1_Caregiver_Clickable__c = false, VTD2_isRequestVisitRedirect__c = false, Status = 'Open', WhatId = caseObj.Id, Category__c = 'Signature Required');
        insert new Task(VTD2_My_Task_List_Redirect__c = 'full-profile', VTD1_Case_lookup__c = caseObj.Id, ActivityDate = System.now().addDays(-5).date(), OwnerId = u.Id, Subject = 'some subject', VTD1_Caregiver_Clickable__c = false, VTD2_isRequestVisitRedirect__c = false, Status = 'Completed', WhatId = caseObj.Id, Category__c = 'Signature Required');
        insert new Task(VTR5_EndDate__c =System.now().addDays(5).date(), VTD1_Case_lookup__c = caseObj.Id, OwnerId = u.Id, Subject = 'some subject', VTD1_Caregiver_Clickable__c = false, VTD2_isRequestVisitRedirect__c = false, Status = 'Completed', WhatId = caseObj.Id, Category__c = 'Signature Required');

        //copado
        if (fullHome) {
            caseObj.Contact.VTD1_FullHomePage__c = true;
            update caseObj.Contact;
        }
    }
    public static void firstTest() {
        User user = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.PT_USERNAME LIMIT 1];
        Test.startTest();
        extendSetup(user, false);
        System.runAs(user) {
            //copado
            VT_R3_RestPatientHomepage.getHomepageInfo();
        }
        Test.stopTest();
    }
    public static void secondTest() {
        User user = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        Test.startTest();
        extendSetup(user, true);
        System.runAs(user) {
            //copado
            VT_R3_RestPatientHomepage.getHomepageInfo();
        }
        Test.stopTest();
    }
    public static void thirdTest() {
        User user = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.CG_USERNAME LIMIT 1];
        Test.startTest();
        extendSetup(user, true);
        System.runAs(user) {
            //copado
            VT_R3_RestPatientHomepage.getEmptyTasklistSection();
            VT_R3_RestPatientHomepage.getHomepageInfo();
        }
        Test.stopTest();
    }
}