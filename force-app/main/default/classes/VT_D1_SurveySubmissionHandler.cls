public without sharing class VT_D1_SurveySubmissionHandler {

    List<VTD1_Survey__c> submittedSurveys = new List<VTD1_Survey__c>();
    Map<Id, List<VTD1_Survey_Answer__c>> answerMap = new Map<Id, List<VTD1_Survey_Answer__c>>();

    public void handleSubmissions(List<VTD1_Survey__c> submittedSurveys) {
        this.submittedSurveys = submittedSurveys;
        if (! this.submittedSurveys.isEmpty()) {
            getAnswerMap();
            setSafetyConcernFlag();
            setTimeStamps();
            closeOpenPatientTasks();
            updateVisitOccurredField();
        }
    }

    private void getAnswerMap() {
        for (VTD1_Survey_Answer__c item : [
            SELECT VTD1_Survey__c, VTD1_Outside_Threshold__c, VTD1_Answer__c,
                VTD1_Question__r.VTD1_Default_PSC_Question__c, VTR2_English_Answer__c
            FROM VTD1_Survey_Answer__c
            WHERE VTD1_Survey__c IN :this.submittedSurveys
            ORDER BY VTD1_Outside_Threshold__c DESC
        ]) {
            if (! this.answerMap.containsKey(item.VTD1_Survey__c)) {
                this.answerMap.put(item.VTD1_Survey__c, new List<VTD1_Survey_Answer__c>());
            }
            this.answerMap.get(item.VTD1_Survey__c).add(item);
        }
    }

    private void setSafetyConcernFlag() {
        for (VTD1_Survey__c item : this.submittedSurveys) {
            if (! item.VTD1_Possible_ePRO_Safety_Concern__c) {
                checkForOutsideThreshold(item);
            }

            if (! item.VTD1_Possible_ePRO_Safety_Concern__c) { // check this condition a second time incase safety was flagged from threshold
                checkForKeywords(item);
            }
        }
    }

    private void checkForOutsideThreshold(VTD1_Survey__c survey) {
        survey.VTD1_Possible_ePRO_Safety_Concern__c =
            this.answerMap.containsKey(survey.Id) && this.answerMap.get(survey.Id)[0].VTD1_Outside_Threshold__c;
    }

    private void checkForKeywords(VTD1_Survey__c survey) {
        if (this.answerMap.containsKey(survey.Id) && survey.VTD1_Safety_Keywords__c != null) {
            List<String> keyWords = survey.VTD1_Safety_Keywords__c.replaceAll(',', ';').normalizeSpace().split(';');
            for (VTD1_Survey_Answer__c answer : this.answerMap.get(survey.Id)) {
                if (survey.VTD1_Possible_ePRO_Safety_Concern__c) { break; }
                for (String keyWord : keyWords) {
                    if (answer.VTR2_English_Answer__c != null && answer.VTR2_English_Answer__c.containsIgnoreCase(keyWord)) {
                        survey.VTD1_Possible_ePRO_Safety_Concern__c = true;
                        break;
                    }
                }
            }
        }
    }

    private void setTimeStamps() {
        Set<Id> scoringRequiredIds = new Set<Id>();
        for (VTD1_Survey_Answer__c item : [
            SELECT VTD1_Survey__c
            FROM VTD1_Survey_Answer__c
            WHERE VTD1_Survey__c IN :this.submittedSurveys
            AND VTD1_Question__r.VTR2_Scoring_Type__c = 'Manual'
            AND VTR2_Is_Answered__c = true
        ]) {
            scoringRequiredIds.add(item.VTD1_Survey__c);
        }

        for (VTD1_Survey__c item : this.submittedSurveys) {
            if (item.VDD1_Scoring_Window__c != null && item.VDD1_Scoring_Window__c > 0) {
                String fieldName = scoringRequiredIds.contains(item.Id) ? 'VTD1_Scoring_Due_Date__c' : 'VTR2_Review_Due_Date__c';
                item.put(fieldName, System.now().addMinutes(Math.round(item.VDD1_Scoring_Window__c * 60)));
            }
        }
    }

    private void closeOpenPatientTasks() {
        List<Task> closedTasks = new List<Task>();

        for (Task item : [
            SELECT Id
            FROM Task
            WHERE WhatId IN :this.submittedSurveys
            AND Subject = :VT_R4_ConstantsHelper_Tasks.SURVEY_PATIENT_TASK_SUBJECT
        ]) {
            closedTasks.add(new Task(Id = item.Id, Status = VT_R4_ConstantsHelper_Tasks.TASK_CLOSED));
        }
        if (! closedTasks.isEmpty()) { update closedTasks; }
    }

    private void updateVisitOccurredField () {
        Id satisfactionSurveyRecTypeId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('SatisfactionSurvey').getRecordTypeId();
        List<VTD1_Actual_Visit__c> visitsToUpdate = new List<VTD1_Actual_Visit__c>();

        for (VTD1_Survey__c item : this.submittedSurveys) {
            if (item.RecordTypeId == satisfactionSurveyRecTypeId && this.answerMap.containsKey(item.Id)) {
                for (VTD1_Survey_Answer__c answer : this.answerMap.get(item.Id)) {
                    if (answer.VTD1_Question__r.VTD1_Default_PSC_Question__c) {
                        VTD1_Actual_Visit__c visit = new VTD1_Actual_Visit__c(
                            Id = item.VTD1_Actual_Visit__c,
                            VTD1_Visit_Occurred__c = answer.VTD1_Answer__c
                        );
                        if (answer.VTR2_English_Answer__c != null && answer.VTR2_English_Answer__c.equalsIgnoreCase('Yes')) {
                            visit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED;
                        }
                        visitsToUpdate.add(visit);
                    }
                }
            }
        }

        if (! visitsToUpdate.isEmpty()) { update visitsToUpdate; }
    }
}