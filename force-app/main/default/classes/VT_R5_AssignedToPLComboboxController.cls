public with sharing class VT_R5_AssignedToPLComboboxController {
    private static List<Map<String, String>> usersList;
    @AuraEnabled
    public static List<Map<String, String>> getTaskOwnerUsersList(Id recordId) {
        User u = [SELECT Id, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
        String profileNameAbbr = getProfileNameAbbr(u.Profile.Name);
        Task task = [SELECT HealthCloudGA__CarePlanTemplate__c FROM Task WHERE Id = :recordId];
        HealthCloudGA__CarePlanTemplate__c study = [
                SELECT VTD1_Project_Lead__r.Name
                FROM HealthCloudGA__CarePlanTemplate__c
                WHERE Id = :task.HealthCloudGA__CarePlanTemplate__c
        ];
        if (profileNameAbbr == 'PL' && u.Id != study.VTD1_Project_Lead__c) {
            usersList = new List<Map<String, String>>{
                    new Map<String, String>{
                            'value' => study.VTD1_Project_Lead__r.Id,
                            'label' => study.VTD1_Project_Lead__r.Name
                    }
            };
        } else {
            setUsersListFromSTM(task.HealthCloudGa__CarePlanTemplate__c, profileNameAbbr);
        }
        return usersList;
    }
    @TestVisible
    private static void setUsersListFromSTM(Id studyId, String profileNameAbbr) {
        usersList = new List<Map<String, String>>();
        List<Study_Team_Member__c> stdMembers = [
                SELECT User__r.Name
                FROM Study_Team_Member__c
                WHERE Study__c = :studyId AND RecordType.DeveloperName = :profileNameAbbr
        ];
        for (Study_Team_Member__c studyTeamMember : stdMembers) {
            usersList.add(
                    new Map<String, String>{
                            'value' => studyTeamMember.User__r.Id,
                            'label' => studyTeamMember.User__r.Name
                    }
            );
        }
    }
    private static String getProfileNameAbbr(String profName) {
        String profNameAbbr = '';
        for (String s : profName.split(' ')) {
            profNameAbbr += s.substring(0, 1);
        }
        return profNameAbbr;
    }
}