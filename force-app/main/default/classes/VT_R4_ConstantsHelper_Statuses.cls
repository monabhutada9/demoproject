/**
 * THIS CLASS IS DEPRECATED! DON'T USE IT!
 *
 * Use the following instead:
 * @see VT_R4_ConstantsHelper_AccountContactCase
 * @see VT_R4_ConstantsHelper_Documents
 * @see VT_R4_ConstantsHelper_Integrations
 * @see VT_R4_ConstantsHelper_KitsOrders
 * @see VT_R4_ConstantsHelper_ProfilesSTM
 * @see VT_R4_ConstantsHelper_Protocol_ePRO
 * @see VT_R4_ConstantsHelper_Tasks
 * @see VT_R4_ConstantsHelper_VisitsEvents
 */
public with sharing class VT_R4_ConstantsHelper_Statuses {
    public static final String ACTUAL_VISIT_STATUS_SCHEDULED = 'Scheduled';
    public static final String ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED = 'To Be Scheduled';
    public static final String ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED = 'To Be Rescheduled';
    public static final String ACTUAL_VISIT_STATUS_REQUESTED = 'Requested';
    public static final String ACTUAL_VISIT_STATUS_COMPLETED = 'Completed';
    public static final String ACTUAL_VISIT_STATUS_CANCELLED = 'Cancelled';
    public static final String ACTUAL_VISIT_STATUS_MISSED = 'Missed';
    public static final String ACTUAL_VISIT_STATUS_FUTURE_VISIT = 'Future Visit';
    public static final String SURVEY_NOT_STARTED = 'Not Started';
    public static final String SURVEY_IN_PROGRESS = 'In Progress';
    public static final String SURVEY_REVIEW_REQUIRED = 'Review Required';
    public static final String SURVEY_REVIEWED = 'Reviewed';
    public static final String SURVEY_REVIEWED_PARTIALLY_COMPLETED = 'Reviewed (Partially Completed)';
    public static final String TASK_CLOSED = 'Closed';
    public static final String CASE_PRE_CONSENT = 'Pre-Consent';
    public static final String CASE_WASHOUT_RUN_IN = 'Washout/Run-In';
    public static final String CASE_WASHOUT_RUN_IN_FAILURE = 'Washout/Run-In Failure';
    public static final String CASE_ACTIVE_RANDOMIZED = 'Active/Randomized';
    public static final String CASE_RANDOMIZED = 'Randomized';
    public static final String CASE_SCREENED = 'Screened';
    public static final String CASE_CONSENTED = 'Consented';
    public static final String CASE_SCREEN_FAILURE = 'Screen Failure';
    public static final String CASE_LOST_TO_FOLLOW_UP = 'Lost to Follow-Up';    // Obsolete in R1.1?
    public static final String CASE_DROPPED = 'Dropped';
    public static final String CASE_COMPLETED = 'Completed';
    public static final String CASE_CLOSED = 'Closed';
    public static final String CASE_DOES_NOT_QUALIFY = 'Does Not Qualify';
    public static final String CASE_DROPPED_IN_FOLLOW_UP = 'Dropped Treatment,In F/Up';
    public static final String CASE_DROPPED_COMPL_FOLLOW_UP = 'Dropped Treatment,Compl F/Up';
    public static final String CASE_COMPL_IN_FOLLOW_UP = 'Compl Treatment,In F/Up';
    public static final String CASE_COMPL_DROPPED_FOLLOW_UP = 'Compl Treatment,Dropped F/Up';
    public static final String CASE_OPEN = 'OPEN';
    public static final String PROTOCOL_DELIVERY_NOT_STARTED = 'Not Started';
    public static final String TIMESLOT_PROPOSED = 'Proposed';
    public static final String TIMESLOT_CONFIRMED = 'Confirmed';
    public static final String TIMESLOT_REJECTED = 'Rejected';
    public static final String DOCUMENT_APPROVED = 'Approved';
    public static final String DOCUMENT_REJECTED = 'Rejected';
    public static final String ASSIGNMENT_HISTORY_CURRENT = 'Current';
    public static final String ASSIGNMENT_HISTORY_TRANSFERRED = 'Transferred';
    public static final String CANDIDATE_PATIENT_CONVERSION_CONVERTED = 'Converted';

    public static final Set<String> SEND_SUBJECT_CASE_STATUSES = new Set<String>{   // For Cenduit / CSM only
            CASE_CONSENTED,
            //CASE_DOES_NOT_QUALIFY,
            CASE_SCREENED,
            CASE_SCREEN_FAILURE,
            CASE_WASHOUT_RUN_IN,
            CASE_WASHOUT_RUN_IN_FAILURE,
            CASE_ACTIVE_RANDOMIZED,
            CASE_DROPPED,
            CASE_DROPPED_IN_FOLLOW_UP,
            CASE_DROPPED_COMPL_FOLLOW_UP,
            CASE_COMPLETED,
            CASE_COMPL_IN_FOLLOW_UP,
            CASE_COMPL_DROPPED_FOLLOW_UP
    };

    public static final Set<String> SEND_SUBJECT_CASE_STATUSES_RH = new Set<String>{    // For RR only
            CASE_PRE_CONSENT,
            CASE_CONSENTED,
            CASE_DOES_NOT_QUALIFY,
            CASE_SCREENED,
            CASE_SCREEN_FAILURE,
            CASE_WASHOUT_RUN_IN,
            CASE_WASHOUT_RUN_IN_FAILURE,
            CASE_ACTIVE_RANDOMIZED,
            CASE_DROPPED//,
            //CASE_DROPPED_IN_FOLLOW_UP,
            //CASE_DROPPED_COMPL_FOLLOW_UP,
            //CASE_COMPLETED,
            //CASE_COMPL_IN_FOLLOW_UP,
            //CASE_COMPL_DROPPED_FOLLOW_UP
    };

}