/**
 * @author: Alexander Komarov
 * @date: 12.05.2020
 * @description:
 */

public without sharing class VT_R4_BatchVideoConferencePush implements Database.Batchable<SObject>, Database.AllowsCallouts {

    private List<Id> videoConferences;
    public VT_R4_BatchVideoConferencePush(List<Id> ids) {
        this.videoConferences = ids;
    }

    public void execute(Database.BatchableContext param1, List<Video_Conference__c> conferences) {
        VT_R3_InvocablePush.sendPushNotificationsSync(conferences);
    }

    public Database.QueryLocator start(Database.BatchableContext param1) {
        return Database.getQueryLocator([
                SELECT
                        Name,
                        VTD1_Actual_Visit__c,
                        Scheduled_For__c,
                        VTD1_Scheduled_Visit_End__c
                FROM Video_Conference__c
                WHERE Id IN :videoConferences
        ]);
    }

    public void finish(Database.BatchableContext param1) {
    }
}