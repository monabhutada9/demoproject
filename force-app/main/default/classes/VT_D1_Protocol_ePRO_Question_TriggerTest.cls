/**
 * Created by User on 19/05/14.
 * updated by Galiya Khalikova on 29/01/2020
 */
@isTest
private with sharing class VT_D1_Protocol_ePRO_Question_TriggerTest {

    @TestSetup
    static void dataSetup() {

        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Id eProRecTypeId = Schema.SObjectType.VTD1_Protocol_ePRO__c.getRecordTypeInfosByDeveloperName().get('ePRO').getRecordTypeId();

        List<VTD1_Protocol_ePRO__c> pEdiaries = new List<VTD1_Protocol_ePRO__c>();
        pEdiaries.add(new VTD1_Protocol_ePRO__c(
                Name = 'EdiaryWithoutBranhing',
                RecordTypeId = eProRecTypeId,
                VTD1_Type__c = 'eDiary',
                VTD1_Subject__c = 'Patient',
                VTD1_Study__c = study.Id,
                VTD1_Response_Window__c = 1,
                VTR2_Protocol_Reviewer__c = 'Patient Guide'
        ));
        pEdiaries.add(new VTD1_Protocol_ePRO__c(
                Name = 'EdiaryWithBranhing',
                RecordTypeId = eProRecTypeId,
                VTD1_Type__c = 'eDiary',
                VTD1_Subject__c = 'Patient',
                VTD1_Study__c = study.Id,
                VTD1_Response_Window__c = 1,
                VTR2_Protocol_Reviewer__c = 'Patient Guide',
                VTR4_Is_Branching_eDiary__c = true
        ));
        insert pEdiaries;

        List<VTD1_Protocol_ePro_Question__c> questions = new List<VTD1_Protocol_ePro_Question__c>();
        questions.add(new VTD1_Protocol_ePro_Question__c(
                VTD1_Protocol_ePRO__c = pEdiaries[0].Id,
                VTD1_Type__c = 'Drop Down',
                VTR2_Scoring_Type__c = 'Not Scored'
        ));
        questions.add(new VTD1_Protocol_ePro_Question__c(
                VTD1_Protocol_ePRO__c = pEdiaries[1].Id,
                VTD1_Type__c = 'Drop Down',
                VTD1_Question_Content__c = '1',
                VTD1_Options__c = 'yes;no',
                VTR2_Scoring_Type__c = 'Not Scored',
                VTR4_Contains_Branching_Logic__c = false
        ));
        questions.add(new VTD1_Protocol_ePro_Question__c(
                VTD1_Protocol_ePRO__c = pEdiaries[1].Id,
                VTD1_Type__c = 'Drop Down',
                VTD1_Question_Content__c = '2',
                VTD1_Options__c = 'yes;no',
                VTR2_Scoring_Type__c = 'Not Scored',
                VTR4_Contains_Branching_Logic__c = false
        ));
        insert questions;
        Test.stopTest();

    }

    @isTest
    private static void insertChildQuestionToEdiaryWithoutBranching() {

        VTD1_Protocol_ePRO__c protocolEdiary = [SELECT Id FROM VTD1_Protocol_ePRO__c WHERE Name = 'EdiaryWithoutBranhing' LIMIT 1];
        VTD1_Protocol_ePro_Question__c protocolEProQuestions = [SELECT Id FROM VTD1_Protocol_ePro_Question__c LIMIT 1];
        VTD1_Protocol_ePro_Question__c protocolChildQuestion = new VTD1_Protocol_ePro_Question__c(
                VTD1_Protocol_ePRO__c = protocolEdiary.Id,
                VTR4_Branch_Parent__c = protocolEProQuestions.Id
        );

        String dmlMessage;
        Test.startTest();

        try {
            insert protocolChildQuestion;
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }

        String expectedMessage = 'Parent Question is only applicable when the eDiary contains branching logic.';
        System.assertEquals(expectedMessage, dmlMessage);
        Test.stopTest();
    }

    @isTest
    private static void CheckEdiaryWithBranching() {

        VTD1_Protocol_ePRO__c protocolEdiary = [SELECT Id FROM VTD1_Protocol_ePRO__c WHERE Name = 'EdiaryWithBranhing' LIMIT 1];
        VTD1_Protocol_ePro_Question__c parentQuestionFirst = [SELECT VTR4_Contains_Branching_Logic__c FROM VTD1_Protocol_ePro_Question__c WHERE VTD1_Question_Content__c = '1' LIMIT 1];
        VTD1_Protocol_ePro_Question__c parentQuestionSecond = [SELECT Id, VTR4_Contains_Branching_Logic__c FROM VTD1_Protocol_ePro_Question__c WHERE VTD1_Question_Content__c = '2' LIMIT 1];

        VTD1_Protocol_ePro_Question__c childQuestion = new VTD1_Protocol_ePro_Question__c(
                VTD1_Protocol_ePRO__c = protocolEdiary.Id,
                VTR4_Branch_Parent__c = parentQuestionFirst.Id,
                VTR4_Response_Trigger__c = 'yes',
                VTR4_Contains_Branching_Logic__c = false
        );
        insert childQuestion;
        System.assertEquals(true, [SELECT VTR4_Contains_Branching_Logic__c FROM VTD1_Protocol_ePro_Question__c WHERE VTD1_Question_Content__c = '1' LIMIT 1].VTR4_Contains_Branching_Logic__c);

        childQuestion.VTR4_Branch_Parent__c = parentQuestionSecond.Id;
        update childQuestion;

        System.assertEquals(false,  [SELECT VTR4_Contains_Branching_Logic__c FROM VTD1_Protocol_ePro_Question__c WHERE VTD1_Question_Content__c = '1' LIMIT 1].VTR4_Contains_Branching_Logic__c);
        System.assertEquals(true,  [SELECT VTR4_Contains_Branching_Logic__c FROM VTD1_Protocol_ePro_Question__c WHERE VTD1_Question_Content__c = '2' LIMIT 1].VTR4_Contains_Branching_Logic__c);

        delete childQuestion;
        System.assertEquals(false, [SELECT VTR4_Contains_Branching_Logic__c FROM VTD1_Protocol_ePro_Question__c WHERE VTD1_Question_Content__c = '2' LIMIT 1].VTR4_Contains_Branching_Logic__c);
    }
}