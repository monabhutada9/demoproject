/**
* @author: Carl Judge
* @date: 13-Oct-20
* @description: Add and remove members from share groups on study and site
**/

public without sharing class VT_R5_ShareGroupMembershipService {
    public static final String READ_ACCESS = 'Read';
    public static final String EDIT_ACCESS = 'Edit';
    public static final String NO_ACCESS = 'None';
    public static List<String> RETRY_ERRORS = new List<String>{
        'unable to obtain exclusive access to this record',
        'group membership operation already in progress',
        'Too many retries of batch save in the presence of Apex triggers with failures'
    };
    public static final Set<String> STUDY_LEVEL_PROFILES = new Set<String> {
        VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.CM_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.RE_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.AUDITOR_PROFILE_NAME // Added to enable sharing for Auditor Profile, Epic# SH-8126
    };
    public static Boolean disableMembership = false;

    public static void linkSiteGroupsToStudy(List<Virtual_Site__c> sites) {
        if (disableMembership) { return; }
        List<GroupMember> members = new List<GroupMember>();
        for (Virtual_Site__c site : [
            SELECT VTR5_ReadShareGroupId__c, VTR5_EditShareGroupId__c,
                VTD1_Study__r.VTR5_ReadShareGroupId__c, VTD1_Study__r.VTR5_EditShareGroupId__c
            FROM Virtual_Site__c
            WHERE Id IN :sites
        ]) {
            members.add(new GroupMember(
                UserOrGroupId = site.VTD1_Study__r.VTR5_ReadShareGroupId__c,
                GroupId = site.VTR5_ReadShareGroupId__c
            ));
            members.add(new GroupMember(
                UserOrGroupId = site.VTD1_Study__r.VTR5_EditShareGroupId__c,
                GroupId = site.VTR5_EditShareGroupId__c
            ));
        }
        insertMembers(members);
    }

    public static void addSiteMembers(List<VT_R5_SiteMemberCalculator.SiteUser> siteUsers) {
        if (disableMembership) { return; }
        new SiteUserCollection(siteUsers).createGroupMembers();
    }

    public static void removeSiteMembers(List<VT_R5_SiteMemberCalculator.SiteUser> siteUsers) {
        if (disableMembership) { return; }
        new SiteUserCollection(siteUsers).removeGroupMembers();
    }

    public static void processStudyConfigs(List<VTD1_Study_Sharing_Configuration__c> configs, Map<Id, VTD1_Study_Sharing_Configuration__c> oldMap) {
        if (disableMembership) { return; }
        StudyConfigCollection studyLevelRemoves = new StudyConfigCollection();
        StudyConfigCollection siteLevelRemoves = new StudyConfigCollection();
        StudyConfigCollection studyLevelAdds = new StudyConfigCollection();
        StudyConfigCollection siteLevelAdds = new StudyConfigCollection();

        for (VTD1_Study_Sharing_Configuration__c config : [
            SELECT Id, VTD1_Study__c, VTD1_User__c, VTD1_Access_Level__c, VTD1_User__r.Profile.Name,
                VTD1_Study__r.VTR5_ReadShareGroupId__c, VTD1_Study__r.VTR5_EditShareGroupId__c
            FROM VTD1_Study_Sharing_Configuration__c
            WHERE Id IN :configs
            AND VTD1_Study__r.VTR5_ReadShareGroupId__c != NULL
            AND VTD1_Study__r.VTR5_EditShareGroupId__c != NULL
            AND (VTD1_User__r.Profile.Name IN :STUDY_LEVEL_PROFILES OR VTD1_User__r.Profile.Name IN :VT_R5_SiteMemberCalculator.SITE_PROFILES)
        ]) {
            Boolean isStudyLevel = STUDY_LEVEL_PROFILES.contains(config.VTD1_User__r.Profile.Name);
            StudyConfigCollection addCollection = isStudyLevel ? studyLevelAdds : siteLevelAdds;
            StudyConfigCollection removeCollection = isStudyLevel ? studyLevelRemoves : siteLevelRemoves;
            if (config.VTD1_Access_Level__c == NO_ACCESS || accessWasReadEdit(config, oldMap)) {
                removeCollection.addConfig(config);
            }
            if (config.VTD1_Access_Level__c != NO_ACCESS) {
                addCollection.addConfig(config);
            }
        }

        if (!studyLevelRemoves.isEmpty()) studyLevelRemoves.removeGroupMembers();
        if (!siteLevelRemoves.isEmpty()) siteLevelRemoves.getSiteUserCollection().removeGroupMembers();
        if (!studyLevelAdds.isEmpty()) studyLevelAdds.createGroupMembers();
        if (!siteLevelAdds.isEmpty()) siteLevelAdds.getSiteUserCollection().createGroupMembers();
    }

    private static Boolean accessWasReadEdit(VTD1_Study_Sharing_Configuration__c config, Map<Id, VTD1_Study_Sharing_Configuration__c> oldMap) {
        return oldMap != null && oldMap.containsKey(config.Id) && oldMap.get(config.Id).VTD1_Access_Level__c != NO_ACCESS;
    }

    private static void insertMembers(List<GroupMember> members) {
        if (Test.isRunningTest()) {
            System.runAs(new User(Id = UserInfo.getUserId())) {
                insert members;
            }
        } else {
            doMemberDML(members, DMLOperation.INS);
        }
    }

    private static void deleteMembers(List<GroupMember> members) {
        if (Test.isRunningTest()) {
            System.runAs(new User(Id = UserInfo.getUserId())) {
                delete members;
            }
        } else {
            doMemberDML(members, DMLOperation.DEL);
        }
    }

    private static void doMemberDML(List<GroupMember> members, DMLOperation operation) {
        new VT_D1_AsyncDML(members, operation)
            .setAllOrNothing(false)
            .setBatchSize(500)
            .setRetryErrors(RETRY_ERRORS)
            .setSemaphoreName('GroupMemberRetry')
            .setSemaphoreOnlyOnRetry(true)
            .setLoggingEnabled(true)
            .setLogFullRecord(true)
            .enqueue();
    }

    private abstract class ObjectMembershipCollection {
        public abstract void createGroupMembers();

        public void removeGroupMembers() {
            Set<Id> userIds = getUserIds();
            Set<String> compositeKeys = getCompositeKeys();
            List<Id> groupIds = getGroupIds();
            List<GroupMember> membersToDelete = new List<GroupMember>();
            for (GroupMember member : [
                SELECT Id, UserOrGroupId, GroupId, Group.DeveloperName
                FROM GroupMember
                WHERE UserOrGroupId IN :userIds
                AND GroupId IN :groupIds
            ]) {
                String compositeKey = member.Group.DeveloperName.substringAfter('_') + member.UserOrGroupId;
                if (compositeKeys.contains(compositeKey)) {
                    membersToDelete.add(member);
                }
            }
            deleteMembers(membersToDelete);
        }

        protected Id getGroupId(String accessLevel, SObject obj) {
            return accessLevel == READ_ACCESS
                ? (Id) obj.get('VTR5_ReadShareGroupId__c')
                : (Id) obj.get('VTR5_EditShareGroupId__c');
        }

        protected abstract Set<Id> getUserIds();
        protected abstract Set<String> getCompositeKeys();
        protected abstract List<Id> getGroupIds();
    }

    private class StudyConfigCollection extends ObjectMembershipCollection {
        private List<VTD1_Study_Sharing_Configuration__c> configs = new List<VTD1_Study_Sharing_Configuration__c>();
        private Set<Id> userIds = new Set<Id>();
        private Set<Id> studyIds = new Set<Id>();
        private Set<String> compositeKeys = new Set<String>();
        private List<Id> groupIds = new List<Id>();

        public void addConfig(VTD1_Study_Sharing_Configuration__c config) {
            configs.add(config);
            studyIds.add(config.VTD1_Study__c);
            userIds.add(config.VTD1_User__c);
            compositeKeys.add('' + config.VTD1_Study__c + config.VTD1_User__c);
            groupIds.add(config.VTD1_Study__r.VTR5_ReadShareGroupId__c);
            groupIds.add(config.VTD1_Study__r.VTR5_EditShareGroupId__c);
        }

        public override void createGroupMembers() {
            List<GroupMember> members = new List<GroupMember>();
            for (VTD1_Study_Sharing_Configuration__c config : configs) {
                members.add(new GroupMember(
                    UserOrGroupId = config.VTD1_User__c,
                    GroupId = getGroupId(config.VTD1_Access_Level__c, config.VTD1_Study__r)
                ));
            }
            insertMembers(members);
        }

        public SiteUserCollection getSiteUserCollection() {
            VT_R5_SiteMemberCalculator calc = new VT_R5_SiteMemberCalculator();
            calc.initForUsers(userIds, studyIds);
            return new SiteUserCollection(calc.getSiteUsers());
        }

        public Boolean isEmpty() {
            return configs.isEmpty();
        }

        protected override Set<Id> getUserIds() {
            return userIds;
        }

        protected override Set<String> getCompositeKeys() {
            return compositeKeys;
        }

        protected override List<Id> getGroupIds() {
            return groupIds;
        }
    }

    private class SiteUserCollection extends ObjectMembershipCollection {
        private List<VT_R5_SiteMemberCalculator.SiteUser> siteUsers;
        private Set<Id> userIds = new Set<Id>();
        private Set<Id> siteIds = new Set<Id>();
        private Set<String> compositeKeys = new Set<String>();
        private Map<Id, Virtual_Site__c> siteMap;

        public SiteUserCollection(List<VT_R5_SiteMemberCalculator.SiteUser> siteUsers){
            this.siteUsers = siteUsers;
            for (VT_R5_SiteMemberCalculator.SiteUser siteUser : siteUsers) {
                siteIds.add(siteUser.siteId);
                userIds.add(siteUser.userId);
                compositeKeys.add('' + siteUser.siteId + siteUser.userId);
            }
        }

        public override void createGroupMembers() {
            Map<Id, Virtual_Site__c> siteMap = getSiteMap();
            Map<Id, Map<Id, String>> accessLevelMap = getAccessLevels(); // studyId => userId => accessLevel
            List<GroupMember> members = new List<GroupMember>();
            for (VT_R5_SiteMemberCalculator.SiteUser siteUser : siteUsers) {
                if (siteMap.containsKey(siteUser.siteId)) {
                    Virtual_Site__c site = siteMap.get(siteUser.siteId);
                    String accessLevel = accessLevelMap.get(site.VTD1_Study__c)?.get(siteUser.userId);
                    if (accessLevel == READ_ACCESS || accessLevel == EDIT_ACCESS) {
                        members.add(new GroupMember(
                            UserOrGroupId = siteUser.userId,
                            GroupId = getGroupId(accessLevel, site)
                        ));
                    }
                }
            }
            insertMembers(members);
        }

        protected override Set<Id> getUserIds() {
            return userIds;
        }

        protected override Set<String> getCompositeKeys() {
            return compositeKeys;
        }

        protected override List<Id> getGroupIds() {
            List<Id> groupIds = new List<Id>();
            for (Virtual_Site__c site : getSiteMap().values()) {
                groupIds.add(site.VTR5_ReadShareGroupId__c);
                groupIds.add(site.VTR5_EditShareGroupId__c);
            }
            return groupIds;
        }

        private Map<Id, Virtual_Site__c> getSiteMap() {
            if (siteMap == null) {
                siteMap = new Map<Id, Virtual_Site__c>([
                    SELECT Id, VTR5_ReadShareGroupId__c, VTR5_EditShareGroupId__c, VTD1_Study__c
                    FROM Virtual_Site__c
                    WHERE Id IN :siteIds
                ]);
            }
            return siteMap;
        }

        private Map<Id, Map<Id, String>> getAccessLevels() {
            List<Id> studyIds = getStudyIds();
            Map<Id, Map<Id, String>> accessLevelMap = new Map<Id, Map<Id, String>>();
            for (VTD1_Study_Sharing_Configuration__c config : [
                SELECT VTD1_User__c, VTD1_Access_Level__c, VTD1_Study__c
                FROM VTD1_Study_Sharing_Configuration__c
                WHERE VTD1_User__c IN :userIds
                AND VTD1_Study__c IN :studyIds
            ]) {
                if (!accessLevelMap.containsKey(config.VTD1_Study__c)) {
                    accessLevelMap.put(config.VTD1_Study__c, new Map<Id, String>());
                }
                accessLevelMap.get(config.VTD1_Study__c).put(config.VTD1_User__c, config.VTD1_Access_Level__c);
            }
            return accessLevelMap;
        }

        private List<Id> getStudyIds() {
            List<Id> studyIds = new List<Id>();
            for (Virtual_Site__c site : getSiteMap().values()) {
                studyIds.add(site.VTD1_Study__c);
            }
            return studyIds;
        }
    }
}