/** DEPRECATED. MOVED TO VT_R5_CustomAuditHandler_Misc. */

/**
 * Created by user on 21.09.2020.
 * Update By SOnam on 28.10.2020 for User Story SH - 19487
 */

public with sharing class VT_R5_SSTMauditHandler {
    public static void createSSAA(List<Study_Site_Team_Member__c> members) {


        Map<Id, Study_Team_Member__c> associatedSTM = new Map<Id, Study_Team_Member__c>();
        for (Study_Site_Team_Member__c member : members) {
            associatedSTM.put(member.VTR2_Associated_PI3__c, null);
            associatedSTM.put(member.VTD1_Associated_PI__c, null);
            associatedSTM.put(member.VTR2_Associated_PI__c, null);
            associatedSTM.put(member.VTR2_Associated_SCr__c, null);
            associatedSTM.put(member.VTD1_Associated_PG__c, null);
            associatedSTM.put(member.VTR2_Associated_SubI__c, null);
            
            associatedSTM.put(member.VTR5_Associated_CRA__c, null); //Start SH-19487 add the CRA as well for audit Report
        }

        associatedSTM.remove(null);
        associatedSTM.putAll([SELECT Id, Study__c, VTD1_VirtualSite__c, User__r.Id  FROM Study_Team_Member__c WHERE Id IN :associatedSTM.keySet()]);

        // get existing User Audits for users or create new
        Set<Id> userIds = new Set<Id>();
        Set<Id> membersIds = new Set<Id>();
        for (Study_Site_Team_Member__c member : members) {
            Id userId, memberId;
            if(member.VTR2_Associated_SCr__c != null){
                userId = associatedSTM.get(member.VTR2_Associated_SCr__c).User__r.Id;
                memberId = associatedSTM.get(member.VTR2_Associated_SCr__c).Id;
            }else if(member.VTD1_Associated_PG__c != null){
                userId = associatedSTM.get(member.VTD1_Associated_PG__c).User__r.Id;
                memberId = associatedSTM.get(member.VTD1_Associated_PG__c).Id;
            }else if(member.VTR2_Associated_SubI__c != null){
                userId = associatedSTM.get(member.VTR2_Associated_SubI__c).User__r.Id;
                memberId = associatedSTM.get(member.VTR2_Associated_SubI__c).Id;
            } 
            //Start SH-19487 add the CRA as well for audit Report
            else if(member.VTR5_Associated_CRA__c != null){
                userId = associatedSTM.get(member.VTR5_Associated_CRA__c).User__r.Id;
                memberId = associatedSTM.get(member.VTR5_Associated_CRA__c).Id;
            } // End SH-19487


            userIds.add(userId);
            membersIds.add(memberId);
        }
        Map<Id, VTR5_UserAudit__c> userAuditByUser = VT_R5_CustomAuditHandler_Misc.getOrCreateUserAudits(userIds);

        // Get SSTM's with existed SSAA
        List<VTR5_SiteStudyAccessAudit__c> existedSSAA = [
                SELECT Id, User_Audit__c,  VTR5_StudyName__c, VTR5_SiteName__c, VTR5_SiteAccessStatus__c,
                        VTR5_AssignedDateSite__c, VTR5_SiteDeactivatedDate__c, VTR5_StudyTeamMember__c
                        FROM VTR5_SiteStudyAccessAudit__c
                        WHERE User_Audit__c IN :userAuditByUser.values()
                        AND VTR5_SiteDeactivatedDate__c = NULL
        ];

        System.debug('existedSSAA Records ' + existedSSAA);


        Map<Id, VTR5_SiteStudyAccessAudit__c> studyToSSAAmap = new Map<Id, VTR5_SiteStudyAccessAudit__c>();
        for(VTR5_SiteStudyAccessAudit__c ssaa : existedSSAA){
            studyToSSAAmap.put(ssaa.VTR5_StudyName__c , ssaa);
        }



        //to get old date of assign on study
        List<VTR5_SiteStudyAccessAudit__c> allSSAA = [

                SELECT Id, User_Audit__c,  VTR5_StudyName__c, VTR5_SiteName__c, VTR5_SiteAccessStatus__c,
                        VTR5_AssignedDateSite__c, VTR5_SiteDeactivatedDate__c, VTR5_AssignedDateStudy__c, VTR5_StudyTeamMember__c
                FROM VTR5_SiteStudyAccessAudit__c
                WHERE User_Audit__c IN :userAuditByUser.values()

        ];

        Map<Id, VTR5_SiteStudyAccessAudit__c> stmToSSAAmap = new Map<Id, VTR5_SiteStudyAccessAudit__c>();
        for(VTR5_SiteStudyAccessAudit__c ssaa : allSSAA) {
            stmToSSAAmap.put(ssaa.VTR5_StudyTeamMember__c, ssaa);
        }
        List<VTR5_SiteStudyAccessAudit__c> ssaaToUpsert = new List<VTR5_SiteStudyAccessAudit__c>();

        // Create, Update SSAA
        for(Study_Site_Team_Member__c member : members) {
            Id userId, memberStudy, memberStm, memberSiteName;
            if(member.VTR2_Associated_SCr__c != null){
                userId = associatedSTM.get(member.VTR2_Associated_SCr__c).User__r.Id;
                memberStudy = associatedSTM.get(member.VTR2_Associated_PI__c).Study__c;
                memberSiteName = associatedSTM.get(member.VTR2_Associated_PI__c).VTD1_VirtualSite__c;

                memberStm = member.VTR2_Associated_SCr__c;
            }else if(member.VTD1_Associated_PG__c != null){
                userId = associatedSTM.get(member.VTD1_Associated_PG__c).User__r.Id;
                memberStudy = associatedSTM.get(member.VTD1_Associated_PI__c).Study__c;

                memberSiteName = associatedSTM.get(member.VTD1_Associated_PI__c).VTD1_VirtualSite__c;

                memberStm = member.VTD1_Associated_PG__c;
            }else if(member.VTR2_Associated_SubI__c != null){
                userId = associatedSTM.get(member.VTR2_Associated_SubI__c).User__r.Id;
                memberStudy = associatedSTM.get(member.VTR2_Associated_PI3__c).Study__c;

                memberSiteName = associatedSTM.get(member.VTR2_Associated_PI3__c).VTD1_VirtualSite__c;

                memberStm = member.VTR2_Associated_SubI__c;
            }
            //Start SH-19487 add the CRA as well for audit Report
            else if(member.VTR5_Associated_CRA__c != null){
                userId = associatedSTM.get(member.VTR5_Associated_CRA__c).User__r.Id;
                memberStudy = associatedSTM.get(member.VTR5_Associated_CRA__c).Study__c;
                memberSiteName = member.VTD1_SiteID__c;
                memberStm = member.VTR5_Associated_CRA__c;
            } // END SH-19487


            VTR5_SiteStudyAccessAudit__c memberStudySSAA = studyToSSAAmap.get(memberStudy);

            if (memberStudySSAA == null){
                    ssaaToUpsert.add(new VTR5_SiteStudyAccessAudit__c(
                            VTR5_StudyTeamMember__c = memberStm,
                            VTR5_StudyName__c = memberStudy,
                            VTR5_SiteName__c = memberSiteName,
                            VTR5_SiteAccessStatus__c = true,
                            VTR5_StudyAccessStatus__c = true,
                            VTR5_AssignedDateStudy__c = stmToSSAAmap.get(memberStm) != null ? stmToSSAAmap.get(memberStm).VTR5_AssignedDateStudy__c : Datetime.now(),
                            VTR5_AssignedDateSite__c = Datetime.now(),
                            User_Audit__c = userAuditByUser.get(userId).Id
                    ));
                //Check That SSAA exist on Virtual Site
            } else if(memberStudySSAA.VTR5_StudyName__c != null && memberStudySSAA.User_Audit__c == userAuditByUser.get(userId).Id
                    && memberStudySSAA.VTR5_SiteDeactivatedDate__c == null && ( memberStudySSAA.VTR5_SiteName__c == memberSiteName
                                                                            || memberStudySSAA.VTR5_SiteName__c == null)) {
                //if exist then update SSAA Fields
                memberStudySSAA.VTR5_SiteName__c = memberSiteName;
                memberStudySSAA.VTR5_SiteAccessStatus__c = true;
                memberStudySSAA.VTR5_AssignedDateSite__c = Datetime.now();

                ssaaToUpsert.add(memberStudySSAA);
            } else {
                    ssaaToUpsert.add(new VTR5_SiteStudyAccessAudit__c(
                            VTR5_StudyTeamMember__c = memberStm,
                            VTR5_StudyName__c = memberStudy,
                            VTR5_SiteName__c = memberSiteName,
                            VTR5_SiteAccessStatus__c = true,
                            VTR5_StudyAccessStatus__c = true,
                            VTR5_AssignedDateStudy__c = stmToSSAAmap.get(memberStm) != null ? stmToSSAAmap.get(memberStm).VTR5_AssignedDateStudy__c : Datetime.now(),

                                        VTR5_AssignedDateSite__c = Datetime.now(),
                                        User_Audit__c = userAuditByUser.get(userId).Id
                                ));
                            }
                        }

        System.debug('ssaaToUpsert --> ' + ssaaToUpsert);
        // SH - 19487 Audit Report issue
       try{
           upsert ssaaToUpsert;
       } catch (Exception ex) {
           ErrorLogUtility.logException(ex, null, VT_R5_SSTMauditHandler.class.getName());
       } // End Sh - 19487
       
    }

    public static void deactivateSSAA(List<Study_Site_Team_Member__c> members) {
        Map<Id, Study_Team_Member__c> associatedSTM = new Map<Id, Study_Team_Member__c>();
        for (Study_Site_Team_Member__c member : members) {
            associatedSTM.put(member.VTR2_Associated_PI3__c, null);
            associatedSTM.put(member.VTD1_Associated_PI__c, null);
            associatedSTM.put(member.VTR2_Associated_PI__c, null);

            associatedSTM.put(member.VTR2_Associated_SCr__c, null);
            associatedSTM.put(member.VTD1_Associated_PG__c, null);
            associatedSTM.put(member.VTR2_Associated_SubI__c, null);

            associatedSTM.put(member.VTR5_Associated_CRA__c, null); // Start SH - 19487 Add the CRA user as well for the Audit Object
        }

        associatedSTM.remove(null);
        associatedSTM.putAll([SELECT Id, Study__c, VTD1_VirtualSite__c, User__r.Id  FROM Study_Team_Member__c WHERE Id IN :associatedSTM.keySet()]);

        // get existing User Audits for users or create new
        Set<Id> userIds = new Set<Id>();
        Set<Id> membersIds = new Set<Id>();
        Map<Id, Study_Team_Member__c> userByStmMap = new Map<Id, Study_Team_Member__c>();
        System.debug('members --> ' + members);
        for (Study_Site_Team_Member__c member : members) {
            if(member.VTR2_Associated_SCr__c != null){
                userByStmMap.put(associatedSTM.get(member.VTR2_Associated_SCr__c).User__r.Id,associatedSTM.get(member.VTR2_Associated_PI__c));
            }else if(member.VTD1_Associated_PG__c != null){
                userByStmMap.put(associatedSTM.get(member.VTD1_Associated_PG__c).User__r.Id, associatedSTM.get(member.VTD1_Associated_PI__c));
            }else if(member.VTR2_Associated_SubI__c != null){
                userByStmMap.put(associatedSTM.get(member.VTR2_Associated_SubI__c).User__r.Id, associatedSTM.get(member.VTR2_Associated_PI3__c));
            }
            // Start SH - 19487 Add the CRA user as well for the Audit Object
            else if(member.VTR5_Associated_CRA__c != null){
                userByStmMap.put(associatedSTM.get(member.VTR5_Associated_CRA__c).User__r.Id, associatedSTM.get(member.VTR5_Associated_CRA__c));
            } // End SH - 19487
        }

        System.debug('--> ' + userByStmMap);
        List<VTR5_SiteStudyAccessAudit__c> ssaaToUpdate = [
                SELECT Id, User_Audit__c,  VTR5_StudyName__c, VTR5_SiteName__c, VTR5_SiteAccessStatus__c,
                        VTR5_AssignedDateSite__c, VTR5_SiteDeactivatedDate__c, VTR5_StudyTeamMember__c, User_Audit__r.User__c
                FROM VTR5_SiteStudyAccessAudit__c
                WHERE User_Audit__r.User__c IN :userByStmMap.keySet()
                AND VTR5_SiteDeactivatedDate__c = NULL
        ];


        System.debug('-> ' + ssaaToUpdate);
        for(VTR5_SiteStudyAccessAudit__c ssaa : ssaaToUpdate){
            Study_Team_Member__c stm = userByStmMap.get(ssaa.User_Audit__r.User__c);
            if (stm.VTD1_VirtualSite__c == ssaa.VTR5_SiteName__c && stm.Study__c == ssaa.VTR5_StudyName__c && ssaa.VTR5_SiteAccessStatus__c) {

            ssaa.VTR5_SiteDeactivatedBy__c = UserInfo.getName();
            ssaa.VTR5_SiteDeactivatedDate__c = Datetime.now();
            ssaa.VTR5_SiteAccessStatus__c = false;
        }

        }
        System.debug('ssaaToUpdate ' + ssaaToUpdate);
        // SH - 19487 Audit Report issue
        try {
            update ssaaToUpdate;
        } catch (Exception ex) {
            ErrorLogUtility.logException(ex, null, VT_R5_SSTMauditHandler.class.getName());
        }
            
    } // End SH - 19487 

}