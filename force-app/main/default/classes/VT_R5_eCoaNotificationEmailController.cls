/**
 * Created by Stanislav Frolov on 8/6/2020.
 */

public without sharing class VT_R5_eCoaNotificationEmailController {

    private static final String MAIN_LINK_PART = getMainLinkPart();

    public Id notificationId { get; set; }
    public String notificationMessage { get; set; }
    public String notificationLink { get; set; }

    public String eDiaryLink {
        get {
            return MAIN_LINK_PART + notificationLink.replace('#message', EncodingUtil.urlEncode(notificationMessage, 'UTF-8'));
        }
    }
    private static String getMainLinkPart() {

        List<VTD1_General_Settings__mdt> generalSettingsList = [
                SELECT VTR4_DeepActivationLink__c
                FROM VTD1_General_Settings__mdt
        ];
        if (generalSettingsList.isEmpty()) return '';

        String herokuAppURL = generalSettingsList[0].VTR4_DeepActivationLink__c;
        if (herokuAppURL.endsWith('/')) {
            herokuAppURL = herokuAppURL.removeEnd('/');
        }
        return herokuAppURL + VT_R5_ConstantsHelper_Mobile.MY_DIARY_DEEP_LINK_PATH + VTD1_RTId__c.getInstance().VTD2_Patient_Community_URL__c + '/';
    }
}