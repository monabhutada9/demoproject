/************************************************************************
 * Name	 : VTR5_SkillAndChatButtonBatch
 * Author : Mahesh Shimpi
 * Desc	 : Batch which is executed once on production for creating Study Skill, 
 *         LiveChatButtons & LiveChatButtonSkills for existing studies.
 * Modification Log:
 * ----------------------------------------------------------------------
 * Developer		        Date			    Description
 * ----------------------------------------------------------------------
 * ---ABC-----  		   05/04/2019           Original 
 * 
 *************************************************************************/
public class VTR5_SkillAndChatButtonBatch implements Database.Batchable<SObject>, Database.AllowsCallouts{
    public final String query;
    public VTR5_SkillAndChatButtonBatch(String query){
        this.query = query;
    }
    public Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('TEST! ' + Database.getQueryLocator(this.query));
        return  Database.getQueryLocator(this.query);
    }
    public void execute(Database.BatchableContext bc, List<HealthCloudGA__CarePlanTemplate__c> studies){
        // process each batch of records
        VTR5_SkillAndChatButtonController.isRunningFromBatch = true;
        System.debug('Studies :' + studies);
        for (HealthCloudGA__CarePlanTemplate__c study : studies) {
            VTR5_SkillAndChatButtonController.ResponseJSON result = VTR5_SkillAndChatButtonController.createStudySkillAndChatButtons(study.Id);
            System.debug('Response in Batch :' + result);
            if (!Test.isRunningTest()) {
                VTR5_SkillAndChatButtonController.updateStudies(study, result);
            }
        }
    }    
    public void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }    
    public static void runBatch(Boolean forFewOrAll){
        if (!Test.isRunningTest()){
            String query = 'SELECT Id, Name, VT_R5_Is_study_skill_created__c, VT_R5_Are_chatbuttons_created__c '+
                            'FROM HealthCloudGA__CarePlanTemplate__c';
            if (forFewOrAll) {
                query = 'SELECT Id, Name, VT_R5_Is_study_skill_created__c, VT_R5_Are_chatbuttons_created__c '+ 
                            'FROM HealthCloudGA__CarePlanTemplate__c '+
                            'WHERE VT_R5_Is_study_skill_created__c=false OR VT_R5_Are_chatbuttons_created__c = false';
            }
            VTR5_SkillAndChatButtonBatch batch = new VTR5_SkillAndChatButtonBatch(query);
            Database.executeBatch(batch, 1);
        }else {
            String query = 'SELECT Id, Name, VT_R5_Is_study_skill_created__c, VT_R5_Are_chatbuttons_created__c '+
                            'FROM HealthCloudGA__CarePlanTemplate__c '+
                            'WHERE VT_R5_Is_study_skill_created__c=false OR VT_R5_Are_chatbuttons_created__c = false LIMIT 10';
            VTR5_SkillAndChatButtonBatch batch = new VTR5_SkillAndChatButtonBatch(query);
            Database.executeBatch(batch);
        }
    }
}