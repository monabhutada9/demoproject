/**
* @author: Carl Judge
* @date: 30-Jan-19
* @description: Test for VTD1_LiveChatTranscriptTriggerHandler
**/

@IsTest
public with sharing class VTD1_LiveChatTranscriptTriggerTest {

    public static void doTest() { 
        Case carePlan = [
            SELECT Id, VTD1_Patient_User__r.ContactId
            FROM Case
            WHERE RecordType.DeveloperName = 'CarePlan'
        ];

        LiveChatVisitor lcv = new LiveChatVisitor();
        insert lcv;

        insert new LiveChatTranscript(
            CaseId = carePlan.Id,
            ContactId = carePlan.VTD1_Patient_User__r.ContactId,
            LiveChatVisitorId = lcv.Id
        );

        LiveChatTranscript lctToUpdate = new LiveChatTranscript(LiveChatVisitorId = lcv.Id);
        insert lctToUpdate;
        lctToUpdate.CaseId = carePlan.Id;
        lctToUpdate.ContactId = carePlan.VTD1_Patient_User__r.ContactId;
        update lctToUpdate;
    }
}