/**
 * Created by user on 06-Oct-20.
 */

public with sharing class VT_R5_ScheduleUpdateUserAudit implements Schedulable{
    public void execute(SchedulableContext sc) {
        List<VTR5_UserAudit__c> userAudits = [SELECT Id, User__c, VTR5_LastSystemLogin__c FROM VTR5_UserAudit__c];
        Map<Id, VTR5_UserAudit__c> userByUserAudit = new Map<Id, VTR5_UserAudit__c>();
        for (VTR5_UserAudit__c ua : userAudits) {
            userByUserAudit.put(ua.User__c, ua);
        }
        List<User> users = [SELECT LastLoginDate FROM User WHERE Id IN :userByUserAudit.keySet()];

        List<VTR5_UserAudit__c> uaToUpdate = new List<VTR5_UserAudit__c>();
        for (User u : users) {
            VTR5_UserAudit__c ua = userByUserAudit.get(u.Id);
            if (ua.VTR5_LastSystemLogin__c != u.LastLoginDate) {
                ua.VTR5_LastSystemLogin__c = u.LastLoginDate;
                uaToUpdate.add(ua);
            }
        }
        if (uaToUpdate.size() > 0) {
            update uaToUpdate;
        }
    }
}