/**
 * @author Ruslan Mullayanov
 * @see VT_D1_SubjectIntegration
 */
@IsTest
private class VT_D1_Test_SubjectIntegration {
    public class CalloutMock implements HttpCalloutMock {
        private Boolean failResponse = false;

        public CalloutMock(){}

        public CalloutMock(Boolean failResponse){
            this.failResponse = failResponse;
        }

        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/xml');
            res.setBody('');
            res.setStatusCode(200);
            if(failResponse){
                res.setStatusCode(500);
                res.setBody('<Error/>');
            }
            return res;
        }
    }

    @TestSetup
    private static void setup() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();

        Case cas = [SELECT Id,VTD1_Patient_User__c, VTD1_Primary_PG__c,VTD1_PI_user__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        User patient = [SELECT Id, Name, ContactId, AccountId, Contact.AccountId FROM User WHERE Id = :cas.VTD1_Patient_User__c];
        User CG = VT_D1_TestUtils.createUserByProfile('Caregiver', 'CaregiverTest');
        Contact cgCon = new Contact(FirstName = CG.FirstName, LastName = 'L', AccountId = patient.Contact.AccountId);
        insert cgCon;
        CG.ContactId = cgCon.Id;
        insert CG;

        List<Address__c> addresses = new List<Address__c>();
        Set<Id> contactIds = new Set<Id>();
        for (Address__c item : [
            SELECT Id, VTD1_Primary__c, VTD1_Contact__c
            FROM Address__c
            WHERE (VTD1_Contact__c = :patient.ContactId OR VTD1_Contact__c = :CG.ContactId)
                AND VTD1_Primary__c = true
        ]) {
            contactIds.add(item.VTD1_Contact__c);
        }

        for (User u : new List<User>{patient, CG}) {
            addresses.add(new Address__c(
                Name = 'Test address '+u.Name,
                VTD1_Contact__c = u.ContactId,
                VTD1_Address2__c = '26109 Cherry Blossom Court',
                City__c = 'New-York',
                State__c = 'NY',
                ZipCode__c = '100100',
                VTD1_Primary__c = !contactIds.contains(u.ContactId),
                Country__c = 'US'
            ));
            contactIds.add(u.ContactId);
        }

        List<VT_D1_Phone__c> caregiverPhones = new List<VT_D1_Phone__c>{
            new VT_D1_Phone__c(PhoneNumber__c='12345', IsPrimaryForPhone__c = true, Type__c='Home', VTD1_Contact__c=CG.ContactId, Account__c = patient.Contact.AccountId),
            new VT_D1_Phone__c(PhoneNumber__c='67890', IsPrimaryForPhone__c = false, Type__c='Home', VTD1_Contact__c=CG.ContactId, Account__c = patient.Contact.AccountId)
        };

        insert addresses;
        insert caregiverPhones;
    }

    @IsTest
    private static void sendSubjectStatusTest() {
        HttpCalloutMock cMock = new CalloutMock();
        Test.setMock(HttpCalloutMock.class, cMock);

        /*
        Account account = new Account(
            Name = 'Account'
        );
        insert account;

        VTD1_Patient__c patient = new VTD1_Patient__c(
            VTD1_First_Name__c  = 'Patient1',
            VTD1_Last_Name__c   = 'Smith',
            VTD1_Account__c     = account.Id
        );
        insert patient;
        */
        Id caseId = [SELECT Id FROM Case LIMIT 1].Id;
        Test.startTest();
        VT_D1_SubjectIntegration.sendSubjectStatus(caseId);
        Test.stopTest();
    }

    @IsTest
    private static void upsertSubjectTestCSM() {
        HttpCalloutMock cMock = new CalloutMock();
        Test.setMock(HttpCalloutMock.class, cMock);

        Case cas = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        cas.VTD1_CreatedInCenduitCSM__c = true;
        update cas;
        Test.startTest();
        VT_D1_SubjectIntegration.upsertSubject(cas.Id);
        Test.stopTest();
    }

    @IsTest
    private static void upsertSubjectTestNoCSM() {
        HttpCalloutMock cMock = new CalloutMock();
        Test.setMock(HttpCalloutMock.class, cMock);

        Case cas = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];

        Test.startTest();
        VT_D1_SubjectIntegration.upsertSubject(cas.Id);
        Test.stopTest();
    }

    @IsTest
    private static void randomizeSubjectTest() {
        HttpCalloutMock cMock = new CalloutMock();
        Test.setMock(HttpCalloutMock.class, cMock);
        //Get Case record
        Case cas = [SELECT Id,VTD1_Study__c,VTD1_ContactGender__c, ContactId, VTD1_Patient_User__c, VTD1_Primary_PG__c,VTD1_PI_user__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        Contact con = [SELECT id,HealthCloudGA__Gender__c from Contact where Id =: cas.ContactId];
        con.HealthCloudGA__Gender__c = 'Female';
        Update con;
        //Study Stratification Record creation
        VTD1_Study_Stratification__c studyStratification = new VTD1_Study_Stratification__c();
        studyStratification.VTD1_Study__c = cas.VTD1_Study__c;
        Insert studyStratification;
        //Patient stratification record insersion
        VTD1_Patient_Stratification__c patientStratification = new VTD1_Patient_Stratification__c();
        patientStratification.Value__c ='Value1';
        patientStratification.VTD1_Patient__c = cas.Id;
        patientStratification.VTD1_Study_Stratification__c = studyStratification.Id;
        Insert patientStratification; 
        //Survey Record Insersion
        VTD1_Survey__c survey = new VTD1_Survey__c();
        survey.VTD1_CSM__c = cas.Id;
        Insert survey;
        //Survey Answer Record Insersion
        VTD1_Survey_Answer__c surveyAns = new VTD1_Survey_Answer__c();
        surveyAns.VTD1_Answer__c = '10';
        surveyAns.VTD1_Survey__c = survey.Id;
        surveyAns.VT_R5_External_Widget_Data_Key__c = 'diabetes_month';
        Insert surveyAns;
        VTD1_Survey_Answer__c surveyAns1 = new VTD1_Survey_Answer__c();
        surveyAns1.VTD1_Answer__c = '2019';
        surveyAns1.VTD1_Survey__c = survey.Id;
        surveyAns1.VT_R5_External_Widget_Data_Key__c = 'diabetes_year';
        Insert surveyAns1;
        Test.startTest();
        VT_D1_SubjectIntegration.randomizeSubject(cas.Id);
        Test.stopTest();
    }
}