/**
* Action Items list, filtered by Study Ids
* @author Ruslan Mullayanov
*/
public with sharing class VT_D1_ActionItemsController {
    @AuraEnabled(Cacheable=true)
    public static List<ActionItemWrapper> getActionItems(Id studyId) {
        User user = [select Id, Profile.Name from User where Id = :UserInfo.getUserId()];

        List<ActionItemWrapper> lst_res = new List<ActionItemWrapper>();
        try {
            List<Id> myStudyIds = VT_D1_PIStudySwitcherRemote.getMyStudyIds(studyId);
            System.debug('myStudyIds: '+myStudyIds);

            List<VTD1_Action_Item__c> lst = [
                    SELECT Id, Name, VTD1_Study__r.Name, VTD1_Patient__r.VTD1_Subject_ID__c, VTD1_Status_Description__c,
                            VTD1_Severity_Description__c,
                            Owner.FirstName, Owner.LastName, VTD1_Due_Date__c,
                            CreatedBy.FirstName, CreatedBy.LastName, CreatedDate,
                            VTD1_Country__c, VTD1_Days_Open__c, VTD1_Monitoring_Visit__r.Name,
                            VTD1_Resolution_Description__c, VTD1_Resolution_Date__c,
                            VTD1_Action_Item_Note_Description__c, VTD1_Note_Create_Date__c,
                            VTD1_PI_Name__r.Name, VTD2_Description__c,  VTD1_Open_Date__c, VTD1_Action_Item_Description__c,
                            //START RAJESH SH-17440 remove the CRA reference.
                            //VTD1_Remote_AI_Flag__c,
                            //END:SH-17440
                             VTD2_Assigned_To__c, VTD2_Assigned_To__r.Name
                    FROM VTD1_Action_Item__c
                    WHERE VTD1_Study__c IN :myStudyIds
                    ORDER BY Name ASC
            ];

            for (VTD1_Action_Item__c ai : lst) {
                ActionItemWrapper aiw = new ActionItemWrapper(ai);
                aiw.ProfileName = user.Profile.Name;
                lst_res.add(aiw);
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
        return lst_res;
    }

    @AuraEnabled
    public static String getUsersFilter(Id studyId) {
        List <String> results = new List<String>();
        List <Study_Team_Member__c> studyTeamMembers = [select User__c from Study_Team_Member__c where Study__c = : studyId and
            User__r.Profile.Name in ('Patient Guide', 'Site Coordinator', 'Study Concierge', 'CRA', 'Monitor')];

        for (Study_Team_Member__c studyTeamMember : studyTeamMembers) {
            results.add('\'' + studyTeamMember.User__c + '\'');
        }

        return 'Id in (' + String.join(results, ',') + ')';
    }

    @AuraEnabled
    public static String updateActionItem(Id actionItemId, Id userId, String resolutionDescription, String resolutionDate) {
        try {
            VTD1_Action_Item__c actionItem = [select Id, VTD2_Assigned_To__c, VTD1_Resolution_Date__c, VTD1_Resolution_Description__c from VTD1_Action_Item__c where Id = : actionItemId];
            actionItem.VTD2_Assigned_To__c = userId;
            if (resolutionDate != null) {
                actionItem.VTD1_Resolution_Date__c = Date.parse(resolutionDate);
            }
            actionItem.VTD1_Resolution_Description__c = resolutionDescription;
            update actionItem;
            return '';// + actionItem.VTD1_Resolution_Date__c;
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public class ActionItemWrapper {
        @AuraEnabled public VTD1_Action_Item__c ai { get; set; }

        @AuraEnabled public String Id { get; set; }
        @AuraEnabled public String Name { get; set; }
        @AuraEnabled public String Study { get; set; }
        @AuraEnabled public String SubjectId { get; set; }
        @AuraEnabled public String Status { get; set; }
        @AuraEnabled public String Severity { get; set; }
        @AuraEnabled public String ActionRequired { get; set; }
        @AuraEnabled public String AssignedTo { get; set; }
        @AuraEnabled public String DueDate { get; set; }

        @AuraEnabled public String PrimaryInvestigator { get; set; }
        @AuraEnabled public String Description { get; set; }
        @AuraEnabled public String DateOccurred { get; set; }
        @AuraEnabled public String MonitoringVisit { get; set; }
        //STATR RAJESH SH-17440
       // @AuraEnabled public String ResolutionMethod { get; set; }
        //END:SH-17440
        @AuraEnabled public String ResolutionDescription { get; set; }
        @AuraEnabled public String ResolutionDate { get; set; }
        @AuraEnabled public String Notes { get; set; }
        @AuraEnabled public String ProfileName { get; set; }

        public ActionItemWrapper(VTD1_Action_Item__c a) {
            ai = a;

            Id = a.Id;
            Name = a.Name;
            Study = a.VTD1_Study__r.Name!=null? a.VTD1_Study__r.Name:'';
            SubjectId = a.VTD1_Patient__r.VTD1_Subject_ID__c!=null? a.VTD1_Patient__r.VTD1_Subject_ID__c:'';
            Status = a.VTD1_Status_Description__c;
            Severity = a.VTD1_Severity_Description__c;
            ActionRequired = a.VTD1_Action_Item_Description__c;
            AssignedTo = a.VTD2_Assigned_To__r.Name;//(a.Owner.FirstName!=null && a.Owner.LastName!=null)? a.Owner.FirstName + ' ' + a.Owner.LastName:'';
            DueDate = a.VTD1_Due_Date__c!=null? ((Datetime)a.VTD1_Due_Date__c).format('dd-MMM-yyyy'):'';

            //DocumentedBy = (a.CreatedBy.FirstName!=null && a.CreatedBy.LastName!=null)? a.CreatedBy.FirstName + ' ' + a.CreatedBy.LastName:'';
            //DocumentedBy = a.VTD1_Author_First_Name__c + ' ' + a.VTD1_Author_Last_Name__c;
            //DocumentedDate = a.CreatedDate.format('dd-MMM-yyyy');
            //Country = a.VTD1_Country__c;
            //DaysOpen = String.valueOf(a.VTD1_Days_Open__c);
            PrimaryInvestigator = a.VTD1_PI_Name__r.Name;
            Description = a.VTD2_Description__c;
            DateOccurred = a.VTD1_Open_Date__c!=null? ((Datetime)a.VTD1_Open_Date__c).format('dd-MMM-yyyy'):'';
            MonitoringVisit = a.VTD1_Monitoring_Visit__r.Name!=null? a.VTD1_Monitoring_Visit__r.Name:'';
            //START RAJESH SH-17440:Remvoe CRA reference
            //ResolutionMethod = a.VTD1_Remote_AI_Flag__c;
            //END:SH-17440
            ResolutionDescription = a.VTD1_Resolution_Description__c;
            ResolutionDate = a.VTD1_Resolution_Date__c!=null? ((Datetime)a.VTD1_Resolution_Date__c).formatGMT('dd-MMM-yyyy'):'';
            Notes = a.VTD1_Action_Item_Note_Description__c;
            //NoteDescriptionDate = a.VTD1_Note_Create_Date__c!=null? ((DateTime)a.VTD1_Note_Create_Date__c).format('dd-MMM-yyyy'):'';
        }
    }
}