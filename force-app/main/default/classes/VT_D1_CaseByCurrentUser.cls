public without sharing class  VT_D1_CaseByCurrentUser {
    @AuraEnabled(Cacheable=true)
    public static CaseWrapper getCase() {
        System.debug('STARTING GETTING CASES!');
        Id contactId;
        User patientUser = [SELECT ContactId FROM User WHERE Id = :VT_D1_PatientCaregiverBound.getPatientId()];
        if (patientUser.ContactId != null) {
            contactId = patientUser.ContactId;
        }

        System.debug('contactId : ' + contactId);
        List<Case> cases = new List<Case>();
        if (contactId != null) {
            cases = [
                    SELECT
                            Id,
                            AccountId,
                            VTD1_Patient__c,
                            VTD1_Patient__r.VTD1_Patient_Name__c,
                            VTD1_Patient_User__c,
                            VTD1_Patient_User__r.Name,
                            VTD1_Patient_User__r.VTD1_Full_Name__c,
                            VTD1_PI_contact__c,
                            VTD1_PI_contact__r.Name,
                            VTD1_PI_contact__r.FirstName,
                            VTD1_PI_contact__r.LastName,
                            VTD1_PI_user__c,
                            VTD1_PI_user__r.MediumPhotoUrl,
                            VTD1_PI_user__r.FullPhotoUrl,
                            VTD1_PI_user__r.Name,
                            VTD1_PI_user__r.FirstName,
                            VTD1_PI_user__r.LastName,
                            VTD1_Primary_PG__c,
                            VTD1_Primary_PG__r.Name,
                            VTD1_Primary_PG__r.FirstName,
                            VTD1_Primary_PG__r.LastName,
                            VTD1_Primary_PG__r.MediumPhotoUrl,
                            VTD1_Primary_PG__r.FullPhotoUrl,
                            VTR2_SiteCoordinator__c,
                            VTR2_SiteCoordinator__r.Name,
                            VTR2_SiteCoordinator__r.FirstName,
                            VTR2_SiteCoordinator__r.LastName,
                            VTR2_SiteCoordinator__r.MediumPhotoUrl,
                            VTR2_SiteCoordinator__r.FullPhotoUrl,
                            VTD1_Study__c,
                            VTD1_Study__r.VTD1_Study_Logo__c,
                            VTD1_Study__r.Name,
                            VTD1_Study__r.VTD1_Name__c,
                            VTD1_Study__r.VTD1_Study_Library__c,
                            VTD1_Study__r.VTD1_Protocol_Nickname__c,
                            VTD1_Study__r.VTD1_Return_Process__c,
                            VTD1_Study__r.VTD1_IMP_Return_Numbers__c,
                            VTD1_Study__r.VDT2_Send_Visit_Reminders__c,
                            VTD1_Study__r.VTR2_Send_eDiary_Notifications__c,
                            VTD1_Study__r.VTR2_Send_New_Messages__c,
                            VTD2_Study_Geography__c,
                            VTD2_Study_Geography__r.Name,
                            VTD2_Study_Geography__r.VTD2_Geographical_Region__c,
                            VTD2_Study_Geography__r.VTD2_Study__c,
                            VTR2_Receive_SMS_Visit_Reminders__c,
                            VTR2_Receive_SMS_New_Notifications__c,
                            VTR2_Receive_SMS_eDiary_Notifications__c,
                            VTR2_StudyPhoneNumber__c,
                            VTR2_StudyPhoneNumber__r.Name,
                            VTD1_ePro_can_be_completed_by_Caregiver__c,
                            Status,
                            VTD1_Virtual_Site__c,
                            VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c,
                            VTD1_Study__r.VTR5_IRTSystem__c,
                            VTR5_Day_1_Dose__c,
                            VTR5_Day_57_Dose__c
                    FROM Case
                    WHERE Id IN (SELECT VTD1_Clinical_Study_Membership__c FROM Contact WHERE Id = :contactId)
                    LIMIT 1
                    //WHERE RecordType.Name='CarePlan' AND ContactId =:contactId AND ContactId != null
            ];
        }
        if (!cases.isEmpty()) {
            Case c = cases[0];
            String link = '';
            if (c.VTD1_PI_user__c!= null && c.VTD1_Study__c!=null) {
                List<Study_Team_Member__c> stm = [
                        SELECT Study__c, VTD1_VirtualSite__r.VTR5_ConsentPacketLink__c ,VTD1_UserName__c
                        FROM Study_Team_Member__c
                        WHERE Study__c=:c.VTD1_Study__c AND User__c=:c.VTD1_PI_user__c
                ];
                if (stm.size() > 0 && !(stm[0].VTD1_VirtualSite__c == null)) {
                    link = stm[0].VTD1_VirtualSite__r.VTR5_ConsentPacketLink__c;
                }
            }
            try {
                VT_D1_TranslateHelper.translate(cases);
                VT_D1_TranslateHelper.translate(c.VTR2_SiteCoordinator__r);
                VT_D1_TranslateHelper.translate(c.VTD1_Primary_PG__r);
                VT_D1_TranslateHelper.translate(c.VTD1_PI_user__r);
                VT_D1_TranslateHelper.translate(c.VTD1_Study__r);
                VT_D1_TranslateHelper.translate(c.VTD2_Study_Geography__r);
            } catch (Exception e) {
                System.debug('Exception: ' + e.getMessage());
            }
            System.debug('Case: ' + c);
            return new CaseWrapper(c, link);
        } else {
            return new CaseWrapper(new Case(), '');
        }
    }

    public class CaseWrapper {
        @AuraEnabled public Case caseObj { get; set; }
        @AuraEnabled public String consentPacketLink { get; set; }

        public CaseWrapper(Case case_v, String link) {
            caseObj = case_v;
            consentPacketLink = link;
        }
    }
}