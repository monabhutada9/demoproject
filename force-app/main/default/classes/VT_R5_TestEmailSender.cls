public with sharing class VT_R5_TestEmailSender {
    public VT_R5_TestEmailSender() {

    }

    @AuraEnabled(Cacheable=true)
    public static void SendEmailToRecipients(){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail;
        mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId('0051w000004VAE0');
        mail.setWhatId('a291w000000OfAAAA0');
        mail.setTemplateId('00X1N000000cTzQUAU');
        mail.setOrgWideEmailAddressId('0D21N000000PMCcSAO');
        mail.setSaveAsActivity(false);
        mails.add(mail);
        VT_R5_TestEmailSender.sendEmails(mails);
    }
    
    public static void sendEmails(List<Messaging.SingleEmailMessage> mails) {
        if (!mails.isEmpty()) {
            List<Messaging.SendEmailResult> results = Messaging.sendEmail(mails, false);
            for (Messaging.SendEmailResult result : results) {
                System.debug('result = ' + result.isSuccess());
                for (Messaging.SendEmailError error : result.errors) {
                    System.debug(error.getMessage());
                    throw new AuraHandledException('Exception : '+error.getMessage());   
                }
            }
        }   
    }
    
    
    
    /*******************************************************************************/
    /*
    **@author:      IQVIA Strikers
    **@date:        12/01/2020
    **@description: The Wrapper class is used to check if the running user has access of recipients(in this case PT and CG) 
                    to which the emails should be sent. "Without Sharing" keyword is used in order to query relevant records from the database.
    */
    public without sharing Class VT_R5_calculateSharingOfRecipientsWrp{

        public VT_R5_calculateSharingOfRecipientsWrp(){}

        //If running  user's profile belongs to these profiles then Usershare records will not be reuired 
        //since existing sharing design provides access to these users.
        public Set<String> profileNameSet = new Set<String>{ VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME,
                                                             VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME,
                                                             VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME,
                                                             VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME,
                                                             VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME,
                                                             VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME,
                                                             VT_R4_ConstantsHelper_ProfilesSTM.CM_PROFILE_NAME,
                                                             VT_R4_ConstantsHelper_ProfilesSTM.RE_PROFILE_NAME,
                                                             VT_R4_ConstantsHelper_ProfilesSTM.AUDITOR_PROFILE_NAME };

        /*
        **@author:      IQVIA Strikers
        **@date:        12/01/2020
        **@description: This method queries the UserRecordAccess in order to check running user's access for recipients(in this case PT and CG).
        */
        public Set<Id> checkWithUserRecordAccess(Set<Id> recipientIdSet){
            Set<Id> recipeintWithoutAccess = new set<Id>();
            Map<Id,UserRecordAccess> userRecordAccessMap = new Map<Id,UserRecordAccess>();
            for(UserRecordAccess ura : [SELECT RecordId,HasReadAccess FROM UserRecordAccess 
                                        where UserId =: UserInfo.getUserId() and RecordId in:recipientIdSet]){                            
                if(ura.HasReadAccess){
                   userRecordAccessMap.put(ura.RecordId,ura);
                }
            }
            for(id uId: recipientIdSet){
                if(!userRecordAccessMap.containskey(uId)){
                recipeintWithoutAccess.add(uId);
                }
            }
            return recipeintWithoutAccess;
        }

        /*
        **@author:      IQVIA Strikers
        **@date:        12/01/2020
        **@description: This method queries the GroupMember in order to check running user's access for recipients(in this case PT and CG).
        */
        public Set<Id> checkWithGroupSharing(Set<Id> recipientIdSet){
            Set<Id> recipeintWithoutAccess = new set<Id>();
            Set<Id> recipientWithAccessIdSet  = new Set<Id>();
            Set<Id> childGroupsSet = new Set<Id>();
            Set<Id> parentRecipientValues = new Set<Id>();
            Map<Id,Set<Id>> userOrGroupWithUsersMap = new Map<Id,Set<Id>>();
            Map<Id,Set<Id>> recipeintWithUserSetMap = new Map<Id,Set<Id>>();
            for (UserShare Ushr : [ SELECT Id, UserId, UserOrGroupId FROM UserShare WHERE UserId IN :recipientIdSet]){
                
                if(String.valueof(Ushr.UserOrGroupId).startsWith('00G') && !recipientWithAccessIdSet.contains(Ushr.UserId)){
                    if(userOrGroupWithUsersMap.containsKey(Ushr.UserOrGroupId)){
                        userOrGroupWithUsersMap.get(Ushr.UserOrGroupId).add(Ushr.UserId);
                    }else{
                        userOrGroupWithUsersMap.put(Ushr.UserOrGroupId, new set<Id>{Ushr.UserId});
                    }

                    if(recipeintWithUserSetMap.containsKey(Ushr.UserId)){
                    recipeintWithUserSetMap.get(Ushr.UserId).add(Ushr.UserOrGroupId);
                    }else{
                    recipeintWithUserSetMap.put(Ushr.UserId, new Set<Id>{Ushr.UserOrGroupId});
                    }
                }

                if(Ushr.UserOrGroupId == UserInfo.getUserId()) {
                    recipientWithAccessIdSet.add(Ushr.UserId);
                    if(recipeintWithUserSetMap.containsKey(Ushr.UserId)){
                       recipeintWithUserSetMap.remove(Ushr.UserId);
                    }
                }
            }
            /*
            ** If Direct sharing is not available for the users then Query on Group member records to check record access.
            */
            if(recipientIdSet.size()!=recipientWithAccessIdSet.size()){

                Set<Id> groupIdSet = new Set<Id>();
                for(Set<Id> grpSet: recipeintWithUserSetMap.values()){
                    groupIdSet.addAll(grpSet);
                }
                //This will be bring all the groupmember where UserOrGroupId could be user id or group id.
                Map<Id,GroupMember> groupMemberMap= new Map<Id,GroupMember>([select id,GroupId,UserOrGroupId from GroupMember 
                                                                             where GroupId in: GroupIdSet 
                                                                             or UserOrGroupId =: UserInfo.getUserId()]);
                for(GroupMember GrpMbr: groupMemberMap.values()){ 
                    //If the UserOrGroupId is equal to Current user id and GroupId is Parent Group Id.
                    if(userOrGroupWithUsersMap.containsKey(GrpMbr.GroupId) && GrpMbr.UserOrGroupId == UserInfo.getUserId()){
                        recipientWithAccessIdSet.addAll(userOrGroupWithUsersMap.get(GrpMbr.GroupId));
                    }// If the UserOrGroupId is Group Id
                    else if(String.valueof(GrpMbr.UserOrGroupId).startsWith('00G')){
                        childGroupsSet.add(GrpMbr.UserOrGroupId);
                        parentRecipientValues = userOrGroupWithUsersMap.get(GrpMbr.GroupId);
                        if(userOrGroupWithUsersMap.containsKey(GrpMbr.UserOrGroupId)){
                            userOrGroupWithUsersMap.get(GrpMbr.UserOrGroupId).addall(parentRecipientValues);
                        }else {
                            userOrGroupWithUsersMap.put(GrpMbr.UserOrGroupId,parentRecipientValues);
                        }
                    }
                }
                
                for(GroupMember GrpMbr:groupMemberMap.values()){
                    //If the UserOrGroupId is equal to Current user id and GroupId is Child Group Id.
                    if(childGroupsSet.contains(GrpMbr.GroupId) && GrpMbr.UserOrGroupId == UserInfo.getUserId() 
                       && userOrGroupWithUsersMap.containsKey(GrpMbr.GroupId)){
                       recipientWithAccessIdSet.addAll(userOrGroupWithUsersMap.get(GrpMbr.GroupId)); 
                    }
                }
            }
            
            for(Id uId : recipientIdSet){
                if(!recipientWithAccessIdSet.contains(uId)){
                    recipeintWithoutAccess.add(uId);
                }
            }
            return recipeintWithoutAccess;
        }

        /*
        **@author:      IQVIA Strikers
        **@date:        12/01/2020
        **@description: This method is used to create usershare records if the running user doesn't have access to recipients.
        */
        public List<UserShare> createUserSharelist(List<ParamsHolder> params){
            List<UserShare> UserSharelist = new List<UserShare>();
            Set<Id> recipientIdSet = new Set<Id>();
            Set<Id> recipientIdsToConsiderSet = new Set<Id>();
            Set<Id> recipeintWithoutAccessSet = new set<Id>();
            Map<Id, User> specificUserMap;
            /*
            **@author:      IQVIA Strikers
            **@date:        08/18/2020
            **@description: APEX library function "renderStoredEmailTemplate" fails if current user does not have access to recipient user
                            record. Quick fix solution is to provide current user access to recipient user records (if not having already)
                            temporarily (within transaction) just before sending an email and then delete all inserted share records in
                            same transaction once email is sent successfully.
            */
            User autoProc;
            User currentUserCheck;
            for (ParamsHolder ph : params) {
                if (ph.recipientId != null && ph.recipientId.startsWith('005') && UserInfo.getUserType() != 'Guest') {
                    recipientIdSet.add(ph.recipientId);
                }
            }
            
            if(!recipientIdSet.isEmpty()){
                specificUserMap = new Map<Id, user>([SELECT Id,ProfileId,Profile.Name,alias FROM User WHERE alias = :'autoproc' 
                                                    OR (Id in:recipientIdSet AND (Profile.name=:VT_R4_ConstantsHelper_Profiles.PATIENT_PROFILE_NAME 
                                                        OR Profile.name=:VT_R4_ConstantsHelper_Profiles.CAREGIVER_PROFILE_NAME))
                                                    OR Id=:UserInfo.getUserId()]);
            
                for(User u: specificUserMap.values()){
                    if(u.alias=='autoproc') {
                        autoProc=u;
                    }else if(u.Id==UserInfo.getUserId()){
                        currentUserCheck=u;
                    }else{
                        recipientIdsToConsiderSet.add(u.Id);// Only PT and CG
                    }
                }
                
                if (!recipientIdsToConsiderSet.isEmpty() && UserInfo.getUserId()!=autoProc.Id 
                    && !profileNameSet.contains(currentUserCheck.Profile.Name)) {

                    if(recipientIdSet.size()<200) {
                        recipeintWithoutAccessSet = checkWithUserRecordAccess(recipientIdsToConsiderSet);
                    } else {
                        recipeintWithoutAccessSet = checkWithGroupSharing(recipientIdsToConsiderSet);
                    }
                    
                    if(!recipeintWithoutAccessSet.isEmpty()){
                        for (Id UiD : recipeintWithoutAccessSet) { 
                            UserShare u = new UserShare();
                            u.UserId = UiD;
                            u.UserOrGroupId = UserInfo.getUserId();
                            u.UserAccessLevel = 'Read';
                            UserSharelist.add(u);
                        }
                    }
                }
            }
            system.debug('#### UserSharelist '+UserSharelist);
            return UserSharelist;
        }
    }
    
    
    
    public class ParamsHolder {
        @InvocableVariable(Label = 'Recipient Id' Required = true)
        public String recipientId;
        @InvocableVariable(Label = 'Related To Record' Required = true)
        public String relatedToId;
        @InvocableVariable(Label = 'Template Unique Name' Required = true)
        public String templateDevName;
        @InvocableVariable(Label = 'To Addresses' Required = false Description = 'Optionally')
        public String toAddresses;
        @InvocableVariable(Label = 'To Addresses Only' Required = false Description = 'Optionally. Send email only to addresses')
        public Boolean toAddressesOnly;
        @InvocableVariable(Label = 'Receiver' Required = false Description = 'For removed visit members')
        public String receiver;
        @InvocableVariable(Label = 'Subject is static?' Required = false Description = 'True if subject is static text, false if subject require translation')
        public Boolean subjectIsStatic;
        @InvocableVariable(Label = 'Is translation needed?' Required = false Description = 'True if email requires translation')
        public Boolean isTranslationNeeded = true;
        public ParamsHolder(String recipientId, String relatedToId, String templateDevName) {
            this.recipientId = recipientId;
            this.relatedToId = relatedToId;
            this.templateDevName = templateDevName;
            this.toAddresses = null;
            this.toAddressesOnly = false;
            this.subjectIsStatic = false;
        }
        public ParamsHolder(String recipientId, String relatedToId, String templateDevName, String toAddresses) {
            this.recipientId = recipientId;
            this.relatedToId = relatedToId;
            this.templateDevName = templateDevName;
            this.toAddresses = toAddresses;
            this.toAddressesOnly = false;
            this.subjectIsStatic = false;
        }
        public ParamsHolder(String recipientId, String relatedToId, String templateDevName, String toAddresses, Boolean toAddressesOnly) {
            this.recipientId = recipientId;
            this.relatedToId = relatedToId;
            this.templateDevName = templateDevName;
            this.toAddresses = toAddresses;
            this.toAddressesOnly = toAddressesOnly;
            this.subjectIsStatic = false;
        }
        public ParamsHolder() {
            this.subjectIsStatic = false;
        }
    }
    
    
    
    
    
    
}