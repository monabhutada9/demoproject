/**
 * @author: Roman Stepanov
 * @date: 04-December-2020
 * @description: SH-19579 Updating Patient ID with Cenduit ID number for patients in Pre-consent and if Patient ID begins 
 * with "AST001-". Moving the existing Patient ID (begins with AST001-) to Study Hub internal ID, and changing the Patient ID * contents to blank
 * 
 * For start processing of queue execute code:
 * 
 * VT_R5_ClearPatientIdBatch.run;
 */

public class VT_R5_ClearPatientIdBatch implements Database.Batchable<SObject> {

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
            SELECT Id, RecordType.DeveloperName, VTD1_Study__r.VTD1_Name__c, Status, VTR5_Internal_Subject_Id__c, VTD1_Subject_ID__c
            FROM Case
            WHERE RecordType.DeveloperName = 'CarePlan'
            AND VTD1_Study__r.VTD1_Name__c = 'D8110C00001'
            AND Status = 'Pre-Consent'
            AND VTD1_Subject_ID__c LIKE 'AST001-%'
        ]);
    }

    public void execute(Database.BatchableContext bc, List<Case> cases) {
        System.debug('he: cases.size(): ' + cases.size());
        List<Case> casesToUpdate = new List<Case>();

        for (Case c : cases) {
            c.VTR5_Internal_Subject_Id__c = c.VTD1_Subject_ID__c;
            c.VTD1_Subject_ID__c = null;
            casesToUpdate.add(c);
        }
        update casesToUpdate;
    }

    public void finish(Database.BatchableContext bc) {}
}