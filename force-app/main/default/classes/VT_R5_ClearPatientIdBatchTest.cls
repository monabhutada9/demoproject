/**
 * @author: Roman Stepanov
 * @date: 10-December-2020
 */

@IsTest
private without sharing class VT_R5_ClearPatientIdBatchTest {
    @TestSetup
    static void setup() {
        HealthCloudGA__CarePlanTemplate__c study = (HealthCloudGA__CarePlanTemplate__c) new DomainObjects.Study_t()
                .setName('D8110C00001')
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'))
                .persist();

        study.VTD1_Randomized__c = 'No';
        update study;

        HealthCloudGA__CarePlanTemplate__c studybad = (HealthCloudGA__CarePlanTemplate__c) new DomainObjects.Study_t()
            .setName('D1234C567890')
            .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'))
            .persist();

        studybad.VTD1_Randomized__c = 'No';
        update studybad;

        createCase('Pre-Consent', VT_R4_ConstantsHelper_AccountContactCase.CASE_PRE_CONSENT, study.Id);
        createCase('Consented', VT_R4_ConstantsHelper_AccountContactCase.CASE_CONSENTED, study.Id);
        createCase('Pre-Consent-BadStudyName', VT_R4_ConstantsHelper_AccountContactCase.CASE_PRE_CONSENT, studybad.Id);
        createBadCase('Pre-Consent-BadSubject_ID', VT_R4_ConstantsHelper_AccountContactCase.CASE_PRE_CONSENT, study.Id);
    }

    @IsTest
    static void testBatch() {

        Test.startTest();
        Database.executeBatch(new VT_R5_ClearPatientIdBatch(), 5);
        Test.stopTest();

        Case casePreConsent = getCase('Pre-Consent');
        System.assertEquals('AST001-12345', casePreConsent.VTR5_Internal_Subject_Id__c);
        System.assertEquals(null, casePreConsent.VTD1_Subject_ID__c);

        Case caseConsented = getCase('Consented');
        System.assertEquals(null, caseConsented.VTR5_Internal_Subject_Id__c);
        System.assertEquals('AST001-12345', caseConsented.VTD1_Subject_ID__c);

        Case casePreConsentBadStudyName = getCase('Pre-Consent-BadStudyName');
        System.assertEquals(null, casePreConsentBadStudyName.VTR5_Internal_Subject_Id__c);
        System.assertEquals('AST001-12345', casePreConsentBadStudyName.VTD1_Subject_ID__c);

        Case casePreConsentBadSubject_ID = getCase('Pre-Consent-BadSubject_ID');
        System.assertEquals(null, casePreConsentBadSubject_ID.VTR5_Internal_Subject_Id__c);
        System.assertEquals('AST999-12345', casePreConsentBadSubject_ID.VTD1_Subject_ID__c);
    }

    static private void createCase(String patientName, String caseStatus, Id studyId) {
        DomainObjects.Contact_t con_t = new DomainObjects.Contact_t()
                .setFirstName(patientName)
                .setLastName('VT_R5_ClearPatientIdBatchTest');
        DomainObjects.User_t user_t = new DomainObjects.User_t()
                .addContact(con_t)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        Case c = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .setStudy(studyId)
                .addVTD1_Patient_User(user_t)
                .addContact(con_t)
                .setVTD1_Subject_ID('AST001-12345')
                .setStatus(caseStatus)
                .persist();
        con_t.setVTD1_Clinical_Study_Membership(c.Id);
        update con_t.toObject();
    }

    static private void createBadCase(String patientName, String caseStatus, Id studyId) {
        DomainObjects.Contact_t con_t = new DomainObjects.Contact_t()
                .setFirstName(patientName)
                .setLastName('VT_R5_ClearPatientIdBatchTest');
        DomainObjects.User_t user_t = new DomainObjects.User_t()
                .addContact(con_t)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        Case c = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .setStudy(studyId)
                .addVTD1_Patient_User(user_t)
                .addContact(con_t)
                .setVTD1_Subject_ID('AST999-12345')
                .setStatus(caseStatus)
                .persist();
        con_t.setVTD1_Clinical_Study_Membership(c.Id);
        update con_t.toObject();
    }

    static private Case getCase(String patientName) {
        return [
                SELECT Id, RecordType.DeveloperName, VTD1_Study__r.VTD1_Name__c, Status, VTR5_Internal_Subject_Id__c, VTD1_Subject_ID__c
                FROM Case
                WHERE Contact.LastName = 'VT_R5_ClearPatientIdBatchTest' AND Contact.FirstName = :patientName
        ];
    }
}