/**
 * @author: N.Arbatskiy
 * @date: 11-June-20
 * @description: Test class for VT_R5_eCoaQueryBuilder
 */
@IsTest
public with sharing class VT_R5_eCoaQueryBuilderTest {

    @IsTest
    private static void getDefaultQueryTest() {
        VT_R5_eCoaQueryBuilder.getDefaultQuery();
        VT_R5_eCoaQueryBuilder queryBuilder = new VT_R5_eCoaQueryBuilder();
        queryBuilder.addField('testField');
        queryBuilder.toJSON();
    }

    @IsTest
    private static void TypeDbFilterNumberRangeTest() {
        VT_R5_eCoaQueryBuilder queryBuilder = new VT_R5_eCoaQueryBuilder();
        VT_R5_eCoaQueryBuilder.TypeDbFilterNumberRange fliterNumber =
                new VT_R5_eCoaQueryBuilder.TypeDbFilterNumberRange('testField');
        queryBuilder.addFilter(fliterNumber);
        fliterNumber.setMin(1);
        fliterNumber.setMinInclusive(true);
        fliterNumber.setMax(2);
        fliterNumber.setMaxInclusive(true);
        fliterNumber.setInvert(true);
    }

    @IsTest
    private static void typeDbSortTest() {
        VT_R5_eCoaQueryBuilder.TypeDbSort typeDbSort =
                new VT_R5_eCoaQueryBuilder.TypeDbSort('testField');
        typeDbSort.setNullsFirst(true);
    }

    @IsTest
    private static void TypeDbFilterNumberIncludesTest() {
        List<Long>longList = new List<Long>{1, 2, 3};
        VT_R5_eCoaQueryBuilder.TypeDbFilterNumberIncludes typeDbFilterNumberIncludes =
                new VT_R5_eCoaQueryBuilder.TypeDbFilterNumberIncludes
                        ('testField', longList);
        typeDbFilterNumberIncludes.setInvert(true);
    }

    @IsTest
    private static void TypeDbFilterStringIncludesTest() {
        List<String>stringList = new List<String>{
                'value1', 'value2'
        };
        VT_R5_eCoaQueryBuilder.TypeDbFilterStringIncludes typeDbFilterStringIncludes =
                new VT_R5_eCoaQueryBuilder.TypeDbFilterStringIncludes
                        ('testField', stringList);
        typeDbFilterStringIncludes.setIgnoreCase(true);
        typeDbFilterStringIncludes.setInvert(true);
    }

    @IsTest
    private static void TypeDbFilterStringSearchTest() {
        VT_R5_eCoaQueryBuilder.TypeDbFilterStringSearch typeDbFilterStringSearch =
                new VT_R5_eCoaQueryBuilder.TypeDbFilterStringSearch
                        ('testField', 'value');
        typeDbFilterStringSearch.setSubstringOption
                (VT_R5_eCoaQueryBuilder.SubstringOption.startsWith);
        typeDbFilterStringSearch.setIgnoreCase(true);
        typeDbFilterStringSearch.setInvert(true);
    }

    @IsTest
    private static void TypeDbFilterBooleanTest() {
        VT_R5_eCoaQueryBuilder.TypeDbFilterBoolean typeDbFilterBoolean =
                new VT_R5_eCoaQueryBuilder.TypeDbFilterBoolean
                        ('testField', true);
    }

    @IsTest
    private static void TypeDbFilterTimeNamedRangeTest() {
        VT_R5_eCoaQueryBuilder.TypeDbFilterTimeNamedRange typeDbFilterTimeNamedRange =
                new VT_R5_eCoaQueryBuilder.TypeDbFilterTimeNamedRange
                        ('testField', VT_R5_eCoaQueryBuilder.TimeRange.lastYear, 'US');
    }

    @IsTest
    private static void TypeDbFilterTimeDayRangeTest() {
        VT_R5_eCoaQueryBuilder.TypeDbFilterTimeDayRange typeDbFilterTimeDayRange =
                new VT_R5_eCoaQueryBuilder.TypeDbFilterTimeDayRange
                        ('testField', 'US');
        typeDbFilterTimeDayRange.setMaxDaysAgo(2);
        typeDbFilterTimeDayRange.setMinDaysAgo(1);
    }

}