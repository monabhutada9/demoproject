/**
 * Created by Alexander Komarov on 20.05.2019.
 */

@RestResource(UrlMapping='/Patient/TrainingMaterials/*')
global with sharing class VT_R3_RestPatientTrainingMaterials {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();

    @HttpGet
    global static String getTrainingMaterials() {

        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        String temp = request.requestURI.substringAfterLast('/TrainingMaterials/');

        if (String.isBlank(temp)) {
            try {
                List<Knowledge__kav> tempRes = VT_D1_TrainingMaterialsControllerRemote.getTrainingMaterials(false, null);
                List<TrainingMaterial> result = new List<VT_R3_RestPatientTrainingMaterials.TrainingMaterial>();
                for (Knowledge__kav k : tempRes) {
                    result.add(new TrainingMaterial(
                            k.Id,
                            k.Summary,
                            k.Title
                    ));
                }
                cr.buildResponse(result);
                return JSON.serialize(cr,true);
            } catch (Exception e) {
                System.debug('EXCEPTION ### ' + e);
                response.statusCode = 500;
                cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName());
                return JSON.serialize(cr,true);
            }

        } else {
            if (!helper.isIdCorrectType(temp, 'Knowledge__kav')) {
                response.statusCode = 400;
                cr.buildResponse(helper.forAnswerForIncorrectInput());
                return JSON.serialize(cr,true);
            }
            try {
                Id kavId = temp;
                List<Knowledge__kav> tempRes = VT_D1_TrainingMaterialsControllerRemote.getTrainingMaterials(false, null);
                List<SingleTrainingMaterial> result = new List<SingleTrainingMaterial>();
                for (Knowledge__kav k : tempRes) {
                    if(kavId.equals(k.Id)) {
                        result.add(new SingleTrainingMaterial(
                                k.Id,
                                k.Summary,
                                k.Title,
                                k.VTD1_Content__c
                        ));
                    }
                }
                cr.buildResponse(result);
                return JSON.serialize(cr,true);
            } catch (Exception e) {
                response.statusCode = 500;
                cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName());
                return JSON.serialize(cr,true);
            }
        }
    }

    private class SingleTrainingMaterial {
        public String id;
        public String summary;
        public String title;
        public String content;

        public SingleTrainingMaterial(String i, String s, String t, String c) {
            this.id = i;
            this.summary = s;
            this.title = t;
            this.content = c;
        }
    }

    private class TrainingMaterial {
        public String id;
        public String summary;
        public String title;

        public TrainingMaterial(String i, String s, String t) {
            this.id = i;
            this.summary = s;
            this.title = t;
        }
    }
}