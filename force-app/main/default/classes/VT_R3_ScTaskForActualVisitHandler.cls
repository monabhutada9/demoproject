public without sharing class VT_R3_ScTaskForActualVisitHandler extends Handler {

    public override void onAfterUpdate(Handler.TriggerContext context) {
        createCsTask(context);
    }

    public override void onAfterInsert(Handler.TriggerContext context) {
        createCsTask(context);
    }

    public static void createCsTask(Handler.TriggerContext context) {
        List<Id> actualVisitIds = new List<Id>();
        List<String> taskUniqueCodes = new List<String>();
        for (VTD1_Actual_Visit__c actualVisit : (List<VTD1_Actual_Visit__c>) context.newList) {
            if (isRequireNotification(actualVisit, context)) {
                actualVisitIds.add(actualVisit.Id);
                taskUniqueCodes.add('T529');
            }
        }

        if (!actualVisitIds.isEmpty() && !taskUniqueCodes.isEmpty()) {
            VT_D2_TNCatalogTasks.generateTasks(taskUniqueCodes, actualVisitIds, new Map <String, String>(), null);
        }
    }

    private static Boolean isRequireNotification(VTD1_Actual_Visit__c actualVisit, Handler.TriggerContext context) {
        if (!isPatientRemovedFlagChangedToTrue(actualVisit, context)) {
            return false;
        }

        return ((actualVisit.VTD2_Common_Visit_Type__c == 'Study Team'
                || actualVisit.Sub_Type__c == 'Home Health Nurse') && actualVisit.VTR2_Independent_Rater_Flag__c)
                || actualVisit.Sub_Type__c == 'Home Health Nurse'
                || actualVisit.Sub_Type__c == 'Phlebotomist'
                || actualVisit.VTD2_Common_Visit_Type__c == 'Health Care Professional (HCP)'
                || actualVisit.VTD2_Common_Visit_Type__c == 'Health Care Professional'
                || actualVisit.Sub_Type__c == 'PSC';
    }

    private static Boolean isPatientRemovedFlagChangedToTrue(VTD1_Actual_Visit__c actualVisit, Handler.TriggerContext context) {
        if (isInsertContext(context)) {
            return actualVisit.VTD1_Patient_Removed_Flag__c;
        }

        VTD1_Actual_Visit__c oldActualVisitState = getOldActualVisitState(actualVisit, context);
        return actualVisit.VTD1_Patient_Removed_Flag__c && !oldActualVisitState.VTD1_Patient_Removed_Flag__c;
    }

    private static Boolean isInsertContext(Handler.TriggerContext context) {
        return context.oldMap.isEmpty();
    }

    private static VTD1_Actual_Visit__c getOldActualVisitState(VTD1_Actual_Visit__c actualVisit, Handler.TriggerContext context) {
        return ((Map<Id, VTD1_Actual_Visit__c>) context.oldMap).get(actualVisit.Id);
    }
}