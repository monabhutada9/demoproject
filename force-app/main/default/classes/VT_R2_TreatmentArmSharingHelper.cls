public without sharing class VT_R2_TreatmentArmSharingHelper {
    public static final Map<String, Integer> ACCESS_PRECEDENCE = new Map<String, Integer> {
        'None' => 0,
        'Read' => 1,
        'Edit' => 2
    };

    private Id studyId;
    private List<User> studyMembers = new List<User>();
//    private Map<Id, List<HealthCloudGA__CarePlanTemplate__Share>> existingSharesByUserId = new Map<Id, List<HealthCloudGA__CarePlanTemplate__Share>>();
    private Map<Id, VTR2_Treatment_Arm_Sharing_Configuration__c> existingConfigsByUserId = new Map<Id, VTR2_Treatment_Arm_Sharing_Configuration__c>();
    private Map<Id, VT_R2_TreatmentArmSharingController.ShareConfigItem> shareConfigItems = new Map<Id, VT_R2_TreatmentArmSharingController.ShareConfigItem>(); // mapped by user ID so we can check against existing shares and see if we missed anyone

    public List<VT_R2_TreatmentArmSharingController.ShareConfigItem> getConfigs(Id studyId) {
        this.studyId = studyId;

        getUsersFromStudy();
        getExistingConfigsByUserId();
        getShareConfigItems();
        List<VT_R2_TreatmentArmSharingController.ShareConfigItem> items = this.shareConfigItems.values();
        items.sort();
        return items;
    }

    private void getUsersFromStudy() {
        for (Study_Team_Member__c item : [
            SELECT Id, User__r.Id, User__r.Profile.Name, User__r.Name
            FROM Study_Team_Member__c
            WHERE Study__c = :studyId AND User__r.IsActive = TRUE
        ]) {
            this.studyMembers.add(item.User__r);
        }
    }

    private void getExistingConfigsByUserId() {
        for (VTR2_Treatment_Arm_Sharing_Configuration__c item : [
            SELECT Id, VTR2_User__c, VTR2_Profile_Name__c, VTR2_Access_Level__c, VTR2_User__r.Name,
                VTR2_User__r.Id, VTR2_User__r.Profile.Name
            FROM VTR2_Treatment_Arm_Sharing_Configuration__c
            WHERE VTR2_Study__c = :this.studyId
        ]) {
            this.existingConfigsByUserId.put(item.VTR2_User__c, item);
        }
    }

    private void getShareConfigItems() {
        for (User u : this.studyMembers) {
            VT_R2_TreatmentArmSharingController.ShareConfigItem shareConfigItem = new VT_R2_TreatmentArmSharingController.ShareConfigItem();

            shareConfigItem.userId = u.Id;
            shareConfigItem.userName = u.Name;
            shareConfigItem.profileName = u.Profile.Name;
			if (u.Profile.Name == 'Primary Investigator') {
				shareConfigItem.disableEdit = true;
			}

            shareConfigItem.config = this.existingConfigsByUserId.containsKey(u.Id) ?
                this.existingConfigsByUserId.get(u.Id) :
                new VTR2_Treatment_Arm_Sharing_Configuration__c(
                    VTR2_User__c = u.Id,
                    VTR2_Study__c = this.studyId,
                    VTR2_Access_Level__c = 'None'
                );
            shareConfigItem.accessLevel = shareConfigItem.config.VTR2_Access_Level__c;

            this.shareConfigItems.put(u.Id, shareConfigItem);
        }
    }
}