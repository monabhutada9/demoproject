/**
* @author: Carl Judge
* @date: 30-Jan-19
* @description: Test for CustomThemeLayoutControllerTest class
**/

@IsTest
public class CustomThemeLayoutControllerTest {

    @IsTest
    private static void doTest() {

        VTD1_NotificationC__c notificationC = new VTD1_NotificationC__c(
            Message__c = 'test',
            VTD1_Receivers__c = UserInfo.getUserId()
        );
        insert notificationC;
        insert new VTR4_HerokuNotificationService__c(
                VTR4_AppUrl__c = 'test_url',
                VTR4_AesKey__c = JSON.serialize(Crypto.generateAesKey(256)).replaceAll('"', ''),
                VTR4_ApiKey__c = 'test_api_key'
        );
        Id accountId = new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor')
                .persist()
                .Id;
        User testUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t()
                        .setFirstName('Patient')
                        .setLastName('Patient')
                        .setAccountId(accountId))
                .setProfile(VT_D1_ConstantsHelper.PATIENT_PROFILE_NAME)
                .persist();

        
        CustomThemeLayoutController.getUserMetricsData();
        CustomThemeLayoutController.getCurrentVideoId();
        CustomThemeLayoutController.getSessionIdUserId();
        CustomThemeLayoutController.translateNotificationMessage(notificationC.Id);
        CustomThemeLayoutController.translateNotificationMessage(null);
        CustomThemeLayoutController.getPatientCaregiverCase(); 
        CustomThemeLayoutController.getWebSocketUrl();
        CustomThemeLayoutController.getUserMetricsData();
        System.runAs(testUser) {
            CustomThemeLayoutController.getPatientCaregiverCase();
        }
        CustomThemeLayoutController.UserMetricsData umd = new CustomThemeLayoutController.UserMetricsData();
        umd.city = 'test';
        umd.country = 'test';

    }
}