/**
 * Created by Michael Salamakha on 20.02.2019.
 */
public without sharing class VT_R2_RemoveVisit {
    public static void deleteRelations(List<Visit_Member__c> records){
        if (records != null && ! records.isEmpty()) {
            Set <Id> usersIds                  = new Set<Id>();
            Set <Id> visitMemberIds            = new Set<Id>();
            Set <Id> recordsActualVisitIds     = new Set<Id>();
            Set <String> unique                = new Set<String>();
            List<EventRelation> eventRelations = new List<EventRelation>();
            for (Visit_Member__c vsm : records) {
                visitMemberIds.add(vsm.id);
                if (vsm.VTD1_Participant_User__c != null) {
                    usersIds.add(vsm.VTD1_Participant_User__c);
                    recordsActualVisitIds.add(vsm.VTD1_Actual_Visit__c);
                    unique.add(String.valueOf(vsm.VTD1_Participant_User__c) + String.valueOf(vsm.VTD1_Actual_Visit__c));
                }
            }
            System.debug('recordsActualVisitIds = ' + recordsActualVisitIds);
            if (!recordsActualVisitIds.isEmpty() && !usersIds.isEmpty()) {
                // delete visit member from event participants
                List<EventRelation> deleteEventRelations = new List<EventRelation>();
                eventRelations = [select Id, RelationId, Event.WhatId, Event.VTD1_Actual_Visit__c, Event.OwnerId
                from EventRelation
                where Event.WhatId in:recordsActualVisitIds
                and RelationId in:usersIds and IsInvitee = true];
                System.debug('recordsActualVisitIds ' + recordsActualVisitIds);
                System.debug('usersIds ' + usersIds);
                System.debug('unique ' + unique);
                for (EventRelation er: eventRelations) {
                    System.debug('er.Event.OwnerId'+  er.RelationId + er.Event.WhatId);
                    if (unique.contains(String.valueOf(er.RelationId) + String.valueOf(er.Event.WhatId))) {
                        deleteEventRelations.add(er);
                    }
                }
                System.debug('!!!!!!!~~~~~~~~~~~~~~~~~~~~~~~~~   deletelist:::: '+ deleteEventRelations);
                if (!deleteEventRelations.isEmpty())                 delete deleteEventRelations;
                // change event owner if related deleted visit member is the owner
                List<Event> eventList = [select id, WhatId, OwnerId, VTD1_IsBufferEvent__c
                from Event
                where WhatId in:recordsActualVisitIds and IsChild=false
                and OwnerId in:usersIds];
                System.debug('eventList = ' + eventList);
                if (!eventList.isEmpty()) {
                    List<Event> buffersToDelete = new List<Event>();
                    Set <Id> eventIds = new Set<Id>();
                    Map <Id, Id> changeOwnerMap = new Map<Id, Id>();
                    Map <String, Id> eventRelMap = new Map<String, Id>();
                    for (Event evt : eventList) {
                        if (unique.contains(String.valueOf(evt.OwnerId) + String.valueOf(evt.WhatId))) {
                            if (evt.VTD1_IsBufferEvent__c) {
                                buffersToDelete.add(evt);
                            } else {
                                eventIds.add(evt.id);
                            }
                        }
                    }
                    // find at least one participant from EventRelation and assign it as an event owner
                    for (AggregateResult ar : [select MAX(Id) Id, MAX(RelationId) RelationId, EventId
                                                from EventRelation
                                                where EventId in:eventIds and IsInvitee = true
                                                group by EventId]) {
                        changeOwnerMap.put(String.valueOf(ar.get('EventId')), String.valueOf(ar.get('RelationId')));
                        eventRelMap.put(String.valueOf(ar.get('EventId')) + String.valueOf(ar.get('RelationId')), String.valueOf(ar.get('Id')));
                    }
                    if (!changeOwnerMap.isEmpty()) {
                        List<EventRelation> evRelToDelete = new List<EventRelation>();
                        List <Event> ownerChangedRecords = new List<Event>();
                        for (Event evt : eventList) {
                            Id newOwnereId = changeOwnerMap.get(evt.id);
                            if (newOwnereId != null) {
                                evt.OwnerId = newOwnereId;
                                ownerChangedRecords.add(evt);
                                evRelToDelete.add(new EventRelation(Id = eventRelMap.get(String.valueOf(evt.id) + String.valueOf(newOwnereId))));
                            }
                        }
                        if(!evRelToDelete.isEmpty() && !ownerChangedRecords.isEmpty()) {
                            delete evRelToDelete;
                            update ownerChangedRecords;
                        }
                    }
                    if (!buffersToDelete.isEmpty()) delete buffersToDelete;
                }
            }
            // delete conference members
            if(!visitMemberIds.isEmpty()) {
                List<VTD1_Conference_Member__c> deleteConferenceMemberList = new List<VTD1_Conference_Member__c>();
                deleteConferenceMemberList = [select id from VTD1_Conference_Member__c
                where VTD1_Visit_Member__c in:visitMemberIds];
                System.debug('!!!!!!!~~~~~~~~~~~~~~~~~~~~~~~~~   delelteList ConferenceMmeber::::: '+deleteConferenceMemberList);
                if (!deleteConferenceMemberList.isEmpty()) delete deleteConferenceMemberList;
            }
        }
    }
    public static void handleRemovedVisitMembers(Map <Id, Visit_Member__c> membersMap){
        System.debug('handleRemovedVisitMembers = ' + membersMap);
        List <Visit_Member__c> visitMembers = [select Id, Visit_Member_Name__c,
                                                VTD1_Participant_User__c,
                                                VTD1_Participant_User__r.FirstName,
                                                VTD1_Participant_User__r.LastName,
                                                VTD1_Is_internal__c,
                                                VTD1_Member_Type__c,
                                                VTD1_Participant_User__r.Profile.Name,
                                                VTD1_Actual_Visit__c,
                                                VTD1_External_Participant_Email__c,
                                                VTD1_Actual_Visit__r.VTD1_Status__c
        from Visit_Member__c where Id in : membersMap.keySet()];
        List <VT_R3_EmailsSender.ParamsHolder> emaiParamsHolderList = new List <VT_R3_EmailsSender.ParamsHolder>();
        for (Visit_Member__c member : visitMembers) {
            String templateName;
			System.debug('visitMember *** ' + member);
            if (member.VTD1_Actual_Visit__r.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED ||
                    member.VTD1_Actual_Visit__r.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED) {
               if (member.VTD1_Participant_User__c == null){
                   templateName = 'VTR4_Visit_Member_Removed_External';
//                   templateName = 'VTR2_Visit_Member_Removed_External';
                } else {
//                   templateName = 'VTR2_Visit_Member_Removed';
                   templateName = 'VTR4_Visit_Member_Removed_PI_SCR_PT';
                }
                VT_R3_EmailsSender.ParamsHolder phHolder =new VT_R3_EmailsSender.ParamsHolder(
                        member.VTD1_Participant_User__c,
                        member.VTD1_Actual_Visit__c,
                        templateName,
                        member.VTD1_External_Participant_Email__c,
                        true
                );
                phHolder.receiver = new VT_R2_VisitMemberNotificationController().getRecipientName (member);
                emaiParamsHolderList.add(phHolder);
            }
        }
        if (!emaiParamsHolderList.isEmpty() && !Test.isRunningTest()) {
            VT_R3_EmailsSender.sendEmails(emaiParamsHolderList); //Messaging.sendEmail(mails);
        }
        List <Visit_Member__c> communityVisitMembers = new List<Visit_Member__c>();
        List <Visit_Member__c> internalVisitMembers = new List<Visit_Member__c>();
        Set <String> communityProfileNames = new Set<String>{
                VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME,
                                                        VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME,
                                                        VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME,
                                                        VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME};
        for (Visit_Member__c member : visitMembers) {
            if (member.VTD1_Is_internal__c && communityProfileNames.contains(member.VTD1_Member_Type__c)) {
                communityVisitMembers.add(member);
            } else if (member.VTD1_Is_internal__c && !communityProfileNames.contains(member.VTD1_Member_Type__c)) {
                internalVisitMembers.add(member);
            }
        }
        System.debug('communityVisitMembers = ' + communityVisitMembers);
        if (!communityVisitMembers.isEmpty()) {
            List <String> tnCodes = new List<String>();
            List <Id> sourceIds = new List<Id>();
            for (Visit_Member__c member : communityVisitMembers) {
                tnCodes.add('N025');
                sourceIds.add(member.Id);
            }
            System.debug('generateNotifications ' + sourceIds);
            VT_D2_TNCatalogNotifications.generateNotifications(tnCodes, sourceIds, new Map <String, String>());
        }
        if (!internalVisitMembers.isEmpty()) {
            List<String> tnCodes = new List<String>();
            List<Id> sourceIds = new List<Id>();
        for (Visit_Member__c member : internalVisitMembers) {
                tnCodes.add('N550');
                sourceIds.add(member.Id);
            }
            VT_D2_TNCatalogNotifications.generateNotifications(tnCodes, sourceIds, new Map<String, String>());
        }
    }
    /**
     * RECOMMITTING
     */
}