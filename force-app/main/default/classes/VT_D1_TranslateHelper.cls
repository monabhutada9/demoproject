/**
 * Created by Leonid Bartenev
 */

public without sharing class VT_D1_TranslateHelper {

    private static final List<String> DATETIME_STRINGS = new List<String>{
            'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December',
            'Jan', 'Feb', 'Mar', 'Apr', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
            'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday',
            'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat',
            'AM', 'PM'
    };
    private static final String DATETIME_REGEXP =  '(?i)('+ String.join(DATETIME_STRINGS, '|') +')+';

    private static List<SObject> accumulatedRecords = new List<SObject>();
    public static String language = UserInfo.getLanguage();
    //private static List<VTD1_Translation__c> translationsBulk;
    private static Map<String, List<VTD1_Translation__c>> translationsBulkMap = new Map<String, List<VTD1_Translation__c>>();



    private static Map <String, Map <String, String>> transactionCache = new Map <String, Map<String, String>>();
    private static List <VTD1_General_Settings__mdt> settings;

    private static String CUSTOM_LABELS_CACHE_PREFIX = 'Lang';
    private static Integer CUSTOM_LABELS_CACHE_TTL = 86400;


    private static String sessionIdForGuest;

    private static String getSessionIdForGuest() {
        if (sessionIdForGuest == null) {
            sessionIdForGuest = VT_D1_HelperClass.getSessionIdForGuest();
        }
        return sessionIdForGuest;
    }


    private static void initCacheSettings() {
        if (settings == null) {
            settings = [
                    SELECT VTR5_CustomLabelsCachePrefix__c, VTR5_CustomLabelsCacheTTL__c
                    FROM VTD1_General_Settings__mdt
                    LIMIT 1
            ];
            if (!settings.isEmpty()) {
                if (settings[0].VTR5_CustomLabelsCachePrefix__c != null) CUSTOM_LABELS_CACHE_PREFIX = settings[0].VTR5_CustomLabelsCachePrefix__c;
                if (settings[0].VTR5_CustomLabelsCacheTTL__c != null) CUSTOM_LABELS_CACHE_TTL = settings[0].VTR5_CustomLabelsCacheTTL__c.intValue();
            }
        }


    }

    public static void addToTranslate(List<SObject> inputRecords) {
        accumulatedRecords.addAll(inputRecords);
    }

    public static void addToTranslate(SObject inputRecord) {
        accumulatedRecords.add(inputRecord);
    }

    public static void translateAll() {
        translate(accumulatedRecords);
        accumulatedRecords = new List<SObject>();
    }

    public static void translate(SObject so){
        translate(new List<SObject>{so});
    }

    public static void translate(List<SObject> inputRecords){
        if(inputRecords == null || inputRecords.isEmpty()) return;
        List<SObject> recordsList = new List<SObject>();
        for(SObject sobj : inputRecords) recordsList.addAll(getAllSObjects(sobj));

        List<Id> recordIds = new List<Id>();
        for(SObject so : recordsList) {
            if(so.Id != null) recordIds.add(so.Id);
        }

        List<VTD1_Translation__c> translationsBulk = translationsBulkMap.get(language);

        if (translationsBulk == null) {
            String userProfileName = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
            if (VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME.equals(userProfileName)){
            translationsBulk = getTranslationsBulk(language);
            } else{
                translationsBulk = [
                        SELECT Id,
                                VTD1_Field_Name__c,
                                VTD1_Language__c,
                                VTD1_Object_Name__c,
                                VTD1_Record_Id__c,
                                VTD1_Value__c,
                                VTR2_Value__c
                        FROM VTD1_Translation__c
                        WHERE VTD1_Record_Id__c IN: recordIds AND VTD1_Language__c =: language
                ];
            }
            translationsBulkMap.put(language, translationsBulk);
        }

        /*
        not worked for multiple languages in single transaction
        if (translationsBulk == null) translationsBulk = getTranslationsBulk(language);
        */

        Map<Id, List<VTD1_Translation__c>> translationMap = new Map<Id, List<VTD1_Translation__c>>();
        for(VTD1_Translation__c translation : translationsBulk){
            List<VTD1_Translation__c> recordTranslations = translationMap.get(translation.VTD1_Record_Id__c);
            if(recordTranslations == null) recordTranslations = new List<VTD1_Translation__c>();
            recordTranslations.add(translation);
            translationMap.put(translation.VTD1_Record_Id__c, recordTranslations);
        }
        for(SObject so : recordsList){
            if(so.Id == null) {
                System.debug('ERROR: Can not translate record without record Id: ' + so);
                return;
            }
            List<VTD1_Translation__c> recordTranslations = translationMap.get(so.Id);
            if(recordTranslations == null) continue;
            for(VTD1_Translation__c translation : recordTranslations){
                try{
                    if (translation.VTR2_Value__c != null) {
                        so.put(translation.VTD1_Field_Name__c, translation.VTR2_Value__c);
                    }  else  {
                        so.put(translation.VTD1_Field_Name__c, translation.VTD1_Value__c);
                    }
                }catch (Exception e){
                    System.debug('ERROR: can not translate record: ' + so + ' Translation: ' + translation);
                    System.debug('ERROR: ' + e.getMessage() + '\n' + e.getStackTraceString());
                }
            }
        }
    }

    private static List<VTD1_Translation__c> getTranslationsBulk(String lang) {
        return [
                SELECT
                        Id,
                        VTD1_Field_Name__c,
                        VTD1_Language__c,
                        VTD1_Object_Name__c,
                        VTD1_Record_Id__c,
                        VTD1_Value__c,
                        VTR2_Value__c
                FROM VTD1_Translation__c
                WHERE VTD1_Language__c =: lang
                AND
                (
                        (VTR4_Study__c = :findCurrentStudyId() AND VTR4_Study__c != NULL)
                        OR
                        (VTR4_User__c = :UserInfo.getUserId() AND VTR4_User__c != NULL)
                        OR
                        (VTR4_Study__c = NULL AND VTR4_User__c = NULL AND VTD1_Object_Name__c = 'User') //for STM users who are the same for multiple studies
                        OR
                        (VTR4_Study__c = NULL AND VTR4_User__c = NULL AND VTD1_Object_Name__c = 'Profile') //for Profile translations which are the same for multiple studies
                )
        ];
    }

    public static void translate(List<SObject> inputRecords, String lang) {
        language = lang;
        translate(inputRecords);
    }

    // translates datetime strings by using custom labels
    public static String translate(String input){
        if (input != null) {
            input = translate(input, language);
        }
        return input;
    }

    public static String translate(String input, String lang) {
        if (lang != 'en_US') {
            //String reg = '(?i)('+ String.join(new List<String>(DATETIME_STRINGS), '|') +')+';
            Matcher mat = Pattern.compile(DATETIME_REGEXP).matcher(input);
            while (mat.find()) {
                String key = mat.group(1);
                if (key != null) {
                    String translatedLabel = getLabelValue('VTR3_'+key, lang);
                    if (translatedLabel != null) {
                        input = input.replace(key, translatedLabel);
                    }
                }
            }
        }
        return input;
    }
    //return parent sObject with all nested sObjects
    public static List<SObject> getAllSObjects(SObject sobj){
        List<SObject> sObjects = new List<SObject>();
        Map<String, Object> populatedFields = sobj.getPopulatedFieldsAsMap();
        for(String fieldName : populatedFields.keySet()){
            if(fieldName.endsWithIgnoreCase('__c') || fieldName.endsWithIgnoreCase('Id')) continue;
            try{
                SObject nestedSObj = sobj.getSObject(fieldName);
                sObjects.addAll(getAllSObjects(nestedSObj));
            }catch (Exception e){
            }
        }
        sObjects.add(sobj);
        return sObjects;
    }


    @Future(Callout=true)
    public static void translateNotificationsFuture(Set<Id> notificationIds){
        List<VTD1_NotificationC__c> notifications = [
                SELECT Id, OwnerId,
                        Title__c,
                        Message__c,
                        VTD2_Title_Parameters__c,
                        VTD1_Parameters__c, VTR5_Email_Body__c, VTR5_Email_Body_Parameters__c, VTR5_Email_Subject__c, VTR5_Email_Subject_Parameters__c 
                FROM VTD1_NotificationC__c
                WHERE Id IN: notificationIds
        ];
        translate(notifications, true);
        update notifications;
    }

    @Future(Callout=true)
    public static void translateAndInsertNotifications(String notificationsJSON){
        List<VTD1_NotificationC__c> notifications = (List<VTD1_NotificationC__c>) JSON.deserialize(notificationsJSON, List<VTD1_NotificationC__c>.class);
        translate(notifications, true);
        insert notifications;
    }

    public static void translate(List<VTD1_NotificationC__c> notifications){
        translate(notifications, false);
    }
    
    //Adding the method for SH-9956 (This is used for the VF Component - StudyNotificationComponent)
    public static void translate(VTD1_NotificationC__c notifications,Boolean useOwnerLanguage){
        
        translate(new List<VTD1_NotificationC__c>{notifications}, useOwnerLanguage);
    }

    public static void translate(List<VTD1_NotificationC__c> notifications, Boolean useOwnerLanguage){
        //define pairs: field with label - field with parameters:        
        Map<String, String> fieldsMap = new Map<String, String> {
                VTD1_NotificationC__c.Title__c + '' => VTD1_NotificationC__c.VTD2_Title_Parameters__c + '',
                VTD1_NotificationC__c.Message__c + '' => VTD1_NotificationC__c.VTD1_Parameters__c + ''
        };
        //translate labels:
        prepareLabelFields(notifications, fieldsMap.keySet(), useOwnerLanguage);
        //substitute params:
        substituteParameters(notifications, fieldsMap);
        // delete unused tags
        deleteUnusedTagsNotifications(notifications);
    }
    
    //SH-8206 Added Email fields. Created new method becuase existing translate method will get impact after addition of email fields.
    public static void translateAllfields(List<VTD1_NotificationC__c> notifications, Boolean useOwnerLanguage){
        Map<String, String> fieldsMap = new Map<String, String> {
                VTD1_NotificationC__c.Title__c + '' => VTD1_NotificationC__c.VTD2_Title_Parameters__c + '',
                VTD1_NotificationC__c.Message__c + '' => VTD1_NotificationC__c.VTD1_Parameters__c + '',
                VTD1_NotificationC__c.VTR5_Email_Body__c + '' => VTD1_NotificationC__c.VTR5_Email_Body_Parameters__c + '',
                VTD1_NotificationC__c.VTR5_Email_Subject__c + '' => VTD1_NotificationC__c.VTR5_Email_Subject_Parameters__c + ''
        };
        //translate labels:
        prepareLabelFields(notifications, fieldsMap.keySet(), useOwnerLanguage);
        //substitute params:
        substituteParameters(notifications, fieldsMap);
        // delete unused tags
        deleteUnusedTagsNotifications(notifications);
    }
    
    public static void translate(List<Task> tasksList){
        //define pairs: field with label - field with parameters:
        Map<String, String> fieldsMap = new Map<String, String> {
                Task.Subject + '' => Task.VTD2_Subject_Parameters__c + '',
                Task.Description + '' => Task.VTD2_Description_Parameters__c + ''
        };
        VT_D1_TranslateHelper.translate((List<SObject>) tasksList);
        //translate labels:
        prepareLabelFields(tasksList, fieldsMap.keySet());
        //substitute params:
        substituteParameters(tasksList, fieldsMap);
        // delete unused tags
        deleteUnusedTagsTasks(tasksList);
    }

     //SH-8206 Added Email fields. Created new method becuase existing translate method will get impact after addition of email fields.
    public static void translateAllfields(List<Task> tasksList){ 
        //define pairs: field with label - field with parameters:
        Map<String, String> fieldsMap = new Map<String, String> {
                Task.Subject + '' => Task.VTD2_Subject_Parameters__c + '',
                Task.Description + '' => Task.VTD2_Description_Parameters__c + '',
                Task.VTR5_Email_Body__c + '' => Task.VTR5_Email_Body_Parameters__c + '',
                Task.VTR5_Email_Subject__c + '' => Task.VTR5_Email_Subject_Parameters__c + ''
        };
        VT_D1_TranslateHelper.translate((List<SObject>) tasksList);
        //translate labels:
        prepareLabelFields(tasksList, fieldsMap.keySet());
        //substitute params:
        substituteParameters(tasksList, fieldsMap);
        // delete unused tags
        deleteUnusedTagsTasks(tasksList);
    }
    
    //private static Map<String, String> labelValuesMap = new Map<String, String>();

    public static String getLabelValue(String labelName){
        return getLabelValue(labelName,UserInfo.getLanguage()); /*'en_US'*/
    }

    public static String getLabelValue(String labelName, String language) {
        /*String labelKey = labelName + '*' + language;
        String translatedLabel = labelValuesMap.get(labelKey);
        if (translatedLabel == null) {
            translatedLabel = getTranslatedValue(labelName, language);
            labelValuesMap.put(labelKey, translatedLabel);
        }
        return translatedLabel;*/

        if (labelName.containsWhitespace()) {
            return labelName;
        }
        Map <String, String> labelsMap = getCachedLabels(new List <String> {labelName}, language, true);
        if (!labelsMap.isEmpty()) {
            return (String)labelsMap.values().iterator().next();
        } else {
            return labelName;
        }

    }

    public static String getTranslatedValue(String labelName, String language) {
        try {
            PageReference pageRef;
            if (UserInfo.getUserType() == 'Guest') {  //SH-7449
                pageRef = new PageReference(VisitRequestConfirmSite__c.getInstance().TranslateLabelURL__c + '?labelName=' + labelName + '&lang=' + language);
            } else {
                pageRef = new PageReference('/apex/VT_D2_CustomLabelRenderPage?labelName=' + labelName + '&lang=' + language);
            }
            String responseString = pageRef.getContent().toString().trim();
            if (responseString.startsWith('<!DOCTYPE html PUBLIC')) {  // wrong response (i.e., from community builder)
                return labelName;
            }
            return responseString;
        } catch (Exception e) {
            System.debug('### label exception '+e.getMessage());
            return labelName;
        }
    }

    private static List <Object> queryByHttp(String query) {
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');

        req.setHeader('Authorization', 'Bearer ' + getSessionIdForGuest());

        req.setEndpoint(Url.getOrgDomainUrl().toExternalForm() + '/services/data/v49.0/tooling/query/' + query);
        Http http = new Http();
        if (!Test.isRunningTest()) {
            HttpResponse res = http.send(req);
            try {
                Map <String, Object> resSer = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
                List <Object> resSerRecords = (List <Object>) resSer.get('records');
                return resSerRecords;
            } catch (Exception e) {
                System.debug('queryByHttp error ' + e.getStackTraceString());
                return new List <Object>();
            }
        } else {

            return new List <Object>{
                    new Map <String, String> {'Id' => 'fakeId', 'Name' => 'TestLabelName', 'Value' => 'TestLabelContent'}};

        }
    }

    public static Map <String, String> getExternalStringLocalizations(List<String> labelNames, String language) {
        Map <String, String> result = new Map<String, String>();
        String whereClause = String.join(labelNames, '\',\'').replaceAll(' ', '+');
        String queryString = '?q=SELECT+Id,+Name,+Value,+Language+FROM+ExternalString+WHERE+Name+IN+(\'' + whereClause + '\')';
        List<Object> resSerRecords = queryByHttp(queryString);
        if (resSerRecords.isEmpty()) {
            return new Map <String, String>();
        }

        Map <String, String> nameToLabelIdMap = new Map<String, String>();
        List <String> clIds = new List<String>();

        for (Object obj : resSerRecords) {
            Map <String, Object> m = (Map <String, Object>)obj;
            String name = (String)m.get('Name');
            String value = (String)m.get('Value');

            String id = (String)m.get('Id');

            nameToLabelIdMap.put(name, id);
            result.put(name, value);
            clIds.add(id);
        }
        whereClause = String.join(clIds, '\',\'');
        queryString = '?q=SELECT+ExternalStringId,+Value,+Language+FROM+ExternalStringLocalization+WHERE+ExternalStringId+IN+(\'' + whereClause + '\')+AND+Language=\'' + language + '\'';
        resSerRecords = queryByHttp(queryString);


        Map <String, String> valueMap = new Map<String, String>();
        for (Object obj : resSerRecords) {
            Map <String, Object> m = (Map <String, Object>) obj;
            valueMap.put((String)m.get('ExternalStringId'), (String)m.get('Value'));
        }
        for (String name : nameToLabelIdMap.keySet()) {
            String id = nameToLabelIdMap.get(name);


            String value = valueMap.get(id);
            if (value != null) {
                result.put(name, value);
            }
        }
        return result;
    }


    public static Map <String, String> getCachedLabels (List <String> labelNames) {
        return getCachedLabels(labelNames, UserInfo.getLanguage(), false);
    }

    public static Map <String, String> getCachedLabels (Map <String, String> labelDefTranslationMap) {
        return getCachedLabels(labelDefTranslationMap, UserInfo.getLanguage(), false);
    }

    public static Map <String, String> getCachedLabels (Map <String, String> labelDefTranslationMap, String languageName) {
        return getCachedLabels(labelDefTranslationMap, languageName, false);
    }


    public static Map <String, String> getCachedLabels (List <String> labelNames, String languageName, Boolean queryByPageReference) {
        Map <String, String> labelDefTranslationMap = new Map<String, String>();
        for (String labelName : labelNames) {
            labelDefTranslationMap.put(labelName, null);
        }
        return getCachedLabels(labelDefTranslationMap, languageName, queryByPageReference);
    }

    public static Map <String, String> getCachedLabels (Map <String, String> labelDefTranslationMap, String languageName, Boolean queryByPageReference) {
        initCacheSettings();
        String reducedLanguageName = languageName.replaceAll('_', '');
        String platformCacheKey = CUSTOM_LABELS_CACHE_PREFIX + reducedLanguageName;
        Map <String, String> result = new Map<String, String>();
        List <String> labelsToQuery = new List<String>();
        Map <String, String> filteredLabelDefTranslationMap = new Map<String, String>();
        String userLanguageName = UserInfo.getLanguage();
        for (String labelName : labelDefTranslationMap.keySet()) {
            if (!transactionCache.containsKey(reducedLanguageName) || !transactionCache.get(reducedLanguageName).containsKey(labelName)) {
                String defaultLabelValue = labelDefTranslationMap.get(labelName);
                if (languageName == userLanguageName && defaultLabelValue != null) {
                    filteredLabelDefTranslationMap.put(labelName, defaultLabelValue);
                    result.put(labelName, defaultLabelValue);
                    continue;
                }
                Map <String, String> thisLanguageCache = (Map <String, String>) Cache.Org.get(platformCacheKey);
                if (thisLanguageCache != null) {
                    String value = thisLanguageCache.get(labelName);
                    if (value != null) {
                        result.put(labelName, value);
                        if (!transactionCache.containsKey(reducedLanguageName)) {
                            transactionCache.put(reducedLanguageName, new Map <String, String> {labelName => value});
                        } else {
                            transactionCache.get(reducedLanguageName).put(labelName, value);
                        }
                    } else {
                        labelsToQuery.add(labelName);
                    }
                } else {
                    labelsToQuery.add(labelName);
                }
            } else {
                result.put(labelName, transactionCache.get(reducedLanguageName).get(labelName));
            }
        }
        if (!labelsToQuery.isEmpty() || !filteredLabelDefTranslationMap.isEmpty()) {
            Map <String, String> queriedLabelsMap = new Map<String, String>();
            if (!labelsToQuery.isEmpty()) {
                if (queryByPageReference) {
                    queriedLabelsMap = new Map<String, String>();
                    String labelName = labelsToQuery[0];
                    queriedLabelsMap.put(labelName, getTranslatedValue(labelName, languageName));
                } else {
                    queriedLabelsMap = getExternalStringLocalizations(labelsToQuery, languageName);
                }
            }
            Map <String, String> languageTransactionCache = (Map <String, String>) transactionCache.get(reducedLanguageName);
            Map <String, String> languageOrgCache = (Map <String, String>) Cache.Org.get(platformCacheKey);
            result.putAll(queriedLabelsMap);
            if (languageOrgCache == null) {
                languageOrgCache = new Map<String, String>();
            }
            Integer prevCacheSize = languageOrgCache.size();
            if (languageTransactionCache == null) {
                languageTransactionCache = new Map<String, String>();
                transactionCache.put(reducedLanguageName, languageTransactionCache);
            }
            if (!queriedLabelsMap.isEmpty()) {
                languageOrgCache.putAll(queriedLabelsMap);
                languageTransactionCache.putAll(queriedLabelsMap);
            }
            if (!filteredLabelDefTranslationMap.isEmpty()) {
                languageOrgCache.putAll(filteredLabelDefTranslationMap);
                languageTransactionCache.putAll(filteredLabelDefTranslationMap);
            }
            if (languageOrgCache.size() != prevCacheSize) {
                Cache.Org.put(platformCacheKey, languageOrgCache, CUSTOM_LABELS_CACHE_TTL, Cache.Visibility.ALL, false);
            }
        }
        return result;
    }



    public static void prepareLabelFields(List<SObject> records, Set<String> fieldNames){
        prepareLabelFields(records, fieldNames, false);
    }

    public static void prepareLabelFields(List<SObject> records, Set<String> fieldNames, Boolean useOwnerLanguage){
        Map<Id, String> userLanguageMap = new Map<Id, String>();
        if(useOwnerLanguage){
            List<Id> useIds = new List<Id>();
            for(SObject sobj :  records) useIds.add((Id)sobj.get('OwnerId'));
            List<User> users = [SELECT Id, LanguageLocaleKey FROM User WHERE Id IN: useIds];
            for(User user : users) userLanguageMap.put(user.Id, user.LanguageLocaleKey);
        }

        for(SObject sobj : records){
            System.debug('sobj = ' + sobj);
            for(String fieldName : fieldNames){
                System.debug('fieldName = ' + fieldName);
                String labelName = (String)sobj.get(fieldName);
                if(String.isEmpty(labelName)) continue;
                String labelValue = getLabelValue(labelName, useOwnerLanguage ? userLanguageMap.get((Id)sobj.get('OwnerId')) : UserInfo.getLanguage());
                sobj.put(fieldName, labelValue);
            }
        }
    }

    public static void substituteParameters(List<SObject> records, Map<String, String> sentenceFieldParamsFieldMap){
        //retrieve param values:
        List<VT_D2_TranslateParameters.Parameter> params = new List<VT_D2_TranslateParameters.Parameter>();
        for(SObject sobj : records) {
            for (String sentenceFieldName : sentenceFieldParamsFieldMap.keySet()) {
                String paramsFieldName = sentenceFieldParamsFieldMap.get(sentenceFieldName);
                params.addAll(VT_D2_TranslateParameters.getParameters(sobj, sentenceFieldName, paramsFieldName));
            }
        }
        VT_D2_TranslateParameters.retrieveParamsAndUpdateTargetObjects(params);
    }

    public static void  deleteUnusedTagsTasks(List<Task> tasksList) {
        for (Task task : tasksList) {
            if(task.Subject != null){
                task.Subject = task.Subject.remove('#date');
            }
        }


    }

    public static void  deleteUnusedTagsNotifications(List<VTD1_NotificationC__c> notsList) {
        for (VTD1_NotificationC__c n : notsList) {
            if (n.Message__c != null) {
                n.Message__c = n.Message__c.remove('#date');
            }
        }
    }

    public static String translateTimeZone(String inputTimeZone, String lang) {
        String key = inputTimeZone.remove('/');
        return getLabelValue('VTR3_TZ_'+key, lang);
    }

    private static Id findCurrentStudyId() {
        return [
                SELECT
                        Contact.Account.VTD2_Patient_s_Contact__r.VTD1_Clinical_Study_Membership__r.VTD1_Study__c
                FROM User
                WHERE Id = :UserInfo.getUserId()
                LIMIT 1
        ][0].Contact.Account.VTD2_Patient_s_Contact__r.VTD1_Clinical_Study_Membership__r.VTD1_Study__c;
    }
}