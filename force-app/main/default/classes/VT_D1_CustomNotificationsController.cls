public with sharing class VT_D1_CustomNotificationsController {
    @AuraEnabled(Cacheable=true)
    public static String getSessionIdUserId() {
        Long offset = (Datetime.newInstance(Datetime.now().date(), Datetime.now().time()).getTime() -
                Datetime.newInstance(Datetime.now().dateGmt(), Datetime.now().timeGmt()).getTime()) / (60 * 60 * 1000);
        Long offsetMs = Datetime.newInstance(Datetime.now().date(), Datetime.now().time()).getTime() -
                Datetime.newInstance(Datetime.now().dateGmt(), Datetime.now().timeGmt()).getTime();
        return UserInfo.getSessionId() + ',' + UserInfo.getUserId() + ',' + Site.getPathPrefix() + ',' + offset + ',' + offsetMs;
    }
//    @AuraEnabled
//    public static Datetime notificationDateTime(Id notId) {
//        List <VTD1_NotificationC__c> nots = [
//                SELECT FORMAT(LastModifiedDate) LastModifiedDate
//                FROM VTD1_NotificationC__c
//                WHERE Id = :notId
//        ];
//        return nots[0].LastModifiedDate;
//    }
    @AuraEnabled
    public static List<VTD1_NotificationC__c> getNotificationsForCurrentUser() {
        Id userId = UserInfo.getUserId();
        try {
            Id caseId = null;
            User currentUser = [SELECT Profile.Name, Contact.VTD1_Clinical_Study_Membership__c FROM User WHERE Id = :userId];
            if (currentUser.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
                    || currentUser.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME) {
                caseId = currentUser.Contact.VTD1_Clinical_Study_Membership__c;
            }
            String queryString = 'SELECT\n' +
                    'Id,\n' +
                    'Title__c,\n' +
                    'VTD2_Title_Parameters__c,\n' +
                    'Link_to_related_event_or_object__c,\n' +
                    'CreatedDate,\n' +
                    'VTD2_New_Message_Added_DateTme__c,\n' +
                    'FORMAT(CreatedDate) CreatedDateF,\n' +
                    'FORMAT(VTD2_New_Message_Added_DateTme__c) VTD2_New_Message_Added_DateTme__cF,\n' +
                    'Message__c,\n' +
                    'VTD1_Read__c,\n' +
                    'Number_of_unread_messages__c,\n' +
                    'Type__c,\n' +
                    'HasDirectLink__c,\n' +
                    'isActualVisitLink__c,\n' +
                    'VTD1_Parameters__c,\n' +
                    'VTD1_CSM__c,\n' +
                    'VTD2_VisitID__c,\n' +
                    'VTR3_Notification_Type__c,\n' +
                    'VTR2_PatientNotificationId__c,\n' +
                    'VDT2_Unique_Code__c,\n' +
                    'VTR5_TitleValue__c,\n' +
                    'VTR4_CurrentPatientCase__c\n' +
                    'FROM VTD1_NotificationC__c\n' +
                    'WHERE OwnerId = :userId\n' +
                    (caseId != null ? 'AND VTR4_CurrentPatientCase__c = :caseId\n' : '') +
                    'AND VTR5_HideNotification__c = FALSE ' +
                    'AND Type__c != \'Televisit\'' +
                    'AND ((Type__c = \'Messages\' AND Number_of_unread_messages__c > 0) OR (Type__c != \'Messages\'))\n' +
                    'ORDER BY LastModifiedDate DESC\n' +
                    'LIMIT 150';
            List<VTD1_NotificationC__c> notifications = Database.query(queryString);
            //System.debug('Notifications: ' + notifications);

            Map<Id, NotificationWrapper> notificationWrappersMap = new Map<Id, NotificationWrapper>();
            Map<Id, Id> visitIdNotificationIdMap = new Map<Id, Id>();
            for (VTD1_NotificationC__c n : notifications) {
                if (!n.HasDirectLink__c) {
                    if (n.Type__c == 'Messages') {
                        n.Link_to_related_event_or_object__c = 'messages';
                    } else if ((n.Type__c == 'PCF' || n.VTD1_CSM__c != null) && n.VTD2_VisitID__c == null) {
                        n.Link_to_related_event_or_object__c = 'contact-form?pcfId=' + n.Link_to_related_event_or_object__c + '&caseId=' + n.VTD1_CSM__c;
                        /*} else if (n.isActualVisitLink__c) {
                            n.Link_to_related_event_or_object__c = 'calendar?id=' + n.Link_to_related_event_or_object__c;*/
                    } else {
                        n.Link_to_related_event_or_object__c = n.Link_to_related_event_or_object__c != null ? 'detail/' + n.Link_to_related_event_or_object__c : '';
                    }
                }
                if (n.Type__c == 'Study Team Visit' && n.VTD2_VisitID__c != null) {
                    visitIdNotificationIdMap.put(n.VTD2_VisitID__c, n.Id);
                }
                notificationWrappersMap.put(n.Id, new NotificationWrapper(n));
            }

            if (!visitIdNotificationIdMap.isEmpty()) {
                Map<Id, VTD1_Actual_Visit__c> actualVisitsMap = new Map<Id, VTD1_Actual_Visit__c> (
                [
                        SELECT Id
                        FROM VTD1_Actual_Visit__c
                        WHERE Id IN :visitIdNotificationIdMap.keySet()
                        AND VTD1_Status__c IN (:VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
                                :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED)
                ]);
                for (Id visitId : visitIdNotificationIdMap.keySet()) {
                    if (!actualVisitsMap.containsKey(visitId)) {
                        notificationWrappersMap.get(visitIdNotificationIdMap.get(visitId)).notification.Type__c = '';
                    }
                }
            }

            List<NotificationWrapper> notificationWrappers = notificationWrappersMap.values();
            notificationWrappers.sort(); // the custom sort is required as we can't sort messageNotificationC by the CreatedDate

            notifications.clear();
            for (NotificationWrapper nw : notificationWrappers) {
                notifications.add(nw.notification);
            }

            if (!notifications.isEmpty()) {
                VT_D1_TranslateHelper.translate(notifications);
            }

            return notifications;
        } catch (DmlException dmlE) {
            System.debug('getNotificationsForCurrentUser: ' + dmlE);
            throw dmlE;
        } catch (Exception e) {
            System.debug('getNotificationsForCurrentUser: ' + e.getMessage() + ' ' + e.getStackTraceString());
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
    public class NotificationWrapper implements Comparable {
        VTD1_NotificationC__c notification;
        Datetime wrapperDateTime;

        NotificationWrapper(VTD1_NotificationC__c n) {
            this.notification = n;
            this.wrapperDateTime = n.Type__c == 'Messages' ? n.VTD2_New_Message_Added_DateTme__c : n.CreatedDate;
        }

        public Integer compareTo(Object obj) {
            NotificationWrapper compareTo = (NotificationWrapper) obj;

            if (wrapperDateTime != compareTo.wrapperDateTime) {
                if (wrapperDateTime == null) {
                    return 1;
                }
                if (compareTo.wrapperDateTime == null) {
                    return -1;
                }
                return wrapperDateTime < compareTo.wrapperDateTime ? 1 : -1;
            }
            return 0;
        }
    }
//    private static List<VTD1_NotificationC__c> getNotificationsForPatientCGBasedOnCaseId(Id caseId) {
//        return [
//                SELECT Id, Title__c,
//                        VTD2_Title_Parameters__c,
//                        Link_to_related_event_or_object__c,
//                        CreatedDate,
//                        VTD2_New_Message_Added_DateTme__c,
//                        FORMAT(CreatedDate) CreatedDateF,
//                        FORMAT(VTD2_New_Message_Added_DateTme__c) VTD2_New_Message_Added_DateTme__cF,
//                        Message__c,
//                        VTD1_Read__c,
//                        Number_of_unread_messages__c,
//                        Type__c,
//                        HasDirectLink__c,
//                        isActualVisitLink__c,
//                        VTD1_Parameters__c,
//                        VTD1_CSM__c,
//                        VTD2_VisitID__c,
//                        VTR2_PatientNotificationId__c,
//                        VDT2_Unique_Code__c,
//                        VTR4_CurrentPatientCase__c,
//            			VTR5_TitleValue__c
//                FROM VTD1_NotificationC__c
//                WHERE VTR4_CurrentPatientCase__c = :caseId
//                AND ((Type__c = 'Messages' AND Number_of_unread_messages__c > 0) OR (Type__c != 'Messages'))
//                AND Type__c != 'Televisit'
//                AND (OwnerId = :UserInfo.getUserId() OR VTD1_Receivers__c = :UserInfo.getUserId())
//                ORDER BY CreatedDate DESC
//                LIMIT 150
//        ];
//    }

    @AuraEnabled
    public static void updateNotifications(List<Id> notIds) {
        VTD1_NotificationC__c[] nots = new List<VTD1_NotificationC__c>();
        for (String notId : notIds) {
            nots.add(new VTD1_NotificationC__c(
                    Id = notId,
                    VTD1_Read__c = true
            ));
        }
        update nots;
    }

    @AuraEnabled
    public static void hideNotifications(List<VTD1_NotificationC__c> notifications) {
        try {
            for (VTD1_NotificationC__c notification : notifications) {
                notification.VTR5_HideNotification__c = true;
            }
            update notifications;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
}