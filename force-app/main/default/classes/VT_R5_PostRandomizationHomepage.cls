/**
 * Created by Yuliya Yakushenkova on 10/13/2020.
 */

public class VT_R5_PostRandomizationHomepage implements VT_R5_RandomizationHomepage {

    public String userName;
    public String patientFirstName;
    public String patientLastName;
    public String version;

    public Tasks tasks;
    public List<Timeline> timelines = new List<Timeline>();
    public VT_R5_RandomizationHomepageService.StudyInformation studyInformation;
    public List<VT_R5_RandomizationHomepageService.Teammate> myStudyTeammates;
    public VT_R5_RandomizationHomepageService.NewsFeedSection newsFeed;

    private static Map<String, String> DESTINATIONS = new Map<String, String>{
            'full-profile' => 'Profile',
            'my-study-supplies' => 'StudySupplies',
            'calendar' => 'Calendar',
            'my-diary' => 'Ediary',
            'regulatory-binder' => 'RegulatoryBinder'
    };

    public VT_R5_PostRandomizationHomepage() {
        this.userName = UserInfo.getFirstName();
        this.version = 'PostRandomizedHomepage';
    }

    public VT_R5_RandomizationHomepage setTeammateSection(List<VT_R5_RandomizationHomepageService.Teammate> teammates) {
        this.myStudyTeammates = teammates;
        return this;
    }

    public VT_R5_RandomizationHomepage setStudyInformation(VT_R5_RandomizationHomepageService.StudyInformation studyInformation) {
        this.studyInformation = studyInformation;
        return this;
    }
    public VT_R5_RandomizationHomepage addNewsFeed(VT_R5_RandomizationHomepageService.NewsFeedSection news) {
        this.newsFeed = news;
        return this;
    }

    public VT_R5_RandomizationHomepage getPage(Case patientCase) {
        this.timelines = getTimeLines(patientCase.Id);
        this.tasks = getOpenClosedTasks(patientCase);
        this.patientFirstName = patientCase.VTD1_Patient__r.VTD1_First_Name__c;
        this.patientLastName = patientCase.VTD1_Patient__r.VTD1_Last_Name__c;
        return this;
    }


    //TODO: optimization needed, another service needed
    public List<Timeline> getTimeLines(Id caseId) {
        List<Timeline> timeLines = new List<Timeline>();
        Integer startInteger = System.Limits.getCpuTime();
        List<VT_D1_CommunityTimelineController.TimelineItem> timelineItems
                = VT_D1_CommunityTimelineController.getTimeLineItems(caseId);
        System.debug('>>>>>>>> ' + (System.Limits.getCpuTime() - startInteger));
        for (VT_D1_CommunityTimelineController.TimelineItem item : timelineItems) {
            timeLines.add(new Timeline(item));
        }
        return timeLines;
    }

    private Tasks getOpenClosedTasks(Case patientCase) {
        List<Task> tasks = VT_R5_PatientQueryService.getTasksByCase(patientCase);
        Map<String, List<TaskObj>> statusByTasks = new Map<String, List<TaskObj>>{
                'Open' => new List<TaskObj>(),
                'Closed' => new List<TaskObj>()
        };
        VT_D1_TranslateHelper.translate(tasks);
        Datetime previousSuccessUserLoginTime = VT_R3_RestHelper.getLastLoginDatetime();
        Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());
        List<String> newTaskIds = new List<String>();
        for (Task task : tasks) {
            TaskObj taskObj = new TaskObj(task, timeOffset);
            if (task.Status.equals('Open')) {
                if (task.CreatedDate > previousSuccessUserLoginTime) {
                    newTaskIds.add(task.Id);
                }
                statusByTasks.get('Open').add(taskObj);
            }
            if (task.Status.equals('Closed')) {
                statusByTasks.get('Closed').add(taskObj);
            }
        }
        return new Tasks(statusByTasks, newTaskIds);
    }

    private class TaskObj {
        public String id;
        public String title;
        public String type;
        public String status;
        public String category;
        public String eCOABanner;
        public Long dueDate;
        public String redirectValue;
        public Boolean caregiverClickable;
        public Boolean isRequestVisitRedirect;
        public String relevantObjId;

        public TaskObj(Task task, Integer offset) {
            this.id = task.Id;
            this.title = task.Subject;
            setDueDate(task.ActivityDate, task.VTR5_EndDate__c, offset);
            this.type = task.Type;
            this.caregiverClickable = task.VTD1_Caregiver_Clickable__c;
            setRedirectValue(task.VTD2_My_Task_List_Redirect__c);
            this.isRequestVisitRedirect = task.VTD2_isRequestVisitRedirect__c;
            this.status = task.Status;
            this.relevantObjId = task.WhatId;
            this.category = task.Category__c;
        }

        private void setDueDate(Date activityDate, Datetime dueDate, Integer timeOffset) {
            Long dueTime;
            if (activityDate != null) {
                Time emptyTime = Time.newInstance(0, 0, 0, 0);
                dueTime = Datetime.newInstance(activityDate, emptyTime).getTime();
            } else if (dueDate != null) {
                dueTime = dueDate.getTime();
            } else return;
            this.dueDate = dueTime + timeOffset;
        }

        private void setRedirectValue(String redirect) {
            if (redirect == null) {
                redirectValue = '';
                return;
            } else if (redirect.contains('banner')) {
                String baseRedirect = redirect;
                redirect = redirect.substring(0, redirect.indexOf('banner') - 1);
                setECOABanner(EncodingUtil.urlDecode(baseRedirect.substring(baseRedirect.indexOf('=') + 1), 'UTF-8'));
            }
            redirectValue = DESTINATIONS.containsKey(redirect) ? DESTINATIONS.get(redirect) : redirect;
        }

        private void setECOABanner(String banner) {
            this.eCOABanner = banner;
        }
    }

    private class Tasks {
        public List<TaskObj> openTasks;
        public List<TaskObj> closedTasks;
        public Integer openTasksCount;
        public List<String> newTasksIds;

        public Tasks(Map<String, List<TaskObj>> statusByTasks, List<String> ids) {
            openTasks = statusByTasks.get('Open');
            closedTasks = statusByTasks.get('Closed');
            openTasksCount = openTasks.size();
            newTasksIds = ids;
        }
    }

    private class Timeline {
        public String id;
        public String type;
        public String typeTranslated;
        public String title;
        public String modality;
        public String modalityTranslated;
        public String status;
        public String statusLabel;
        public String subStatusLabel;
        public String lineStatus;
        public Long dateAndTime;
        public Boolean noDetails;
        public String icon;
        public String description;
        public String visitType;
        public List<Long> itemDatesRange;

        public Timeline(VT_D1_CommunityTimelineController.TimelineItem tli) {
            id = tli.id;
            typeTranslated = tli.itemType;
            type = getItemType(tli.itemType);
            title = tli.label;
            modality = tli.modality;
            modalityTranslated = tli.translatedModality;
            statusLabel = tli.status;
            status = tli.statusValue;
            subStatusLabel = tli.subLabel;
            lineStatus = tli.lineStatus;
            dateAndTime = getItemDateTime(tli.itemDateTime);
            if (tli.itemType == Label.VTD2_VisitType) {
                itemDatesRange = getItemDatesRange(tli.visitWindowRaw);
                noDetails = tli.noDetails == null ? false : tli.noDetails;
            }
            icon = tli.icon;
            description = tli.description;
            visitType = tli.visitType;
        }

        private String getItemType(String value) {
            Map<String, String> timeLineItemTypes = new Map<String, String>{
                    Label.VTD2_VisitType => 'Visit',
                    Label.VTD2_MilestoneType => 'Milestone',
                    Label.VTR5_Intervention => 'Intervention'
            };
            return timeLineItemTypes.get(value);
        }

        private Long getItemDateTime(Datetime value) {
            if (value != null) {
                return value.getTime() + UserInfo.getTimeZone().getOffset(System.now());
            }
            return 0L;
        }

        private List<Long> getItemDatesRange(List<Datetime> visitWindowRaw) {
            if (visitWindowRaw == null || visitWindowRaw.isEmpty()) return null;
            List<Long> itemDatesRange = new List<Long>();
            for (Datetime dt : visitWindowRaw) {
                itemDatesRange.add(dt.getTime());
            }
            return itemDatesRange;
        }
    }
}