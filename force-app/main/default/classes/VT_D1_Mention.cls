public class VT_D1_Mention {

    public final String RawCommentBody;
    public final String rawBody;
    public final Id parentId;
    public final Id Id;

    public Set<Id> userIds;
    public Set<Id> oooUserIds;
    public Map<Id, Set<User>> backupUserIdsByOooUsers = new Map<Id, Set<User>>();

    public String getBody() {
        return rawBody != null ? rawBody : RawCommentBody;
    }

    public Set<Id> getUserIds() {
        if (this.userIds == null) {
            this.userIds = new Set<Id>();
            Matcher m = VT_D1_MentionHelper.RAW_MENTION.matcher(this.getBody());
            while (m.find()) this.userIds.add(m.group(2));

        }
        return this.userIds;
    }
}