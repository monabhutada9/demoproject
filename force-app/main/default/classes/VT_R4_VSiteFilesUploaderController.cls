public with sharing class VT_R4_VSiteFilesUploaderController {

    @AuraEnabled(cacheable = true)
    public static InitData getInitData() {
        Id currentUserId = UserInfo.getUserId();
        InitData data = new InitData();
        Set<Id> availableSiteIds = getAvailableSiteIds(currentUserId);

        for (HealthCloudGA__CarePlanTemplate__c study : [
                SELECT Id, Name,
                (SELECT Id, Name FROM Virtual_Sites__r
                WHERE Id IN :availableSiteIds
                OR VTD1_Study_Team_Member__r.User__c = :currentUserId
                OR VTR2_Backup_PI_STM__r.User__c = :currentUserId
                )
                FROM HealthCloudGA__CarePlanTemplate__c
                WHERE HealthCloudGA__Active__c = true
                AND Id IN (SELECT VTD1_Study__c FROM Virtual_Site__c WHERE Id IN :availableSiteIds)
        ]) {
            data.studies.add(new Study(study));
        }
        return data;
    }

    @AuraEnabled
    public static void attachFilesToRecord(List<String> fileIds, String siteId) {
        List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink>();
        for (String fileId : fileIds) {
            contentDocumentLinks.add(new ContentDocumentLink (
                LinkedEntityId = siteId,
                ContentDocumentId = fileId,
                ShareType = 'I',
                Visibility = 'AllUsers'
            ));
        }
        try {
            insert contentDocumentLinks;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    public class InitData {
        @AuraEnabled public List<Study> studies { get; set; }

        public InitData() {
            this.studies = new List<Study>();
        }
    }
    public class Study {
        @AuraEnabled public  String studyLabel { get; set; }
        @AuraEnabled public  List<Map<String, String>> sites { get; set; }

        public Study(HealthCloudGA__CarePlanTemplate__c studyWithSites) {
            this.studyLabel = studyWithSites.Name;
            this.sites = new List<Map<String, String>>();
            for (Virtual_Site__c virtualSite : studyWithSites.Virtual_Sites__r) {
                Map<String, String> site = new Map<String, String>();
                site.put('label', virtualSite.Name);
                site.put('value', virtualSite.Id);
                this.sites.add(site);
            }
        }
    }

    //TODO Create Helper The same logic in VT_D1_DocumentController

    public static Set<Id> getAvailableSiteIds(Id currentUserId) {
        Set<Id> availableSiteIds = new Set<Id>();
        for (Study_Site_Team_Member__c studySiteTeamMember : [
                SELECT VTR2_Associated_PI3__r.VTD1_VirtualSite__c, VTR2_Associated_PI__r.VTD1_VirtualSite__c
                FROM Study_Site_Team_Member__c
                WHERE VTR2_Associated_SubI__r.User__c = :currentUserId
                OR VTR2_Associated_SCr__r.User__c = :currentUserId
        ]) {
            availableSiteIds.add(studySiteTeamMember.VTR2_Associated_PI3__r.VTD1_VirtualSite__c);
            availableSiteIds.add(studySiteTeamMember.VTR2_Associated_PI__r.VTD1_VirtualSite__c);
        }
        availableSiteIds.remove(null);
        return availableSiteIds;
    }
}