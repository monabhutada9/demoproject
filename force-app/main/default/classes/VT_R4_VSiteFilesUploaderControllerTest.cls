@IsTest
public with sharing class VT_R4_VSiteFilesUploaderControllerTest {

    public static void attachFilesToRecordTest() {
        VT_R3_GlobalSharing.disableForTest = true;
        //System.runAs(getNonAdminLightningUser()) {
            HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c(HealthCloudGA__Active__c = true);
            insert study;

            Virtual_Site__c virtualSite = new Virtual_Site__c(VTD1_Study__c = study.Id, VTD1_Study_Site_Number__c = 'test');
            insert virtualSite;

            Study_Team_Member__c studyTeamMember = new Study_Team_Member__c(Study__c = study.Id, User__c = UserInfo.getUserId(), VTD1_VirtualSite__c = virtualSite.Id);
            insert studyTeamMember;

            Study_Site_Team_Member__c studySiteTeamMember = new Study_Site_Team_Member__c(VTR2_Associated_SubI__c = studyTeamMember.Id, VTR2_Associated_PI3__c = studyTeamMember.Id);
            insert studySiteTeamMember;

            Test.startTest();
            VT_R4_VSiteFilesUploaderController.InitData data = VT_R4_VSiteFilesUploaderController.getInitData();
            System.assertEquals(1, data.studies.size());
            System.assertEquals(1, data.studies.get(0).sites.size());

            emulateUserUploadBehavior();
            List<ContentDocument> contentDocuments = [SELECT Id FROM ContentDocument LIMIT 100];
            System.assertEquals(1, contentDocuments.size());
            System.assertEquals(1, [SELECT Count() FROM ContentDocumentLink WHERE ContentDocumentId = :contentDocuments.get(0).Id]);
            VT_R4_VSiteFilesUploaderController.attachFilesToRecord(new List<Id>{contentDocuments.get(0).Id}, virtualSite.Id);
            System.assertEquals(1, [SELECT Count() FROM ContentDocument]);
            System.assertEquals(2, [SELECT Count() FROM ContentDocumentLink WHERE ContentDocumentId = :contentDocuments.get(0).Id]);
            Test.stopTest();
        //}
    }

    private static void emulateUserUploadBehavior() {
        ContentVersion contentVersion = createNewTestContentVersion();
        insert contentVersion;
    }
    private static ContentVersion createNewTestContentVersion() {
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.PathOnClient = 'test.txt';
        contentVersion.Title = 'Test file';
        contentVersion.VersionData = Blob.valueOf('Test Data');
        return contentVersion;
    }
    private static User getNonAdminLightningUser() {
        String userName = VT_D1_TestUtils.generateUniqueUserName();
        return new User (
            ProfileId = VT_D1_HelperClass.getProfileIdByName('Patient Guide'),
            Username = userName,
            LastName = userName,
            Email = userName,
            Alias = 'Test',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            TimeZoneSidKey = 'America/Los_Angeles'
        );
    }
}