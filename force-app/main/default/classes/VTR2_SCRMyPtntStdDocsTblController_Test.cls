@IsTest
public with sharing class VTR2_SCRMyPtntStdDocsTblController_Test {

    public static void isDownloadAllowedSuccess() {
        User scrUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Site Coordinator')
                .persist();
        User piUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Primary Investigator')
                .persist();

        Boolean allowedForScr;
        Boolean allowedForPi;

        Test.startTest();
        System.runAs(scrUser) {
            allowedForScr = VTR2_SCRMyPatientStdDocsTableController.isDownloadAllowed();
        }
        System.runAs(piUser) {
            allowedForPi = VTR2_SCRMyPatientStdDocsTableController.isDownloadAllowed();
        }
        Test.stopTest();

        System.assertEquals(true, allowedForScr);
        System.assertEquals(false, allowedForPi);
    }
}