public without sharing class VT_R2_SCRPatientVisitDetail {
    @AuraEnabled
    public static VTD1_Actual_Visit__c cancelVisitRemote(Id visitId, String reason) {
        VTD1_Actual_Visit__c vt = new VTD1_Actual_Visit__c();
        vt.Id = visitId;
        vt.VTD1_Reason_for_Cancellation__c = reason;
        vt.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED;
        vt.VTD1_Date_Time_Stamp__c = Datetime.now();
        try {
            update vt;
            return vt;
        } catch (Exception e) {
            System.debug('Error while cancelling visit');
            throw new DmlException();
        }
    }
    @AuraEnabled
    public static Map<String, Object> getVisitData(Id visitId, Id caseId) {
        Map<String, Object> dataMap = new Map<String, Object>();

        List<Case> cases = [
                SELECT Id, 
                       Contact.Name, 
                       VTD1_Study__r.Name, 
                       Subject, 
                       Preferred_Lab_Visit__c, 
                       
                       // SH-13732: Added First onboarding visit completion date in SOQL
                       VTR5_FirstOnboardingVisitCompleteDate__c 	
                    FROM Case WHERE Id=:caseId
        ];
        if (!cases.isEmpty()) {
            dataMap.put('case', cases[0]);
        }

        if (visitId!=null) {
            List<VTD1_Actual_Visit__c> visits = [
                    SELECT
                            Id,
                            VTD1_Case__c,
                            Name,
                            VTD1_Visit_Name__c,
                            Patient_Name_Formula__c,
                            VTD1_Visit_Type__c,
                            VTD2_Common_Visit_Type__c,
                            VTD2_Scheduled_Date_Time_Formula__c,
                            VTD1_Scheduled_Date_Time__c,
							VTD1_Visit_Completion_Date__c,
                            VTD2_Scheduled_Time_Formula__c,
                            VTD1_Visit_Duration__c,
                            VTD2_Visit_Activities_Completed__c,
                            VTD1_Status__c,
                            toLabel(VTD1_Status__c) statusLabel,
                            VTD1_Additional_Patient_Visit_Checklist__c,
                            VTD1_Additional_Visit_Checklist__c,
                            VTD1_Compensation_Approved__c,
                            Compensation_Approving_User_Formula__c,
                            VTD1_Compensation_Approved_Date__c,
                            VTD1_Pre_Visit_Instructions__c,
                            VTR2_Modality__c,
                            VTR2_Visit_location__c,
                            VTR2_Visit_location__r.VTR2_LocationName__c,
                            VTR2_Visit_location__r.VTR2_AddressLine1__c,
                            VTR2_Visit_location__r.Name,
                            VTR2_Televisit__c,
                            VTR2_Loc_Phone__c,
                            VTR2_ReasonToRemove__c,
                            VTD1_Reason_for_Reschedule__c,
                            VTD1_Unscheduled_Visit_Type__c,
                            VTD1_Protocol_Visit__r.RecordType.DeveloperName,
                            VTD1_Reason_for_Cancellation__c,
                            VTD1_Case__r.Status,
                            VTD1_Case__r.VTR3_LTFU__c,
                            VTR3_Visit_Number_UI__c,
                            VTR2_Independent_Rater_Flag__c,

                            // SH-13732: Added a few field in the SOQL
                            VTR5_Reason_Of_Backdating__c, 
                            VTD1_Case__r.VTR5_Baseline_Visit_After_A_R_Flag__c, 
                            VTD1_Visit_Number__c  
                    FROM VTD1_Actual_Visit__c
                    WHERE Id = :visitId
            ];
            System.debug('Independent_Rater_Flag ** '+ visits[0].VTR2_Independent_Rater_Flag__c);
            if (!visits.isEmpty()) {
                dataMap.put('visit', visits[0]);
            }
            List<ContentDocumentLink> visitDocuments = [SELECT Id, ContentDocument.Title, ContentDocument.FileExtension, ContentDocument.LatestPublishedVersionId FROM ContentDocumentLink WHERE LinkedEntityId = :visitId];
            dataMap.put('files', visitDocuments);
            dataMap.put('availableDates', new VT_D1_ActualVisitsHelper().getAvailableDateTimes(visitId));
            dataMap.put('maxReasonRemoveVisit', Schema.SObjectType.VTD1_Actual_Visit__c.fields.VTR2_ReasonToRemove__c.getLength());
            dataMap.put('userTimezoneOffset', UserInfo.getTimeZone().getOffset(Datetime.now()));
        }

        return dataMap;
    }


//  Added during code merge with r3
//    @AuraEnabled(Cacheable=true)
//    public static Map<String, String> getInputSelectOptions(String sObjName, String fieldName){
//        Map<String, String> options = new Map<String, String>();
//        if (!String.isEmpty(sObjName) && !String.isEmpty(fieldName)) {
//            Schema.SObjectType objType = Schema.getGlobalDescribe().get(sObjName);
//            SObject sObj = objType.newSObject();
//            Map<String, String> optionsMap = VT_D1_CustomMultiPicklistController.getSelectOptionsWithLabel(sObj, fieldName);
//            for (String value : optionsMap.keySet()) {
//              options.put(value, optionsMap.get(value));
//            }
//        }
//        return options;
//    }

    @AuraEnabled
    public static void saveVisitData(VTD1_Actual_Visit__c visit) {
//      System.debug('id:');
//      System.debug(visitId);
//      VTD1_Actual_Visit__c visit = [
//              SELECT
//                      Id,
//                      VTD1_Case__c,
//                      Name,
//                      VTD1_Visit_Type__c,
//                      VTD2_Scheduled_Date_Time_Formula__c,
//                      VTD2_Scheduled_Time_Formula__c,
//                      VTD1_Visit_Duration__c,
//                      VTD2_Visit_Activities_Completed__c,
//                      VTD1_Status__c,
//                      VTD1_Additional_Patient_Visit_Checklist__c,
//                      VTD1_Additional_Visit_Checklist__c,
//                      VTD1_Compensation_Approved__c,
//                      Compensation_Approving_User_Formula__c,
//                      VTD1_Compensation_Approved_Date__c,
//                      VTD1_Pre_Visit_Instructions__c
//              FROM VTD1_Actual_Visit__c WHERE Id=:visitId
//      ];
//      System.debug(visit);
//      visit.Name = name;
//      visit.VTD1_Status__c = status;
//      visit.VTD2_Visit_Activities_Completed__c = activitiesCompleted;
//      visit.VTD1_Pre_Visit_Instructions__c = preVisitInstructions;
//      visit.VTD1_Additional_Visit_Checklist__c = visitChecklist;
        update visit;
    }

    @AuraEnabled
    public static void cancelVisit(Id visitId, String chosenOption, String removeReason) {
        System.debug('VT_R2_SCRPatientVisitDetail.cancelVisit');
        VT_D1_ActualVisitsHelper.removeVisit(visitId, chosenOption, removeReason);
    }
}