/**
* @author: Carl Judge
* @date: 05-May-20
* @description: eCOA study version API
**/

public class VT_R5_eCoaStudyVersion {
    /**
     * Get version info for a study from eCOA. Needed for other operations
     * @param studyGuid - eCOA guid of the study
     * @return the version guid for the study
     */
    public static VT_D1_HTTPConnectionHandler.Result getStudyVersion(String studyGuid) {
        String endpoint = '/api/v1/orgs/:orgGuid/studies/:studyGuid/versions';
        String method = 'GET';
        String action = 'eCOA Get Study Version';

        return new VT_R5_eCoaApiRequest()
            .setStudyGuid(studyGuid)
            .setEndpoint(endpoint)
            .setMethod(method)
            .setAction(action)
            .setSkipLogging(true)
            .send();
    }

    public static String getVersionGuidFromVersionResponse(VT_D1_HTTPConnectionHandler.Result versionResponse) {
        // returning the first (latest) version from the response
        return versionResponse.httpResponse.getBody().substringAfter('"guid":"').substringBefore('"');
    }

/*
    public static String getRuleSetGuidFromVersionResponse(VT_D1_HTTPConnectionHandler.Result versionResponse) {
        // assuming only 1 rule set per study. Pulling the first if there are more than 1 for some reason.
        return versionResponse.httpResponse.getBody().substringAfter('ruleSetGuids":["').substringBefore('"');
    } */

}