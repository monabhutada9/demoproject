/**
* @author: N.Arbatskiy
* @date: 05-May-20
* @description: Class for using Subject Update method from eCOA API
**/
public with sharing class VT_R5_eCoaUpdateSubject extends VT_R5_eCoaAbstractAction {
    private Id userId;
    private String method = 'PUT';

    private String endpoint = VT_R4_ConstantsHelper_Integrations.ENDPOINT_URL_ECOA_SUBJECT_UPDATE;
    private String action = 'eCOA Update Subject';
    private String studyGuid = '';
    private String subjectGuid = '';
    private Boolean refreshSchedule = false;

    /**
    * @param userId takes an Id from userList @method updateEcoaCallout(@class VT_D1_UserTriggerHandler)
    */
    public VT_R5_eCoaUpdateSubject(Id userId) {
        this.userId = userId;
    }
    public VT_R5_eCoaUpdateSubject(Id userId, Boolean refreshSchedule) {
        this.userId = userId;
        this.refreshSchedule = refreshSchedule;
    }
    /**
   * @description: Executes HTTP PUT
   * @return VT_D1_HTTPConnectionHandler.Result in order to be passed as
   * a parameter to @method processResponse(VT_R5_eCoaAbstractAction class).
   *
   */
    public override VT_D1_HTTPConnectionHandler.Result doCallout() {
        if (userId != null) {
            User user = [
                    SELECT
                            Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eCOA_Guid__c,
                            LanguageLocaleKey,
                            TimeZoneSidKey,
                            VTR5_eCOA_Guid__c,
                            VTD1_StudyId__c,
                            VTR5_ePRO_GUID__c,
                            VTR5_ClinRO_GUID__c
                    FROM User
                    WHERE Id = :userId
            ];
            studyGuid = user.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eCOA_Guid__c;
            subjectGuid = user.VTR5_eCOA_Guid__c;


            VT_R5_RequestBuilder_eCoaUpdateSubject body = new VT_R5_RequestBuilder_eCoaUpdateSubject(user);
            VT_R5_eCoaApiRequest request = new VT_R5_eCoaApiRequest();
            request.setEndpoint(endpoint);
            request.setMethod(method);
            request.setAction(action);
            request.setStudyGuid(studyGuid);
            request.setSubjectGuid(subjectGuid);
            request.setBody(body);
            request.setSkipLogging(true);
            return request.send();
        }
        return null;
    }
    public override Type getType() {
        return VT_R5_eCoaUpdateSubject.class;
    }

    protected override void handleFinal(VT_D1_HTTPConnectionHandler.Result result) {
        if (!this.skipLogging) { insert result.log; }
    }
}