@IsTest
public with sharing class VT_D2_CasePGReassignTest {

    public static void getCaseSuccessTest() {
        Case caseRecord = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];

        Test.startTest();
        Case fetchedCase = VT_D2_CasePGReassignController.getCase(caseRecord.Id);
        Test.stopTest();

        System.assertEquals(caseRecord.Id, fetchedCase.Id);
    }

    public static void getCaseFailTest() {
        String errorMessage;

        Test.startTest();
        try {
            VT_D2_CasePGReassignController.getCase('');
        } catch (AuraHandledException e) {
            errorMessage = e.getMessage();
        }
        Test.stopTest();

        System.assertEquals('Script-thrown exception', errorMessage);
    }

    public static void updateCaseSuccessTest() {
        Case caseRecord = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];

        Test.startTest();
        VT_D2_CasePGReassignController.updateCase(caseRecord.Id);
        Test.stopTest();

        System.assertEquals(UserInfo.getUserId(), [SELECT Id, OwnerId FROM Case WHERE Id = :caseRecord.Id].OwnerId);
    }

    public static void updateCaseFailTest() {
        String errorMessage;

        Test.startTest();
        try {
            VT_D2_CasePGReassignController.updateCase('');
        } catch (AuraHandledException e) {
            errorMessage = e.getMessage();
        }
        Test.stopTest();

        System.assertEquals('Script-thrown exception', errorMessage);
    }

}