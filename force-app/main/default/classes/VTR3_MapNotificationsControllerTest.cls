/**
 * Created by user on 30.10.2019.
 */

@IsTest
public with sharing class VTR3_MapNotificationsControllerTest {
    public static void mapNotificationsTest() {
        List <VTD2_TN_Catalog_Code__c> tnCatalogCodes = new List<VTD2_TN_Catalog_Code__c>();
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = '001',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Task').getRecordTypeId(),
                VTD2_T_Assigned_To_ID__c = 'VTD1_Document__c.VTD1_PG_Approver__c',
                VTD2_T_Related_To_Id__c = 'VTD1_Document__c.Id',
                VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD_RTId__c.VTD2_Task_TaskForDocumentReview__c',
                VTD2_T_Description__c = '"Approve or reject medical records for" & VTD1_Document__c.VTD1_Clinical_Study_Membership__r.VTD1_Patient__r.VTD1_First_Name__c & " " & VTD1_Document__c.VTD1_Clinical_Study_Membership__r.VTD1_Patient__r.VTD1_Last_Name__c',
                VTD2_T_Subject__c = 'VTD2_Review_And_Approve_Medical_Records & FORMAT(VTD1_Document__c.CreatedDate, \'EEE, MMM d yyyy\') ',
                VTD2_T_Care_Plan_Template__c = 'VTD1_Document__c.VTD1_Clinical_Study_Membership__r.VTD1_Study__c',
                VTD2_T_Due_Date__c = 'today()',
                VTD2_T_Reminder_Date_Time__c = 'NOW()',
                VTD2_T_Category__c = 'Enrollment Related',
                VTD2_T_Priority__c = 'Normal',
                VTD2_T_Status__c = 'Open',
                VTD2_T_Reminder_Set__c = true,
                VTD2_T_Document__c = 'VTD1_Document__c.Id',
                VTD2_T_Patient__c = 'VTD1_Document__c.VTD1_Clinical_Study_Membership__c',
                VTD2_T_Pre_Visit_Instructions__c = null,
                VTD2_T_IsRequestVisitRedirect__c = false,
                VTD2_T_My_Task_List_Redirect__c = null,
                VTD2_T_Actual_Visit__c = null,
                VTD2_T_Description_Parameters__c = 'test',
                VTR3_T_TN_Notification_code__c = 'N003',
                VTD2_T_Type_for_Backend_Logic__c = 'Confirm Drop Off of Study Medication',
                VTD2_T_Subject_Parameter_Field__c = 'test'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'N003',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Notification').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Opportunity',
                VTD2_T_Assigned_To_ID__c = 'Opportunity.Account.Owner.Id',
                VTD2_T_Related_To_Id__c = 'Opportunity.Id',
                VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD_RTId__c.VTD1_Task_SimpleTask__c',
                VTD2_T_Description__c = 'Opportunity.Account.Name & " and " & LEFT(Opportunity.Account.Name, 1)',
                VTD2_T_Subject__c = 'IF(Opportunity.Id != null AND NOT(ISBLANK(Opportunity.Id)),Opportunity.Account.Name,Opportunity.Account.Name))',
                VTD2_T_Care_Plan_Template__c = null,
                VTD2_T_Due_Date__c = null,
                VTD2_T_Reminder_Date_Time__c = null,
                VTD2_T_Category__c = 'Enrollment Related',
                VTD2_T_Priority__c = 'Normal',
                VTD2_T_Receiver__c ='Opportunity.OwnerId',
                VTD2_T_Status__c = 'Open',
                VTD2_T_Reminder_Set__c = true,
                VTD2_T_Document__c = null,
                VTD2_T_Message_Parameters__c = null, //'IF(Opportunity.Id != null AND NOT(ISBLANK(Opportunity.Id)),Opportunity.Account.Name,Opportunity.Account.Name))',
                VTD2_T_Patient__c = null,
                VTD2_T_Pre_Visit_Instructions__c = null,
                VTD2_T_IsRequestVisitRedirect__c = false,
                VTD2_T_My_Task_List_Redirect__c = null,
                VTD2_T_Actual_Visit__c = null,
                VTD2_T_Description_Parameters__c = 'test',
                VTD2_T_Type_for_Backend_Logic__c = 'Confirm Drop Off of Study Medication',
                VTD2_T_Subject_Parameter_Field__c = 'test'
        ));
        insert tnCatalogCodes;
        Test.startTest();
        VTR3_MapNotificationsController.mapNotifications();
        Test.stopTest();
    }
}