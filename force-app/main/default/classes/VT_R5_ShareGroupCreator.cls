/**
* @author: Carl Judge
* @date: 13-Oct-20
* @description: Create sharing groups for new studies and sites
**/

public without sharing class VT_R5_ShareGroupCreator {
    public static void createGroups(List<SObject> records) {
        Map<Id, Group> readGroups = new Map<Id, Group>();
        Map<Id, Group> editGroups = new Map<Id, Group>();
        for (SObject record : records) {
            readGroups.put(record.Id, getGroup(record, 'R'));
            editGroups.put(record.Id, getGroup(record, 'E'));
        }

        List<Group> groupsToInsert = new List<Group>();
        groupsToInsert.addAll(readGroups.values());
        groupsToInsert.addAll(editGroups.values());
        insert groupsToInsert;

        List<SObject> recordsToUpdate = new List<SObject>();
        for (SObject record : records) {
            SObject recToUpdate = record.getSObjectType().newSObject();
            recToUpdate.Id = record.Id;
            recToUpdate.put('VTR5_ReadShareGroupId__c', readGroups.get(record.Id).Id);
            recToUpdate.put('VTR5_EditShareGroupId__c', editGroups.get(record.Id).Id);
            recordsToUpdate.add(recToUpdate);
        }
        update recordsToUpdate;
    }

    private static Group getGroup(SObject record, String prefix) {
        return new Group (
            Name = getName(record, prefix),
            DeveloperName = prefix + '_' + record.Id,
            DoesIncludeBosses = false,
            DoesSendEmailToMembers = false,
            Type = 'Regular'
        );
    }

    private static String getName(SObject record, String prefix) {
        String name;
        if (record instanceof HealthCloudGA__CarePlanTemplate__c) {
            name = (String) record.get('Name');
        } else {
            Object siteNumber = record.get('VTD1_Study_Site_Number__c');
            name = siteNumber != null ? (String) siteNumber : record.Id;
        }
        name = prefix + '_' + name;
        if (name.length() > 40) {
            name = name.substring(0,40);
        }
        return name;
    }
}