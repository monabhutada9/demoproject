public without sharing class VT_D1_MentionHelper {

    public static final String USER_PREFIX = SObjectType.User.getKeyPrefix();
    public static final String EXPRESSION = '\\{(!|@)(' + USER_PREFIX + '[0-9a-zA-Z]{15})\\}';
    public static Pattern RAW_MENTION = Pattern.compile(EXPRESSION);

    public static boolean mentionsProcessed = false;

    public static void getOooUserIds(List<VT_D1_Mention> mentions) {
        List<Id> userIds = new List<Id>();
        for (VT_D1_Mention mention : mentions) {
            userIds.addAll(mention.getUserIds());
        }

        if (!userIds.isEmpty()) {
            Set<Id> oooUserIds = new Set<Id>();
            DateTime now = DateTime.now();
            for (Event item : [
                    SELECT OwnerId
                    FROM Event
                    WHERE StartDateTime <= :now AND EndDateTime > :now AND RecordType.DeveloperName = 'OutOfOffice'
            ]) {
                oooUserIds.add(item.OwnerId);
            }

            if (!oooUserIds.isEmpty()) {
                for (VT_D1_Mention mention : mentions) {
                    if (mention.oooUserIds == null) {
                        mention.oooUserIds = new Set<Id>();
                    }
                    for (Id userId : mention.getUserIds()) {
                        if (oooUserIds.contains(userId)) {
                            mention.oooUserIds.add(userId);
                        }
                    }
                }
            }
        }
    }

    public static Map<Id, VT_D1_Mention> getFinalUpdateMap(List<VT_D1_Mention> mentions) {
        Map<Id, VT_D1_Mention> finalMap = new Map<Id, VT_D1_Mention>();
        for (VT_D1_Mention item : mentions) {
            if (item.backupUserIdsByOooUsers != null && !item.backupUserIdsByOooUsers.isEmpty()) {
                finalMap.put(item.Id, item);
            }
        }
        return finalMap;
    }

    public static List<Object> getBatchResults(List<ConnectApi.BatchResult> batchResults) {
        List<Object> results = new List<Object>();
        for (ConnectApi.BatchResult item : batchResults) {
            if (item.isSuccess()) {
                results.add(item.getResult());
            } else {
                system.debug(item.getErrorMessage());
            }
        }
        System.debug('getBatchResults ' + results);
        return results;
    }

    public static ConnectApi.MessageBodyInput getMessageBodyWithAppendedBackups(Id recId, List<ConnectApi.MessageSegment> existingSegments, Map<Id, VT_D1_Mention> mentionMap) {
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

        // add existing message segments first
        for (ConnectApi.MessageSegment msgSeg : existingSegments) {
            String segType = String.ValueOf(msgSeg.type);

            if (segType == 'MarkupBegin') {
                messageBodyInput.messageSegments.add(getMarkupBeginSeg(msgSeg));
            } else if (segType == 'Mention') {
                messageBodyInput.messageSegments.add(getMentionSeg(msgSeg));
            } else if (segType == 'Text') {
                messageBodyInput.messageSegments.add(getTextSeg(msgSeg.text));
            } else if (segType == 'Link') {
                messageBodyInput.messageSegments.add(getLinkSeg(msgSeg));
            } else if (segType == 'MarkupEnd') {
                messageBodyInput.messageSegments.add(getMarkupEndSeg(msgSeg));
            } else if (segType == 'Hashtag') {
                messageBodyInput.messageSegments.add(getHashtagSeg(msgSeg));
            }

        }

        // add new mentions
        for (Id backUpId : mentionMap.get(recId).backupUserIdsByOooUsers.keySet()) {
            messageBodyInput.messageSegments.add(getTextSeg('\n'));

            messageBodyInput.messageSegments.add(getMentionSeg(backUpId));

            String extraText = ' (';
            for (User u : mentionMap.get(recId).backupUserIdsByOooUsers.get(backUpId)) {
                extraText += u.Name + ',';
            }
            extraText = extraText.substringBeforeLast(',');
            extraText += ' out of office)';
            messageBodyInput.messageSegments.add(getTextSeg(extraText));
        }

        return messageBodyInput;
    }


    public static void addSharingToCaseGFC(Map<Id, Set<Id>> caseWithMentions) {
        Map<Id, Set<Id>> caseWithShare = new Map<Id, Set<Id>>();
        List<CaseShare> caseShares = [
                SELECT Id, CaseId, UserOrGroupId, CaseAccessLevel, RowCause
                FROM CaseShare
                WHERE CaseId IN :caseWithMentions.keySet()
        ];

        for (CaseShare caseShare : caseShares) {
            Id caseId = caseShare.CaseId;
            Id userId = caseShare.UserOrGroupId;
            if (!caseWithShare.containsKey(caseId)) {
                caseWithShare.put(caseId, new Set<Id>{
                        userId
                });
            } else {
                Set<Id> usersId = caseWithShare.get(caseId);
                usersId.add(userId);
                caseWithShare.put(caseId, usersId);
            }
        }

        System.debug('caseWithShare ' + caseWithShare);
        List<CaseShare> newCaseShares = new List<CaseShare>();
        for (Id caseId : caseWithMentions.keySet()) {
            Set<Id> mentionedUsersIds = caseWithMentions.get(caseId);
            Set<Id> sharedUsersIds = caseWithShare.get(caseId);
            Boolean result = mentionedUsersIds.removeAll(sharedUsersIds);
            for (Id mentionedId : mentionedUsersIds) {
                newCaseShares.add(new CaseShare(
                        CaseId = caseId,
                        UserOrGroupId = mentionedId,
                        CaseAccessLevel = 'Edit',
                        RowCause = 'Manual'
                ));

            }
        }
        if (!newCaseShares.isEmpty()) {
            Database.insert(newCaseShares, false);
        }
    }

    public static Map<String, List<VT_D1_Mention>> getMentionsByParentType(List<VT_D1_Mention> mentions) {
        Map<String, List<VT_D1_Mention>> mentionsByParentType = new Map<String, List<VT_D1_Mention>>();

        for (VT_D1_Mention mention : mentions) {
            String parentObjType = String.ValueOf(mention.parentId.getSobjectType());
            if (!mentionsByParentType.containsKey(parentObjType)) {
                mentionsByParentType.put(parentObjType, new List<VT_D1_Mention>());
            }
            mentionsByParentType.get(parentObjType).add(mention);
        }

        return mentionsByParentType;
    }


/* ----- Message Segment Input Helpers ----- */
    public static ConnectApi.TextSegmentInput getTextSeg(String text) {
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        textSegmentInput.text = text;
        return textSegmentInput;
    }

    public static ConnectApi.MentionSegmentInput getMentionSeg(String recId) {
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        mentionSegmentInput.id = recId;
        return mentionSegmentInput;
    }
    public static ConnectApi.MentionSegmentInput getMentionSeg(ConnectApi.MessageSegment msgSeg) {
        ConnectApi.MentionSegment castSeg = (ConnectApi.MentionSegment) msgSeg;
        return getMentionSeg(castSeg.record.Id);
    }

    public static ConnectApi.MarkupBeginSegmentInput getMarkupBeginSeg(ConnectApi.MarkupType mType) {
        ConnectApi.MarkupBeginSegmentInput markupBeginSeg = new ConnectApi.MarkupBeginSegmentInput();
        markupBeginSeg.markupType = mType;
        return markupBeginSeg;
    }
    public static ConnectApi.MarkupBeginSegmentInput getMarkupBeginSeg(ConnectApi.MessageSegment msgSeg) {
        ConnectApi.MarkupBeginSegment castSeg = (ConnectApi.MarkupBeginSegment) msgSeg;
        return getMarkupBeginSeg(castSeg.markupType);
    }

    public static ConnectApi.MarkupEndSegmentInput getMarkupEndSeg(ConnectApi.MarkupType mType) {
        ConnectApi.MarkupEndSegmentInput markupEndSeg = new ConnectApi.MarkupEndSegmentInput();
        markupEndSeg.markupType = mType;
        return markupEndSeg;
    }
    public static ConnectApi.MarkupEndSegmentInput getMarkupEndSeg(ConnectApi.MessageSegment msgSeg) {
        ConnectApi.MarkupEndSegment castSeg = (ConnectApi.MarkupEndSegment) msgSeg;
        return getMarkupEndSeg(castSeg.markupType);
    }

    public static ConnectApi.LinkSegmentInput getLinkSeg(String url) {
        ConnectApi.LinkSegmentInput linkSeg = new ConnectApi.LinkSegmentInput();
        linkSeg.url = url;
        return linkSeg;
    }
    public static ConnectApi.LinkSegmentInput getLinkSeg(ConnectApi.MessageSegment msgSeg) {
        ConnectApi.LinkSegment castSeg = (ConnectApi.LinkSegment) msgSeg;
        return getLinkSeg(castSeg.url);
    }

    public static ConnectApi.HashtagSegmentInput getHashtagSeg(String tag) {
        ConnectApi.HashtagSegmentInput tagSeg = new ConnectApi.HashtagSegmentInput();
        tagSeg.tag = tag;
        return tagSeg;
    }
    public static ConnectApi.HashtagSegmentInput getHashtagSeg(ConnectApi.MessageSegment msgSeg) {
        ConnectApi.HashtagSegment castSeg = (ConnectApi.HashtagSegment) msgSeg;
        return getHashtagSeg(castSeg.tag);
    }

    public class Mention
    {
        final String rawBody;
        final Id parentId;
        Set<Id> getUserIds()
        {
            Set<Id> userIds = new Set<Id>();
            Matcher m = RAW_MENTION.matcher(rawBody);
            while (m.find()) userIds.add(m.group(1));
            return userIds;
        }
    }
}