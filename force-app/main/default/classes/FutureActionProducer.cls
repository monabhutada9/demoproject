/**
 * Created by triestelaporte on 11/6/20.
 */

public without sharing class FutureActionProducer {

    private static final Set<String> ASYNC_STATUS_RUNNING = new Set<String>{
            'Queued', 'Preparing', 'Processing', 'Holding', 'Scheduled'
    };
    public static Boolean bypassRunBatchJob = false;
    private static List<FutureActionQueueRecord__c> actionsToQueue;

    static {
        actionsToQueue = new List<FutureActionQueueRecord__c>();
    }

    // this should be called in the after insert/after update trigger on the queue item table
    public static void queueJobIfNeeded () {
        if (bypassRunBatchJob) {
            return;
        }

        FutureActionSettings theSettings = FutureActionSettings.getSettings();

        // this query gets currently running jobs
        List<AsyncApexJob> runningJobs = [
                SELECT Id,
                        Status
                FROM AsyncApexJob
                WHERE ApexClass.Name = :theSettings.className
                        AND Status IN :ASYNC_STATUS_RUNNING
        ];
        if (!runningJobs.isEmpty()) {
            return;
        }
        // and this one gets jobs that are on a scheduled delay (like from the finish method)
        List<CronTrigger> scheduledJobs = [
                SELECT Id
                FROM CronTrigger
                WHERE CronJobDetail.Name = :theSettings.jobName
        ];
        if (!scheduledJobs.isEmpty()) {
            return;
        }

        FutureActionProcessor.scheduleBatchJob();

    }

    public static void queueFutureEmail (User recipient, Id whatId, Id emailTemplateId, Datetime actionDatetime) {
        createAndQueueFutureAction(null, FutureActionSettings.ACTION_TYPE.SEND_EMAIL, actionDatetime, whatId, emailTemplateId, recipient.Id);
    }

    public static void queueFutureRecordUpdate (SObject theRecord, Datetime actionDatetime) {
        createAndQueueFutureAction(theRecord, FutureActionSettings.ACTION_TYPE.UPDATE_RECORD, actionDatetime, null, null, null);
    }

    public static void queueFutureRecordCreate (SObject theRecord, Datetime actionDatetime) {
        createAndQueueFutureAction(theRecord, FutureActionSettings.ACTION_TYPE.CREATE_RECORD, actionDatetime, null, null, null);
    }

    public static void queueFutureRecordDelete (SObject theRecord, Datetime actionDatetime) {
        createAndQueueFutureAction(theRecord, FutureActionSettings.ACTION_TYPE.DELETE_RECORD, actionDatetime, null, null, null);
    }

    private static void createAndQueueFutureAction (SObject theRecord, FutureActionSettings.ACTION_TYPE actionType, Datetime actionDatetime, Id emailWhatId, Id emailTemplateId, Id emailWhoId) {
        actionsToQueue.add(new FutureActionQueueRecord__c(
                SerializedRecord__c = (theRecord != null) ? JSON.serialize(theRecord) : '',
                ActionType__c = String.valueOf(actionType),
                ActionDate__c = actionDatetime,
                EmailWhatId__c = emailWhatId,
                EmailTemplateId__c = emailTemplateId,
                EmailWhoId__c = emailWhoId
        ));
    }

    public static void commitActions () {
        insert actionsToQueue;
    }
}