/**
* @author: N.Arbatskiy
* @date: 10-Aug-20
* @description: Handles eCOA inbound emails
**/

global class VT_R5_eCoaSiteStaffEmailHandler implements Messaging.InboundEmailHandler {
    public class EmptyEmailException extends Exception {
    }
    public class NoDiaryFoundException extends Exception {
    }

    private static List<String>siteStaffProfileNames = new List<String>{
            'Primary Investigator',
            'Site Coordinator',
            'Study Concierge',
            'Patient Guide'
    };
    private static Datetime dueDate;
    private static Datetime availableDate;
    private static String studyId;


    /**
    * @description: This method takes the inbound email from eCOA with ResponseGuids.
    * Only the first ResponseGuid is used then to send notifications and tasks to the Site Staff.
    * @param email is used for getting message text body.
    * @param env is unused.
    * @return Messaging.InboundEmailResult result to be processed further on.
    *
    */
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope env) {
        String body = email.plainTextBody;
        if (String.isEmpty(body)) {
            throw new EmptyEmailException('eCOA email contains no Response Guids.');
        }
        List<String>respGuidList = new List<String>();
        Integer currentPosition = 0;
        while (currentPosition <= body.length()) {
            Integer nextNewLine = body.indexOf('\n', currentPosition);
            if (nextNewLine == -1) {
                nextNewLine = body.length();
            }
            String rowString = body.substring(currentPosition, nextNewLine);
            if (String.isNotBlank(rowString)) {
                respGuidList.add(rowString);
            }
            currentPosition = nextNewLine + 1;
        }
        List<User>stms = getSiteStaffUsersViaDiaries(respGuidList);
        VTR5_DiaryListView__c diaryListView = new VTR5_DiaryListView__c();
        insert diaryListView;

        insert new Attachment(
                ParentId = diaryListView.Id,
                Body = Blob.valueOf(email.plainTextBody),
                Name = 'Email ' + Datetime.now());

        sendNotifications(stms, diaryListView.Id);
        sendTasks(stms, diaryListView.Id);

        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        result.success = true;
        return result;
    }

    /**
    * @description: This one is to get all the Site Staff members using the ResponseGuid.
    * @param respGuidList is used for fetching the eDiary to get the STMs then.
    * @return List of users which are members of the Site Staff.
    *
    */
    private static List<User> getSiteStaffUsersViaDiaries(List<String>respGuidList) {
        List<VTD1_Survey__c> diaries = [
                SELECT VTD1_CSM__r.VTD1_Study__c,
                        VTD1_CSM__r.VTD1_Virtual_Site__c,
                        VTD1_Due_Date__c,
                        VTD1_Date_Available__c
                FROM VTD1_Survey__c
                WHERE VTR5_Response_GUID__c IN :respGuidList
        ];

        if (diaries.isEmpty()) {
            throw new NoDiaryFoundException('No diaries with such VTR5_Response_GUID__c were found in SH.');
        }

        Set<Id>siteIds = new Set<Id>();
        for (VTD1_Survey__c dairy : diaries) {
            if (dairy.VTD1_CSM__r.VTD1_Virtual_Site__c != null) {
                siteIds.add(dairy.VTD1_CSM__r.VTD1_Virtual_Site__c);
            }
        }

        Set<Id>stmIds = new Set<Id>();
        if (!siteIds.isEmpty()) {
            VT_R5_SiteMemberCalculator calculator = new VT_R5_SiteMemberCalculator();
            calculator.initForSites(siteIds);
            for (Id siteId : siteIds) {
                stmIds.addAll(calculator.getSiteUserIds(siteId));
            }
        }

        dueDate = diaries[0].VTD1_Due_Date__c;
        availableDate = diaries[0].VTD1_Date_Available__c;
        studyId = diaries[0].VTD1_CSM__r.VTD1_Study__c;

        List<HealthCloudGA__CarePlanTemplate__c> study = [
                SELECT Id, (
                        SELECT Id, User__r.Id
                        FROM Study_Team_Members__r
                        WHERE User__r.VTD1_Profile_Name__c = 'Study Concierge'
                        AND User__r.IsActive = TRUE
                )
                FROM HealthCloudGA__CarePlanTemplate__c
                WHERE Id = :studyId
        ];

        if (!study[0].Study_Team_Members__r.isEmpty()) {
            for (Study_Team_Member__c sc : study[0].Study_Team_Members__r) {
                stmIds.add(sc.User__r.Id);
            }
        }
        return [
                SELECT Id,Name,VTD1_Profile_Name__c
                FROM User
                WHERE Id IN :stmIds
                AND VTD1_Profile_Name__c IN :siteStaffProfileNames
                AND IsActive = TRUE
        ];
    }

    /**
    * @description: Sends notifications to the STMs with a link to redirect to Diary List View.
    * @param users is the STMs whom these notifications are sent.
    * @param diaryListViewId is to specify the particular Diary List View.
    *
    */
    private static void sendNotifications(List<User>users, Id diaryListViewId) {
        List<VTD1_NotificationC__c> notificationList = new List<VTD1_NotificationC__c>();
        List<Id>scIds = new List<Id>();
        for (User u : users) {
            if (u.VTD1_Profile_Name__c == 'Study Concierge' || u.VTD1_Profile_Name__c == 'Patient Guide') {
                scIds.add(u.Id);
            } else {
                notificationList.add(
                        new VTD1_NotificationC__c(
                                VTD1_Receivers__c = u.Id,
                                Message__c = 'Patients feel unwell.',
                                OwnerId = u.Id,
                                Title__c = 'Patients Status',
                                Link_to_related_event_or_object__c = 'my-patients?listViewId=' + diaryListViewId,
                                HasDirectLink__c = true
                        )
                );
            }
        }
        if (!scIds.isEmpty()) {
            VT_R3_CustomNotification.sendCustomNotifications(scIds,
                    'Patients Status', 'Patients feel unwell', diaryListViewId);
        }
        VT_R2_NotificationCHandler.eCoaBulkAlerts = true;
        insert notificationList;
    }

    /**
    * @description: Sends tasks to the STMs with a link to redirect to Diary List View.
    * @param users is the STMs whom these tasks are sent.
    * @param diaryListViewId is to specify the particular Diary List View.
    *
    */
    public static void sendTasks(List<User>users, Id diaryListViewId) {
        String REC_TYPE_ID = Task.getSObjectType().getDescribe().getRecordTypeInfosByName().get('SimpleTask').getRecordTypeId();
        List<Task> tasks = new List<Task>();
        for (User siteStaffUser : users) {
            Task siteStaffTask = new Task(
                    Subject = 'Patients feel unwell',
                    OwnerId = siteStaffUser.Id,
                    VTR5_AutocompleteWhenClicked__c = true,
                    VTR5_PreventPBInvocation__c = true,
                    Category__c = 'Follow Up Required',
                    HealthCloudGA__CarePlanTemplate__c = studyId,
                    VTR5_StartDate__c = availableDate,
                    ActivityDate = dueDate.date(),
                    VTD2_My_Task_List_Redirect__c =
                            (siteStaffUser.VTD1_Profile_Name__c == 'Primary Investigator' ||
                                    siteStaffUser.VTD1_Profile_Name__c == 'Site Coordinator') ?
                                    'my-patients?listViewId=' + diaryListViewId : diaryListViewId,
                    RecordTypeId = REC_TYPE_ID
            );
            tasks.add(siteStaffTask);
        }

        VT_D1_AsyncDML asyncDml = new VT_D1_AsyncDML(tasks, DMLOperation.INS);
        asyncDml.setAllOrNothing(false);
        asyncDml.setBatchSize(50);
        asyncDml.enqueue();
    }
}