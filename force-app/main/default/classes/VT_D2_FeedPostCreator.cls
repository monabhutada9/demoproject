/**
* @author: Ruslan Mullayanov
* @date: 15-Jan-19
* @description: Creates VTD2_Feed_Post_on_PCF__c or VTD2_Feed_Post_on_Chat__c on FeedItems/Comments for custom Chatter in Communities
**/

public without sharing class VT_D2_FeedPostCreator {

    public static final Set<String> CASE_PROFILES = new Set<String> {
            'Primary Investigator',
            'Patient',
            'Caregiver',
            'Site Coordinator'
    };
    public static final List<String> CHAT_PROFILES = new List<String> {
            'Primary Investigator',
            'Patient Guide',
            'Patient',
            'Caregiver',
            'Site Coordinator'
    };

    protected List<FeedItem> recs = new List<FeedItem>();
    protected List<FeedItem> recsToProcess = new List<FeedItem>();
    protected List<Id> caseIds = new List<Id>();
    protected List<Id> chatIds = new List<Id>();
    protected Map<Id, List<Id>> parentToMemberIdsMap = new Map<Id, List<Id>>();

    public void processComments(List<FeedComment> recs) {
        Set<Id> itemIds = new Set<Id>();
        for (FeedComment comment : recs) {
            itemIds.add(comment.FeedItemId);
        }
        List<FeedItem> itemsToProcess = [SELECT Id, ParentId, Visibility FROM FeedItem WHERE Id IN :itemIds];
        processItems(itemsToProcess);
    }

    public void processItems(List<FeedItem> recs) {
        this.recs = recs;

        getIdsToProcess();
        getParentToPCFChatMemberIds();
        getParentToChatMemberIds();
        System.debug('Got parentToMemberIdsMap: ' + this.parentToMemberIdsMap);
        if (! this.parentToMemberIdsMap.isEmpty()) {
            createFeedPostOnObject();
        }
    }

    private void getIdsToProcess() {
        Set<Id> inboundCaseIds = new Set<Id>();
        for (FeedItem item : this.recs) {
            Id parentId = item.ParentId;
            if (parentId != null && item.Visibility != 'InternalUsers') {
                String pType = parentId.getSobjectType().getDescribe().getName();
                if (pType == 'Case' || pType == 'VTD1_Chat__c') {
                    if (pType == 'Case') {
                        inboundCaseIds.add(parentId);
                    } else if (pType == 'VTD1_Chat__c') {
                        this.chatIds.add(parentId);
                    }
                    this.recsToProcess.add(item);
                }
            }
        }
        if (!inboundCaseIds.isEmpty()) {
            for (Case cs : [SELECT Id FROM Case WHERE Id IN :inboundCaseIds AND RecordType.Id IN :VT_D1_HelperClass.getRecordTypeContactForm()]) {
                this.caseIds.add(cs.Id);
            }
        }
    }

    private void getParentToPCFChatMemberIds() {
        List<VTD1_PCF_Chat_Member__c> items = (new VT_D1_FeedItemAndCommentNotifications()).getPCFChatMembers(caseIds);
        System.debug('Got VTD1_PCF_Chat_Members: ' + items);
        for (VTD1_PCF_Chat_Member__c item : items) {
            if (! this.parentToMemberIdsMap.containsKey(item.VTD1_PCF__c)) {
                this.parentToMemberIdsMap.put(item.VTD1_PCF__c, new List<Id>());
            }
            this.parentToMemberIdsMap.get(item.VTD1_PCF__c).add(item.Id);
        }
    }

    private void getParentToChatMemberIds() {
        if(chatIds.isEmpty())
            return;

        for (VTD1_Chat_Member__c item : [
                SELECT VTD1_User__c, VTD1_Chat__c
                FROM VTD1_Chat_Member__c
                WHERE VTD1_Chat__c IN :THIS.chatIds
                AND VTD1_User__r.Profile.Name IN :CHAT_PROFILES
        ]) {
            if (! this.parentToMemberIdsMap.containsKey(item.VTD1_Chat__c)) {
                this.parentToMemberIdsMap.put(item.VTD1_Chat__c, new List<Id>());
            }
            this.parentToMemberIdsMap.get(item.VTD1_Chat__c).add(item.Id);
        }
    }

    private void createFeedPostOnObject() {
        try {
            List<SObject> feedPostsOnObject = new List<SObject>();
            for (FeedItem item : this.recsToProcess) {
                Id parentId = item.ParentId;
                if (parentId != null) {
                    String pType = parentId.getSobjectType().getDescribe().getName();
                    if (pType == 'Case') {
                        for (Id memberId : this.parentToMemberIdsMap.get(parentId)) {
                            VTD2_Feed_Post_on_PCF__c fp = new VTD2_Feed_Post_on_PCF__c();
                            fp.VTD2_Feed_Element__c = item.Id;
                            fp.VTD2_PCF_Chat_Member__c = memberId;
                            feedPostsOnObject.add(fp);
                        }
                    } else if (pType == 'VTD1_Chat__c') {
                        for (Id memberId : this.parentToMemberIdsMap.get(parentId)) {
                            VTD2_Feed_Post_on_Chat__c fp = new VTD2_Feed_Post_on_Chat__c();
                            fp.VTD2_Feed_Element__c = item.Id;
                            fp.VTD2_Chat_Member__c = memberId;
                            feedPostsOnObject.add(fp);
                        }
                    }
                }
            }
            if (!feedPostsOnObject.isEmpty()) {
                insert feedPostsOnObject;
                System.debug('feedPostsOnObject: ' + feedPostsOnObject);
            }
        } catch (Exception e) {
            System.debug(e.getMessage() + ' ' + e.getStackTraceString());
        }
    }
}