public with sharing class VT_D1_PIMyPatientsController {

    public class Patient {
        public Case cas;
        public HealthCloudGA__CandidatePatient__c cand;
        public String study;
        public String firstName;
        public String lastName;
        public String status;
        public String initials;
        public Integer qtySTM;
        public String visitName;
        public Datetime visitDate;
        public String visitStatus;
        public VTD1_Actual_Visit__c visit;
        public String phone;

        /*public Patient(Case cas) {
            this.cas = cas;
            this.study = cas.VTD1_Study__r.Name;
            this.firstName = getNonNullString(cas.VTD1_Patient__r.VTD1_First_Name__c);
            this.lastName = getNonNullString(cas.VTD1_Patient__r.VTD1_Last_Name__c);
            this.status = cas.Status;
            this.initials = getInitials(this.firstName, this.lastName);
        }*/

        public Patient(VTD1_Actual_Visit__c visit) {
            this.cas = visit.VTR5_PatientCaseId__r;
            this.visit = visit;
            this.phone = visit.VTR5_PatientCaseId__r.Contact.VTR5_PatientPhone__c;
            this.study = visit.VTR5_PatientCaseId__r.VTD1_Study__r.Name;
            this.firstName = getNonNullString(visit.VTR5_PatientCaseId__r.VTD1_Patient__r.VTD1_First_Name__c);
            this.lastName = getNonNullString(visit.VTR5_PatientCaseId__r.VTD1_Patient__r.VTD1_Last_Name__c);
            this.status = visit.VTR5_PatientCaseId__r.Status;
            this.visitName = visit.Name;
            this.visitDate = visit.VTD1_Scheduled_Date_Time__c;
            this.visitStatus = visit.VTD1_Status__c;
            this.initials = getInitials(this.firstName, this.lastName);
        }

        /*public Patient(HealthCloudGA__CandidatePatient__c cand) {
            this.cand = cand;
            this.study = cand.Study__r.Name;
            this.firstName = getNonNullString(cand.rr_firstName__c);
            this.lastName = getNonNullString(cand.rr_lastName__c);
            this.status = cand.Conversion__c;
            this.initials = getInitials(this.firstName, this.lastName);
        }*/

        private String getNonNullString(String s) {
            return s != null ? s : '';
        }

        private String getInitials(String firstName, String lastName){
            return (((firstName != null&&firstName > '') ? firstName.substring(0,1) : '') + ((lastName != null&&lastName > '') ? lastName.substring(0,1) : '')).toUpperCase();
        }


    }

    public class StudyTeamMember {
        public Study_Team_Member__c stm;
        public String name;
        public String role;
        public String email;
        public String initials;
        public String photo;

        public StudyTeamMember(Study_Team_Member__c stm) {
            this.stm = stm;
            this.name = stm.User__r.Name;
            this.role = stm.VTD1_Type__c;
            this.email = stm.User__r.VTD2_Email_Formula__c;
            this.initials = getInitials(stm.User__r.FirstName,stm.User__r.LastName);
            this.photo = stm.User__r.FullPhotoUrl;
        }
        private String getInitials(String firstName, String lastName){
            return (((firstName != null&&firstName > '') ? firstName.substring(0,1) : '') + ((lastName != null&&lastName > '') ? lastName.substring(0,1) : '')).toUpperCase();
        }
    }
    public class ColumnWrapper{
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String columnName;
        @AuraEnabled
        public Boolean checked;

        public ColumnWrapper(String label, String columnName, Boolean checked){
            this.label = label;
            this.columnName = columnName;
            this.checked = checked;
        }
    }

    public class PicklistValues {
        public List<ColumnWrapper> visitStatuses;
        public List<ColumnWrapper> patientStatuses;

        public  PicklistValues() {
            this.visitStatuses = getVisitStatuses();
            this.patientStatuses = getPatientStatuses();
        }

        private List<ColumnWrapper> getVisitStatuses(){
            List<ColumnWrapper> visitStatuses = new List<ColumnWrapper>();
            for (Schema.PicklistEntry picklistItem : VTD1_Actual_Visit__c.VTD1_Status__c.getDescribe().getPicklistValues()) {
                visitStatuses.add(new ColumnWrapper(picklistItem.getLabel(), picklistItem.getValue(), false));
            }
            return visitStatuses;

        }
        private List<ColumnWrapper> getPatientStatuses(){
            List<ColumnWrapper> patientStatuses = new List<ColumnWrapper>();
            for (Schema.PicklistEntry picklistItem : Case.Status.getDescribe().getPicklistValues()) {
                patientStatuses.add(new ColumnWrapper(picklistItem.getLabel(), picklistItem.getValue(), false));
            }
            return patientStatuses;
        }
    }
    @AuraEnabled(Cacheable=true)
    public static String getPickListValues() {
        PicklistValues values = new PicklistValues();
        return JSON.serialize(values);
    }

    @AuraEnabled(Cacheable=true)
    public static String getMyPatients(Id studyId, String jsonParams) {
        MyPatientsFactory factory = new MyPatientsFactory();
        factory.addAlgorithm(new PiMyPatientsSelector());
        factory.addAlgorithm(new ScrMyPatientsSelector());
        return factory.getMyPatients(studyId, jsonParams);
    }

    @AuraEnabled(Cacheable=true)
    public static String getStudyTeamMembers(Id primaryPiId) {
        MyPatientsFactory factory = new MyPatientsFactory();
        factory.addAlgorithm(new PiMyPatientsSelector());
        factory.addAlgorithm(new ScrMyPatientsSelector());
        return factory.getStudyTeamMembers(primaryPiId);
    }

    public class MyPatientsFactory {

        public List<MyPatientsSelector> algorithms;

        public MyPatientsFactory() {
            this.algorithms = new List<MyPatientsSelector>();
        }

        public void addAlgorithm(MyPatientsSelector myPatients) {
            this.algorithms.add(myPatients);
        }

        public String getMyPatients(String studyId, String jsonParams) {
            VT_R5_PagingQueryHelper.ResultWrapper result = new VT_R5_PagingQueryHelper.ResultWrapper();
            for (MyPatientsSelector algorithm : this.algorithms) {
                if (algorithm.isRequiredProfile()) {
                    result = algorithm.getMyPatients(studyId, jsonParams);
                    break;
                }
            }
            return JSON.serialize(result);
        }

        public String getStudyTeamMembers(String primaryPiId) {
            List<StudyTeamMember> result = new List<StudyTeamMember>();
            for (MyPatientsSelector algorithm : this.algorithms) {
                if (algorithm.isRequiredProfile()) {
                    result = algorithm.getStudyTeamMembers(primaryPiId);
                    break;
                }
            }
            return JSON.serialize(result);
        }

    }

    interface MyPatientsSelector {

        Boolean isRequiredProfile();
        VT_R5_PagingQueryHelper.ResultWrapper getMyPatients(Id studyId, String jsonParams);
        List<StudyTeamMember> getStudyTeamMembers(Id primaryPiId);
    }

    public abstract class ProfileMyPatientsSelector implements MyPatientsSelector {

        protected List<Id> myStudyIds;

        public Boolean isRequiredProfile() {
            return UserInfo.getProfileId() == this.getProfileId();
        }

        protected abstract Id getProfileId();

        protected abstract VT_R5_PagingQueryHelper.ResultWrapper getPatients(String studyId, String jsonParams);

        protected virtual List<HealthCloudGA__CandidatePatient__c> getCandidatePatients() {
            return new List<HealthCloudGA__CandidatePatient__c>();
        }

        public VT_R5_PagingQueryHelper.ResultWrapper getMyPatients(Id studyId, String jsonParams) {
            try {
                List<Patient> myPatientList = new List<Patient>();
                VT_R5_PagingQueryHelper.ResultWrapper wrapperCases = this.getPatients(studyId, jsonParams);
                List<VTD1_Actual_Visit__c> visitList = (List<VTD1_Actual_Visit__c>)wrapperCases.records;
                Set<Id> primaryPiIds = new Set<Id>();
                Set<Id> caseIds = new Set<Id>();
                for (VTD1_Actual_Visit__c visit : visitList) {
                    caseIds.add(visit.VTR5_PatientCaseId__c);
                    //patientUserIdList.add(cas.VTD1_Patient_User__c);
                    if (visit.VTR5_PatientCaseId__r.VTD1_Virtual_Site__c != null && visit.VTR5_PatientCaseId__r.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c != null) {
                        primaryPiIds.add(visit.VTR5_PatientCaseId__r.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c);
                    }
                }
                //Map<Id, String> phoneByCaseMap = getPhoneByCaseMap(caseIds);
                Map<Id,Integer> qtySTMMap = this.getSTMQuantity(primaryPiIds);

                //Map<Id, User> userIdToUserMap = new Map<Id, User>([SELECT Id, FullPhotoUrl FROM User WHERE Id IN :patientUserIdList]);

                for (VTD1_Actual_Visit__c visit : visitList) {
                    Patient myPatient = new Patient(visit);
                    //myPatient.u = userIdToUserMap.get(cas.VTD1_Patient_User__c);
                   // myPatient.phone = phoneByCaseMap.get(visit.VTD1_Case__c);
                    myPatient.qtySTM = 0;
                    if (visit.VTR5_PatientCaseId__r.VTD1_Virtual_Site__c != null) {
                        Id primaryPiId = visit.VTR5_PatientCaseId__r.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c;
                        if (primaryPiId != null) {
                            myPatient.qtySTM = 1; // has primary PI
                            if (visit.VTR5_PatientCaseId__r.VTD1_Virtual_Site__r.VTR2_Backup_PI_STM__c != null
                                    && visit.VTR5_PatientCaseId__r.VTD1_Virtual_Site__r.VTR2_Backup_PI_STM__r.Study_Team_Member__c == primaryPiId) {
                                myPatient.qtySTM += 1; //has BackUP PI
                            }
                            if (qtySTMMap.get(primaryPiId) != null) {
                                myPatient.qtySTM += Integer.valueOf(qtySTMMap.get(primaryPiId)); //has other STMs
                            }
                        }
                    }
                    myPatientList.add(myPatient);
                }

                VT_R5_PagingQueryHelper.ResultWrapper wrapperPatients = new VT_R5_PagingQueryHelper.ResultWrapper();
                wrapperPatients.count = wrapperCases.count;
                wrapperPatients.countByGroup = wrapperCases.countByGroup;
                wrapperPatients.records = myPatientList;
                return wrapperPatients;
            } catch (Exception e) {
                throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
            }

        }

        public Map<Id,Integer> getSTMQuantity(Set<Id> primaryPiIds) {
            Map<Id,Integer> stmMap = new Map<Id,Integer>();
            List<AggregateResult> aggregateResults = [SELECT VTD1_Associated_PI__c pi_pg,
                    VTR2_Associated_PI__c pi_scr,
                    VTR2_Associated_PI3__c pi_subi,
                    COUNT(VTD1_Associated_PG__c) cntPG,
                    COUNT(VTR2_Associated_SCr__c) cntSCR,
                    COUNT(VTR2_Associated_SubI__c) cntSubI
            FROM Study_Site_Team_Member__c
            WHERE (VTD1_Associated_PI__c IN :primaryPiIds AND VTD1_Associated_PG__r.User__r.IsActive = TRUE)
            OR (VTR2_Associated_PI__c IN :primaryPiIds AND VTR2_Associated_SCr__r.User__r.IsActive = TRUE)
            OR (VTR2_Associated_PI3__c IN :primaryPiIds AND VTR2_Associated_SubI__r.User__r.IsActive = TRUE)
            GROUP BY VTD1_Associated_PI__c, VTR2_Associated_PI__c, VTR2_Associated_PI3__c];

            for (AggregateResult ar: aggregateResults) {
                Id pi_pg = (Id) ar.get('pi_pg');
                Id pi_scr = (Id) ar.get('pi_scr');
                Id pi_subi = (Id) ar.get('pi_subi');

                if (pi_pg!=null) {
                    Integer cntPG = stmMap.containsKey(pi_pg)? stmMap.get(pi_pg) : 0;
                    stmMap.put(pi_pg, cntPG + (Integer) ar.get('cntPG'));
                } else if (pi_scr!=null) {
                    Integer cntSCR = stmMap.containsKey(pi_scr)? stmMap.get(pi_scr) : 0;
                    stmMap.put(pi_scr, cntSCR + (Integer) ar.get('cntSCR'));
                } else if (pi_subi!=null) {
                    Integer cntSubI = stmMap.containsKey(pi_subi)? stmMap.get(pi_subi) : 0;
                    stmMap.put(pi_subi, cntSubI + (Integer) ar.get('cntSubI'));
                }
            }
            return stmMap;
        }

        public List<StudyTeamMember> getStudyTeamMembers(Id primaryPiId) {
            List<StudyTeamMember> studyTeamMembers = new List<StudyTeamMember>();

            Set<Id> studyTeamMemberIds = this.getStudyTeamMemberIds(primaryPiId);
            try {
                List<Study_Team_Member__c> stmList = [SELECT User__r.Name,
                        User__r.LastName,
                        User__r.FirstName,
                        User__r.FullPhotoUrl,
                        User__r.VTD2_Email_Formula__c,
                        VTD1_Type__c,
                        Study_Team_Member__c
                FROM Study_Team_Member__c
                WHERE Study_Team_Member__c = :primaryPiId
                OR Id IN :studyTeamMemberIds
                ORDER BY User__r.Name];

                List<StudyTeamMember> ListPI = new List<StudyTeamMember>();
                List<StudyTeamMember> ListSCR = new List<StudyTeamMember>();
                List<StudyTeamMember> ListPG = new List<StudyTeamMember>();
                for (Study_Team_Member__c stm : stmList) {
                    if(stm.VTD1_Type__c == 'Primary Investigator'){
                        ListPI.add(new StudyTeamMember(stm));
                    } else if (stm.VTD1_Type__c == 'Site Coordinator'){
                        ListSCR.add(new StudyTeamMember(stm));
                    } else if (stm.VTD1_Type__c == 'Patient Guide'){
                        ListPG.add(new StudyTeamMember(stm));
                    }
                }
                studyTeamMembers.addAll(ListPI);
                studyTeamMembers.addAll(ListSCR);
                studyTeamMembers.addAll(ListPG);

                return studyTeamMembers;
            }
            catch (Exception e){
                throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
            }
        }

        public Set<Id> getStudyTeamMemberIds(Id stmId) {
            Set<Id> stmIds = new Set<Id>();
            if (stmId==null) {
                return stmIds;
            }
            stmIds.add(stmId);
            for (Study_Site_Team_Member__c item : [
                    SELECT VTD1_Associated_PI__c, VTD1_Associated_PG__c,
                            VTR2_Associated_PI__c, VTR2_Associated_SCr__c,
                            VTR2_Associated_PI3__c, VTR2_Associated_SubI__c
                    FROM Study_Site_Team_Member__c
                    WHERE (VTD1_Associated_PI__c = :stmId AND VTD1_Associated_PG__r.User__r.IsActive = TRUE)
                    OR (VTR2_Associated_PI__c = :stmId AND VTR2_Associated_SCr__r.User__r.IsActive = TRUE)
                    OR (VTR2_Associated_PI3__c = :stmId AND VTR2_Associated_SubI__r.User__r.IsActive = TRUE)
            ])
            {
                if (item.VTD1_Associated_PG__c != null) {
                    stmIds.add(item.VTD1_Associated_PG__c);
                } else if (item.VTR2_Associated_SCr__c != null) {
                    stmIds.add(item.VTR2_Associated_SCr__c);
                } else if (item.VTR2_Associated_SubI__c != null) {
                    stmIds.add(item.VTR2_Associated_SubI__c);
                }
            }
            return stmIds;
        }
    }

    public with sharing class PiMyPatientsSelector extends ProfileMyPatientsSelector {

        protected override Id getProfileId() {
            return [SELECT Id FROM Profile WHERE Name = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME][0].Id;
        }

        protected override VT_R5_PagingQueryHelper.ResultWrapper getPatients(String studyId, String jsonParams) {
            myStudyIds = VT_D1_PIStudySwitcherRemote.getMyStudyIds(studyId);
            Map <String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(jsonParams);//new Map<String, Object>();
            String dateFilter = '';
            String startDate = (String)params.get('startDate');
            String endDate = (String)params.get('endDate');
            if(startDate <= endDate || (startDate != null && endDate == null) || (startDate == null && endDate != null)) {
                if (startDate != null) {
                    Datetime startDateTime = Datetime.newInstance(Date.valueOf(startDate), Time.newInstance(0, 0, 0, 0));
                    dateFilter = 'VTD1_Scheduled_Date_Time__c > ' + startDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ');
                }
                if (endDate != null) {
                    if (dateFilter != '') {
                        dateFilter = dateFilter + ' AND ';
                    }
                    Datetime endDateTime = Datetime.newInstance(Date.valueOf(endDate), Time.newInstance(23, 59, 0, 0));
                    dateFilter = dateFilter + 'VTD1_Scheduled_Date_Time__c < ' + endDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ');
                }
            }
            String query = 'SELECT Id,' +
                    'VTD1_Case__c,' +
                    'Name,' +
                    'VTD1_Status__c,' +
                    'VTD1_Scheduled_Date_Time__c,' +
                    'VTR5_PatientCaseId__r.Contact.VTR5_PatientPhone__c,' +
                    'VTR5_PatientCaseId__r.VTD1_Patient_User__c,' +
                    'VTR5_PatientCaseId__r.VTD1_Patient_User__r.SmallPhotoUrl,' +
                    'VTR5_PatientCaseId__r.VTD1_Patient_User__r.MediumPhotoUrl,' +
                    'VTR5_PatientCaseId__r.VTD1_Patient__c,' +
                    'VTR5_PatientCaseId__r.VTD1_Patient__r.VTD1_First_Name__c,' +
                    'VTR5_PatientCaseId__r.VTD1_Patient__r.VTD1_Last_Name__c,' +
                    'VTR5_PatientCaseId__r.VTD1_Subject_ID__c,' +
                    'VTR5_PatientCaseId__r.VTD1_Study__r.Name,' +
                    'VTR5_PatientCaseId__r.VTD1_Reconsent_Needed__c,' +
                    'VTR5_PatientCaseId__r.Status,' +
                    'VTR5_PatientCaseId__r.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c,' + // Primary PI
                    'VTR5_PatientCaseId__r.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__r.User__r.Name,' +
                    'VTR5_PatientCaseId__r.VTD1_Virtual_Site__r.VTR2_Backup_PI_STM__c,' + //Backup PI
                    'VTR5_PatientCaseId__r.VTD1_Virtual_Site__r.VTR2_Backup_PI_STM__r.Study_Team_Member__c FROM VTD1_Actual_Visit__c';

            String filter = 'VTR5_PatientCaseId__r.RecordTypeId = \'' + VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN + '\'' +
                    ' AND VTR5_PatientCaseId__r.VTD1_Patient__c != NULL' +
                    ' AND VTR5_PatientCaseId__r.VTR4_CandidatePatient__r.Conversion__c = \'' + VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_CONVERTED + '\'' +
                    ' AND VTR5_PatientCaseId__r.VTD1_Study__c IN (\'' + String.join(myStudyIds, '\',\'') + '\')';
            params.put('groupBy', 'VTR5_PatientCaseId__r.Status,VTR5_PatientCaseId__r.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__r.User__r.Name');
            String clientFilter = (String)params.get('filter');
            if(dateFilter != ''){
                filter = filter + ' AND ' + dateFilter;
            }
            if (clientFilter == null) {
                params.put('filter', filter);
            } else {
                params.put('filter', filter + ' and ' + clientFilter);
            }

            String sObjectName = (String)params.get('sObjectName');
            VT_R5_PagingQueryHelper.ResultWrapper wrapper = VT_R5_PagingQueryHelper.query(query, sObjectName, JSON.serialize(params));
            return wrapper;
        }
    }

    public with sharing class ScrMyPatientsSelector extends ProfileMyPatientsSelector {

        protected override Id getProfileId() {
            return [SELECT Id FROM Profile WHERE Name = :VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME][0].Id;
        }

        protected override VT_R5_PagingQueryHelper.ResultWrapper getPatients(String studyId, String jsonParams) {
            myStudyIds = VT_D1_PIStudySwitcherRemote.getMyStudyIds(studyId);
            Map <String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(jsonParams);//new Map<String, Object>()
            String dateFilter = '';
            String startDate = (String)params.get('startDate');
            String endDate = (String)params.get('endDate');
            if(startDate <= endDate || (startDate != null && endDate == null) || (startDate == null && endDate != null)) {
                if (startDate != null) {
                    Datetime startDateTime = Datetime.newInstance(Date.valueOf(startDate), Time.newInstance(0, 0, 0, 0));
                    dateFilter = 'VTD1_Scheduled_Date_Time__c > ' + startDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ');
                }
                if (endDate != null) {
                    if (dateFilter != '') {
                        dateFilter = dateFilter + ' AND ';
                    }
                    Datetime endDateTime = Datetime.newInstance(Date.valueOf(endDate), Time.newInstance(23, 59, 0, 0));
                    dateFilter = dateFilter + 'VTD1_Scheduled_Date_Time__c < ' + endDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ');
                }
            }
            String query = 'SELECT Id,' +
                    'VTD1_Case__c,' +
                    'Name,' +
                    'VTD1_Status__c,' +
                    'VTD1_Scheduled_Date_Time__c,' +
                    'VTR5_PatientCaseId__r.Contact.VTR5_PatientPhone__c,' +
                    'VTR5_PatientCaseId__r.VTD1_Patient_User__c,' +
                    'VTR5_PatientCaseId__r.VTD1_Patient_User__r.SmallPhotoUrl,' +
                    'VTR5_PatientCaseId__r.VTD1_Patient_User__r.MediumPhotoUrl,' +
                    'VTR5_PatientCaseId__r.VTD1_Patient__c,' +
                    'VTR5_PatientCaseId__r.VTD1_Patient__r.VTD1_First_Name__c,' +
                    'VTR5_PatientCaseId__r.VTD1_Patient__r.VTD1_Last_Name__c,' +
                    'VTR5_PatientCaseId__r.VTD1_Subject_ID__c,' +
                    'VTR5_PatientCaseId__r.VTD1_Study__r.Name,' +
                    'VTR5_PatientCaseId__r.VTD1_Reconsent_Needed__c,' +
                    'VTR5_PatientCaseId__r.Status,' +
                    'VTR5_PatientCaseId__r.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c,' +
                    'VTR5_PatientCaseId__r.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__r.User__r.Name,' +
                    'VTR5_PatientCaseId__r.VTD1_Virtual_Site__r.VTR2_Backup_PI_STM__c,' +
                    'VTR5_PatientCaseId__r.VTD1_Virtual_Site__r.VTR2_Backup_PI_STM__r.Study_Team_Member__c' +  //Primary PI
                    ' FROM VTD1_Actual_Visit__c';
            String filter = 'VTR5_PatientCaseId__r.RecordTypeId = \'' + VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN + '\'' +
                    ' AND VTR5_PatientCaseId__r.VTR4_CandidatePatient__r.Conversion__c = \'' + VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_CONVERTED + '\'' +
                    ' AND VTR5_PatientCaseId__r.VTD1_Patient__c != NULL AND VTR5_PatientCaseId__r.VTD1_Study__c IN (\'' + String.join(myStudyIds, '\',\'') + '\')';
            //String order = 'VTR5_PatientCaseId__r.VTD1_Patient__r.VTD1_First_Name__c, VTR5_PatientCaseId__r.VTD1_Patient__r.VTD1_Last_Name__c';
            params.put('groupBy', 'VTR5_PatientCaseId__r.Status,VTR5_PatientCaseId__r.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__r.User__r.Name');
            String clientFilter = (String)params.get('filter');
            if(dateFilter != ''){
                filter = filter + ' AND ' + dateFilter;
            }
            if (clientFilter == null) {
                params.put('filter', filter);
            } else {
                params.put('filter', filter + ' and ' + clientFilter);
            }
            String sObjectName = (String)params.get('sObjectName');
            VT_R5_PagingQueryHelper.ResultWrapper wrapper = VT_R5_PagingQueryHelper.query(query, sObjectName, JSON.serialize(params));
            return wrapper;
        }

       /* protected override List<HealthCloudGA__CandidatePatient__c> getCandidatePatients() {
            return [
                    SELECT Id, Study__r.Name, rr_firstName__c, rr_lastName__c, Conversion__c
                    FROM HealthCloudGA__CandidatePatient__c
                    WHERE Study__c = :myStudyIds
                    AND Conversion__c != :VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_CONVERTED
                    AND (VTR2_Created_By_User__c = :UserInfo.getUserId() OR CreatedById = :UserInfo.getUserId())
            ];
        }*/
    }
}