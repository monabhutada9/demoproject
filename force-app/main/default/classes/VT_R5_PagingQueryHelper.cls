/**
 * Created by user on 23.07.2020.
 */

public with sharing class VT_R5_PagingQueryHelper {
    public class ResultWrapper {
        @AuraEnabled
        public String offset;
        @AuraEnabled
        public Integer count = -1;
        @AuraEnabled
        public Map <String, Map <String, Integer>> countByGroup;
        @AuraEnabled
        public List <Object> records;
    }
    public static Datetime dateTimeNow = Datetime.now();
    public static ResultWrapper query(String query, String sObjectName, String jsonParams) {
        ResultWrapper wrapper = new ResultWrapper();
        Map <String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(jsonParams);
        String filter = (String) params.get('filter');
        String order = (String) params.get('order');
        String groupBySet = (String) params.get('groupBy');
        String includeEmptyGroupByKey = (String) params.get('includeEmptyGroupByKey');
        if (sObjectName != null) {
            if (groupBySet == null) {
                String countQuery = 'select count(Id) num from ' + sObjectName + (filter != null ? ' where ' + filter : '');
                System.debug('countQuery = ' + countQuery);
                AggregateResult [] results = Database.query(countQuery);
                wrapper.count = (Integer) results[0].get('num');
                System.debug('wrapper.count = ' + wrapper.count);
            } else {
                wrapper.countByGroup = new Map<String, Map<String, Integer>>();
                List <String> groupByParts = groupBySet.split(',');
                Integer i = 1;
                for (String groupBy : groupByParts) {
                    String countQuery = 'select count(Id) num,' + groupBy + ' value' + i + ' from ' + sObjectName + (filter != null ? ' where ' + filter + (includeEmptyGroupByKey == null ? ' and ' + groupBy + ' != null' : '') : (includeEmptyGroupByKey == null ? ' where ' + groupBy + ' != null' : '')) + ' group by ' + groupBy + ' order by ' + groupBy;
                    System.debug('countQuery = ' + countQuery);
                    AggregateResult [] results = Database.query(countQuery);
                    Map <String, Integer> countByGroup = new Map<String, Integer>();
                    for (AggregateResult result : results) {
                        countByGroup.put((String) result.get('value' + i), (Integer) result.get('num'));
                    }
                    wrapper.countByGroup.put(groupBy, countByGroup);
                    i ++;
                }
            }
        }
        String offset = (String) params.get('offset');
        wrapper.offset = offset;
        String lim = (String) params.get('limit');
        List <SObject> skippedRecords = new List<SObject>();
        if (offset != '0') {
            String queryToSkip = query + (filter != null ? ' where ' + filter : '') + (order != null ? ' order by ' + order : '') + ' limit ' + offset;
            System.debug('queryToSkip = ' + queryToSkip);
            skippedRecords = Database.query(queryToSkip);
        }
        List <String> whereParts = new List<String>();
        if (!skippedRecords.isEmpty()) {
            List <String> idNotInParts = new List<String>();
            for (SObject sobj : skippedRecords) {
                idNotInParts.add(sobj.Id);
            }
            whereParts.add('Id not in (\'' + String.join(idNotInParts, '\',\'') + '\')');
        }
        if (filter != null) {
            whereParts.add(filter);
        }
        if (!whereParts.isEmpty()) {
            query = query + ' where ' + String.join(whereParts, ' and ');
        }
        query = query + (order != null ? ' order by ' + order : '') + ' limit ' + lim;
        System.debug('query = ' + query);
        wrapper.records = Database.query(query);
        return wrapper;
    }
}