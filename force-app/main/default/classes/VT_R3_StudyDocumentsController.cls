public class VT_R3_StudyDocumentsController {
    public class StudyDocumentsWrapper {
        @AuraEnabled public List<DocumentWrapper> documentsList { get; set; }
        @AuraEnabled public Integer totalPages { get; set; }
        @AuraEnabled public List<VisitFilterWrapper> visitsList { get; set; }
    }

    public class DocumentWrapper {
        @AuraEnabled public VTD1_Document__c document { get; set; }
        @AuraEnabled public Boolean editable { get; set;  }

        public DocumentWrapper(VTD1_Document__c doc,  Boolean edit){
            document = doc;
            editable = edit;
        }

    }

    public class  VisitFilterWrapper {
        @AuraEnabled public String label { get; set; }
        @AuraEnabled public String value { get; set;    }

        public VisitFilterWrapper(String l, String v){
            label = l;
            value = v;
        }
    }

    @AuraEnabled
    public static StudyDocumentsWrapper getStudyDocuments(Id caseId ,String querySettingsString) {
        system.debug('-444 ');
        system.debug('-444 ' + querySettingsString);
        StudyDocumentsWrapper ppWrapper = new StudyDocumentsWrapper();
        try {
            VT_TableHelper.TableQuerySettings querySettings = VT_TableHelper.parseQuerySettings(querySettingsString);
            ppWrapper.documentsList = getDocumentsList(caseId, querySettings);
            ppWrapper.totalPages = getTotalPages(caseId, querySettings);
            ppWrapper.visitsList = getVisitsList(caseId);
            system.debug('-444 ' + ppWrapper);
            return ppWrapper;
        }catch (Exception e) {
            system.debug('-4442 ');
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    static Map<String,Id> docRT = VT_D1_HelperClass.getRTMap().get(VT_R4_ConstantsHelper_Documents.SOBJECT_DOC_CONTAINER);
    static List<Id> medicalRecordTypes = new List<Id>{
            docRT.get('Medical_Record_Archived'),
            docRT.get('Medical_Record_Release_Form'),
            docRT.get('Medical_Record_Rejected'),
            docRT.get('VTD1_Medical_Record')
    };

    static List<Id> visitDocumentRecordTypes = new List<Id>{
            docRT.get('VTR3_VisitDocument')
    };

    private static List<DocumentWrapper> getDocumentsList(Id caseId, VT_TableHelper.TableQuerySettings querySettings) {
        String queryFilter = (querySettings.queryFilter != null && querySettings.queryFilter != '') ? ' AND ' + querySettings.queryFilter : '';
        String queryOffset = (querySettings.queryOffset != null && querySettings.queryOffset != '') ? ' ' + querySettings.queryOffset : '';
        String queryLimit = (querySettings.queryLimit != null && querySettings.queryLimit != '') ? ' ' + querySettings.queryLimit : '';
        String query = '';
        String filetLogicCondition ='';
        String profileName = VT_D1_HelperClass.getProfileNameOfCurrentUser();
        /*SH-15990-Visit documents should not be visible to the patients/caregiver uploaded by other user
         if VTR5_Hide_Study_Documents_For_Patients__c is true on thier study */
        String patientId;
        String caregiverId;
        String studyDocFilterLogic ='';
        if(profileName == 'Patient'){
            patientId = UserInfo.getUserId();
            String filterLogicforPT='';
            String filterLogicforCare ='';
            if(hideStudyDocumentForPTCar(patientId)==true){
                  filterLogicforPT = '(CreatedById = :patientId)';
                  caregiverId = VT_D1_PatientCaregiverBound.findCaregiverOfPatient(patientId);
                  if(String.isNotBlank(caregiverId)){
                       filterLogicforCare = ' OR(CreatedById = :caregiverId )';
                   }
                  studyDocFilterLogic ='AND('+filterLogicforPT + filterLogicforCare +')';      
            }             
        }
        if(profileName =='Caregiver'){
            caregiverId = UserInfo.getUserId();
            patientId = VT_D1_PatientCaregiverBound.findPatientOfCaregiver(caregiverId);
             System.debug('document filter cond : '+patientId+'  ==' + hideStudyDocumentForPTCar(patientId));
            if(String.isNotBlank(patientId) && hideStudyDocumentForPTCar(patientId)==true){
                studyDocFilterLogic ='AND (CreatedById = :caregiverId OR CreatedById = :patientId)';
            }
          
       }  
 
        query = 'SELECT Id, Name, VTD1_FileNames__c, VTD1_Nickname__c, VTD1_Comment__c, VTD1_Version__c, ' +
                    'VTD1_Status__c, toLabel(VTD1_Status__c) translatedStatus, VTD1_Does_file_needs_deletion__c, ' +
                    'VTD1_Deletion_Date__c, VTD1_Deletion_User__c, VTD1_Deletion_Reason__c, ' +
                    'VTD1_Why_File_is_Relevant__c, VTD1_Why_file_is_irrelevant__c, VTD1_PI_Comment__c, ' +
                    'CreatedBy.FirstName ,CreatedById, CreatedBy.LastName , CreatedDate, VTR3_Associated_Visit__c, ' +
                    'VTR3_Associated_Visit__r.Name, VTR3_Category__c,VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_Hide_Study_Documents_For_Patients__c, VTD1_Clinical_Study_Membership__c, OwnerId ' +
                'FROM VTD1_Document__c ' +
                'WHERE ' + ((caseId == null || String.valueOf(caseId).length() <1 )? ' ' : ' VTD1_Clinical_Study_Membership__c = :caseId AND ' )
                +' (((RecordTypeId IN :medicalRecordTypes) AND (NOT (VTD1_Does_file_needs_deletion__c = \'Yes\' AND VTD1_Deletion_Reason__c = \'Not Relevant\'))) '
                +      'OR (RecordTypeId IN :visitDocumentRecordTypes)) AND (NOT(Owner.Name = \'API User\' AND VTD1_Status__c = \'Pending Approval\')) ' + queryFilter +studyDocFilterLogic
                +' ORDER BY CreatedDate DESC'
                + queryLimit + queryOffset;

        System.debug('YY queryString: ' + query);
        String patientOfCaregiverId = VT_D1_PatientCaregiverBound.findPatientOfCaregiver(UserInfo.getUserId());
        
        List<DocumentWrapper> documentWrapperList = new List<DocumentWrapper>();
        Boolean editable;
        List<VTD1_Document__c> documents = Database.query(query);
        System.debug('YY documentList: ' + documents);
        VT_D1_TranslateHelper.translate(documents);
        for ( VTD1_Document__c document: documents) {
            if ((profileName == 'Caregiver' && (document.OwnerId != patientOfCaregiverId && document.OwnerId != UserInfo.getUserId()))
               ||(profileName == 'Patient' && document.OwnerId != UserInfo.getUserId()))
            {
                editable = false;

            }else{
                editable = true;
            }

              documentWrapperList.add(new DocumentWrapper(document,editable));

        }
        System.debug('YY documentWrapperList: ' + documentWrapperList);
        return documentWrapperList;
    }

    private static Integer getTotalPages(Id caseId, VT_TableHelper.TableQuerySettings querySettings) {
        String queryFilter = (querySettings.queryFilter != null && querySettings.queryFilter != '') ? ' AND ' + querySettings.queryFilter : '';
        Integer entriesOnPage = (querySettings.entriesOnPage != null && querySettings.entriesOnPage != 0) ? querySettings.entriesOnPage : 100 ;
        System.debug('queryFilter: '+queryFilter);
        String query = 'SELECT Count(Id) FROM VTD1_Document__c  WHERE VTD1_Clinical_Study_Membership__c = :caseId '
                +'AND (((RecordTypeId IN :medicalRecordTypes) AND (NOT (VTD1_Does_file_needs_deletion__c = \'Yes\' AND VTD1_Deletion_Reason__c = \'Not Relevant\'))) '
                +      'OR (RecordTypeId IN :visitDocumentRecordTypes)) '
                + queryFilter;

        AggregateResult[] ar = Database.query(query);
        System.debug('ar'+ar);
        Integer totalPages = (Integer) Math.ceil((Decimal) ar[0].get('expr0') / entriesOnPage);
        System.debug('totalPages:'+totalPages);
        return totalPages;
    }

    private static List<VisitFilterWrapper> getVisitsList(Id caseId) {

        List<VTD1_Document__c> documents =  [SELECT VTR3_Associated_Visit__c, VTR3_Associated_Visit__r.Name
                                            FROM VTD1_Document__c
                                            WHERE VTD1_Clinical_Study_Membership__c = :caseId AND VTR3_Associated_Visit__c <> NULL
        ];
        VT_D1_TranslateHelper.translate(documents);
        List<VisitFilterWrapper> visitFiltList = new List<VisitFilterWrapper>();
        Map<Id, String> associatedVisitNameById = new Map<Id, String>();
        for (VTD1_Document__c document : documents) {
            if (!associatedVisitNameById.containsKey(document.VTR3_Associated_Visit__c)) {
                associatedVisitNameById.put(document.VTR3_Associated_Visit__c, document.VTR3_Associated_Visit__r.Name);
            }
        }
        for (Id Id : associatedVisitNameById.keySet()) {
            visitFiltList.add(new VisitFilterWrapper(associatedVisitNameById.get(Id),(Id) Id));
        }
        System.debug('visitFiltList::: ' + visitFiltList);
        return visitFiltList;
    }
    @AuraEnabled
    public static Boolean PatientCaregiverProfileInfo(){
        
        if(UserInfo.getProfileId() == VTD1_RTId__c.getInstance().VTD2_Profile_Caregiver__c ||
            UserInfo.getProfileId() == VTD1_RTId__c.getInstance().VTD2_Profile_Patient__c){ 
              Boolean isHideStudyDocument = true;
			  return isHideStudyDocument;
           }
		   return false;   
    }
    @AuraEnabled
    public static Case getCase() {
        return VT_D1_PatientCaregiverBound.getPatientCase(UserInfo.getUserId());
    }

    @AuraEnabled(Cacheable=true)
    public static String getDocumentDownloadLink(Id recordId) {
        return System.Url.getSalesforceBaseUrl().toExternalForm() + VT_D1_HelperClass.getSandboxPrefix() + '/sfc/servlet.shepherd/document/download/' + getFileByRecord(recordId).ContentDocumentId;
    }

    @AuraEnabled
    public static ContentDocumentLink getFileByRecord(Id recordId) {
        ContentDocumentLink link = [
                SELECT ContentDocumentId
                FROM ContentDocumentLink
                WHERE LinkedEntity.Id=:recordId
        ];
        return link;
    }

    @AuraEnabled
    public static String certifyDoc(Id caseId, Id docToCertifyId) {
        return VT_D2_MedicalRecordsCertifyDocsHelper.certifyDoc(caseId, docToCertifyId);
    }

    private static boolean  hideStudyDocumentForPTCar(Id patientId){
        Boolean isHideStudentDocument = false;
        for(case cas :[Select id,VTD1_Study__r.VTR5_Hide_Study_Documents_For_Patients__c 
                      from case where VTD1_Patient_User__c =: patientId ]){
		               if(cas.VTD1_Study__r.VTR5_Hide_Study_Documents_For_Patients__c ==true){
                           isHideStudentDocument = true;		   
		                }		  
					  
	    }

	    return isHideStudentDocument==true?true:false;
   }
}