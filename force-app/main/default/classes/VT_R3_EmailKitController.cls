/**
 * Created by Dmitry Gavrilov on 13.09.2019.
 **/
public class VT_R3_EmailKitController {
    //private  List<Contact> currentContact;
    public  Id 		conId					{get;set;}
    public  Id 		pDId					{get;set;}
    //public  String  linkToStudyHub          {get; set;}
    public  String  template	            {get; set;}
    private String firstName;
    private String studyPhoneNumber;
    private VTD1_Order__c patientDelivery;
    private List<VTD1_Order__c> patientDeliveryList;
    private Contact con;
    public String language                   {get; set;}

    public Contact getContact(){
        System.debug('conId' + conId);
        con = conId != null ? [SELECT Id, FirstName FROM Contact WHERE Id = :conId LIMIT 1] : new Contact();
        return  con;
    }

    public VTD1_Order__c getPatientDelivery(){
        System.debug('pDId' + pDId);
        //change to list for translation helper
//        patientDelivery = [SELECT Id,Name,VTD2_Study_Nickname__c, VTD2_Study_Phone_Number__c FROM VTD1_Order__c WHERE Id = :pDId];
        patientDeliveryList = pDId != null ? [SELECT Id,VTD1_Protocol_Delivery__r.Name,VTD1_Case__r.VTD1_Study__r.VTD1_Protocol_Nickname__c, VTD2_Study_Phone_Number__c FROM VTD1_Order__c WHERE Id = :pDId] : new List<VTD1_Order__c>();
        if (patientDeliveryList.size()>0) {
            VT_D1_TranslateHelper.translate(patientDeliveryList, language);
            patientDelivery = patientDeliveryList[0];
        }
        return patientDelivery;
    }

    public String getFirstName(){
        //firstName = [select id, FirstName from Contact where Id = :conId limit 1].FirstName;
        firstName = getContact().FirstName;
        return firstName;
    }

    public String getSalutation(){
        String salutation = VT_D1_TranslateHelper.getLabelValue('VT_R2_VisitEmailSalutaion', language);
        return salutation.replace('#Visit_participant', getFirstName());
    }
    
    public String getStudyPhoneNumber(){
        studyPhoneNumber = getPatientDelivery().VTD2_Study_Phone_Number__c; //studyPhoneNumber;
        //return [select id, VTD2_Study_Phone_Number__c from vtd1_order__c where Id = :pDId].VTD2_Study_Phone_Number__c;
        return studyPhoneNumber;
    }
    public String getMessageBody(){
        String messagePattern;
        if (template == 'delivered'){
            messagePattern = VT_D1_TranslateHelper.getLabelValue('VTR3_Your_DeliveryName_for_Study_has_been_delivered_Login_confirm', language);
        }else if (template == 'readyshipped') {
            messagePattern = VT_D1_TranslateHelper.getLabelValue('VTR3_Your_DeliveryName_for_Study_is_ready_to_be_shipped', language);
        }else if (template == 'readyreturned') {
            messagePattern = VT_D1_TranslateHelper.getLabelValue('VTR3_Your_DeliveryName_for_Study_is_ready_to_be_returned', language);
        }else if (template == 'return') {
            messagePattern = VT_D1_TranslateHelper.getLabelValue('VTR3_Your_DeliveryName_for_Study_is_ready_to_be_returned_follow_instructions', language);
        }else if (template == 'shipped') {
            messagePattern = VT_D1_TranslateHelper.getLabelValue('VTR3_Your_DeliveryName_for_Study_has_been_shipped_You_can_view_delivery_details', language);
        }
        else if (template == 'delayed') {
            messagePattern = VT_D1_TranslateHelper.getLabelValue('VTR3_DeliveryHasBeenDelayed', language);
        }
        System.debug('template '+template);
        System.debug('messagePattern '+messagePattern);
        System.debug('messagePattern '+getPatientDelivery().VTD1_Protocol_Delivery__r.Name);
        System.debug('messagePattern '+getPatientDelivery().VTD1_Case__r.VTD1_Study__r.VTD1_Protocol_Nickname__c);

        messagePattern = messagePattern.replace('#DeliveryName',getPatientDelivery().VTD1_Protocol_Delivery__r.Name);
        if (getPatientDelivery().VTD1_Case__r.VTD1_Study__r.VTD1_Protocol_Nickname__c != null) {
            messagePattern = messagePattern.replace('#StudyNickname', getPatientDelivery().VTD1_Case__r.VTD1_Study__r.VTD1_Protocol_Nickname__c);
        }
        return messagePattern;
    }
}