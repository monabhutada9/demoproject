@IsTest
private class VT_D1_PatientFullProfileControllerTest {
	private static Case cas;
	private static User u;

	@testSetup
	static void setup() {
		Test.startTest();
		HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
		VT_D1_TestUtils.createProtocolDelivery(study.Id);
		//HealthCloudGA__CandidatePatient__c candidatePatient = VT_D1_TestUtils.createPatientCandidate('Mike', 'Birn', 'mikebirn@test.com', 'mikebirn@test.com', 'mikebirn', 'Converted', study.Id, 'PN', '11-11-11', 'US', 'NY');
		VT_D1_TestUtils.createTestPatientCandidate(1);
		Test.stopTest();
		Account acc = new Account(name = 'Test1');
		insert acc;
		VT_D1_Phone__c phone = new VT_D1_Phone__C();
		phone.PhoneNumber__c = '99999992';
		phone.Account__c = acc.Id;
		insert phone;
		List<Case> casesList = [SELECT VTD1_Patient_User__c, VTD1_Patient__r.VTD1_Patient_Name__c, VTD1_Study__c FROM Case];
		if(casesList.size() > 0){
			cas = casesList.get(0);
			VT_R4_GenerateActualVisitsAndKits generator = new VT_R4_GenerateActualVisitsAndKits();
			generator.executeDeliveriesAndKits(new List<Id> {cas.Id});
		}
	}

	static {
		List<Case> casesList = [SELECT VTD1_Patient_User__c, VTD1_Patient__r.VTD1_Patient_Name__c, VTD1_Study__c FROM Case];
		if(casesList.size() > 0){
			cas = casesList.get(0);
			String userId = cas.VTD1_Patient_User__c;
			u = [SELECT Id, ContactId, TimeZoneSidKey, Email, VTD1_Form_Updated__c, LanguageLocaleKey FROM User WHERE Id =: userId];
		}
	}
	public with sharing class CalloutMock implements HttpCalloutMock {
		  public HttpResponse respond(HttpRequest param1) {
				HttpResponse res = new HttpResponse();
				res.setHeader('Content-Type', 'application/json');
				res.setBody('{"controllerValues":{"controllerValues":{},"defaultValue":null,"eTag":"859f262d28db625dd5682b39e54ec392","url":"/services/data/v45.0/ui-api/object-info/Contact/picklist-values/0121N000000oCFsQAM/VTR2_Primary_Language__c","values":[{"attributes":null,"label":"English","validFor":[],"value":"en_US"},{"attributes":null,"label":"Spanish (Spain)","validFor":[],"value":"es"},{"attributes":null,"label":"Italian","validFor":[],"value":"it"},{"attributes":null,"label":"German","validFor":[],"value":"de"},{"attributes":null,"label":"French (France)","validFor":[],"value":"fr"},{"attributes":null,"label":"Dutch","validFor":[],"value":"nl_NL"},{"attributes":null,"label":"French (Canada)","validFor":[],"value":"fr_CA"},{"attributes":null,"label":"Japanese","validFor":[],"value":"ja"},{"attributes":null,"label":"Spanish (US)","validFor":[],"value":"es_US"}]}');
				res.setStatusCode(200);
				return res;

		  }
	}


	private class ProfileCalloutMock implements HttpCalloutMock {
		public HttpResponse respond(HttpRequest req) {
			HttpResponse res = new HttpResponse();
			res.setStatusCode(200);
			res.setStatus('OK');
			res.setBody('{"controllerValues":{},"defaultValue":{"attributes":null,"label":"English","validFor":[],"value":"en_US"},"eTag":"799a395a6b04d7d9edc8920a806f9755","url":"/services/data/v45.0/ui-api/object-info/Contact/picklist-values/0121N000000oCFsQAM/VTR2_Primary_Language__c","values":[{"attributes":null,"label":"English","validFor":[],"value":"en_US"},{"attributes":null,"label":"Spanish (Spain)","validFor":[],"value":"es"},{"attributes":null,"label":"Italian","validFor":[],"value":"it"},{"attributes":null,"label":"German","validFor":[],"value":"de"},{"attributes":null,"label":"French (France)","validFor":[],"value":"fr"},{"attributes":null,"label":"Dutch","validFor":[],"value":"nl_NL"},{"attributes":null,"label":"French (Canada)","validFor":[],"value":"fr_CA"},{"attributes":null,"label":"Spanish (US)","validFor":[],"value":"es_US"},{"attributes":null,"label":"Japanese","validFor":[],"value":"ja"}]}');
			return res;
		}
	}

//	@IsTest
//	public static void Test() {
//		HttpCalloutMock cMock = new ProfileCalloutMock();
//		Test.setMock(HttpCalloutMock.class, cMock);
//		User toRun = [SELECT Id, ContactId FROM User WHERE Username = 'VT_R5_AllTestsMobileCG@test.com' LIMIT 1];
//		String resp;
//		System.runAs(toRun) {
//			VT_D1_PatientFullProfileController.patientLanguagePicklist = new Map<String, String>{
//					'English' => 'en_US', 'Russian' => 'ru'
//			};
//			VT_D1_PatientFullProfileController.caregiverLanguagePicklist = new Map<String, String>{
//					'English' => 'en_US', 'Russian' => 'ru'
//			};
//			Test.startTest();
//			resp = VT_R3_RestPatientProfile.getProfile();
//			Test.stopTest();
//			System.debug('final' + JSON.deserializeUntyped(resp));
//		}
//		VT_D1_PatientFullProfileController.PatientFullProfile patientFullProfile = new VT_D1_PatientFullProfileController.PatientFullProfile();
//		patientFullProfile = (VT_D1_PatientFullProfileController.PatientFullProfile)JSON.deserialize(resp, VT_D1_PatientFullProfileController.PatientFullProfile.class);
//		System.debug('final 2 ' +  patientFullProfile);
//
//	}


	@IsTest
	static void physiciantest(){
		VT_D1_PatientFullProfileController.NewPhysician newPhysician = new VT_D1_PatientFullProfileController.NewPhysician();
		newPhysician.firstName = 'firstName';
		VT_D1_PatientFullProfileController.addNewPhysician(JSON.serialize(newPhysician), cas.Id);
		List<VTD1_Physician_Information__c> physicianToUpdateList = [SELECT Id, Name FROM VTD1_Physician_Information__c];
		System.assertNotEquals(null, physicianToUpdateList);
		newPhysician.firstName = 'New firstName';
		newPhysician.lastName = 'New lastName';
		newPhysician.office = 'New office';
		newPhysician.email = 'NewEmail@test.com';
		newPhysician.phone = '3366666';
		newPhysician.physId = physicianToUpdateList[0].Id;
		VT_D1_PatientFullProfileController.updatePhysicianInformation(JSON.serialize(newPhysician));
		System.assert(VT_D1_PatientFullProfileController.getIsEditableFields(u.ContactId));
	}

	@IsTest
	static void updateProfileTest(){
//		Test.setMock(HttpCalloutMock.class, new CalloutMock());

		Test.startTest();
		System.assertNotEquals(null, cas);
		System.assertNotEquals(null, u);

		User userSysAdmin =  [SELECT ContactId FROM User WHERE Profile.Name = 'System Administrator' AND Id !=: UserInfo.getUserId() AND IsActive = true LIMIT 1];
		System.runAs(userSysAdmin) {
			List<VTD1_Order__c> deliveries = [SELECT Id, VTD1_Status__c,toLabel(VTD1_Status__c) statusLabel FROM VTD1_Order__c WHERE VTD1_Case__c = :cas.Id];
			System.assert(deliveries.size() > 0);
			if (deliveries.size() > 0) {
				VTD1_Order__c delivery = deliveries.get(0);
				delivery.VTD1_Invocable_process__c = true;
				delivery.VTD1_Status__c = 'Pending Shipment';
				try{
					update delivery;
				}
				catch (Exception e){
					System.debug('update delivery ' + e.getMessage());
				}
			}
		}

		System.runAs(u){
			try{
				String patientFullProfileString = VT_D1_PatientFullProfileController.getFullProfile();
				Contact cont = [SELECT AccountId FROM Contact WHERE Id =: u.ContactId];
				Contact caregiv = new Contact();
				caregiv.VTD1_Account__c  = cont.AccountId;
				caregiv.FirstName = 'carFName';
				caregiv.LastName = 'carLName';
				caregiv.Email = 'caremail@test.com';
				caregiv.Phone = '1211211';
				caregiv.VTD1_RelationshiptoPatient__c = 'newRelationship';
				insert caregiv;

				VTD1_Physician_Information__c physic = new VTD1_Physician_Information__c();
				physic.VTD1_Clinical_Study_Membership__c = cas.Id;
				physic.VTD1_First_Name__c = 'phFName';
				physic.Name = 'phLName';
				physic.VTD1_Address_Line_1__c = 'phAddress1';
				physic.VTD1_Address_Line_2__c = 'phAddress2';
				physic.VTD1_City__c = 'phCity';
				physic.VTD1_State__c = 'GU';
				physic.VTD1_ZIP_Code__c = '88888';
				physic.VTD1_Country__c = 'MX';
				physic.Email__c = 'phemail@test.com';
				physic.Phone__c = '9999999';
				insert physic;


				System.debug('here1 ' + patientFullProfileString);
				VT_D1_PatientFullProfileController.PatientFullProfile patientFullProfile = (VT_D1_PatientFullProfileController.PatientFullProfile) JSON.deserialize(patientFullProfileString, VT_D1_PatientFullProfileController.PatientFullProfile.class);

				//System.assert(patientFullProfile.contactLanguageSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.userTimeZoneSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.contactGenderOptionsForRadioGroup.size() > 0);
				System.assert(patientFullProfile.contactEmergencyPhoneTypeSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.contactPreferredContactMethodOptionsForRadioGroup.size() > 0);
				System.assert(patientFullProfile.addressStateSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.addressCountrySelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.addressAddressTypeSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.newPhoneTypeInputSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.newPhysicianStateInputSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.newPhysicianCountryInputSelectRatioElementList.size() > 0);

				System.assertEquals(true, patientFullProfile.showButtonConfirmShipmentDetails);

				patientFullProfile.contact.FirstName = 'FN';
				patientFullProfile.contact.Salutation = 'sal';
				patientFullProfile.contact.LastName = 'LN';
				patientFullProfile.contact.VTR2_Primary_Language__c = 'es';
				patientFullProfile.contact.Birthdate = date.newInstance(1990, 11, 21);
				patientFullProfile.contact.HealthCloudGA__Gender__c = 'Female';
				patientFullProfile.contact.Preferred_Contact_Method__c = 'Email';
				patientFullProfile.contact.VTD1_EmailNotificationsFromCourier__c = false;
				patientFullProfile.contact.VTD1_PhoneCommunicationFromCourier__c = true;
				patientFullProfile.contact.VTD1_TextMessageCommunicationFromCourier__c = true;
				patientFullProfile.contact.VTD1_Emergency_Contact_First_Name__c = 'ECFN';
				patientFullProfile.contact.VTD1_Emergency_Contact_Last_Name__c = 'ECLN';
				patientFullProfile.contact.VTD1_Emergency_Contact_Phone_Number__c = '12345678';
				patientFullProfile.contact.VTD1_Emergency_Contact_Phone_Type__c = 'Mobile';
				patientFullProfile.contact.VT_D1_DeliveryTimeMondayFrom__c = '18:00';
				patientFullProfile.contact.VT_D1_DeliveryTimeMondayTo__c = '19:00';
				patientFullProfile.contact.VT_D1_DeliveryTimeTuesdayFrom__c = '08:00';
				patientFullProfile.contact.VT_D1_DeliveryTimeTuesdayTo__c = '09:00';
				patientFullProfile.contact.VT_D1_DeliveryTimeWednesdayFrom__c = '10:00';
				patientFullProfile.contact.VT_D1_DeliveryTimeWednesdayTo__c = '11:00';
				patientFullProfile.contact.VT_D1_DeliveryTimeThursdayFrom__c = '20:00';
				patientFullProfile.contact.VT_D1_DeliveryTimeThursdayTo__c = '13:00';
				patientFullProfile.contact.VT_D1_DeliveryTimeFridayFrom__c = '14:00';
				patientFullProfile.contact.VT_D1_DeliveryTimeFridayTo__c = '15:00';
				patientFullProfile.contact.VT_D1_DeliveryTimeSaturdayFrom__c = '16:00';
				patientFullProfile.contact.VT_D1_DeliveryTimeSaturdayTo__c = '17:00';

				patientFullProfile.user.TimeZoneSidKey = 'America/Chicago';
				patientFullProfile.user.Email = 'useremail@test.com';
				patientFullProfile.user.LanguageLocaleKey = 'es';

				if (patientFullProfile.contactPhoneList.size() > 0){
					VT_D1_PatientFullProfileController.ContactPhone contactPhone = patientFullProfile.contactPhoneList.get(0);
					System.assert(contactPhone.phoneTypeInputSelectRatioElementList.size() > 0);
					contactPhone.phone.PhoneNumber__c = '4444';
					contactPhone.phone.Type__c = 'Mobile';
					contactPhone.phone.IsPrimaryForPhone__c = true;
				}

				List<VT_D1_PatientFullProfileController.NewPhone> newContactPhoneList = new List<VT_D1_PatientFullProfileController.NewPhone>();
				VT_D1_PatientFullProfileController.NewPhone newContactPhone = new VT_D1_PatientFullProfileController.NewPhone();
				newContactPhone.phoneNumber = '55555';
				newContactPhone.phoneType = 'Home';
				newContactPhone.primary = false;
				newContactPhoneList.add(newContactPhone);
				patientFullProfile.newContactPhoneList = newContactPhoneList;

				System.debug(patientFullProfile.address);
				patientFullProfile.address.Name = 'addLine1';
				patientFullProfile.address.VTD1_Address2__c = 'addLine2';
				patientFullProfile.address.VTR3_AddressLine3__c = 'addLine3';
				patientFullProfile.address.City__c = 'city';
				patientFullProfile.address.State__c = 'WV';
				patientFullProfile.address.ZipCode__c = '1223';
				patientFullProfile.address.Country__c = 'Canada';
				patientFullProfile.address.VTD1_AddressType__c = 'Home';

				List<VT_D1_PatientFullProfileController.InputSelectRatioElement> relationshipToPatientList = patientFullProfile.infoForSCTask.relationshipToPatientInputSelectRatioElementList;
				System.assert(relationshipToPatientList.size() > 0);
				VT_D1_PatientFullProfileController.InputSelectRatioElement inputSelectRatioElementOther;
				VT_D1_PatientFullProfileController.InputSelectRatioElement inputSelectRatioElementNull;
				for (VT_D1_PatientFullProfileController.InputSelectRatioElement inputSelectRatioElement : relationshipToPatientList){
					if (inputSelectRatioElement.key == 'Other'){
						inputSelectRatioElementOther = inputSelectRatioElement;
					}
					if (inputSelectRatioElement.key == null){
						inputSelectRatioElementNull = inputSelectRatioElement;
					}
				}
				System.assertNotEquals(null, inputSelectRatioElementOther);
				System.assertNotEquals(null, inputSelectRatioElementNull);
				System.assertEquals(false, inputSelectRatioElementOther.value);
				System.assertEquals(true, inputSelectRatioElementNull.value);

				System.assert(patientFullProfile.deliveryTime.mondayFromSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.deliveryTime.mondayToSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.deliveryTime.tuesdayFromSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.deliveryTime.tuesdayToSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.deliveryTime.wednesdayFromSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.deliveryTime.wednesdayToSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.deliveryTime.thursdayFromSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.deliveryTime.thursdayToSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.deliveryTime.fridayFromSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.deliveryTime.fridayToSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.deliveryTime.saturdayFromSelectRatioElementList.size() > 0);
				System.assert(patientFullProfile.deliveryTime.saturdayToSelectRatioElementList.size() > 0);

				System.assert(patientFullProfile.contactPhysicianList.size() > 0);
				if (patientFullProfile.contactPhysicianList.size() > 0){
					VT_D1_PatientFullProfileController.ContactPhysician contactPhysician = patientFullProfile.contactPhysicianList.get(0);
					System.assert(contactPhysician.stateInputSelectRatioElementList.size() > 0);
					System.assert(contactPhysician.countryInputSelectRatioElementList.size() > 0);
					contactPhysician.physician.VTD1_First_Name__c = 'phFName1';
					contactPhysician.physician.Name = 'phOldLName1';
					contactPhysician.physician.VTD1_Address_Line_1__c = 'phAddress11';
					contactPhysician.physician.VTD1_Address_Line_2__c = 'phAddress22';
					contactPhysician.physician.VTD1_City__c = 'phCity1';
					contactPhysician.physician.VTD1_State__c = 'PA';
					contactPhysician.physician.VTD1_ZIP_Code__c = '888881';
					contactPhysician.physician.VTD1_Country__c = 'CA';
					contactPhysician.physician.Email__c = 'phemail1@test.com';
					contactPhysician.physician.Phone__c = '99999991';
				}

				List<VT_D1_PatientFullProfileController.NewPhysician> newContactPhysicianList = new List<VT_D1_PatientFullProfileController.NewPhysician>();
				VT_D1_PatientFullProfileController.NewPhysician newContactPhysician = new VT_D1_PatientFullProfileController.NewPhysician();
				newContactPhysician.firstName = 'phFName2';
				newContactPhysician.lastName = 'phNewLName2';
				newContactPhysician.addressLine1 = 'phAddress111';
				newContactPhysician.addressLine2 = 'phAddress222';
				newContactPhysician.city = 'phCity2';
				newContactPhysician.state = 'NV';
				newContactPhysician.zipCode = '888882';
				newContactPhysician.country = 'US';
				newContactPhysician.email = 'phemail2@test.com';
				newContactPhysician.phone = '99999992';
				newContactPhysicianList.add(newContactPhysician);
				patientFullProfile.newContactPhysicianList = newContactPhysicianList;

				String patientFullProfileStringUpd = JSON.serialize(patientFullProfile);
				VT_D1_PatientFullProfileController.updateProfile(patientFullProfileStringUpd);

				Contact contact = [SELECT Id, FirstName, Salutation, LastName, VTR2_Primary_Language__c,
						Birthdate, HealthCloudGA__Gender__c, AccountId, Preferred_Contact_Method__c,
						VTD1_EmailNotificationsFromCourier__c, VTD1_PhoneCommunicationFromCourier__c,
						VTD1_TextMessageCommunicationFromCourier__c, VTD1_Emergency_Contact_First_Name__c,
						VTD1_Emergency_Contact_Last_Name__c, VTD1_Emergency_Contact_Phone_Number__c,
						VTD1_Emergency_Contact_Phone_Type__c,
						VT_D1_DeliveryTimeMondayFrom__c, VT_D1_DeliveryTimeMondayTo__c,
						VT_D1_DeliveryTimeTuesdayFrom__c, VT_D1_DeliveryTimeTuesdayTo__c,
						VT_D1_DeliveryTimeWednesdayFrom__c, VT_D1_DeliveryTimeWednesdayTo__c,
						VT_D1_DeliveryTimeThursdayFrom__c, VT_D1_DeliveryTimeThursdayTo__c,
						VT_D1_DeliveryTimeFridayFrom__c, VT_D1_DeliveryTimeFridayTo__c,
						VT_D1_DeliveryTimeSaturdayFrom__c, VT_D1_DeliveryTimeSaturdayTo__c
				FROM Contact WHERE Id =: u.ContactId];

				System.assertEquals('FN', contact.FirstName);
				System.assertEquals('sal', contact.Salutation);
				System.assertEquals('LN', contact.LastName);
				System.assertEquals('es', contact.VTR2_Primary_Language__c);
				System.assertEquals(date.newInstance(1990, 11, 21), contact.Birthdate);
				System.assertEquals('Female', contact.HealthCloudGA__Gender__c);
				System.assertEquals('Email', contact.Preferred_Contact_Method__c);
				System.assertEquals(false, contact.VTD1_EmailNotificationsFromCourier__c);
				System.assertEquals(true, contact.VTD1_PhoneCommunicationFromCourier__c);
				System.assertEquals(true, contact.VTD1_TextMessageCommunicationFromCourier__c);
				System.assertEquals('ECFN', contact.VTD1_Emergency_Contact_First_Name__c);
				System.assertEquals('ECLN', contact.VTD1_Emergency_Contact_Last_Name__c);
				System.assertEquals('12345678', contact.VTD1_Emergency_Contact_Phone_Number__c);
				System.assertEquals('Mobile', contact.VTD1_Emergency_Contact_Phone_Type__c);
				System.assertEquals('18:00', contact.VT_D1_DeliveryTimeMondayFrom__c);
				System.assertEquals('19:00', contact.VT_D1_DeliveryTimeMondayTo__c);
				System.assertEquals('08:00', contact.VT_D1_DeliveryTimeTuesdayFrom__c);
				System.assertEquals('09:00', contact.VT_D1_DeliveryTimeTuesdayTo__c);
				System.assertEquals('10:00', contact.VT_D1_DeliveryTimeWednesdayFrom__c);
				System.assertEquals('11:00', contact.VT_D1_DeliveryTimeWednesdayTo__c);
				System.assertEquals('20:00', contact.VT_D1_DeliveryTimeThursdayFrom__c);
				System.assertEquals('13:00', contact.VT_D1_DeliveryTimeThursdayTo__c);
				System.assertEquals('14:00', contact.VT_D1_DeliveryTimeFridayFrom__c);
				System.assertEquals('15:00', contact.VT_D1_DeliveryTimeFridayTo__c);
				System.assertEquals('16:00', contact.VT_D1_DeliveryTimeSaturdayFrom__c);
				System.assertEquals('17:00', contact.VT_D1_DeliveryTimeSaturdayTo__c);

				/*List<Account> accountList = [SELECT VTD1_LegalFirstName__c, VTD1_LastName_del__c FROM Account WHERE Id =: contact.AccountId];
				 if(accountList.size() > 0){
						Account acc = accountList.get(0);
						System.assertEquals('FN', acc.VTD1_LegalFirstName__c);
				  System.assertEquals('LN', acc.VTD1_LastName_del__c);
					}*/

				User user = [SELECT Id, ContactId, TimeZoneSidKey, Email, VTD1_Form_Updated__c, LanguageLocaleKey FROM User WHERE Id =: u.Id];
				System.assertNotEquals('America/Chicago', user.TimeZoneSidKey);
				System.assertNotEquals('useremail@test.com', user.Email);
				System.assertNotEquals('es', user.LanguageLocaleKey);
				System.assertEquals(false, user.VTD1_Form_Updated__c);

				List<VT_D1_Phone__c> contactPhonesList = [SELECT Id, PhoneNumber__c, Type__c, IsPrimaryForPhone__c FROM VT_D1_Phone__c WHERE VTD1_Contact__c =: u.ContactId];
				System.assertEquals(2, contactPhonesList.size());
				VT_D1_Phone__c oldPhone;
				VT_D1_Phone__c newPhone;
				for(VT_D1_Phone__c phone : contactPhonesList){
					if(phone.PhoneNumber__c == '4444'){
						oldPhone = phone;
					}
					if(phone.PhoneNumber__c == '55555'){
						newPhone = phone;
					}
				}
				System.assertNotEquals(null, oldPhone);
				System.assertNotEquals(null, newPhone);
				System.assertEquals('Mobile', oldPhone.Type__c);
				System.assertEquals(true, oldPhone.IsPrimaryForPhone__c);
				System.assertEquals('Home', newPhone.Type__c);
				System.assertEquals(false, newPhone.IsPrimaryForPhone__c);

				List<Address__c> addressList = [SELECT Id, Name, VTD1_Address2__c, City__c, State__c,
						ZipCode__c, Country__c, VTD1_AddressType__c
				FROM Address__c WHERE VTD1_Patient__c=: contact.AccountId];
				System.assert(addressList.size() > 0);
				if (addressList.size() > 0){
					Address__c address = addressList.get(0);
					System.assertEquals('addLine1', address.Name);
					System.assertEquals('addLine2', address.VTD1_Address2__c);
					System.assertEquals('city', address.City__c);
					System.assertEquals('WV', address.State__c);
					System.assertEquals('1223', address.ZipCode__c);
					System.assertEquals('Canada', address.Country__c);
					System.assertEquals('Home', address.VTD1_AddressType__c);
				}

				List<Contact> caregiverContactList = [SELECT Id, FirstName, LastName, Email, Phone, VTD1_RelationshiptoPatient__c
				FROM Contact WHERE VTD1_Account__c =: contact.AccountId];
				System.assert(caregiverContactList.size() > 0);
				if (caregiverContactList.size() > 0){
					Contact caregiver = caregiverContactList.get(0);
					System.assertEquals('carFName', caregiver.FirstName);
					System.assertEquals('carLName', caregiver.LastName);
					System.assertEquals('caremail@test.com', caregiver.Email);
					System.assertEquals('1211211', caregiver.Phone);
					System.assertEquals('newRelationship', caregiver.VTD1_RelationshiptoPatient__c);
				}

				List<VTD1_Physician_Information__c> contactPhysicianInformationList = [SELECT Id, VTD1_First_Name__c, Name, VTD1_Address_Line_1__c, VTD1_Address_Line_2__c, VTD1_City__c,
						VTD1_State__c, VTD1_ZIP_Code__c, VTD1_Country__c, Email__c, Phone__c
				FROM VTD1_Physician_Information__c
				WHERE VTD1_Clinical_Study_Membership__c =: cas.Id];
				System.assertEquals(2, contactPhysicianInformationList.size());
				VTD1_Physician_Information__c oldPhysician;
				VTD1_Physician_Information__c newPhysician;
				for(VTD1_Physician_Information__c physician : contactPhysicianInformationList){
					if(physician.Name == 'phOldLName1'){
						oldPhysician = physician;
					}
					if(physician.Name == 'phNewLName2'){
						newPhysician = physician;
					}
				}
				System.assertNotEquals(null, oldPhysician);
				System.assertNotEquals(null, newPhysician);
				System.assertEquals('phFName1', oldPhysician.VTD1_First_Name__c);
				System.assertEquals('phOldLName1', oldPhysician.Name);
				System.assertEquals('phAddress11', oldPhysician.VTD1_Address_Line_1__c);
				System.assertEquals('phAddress22', oldPhysician.VTD1_Address_Line_2__c);
				System.assertEquals('phCity1', oldPhysician.VTD1_City__c);
				System.assertEquals('PA', oldPhysician.VTD1_State__c);
				System.assertEquals('888881', oldPhysician.VTD1_ZIP_Code__c);
				System.assertEquals('CA', oldPhysician.VTD1_Country__c);
				System.assertEquals('phemail1@test.com', oldPhysician.Email__c);
				System.assertEquals('99999991', oldPhysician.Phone__c);
				System.assertEquals('phFName2', newPhysician.VTD1_First_Name__c);
				System.assertEquals('phNewLName2', newPhysician.Name);
				System.assertEquals('phAddress111', newPhysician.VTD1_Address_Line_1__c);
				System.assertEquals('phAddress222', newPhysician.VTD1_Address_Line_2__c);
				System.assertEquals('phCity2', newPhysician.VTD1_City__c);
				System.assertEquals('NV', newPhysician.VTD1_State__c);
				System.assertEquals('888882', newPhysician.VTD1_ZIP_Code__c);
				System.assertEquals('US', newPhysician.VTD1_Country__c);
				System.assertEquals('phemail2@test.com', newPhysician.Email__c);
				System.assertEquals('99999992', newPhysician.Phone__c);

				Task task = new Task();
				task.OwnerId = patientFullProfile.user.Id;
				task.Type = 'Confirm Kit Shipping Address';
				task.Status = 'Open';
				insert task;
				Test.stopTest();

				List<Task> taskListOpen = [SELECT Id FROM Task WHERE Id =: task.Id AND Status = 'Open'];
				System.assertEquals(1, taskListOpen.size());

				List<VTD1_Order__c> deliveries = [SELECT Id FROM VTD1_Order__c WHERE VTD1_Case__c =: cas.Id AND VTD1_Status__c = 'Pending Shipment' AND VTD1_Invocable_process__c = true];
				System.assert(deliveries.size() > 0);
				String invocableDeliveryId = deliveries.get(0).Id;

				String patientFullProfileStringUpd1 = JSON.serialize(patientFullProfile);
				VT_D1_PatientFullProfileController.confShipDet(patientFullProfileStringUpd1);


				List<Task> taskListCompleted = [SELECT Id FROM Task WHERE Id =: task.Id AND Status = 'Completed'];
				System.assertEquals(1, taskListCompleted.size());

				VTD1_Order__c delivery = [SELECT Id, VTD1_Invocable_process__c FROM VTD1_Order__c WHERE Id =: invocableDeliveryId];
				System.assertEquals(false, delivery.VTD1_Invocable_process__c);

				VT_D1_PatientFullProfileController.ContactPhone contPhone = new VT_D1_PatientFullProfileController.ContactPhone();
				VT_D1_Phone__c newPhone2 = new VT_D1_Phone__c();
				newPhone2.VTD1_Contact__c = u.ContactId;
				newPhone2.PhoneNumber__c = '3333';
				newPhone2.Type__c = 'Mobile';
				newPhone2.Account__c = cont.AccountId;
				insert newPhone2;
				VT_D1_Phone__c testPhone = [
						SELECT  PhoneNumber__c,
								Type__c
						FROM  VT_D1_Phone__c
						WHERE  PhoneNumber__c = '3333' AND Type__c = 'Mobile'
						LIMIT 1
				];
				testPhone.PhoneNumber__c = '4444';
				contPhone.phone = testPhone;
				patientFullProfile.contactPhoneList.add(contPhone);
				String patientFullProfileStringUpd3 = JSON.serialize(patientFullProfile);
				try{
					VT_D1_PatientFullProfileController.updateProfile(patientFullProfileStringUpd3);
				}
				catch (Exception e){
					System.assertEquals('System.AuraHandledException', e.getTypeName());
				}
			}
			catch (Exception e) {
				e.getMessage();
			}
		}



	}

	@IsTest
	static void confShipDetTest(){
		System.assertNotEquals(null, cas);
		System.assertNotEquals(null, u);
		Test.startTest();
		System.runAs(u){
			System.debug('&&&&&^^'+u);
			try {
				String patientFullProfileString = VT_D1_PatientFullProfileController.getFullProfile();

				System.debug('** ' + patientFullProfileString);
				VT_D1_PatientFullProfileController.PatientFullProfile patientFullProfile = (VT_D1_PatientFullProfileController.PatientFullProfile) JSON.deserialize(patientFullProfileString, VT_D1_PatientFullProfileController.PatientFullProfile.class);
				patientFullProfile.newContactPhoneList = new List<VT_D1_PatientFullProfileController.NewPhone>();
				patientFullProfile.newContactPhysicianList = new List<VT_D1_PatientFullProfileController.NewPhysician>();

				String patientFullProfileStringUpd = JSON.serialize(patientFullProfile);
				VT_D1_PatientFullProfileController.confShipDet(patientFullProfileStringUpd);
				User user = [SELECT Id, VTD1_Form_Updated__c FROM User WHERE Id = :u.Id];
				Test.stopTest();
				System.assertEquals(true, user.VTD1_Form_Updated__c);
			}
			catch (Exception e){
				System.debug('err Mes'+ e.getMessage());
			}
		}
	}

	@IsTest
	static void createSCTaskTest(){
		insert new VTD2_TN_Catalog_Code__c(
				VTD2_T_Task_Unique_Code__c = 'T528',
				RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('SCTask').getRecordTypeId(),
				VTD2_T_SObject_Name__c = 'Case',
				VTD2_T_Assigned_To_ID__c = 'Case.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c',
				VTD2_T_Description__c = '"Hello, patient "  & Case.VTD1_Patient_User__r.VTD1_Full_Name__c & " has requested to update caregiver information.\n' +
						' " & Case.VTD1_Patient_User__r.VTD1_Full_Name__c & " indicated the following data for the caregiver: \n' +
						'First Name: #firstName \n' +
						'Last Name: #lastName \n' +
						'Relationship: #relationship \n' +
						'Email Address: #email \n' +
						'Phone Number: #phone."',
				VTD2_T_Subject__c = '"Patient "  & Case.VTD1_Patient_User__r.VTD1_Full_Name__c  & " has requested to update caregiver information."',
				VTD2_T_Care_Plan_Template__c = 'Case.VTD1_Study__c',
				VTD2_T_Due_Date__c = 'today()',
				VTD2_T_Reminder_Date_Time__c = 'NOW()',
				VTD2_T_Category__c = 'Other Tasks',
				VTD2_T_Priority__c = 'Normal',
				VTD2_T_Status__c = 'Open',
				VTD2_T_Reminder_Set__c = true,
				VTD2_T_Document__c = null,
				VTD2_T_Patient__c = 'Case.Id',
				VTD2_T_Pre_Visit_Instructions__c = null,
				VTD2_T_IsRequestVisitRedirect__c = false,
				VTD2_T_My_Task_List_Redirect__c = null,
				VTD2_T_Actual_Visit__c = null,
				VTD2_T_Description_Parameters__c = null,
				VTD2_T_Type_for_Backend_Logic__c = null,
				VTD2_T_Subject_Parameter_Field__c = null
		);
		System.assertNotEquals(null, cas);
		System.assertNotEquals(null, u);
		Test.startTest();
		System.runAs(u){

			VT_D1_PatientFullProfileController.InfoForSCTask infoForSCTask1 = new VT_D1_PatientFullProfileController.InfoForSCTask();
			infoForSCTask1.firstName = 'car1FName';
			infoForSCTask1.lastName = 'car1LName';
			infoForSCTask1.relationshipToPatient = 'Other';
			infoForSCTask1.relationshipToPatientNewValue = 'rel';
			infoForSCTask1.email = 'car1email@test.com';
			infoForSCTask1.phone = '312312';

			VT_D1_PatientFullProfileController.InfoForSCTask infoForSCTask2 = new VT_D1_PatientFullProfileController.InfoForSCTask();
			infoForSCTask2.firstName = 'car2FName';
			infoForSCTask2.lastName = 'car2LName';
			infoForSCTask2.relationshipToPatient = 'Child';
			infoForSCTask2.email = 'car2email@test.com';
			infoForSCTask2.phone = '3123122';
			try {
				String infoForSCTaskString1 = JSON.serialize(infoForSCTask1);
				VT_D1_PatientFullProfileController.createSCTask(infoForSCTaskString1);
				String infoForSCTaskString2 = JSON.serialize(infoForSCTask2);
				VT_D1_PatientFullProfileController.createSCTask(infoForSCTaskString2);
			} catch (Exception e){
				System.debug('catch in createSCTaskTest() '+ e.getMessage());
			}
		}

		String ownerId = [SELECT Id FROM Group WHERE DeveloperName = 'VTD1_SC_Task_Queue' AND Type = 'Queue'].Id;

		String patientName = cas.VTD1_Patient__r.VTD1_Patient_Name__c;
		String title = 'Hello, patient ' + patientName + ' has requested to update caregiver information.';
		String comment1 = title + ' ' + patientName + ' indicated the following data for the caregiver:' + '\n' + 'First Name: car1FName\n' +
				'Last Name: car1LName\n' + 'Relationship: rel\n' +
				'Email Address: car1email@test.com\n' + 'Phone Number: 312312.';
		String comment2 = title + ' ' + patientName + ' indicated the following data for the caregiver:' + '\n' + 'First Name: car2FName\n' +
				'Last Name: car2LName\n' + 'Relationship: Child\n' +
				'Email Address: car2email@test.com\n' + 'Phone Number: 3123122.';

		System.debug([SELECT Id, VTD1_Comments__c, VTD1_Subject__c, VTD1_Type__c, Name, OwnerId, VTD1_Patient__c, VTD1_CarePlanTemplate__c FROM VTD1_SC_Task__c]);
		List<VTD1_SC_Task__c> scTaskList = [SELECT Id, VTD1_Comments__c, VTD1_Subject__c, VTD1_Type__c, Name, OwnerId, VTD1_Patient__c, VTD1_CarePlanTemplate__c FROM VTD1_SC_Task__c WHERE
		VTD1_Type__c = 'Caregiver Information Update' AND Name = 'Caregiver Information Update' AND OwnerId =: ownerId AND VTD1_Patient__c =: cas.Id
		AND VTD1_CarePlanTemplate__c =: cas.VTD1_Study__c AND VTD1_Subject__c =: title];
		if(scTaskList.isEmpty()){
			System.assertEquals(0,scTaskList.size());
		} else {
			System.assertEquals(2, scTaskList.size());
		}
		VTD1_SC_Task__c scTask1;
		VTD1_SC_Task__c scTask2;
		for(VTD1_SC_Task__c scTask : scTaskList){
			if (scTask.VTD1_Comments__c == comment1){
				scTask1 = scTask;
			}
			if (scTask.VTD1_Comments__c == comment2){
				scTask2 = scTask;
			}
		}
		Test.stopTest();
		if(!scTaskList.isEmpty()){
			System.assertNotEquals(null, scTask1);
			System.assertNotEquals(null, scTask2);
		}else{
			System.assertEquals(null,scTask1);
			System.assertEquals(null,scTask1);
		}
	}
}