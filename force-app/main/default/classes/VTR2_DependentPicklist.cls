public with sharing class VTR2_DependentPicklist {

    @AuraEnabled
    public static Picklist getDependentPicklist(String sobjectName, String fieldName, String dependentFieldName) {

        Map<String, List<VTR2_DependentPicklist.PicklistOption>> dependentPicklists = getDependentPicklistValues(sobjectName, dependentFieldName);

        Picklist newPicklist = new Picklist();
        newPicklist.sobjectName = sobjectName;
        newPicklist.picklistLabel = Schema.getGlobalDescribe().get(sobjectName).getDescribe().fields.getMap().get(fieldName).getDescribe().getLabel();
        newPicklist.picklistName = fieldName;

        List<PicklistOption> controllingOptions = new List<PicklistOption>();
        for (String controllingField : dependentPicklists.keySet()) {
            PicklistOption controllingOption = new PicklistOption();
            controllingOption.active = 'true';
            String[] opts = controllingField.split(';');
            controllingOption.label = opts[0];
            controllingOption.value = opts[1];
            controllingOption.defaultValue = Boolean.valueOf(opts[2]);
            controllingOption.dependentOptions = dependentPicklists.get(controllingField);
            controllingOptions.add(controllingOption);
        }

        newPicklist.options = controllingOptions;
        return newPicklist;
    }

    public static Map<String, List<VTR2_DependentPicklist.PicklistOption>> getDependentPicklistValues( String sObjectName, String fieldName )
    {
        return getDependentPicklistValues
                (   Schema.getGlobalDescribe().get( sObjectName ).getDescribe().fields.getMap().get( fieldName )
                );
    }

    private static Map<String, List<VTR2_DependentPicklist.PicklistOption>> getDependentPicklistValues( Schema.sObjectField dependToken )
    {
        Schema.DescribeFieldResult depend = dependToken.getDescribe();
        Schema.sObjectField controlToken = depend.getController();
        if ( controlToken == null ) return null;
        Schema.DescribeFieldResult control = controlToken.getDescribe();
        List<Schema.PicklistEntry> controlEntries =
                (   control.getType() == Schema.DisplayType.Boolean
                        ?   null
                        :   control.getPicklistValues()
                );

        String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/-';
        Map<String, List<VTR2_DependentPicklist.PicklistOption>> dependentPicklistValues = new Map<String, List<VTR2_DependentPicklist.PicklistOption>>();

        for ( Schema.PicklistEntry entry : depend.getPicklistValues() ) if ( entry.isActive() )
        {
            List<String> base64chars =
                    String.valueOf
                            (   ((Map<String,Object>) JSON.deserializeUntyped( JSON.serialize( entry ) )).get( 'validFor' )
                            ).split( '' );

            for ( Integer index = 0; index < (controlEntries != null ? controlEntries.size() : 2); index++ )
            {
                Object controlValue =
                        (   controlEntries == null
                                ?   (Object) (index == 1)
                                :   (Object) (controlEntries[ index ].isActive() ? controlEntries[ index ].getLabel() : null)
                        );
                Integer bitIndex = index / 6, bitShift = 5 - Math.mod( index, 6 );

                String controlValueStr = controlValue + ';' + controlEntries[index].getValue() + ';' + controlEntries[index].isDefaultValue();
                if ( !dependentPicklistValues.containsKey( controlValueStr ) )
                {
                    dependentPicklistValues.put( controlValueStr, new List<VTR2_DependentPicklist.PicklistOption>() );
                }

                if  (   controlValue == null || bitIndex >= base64chars.size()
                        ||  (base64map.indexOf( base64chars[ bitIndex ] ) & (1 << bitShift)) == 0
                        ) continue;

                dependentPicklistValues.get( controlValueStr ).add(
                        new VTR2_DependentPicklist.PicklistOption(entry.getLabel(), entry.getValue(), entry.isDefaultValue(), 'true' ));
            }
        }
        return dependentPicklistValues;
    }

    public class Picklist {

        @AuraEnabled
        public String sobjectName;

        @AuraEnabled
        public String picklistLabel;

        @AuraEnabled
        public String picklistName;

        @AuraEnabled
        public List<PicklistOption> options;
    }

    public class PicklistOption {

        @AuraEnabled
        public String active;

        @AuraEnabled
        public Boolean defaultValue;

        @AuraEnabled
        public String label;

        @AuraEnabled
        public String value;

        @AuraEnabled
        public List<PicklistOption> dependentOptions;

        public PicklistOption() { }

        public PicklistOption(String label, String value, Boolean isDefaultValue, String isActive) {
            this.label = label;
            this.value = value;
            this.defaultValue = isDefaultValue;
            this.active = isActive;
        }
    }
}