/**
 * Created by Leonid Bartenev
 * https://gitlabhq.customertimes.com/ct-libs/search-framework/
 */

public with sharing class VT_R5_SearchService {

    public interface SearchFilter {
        List<String> getFields();
        Type getSObjectType();
        String getFilterString();
        String getOrderByString();
    }

    public static SearchResponse search(SearchFilter searchFilter, PaginationData paginationData) {
        String whereClause = searchFilter.getFilterString();
        whereClause = String.isBlank(whereClause)? '' : 'WHERE ' + whereClause;
        String countQuery =
                'SELECT COUNT() FROM ' + searchFilter.getSObjectType() + ' ' + whereClause;
        System.debug('COUNT QUERY: ' + countQuery);
        Integer allRecordsCount = Database.countQuery(countQuery);
        paginationData.setRecordsCount(allRecordsCount);

        List<SObject> pageRecords = VT_R5_DatabaseQueryService.query(
                searchFilter.getFields(),
                searchFilter.getSObjectType(),
                searchFilter.getFilterString(),
                searchFilter.getOrderByString(),
                paginationData.pageRecordsCount,
                paginationData.offset
        );
        return new SearchResponse(pageRecords, paginationData);
    }

    public class SearchResponse {

        @AuraEnabled public List<SObject> pageRecords;
        @AuraEnabled public PaginationData paginationData;

        public SearchResponse(List<SObject> pageRecords, PaginationData paginationData) {
            this.pageRecords = pageRecords;
            this.paginationData = paginationData;
        }
    }

    public class PaginationData {

        @AuraEnabled public Integer allRecordsCount;
        @AuraEnabled public Integer pagesCount;
        @AuraEnabled public Integer pageRecordsCount;
        @AuraEnabled public Integer currentPage;
        @AuraEnabled public Integer currentPageCount;
        @AuraEnabled public Integer offset;
        @AuraEnabled public Integer endOffset;

        public PaginationData() {
            currentPage = 1;
            pageRecordsCount = 5;
        }

        public PaginationData(Integer recordsPerPage) {
            currentPage = 1;
            pageRecordsCount = recordsPerPage;
        }

        public void setRecordsCount(Integer allRecordsCount) {
            this.allRecordsCount = allRecordsCount;
            recalc();
        }

        public void recalc() {
            Decimal allRecordsCountDecimal = allRecordsCount;
            Decimal pageRecordsCountDecimal = pageRecordsCount;
            Decimal allPagesDecimal = allRecordsCountDecimal/pageRecordsCountDecimal;
            allPagesDecimal = allPagesDecimal.round(System.RoundingMode.UP);
            pagesCount = allPagesDecimal.intValue() == 0 ? 1 : allPagesDecimal.intValue();
            if(currentPage > allPagesDecimal) currentPage = pagesCount;
            offset = (currentPage - 1) * pageRecordsCount;
            endOffset = offset + pageRecordsCount;
            if(endOffset > allRecordsCount) endOffset = allRecordsCount;
            currentPageCount = endOffset - offset;
        }
    }
}