/**
 * Created by Elena Shane on 20.11.2018.
 */

public without sharing class VT_D2_TaskHandler {
    public static Boolean eCoaBulkAlerts = false;
   //map<Task, Boolean> task_TMAReviewProperty = new map<Task, Boolean>();
    //map<Task, Id> task_DocId = new map<Task, Id>();
    private static List<Task> cancelledPatientTasks = new List<Task>();

    private static VTD1_RTId__c rtIds;

    private static List<String> tnCodesNotifications = new List<String>();
    private static List<String> tnCodesTasks = new List<String>();
    private static List<Id> sourceIdsNotifications = new List<Id>();
    private static List<Id> sourceIdsTasks = new List<Id>();

    public static void onBeforeInsert(List<Task> tasks){
        if (eCoaBulkAlerts) {
            return;
        }
        Set<Id> cancelledPatientsIds = queryCancelledPatients(tasks);
        // remove tasks for cancelled patients
        if (!cancelledPatientsIds.isEmpty()) {
            VTD1_RTId__c rtIds = VTD1_RTId__c.getInstance();
            List<Task> newTaskList = new List<Task>();
            for(Task t: tasks) {
                if(!cancelledPatientsIds.contains(t.OwnerId)) {
                    newTaskList.add(t);
                }
                else {
                    t.OwnerId  = rtIds.VTR2_Backend_Logic_User_ID__c;
                    cancelledPatientTasks.add(t);
                }
            }
            tasks = newTaskList;
        }

        new VT_D1_OutOfOfficeTaskReassigner().doReassign(tasks);
    }

    public static void onAfterInsert(List<Task> tasks) {
        if (!cancelledPatientTasks.isEmpty()) {
            Set<Id> taskIdsToDelete = new Map<Id, Task>(cancelledPatientTasks).keySet();
            System.enqueueJob(new CancelledPatientTasksDeleter(taskIdsToDelete));
        }

        sharePatientTaskToCG(tasks);
        if (!eCoaBulkAlerts) {
            generateNotificationsFromTasks (tasks);
            if (!tnCodesNotifications.isEmpty()) {
                VT_D2_TNCatalogNotifications.generateNotifications(tnCodesNotifications, sourceIdsNotifications, new Map<String, String>());
                tnCodesNotifications.clear();
                sourceIdsNotifications.clear();
            }

            List<Id> tasksToUpdLinkIds = new List<Id>();
            for (Task t : tasks) {
                if (t.VTD1_Type_for_backend_logic__c == 'Final Eligibility Decision Task') {
                    //RAND2 Final Eligibility Review Notification for PI
                    tasksToUpdLinkIds.add(t.Id);
                }
            }
            if (!tasksToUpdLinkIds.isEmpty()) {
                updateTasks(tasksToUpdLinkIds);
            }
        }
    }

    public static void onBeforeUpdate(List<Task> tasks, Map<Id, Task> oldMap) {
        if (!cancelledPatientTasks.isEmpty()) {
            Set<Id> taskIdsToDelete = new Map<Id, Task>(cancelledPatientTasks).keySet();
            System.enqueueJob(new CancelledPatientTasksDeleter(taskIdsToDelete));
        }
        String patientProfileId = getHardCodedIds().VTD2_Profile_Patient__c;
        String caregiverProfileId = getHardCodedIds().VTD2_Profile_Caregiver__c;
        System.debug('patientProfileId' + patientProfileId);
        System.debug( UserInfo.getProfileId() );
        for (Task t : tasks) {
            if (t.Status == 'Completed' && t.Status != oldMap.get(t.Id).Status) {
                if (patientProfileId != null && UserInfo.getProfileId().contains(patientProfileId)
                        || caregiverProfileId != null &&  UserInfo.getProfileId().contains(caregiverProfileId)) {
                    // AutoPopulation (Closed by) PB
                    t.Closed_by__c = UserInfo.getName();
                }
            }
        }
    }

    public static void onAfterUpdate(List<Task> tasks, Map<Id, Task> oldMap) {
        List<Id> reviewLabResActualV_ids = new List<Id> ();
        List<Task> reviewLabResTasks = new List<Task> ();
        List<Id> shipWelcomeKitSCTasksIds = new List<Id> ();
        List<Task> shipWelcomeKitTasks = new List<Task> ();
        for (Task t : tasks) {
            if (t.Status == 'Completed' && t.Status != oldMap.get(t.Id).Status) {
                if (t.RecordTypeId == getHardCodedIds().VTD1_Task_TaskForCountersignPacket__c && t.VTD2_Patient_Status__c == VT_R4_ConstantsHelper_Statuses.CASE_PRE_CONSENT) {
                    //PG receives 'Verify Consent Packet' Task PB
                    tnCodesTasks.add('T559');
                    sourceIdsTasks.add(t.Id);
                } else if (t.VTD1_Compensation_Quality_Check__c) {
                    // PI PAYMENTS Compensation Quality Check Task
                    tnCodesTasks.add('T568');
                    sourceIdsTasks.add(t.Id);
                } else if (t.Type == 'Review Lab Results' && t.VTD1_Actual_Visit__c != null) {
                    //KITS - Task Support Process PB // Review Lab Results task is Closed
                    reviewLabResActualV_ids.add(t.VTD1_Actual_Visit__c);
                    reviewLabResTasks.add(t);
                } else if (t.Subject == 'Ship Welcome Kit') {
                    //KITS - Task Support Process Pb // Ship Welcome Kit is complete
                    shipWelcomeKitSCTasksIds.add(t.WhatId);
                    shipWelcomeKitTasks.add(t);
                }
            }
        }
        if (!reviewLabResActualV_ids.isEmpty()) {
            Map<Id, VTD1_Actual_Visit__c> avMap = new Map <Id, VTD1_Actual_Visit__c> ([
                    SELECT Id, VTR2_Task_Assignment__c
                    FROM VTD1_Actual_Visit__c
                    WHERE Id IN :reviewLabResActualV_ids]);
            for (Task t : reviewLabResTasks) {
                if (avMap.get(t.VTD1_Actual_Visit__c).VTR2_Task_Assignment__c == 'Patient Guide') {
                    tnCodesTasks.add('225');
                    sourceIdsTasks.add(t.Id);
                } else if (avMap.get(t.VTD1_Actual_Visit__c).VTR2_Task_Assignment__c == 'Site Coordinator') {
                    tnCodesTasks.add('224');
                    sourceIdsTasks.add(t.Id);

                    tnCodesNotifications.add('N026');
                    sourceIdsNotifications.add(t.Id);
                }
            }
        }
        if (!shipWelcomeKitTasks.isEmpty()) {
            Set<VTD1_Order__c> deliveriesToUpd = new Set<VTD1_Order__c>();
            List<VTD1_SC_Task__c> scTasks = [
                    SELECT Id, VTD1_PatientDelivery__c
                    FROM VTD1_SC_Task__c
                    WHERE Id IN :shipWelcomeKitSCTasksIds
            ];
            for (VTD1_SC_Task__c scTask : scTasks) {
                if (scTask.VTD1_PatientDelivery__c != null) {
                    deliveriesToUpd.add(new VTD1_Order__c (Id = scTask.VTD1_PatientDelivery__c, VTD1_Status__c = 'Shipped'));
                }
            }
            if (!deliveriesToUpd.isEmpty()) {
                update new List<VTD1_Order__c>(deliveriesToUpd);
            }
        }
        if (!tnCodesTasks.isEmpty()) {
            VT_D2_TNCatalogTasks.generateTasks(tnCodesTasks, sourceIdsTasks, null, null);
            tnCodesTasks.clear();
            sourceIdsTasks.clear();
        }
        if (!tnCodesNotifications.isEmpty()) {
            VT_D2_TNCatalogNotifications.generateNotifications(tnCodesNotifications, sourceIdsNotifications, new Map<String, String>());
            tnCodesNotifications.clear();
            sourceIdsNotifications.clear();
        }
    }

    /*
    NotificationC for SCR when task is created
     */
    public static void generateNotificationsFromTasks (List<Task> tasks) {
        Set<String> taskCodesForNotificationGenerationSet1 = new Set<String> {'003', '009', '008', '303', '002', '307', '226', '227', '300', '301'};
        Set<String> taskCodesForNotificationGenerationSet1_subset = new Set<String> {'002', '003', '008','009','300', '303', '307'};

        Set<String> taskCodesForNotificationGenerationSet2 = new Set<String> {'310', '311'};
        Set<String> taskCodesForNotificationGenerationSet3 = new Set<String> {'005', '304'};

        List<VTD1_NotificationC__c> notificationCs = new List<VTD1_NotificationC__c>();
        for (Task t : tasks) {
            if (t.VTD1_OwnerProfileName__c == VT_R4_ConstantsHelper_Profiles.SCR_PROFILE_NAME) {
                if (t.VTD2_Task_Unique_Code__c == '312') {
                    tnCodesNotifications.add('N044');
                    sourceIdsNotifications.add(t.Id);
                } else if (t.VTD2_Task_Unique_Code__c == '313') {
                    tnCodesNotifications.add('N047');
                    sourceIdsNotifications.add(t.Id);
                } else if (taskCodesForNotificationGenerationSet2.contains(t.VTD2_Task_Unique_Code__c)) {
                    tnCodesNotifications.add('N038');
                    sourceIdsNotifications.add(t.Id);
                } else if (taskCodesForNotificationGenerationSet3.contains(t.VTD2_Task_Unique_Code__c)) {
                    tnCodesNotifications.add('N074');
                    sourceIdsNotifications.add(t.Id);
                } else if (taskCodesForNotificationGenerationSet1.contains(t.VTD2_Task_Unique_Code__c)) {
                    //
                    VTD1_NotificationC__c n = new VTD1_NotificationC__c(
                            VTD1_Receivers__c = t.OwnerId,
                            HasDirectLink__c = true,
                            Message__c = t.Subject,
                            Title__c = taskCodesForNotificationGenerationSet1_subset.contains(t.VTD2_Task_Unique_Code__c) ? 'Visits' : t.Category__c);

                    if (t.VTD2_Task_Unique_Code__c == '301') {
                        n.Link_to_related_event_or_object__c = 'study-documents?caseId=' + t.VTD1_Case_lookup__c;
                    } else if (t.VTD2_Task_Unique_Code__c == '227') {
                        n.Link_to_related_event_or_object__c = 'patient-menu?caseId=' + t.VTD1_Case_lookup__c;
                    } else if (t.VTD2_Task_Unique_Code__c == '226') {
                        n.Link_to_related_event_or_object__c = 'patient-menu?caseId=' + t.WhatId;
                    } else {
                        n.Link_to_related_event_or_object__c = 'visit?visitId=' + t.VTD1_Actual_Visit__c + '&caseId=' +t.VTD1_Case_lookup__c;
                    }
                    notificationCs.add(n);
                }
            }
            if (t.VTD1_Type_for_backend_logic__c == 'Final Eligibility Decision Task') {
                // RAND2 Final Eligibility Review Notification for PI PB
                tnCodesNotifications.add('N060');
                sourceIdsNotifications.add(t.Id);
            }
        }
        if (!notificationCs.isEmpty()) {
            insert notificationCs;
        }
    }

    /*
    Share Patient Task To Caregiver PB
     */
    public static void sharePatientTaskToCG(List<Task> tasks) {
        List<Task> patientTasks;
        Map<Id, List<Task>> patientsAndTasks = new Map<Id, List<Task>>();
        for (Task t : tasks) {
            if (t.VTD1_OwnerProfileName__c == VT_R4_ConstantsHelper_Profiles.PATIENT_PROFILE_NAME) {
                patientTasks = patientsAndTasks.get(t.OwnerId);
                if (patientTasks == null) {
                    patientTasks = new List<Task>();

                    patientsAndTasks.put(t.OwnerId, patientTasks);
                }
                patientTasks.add(t);
            }

        }
        if (!patientsAndTasks.isEmpty()) {
            List<Contact> caregiverContacts = [SELECT Id, Account.VTD2_Patient_s_Contact__r.VTD1_UserId__c
                            FROM Contact
                            WHERE Account.VTD2_Patient_s_Contact__r.VTD1_UserId__c IN :patientsAndTasks.keySet() AND RecordType.DeveloperName = 'Caregiver'];
            if (!caregiverContacts.isEmpty()) {
                List<TaskRelation> taskRelations = new List<TaskRelation>();
                Map<Id, Task> taskListRestoredWhoId = new Map<Id, Task>();
                for (Contact c : caregiverContacts) {
                    patientTasks = patientsAndTasks.get(c.Account.VTD2_Patient_s_Contact__r.VTD1_UserId__c);
                    for (Task t : patientTasks) {
                        taskRelations.add(new TaskRelation(RelationId = c.Id, TaskId = t.Id));
                        taskListRestoredWhoId.put(t.Id, new Task(Id = t.Id, WhoId = t.WhoId));
                    }
                }
                insert taskRelations;
                update taskListRestoredWhoId.values();
            }
        }
    }

    public static void updateTasks (List<Id> tasksToUpdLinkIds) {
        if (System.isBatch() || System.isFuture()) {
            updateTasksImmediatly(tasksToUpdLinkIds) ;
        } else {
            updateTasksFuture(tasksToUpdLinkIds) ;
        }
    }

    @Future
    public static void updateTasksFuture (List<Id> tasksToUpdLink) {
        updateTasksImmediatly(tasksToUpdLink) ;
    }

    public static void updateTasksImmediatly (List<Id> tasksToUpdLinkIds) {
        List<Task> tasksToUpdLink = new List<Task>();
        for (Id tId : tasksToUpdLinkIds) {
            tasksToUpdLink.add(new Task(Id = tId, VTD2_My_Task_List_Redirect__c = 'final-eligibility-review?id=' + tId));
        }
        update tasksToUpdLink;
    }

    public static VTD1_RTId__c getHardCodedIds () {
        if (rtIds == null) {
            rtIds = VTD1_RTId__c.getInstance();
        }
        return rtIds;
    }

    public class CancelledPatientTasksDeleter implements Queueable {
        private Set<Id> taskIdsToDelete;

        public CancelledPatientTasksDeleter(Set<Id> taskIdsToDelete) {
            this.taskIdsToDelete = taskIdsToDelete;
        }

        public void execute(QueueableContext context) {
            delete [SELECT Id FROM Task WHERE Id IN:taskIdsToDelete];
        }
    }

    private static Set<Id> queryCancelledPatients(List<Task> tasks) {
        Set<Id> cancelledPatientsIDs = new Set<Id>();
        Set<Id> ownerIds = new Set<Id>();
        Set<String> patientStatuses = new Set<String>{'Dropped', 'Dropped Treatment,Compl F/Up',
                                                        'Completed', 'Compl Treatment,Dropped F/Up', 'Closed'};
        for(Task t:tasks){
            ownerIds.add(t.OwnerId);
        }
        System.debug('sdsffsdf  ' + ownerIds);
//        System.debug('sdsffsdf  ' + [SELECT VTD1_Patient_User__c, Status, VTD1_End_of_study_visit_completed__c FROM Case]);

        for(Case c : [SELECT VTD1_Patient_User__c FROM Case WHERE Status IN :patientStatuses AND VTD1_Patient_User__c IN : ownerIds]) {
            cancelledPatientsIDs.add(c.VTD1_Patient_User__c);
        }

        return cancelledPatientsIDs;
    }

}