/**
 * Created by Yuliya Yakushenkova on 10/19/2020.
 */

public class VT_R5_MobilePatientPayments extends VT_R5_MobileRestResponse implements VT_R5_MobileLibrary.Routable {

    public List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/payments',
                '/patient/{version}/payments/{id}'
        };
    }

    public VT_R5_MobileRestResponse versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        String requestId = parameters.get('id');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        if (String.isNotBlank(requestId)){
                            if (requestId == 'sum') {
                                return get_v1_sum();
                            } else {
                                return get_v1(requestId);
                            }
                        } else {
                            return get_v1();
                        }
                    }
                }
            }
            when 'PATCH' {
                switch on requestVersion {
                    when 'v1' {
                        if (String.isNotBlank(requestId)) {
                            return patch_v1(requestId);
                        }
                    }
                }
            }
        }
        return null;

    }

    public Map<String, String> getMinimumVersions() {
        return null;
    }

    public void initService() {
    }

    private VT_R5_MobileRestResponse get_v1() {
        return this.buildResponse(new VT_R5_PatientPaymentsService(null).getPayments());
    }

    private VT_R5_MobileRestResponse get_v1_sum() {
        return this.buildResponse(new VT_R5_PatientPaymentsService(null).getPaymentsSum());
    }

    private VT_R5_MobileRestResponse get_v1(String id) {
        return this.buildResponse(new VT_R5_PatientPaymentsService(id).getPaymentById());
    }

    private VT_R5_MobileRestResponse patch_v1(String id) {
        String paymentName = new VT_R5_PatientPaymentsService(id).setPaymentCompleteAndGetPaymentName();
        return this.buildResponse(System.Label.VTR3_CompletePaymentActivity.replace('#1', paymentName));
    }
}