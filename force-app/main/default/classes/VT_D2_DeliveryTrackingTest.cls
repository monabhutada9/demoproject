/**
 * Created by User on 19/05/13.
 */
@isTest
public with sharing class VT_D2_DeliveryTrackingTest {

    public static void httpPostErrorMissingFieldsTest() {
        VT_D2_DeliveryTracking.DeliveryTrackingRequest reqst = new  VT_D2_DeliveryTracking.DeliveryTrackingRequest();
        String JsonMsg=JSON.serialize(reqst);
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/DeliveryTracking';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        VT_D2_DeliveryTracking.DeliveryTrackingResponse resp =new VT_D2_DeliveryTracking.DeliveryTrackingResponse();
        VT_D2_DeliveryTracking.doPost();
        System.assertEquals(400,  res.statusCode);
        Test.stopTest();
    }

    public static void httpPostSuccessTest() {
        VT_D2_DeliveryTracking.DeliveryTrackingRequest reqst = new  VT_D2_DeliveryTracking.DeliveryTrackingRequest();
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );
        DomainObjects.User_t user = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5));
         Case cas = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .addStudy(study)
                .addUser(user)
                .setVTD1_Subject_ID('subjectId')
                .persist();
        VTD1_Order__c order = (VTD1_Order__c) new DomainObjects.VTD1_Order_t()
                .setVTD1_Status('Shipped')
                .setVTD1_Case(cas.Id)
                .setVTD1_Expected_Shipment_Date(Date.today())
                .setVTD1_Delivery_Order(1)
                .setVTD1_ShipmentId('shipmentId')
                .persist();
        new DomainObjects.VTD1_Patient_Kit_t()
                .setVTD1_Case(cas.Id)
                .setVTD1_Patient_Delivery(order.Id)
                .setVTD2_MaterialId('materialId')
                .persist();

        reqst.protocolId = 'protocolId';
        reqst.subjectId = 'subjectId';
        reqst.shipmentName = 'shipmentName';
        reqst.shipmentId = 'shipmentId';
        reqst.trackingURL = 'http://google.com';
        reqst.carrier = 'carrier';
        reqst.carrierTrackingNumber = '1';
        reqst.carrierStatus = 'carrierStatus';
        reqst.carrierActivity = 'carrierActivity';
        reqst.deliveryDate = System.TODAY();
        reqst.shipmentStatus = 'Shipped';
        reqst.changeSource = 'SH';

        VT_D2_DeliveryTracking.Kit kit = new VT_D2_DeliveryTracking.Kit();
        kit.kitName = 'kit';
        kit.materialId = 'materialId';
        kit.kitType = 'Lab';
        kit.quantity = 2;
        reqst.kits = new List<VT_D2_DeliveryTracking.Kit>{kit};

        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/DeliveryTracking';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(JSON.serialize(reqst));
        RestContext.request = req;
        RestContext.response= res;
        VT_D2_DeliveryTracking.DeliveryTrackingResponse resp =new VT_D2_DeliveryTracking.DeliveryTrackingResponse();
        VT_D2_DeliveryTracking.doPost();
        System.assertNotEquals(null,res);
        Test.stopTest();
    }
}