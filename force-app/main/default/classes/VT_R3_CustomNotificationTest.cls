/**
 * @author: Dmitry Yartsev
 * date: 09.10.2019
 * @description: Test for VT_R3_CustomNotification
 */

@IsTest
public with sharing class VT_R3_CustomNotificationTest {
    public class MockCustomNotificationsHttpResponseGenerator implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            if (req.getMethod() == 'GET') {
                res.setBody('{"size":1,"totalSize":1,"done":true,"queryLocator":null,"entityTypeName":"CustomNotificationType","records":[{"attributes":{"type":"CustomNotificationType","url":"/services/data/v46.0/tooling/sobjects/CustomNotificationType/0ML8A0000008ONAWA2"},"Id":"0ML8A0000008ONAWA2"}]}');
                res.setStatusCode(200);
            }
            if (req.getMethod() == 'POST') {
                res.setBody('[{"actionName":"customNotificationAction","errors":null,"isSuccess":true,"outputValues":{"SuccessMessage":"Your custom notification is processed successfully."}}');
                res.setStatusCode(200);
            }
            return res;
        }
    }

    public static void test() {
        Test.setMock(HttpCalloutMock.class, new MockCustomNotificationsHttpResponseGenerator());
        VT_R3_CustomNotification.sendCustomNotifications(new List<Id>{UserInfo.getUserId()}, 'Title', 'Body', UserInfo.getUserId());
        VT_R3_CustomNotification.sendCustomNotifications(new List<VT_R3_CustomNotification.NotificationWrapper>{
                new VT_R3_CustomNotification.NotificationWrapper(
                        new List<Id>{UserInfo.getUserId()},
                        'Title',
                        'Body',
                        UserInfo.getUserId()
                )
        });
    }
}