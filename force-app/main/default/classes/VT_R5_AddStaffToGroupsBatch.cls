/**
* @author: Carl Judge
* @date: 12-Nov-20
* @description: 
**/

public without sharing class VT_R5_AddStaffToGroupsBatch implements Database.Batchable<SObject> {
    public static final String READ_ACCESS = 'Read';
    public static final String EDIT_ACCESS = 'Edit';

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
            SELECT Id, VTD1_Study__c, VTD1_User__c, VTD1_User__r.Profile.Name, VTD1_Access_Level__c,
                VTD1_Study__r.VTR5_EditShareGroupId__c, VTD1_Study__r.VTR5_ReadShareGroupId__c
            FROM VTD1_Study_Sharing_Configuration__c
            WHERE VTD1_Access_Level__c != 'None'
            AND (VTD1_User__r.Profile.Name IN :VT_R5_ShareGroupMembershipService.STUDY_LEVEL_PROFILES OR VTD1_User__r.Profile.Name IN :VT_R5_SiteMemberCalculator.SITE_PROFILES)
        ]);
    }

    public void execute(Database.BatchableContext bc, List<VTD1_Study_Sharing_Configuration__c> scope) {
        List<GroupMember> members = new List<GroupMember>();
        List<VTD1_Study_Sharing_Configuration__c> siteLevelConfigs = new List<VTD1_Study_Sharing_Configuration__c>();
        Set<Id> siteUserIds = new Set<Id>();
        Set<Id> siteStudyIds = new Set<Id>();

        for (VTD1_Study_Sharing_Configuration__c config : scope) {
            if (VT_R5_ShareGroupMembershipService.STUDY_LEVEL_PROFILES.contains(config.VTD1_User__r.Profile.Name)) {
                members.add(new GroupMember(
                    UserOrGroupId = config.VTD1_User__c,
                    GroupId = getGroupId(config.VTD1_Access_Level__c, config.VTD1_Study__r)
                ));
            } else {
                siteLevelConfigs.add(config);
                siteUserIds.add(config.VTD1_User__c);
                siteStudyIds.add(config.VTD1_Study__c);
            }
        }

        if (!siteLevelConfigs.isEmpty()) {
            VT_R5_SiteMemberCalculator calc = new VT_R5_SiteMemberCalculator();
            calc.initForUsers(siteUserIds, siteStudyIds);

            Set<Id> userIds = new Set<Id>();
            Set<Id> siteIds = new Set<Id>();
            Set<String> compositeKeys = new Set<String>();
            List<VT_R5_SiteMemberCalculator.SiteUser> siteUsers = calc.getSiteUsers();
            for (VT_R5_SiteMemberCalculator.SiteUser siteUser : siteUsers) {
                siteIds.add(siteUser.siteId);
                userIds.add(siteUser.userId);
                compositeKeys.add('' + siteUser.siteId + siteUser.userId);
            }

            Map<Id, Virtual_Site__c> siteMap = new Map<Id, Virtual_Site__c>([
                SELECT Id, VTR5_ReadShareGroupId__c, VTR5_EditShareGroupId__c, VTD1_Study__c
                FROM Virtual_Site__c
                WHERE Id IN :siteIds
            ]);


            List<Id> studyIds = new List<Id>();
            for (Virtual_Site__c site : siteMap.values()) {
                studyIds.add(site.VTD1_Study__c);
            }

            Map<Id, Map<Id, String>> accessLevelMap = new Map<Id, Map<Id, String>>();
            for (VTD1_Study_Sharing_Configuration__c config : [
                SELECT VTD1_User__c, VTD1_Access_Level__c, VTD1_Study__c
                FROM VTD1_Study_Sharing_Configuration__c
                WHERE VTD1_User__c IN :userIds
                AND VTD1_Study__c IN :studyIds
            ]) {
                if (!accessLevelMap.containsKey(config.VTD1_Study__c)) {
                    accessLevelMap.put(config.VTD1_Study__c, new Map<Id, String>());
                }
                accessLevelMap.get(config.VTD1_Study__c).put(config.VTD1_User__c, config.VTD1_Access_Level__c);
            }

            for (VT_R5_SiteMemberCalculator.SiteUser siteUser : siteUsers) {
                if (siteMap.containsKey(siteUser.siteId)) {
                    Virtual_Site__c site = siteMap.get(siteUser.siteId);
                    String accessLevel = accessLevelMap.get(site.VTD1_Study__c)?.get(siteUser.userId);
                    if (accessLevel == READ_ACCESS || accessLevel == EDIT_ACCESS) {
                        members.add(new GroupMember(
                            UserOrGroupId = siteUser.userId,
                            GroupId = getGroupId(accessLevel, site)
                        ));
                    }
                }
            }
        }

        insert members;
    }

    public void finish(Database.BatchableContext bc) {

    }

    public Id getGroupId(String accessLevel, SObject obj) {
        return accessLevel == READ_ACCESS
            ? (Id) obj.get('VTR5_ReadShareGroupId__c')
            : (Id) obj.get('VTR5_EditShareGroupId__c');
    }
}