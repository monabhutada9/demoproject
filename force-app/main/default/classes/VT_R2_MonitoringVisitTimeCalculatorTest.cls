/**
 * Refactored by Dmitry Kovalev on 10/09/20.
 */
@IsTest
public with sharing class VT_R2_MonitoringVisitTimeCalculatorTest {
    private static final String TIMEZONE_PI = 'America/New_York';
    private static final String TIMEZONE_PG = 'America/Los_Angeles';
    private static final Datetime DATETIME_VISIT = Datetime.newInstance(Date.today(), Time.newInstance(15, 0, 0, 0));

    public static void calculateTimesTest() {
        // Test Data
        DomainObjects.Study_t study_t = new DomainObjects.Study_t();

        DomainObjects.StudyTeamMember_t remoteCra_t = new DomainObjects.StudyTeamMember_t()
            .addStudy(study_t)
            .addUser(new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_Profiles.CRA_PROFILE_NAME)
            );

        Study_Team_Member__c remoteCra = (Study_Team_Member__c) remoteCra_t.persist();

        DomainObjects.User_t piUser_t = new DomainObjects.User_t()
            .addContact(new DomainObjects.Contact_t())
            .setTimezone(TimeZone.getTimeZone(TIMEZONE_PI))
            .setProfile(VT_R4_ConstantsHelper_Profiles.PI_PROFILE_NAME);

        DomainObjects.StudyTeamMember_t pi_t = new DomainObjects.StudyTeamMember_t()
            .addStudy(study_t)
            .addUser(piUser_t);
        //START SH-17440
        //    .setRemoteCraId(remoteCra.Id);
		//END:SH-17440



        Virtual_Site__c site = (Virtual_Site__c) new DomainObjects.VirtualSite_t()
            .addStudy(study_t)
            .addStudyTeamMember(pi_t)
            .persist();

        User pgUser = (User) new DomainObjects.User_t()
            .setProfile(VT_R4_ConstantsHelper_Profiles.PG_PROFILE_NAME)
            .setTimezone(TimeZone.getTimeZone(TIMEZONE_PG))
            .persist();
        List<Study_Team_Member__c> stms = [SELECT Id,User__r.Profile.Name FROM Study_Team_Member__c WHERE Study__c =:study_t.id AND User__r.Profile.Name = 'CRA' LIMIT 1];
        Study_Site_Team_Member__c sstm = new Study_Site_Team_Member__c();
        sstm.VTR5_Associated_CRA__c = stms[0].Id;
        sstm.VTD1_SiteID__c = site.Id;
        insert sstm;

        HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
        objVirtualShare.ParentId = study_t.Id;
        objVirtualShare.UserOrGroupId = stms[0].User__c;
        objVirtualShare.AccessLevel = 'Edit';
        insert objVirtualShare;
        VTD1_Monitoring_Visit__c mVisit  = new VTD1_Monitoring_Visit__c();
        mVisit.VTD1_Site_Visit_Type__c = 'Interim Visit';
        mVisit.VTR2_Remote_Onsite__c = 'Remote';
        mVisit.VTD1_Virtual_Site__c = site.Id;
        mVisit.VTD1_Study__c = study_t.id;
        mVisit.VTD1_Assigned_PI__c = piUser_t.id;
        mVisit.VTD1_Assigned_PG__c = pgUser.Id;
        mVisit.VTD1_Visit_Start_Date__c = DATETIME_VISIT;
        mVisit.VTD1_Study_Site_Visit_Status__c = 'Confirmed'; // to ignore "Monitoring Visit Processing (Scheduled)"

        System.runAs(new User(Id = stms[0].User__c)){
            insert mVisit;
        }
        // Calculations
        new VT_R2_MonitoringVisitTimeCalculator().calculateTimes(new List<SObject> { mVisit });

        // Asserts
        System.assertEquals(timeToMinutes(DATETIME_VISIT, TIMEZONE_PI), timeToMinutes(mVisit.VTD2_PI_scheduled_Time__c));
        System.assertEquals(timeToMinutes(DATETIME_VISIT, TIMEZONE_PG), timeToMinutes(mVisit.VTD2_PG_scheduled_time__c));
    }

    private static Integer timeToMinutes(String t) {
        return new VT_TimeParser(t).toMinutes();
    }

    private static Integer timeToMinutes(Datetime dt, String timeZoneSidKey) {
        return new VT_TimeParser(dt).toMinutes() + getOffsetInMinutes(timeZoneSidKey);
    }

    private static Integer getOffsetInMinutes(String timeZoneSidKey) {
        return Math.round(TimeZone.getTimeZone(timeZoneSidKey).getOffset(Datetime.now()) / 60000);
    }
}