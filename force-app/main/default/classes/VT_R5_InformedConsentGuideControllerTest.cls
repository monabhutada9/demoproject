/**
 * Created by Eugene Pavlovskiy on 13-May-20.
 */

@IsTest
private class VT_R5_InformedConsentGuideControllerTest {
    @TestSetup
    private static void setupMethod() {
        Test.startTest();
        VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
        VT_R3_GlobalSharing.disableForTest = true;
        User patient = [
                SELECT  Id,
                        Account.VTD2_Caregiver_s_ID__c,
                        AccountId,
                        ContactId,
                        Contact.AccountId,
                        Contact.VTR2_Primary_Language__c,
                        Contact.VTD1_Clinical_Study_Membership__c
                FROM User
                WHERE Profile.Name = 'Patient'
                AND Username LIKE '%@iqviavttest.com'
                AND IsActive = TRUE
                AND Contact.AccountId != NULL
                AND Contact.VTD1_Clinical_Study_Membership__c != NULL
                LIMIT 1
        ];

        Case cas= [
                SELECT  Id,
                        AccountId,
                        VTD1_Study__c,
                        RecordType.Name,
                        VTD2_Study_Geography__c,
                        VTD1_Virtual_Site__c
                FROM    Case
                WHERE   VTD1_Study__c != NULL
                AND RecordType.Name = 'CarePlan'
                AND Id =: patient.Contact.VTD1_Clinical_Study_Membership__c
        ];

        Virtual_Site__c virtualSite = [SELECT Id, VTR3_Study_Geography__c FROM Virtual_Site__c];
        VTD2_Study_Geography__c geography = [SELECT Id FROM VTD2_Study_Geography__c];
        virtualSite.VTR3_Study_Geography__c = geography.Id;
        update virtualSite;

        cas.VTD2_Study_Geography__c = geography.Id;
        update cas;

        VTD1_Document__c doc = new VTD1_Document__c();
        doc.VTD1_Clinical_Study_Membership__c = cas.Id;
        doc.VTR5_DocumentVersion__c = '1';
        doc.VTR5_DocumentTitle__c = 'TestDocumentTitle';
        doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_INFORMED_CONSENT_GUIDE;
        doc.VTD1_Study__c = cas.VTD1_Study__c;
        doc.VTR2_Language__c = patient.Contact.VTR2_Primary_Language__c;
        doc.VTR2_Geography__c = geography.Id;
        doc.VTR5_LastFileUploadedDate__c = Datetime.now().addMinutes(-5);
        insert doc;

        ContentVersion contentVersion = new ContentVersion();
        contentVersion.PathOnClient = 'test.txt';
        contentVersion.Title = 'Test file';
        contentVersion.VersionData = Blob.valueOf('Test Data');
        insert contentVersion;


    }
    @IsTest
    static void getDocumentsTest() {
        User patient = [
                SELECT  Id,
                        ContactId,
                        Contact.AccountId,
                        Contact.VTR2_Primary_Language__c,
                        Contact.VTD1_Clinical_Study_Membership__c
                FROM User
                WHERE Profile.Name = 'Patient'
                AND IsActive = TRUE
                AND Contact.AccountId != NULL
                AND Contact.VTD1_Clinical_Study_Membership__c != NULL
                LIMIT 1
        ];

        Case cas= [
                SELECT  Id,
                        VTD1_Study__c,
                        RecordType.Name,
                        VTD2_Study_Geography__c
                FROM    Case
                WHERE   VTD1_Study__c != NULL
                AND RecordType.Name = 'CarePlan'
                AND Id =: patient.Contact.VTD1_Clinical_Study_Membership__c
        ];

        VTD1_Document__c doc = new VTD1_Document__c();
        doc.VTD1_Clinical_Study_Membership__c = cas.Id;
        doc.VTR5_DocumentVersion__c = '1';
        doc.VTR5_DefaultDocument__c = true;
        doc.VTR5_DocumentTitle__c = 'TestDocumentTitle';
        doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_INFORMED_CONSENT_GUIDE;
        doc.VTD1_Study__c = cas.VTD1_Study__c;
        doc.VTR2_Language__c = patient.Contact.VTR2_Primary_Language__c;
        doc.VTR2_Geography__c = cas.VTD2_Study_Geography__c;
        doc.VTD1_Archived_Date_Stamp__c = Date.today();
        insert doc;
        VTD2_Study_Geography__c studyGeography = new VTD2_Study_Geography__c();
        studyGeography.VTR2_HierarchicalLevel__c = 1;
        studyGeography.VTD2_Study__c = cas.VTD1_Study__c;
        studyGeography.VTD2_Geographical_Region__c = 'Country';
        studyGeography.VTR3_Country__c = 'US';
        studyGeography.VTR2_StateProvince__c = 'NY,CA';

        ContentVersion contentVersion = new ContentVersion();
        contentVersion.PathOnClient = 'test.txt';
        contentVersion.Title = 'Test file';
        contentVersion.VersionData = Blob.valueOf('Test Data');
        insert contentVersion;

        ContentVersion contentVersion1 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id=:contentVersion.Id];

        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = doc.Id;
        contentDocumentLink.ContentDocumentId = contentVersion1.ContentDocumentId;
        contentDocumentLink.ShareType = 'I';
        contentDocumentLink.Visibility = 'AllUsers';
        insert contentDocumentLink;

        String querySettingsString = '{"sortingParams":null,"filterParams":[],"searchParams":null,"entriesOnPage":10,"currentPage":1}';
        Test.startTest();
        System.runAs(patient){
            VT_R5_InformedConsentGuideController.DocumentsWrapper aDocumentsWrapper = VT_R5_InformedConsentGuideController.getDocuments(querySettingsString);
            for (VT_R5_InformedConsentGuideController.ConsentPacketWrapper aConsentPacketWrapper : aDocumentsWrapper.documentsList){
                System.assertEquals(doc.VTR5_DocumentVersion__c,aConsentPacketWrapper.documentVersion);
                System.assertEquals(doc.VTR5_DocumentTitle__c,aConsentPacketWrapper.documentTitle);
                System.assertEquals(contentDocumentLink.ContentDocumentId,aConsentPacketWrapper.documentId);
            }
        }
        Test.stopTest();
    }


    @IsTest
    static void deleteContentDocTest() {
        VTD1_Document__c doc = [SELECT Id, VTR4_DeletionReason__c FROM VTD1_Document__c];
        ContentVersion contentVersion = [SELECT Id FROM ContentVersion];

        ContentVersion contentVersion1 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id=:contentVersion.Id];

        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = doc.Id;
        contentDocumentLink.ContentDocumentId = contentVersion1.ContentDocumentId;
        contentDocumentLink.ShareType = 'I';
        contentDocumentLink.Visibility = 'AllUsers';
        insert contentDocumentLink;

        doc.VTR4_DeletionReason__c = 'because';
        update doc;

        List<ContentDocument> listContentDocuments = [SELECT Id FROM ContentDocument];
        System.debug(listContentDocuments);
        delete listContentDocuments;

        Test.startTest();
        VTD1_Document__c docTest = [SELECT VTR5_LastFileUploadedDate__c FROM VTD1_Document__c LIMIT 1];
        system.assertEquals(null, docTest.VTR5_LastFileUploadedDate__c);
        Test.stopTest();
    }


    /**
    * VT_D1_CaseHandler.eConsentDocNotification; VT_R5_InformedConsentGuideController.getLastAppropriateDocumentByPTMap VT_R5_InformedConsentGuideController.prepareAppropriateDocumentMap
    * @author Viktar Bobich
    * @date 28/05/2020
    */
    @IsTest
    public static void eConsentNotifTest() {
        Case currentCase = [SELECT Id, VTD1_Reconsent_Needed__c FROM Case];
        currentCase.VTD1_Reconsent_Needed__c = true;
        update currentCase;
        List<VTD1_NotificationC__c> notificationList = [SELECT Id, VTD1_Parameters__c FROM VTD1_NotificationC__c];
        System.assertNotEquals(null, notificationList.size());
        System.assertEquals('TestDocumentTitle', notificationList[0].VTD1_Parameters__c);
    }
}