/**
* @author: Carl Judge
* @date: 06-Aug-20
**/

@IsTest
private class VT_R5_eCOAeDiaryCreatorTest {
    public static final String DIARY_JSON = '{"data":{"headers":["Subject ID","Randomization ID","Study Name","Study ID","Protocol Number","Protocol Title","Sponsor Name","Site Name","Schedule Name","Rule Name","Diary Name","Form Name","Widget Name","Widget Type","Widget Data Key","Operation","Value","Value Type","Old Value","Old Value Type","Response Started","Response Updated","Response Completed","Received Time","Event Time","Rule Triggered","Response Status","Modified","Active","Query Status","Locked","Response Locale","Country Code","Subject Status","Subject Has Phone","Rule Key","Trigger Reason","Record Version","Response GUID","Parent Response GUID","Subject GUID","Study GUID","Site GUID","Version GUID","Schedule GUID","Rule GUID","Diary GUID","Form GUID","Widget GUID","Device GUID","Reporting User","Clinician Name","Clinician GUID"],"rows":[["clinrotest2@mailinator.com",null,"TestStudy123","TestStudy123","TestStudy123","","TestStudy123","NewSite","ClinRO Schedule","TEST RULE","Clinro Diary","Clinro Question1","Selector 1","WIDGET_SELECTOR","selector_1","modified","three","stringArray","two","stringArray","2020-05-14T14:41:58+03:00","2020-06-03T15:49:18Z","2020-05-14T14:42:01+03:00","2020-05-14T11:42:03Z","2020-06-03T18:49:18+03:00","2020-05-14T00:00:00+03:00","changed","Yes","Active","approved","","en","US","Enabled","No","clinro_diary","resolvedDate study_subject_added occurred",2,"runtime_instance_e3d552c1-c3d2-4eda-9bad-e37f5d4b429a",null,"subject_0497a6d7-9a56-4a1b-af79-f985d5f9TEST","study_f76fbda9-05d0-4a32-84a3-9f114b53c8f0","site_7d4ff059-2a12-45d6-b63d-7b9f6d361120","version_be7f9a0e-397a-46fc-9ea4-182e8314e6e5","rule_set_91d16ced-f52b-42d1-ad76-23fa68008641","responseRule_fe6b6fcf-8c1e-4653-8bae-317edbf03f66","diary_67986566-6c98-4181-b019-60b31b610025","form_67422bfe-e5e7-440c-a0ea-f11a99e426e5","WIDGET_SELECTOR_6e1bbdca-9e1c-404d-b3f4-c5441e6252ee","f3638595fb06cb18","clinician","CarlClinician","2bc3fb27-b9b2-4d06-b095-d14dadfdd6b2"],["clinrotest2@mailinator.com",null,"TestStudy123","TestStudy123","TestStudy123","","TestStudy123","NewSite","ClinRO Schedule","TEST DIARY","Clinro Diary","Clinro Question1","Selector 1","WIDGET_SELECTOR","selector_1","added","two","stringArray","","unknown","2020-05-14T14:41:58+03:00","2020-05-14T14:42:01+03:00","2020-05-14T14:42:01+03:00","2020-05-14T11:42:03Z","2020-05-14T14:42:01+03:00","2020-05-14T00:00:00+03:00","saved","","Inactive","await_approval_three","","en","US","Enabled","No","clinro_diary","resolvedDate study_subject_added occurred",1,"runtime_instance_e3d552c1-c3d2-4eda-9bad-e37f5d4b429a",null,"subject_0497a6d7-9a56-4a1b-af79-f985d5f9TEST","study_f76fbda9-05d0-4a32-84a3-9f114b53c8f0","site_7d4ff059-2a12-45d6-b63d-7b9f6d361120","version_be7f9a0e-397a-46fc-9ea4-182e8314e6e5","rule_set_91d16ced-f52b-42d1-ad76-23fa68008641","responseRule_fe6b6fcf-8c1e-4653-8bae-317edbf03f66","diary_67986566-6c98-4181-b019-60b31b610025","form_67422bfe-e5e7-440c-a0ea-f11a99e426e5","WIDGET_SELECTOR_6e1bbdca-9e1c-404d-b3f4-c5441e6252ee","f3638595fb06cb18","clinician","CarlClinician","2bc3fb27-b9b2-4d06-b095-d14dadfdd6b2"],["Site123-001","maynotneed","TestStudy123","TestStudy123","TestStudy123","","TestStudy123","NewSite","Visits Schedule","Visit 1","Visit 1","Page 1","Selector","WIDGET_SELECTOR","selector","added","good","stringArray","","unknown","2020-04-20T16:54:01+03:00","2020-04-20T16:54:05+03:00","2020-04-20T16:54:05+03:00","2020-04-20T13:54:07Z","2020-04-20T16:54:05+03:00","2020-04-20T00:00:00+03:00","saved","","Active","","","en","US","Enabled","No","visit_1","resolvedDate visit_1 occurred",1,"runtime_instance_0956c2de-4003-430c-9e6b-88c19eb5d352",null,"subject_6c254006-03b5-4958-aa55-51ecd94ced05","study_f76fbda9-05d0-4a32-84a3-9f114b53c8f0","site_7d4ff059-2a12-45d6-b63d-7b9f6d361120","version_8ddd3086-0520-4bf5-9bfd-88d5cde9be5a","rule_set_d0db3dcf-5b69-485b-a582-e2af2bf11d37","responseRule_6236a36e-15c7-48de-8494-bf39524e0005","diary_4e8b137f-88f6-4023-ade6-dd77f8b29cd1","form_5e7ab8c8-5816-47e8-bd40-5ce573d51f09","WIDGET_SELECTOR_f6125700-a564-4f7d-9e5d-d96f2f9954b5","f3638595fb06cb18",null,null,null],["Site123-001","maynotneed","TestStudy123","TestStudy123","TestStudy123","","TestStudy123","NewSite","Visits Schedule","On Demand","On Demand Diary","Page 1","Selector","WIDGET_SELECTOR","selector","added","good","stringArray","","unknown","2020-04-20T12:50:54+03:00","2020-04-20T12:50:56+03:00","2020-04-20T12:50:56+03:00","2020-04-20T09:50:58Z","2020-04-20T12:50:56+03:00","2020-04-20T00:00:00+03:00","saved","","Active","","","en","US","Enabled","No","on_demand","resolvedDate study_subject_added occurred",1,"runtime_instance_77f9040e-e1e4-40a3-83e9-f93137275393",null,"subject_6c254006-03b5-4958-aa55-51ecd94ced05","study_f76fbda9-05d0-4a32-84a3-9f114b53c8f0","site_7d4ff059-2a12-45d6-b63d-7b9f6d361120","version_8ddd3086-0520-4bf5-9bfd-88d5cde9be5a","rule_set_d0db3dcf-5b69-485b-a582-e2af2bf11d37","responseRule_a94f0f23-7222-4ca7-98c2-6a2ffe9fc05f","diary_3f137af0-9404-424f-89ad-def7f24f3389","form_6706536b-77a7-4b54-9b38-14d9fc89c6f6","WIDGET_SELECTOR_49c62343-1f69-420b-92eb-f6aeaecdc049","f3638595fb06cb18",null,null,null],["Site123-001","maynotneed","TestStudy123","TestStudy123","TestStudy123","","TestStudy123","NewSite","Visits Schedule","On Demand","On Demand Diary","Page 1","Selector","WIDGET_SELECTOR","selector","added","good","stringArray","","unknown","2020-04-20T12:50:50+03:00","2020-04-20T12:50:53+03:00","2020-04-20T12:50:53+03:00","2020-04-20T09:50:54Z","2020-04-20T12:50:53+03:00","2020-04-20T00:00:00+03:00","saved","","Active","","","en","US","Enabled","No","on_demand","resolvedDate study_subject_added occurred",1,"runtime_instance_2ca9b350-f963-473c-8606-824b7d4c16a2",null,"subject_6c254006-03b5-4958-aa55-51ecd94ced05","study_f76fbda9-05d0-4a32-84a3-9f114b53c8f0","site_7d4ff059-2a12-45d6-b63d-7b9f6d361120","version_8ddd3086-0520-4bf5-9bfd-88d5cde9be5a","rule_set_d0db3dcf-5b69-485b-a582-e2af2bf11d37","responseRule_a94f0f23-7222-4ca7-98c2-6a2ffe9fc05f","diary_3f137af0-9404-424f-89ad-def7f24f3389","form_6706536b-77a7-4b54-9b38-14d9fc89c6f6","WIDGET_SELECTOR_49c62343-1f69-420b-92eb-f6aeaecdc049","f3638595fb06cb18",null,null,null],["Site123-001","maynotneed","TestStudy123","TestStudy123","TestStudy123","","TestStudy123","NewSite","Visits Schedule","On Demand","On Demand Diary","Page 1","Selector","WIDGET_SELECTOR","selector","added","good","stringArray","","unknown","2020-04-20T12:50:41+03:00","2020-04-20T12:50:47+03:00","2020-04-20T12:50:47+03:00","2020-04-20T09:50:49Z","2020-04-20T12:50:47+03:00","2020-04-20T00:00:00+03:00","saved","","Active","","","en","US","Enabled","No","on_demand","resolvedDate study_subject_added occurred",1,"runtime_instance_0e0675a7-6f37-457a-80b2-b476dbbf8fb2",null,"subject_6c254006-03b5-4958-aa55-51ecd94ced05","study_f76fbda9-05d0-4a32-84a3-9f114b53c8f0","site_7d4ff059-2a12-45d6-b63d-7b9f6d361120","version_8ddd3086-0520-4bf5-9bfd-88d5cde9be5a","rule_set_d0db3dcf-5b69-485b-a582-e2af2bf11d37","responseRule_a94f0f23-7222-4ca7-98c2-6a2ffe9fc05f","diary_3f137af0-9404-424f-89ad-def7f24f3389","form_6706536b-77a7-4b54-9b38-14d9fc89c6f6","WIDGET_SELECTOR_49c62343-1f69-420b-92eb-f6aeaecdc049","f3638595fb06cb18",null,null,null],["Site123-001","maynotneed","TestStudy123","TestStudy123","TestStudy123","","TestStudy123","NewSite","Visits Schedule","Visit 1","Visit 1","Page 1","Selector","WIDGET_SELECTOR","selector","added","bad","stringArray","","unknown","2020-04-20T12:50:34+03:00","2020-04-20T12:50:38+03:00","2020-04-20T12:50:38+03:00","2020-04-20T09:50:41Z","2020-04-20T12:50:38+03:00","2020-04-20T00:00:00+03:00","saved","","Active","","","en","US","Enabled","No","visit_1","resolvedDate visit_1 occurred",1,"runtime_instance_8a2b4b77-7231-4572-ab4d-2a4760d43993",null,"subject_6c254006-03b5-4958-aa55-51ecd94ced05","study_f76fbda9-05d0-4a32-84a3-9f114b53c8f0","site_7d4ff059-2a12-45d6-b63d-7b9f6d361120","version_8ddd3086-0520-4bf5-9bfd-88d5cde9be5a","rule_set_d0db3dcf-5b69-485b-a582-e2af2bf11d37","responseRule_6236a36e-15c7-48de-8494-bf39524e0005","diary_4e8b137f-88f6-4023-ade6-dd77f8b29cd1","form_5e7ab8c8-5816-47e8-bd40-5ce573d51f09","WIDGET_SELECTOR_f6125700-a564-4f7d-9e5d-d96f2f9954b5","f3638595fb06cb18",null,null,null],["Site123-001","maynotneed","TestStudy123","TestStudy123","TestStudy123","","TestStudy123","NewSite","Visits Schedule","On Demand","On Demand Diary","Page 1","Selector","WIDGET_SELECTOR","selector","added","good","stringArray","","unknown","2020-04-08T18:15:55+03:00","2020-04-08T18:15:58+03:00","2020-04-08T18:15:58+03:00","2020-04-08T15:15:59Z","2020-04-08T18:15:58+03:00","2020-04-08T00:00:00+03:00","saved","","Active","","","en","US","Enabled","No","on_demand","resolvedDate study_subject_added occurred",1,"runtime_instance_c622bb9e-0ae1-4341-8b7f-6db1d65ce02d",null,"subject_6c254006-03b5-4958-aa55-51ecd94ced05","study_f76fbda9-05d0-4a32-84a3-9f114b53c8f0","site_7d4ff059-2a12-45d6-b63d-7b9f6d361120","version_8ddd3086-0520-4bf5-9bfd-88d5cde9be5a","rule_set_d0db3dcf-5b69-485b-a582-e2af2bf11d37","responseRule_a94f0f23-7222-4ca7-98c2-6a2ffe9fc05f","diary_3f137af0-9404-424f-89ad-def7f24f3389","form_6706536b-77a7-4b54-9b38-14d9fc89c6f6","WIDGET_SELECTOR_49c62343-1f69-420b-92eb-f6aeaecdc049","f3638595fb06cb18",null,null,null],["Site123-001","maynotneed","TestStudy123","TestStudy123","TestStudy123","","TestStudy123","NewSite","Visits Schedule","On Demand","On Demand Diary","Page 1","Selector","WIDGET_SELECTOR","selector","added","normal","stringArray","","unknown","2020-04-08T18:15:50+03:00","2020-04-08T18:15:54+03:00","2020-04-08T18:15:54+03:00","2020-04-08T15:15:55Z","2020-04-08T18:15:54+03:00","2020-04-08T00:00:00+03:00","saved","","Active","","","en","US","Enabled","No","on_demand","resolvedDate study_subject_added occurred",1,"runtime_instance_a7670e8e-9d78-41c3-a909-f3317d8adfb7",null,"subject_6c254006-03b5-4958-aa55-51ecd94ced05","study_f76fbda9-05d0-4a32-84a3-9f114b53c8f0","site_7d4ff059-2a12-45d6-b63d-7b9f6d361120","version_8ddd3086-0520-4bf5-9bfd-88d5cde9be5a","rule_set_d0db3dcf-5b69-485b-a582-e2af2bf11d37","responseRule_a94f0f23-7222-4ca7-98c2-6a2ffe9fc05f","diary_3f137af0-9404-424f-89ad-def7f24f3389","form_6706536b-77a7-4b54-9b38-14d9fc89c6f6","WIDGET_SELECTOR_49c62343-1f69-420b-92eb-f6aeaecdc049","f3638595fb06cb18",null,null,null],["Site123-001","maynotneed","TestStudy123","TestStudy123","TestStudy123","","TestStudy123","NewSite","Visits Schedule","On Demand","On Demand Diary","Page 1","Selector","WIDGET_SELECTOR","selector","added","good","stringArray","","unknown","2020-04-08T18:15:45+03:00","2020-04-08T18:15:48+03:00","2020-04-08T18:15:48+03:00","2020-04-08T15:15:49Z","2020-04-08T18:15:48+03:00","2020-04-08T00:00:00+03:00","saved","","Active","","","en","US","Enabled","No","on_demand","resolvedDate study_subject_added occurred",1,"runtime_instance_b99581b3-9341-4053-b9c9-b5cf098bb07b",null,"subject_6c254006-03b5-4958-aa55-51ecd94ced05","study_f76fbda9-05d0-4a32-84a3-9f114b53c8f0","site_7d4ff059-2a12-45d6-b63d-7b9f6d361120","version_8ddd3086-0520-4bf5-9bfd-88d5cde9be5a","rule_set_d0db3dcf-5b69-485b-a582-e2af2bf11d37","responseRule_a94f0f23-7222-4ca7-98c2-6a2ffe9fc05f","diary_3f137af0-9404-424f-89ad-def7f24f3389","form_6706536b-77a7-4b54-9b38-14d9fc89c6f6","WIDGET_SELECTOR_49c62343-1f69-420b-92eb-f6aeaecdc049","f3638595fb06cb18",null,null,null],["Site123-001","maynotneed","TestStudy123","TestStudy123","TestStudy123","","TestStudy123","NewSite","Visits Schedule","On Demand","On Demand Diary","Page 1","Selector","WIDGET_SELECTOR","selector","added","bad","stringArray","","unknown","2020-04-08T18:15:37+03:00","2020-04-08T18:15:42+03:00","2020-04-08T18:15:42+03:00","2020-04-08T15:15:44Z","2020-04-08T18:15:42+03:00","2020-04-08T00:00:00+03:00","saved","","Active","","","en","US","Enabled","No","on_demand","resolvedDate study_subject_added occurred",1,"runtime_instance_ec556e4b-94ed-445a-9d05-1ba2702a7c69",null,"subject_6c254006-03b5-4958-aa55-51ecd94ced05","study_f76fbda9-05d0-4a32-84a3-9f114b53c8f0","site_7d4ff059-2a12-45d6-b63d-7b9f6d361120","version_8ddd3086-0520-4bf5-9bfd-88d5cde9be5a","rule_set_d0db3dcf-5b69-485b-a582-e2af2bf11d37","responseRule_a94f0f23-7222-4ca7-98c2-6a2ffe9fc05f","diary_3f137af0-9404-424f-89ad-def7f24f3389","form_6706536b-77a7-4b54-9b38-14d9fc89c6f6","WIDGET_SELECTOR_49c62343-1f69-420b-92eb-f6aeaecdc049","f3638595fb06cb18",null,null,null],["Site123-001","maynotneed","TestStudy123","TestStudy123","TestStudy123","","TestStudy123","NewSite","Visits Schedule","Visit 1","Visit 1","Page 1","Selector","WIDGET_SELECTOR","selector","added","normal","stringArray","","unknown","2020-04-01T20:22:33+03:00","2020-04-01T20:22:37+03:00","2020-04-01T20:22:37+03:00","2020-04-01T17:22:38Z","2020-04-01T20:22:37+03:00","2020-04-01T00:00:00+03:00","saved","","Active","","","en","US","Enabled","No","visit_1","resolvedDate visit_1 occurred",1,"runtime_instance_aa2d9187-90da-4952-af2e-857febd8496c",null,"subject_6c254006-03b5-4958-aa55-51ecd94ced05","study_f76fbda9-05d0-4a32-84a3-9f114b53c8f0","site_7d4ff059-2a12-45d6-b63d-7b9f6d361120","version_8ddd3086-0520-4bf5-9bfd-88d5cde9be5a","rule_set_d0db3dcf-5b69-485b-a582-e2af2bf11d37","responseRule_6236a36e-15c7-48de-8494-bf39524e0005","diary_4e8b137f-88f6-4023-ade6-dd77f8b29cd1","form_5e7ab8c8-5816-47e8-bd40-5ce573d51f09","WIDGET_SELECTOR_f6125700-a564-4f7d-9e5d-d96f2f9954b5","f3638595fb06cb18",null,null,null],["Site123-001","maynotneed","TestStudy123","TestStudy123","TestStudy123","","TestStudy123","NewSite","Visits Schedule","On Demand","On Demand Diary","Page 1","Selector","WIDGET_SELECTOR","selector","added","good","stringArray","","unknown","2020-04-01T20:22:24+03:00","2020-04-01T20:22:30+03:00","2020-04-01T20:22:30+03:00","2020-04-01T17:22:31Z","2020-04-01T20:22:30+03:00","2020-04-01T00:00:00+03:00","saved","","Active","","","en","US","Enabled","No","on_demand","resolvedDate study_subject_added occurred",1,"runtime_instance_d5374488-7591-4d2b-a200-79fc385234ee",null,"subject_6c254006-03b5-4958-aa55-51ecd94ced05","study_f76fbda9-05d0-4a32-84a3-9f114b53c8f0","site_7d4ff059-2a12-45d6-b63d-7b9f6d361120","version_8ddd3086-0520-4bf5-9bfd-88d5cde9be5a","rule_set_d0db3dcf-5b69-485b-a582-e2af2bf11d37","responseRule_a94f0f23-7222-4ca7-98c2-6a2ffe9fc05f","diary_3f137af0-9404-424f-89ad-def7f24f3389","form_6706536b-77a7-4b54-9b38-14d9fc89c6f6","WIDGET_SELECTOR_49c62343-1f69-420b-92eb-f6aeaecdc049","f3638595fb06cb18",null,null,null]]}}';
    public static final String SUBJECT_GUID = 'subject_0497a6d7-9a56-4a1b-af79-f985d5f9TEST';

    @TestSetup
    private static void setupMethod() {
        Test.startTest();
        DomainObjects.Account_t account_t = new DomainObjects.Account_t().setRecordTypeByName('Sponsor');
        DomainObjects.Study_t study = new DomainObjects.Study_t()
            .setMaximumDaysWithoutLoggingIn(1)
            .addAccount(account_t);
        study.persist();

        Account account = (Account) account_t.toObject();

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
            .setFirstName('Patient')
            .setLastName('Patient')
            .setAccountId(account.Id);

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
            .addContact(patientContact)
            .setProfile('Patient');
        patientUser.persist();

        Case patientCase = (Case) new DomainObjects.Case_t()
            .addPIUser(new DomainObjects.User_t())
            .addVTD1_Patient_User(patientUser)
            .setStudy(((HealthCloudGA__CarePlanTemplate__c) study.toObject()).Id)
            .setContactId(((User) patientUser.toObject()).ContactId)
            .setRecordTypeByName('CarePlan')
            .setAccountId(account.Id)
            .persist();

        update new Contact(Id = patientContact.id, VTD1_Clinical_Study_Membership__c = patientCase.Id);

        System.runAs(new User(Id = UserInfo.getUserId())) {
            update new User(Id = patientUser.id, VTR5_eCOA_Guid__c = SUBJECT_GUID);
        }
        Test.stopTest();
    }

    @IsTest
    private static void doTest() {
        VT_Stubber.applyStub(
            'VT_R5_eCOAeDiaryCreator.ruleToDiaryMapping',
            new List<VTR5_eCOA_Rule_to_Diary_Name_Mapping__mdt>{
                new VTR5_eCOA_Rule_to_Diary_Name_Mapping__mdt(
                    VTR5_RuleName__c = 'TEST RULE',
                    VTR5_DiaryName__c = 'TEST DIARY'
                )
            });

        VT_R5_eCoaResponseExport.Response response = (VT_R5_eCoaResponseExport.Response) JSON.deserialize(DIARY_JSON, VT_R5_eCoaResponseExport.Response.class);
        new VT_R5_eCOAeDiaryCreator(response).doCreate();
        
        System.assert(![SELECT Id FROM VTD1_Survey__c WHERE Name = 'TEST DIARY'].isEmpty());
    }

    @IsTest
    private static void testFromAPI() {
        VT_Stubber.applyStub(
            'VT_R5_eCOAeDiaryCreator.ruleToDiaryMapping',
            new List<VTR5_eCOA_Rule_to_Diary_Name_Mapping__mdt>{
                new VTR5_eCOA_Rule_to_Diary_Name_Mapping__mdt(
                    VTR5_RuleName__c = 'TEST RULE',
                    VTR5_DiaryName__c = 'TEST DIARY'
                )
            });


        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/eCoaResponseService';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(DIARY_JSON);

        RestContext.request = req;
        RestContext.response= res;

        Test.startTest();
        VT_R5_eCoaResponseRestService.processResponse();
        Test.stopTest();
        
        System.assert(![SELECT Id FROM VTD1_Survey__c WHERE Name = 'TEST DIARY'].isEmpty());
    }
}