public with sharing class VT_D2_CasePGReassignController {
    @AuraEnabled
    public static Case getCase(String caseId) {
        try {
            Case c = [SELECT Id, OwnerId, VTD1_PCF_Safety_Concern_Indicator__c, VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c, VTD1_Clinical_Study_Membership__r.VTD1_Secondary_PG__c FROM Case WHERE Id = :caseId];
            return c;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static void updateCase(String caseId) {
        try {
            Case c = new Case();
            c.Id = caseId;
            c.OwnerId = UserInfo.getUserId();
            update c;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
}