/**
 * Created by user on 3/27/2019.
 */

@IsTest
private class VT_R2_AssignmentHistoryTriggerTest {
/*
    @testSetup
    private static void setupMethod() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);

        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();

        Id studyId = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1].Id;
        User PrimaryPGUser = VT_D1_TestUtils.createUserByProfile('Patient Guide', 'PrimaryPGUser3');

        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();
        VT_D1_TestUtils.persistUsers();

        Study_Team_Member__c PGSTM = new Study_Team_Member__c();
        PGSTM.Study__c = study.Id;
        PGSTM.User__c = PrimaryPGUser.Id;
        PGSTM.VTD1_Active__c = true;
        PGSTM.VTD1_Ready_For_Assignment__c = true;
        PGSTM.RecordTypeId = VT_R4_ConstantsHelper_ProfilesSTM.RECORD_TYPE_ID_STUDY_TEAM_MEMBER_PG;
        insert PGSTM;

        VTD1_Patient_Guide_Capacity__c pgCapacity = new VTD1_Patient_Guide_Capacity__c();
        insert pgCapacity;

        VTD1_Assignment_History__c assignmentHistory = new VTD1_Assignment_History__c();
        assignmentHistory.VTR2_StudyTeamMember__c = PGSTM.Id;
        assignmentHistory.Assignment_History_Status__c = 'Current';
        assignmentHistory.VTR2_PatientGuideCapacity__c = pgCapacity.Id;
        insert assignmentHistory;
    }
    @IsTest
    private static void afterInsertTest() {
        VTD1_Patient_Guide_Capacity__c pgCapacity = [SELECT Id, VTR2_Rollup_Assignment_History__c FROM VTD1_Patient_Guide_Capacity__c LIMIT 1];
        VTD1_Assignment_History__c assignmentHistory = [SELECT Id, VTR2_StudyTeamMember__c FROM VTD1_Assignment_History__c LIMIT 1];
        VTD1_Assignment_History__c assignmentHistory1 = new VTD1_Assignment_History__c();
        assignmentHistory1.VTR2_StudyTeamMember__c = assignmentHistory.VTR2_StudyTeamMember__c;
        assignmentHistory1.Assignment_History_Status__c = 'Current';
        assignmentHistory1.VTR2_PatientGuideCapacity__c = pgCapacity.Id;

        Test.startTest();
        insert assignmentHistory1;
        Test.stopTest();
        VTD1_Patient_Guide_Capacity__c newPgCapacity = [SELECT Id, VTR2_Rollup_Assignment_History__c FROM VTD1_Patient_Guide_Capacity__c LIMIT 1];
        System.assertEquals(pgCapacity.VTR2_Rollup_Assignment_History__c + 1, newPgCapacity.VTR2_Rollup_Assignment_History__c);
    }

    @IsTest
    private static void afterUpdate() {
        VTD1_Assignment_History__c assignmentHistory = [
            SELECT Id, Assignment_History_Status__c, VTR2_PatientGuideCapacity__c
            FROM VTD1_Assignment_History__c
            WHERE VTR2_PatientGuideCapacity__c != null
            LIMIT 1
        ];
        VTD1_Patient_Guide_Capacity__c pgCapacity = [
            SELECT Id, VTR2_Rollup_Assignment_History__c
            FROM VTD1_Patient_Guide_Capacity__c
            WHERE Id = :assignmentHistory.VTR2_PatientGuideCapacity__c
        ];

        assignmentHistory.Assignment_History_Status__c = 'Transferred';

        Test.startTest();
        update assignmentHistory;
        Test.stopTest();
        VTD1_Patient_Guide_Capacity__c newPgCapacity = [SELECT Id, VTR2_Rollup_Assignment_History__c FROM VTD1_Patient_Guide_Capacity__c WHERE Id = :pgCapacity.Id];
        System.assertEquals(pgCapacity.VTR2_Rollup_Assignment_History__c - 1, newPgCapacity.VTR2_Rollup_Assignment_History__c);
    }

    @IsTest
    private static void afterDelete() {
        VTD1_Patient_Guide_Capacity__c pgCapacity = [SELECT Id, VTR2_Rollup_Assignment_History__c FROM VTD1_Patient_Guide_Capacity__c LIMIT 1];
        List<VTD1_Assignment_History__c> assignmentHistoryList = [SELECT Id FROM VTD1_Assignment_History__c WHERE VTR2_PatientGuideCapacity__c =: pgCapacity.Id LIMIT 1];

        Test.startTest();
        delete assignmentHistoryList;
        Test.stopTest();
        VTD1_Patient_Guide_Capacity__c newPgCapacity = [SELECT Id, VTR2_Rollup_Assignment_History__c FROM VTD1_Patient_Guide_Capacity__c LIMIT 1];
        System.assertEquals(pgCapacity.VTR2_Rollup_Assignment_History__c - 1, newPgCapacity.VTR2_Rollup_Assignment_History__c);
    }

    @IsTest
    private static void afterUndelete() {
        VTD1_Patient_Guide_Capacity__c pgCapacity = [SELECT Id, VTR2_Rollup_Assignment_History__c FROM VTD1_Patient_Guide_Capacity__c LIMIT 1];
        List<VTD1_Assignment_History__c> assignmentHistoryList = [SELECT Id FROM VTD1_Assignment_History__c WHERE VTR2_PatientGuideCapacity__c =: pgCapacity.Id];

        Test.startTest();
        delete assignmentHistoryList;
        undelete assignmentHistoryList;
        Test.stopTest();
        VTD1_Patient_Guide_Capacity__c newPgCapacity = [SELECT Id, VTR2_Rollup_Assignment_History__c FROM VTD1_Patient_Guide_Capacity__c LIMIT 1];
        System.assertEquals(pgCapacity.VTR2_Rollup_Assignment_History__c, newPgCapacity.VTR2_Rollup_Assignment_History__c);
    }
    */
}