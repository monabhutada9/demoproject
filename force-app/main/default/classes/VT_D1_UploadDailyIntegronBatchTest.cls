/**
 * Created by User on 19/05/16.
 * Edited by Galiya Khalikova on 27.12.2019.
 * Edited as part of SH-7022
 * Add needed tn catalog record
 */
@isTest
private with sharing class VT_D1_UploadDailyIntegronBatchTest {

    @testSetup
    private static void setup() {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                )
                .setVTD1_Integron_Bulk_Upload_Flag(true)
                .addVTD1_Virtual_Trial_Study_Lead(new DomainObjects.User_t());
        DomainObjects.VTD1_Protocol_Delivery_t pDelivery = new DomainObjects.VTD1_Protocol_Delivery_t()
                .addCare_Plan_Template(study);
        DomainObjects.VTD1_Protocol_Kit_t pKit = new DomainObjects.VTD1_Protocol_Kit_t()
                .setRecordTypeByName('VTD1_Study_Hub_Tablet')
                .addVTD1_Care_Plan_Template(study)
                .addVTD1_Protocol_Delivery(pDelivery);
        pKit.persist();
        new DomainObjects.HealthCloudGA_EhrDevice_t()
                .addVTD1_Care_Plan_Template(study)
                .addVTD1_Protocol_Kit(pKit)
                .setVTD1_Kit_Type('Study Hub Tablet')
                .persist();

        List <VTD2_TN_Catalog_Code__c> tnCatalogCodes = new List<VTD2_TN_Catalog_Code__c>();
        Test.startTest();
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'T565',
                VTD2_T_Subject__c = 'test',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Task').getRecordTypeId(),
                VTD2_T_Care_Plan_Template__c = 'HealthCloudGA__CarePlanTemplate__c.Id',
                VTD2_T_SObject_Name__c = 'HealthCloudGA__CarePlanTemplate__c',
                VTD2_T_Assigned_To_ID__c = 'HealthCloudGA__CarePlanTemplate__c.VTD1_Virtual_Trial_Study_Lead__c',
                VTD2_T_Related_To_Id__c = 'HealthCloudGA__CarePlanTemplate__c.Id',
                VTD2_T_Category__c = 'Other Tasks',
                VTD2_T_Due_Date__c = '' + Date.today()
        ));
        Test.stopTest();
        insert tnCatalogCodes;
    }

    @isTest
    private static void doTest() {
        Test.startTest();
        //System.schedule('Test Sched', '0 0 1 * * ?',  new VT_D1_UploadDailyIntegronBatch ());
        Database.executeBatch(new VT_D1_UploadDailyIntegronBatch());
        Test.stopTest();
        System.assertEquals(1, [SELECT Id FROM Task].size());
    }
}