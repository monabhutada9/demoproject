/**
 * Created by shume on 22/11/2018.
 */

public with sharing class VT_D2_SCTasksController {
    public class TaskInfo {
        @AuraEnabled public List<String> categories = new List<String>();
        @AuraEnabled public VT_R5_PagingQueryHelper.ResultWrapper result = new VT_R5_PagingQueryHelper.ResultWrapper();
        public void setCategories() {
            Schema.DescribeSObjectResult objDescribe = VTD1_SC_Task__c.getSObjectType().getDescribe();
            Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
            List<Schema.PicklistEntry> values = fieldMap.get('VTD2_Category__c').getDescribe().getPickListValues();
            for (Schema.PicklistEntry d: values) {
                this.categories.add(d.getValue());
            }
        }
    }

    @AuraEnabled
    public static String getSCTasksFromPool(String category, String jsonParams) {
        TaskInfo taskInfo = new TaskInfo();
        if (category == null) {
            taskInfo.setCategories();
        }
        taskInfo.result = getTasks(category, jsonParams);
        return JSON.serialize(taskInfo);
    }

    public static VT_R5_PagingQueryHelper.ResultWrapper getTasks(String category, String jsonParams) {
        // user groups
        List<String> groupIds = new List<String>();
        for (GroupMember gm : [SELECT GroupId
                                FROM GroupMember
                                WHERE UserOrGroupId = :UserInfo.getUserId() AND Group.Type != 'Queue']) {
            groupIds.add(gm.Id);
        }
        String query = 'SELECT Id, ' +
                'VTD1_Subject__c, ' +
                'VTD1_CarePlanTemplate__c, ' +
                'VTD1_CarePlanTemplate__r.Name, VTD1_Due_Date__c,' +
                'VTD2_Category__c ' +
                'FROM VTD1_SC_Task__c ';
        String filter = 'OwnerId IN (SELECT GroupId ' +
                'FROM GroupMember ' +
                'WHERE ((UserOrGroupId = \'' + UserInfo.getUserId() + '\' OR UserOrGroupId IN  (\'' + String.join(groupIds,'\',\'') +  '\'))' +
                ' AND Group.Type = \'Queue\'))';
        filter += category != null ? ' AND VTD2_Category__c IN (\'' + category + '\')' : '' ;
        System.debug(filter );
        String order = 'CreatedDate DESC';
        Map <String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(jsonParams);//new Map<String, Object>();
        params.put('filter', filter);
        params.put('order', order);
        params.put('groupBy', 'VTD2_Category__c');
        String sObjectName = (String)params.get('sObjectName');
        try {
            VT_R5_PagingQueryHelper.ResultWrapper wrapper = VT_R5_PagingQueryHelper.query(query, sObjectName, JSON.serialize(params));
            return wrapper;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
        }

    }


    @AuraEnabled
    public static void claimTasksRemote (List<Id> taskIds) {
        List<VTD1_SC_Task__c> tasksToUpdate = new List<VTD1_SC_Task__c>();
        List<VTD1_SC_Task__c> tasks =  [SELECT Id, OwnerId
                                        FROM VTD1_SC_Task__c
                                        WHERE Id IN :taskIds AND OwnerId!=:UserInfo.getUserId()
                                        FOR UPDATE ];
        for (VTD1_SC_Task__c t : tasks) {
            if (t.OwnerId.getSobjectType().getDescribe().getName() == 'Group') {
                tasksToUpdate.add(t);
            }
        }
        for (VTD1_SC_Task__c t : tasksToUpdate) {
            t.OwnerId = UserInfo.getUserId();
        }
        try {
            update tasks;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
        }
    }
}