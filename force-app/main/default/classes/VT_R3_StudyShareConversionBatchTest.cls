/**
* @author: Carl Judge
* @date: 08-Oct-19
**/

@IsTest
public with sharing class VT_R3_StudyShareConversionBatchTest {

    public static void doTest() {
        Test.startTest();
        Database.executeBatch(new VT_R3_StudyShareConversionBatch());
        Test.stopTest();
    }
}