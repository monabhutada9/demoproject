@IsTest
private class VTR2_SCR_ProfileController_Test {

    @TestSetup
    static void setup() {

        // For getSCRProfile

        DomainObjects.User_t scrUser = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t().setLastName('Con1'))
                .setProfile('Site Coordinator');
        DomainObjects.User_t piUser1 = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Primary Investigator');
        DomainObjects.User_t piUser2 = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Primary Investigator');


        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'));


        DomainObjects.StudyTeamMember_t stm1 = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('SCR')
                .addUser(scrUser)
                .addStudy(study);

        DomainObjects.StudyTeamMember_t stm2 = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('PI')
                .addUser(piUser2)
                .addStudy(study);

        DomainObjects.StudyTeamMember_t stm3 = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('PI')
                .addUser(piUser2)
                .addStudy(study);


        new DomainObjects.StudySiteTeamMember_t()
                .setAssociatedSCr(stm1)
                .setAssociatedPI2(stm2)
                .persist();

        new DomainObjects.StudySiteTeamMember_t()
                .setAssociatedSCr(stm1)
                .setAssociatedPI2(stm3)
                .persist();

        // For updateCurrentUserScrProfile

        DomainObjects.Account_t scrAccount = new DomainObjects.Account_t();
        DomainObjects.Contact_t scrContact = new DomainObjects.Contact_t()
                .setLastName('Con2')
                .addAccount(scrAccount);
        User scrUser1 = (User) new DomainObjects.User_t()
                .setProfile('Site Coordinator')
                .addContact(scrContact)
                .persist();

        VT_D1_Phone__c phone1 = (VT_D1_Phone__c) new DomainObjects.Phone_t(scrAccount, '123-4567')
                .setType('Mobile')
                .setPrimaryForPhone(true)
                .addContact(scrContact)
                .persist();

        VT_D1_Phone__c phone2 = (VT_D1_Phone__c) new DomainObjects.Phone_t(scrAccount, '234-5678')
                .setType('Work')
                .setPrimaryForPhone(false)
                .addContact(scrContact)
                .persist();

        VT_D1_Phone__c phone3 = (VT_D1_Phone__c) new DomainObjects.Phone_t(scrAccount, '345-6789')
                .setType('Home')
                .setPrimaryForPhone(false)
                .addContact(scrContact)
                .persist();

        VT_D1_Phone__c phone4 = (VT_D1_Phone__c) new DomainObjects.Phone_t(scrAccount, '456-7890')
                .setType('Home')
                .setPrimaryForPhone(false)
                .addContact(scrContact)
                .addAccount(scrAccount)
                .persist();

        VT_D1_Phone__c phone5 = (VT_D1_Phone__c) new DomainObjects.Phone_t(scrAccount, '345-6789')
                .setType('Home')
                .setPrimaryForPhone(false)
                .addContact(scrContact)
                .persist();
    }

    @IsTest
    static void getSCRProfileSuccess() {
        Test.startTest();
        System.runAs([SELECT Id FROM User WHERE Profile.Name = 'Site Coordinator' AND Contact.LastName = 'Con1'][0]) {
            System.assertNotEquals(null, VTR2_SCR_ProfileController.getCurrentUserScrProfile());
        }
        Test.stopTest();
    }

    @IsTest
    static void getSCRProfileFail() {
        Test.startTest();
        try {
            VTR2_SCR_ProfileController.getCurrentUserScrProfile();
        } catch (AuraHandledException e) {
            System.assertNotEquals(null, e.getMessage());
        }
        Test.stopTest();
    }


    @IsTest
    static void updateCurrentUserScrProfileSuccess() {

        User u = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        Contact scrContact = [SELECT Id, LastName, AccountId FROM Contact WHERE LastName = 'Con2'];
        User scrUser = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Site Coordinator' AND Contact.LastName = :scrContact.LastName LIMIT 1];
        VT_D1_Phone__c phone1 = [SELECT Id, PhoneNumber__c, VTD1_Contact__c, Type__c, IsPrimaryForPhone__c FROM VT_D1_Phone__c WHERE PhoneNumber__c = '123-4567' LIMIT 1];
        VT_D1_Phone__c phone2 = [SELECT Id, PhoneNumber__c, VTD1_Contact__c, Type__c, IsPrimaryForPhone__c FROM VT_D1_Phone__c WHERE PhoneNumber__c = '234-5678' LIMIT 1];
        VT_D1_Phone__c phone3 = [SELECT Id, PhoneNumber__c, VTD1_Contact__c, Type__c, IsPrimaryForPhone__c FROM VT_D1_Phone__c WHERE PhoneNumber__c = '345-6789' LIMIT 1];
        VT_D1_Phone__c phone4 = [SELECT Id, PhoneNumber__c, VTD1_Contact__c, Type__c, IsPrimaryForPhone__c FROM VT_D1_Phone__c WHERE PhoneNumber__c = '456-7890' LIMIT 1];
        VT_D1_Phone__c phone5 = [SELECT Id, PhoneNumber__c, VTD1_Contact__c, Type__c, IsPrimaryForPhone__c FROM VT_D1_Phone__c WHERE PhoneNumber__c = '345-6789' AND Id != :phone3.Id LIMIT 1];

        scrUser.Email = 'davidmason@scrtest.com.ua';
        scrUser.TimeZoneSidKey = 'America/Mazatlan';
        phone1.PhoneNumber__c = '765-4321';
        phone1.Type__c = 'Work';
        phone1.IsPrimaryForPhone__c = false;

        phone2.Type__c = 'Mobile';
        phone2.IsPrimaryForPhone__c = true;

        List<VT_D1_Phone__c> phonesToUpsert = new List<VT_D1_Phone__c>{
                phone1, phone2, phone4
        };
        List<VT_D1_Phone__c> phonesToDelete = new List<VT_D1_Phone__c>{
                phone3
        };

        VTR2_SCR_ProfileController.ProfileToUpdate profileToUpdate = new VTR2_SCR_ProfileController.ProfileToUpdate();
        profileToUpdate.user = scrUser;
        profileToUpdate.phonesToUpsert = phonesToUpsert;
        profileToUpdate.phonesToDelete = phonesToDelete;


        Test.startTest();
            System.runAs(u) {
                VTR2_SCR_ProfileController.updateCurrentUserScrProfile(JSON.serialize(profileToUpdate));
            }
        Test.stopTest();

        System.debug('USER INFO: ' + scrUser);

        Map<Id, VT_D1_Phone__c> updatedPhones = new Map<Id, VT_D1_Phone__c>([
                SELECT
                        Id
                        , PhoneNumber__c
                        , Type__c
                        , IsPrimaryForPhone__c,
                        VTD1_Contact__c
                FROM VT_D1_Phone__c
                WHERE VTD1_Contact__c = :scrUser.ContactId
        ]);
        System.debug('updated-phones: ' + updatedPhones);
        VT_D1_Phone__c insertedPhone;
        for (Id phoneId : updatedPhones.keySet()) {
            if (updatedPhones.get(phoneId).PhoneNumber__c == '765-4321') {
                insertedPhone = updatedPhones.get(phoneId);
            }
        }

        System.assertEquals('765-4321', updatedPhones.get(phone1.Id).PhoneNumber__c);
        System.assertEquals('Work', updatedPhones.get(phone1.Id).Type__c);
        System.assertEquals(false, updatedPhones.get(phone1.Id).IsPrimaryForPhone__c);
        System.assertEquals('Mobile', updatedPhones.get(phone2.Id).Type__c);
        System.assertEquals(true, updatedPhones.get(phone2.Id).IsPrimaryForPhone__c);
        System.assertEquals(null, updatedPhones.get(phone3.Id));
        System.assertNotEquals(null, insertedPhone);

        phonesToUpsert.get(0).PhoneNumber__c = '111-1111';
        phonesToUpsert.get(0).Type__c = 'Mobile';
        phonesToUpsert.get(1).PhoneNumber__c = '111-1111';
        phonesToUpsert.get(1).Type__c = 'Mobile';
        profileToUpdate.phonesToUpsert = phonesToUpsert;
        List<VT_D1_Phone__c> phonesToDelete2 = new List<VT_D1_Phone__c>{phone5};

        profileToUpdate.phonesToDelete = phonesToDelete2;

        System.runAs(u) {
           try {
               VTR2_SCR_ProfileController.updateCurrentUserScrProfile(JSON.serialize(profileToUpdate));
           } catch (Exception e){
               System.debug('Epx** '+e.getMessage());

           }
        }
    }

    @IsTest
    static void updateCurrentUserScrProfileFail() {
        Test.startTest();
        try {
            VTR2_SCR_ProfileController.updateCurrentUserScrProfile('');
        } catch (AuraHandledException e) {
            System.assertEquals('Script-thrown exception', e.getMessage());
        }
        Test.stopTest();
    }

}