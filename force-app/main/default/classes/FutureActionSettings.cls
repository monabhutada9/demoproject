/**
 * Created by triestelaporte on 11/6/20.
 */

public inherited sharing class FutureActionSettings {

    private static FutureActionSettings settingsCache;

    public String jobName;
    public String className;
    public Integer delayMinutes;
    public Integer maxRetryCount;
    public Integer batchSize;

    public enum ACTION_TYPE {
        CREATE_RECORD, UPDATE_RECORD, DELETE_RECORD, SEND_EMAIL
    }

    private FutureActionSettings () {
        // this being private forces calling code to use getSettings, and thus, the cache.
    }

    public static FutureActionSettings getSettings () {
        if (settingsCache == null) {
            settingsCache = new FutureActionSettings();

            // set defaults, so this will return values even if the custom settings are missing (like in unit tests)
            settingsCache.jobName = 'FutureActionsProcessor';
            settingsCache.className = 'FutureActionsProcessor';
            settingsCache.delayMinutes = 5;
            settingsCache.maxRetryCount = 5;
            settingsCache.batchSize = 200;

            // and here we would do something like CustomSettingsRecord__c.getAll(); to get the overrides
            // and set settingsCache.jobName = valueFromCustomSettings;
        }

        return settingsCache;
    }
}