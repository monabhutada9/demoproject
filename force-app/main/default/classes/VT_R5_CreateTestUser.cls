public class VT_R5_CreateTestUser{
    
    static Map<String, String> createTestUserList = new Map<String, String>{
                                            'PIUser' => 'Primary Investigator',
                                            'PIBackupUser' => 'Primary Investigator',
                                            'PrimaryPGUser' => 'Patient Guide',
                                            'SecondaryPGUser '=> 'Patient Guide',
                                            'ThirdPGUser'=>'Patient Guide',
                                            'FourthPGUser'=>'Patient Guide',
                                            'PatientUser'=>'Patient',
                                            'StudyConciergeUser'=>'Study Concierge',
                                            'VirtualTrialStudyLeadUser'=>'Virtual Trial Study Lead',
                                            'ProjectLeadUser'=>'Project Lead',
                                            'PMAUser'=>'Project Management Analyst',
                                            'RSUser'=>'Regulatory Specialist',
                                            'CRAUser1'=>'CRA',
                                            'MonitorReportReviewerUser'=>'Monitoring Report Reviewer',
                                            'VTHeadOfOperationsUser'=>'Virtual Trial Head of Operations',
                                            'SiteCoordinator'=>'Site Coordinator',
                                            'SiteCoordinatorBackup'=>'Site Coordinator',
                                            'StudyConciergeUser2'=>'Study Concierge'};
    
    static List<Profile> profileList = [SELECT Id, Name, UserType FROM Profile];
    static Map<String, Profile> profileMap = new Map<String, Profile>();
    static Map<String, Contact> contactMap = new Map<String, Contact>();
                                 
    static Map<Id, User> userMap = new Map<Id, User>();
    
    public static void createContactForTestUser(){
        
        List<Contact> contactList = new List<Contact>();
        
        for(Profile profile : profileList){
            profileMap.put(profile.Name, profile);
        }
        
        for(String key : createTestUserList.keySet()){           
            if(profileMap.get(createTestUserList.get(key)).UserType != 'Standard'){
                Contact contactObj = new Contact(FirstName = key, LastName = createTestUserList.get(key));
                contactList.add(contactObj);
            }            
        }
        
        if(!contactList.isEmpty()){
            insert contactList;
        }
    }
    
    public static void createTestUser() {
        
        List<User> initUsers = new List<User>();        
        
        UserRole role = new UserRole(DeveloperName = 'Standard', Name = 'Standard');        
        List<Contact> contactList = [Select Id, FirstName from Contact WHERE FirstName IN : createTestUserList.keySet()];
        
        for(Profile profile : profileList){
            profileMap.put(profile.Name, profile);
        }
        
        for(Contact con : contactList ){
            contactMap.put(con.FirstName, con);
        }
                    
        for(String  key : createTestUserList.keySet()){
        
            Profile profile = profileMap.get(createTestUserList.get(key));
            String uniqueUserName = generateUniqueUserName(); 
            
            User user = new User(
                    ProfileId = profile.Id,
                    FirstName = key,
                    LastName = profile.Name,
                    Email = uniqueUserName,
                    Username = uniqueUserName,
                    CompanyName = 'TEST',
                    Title = 'title',
                    Alias = 'alias',
                    TimeZoneSidKey = 'America/Los_Angeles',
                    EmailEncodingKey = 'UTF-8',
                    LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_US',
                    IsActive=true,
                    Is_Test_User__c = true,
                    UserRoleId = profile.UserType == 'Standard' ? role.Id : null
            );
            
            if(profile.UserType != 'Standard' && contactMap.containsKey(key)){
                user.ContactId = contactMap.get(key).Id;
            }
                        
            initUsers.add(user);
        }
        
        if(!initUsers.isEmpty()){
            insert initUsers;
        }
    }
    
    public static String generateUniqueUserName() {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueOf(Datetime.now()).replace(' ', '').replace(':', '').replace('-', '');
        Integer randomInt = getRandomInteger();
        String dynamicPart = orgId + dateString + randomInt;
        String staticPart = 'testuser';
        String uniqueUsername = staticPart + dynamicPart + '@iqviavttest.com';
        return uniqueUsername;
    }
    
    public static Integer getRandomInteger() {
        return Integer.valueOf(Math.rint(Math.random() * 1000000));
    }
}