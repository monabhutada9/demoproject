/**
 * Created by Mikhail Platonov on 10-Apr-20.
 *
 * @description This class is used to provide the coverage VT_R4_GeneralVisitsController, VT_R4_GeneralVisitsHelper.
 */
@isTest
public with sharing class VT_R4_GeneralVisitsHelperTest {
    @testSetup
    static void setup() {
        Set<String> profileSet = new Set<String>{'CRA', 'Primary Investigator', 'Site Coordinator'};
        List<Profile> profiles = [SELECT Id, Name FROM Profile WHERE Name IN :profileSet order by Name];
        System.debug('profiles = ' + profiles);
        List<User> users = new List<User>();
        User CRAUser = new User(
                ProfileId = profiles[0].Id,
                FirstName = 'CRA Test',
                LastName = 'LT' ,
                Email = VT_D1_TestUtils.generateUniqueUserName(),
                Username = VT_D1_TestUtils.generateUniqueUserName(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US', 
                LocaleSidKey = 'en_US',
                IsActive = true,
                VTR2_WelcomeEmailSent__c = true
        );
        List <Contact> contacts = new List<Contact>();
        contacts.add(new Contact(LastName = 'PI contact', Email = 'PI@email.com'));
        contacts.add(new Contact(LastName = 'SCR contact', Email = 'SCR@email.com'));
        insert contacts;
        User PIUser = new User(
                ProfileId = profiles[1].Id,
                FirstName = 'PI Test',
                LastName = 'LT' ,
                Email = VT_D1_TestUtils.generateUniqueUserName(),
                Username = VT_D1_TestUtils.generateUniqueUserName(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                IsActive = true,
                VTR2_WelcomeEmailSent__c = true,
                ContactId = contacts[0].Id
        );
        User SCRUser = new User(
                ProfileId = profiles[1].Id,
                FirstName = 'SCR Test',
                LastName = 'LT' ,
                Email = VT_D1_TestUtils.generateUniqueUserName(),
                Username = VT_D1_TestUtils.generateUniqueUserName(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                IsActive = true,
                VTR2_WelcomeEmailSent__c = true,
                ContactId = contacts[1].Id
        );
        users.add(CRAUser);
        users.add(PIUser);
        users.add(SCRUser);
        System.runAs(new User(Id = UserInfo.getUserId())){
            insert users;
        }
        List <VTR4_General_Visit__c> visits = new List<VTR4_General_Visit__c>();
        VTR4_General_Visit__c visit = new VTR4_General_Visit__c(VTR4_Duration__c  = 60.0,
                VTR4_Scheduled_Date_Time__c = System.now().addHours(2), VTR4_Status__c = 'Scheduled');
        visits.add(visit);
        visit = new VTR4_General_Visit__c(VTR4_Duration__c  = 60.0,
                VTR4_Scheduled_Date_Time__c = System.now().addHours(2), VTR4_Status__c = 'Scheduled');
        visits.add(visit);
        insert visits;
        List <VTR4_General_Visit_Member__c> members = new List<VTR4_General_Visit_Member__c>();
        members.add(new VTR4_General_Visit_Member__c(VTR4_General_Visit__c = visits[0].Id,
                VTR4_External_Member_Name__c = CRAUser.FirstName,
                VTR4_External_Participant_Email__c = CRAUser.Email,
                VTR4_Participant_User__c = CRAUser.Id));
        members.add(new VTR4_General_Visit_Member__c(VTR4_General_Visit__c = visits[0].Id,
                VTR4_External_Member_Name__c = PIUser.FirstName,
                VTR4_External_Participant_Email__c = PIUser.Email,
                VTR4_Participant_User__c = PIUser.Id));
        members.add(new VTR4_General_Visit_Member__c(VTR4_General_Visit__c = visits[1].Id,
                VTR4_External_Member_Name__c = CRAUser.FirstName,
                VTR4_External_Participant_Email__c = CRAUser.Email,
                VTR4_Participant_User__c = CRAUser.Id));
        members.add(new VTR4_General_Visit_Member__c(VTR4_General_Visit__c = visits[1].Id,
                VTR4_External_Member_Name__c = SCRUser.FirstName,
                VTR4_External_Participant_Email__c = SCRUser.Email,
                VTR4_Participant_User__c = SCRUser.Id));
        insert members;
        List <Event> events = new List<Event>();
        events.add(new Event(OwnerId = CRAUser.Id, StartDateTime = System.now().addDays(1).addHours(2), EndDateTime = System.now().addDays(1).addHours(3)));
        events.add(new Event(OwnerId = CRAUser.Id, StartDateTime = System.now().addDays(1).addHours(0), EndDateTime = System.now().addDays(1).addHours(1)));
        insert events;
        System.debug('events = ' + events);
    }
    @isTest
    public static void testEmptySlots() {
        List <VTR4_General_Visit_Member__c> members = [select Id, VTR4_Participant_User__c, VTR4_General_Visit__c from VTR4_General_Visit_Member__c];
        System.debug('duration 15');
        List <String> slots = VT_R4_GeneralVisitsController.getAvailableTimeSlots(members, 15, System.today().addDays(0));
        System.debug('slots number = ' + slots.size());
        for (String slot : slots) {
            System.debug('slot = ' + slot);
        }
        System.debug('duration 60');
        slots = VT_R4_GeneralVisitsController.getAvailableTimeSlots(members, 60, System.today().addDays(1));
        System.debug('slots number = ' + slots.size());
        for (String slot : slots) {
            System.debug('slot = ' + slot);
        }
    }
    @isTest
    public static void testGetVisit() {
        VTR4_General_Visit__c visit = [select Id from VTR4_General_Visit__c limit 1];
        VT_R4_GeneralVisitsController.getGeneralVisit(visit.Id);
    }
    @isTest
    public static void testUpdateVisit() {
        List <VTR4_General_Visit__c> visits = [select Id, VTR4_Scheduled_Date_Time__c, VTR4_Duration__c,
            (select Id, VTR4_Participant_User__r.FirstName, VTR4_External_Member_Name__c, VTR4_External_Participant_Email__c from General_Visit_Members__r) from VTR4_General_Visit__c];
        VTR4_General_Visit__c updatedVisit = null;
        VTR4_General_Visit__c newVisit = null;
        VTR4_General_Visit_Member__c updatedVisitMember = null;
        VTR4_General_Visit_Member__c newVisitMember = null;
        for (VTR4_General_Visit__c visit : visits) {
            System.debug('visit = ' + visit);
            for (VTR4_General_Visit_Member__c member : visit.General_Visit_Members__r) {
                System.debug('member = ' + member + ' ' + member.VTR4_Participant_User__r.FirstName);
                if (member.VTR4_Participant_User__r.FirstName == 'PI Test') {
                    updatedVisit = visit;
                } else if (member.VTR4_Participant_User__r.FirstName == 'SCR Test') {
                    newVisit = visit;
                    member.Id = null;
                }
            }
        }
        for (VTR4_General_Visit__c visit : visits) {
            System.debug('visit = ' + visit);
            for (VTR4_General_Visit_Member__c member : visit.General_Visit_Members__r) {
                System.debug('member = ' + member + ' ' + member.VTR4_Participant_User__r.FirstName);
                if (member.VTR4_Participant_User__r.FirstName == 'CRA Test') {
                    if (visit == updatedVisit) {
                        updatedVisitMember = member;
                    } else {
                        newVisitMember = member;
                    }
                }
            }
        }
        System.debug('updatedVisit = ' + updatedVisit);
        System.debug('newVisit = ' + newVisit);
        newVisit.Id = updatedVisit.Id;
        newVisitMember.Id = updatedVisitMember.Id;
        newVisit.VTR4_Scheduled_Date_Time__c = System.now().addDays(1).addHours(-5);
        VT_R4_GeneralVisitsController.GeneralVisitData data = new VT_R4_GeneralVisitsController.GeneralVisitData();
        data.visit = newVisit;
        data.members = newVisit.General_Visit_Members__r;
        System.runAs(new User(Id = UserInfo.getUserId())){
            VT_R4_GeneralVisitsController.updateGeneralVisit(data);
        }
    }
    @isTest
    public static void testNewVisit() {
        try {
            VTR4_General_Visit__c newVisit = [select Id, VTR4_Scheduled_Date_Time__c, VTR4_Duration__c,
            (select Id, VTR4_Participant_User__r.FirstName, VTR4_External_Member_Name__c, VTR4_External_Participant_Email__c from General_Visit_Members__r) from VTR4_General_Visit__c limit 1];
            for (VTR4_General_Visit_Member__c member : newVisit.General_Visit_Members__r) {
                member.Id = null;
            }
            newVisit.Id = null;
            newVisit.VTR4_Scheduled_Date_Time__c = System.now().addDays(1).addHours(-2);
            VT_R4_GeneralVisitsController.GeneralVisitData data = new VT_R4_GeneralVisitsController.GeneralVisitData();
            data.visit = newVisit;
            data.members = newVisit.General_Visit_Members__r;
            VT_R4_GeneralVisitsController.updateGeneralVisit(data);
        } catch (Exception e) {
            System.debug('testNewVisit error ' + e.getMessage());
        }
    }
    @isTest
    public static void testNewVisitNegative() {
        try {
            VTR4_General_Visit__c newVisit = [
                    select Id, VTR4_Scheduled_Date_Time__c, VTR4_Duration__c, (select Id, VTR4_Participant_User__r.FirstName, VTR4_External_Member_Name__c, VTR4_External_Participant_Email__c from General_Visit_Members__r)
                    from VTR4_General_Visit__c
                    limit 1
            ];
            for (VTR4_General_Visit_Member__c member : newVisit.General_Visit_Members__r) {
                member.Id = null;
            }
            newVisit.Id = null;
            newVisit.VTR4_Scheduled_Date_Time__c = System.now().addDays(1).addHours(2);
            VT_R4_GeneralVisitsController.GeneralVisitData data = new VT_R4_GeneralVisitsController.GeneralVisitData();
            data.visit = newVisit;
            data.members = newVisit.General_Visit_Members__r;
            VT_R4_GeneralVisitsController.updateGeneralVisit(data);
        } catch (Exception e) {
            System.debug('testNewVisitNegative error ' + e.getMessage());
        }
    }
    @isTest
    public static void testGetUsersForSearch() {
        VT_R4_GeneralVisitsController.getUsersForSearchComponent();
    }
}