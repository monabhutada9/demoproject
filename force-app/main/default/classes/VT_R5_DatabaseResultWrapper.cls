/**
* @author: Carl Judge
* @date: 02-Oct-20
* @description: Wrap a list of database results for generic access to methods
**/

public abstract class VT_R5_DatabaseResultWrapper {
    public abstract Boolean isSuccess();
    public abstract List<Database.Error> getErrors();
    public abstract Id getId();
    public virtual Boolean isCreated() { return null; }
    public abstract Type getType();
    private abstract void setResult(Object result);

    public static List<VT_R5_DatabaseResultWrapper> wrapResults(List<Object> results) {
        Type wrapperType = getWrapperType(results);
        List<VT_R5_DatabaseResultWrapper> wrappedResults = new List<VT_R5_DatabaseResultWrapper>();
        for (Object result : results) {
            VT_R5_DatabaseResultWrapper wrappedResult = (VT_R5_DatabaseResultWrapper) wrapperType.newInstance();
            wrappedResult.setResult(result);
            wrappedResults.add(wrappedResult);
        }
        return wrappedResults;
    }

    private static Type getWrapperType(List<Object> results) {
        if (results instanceof List<Database.SaveResult>) {
            return WrappedSaveResult.class;
        } else if (results instanceof List<Database.UpsertResult>) {
            return WrappedUpsertResult.class;
        } else if (results instanceof List<Database.DeleteResult>) {
            return WrappedDeleteResult.class;
        } else if (results instanceof List<Database.UndeleteResult>){
            return WrappedUndeleteResult.class;
        } else {
            throw new DatabaseResultWrapperException('Invalid Type');
        }
    }

    public class WrappedSaveResult extends VT_R5_DatabaseResultWrapper {
        private Database.SaveResult saveResult;
        private override void setResult(Object saveResult) {
            this.saveResult = (Database.SaveResult) saveResult;
        }
        public override Boolean isSuccess() {
            return saveResult.isSuccess();
        }
        public override List<Database.Error> getErrors() {
            return saveResult.getErrors();
        }
        public override Id getId() {
            return saveResult.getId();
        }
        public override Type getType() {
            return WrappedSaveResult.class;
        }
    }

    public class WrappedUpsertResult extends VT_R5_DatabaseResultWrapper {
        private Database.UpsertResult upsertResult;
        private override void setResult(Object upsertResult) {
            this.upsertResult = (Database.UpsertResult) upsertResult;
        }
        public override Boolean isSuccess() {
            return upsertResult.isSuccess();
        }
        public override List<Database.Error> getErrors() {
            return upsertResult.getErrors();
        }
        public override Id getId() {
            return upsertResult.getId();
        }
        public override Boolean isCreated() {
            return upsertResult.isCreated();
        }
        public override Type getType() {
            return WrappedUpsertResult.class;
        }
    }

    public class WrappedDeleteResult extends VT_R5_DatabaseResultWrapper {
        private Database.DeleteResult deleteResult;
        private override void setResult(Object deleteResult) {
            this.deleteResult = (Database.DeleteResult) deleteResult;
        }
        public override Boolean isSuccess() {
            return deleteResult.isSuccess();
        }
        public override List<Database.Error> getErrors() {
            return deleteResult.getErrors();
        }
        public override Id getId() {
            return deleteResult.getId();
        }
        public override Type getType() {
            return WrappedDeleteResult.class;
        }
    }

    public class WrappedUndeleteResult extends VT_R5_DatabaseResultWrapper {
        private Database.UndeleteResult undeleteResult;
        private override void setResult(Object undeleteResult) {
            this.undeleteResult = (Database.UndeleteResult) undeleteResult;
        }
        public override Boolean isSuccess() {
            return undeleteResult.isSuccess();
        }
        public override List<Database.Error> getErrors() {
            return undeleteResult.getErrors();
        }
        public override Id getId() {
            return undeleteResult.getId();
        }
        public override Type getType() {
            return WrappedUndeleteResult.class;
        }
    }

    public class DatabaseResultWrapperException extends Exception{}
}