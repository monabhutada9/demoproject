/**
 * Created by Oleh Berehovskyi on 26-Jun-19.
 */

@IsTest
private class VT_D1_ShareUtilitiesTest {

    @IsTest
    static void getShareObjectNameTest() {
        String expectedResult1 = 'AccountShare';
        String expectedResult2 = 'VT_D1_Document__Share';

        Test.startTest();
            String accountShareObjectName = VT_D1_ShareUtilities.getShareObjectName('Account');
            String documentShareObjectName = VT_D1_ShareUtilities.getShareObjectName('VT_D1_Document__c');
        Test.stopTest();

        System.assertEquals(expectedResult1, accountShareObjectName);
        System.assertEquals(expectedResult2, documentShareObjectName);
    }

    @IsTest
    static void getShareObjectParentFieldTest() {
        String expectedResult1 = 'Account';
        String expectedResult2 = 'Parent';

        Test.startTest();
            String accountShareObjectName = VT_D1_ShareUtilities.getShareObjectParentField('Account');
            String documentShareObjectName = VT_D1_ShareUtilities.getShareObjectParentField('VT_D1_Document__c');
        Test.stopTest();

        System.assertEquals(expectedResult1, accountShareObjectName);
        System.assertEquals(expectedResult2, documentShareObjectName);
    }

    @IsTest
    static void getShareObjectAccessLevelFieldTest() {
        String expectedResult1 = 'AccountAccessLevel';
        String expectedResult2 = 'AccessLevel';

        Test.startTest();
            String accountShareObjectName = VT_D1_ShareUtilities.getShareObjectAccessLevelField('Account');
            String documentShareObjectName = VT_D1_ShareUtilities.getShareObjectAccessLevelField('VT_D1_Document__c');
        Test.stopTest();

        System.assertEquals(expectedResult1, accountShareObjectName);
        System.assertEquals(expectedResult2, documentShareObjectName);
    }

    @IsTest
    static void isCustomObjectTest() {
        Test.startTest();
            Boolean isAccountCustomObject = VT_D1_ShareUtilities.isCustomObject('Account');
            Boolean isVTDocumentCustomObject = VT_D1_ShareUtilities.isCustomObject('VT_D1_Document__c');
        Test.stopTest();

        System.assertEquals(false, isAccountCustomObject);
        System.assertEquals(true, isVTDocumentCustomObject);
    }

    @IsTest
    static void getFieldNameFromPathTest() {
        String expectedResult1 = 'AccountId';
        String expectedResult2 = 'VT_D1_Document__c';

        Test.startTest();
            String accountShareObjectName = VT_D1_ShareUtilities.getFieldNameFromPath('Account');
            String documentShareObjectName = VT_D1_ShareUtilities.getFieldNameFromPath('VT_D1_Document__r');
        Test.stopTest();

        System.assertEquals(expectedResult1, accountShareObjectName);
        System.assertEquals(expectedResult2, documentShareObjectName);
    }
}