/**
 * Created by Alexey Mezentsev on 10/13/2020.
 */

public without sharing class VT_R5_ActualVisitScheduledActionsEvent extends VT_R3_AbstractPublishedAction {
    private List<VT_R5_ActualVisitScheduledActions.ScheduledActionData> scheduledActions = new List<VT_R5_ActualVisitScheduledActions.ScheduledActionData>();

    public VT_R5_ActualVisitScheduledActionsEvent(List<VT_R5_ActualVisitScheduledActions.ScheduledActionData> scheduledActions) {
        this.scheduledActions = scheduledActions;
    }

    public override Type getType() {
        return VT_R5_ActualVisitScheduledActionsEvent.class;
    }

    public override void execute() {
        System.enqueueJob(new VT_R5_ActualVisitScheduledActions(scheduledActions));
    }
}