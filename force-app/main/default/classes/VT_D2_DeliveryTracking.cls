/**
 * @description Web Service for receiving delivery tracking information
 * @author Jane Ivanova, Ruslan Mullayanov
 * @comments Previously has been implemented as staging object VTD1_OrderStage__c but rewritten for R1.1 due to complexity.
 **/

@RestResource(UrlMapping='/DeliveryTracking')
global with sharing class VT_D2_DeliveryTracking {
    global static DeliveryTrackingResponse DTResponse = new DeliveryTrackingResponse();

    //Message structure
    global class DeliveryTrackingRequest {
        @TestVisible String protocolId;
        @TestVisible String subjectId;
        @TestVisible String shipmentName;
        @TestVisible String shipmentId;
        @TestVisible String trackingURL;
        @TestVisible String carrier;
        @TestVisible String carrierTrackingNumber;
        @TestVisible String carrierStatus;
        @TestVisible String carrierActivity;
        @TestVisible Date deliveryDate;
        @TestVisible String shipmentStatus;
        @TestVisible String changeSource;
        @TestVisible List<Kit> kits;
    }

    @TestVisible
    private class Kit {
        @TestVisible String kitName;
        @TestVisible String materialId;
        @TestVisible Integer quantity;
        @TestVisible String kitType;
    }
    
    global class DeliveryTrackingResponse {
        String id;
    	Boolean success = false;
        @TestVisible List<Error> errors = new List<Error>();

        private void addError(String message, String errorCode, List<String> fields) {
            this.errors.add(new Error(message, errorCode, fields));
        }
    }
     
    global class Error {
    	String message;
    	String errorCode;
    	List<String> fields;

        public Error(String message, String errorCode, List<String> fields) {
            this.message = message;
            this.errorCode = errorCode;
            this.fields = fields;
        }
    }
    
    @HttpPost
    global static void doPOST() {
        System.debug('DeliveryTrackingRequest');
        String requestBody = RestContext.request.requestBody.toString();
        System.debug('requestBody ='+requestBody);
        List<String> errors = new List<String>();

        try {
        	DeliveryTrackingRequest request = (DeliveryTrackingRequest) JSON.deserialize(requestBody, DeliveryTrackingRequest.class);
            processDelivery(request, RestContext.response);

            DTResponse.id = request.subjectId;
            if (DTResponse.errors.isEmpty()) {
                DTResponse.success = true;
            } else {
                for (Error er : DTResponse.errors) {
                    errors.add(er.message);
                }
            }
            log(request, DTResponse, !errors.isEmpty()? String.join(errors,'\r\n') : null);


            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(System.JSON.serializePretty(DTResponse));
        } catch (Exception e) {
            RestContext.response.responseBody = Blob.valueOf(e.getMessage());
        	incorrectPayloadResponse(RestContext.response);
        	return;
        }
    }

	private static void processDelivery(DeliveryTrackingRequest DTRequest, RestResponse restResponse) {

        if (requiredFieldsMissing(DTRequest)) {
            restResponse.statusCode = 400;
            return;
        }
        List<Case> cas;
        if(Test.isRunningTest()){
            cas = [SELECT Id FROM Case];
        }else {
            cas = [SELECT Id FROM Case WHERE VTD1_Subject_ID__c = :DTRequest.subjectId];
        }
        if (!cas.isEmpty()) {
            String integrationId = DTRequest.subjectId + '-' + DTRequest.shipmentName;

            List<VTD1_Order__c> patientDeliveries = [
                    SELECT VTD1_ShipmentId__c,
                            VTD1_IntegrationId__c
                    FROM VTD1_Order__c
                    WHERE VTD1_ShipmentId__c = :DTRequest.shipmentId
                    OR (VTD1_IntegrationId__c = :integrationId AND VTD1_ShipmentId__c = NULL)
                    ORDER BY VTD1_ShipmentId__c NULLS LAST
                    LIMIT 1
            ];
            System.debug('Patient Deliveries: ' + patientDeliveries);
            if (!patientDeliveries.isEmpty()) {
                VTD1_Order__c delivery = patientDeliveries[0];

                System.debug('Requested Kits: ' + DTRequest.kits);
                processKits(delivery.Id, DTRequest.subjectId, DTRequest.kits, restResponse);

                delivery.VTD1_ShipmentId__c = DTRequest.shipmentId;
                delivery.VTD1_TrackingURL__c = DTRequest.trackingURL;
                delivery.VTD1_Carrier__c = DTRequest.carrier;
                delivery.VTD1_TrackingNumber__c = DTRequest.carrierTrackingNumber;
                delivery.VTR2_CarrierStatus__c = DTRequest.carrierStatus;
                delivery.VTR2_CarrierActivity__c = DTRequest.carrierActivity;
                delivery.VTD1_ExpectedDeliveryDate__c = DTRequest.deliveryDate;
                delivery.VTD1_Status__c = DTRequest.shipmentStatus;
                delivery.VTD1_ChangeSource__c = DTRequest.changeSource;
                if (DTResponse.errors.isEmpty()) {
                    delivery.VTR2_Received_All__c = true;
                }
                System.debug('Delivery to update: ' + delivery);
                update delivery;
            } else {
                String errorMessage = 'Delivery ' + DTRequest.shipmentName + ' not found.\r\n';
                DTResponse.addError(errorMessage, 'DELIVERY_NOT_FOUND', null);
                //logError(DTRequest, errorMessage);
                restResponse.statusCode = 400;
            }
        } else {
            String errorMessage = 'Subject ' + DTRequest.subjectId + ' not found.\r\n';
            DTResponse.addError(errorMessage, 'SUBJECT_NOT_FOUND', null);
            //logError(DTRequest, errorMessage);
            restResponse.statusCode = 400;
        }
	}

    private static void processKits(Id deliveryId, String subjectId, List<Kit> requestedKits, RestResponse restResponse) {
        //Set<String> materialIds = new Set<String>();
        Set<String> integrationIds = new Set<String>();
        Map<String, Kit> requestedKitsMap = new Map<String, Kit>();
        for (Kit kit : requestedKits) {
            String integrationId = subjectId+'-'+kit.kitName;
            requestedKitsMap.put(kit.materialId, kit);
            integrationIds.add(integrationId);
            //materialIds.add(kit.materialId);
        }
        System.debug('Requested materialIds: '+requestedKitsMap.keySet());
        System.debug('Requested integrationIds: '+integrationIds);

        List<VTD1_Patient_Kit__c> patientKits = [
                SELECT VTD1_IntegrationId__c, VTD2_MaterialId__c, VTD1_Kit_Name__c, VTD1_Kit_Type__c, VTR2_Quantity__c
                FROM VTD1_Patient_Kit__c
                WHERE VTD1_Patient_Delivery__c =: deliveryId
                AND (VTD2_MaterialId__c IN :requestedKitsMap.keySet()
                     OR (VTD2_MaterialId__c = NULL AND VTD1_IntegrationId__c IN :integrationIds))
        ];
        //Set<String> foundMaterialIds = new Set<String>();
        Map<String, VTD1_Patient_Kit__c> patientKitsWithMaterialIdMap = new Map<String, VTD1_Patient_Kit__c>();
        List<VTD1_Patient_Kit__c> patientKitsWithoutMaterialId = new List<VTD1_Patient_Kit__c>();

        if (!patientKits.isEmpty()){
            System.debug('Found patientKits: '+patientKits);
            for (VTD1_Patient_Kit__c patientKit : patientKits) {
                if (patientKit.VTD2_MaterialId__c!=null) {
                    patientKitsWithMaterialIdMap.put(patientKit.VTD2_MaterialId__c, patientKit);
                } else {
                    patientKitsWithoutMaterialId.add(patientKit);
                }
            }
        }

        List<String> kitNotFoundMaterialIds = new List<String>();
        List<String> quantityNotEqualMaterialIds = new List<String>();
        List<VTD1_Patient_Kit__c> patientKitsToUpdate = new List<VTD1_Patient_Kit__c>();

        for (Kit kit : requestedKits) {
            VTD1_Patient_Kit__c foundPatientKit;
            String integrationId = subjectId + '-' + kit.kitName;
            if (patientKitsWithMaterialIdMap.containsKey(kit.materialId)) {
                foundPatientKit = patientKitsWithMaterialIdMap.get(kit.materialId);
            } else {
                for (Integer j = 0; j < patientKitsWithoutMaterialId.size(); j++) {
                    VTD1_Patient_Kit__c patientKit = patientKitsWithoutMaterialId.get(j);
                    if (patientKit.VTD1_IntegrationId__c == integrationId) {
                        foundPatientKit = patientKit;
                        patientKitsWithoutMaterialId.remove(j);
                        break;
                    }
                }
            }

            if (foundPatientKit!=null) {
                if (kit.kitType=='Lab' && foundPatientKit.VTR2_Quantity__c!=kit.quantity) {
                    quantityNotEqualMaterialIds.add(kit.materialId);
                } else {
                    VTD1_Patient_Kit__c patientKitToUpdate = new VTD1_Patient_Kit__c(
                            Id = foundPatientKit.Id,
                            VTD2_MaterialId__c = kit.materialId
                    );
                    patientKitsToUpdate.add(patientKitToUpdate);
                }
            } else {
                kitNotFoundMaterialIds.add(kit.materialId);
            }
        }

        if (!kitNotFoundMaterialIds.isEmpty()) {
            String errorMessage = 'The following Patient Kits were not found: '+String.join(kitNotFoundMaterialIds, ', ')+'.\r\n';
            DTResponse.addError(errorMessage, 'PATIENT_KITS_NOT_FOUND', null);
            restResponse.statusCode = 400;
        }
        if (!quantityNotEqualMaterialIds.isEmpty()) {
            String errorMessage = 'For the following Lab Kits quantity is not the same as received: '+String.join(quantityNotEqualMaterialIds, ', ')+'.\r\n';
            DTResponse.addError(errorMessage, 'LAB_KITS_QUANTITY_NOT_EQUAL', null);
            restResponse.statusCode = 400;
        }

        System.debug('Patient Kits to update: ' + patientKitsToUpdate);
        update patientKitsToUpdate;
    }

    private static void log(Object request, Object response, String errorMessage) {
        VTD1_IntegrationInbound_Log__c log = VT_D1_HTTPConnectionHandler.createIntegrationInboundLogRecord(
                System.JSON.serializePretty(request),
                System.JSON.serializePretty(response),
                Datetime.now(),
                errorMessage
        );
        insert log;
    }

    // Errors
    private static Boolean requiredFieldsMissing(DeliveryTrackingRequest DTRequest) {
        List<String> requiredFieldsMissing = new List<String>();
        if (String.isEmpty(DTRequest.subjectId)) {
            requiredFieldsMissing.add('subjectId');
        }
        if (String.isEmpty(DTRequest.shipmentId)) {
            requiredFieldsMissing.add('shipmentId');
        }
        if (String.isEmpty(DTRequest.shipmentName)) {
            requiredFieldsMissing.add('shipmentName');
        }
        if (DTRequest.kits==null || DTRequest.kits.isEmpty()) {
            requiredFieldsMissing.add('kits');
        }
        if (!requiredFieldsMissing.isEmpty()) {
            String errorMessage = 'Required fields are missing: '+String.join(requiredFieldsMissing, ', ')+'.\r\n';
            DTResponse.addError(errorMessage, 'REQUIRED_FIELD_MISSING', requiredFieldsMissing);
            return true;
        }
        return false;
    }

	private static void incorrectPayloadResponse(RestResponse restResponse) {
        restResponse.statusCode = 422;
	}
	
//	private static void incorrectDataResponse(RestResponse restResponse) {
//        restResponse.addHeader('Content-Type', 'application/json');
//        restResponse.responseBody = Blob.valueOf(System.JSON.serializePretty(restResponse));
//        restResponse.statusCode = 400;
//	}
}