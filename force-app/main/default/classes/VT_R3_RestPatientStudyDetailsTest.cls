@isTest
public with sharing class VT_R3_RestPatientStudyDetailsTest {
    
    public static void getStudyDetailsTest() {
        String responseString;
        Map<String, Object>responseMap = new Map<String, Object>();
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/Patient/StudyDetails/';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = response;

        responseString = VT_R3_RestPatientStudyDetails.getStudyDetails();

        Knowledge__kav knowledge = new Knowledge__kav(Title = 'knowledge Title',
                UrlName = 'UrlName',
                VTD2_Type__c = 'Study_Details',
                VTD2_Study__c = null);
        insert knowledge;
        knowledge = [SELECT KnowledgeArticleId FROM Knowledge__kav WHERE Id = :knowledge.Id];
        Contact contact = new Contact(FirstName = 'fn', LastName = 'ln');

        List<User> listUsers = [SELECT Id, ContactId FROM User WHERE UserPermissionsKnowledgeUser = true AND IsActive = true AND Profile.Name = 'System Administrator'];
        if (!listUsers.isEmpty()) {
            User user = listUsers.get(0);
            user.ContactId = contact.Id;
            update user;
            System.runAs(user) {
                KbManagement.PublishingService.publishArticle(knowledge.KnowledgeArticleId, true);
            }
            responseString = VT_R3_RestPatientStudyDetails.getStudyDetails();
        }
    }
}