/**
 * @author: N.Arbatskiy
 * @date: 13-May-20
 */
@IsTest
private class VT_R5_eCoaUserUpdateHelperTest {
    private static final String SUBJECT_GUID = 'subject_9ecb75cf-0000-0000-0000-7d11bd9bd41d';
    public with sharing class CalloutMock implements HttpCalloutMock {
        private Integer statusCode;
        public CalloutMock(Integer statusCode) {
            this.statusCode = statusCode;
        }
        public HttpResponse respond(HttpRequest request) {
            if (request.getEndpoint().endsWith('/subjects')) {
                HttpResponse res = new HttpResponse();
                res.setBody('{"username":"Ecoa User","phone":"+1666000444",' +
                        '"studyGuid":"study_79826f74-6967-4eb0-bc99-f50629e66f0c"}');
                res.setStatusCode(this.statusCode);
                return res;
            } else if (request.getEndpoint().endsWith('/blind')) {
                HttpResponse res = new HttpResponse();
                res.setBody('{"email":"testecoablind@test.com"}');
                res.setStatusCode(this.statusCode);
                return res;
            } else {
                HttpResponse res = new HttpResponse();
                res.setBody('{"timezone":"Russia/Moscow", "subjectLocale":"ruRU"}');
                res.setStatusCode(this.statusCode);
                return res;
            }
        }
    }
    @TestSetup
    private static void setup() {
        insert new eCOA_IntegrationDetails__c(
                eCOA_OrgGuid__c = 'org_altavoz',
                Cognito_UserPoolId__c = 'us-east-1_RpJI95p9g',
                Cognito_ClientId__c = '7s29ancc3i683c6fcme3rm2m0f');

        Contact contact = new Contact(
                FirstName = 'Solid',
                LastName = 'Snake'
        );
        insert contact;
        User ecoaUser = new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'Patient'].Id,
                FirstName = 'eCOA',
                LastName = 'User',
                Email = VT_D1_TestUtils.generateUniqueUserName(),
                Username = VT_D1_TestUtils.generateUniqueUserName(),
                CompanyName = 'eCoa Test CN',
                Title = 'title',
                Alias = 'alias',
                ContactId = contact.Id,
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                VTR5_eCOA_Guid__c = SUBJECT_GUID
        );
        insert ecoaUser;
    }

    @Future
    private static void processEcoaUpdateFuture(String user) {
        User u = (User) JSON.deserialize(user, User.class);
        User clonedUser = u.clone(true, true, true, true);
        u.VTR5_eCOA_Guid__c = null;
        u.VTR5_Create_Subject_in_eCOA__c = true;
        VT_R5_eCoaUserUpdateHelper.processEcoaUpdate(
                new List<User>{
                        u
                },
                new Map<Id, User>{
                        u.Id => clonedUser
                }
        );
    }

    @IsTest
    static void test_VT_R5_eCoaQueueAction_create() {
        test_VT_R5_eCoaUserUpdateHelper(null);

    }

    @IsTest
    static void test_VT_R5_eCoaQueueAction_update() {
        test_VT_R5_eCoaUserUpdateHelper(SUBJECT_GUID);

    }

    @IsTest
    static void testEcoaUpdateSubject() {
        testAbstractAction('VT_R5_eCoaUpdateSubject', SUBJECT_GUID);


    }

    @IsTest
    static void testEcoaUpdateBlind() {
        testAbstractAction('VT_R5_eCoaUpdateBlind', SUBJECT_GUID);
    }

    @IsTest
    static void testEcoaCreateSubject() {
        testAbstractAction('VT_R5_eCoaCreateSubject', SUBJECT_GUID);
        new VT_R5_eCoaCreateSubject(getUserIdByGuid(null)).execute();
    }

    @IsTest
    static void test_VT_R5_eCoaAddEventDate() {
        testAbstractAction('VT_R5_eCoaAddEventDate', SUBJECT_GUID);
    }

    @IsTest
    static void testEcoaActionStatus200() {
        testEcoaActionStatus(200, SUBJECT_GUID);
    }

    @IsTest
    static void testEcoaActionStatus500() {
        testEcoaActionStatus(500, SUBJECT_GUID);
    }

    @IsTest
    static void testEcoaActionStatusOther() {
        testEcoaActionStatus(800, SUBJECT_GUID);
    }

    @IsTest
    static void test_VT_R5_eCoaSubjectSchedule() {
        List<Id> userIds = new List<Id>{
                getUserIdByGuid(SUBJECT_GUID)
        };
        VT_R5_eCoaSubjectSchedule subjectSchedule = new VT_R5_eCoaSubjectSchedule(userIds, false);
        Test.startTest();
        subjectSchedule.setIsStopEvent(false);
        subjectSchedule.setDelay(5);
        subjectSchedule.getType();
        subjectSchedule.doCallout();
        System.assertNotEquals(null, subjectSchedule);
        System.assertNotEquals(null, new VT_R5_eCoaSubjectSchedule(userIds));
        Test.stopTest();
    }

    private static void test_VT_R5_eCoaUserUpdateHelper(String ecoaSubjectGuid) {
        User u = [
                SELECT Id,Name,Email,LanguageLocaleKey,
                        TimeZoneSidKey,VTR5_Create_Subject_in_eCOA__c,VTR5_eCOA_Guid__c
                FROM User
                WHERE VTR5_eCOA_Guid__c = :ecoaSubjectGuid
                LIMIT 1
        ];
        Test.startTest();
        {
            User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
            System.runAs (thisUser) {
                processEcoaUpdateFuture(JSON.serialize(u));
            }
            System.runAs (thisUser) {
                User clonedUser = u.clone(true, true, true, true);
                clonedUser.Email = null;
                clonedUser.LanguageLocaleKey = null;
                VT_R5_eCoaUserUpdateHelper.processEcoaUpdate(
                        new List<User>{
                                u
                        },
                        new Map<Id, User>{
                                u.Id => clonedUser
                        }
                );
            }
        }
        Test.stopTest();
    }

    private static void testEcoaActionStatus(Integer statusCode, String ecoaSubjectGuid) {
        VT_R5_eCoaUpdateSubject updateSubject = new VT_R5_eCoaUpdateSubject(getUserIdByGuid(ecoaSubjectGuid));
        Test.setMock(HttpCalloutMock.class, new CalloutMock(statusCode));
        Test.startTest();
        updateSubject.execute();
        updateSubject.setSkipLogging(true);
        Test.stopTest();
    }

    private static Id getUserIdByGuid(String ecoaSubjectGuid) {
        User u = [
                SELECT Id, VTR5_Create_Subject_in_eCOA__c, VTR5_eCOA_Guid__c
                FROM User
                WHERE VTR5_eCOA_Guid__c = :ecoaSubjectGuid
                LIMIT 1
        ];
        return u.Id;
    }

    private static void testAbstractAction(String actionType, String ecoaSubjectGuid) {
        Test.setMock(HttpCalloutMock.class, new CalloutMock(200));
        Test.startTest();
        VT_R5_eCoaAbstractAction action;
        if (actionType == 'VT_R5_eCoaUpdateBlind') {
            action = new VT_R5_eCoaUpdateBlind(getUserIdByGuid(ecoaSubjectGuid));
            System.assertEquals(null, new VT_R5_eCoaUpdateBlind(null).doCallout());
        } else if (actionType == 'VT_R5_eCoaUpdateSubject') {
            action = new VT_R5_eCoaUpdateSubject(getUserIdByGuid(ecoaSubjectGuid));
            System.assertEquals(null, new VT_R5_eCoaUpdateSubject(null).doCallout());
        } else if (actionType == 'VT_R5_eCoaCreateSubject') {
            action = new VT_R5_eCoaCreateSubject(getUserIdByGuid(ecoaSubjectGuid));
            new VT_R5_eCoaCreateSubjectQueueable(new List<VT_R5_eCoaCreateSubject>{
                    (VT_R5_eCoaCreateSubject) action
            }).getType();
            System.assertEquals(null, new VT_R5_eCoaCreateSubject(null).doCallout());
        } else if (actionType == 'VT_R5_eCoaAddEventDate') {

            //action = new VT_R5_eCoaAddEventDate(getUserIdByGuid(SUBJECT_GUID), '');
            VT_R5_eCoaAddEventDate newaction = new VT_R5_eCoaAddEventDate(getUserIdByGuid(SUBJECT_GUID), '');
            newaction.phoenixTime = String.valueOf(System.now().getTime() / 1000);
            action = newaction;

            System.assertNotEquals(null, new VT_R5_eCoaAddEventDate(getUserIdByGuid(SUBJECT_GUID), '', true));
        }
        action.execute();
        action.getType();
        System.assertNotEquals(null, action);
        Test.stopTest();
    }

}