@IsTest
public with sharing class VT_D2_CaseEscalateControllerTest {

    public static void getCaseTest() {
        List<Case> casesList = [SELECT Id FROM Case];
        System.assert(casesList.size() > 0);
        if(casesList.size() > 0){
            Case cas = casesList.get(0);
            Test.startTest();
            String pcfId = VT_D1_CommunityChat.createPcf(cas.Id, 'subject', 'description', null, 'Possible');
            Test.stopTest();

            VT_D2_CaseEscalateController.updateCase(pcfId, true);
            Case pcf1 = [SELECT Id, IsEscalated, VTD1_IsEscalatedDateTime__c FROM Case WHERE Id = :pcfId];
            System.assertNotEquals(null, pcf1);
            System.assertEquals(true, pcf1.IsEscalated);
            System.assertNotEquals(null, pcf1.VTD1_IsEscalatedDateTime__c);

            Case pcf2 = VT_D2_CaseEscalateController.getCase(pcfId);
            System.assertNotEquals(null, pcf2);
            System.assertEquals(true, pcf2.IsEscalated);

            String profile = VT_D2_CaseEscalateController.getProfile(pcfId);
            System.assertNotEquals(null, profile);

            VT_D2_CaseEscalateController.updateCase(pcfId, false);
            Case pcf3 = [SELECT Id, IsEscalated, VTD1_IsEscalatedDateTime__c FROM Case WHERE Id = :pcfId];
            System.assertNotEquals(null, pcf3);
            System.assertEquals(false, pcf3.IsEscalated);
            System.assertEquals(null, pcf3.VTD1_IsEscalatedDateTime__c);
        }
    }

}