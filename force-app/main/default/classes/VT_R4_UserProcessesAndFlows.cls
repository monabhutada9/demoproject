/**
 * Created by Alexey Mezentsev on 6/29/2020.
 */

public with sharing class VT_R4_UserProcessesAndFlows extends Handler {
    private final static VTD1_RTId__c CONSTANTS = VTD1_RTId__c.getInstance();
    private static Set<Id> communityProfilesIds = new Set<Id>{
            CONSTANTS.VTD2_Profile_PI__c,
            CONSTANTS.VTD2_Profile_SCR__c,
            CONSTANTS.VTD2_Profile_Patient__c,
            CONSTANTS.VTD2_Profile_Caregiver__c
    };
    private static Map<String, String> deliveryType = new Map<String, String>{
            'Welcome' => 'Ship Welcome Kit',
            'Device' => 'Order Patient Device Kit',
            'IMP' => 'Ship Packaging Materials IMP Kit',
            'Lab' => 'Ship Packaging Materials Lab Kit'
    };

    protected override void onAfterInsert(Handler.TriggerContext context) {
        handleCreatePGCapacity(context.newList);
    }

    protected override void onAfterUpdate(Handler.TriggerContext context) {
        handleChangeTelevisitBufferOnUser(context.newList, context.oldMap);
        handleWelcomeKitSCTaskGeneration(context.newList, context.oldMap);
    }

    // PB Name: "Televisit Buffer onUser"
    // Flow Names: "Televisit Buffer on User Start", "Televisit Buffer on User Videosignal Reset"
    public static void handleChangeTelevisitBufferOnUser(List<User> newList, Map<Id, SObject> oldMap) {
        List<Id> userIdsToSendNotificationsList = new List<Id>();
        List<String> tnCodes = new List<String>();
        List<Id> videoConfIdsToSendSignalList = new List<Id>();
        List<Id> userIdsToResetSignalList = new List<Id>();
        for (User u : newList) {
            User oldUser = (User) oldMap.get(u.Id);
            if (u.Televisit_Buffer__c != null && u.Televisit_Buffer__c != oldUser.Televisit_Buffer__c) {
                if (communityProfilesIds.contains(u.ProfileId)) {
                    userIdsToSendNotificationsList.add(u.Id);
                    tnCodes.add('N578');
                } else {
                    videoConfIdsToSendSignalList.add(u.Televisit_Buffer__c);
                }
            } else if (u.Televisit_Buffer__c == null && oldUser.Televisit_Buffer__c != null) {
                userIdsToResetSignalList.add(u.Id);
            }
        }
        if (!userIdsToSendNotificationsList.isEmpty()) {
            VT_D2_TNCatalogNotifications.generateNotifications(tnCodes, userIdsToSendNotificationsList, new Map<String, String>());
        }
        if (!videoConfIdsToSendSignalList.isEmpty()) {
            // covered in VT_D2_SendVideoConfSignalTest
            VT_D1_SendVideoConfSignal.sendSignalToNotConnectedParticipants(videoConfIdsToSendSignalList);
        }
        if (!userIdsToResetSignalList.isEmpty()) {
            // covered in VT_D1_ResetVideoConfSignalTest
            VT_D1_ResetVideoConfSignal.resetSignalToNotConnectedParticipants(userIdsToResetSignalList);
        }
    }

    // PB Name: "SC_a Ship Welcome Kit SC task generation"
    // Flow Name: "Ship Welcome Kit SC Task"
    public static void handleWelcomeKitSCTaskGeneration(List<User> newList, Map<Id, SObject> oldMap) {
        Set<Id> userIdsToProceed = new Set<Id>();
        for (User u : newList) {
            if (u.VTD1_Form_Updated__c && (oldMap == null || !((User) oldMap.get(u.Id)).VTD1_Form_Updated__c)) {
                userIdsToProceed.add(u.Id);
            }
        }
        if (!userIdsToProceed.isEmpty()) {
            List<Task> patientTasksToUpdate = new List<Task>();
            List<VTD1_SC_Task__c> scTasksToInsert = new List<VTD1_SC_Task__c>();
            for (Task t : [
                    SELECT Id
                    FROM Task
                    WHERE OwnerId IN :userIdsToProceed
                    AND RecordTypeId = :CONSTANTS.VTD1_Task_SimpleTask__c
                    AND Status = 'Open'
                    AND Type = 'Confirm Kit Shipping Address'
            ]) {
                t.Status = 'Completed';
                patientTasksToUpdate.add(t);
            }
            update patientTasksToUpdate;
            for (Case cas : [
                    SELECT
                            VTD1_Patient_User__r.Name,
                            VTD1_Subject_ID__c,
                            VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c, (
                            SELECT
                                    VTD1_Patient_Kit__c,
                                    VTD1_Patient_Delivery__r.VTD1_isIMP__c,
                                    VTD1_Patient_Delivery__r.VTD1_isLab__c,
                                    VTD1_Shipment_Type_formula__c,
                                    VTD1_isPackageDelivery__c,
                                    VTD1_isWelcomeToStudyPackage__c,
                                    VTD1_containsDevice__c,
                                    VTD1_containsTablet__c,
                                    VTD1_Kit_Type__c,
                                    Name
                            FROM Deliveries__r
                            WHERE VTD1_Status__c = 'Pending Shipment'
                    )
                    FROM Case
                    WHERE VTD1_Patient_User__c IN :userIdsToProceed
                    AND RecordTypeId = :CONSTANTS.VTD1_Case_CarePlan__c
            ]) {
                for (VTD1_Order__c delivery : cas.Deliveries__r) {
                    String subject = 'Ship ' + delivery.Name + ' for ' + cas.VTD1_Patient_User__r.Name + ' (' + cas.VTD1_Subject_ID__c + ')';
                    Id deliveryId = delivery.Id;
                    // Is Welcome kit?
                    if (delivery.VTD1_isWelcomeToStudyPackage__c) {
                        scTasksToInsert.add(createSCTask(cas, deliveryId, deliveryType.get('Welcome'), subject));
                        // Is_Device_or_Tablet_kit
                    } else if (delivery.VTD1_containsDevice__c || delivery.VTD1_containsTablet__c) {
                        subject = subject.replaceFirst('Ship', 'Order');
                        scTasksToInsert.add(createSCTask(cas, deliveryId, deliveryType.get('Device'), subject));
                        // Is_Package_Delivery
                    } else if (delivery.VTD1_isPackageDelivery__c || delivery.VTD1_Shipment_Type_formula__c == 'Packaging Materials') {
                        // Ad-hoc
                        if (delivery.VTD1_Patient_Delivery__c == null) {
                            if (delivery.VTD1_Kit_Type__c == 'IMP') {
                                scTasksToInsert.add(createSCTask(cas, deliveryId, deliveryType.get('IMP'), subject));
                            } else if (delivery.VTD1_Kit_Type__c == 'Lab') {
                                scTasksToInsert.add(createSCTask(cas, deliveryId, deliveryType.get('Lab'), subject));
                            }
                            // Recurring
                        } else {
                            deliveryId = delivery.VTD1_Patient_Delivery__r.Id;
                            if (delivery.VTD1_Patient_Delivery__r.VTD1_isIMP__c) {
                                scTasksToInsert.add(createSCTask(cas, deliveryId, deliveryType.get('IMP'), subject));
                            } else if (delivery.VTD1_Patient_Delivery__r.VTD1_isLab__c) {
                                scTasksToInsert.add(createSCTask(cas, deliveryId, deliveryType.get('Lab'), subject));
                            }
                        }
                    }
                }
            }
            insert scTasksToInsert;
        }
    }

    private static VTD1_SC_Task__c createSCTask(Case cas, Id deliveryId, String type, String subject) {
        VTD1_SC_Task__c scTask = new VTD1_SC_Task__c(
                Name = type,
                OwnerId = cas.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c,
                RecordTypeId = CONSTANTS.VTD1_SC_Task_Delivery__c,
                VTD1_CarePlanTemplate__c = cas.VTD1_Study__r.Id,
                VTD1_PatientDelivery__c = deliveryId,
                VTD1_Patient__c = cas.Id,
                VTD1_Type__c = type,
                VTD1_Subject__c = subject,
                VTD2_Category__c = 'Kit Related'
        );
        return scTask;
    }

    // PB Name: "Create PG capacity"
    public static void handleCreatePGCapacity(List<User> newList) {
        Set<Id> pgUserIds = new Set<Id>();
        for (User u : newList) {
            if (u.ProfileId == CONSTANTS.VTD2_Profile_PG__c) {
                pgUserIds.add(u.Id);
            }
        }
        if (!pgUserIds.isEmpty()) {
            if (!System.isFuture() && !System.isBatch()) {
                createPGCapacityFuture(pgUserIds);
            } else if (!Test.isRunningTest()) {
                Datetime dt = Datetime.now() + (0.00162); // 2 mins from now
                String timeForScheduler = dt.format('s m H d M \'?\' yyyy');
                System.schedule(
                        'PGCapacity ' + timeForScheduler + ' ' + dt.millisecond() + ' ' + getRandomInteger(),
                        timeForScheduler,
                        new VT_R4_PGCapacityScheduler(pgUserIds)
                );
            }
        }
    }

    @Future
    private static void createPGCapacityFuture(Set<Id> pgUserIds) {
        List<VTD1_Patient_Guide_Capacity__c> pgCapacityToInsert = new List<VTD1_Patient_Guide_Capacity__c>();
        for (Id userId : pgUserIds) {
            pgCapacityToInsert.add(new VTD1_Patient_Guide_Capacity__c(User__c = userId, VTD1_Overall_Capacity__c = 15));
        }
        insert pgCapacityToInsert;
    }

    @TestVisible

    private static String getRandomInteger() {
        return String.valueOf(Math.random()).substring(2,10);
    }
}