public without sharing class VT_D1_VirtualSiteHandler {

    
    public void onBeforeDelete(List<Virtual_Site__c> recs) {
        VT_D1_LegalHoldDeleteValidator.validateDeletion(recs);
    }

    public void onBeforeInsert(List<Virtual_Site__c> recs) {
        new VT_R2_CalculateBackupPI().calculateBackupPISTM(recs);
        
    }

    public void onBeforeUpdate(List<Virtual_Site__c> recs, Map<Id, Virtual_Site__c> oldMap) {
		new VT_R2_CalculateBackupPI().calculateBackupPISTM(recs, oldMap);
        new VT_R2_SubITakesOverPIHandler().handleChangeComplete(recs, oldMap);
        for (Virtual_Site__c vs : recs) {
            if (vs.VTR3_Update_RB__c == true)
                VT_D1_DocumentProcessHelper.createMissingPlaceholdersBySiteLevel(recs);
        }
        VT_R5_SiteParticipantSharingHandler.handleBeforeUpdate(recs, oldMap);
        
    }

    public void onAfterInsert(List<Virtual_Site__c> recs) {
        VT_D1_DocumentProcessHelper.createPlaceholdersBySiteLevel(recs);
        VT_R5_ShareGroupCreator.createGroups(recs);
        VT_R5_ShareGroupMembershipService.linkSiteGroupsToStudy(recs);
        VT_R5_SiteParticipantSharingHandler.handleAfterInsert(recs);
        // Jira Ref: SH-17543 Akanksha Singh
        // Create Queue while inserting Virtual Site
        VT_R5_QueueBasedOnVirtualSite.createQueueBasedOnVirtualSite(recs);
        // Jira Ref: SH-17543 Akanksha Singh
    }

    public void onAfterUpdate(List<Virtual_Site__c> recs, Map<Id, Virtual_Site__c> oldMap, Map <Id, Virtual_Site__c> newMap) {
            // Jira Ref: SH-17543 Akanksha 
            // If 'isAfterUpdate' is true, then only perform afterUpdate operation, otherwise not required.
            if(VT_R5_QueueBasedOnVirtualSite.isAfterUpdate)
            {
            // Jira Ref: SH-17543 Akanksha Singh
            new VT_R2_VirtualSiteMilestoneHandler().createMilestones(recs, oldMap);
            VT_R5_SiteParticipantSharingHandler.handleAfterUpdate(recs, oldMap);
            new VT_R2_SubITakesOverPIHandler().reassignPatientsToNewPI(recs, oldMap);
            updateSiteAddress(recs, oldMap, newMap);
            checkDefaultExternalDiaryLanguage(newMap);
            }
            
    }

    private void checkDefaultExternalDiaryLanguage(Map<Id, Virtual_Site__c> newMap) {
        System.debug('checkDefaultExternalDiaryLanguage ' + newMap);
        List<String>nullList = new List<String>();
        List<Virtual_Site__c> sites = [
            SELECT Id,Name, VTR5_Default_External_Diary_Language__c, VTR5_External_Diary_Languages__c
            FROM Virtual_Site__c
            WHERE Id IN :newMap.keySet()
        ];
        for (Virtual_Site__c site : sites) {
            if (site.VTR5_Default_External_Diary_Language__c != null) {
                Set<String> selectedValues = new Set<String>
                    (site.VTR5_External_Diary_Languages__c != null ? site.VTR5_External_Diary_Languages__c.split(';') : nullList);
                if (!selectedValues.contains(site.VTR5_Default_External_Diary_Language__c)) {
                    Virtual_Site__c actualRecord = newMap.get(site.Id);
                    actualRecord.VTR5_Default_External_Diary_Language__c.addError
                        ('This value must be among selected in External Diary Languages');
                }
            }
        }
    }

    private void updateSiteAddress(List<Virtual_Site__c> recs, Map<Id, Virtual_Site__c> oldMap, Map<Id, Virtual_Site__c> newMap) {
        System.debug('ARE WE IB>>>');
        Set<id> vsIds = new Set<Id>();
        for (Virtual_Site__c v : recs) {
            if (oldMap.get(v.id).VTR2_PrimarySiteAddress__c != newMap.get(v.id).VTR2_PrimarySiteAddress__c) {
                vsIds.add(v.Id);
            }
        }
        if (!vsIds.isEmpty()) {

            List<VTD1_Order__c> patientDeliveries = [
                SELECT Id, VTR2_Destination_Type__c, VTR2_Site_Shipping_Address__c,
                    VTD1_Case__r.VTD1_Virtual_Site__r.Id
                FROM VTD1_Order__c
                WHERE ((VTD1_Case__r.VTD1_Virtual_Site__c in:vsIds) AND
                (VTD1_Status__c = 'Not Started' OR VTD1_Status__c = 'Pending Shipment')
                AND VTR2_Destination_Type__c = 'Site')
            ];

            System.debug('LIST>??????    :::  ' + patientDeliveries);

            for (VTD1_Order__c order : patientDeliveries) {
                order.VTR2_Site_Shipping_Address__c = newMap.get(order.VTD1_Case__r.VTD1_Virtual_Site__c).VTR2_PrimarySiteAddress__c;
            }
            System.debug('NEW LIST::::: ' + patientDeliveries);
            update patientDeliveries;
        }
    }
}