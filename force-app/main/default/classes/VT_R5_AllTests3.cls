/**
 * Created by Igor Shamenok on 04-Oct-20.
 */
@IsTest
private with sharing class VT_R5_AllTests3 {
    @TestSetup
    static void setup() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();

        VT_D1_TestUtils.prepareStudy(1);

        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }
    @IsTest
    static void VT_D2_CreatePCFFromCaseControllerTest() {
        VT_D2_CreatePCFFromCaseControllerTest.doTest();
    }
    @IsTest
    static void VT_D1_SkillsBasedRoutingTest() {
        VT_D1_SkillsBasedRoutingTest.testBehavior();
    }
    @IsTest
    static void VT_D1_PIStudyDocumentsControllerTest1() {
        VT_D1_PIStudyDocumentsControllerTest.getPatientDocumentsTest();
    }
    @IsTest
    static void VT_D1_PIStudyDocumentsControllerTest2() {
        VT_D1_PIStudyDocumentsControllerTest.testIsSandbox();
    }
    @IsTest
    static void VT_D2_CaseEscalateControllerTest() {
        VT_D2_CaseEscalateControllerTest.getCaseTest();
    }
    @IsTest
    static void VT_D2_EmailDriverTriggerHandlerTest1() {
        VT_D2_EmailDriverTriggerHandlerTest.doFirstTest();
    }
    @IsTest
    static void VT_D2_EmailDriverTriggerHandlerTest2() {
        VT_D2_EmailDriverTriggerHandlerTest.doSecondTest();
    }
    @IsTest
    static void VT_R4_PatientDeviceTriggerHandlerTest() {
        VT_R4_PatientDeviceTriggerHandlerTest.createPatientDeviceNotificationForRetakeReqTest();
    }
    @IsTest
    static void VT_R3_StudyShareConversionBatchTest() {
        VT_R3_StudyShareConversionBatchTest.doTest();
    }
    @IsTest
    static void VT_R3_RestPatientChatHistoryTest() {
        VT_R3_RestPatientChatHistoryTest.getChatHistoriesTest();
    }
    @IsTest
    static void VT_R5_RunConsentVisitTest() {
        VT_R5_RunConsentVisitTest.sendSignalTest();
    }
    @IsTest
    static void VT_R5_UpdatePreferLangOnContactBatchTest() {
        VT_R5_UpdatePreferLangOnContactBatchTest.testLanguage();
    }
    @IsTest
    static void VT_R2_TreatmentArmSharingControllerTest1() {
        VT_R2_TreatmentArmSharingControllerTest.testGetConfigs();
    }
    @IsTest
    static void VT_R2_TreatmentArmSharingControllerTest2() {
        VT_R2_TreatmentArmSharingControllerTest.testCheckNotSysAdminProfile();
    }
    @IsTest
    static void VT_R2_TreatmentArmSharingControllerTest3() {
        VT_R2_TreatmentArmSharingControllerTest.testGetArmAccessByStudy();
    }
    @IsTest
    static void VT_R2_TreatmentArmSharingControllerTest4() {
        VT_R2_TreatmentArmSharingControllerTest.testGetArmAccessByCase();
    }
    @IsTest
    static void VT_R2_TreatmentArmSharingControllerTest5() {
        VT_R2_TreatmentArmSharingControllerTest.testSetConfigFailed();
    }
    @IsTest
    static void VT_R2_TreatmentArmSharingControllerTest6() {
        VT_R2_TreatmentArmSharingControllerTest.testSetConfigSuccess();
    }
    @IsTest
    static void VT_R3_PublishedAction_ResetCommPassTest() {
        VT_R3_PublishedAction_ResetCommPassTest.doTest();
    }
    @IsTest
    static void VT_D2_BatchLockoutUsersProcessorTest() {
        VT_D2_BatchLockoutUsersProcessorTest.lockoutUserTest();
    }
    @IsTest
    static void VT_R2_RequestBuilder_AdHocReplacemTest() {
        VT_R2_RequestBuilder_AdHocReplacemTest.buildRequestBodyTest();
    }
    @IsTest
    static void VT_R3_RestVisitTimeslotsTest() {
        VT_R3_RestVisitTimeslotsTest.getTimeSlotsTest();
    }
    @IsTest
    static void VT_R4_BroadcastCreationBatchTest() {
        VT_R4_BroadcastCreationBatchTest.createMembersInBatch();
    }
    @IsTest
    static void VT_R3_RestPatientPhoneSMSTest1() {
        VT_R3_RestPatientPhoneSMSTest.updatePhoneOptTest();
    }
    @IsTest
    static void VT_R3_RestPatientPhoneSMSTest2() {
        VT_R3_RestPatientPhoneSMSTest.updatePhoneOptTest2();
    }
    @IsTest
    static void VT_R3_RestPatientStudyDetailsTest() {
        VT_R3_RestPatientStudyDetailsTest.getStudyDetailsTest();
    }
    @IsTest
    static void VT_R3_RestPatientMessagesTest() {
        VT_R3_RestPatientMessagesTest.getPostsTest();
    }
    @IsTest
    static void VT_R2_PopulatePatientLanguageBatchTest() {
        VT_R2_PopulatePatientLanguageBatchTest.testUpdate();
    }
    @IsTest
    static void VT_R2_SCRTaskListControllerTest() {
        VT_R2_SCRTaskListControllerTest.test();
    }
    @IsTest
    static void VT_R2_Test_SCRStudySuppliesController() {
        VT_R2_Test_SCRStudySuppliesController.test();
    }
    @IsTest
    static void VT_R3_PublishedAction_CreateCGTest() {
        VT_R3_PublishedAction_CreateCGTest.doTest();
    }
    @IsTest
    static void VT_R2_ReplacementItemTriggerHandlerTest() {
        VT_R2_ReplacementItemTriggerHandlerTest.doTest();
    }
    @IsTest
    static void VT_R2_GlobalAdhocSharingTest() {
        VT_R2_GlobalAdhocSharingTest.doTest();
    }
    @IsTest
    static void VT_D1_TreatmentArmSharingControllerTest() {
        VT_D1_TreatmentArmSharingControllerTest.getRowsTest();
    }
}