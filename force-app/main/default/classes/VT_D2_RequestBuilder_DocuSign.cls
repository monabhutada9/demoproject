/**
* @author: Carl Judge
* @date: 30-Oct-18
* @description: 
**/

public class VT_D2_RequestBuilder_DocuSign implements VT_D1_RequestBuilder {

    public class Envelope {
        public String accountId;
        public String emailSubject;
        public String emailBlurb;
        public String status;
        public DSDocument[] documents;
        public Recipients recipients;
        public AccountCustomFields customFields;

        // Add a SFDC record ID to the envelope - the document will be attached to this record after signing
        public void addRecId(Id recId) {
            TextCustomField customField = new TextCustomField();
            customField.name = 'DSFSSourceObjectId';
            customField.value = recId + '~' + recId.getSobjectType().getDescribe().getName();
            customField.show = 'false';

            AccountCustomFields customFields = new AccountCustomFields();
            customFields.textCustomFields = new List<TextCustomField> {
                customField
            };

            this.customFields = customFields;
        }
    }

    public class DSDocument {
        public String documentId;
        public String name;
        public String documentBase64;
        public String remoteUrl;
        public String fileExtension;
    }

    public class Recipients {
        public Signer[] signers;
    }

    public class Signer {
        public String name;
        public String email;
        public String recipientId;
        public String routingOrder;
        public Tabs tabs;
    }

    public class Tabs {
    }

    public class Tab {
        public String xPosition;
        public String yPosition;
        public String documentId;
        public String pageNumber;
    }

    public class AccountCustomFields {
        public TextCustomField[] textCustomFields;
    }

    public class TextCustomField {
        public String name;
        public String value;
        public String show;
    }

    private Id docId;
    private VTD1_Document__c doc;

    public VT_D2_RequestBuilder_DocuSign(Id docId) {
        this.docId = docId;
        this.doc = [SELECT Id, Name,
                    RecordTypeId,
                    VTD1_Clinical_Study_Membership__r.VTD1_PI_user__c,
                    VTD2_Associated_Medical_Record_id__r.VTD1_FileNames__c,
                    VTD2_Associated_Medical_Record_id__r.VTD1_Nickname__c,
                    VTD1_Eligibility_Assessment_Status__c,
                    VTD1_Site__r.VTD1_Study_Team_Member__r.User__c,
                    VTD1_Regulatory_Binder__r.VTD1_Regulatory_Document__r.VTD1_Document_Type__c,
                    VTD1_Site__r.VTR2_Backup_PI_STM__r.User__c,
                    VTD1_Protocol_Amendment__c,
                    VTD1_Regulatory_Document_Type__c
                    FROM VTD1_Document__c
                    WHERE Id = :docId];
    }

    public String buildRequestBody() {
        ContentVersion content = getContentVersion(this.docId);
        User recipient = getRecipient(this.docId, this.doc);
        Envelope env = getEnvelope(content, recipient, this.docId, this.doc);

        return JSON.serialize(env, true);
    }

    private static ContentVersion getContentVersion(Id docId) {
        Id contentDocId = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :docId].ContentDocumentId;

        return [
            SELECT Id, VersionData, PathOnClient, FileExtension, Title
            FROM ContentVersion
            WHERE ContentDocumentId = :contentDocId
            ORDER BY VersionNumber desc
        ][0];
    }

    private static User getRecipient(Id docId, VTD1_Document__c document) {
        if (document.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE
                && document.VTD1_Eligibility_Assessment_Status__c == null
                && document.VTD1_Clinical_Study_Membership__r.VTD1_PI_user__c != null)
        {
            User piInfo = [SELECT Name, Email
                            FROM User
                            WHERE Id =:document.VTD1_Clinical_Study_Membership__r.VTD1_PI_user__c];
            return piInfo;
        }
        else if(document.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_NOTE_OF_TRANSFER
                || VT_D1_DocumentCHandler.isPASignaturePage(document)) {
            return [SELECT Name, Email FROM User WHERE Id =:document.VTD1_Site__r.VTD1_Study_Team_Member__r.User__c];
        }
        else if(document.VTD1_Regulatory_Binder__r.VTD1_Regulatory_Document__r.VTD1_Document_Type__c ==
                VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_NOTE_TO_FILE_STEP_DOWN) {
            return [SELECT Name, Email FROM User WHERE Id =:document.VTD1_Site__r.VTR2_Backup_PI_STM__r.User__c];
        }
        else {
            return [SELECT Name, Email FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        }
    }

    private static Envelope getEnvelope(ContentVersion content, User recipient, Id docId, VTD1_Document__c document) {
        DSDocument dsDoc = new DSDocument();
        dsDoc.documentId = '1';

        // Maksim Fedarenka, SH-12553 EAF. Extended symbol appeared after signing by PI in the name of the file.
        // Replaced dsDoc.name = content.Title instead of dsDoc.name = content.PathOnClient
        dsDoc.name = content.Title;
        dsDoc.documentBase64 = EncodingUtil.base64Encode(content.VersionData);
        dsDoc.fileExtension = content.FileExtension;

        Signer signer = new Signer();
        signer.name = recipient.Name;
        signer.email = recipient.Email;
        if (document.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE
                && document.VTD1_Eligibility_Assessment_Status__c != null) {
            signer.recipientId = '2';
        } else {
            signer.recipientId = '1';
        }
        signer.routingOrder = '1';
        signer.tabs = new Tabs();


        Envelope env = new Envelope();
        env.addRecId(docId);
  //      env.accountId = DocuSignAPISettings__c.getInstance().AccountNumber__c;
        if (document.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE) {
            env.emailSubject = Label.VTD2_PleaseSignEligibilityReviewFor + ' ' + document.Name;
        } else if(document.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_NOTE_OF_TRANSFER) {
            env.emailSubject = 'Note of transfer subject';
            env.emailBlurb = 'Note of transfer body';
        } else if(VT_D1_DocumentCHandler.isPASignaturePage(document)) {
            env.emailSubject = Label.VTD2_PleaseSign + ' ' + document.Name;
        }
        else {
            env.emailSubject = Label.VTD2_PleaseSign + ' ' + document.VTD2_Associated_Medical_Record_id__r.VTD1_FileNames__c;
            env.emailBlurb = Label.VTD2_StudyHubSent
                            + '\n \n ' + document.getSobjectType().getDescribe()
                                        .fields.getMap().get('VTD1_FileNames__c').getDescribe().getLabel()  + ': '
                    + (document.VTD2_Associated_Medical_Record_id__r.VTD1_FileNames__c != null
                        ? document.VTD2_Associated_Medical_Record_id__r.VTD1_FileNames__c : '')
                            + '\n ' + Label.VTD1_Nickname + ': '
                    + (document.VTD2_Associated_Medical_Record_id__r.VTD1_Nickname__c != null
                    ? document.VTD2_Associated_Medical_Record_id__r.VTD1_Nickname__c : '');
        }

        env.status = 'sent';
        env.documents = new DSDocument[]{dsDoc};
        env.recipients = new Recipients();
        env.recipients.signers = new Signer[]{signer};

        return env;
    }

   /* private static Tab getTab(String x, String y) {
        Tab tab = new Tab();
        tab.xPosition = x;
        tab.yPosition = y;
        tab.documentId = '1';
        tab.pageNumber = '1';
        return tab;
    }*/
}