/**
 * Created by Yevhen Kharchuk on 16-Mar-20.
 */

public without sharing class VT_R4_PatientTransferCloneFiles implements Database.Batchable<SObject>, Database.Stateful {
    private Map<Id, VTD1_Document__c> docIdToClonedDoc;
    private Id newCaseId;
    private Id networkId;

    public VT_R4_PatientTransferCloneFiles(Map<Id, VTD1_Document__c> docIdToClonedDoc, Id newCaseId) { 
        this.docIdToClonedDoc = docIdToClonedDoc;
        this.newCaseId = newCaseId;
        this.networkId = this.getNetworkId();
    }

    public Database.QueryLocator start(Database.BatchableContext context) {
        updateClonedDocCertifications(this.docIdToClonedDoc);
        return Database.getQueryLocator([
                SELECT Id
                        , (SELECT Id, ContentDocument.LatestPublishedVersionId FROM ContentDocumentLinks)
                FROM VTD1_Document__c
                WHERE Id IN :THIS.docIdToClonedDoc.keySet()
        ]);
    }

    public void execute(Database.BatchableContext context, List<SObject> documents) {
        List<VTD1_Document__Share> forInsert = new List<VTD1_Document__Share>();
        List<User> caregivers = [
                SELECT Id
                FROM User
                WHERE Profile.Name = :VT_D1_ConstantsHelper.CAREGIVER_PROFILE_NAME
                AND Contact.VTD1_Clinical_Study_Membership__c = :THIS.newCaseId
        ];

        for (VTD1_Document__c document : (List<VTD1_Document__c>) documents) {
            for (User caregiver : caregivers) {
                VTD1_Document__Share docShare = new VTD1_Document__Share();
                docShare.ParentId = docIdToClonedDoc.get(document.Id).Id;
                docShare.UserOrGroupId = caregiver.Id;
                docShare.RowCause = 'GlobalSharing__c';
                docShare.AccessLevel = 'Edit';
                forInsert.add(docShare);
            }

            if (document.ContentDocumentLinks.isEmpty()) {
                continue;
            }

            Id lastCVId = document.ContentDocumentLinks[0].ContentDocument.LatestPublishedVersionId;

            ContentVersion cv = [
                    SELECT Title, PathOnClient, ContentUrl, VersionData, ContentBodyId
                    FROM ContentVersion
                    WHERE Id = :lastCVId
            ];

            ContentVersion cv1 = new ContentVersion(
                    Title = cv.Title,
                    PathOnClient = '/' + cv.PathOnClient,
                    VersionData = cv.VersionData,
                    NetworkId = this.networkId,
                    IsMajorVersion = true
            );
            insert cv1;

            ContentDocumentLink cdl = new ContentDocumentLink(
                    LinkedEntityId = docIdToClonedDoc.get(document.Id).Id,
                    ContentDocumentId = [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :cv1.Id].Id,
                    ShareType = 'I',
                    Visibility = 'AllUsers'
            );
            insert cdl;
        }

        insert forInsert;
    }

    public void finish(Database.BatchableContext param1) {
    }

    private Id getNetworkId() {
        Profile currentProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
        if (currentProfile.Name == VT_D1_ConstantsHelper.SCR_PROFILE_NAME) {
            List<Network> scrCommunities = [SELECT Id FROM Network WHERE Name LIKE '%SCR%'];
            if (!scrCommunities.isEmpty()) {
                return scrCommunities[0].Id;
            } else {
                List<Network> communities = [SELECT Id FROM Network];
                return communities[0].Id;
            }
        } else {
            return null;
        }
    }

    private void updateClonedDocCertifications(Map<Id, VTD1_Document__c> docIdToClonedDoc) {
        Set<VTD1_Document__c> forUpdate = new Set<VTD1_Document__c>();
        for (VTD1_Document__c clonedDoc : docIdToClonedDoc.values()) {
            if (clonedDoc.VTD2_Certification__c != null) {
                clonedDoc.VTD2_Certification__c = docIdToClonedDoc.get(clonedDoc.VTD2_Certification__c).Id;
                forUpdate.add(clonedDoc);

                if (clonedDoc.VTD2_Certification__c != null) {
                    forUpdate.add(new VTD1_Document__c(
                            Id = clonedDoc.VTD2_Certification__c,
                            VTD2_Associated_Medical_Record_id__c = clonedDoc.Id,
                            VTD2_Associated_Medical_Records__c = clonedDoc.Name)
                    );
                }
            }
        }
        update new List<VTD1_Document__c>(forUpdate);
    }
}