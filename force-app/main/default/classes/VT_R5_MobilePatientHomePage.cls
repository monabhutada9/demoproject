/**
 * Created by Yuliya Yakushenkova on 10/13/2020.
 */

public class VT_R5_MobilePatientHomePage extends VT_R5_MobileRestResponse implements VT_R5_MobileLibrary.Routable {


    public List<String> getMapping() {
        return new List<String>{
                '/mobile/{version}/homepage'
        };
    }

    public VT_R5_MobileRestResponse versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        return get_v1();

                    }
                }
            }
        }
        return null;
    }

    public void initService() {
    }

    private VT_R5_MobileRestResponse get_v1() {
        Case casePatient = VT_R5_PatientQueryService.getCaseByCurrentUser();
        if (casePatient == null) return null;
        VT_R5_RandomizationHomepage homepage = VT_R5_RandomizationHomepageService.getHomePage(casePatient);
        this.buildResponse(homepage);
        return this;
    }

    public Map<String, String> getMinimumVersions() {
        return null;
    }
}