/**
* @author: Carl Judge
* @date: 01-Nov-18
* @description: Test FeedItem and FeedComment triggers and related classes
**/

@IsTest
private class VT_D1_FeedItemFeedComments_Test {
    private final static String PCF_SUBJECT = 'Subject_VT_D1_FeedItemFeedComments_Test';

    @IsTest(SeeAllData=true)
    private static void testItemsAndComments() {
        User pg1User = (User) new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME)
                .persist();

        User pg2User = (User) new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME)
                .persist();

        Case pcf = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName(VT_R4_ConstantsHelper_AccountContactCase.RT_PCF)
                .setSubject(PCF_SUBJECT)
                .setVTD1_Primary_PG(pg1User.Id)
                .setVTD1_Secondary_PG(pg2User.Id)
                .persist();

        Test.startTest();
        Event evnt = (Event) new DomainObjects.Event_t()
                .setStartDatetime(System.now().addHours(-1))
                .setEndDatetime(System.now().addHours(1))
                .setOwnerId(pcf.VTD1_Primary_PG__c)
                .setRecordTypeId(Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('OutOfOffice').getRecordTypeId())
                .persist();

        FeedItem taggedFeedItemUser_t = (FeedItem) new DomainObjects.FeedItem_t()
                .setBodyFeedItem('Test {@' + UserInfo.getUserId() + '} tagged')
                .setParentId(UserInfo.getUserId())
                .persist();

        FeedItem untaggedFeedItemUser_t = (FeedItem) new DomainObjects.FeedItem_t()
                .setBodyFeedItem('Test {@' + UserInfo.getName() + '} tagged')
                .setParentId(UserInfo.getUserId())
                .persist();

        FeedItem taggedFeedItemCase_t = (FeedItem) new DomainObjects.FeedItem_t()
                .setBodyFeedItem('Test {@' + UserInfo.getUserId() + '} tagged')
                .setParentId(pcf.Id)
                .persist();

        FeedItem untaggedFeedItemCase = (FeedItem) new DomainObjects.FeedItem_t()
                .setBodyFeedItem('Test {@' + UserInfo.getName() + '} tagged')
                .setParentId(pcf.Id)
                .persist();

        FeedComment taggedFeedCommentUser = (FeedComment)  new DomainObjects.FeedComment_t()
                .setCommentBody('Test {@' + UserInfo.getUserId() + '} tagged')
                .setFeedItemId(taggedFeedItemUser_t.Id)
                .persist();

        FeedComment untaggedFeedCommentUser = (FeedComment)  new DomainObjects.FeedComment_t()
                .setCommentBody('Test {@' + UserInfo.getName() + '} tagged')
                .setFeedItemId(untaggedFeedItemUser_t.Id)
                .persist();

        FeedComment taggedFeedCommentCase = (FeedComment) new DomainObjects.FeedComment_t()
                .setCommentBody('Test {@' + UserInfo.getName() + '} tagged')
                .setFeedItemId(taggedFeedItemCase_t.Id)
                .persist();

        FeedComment untaggedFeedCommentCase = (FeedComment) new DomainObjects.FeedComment_t()
                .setCommentBody('Test {@' + UserInfo.getName() + '} tagged')
                .setFeedItemId(untaggedFeedItemCase.Id)
                .persist();

        List<FeedComment> comments = new List<FeedComment>{
                taggedFeedCommentUser, untaggedFeedCommentUser, taggedFeedCommentCase, untaggedFeedCommentCase
        };

        try {
            delete comments;
        } catch (Exception e) {}
        Test.stopTest();
    }

    @IsTest(SeeAllData=true)
    private static void testNotifications() {
        User patientUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME)
                .persist();

        Case pcf = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName(VT_R4_ConstantsHelper_AccountContactCase.RT_PCF)
                .setSubject(PCF_SUBJECT)
                .persist();

        Test.startTest();
        VTD1_PCF_Chat_Member__c pfcUserChatMember = (VTD1_PCF_Chat_Member__c) new DomainObjects.VTD1_PCF_Chat_Member_t()
                .setPCFId(pcf.Id)
                .setUserId(patientUser.Id)
                .persist();

        VTD1_Chat__c chat = (VTD1_Chat__c) new DomainObjects.VTD1_Chat_t().persist();

        VTD1_Chat_Member__c chatMember = (VTD1_Chat_Member__c) new DomainObjects.VTD1_Chat_Member_t()
                .setChat(chat.Id)
                .setUser(pfcUserChatMember.VTD1_User_Id__c)
                .persist();

        FeedItem caseFeedItem = (FeedItem) new DomainObjects.FeedItem_t()
                .setBodyFeedItem('Test')
                .setParentId(pfcUserChatMember.VTD1_PCF__c)
                .persist();

        FeedItem chatFeedItem = (FeedItem) new DomainObjects.FeedItem_t()
                .setBodyFeedItem('Test')
                .setParentId(chat.Id)
                .persist();

        FeedComment caseFeedComment = (FeedComment) new DomainObjects.FeedComment_t()
                .setCommentBody('Test')
                .setFeedItemId(caseFeedItem.Id)
                .persist();
        /*
        FeedComment chatFeedComment = (FeedComment) new DomainObjects.FeedComment_t()
                .setCommentBody('Test')
                .setFeedItemId(chatFeedItem.Id)
                .persist(); */
        Test.stopTest();
    }

    @IsTest(SeeAllData=true)
    public static void testHelper() {
        Test.startTest();
        ConnectApi.HashtagSegment hashTagSegment = new ConnectApi.HashtagSegment();
        hashTagSegment.tag = 'test';

        ConnectApi.LinkSegment linksegment = new ConnectApi.LinkSegment();
        linksegment.url = 'http://testme.com';

        ConnectApi.MarkupBeginSegment markupBeginSegment = new ConnectApi.MarkupBeginSegment();
        markupBeginSegment.markupType = ConnectApi.MarkupType.Code;

        ConnectApi.MarkupEndSegment markupEndSegment = new ConnectApi.MarkupEndSegment();
        markupEndSegment.markupType = ConnectApi.MarkupType.Code;

        VT_D1_MentionHelper.getLinkSeg((ConnectApi.MessageSegment) linksegment);
        VT_D1_MentionHelper.getHashtagSeg((ConnectApi.MessageSegment) hashTagSegment);
        VT_D1_MentionHelper.getMarkupBeginSeg((ConnectApi.MessageSegment) markupBeginSegment);
        VT_D1_MentionHelper.getMarkupEndSeg((ConnectApi.MessageSegment) markupEndSegment);
        Test.stopTest();
    }
}