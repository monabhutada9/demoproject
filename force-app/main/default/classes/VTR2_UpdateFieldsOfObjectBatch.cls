global class VTR2_UpdateFieldsOfObjectBatch implements Database.Batchable<sObject> {
	
    global final String query;

   	global VTR2_UpdateFieldsOfObjectBatch(String q){
        query=q;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        //'SELECT Id, Profile.Name, VTR2_WelcomeEmailSent__c FROM User WHERE isActive = true and UserType IN (\'PowerCustomerSuccess\', \'PowerPartner\')'
        return Database.getQueryLocator(query);
    }

   global void execute(Database.BatchableContext BC, List<User> scope) {
    
        List<User> userList = new List<User>();
        for(User user : scope){
            user.VTR2_WelcomeEmailSent__c = false;
            userList.add(user);
        }
        Database.update(userList, false);
    
   }

   global void finish(Database.BatchableContext BC) {
       
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       mail.setToAddresses(new String[] {UserInfo.getUserEmail()});
       mail.setReplyTo('batch@acme.com');
       mail.setSenderDisplayName('Batch Processing');
       mail.setSubject('Batch Process Completed');
       mail.setPlainTextBody('Batch Process has completed. WelcomeEmailSent field of User object was updated for necessary records');
       if(!Test.isRunningTest()) {
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       }
          
   }

}