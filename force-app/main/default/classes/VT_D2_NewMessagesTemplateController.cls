/*
 * Created by shume on 08/10/2018.
 */
public class VT_D2_NewMessagesTemplateController {
    public Id notificationId { get; set; }
    public VTD1_NotificationC__c notification;

    public VTD1_NotificationC__c getNot() {
        if (this.notification == null && this.notificationId!=null) {
            this.notification = [
                    SELECT OwnerId, Owner.Name, Owner.FirstName, Owner.LastName, Owner.Profile.Name, Number_of_unread_messages__c
                    FROM VTD1_NotificationC__c
                    WHERE Id = :this.notificationId
                    LIMIT 1
            ];
        }
        return this.notification;
    }

    public String getUnreadMessagesText() {
        if (getNot()==null) return '';
        if (getNot().Number_of_unread_messages__c == 1) {
            return Label.VTD2_OneUnreadMessage;
        } else {
            return String.format(Label.VTD2_NumberOfUnreadMessages, new List<String>{String.valueOf(getNot().Number_of_unread_messages__c)});
        }
    }

    public String getMessagesUrl() {
        if (getNot()==null) return '';
        if (getNot().Owner.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) {
            //return VTD2_NewMessagesEmailNotification__c.getInstance().VTD2_PiMessagesURL__c;
            return VTD1_RTId__c.getInstance().PI_URL__c + '/s/messages';
        } else {
            //return VTD2_NewMessagesEmailNotification__c.getInstance().VTD2_PTandCGMessagesURL__c;
            return VTD1_RTId__c.getInstance().VTD2_Patient_Community_URL__c + '/messages';
        }
    }

    public PatientInfo getPatientInfo() {
        PatientInfo patientInfo = new PatientInfo();
        if (getNot()!=null) {
            List<Contact> contacts = [
                    SELECT FirstName, VTR2_Primary_Language__c, VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTD1_Protocol_Nickname__c, VTD1_Clinical_Study_Membership__r.VTD2_Study_Phone_Number__c
                    FROM Contact
                    WHERE VTD1_UserId__c = :getNot().OwnerId
                    LIMIT 1
            ];
            if (!contacts.isEmpty()) {
                VT_D1_TranslateHelper.translate(contacts, contacts[0].VTR2_Primary_Language__c);
                patientInfo.firstName = contacts[0].FirstName;
                patientInfo.studyNickname = contacts[0].VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTD1_Protocol_Nickname__c;
                patientInfo.studyPhoneNumber = contacts[0].VTD1_Clinical_Study_Membership__r.VTD2_Study_Phone_Number__c;
            }
        }
        return patientInfo;
    }

    public class PatientInfo {
        public String firstName { get; set; }
        public String studyNickname { get; set; }
        public String studyPhoneNumber { get; set; }
    }
}