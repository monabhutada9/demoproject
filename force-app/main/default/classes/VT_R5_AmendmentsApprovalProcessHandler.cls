/**
 * Created by Alexey Mezentsev on 11/24/2020.
 * PB Name : "Amendments | Approval" (VTD2_PI_notified_for_Protocol_Amendment_signature) Replaced by Apex SH-16209
 * PB Description: "IQVTR2NEO-531/FR-PA-236 Tasks-004. Sign Protocol Amendments Signature Page task. |SH-7370 |SH-7311|SH-7585"
 */

public without sharing class VT_R5_AmendmentsApprovalProcessHandler extends Handler {
    private static List<VT_D2_TNCatalogNotifications.ParamsHolder> notifications = new List<VT_D2_TNCatalogNotifications.ParamsHolder>();
    private static List<VT_D2_TNCatalogTasks.ParamsHolder> userTasks = new List<VT_D2_TNCatalogTasks.ParamsHolder>();
    private static List<VT_R3_EmailsSender.ParamsHolder> emailParamsHolders = new List<VT_R3_EmailsSender.ParamsHolder>();
    private static Set<Id> documentsIdsWithApproval = new Set<Id>();

    protected override void onBeforeUpdate(Handler.TriggerContext context) {
        Date today = Date.today();
        for (VTD1_Document__c doc : (List<VTD1_Document__c>) context.newList) {
            VTD1_Document__c oldDoc = (VTD1_Document__c) context.oldMap.get(doc.Id);
            if (isIRBApproved(doc, oldDoc)) {
                doc.VTD1_Approval_Date__c = today;
                documentsIdsWithApproval.add(doc.Id);
            }
        }
    }

    protected override void onAfterUpdate(Handler.TriggerContext context) {
        Set<Id> documentsIdsForSigning = new Set<Id>();
        Set<Id> documentsIdsForCloseTasks = new Set<Id>();
        for (VTD1_Document__c doc : (List<VTD1_Document__c>) context.newList) {
            VTD1_Document__c oldDoc = (VTD1_Document__c) context.oldMap.get(doc.Id);
            if (isSentForSignature(doc, oldDoc)) {
                documentsIdsForSigning.add(doc.Id);
            }
            if (isReadyToCloseTasks(doc, oldDoc)) {
                documentsIdsForCloseTasks.add(doc.Id);
            }
        }
        if (!documentsIdsForSigning.isEmpty()) {
            notifyPIAboutSigning(documentsIdsForSigning);
        }
        if (!documentsIdsForCloseTasks.isEmpty()) {
            closeRelatedTasks(documentsIdsForCloseTasks);

        }
        if (!documentsIdsWithApproval.isEmpty()) {
            createNotificationForMembers(documentsIdsWithApproval);
            documentsIdsWithApproval.clear();
        }
        sendTaskNotificationAndEmail();
    }

    private static Boolean isIRBApproved(VTD1_Document__c doc, VTD1_Document__c oldDoc) {
        return doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT
                && doc.VTD1_Regulatory_Document_Type__c == VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_SIGNATURE_PAGE
                && doc.VTD1_Study__c != null
                && doc.VTR4_IRB_Approved__c && !oldDoc.VTR4_IRB_Approved__c;
    }

    private static Boolean isSentForSignature(VTD1_Document__c doc, VTD1_Document__c oldDoc) {
        return doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT
                && doc.VTD1_Regulatory_Document_Type__c == VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_SIGNATURE_PAGE
                && doc.VTD1_Site__c != null
                && doc.VTD2_Sent_for_Signature__c
                && !oldDoc.VTD2_Sent_for_Signature__c;
    }

    private static Boolean isReadyToCloseTasks(VTD1_Document__c doc, VTD1_Document__c oldDoc) {
        return doc.VTD1_Regulatory_Document_Type__c == VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_SIGNATURE_PAGE
                && doc.VTD2_Sent_for_Signature__c
                && doc.VTD2_EAFStatus__c == VT_R4_ConstantsHelper_Documents.DOCUMENT_EAF_STATUS_COMPLETED
                && oldDoc.VTD2_EAFStatus__c != VT_R4_ConstantsHelper_Documents.DOCUMENT_EAF_STATUS_COMPLETED;
    }

    // Flow Name: "Amendment | Protocol Approved"
    private static void createNotificationForMembers(Set<Id> docIds) {
        Map<Id, Set<Id>> docIdsByPrimaryPIMap = new Map<Id, Set<Id>>();
        Map<Id, List<Id>> communityReceiversByDocIds = new Map<Id, List<Id>>();
        Map<Id, List<Id>> lightningReceiversByDocIds = new Map<Id, List<Id>>();
        List<VTD1_Document__c> docListWithRelations = [
                SELECT VTD1_Site__r.VTR3_Study_Geography__r.VTR5_Primary_VTSL__c,
                        VTD1_Study__r.VTD1_Project_Lead__c,
                        VTD1_Site__r.VTD1_PI_User__c,
                        VTD1_Site__r.VTD1_Study_Team_Member__c
                FROM VTD1_Document__c
                WHERE Id IN :docIds
        ];
        for (VTD1_Document__c doc : docListWithRelations) {
            addReceiver(doc.Id, doc.VTD1_Site__r.VTR3_Study_Geography__r.VTR5_Primary_VTSL__c, lightningReceiversByDocIds);
            addReceiver(doc.Id, doc.VTD1_Study__r.VTD1_Project_Lead__c, lightningReceiversByDocIds);
            addReceiver(doc.Id, doc.VTD1_Site__r.VTD1_PI_User__c, communityReceiversByDocIds);
            Id siteStudyMemberPI = doc.VTD1_Site__r.VTD1_Study_Team_Member__c;
            if (siteStudyMemberPI != null) {
                if (!docIdsByPrimaryPIMap.containsKey(siteStudyMemberPI)) {
                    docIdsByPrimaryPIMap.put(siteStudyMemberPI, new Set<Id>());
                }
                docIdsByPrimaryPIMap.get(siteStudyMemberPI).add(doc.Id);
            }
        }
        if (!docIdsByPrimaryPIMap.isEmpty()) {
            List<Study_Site_Team_Member__c> SCRandPGList = [
                    SELECT VTR2_SCr_User_ID__c, VTD1_PG_User_ID__c, VTR2_Associated_PI__c, VTD1_Associated_PI__c
                    FROM Study_Site_Team_Member__c
                    WHERE (VTD1_Associated_PI__c IN :docIdsByPrimaryPIMap.keySet() AND VTD1_PG_User_ID__c != NULL)
                    OR (VTR2_Associated_PI__c IN :docIdsByPrimaryPIMap.keySet() AND VTR2_SCr_User_ID__c != NULL)
            ];
            for (Study_Site_Team_Member__c siteMember : SCRandPGList) {
                Boolean isCommunityUser = siteMember.VTR2_SCr_User_ID__c != null;
                Id receiver = isCommunityUser ? siteMember.VTR2_SCr_User_ID__c : siteMember.VTD1_PG_User_ID__c;
                Set<Id> relatedDocs = isCommunityUser ?
                        docIdsByPrimaryPIMap.get(siteMember.VTR2_Associated_PI__c)
                        : docIdsByPrimaryPIMap.get(siteMember.VTD1_Associated_PI__c);
                for (Id docId : relatedDocs) {
                    addReceiver(docId, receiver, isCommunityUser ? communityReceiversByDocIds : lightningReceiversByDocIds);
                }
            }
        }
        if (!communityReceiversByDocIds.isEmpty() || !lightningReceiversByDocIds.isEmpty()) {
            List<String> tnCatalogCodes = new List<String>();
            List<Id> sourceIds = new List<Id>();
            Map<String, List<Id>> receiversMap = new Map<String, List<Id>>();
            for (Id docId : communityReceiversByDocIds.keySet()) {
                tnCatalogCodes.add('N563');
                sourceIds.add(docId);
                receiversMap.put('N563' + docId, communityReceiversByDocIds.get(docId));
            }
            for (Id docId : lightningReceiversByDocIds.keySet()) {
                tnCatalogCodes.add('N546');
                sourceIds.add(docId);
                receiversMap.put('N546' + docId, lightningReceiversByDocIds.get(docId));
            }
            VT_D2_TNCatalogNotifications.generateNotifications(tnCatalogCodes, sourceIds, null, null, receiversMap);
        }
    }

    private static void notifyPIAboutSigning(Set<Id> docIds) {
        List<VTD1_Document__c> docListWithPI = [SELECT VTD1_Site__r.VTD1_PI_User__c FROM VTD1_Document__c WHERE Id IN :docIds];
        for (VTD1_Document__c doc : docListWithPI) {
            Id userPI = doc.VTD1_Site__r.VTD1_PI_User__c;
            if (userPI != null) {
                notifications.add(new VT_D2_TNCatalogNotifications.ParamsHolder(doc.Id, 'N548'));
                userTasks.add(new VT_D2_TNCatalogTasks.ParamsHolder(doc.Id, 'T602'));
                emailParamsHolders.add(new VT_R3_EmailsSender.ParamsHolder(
                        userPI,
                        doc.Id,
                        'VTR4_PI_notified_for_PA_signature'
                ));
            }
        }
    }

    private static void closeRelatedTasks(Set<Id> docIds) {
        List<Task> tasksToComplete = [
                SELECT Id
                FROM Task
                WHERE WhatId IN :docIds
                AND Status = 'Open'
                AND VTD2_Task_Unique_Code__c = 'T602'
        ];
        for (Task t : tasksToComplete) {
            t.Status = VT_R4_ConstantsHelper_Tasks.TASK_STATUS_COMPLETED;
        }
        update tasksToComplete;
    }

    private static void sendTaskNotificationAndEmail() {
        if (!userTasks.isEmpty()) {
            VT_D2_TNCatalogTasks.generateTask(userTasks);
            userTasks.clear();
        }
        if (!notifications.isEmpty()) {
            VT_D2_TNCatalogNotifications.generateNotification(notifications);
            notifications.clear();
        }
        if (!emailParamsHolders.isEmpty() && !Test.isRunningTest()) {
            VT_R3_EmailsSender.sendEmails(emailParamsHolders);
            emailParamsHolders.clear();
        }
    }

    private static void addReceiver(Id docId, Id receiverId, Map<Id, List<Id>> receiversByDocMap) {
        if (receiverId != null) {
            if (!receiversByDocMap.containsKey(docId)) {
                receiversByDocMap.put(docId, new List<Id>());
            }
            receiversByDocMap.get(docId).add(receiverId);
        }
    }
}