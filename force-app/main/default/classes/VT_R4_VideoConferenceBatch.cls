/**
 * Created by Andrey Pivovarov on 4/17/2020.
 */

global without sharing class VT_R4_VideoConferenceBatch implements Database.Batchable<SObject> {
    private VT_R4_VideoConferenceScheduler context;
    private Datetime curDate;

    public VT_R4_VideoConferenceBatch(VT_R4_VideoConferenceScheduler context, Datetime dt) {
        curDate = dt.addSeconds(-dt.second());
        this.context = context;
    }
    /*for single run*/
    public VT_R4_VideoConferenceBatch() {
        curDate = System.now();
        curDate = curDate.addSeconds(-curDate.second());
    }
    global Iterable<SObject> start(Database.BatchableContext BC) {
        List<Video_Conference__c> videoConferences = new List<Video_Conference__c>();
        if (Test.isRunningTest() || (VTR4_ExternalScheduler__c.getInstance().VTR4_Active__c)) {
            System.debug('curTime ' + curDate);
            videoConferences = [
                    SELECT Scheduled_For__c, VTD1_Scheduled_Visit_End__c, LastModifiedDate,
                    (SELECT VTR4_Scheduled_for__c, VTR4_Scheduled_Visit_End__c, VTR4_ReadyToStart__c, VTR4_ReadyToEnd__c
                    FROM Scheduler_Video_Conferences__r)
                    FROM Video_Conference__c
                    WHERE (Scheduled_For__c >: curDate.addMinutes(-15)
                    AND Scheduled_For__c <=: curDate.addHours(1))
                    AND VTD1_Actual_Visit__c != NULL
                    ORDER BY Scheduled_For__c
            ];
        }
        return videoConferences;
    }

    global void execute(Database.BatchableContext BC, List<Video_Conference__c> scope) {
        if (scope == null || scope.isEmpty()) return;
        Integer countSchedulerVC = [SELECT COUNT() FROM VTR4_Scheduler_Video_Conference__c];
        List<VTR4_Scheduler_Video_Conference__c> currentScheduleVideos = new List<VTR4_Scheduler_Video_Conference__c>();
        for (Video_Conference__c v : scope) {
            if (v.Scheduler_Video_Conferences__r.size() == 0 && countSchedulerVC < 9500) {
                if (v.Scheduled_For__c >= curDate.addMinutes(5)) {
                    currentScheduleVideos.add(new VTR4_Scheduler_Video_Conference__c(
                            VTR4_Video_Conference__c = v.Id,
                            VTR4_Scheduled_for__c = v.Scheduled_For__c,
                            VTR4_Scheduled_Visit_End__c = v.VTD1_Scheduled_Visit_End__c)
                    );
                }
            }
            else if ((v.Scheduler_Video_Conferences__r.size() != 0 &&
                    v.Scheduler_Video_Conferences__r[0].VTR4_Scheduled_for__c != v.Scheduled_For__c ||
                    v.Scheduler_Video_Conferences__r[0].VTR4_Scheduled_Visit_End__c != v.VTD1_Scheduled_Visit_End__c)) {
                if (v.Scheduler_Video_Conferences__r[0].VTR4_Scheduled_for__c != v.Scheduled_For__c) {
                    v.Scheduler_Video_Conferences__r[0].VTR4_Scheduled_for__c = v.Scheduled_For__c;
                    v.Scheduler_Video_Conferences__r[0].VTR4_ReadyToStart__c = false;
                }
                v.Scheduler_Video_Conferences__r[0].VTR4_ReadyToEnd__c = false;
                v.Scheduler_Video_Conferences__r[0].VTR4_Scheduled_Visit_End__c = v.VTD1_Scheduled_Visit_End__c;
                currentScheduleVideos.add(v.Scheduler_Video_Conferences__r[0]);
            }
        }
        if (!currentScheduleVideos.isEmpty()) upsert currentScheduleVideos;
    }
    global void finish(Database.BatchableContext BC) {
        try {
            List<VTR4_Scheduler_Video_Conference__c> schedulerVideoConferences = [
                    SELECT Id
                    FROM VTR4_Scheduler_Video_Conference__c
                    WHERE VTR4_Video_Conference__r.VTD1_Scheduled_Visit_End__c <: curDate
                    OR VTR4_Video_Conference__r.Scheduled_For__c >: curDate.addHours(1)
            ];
            delete schedulerVideoConferences;
        }
        catch(Exception e) {
            System.debug('VTR4_Scheduler_Video_Conference__c delete exception ' + e);
        }
        finally {
            if (this.context != null) this.context.scheduleJob(context.getAlignedTimeJob(System.now()));
        }
    }
}