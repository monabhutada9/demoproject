/**
* @author: Carl Judge
* @date: 11-Sep-18
* @description: 
**/

@IsTest
public class VTD1_ChatMemberHandler_Test {

    @testSetup
    private static void setupMethod() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.stopTest();
    }

    @isTest
    private static void doTest() {
        Id studyId = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1].Id;
        Id userId = [SELECT VTD1_User__c FROM VTD1_Study_Sharing_Configuration__c WHERE VTD1_Study__c = :studyId LIMIT 1].VTD1_User__c;

        VTD1_Chat__c chat = new VTD1_Chat__c(VTR2_Study__c = studyId);
        insert chat;

        VTD1_Chat_Member__c member = new VTD1_Chat_Member__c(
            VTD1_User__c = userId,
            VTD1_Chat__c = chat.Id
        );
        Test.startTest();
        insert member;
        Test.stopTest();
//        System.assertEquals(1, [SELECT Count() FROM VTD1_Chat__Share WHERE UserOrGroupId = :userId AND ParentId = :chat.Id]);
        System.assertEquals(1, [SELECT Count() FROM EntitySubscription WHERE SubscriberId = :userId AND ParentId = :chat.Id]);

        delete member;
        System.assertEquals(0, [SELECT Count() FROM EntitySubscription WHERE SubscriberId = :userId AND ParentId = :chat.Id]);

        undelete member;
        System.assertEquals(1, [SELECT Count() FROM EntitySubscription WHERE SubscriberId = :userId AND ParentId = :chat.Id]);
    }
}