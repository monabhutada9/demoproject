/**
 * @author: Alexander Komarov
 * @date: 24.09.2020
 * @description:
 */

@IsTest
private class VT_R5_MobilePushRegServiceTest {
    @IsTest
    static void testBehavior1() {
        insertRegistration();
        setRestContext('/patient/v1/push-service/','POST', 'Unsupported');
        VT_R5_MobileRestRouter.doPOST();
        System.assert(getCurrentUserRegistrations().size() == 1);
    }

    @IsTest
    static void testBehavior2() {
        insertRegistration();
        setRestContext('/patient/v1/push-service/','POST',
                '{"deviceUniqueCode" : "deviceId","connectionToken":"token2","serviceType":"iOS"}');
        VT_R5_MobileRestRouter.doPOST();
        System.assert(getCurrentUserRegistrations().size() == 1);

    }
    @IsTest
    static void testBehavior3() {
        insertRegistration();
        setRestContext('/patient/v1/push-service/','DELETE',
                '{"deviceUniqueCode" : "deviceId"}');
        VT_R5_MobileRestRouter.doDELETE();
    }

    private static List<VTR5_MobilePushRegistrations__c> getCurrentUserRegistrations() {
        return [SELECT Id FROM VTR5_MobilePushRegistrations__c WHERE VTR5_User__c = :UserInfo.getUserId()];
    }

    private static void setRestContext(String URI, String method, String body) {
        RestContext.request = new RestRequest();
        RestContext.response = new RestResponse();
        RestContext.request.requestURI = URI;
        RestContext.request.httpMethod = method;
        RestContext.request.requestBody = Blob.valueOf(body);
    }

    private static void insertRegistration() {
        insert new VTR5_MobilePushRegistrations__c(
                VTR5_User__c = UserInfo.getUserId(),
                VTR5_ConnectionToken__c = 'token',
                VTR5_ServiceType__c = 'Android',
                VTR5_DeviceIdentifier__c = 'deviceId');
    }
}