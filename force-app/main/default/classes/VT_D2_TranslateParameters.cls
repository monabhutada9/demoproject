/**
 * Created by Leonid Bartenev
 *
 * Retrieve and translate parameters:
 *
 * parameter can be reference, format: [recordId][field name]
 * Example of reference parameter: '00B3D000001XDmUUAWStatus__c'
 *
 * Or parameter can be simple string: 'Some text'
 *
 */

public without sharing class VT_D2_TranslateParameters {
    private static Map<Id, String> idNameProfiles;
    private static Set<String> timeZoneSet;

    public class TranslateParametersException extends Exception{}
    
    public class Parameter{
        public Integer paramNumber;
        public Boolean isReference;
        public String value;
        public Id recordId;
        public String fieldName;
        public String queryFieldName;
        public String objectName;
        public transient SObject targetObject;
        public String sentenceFieldName;
        public String tag;

        public Parameter(){}

        public Parameter(SObject targetObject, String sentenceFieldName, String paramStr, Integer paramNumber){
            isReference = false;
            this.targetObject = targetObject;
            this.sentenceFieldName = sentenceFieldName;
            this.paramNumber = paramNumber;
            value = paramStr;
            String id = paramStr.mid(0, 18);
            if (VT_D1_HelperClass.isValidId(id)){
                try{
                    recordId = id;
                    fieldName = paramStr.removeStart(id);
                    if(String.isEmpty(fieldName)) throw new TranslateParametersException('Empty field name');
                    fieldName = fieldName.toLowerCase();
                    DescribeSObjectResult objectDescribe = recordId.getSobjectType().getDescribe();
                    objectName = objectDescribe.getName();
                    SObjectField objectField = objectDescribe.fields.getMap().get(fieldName);
                    if(objectField == null) throw new TranslateParametersException('Wrong field name: ' + fieldName);
                    queryFieldName = fieldName;
                    DisplayType fieldType = objectField.getDescribe().getType();
                    if(fieldType == DisplayType.PICKLIST){
                        queryFieldName = 'toLabel(' + fieldName + ')';
                    } else if(fieldType == DisplayType.DATE || fieldType == DisplayType.DATETIME){
                        queryFieldName = 'format(' + fieldName + ')';
                    }
                    isReference = true;
                }catch (Exception e){
                    value = paramStr + ' ERROR: ' + e.getMessage();
                }
            }
            if (paramStr.contains('#date')) {
                value = VT_D1_TranslateHelper.translate(paramStr.remove('#date'), UserInfo.getLanguage());
            }
            try {
                if (getAllTimeZone().contains(paramStr)) {
                    value = VT_D1_TranslateHelper.translateTimeZone(paramStr, UserInfo.getLanguage());
            }
        }
            catch (Exception e){}

        }

        public void setValue(SObject sobj){
            if(sobj == null) {
                value = 'Record ' + recordId + ' not found';
                return;
            }
            if (tag == '#VisitName') {
              value = VT_D1_HelperClass.getActualVisitName((VTD1_Actual_Visit__c) sobj);
            } else {
                if(sobj.get(fieldName) != null){
                    value = sobj.get(fieldName) + '';
                }else{
                    value = null;
                }
            }
        }
        
        public void substituteInTargetField(){
            String targetFieldValue = (String)targetObject.get(sentenceFieldName);
            if (tag == null) {
                tag = '#' + paramNumber;
            }
            if (targetFieldValue != null) {
                targetFieldValue = targetFieldValue.replace(tag, value + '');
                targetObject.put(sentenceFieldName, targetFieldValue);
            }
        }
        private Set<String> getAllTimeZone(){
            if(timeZoneSet == null){
                timeZoneSet = new Set<String>();
                Schema.DescribeFieldResult fieldResult = User.TimeZoneSidKey.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry f : ple){
                    timeZoneSet.add(f.getValue());
                }
            }
            return timeZoneSet;
        }
    }
    
//    private static String getProfileName (Id profileId) {
//        if (idNameProfiles == null) {
//            idNameProfiles = new Map<Id, String>{
//                    VT_D1_HelperClass.getProfileIdByName(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) => VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME,
//                    VT_D1_HelperClass.getProfileIdByName(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME) => VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME};
//        }
//        return idNameProfiles.get(profileId);
//    }
    
    public static List<Parameter> getParameters(SObject targetObject, String sentenceFieldName, String paramsFieldName){
        List<Parameter> parameters = new List<Parameter>();
        if(String.isEmpty(paramsFieldName)) return parameters;
        String paramsFieldValue = ((String)targetObject.get(paramsFieldName));
        if(String.isEmpty(paramsFieldValue)) return parameters;
        paramsFieldValue = paramsFieldValue.trim();
        Integer i = 1;
        for(String paramStr : paramsFieldValue.split(';')){
            if (paramStr == '#VisitName') {
                String objName = targetObject.getSObjectType().getDescribe().getName();
                if (objName == 'VTD1_NotificationC__c' && ((VTD1_NotificationC__c) targetObject).VTD2_VisitID__c != null
                        || objName == 'Task' && (((Task) targetObject).VTD1_Actual_Visit__c != null
                            || (((Task) targetObject).WhatId != null
                                && ((Task) targetObject).WhatId.getSObjectType().getDescribe().getName() == 'VTD1_Actual_Visit__c'))) {
                    String recordId = '';
                    if (objName == 'VTD1_NotificationC__c') {
                        recordId = ((VTD1_NotificationC__c) targetObject).VTD2_VisitID__c;
                    } else {
                        recordId = (((Task) targetObject).VTD1_Actual_Visit__c != null ?
                                ((Task) targetObject).VTD1_Actual_Visit__c
                                 : ((Task) targetObject).WhatId);
                    }
                    Parameter visitNameParameter = new Parameter();
                    visitNameParameter.value = paramStr;
                    visitNameParameter.tag = paramStr;
                    visitNameParameter.targetObject = targetObject;
                    visitNameParameter.recordId = recordId;
                    visitNameParameter.isReference = true;
                    visitNameParameter.fieldName = 'Name, VTD1_Unscheduled_Visit_Type__c, VTD1_Protocol_Visit__r.VTD1_EDC_Name__c, VTR3_Visit_Number_UI__c, VTR3_LTFUStudyVisit__c';
                    visitNameParameter.sentenceFieldName = sentenceFieldName;
                    visitNameParameter.queryFieldName = visitNameParameter.fieldName;
                    visitNameParameter.objectName = 'VTD1_Actual_Visit__c';
                    visitNameParameter.paramNumber = 97;
                    parameters.add(visitNameParameter);
                }
            } else {
                parameters.add(new Parameter(targetObject, sentenceFieldName, paramStr, i));
            }
            i++;
        }
        while (paramsFieldValue.endsWith(';')) {
            paramsFieldValue = paramsFieldValue.removeEnd(';');
            parameters.add(new Parameter(targetObject, sentenceFieldName, '', i));
            i++;
        }
        return parameters;
    }
    
    public static void retrieveParamsAndUpdateTargetObjects(List<Parameter> params){
        Map<String, Set<String>> objectFieldsMap = new Map<String, Set<String>>();
        Map<String, Set<Id>> objectIdsMap = new Map<String, Set<Id>>();
        for(Parameter param : params){
            if(param.isReference){
                Set<String> objectFields = objectFieldsMap.get(param.objectName);
                if(objectFields == null) objectFields = new Set<String>();
                objectFields.add(param.queryFieldName);
                objectFieldsMap.put(param.objectName, objectFields);
                
                Set<Id> objectIds = objectIdsMap.get(param.objectName);
                if(objectIds == null) objectIds = new Set<Id>();
                objectIds.add(param.recordId);
                objectIdsMap.put(param.objectName, objectIds);
            }
        }
        Map<String, Map<Id, SObject>> objectsMap = new Map<String, Map<Id, SObject>>();
        List<SObject> dataTranslateList = new List<SObject>();
        for(String objectName : objectFieldsMap.keySet()){
            List<String> fields = new List<String>(objectFieldsMap.get(objectName));
            List<Id> recordIds = new List<Id>(objectIdsMap.get(objectName));
            String query = 'SELECT ' + String.join(fields, ',') + ' FROM ' + objectName + ' WHERE Id IN (\'' + String.join(recordIds, '\',\'') + '\')';
            Map<Id, SObject> objectRecordsMap = new Map<Id, SObject>(Database.query(query));
            objectsMap.put(objectName, objectRecordsMap);
            dataTranslateList.addAll(objectRecordsMap.values());
        }
        //translate retrieved data:
        VT_D1_TranslateHelper.translate(dataTranslateList);
        
        for(Parameter param : params){
            if(param.isReference){
                SObject sobj = objectsMap.get(param.objectName).get(param.recordId);
                param.setValue(sobj);
            }
            param.substituteInTargetField();
        }
    }

}