/**
* @author: Carl Judge
* @date: 31-Oct-18
* @description: Test for all milestone code
**/

@isTest
private class VT_D1_Milestone_Test {

    @testSetup
    private static void setupMethod() {
        VT_R5_ShareGroupMembershipService.disableMembership = true;
        VT_R3_GlobalSharing.disableForTest = true;
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        study.VTD1_Randomized__c = 'No';
        update study;
        Set<String> setFirstNames = new Set<String>{'PrimaryPGUser1', 'SecondaryPGUser1','PIUser1', 'PIBackupUser1', 'CRAUser11'};
        User priPG;
        User secPG;
        User priPI;
		User secPI; 
        User cra;    
        for(User objUser : [SELECT Id, FirstName, ContactId FROM User WHERE FirstName IN : setFirstNames]){
            if(objUser.FirstName == 'PrimaryPGUser1'){
                priPG = objUser;
            }else if(objUser.FirstName == 'SecondaryPGUser1'){
                secPG = objUser;
            }else if(objUser.FirstName == 'PIUser1'){
                priPI = objUser;
            }else if(objUser.FirstName == 'PIBackupUser1'){
                secPI = objUser;
            }else if(objUser.FirstName == 'CRAUser11'){
                cra = objUser;
            }
        }


		/*User priPG = [SELECT Id, ContactId FROM User WHERE FirstName = 'PrimaryPGUser1'];
        User secPG = [SELECT Id, ContactId FROM User WHERE FirstName = 'SecondaryPGUser1'];
        User priPI = [SELECT Id, ContactId FROM User WHERE FirstName = 'PIUser1'];
        User secPI = [SELECT Id, ContactId FROM User WHERE FirstName = 'PIBackupUser1'];*/

        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);

        User patientUser = [
            SELECT Name, Profile.Name, ContactId, Contact.AccountId,  Contact.Account.Candidate_Patient__c,
                Contact.Account.Candidate_Patient__r.HealthCloudGA__Address1Country__c, IsActive
            FROM User
            ORDER BY CreatedDate DESC LIMIT 1
        ];
        VTD1_Patient__c patient = new VTD1_Patient__c(VTD1_Account__c = patientUser.Contact.AccountId, VTD1_Contact__c = patientUser.ContactId);
        insert patient;

        Case cas = new Case(
            AccountId = patientUser.Contact.AccountId,
            VTD1_Study__c = study.Id,
            VTD1_Primary_PG__c = priPG.Id,
            VTD1_Secondary_PG__c = secPG.Id,
            VTD1_PI_user__c = priPI.Id,
            VTD1_Backup_PI_User__c = secPI.Id,
            ContactId = patientUser.ContactId,
            Status = 'Consented',
            VTD1_PCF_Safety_Concern_Indicator__c = 'No',
            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VTD1_PCF').getRecordTypeId(),
            VTD1_PI_contact__c = priPI.ContactId,
            VTD1_Patient_User__c = patientUser.Id,
            VTD1_Patient__c = patient.Id,
            VTD1_Screening_Complete__c = true,
            VTD1_Eligibility_Status__c = 'Eligible',
            VTD2_Baseline_Visit_Complete__c = true
        );
        insert cas;

    Set<String> setStrings = new Set<String>{'PIUser1', 'PIBackupUser1', 'CRAUser11'};
    Study_Team_Member__c pi;
    Study_Team_Member__c pi2;
    Study_Team_Member__c craSTM;
    for(Study_Team_Member__c objSTM : [SELECT	Id, 
                                                User__c,
                                                User__r.FirstName, 
                                                User__r.Profile.Name,
                                                Study_Team_Member__c                                           
                                        FROM Study_Team_Member__c
                                        WHERE (Study__c = :study.Id AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName IN : setStrings) OR (Study__c = :study.Id AND User__r.Profile.Name = 'CRA')]){
                                            if(objSTM.User__r.Profile.Name == 'CRA'){
                                                craSTM = objSTM;
                                            }else if(objSTM.User__r.FirstName == 'PIBackupUser1'){
                                                pi2 = objSTM;
                                            }else if(objSTM.User__r.FirstName == 'PIUser1'){
                                                pi = objSTM;
                                            }         
                                        }

        /*
        Study_Team_Member__c pi = [
                SELECT Id,User__r.FirstName,Study_Team_Member__c
                FROM Study_Team_Member__c
                WHERE Study__c = :study.Id AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName = 'PIUser1'
        ];

        Study_Team_Member__c pi2 = [
                SELECT Id,User__r.FirstName,Study_Team_Member__c
                FROM Study_Team_Member__c
                WHERE Study__c = :study.Id AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName = 'PIBackupUser1'
        ];*/
        //Commented by Priyanka Ambre to remove Remote, Onsite references- SH-17441
        //pi.VTR2_Remote_CRA__c = pi2.Id;
        //pi.VTR2_Onsite_CRA__c = pi2.Id;
        update pi;       

        Virtual_Site__c vSite = new Virtual_Site__c(
                VTD1_Study__c = study.Id,
                VTD1_Study_Site_Number__c = '12345',
                VTD1_Study_Team_Member__c = pi.Id
        );
        insert vSite;
        Virtual_Site__c vSite1 = new Virtual_Site__c(
                VTD1_Study__c = study.Id,
                VTD1_Study_Site_Number__c = '12355',
                Name = 'REG_TEST',
            	VTD1_Study_Team_Member__c = pi2.Id
        );
        insert vSite1;

        HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
        objVirtualShare.ParentId = study.Id;
        objVirtualShare.UserOrGroupId = craSTM.User__c;
        objVirtualShare.AccessLevel = 'Edit';
        insert objVirtualShare;

        
        Study_Site_Team_Member__c sstm1 = new Study_Site_Team_Member__c();
        sstm1.VTR5_Associated_CRA__c = craSTM.Id;
        sstm1.VTD1_SiteID__c = vSite1.Id ;
        insert sstm1;

        VTD1_Monitoring_Visit__c mVisit = new VTD1_Monitoring_Visit__c(
            VTD1_Study__c = study.Id,
            VTD1_Virtual_Site__c = vSite1.Id
            //VTR2_Remote_Onsite__c = 'Remote'
        );

        System.runAs(new User(Id = craSTM.User__c)){
            insert mVisit;
        }

        VTD1_Document__c doc = new VTD1_Document__c(
            VTD1_Current_Workflow__c = 'Site RSU No IRB',
            VTD1_Site__c = vSite.Id,
            RecordTypeId = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD1_Regulatory_Document').getRecordTypeId()

        );
        insert doc;
        Test.stopTest();

    }

    @isTest
    private static void testCase() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
            Id studyId = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c WHERE Name = 'TestProt-001'].Id;

            Case cas = [
                    SELECT Id, Status, VTD1_Screening_Complete__c, VTD1_Eligibility_Status__c
                    FROM Case
                    WHERE RecordType.DeveloperName = 'VTD1_PCF'
            ];

            cas.Status = 'Consented';
            update cas;

            cas.Status = VT_R4_ConstantsHelper_AccountContactCase.CASE_SCREENED;
            update cas;

            cas.Status = VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED;
            update cas;

        Test.stopTest();
    }

    @isTest
    private static void testOther() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        Id studyId = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c WHERE Name = 'TestProt-001'].Id;

        VTD1_Monitoring_Visit__c mVisit = [SELECT Id FROM VTD1_Monitoring_Visit__c WHERE VTD1_Study__c = :studyId LIMIT 1];
        mVisit.VTD1_Study_Site_Visit_Status__c = VT_R4_ConstantsHelper_VisitsEvents.MONITORING_VISIT_COMPLETE;
        mVisit.VTD1_Site_Visit_Type__c = VT_R4_ConstantsHelper_VisitsEvents.MONITORING_VISIT_TYPE_CLOSEOUT;
        update mVisit;
        mVisit.VTD1_Site_Visit_Type__c = VT_R4_ConstantsHelper_VisitsEvents.MONITORING_VISIT_TYPE_INITIATION;
        update mVisit;
        mVisit.VTD1_Site_Visit_Type__c = VT_R4_ConstantsHelper_VisitsEvents.MONITORING_VISIT_TYPE_SELECTION;
        update mVisit;


        Virtual_Site__c vSite = [SELECT Id FROM Virtual_Site__c WHERE VTD1_Study__c = :studyId LIMIT 1];
        vSite.VTD1_Site_Status__c = VT_R4_ConstantsHelper_AccountContactCase.SITE_ENROLLMENT_OPEN;
        update vSite;
        vSite.VTD1_Site_Status__c = VT_R4_ConstantsHelper_AccountContactCase.SITE_CLOSED;
        update vSite;
        Id rtId = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByName().get('Regulatory Document').getRecordTypeId();
        VTD1_Document__c document = new VTD1_Document__c(RecordTypeId = rtId);
        VTD1_Document__c doc = [SELECT Id FROM VTD1_Document__c];
        doc.Document_Name__c = VT_R4_ConstantsHelper_Documents.DOCUMENT_EDPRC_NAME;
        doc.VTD1_Status__c = VT_R4_ConstantsHelper_Documents.DOCUMENT_SITE_RSU_COMPLETE;
        update doc;

        VT_D1_MilestoneTableController.getParentType(studyId);
        Map<String, VTD1_Milestone_Component_Config__mdt> mdtMap = VT_D1_MilestoneTableController.getMdtMap(studyId);
        List<sObject> mileStones = VT_D1_MilestoneTableController.getMilestones(studyId, mdtMap);
        VT_D1_MilestoneTableController.doSave(JSON.serialize(mileStones[0]));
        Test.stopTest();
    }
}