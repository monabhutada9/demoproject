@IsTest
public with sharing class VT_R2_DocumentVersionsListControllerTest {

    public static void testGetVersions() {
        new DomainObjects.ContentVersion_t().persist();

        Test.startTest();
        List<ContentVersion> contentVersions = VT_D1_DocumentVersionsListController.getVersions([SELECT ContentDocumentId FROM ContentVersion][0].ContentDocumentId);
        Test.stopTest();

        System.assert(!contentVersions.isEmpty());
    }
}