/**
* @author Ruslan Mullayanov
* @description Data for the current user's filtered links
*/

public with sharing class VT_D2_QuickLinksController {
    @AuraEnabled(Cacheable=true)
    public static String getProfileName() {
        return VT_D1_HelperClass.getProfileNameOfCurrentUser();
    }

    @AuraEnabled(Cacheable=true)
    public static String getOpMetricsDashboardId() {
        try {
            return VTD1_RTId__c.getOrgDefaults().VTD2_Operational_Metrics_Dashboard__c;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled(Cacheable=true)
    public static String getAllListViews(List<String> sobjectTypes) {
        try {
            List<ListView> listViews = [SELECT Id, Name, SobjectType FROM ListView WHERE SobjectType IN :sobjectTypes AND DeveloperName='All'];
            return JSON.serialize(listViews);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
}