/**
* @author: Carl Judge
* @date: 27-May-20
* @description: eCOA response export API
**/

public class VT_R5_eCoaResponseExport {
    /**
     * Get diary responses from eCOA
     * @param studyGuid - Guid of the study from eCOA
     * @param queryBuilder - query for the request
     * @return JSON response
     */
    public static VT_D1_HTTPConnectionHandler.Result getResponseExport(String studyGuid, VT_R5_eCoaQueryBuilder queryBuilder) {
        String endpoint = VT_R4_ConstantsHelper_Integrations.ENDPOINT_URL_ECOA_RESPONSES;
        String method = 'POST';
        String action = 'eCOA Response Export';

        return new VT_R5_eCoaApiRequest()
            .setStudyGuid(studyGuid)
            .setMethod(method)
            .setAction(action)
            .setBody(new VT_R5_RequestBuilder_eCoaResponseExport(studyGuid, queryBuilder))
            .setEndpoint(endpoint)
            .setSkipLogging(true)
            .send();
    }
    public static VT_D1_HTTPConnectionHandler.Result getResponseExport(String studyGuid) {
        return getResponseExport(studyGuid, VT_R5_eCoaQueryBuilder.getDefaultQuery());
    }

    public static Response getResponseFromResult(VT_D1_HTTPConnectionHandler.Result result) {
        return (Response)JSON.deserialize(result.httpResponse.getBody(), Response.class);
    }

    public class Response {
        public Data data;
        public QueryResults queryResults;
    }
    public class Data {
        public List<String> headers;
        public List<List<String>> rows;
    }
    public class QueryResults {
        public Paging paging;
    }
    public class Paging {
        public Integer totalRecords;
        public Integer offset;
        public Integer offsetNext;
        public Integer offsetPrevious;
    }
}