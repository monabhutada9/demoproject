@IsTest
private with sharing class VT_D1_UpNextVisitTest {
    private static Case cas;
    private static User u;

    @TestSetup
    private static void setupMethod() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }

    static {
        List<Case> casesList = [SELECT VTD1_Patient_User__c, VTD1_Patient__r.VTD1_Patient_Name__c, VTD1_Study__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        if(casesList.size() > 0){
            casesList[0].VTD1_Enrollment_Date__c = Date.today();
            update casesList;

            cas = casesList[0];
            u = [SELECT Id, ContactId FROM User WHERE Id =: cas.VTD1_Patient_User__c];
        }
    }

    @IsTest
    private static void getVisitTest(){
        List<VTD1_ProtocolVisit__c> protocolVisits = new List<VTD1_ProtocolVisit__c>{
                new VTD1_ProtocolVisit__c(
                        VTD1_Range__c = 4,
                        VTD1_VisitOffset__c = 2,
                        VTD1_VisitOffsetUnit__c = 'days',
                        VTD1_VisitDuration__c = '30 minutes',
                        VTD1_Onboarding_Type__c = 'N/A',
                        VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient',
                        VTD1_VisitNumber__c = 'o1'
                ),
                new VTD1_ProtocolVisit__c(
                        VTD1_Range__c = 4,
                        VTD1_VisitOffset__c = 2,
                        VTD1_VisitOffsetUnit__c = 'days',
                        VTD1_VisitDuration__c = '30 minutes',
                        VTD1_Onboarding_Type__c = 'N/A',
                        VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient',
                        VTD1_VisitNumber__c = 'v2'
                )
        };
        insert protocolVisits;

        List<VTD1_Actual_Visit__c> actualVisits = new List<VTD1_Actual_Visit__c>{
                new VTD1_Actual_Visit__c(
                        VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED,
                        VTR2_Televisit__c = false,
                        Unscheduled_Visits__c = cas.Id
                ),
                new VTD1_Actual_Visit__c(
                        VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED,
                        VTR2_Televisit__c = false,
                        VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today(),
                        Unscheduled_Visits__c = cas.Id
                ),
                new VTD1_Actual_Visit__c(
                        VTD1_Protocol_Visit__c = protocolVisits[1].Id,
                        VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
                        VTD1_Scheduled_Date_Time__c = Datetime.now() + 1,
                        VTR2_Televisit__c = false,
                        VTD1_Case__c = cas.Id
                )
                /*,
                new VTD1_Actual_Visit__c(
                        VTD1_Protocol_Visit__c = protocolVisits[0].Id,
                        VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
                        VTD1_Scheduled_Date_Time__c = Datetime.now() + 1,
                        VTR2_Televisit__c = false,
                        VTD1_Case__c = cas.Id
                ),
                new VTD1_Actual_Visit__c(
                        VTD1_Protocol_Visit__c = protocolVisits[1].Id,
                        VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
                        VTD1_Scheduled_Date_Time__c = Datetime.now(),
                        VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today(),
                        VTR2_Televisit__c = false,
                        Unscheduled_Visits__c = cas.Id
                )*/
        };
                //,
//                new VTD1_Actual_Visit__c(
//                        VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
//                        VTD1_Scheduled_Date_Time__c = Datetime.now(),
//                        VTR2_Televisit__c = false,
//                        Unscheduled_Visits__c = cas.Id
//                ),
//                new VTD1_Actual_Visit__c(
//                        VTD1_Protocol_Visit__c = protocolVisits[0].Id,
//                        VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
//                        VTD1_Scheduled_Date_Time__c = Datetime.now() + 1,
//                        VTR2_Televisit__c = false,
//                        VTD1_Case__c = cas.Id
//                ),
//                new VTD1_Actual_Visit__c(
//                        VTD1_Protocol_Visit__c = protocolVisits[1].Id,
//                        VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
//                        VTD1_Scheduled_Date_Time__c = Datetime.now() + 1,
//                        VTR2_Televisit__c = false,
//                        VTD1_Case__c = cas.Id
//                ),
//                new VTD1_Actual_Visit__c(
//                        VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED,
//                        VTR2_Televisit__c = false,
//                        Unscheduled_Visits__c = cas.Id
//                ),
//                new VTD1_Actual_Visit__c(
//                        VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED,
//                        VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today(),
//                        VTR2_Televisit__c = false,
//                        Unscheduled_Visits__c = cas.Id
//                ),
//                new VTD1_Actual_Visit__c(
//                        VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
//                        VTD1_Scheduled_Date_Time__c = Datetime.now() + 1,
//                        VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today(),
//                        VTR2_Televisit__c = false,
//                        Unscheduled_Visits__c = cas.Id
//                ),
//                new VTD1_Actual_Visit__c(
//                        VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
//                        VTD1_Scheduled_Date_Time__c = Datetime.now() + 1,
//                        VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today()+1,
//                        VTR2_Televisit__c = false,
//                        Unscheduled_Visits__c = cas.Id
//                ),
//                new VTD1_Actual_Visit__c(
//                        VTD1_Protocol_Visit__c = protocolVisits[1].Id,
//                        VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
//                        VTD1_Scheduled_Date_Time__c = Datetime.now(),
//                        VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today(),
//                        VTR2_Televisit__c = false,
//                        Unscheduled_Visits__c = cas.Id
//                ),
//                new VTD1_Actual_Visit__c(
//                        VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
//                        VTD1_Scheduled_Date_Time__c = Datetime.now(),
//                        VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today(),
//                        VTR2_Televisit__c = false,
//                        Unscheduled_Visits__c = cas.Id
//                )
//        };

        Test.startTest();
        System.runAs(u){
            insert actualVisits;
            VT_D1_UpNextVisit.ActualVisitWrapper visit = VT_D1_UpNextVisit.getVisit();
            System.assertNotEquals(null, visit);
        }
        Test.stopTest();
    }

    @IsTest
    private static void getMembersTest(){
        VT_R3_GlobalSharing.disableForTest = true;
        DomainObjects.VTD1_Actual_Visit_t actualVisit = new DomainObjects.VTD1_Actual_Visit_t()
                .setVTD1_Case(cas.Id)
                .setVTD1_Status(VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED);
        actualVisit.persist();
        Test.startTest();
        DomainObjects.Visit_Member_t member = new DomainObjects.Visit_Member_t()
                .setVTD1_External_Participant_Type('Patient')
                .addVTD1_Actual_Visit(actualVisit);
        member.persist();
            System.assertNotEquals(null, VT_D1_UpNextVisit.getMembers(actualVisit.Id));
            System.assertNotEquals(null, VT_D1_UpNextVisit.getAvailableDateTimes(actualVisit.Id));
        Test.stopTest();
    }
}