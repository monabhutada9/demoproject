/**

 * @author Andrey Pivovarov
 * @date 10/12/2020
 * @description rewrited process builder 'Pop Up Notifications for scheduled visits'

 */

public without sharing class VT_R5_ScheduleVisitsPopUpNotification extends Handler {

    private static Datetime dateTimeNow = System.now();
    private static VTR4_ExternalScheduler__c externalScheduler = VTR4_ExternalScheduler__c.getInstance();

    private static List<VT_D2_TNCatalogNotifications.ParamsHolder> notifications = new List<VT_D2_TNCatalogNotifications.ParamsHolder>();


    public override void onBeforeUpdate(Handler.TriggerContext context) {
        List<VTD1_Actual_Visit__c> actualVisits = (List<VTD1_Actual_Visit__c>) context.newList;
        Map<Id, VTD1_Actual_Visit__c> oldActualVisits = (Map<Id, VTD1_Actual_Visit__c>) context.oldMap; //update only

        Map<Id, Map<String, Boolean>> actualVisitNotificationFlow = new Map<Id, Map<String, Boolean>>();

        for (VTD1_Actual_Visit__c av : actualVisits) {
            VTD1_Actual_Visit__c oldAv = oldActualVisits.get(av.Id);
            if (av.VTD1_Status__c != oldAv.VTD1_Status__c
                    || av.VTD1_Scheduled_Date_Time__c != oldAv.VTD1_Scheduled_Date_Time__c) {
                if (av.VTD1_Status__c == 'Scheduled' && av.VTD1_Scheduled_Date_Time__c != null) {
                    if (av.VTD1_Status__c != oldAv.VTD1_Status__c && av.VTR3_Previous_Schedule_Date__c == null
                            && oldAv.VTD1_Scheduled_Date_Time__c == null) {

                        /**2 diamond*/

                        actualVisitNotificationFlow.put(av.Id, new Map<String, Boolean>{
                                'isTelevisit' => av.VTR2_Televisit__c,
                                'isLessThenIn1h'=> av.VTD1_Scheduled_Date_Time__c.getTime() - dateTimeNow.getTime() < 1 / 24,
                                'excludeExternals' => av.VTR2_Televisit__c && oldAv.VTD1_Status__c == 'Missed'
                        });
                    }
                    if (av.VTR3_Previous_Schedule_Date__c != null) {

                        /**3 diamond*/

                        actualVisitNotificationFlow.put(av.Id, new Map<String, Boolean>{
                                'isReschedule' => true,
                                'isTelevisit' => av.VTR2_Televisit__c,
                                'isLessThenIn1h'=> av.VTD1_Scheduled_Date_Time__c.getTime() - dateTimeNow.getTime() < 1 / 24,
                                'excludeExternals' => false
                        });
                    }
                }


            }
            /**4 diamond*/
            if (av.VTD1_Scheduled_Date_Time__c != null && av.VTD1_Scheduled_Date_Time__c != oldAv.VTD1_Scheduled_Date_Time__c) {
                av.VTR3_Previous_Schedule_Date__c = av.VTD1_Scheduled_Date_Time__c;
                av.VTR3_is_Rescheduling__c = oldAv.VTR3_Previous_Schedule_Date__c != null;
            }
        }
        if (!actualVisitNotificationFlow.isEmpty()) {
            visitNotificationsToPI_SCR(actualVisitNotificationFlow);
        }
    }

    public override void onAfterUpdate(Handler.TriggerContext context) {
        List<VTD1_Actual_Visit__c> actualVisits = (List<VTD1_Actual_Visit__c>) context.newList;
        Map<Id, VTD1_Actual_Visit__c> oldActualVisits = (Map<Id, VTD1_Actual_Visit__c>) context.oldMap;
        Map<Id, VTD1_Actual_Visit__c> consentReconsentVisitsMap = new Map<Id, VTD1_Actual_Visit__c>();
        Map<Id, VTD1_Actual_Visit__c> consentReconsentVisitsOldMap = new Map<Id, VTD1_Actual_Visit__c>();
        Map<Id, VTD1_Actual_Visit__c> visitEmailForPatient = new Map<Id, VTD1_Actual_Visit__c>();
        for (VTD1_Actual_Visit__c av : actualVisits) {
            VTD1_Actual_Visit__c oldAv = oldActualVisits.get(av.Id);
            if (av.VTD1_Status__c != oldAv.VTD1_Status__c
                    || av.VTD1_Scheduled_Date_Time__c != oldAv.VTD1_Scheduled_Date_Time__c) {


                /**5 diamond*/
                if ((av.VTD1_Status__c == 'Completed' && oldAv.VTD1_Status__c == 'Scheduled')
                        || (oldAv.VTD1_Status__c != 'To Be Scheduled' && av.VTD1_Status__c == 'To Be Scheduled'
                        && av.VTD1_Scheduled_Date_Time__c == null && av.VTD2_Common_Visit_Type__c == 'Study Team'
                        && av.VTD1_Onboarding_Type__c == 'N/A' && av.VTR2_ScheduleVisitTaskPatient__c != null)) {
                    visitEmailForPatient.put(av.Id, av);
                }
                /**7,8 diamond*/
                if (!externalScheduler.VTR4_RemindersIsActive__c && (av.VTD1_Onboarding_Type__c == 'Consent'
                        || av.VTD1_Unscheduled_Visit_Type__c == 'Consent' || av.VTD1_Unscheduled_Visit_Type__c == 'Re-Consent')
                        && av.VTD1_Scheduled_Date_Time__c != null) {
                    consentReconsentVisitsMap.put(av.Id, av);
                    consentReconsentVisitsOldMap.put(av.Id, oldAv);
                }
            }

            /**6 diamond - see in VT_R5_ActualVisitScheduledActionsHandler*/
            /**9 diamond - see in VT_R5_ActualVisitScheduledActionsHandler*/

        }
        if (!consentReconsentVisitsMap.isEmpty()) {
            runConsentReconsent(consentReconsentVisitsMap, consentReconsentVisitsOldMap);
        }

        if (!visitEmailForPatient.isEmpty()) {
            visitEmailNotificationsForPatient(visitEmailForPatient);
        }
        sendTaskNotificationAndEmail();

    }

    public override void onBeforeInsert(Handler.TriggerContext context) {
    }
    public override void onAfterInsert(Handler.TriggerContext context) {
        List<VTD1_Actual_Visit__c> actualVisits = (List<VTD1_Actual_Visit__c>) context.newList;
        Map<Id, Map<String, Boolean>> actualVisitNotificationFlow = new Map<Id, Map<String, Boolean>>();
        for (VTD1_Actual_Visit__c av : actualVisits) {

            /**2 diamond*/

            if (av.VTD1_Status__c == 'Scheduled' && av.VTD1_Scheduled_Date_Time__c != null
                    && av.VTR3_Previous_Schedule_Date__c == null) {
                actualVisitNotificationFlow.put(av.Id, new Map<String, Boolean>{
                        'isTelevisit' => av.VTR2_Televisit__c,
                        'isLessThenIn1h'=> av.VTD1_Scheduled_Date_Time__c.getTime() - dateTimeNow.getTime() < 1 / 24,
                        'excludeExternals' => false
                });
            }
        }
        if (!actualVisitNotificationFlow.isEmpty()) {
            visitNotificationsToPI_SCR(actualVisitNotificationFlow);
        }

        sendTaskNotificationAndEmail();
    }

    private static void sendTaskNotificationAndEmail() {
        if (!notifications.isEmpty()) {
            VT_D2_TNCatalogNotifications.generateNotification(notifications);
        }
        notifications.clear();
    }

    /**flow 'Visit Email Notifications for Patient'*/

    public static void visitEmailNotificationsForPatient(Map<Id, VTD1_Actual_Visit__c> visitEmailForPatient) {
        List<Visit_Member__c> visitMembers = [SELECT Id, VTD1_External_Participant_Type__c,
                                                    VTD1_Participant_User__c, VTD1_Actual_Visit__c
                                                FROM Visit_Member__c
                                                WHERE VTD1_Actual_Visit__c IN : visitEmailForPatient.keySet()];
        List<Visit_Member__c> visitIsReadyToBeScheduled = new List<Visit_Member__c>();
        List<Visit_Member__c> completedVisitForMembers = new List<Visit_Member__c>();


        for (Visit_Member__c vm : visitMembers) {
            if ((visitEmailForPatient.get(vm.VTD1_Actual_Visit__c).VTD1_Status__c == 'To Be Scheduled'
                    || visitEmailForPatient.get(vm.VTD1_Actual_Visit__c).VTD1_Status__c == 'Completed')
                    && (vm.VTD1_External_Participant_Type__c == 'Patient'
                    || vm.VTD1_External_Participant_Type__c == 'Caregiver')) {
                if (visitEmailForPatient.get(vm.VTD1_Actual_Visit__c).VTD1_Status__c == 'To Be Scheduled') {
                    if (vm.VTD1_External_Participant_Type__c == 'Caregiver') {

                        VT_D2_TNCatalogNotifications.ParamsHolder notification = new VT_D2_TNCatalogNotifications.ParamsHolder();
                        notification.sourceId = vm.VTD1_Actual_Visit__c;
                        notification.tnCode = 'N008';
                        notification.Receiver = vm.VTD1_Participant_User__c;
                        notifications.add(notification);

                    }
                    visitIsReadyToBeScheduled.add(vm);
                } else {
                    completedVisitForMembers.add(vm);
                }
            }
        }
        if (!visitIsReadyToBeScheduled.isEmpty() || !completedVisitForMembers.isEmpty()) {
            sendEmailNotificationsVisitMembers(new Map<String, List<Visit_Member__c>>{
                    'VisitIsReadyToBeScheduledPatient' => visitIsReadyToBeScheduled,
                    'VTR4_Visit_Compliedted_Patient' => completedVisitForMembers
            });
        }


    }

    public static void sendEmailNotificationsVisitMembers(Map<String, List<Visit_Member__c>> visitMembersEmailTemplateMap) {
        List<VT_R3_EmailsSender.ParamsHolder> paramsHolders = new List<VT_R3_EmailsSender.ParamsHolder>();
        for (String emailTemplate : visitMembersEmailTemplateMap.keySet()) {
            for (Visit_Member__c vm : visitMembersEmailTemplateMap.get(emailTemplate)) {
                VT_R3_EmailsSender.ParamsHolder emailParamsHolder = new VT_R3_EmailsSender.ParamsHolder();
                emailParamsHolder.templateDevName = emailTemplate;
                emailParamsHolder.relatedToId = vm.Id;
                if (vm.VTD1_Participant_User__c == null) {
                    emailParamsHolder.recipientId = vm.Id;
                    emailParamsHolder.toAddressesOnly = true;
                    emailParamsHolder.toAddresses = vm.VTD1_External_Participant_Email__c;
                } else {
                    emailParamsHolder.recipientId = vm.VTD1_Participant_User__c;
                }
                paramsHolders.add(emailParamsHolder);
            }
        }
        if (!Test.isRunningTest()) VT_R3_EmailsSender.sendEmails(paramsHolders);
    }


    /**rewrited flow 'Visit Notifications to PI/SCR'*/

    public static void visitNotificationsToPI_SCR(Map<Id, Map<String, Boolean>> actualVisitNotificationFlow) {
        Set<Id> actualVisitIds = actualVisitNotificationFlow.keySet();
        String query = 'SELECT Id, (SELECT Id, VTD1_External_Participant_Type__c, VTD1_Participant_User__c, VTD1_External_Participant_Email__c' +
                ' FROM Visit_Members__r)' +
                ' FROM VTD1_Actual_Visit__c' +
                ' WHERE Id IN : actualVisitIds';
        Map<Id, VTD1_Actual_Visit__c> actualVisits = new Map<Id, VTD1_Actual_Visit__c>((List<VTD1_Actual_Visit__c>) Database.query(query));


        Map<String, List<Visit_Member__c>> emailTemplateVisitMembersMap = new Map<String, List<Visit_Member__c>> {
                'VTR3_Visit_Scheduled_External' => new List<Visit_Member__c>(),
                'VTR3_Visit_Scheduled_Ext_Less_1Hour_TV' => new List<Visit_Member__c>(),
                'VTR3_Visit_Scheduled_External_TV' => new List<Visit_Member__c>(),
                'VTR3_Visit_Has_Been_Rescheduled_Ext' => new List<Visit_Member__c>(),
                'VTR3_Visit_Rescheduled_Ext_Less_1Hour_TV' => new List<Visit_Member__c>(),
                'VTR3_Visit_Has_Been_Rescheduled_Ext_TV' => new List<Visit_Member__c>(),
                'VTR4_Visit_Has_Been_Rescheduled' => new List<Visit_Member__c>(),
                'VTR4_Study_Visit_Scheduled' => new List<Visit_Member__c>(),
                'VTD2_PIEmailnotificationwhenvisithasbeenRescheduled_NEW' => new List<Visit_Member__c>(),
                'VTD2_PIEmailnotificationwhenvisithasbeenScheduled_NEW' => new List<Visit_Member__c>()
                };
        for (Id avId : actualVisits.keySet()) {
            Boolean excludeExternals = false;
            Boolean onlyExternals = false;
            Boolean isReschedule = false;
            Boolean isTelevisit = false;
            Boolean isLessThenIn1h = false;
            Map<String, Boolean> actualVisitFlowParams = actualVisitNotificationFlow.get(avId);
            if (actualVisitFlowParams != null) {


                excludeExternals = actualVisitFlowParams.get('excludeExternals') == true;
                onlyExternals = actualVisitFlowParams.get('onlyExternals') == true;
                isReschedule = actualVisitFlowParams.get('isReschedule') == true;
                isTelevisit = actualVisitFlowParams.get('isTelevisit') == true;
                isLessThenIn1h = actualVisitFlowParams.get('isLessThenIn1h') == true;


            }
            String tnNotificationCodePTCG;
            String tnNotificationCodePGRE;
            String tnNotificationCodePISCR;
            String emailTemplatePTCG;
            String emailTemplatePI;
            String emailTemplateExternal;
            if (isReschedule) {
                tnNotificationCodePTCG = 'N519';
                tnNotificationCodePGRE = 'P503';
                tnNotificationCodePISCR = 'N527';
                emailTemplatePTCG = 'VTR4_Visit_Has_Been_Rescheduled';
                emailTemplatePI = 'VTD2_PIEmailnotificationwhenvisithasbeenRescheduled_NEW';
                if (isTelevisit) {
                    emailTemplateExternal = isLessThenIn1h ? 'VTR3_Visit_Rescheduled_Ext_Less_1Hour_TV' :
                            'VTR3_Visit_Has_Been_Rescheduled_Ext_TV';
                } else {
                    emailTemplateExternal = 'VTR3_Visit_Has_Been_Rescheduled_Ext';
                }
            } else {
                tnNotificationCodePTCG = 'N505';
                tnNotificationCodePGRE = 'P502';
                tnNotificationCodePISCR = 'N504';
                emailTemplatePTCG = 'VTR4_Study_Visit_Scheduled';
                emailTemplatePI = 'VTD2_PIEmailnotificationwhenvisithasbeenScheduled_NEW';
                if (isTelevisit) {
                    emailTemplateExternal = isLessThenIn1h ? 'VTR3_Visit_Scheduled_Ext_Less_1Hour_TV' :
                            'VTR3_Visit_Scheduled_External_TV';
                } else {
                    emailTemplateExternal = 'VTR3_Visit_Scheduled_External';
                }
            }
            for (Visit_Member__c vm : actualVisits.get(avId).Visit_Members__r) {
                if (vm.VTD1_Participant_User__c == null && !excludeExternals && isReschedule) { //external
                    emailTemplateVisitMembersMap.get(emailTemplateExternal).add(vm);
                } else if (!onlyExternals) {
                    if (vm.VTD1_External_Participant_Type__c == 'Patient'
                            || vm.VTD1_External_Participant_Type__c == 'Caregiver') { //PT/CG

                        notifications.add(new VT_D2_TNCatalogNotifications.ParamsHolder(vm.Id, tnNotificationCodePTCG));
                        emailTemplateVisitMembersMap.get(emailTemplatePTCG).add(vm);
                    } else if (vm.VTD1_External_Participant_Type__c == 'Patient Guide'
                            || vm.VTD1_External_Participant_Type__c == 'Recruitment Expert') { //PG or RE
                        notifications.add(new VT_D2_TNCatalogNotifications.ParamsHolder(vm.Id, tnNotificationCodePGRE));

                    } else if (vm.VTD1_External_Participant_Type__c == 'Primary Investigator'
                            || vm.VTD1_External_Participant_Type__c == 'Site Coordinator') { //PI and SCR
                        if (vm.VTD1_External_Participant_Type__c == 'Primary Investigator') {
                            emailTemplateVisitMembersMap.get(emailTemplatePI).add(vm);
                        }
                        notifications.add(new VT_D2_TNCatalogNotifications.ParamsHolder(vm.Id, tnNotificationCodePISCR));
                    }
                }
            }
        }
        if (!emailTemplateVisitMembersMap.isEmpty()) {
            sendEmailNotificationsVisitMembers(emailTemplateVisitMembersMap);
        }


    }

    private static void runConsentReconsent(Map<Id, VTD1_Actual_Visit__c> actualVisitsMap, Map<Id, VTD1_Actual_Visit__c> oldActualVisits) {
        Map<Id, VTD1_Actual_Visit__c> actualVisitMap = new Map<Id, VTD1_Actual_Visit__c>([
                SELECT Unscheduled_Visits__r.AccountId, VTD1_Case__r.AccountId
                FROM VTD1_Actual_Visit__c
                WHERE Id IN :actualVisitsMap.keySet()
        ]);
        List<VT_R5_RunConsentVisit.ParamsVisit> consentVisitParams = new List<VT_R5_RunConsentVisit.ParamsVisit>();
        for (VTD1_Actual_Visit__c av : actualVisitsMap.values()) {
            VTD1_Actual_Visit__c oldAv = oldActualVisits.get(av.Id);
            /**7 and 8 diamond*/
            if (av.VTD1_Status__c != oldAv.VTD1_Status__c && oldAv.VTD1_Status__c == 'Scheduled'
                    || av.VTD1_Status__c == 'Scheduled' && av.VTR3_Previous_Schedule_Date__c != null) {
                VT_R5_RunConsentVisit.ParamsVisit paramsVisit = new VT_R5_RunConsentVisit.ParamsVisit();
                paramsVisit.scheduledDate = av.VTD1_Scheduled_Date_Time__c;
                paramsVisit.accountId =
                        av.VTD1_Protocol_Visit__c == null ? actualVisitMap.get(av.Id).Unscheduled_Visits__r.AccountId
                                : actualVisitMap.get(av.Id).VTD1_Case__r.AccountId;
                paramsVisit.visitId = av.Id;
                consentVisitParams.add(paramsVisit);
            }
        }
        if (!consentVisitParams.isEmpty()) {
            VT_R5_RunConsentVisit.sendSignal(consentVisitParams);
        }
    }
}