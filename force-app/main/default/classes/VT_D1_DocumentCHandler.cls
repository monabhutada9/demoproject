public with sharing class VT_D1_DocumentCHandler {
    public static final Set<Id> MEDICAL_RECORDTYPES = new Set<Id>{
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD,
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON,
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_ARCHIVED,
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_REJECTED,
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_RELEASE_FORM
    };

    public static final String MULTIPLE_DOCS_CRA_TNCODE = 'N513';
    public static final String SINGLE_DOC_CRA_TNCODE = 'N514';
    public static final String APPROVED_DOCS_PI_TNCODE = 'N515';
    public static final String REJECTED_DOCS_PI_TNCODE = 'N516';

    public static final Map<String, String> piEmailTemplatesMap = new Map<String, String>{
        VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED => 'VT_R2_Medical_Records_Approved_for_PI',
        VT_R4_ConstantsHelper_Documents.DOCUMENT_REJECTED => 'VT_R2_Medical_Records_Rejected_for_PI'
    };

    public static final Id MEDICAL_RECORD_TYPE_ID = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD;
    public static final Id VISIT_DOC_RECORD_TYPE_ID = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT;

    private static Map <Id, String> docIdToStatusMap = new Map<Id, String>();

    public void onBeforeInsert(List<VTD1_Document__c> docs) {
        Set<Id> regDocSiteIdsSet = new Set<Id>();
        Set<Id> regBinderIdsSet = new Set<Id>();
        for (VTD1_Document__c doc : docs) {
            if (doc.VTD1_Site__c != null) {
                regDocSiteIdsSet.add(doc.VTD1_Site__c);
            }
            if (doc.VTD1_Regulatory_Binder__c != null) {
                regBinderIdsSet.add(doc.VTD1_Regulatory_Binder__c);
            }
        }
        Map<Id,Virtual_Site__c> siteMap = new Map<Id,Virtual_Site__c>([SELECT Signature_Type__c FROM Virtual_Site__c WHERE Id IN :regDocSiteIdsSet]);
        Map<Id,VTD1_Regulatory_Binder__c> regulatoryBinderMap = new Map<Id,VTD1_Regulatory_Binder__c>([SELECT VTD1_Regulatory_Document__c, VTD1_Regulatory_Document__r.VTD1_Signature_Type__c FROM VTD1_Regulatory_Binder__c WHERE Id IN :regBinderIdsSet]);
        Set<Id> targetRecordTypeIds = new Set<Id>{
                MEDICAL_RECORD_TYPE_ID,
                VISIT_DOC_RECORD_TYPE_ID
        };
        Map<Id, List<VTD1_Document__c>> caseIdtoDocsMap = new Map<Id, List<VTD1_Document__c>>();
        for (VTD1_Document__c doc : docs) {
            if (doc.VTD1_Clinical_Study_Membership__c != null && targetRecordTypeIds.contains(doc.RecordTypeId)) {
                if (!caseIdtoDocsMap.containsKey(doc.VTD1_Clinical_Study_Membership__c)) {
                    caseIdtoDocsMap.put(doc.VTD1_Clinical_Study_Membership__c, new List<VTD1_Document__c>());
                }
                caseIdtoDocsMap.get(doc.VTD1_Clinical_Study_Membership__c).add(doc);
            }
            if (doc.VTD1_Site__c != null && siteMap.get(doc.VTD1_Site__c).Signature_Type__c !=  null){
                doc.VTR4_Signature_Type__c = siteMap.get(doc.VTD1_Site__c).Signature_Type__c;
            }else if(doc.VTD1_Regulatory_Binder__c != null && regulatoryBinderMap.get(doc.VTD1_Regulatory_Binder__c).VTD1_Regulatory_Document__c != null){
                doc.VTR4_Signature_Type__c =  regulatoryBinderMap.get(doc.VTD1_Regulatory_Binder__c).VTD1_Regulatory_Document__r.VTD1_Signature_Type__c;
            }
        }
        if (!caseIdtoDocsMap.isEmpty()) {
            for (Case cas : [SELECT Id, VTD1_Primary_PG__c, VTD1_PI_user__c, VTR2_SiteCoordinator__c FROM Case WHERE Id IN :caseIdtoDocsMap.keySet()]) {
                for (VTD1_Document__c doc : caseIdtoDocsMap.get(cas.Id)) {
                    doc.VTD1_PG_Approver__c = cas.VTD1_Primary_PG__c;
                    doc.VTD1_PI_Approver__c = cas.VTD1_PI_user__c;
                    doc.VTR2_SCR_Approver__c = cas.VTR2_SiteCoordinator__c;
                }
            }
        } 
        validateWetSignatureDate(docs, null);
        VT_R4_DocumentService.populateStudyAndVirtualSite(VT_R4_DocumentService.queryDocumentCaseAndSiteRecords(docs));
    }
    public void onBeforeUpdate(List<VTD1_Document__c> docs, Map<Id, VTD1_Document__c> oldMap) {
        validateWetSignatureDate(docs, oldMap);
        validateCraReviewCompletedCheckbox(docs, oldMap);
    }
    //replaced with pb
    /*public void onBeforeUpdate(List<VTD1_Document__c> docs, Map<Id, VTD1_Document__c> oldMap) {
        List<VTD1_Document__c> docList = new List<VTD1_Document__c>();
        for (VTD1_Document__c doc : docs) {
            if (doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT
                && oldMap.get(doc.Id).VTR3_Category__c == 'Uncategorized'
                && doc.VTR3_Category__c == 'Medical Record') {
                docList.add(doc);
            }
        }
        if (!docList.isEmpty()) {
            visitDocToMedRecRecordTypeChange(docList);
        }
    }*/

    public void processReview(List<VTD1_Document__c> newList, Map<Id, VTD1_Document__c> oldMap) {
        for (VTD1_Document__c doc : newList) {
            if (doc.VTR4_ReviewCompleted__c != oldMap.get(doc.Id).VTR4_ReviewCompleted__c) {
                if (doc.VTR4_ReviewCompleted__c) {
                    doc.VTR4_DateReviewed__c = Datetime.now();
                    doc.VTR4_ReviewedBy__c = UserInfo.getUserId();
                } else {
                    doc.VTR4_DateReviewed__c = null;
                    doc.VTR4_ReviewedBy__c = null;
                }
            }
        }
    }

    public void validateWetSignatureDate(List<VTD1_Document__c> docs, Map<Id, VTD1_Document__c> oldMap) {
        /*   Set<Id> certificationIds = new Set<Id>();
        for(VTD1_Document__c document: docs){
            if (document.VTD2_Certification__c != null) {
                certificationIds.add(document.VTD2_Certification__c);
            }
        }
        Map<Id,VTD1_Document__c> certificationMap = new Map<Id,VTD1_Document__c>(
                                [SELECT Id, VTD1_Signature_Date__c, VTD1_Status__c
                                 FROM VTD1_Document__c
                                 WHERE RecordTypeId = :VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON
                                 AND Id IN :certificationIds]
                                );*/
        Map<Id, VTD1_Document__c> certificationMap = VT_D1_ContentVersionProcessHandler.getCertDocsWithSignatureDate(docs);
        for (VTD1_Document__c document : docs) {
            if (document.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT
                && document.VTD2_PI_Signature_required__c
                && document.VTR4_Signature_Type__c == 'Wet'
                && document.VTD1_Signature_Date__c != null
                && (oldMap == null || document.VTD1_Signature_Date__c != oldMap.get(document.Id).VTD1_Signature_Date__c)
                    && certificationMap.get(document.Id) == null
               )
            {
                 document.VTD1_Signature_Date__c.addError(String.format(Label.VTR4_SignatureDateCannotBeEnteredUntilWetSignatureCertified, new List<String>{
                            document.Name
                    }));


            }
        }
    }
    public void validateCraReviewCompletedCheckbox(List<VTD1_Document__c> docs, Map<Id, VTD1_Document__c> oldMap) {
        Set<String> profilesToSelect = new Set<String> {
            VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME
        };
        Set<Id> forbiddenProfileIds = new Map<Id, Profile>([SELECT Id FROM Profile WHERE Name IN :profilesToSelect]).keySet();
        Set<String> allowedDocumentStatuses = new Set<String> {
            VT_D1_ConstantsHelper.DOCUMENT_STATUS_APPROVED,
            VT_D1_ConstantsHelper.DOCUMENT_STATUS_CERTIFIED,
            VT_D1_ConstantsHelper.DOCUMENT_STATUS_NO_CERTIFICATION_REQUIRED
        };
        for (VTD1_Document__c document : docs) {
            Boolean isReviewCompletedChanged = document.VTR4_ReviewCompleted__c != oldMap.get(document.Id).VTR4_ReviewCompleted__c;
            if (isReviewCompletedChanged
                    && document.VTR4_ReviewCompleted__c
                    && forbiddenProfileIds.contains(UserInfo.getProfileId())
                    && !allowedDocumentStatuses.contains(document.VTD1_Status__c)) {
                document.addError(Label.VTR4_ErrorDocumentReviewCheckboxChecked);
            }
        }
    }
    public void onBeforeDelete(List<VTD1_Document__c> recs) {
        VT_R5_DeletedRecordHandler.createDeletedRecords(recs);
        List<VTD1_Document__c> toValidate = new List<VTD1_Document__c>();
        for (VTD1_Document__c item : recs) {
            if (! item.VTD1_Uploading_Not_Finished__c) {
                toValidate.add(item);
            }
        }
        if (! toValidate.isEmpty()) { VT_D1_LegalHoldDeleteValidator.validateDeletion(toValidate); }
    }

    public void onAfterInsert(List<VTD1_Document__c> recs) {
        VT_R3_GlobalSharing.doSharing(recs);
    }

    public void onAfterUpdate(List<VTD1_Document__c> recs, Map<Id, VTD1_Document__c> oldMap) {
        updateRegDocSignatureDate(recs, oldMap);
        createMilestones(recs, oldMap);
        processLifecycleStatuses(recs, oldMap);
        processCascadeOnSite(recs, oldMap);
        recalcSharingIfNeeded(recs, oldMap);
        sendNotifications(recs, oldMap);
        checkAppliedAllPatients(recs, oldMap);
    }

    //replaced with pb
    /*private void visitDocToMedRecRecordTypeChange (List<VTD1_Document__c> docList) {
        //Change record type to Medical Record if Visit Document category is changed from Uncategorized to Medical Record
        for (VTD1_Document__c doc : docList) {
            doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD;
        }
    }*/

    private void updateRegDocSignatureDate(List<VTD1_Document__c> recs, Map<Id, VTD1_Document__c> oldMap){
        Set<Id> regDocIds = new Set<Id>();
        List<VTD1_Document__c> certificationsToHandle = new List<VTD1_Document__c>();
        for(VTD1_Document__c certCopy : recs){
            if (certCopy.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON
                && certCopy.VTD2_Associated_Medical_Record_id__c != null
                    && certCopy.VTD1_Status__c == 'Certified'
                    && certCopy.VTD1_Signature_Date__c != null)
            {
                regDocIds.add(certCopy.VTD2_Associated_Medical_Record_id__c);
                certificationsToHandle.add(certCopy);
            }
        }
        if(certificationsToHandle.isEmpty()) return;

        Map<Id,VTD1_Document__c> regDocMap = new Map<Id,VTD1_Document__c>(
                [SELECT Id, Name, VTD1_Version__c, VTD1_Status__c, VTD1_Lifecycle_State__c, VTD1_Current_Workflow__c, VTD1_Document_Locked__c,
                        VTD1_Study_RSU__c, VTD1_Study_IRB_Flag__c, VTD1_Site_RSU__c, VTD1_Site_IRB_Flag__c, VTD1_ISF__c, VTD1_TMF__c,
                        VTD1_ISF_Complete__c, VTD1_TMF_Complete__c, VTD1_Site_RSU_Complete__c, VTD1_Study_RSU_Complete__c,
                        VTD2_PI_Signature_required__c, VTD1_Signature_Date__c, VTD1_Regulatory_Document_Type__c, RecordTypeId,
                        VTD1_Site__c, VTD1_Protocol_Amendment__c, VTD2_EAFStatus__c, VTR4_EDP_Completed__c, VTR4_Signature_Type__c
                FROM VTD1_Document__c
                WHERE Id IN :regDocIds
                AND RecordTypeId = :VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT
                AND VTR4_Signature_Type__c = 'Wet']
        );
        System.debug('regDocMap ' + regDocMap);
        List<VTD1_Document__c> regDocsToUpdate = new List<VTD1_Document__c>();
        for (VTD1_Document__c certCopy : certificationsToHandle) {
            VTD1_Document__c old = oldMap.get(certCopy.Id);
            VTD1_Document__c regDoc = regDocMap.get(certCopy.VTD2_Associated_Medical_Record_id__c);
            system.debug('regDoc '+regDoc);
            if ( certCopy.VTD1_Signature_Date__c != old.VTD1_Signature_Date__c
                    && old.VTD1_Signature_Date__c == null
                    && regDoc != null            )
            {
                if (regDoc.VTD1_Signature_Date__c == null) {
                    regDoc.VTD1_Signature_Date__c = certCopy.VTD1_Signature_Date__c;
                    VT_D1_ContentVersionProcessHandler.handleFlags(regDoc);
                }
                regDoc.VTD2_Sent_for_Signature__c = false;

                System.debug('certCopy = ' + certCopy);
                regDocsToUpdate.add(regDoc);
            }
        }
        System.debug('regDocsToUpdate ' + regDocsToUpdate);
        update regDocsToUpdate;
    }

    private void createMilestones(List<VTD1_Document__c> recs, Map<Id, VTD1_Document__c> oldMap) {
        VT_D1_MilestoneCreator milestoneCreator = new VT_D1_MilestoneCreator();
        for (VTD1_Document__c item : recs) {
            VTD1_Document__c old = oldMap.get(item.Id);
            if (VT_D1_MilestoneHelper.isApprovedEssentialDoc(item) && !VT_D1_MilestoneHelper.isApprovedEssentialDoc(old)) {
                milestoneCreator.addPotentialSiteMilestone(item.VTD1_Site__c, VT_R4_ConstantsHelper_AccountContactCase.SITE_VIRTUAL_SITE_STARTUP_COMPLETION);
            }
        }
        milestoneCreator.createMilestones();
    }

    private void processLifecycleStatuses(List<VTD1_Document__c> documents, Map<Id, VTD1_Document__c> oldMap) {
        System.debug('processLifecycleStatuses ' + documents);
        Set <Id> docIds = new Set<Id>();
        Map <Id, VTD1_Document__c> docIdToDocumentMap = new Map<Id, VTD1_Document__c>();
        for (VTD1_Document__c document : documents) {
            if ((document.VTD1_TMF_Complete__c && document.VTD1_TMF_Complete__c != oldMap.get(document.Id).VTD1_TMF_Complete__c) ||
                (!document.VTD1_TMF__c && document.VTD1_Status__c == 'Study RSU Complete' && document.VTD1_Status__c != oldMap.get(document.Id).VTD1_Status__c) ||
                (document.VTD1_ISF__c && !document.VTD1_TMF__c && document.VTD1_Status__c == 'ISF Complete' && document.VTD1_Status__c != oldMap.get(document.Id).VTD1_Status__c)) {
                System.debug('processLifecycleStatuses1');
                String status = docIdToStatusMap.get(document.Id);
                if (status == 'Approved')
                    continue;
                docIdToStatusMap.put(document.Id, 'Approved');
                docIds.add(document.Id);
            } else if ((document.VTD1_Lifecycle_State__c == 'Draft' && oldMap.get(document.Id).VTD1_Lifecycle_State__c == 'Approved') ||
                (document.VTD1_Status__c != oldMap.get(document.Id).VTD1_Status__c &&
                    (document.VTD1_Status__c == 'Study RSU Started' ||
                        document.VTD1_Status__c == 'Site RSU Not Started' ||
                        document.VTD1_Status__c == 'ISF Started' ||
                        document.VTD1_Status__c == 'TMF Started'))) {
                System.debug('processLifecycleStatuses2');

                String status = docIdToStatusMap.get(document.Id);
                if (status == 'In–Progress')
                    continue;
                docIdToStatusMap.put(document.Id, 'In–Progress');
                docIds.add(document.Id);
                docIdToDocumentMap.put(document.Id, document);
            }
        }
        if (docIds.isEmpty())
            return;

        System.debug('docIds = ' + docIds);

        List <VTD1_Document__c> documentsInProgress = [select Id, VTD1_Lifecycle_State__c, VTD1_Current_Workflow__c from VTD1_Document__c where Id in : docIdToDocumentMap.keySet()];
        for (VTD1_Document__c document : documentsInProgress) {
            docIdToDocumentMap.put(document.Id, document);
        }

        List <ContentDocumentLink> contentDocumentLinks =
        [select ContentDocumentId, LinkedEntityId from ContentDocumentLink where LinkedEntityId in : docIds];

        Map <Id, Id> entityIdToContentDocIdMap = new Map<Id, Id>();
        for (ContentDocumentLink contentDocumentLink : contentDocumentLinks) {
            entityIdToContentDocIdMap.put(contentDocumentLink.LinkedEntityId, contentDocumentLink.ContentDocumentId);
        }
        Map <Id, ContentVersion> contentDocIdToContentVersionMap = new Map<Id, ContentVersion>();
        List <ContentVersion> contentVersions = [select VTD1_CompoundVersionNumber__c, ContentDocumentId, VTD1_Lifecycle_State__c
        from ContentVersion where ContentDocumentId in : entityIdToContentDocIdMap.values() and IsLatest = true];
        System.debug('contentVersions = ' + contentVersions);
        for (ContentVersion contentVersion : contentVersions) {
            contentDocIdToContentVersionMap.put(contentVersion.ContentDocumentId, contentVersion);
        }

        for (Id docId : docIdToStatusMap.keySet()) {
            Id contentDocId = entityIdToContentDocIdMap.get(docId);
            ContentVersion contentVersion = contentDocIdToContentVersionMap.get(contentDocId);
            if (contentVersion == null)
                continue;
            contentVersion.VTD1_Lifecycle_State__c = docIdToStatusMap.get(docId);
            VTD1_Document__c document = docIdToDocumentMap.get(docId);
            if (document != null) {
                document.VTD1_Lifecycle_State__c = contentVersion.VTD1_Lifecycle_State__c;
                List <String> parts = new List<String>();
                parts.add(contentVersion.VTD1_CompoundVersionNumber__c);
                parts.add('Current');
                parts.add(contentVersion.VTD1_Lifecycle_State__c);
                parts.add(document.VTD1_Current_Workflow__c);
                contentVersion.VTD1_FullVersion__c = String.join(parts, ',');
            }
        }

        if (!contentVersions.isEmpty()) {
            update contentVersions;
        }
        if (!docIdToDocumentMap.isEmpty())
            update docIdToDocumentMap.values();
    }

    public static void processCascadeOnSite(List<VTD1_Document__c> documents, Map<Id, VTD1_Document__c> oldMap) {
        //Map <Id, String> studyIdToLifecycleStateMap = new Map<Id, String>();
        Set <Id> studyIdWithLifecycleApproved = new Set<Id>();
        Set <Id> studyIdWithStatusComplete = new Set<Id>();
        Set <Id> excludeDocIds = new Set<Id>();
        for (VTD1_Document__c document : documents) {
            /*if (document.VTD1_Site__c != null && document.VTD1_Regulatory_Document_Type__c == 'EDPRC' && document.VTD1_Lifecycle_State__c != oldMap.get(document.Id).VTD1_Lifecycle_State__c &&
                    (document.VTD1_Lifecycle_State__c == 'Approved' || document.VTD1_Lifecycle_State__c == 'Site RSU Complete')) {
                    studyIdToLifecycleStateMap.put(document.VTD1_Site__c, document.VTD1_Lifecycle_State__c);
            }*/
            if (document.VTD1_Site__c != null && document.VTD1_Regulatory_Document_Type__c == 'EDPRC') {
                if (document.VTD1_Lifecycle_State__c == 'Approved' && document.VTD1_Lifecycle_State__c != oldMap.get(document.Id).VTD1_Lifecycle_State__c) {
                    studyIdWithLifecycleApproved.add(document.VTD1_Site__c);
                    excludeDocIds.add(document.Id);
                } else if (document.VTD1_Status__c == 'Site RSU Complete' && document.VTD1_Status__c != oldMap.get(document.Id).VTD1_Status__c) {
                    studyIdWithStatusComplete.add(document.VTD1_Site__c);
                    excludeDocIds.add(document.Id);
                }
            }
        }
        System.debug('studyIdWithLifecycleApproved = ' + studyIdWithLifecycleApproved);
        System.debug('studyIdWithStatusComplete = ' + studyIdWithStatusComplete);
        if (studyIdWithLifecycleApproved.size() + studyIdWithStatusComplete.size() > 0) {
            processCascadeOnSiteFuture(studyIdWithLifecycleApproved, studyIdWithStatusComplete, excludeDocIds);
        }
    }
    @future
    private static void processCascadeOnSiteFuture(Set <Id> studyIdWithLifecycleApproved, Set <Id> studyIdWithStatusComplete, Set <Id> excludeDocIds) {
        System.debug('studyIdWithLifecycleApproved = ' + studyIdWithLifecycleApproved);
        System.debug('studyIdWithStatusComplete = ' + studyIdWithStatusComplete);
        List <VTD1_Document__c> documents = [select VTD1_Lifecycle_State__c, VTD1_Status__c, VTD1_Site__c, VTD1_Current_Workflow__c from
            VTD1_Document__c where Id not in : excludeDocIds and ((VTD1_Site__c in : studyIdWithLifecycleApproved and VTD1_Lifecycle_State__c != 'Approved' and VTD1_Site_RSU__c = True and VTD1_ISF__c = False and VTD1_TMF__c = False) or
        (VTD1_Site__c in : studyIdWithStatusComplete and VTD1_Status__c != 'Site RSU Complete' and VTD1_Site_RSU__c = True))];

        for (VTD1_Document__c document : documents) {
            if (studyIdWithLifecycleApproved.contains(document.VTD1_Site__c)) {
                document.VTD1_Lifecycle_State__c = 'Approved';
            } else if (studyIdWithStatusComplete.contains(document.VTD1_Site__c)) {
                document.VTD1_Status__c = 'Site RSU Complete';
            }
        }

        for (VTD1_Document__c document : documents) {
            System.debug('processLifecycleStateFuture document = ' + document);
        }
        if (!documents.isEmpty())
            System.debug('DocumentCHandler 371: Number of SOQL queries: ' + Limits.getQueries());
            update documents;
    }

    private void recalcSharingIfNeeded(List<VTD1_Document__c> recs, Map<Id, VTD1_Document__c> oldMap) {
        List<VTD1_Document__c> toRecalc = new List<VTD1_Document__c>();
        for (VTD1_Document__c item : recs) {
            VTD1_Document__c old = oldMap.get(item.Id);
            if (item.RecordTypeId != old.RecordTypeId ||
                item.VTD1_Clinical_Study_Membership__c != old.VTD1_Clinical_Study_Membership__c ||
                item.VTD1_Study__c != old.VTD1_Study__c)
            {
                toRecalc.add(item);
            } else if (MEDICAL_RECORDTYPES.contains(item.RecordTypeId)) {
                if ((item.VTD1_Status__c == VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED) != (old.VTD1_Status__c == VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED)) {
                    toRecalc.add(item);
                } else if (item.VTD2_Redacted__c != old.VTD2_Redacted__c) {
                    toRecalc.add(item);
                }
            }
        }

        if (! toRecalc.isEmpty()) { VT_R3_GlobalSharing.doSharing(toRecalc); }
    }

    private static void sendNotifications(List<VTD1_Document__c> documents, Map<Id, VTD1_Document__c> oldMap) {
        Map<Id, Map<String, List<VTD1_Document__c>>> caseIdToDocsMap = new Map<Id, Map<String, List<VTD1_Document__c>>>();
        Map<Id, List<VTD1_Document__c>> siteDocumentMap = new Map<Id, List<VTD1_Document__c>>();
        for (VTD1_Document__c doc : documents) {
            if (doc.VTD1_Clinical_Study_Membership__c != null && MEDICAL_RECORDTYPES.contains(doc.RecordTypeId) && doc.VTD1_Status__c != oldMap.get(doc.Id).VTD1_Status__c) {
                if (!caseIdToDocsMap.containsKey(doc.VTD1_Clinical_Study_Membership__c)) {
                    caseIdToDocsMap.put(
                        doc.VTD1_Clinical_Study_Membership__c,
                        new Map<String, List<VTD1_Document__c>>{
                            VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED => new List<VTD1_Document__c>(),
                            VT_R4_ConstantsHelper_Documents.DOCUMENT_REJECTED => new List<VTD1_Document__c>()
                        });
                }
                if (doc.VTD1_Status__c == VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED) {
                    caseIdToDocsMap.get(doc.VTD1_Clinical_Study_Membership__c).get(VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED).add(doc);
                } else if (doc.VTD1_Status__c == VT_R4_ConstantsHelper_Documents.DOCUMENT_REJECTED) {
                    caseIdToDocsMap.get(doc.VTD1_Clinical_Study_Membership__c).get(VT_R4_ConstantsHelper_Documents.DOCUMENT_REJECTED).add(doc);
                }
            }
            if (isPASignaturePage(doc) && doc.VTR4_Amendment_Setup_Completed__c && doc.VTR4_IRB_Approved__c
                    && doc.VTD2_EAFStatus__c == VT_R4_ConstantsHelper_Documents.DOCUMENT_EAF_STATUS_COMPLETED
                    && (oldMap.get(doc.Id).VTD2_EAFStatus__c != VT_R4_ConstantsHelper_Documents.DOCUMENT_EAF_STATUS_COMPLETED
                        || !oldMap.get(doc.Id).VTR4_IRB_Approved__c)) {
                List<VTD1_Document__c> docList = siteDocumentMap.get(doc.VTD1_Site__c);
                if (docList == null) {
                    docList = new List<VTD1_Document__c> ();
                }
                docList.add(doc);
                siteDocumentMap.put(doc.VTD1_Site__c, docList);
            }
        }
        if (!caseIdToDocsMap.isEmpty()) {
            Map<Id, Case> caseMap = new Map<Id, Case>([
                SELECT Id, VTD1_PI_contact__c, VTD1_PI_user__c
                FROM Case
                WHERE Id IN :caseIdToDocsMap.keySet()
            ]);
            List<Id> tnSourceIds = new List<Id>();
            List<String> tnCodes = new List<String>();
            List<VTD2_Email_Driver__c> emails = new List<VTD2_Email_Driver__c>();
            for (Id casId : caseIdToDocsMap.keySet()) {
                Integer counterForCRANotif = 0;
                for (VTD1_Document__c doc : caseIdToDocsMap.get(casId).get(VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED)) {
                    if (doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD) {
                        counterForCRANotif++;
                        if (counterForCRANotif > 1) {
                            break;
                        }
                    }
                }
                if (counterForCRANotif == 1) {
                    tnSourceIds.add(caseIdToDocsMap.get(casId).get(VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED)[0].Id);
                    tnCodes.add(SINGLE_DOC_CRA_TNCODE);
                } else if (counterForCRANotif > 1) {
                    tnSourceIds.add(casId);
                    tnCodes.add(MULTIPLE_DOCS_CRA_TNCODE);
                }
                if (caseIdToDocsMap.get(casId).get(VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED).size() > 0) {
                    emails.add(createEmail(caseMap.get(casId), VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED));
                    tnSourceIds.add(casId);
                    tnCodes.add(APPROVED_DOCS_PI_TNCODE);
                }
                if (caseIdToDocsMap.get(casId).get(VT_R4_ConstantsHelper_Documents.DOCUMENT_REJECTED).size() > 0) {
                    emails.add(createEmail(caseMap.get(casId), VT_R4_ConstantsHelper_Documents.DOCUMENT_REJECTED));
                    tnSourceIds.add(casId);
                    tnCodes.add(REJECTED_DOCS_PI_TNCODE);
                }
            }
            if (tnCodes.size() > 0) {
                VT_D2_TNCatalogNotifications.generateNotifications(tnCodes, tnSourceIds, null);
            }
            if (emails.size() > 0) {
                insert emails;
            }
        }

        if (!siteDocumentMap.isEmpty()) {
            new VT_D2_ProtocolAmendmentTriggerHandler().checkReconsentFlagOnCases(siteDocumentMap.keySet());
            new VT_D2_ProtocolAmendmentTriggerHandler().sendNotificationsAndTasksToTeamMembers(siteDocumentMap);
        }
    }

    private static VTD2_Email_Driver__c createEmail(Case cas, String status) {
        String emailTemplate = piEmailTemplatesMap.get(status);
        VTD2_Email_Driver__c email = new VTD2_Email_Driver__c(
            VTD2_Record_Id__c = cas.Id,
            VTD2_Email_Recepient__c = cas.VTD1_PI_contact__c,
            VTD2_Email_template_API_Name__c = emailTemplate
        );
        return email;
    }

    // for Secure Portal file uploading only
    public void shareWithPgApprover(List<VTD1_Document__c> documents) {
        if (UserInfo.getUserName() == VTD1_FileUploadUserCredentials__c.getInstance().username__c) {
            List<VTD1_Document__Share> docShare = new List<VTD1_Document__Share>();
            for (VTD1_Document__c doc : documents) {
                if (doc.VTD1_PG_Approver__c != null) {
                    docShare.add(new VTD1_Document__Share(
                            UserOrGroupId = doc.VTD1_PG_Approver__c,
                            ParentId = doc.Id,
                            RowCause = Schema.VTD1_Document__Share.RowCause.GlobalSharing__c,
                            AccessLevel = 'Edit'
                    ));
                }
            }
            if (!docShare.isEmpty()) {
                insert docShare;
            }
        }
    }

    public void checkAppliedAllPatients(List<VTD1_Document__c> documents, Map<Id, VTD1_Document__c> oldDocuments) {
        Set<Id> protocolAmendmentsIds = new Set<Id>();
        for (VTD1_Document__c doc : documents) {
            if (isPASignaturePage(doc)) {
                if ((doc.VTR4_ChangesAppliedToAllPatients__c != oldDocuments.get(doc.Id).VTR4_ChangesAppliedToAllPatients__c)
                        || (Trigger.isAfter && Trigger.isDelete)) {
                    protocolAmendmentsIds.add(doc.VTD1_Protocol_Amendment__c);
                }
            }
        }
        if (protocolAmendmentsIds.isEmpty()) return;

        Map<Id, VTD1_Protocol_Amendment__c> mapPa = new Map<Id, VTD1_Protocol_Amendment__c>();
        Map<Id, List<VTD1_Document__c>> mapPaDocs = new Map<Id, List<VTD1_Document__c>>();
        for (VTD1_Protocol_Amendment__c PAmendment : [
                SELECT Id, VTD1_Changes_Applied_to_All_Patients__c,
                    (SELECT VTR4_ChangesAppliedToAllPatients__c, VTD1_Name__c
                    FROM Documents__r)
                FROM VTD1_Protocol_Amendment__c
                WHERE Id IN : protocolAmendmentsIds
        ]) {
            mapPaDocs.put(PAmendment.Id, PAmendment.Documents__r);
            mapPa.put(PAmendment.Id, PAmendment);
        }
        List<VTD1_Protocol_Amendment__c> PAChanged = new List<VTD1_Protocol_Amendment__c>();
        Boolean changesApplied;
        for (Id protocolAmendmentId : mapPaDocs.keySet()) {
            changesApplied = true;
            for (VTD1_Document__c doc : mapPaDocs.get(protocolAmendmentId)) {
                if (!doc.VTR4_ChangesAppliedToAllPatients__c) {
                    changesApplied = false;
                    break;
                }
            }
            if (mapPa.get(protocolAmendmentId).VTD1_Changes_Applied_to_All_Patients__c != changesApplied
                    || (mapPaDocs.get(protocolAmendmentId).isEmpty() &&
                    mapPa.get(protocolAmendmentId).VTD1_Changes_Applied_to_All_Patients__c != false)) {
                mapPa.get(protocolAmendmentId).VTD1_Changes_Applied_to_All_Patients__c = !mapPa.get(protocolAmendmentId).VTD1_Changes_Applied_to_All_Patients__c;
                PAChanged.add(mapPa.get(protocolAmendmentId));
            }
        }
        update PAChanged;
    }

    public static Boolean isPASignaturePage(VTD1_Document__c doc) {
            return doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT
                && doc.VTD1_Regulatory_Document_Type__c == VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_SIGNATURE_PAGE
                && doc.VTD1_Protocol_Amendment__c != null
                && doc.VTD1_Site__c != null;
    }
}