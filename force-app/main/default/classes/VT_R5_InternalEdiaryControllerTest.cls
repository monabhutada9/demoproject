/**
 * Created by Dmitry Kovalev on 27.08.2020.
 */

@IsTest
public class VT_R5_InternalEdiaryControllerTest {
    private static final Datetime DT_DUE_DATE = Datetime.newInstance(2020, 1, 1, 12, 0, 0);
    private static final Datetime DT_COMPLETION_TIME = Datetime.newInstance(2020, 1, 1, 11, 0, 0);

    // DK TODO: replace with constants from ConstantHelper
    private static final String RT_SOURCE_FORM = 'VTR5_SourceForm';
    private static final String RT_EXTERNAL = 'VTR5_External';

    @TestSetup
    private static void setup() {
        DomainObjects.User_t ptUserT = new DomainObjects.User_t()
            .addContact(new DomainObjects.Contact_t())
            .setProfile(VT_R4_ConstantsHelper_Profiles.PATIENT_PROFILE_NAME);
        User ptUser = (User) ptUserT.persist();

        User cgUser = (User) new DomainObjects.User_t()
            .addContact(new DomainObjects.Contact_t())
            .setProfile(VT_R4_ConstantsHelper_Profiles.CAREGIVER_PROFILE_NAME)
            .persist();

        Case ptCase = (Case) new DomainObjects.Case_t()
            .setRecordTypeByName('CarePlan')
            .addAccount(new DomainObjects.Account_t())
            .addUser(ptUserT)
            .persist();

        List<DomainBuilder> surveysT = new List<DomainBuilder> {
            createCompletedSurveyT(RT_SOURCE_FORM, ptCase, ptUser),
            createCompletedSurveyT(RT_SOURCE_FORM, ptCase, cgUser),
            createCompletedSurveyT(RT_EXTERNAL, ptCase, ptUser),
            createCompletedSurveyT(RT_EXTERNAL, ptCase, cgUser)
        };
        for (DomainBuilder surveyT : surveysT) {
            surveyT.persist();
        }
    }

    private static DomainBuilder createCompletedSurveyT(String recordTypeName, Case ptCase, User completedBy) {
        return new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(ptCase.Id)
                .setRecordTypeByName(recordTypeName)
                .setVTD1_Due_Date(DT_DUE_DATE)
                .setVTD1_Status(VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_COMPLETED)
                .setCompletedBy(completedBy)
                .setCompletionTime(DT_COMPLETION_TIME);
    }

    @IsTest
    public static void getSourceFormDataTest() {
        Id ptCaseId = [SELECT Id FROM Case LIMIT 1].Id;
        List<VTD1_Survey__c> eDiaries = VT_R5_InternalEdiaryController.getData(ptCaseId, true);

        System.assertEquals(2, eDiaries.size());
    }

    @IsTest
    public static void getNotSourceFormDataTest() {
        Id ptCaseId = [SELECT Id FROM Case LIMIT 1].Id;
        List<VTD1_Survey__c> eDiaries = VT_R5_InternalEdiaryController.getData(ptCaseId, false);

        System.assertEquals(2, eDiaries.size());
    }
}