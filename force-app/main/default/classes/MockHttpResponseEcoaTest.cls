/**
  Created by Avengers in R5.7 for Epic# 20625, to create mock response for testCallout method of VT_R5_eCoaAlertsTest
**/
@isTest
global class MockHttpResponseEcoaTest implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest req) {
       
        HttpResponse res = new HttpResponse();
        
        res.setStatusCode(200);
        return res;
    }
}