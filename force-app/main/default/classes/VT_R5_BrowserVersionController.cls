/**
 * Created by K.Latysh on 31-Mar-20.
 */

public with sharing class VT_R5_BrowserVersionController {

    @AuraEnabled(Cacheable=true)
    public static String getCountryISO() {
        List<LoginHistory>logs = [
                SELECT
                        CountryIso
                FROM    LoginHistory
                WHERE   UserId = :UserInfo.getUserId()
                ORDER BY LoginTime DESC
                LIMIT 1
        ];
        if(!logs.isEmpty()){
            return logs[0].CountryIso;
        }
        return null;
    }

}