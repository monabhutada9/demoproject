/**
 * Created by shume on 6/29/2020.
 * Commented and requires deleting - not longer needed - switched to batch for conversion instead of platform events
 */

public with sharing class VT_R4_CreateCaseHandler {
   /* public void processEvents(List <VTR4_New_Transaction__e> events) {
        System.debug('VT_R4_CreateCaseHandler');
        try {
            List <VT_R4_PatientConversionHelper.ContactCandidateWrapper> cpWraps = new List<VT_R4_PatientConversionHelper.ContactCandidateWrapper>();
            for (VTR4_New_Transaction__e event : events) {
                VT_R4_PatientConversionHelper.ContactCandidateWrapper wrapper = (VT_R4_PatientConversionHelper.ContactCandidateWrapper)JSON.deserialize(event.VTR4_Parameters__c, VT_R4_PatientConversionHelper.ContactCandidateWrapper.class);
                cpWraps.add(wrapper);
            }

            try {
                VT_R4_PatientConversion.createCases(cpWraps, true, false);
            } catch (Exception e) {
                for (VT_R4_PatientConversionHelper.ContactCandidateWrapper wrapper : cpWraps) {
                    VT_R4_PatientConversionHelper.logError(null, wrapper.candidate, 'VTR4_StatusCreate__c', e);
                }
                VT_R4_PatientConversion.saveErrors();
            }
        } catch (Exception e) {

            insert new VTR4_Conversion_Log__c(VTR4_Error_Message__c = e.getMessage()
                    + ' ' + e.getStackTraceString()
                    + ' ' + e.getCause() + e.getLineNumber() + ' ' + e.getTypeName());
        }
    }

    public Object call(String action, Map<String, Object> args) {
        processEvents((List <VTR4_New_Transaction__e>)args.get('events'));
        return null;
    }
    */
}