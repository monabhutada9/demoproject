@IsTest
public with sharing class VT_R2_SendSMSManuallyTest {

    public static void testBehavior() {
        setUpMCSettings();
        VTR2_SendSMSLog__c smsLog = (VTR2_SendSMSLog__c) new DomainObjects.VTR2_SendSMSLog_t()
                .setPhoneNumber('someNumber')
                .persist();


        Test.startTest();
        String result = VT_R2_SendSMSManuallyController.sendSMS(smsLog.Id);
        Test.stopTest();

        System.assertEquals('Message cannot be empty', result);
    }

    private static void setUpMCSettings() {
        List<VTR2_McloudSettings__mdt> mcloudSettings = new List<VTR2_McloudSettings__mdt>();
        mcloudSettings.add(new VTR2_McloudSettings__mdt(VTR2_OutboundMessageKey_API_Triggered__c = 'NjU6Nzg6MA',
                VTR2_RestURI__c = 'https://mc2gwv28hdsr86gd-sldnjzvzzgm.rest.marketingcloudapis.com/',
                VTR2_ShortCode__c = '99808'));
        VT_R2_McloudBroadcaster.setMCsettings(mcloudSettings);
    }
}