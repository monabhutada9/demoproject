/**
 * Created by Vlad Tyazhov on 18.11.2020.
 */

@IsTest
private without sharing class VT_R5_HotfixEligibilityStatusBatchTest {
    @TestSetup
    static void setup() {
        HealthCloudGA__CarePlanTemplate__c study = (HealthCloudGA__CarePlanTemplate__c) new DomainObjects.Study_t()
                .setName('HotfixEligibilityStudy')
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'))
                .persist();

        study.VTD1_Randomized__c = 'No';
        update study;

        createCase('Pre-Consent|NULL', VT_R4_ConstantsHelper_AccountContactCase.CASE_PRE_CONSENT, null, study.Id);
        createCase('Consented|NULL', VT_R4_ConstantsHelper_AccountContactCase.CASE_CONSENTED, null, study.Id);
        createCase('Screened|NULL', VT_R4_ConstantsHelper_AccountContactCase.CASE_SCREENED, null, study.Id);
        createCase('Active-Randomized|NULL', VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED, null, study.Id);
//        createCase('Screen-Failure|Ineligible', VT_R4_ConstantsHelper_AccountContactCase.CASE_SCREEN_FAILURE, VT_R4_ConstantsHelper_Misc.CASE_ELIGIBLITY_STATUS_INELIGIBLE, study.Id);

    }

    @IsTest
    static void testBatch() {
        List<Case> cases = [
                SELECT Id, VTD1_Subject_ID__c, Status, VTD1_Eligibility_Status__c
                FROM Case
                WHERE Contact.LastName = 'VT_R5_HotfixEligibilityStatusBatchTest'
        ];
        System.debug('het: cases: ' + cases);
        List<String> patientNumbers = new List<String>();
        for (Case c: cases) {
            patientNumbers.add(c.VTD1_Subject_ID__c);
        }

        Test.startTest();
        Database.executeBatch(new VT_R5_HotfixEligibilityStatusBatch(patientNumbers), 5);
        Test.stopTest();

        Case casePreConsent = getCase('Pre-Consent|NULL');
        System.assertEquals(VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED, casePreConsent.Status);
        System.assertEquals(VT_R4_ConstantsHelper_Misc.CASE_ELIGIBLITY_STATUS_ELIGIBLE, casePreConsent.VTD1_Eligibility_Status__c);
        System.assertEquals(true, casePreConsent.VTD1_Screening_Complete__c);
        System.assertEquals(true, casePreConsent.VTD2_Baseline_Visit_Complete__c);

        Case caseConsented = getCase('Consented|NULL');
        System.assertEquals(VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED, caseConsented.Status);
        System.assertEquals(VT_R4_ConstantsHelper_Misc.CASE_ELIGIBLITY_STATUS_ELIGIBLE, caseConsented.VTD1_Eligibility_Status__c);
        System.assertEquals(true, caseConsented.VTD1_Screening_Complete__c);
        System.assertEquals(true, caseConsented.VTD2_Baseline_Visit_Complete__c);

        Case caseScreened = getCase('Screened|NULL');
        System.assertEquals(VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED, caseScreened.Status);
        System.assertEquals(VT_R4_ConstantsHelper_Misc.CASE_ELIGIBLITY_STATUS_ELIGIBLE, caseScreened.VTD1_Eligibility_Status__c);
        System.assertEquals(true, caseScreened.VTD1_Screening_Complete__c);
        System.assertEquals(true, caseScreened.VTD2_Baseline_Visit_Complete__c);

        Case caseActiveRandomized = getCase('Active-Randomized|NULL');
        System.assertEquals(VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED, caseActiveRandomized.Status);
        System.assertEquals(VT_R4_ConstantsHelper_Misc.CASE_ELIGIBLITY_STATUS_ELIGIBLE, caseActiveRandomized.VTD1_Eligibility_Status__c);
        System.assertEquals(false, caseActiveRandomized.VTD1_Screening_Complete__c);
        System.assertEquals(false, caseActiveRandomized.VTD2_Baseline_Visit_Complete__c);

//        Case caseScreenFailure = getCase('Screen-Failure|Ineligible');
//        System.assertEquals(VT_R4_ConstantsHelper_AccountContactCase.CASE_SCREEN_FAILURE, caseScreenFailure.Status);
//        System.assertEquals(VT_R4_ConstantsHelper_Misc.CASE_ELIGIBLITY_STATUS_INELIGIBLE, caseScreenFailure.VTD1_Eligibility_Status__c);
//        System.assertEquals(null, caseScreened.VTD1_Screening_Complete__c);
//        System.assertEquals(null, caseScreened.VTD2_Baseline_Visit_Complete__c);
    }

    static private void createCase(String patientName, String caseStatus, String eligibilityStatus, Id studyId) {
        DomainObjects.Contact_t con_t = new DomainObjects.Contact_t()
                .setFirstName(patientName)
                .setLastName('VT_R5_HotfixEligibilityStatusBatchTest');
        DomainObjects.User_t user_t = new DomainObjects.User_t()
                .addContact(con_t)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        Case c = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .setStudy(studyId)
                .addVTD1_Patient_User(user_t)
                .addContact(con_t)
                .setStatus(caseStatus)
                .setVTD1_Eligibility_Status(eligibilityStatus)
                .persist();
        con_t.setVTD1_Clinical_Study_Membership(c.Id);
        update con_t.toObject();
    }

    static private Case getCase(String patientName) {
        return [
                SELECT Id, VTD1_Subject_ID__c, Status, VTD1_Eligibility_Status__c, VTD1_Screening_Complete__c, VTD2_Baseline_Visit_Complete__c
                FROM Case
                WHERE Contact.LastName = 'VT_R5_HotfixEligibilityStatusBatchTest' AND Contact.FirstName = :patientName
        ];
    }
}