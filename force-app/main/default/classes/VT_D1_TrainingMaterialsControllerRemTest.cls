/**
 * Created by User on 19/05/16.
 */
@isTest
private with sharing class VT_D1_TrainingMaterialsControllerRemTest {

    @TestSetup
    static void setup(){
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );
        new DomainObjects.VTD2_Study_Geography_t(study).persist();

        Case cas = (Case) new DomainObjects.Case_t()
                .setStudy(study.id).setRecordTypeByName(VT_D1_ConstantsHelper.RT_CAREPLAN)
                .persist();
        Test.stopTest();
        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
            .setFirstName('testTrainingMaterialsControllerRem');

        System.debug('patientContact ' + patientContact);
        //UserRole ur = [Select PortalType, PortalAccountId From UserRole where PortalType =:'CustomerPortal' limit 1];
        DomainObjects.User_t userPatient = new DomainObjects.User_t()
                .setEmail('testTrainingMaterialsControllerRemTest@test.com')
                .addContact(patientContact)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
                //.setUserRoleId(ur.Id);
        //System.runAs(new User(Id = UserInfo.getUserId(), User_Role__c ='System Administrator')){
            User user = (User) userPatient.persist();
        //}
        Contact con = [SELECT Id FROM Contact WHERE FirstName = 'testTrainingMaterialsControllerRem' LIMIT 1];
        con.VTD1_Clinical_Study_Membership__c = cas.Id;
        update con;
        User userRun = [SELECT Id, ContactId FROM User WHERE Email = 'testTrainingMaterialsControllerRemTest@test.com' LIMIT 1];
        CaseShare jobShr = new CaseShare();
        jobShr.CaseId = cas.Id;
        jobShr.UserOrGroupId = userRun.id;
        jobShr.CaseAccessLevel = 'Read';
        ContactShare conShr = new ContactShare();
        conShr.ContactId = con.Id;
        conShr.UserOrGroupId = userRun.id;
        conShr.ContactAccessLevel = 'Read';
        insert jobShr;
        insert conShr;
    }

    @isTest
    private static void getTrainingMaterialsTrueTest(){
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setName('Study Name')
                /*.addPMA(new DomainObjects.User_t()
                        .addContact(new DomainObjects.Contact_t())
                        .setProfile('Site Coordinator'))*/
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));
        study.persist();
        VT_D1_TrainingMaterialsControllerRemote.getTrainingMaterials(true,study.Id);
    }
    @isTest
    private static void getTrainingMaterialsTest() {
        User userPatient = [SELECT Id, ContactId FROM User WHERE Email = 'testTrainingMaterialsControllerRemTest@test.com' LIMIT 1];
        System.runAs(userPatient) {
            VT_D1_TrainingMaterialsControllerRemote.getTrainingMaterials(false, null);
        }
    }
    @isTest
    private static void getSingleArticleTest(){
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'));
        VT_D1_TrainingMaterialsControllerRemote.getSingleArticle(true,study.Id,null);
    }
    @isTest
    private static void getContentDocsTest(){
        VT_D1_TrainingMaterialsControllerRemote.getContentDocs(null);
    }
}