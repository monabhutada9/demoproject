/**
* @author: Carl Judge / Dmitry Yartsev
* @date: 30-Jun-20
* @description: Class to stub things
**/

public class VT_Stubber {
    private static Map<String, Object> stubRegistry = new Map<String, Object>();
    public static void applyStub(String targetStubName, Object stubbedResult) {
        stubRegistry.put(targetStubName, stubbedResult);
    }
    public static void removeStub(String targetStubName) {
        stubRegistry.remove(targetStubName);
    }
    public static void clearAllStub() {
        stubRegistry.clear();
    }

    private abstract class Stubber {
        private Object result;
        private String name;
        public abstract Object getResult();
    }

    public class ResultStub extends Stubber {
        public ResultStub(String name, Object result) {
            this.name = name;
            this.result = result;
        }
        public override Object getResult() {
            return stubRegistry.keySet().contains(this.name)
                ? stubRegistry.get(this.name)
                : this.result;
        }
    }

    public inherited sharing class DMLStub extends Stubber {
        private DMLOperation operation;
        private Boolean allOrNothing = true;
        private Object target;

        public DMLStub(String name, Object target, DMLOperation operation) {
            this.name = name;
            this.target = target;
            this.operation = operation;
        }
        public DMLStub(String name, Object target, DMLOperation operation, Boolean allOrNothing) {
            this.name = name;
            this.target = target;
            this.operation = operation;
            this.allOrNothing = allOrNothing;
        }

        public DMLStub perform() {
            if (stubRegistry.keySet().contains(this.name)) {
                this.result = stubRegistry.get(this.name);
            } else {
                List<SObject> recs = this.target instanceof List<SObject>
                    ? (List<SObject>) this.target
                    : new List<SObject>{(SObject) this.target};

                switch on this.operation {
                    when INS {
                        this.result = Database.insert(recs, allOrNothing);
                    }
                    when UPD {
                        this.result = Database.update(recs, allOrNothing);
                    }
                    when UPS {
                        this.result = Database.upsert(recs, allOrNothing);
                    }
                    when DEL {
                        this.result = Database.delete(recs, allOrNothing);
                    }
                    when UND {
                        this.result = Database.undelete(recs, allOrNothing);
                    }
                }
            }
            return this;
        }
        public override Object getResult() {
            return this.result;
        }
    }
}