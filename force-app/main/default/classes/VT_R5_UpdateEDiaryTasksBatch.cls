/**
 * Created by Aleksandr Mazan on 02-Dec-20.
 */

//WARNING
//R5.6
//Run only one time to update VTD2_Subject_Parameters__c for Heroku Scheduler's tasks
public with sharing class VT_R5_UpdateEDiaryTasksBatch implements Database.Batchable<sObject> {

    public Database.QueryLocator start(Database.BatchableContext BC) {
        List<User> users = [SELECT Id FROM User WHERE Name = 'Heroku Scheduler'];
        Id herokuSchedulerId;
        String query = 'SELECT Id, VTD2_Subject_Parameters__c FROM Task WHERE CreatedById = :herokuSchedulerId AND (NOT VTD2_Subject_Parameters__c LIKE \'%Name\') AND VTD2_Subject_Parameters__c != null';
        if (users.isEmpty()) {
            query += ' LIMIT 0';
        } else {
            herokuSchedulerId = users[0].Id;
        }
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<Task> scope) {
        for (Task task : scope) {
            task.VTD2_Subject_Parameters__c += 'Name';
        }
        update scope;
    }

    public void finish(Database.BatchableContext BC) {
    }

}