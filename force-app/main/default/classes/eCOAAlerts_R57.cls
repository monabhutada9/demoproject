/**
 * Created by Dmitry Kovalev on 21.08.2020.
 * Release 5.1, Sprint 1
 */

@IsTest
public class eCOAAlerts_R57{

    private enum Scenario { SITE_STAFF_ALERTS, SITE_STAFF_ALERTS_MISSED, PATIENT_ALERTS }

    private static final Id DAILY_ALERT_RECORD_TYPE_ID = Schema.SObjectType.VTR5_eDiaryNotificationBuilder__c.getRecordTypeInfosByDeveloperName().get('VTR5_DailyAlert').getRecordTypeId();
    private static final Id EXPIRATION_ALERT_RECORD_TYPE_ID = Schema.SObjectType.VTR5_eDiaryNotificationBuilder__c.getRecordTypeInfosByDeveloperName().get('VTR5_ExpirationAlert').getRecordTypeId();
    private static final Id SIMPLE_TASK_RT = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('SimpleTask').getRecordTypeId();
    private static final String BUILDER_OPERATION_BEFORE = 'BEFORE the Expiration Date';
    private static final String BUILDER_OPERATION_AFTER = 'AFTER the Expiration Date';
    private static final String BUILDER_SITE_STAFF_RECIPIENT = 'Site Staff';
    private static final String BUILDER_PT_CG_RECIPIENT = 'Patient/Caregiver';

    private static final Set<String> COMMUNITY_PROFILES = new Set<String> {
            VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME,
            VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME
    };

    private static VTD1_Survey__c sv;

    static {
        sv = [
                SELECT Id, VTD1_Due_Date__c, VTD1_Status__c, Name, VTD1_CSM__r.VTD1_Patient_User__r.Profile.Name,
                        VTD1_CSM__r.VTD1_Patient_User__r.TimeZoneSidKey, VTD1_CSM__r.VTD1_Study__c,
                        VTD1_CSM__r.VTD1_Virtual_Site__r.VTD1_PI_User__r.TimeZoneSidKey
                FROM VTD1_Survey__c 
                LIMIT 1
        ].get(0);
    }

   /* public static void doSiteStaffAlertsTest() {
        
        Test.startTest();
        {
            runBatchTestScenario(Scenario.SITE_STAFF_ALERTS);
            insertSiteStaffAlerts();
        }
        Test.stopTest();
        
       System.assert([SELECT Id FROM Task].size() > 0);
       System.assert([SELECT Id FROM VTD1_NotificationC__c].size() > 0);

        Id piUserWithoutCaseId = [Select Id from User where email='piNotificationUser1@email.com'].Id;
        Id piUserWithCaseId = [Select Id from User where email='test@email.com'].Id;
        Boolean notificationReceviedforUserWithoutCase = false;
        Boolean notificationReceviedforUserWithCase = false;       
        for(VTD1_NotificationC__c notification : [SELECT Id,VTD1_Receivers__c FROM VTD1_NotificationC__c]){
            if(notification.VTD1_Receivers__c == piUserWithoutCaseId){
                notificationReceviedforUserWithoutCase = true;
                System.debug('notification record===='+notification);            
        }
            if(notification.VTD1_Receivers__c == piUserWithCaseId){
                notificationReceviedforUserWithCase = true;
                System.debug('notification record===='+notification);            
        }
        System.debug('notificationReceviedforUserWithoutCase===='+notificationReceviedforUserWithoutCase);
        System.debug('notificationReceviedforUserWithCase===='+notificationReceviedforUserWithCase);    
     }
    } */
    
    public static void doSiteStaffAlertsTest2() {
        Test.startTest();
        {
            runBatchTestScenario(Scenario.SITE_STAFF_ALERTS);
        }
        Test.stopTest();
     
        Id piUserWithoutCaseId = [Select Id from User where email='piNotificationUser1@email.com'].Id;
        Id piUserWithCaseId = [Select Id from User where email='test@email.com'].Id;
        Boolean notificationReceviedforUserWithoutCase = false;
        Boolean notificationReceviedforUserWithCase = false;       
        for(VTD1_NotificationC__c notification : [SELECT Id,VTD1_Receivers__c FROM VTD1_NotificationC__c]){
            if(notification.VTD1_Receivers__c == piUserWithoutCaseId){
                notificationReceviedforUserWithoutCase = true;
        }
            if(notification.VTD1_Receivers__c == piUserWithCaseId){
                notificationReceviedforUserWithCase = true;
        }
        }
        System.assertEquals(notificationReceviedforUserWithoutCase,false);
        System.assertEquals(notificationReceviedforUserWithCase,true);
    }
    
    public static void doSiteStaffAlertsMissedTest() {
        Test.startTest();
        {
            runBatchTestScenario(Scenario.SITE_STAFF_ALERTS_MISSED);
            insertSiteStaffAlerts();
        }
        Test.stopTest();
        System.assert([SELECT Id FROM Task].size() > 0);
        System.assert([SELECT Id FROM VTD1_NotificationC__c].size() > 0);
    }

    public static void doPtCgAlertsTest() {
        Test.startTest();
        {
            runBatchTestScenario(Scenario.PATIENT_ALERTS);
            insertPtCgAlerts();
        }
        Test.stopTest();
        System.assert([SELECT Id FROM VTD1_NotificationC__c].size() > 0);
    }

    private static void runBatchTestScenario(Scenario sc) {
        if (sc != Scenario.PATIENT_ALERTS) {
            insertPgStm(sv.VTD1_CSM__r.VTD1_Study__c);
        }

        String payload = getPayloadString(sc, sv);
        new VT_R5_eDiaryAlertsService().herokuExecute(payload);
    }

    private static void insertSiteStaffAlerts() {
        List<User> users = new List<User>();
        for (Study_Team_Member__c stm : [SELECT User__r.Profile.Name FROM Study_Team_Member__c WHERE Study__c = :sv.VTD1_CSM__r.VTD1_Study__c]) {
            users.add(stm.User__r);
        }
        VT_R5_eDiaryAlertsBulkInsertRestService.RequestPayload payload = getRequestPayload(Scenario.SITE_STAFF_ALERTS, users);
        insertAlerts(payload);
    }

    private static void insertPtCgAlerts() {
        VT_R5_eDiaryAlertsBulkInsertRestService.RequestPayload payload = getRequestPayload(Scenario.PATIENT_ALERTS, new List<User> { sv.VTD1_CSM__r.VTD1_Patient_User__r });
        insertAlerts(payload);
    }

    private static void insertPgStm(Id studyId) {
        String userName = VT_D1_TestUtils.generateUniqueUserName();
        DomainObjects.StudyTeamMember_t stm = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('PG')
                .addUser(
                        new DomainObjects.User_t()
                                .setProfile('Patient Guide')
                                .setEmail(userName)
                                .setUsername(userName)
                )
                .setStudyId(studyId);
        stm.persist();
    }

    private static String getPayloadString(Scenario sc, VTD1_Survey__c survey) {

        Id studyId = survey.VTD1_CSM__r.VTD1_Study__c;
        String surveyName = survey.Name;

        List<VTR5_eDiaryNotificationBuilder__c> buildersList = prepareBuilders(sc, studyId, surveyName);
        insert buildersList;

        List<VT_R5_eDiaryAlertsService.AlertRequestInfo> alertsList = new List<VT_R5_eDiaryAlertsService.AlertRequestInfo>();

        for (VTR5_eDiaryNotificationBuilder__c builder : buildersList) {
            alertsList.add(getAlertRequestInfoInstance(builder, survey));
        }

        return JSON.serialize(alertsList);
    }

    private static VT_R5_eDiaryAlertsBulkInsertRestService.RequestPayload  getRequestPayload(Scenario sc, List<User> users) {
        String emailTemplateName;
        Set<Id> usersForEmails = new Set<Id>();
        Set<Id> usersForPush = new Set<Id>();
        List<Task> tasks = new List<Task>();
        List<VTD1_NotificationC__c> communityNotifications = new List<VTD1_NotificationC__c>();

        List<VTR5_eDiaryNotificationBuilder__c> buildersList = [SELECT Id, VTR5_Study__c FROM VTR5_eDiaryNotificationBuilder__c];

        for (User u : users) {
            usersForEmails.add(u.Id);
            communityNotifications.addAll(getNotificationCList(u, buildersList));
            tasks.addAll(getTaskList(u, buildersList));
        }

        if (sc == Scenario.PATIENT_ALERTS) {
            emailTemplateName = 'VT_R5_eCOA_Notification';
            usersForPush.add(sv.VTD1_CSM__r.VTD1_Patient_User__c);
        } else {
            emailTemplateName = 'Notification_as_Email';
        }

        return sc == Scenario.PATIENT_ALERTS
                ? new VT_R5_eDiaryAlertsBulkInsertRestService.RequestPayload(emailTemplateName, usersForEmails, communityNotifications, usersForPush)
                : new VT_R5_eDiaryAlertsBulkInsertRestService.RequestPayload(emailTemplateName, usersForEmails, communityNotifications, tasks);

    }

    private static List<VTR5_eDiaryNotificationBuilder__c> prepareBuilders(Scenario sc, Id studyId, String eDiaryName) {
        List<VTR5_eDiaryNotificationBuilder__c> buildersList = new List<VTR5_eDiaryNotificationBuilder__c>();

        switch on sc {
            when SITE_STAFF_ALERTS {
                buildersList.add(getNotificationBuilderInstance(DAILY_ALERT_RECORD_TYPE_ID, studyId, eDiaryName, null, BUILDER_SITE_STAFF_RECIPIENT, VT_R4_ConstantsHelper_ProfilesSTM.SITE_STAFF));
               // buildersList.add(getNotificationBuilderInstance(EXPIRATION_ALERT_RECORD_TYPE_ID, studyId, eDiaryName, BUILDER_OPERATION_BEFORE, BUILDER_SITE_STAFF_RECIPIENT, VT_R4_ConstantsHelper_ProfilesSTM.SITE_STAFF));
            }
            when SITE_STAFF_ALERTS_MISSED {
                buildersList.add(getNotificationBuilderInstance(EXPIRATION_ALERT_RECORD_TYPE_ID, studyId, eDiaryName, BUILDER_OPERATION_AFTER, BUILDER_SITE_STAFF_RECIPIENT, VT_R4_ConstantsHelper_ProfilesSTM.SITE_STAFF));
            }
            when PATIENT_ALERTS {
                buildersList.add(getNotificationBuilderInstance(DAILY_ALERT_RECORD_TYPE_ID, studyId, eDiaryName, null, BUILDER_PT_CG_RECIPIENT, VT_R4_ConstantsHelper_ProfilesSTM.HIPAA_PROFILES));
                buildersList.add(getNotificationBuilderInstance(EXPIRATION_ALERT_RECORD_TYPE_ID, studyId, eDiaryName, BUILDER_OPERATION_BEFORE, BUILDER_PT_CG_RECIPIENT, VT_R4_ConstantsHelper_ProfilesSTM.HIPAA_PROFILES));
            }
        }

        return buildersList;
    }

    private static VT_R5_eDiaryAlertsService.AlertRequestInfo getAlertRequestInfoInstance(VTR5_eDiaryNotificationBuilder__c builder, VTD1_Survey__c survey) {
        VT_R5_eDiaryAlertsService.RecipientType recipient = builder.VTR5_RecipientType__c == BUILDER_SITE_STAFF_RECIPIENT
                ? VT_R5_eDiaryAlertsService.RecipientType.SITE_STAFF
                : VT_R5_eDiaryAlertsService.RecipientType.PT_CG;

        return builder.RecordTypeId == DAILY_ALERT_RECORD_TYPE_ID
                ? new VT_R5_eDiaryAlertsService.AlertRequestInfo(builder.Id, recipient, getTimezoneOffset(builder.VTR5_RecipientType__c, survey.VTD1_CSM__r), true)
                : new VT_R5_eDiaryAlertsService.AlertRequestInfo(builder.Id, recipient, survey.VTD1_Due_Date__c);
    }

    private static VTR5_eDiaryNotificationBuilder__c getNotificationBuilderInstance(Id rtId, Id studyId, String eDiaryName, String operationType, String recipientType, Set<String> receivers) {
        return new VTR5_eDiaryNotificationBuilder__c(
                RecordTypeId = rtId,
                VTR5_Study__c = studyId,
                VTR5_eDiaryName__c = eDiaryName,
                VTR5_OperationType__c = operationType,
                VTR5_RecipientType__c = recipientType,
                VTR5_NotificationRecipient__c = String.join(new List<String>(receivers), ';'),
                VTR5_Offset__c = 1,
                VTR5_AlertTime__c = '10:00AM',
                VTR5_DaysOfWeek__c = 'Monday',
                VTR5_Notification_Title__c = 'test',
                VTR5_NotificationMessage__c = 'test',
                VTR5_Active__c = true
        );
    }

    private static Integer getTimezoneOffset(String recipientType, Case cas) {
        String timezoneKey = recipientType == BUILDER_SITE_STAFF_RECIPIENT
                ? cas.VTD1_Virtual_Site__r.VTD1_PI_User__r.TimeZoneSidKey
                : cas.VTD1_Patient_User__r.TimeZoneSidKey;
        Datetime dt = Datetime.now();
        return TimeZone.getTimeZone(timezoneKey).getOffset(dt) / 60000;
    }

    private static List<Task> getTaskList(User u, List<VTR5_eDiaryNotificationBuilder__c> builders) {
        List<Task> taskList = new List<Task>();

        for (VTR5_eDiaryNotificationBuilder__c builder : builders) {
            taskList.add(new Task(
                    Status = 'Open',
                    OwnerId = u.Id,
                    RecordTypeId = SIMPLE_TASK_RT,
                    VTR5_PreventPBInvocation__c = true,
                    Category__c = 'Follow Up Required',
                    VTR5_AutocompleteWhenClicked__c = true,
                    HealthCloudGA__CarePlanTemplate__c = builder.VTR5_Study__c,
                    Subject = 'test',
                    VTD2_My_Task_List_Redirect__c = 'test'
            ));
        }

        return taskList;
    }

    private static List<VTD1_NotificationC__c> getNotificationCList(User u, List<VTR5_eDiaryNotificationBuilder__c> builders) {
        List<VTD1_NotificationC__c> notificationsList = new List<VTD1_NotificationC__c>();

        for (VTR5_eDiaryNotificationBuilder__c builder : builders) {
            notificationsList.add(new VTD1_NotificationC__c(
                    VTD1_Receivers__c = u.Id,
                    OwnerId = u.Id,
                    Message__c = 'test',
                    Title__c = 'test',
                    VTR3_Notification_Type__c = 'eCOA Notification',
                    VTR5_eDiaryNotificationBuilder__c = builder.Id,
                    Link_to_related_event_or_object__c = 'test',
                    HasDirectLink__c = true
            ));
        }

        return notificationsList;
    }

    private static void insertAlerts(VT_R5_eDiaryAlertsBulkInsertRestService.RequestPayload payload) {
        RestResponse response = new RestResponse();
        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf(JSON.serialize(payload));
        RestContext.request = request;
        RestContext.response = response;

        VT_R5_eDiaryAlertsBulkInsertRestService.doPost();
    }
    public static void testCallout(){
         List<User> users = new List<User>();
        for (Study_Team_Member__c stm : [SELECT User__r.Profile.Name FROM Study_Team_Member__c WHERE Study__c = :sv.VTD1_CSM__r.VTD1_Study__c]) {
            users.add(stm.User__r);
        }
        VT_R5_eDiaryAlertsBulkInsertRestService.RequestPayload payload = getRequestPayload(Scenario.SITE_STAFF_ALERTS, users);
       Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        VT_R5_AbstractEDiaryAlertsBatch.bulkAlertsInsert(payload);
        
    }
}