public class VT_D1_DocumentVersionsListController {
	@AuraEnabled
    public static List<ContentVersion> getVersions(Id contentDocumentId) {
        try {            
			List<ContentVersion> versionsList;
            
            if (contentDocumentId != null) {
                versionsList = [
                    SELECT Id, Title, ContentDocumentId, VTD1_CompoundVersionNumber__c, VTD1_Lifecycle_State__c, VTD1_Current_Version__c, ContentModifiedDate, ContentModifiedBy.Name
                    FROM ContentVersion
                    WHERE ContentDocumentId = :contentDocumentId
                    ORDER BY ContentModifiedDate DESC
                ];
            }
            
			return versionsList;
		} catch (Exception e){
			throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
		}
    }
}