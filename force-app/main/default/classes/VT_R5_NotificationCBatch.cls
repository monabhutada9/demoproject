/**
 * @author: Maksim Fiasenka
 * @date: 06.11.2020
 * @version 1.0
 * @description: Class for inserting Weekly NotificationC__c records in iterable way. It needs for handling huge amount weekly NotificationC__c records.
 * @see VT_R5_NotificationCBatchTest as unit test
 */

public with sharing class VT_R5_NotificationCBatch implements Database.Batchable<SObject> {

    private Map<Id, Set<Id>> notificationsByStudyId;
    private Set<String> targetStatuses;

    public VT_R5_NotificationCBatch() {
        this.notificationsByStudyId = new Map<Id, Set<Id>>();
        this.targetStatuses = new Set<String>();
    }

    public VT_R5_NotificationCBatch(Map<Id, Set<Id>> notificationsByStudyId, Set<String> targetStatuses) {
        this();
        this.notificationsByStudyId = notificationsByStudyId;
        this.targetStatuses = targetStatuses;
    }

    public Database.QueryLocator start(Database.BatchableContext context) {
        QueryBuilder builder = new QueryBuilder(Case.class)
                .addFields(new List<String> {'Status', 'VTD1_Study__c', 'VTD1_Patient_User__c'})
                .addConditions()
                .add(new QueryBuilder.InCondition(Case.VTD1_Study__c).inCollection(this.notificationsByStudyId.keySet()))
                .add(new QueryBuilder.InCondition(Case.Status).inCollection(this.targetStatuses))
                .add(new QueryBuilder.CompareCondition('RecordType.DeveloperName').eq('CarePlan'))
                .setConditionOrder('1 AND 2 AND 3')
                .endConditions()
                .setLimit(30000);
        System.debug('query patients: ' + builder.toString());
        return builder.toQueryLocator();
    }

    public void execute(Database.BatchableContext context, List<Case> patients) {
        //Get Study Notification ids only for these patients
        Set<Id> studyNotificationIds = new Set<Id>();
        for (Id studyId : this.notificationsByStudyId.keySet()) {
            studyNotificationIds.addAll(this.notificationsByStudyId.get(studyId));
        }
        List<VTR3_Study_Notification__c> studyNotifications = queryStudyNotification(studyNotificationIds);

        Map<Id,Set<String>> studyPatientStatusMap = new Map<Id,Set<String>>();
        Map<Id, List<VTR3_Study_Notification__c>> notificationMap = new Map<Id,List<VTR3_Study_Notification__c>>();
        for (VTR3_Study_Notification__c studyNotification : studyNotifications) {
            studyPatientStatusMap.put(studyNotification.Id, new Set<String>(studyNotification.VTR3_Target_Status__c.split(';')));
            if ( ! notificationMap.containsKey(studyNotification.VTR3_Study__c)) {
                notificationMap.put(studyNotification.VTR3_Study__c, new List<VTR3_Study_Notification__c>());
            }
            notificationMap.get(studyNotification.VTR3_Study__c).add(studyNotification);
        }

        List<VTD1_NotificationC__c> weeklyNotifications = new List<VTD1_NotificationC__c>();
        for (Case patient : patients) {
            for (VTR3_Study_Notification__c stNotification : notificationMap.get(patient.VTD1_Study__c)) {
                if (studyPatientStatusMap.get(stNotification.Id).contains(patient.Status)) {
                    weeklyNotifications.add(new VTD1_NotificationC__c(
                        VTD2_DoNotDuplicate__c = true,
                        VTR3_Notification_Type__c = 'Study Notification',
                        Message__c = stNotification.VTR3_Notification_Message__c,
                        VTR2_Study__c = stNotification.VTR3_Study__c,
                        VTR3_Study_Notification__c = stNotification.Id,
                        Title__c = stNotification.VTR3_Category_name__c,
                        VTD1_Receivers__c = patient.VTD1_Patient_User__c,
                        VTR3_Recurrence_Number__c = 1
                    ));
                }
            }
        }
        insert weeklyNotifications;
    }

    public void finish(Database.BatchableContext context) {}

    private List<VTR3_Study_Notification__c> queryStudyNotification(Set<Id> studyNotificationIds) {
        List<String> studyNotificationFields = new List<String> {
            'VTR3_Target_Status__c', 'VTR3_Study__c', 'VTR3_Recurring_Time__c', 'VTR3_Days__c',
            'VTR3_Notification_Message__c', 'VTR3_Category_name__c'
        };

        QueryBuilder builder = new QueryBuilder(VTR3_Study_Notification__c.class)
            .addFields(studyNotificationFields)
            .addConditions()
            .add(new QueryBuilder.InCondition(VTR3_Study_Notification__c.Id).inCollection(studyNotificationIds))
            .endConditions();
        System.debug('query Study Notifications: ' + builder.toString());
        return (List<VTR3_Study_Notification__c>)builder.toList();
    }
}