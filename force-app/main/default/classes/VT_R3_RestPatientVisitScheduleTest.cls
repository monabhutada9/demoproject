@IsTest
private class VT_R3_RestPatientVisitScheduleTest {
	@TestSetup
	public static void setup() {
		Account account = new Account(Name = 'Test Account');
		insert account;

		String userName = VT_D1_TestUtils.generateUniqueUserName();
		Test.startTest();
		HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
		HealthCloudGA__CandidatePatient__c candidatePatient =
				VT_D1_TestUtils.createPatientCandidate('Ter', 'kin', userName, userName, 'terkin', 'Converted', study.Id,'PN', '11-11-11', 'US', 'NY');
		Test.stopTest();

		Case cs = [SELECT Id, VTR2_SiteCoordinator__c, VTD1_Primary_PG__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];
		Id caseId = cs.Id;
		cs.VTD1_Enrollment_Date__c = Datetime.now().addDays(-5).date();
		update cs;

		VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
		protocolVisit.VTD1_Range__c = 4;
		protocolVisit.VTD1_VisitOffset__c = 2;
		protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
		protocolVisit.VTD1_VisitDuration__c = '30 minutes';
		protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
		protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
		insert protocolVisit;

		VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
		actualVisit.VTD1_Protocol_Visit__c = protocolVisit.Id;
		actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
		actualVisit.VTD1_Case__c = caseId;
        insert actualVisit;
	}

	@IsTest
	public static void getScheduleInfoTest() {
		prepareScheduleRestContext(getVisit().Id, 'GET');

		Test.startTest();
		String response = VT_R3_RestPatientVisitSchedule.getScheduleInfo();
		System.assert(response.contains('serviceResponse'));
		Test.stopTest();
	}

	@IsTest
	public static void getScheduleInfoWithoutIdTest() {
		// Passing an empty record Id to cover the blank id use case
		prepareScheduleRestContext('', 'GET');

		Test.startTest();
		String response = VT_R3_RestPatientVisitSchedule.getScheduleInfo();
		System.assert(response.contains('Visit Id expected'));
		Test.stopTest();
	}

	@IsTest
	public static void getScheduleInfoWithWrongIdTest() {
		// Passing a WRONG ACCOUNT Id to cover incorrect input use case
		prepareScheduleRestContext(getAccount().Id, 'GET');
		Test.startTest();
		String response = VT_R3_RestPatientVisitSchedule.getScheduleInfo();
		System.assert(response.contains('INCORRECT INPUT'));
		Test.stopTest();
	}

	@IsTest
	public static void getScheduleInfoWithWrongVisitIdTest() {
		// Passing a WRONG Visit Id to cover incorrect input use case
		prepareScheduleRestContext(getDeletedVisitId(), 'GET');
		Test.startTest();
		String response = VT_R3_RestPatientVisitSchedule.getScheduleInfo();
		System.assert(response.contains('Server error'));
		Test.stopTest();
	}

	@IsTest
	public static void getScheduleVisitWithWrongVisitIdTest() {
		// Passing a WRONG Visit Id to cover incorrect input use case
		prepareScheduleRestContext(getDeletedVisitId(), 'GET');
		Test.startTest();
		String response = VT_R3_RestPatientVisitSchedule.scheduleVisit(generateTimeSlot());
		System.assert(response.contains('FAILURE'));
		Test.stopTest();
	}

//	@IsTest
//	public static void getScheduleVisitTest() {
//		prepareScheduleRestContext(getVisit().Id, 'GET');
//
//		Test.startTest();
//		String response = VT_R3_RestPatientVisitSchedule.scheduleVisit(generateTimeSlot());
//		System.assert(response.contains('SUCCESS'));
//		Test.stopTest();
//	}

	@IsTest
	public static void getScheduleVisitWithoutTimeSlotRequestTest() {
		prepareScheduleRestContext(getVisit().Id, 'GET');

		Test.startTest();
		String response = VT_R3_RestPatientVisitSchedule.scheduleVisit('');
		System.assert(response.contains('FAILURE'));
		Test.stopTest();
	}

	@IsTest
	public static void getScheduleVisitWithoutIdTest() {
		prepareScheduleRestContext('', 'GET');

		Test.startTest();
		String response = VT_R3_RestPatientVisitSchedule.scheduleVisit('');
		System.assert(response.contains('Visit Id expected'));
		Test.stopTest();
	}

	@IsTest
	public static void getScheduleVisitWithWrongIdTest() {
		// Passing a WRONG ACCOUNT Id to cover incorrect input use case
		prepareScheduleRestContext(getAccount().Id, 'GET');
		Test.startTest();
		String response = VT_R3_RestPatientVisitSchedule.scheduleVisit('');
		System.assert(response.contains('INCORRECT INPUT'));
		Test.stopTest();
	}

	private static String generateTimeSlot() {
		Datetime timeSlot = Datetime.now().addHours(2);
		Datetime roundedTimeSlot = Datetime.newInstance(timeSlot.year(), timeSlot.month(), timeSlot.day(), timeSlot.hour(), 0 ,0);
		return roundedTimeSlot.format('M/d/YYYY h:mm a').toLowerCase();
	}

	private static void prepareScheduleRestContext(String recordId, String httpMethod) {
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		req.requestURI = '/Patient/Visit/Schedule/' + recordId;
		req.httpMethod = httpMethod;

		RestContext.request = req;
		RestContext.response= res;
	}

	private static VTD1_Actual_Visit__c getVisit() {
		return [SELECT Id FROM VTD1_Actual_Visit__c LIMIT 1];
	}

	private static Account getAccount() {
		return [SELECT Id FROM Account LIMIT 1];
	}

	private static Id getDeletedVisitId() {
		Case cs = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];

		VTD1_Actual_Visit__c visit = new VTD1_Actual_Visit__c(VTR5_PatientCaseId__c = cs.Id);
		insert visit;

		Id id = visit.Id;
		delete visit;
		return id;
	}
}