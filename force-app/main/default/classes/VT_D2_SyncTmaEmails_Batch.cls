/**
* @author: Carl Judge
* @date: 27-Nov-18
* @description: To sync email from TMA user to Study Team Member records when changed
**/

global without sharing class VT_D2_SyncTmaEmails_Batch implements Database.Batchable<SObject>, Schedulable {

    // To schedule via apex hourly:
    // system.schedule('Sync TMA Emails', '0 0 * * * ?',  new VT_D2_SyncTmaEmails_Batch());

    global void execute(SchedulableContext sc) {
        Database.executeBatch(new VT_D2_SyncTmaEmails_Batch());
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id, Email
            FROM User
            WHERE Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME
        ]);
    }

    global void execute(Database.BatchableContext BC, List<User> scope) {
        Map<Id, User> userMap = new Map<Id, User>(scope);
        List<Study_Team_Member__c> membersToUpdate = new List<Study_Team_Member__c>();

        for (Study_Team_Member__c item : [
            SELECT Id, VTD2_TMA_Email__c, User__c
            FROM Study_Team_Member__c
            WHERE User__c IN :userMap.keySet()
        ]) {
            if (item.VTD2_TMA_Email__c != userMap.get(item.User__c).Email) {
                item.VTD2_TMA_Email__c = userMap.get(item.User__c).Email;
                membersToUpdate.add(item);
            }
        }

        if (! membersToUpdate.isEmpty()) { update membersToUpdate; }
    }

    global void finish(Database.BatchableContext BC) {

    }
}