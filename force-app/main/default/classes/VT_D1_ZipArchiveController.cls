global with sharing class VT_D1_ZipArchiveController {
    @AuraEnabled
    public static void createZipByRecord(Id recordId){
    	system.debug('!!! createZipByRecord');
    	createZipByRecordFuture(recordId);
    	system.debug('!!! after invoke future');
    }
    @future
    public static void createZipByRecordFuture(Id recordId){
    	system.debug('!!! createZipByRecordFuture');
    	system.debug('!!! recordId='+recordId);
    	
    	
    	List<ContentDocumentLink> fileLinks = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId=:recordId];
       
       	if(fileLinks==null || fileLinks.isEmpty()) return;// 'No records found.';
       	
       	set<Id> setDocumentIds = new set<Id>();
       	for(ContentDocumentLink fileLink: fileLinks){
        	setDocumentIds.add(fileLink.ContentDocumentId);
    	}
    	
    	List<ContentVersion> contVersionList = [select Id, Title, PathOnCLient, VersionData from ContentVersion where ContentDocumentId in: setDocumentIds];
       	
       	if(contVersionList==null || contVersionList.isEmpty()) return;// 'No records found.';
       	
       	Zippex sampleZip = new Zippex();
       	
       	for(ContentVersion cv:contVersionList){
       		Blob fileData = cv.VersionData;//EncodingUtil.base64Decode(cv.VersionData);
       		sampleZip.addFile('sampleFolder/'+cv.PathOnCLient, fileData, null);
       	}
       	
       	Blob zipData = sampleZip.getZipArchive();
       	
       	ContentVersion cvZip = new ContentVersion();
        cvZip.ContentLocation = 'S';
        //cvZip.ContentDocumentId = contentDocumentId;
        cvZip.VersionData = zipData;//EncodingUtil.base64Decode(base64Data);//base64Data
        cvZip.Title = 'archiveFiles.zip';
        cvZip.PathOnClient = 'archiveFiles';
        insert cvZip;
        
        ContentVersion cv = [select Id, ContentDocumentId from ContentVersion where Id=:cvZip.Id];
        
        ContentDocumentLink cdl = new ContentDocumentLink();
		cdl.ContentDocumentId = cv.ContentDocumentId;
		cdl.LinkedEntityId = recordId;
		cdl.ShareType = 'I';
        insert cdl;
       	
       	//return 'OK';
       	
    }
}