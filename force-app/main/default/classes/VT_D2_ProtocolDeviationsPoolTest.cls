/**
* @author: Carl Judge
* @date: 31-Jan-19
* @description: Test for VT_D2_ProtocolDeviationsPool
**/

@IsTest
public class VT_D2_ProtocolDeviationsPoolTest {

    class QueueSobjectCreator implements Queueable {
        final Id queueId;
        public QueueSobjectCreator(Id queueId) {
            this.queueId = queueId;
        }
        public void execute(QueueableContext qc) {
            insert new QueueSobject(SobjectType='VTD1_Protocol_Deviation__c', QueueId=queueId);
        }
    }

    @TestSetup
    private static void setupMethod() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);

        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();

        //Group regularGroup = [SELECT Id FROM Group WHERE DeveloperName = 'Custom_Internal_Users'];
        //Group queue = [SELECT Id FROM Group WHERE DeveloperName = 'VTD1_CM_Pool'];
        Group queue = new Group(Type='Queue', Name='CMTaskQueue');
        Group regularGroup = new Group(Type='Regular', Name='RegularGroup');
        insert new List<Group>{queue, regularGroup};
        //insert new QueueSobject(SobjectType='VTD1_Protocol_Deviation__c', QueueId=queue.Id);
        System.enqueueJob(new QueueSobjectCreator(queue.Id));

        System.runAs(new User(Id = UserInfo.getUserId())) {
            insert new GroupMember(UserOrGroupId = UserInfo.getUserId(), GroupId = regularGroup.Id);
            insert new GroupMember(UserOrGroupId = regularGroup.Id, GroupId = queue.Id);
        }

        /*insert new VTD1_Protocol_Deviation__c(
            OwnerId = queue.Id
        );*/
    }

    @IsTest
    private static void doTest() {
        Group queue = [SELECT Id FROM Group WHERE Name = 'CMTaskQueue'];
        insert new VTD1_Protocol_Deviation__c(OwnerId = queue.Id);

        List<VTD1_Protocol_Deviation__c> deviations = VT_D2_ProtocolDeviationsPool.getProtocolDeviations();
        List<Id> devIds = new List<Id>(new Map<Id, VTD1_Protocol_Deviation__c>(deviations).keySet());
        VT_D2_ProtocolDeviationsPool.claimProtocolDeviations(devIds);
        VT_D2_ProtocolDeviationsPool.getDeviationCategories();
    }
}