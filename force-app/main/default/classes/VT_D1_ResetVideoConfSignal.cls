public with sharing class VT_D1_ResetVideoConfSignal {
    @InvocableMethod
    public static void resetSignalToNotConnectedParticipants(List<Id> usersList){
    	if(usersList==null || usersList.isEmpty()) return;
    	
    	Set<Id> participantsSetIds = new Set<Id>(usersList);
    	
    	//reset signal
    	VT_D1_VideoConfSignalHandler.sendSignal(participantsSetIds, false);
    }
}