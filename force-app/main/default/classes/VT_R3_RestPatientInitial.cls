/**
 * @description Created by Alexander Komarov on 05.06.2019.
 */
@RestResource(UrlMapping='/Patient/Initial/')
global with sharing class VT_R3_RestPatientInitial {
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();
    @HttpGet
    global static String getInitialData() {
        InitialData initialData = new InitialData();
        initialData.videoAPIKey = [SELECT VTR2_Video_APIKey__c FROM VTD1_General_Settings__mdt LIMIT 1].VTR2_Video_APIKey__c;
        initialData.userProfile = [SELECT Profile.Name FROM User WHERE Id =:UserInfo.getUserId()].Profile.Name;

        cr.serviceResponse = initialData;

        Case currentUserCase = VT_D1_PatientCaregiverBound.getCaseForUser();

        if (currentUserCase != null) {
            Case selectedAgainCaseCozCodeDuplication = [SELECT Id,VTD1_Subject_ID__c,VTD1_Study__c FROM Case WHERE Id = :currentUserCase.Id];
            initialData.subjectId = selectedAgainCaseCozCodeDuplication.VTD1_Subject_ID__c;
            initialData.studyId = selectedAgainCaseCozCodeDuplication.VTD1_Study__c;
        }



        return JSON.serialize(cr, true);
    }
    private class InitialData {
        public String videoAPIKey;
        public String userProfile;
        public String subjectId;
        public String studyId;
        public final String userFirstName = UserInfo.getFirstName();
        public final String userLastName = UserInfo.getLastName();
        public final String userLanguage = UserInfo.getLanguage();
        public final String username = UserInfo.getUserName();
    }
    private static void addCaseData(InitialData initialData) {
        Case currentUserCase = VT_D1_PatientCaregiverBound.getCaseForUser();
        Case selectedAgainCaseCozCodeDuplication = [SELECT Id,VTD1_Subject_ID__c,VTD1_Study__c  FROM Case WHERE Id = :currentUserCase.Id];
        initialData.subjectId = selectedAgainCaseCozCodeDuplication.VTD1_Subject_ID__c;
        initialData.studyId = selectedAgainCaseCozCodeDuplication.VTD1_Study__c;
    }
}