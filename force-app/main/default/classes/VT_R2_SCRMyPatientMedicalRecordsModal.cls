public with sharing class VT_R2_SCRMyPatientMedicalRecordsModal {

    public class DocumentWrapper {
        public String contentDocumentId;
        public DocumentData documentData;
    }

    public class DocumentData {
        public String nickname;
        public String comments;
    }

    @AuraEnabled
    public static Boolean deleteDocumentByContentDocumentId(Id contentDocumentId) {
        try {
            VTD1_Document__c document = getDocumentByContentDocumentId(contentDocumentId);
            return Database.delete(document).isSuccess();
        } catch (Exception exc) {
            throw new AuraHandledException(exc.getMessage() + '\n' + exc.getStackTraceString());
        }
    }

    @AuraEnabled
    public static Boolean updateDocument(String wrapper) {
        try {
            DocumentWrapper documentWrapper = (DocumentWrapper) JSON.deserializeStrict(wrapper, DocumentWrapper.class);
            VTD1_Document__c document = getDocumentByContentDocumentId(documentWrapper.contentDocumentId);
            document.VTD1_Nickname__c = documentWrapper.documentData.nickname;
            document.VTD1_Comment__c = documentWrapper.documentData.comments;
            document.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD;
            document.VTD1_Flagged_for_Deletion__c = false;
            return VTR2_SCRPatientDocumentDetails.updateDocument(document);
        } catch (Exception exc) {
            throw new AuraHandledException(exc.getMessage() + '\n' + exc.getStackTraceString());
        }
    }

    @TestVisible
    private static VTD1_Document__c getDocumentByContentDocumentId(Id contentDocumentId) {
        try {
            List<ContentDocumentLink> contentDocumentLinks = [
                    SELECT LinkedEntityId, LinkedEntity.Type
                    FROM ContentDocumentLink
                    WHERE ContentDocumentId = :contentDocumentId
            ];
            Id docId;
            for (ContentDocumentLink link : contentDocumentLinks) {
                if (link.LinkedEntity.Type == 'VTD1_Document__c') {
                    docId = link.LinkedEntityId;
                    break;
                }
            }
            return [
                    SELECT Id, VTD1_Nickname__c, VTD1_Comments__c
                    FROM VTD1_Document__c
                    WHERE Id = :docId
                    LIMIT 1
            ][0];
        } catch (Exception exc) {
            throw new AuraHandledException(exc.getMessage() + '\n' + exc.getStackTraceString());
        }
    }

}