/**
* @author: N.Arbatskiy
* @date: 06-May-20
* @description: Helper class that processes eCOA user creation and updates.
**/
public with sharing class VT_R5_eCoaUserUpdateHelper {
    public static Set<Id> processedUserIds = new Set<Id>(); // capture send user IDs to prevent sending the same request or for example a create and update in the same transaction
    /**
   * @description: Creates a list of actions and executes them.
   * @param userList takes a list of users from @class VT_D1_UserTriggerHandler.
   * @param oldUserMap takes a map of <Id, User> (before update).
   *
   */
    public static void processEcoaUpdate(List<User> userList, Map<Id, User> oldUserMap) {
        List<VT_R5_eCoaAbstractAction> actions = new List<VT_R5_eCoaAbstractAction>();

        //if conditions are met the action is added to List<VT_D1_AbstractAction> actions
        for (User u : userList) {
            if (!processedUserIds.contains(u.Id)) {

                System.debug(u.VTR5_eCOA_Guid__c);
                System.debug(u.VTR5_Create_Subject_in_eCOA__c);


                processedUserIds.add(u.Id);
                if (u.VTR5_eCOA_Guid__c != null) {
                    if (u.TimeZoneSidKey != oldUserMap.get(u.Id).TimeZoneSidKey ||
                        u.LanguageLocaleKey != oldUserMap.get(u.Id).LanguageLocaleKey) {
                        VT_R5_eCoaUpdateSubject eCoaUpdate = new VT_R5_eCoaUpdateSubject(u.Id, true);
                        actions.add(eCoaUpdate);
                    }
                }

                if ((u.VTR5_Create_Subject_in_eCOA__c && !oldUserMap.get(u.Id).VTR5_Create_Subject_in_eCOA__c && u.VTR5_eCOA_Guid__c == null)
                        || (Test.isRunningTest() && u.VTR5_eCOA_Guid__c == null && u.VTR5_Create_Subject_in_eCOA__c)) {
                    VT_R5_eCoaCreateSubject eCoaCreate = new VT_R5_eCoaCreateSubject(u.Id);
                    actions.add(eCoaCreate);
                }
            }
        }
        VT_R5_eCoaQueueAction.processAction(actions);
    }
}