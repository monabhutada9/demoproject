/**
* @author: Carl Judge
* @date: 19-Feb-19
* @description: Create milestones from Virtual Site updates
**/

public without sharing class VT_R2_VirtualSiteMilestoneHandler {

    private VT_D1_MilestoneCreator milestoneCreator;

    public void createMilestones(List<Virtual_Site__c> recs, Map<Id, Virtual_Site__c> oldMap) {
        this.milestoneCreator = new VT_D1_MilestoneCreator();
        List<Id> closedSitesStudyIds = new List<Id>();
        List<Id> closedOrClosedEnrolledSitesStudyIds = new List<Id>();

        for (Virtual_Site__c item : recs) {
            Virtual_Site__c old = oldMap.get(item.Id);
            if (VT_D1_MilestoneHelper.isSiteActivated(item) && !VT_D1_MilestoneHelper.isSiteActivated(old)) {
                this.milestoneCreator.addPotentialStudyMilestone(item.VTD1_Study__c, VT_R4_ConstantsHelper_AccountContactCase.STUDY_FIRST_VIRTUAL_SITE_ACTIVATED);
            }
            if (VT_D1_MilestoneHelper.isSiteClosed(item) && !VT_D1_MilestoneHelper.isSiteClosed(old)) {
                closedSitesStudyIds.add(item.VTD1_Study__c);
            }
            if (VT_D1_MilestoneHelper.isSiteClosedOrClosedEnrollment(item) && !VT_D1_MilestoneHelper.isSiteClosedOrClosedEnrollment(old)) {
                closedOrClosedEnrolledSitesStudyIds.add(item.VTD1_Study__c);
            }
        }

        if (!closedSitesStudyIds.isEmpty() || !closedOrClosedEnrolledSitesStudyIds.isEmpty()) {
            addIdsForStudiesWithClosedSites(closedSitesStudyIds, closedOrClosedEnrolledSitesStudyIds);
        }

        this.milestoneCreator.createMilestones();
    }

    private void addIdsForStudiesWithClosedSites(List<Id> closeIds, List<Id> closeOrCloseEnrolledIds) {
        Map<Id, HealthCloudGA__CarePlanTemplate__c> studyMap = new Map<Id, HealthCloudGA__CarePlanTemplate__c>([
            SELECT Id, (
                SELECT VTD1_Site_Status__c
                FROM Virtual_Sites__r
                WHERE VTD1_Site_Status__c NOT IN :VT_D1_MilestoneHelper.SITE_CLOSED_STATUS
            )
            FROM HealthCloudGA__CarePlanTemplate__c
            WHERE Id IN :closeIds
            OR Id IN :closeOrCloseEnrolledIds
        ]);

        for (Id studyId : closeIds) {
            if (studyMap.get(studyId).Virtual_Sites__r.isEmpty()) {
                this.milestoneCreator.studiesWithAllClosedSites.add(studyId);
            }
        }

        for (Id studyId : closeOrCloseEnrolledIds) {
            Boolean toAdd = true;
            if (!studyMap.get(studyId).Virtual_Sites__r.isEmpty()) {
                for (Virtual_Site__c site : studyMap.get(studyId).Virtual_Sites__r) {
                    if (site.VTD1_Site_Status__c == null ||
                        !site.VTD1_Site_Status__c.equalsIgnoreCase(VT_R4_ConstantsHelper_AccountContactCase.SITE_ENROLLMENT_CLOSED)
                        ) {
                        toAdd = false;
                        break;
                    }
                }
            }
            if (toAdd) { this.milestoneCreator.studiesWithClosedAllReasonsSites.add(studyId); }
        }
    }
}