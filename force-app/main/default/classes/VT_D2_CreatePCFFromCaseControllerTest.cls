/**
* @author: Carl Judge
* @date: 25-Jan-19
* @description: Test for VT_D2_CreatePCFFromCaseController
**/

@IsTest
public with sharing class VT_D2_CreatePCFFromCaseControllerTest {
    
   
    public static void doTest() {
        Case carePlan = [SELECT Id, VTD1_Study__c, VTD1_Patient_User__c, VTD1_Patient__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];
		


        Case pcf = new Case(
                RecordTypeId = [Select Id from RecordType where DeveloperName = 'VTD1_PCF'].id,
                Priority = 'Medium',
                Status = 'Open',
                VTD1_PCF_Reason_for_Contact__c = 'test',
                VTD2_Reason_Safety_Concern_Indicator_Cha__c = 'No', VTR5_Stop_eDiary_Schedule__c = true,
                VTD1_Clinical_Study_Membership__c = carePlan.id
        );


        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c(
                Id = carePlan.VTD1_Study__c,
                eCOA_Stop_Schedule_Event__c = 'stop_sic'
        );


        update study;
        
        Test.startTest();
        VT_D2_CreatePCFFromCaseController.getPcfRecTypeId();
        VT_D2_CreatePCFFromCaseController.getContactIds(carePlan.Id);

        


        VT_D2_CreatePCFFromCaseController.insertNewCaseRecord(pcf);

        VT_D2_CreatePCFFromCaseController.insertNewCaseRecord(null);

        
         VT_D2_CreatePCFFromCaseController.insertNewCaseRecord(null);

        
         VT_D2_CreatePCFFromCaseController.insertNewCaseRecord(null);
        
        VT_D2_CreatePCFFromCaseController.getCaseId(carePlan.VTD1_Patient__c);
        Test.stopTest();



        pcf = [SELECT Id, VTR5_Stop_eDiary_Schedule__c FROM Case WHERE Id = :pcf.Id];
        System.assertEquals(true, pcf.VTR5_Stop_eDiary_Schedule__c);


    }
}