@isTest
public class VT_R5_RunFlowTest {
    @IsTest
    static void runMonitoring_Visit_Report_Reviewer_to_review_the_Interim_Visit_Report() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            myMap.put('userId', UserInfo.getUserId());
            new Flow.Interview.Monitoring_Visit_Report_Reviewer_to_review_the_Interim_Visit_Report(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
		Test.stopTest();
    }
    
    @IsTest
    static void runVTD2_Caregiver_User_update() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            myMap.put('varContactId', '');
            new Flow.Interview.VTD2_Caregiver_User_update(myMap).start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }

        Test.stopTest();
    }
    
    @IsTest
    static void runCaregiver_Update_UserId_on_Contact() {
        Test.startTest();
        try {
            Map<String, Object> myMap = new Map<String, Object>();
            myMap.put('accountId', '');
            myMap.put('recordTypeCarePlanId', '');
            new Flow.Interview.Caregiver_Update_UserId_on_Contact(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }

        Test.stopTest();
    }
    
    @IsTest
    static void runDelete_Exiting_Visit_Member() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            myMap.put('NewVisitMember.Id', '');
            myMap.put('NewVisitMember.VTD1_Actual_Visit__c', '');
            myMap.put('NewVisitMember.VTD1_External_Participant_Type__c', 'Patient Guide');
            new Flow.Interview.Delete_Exiting_Visit_Member(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }

    @IsTest
    static void runVTD2_eDiary_Scheduling() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            myMap.put('eDiaryFlag', true);
            myMap.put('vareDiaryInput', '');
            myMap.put('varProtocolSubject', UserInfo.getUserId());
            Flow.Interview.VTD2_eDiary_Scheduling fl = new Flow.Interview.VTD2_eDiary_Scheduling(myMap);
            fl.start();
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }

        Test.stopTest();
        //myFlow.start();
    }

    @IsTest
    static void runMonitoring_Visit_Report_Status_Update_to_Submitted() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Monitoring_Visit_Report_Status_Update_to_Submitted(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }

        Test.stopTest();
    }

    @IsTest
    static void testVTR2_Share_Patient_Task_to_Caregiver() {




        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTR2_Share_Patient_Task_to_Caregiver(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }

        Test.stopTest();
    }



    @IsTest
    static void testDisable_Caregivers() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            myMap.put('varDisableAccId', '');
            new Flow.Interview.Disable_Caregivers(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }

        Test.stopTest();
    }
    @IsTest
    static void testScheduling_Visit() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Scheduling_Visit(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }

        Test.stopTest();
    }
    @IsTest
    static void testGenerate_Video_Conference() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Generate_Video_Conference(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testRun_consent_re_consent_event() {
        //TODO
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            myMap.put('userId', '');
            myMap.put('visitId', '');
            myMap.put('vScheduledForDateTime', '');
            new Flow.Interview.Run_consent_re_consent_event(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testClose_Secure_Consent_ID_task() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Close_Secure_Consent_ID_task(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testMonitoring_Visit_Follow_Up_Letter_Submission() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Monitoring_Visit_Follow_Up_Letter_Submission(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testMonitoring_Visit_Email_to_CRA() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Monitoring_Visit_Email_to_CRA(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testVTR3_Medical_Records_process() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTR3_Medical_Records_process(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testVTD2_Link_Reg_Binder_To_EAF() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTD2_Link_Reg_Binder_To_EAF(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testVISIT_EMAIL_4_Patient_Reschedule() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VISIT_EMAIL_4_Patient_Reschedule(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testVISIT_EMAIL_1_Notify_Patient_when_visit_was_scheduled() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VISIT_EMAIL_1_Notify_Patient_when_visit_was_scheduled(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testVTR2_PCF_SC_Escalation_Process_NotificationC_PostToChatter() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTR2_PCF_SC_Escalation_Process_NotificationC_PostToChatter(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testPCF_SC_Escalation_Process_Send_email_PI() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.PCF_SC_Escalation_Process_Send_email_PI(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testClose_PI_EAF_Task() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Close_PI_EAF_Task(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testClose_PI_Final_Eligibility_Task() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Close_PI_Final_Eligibility_Task(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testClose_TMA_task() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Close_TMA_task(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }

    @IsTest
    static void testVTD2_Visit_Email_Notifications_for_Patient() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTD2_Visit_Email_Notifications_for_Patient(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testUpdate_Scheduled_Event() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Update_Scheduled_Event(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testVTD2_TMA_emailed_to_review_Patient_eligibility_decision() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTD2_TMA_emailed_to_review_Patient_eligibility_decision(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testVTD1_Televisit_Buffer_on_VC() {
        //TODO
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            myMap.put('varVisitId', '');
            myMap.put('varStartTime', '');
            myMap.put('varConferenceId', '');
            myMap.put('loopUserId', '');
            new Flow.Interview.VTD1_Televisit_Buffer_on_VC(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testTelevisit_Buffer_off_on_Video_Conference() {
        //TODO
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            myMap.put('varVisitId', '');
            new Flow.Interview.Televisit_Buffer_off_on_Video_Conference(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testVTR2_Task_Close_Schedule_Closeout_Visit_Task() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTR2_Task_Close_Schedule_Closeout_Visit_Task(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testVTD2_Task_Schedule_an_End_of_Study_Visit_task() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTD2_Task_Schedule_an_End_of_Study_Visit_task(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testStudy_Team_Member_Sharing() {
        //TODO
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Study_Team_Member_Sharing(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testVTD2_Study_Closeout_Delete_associated_future_Patients_Visits() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTD2_Study_Closeout_Delete_associated_future_Patients_Visits(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testVTD2_Study_Closeout_Delete_associated_Not_Started_ePROs_and_Payments() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTD2_Study_Closeout_Delete_associated_Not_Started_ePROs_and_Payments(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testSend_Signal_About_Video_Conf_Starting() {
        //TODO
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Send_Signal_About_Video_Conf_Starting(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testSchedule_Visits_Task_Notification_Flow() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Schedule_Visits_Task_Notification_Flow(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testVTR2_ReviewKitComments_SendSCRemail() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTR2_ReviewKitComments_SendSCRemail(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testPD_Form_Subject_PD_Creation_notification() {
        //TODO
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.PD_Form_Subject_PD_Creation_notification(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testPCF_SC_TMA_Task_Queue_Email() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.PCF_SC_TMA_Task_Queue_Email(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testVTR3_PCF_SC_Reminders_Sender_Helper() {
        //TODO
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTR3_PCF_SC_Reminders_Sender_Helper(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testPatient_Payment_Case_Compensation_Calculation_on_Study() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Patient_Payment_Case_Compensation_Calculation_on_Study(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testVTD2_case_share_access_to_TMA() {
        //TODO
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.VTD2_case_share_access_to_TMA(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testPatient_Payment_Case_Reimbursement_Calculation_on_Study() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Patient_Payment_Case_Reimbursement_Calculation_on_Study(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }
    @IsTest
    static void testNotify_Patient_Guide_when_the_Patient_changes_State_Flow() {
        Test.startTest();
        try{
            Map<String, Object> myMap = new Map<String, Object>();
            new Flow.Interview.Notify_Patient_Guide_when_the_Patient_changes_State_Flow(myMap).start();
        } catch (Exception e) {
            e.getStackTraceString();
        }
        Test.stopTest();
    }

}