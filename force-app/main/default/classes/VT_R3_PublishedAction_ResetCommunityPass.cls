/**
* @author: Carl Judge
* @date: 22-Nov-19
**/

public without sharing class VT_R3_PublishedAction_ResetCommunityPass implements Callable{
    List<Id> userIds;

    public VT_R3_PublishedAction_ResetCommunityPass(List<Id> userIds) {
        this.userIds = userIds;
    }

    public VT_R3_PublishedAction_ResetCommunityPass() {
    }

    public static Type getType() {
        return VT_R3_PublishedAction_ResetCommunityPass.class;
    }

    public static Object call(String action, Map<String, Object> args){
        if(action == 'processEvents') {
            List<VTR4_New_Transaction__e> events = (List<VTR4_New_Transaction__e>) args.get('events');
            List<Id> userIds = (List<Id>) JSON.deserialize(events[0].VTR4_Parameters__c, List<Id>.class);
            Map<String, String> welcomeTemplatesByProfile = getWelcomeTemplatesByProfile();
            for (User usr : [SELECT Id, Profile.Name FROM User WHERE Id IN :userIds]) {
                if (welcomeTemplatesByProfile.containsKey(usr.Profile.Name)) {
                    String usrId = usr.Id;
                    String welcomeTemplatesByProfile1 = welcomeTemplatesByProfile.get(usr.Profile.Name);
                    System.resetPasswordWithEmailTemplate(usrId, true, welcomeTemplatesByProfile1);
                }
            }
        }
        return null;
    }

    public void publish() {
        VTR4_New_Transaction__e eventToStartActionsExecution = new VTR4_New_Transaction__e(
                VTR4_HandlerName__c = getType().getName(),
                VTR4_Parameters__c = JSON.serialize(userIds)
        );
        if(Test.isRunningTest()){
            List<VTR4_New_Transaction__e> eventsList = new List<VTR4_New_Transaction__e>{eventToStartActionsExecution};
            call('processEvents', new Map<String, Object>{'events' => eventsList});
        }
        else{
            EventBus.publish(eventToStartActionsExecution);
        }
    }

    private static Map<String, String> getWelcomeTemplatesByProfile() {
        Map<String, String> mapProfileTemplate = new Map<String, String>();
        for (VT_R3_WelcomeEmailTemplateforProfile__mdt item : [
            SELECT VTR3_EmailTemplate__c,VTR3_UserProfile__c
            FROM VT_R3_WelcomeEmailTemplateforProfile__mdt
        ]) {
            mapProfileTemplate.put(item.VTR3_UserProfile__c, item.VTR3_EmailTemplate__c);
        }
        return mapProfileTemplate;
    }
}