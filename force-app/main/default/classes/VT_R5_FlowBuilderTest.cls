/**
 * @author:         Rishat Shamratov
 * @date:           12.08.2020
 * @description:    Test class for flows created by Flow Builder
 */

@IsTest
public class VT_R5_FlowBuilderTest {
    @TestSetup
    public static void testSetup() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        DomainObjects.Study_t domainStudy = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));
        domainStudy.persist();

        DomainObjects.User_t domainCRAUser = new DomainObjects.User_t()
                .setEmail(VT_D1_TestUtils.generateUniqueUserName())
                .setProfile(VT_R4_ConstantsHelper_Profiles.CRA_PROFILE_NAME);
        domainCRAUser.persist();

        DomainObjects.User_t domainPGUser = new DomainObjects.User_t()
                .setEmail(VT_D1_TestUtils.generateUniqueUserName())
                .setProfile(VT_R4_ConstantsHelper_Profiles.PG_PROFILE_NAME);
        domainPGUser.persist();

        DomainObjects.User_t domainPIUser = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setEmail(VT_D1_TestUtils.generateUniqueUserName())
                .setProfile(VT_R4_ConstantsHelper_Profiles.PI_PROFILE_NAME);
        domainPIUser.persist();

        DomainObjects.User_t domainSCRUser = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setEmail(VT_D1_TestUtils.generateUniqueUserName())
                .setProfile(VT_R4_ConstantsHelper_Profiles.SCR_PROFILE_NAME);
        domainSCRUser.persist();

        DomainObjects.Case_t domainCarePlan = new DomainObjects.Case_t()
                .addPIUser(domainPIUser)
                .addSCRUser(domainSCRUser)
                .addStudy(domainStudy)
                .addVTD1_Primary_PG(domainPGUser)
                .setRecordTypeByName('CarePlan');
        domainCarePlan.persist();

        DomainObjects.StudyTeamMember_t domainCRAStudyTeamMember = new DomainObjects.StudyTeamMember_t()
                .addStudy(domainStudy)
                .addUser(domainCRAUser);
        domainCRAStudyTeamMember.persist();

        DomainObjects.StudyTeamMember_t domainPIStudyTeamMember = new DomainObjects.StudyTeamMember_t()
                .addStudy(domainStudy)
                .addUser(domainPIUser);
        //START RAJESH S_17440
                //.setRemoteCraId(domainCRAStudyTeamMember.Id);
        //END:SH-17440
        domainPIStudyTeamMember.persist();

        DomainObjects.VirtualSite_t domainVirtualSite = new DomainObjects.VirtualSite_t()
                .addStudy(domainStudy)
                .addStudyTeamMember(domainPIStudyTeamMember)
                .setStudySiteNumber('12345');
        domainVirtualSite.persist();

        DomainObjects.VTD1_Actual_Visit_t domainActualVisit = new DomainObjects.VTD1_Actual_Visit_t()
                .setVTD1_Case(domainCarePlan.Id);
        domainActualVisit.persist();
        Test.stopTest();
    }

    @IsTest
    static void amendment_Study_Admin_uploaded_documentsTest() { //17 of 19 elements
        Map<String, Object> myMap = new Map<String, Object>();
        myMap.put('varStudyId', [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1].Id);

        Test.startTest();
        try {
            new Flow.Interview.Amendment_Study_Admin_uploaded_documents(myMap).start();
        } catch (Exception e) {
            System.debug('Error: ' + e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest
    static void calculate_Task_Assignee_SCR_or_PGTest() { //5 of 7 elements
        VTD1_Actual_Visit__c actualVisit = [
                SELECT Id,
                        VTD1_Case__r.VTD1_Primary_PG__c,
                        VTD1_Case__r.VTR2_SiteCoordinator__c
                FROM VTD1_Actual_Visit__c
                LIMIT 1
        ];

        Map<String, Object> myMap = new Map<String, Object>();
        myMap.put('varCaseId', actualVisit.VTD1_Case__c);
        myMap.put('varPGId', actualVisit.VTD1_Case__r.VTD1_Primary_PG__c);
        myMap.put('varRecordId', actualVisit.Id);
        myMap.put('varSCRId', actualVisit.VTD1_Case__r.VTR2_SiteCoordinator__c);
        myMap.put('varSiteStaffRespForElgl', false);

        Test.startTest();
        try {
            new Flow.Interview.Calculate_Task_Assignee_SCR_or_PG(myMap).start();
        } catch (Exception e) {
            System.debug('Error: ' + e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest
    static void case_Increment_Patients_NumberTest() { //4 of 4 elements
        Map<String, Object> myMap = new Map<String, Object>();
        myMap.put('VarStudyId', [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1].Id);

        Test.startTest();
        try {
            new Flow.Interview.Case_Increment_Patients_Number(myMap).start();
        } catch (Exception e) {
            System.debug('Error: ' + e.getStackTraceString());
        }
        Test.stopTest();
    }

    @IsTest
    static void case_Update_PITest() { //6 of 7 elements
        Map<String, Object> myMap = new Map<String, Object>();

        Test.startTest();
        try {
            new Flow.Interview.Case_Update_PI(myMap).start();
        } catch (Exception e) {
            System.debug('Error: ' + e.getStackTraceString());
        }
        Test.stopTest();
    }
}