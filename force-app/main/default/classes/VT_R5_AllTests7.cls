/**
 * Created by Alexander on 2020-10-09.
 */
@IsTest
public with sharing class VT_R5_AllTests7 {

    @TestSetup
    static void setup() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }


    @IsTest
    static void VT_R3_RestPatientPaymentsTest() {
        VT_R3_RestPatientPaymentsTest.completePaymentTest();
    }
    @IsTest
    static void VT_R3_RestPatientPaymentsTest1() {
        VT_R3_RestPatientPaymentsTest.getPaymentsSumTest();
    }
    @IsTest
    static void VT_R3_RestPatientPaymentsTest2() {
        VT_R3_RestPatientPaymentsTest.getPaymentsTest();
    }
    @IsTest
    static void VT_D1_MedicalRecordsListTest() {
        VT_D1_MedicalRecordsListTest.getRecordsTest();
    }
    @IsTest
    static void VT_R3_RestPatientPrivacyNoticesTest() {
        VT_R3_RestPatientPrivacyNoticesTest.getPrivacyNoticesTest();
    }

    /*
    @IsTest

    static void VT_R5_GlobalSharing_Account_UserTest() {
        VT_R5_GlobalSharing_Account_UserTest.testAccountShares();
    }
    @IsTest
    static void VT_R5_GlobalSharing_Account_UserTest1() {
        VT_R5_GlobalSharing_Account_UserTest.createFromCarePlanIds();
    }

    @IsTest

    static void VT_R5_GlobalSharing_ActionItem_UserTest() {
        VT_R5_GlobalSharing_ActionItem_UserTest.testRecordSharing();
    }
    @IsTest
    static void VT_R5_GlobalSharing_ActionItem_UserTest1() {
        VT_R5_GlobalSharing_ActionItem_UserTest.testUserSharing();
    }*/

}