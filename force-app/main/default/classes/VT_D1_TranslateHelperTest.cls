@IsTest
public with sharing class VT_D1_TranslateHelperTest {
    public static void translateTest () {
        Account acc = new Account(Name = 'acc');
        insert acc;

        FeedItem fItem = new FeedItem(Body = 'Test', ParentId = acc.Id);
        insert fItem;

        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c();
        insert study;

        VTD1_Study_Stratification__c studyStrat = new VTD1_Study_Stratification__c(VTD1_Study__c = study.Id);
        insert studyStrat;

        Case cas = new Case();
        insert cas;


        insert new FeedComment(
                CommentBody = 'Test',
                FeedItemId = fItem.Id
        );
        insert new VTD1_OrderStage__c(
                VTD1_ExpectedDeliveryDate__c = Date.today(),
                VTD1_ProtocolId__c = '1234',
                VTD1_ShipmentId__c = '1234',
                VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED,
                VTD1_SubjectId__c = '1234',
                VTD1_ShipmentName__c = '1234'
        );
        insert new VTD1_Translation__c(
                VTD1_Record_Id__c = acc.Id,
                VTD1_Field_Name__c = 'Name',
                VTD1_Language__c = UserInfo.getLanguage(),
                VTD1_Object_Name__c = 'Account',
                VTD1_Value__c = 'accSpanish'
        );
        insert new VTD1_Patient_Stratification__c(
                VTD1_Patient__c = cas.Id,
                VTD1_Study_Stratification__c = studyStrat.Id
        );
        String queryStr = 'SELECT ' +
                'Id, ' +
                'Name ' +
                'FROM Account ';
        List<Account> accs = Database.query(queryStr);
        System.assertNotEquals(0, accs.size());
        VT_D1_TranslateHelper.translate(accs[0]);
    }

    public static void prepareLabelFieldsTest () {
        Account acc = new Account(Name = 'acc');
        insert acc;

        FeedItem fItem = new FeedItem(Body = 'Test', ParentId = acc.Id);
        insert fItem;

        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c();
        insert study;

        VTD1_Study_Stratification__c studyStrat = new VTD1_Study_Stratification__c(VTD1_Study__c = study.Id);
        insert studyStrat;

        Case cas = new Case();
        insert cas;


        insert new FeedComment(
                CommentBody = 'Test',
                FeedItemId = fItem.Id
        );
        insert new VTD1_OrderStage__c(
                VTD1_ExpectedDeliveryDate__c = Date.today(),
                VTD1_ProtocolId__c = '1234',
                VTD1_ShipmentId__c = '1234',
                VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED,
                VTD1_SubjectId__c = '1234',
                VTD1_ShipmentName__c = '1234'
        );
        insert new VTD1_Translation__c(
                VTD1_Record_Id__c = acc.Id,
                VTD1_Field_Name__c = 'Name',
                VTD1_Language__c = UserInfo.getLanguage(),
                VTD1_Object_Name__c = 'Account',
                VTD1_Value__c = 'accSpanish'
        );
        insert new VTD1_Patient_Stratification__c(
                VTD1_Patient__c = cas.Id,
                VTD1_Study_Stratification__c = studyStrat.Id
        );
        String queryStr = 'SELECT ' +
                'Id, ' +
                'Name ' +
                'FROM Account ';
        List<Account> accs = Database.query(queryStr);
        System.assertNotEquals(0, accs.size());
        VT_D1_TranslateHelper.prepareLabelFields(accs, new Set<String>{
                'Name'
        });
        VT_D1_TranslateHelper.getLabelValue('Name');
    }

    public static void translateNotificationsFutureTest () {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        User patient = [
                SELECT Id,
                        ContactId
                FROM User
                WHERE Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
                        AND IsActive = TRUE
                        AND ContactId != NULL
                LIMIT 1
        ];
        VTD1_NotificationC__c patientNotification = new VTD1_NotificationC__c(
                VTD1_Receivers__c = patient.Id,
                Number_of_unread_messages__c = 2,
                Title__c = 'Test',
                Message__c = 'test'
        );
        insert patientNotification;

        List<VTD1_NotificationC__c> notificationCS = [
                SELECT Id,
                        OwnerId,
                        Title__c,
                        Message__c,
                        VTD2_Title_Parameters__c,
                        VTD1_Parameters__c,
                        VTR5_Email_Body__c,
                        VTR5_Email_Body_Parameters__c,
                        VTR5_Email_Subject__c,
                        VTR5_Email_Subject_Parameters__c
                FROM VTD1_NotificationC__c
        ];

        System.assertNotEquals(0, notificationCS.size());
        VT_D1_TranslateHelper.translateNotificationsFuture(new Set<Id>{
                notificationCS[0].Id
        });
        VT_D1_TranslateHelper.translateAllfields(notificationCS, false);
    }

    public static void translateTasksTest () {
        Task task = new Task();
        task.VTD1_Type_for_backend_logic__c = 'Eligibility Asessment Task';
        task.Description = 'Eligibility Asessment Task';
        task.Subject = 'Call';
        task.ActivityDate = Date.today();
        insert task;
        String queryStr = 'SELECT ' +
                'Id, ' +
                'Subject, ' +
                'VTR5_Email_Body__c, ' +
                'VTR5_Email_Subject__c, ' +
                'Description ' +
                'FROM Task ';
        List<Task> tasks = Database.query(queryStr);
        System.assertNotEquals(0, tasks.size());
        VT_D1_TranslateHelper.translate(new List <SObject>());
        VT_D1_TranslateHelper.translate(tasks);
        VT_D1_TranslateHelper.translateAllFields(tasks);

        Integer d = task.ActivityDate.day();
        Integer m = task.ActivityDate.month();
        Integer y = task.ActivityDate.year();
        Datetime dt = Datetime.newInstance(y, m, d);

        System.assertNotEquals(null, VT_D1_TranslateHelper.translate(dt.format('EEEE, MMM, MMMM, dd-MMM-YYYY HH:mm a')));
    }

    public static void getCachedLabelsTest () {
        Map <String, String> result = VT_D1_TranslateHelper.getCachedLabels(new List <String>{
                'VTD1_Country'
        });
        System.assert(result.size() == 1);
        result = VT_D1_TranslateHelper.getCachedLabels(new List <String>{
                'VTD1_Country'
        }, 'es_AR', false);
        System.assert(result.size() == 1);
        result = VT_D1_TranslateHelper.getCachedLabels(new Map <String, String>{
                'VTD1_PasswordDontMatchError' => Label.VTD1_PasswordDontMatchError,
                'VTD1_PasswordError1Letter' => null
        });
        System.assert(result.size() == 2);
        result = VT_D1_TranslateHelper.getCachedLabels(new Map <String, String>{
                'VTD1_PasswordDontMatchError' => Label.VTD1_PasswordDontMatchError,
                'VTD1_PasswordError1Letter' => null
        }, 'en_US');
        System.assert(result.size() == 2);
        result = VT_D1_TranslateHelper.getCachedLabels(new Map <String, String>{
                'VTD1_PasswordDontMatchError' => Label.VTD1_PasswordDontMatchError,
                'VTD1_PasswordError1Letter' => null
        }, 'en_US', false);
        System.assert(result.size() == 2);
    }

    @IsTest
    static void basic_translate_test () {

        // When this makes a callout, it stores the bad string in the platform cache.
        Map<String, String> home_enUS = VT_D1_TranslateHelper.getCachedLabels(new List <String> {'VTD2_Home'}, 'en_US', false);
        System.assertEquals('Home', home_enUS.values()[0], 'Translated VTD2_Home to en_US Incorrectly');

        /*String home_frFR = VT_D1_TranslateHelper.getLabelValue('VTD2_Home', 'fr_FR');
        System.assertEquals('Accueil', home_frFR, 'Translated VTD2_Home to fr_FR Incorrectly');*/
    }
}