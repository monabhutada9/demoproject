/************************************************************************
 * Name  : ErrorLogUtility
 * Author: Shailesh B
 * Desc  : Test class for VT_R5_DeviceArchivalController
 * 
 *
 * Modification Log:
 * ----------------------------------------------------------------------
 * Developer                Date                description
 * ----------------------------------------------------------------------
 * Shailesh B             11/06/2020           Original 
 * 
 *************************************************************************/

@isTest
private class VT_R5_DeviceArchivalControllerTest {
   
    /* Author: Shailesh B
     * method : setup()
     * param : null
     * Description : Testmethod used to setup data
     * return :  void
     */
    @testSetup
     static void setup(){
            VT_R3_GlobalSharing.disableForTest = true;
            DomainObjects.Study_t study = new DomainObjects.Study_t()
                    .setMaximumDaysWithoutLoggingIn(1)
                    .addAccount(new DomainObjects.Account_t()
                            .setRecordTypeByName('Sponsor'));
            DomainObjects.User_t piUser = new DomainObjects.User_t()
                    .setCreatedDate(Date.today().addDays(-5));
            DomainObjects.User_t patientUser = new DomainObjects.User_t()
                    .setCreatedDate(Date.today().addDays(-5));
            DomainObjects.User_t pgUser = new DomainObjects.User_t()
                    .setCreatedDate(Date.today().addDays(-5));
            DomainObjects.User_t studyAdminUser = new DomainObjects.User_t()
                    .setUsername('studyAdmin@acme.com')
                    .setCreatedDate(Date.today().addDays(-5));
            DomainObjects.Contact_t piContact = new DomainObjects.Contact_t()
                    .addAccount(new DomainObjects.Account_t()
                            .setRecordTypeByName('Sponsor'));

            DomainObjects.StudyTeamMember_t piMemberT = new DomainObjects.StudyTeamMember_t()
                    .addStudy(study)
                    .addUser(piUser);

            new DomainObjects.Case_t()
                    .setRecordTypeByName('CarePlan')
                    .addStudy(study)
                    .addPIUser(piUser)
                    .addVTD1_Patient_User(patientUser)
                    .addStudyGeography(new DomainObjects.VTR2_Study_Geography_t()
                    .addVTD2_Study(study)
                    .setGeographicalRegion('Country')
                    .setCountry('US'))
                    .addVTD1_Primary_PG(pgUser)
                    .persist();
            Case caseRecord = [Select Id from Case where RecordType.DeveloperName = 'CarePlan' limit 1];
            VT_D1_TestUtils.createpatientDeviceConfig(null, caseRecord.Id, 'AppleWatch43-Series5');
            
    }


 /* Author: Shailesh B
     * method : deviceArchInsert()
     * param : null
     * Description : Test method used to insert device archival
     * return :  void
     */
    @IsTest
    private static void deviceArchInsert() {



    VTR5_DeviceReadingAddInfo__c deviceArch = new VTR5_DeviceReadingAddInfo__c();
    deviceArch.VTR5_Archived__c = true;
    deviceArch.VTR5_Comments__c = 'Hellow';
    deviceArch.VTR5_DeviceSerialNumber__c = 'AppleWatch43-Series5';
    deviceArch.VTR5_Reading_ID__c= '14955';
    

    test.startTest();
    //insert deviceArch;

        VT_R5_DeviceArchivalController.insertDeviceReadingRecords(deviceArch, true);
        VT_R5_DeviceArchivalController.insertDeviceReadingRecords(null, true);

    VT_R5_DeviceArchivalController.getContextUserUiThemeDisplayed();
    test.stopTest();

    //check the data validation using assert
    List<VTR5_DeviceReadingAddInfo__c> lstDeviceReading = [select Id, VTR5_Archived__c, VTR5_Comments__c,
                                                           VTR5_DeviceSerialNumber__c,
                                                           VTR5_Reading_ID__c from VTR5_DeviceReadingAddInfo__c ];
    System.debug(' lst size '+ lstDeviceReading.size());
    
    system.AssertEquals(lstDeviceReading.size(), 1);                                                       
    system.AssertEquals(lstDeviceReading[0].VTR5_Comments__c , 'Hellow');
    system.AssertEquals(lstDeviceReading[0].VTR5_DeviceSerialNumber__c , 'AppleWatch43-Series5');
    system.AssertEquals(lstDeviceReading[0].VTR5_Reading_ID__c, '14955'); 


    }

}