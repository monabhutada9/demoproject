/*
 * Mikhail Rzhevsky created by 28.06.2018 -->
 * Reassign Patient Contact Form project IQVIAVT-387
 * for test generation data called from VT_D1_Test_CaseTriggerHandler
*/
@isTest
public class VT_D1_Test_DataFactory {
	public class MyException extends Exception{}
	public static HealthCloudGA__CandidatePatient__c createTestStudyAndCPRecords() {
		
    	//Create Study 
    	HealthCloudGA__CarePlanTemplate__c study = VT_D1_Test_DataFactory.createStudy();
    	
    	//Create candidate patient with parent Account, Case and Contact
		//Important username = email, alias not more 5 chars and unic 
    	HealthCloudGA__CandidatePatient__c	candidatePatient = VT_D1_Test_DataFactory.createCandidatePatient(
    	'Eva','Jones','EvaBJones@jourrapide.com','EvaBJones@jourrapide.com','F4556','Converted',study.Id,
    	'4716','310-955-0028','US','NY');
    	return candidatePatient;
	}
	
	public 	static HealthCloudGA__CarePlanTemplate__c createStudy(){
		Account studyAccount = VT_D1_Test_DataFactory.createStudyAccount();
		HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c(
			Name = 'Test study reassing process',
			VTD1_Primary_Sponsor__c = studyAccount.Id, 
		    VTD1_Return_Process__c = 'Packaging Materials Needed',
		    VTD1_IMP_Return_Interval__c = 'Every 2nd',
		    VTD1_Return_IMP_To__c = 'Other',
		    VTD1_Lab_Return_Process__c = 'Courier Pickup',
		    VTD1_Lab_Return_Interval__c = 'Every 2nd',
		   	VTD1_Name__c='Test pick up address',
		   	VTD1_Address_Line_1__c = 'Test pick up address 1',
		   	VTD1_Address_Line_2__c  = 'Test pick up address 2',
		   	City_for_return__c = 'NY',
		   	State_or_return__c = 'NY',
		   	VTD1_ZIP__c = '666777',
		   	VTD1_Randomized__c ='No'	    
		);
		
    	try {
            upsert study;
        } catch(Exception e) {
            system.debug(e);
        }
    	return 	study; 
	}
	
	public  static Account createStudyAccount(){
		List<RecordType> rtStudy = [SELECT Id, Name FROM RecordType where DeveloperName ='Sponsor']; 
		Account acc = new Account(
			Name = 'Test Study Account',
			RecordTypeId = rtStudy[0].Id,
			VTD1_LegalFirstName__c = 'Test Study Account'
		);
	    try {
            upsert acc;
        } catch(Exception e) {
            system.debug(e);
        }	
    	return 	acc; 		
		
	}
	public 	static HealthCloudGA__CandidatePatient__c createCandidatePatient(String fName, String lName, String uName,
								String email, String alias, String convertion, Id stadyId, String protNumber,
								String phone, String addCountry, String addState){
    	HealthCloudGA__CandidatePatient__c	candidatePatient = new HealthCloudGA__CandidatePatient__c(
    		rr_firstName__c = fName,
    		rr_lastName__c  = lName,
    		rr_Username__c  = uName,
    		rr_Email__c     = email,
    		rr_Alias__c     = alias,
    		Conversion__c   = convertion,
    		Study__c		= stadyId,
    		VTD1_ProtocolNumber__c = protNumber,
    		VTD1_Patient_Phone__c  = phone,
    		HealthCloudGA__Address1Country__c = addCountry,
    		HealthCloudGA__Address1State__c = addState,
			HealthCloudGA__SourceSystemId__c = '123',
			HealthCloudGA__Address1PostalCode__c = '12345',
			VTD2_Language__c = 'en_US'
    	);
    	try {
            upsert candidatePatient;
        } catch(Exception e) {
            system.debug(e);
        }
                if(1==1)
        throw new MyException('7555'+stadyId+'|'+candidatePatient.Id);		
    	return 	candidatePatient;	
		
	}
}