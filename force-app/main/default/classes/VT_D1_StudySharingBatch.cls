global without sharing class VT_D1_StudySharingBatch /*implements Database.Batchable<sObject>, Database.Stateful*/ {

    /*

    public static final List<String> OBJECT_NAMES = new List<String>{
        'HealthCloudGA__CarePlanTemplate__c',
        'Case',
        'VTD1_Actual_Visit__c',
        'VTD1_Patient_Kit__c',
        'VTD1_Patient_Device__c',
        'VTD1_Patient_Accessories__c',
        'VTD1_Document__c',
        'VTD1_SC_Task__c',
        'VTD1_Protocol_Deviation__c',
        'VTD1_Patient_Lab_Sample__c',
        'Patient_LKC__c',
        'VTD1_Packaging_Item__c',
        'VTD1_Replacement_Item__c',
        'VTD1_Return_Item__c',
        'Video_Conference__c',
        'VTD1_Physician_Information__c',
        'VTD1_Monitoring_Visit__c'
   //     'VTD1_Protocol_Amendment__c'
    };

    public static Boolean updateStarted = false;

    public List<String> objNames;
    public Map<Id, Id> caregiverToPatient = new Map<Id, Id>();
    public List<Id> studyIds = new List<Id>();
    public Map<Id, Map<Id, Set<Id>>> validCasesByUserStudy = new Map<Id, Map<Id, Set<Id>>>();

    private List<VTD1_Study_Sharing_Configuration__c> configs;
    private String objName;
    private String caseField;

    global VT_D1_StudySharingBatch(List<VTD1_Study_Sharing_Configuration__c> configs) {
        this.configs = configs;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        if (this.objNames == null) { doInitialSetup(); }

        this.objName = this.objNames.remove(0);
        this.caseField = VT_D1_ShareUtilities.getFieldNameFromPath(getCaseFieldPath(this.objName));

        List<Id> studyIds = this.studyIds;
        String studyField = !VT_D1_FieldPaths.STUDY_PATHS.get(this.objName).equalsIgnoreCase('Id') ? ',' + VT_D1_FieldPaths.STUDY_PATHS.get(this.objName) : '';
        String caseField = this.caseField != null && !this.caseField.equalsIgnoreCase('Id') ? ',' + this.caseField : '';

        String query =
            'SELECT Id' + studyField + caseField
          + ' FROM ' + this.objName
          + ' WHERE ' + VT_D1_FieldPaths.STUDY_PATHS.get(this.objName) + ' IN :studyIds'
        ;

        if (this.objName == 'VTD1_Document__c') {
            query += ' AND (NOT RecordType.DeveloperName LIKE \'%Medical_Record%\')';
        }

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<sObject> scope){
        List<VTD1_Study_Sharing_Configuration__c> toRemoveAccess = new List<VTD1_Study_Sharing_Configuration__c>();
        List<VTD1_Study_Sharing_Configuration__c> toGrantAccess = new List<VTD1_Study_Sharing_Configuration__c>();

        for (VTD1_Study_Sharing_Configuration__c item : this.configs) {
            if (item.VTD1_Access_Level__c == 'None') {
                toRemoveAccess.add(item);
            } else {
                toGrantAccess.add(item);
            }
        }

        if (! toRemoveAccess.isEmpty()) {
            removeAccess(toRemoveAccess, scope);
        }
        if (! toGrantAccess.isEmpty()) {
            grantAccess(toGrantAccess, scope);
        }
    }

    global void finish(Database.BatchableContext bc){
        if (! this.objNames.isEmpty()) {
            launchNextBatch();
        } else {
            updateConfigLocks(false);
        }
    }

    private void doInitialSetup() {
        VT_D1_StudySharingBatch.updateStarted = true;
        this.configs = [SELECT Id, VTD1_User__c, VTD1_Study__c, VTD1_Access_Level__c, VTD1_Profile_Name__c FROM VTD1_Study_Sharing_Configuration__c WHERE Id IN :this.configs];
        updateConfigLocks(true);
        this.objNames = OBJECT_NAMES.clone();
        this.studyIds = getStudyIdsFromConfigs(this.configs);
        getCaregiverToPatientMap();
        getValidCases();
    }

    private void removeAccess(List<VTD1_Study_Sharing_Configuration__c> configs, List<sObject> scope) {
        List<Id> userIds = getUserIdsFromConfigs(configs);
        String query =
            'SELECT Id'
          + ' FROM ' + VT_D1_ShareUtilities.getShareObjectName(this.objName)
          + ' WHERE UserOrGroupId IN :userIds'
          + ' AND ' + VT_D1_ShareUtilities.getFieldNameFromPath(VT_D1_ShareUtilities.getShareObjectParentField(this.objName)) + ' IN :scope'
          + ' AND RowCause = \'Manual\''
        ;
        Database.delete(Database.query(query), false);
    }

    private void grantAccess(List<VTD1_Study_Sharing_Configuration__c> configs, List<sObject> scope) {
        Schema.SObjectType shareObjectType = Schema.getGlobalDescribe().get(VT_D1_ShareUtilities.getShareObjectName(this.objName));
        String shareObjectParentField = VT_D1_ShareUtilities.getShareObjectParentField(this.objName);
        String shareObjectAccessLevelField = VT_D1_ShareUtilities.getShareObjectAccessLevelField(this.objName);
        Map<Id, List<sObject>> scopeByStudyId = getScopeByStudyId(scope);
        List<sObject> newShares = new List<sObject>();

        for (VTD1_Study_Sharing_Configuration__c config : configs) {
            if (scopeByStudyId.containsKey(config.VTD1_Study__c)) {
                for (sObject objRec : scopeByStudyId.get(config.VTD1_Study__c)) {
                    if (this.caseField == null || (casesNeedToBeValidatedForConfig(config) && validateCaseAccess(config, objRec))) {
                        sObject newShare = shareObjectType.newSObject();
                        newShare.put(shareObjectParentField + 'Id', (Id)objRec.get('Id'));
                        newShare.put('UserOrGroupId', config.VTD1_User__c);
                        newShare.put(shareObjectAccessLevelField, config.VTD1_Access_Level__c);
                        newShare.put('RowCause', 'Manual');
                        newShares.add(newShare);
                    }
                }
            }
        }

        if (! newShares.isEmpty()) { Database.insert(newShares, false); }
    }

    private Map<Id, List<sObject>> getScopeByStudyId(List<sObject> scope) {
        Map<Id, List<sObject>> scopeByStudyId = new Map<Id, List<sObject>>();
        for (sObject item : scope) {
            Id studyId = (Id)VT_D1_HelperClass.getCrossObjectField(item, VT_D1_FieldPaths.STUDY_PATHS.get(this.objName));
            if (studyId != null) {
                if (! scopeByStudyId.containsKey(studyId)) {
                    scopeByStudyId.put(studyId, new List<sObject>());
                }
                scopeByStudyId.get(studyId).add(item);
            }
        }
        return scopeByStudyId;
    }

    private Boolean validateCaseAccess(VTD1_Study_Sharing_Configuration__c config, sObject objRec) {
        Id caseId = (Id)VT_D1_HelperClass.getCrossObjectField(objRec, this.caseField);
        return
            caseId != null &&
            this.validCasesByUserStudy.containsKey(config.VTD1_User__c) &&
            this.validCasesByUserStudy.get(config.VTD1_User__c).containsKey(config.VTD1_Study__c) &&
            this.validCasesByUserStudy.get(config.VTD1_User__c).get(config.VTD1_Study__c).contains(caseId)
        ;
    }

    private void updateConfigLocks(Boolean lockStatus) {
        for (VTD1_Study_Sharing_Configuration__c item : this.configs) {
            item.VTD1_Locked__c = lockStatus;
        }
        update this.configs;
    }

    private void getCaregiverToPatientMap() {
        List<Id> caregiverIds = new List<Id>();
        for (VTD1_Study_Sharing_Configuration__c item : this.configs) {
            if (item.VTD1_Profile_Name__c.equalsIgnoreCase(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME) &&
                item.VTD1_Access_Level__c != 'None')
            {
                caregiverIds.add(item.VTD1_User__c);
            }
        }

        if (!caregiverIds.isEmpty()) {
            List<User> caregivers = [SELECT Id, Contact.AccountId FROM User WHERE Id IN :caregiverIds];
            List<Id> accIds = new List<Id>();
            for (User item : caregivers) {
                if (item.Contact.AccountId != null) { accIds.add(item.Contact.AccountId); }
            }

            Map<Id, User> patientsByAccId = new Map<Id, User>();
            for (User item : [
                SELECT Id, Contact.AccountId
                FROM User
                WHERE Contact.AccountId IN :accIds
                    AND Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
            ]) {
                patientsByAccId.put(item.Contact.AccountId, item);
            }

            for (User caregiver : caregivers) {
                if (patientsByAccId.containsKey(caregiver.Contact.AccountId)) {
                    this.caregiverToPatient.put(caregiver.Id, patientsByAccId.get(caregiver.Contact.AccountId).Id);
                }
            }
        }
    }

    private void getValidCases() {
        List<VTD1_Study_Sharing_Configuration__c> configsToValidate = new List<VTD1_Study_Sharing_Configuration__c>();
        List<Id> studyIds = new List<Id>();
        for (VTD1_Study_Sharing_Configuration__c item : this.configs) {
            if (casesNeedToBeValidatedForConfig(item)) {
                configsToValidate.add(item);
                studyIds.add(item.VTD1_Study__c);
            }
        }

        if (!configsToValidate.isEmpty()) {
            List<Case> cases = [
                SELECT VTD1_Primary_PG__c, VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c, VTD1_Study__c,
                    VTD1_Secondary_PG__c, VTD1_PI_user__c, VTD1_Backup_PI_User__c, VTD1_Patient_User__c,
                    VTD1_Clinical_Study_Membership__r.VTD1_Secondary_PG__c, VTD1_Clinical_Study_Membership__r.VTD1_PI_user__c,
                    VTD1_Clinical_Study_Membership__r.VTD1_Backup_PI_User__c, VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c
                FROM Case
                WHERE VTD1_Study__c IN :studyIds
            ];

            for (VTD1_Study_Sharing_Configuration__c config : configsToValidate) {
                for (Case cas : cases) {
                    if (isCaseValidForConfig(cas, config)) {
                        addToValidCases(cas, config);
                    }
                }
            }
        }
    }

    private Boolean isCaseValidForConfig(Case cas, VTD1_Study_Sharing_Configuration__c config) {
        Boolean isValid = false;
        switch on config.VTD1_Profile_Name__c {
            when 'Patient Guide' {
                isValid =
                    cas.VTD1_Primary_PG__c == config.VTD1_User__c ||
                    cas.VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c == config.VTD1_User__c ||
                    cas.VTD1_Secondary_PG__c == config.VTD1_User__c ||
                    cas.VTD1_Clinical_Study_Membership__r.VTD1_Secondary_PG__c == config.VTD1_User__c;
            }
            when 'Primary Investigator' {
                isValid =
                    cas.VTD1_PI_user__c == config.VTD1_User__c ||
                    cas.VTD1_Clinical_Study_Membership__r.VTD1_PI_user__c == config.VTD1_User__c ||
                    cas.VTD1_Backup_PI_User__c == config.VTD1_User__c ||
                    cas.VTD1_Clinical_Study_Membership__r.VTD1_Backup_PI_User__c == config.VTD1_User__c;
            }
            when 'Patient' {
                isValid =
                    cas.VTD1_Patient_User__c == config.VTD1_User__c ||
                    cas.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c == config.VTD1_User__c;
            }
            when 'Caregiver' {
                isValid =
                    this.caregiverToPatient.containsKey(config.VTD1_User__c) && (
                        cas.VTD1_Patient_User__c == this.caregiverToPatient.get(config.VTD1_User__c) ||
                        cas.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c == this.caregiverToPatient.get(config.VTD1_User__c)
                    );
            }
        }
        return isValid;
    }

    private void addToValidCases(Case cas, VTD1_Study_Sharing_Configuration__c config) {
        if (!this.validCasesByUserStudy.containsKey(config.VTD1_User__c)) {
            this.validCasesByUserStudy.put(config.VTD1_User__c, new Map<Id, Set<Id>>());
        }
        if (!this.validCasesByUserStudy.get(config.VTD1_User__c).containsKey(cas.VTD1_Study__c)) {
            this.validCasesByUserStudy.get(config.VTD1_User__c).put(cas.VTD1_Study__c, new Set<Id>());
        }
        this.validCasesByUserStudy.get(config.VTD1_User__c).get(cas.VTD1_Study__c).add(cas.Id);
    }

    private Boolean casesNeedToBeValidatedForConfig(VTD1_Study_Sharing_Configuration__c config) {
        return config.VTD1_Access_Level__c != 'None' && (
            config.VTD1_Profile_Name__c.equalsIgnoreCase(VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME) ||
            config.VTD1_Profile_Name__c.equalsIgnoreCase(VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) ||
            config.VTD1_Profile_Name__c.equalsIgnoreCase(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) ||
            config.VTD1_Profile_Name__c.equalsIgnoreCase(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME)
        );
    }

    private void launchNextBatch() {
        VT_D1_StudySharingBatch nextIteration = new VT_D1_StudySharingBatch(this.configs);
        nextIteration.objNames = this.objNames;
        nextIteration.caregiverToPatient = this.caregiverToPatient;
        nextIteration.validCasesByUserStudy = this.validCasesByUserStudy;
        nextIteration.studyIds = this.studyIds;

        Database.executeBatch(nextIteration);
    }

    private String getCaseFieldPath(String objName) {
        if (objName.equalsIgnoreCase('Case')) {
            return 'Id';
        } else {
            return getCaseFieldPathFromSplitFieldPath(objName, VT_D1_FieldPaths.STUDY_PATHS.get(objName).split('\\.'), '');
        }
    }

    private String getCaseFieldPathFromSplitFieldPath(String objName, List<String> splitFieldPath, String caseFieldPath) {
        if (objName != null && objName.equalsIgnoreCase('Case')) {
            return caseFieldPath;
        } else if (splitFieldPath == null || splitFieldPath.size() <= 1) {
            return null;
        } else {
            String field = splitFieldPath.remove(0);
            if (caseFieldPath != '') { caseFieldPath += '.'; }
            caseFieldPath += field;
            String nextObjName = getNextObjName(objName, field);
            return getCaseFieldPathFromSplitFieldPath(nextObjName, splitFieldPath, caseFieldPath);
        }
    }

    private String getNextObjName(String objName, String field) {
        return Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap()
            .get(VT_D1_ShareUtilities.getFieldNameFromPath(field)).getDescribe().getReferenceTo()[0].getDescribe().getName();
    }



    private List<Id> getStudyIdsFromConfigs(List<VTD1_Study_Sharing_Configuration__c> configs) {
        List<Id> ids = new List<Id>();
        for (VTD1_Study_Sharing_Configuration__c item : configs) {
            ids.add(item.VTD1_Study__c);
        }
        return ids;
    }

    private List<Id> getUserIdsFromConfigs(List<VTD1_Study_Sharing_Configuration__c> configs) {
        List<Id> ids = new List<Id>();
        for (VTD1_Study_Sharing_Configuration__c item : configs) {
            ids.add(item.VTD1_User__c);
        }
        return ids;
    } */
}