public with sharing class VT_R2_DocuSignRecipientStatusHandler extends Handler {

    public static final String TN_CATALOG_CODE_FOR_TASK = '351';
    public static final String TN_CATALOG_CODE_FOR_NOTIFICATION = 'N046';
    public static final String COMPLETED_STATUS = 'Completed';
    public static final String DOA_LOG = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_DOA_LOG;

    public override void onAfterInsert(Handler.TriggerContext context) {
        new DocuSignTaskAndStatusManger(context).generateTasksAndNotifications();
    }

    public override void onAfterUpdate(Handler.TriggerContext context) {
        List<Task> tasksToClose =  new DocuSignTaskAndStatusManger(context).getTasksToClose();
        update tasksToClose;
    }

    public class DocuSignTaskAndStatusManger {

        private final List<dsfs__DocuSign_Recipient_Status__c> newList;

        public DocuSignTaskAndStatusManger(Handler.TriggerContext context) {
            this.newList = filterRecType(((List<dsfs__DocuSign_Recipient_Status__c>) context.newList));
        }

        public void generateTasksAndNotifications() {
            Set<String> userEmails = this.getUserEmails();
            List<User> users = this.getUsersByEmail(userEmails);
            if (users.isEmpty()) {
                return;
            }

            Map<Id, Id> documentIdByUserId = this.getDocumentIdByUserIdMap(users);
            for (Id userId : documentIdByUserId.keySet()) {
                List<Task> tasks = this.generateTasksThroughTnCatalog(documentIdByUserId, userId);
                this.generateNotificationsThroughTnCatalog(tasks);
            }
        }

        public List<Task> getTasksToClose() {
            Set<Id> documentIds = new Set<Id>();
            Set<String> emails = new Set<String>();
            Set<String> compoundForTask = new Set<String>();

            for (dsfs__DocuSign_Recipient_Status__c docuSignRecipientStatus : this.newList) {
                if (docuSignRecipientStatus.dsfs__Recipient_Status__c != COMPLETED_STATUS) {
                    continue;
                }

                String compoundKey = Id.valueOf(docuSignRecipientStatus.VTR2_Document__c) + docuSignRecipientStatus.dsfs__DocuSign_Recipient_Email__c;
                compoundForTask.add(compoundKey);
                documentIds.add(docuSignRecipientStatus.VTR2_Document__c);
                emails.add(docuSignRecipientStatus.dsfs__DocuSign_Recipient_Email__c);
            }

            Map<Id, String> emailByUserId = this.getEmailByUserIdMap(emails);
            List<Task> tasks = this.getTasksToClose(documentIds, emailByUserId.keySet());
            for (Task task : tasks) {
                String compoundKey = Id.valueOf(task.WhatId) + emailByUserId.get(task.OwnerId);
                if (compoundForTask.contains(compoundKey)) {
                    task.Status = COMPLETED_STATUS;
                }
            }
            return tasks;
        }

        private Set<String> getUserEmails() {
            Set<String> userEmails = new Set<String>();
            for (dsfs__DocuSign_Recipient_Status__c docuSignRecipientStatus : this.newList) {
                userEmails.add(docuSignRecipientStatus.dsfs__DocuSign_Recipient_Email__c);
            }
            return userEmails;
        }

        private List<User> getUsersByEmail(Set<String> userEmails) {
            return [
                    SELECT
                            Id
                            , Email
                    FROM User
                    WHERE Email IN :userEmails
            ];
        }

        private Map<Id, Id> getDocumentIdByUserIdMap(List<User> users) {
            Map<String, Id> documentIdByEmail = new Map<String, Id>();
            for (dsfs__DocuSign_Recipient_Status__c docuSignRecipientStatus : this.newList) {
                documentIdByEmail.put(docuSignRecipientStatus.dsfs__DocuSign_Recipient_Email__c, docuSignRecipientStatus.VTR2_Document__c);
            }
            Map<Id, Id> documentIdByUserId = new Map<Id, Id>();
            for (User user : users) {
                documentIdByUserId.put(user.Id, documentIdByEmail.get(user.Email));
            }
            return documentIdByUserId;
        }

        private List<Task> generateTasksThroughTnCatalog(Map<Id, Id> documentIdByUserId, Id userId) {
            Map <String, Id> assignedToMap = new Map<String, Id>();
            assignedToMap.put(TN_CATALOG_CODE_FOR_TASK + documentIdByUserId.get(userId), userId);
            List<Task> tasks = VT_D2_TNCatalogTasks.generateTasks(
                    new List<String>{
                            TN_CATALOG_CODE_FOR_TASK
                    },
                    new List<Id>{
                            documentIdByUserId.get(userId)
                    },
                    null,
                    assignedToMap
            );
            return tasks;
        }

        private void generateNotificationsThroughTnCatalog(List<Task> tasks) {
            if (!tasks.isEmpty()) {
                if (tasks[0].Id != null) {
                    VT_D2_TNCatalogNotifications.generateNotifications(new List<String>{
                            TN_CATALOG_CODE_FOR_NOTIFICATION
                    }, new List<Id>{
                            tasks[0].Id
                    }, null);
                }
            }
        }

        private Map<Id, String> getEmailByUserIdMap(Set<String> emails) {
            Map<Id, String> emailByUserId = new Map<Id, String>();
            for (User user : this.getUsersByEmail(emails)) {
                emailByUserId.put(user.Id, user.Email);
            }
            return emailByUserId;
        }

        private List<Task> getTasksToClose(Set<Id> documentIds, Set<Id> userIds) {
            return [
                    SELECT
                            OwnerId
                            , WhatId
                    FROM Task
                    WHERE OwnerId IN :userIds
                    AND WhatId IN :documentIds
            ];
        }


        private List<dsfs__DocuSign_Recipient_Status__c> filterRecType(List<dsfs__DocuSign_Recipient_Status__c> contextList) {
            Set<Id> recipientStatusIds = (new Map<Id, dsfs__DocuSign_Recipient_Status__c>(contextList)).keySet();
            Set<Id> doaLogRecipientStatusIds = new Set<Id>(new Map<Id, dsfs__DocuSign_Recipient_Status__c>(
            [
                    SELECT Id
                    FROM dsfs__DocuSign_Recipient_Status__c
                    WHERE Id IN:recipientStatusIds
                    AND (VTR2_Document__r.VTD1_Document_Type__c = :DOA_LOG OR VTR2_Document__r.VTD1_Regulatory_Document_Type__c = :DOA_LOG)
            ]).keySet());

            List<dsfs__DocuSign_Recipient_Status__c> filteredContextList = new List<dsfs__DocuSign_Recipient_Status__c>();
            for (dsfs__DocuSign_Recipient_Status__c status : contextList) {
                if (doaLogRecipientStatusIds.contains(status.Id)) {
                    filteredContextList.add(status);
                }
            }

            return filteredContextList;
        }
    }
}