/**
 * Created by shume on 15.07.2019.
 */

public without sharing class VT_R2_GlobalAdhocSharing {
    public class ParamsHolder {
        @InvocableVariable(Label = 'Case ID' Required = true)
        public Id caseId;

        @InvocableVariable(Label = 'Is Object Patient Related' Required = true)
        public Boolean isPatientRelatedObject;
    }

    @InvocableMethod
    public static List<List<User>> getActiveUsers(List<ParamsHolder> paramsHolders) {
        List<Id> patientRelatedCaseIds = new List<Id>();
        List<Id> studyRelatedCaseIds = new List<Id>();

        for (ParamsHolder params : paramsHolders) {
            (params.isPatientRelatedObject ? patientRelatedCaseIds : studyRelatedCaseIds).add(params.caseId);
        }

        Map<Id, List<User>> patientUsersByCaseId = new Map<Id, List<User>>();
        Map<Id, List<User>> studyUsersByCaseId = new Map<Id, List<User>>();
        if (!patientRelatedCaseIds.isEmpty()) { patientUsersByCaseId = getPatientRelatedUsers(patientRelatedCaseIds); }
        if (!studyRelatedCaseIds.isEmpty()) { studyUsersByCaseId = getStudyRelatedUsers(studyRelatedCaseIds); }

        List<List<User>> activeUsers = new List<List<User>>();
        for (ParamsHolder params : paramsHolders) {
            Map<Id, List<User>> usersByCaseId = params.isPatientRelatedObject ? patientUsersByCaseId : studyUsersByCaseId;
            activeUsers.add(usersByCaseId.get(params.caseId));
        }


        return activeUsers;
    }

    private static Map<Id, List<User>> getPatientRelatedUsers(List<Id> caseIds) {
        Map<Id, Map<Id, User>> usersByIdAndCaseId = new Map<Id, Map<Id, User>>();
        for (CaseShare share : [
            SELECT UserOrGroupId, CaseId
            FROM CaseShare
            WHERE CaseId IN :caseIds
            AND UserOrGroup.IsActive = true
            AND UserOrGroup.Type = 'User'
        ]) {
            if (!usersByIdAndCaseId.containsKey(share.CaseId)) {
                usersByIdAndCaseId.put(share.CaseId, new Map<Id, User>());
            }
            usersByIdAndCaseId.get(share.CaseId).put(share.UserOrGroupId, new User(Id = share.UserOrGroupId, IsActive = true));
        }

        Map<Id, List<User>> usersByCaseId = new Map<Id, List<User>>();
        for (Id caseId : caseIds) {
            usersByCaseId.put(caseId, usersByIdAndCaseId.containsKey(caseId) ?
                usersByIdAndCaseId.get(caseId).values() : new List<User>());
        }
        return usersByCaseId;
    }

    private static Map<Id, List<User>> getStudyRelatedUsers(List<Id> caseIds) {
        Map<Id, Id> caseToStudyIds = new Map<Id, Id>();
        for (Case cas : [SELECT Id, VTD1_Study__c FROM Case WHERE Id IN :caseIds]) {
            if (cas.VTD1_Study__c != null) { caseToStudyIds.put(cas.Id, cas.VTD1_Study__c); }
        }

        Map<Id, Map<Id, User>> usersByIdAndStudyId = new Map<Id, Map<Id, User>>();
        if (!caseToStudyIds.isEmpty()) {
            for (HealthCloudGA__CarePlanTemplate__c study : [
                SELECT Id,
                    VTD1_Project_Lead__r.Id,
                    VTD1_Project_Lead__r.IsActive,
                    VTD1_Virtual_Trial_Study_Lead__r.Id,
                    VTD1_Virtual_Trial_Study_Lead__r.IsActive,
                    VTD1_Regulatory_Specialist__r.Id,
                    VTD1_Regulatory_Specialist__r.IsActive,
                    VTD1_Remote_CRA__r.Id,
                    VTD1_Remote_CRA__r.IsActive,
                    VTD1_Virtual_Trial_head_of_Operations__r.Id,
                    VTD1_Virtual_Trial_head_of_Operations__r.IsActive,
                (
                    SELECT User__r.Id, User__r.IsActive
                    FROM Study_Team_Members__r
                    WHERE User__r.IsActive = true
                )
                FROM HealthCloudGA__CarePlanTemplate__c
                WHERE Id IN :caseToStudyIds.values()
            ]) {
                addStudyUserToMap(usersByIdAndStudyId, study.Id, study.VTD1_Project_Lead__r);
                addStudyUserToMap(usersByIdAndStudyId, study.Id, study.VTD1_Virtual_Trial_Study_Lead__r);
                addStudyUserToMap(usersByIdAndStudyId, study.Id, study.VTD1_Regulatory_Specialist__r);
                addStudyUserToMap(usersByIdAndStudyId, study.Id, study.VTD1_Remote_CRA__r);
                addStudyUserToMap(usersByIdAndStudyId, study.Id, study.VTD1_Virtual_Trial_head_of_Operations__r);

                for (Study_Team_Member__c member : study.Study_Team_Members__r) {
                    addStudyUserToMap(usersByIdAndStudyId, study.Id, member.User__r);
                }
            }
        }

        Map<Id, List<User>> usersByCaseId = new Map<Id, List<User>>();
        for (Id caseId : caseIds) {
            Id studyId = caseToStudyIds.get(caseId);
            List<User> userList = new List<User>();
            if (usersByIdAndStudyId.containsKey(studyId)) { userList = usersByIdAndStudyId.get(studyId).values(); }
            usersByCaseId.put(caseId, userList);
        }
        return usersByCaseId;
    }

    private static void addStudyUserToMap(Map<Id, Map<Id, User>> usersByIdAndStudyId, Id studyId, User usr) {
        if (usr != null && usr.IsActive) {
            if (!usersByIdAndStudyId.containsKey(studyId)) {
                usersByIdAndStudyId.put(studyId, new Map<Id, User>());
            }
            usersByIdAndStudyId.get(studyId).put(usr.Id, usr);
        }
    }
}