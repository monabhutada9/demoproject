/**
 * Created by Yuliya Yakushenkova on 11/25/2020.
 */

@IsTest
public class VT_R5_MobilePatientPaymentsTest {

    private static final String URI = '/patient/v1/payments/';

    public static void firstTest() {
        User usr = fillRestContexts();
        String responseBody = VT_R5_TestRestContext.doTest(
                usr,  new VT_R5_TestRestContext.MockRestContext(URI, null)
        );
        System.debug(responseBody);
    }

    public static void secondTest() {
        User usr = fillRestContexts();

        Id paymentId = [SELECT Id FROM VTD1_Patient_Payment__c LIMIT 1].Id;

        String responseBody = VT_R5_TestRestContext.doTest(
                usr, new VT_R5_TestRestContext.MockRestContext(URI + paymentId, null)
        );
        System.debug(responseBody);
    }

    public static void thirdTest() {
        User usr = fillRestContexts();
        String responseBody = VT_R5_TestRestContext.doTest(
                usr, new VT_R5_TestRestContext.MockRestContext(URI + 'sum', null)
        );
        System.debug(responseBody);
    }

    public static void forthTest() {
        User usr = fillRestContexts();
        Id paymentId = [SELECT Id FROM VTD1_Patient_Payment__c LIMIT 1].Id;

        String responseBody = VT_R5_TestRestContext.doTest(
                usr, new VT_R5_TestRestContext.MockRestContext('PATCH', URI + paymentId, '')
        );
        System.debug(responseBody);
    }

    public static User fillRestContexts() {
        User usr = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.PT_USERNAME LIMIT 1];

        return usr;
    }
}