/**
 * @author: Alexander Komarov
 * @date: 16.10.2020
 * @description:
 */

public with sharing class VT_R5_MobilePatientMessaging extends VT_R5_MobileRestResponse implements VT_R5_MobileRestRouter.Routable {

    public List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/messages',
                '/patient/{version}/messages/{id}',
                '/patient/{version}/announcements',
                '/patient/{version}/announcements/{id}'
        };
    }
    private static final String MESSAGES = 'messages';
    private static final String ANNOUNCEMENTS = 'announcements';


    public void versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        String requestId = parameters.get('id');
        String requestSection = request.requestURI.contains(MESSAGES) ? MESSAGES : ANNOUNCEMENTS;
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        get_v1(requestSection, requestId);
                        return;
                    }
                }
            }
            when 'POST' {
                switch on requestVersion {
                    when 'v1' {
                        switch on requestSection {
                            when 'messages' {
                                post_v1_messages(requestId);
                                return;
                            }
                        }
                    }
                }
            }
        }
        VT_R5_MobileRestRouter.unsupportedVersionResponse();
        this.addError('unsupported version');
        this.sendResponse();
    }

    private void post_v1_messages(String requestId) {
        try {
            List<VTD2_Feed_Post_on_Chat__c> validation = [SELECT Id FROM VTD2_Feed_Post_on_Chat__c WHERE VTD2_Feed_Element__c = :requestId LIMIT 1];
            if (validation == null || !validation.isEmpty()) {
                this.sendResponse(400);
                return;
            }
            RequestParams params = (RequestParams) JSON.deserialize(this.request.requestBody.toString(), RequestParams.class);
            if (String.isBlank(params.fileName) || String.isBlank(params.based64FileData)) {
                VT_D1_CommunityChat.sendComment(requestId, params.message);
            } else {
                if (params.based64FileData.length() > 5000000) {
                    this.sendResponse(452, 'FAILURE.  Attachment file can not have more than 5000000 characters.');
                    return;
                }
            }
            VT_D1_CommunityChat.sendCommentWithFile(requestId, params.message, params.fileName, params.based64FileData);
            get_v1(MESSAGES, requestId);
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private List<PatientPersonalChat> chatsList;
    private Unread unreadInfo;
    private List<ChatMessage> comments;
    private void get_v1(String section, String requestId) {
        try {
            if (requestId == null) {
                Map<String, String> parentIdToTitle = section.equals(MESSAGES) ? getCaseIdToSubject() : getChatIdToSubject();
                List<FeedItem> feedItems = new FeedItemsGetter().getFeedItems(parentIdToTitle.keySet());
                chatsList = formListResponse(parentIdToTitle, feedItems);
                unreadInfo = new Unread();
                setUnreadInfo(chatsList);
            } else {
                fillCommentsByPostId(requestId);
                markPostAsRead(requestId);
            }
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private void fillCommentsByPostId(String requestId) {
        comments = new List<ChatMessage>();
        FeedItem firstComment = [
                SELECT Id, Body, CreatedDate, CreatedById, CreatedBy.Name
                FROM FeedItem
                WHERE Id = :requestId
                LIMIT 1
        ];
        ConnectApi.CommentPage commentsPage;
        if (!Test.isRunningTest()) commentsPage = ConnectApi.ChatterFeeds.getCommentsForFeedElement(null, requestId, null, 100);
        Set<Id> filesIds = new Set<Id>();
        comments.add(new ChatMessage(firstComment));
        if (commentsPage != null && commentsPage.items != null) {
            for (ConnectApi.Comment comment : commentsPage.items) {
                if (String.valueOf(comment.type) == 'ContentComment') {
                    filesIds.add(comment.capabilities.content.id);
                }
                comments.add(new ChatMessage(comment));
            }
        }
        Map<Id, ContentVersion> contentDocumentIdToVersionsMap = new Map<Id, ContentVersion>();

        for (ContentVersion cv : [SELECT Id, VersionData, ContentSize, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN :filesIds]) {
            contentDocumentIdToVersionsMap.put(cv.ContentDocumentId, cv);
        }
        for (ChatMessage chatMessage : comments) {
            if (chatMessage.file != null && chatMessage.file.fileId != null) {
                ContentVersion currentCV = contentDocumentIdToVersionsMap.get(chatMessage.file.fileId);
                Integer fileSizeB64 = EncodingUtil.base64Encode(currentCV.VersionData).length();
                chatMessage.file.fileSizeBase64 = String.valueOf(fileSizeB64);
            }
        }
    }

    private void markPostAsRead(String requestId) {
        if (!Test.isRunningTest()) ConnectApi.ChatterFeeds.setIsReadByMe(null, requestId, true);
        User u = [SELECT Id, VTR3_UnreadFeeds__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        String unreadFeeds = u.VTR3_UnreadFeeds__c;
        if (unreadFeeds != null) {
            if (unreadFeeds.contains(String.valueOf(requestId))) {
                u.VTR3_UnreadFeeds__c = unreadFeeds.remove(String.valueOf(requestId) + ';');
                update u;
            }
        }
    }

    private without sharing class FeedItemsGetter {
        List<FeedItem> getFeedItems(Set<String> parents) {
            return [
                    SELECT ParentId,Id, Body, CreatedDate, CreatedBy.Name, CreatedBy.Id, (
                            SELECT Id, CommentBody, CreatedBy.Name, CreatedDate, CreatedBy.Id
                            FROM FeedComments
                            ORDER BY CreatedDate DESC
                            LIMIT 1
                    )
                    FROM FeedItem
                    WHERE ParentId
                            IN :parents
            ];
        }
    }

    private Map<String, String> getCaseIdToSubject() {
        Map<String, String> caseIdToSubject = new Map<String, String>();
        for (AggregateResult ar : [
                SELECT
                        VTD2_PCF_Chat_Member__r.VTD1_PCF__c,
                        VTD2_PCF_Chat_Member__r.VTD1_PCF__r.Subject
                FROM VTD2_Feed_Post_on_PCF__c
                WHERE VTD2_PCF_Chat_Member__r.VTD1_User_Id__c = :UserInfo.getUserId()
                AND VTD2_PCF_Chat_Member__r.VTD1_PCF__c != NULL
                GROUP BY VTD2_PCF_Chat_Member__r.VTD1_PCF__c,
                        VTD2_PCF_Chat_Member__r.VTD1_PCF__r.Subject
        ]) {
            String currentCaseId = String.valueOf(ar.get('VTD1_PCF__c'));
            String currentCaseSubject = String.valueOf(ar.get('Subject'));
            caseIdToSubject.put(currentCaseId, currentCaseSubject == null ? '' : currentCaseSubject);
        }
        return caseIdToSubject;
    }
    private Map<String, String> getChatIdToSubject() {
        Map<String, String> result = new Map<String, String>();
        for (AggregateResult ar : [
                SELECT
                        VTD2_Chat_Member__r.VTD1_Chat__c,
                        VTD2_Chat_Member__r.VTD1_Chat__r.Name
                FROM VTD2_Feed_Post_on_Chat__c
                WHERE VTD2_Chat_Member__r.VTD1_User__c = :UserInfo.getUserId()
                AND VTD2_Chat_Member__r.VTD1_Chat__c != NULL
                GROUP BY VTD2_Chat_Member__r.VTD1_Chat__c,
                        VTD2_Chat_Member__r.VTD1_Chat__r.Name
        ]) {
            String currentChatId = String.valueOf(ar.get('VTD1_Chat__c'));
            String currentChatSubject = String.valueOf(ar.get('Name'));
            result.put(currentChatId, currentChatSubject == null ? '' : currentChatSubject);
        }
        return result;
    }

    private void setUnreadInfo(List<PatientPersonalChat> patientMessages) {
        for (PatientPersonalChat message : patientMessages) {
            if (unreadInfo.newPersonalChats.contains(message.id)) message.read = false;
        }
    }

    private List<PatientPersonalChat> formListResponse(Map<String, String> caseIdToSubject, List<FeedItem> feedItems) {
        List<PatientPersonalChat> result = new List<PatientPersonalChat>();
        for (FeedItem feedItem : feedItems) {
            if (caseIdToSubject.containsKey(feedItem.ParentId)) result.add(new PatientPersonalChat(feedItem, caseIdToSubject.get(feedItem.ParentId)));
        }
        return result;
    }

    public void initService() {
        usersPhotoCache = new Map<String, String>();
    }

    private class ChatMessage {
        String id;
        String authorName;
        Long commentData;
        String text;
        String photo;
        ChatFile file;

        ChatMessage(ConnectApi.Comment comment) {
            ConnectApi.User commentAuthor = (ConnectApi.User) comment.user;
            this.id = comment.id;
            this.authorName = commentAuthor.displayName;
            String tempPhotoString = commentAuthor.photo.largePhotoUrl;
            this.photo = tempPhotoString.contains('default_profile') ? '' : tempPhotoString;
            this.commentData = comment.createdDate.getTime();
            this.text = String.valueOf(comment.body.text);
            if (String.valueOf(comment.type) == 'ContentComment') {
                this.file = new ChatFile(comment.capabilities.content);
            }
        }

        ChatMessage(FeedItem feedItem) {
            this.id = feedItem.Id;
            this.authorName = feedItem.CreatedBy.Name;
            this.commentData = feedItem.CreatedDate.getTime();
            this.text = feedItem.Body;
            this.photo = getUserPhoto(feedItem.CreatedBy.Id);
        }
    }

    private class ChatFile {
        String fileExtension;
        String fileSize;
        String title;
        String fileId;
        String fileSizeBase64;

        ChatFile(ConnectApi.ContentCapability content) {
            this.fileExtension = content.fileExtension;
            this.fileSize = content.fileSize;
            this.title = content.title;
            this.fileId = content.id;
        }
    }

    private class PatientPersonalChat {
        String id;
        String title;
        String lastMessageAuthorName;
        Long lastMessageDate;
        String photo;
        Boolean read = true;
        String textLastMessage;

        PatientPersonalChat(FeedItem feedItem, String title) {
            this.id = feedItem.Id;
            this.title = title;
            String lastMessageAuthorName;
            String textLastMessage;
            Long lastMessageDate;
            String lastMessageAuthorId;
            if (feedItem.FeedComments != null && !feedItem.FeedComments.isEmpty()) {
                textLastMessage = feedItem.FeedComments[0].CommentBody;
                lastMessageAuthorName = feedItem.FeedComments[0].CreatedBy.Name;
                lastMessageDate = feedItem.FeedComments[0].CreatedDate.getTime();
                lastMessageAuthorId = feedItem.FeedComments[0].CreatedBy.Id;
            } else {
                textLastMessage = feedItem.Body;
                lastMessageAuthorName = feedItem.CreatedBy.Name;
                lastMessageDate = feedItem.CreatedDate.getTime();
                lastMessageAuthorId = feedItem.CreatedBy.Id;
            }
            this.lastMessageAuthorName = lastMessageAuthorName;
            this.lastMessageDate = lastMessageDate;
            this.textLastMessage = textLastMessage;
            this.photo = getUserPhoto(lastMessageAuthorId);
        }
    }

    private static Map<String, String> usersPhotoCache;
    private static String getUserPhoto(String userId) {
        if (!usersPhotoCache.containsKey(userId)) {
            String result = '';
            if (!Test.isRunningTest()) result = ConnectApi.UserProfiles.getPhoto(null, userId).standardEmailPhotoUrl;
            result = result.contains('default_profile') ? '' : result;
            usersPhotoCache.put(userId, result);
        }
        return usersPhotoCache.get(userId);
    }

    private class Unread {
        Set<String> newPersonalChats = new Set<String>();
        Set<String> newBroadcasts = new Set<String>();

        Unread() {
            User currentUser = [SELECT VTR3_UnreadFeeds__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
            String unreadFeedsString = currentUser.VTR3_UnreadFeeds__c;
            Set<String> unreadFeedItemsIds = new Set<String>();
            if (unreadFeedsString != null && unreadFeedsString.length() > 3) {
                unreadFeedItemsIds.addAll(unreadFeedsString.split(';'));
            }

            for (AggregateResult ar : [SELECT VTD2_Feed_Element__c FROM VTD2_Feed_Post_on_PCF__c WHERE VTD2_Feed_Element__c IN :unreadFeedItemsIds GROUP BY VTD2_Feed_Element__c]) {
                Id currentId = (Id) ar.get('VTD2_Feed_Element__c');
                unreadFeedItemsIds.remove(currentId);
                newPersonalChats.add(currentId);
            }
            for (AggregateResult ar : [SELECT VTD2_Feed_Element__c FROM VTD2_Feed_Post_on_Chat__c WHERE VTD2_Feed_Element__c IN :unreadFeedItemsIds GROUP BY VTD2_Feed_Element__c]) {
                Id currentId = (Id) ar.get('VTD2_Feed_Element__c');
                unreadFeedItemsIds.remove(currentId);
                newBroadcasts.add(currentId);
            }

            if (!unreadFeedItemsIds.isEmpty()) {
                List<String> newUnreadString = new List<String>();
                newUnreadString.addAll(newBroadcasts);
                newUnreadString.addAll(newPersonalChats);
                currentUser.VTR3_UnreadFeeds__c = String.join(newUnreadString, ';') + ';';
                try {
                    update currentUser;
                } catch (Exception e) {
                    System.debug('unexpected user update fail');//log this
                }
            }
        }
    }
    private class RequestParams {
        String message;
        String fileName;
        String based64FileData;
    }
}