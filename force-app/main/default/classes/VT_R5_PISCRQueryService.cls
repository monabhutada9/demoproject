/**
 * Created by user on 10/27/2020.
 */

public class VT_R5_PISCRQueryService {

    private static final String BRACKET_L = '(';
    private static final String BRACKET_R = ')';
    private static final String AND_STR = ' AND ';
    private static final String OR_STR = ' OR ';

    public static List<Event> getScheduledEventsByUserIdAndVisitId(String userId, String visitId) {
        String scheduledStatus = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED;
        List<String> eventsFields = new List<String>{
                'Subject',
                'OwnerId',
                'StartDateTime',
                'EndDateTime',
                'WhatId',
                'Record_Type__c',
                'VTD1_IsBufferEvent__c',
                'VTD1_Actual_Visit__c',
                'VTD1_Actual_Visit__r.Type__c',
                'VTD1_Actual_Visit__r.Name',
                'VTD1_Actual_Visit__r.VTD1_Unscheduled_Visit_Type__c',
                'VTD1_Actual_Visit__r.VTD1_Pre_Visit_Instructions__c',
                'VTD1_Actual_Visit__r.VTD1_Visit_Description__c',
                'VTD1_Actual_Visit__r.VTD1_Visit_Duration__c',
                'VTD1_Actual_Visit__r.VTD1_Case__c',
                'VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Study__c',
                'VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Study__r.Name',
                'VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Primary_PG__c',
                'VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Secondary_PG__c',
                'VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_PI_user__c',
                'VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Backup_PI_User__c',
                'VTD1_Actual_Visit__r.VTD1_Case__r.VTR2_SiteCoordinator__c',
                'VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Patient_User__c',
                'VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Subject_ID__c',
                'VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c',
                'VTD1_Actual_Visit__r.VTD1_Case__r.Contact.FirstName',
                'VTD1_Actual_Visit__r.VTD1_Case__r.Contact.LastName',
                'VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Study__r.Name',
                'VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Subject_ID__c',
                'VTD1_Actual_Visit__r.Unscheduled_Visits__r.Contact.FirstName',
                'VTD1_Actual_Visit__r.Unscheduled_Visits__r.Contact.LastName'
        };

        String condition = VT_R5_DatabaseQueryService.equal('OwnerId', userId) + AND_STR;

        condition += BRACKET_L
                + VT_R5_DatabaseQueryService.equal('VTD1_Actual_Visit__r.VTD1_Status__c', scheduledStatus)
                + OR_STR + VT_R5_DatabaseQueryService.nullField('VTD1_Actual_Visit__c')
                + BRACKET_R;

        if (String.isNotBlank(visitId)) {
            condition += AND_STR + VT_R5_DatabaseQueryService.equal('VTD1_Actual_Visit__c', visitId);
        }
        condition += AND_STR + VT_R5_DatabaseQueryService.greaterOrEqThan('StartDateTime','TODAY');
        return VT_R5_DatabaseQueryService.query(eventsFields, Event.class, condition, 'StartDateTime');
    }

    public static List<Event> getScheduledEventsByUserId(String userId) {
        return getScheduledEventsByUserIdAndVisitId(userId, null);
    }

    public static List<Visit_Member__c> getVisitMembersBySpecifyCondition(String condition) {
        List<String> visitMembersFields = new List<String>{
                'VTD1_Actual_Visit__c',
                'Visit_Member_Name__c',
                'VTD1_Member_Type__c',
                'VTD1_Participant_User__c',
                'VTD1_Participant_User__r.Name',
                'VTD1_Participant_User__r.MediumPhotoUrl',
                'VTD1_External_Participant_Type__c'
        };

        return VT_R5_DatabaseQueryService.query(
                visitMembersFields, Visit_Member__c.class, condition, 'VTD1_Actual_Visit__c ASC'
        );
    }

    public static List<Visit_Member__c> getVisitMembersByVisitIds(Set<Id> visitIds) {
        String filter =
                VT_R5_DatabaseQueryService.fieldInValues(
                        'VTD1_Actual_Visit__c', new List<Id>(visitIds)
                ) + AND_STR +
                        VT_R5_DatabaseQueryService.equal(
                                'VTD1_Actual_Visit__r.VTD1_Status__c',
                                VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED
                        );
        return getVisitMembersBySpecifyCondition(filter);
    }

    public static List<Visit_Member__c> getVisitMembersByVisitId(Id visitId) {
        return getVisitMembersByVisitIds(new Set<Id>{visitId});
    }

    public static List<Visit_Member__c> getVisitMembersByMembersIds(Set<Id> membersIds) {
        String filter = VT_R5_DatabaseQueryService.fieldInValues('Id', new List<Id>(membersIds));
        return getVisitMembersBySpecifyCondition(filter);
    }

    public static Map<Event, List<Visit_Member__c>> getScheduledEventWithVisitMembersByUserIdAndVisitId(String userId, String visitId) {
        Map<Id, Event> eventsByIds = new Map<Id, Event>();
        for (Event event : getScheduledEventsByUserIdAndVisitId(userId, visitId)) {
            eventsByIds.put(event.VTD1_Actual_Visit__c, event);
        }

        Map<Event, List<Visit_Member__c>> visitMembersByVisitId = new Map<Event, List<Visit_Member__c>>();

        for (Visit_Member__c member : getVisitMembersByVisitIds(eventsByIds.keySet())) {
            Event event = eventsByIds.get(member.VTD1_Actual_Visit__c);
            List<Visit_Member__c> members = new List<Visit_Member__c>();
            if (visitMembersByVisitId.containsKey(event)) {
                members = visitMembersByVisitId.get(event);
            }
            members.add(member);
            visitMembersByVisitId.put(event, members);
        }

        return visitMembersByVisitId;
    }

    public static Map<Event, List<Visit_Member__c>> getScheduledEventsWithVisitMembersByUserId(String userId) {
        return getScheduledEventWithVisitMembersByUserIdAndVisitId(userId, null);
    }

}