/**
 * @author Ruslan Mullayanov
 * @created 2019-05-16
 * @see VT_R2_ActualVisitLocationController
 */
@IsTest
public with sharing class VT_R2_ActualVisitLocationControllerTest {

    public static void test1() {
        List<VTR2_Site_Address__c> siteAddressList = [
                SELECT Id, Name, VTR2_VirtualSiteLookup__c, VTR2_VirtualSiteLookup__r.Name, VTR2_VirtualSiteLookup__r.VTR2_PrimarySiteAddress__c
                FROM VTR2_Site_Address__c LIMIT 1
        ];
        System.assert(!siteAddressList.isEmpty());
        VTR2_Site_Address__c siteAddress = siteAddressList[0];
        Case caseLocation = [SELECT Id, VTD1_Virtual_Site__c FROM Case WHERE RecordType.DeveloperName = 'VTD1_PCF' LIMIT 1];

        Test.startTest();
        System.assert(String.isNotEmpty(VT_R2_ActualVisitLocationController.getSiteAddressName(siteAddress.Id)));
        String siteAddr;
        try {
            siteAddr = VT_R2_ActualVisitLocationController.getSiteAddressName(null);
        } catch (AuraHandledException e) {
            System.assertEquals(null, siteAddr);
        }
        System.assert(String.isNotEmpty(VT_R2_ActualVisitLocationController.getVisitLocations(caseLocation.Id)));
        Test.stopTest();
    }
}