/**
* @author: Carl Judge
* @date: 21-Sep-18
* @description: 
**/

public abstract without sharing class VT_D2_VisitEmailsAbstract {

    public abstract VTD1_Actual_Visit__c getVisit();

    public List<VT_D2_VisitRequestedTemplate_ListItem> getListItems() {
        Map<String, Id> docMap = new Map<String, Id>();
        for (Document item : [
            SELECT Id, DeveloperName
            FROM Document
            WHERE Folder.DeveloperName = 'Visit_Request_Email_Images'
        ]) {
            docMap.put(item.DeveloperName, item.Id);
        }
        List<VT_D2_VisitRequestedTemplate_ListItem> listItems = new List<VT_D2_VisitRequestedTemplate_ListItem>();

        VTD1_Actual_Visit__c visit = getVisit();
        String patientID = visit.VTD2_Patient_ID__c == null ? '' : visit.VTD2_Patient_ID__c;
        listItems.add(new VT_D2_VisitRequestedTemplate_ListItem(docMap.get('Patient'), 'Patient', patientID));
        listItems.add(new VT_D2_VisitRequestedTemplate_ListItem(docMap.get('Study'), 'Study', visit.VTD1_Study_name_formula__c));
        listItems.add(new VT_D2_VisitRequestedTemplate_ListItem(docMap.get('Visit'), 'Visit', visit.VTD1_Formula_Name__c));
        listItems.add(new VT_D2_VisitRequestedTemplate_ListItem(docMap.get('Visit_Window'), 'Visit Window', getVisitWindow(visit)));
        return listItems;
    }

    public String getRescheduleUrl() {
        return VisitRequestConfirmSite__c.getInstance().SiteURL__c + '?id=' + getVisit().Id + '&action=reschedule';
    }

    protected String getDateText(Datetime dt) {
        return dt.format('EEEE, d-MMM-yyyy', getPiTimezone());
    }

    protected String getPiTimezone() {
        return isUnscheduled() ?
            getVisit().Unscheduled_Visits__r.VTD1_PI_user__r.TimeZoneSidKey : getVisit().VTD1_Case__r.VTD1_PI_user__r.TimeZoneSidKey;
    }

    public String getPiTitleAndName() {
        User piUser = isUnscheduled() ? getVisit().Unscheduled_Visits__r.VTD1_PI_user__r : getVisit().VTD1_Case__r.VTD1_PI_user__r;
        return (piUser.Title != null ? piUser.Title + ' ' : '') + piUser.Name;
    }

    protected Boolean isUnscheduled() {
        return getVisit().RecordType.DeveloperName == 'VTD1_Unscheduled';
    }

    public String getVisitWindow(VTD1_Actual_Visit__c visit) {
        String visitWindow = '';
        try {
            visitWindow = new VT_D1_ActualVisitsHelper().getVisitWindow(visit.Id);
        } catch (Exception e) {
            System.debug(e);
        }
        return visitWindow;
    }
}