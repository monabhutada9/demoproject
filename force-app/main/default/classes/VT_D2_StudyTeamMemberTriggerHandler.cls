/**
* @author: Carl Judge
* @date: 24-Nov-18
* @description: 
**/

public without sharing class VT_D2_StudyTeamMemberTriggerHandler {
    private final static Set<String> powerPartnerProfiles = new Set<String>{VT_R4_ConstantsHelper_Profiles.PI_PROFILE_NAME, VT_R4_ConstantsHelper_Profiles.SCR_PROFILE_NAME};

    public void onAfterInsert(Map<Id,Study_Team_Member__c> newMap){
        List<Study_Team_Member__c> recs = newMap.values();
        VT_R3_UpdateAccessStudy.studySharingOnInsert(recs);
        sendWelcomeEmailToPowerPartners(recs, null);
        VT_R5_ChatButtonAndSkillHandler.insertServiceResourceSkills(newMap);  // added Study + Language ServiceREsourceSkills
        VT_R5_CustomAuditHandler_STM.onAfterInsertSTM(newMap);
    }
    
    public void onBeforeUpdate(Map<Id, Study_Team_Member__c> oldMap, Map<Id, Study_Team_Member__c> newMap) {
        deleteUserFromGroupsOnUpdate(newMap, oldMap);
    }

    public void onAfterUpdate(Map<Id,Study_Team_Member__c> oldMap, Map<Id,Study_Team_Member__c> newMap){
        List<Study_Team_Member__c> newList = newMap.values();

        studySharingOnUpdate(newList, oldMap);
        sendWelcomeEmailToPowerPartners(newList, oldMap);
        VT_R5_ChatButtonAndSkillHandler.updateServiceResourceSkills(oldMap, newMap);  // added Study + Language ServiceREsourceSkills

        VT_R5_CustomAuditHandler_STM.onAfterUpdateSTM(newMap, oldMap);
    }

    public void onBeforeDelete(Map<Id, Study_Team_Member__c> oldMap) {
        List<Study_Team_Member__c> oldList = oldMap.values();
        VT_D1_LegalHoldDeleteValidator.validateDeletion(oldList);
        VT_R3_UpdateAccessStudy.studySharingOnDelete(oldList);
        deleteUserFromGroupsOnDelete(oldMap);
        VT_R5_CustomAuditHandler_STM.onBeforeDeleteSTM(oldMap);
    }

    public void onAfterDelete(Map<Id,Study_Team_Member__c> oldMap){
        VT_R5_ChatButtonAndSkillHandler.deleteServiceResourceSkills(oldMap);
    }

    public void deleteUserFromGroupsOnDelete(Map<id, Study_Team_Member__c> oldMap) {
        List<Study_Team_Member__c> oldList = oldMap.values();
        System.debug('Trigger old::::::::::::::::::::::::::::::::::::::::: ' + oldList);
        System.debug('Trigger oldMap::::::::::::::::::::::::::::::::::::::::: ' + oldMap);
        Set <Id> studyIds = new Set<Id>();
        Set <Id> setUserIds = new Set<Id>();
        for (Study_Team_Member__c item : oldList) {
            studyIds.add(item.Study__c);
            if (item.User__c != null) setUserIds.add(item.User__c);
        }
        if (!studyIds.isEmpty() && !setUserIds.isEmpty())
            deleteUserFromGroupsLogic(oldList, oldMap, studyIds, setUserIds);
    }

    public void deleteUserFromGroupsOnUpdate(Map<id, Study_Team_Member__c> newMap, Map<Id, Study_Team_Member__c> oldMap) {
        List<Study_Team_Member__c> oldList = oldMap.values();

        //Method is for deleting records from Community Member groups when user is changed for example from UserA to UserB
        Set <Id> studyIds = new Set<Id>();
        Set <Id> setUserIds = new Set<Id>();
        for (Study_Team_Member__c item : oldList) {
            if (item.User__c != newMap.get(item.id).User__c) {
                studyIds.add(item.Study__c);
                if (item.User__c != null) setUserIds.add(item.User__c);
            }
        }
        if (!studyIds.isEmpty() && !setUserIds.isEmpty())
            deleteUserFromGroupsLogic(oldList, oldMap, studyIds, setUserIds);
    }

    private void deleteUserFromGroupsLogic(List<Study_Team_Member__c> triggerValues, Map<id, Study_Team_Member__c> triggerMapValues, Set<Id>studyIds, Set<Id> setUserIds) {

        Set <Id> GroupIds = new Set<Id>();
        //List<CollaborationGroupMember> listForDeleteOptimisation = new List<CollaborationGroupMember>();
        Map<Id, CollaborationGroupMember> mapForDeleteOptimisation = new Map<Id, CollaborationGroupMember>();
        Map <id, Set<id>> studyGroupIdsMap = new Map<Id, Set<Id>>();

        if (!studyIds.isEmpty()) {
            List<HealthCloudGA__CarePlanTemplate__c> carePlanList = [
                    select VTD1_PIGroupId__c, VTD1_SCGroupId__c, VTD2_CM_Group_Id__c, VTD1_Patient_Group_Id__c, VTD1_DeliveryTeamGroupId__c, VTD1_PG_Study_Group_Id__c, VTR2_CRA_Group_Id__c, VTR2_SCR_Group_Id__c, VTR2_REGroupId__c
                    from HealthCloudGA__CarePlanTemplate__c
                    where id in :studyIds
            ];

            if (!carePlanList.isEmpty())
                for (HealthCloudGA__CarePlanTemplate__c cpt : carePlanList) {
                    Set<id> innerSet = new Set<Id>();
                    if (cpt.VTD1_PIGroupId__c != null) innerSet.add(cpt.VTD1_PIGroupId__c);
                    if (cpt.VTD1_SCGroupId__c != null) innerSet.add(cpt.VTD1_SCGroupId__c);
                    if (cpt.VTD2_CM_Group_Id__c != null) innerSet.add(cpt.VTD2_CM_Group_Id__c);
                    if (cpt.VTR2_CRA_Group_Id__c != null) innerSet.add(cpt.VTR2_CRA_Group_Id__c);
                    if (cpt.VTD1_Patient_Group_Id__c != null) innerSet.add(cpt.VTD1_Patient_Group_Id__c);
                    if (cpt.VTD1_PG_Study_Group_Id__c != null) innerSet.add(cpt.VTD1_PG_Study_Group_Id__c);
                    if (cpt.VTD1_DeliveryTeamGroupId__c != null) innerSet.add(cpt.VTD1_DeliveryTeamGroupId__c);
                    if (cpt.VTR2_SCR_Group_Id__c != null) innerSet.add(cpt.VTR2_SCR_Group_Id__c);
                    if (cpt.VTR2_REGroupId__c != null) innerSet.add(cpt.VTR2_REGroupId__c);
                    GroupIds.addAll(innerSet);
                    studyGroupIdsMap.put(cpt.id, innerSet);
                }
        }
        if (!setUserIds.isEmpty()) {
            List<CollaborationGroupMember> listGroupMembers = [Select id, MemberId, CollaborationGroupId from CollaborationGroupMember where MemberId in :setUserIds and CollaborationGroupId in :GroupIds];

            if (!listGroupMembers.isEmpty()) {
                for (Study_Team_Member__c std : triggerValues) {
                    for (CollaborationGroupMember cgm : listGroupMembers) {
                        if (std.User__c == cgm.MemberId && studyGroupIdsMap.get(std.Study__c).contains(cgm.CollaborationGroupId)) {
                            //listForDeleteOptimisation.add(cgm);
                            mapForDeleteOptimisation.put(cgm.Id, cgm);
                        }
                    }
                }
            }
        }
        if (!triggerMapValues.keySet().isEmpty()) {
            List<VTD1_Chat_Member__c>chatMemberListForDelete = [Select id from VTD1_Chat_Member__c where VTD1_STM__c in :triggerMapValues.keySet()];

            if (!chatMemberListForDelete.isEmpty()) delete chatMemberListForDelete;
            //if (!listForDeleteOptimisation.isEmpty()) delete listForDeleteOptimisation;
            if (!mapForDeleteOptimisation.isEmpty()) delete mapForDeleteOptimisation.values();
        }
    }

    public static void studySharingOnUpdate(List<Study_Team_Member__c> lstNewRecords, map<Id, Study_Team_Member__c> triggerOldMapValues){
        List<Study_Team_Member__c> lstStmToCreateStudtShare = new List<Study_Team_Member__c>();
        for(Study_Team_Member__c member: lstNewRecords){
            if(member.VTD1_Active__c && member.VTD1_Active__c != triggerOldMapValues.get(member.Id).VTD1_Active__c){
                lstStmToCreateStudtShare.add(member);
            }
        }
        if(lstStmToCreateStudtShare.size() > 0){
            VT_R3_UpdateAccessStudy.studySharingOnInsert(lstStmToCreateStudtShare);
        }
    }



    public static void sendWelcomeEmailToPowerPartners(List<Study_Team_Member__c> newStms, Map<Id, Study_Team_Member__c> oldStms){
        Set<Id> activeStmUserIds = new Set<Id>();
        for(Study_Team_Member__c stm : newStms){
            if(stm.VTD1_Active__c && ( oldStms == null || !oldStms.get(stm.ID).VTD1_Active__c )){
                activeStmUserIds.add(stm.User__c);
            }
        }
        if(!activeStmUserIds.isEmpty()){
            List<User> usersToProceed = [
                    SELECT Id
                    FROM User
                    WHERE Id IN :activeStmUserIds
                    AND Profile.Name IN :powerPartnerProfiles
                    AND IsActive = TRUE
                    AND VTR5_ActivationEmailSent__c = FALSE
            ];
            if(!usersToProceed.isEmpty()){
                List<Id> userIds = new List<Id>();
                for(User u : usersToProceed){
                    userIds.add(u.Id);
                    u.VTR5_ActivationEmailSent__c = true;
                }
                new VT_R3_PublishedAction_ResetCommunityPass(userIds).publish();
                if(!Test.isRunningTest()){
                    update usersToProceed;
                }
            }

        }
    }


}