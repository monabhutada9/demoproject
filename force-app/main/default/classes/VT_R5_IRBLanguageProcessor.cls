/**
 * @author: Dmitry Kovalev
 * @date: 15.07.2020
 */

public with sharing class VT_R5_IRBLanguageProcessor {
    /**
     * Updates IRB Languages fields on Virtual Site object.
     * 
     * This method used by configuration trigger (VT_R5_IRBLanguageConfigurationTrigger).
     * 
     * @param  configs list of recently inserted IRB Language Configurations
     */
    public static void updateIRBLanguagesByConfig(List<VTR5_IRBLanguageConfiguration__c> configs) {
        List<Virtual_Site__c> sitesForUpdate = getModifiedVirtualSites(configs);

        if (!sitesForUpdate.isEmpty()) {
            update sitesForUpdate;
        }
    }

    /**
     * Use DML to commit changes.
     * 
     * @param  configs list of recently inserted (or updated) IRB Language Configuration records
     * @return         list of modified Virtual Site records with updated IRB Language fields
     */
    @TestVisible
    private static List<Virtual_Site__c> getModifiedVirtualSites(List<VTR5_IRBLanguageConfiguration__c> configs) {
        // Take study ids for every taken config
        Set<Id> studyIds = new Set<Id>();
        for (VTR5_IRBLanguageConfiguration__c config : configs) {
            studyIds.add(config.VTR5_Study__c);
        }

        // Retrieve all virtual sites related to these studies
        /*
            List<Virtual_Site__c> sites = [
                SELECT VTR3_Country__c, VTR5_DefaultIRBLanguage__c, VTR5_IRBApprovedLanguages__c, VTD1_Study__c
                FROM Virtual_Site__c
                WHERE VTD1_Study__c IN :studyIds
            ];
        */
        // DK FIXME: rewrite onto VT_Stubber later
        List<Virtual_Site__c> sites = (List<Virtual_Site__c>) new QueryBuilder(Virtual_Site__c.class)
            .addField(Virtual_Site__c.Id)
            .addField(Virtual_Site__c.VTR3_Country__c)
            .addField(Virtual_Site__c.VTR5_DefaultIRBLanguage__c)
            .addField(Virtual_Site__c.VTR5_IRBApprovedLanguages__c)
            .addField(Virtual_Site__c.VTD1_Study__c)
            .addConditions()
                .add(new QueryBuilder.InCondition('VTD1_Study__c').inCollection(studyIds))
            .endConditions()
            .namedQueryRegister('VT_R5_IRBLanguageProcessor.getModifiedVirtualSites')
            .toList();

        List<Virtual_Site__c> sitesForUpdate = new List<Virtual_Site__c>();

        // O(n*m) complexity, where n - count of virual sites, m - count of related configs
        // For every Vitrual Site find suitable configuration
        for (Virtual_Site__c site : sites) {
            Boolean siteNeedUpdate = false;

            for (VTR5_IRBLanguageConfiguration__c config : configs) {
                if (config.VTR5_Country__c != site.VTR3_Country__c || config.VTR5_Study__c != site.VTD1_Study__c) {
                    continue;
                }

                siteNeedUpdate = true;
                site = populateLanguagesOnVirtualSite(site, config);

                if (siteNeedUpdate) {
                    sitesForUpdate.add(site);
                    break;
                }
            }
        }

        return sitesForUpdate;
    }

    /**
     * Set or override IRB Languages fields on Virtual Sites if suitable configuration exists.
     * 
     * This method used by virtual site handler (@class VT_D1_VirtualSiteHandler).
     *
     * @param  sites list of recently updated Virtual Sites
     * @return modified list of received sites
     */
    public static List<Virtual_Site__c> updateIRBLanguagesOnVirtualSites(List<Virtual_Site__c> sites) {
        Map<Id, VTR5_IRBLanguageConfiguration__c> configMap = getIRBConfigMap(sites);

        for (Virtual_Site__c site : sites) {
            VTR5_IRBLanguageConfiguration__c config = configMap.get(site.Id);

            if (config != null) {
                site = populateLanguagesOnVirtualSite(site, config);
            }
        }

        return sites;
    }

    /**
     * Set or override IRB Languages fields on Virtual Sites if suitable configuration exists 
     * and if VTR3_Country__c field updated from empty value (null).
     * 
     * This method used by virtual site handler (@class VT_D1_VirtualSiteHandler).
     *
     * @param  sites       list of recently updated Virtual Sites (Trigger.new)
     * @param  oldSitesMap (Trigger.oldMap)
     * @return modified list of received sites (if suitable configurations exist) 
     */
    public static List<Virtual_Site__c> updateIRBLanguagesOnVirtualSitesIfCountryUpdated(List<Virtual_Site__c> sites,
                                                                                        Map<Id, Virtual_Site__c> oldSitesMap) {
        Map<Id, VTR5_IRBLanguageConfiguration__c> configMap = VT_R5_IRBLanguageProcessor.getIRBConfigMap(sites);

        for (Virtual_Site__c site : sites) {
            VTR5_IRBLanguageConfiguration__c config = configMap.get(site.Id);

            String oldCountry = oldSitesMap.get(site.Id).VTR3_Country__c;
            if (!String.isBlank(oldCountry) || String.isBlank(site.VTR3_Country__c)) { 
                continue;
            }
            
            if (config != null) {
                site = populateLanguagesOnVirtualSite(site, config);
            }
        }

        return sites;
    }

    /**
     * Method returns Map, that contains the most relevant IRB configuration (if it exists)
     * for Virtual Site id as a key.
     * 
     * @param  sites list of recently updated Virtual Sites
     * @return map with Virtual Site id as a key and the most relevant configuration as a value
     */
    @TestVisible
    private static Map<Id, VTR5_IRBLanguageConfiguration__c> getIRBConfigMap(List<Virtual_Site__c> sites) {
        Map<Id, VTR5_IRBLanguageConfiguration__c> result = new Map<Id, VTR5_IRBLanguageConfiguration__c>();

        // Create set of study+country keys
        Set<String> studyCountryKeys = new Set<String>();
        for (Virtual_Site__c site : sites) {
            studyCountryKeys.add(site.VTD1_Study__c + site.VTR3_Country__c);
        }

        if (studyCountryKeys.isEmpty()) {
            return result; 
        }

        // Retrieve from DB all related configs
        /*
            List<VTR5_IRBLanguageConfiguration__c> configs = [
                SELECT VTR5_StudyCountryKey__c,
                    VTR5_IRBApprovedLanguages__c, VTR5_DefaultIRBLanguage__c
                FROM VTR5_IRBLanguageConfiguration__c
                WHERE VTR5_StudyCountryKey__c IN :studyCountryKeys
                ORDER BY LastModifiedDate DESC
            ];
        */
        // DK FIXME: rewrite onto VT_Stubber later
        List<VTR5_IRBLanguageConfiguration__c> configs = (List<VTR5_IRBLanguageConfiguration__c>) new QueryBuilder(VTR5_IRBLanguageConfiguration__c.class)
            .addField(VTR5_IRBLanguageConfiguration__c.VTR5_StudyCountryKey__c)
            .addField(VTR5_IRBLanguageConfiguration__c.VTR5_IRBApprovedLanguages__c)
            .addField(VTR5_IRBLanguageConfiguration__c.VTR5_DefaultIRBLanguage__c)
            .addConditions()
                .add(new QueryBuilder.InCondition('VTR5_StudyCountryKey__c').inCollection(studyCountryKeys))
            .endConditions()
            .addOrderDesc('LastModifiedDate')
            .namedQueryRegister('VT_R5_IRBLanguageProcessor.getIRBConfigMap')
            .toList();

        if (configs.isEmpty()) {
            return result;
        }

        // Filter configs, create map of configs with StudyCountryKey field as a key
        Map<String, VTR5_IRBLanguageConfiguration__c> studyCountryConfigMap = new Map<String, VTR5_IRBLanguageConfiguration__c>();

        for (VTR5_IRBLanguageConfiguration__c config : configs) {
            String key = config.VTR5_StudyCountryKey__c;
            if (!studyCountryConfigMap.containsKey(key)) {
                studyCountryConfigMap.put(key, config);
            }
        }

        if (studyCountryConfigMap.isEmpty()) {
            return result;
        }

        // Create Map<Virtual Site Id, Config> with the most relevant configuration for every site
        for (Virtual_Site__c site : sites) {
            if (String.isEmpty(site.VTR3_Country__c)) {
                continue;
            }

            String key = site.VTD1_Study__c + site.VTR3_Country__c;
            if (studyCountryConfigMap.containsKey(key)) {
                VTR5_IRBLanguageConfiguration__c config = studyCountryConfigMap.get(key);

                result.put(site.Id, config);
            }
        }

        return result;
    }

    private static Virtual_Site__c populateLanguagesOnVirtualSite(Virtual_Site__c site, VTR5_IRBLanguageConfiguration__c config) {
        // DK TODO: validateDefaultLanguage(config.VTR5_IRBApprovedLanguages__c, config.VTR5_DefaultIRBLanguage__c);
        // DK NOTE: If the default language is not in the approved languages call config.addError();
        // [!] You can test this behavior using --> ApexPages.hasMessages() <--
        // [!] According to: https://salesforce.stackexchange.com/questions/244388/unit-testing-adderror-without-doing-dml
        
        site.VTR5_IRBApprovedLanguages__c = config.VTR5_IRBApprovedLanguages__c;
        site.VTR5_DefaultIRBLanguage__c = config.VTR5_DefaultIRBLanguage__c;
        return site;
    }
}