public class VT_D1_CaseControllerRemote {
    //Subject integration with Cenduit/CSM. Jane. July 25, 2018.
    @AuraEnabled
    public static String subjectRemote(Id caseId) {
        VT_D1_QAction_SendSubject sendSubjectAction = new VT_D1_QAction_SendSubject(caseId);
        sendSubjectAction.execute();
        if(sendSubjectAction.getStatus() == VT_D1_AbstractAction.STATUS_FAILED) return sendSubjectAction.getMessage();
        return null;
    }
    
    @AuraEnabled
    public static String randomizeRemote(Id caseId) {
        VT_D1_QAction_GetRandomize getRandomizeAction = new VT_D1_QAction_GetRandomize(caseId);
        getRandomizeAction.execute();
        return JSON.serialize(getRandomizeAction.subject);
    }

    @AuraEnabled (Cacheable = true)
    public static String getLogReportId() {
        return VTD1_RTId__c.getInstance().VTD1_Report_Study_Drug_Account_Log_Case__c;
    }
    
    @AuraEnabled (Cacheable = true)
    public static Boolean isUserSystemAdmin(){
        Profile adminProfile = [
                SELECT Id
                FROM Profile
                WHERE (UserType = 'Standard' AND PermissionsCustomizeApplication = TRUE)
                ORDER BY CreatedDate ASC LIMIT 1
        ];
        return UserInfo.getProfileId() == adminProfile.Id;
    }

    @AuraEnabled (Cacheable = true) 
    public static CaseInfoObj getCaseInfo(Id caseId) {
        CaseInfoObj info = new CaseInfoObj(caseId);
        return info;
    }

	public class CaseInfoObj {
		@AuraEnabled public Id id {get;set;}
		@AuraEnabled public String name {get;set;}
		@AuraEnabled public String gender {get;set;}
		@AuraEnabled public DateTime birthdate {get;set;}
		@AuraEnabled public String address {get;set;}
		@AuraEnabled public String visit_location {get;set;}
		@AuraEnabled public String phone {get;set;}

		public CaseInfoObj(Id caseId) {
			case c = [SELECT AccountId, ContactId, VTD1_Patient__c FROM Case WHERE Id=:caseid];
			Account acc = [SELECT VTD1_Gender__c,VTD1_DateOfBirth__c from Account where Id=:c.AccountId];
			Contact cont = [
					SELECT Name,Phone,
					(Select City__c, State__c From Addresses__r Limit 1),
					(Select PhoneNumber__c From Phones__r Limit 1)
					FROM Contact
					WHERE Id=:c.ContactId
			];
			List<VTD1_Patient__c> pat = [
					SELECT Id,
					(Select City__c, State__c From Addresses__r Limit 1)
					FROM VTD1_Patient__c
					WHERE Id=:c.VTD1_Patient__c
			];
			this.id = caseId;
			this.name = cont.Name;
			this.gender = acc.VTD1_Gender__c;
			this.birthdate = acc.VTD1_DateOfBirth__c;
			if(!cont.Addresses__r.isEmpty()) {
				this.address = cont.Addresses__r[0].City__c + ', ' + cont.Addresses__r[0].State__c;
			}
			if(!pat.isEmpty() && !pat[0].Addresses__r.isEmpty()) {
				this.visit_location = pat[0].Addresses__r[0].City__c + ', ' + pat[0].Addresses__r[0].State__c;
			}
			if(!cont.Phones__r.isEmpty()) {
				this.phone = cont.Phones__r[0].PhoneNumber__c;
			}
		}
	}
}