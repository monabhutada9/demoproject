@isTest
public class TestVTSL {
    public static User vtslUser;
    public static User patientUser;
    
     private static void setupMethod(){
         DomainObjects.Study_t study_t = new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'))
        		.setOriginalName('TestNameSponsor');
        DomainObjects.Contact_t con_t = new DomainObjects.Contact_t()
                .setLastName('VT_R3_EmailsSenderTest');
        DomainObjects.User_t user_t = new DomainObjects.User_t()
                .addContact(con_t)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        Case c = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName(VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN)
                .addStudy(study_t)
                .addVTD1_Patient_User(user_t)
                .addContact(con_t)
                .persist();

        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c(
                VTD1_Range__c = 4,
                VTD1_VisitOffset__c = 2,
                VTD1_VisitOffsetUnit__c = 'days',
                VTD1_VisitDuration__c = '30 minutes',
                VTD1_Onboarding_Type__c = 'N/A',
                VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient; Site Coordinator'
        );
        insert protocolVisit;
        
        
        Id profileId =[SELECT Id FROM Profile WHERE Name =:VT_R4_ConstantsHelper_ProfilesSTM.VTSL_PROFILE_NAME].id;
        
       DomainObjects.User_t userVTSL = new DomainObjects.User_t()
           	.setProfile(profileId);
            userVTSL.persist();
        DomainObjects.StudyTeamMember_t stm1 = new DomainObjects.StudyTeamMember_t().setRecordTypeByName('VTSL').addUser(userVTSL).addStudy(Study_t);  
        stm1.persist();
        Study_Team_Member__c stm = [select id, User__c, User__r.Name,Study__r.name from Study_Team_Member__c where Study__r.name='TestNameSponsor' 
        and recordtype.name='VTSL' 
        and User__r.username like '%@scott.com' limit 1]; 

        vtslUser = new User(id = stm.User__c);
        patientUser = [Select id from User where Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.Name = 'TestNameSponsor' limit 1];
        System.debug('***105***'+stm.User__r.userName+stm.Study__r.name);
        System.assert(patientUser!= null);
    }
    @isTest
    public static void testVTSLq()
    {
        Study_Team_Member__c vtslstm=[select id from Study_Team_Member__c where recordtype.name='VTSL'];
        system.assertNotEquals(vtslstm, NULL);
    }
    
     @isTest
    private static void createUserSharePositive2(){ 
        setupMethod();
        List<UserShare> userShareList = new List<UserShare>();
        //User u;
        //User patientUser;
        User adminUser =  [Select id from User where user.profile.name= 'System Administrator'  and isActive = true limit 1];
       System.runAs(adminUser){
            Group grp = new Group();
            grp.name = 'Test Group1';
            grp.Type = 'Regular'; 
            Insert grp;
            System.debug('###205###'+grp.id);
            
            Group subGrp  = new Group();
            subGrp.name = 'Test Group2';
            subGrp.Type = 'Regular'; 
            Insert subGrp; 
            System.debug('###205###'+subGrp.id);
            
            GroupMember grpMem1 = new GroupMember();
            grpMem1.UserOrGroupId = subGrp.Id;
            grpMem1.GroupId = grp.Id;
            Insert grpMem1; 
            System.debug('###205###'+grpMem1.id);        
            GroupMember grpMem2 = new GroupMember();
            grpMem2.UserOrGroupId = vtslUser.Id;
            grpMem2.GroupId = subGrp.Id;
            Insert grpMem2; 
            System.debug('###205###'+grpMem2.id);
            UserShare userShareRecord = new UserShare();
            userShareRecord.UserId = patientUser.id;
            userShareRecord.UserOrGroupId = grp.id;
            userShareRecord.UserAccessLevel = 'Read';
            userShareRecord.RowCause = 'Manual';
            System.debug('###209###'+userShareRecord.id+'####'+[SELECT  HasReadAccess, RecordId FROM UserRecordAccess WHERE 
                                                                RecordId = :patientUser.id and userId=:vtslUser.id]);
            insert userShareRecord;
            System.debug('###210###'+userShareRecord.id+'####'+[SELECT  HasReadAccess, RecordId FROM UserRecordAccess WHERE 
                                                                RecordId = :patientUser.id and userId=:vtslUser.id]);
            System.debug('###205###'+userShareRecord.id+'####'+[SELECT Id, UserId, UserOrGroupId FROM UserShare WHERE id = :userShareRecord.id]);
            
        }
        
    }
        
}