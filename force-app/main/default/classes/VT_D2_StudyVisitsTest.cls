/**
* @author: Carl Judge
* @date: 31-Jan-19
* @description: Test for VT_D2_StudyVisits
**/

@IsTest
public class VT_D2_StudyVisitsTest {

    @TestSetup
    private static void setupMethod() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();

        VTD1_ProtocolVisit__c pVisit = new VTD1_ProtocolVisit__c(VTD1_Study__c = study.Id,VTR2_Visit_Participants__c = 'Patient; PI');
        insert pVisit;

        Case carePlan = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];
        List<VTD1_Actual_Visit__c> visits = new List<VTD1_Actual_Visit__c>{
            new VTD1_Actual_Visit__c(
                VTD1_Case__c = carePlan.Id,
                VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today()+1,
                VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
                VTD1_Protocol_Visit__c = pVisit.Id,
                    VTR2_Televisit__c = false
            ),
            new VTD1_Actual_Visit__c(
                VTD1_Case__c = carePlan.Id,
                VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today()+2,
                VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED,
                VTD1_Protocol_Visit__c = pVisit.Id,
                    VTR2_Televisit__c = false
            ),
            new VTD1_Actual_Visit__c(
                VTD1_Case__c = carePlan.Id,
                VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today()+3,
                VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED,
                VTD1_Protocol_Visit__c = pVisit.Id,
                    VTR2_Televisit__c = false
            ),
            new VTD1_Actual_Visit__c(
                VTD1_Case__c = carePlan.Id,
                VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today()+4,
                VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT,
                VTD1_Protocol_Visit__c = pVisit.Id,
                    VTR2_Televisit__c = false
            ),
            new VTD1_Actual_Visit__c(
                VTD1_Case__c = carePlan.Id,
                VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today()+5,
                VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED,
                VTD1_Protocol_Visit__c = pVisit.Id,
                    VTR2_Televisit__c = false
            ),
            new VTD1_Actual_Visit__c(
                    VTD1_Case__c = carePlan.Id,
                    VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today()+5,
                    VTD1_Scheduled_Date_Time__c = Date.today()+6,
                    VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
                    VTD1_Protocol_Visit__c = pVisit.Id,
                    VTR2_Televisit__c = false
            ),
            new VTD1_Actual_Visit__c(
                    VTD1_Case__c = carePlan.Id,
                    VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today()+6,
                    VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
                    VTD1_Protocol_Visit__c = pVisit.Id,
                    VTR2_Televisit__c = false
            )
        };
        insert visits;

    }

    @IsTest
    private static void doTest() {
        Test.startTest();
        Case carePlan = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];
        List<VTD1_Actual_Visit__c> actualVisitList = [SELECT Id, VTD1_Case__c FROM VTD1_Actual_Visit__c];
        Task task = new Task();
        task.OwnerId = UserInfo.getUserId();
        task.Status = 'Open';
        task.Category__c = 'Open Query';
        task.WhatId = actualVisitList[0].Id;
        insert task;
//        VT_D2_StudyVisits.getVisits('wrong Id', 'qwer AND qwer = qwer');
        VT_D2_StudyVisits.getVisits(carePlan.Id, null);
        for(VTD1_Actual_Visit__c actualVisit : actualVisitList){
            if(actualVisit.VTD1_Case__c == carePlan.Id){
                VT_D2_StudyVisits.cancelVisit(actualVisit.Id, Label.VTR2_RescheduleLater, 'Reason');
                VT_D2_StudyVisits.cancelVisit(actualVisit.Id, Label.VTR2_ChangeStatusToCanceled, 'Reason');
                break;
            }
        }
        VT_D2_StudyVisits.getAdHocRecTypeId();
        Test.stopTest();
    }

    @isTest
    public static void test(){

    }
}