public with sharing class VT_D1_DocumentProcessHelper {
    public static void createPlaceholdersByStudyLevel(List<VTD1_Regulatory_Binder__c> newBinderList) {
        system.debug('In createPlaceholdersByStudyLevel');
        //create placeholders (VTD1_Document__c) for each Binder with Level=Study and link it with Study
        if (newBinderList.isEmpty()) return;

        List<RecordType> recTypeRGList = [select Id from RecordType where DeveloperName = 'VTD1_Regulatory_Document'];
        if (recTypeRGList.isEmpty()) return;
        Id recordTypeId = recTypeRGList[0].Id;

        List<VTD1_Document__c> placeholderList = new List<VTD1_Document__c>();
        for (VTD1_Regulatory_Binder__c bItem : newBinderList) {
            if (bItem.VTD1_Level__c == 'Study') {
                VTD1_Document__c placeholder = new VTD1_Document__c();
                placeholder.VTD1_Study__c = bItem.VTD1_Care_Plan_Template__c;
                placeholder.VTD1_Regulatory_Binder__c = bItem.Id;
                placeholder.VTD1_Current_Workflow__c = 'Not Yet Started';
                placeholder.VTD1_Status__c = 'Pending Approval';
                placeholder.VTD1_IsActive__c = true;
                placeholder.RecordTypeId = recordTypeId;

                placeholderList.add(placeholder);
            }
        }

        if (!placeholderList.isEmpty()) insert placeholderList;
    }

    public static void createPlaceholdersBySiteLevel(List<Virtual_Site__c> newSiteList) {
        system.debug('In createPlaceholdersBySiteLevel:' + newSiteList);
        //create placeholders (VTD1_Document__c) for each Binder with Level=Site and link it with Study
        if (newSiteList.isEmpty()) return;

        set<Id> studyIds = new set<Id>();

        for (Virtual_Site__c siteItem : newSiteList) {
            if (siteItem.VTD1_Study__c != null) {
                studyIds.add(siteItem.VTD1_Study__c);
            }
        }
        if (studyIds.isEmpty()) return;

        List<VTD1_Regulatory_Binder__c> binderList = [
                SELECT Id, VTD1_Care_Plan_Template__c, VTD1_Site_RSU__c
                FROM VTD1_Regulatory_Binder__c
                WHERE VTD1_Care_Plan_Template__c IN :studyIds
                AND VTD1_Level__c = 'Site'
                AND VTD1_Regulatory_Document__r.VTD1_Document_Type__c != 'PA Signature Page'
        ];
        if (binderList.isEmpty()) return;

        Map<Id, Integer> siteId_docCount = createPlaceholders(newSiteList, binderList);

        List<Virtual_Site__c> vsiteList = [SELECT Id, VTD1_EDP_In_Progress__c FROM Virtual_Site__c WHERE Id IN:siteId_docCount.keySet()];
        if (!vsiteList.isEmpty()) {
            for (Virtual_Site__c siteItem : vsiteList) {
                if (siteId_docCount.containsKey(siteItem.Id)) {
                    siteItem.VTD1_EDP_In_Progress__c = siteId_docCount.get(siteItem.Id);
                }
            }
            update vsiteList;
        }
    }

    public static void createMissingPlaceholdersBySiteLevel(List<Virtual_Site__c> newSiteList) {
        system.debug('In createMissingPlaceholdersBySiteLevel:' + newSiteList);
        //create placeholders (VTD1_Document__c) for each Binder with Level=Site and link it with Study
        if (newSiteList.isEmpty()) return;

        set<Id> studyIds = new set<Id>();
        for (Virtual_Site__c siteItem : newSiteList) {
            if (siteItem.VTD1_Study__c != null) {
                studyIds.add(siteItem.VTD1_Study__c);
                siteItem.VTR3_Update_RB__c = false;
            }
            if(siteItem.VTD1_EDP_In_Progress__c == null) siteItem.VTD1_EDP_In_Progress__c = 0;
        }
        if (studyIds.isEmpty()) return;

        List<VTD1_Document__c> documentsInVS = [
                SELECT Id, Name, VTD1_Regulatory_Binder__c,VTD1_Site__c
                FROM VTD1_Document__c
                WHERE VTD1_Site__c IN :newSiteList
        ];

        // reg binder IDs by site - this is from existing VTD1_Document__c recrods
        Map<Id, Set<Id>> regBinderIdBySite = new Map<Id, Set<Id>>();
        //Set<Id> regBinderId= new set<Id>();
        for (VTD1_Document__c doc : documentsInVS) {
            if (!regBinderIdBySite.containsKey(doc.VTD1_Site__c)) {
                regBinderIdBySite.put(doc.VTD1_Site__c, new set<Id>());
            }
            regBinderIdBySite.get(doc.VTD1_Site__c).add(doc.VTD1_Regulatory_Binder__c);
        }

        List<VTD1_Regulatory_Binder__c> regulatoryBinders = [
                SELECT Id, VTD1_Care_Plan_Template__c, VTD1_Site_RSU__c
                FROM VTD1_Regulatory_Binder__c
                WHERE VTD1_Care_Plan_Template__c IN :studyIds
                AND VTD1_Level__c = 'Site'
                AND VTD1_Regulatory_Document__r.VTD1_Document_Type__c != 'PA Signature Page'
        ];
        if (regulatoryBinders.isEmpty()) return;

        // reg binder records by Study ID
        Map<Id, List<VTD1_Regulatory_Binder__c>> regBinderByStudy = new Map<Id, List<VTD1_Regulatory_Binder__c>>();
        for (VTD1_Regulatory_Binder__c regulatoryBinder : regulatoryBinders) {
            List<VTD1_Regulatory_Binder__c> recordsRegulatoryBinder = regBinderByStudy.get(regulatoryBinder.VTD1_Care_Plan_Template__c);
            if (recordsRegulatoryBinder == null) recordsRegulatoryBinder = new List<VTD1_Regulatory_Binder__c>();
            recordsRegulatoryBinder.add(regulatoryBinder);
            regBinderByStudy.put(regulatoryBinder.VTD1_Care_Plan_Template__c, recordsRegulatoryBinder);
        }

        Set<Id> setOfBinderIdsForSite = new set<Id>();
        List<VTD1_Regulatory_Binder__c> listRegulatoryBinder = new List<VTD1_Regulatory_Binder__c>();

        List<VTD1_Document__c> placeholderList = new List<VTD1_Document__c>();
        List<RecordType> recTypeRGList = [SELECT Id FROM RecordType WHERE DeveloperName = 'VTD1_Regulatory_Document'];
        Id recordTypeId = recTypeRGList[0].Id;

        for (Virtual_Site__c siteItem : newSiteList) {
            listRegulatoryBinder = regBinderByStudy.get(siteItem.VTD1_Study__c);
            setOfBinderIdsForSite = regBinderIdBySite.get(siteItem.Id);
            for (VTD1_Regulatory_Binder__c binder : listRegulatoryBinder) {

                if (setOfBinderIdsForSite == null || !setOfBinderIdsForSite.contains(binder.Id)) {
                    VTD1_Document__c placeholder = new VTD1_Document__c();

                    placeholder.VTD1_Site__c = siteItem.Id;
                    placeholder.VTD1_Study__c = binder.VTD1_Care_Plan_Template__c;
                    placeholder.VTD1_Regulatory_Binder__c = binder.Id;
                    placeholder.VTD1_Current_Workflow__c = 'Not Yet Started';
                    placeholder.VTD1_Status__c = null; //SH-10773
                    placeholder.VTD1_IsActive__c = true;
                    placeholder.RecordTypeId = recordTypeId;

                    if(binder.VTD1_Site_RSU__c) siteItem.VTD1_EDP_In_Progress__c  = siteItem.VTD1_EDP_In_Progress__c  + 1;
                    placeholderList.add(placeholder);
                }
            }
        }
        if (!placeholderList.isEmpty()) {
            Integer batchSize = 20;
            if(placeholderList.size() <= batchSize){
                insert placeholderList;
            }else{
                VT_R3_AsyncDML.doBatchedDML(placeholderList, 'INSERT', batchSize); //copied from createPlaceholders method (SH-10773)
            }
        }
    }

    public static Map<Id, Integer> createPlaceholders(List<Virtual_Site__c> siteList, List<VTD1_Regulatory_Binder__c> binderList) {
        Map<Id, Integer> siteId_docCount = new Map<Id, Integer>();
        List<RecordType> recTypeRGList = [SELECT Id FROM RecordType WHERE DeveloperName = 'VTD1_Regulatory_Document'];
        if (recTypeRGList.isEmpty()) return siteId_docCount;
        Id recordTypeId = recTypeRGList[0].Id;

        List<VTD1_Document__c> placeholderList = new List<VTD1_Document__c>();


        for (Virtual_Site__c siteItem : siteList) {
            siteId_docCount.put(siteItem.Id, 0);
            Integer docCount = 0;
            for (VTD1_Regulatory_Binder__c bItem : binderList) {
                if (bItem.VTD1_Care_Plan_Template__c == siteItem.VTD1_Study__c) {
                    VTD1_Document__c placeholder = new VTD1_Document__c();
                    placeholder.VTD1_Site__c = siteItem.Id;
                    placeholder.VTD1_Regulatory_Binder__c = bItem.Id;
                    placeholder.VTD1_Study__c = bItem.VTD1_Care_Plan_Template__c;
                    placeholder.VTD1_Current_Workflow__c = 'Not Yet Started';
                    placeholder.VTD1_Status__c = null; //SH-10773
                    placeholder.VTD1_IsActive__c = true;
                    placeholder.RecordTypeId = recordTypeId;

                    docCount = bItem.VTD1_Site_RSU__c ? ++docCount : docCount;
                    placeholderList.add(placeholder);
                }
            }
            siteId_docCount.put(siteItem.Id, docCount);
        }
        if (!placeholderList.isEmpty()) {
            VT_R3_AsyncDML.doBatchedDML(placeholderList, 'INSERT', 20);
        }
        return siteId_docCount;
    }

    /*
       Takes studyId(s) and a list of reg doc types as input
       Returns a map of StudyId => Reg doc type => Binder ID
   */
    public static Map<String, Id> getRegBinderIdsForStudy(Id studyId, List<String> recDocTypes) {
        Map<Id, Map<String, Id>> binderIdMap = getRegBinderIdsForStudies(new List<Id>{
                studyId
        }, recDocTypes);
        return binderIdMap.containsKey(studyId) ? binderIdMap.get(studyId) : new Map<String, Id>();
    }
    public static Map<Id, Map<String, Id>> getRegBinderIdsForStudies(List<Id> studyIds, List<String> recDocTypes) {
        Map<Id, Map<String, Id>> binderIdMap = new Map<Id, Map<String, Id>>();

        for (VTD1_Regulatory_Binder__c item : [
                SELECT Id, VTD1_Care_Plan_Template__c, VTD1_Regulatory_Document__r.VTD1_Document_Type__c
                FROM VTD1_Regulatory_Binder__c
                WHERE VTD1_Care_Plan_Template__c IN :studyIds
                AND VTD1_Regulatory_Document__r.VTD1_Document_Type__c IN :recDocTypes
        ]) {
            if (!binderIdMap.containsKey(item.VTD1_Care_Plan_Template__c)) {
                binderIdMap.put(item.VTD1_Care_Plan_Template__c, new Map<String, Id>());
            }
            binderIdMap.get(item.VTD1_Care_Plan_Template__c).put(item.VTD1_Regulatory_Document__r.VTD1_Document_Type__c, item.Id);
        }

        return binderIdMap;
    }
}