/**
 * Created by User on 19/05/20.
 */
@isTest
private with sharing class VT_D2_UserTriggerHelperTest {
    public class MockSendVideoConfSignalHttpResponseGenerator implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('["test"]');
            res.setStatusCode(200);
            return res;
        }
    }

    @testSetup
    static void setup() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.startTest();
        //HealthCloudGA__CandidatePatient__c candidatePatient = VT_D1_TestUtils.createPatientCandidate('Mike', 'Birn', 'mikebirn@test.com', 'mikebirn@test.com', 'mikebirn', 'Converted', study.Id, 'PN', '11-11-11', 'US', 'NY');
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }

    @IsTest
    static void sendSignalTest() {
        List<Case> casesList = [SELECT Id, VTD1_Primary_PG__c, VTD1_PI_user__c, VTD1_PI_user__r.ContactId FROM Case];
        System.assert(casesList.size() > 0);
        if (casesList.size() > 0) {
            Case cas = casesList.get(0);

            Video_Conference__c videoConference = new Video_Conference__c();
            insert videoConference;

            VTD1_Conference_Member__c conferenceMember1 = new VTD1_Conference_Member__c();
            conferenceMember1.VTD1_Video_Conference__c = videoConference.Id;
            conferenceMember1.VTD1_User__c = cas.VTD1_Primary_PG__c;
            insert conferenceMember1;

            VTD1_Conference_Member__c conferenceMember2 = new VTD1_Conference_Member__c();
            conferenceMember2.VTD1_Video_Conference__c = videoConference.Id;
            conferenceMember2.VTD1_User__c = cas.VTD1_PI_user__c;
            insert conferenceMember2;

            VTD1_Video_Conf_Connection__c videoConfConnection = new VTD1_Video_Conf_Connection__c();
            videoConfConnection.VTD1_Video_ConfId__c = videoConference.Id;
            videoConfConnection.VTD1_Time_Connection__c = Datetime.now();
            videoConfConnection.VTD1_Participant__c = cas.VTD1_Primary_PG__c;
            insert videoConfConnection;

            VTD1_Actual_Visit__c visit = new VTD1_Actual_Visit__c(
                    VTD1_Scheduled_Date_Time__c = Datetime.now(),
                    VTD1_Status__c = 'To Be Scheduled',
                    VTD1_Case__c = cas.Id,
                    VTD1_Contact__c = cas.VTD1_PI_user__r.ContactId
            );
            insert visit;
            insert new Visit_Member__c(VTD1_Actual_Visit__c = visit.Id, VTD1_Participant_User__c = cas.VTD1_PI_user__c);

            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockSendVideoConfSignalHttpResponseGenerator());
            List<User> userList = [SELECT Id, Email FROM User WHERE Id =:cas.VTD1_PI_user__c];
            User u = new User(Id = userList[0].Id, Email =VT_D1_TestUtils.generateUniqueUserName());
            VT_D2_UserTriggerHelper.updateVisitAndConferenceMembers(new List<User>{u},new Map<Id,User>(userList));
            Test.stopTest();
        }
    }

    @IsTest
    static void changeLanguageTest() {
        List<Case> casesList = [SELECT Id, VTD1_Patient_User__c FROM Case];
        if (casesList.size() > 0) {
            Case cas = casesList.get(0);
            List<User> users = [SELECT Id, LanguageLocaleKey FROM User WHERE Id =:cas.VTD1_Patient_User__c];
            users[0].LanguageLocaleKey = 'es';
            System.runAs(new User(Id = UserInfo.getUserId())){
                update users[0];
            }
        }
    }

    @IsTest
    static void deactivateUser() {
        List<Case> casesList = [SELECT Id, VTD1_Patient_User__c FROM Case];
        if (casesList.size() > 0) {
            Case cas = casesList.get(0);
            List<User> users = [SELECT Id, LanguageLocaleKey FROM User WHERE Id =:cas.VTD1_Patient_User__c];
            users[0].IsActive = false;
            System.runAs(new User(Id = UserInfo.getUserId())){
                update users[0];
            }
        }
    }
}