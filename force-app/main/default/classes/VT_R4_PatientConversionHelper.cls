public without sharing class VT_R4_PatientConversionHelper {
    private static final String STATUS_CONVERTED = VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_CONVERTED;
    private static final String STATUS_FINISHED = VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_FINISHED;
    private static final String STATUS_FAILED = VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_FAILED;
    private static Map<String, VT_D2_UserLanguageLocaleAssignment__mdt> uLocals = new Map<String, VT_D2_UserLanguageLocaleAssignment__mdt>();

    List<ContactCandidateWrapper> contactCandidateWrappers = new List<ContactCandidateWrapper>();

    public static Map<Id, Contact> contactsByIdMap = new Map<Id, Contact>();


    public void createContacts(List<Account> accounts) {
        contactCandidateWrappers = new List<ContactCandidateWrapper>();
        List<Contact> contactsToInsert = new List<Contact>();
        Map<Integer, ContactCandidateWrapper> contactWrapperSequence = new Map<Integer, ContactCandidateWrapper> ();
        Integer i = 1;
        for (Account acc : accounts) {
            HealthCloudGA__CandidatePatient__c cp = VT_R4_PatientConversion.cps.get(acc.Candidate_Patient__c);
            Case dCase = VT_R4_PatientConversion.duplicatesCPcase.get(cp.rr_Email__c);
            if (dCase == null) { //|| !VT_D1_CandidatePatientProcessHandler.isTransferProcess(cp)) {
                Contact patientContact = new Contact(AccountId = acc.Id,
                        Account = acc,
                        Email = cp.rr_Email__c,
                        HealthCloudGA__PreferredName__c = acc.Name,
                        HealthCloudGA__Gender__c = cp.VTR4_Gender__c,
                        Birthdate = cp.HealthCloudGA__BirthDate__c,
                        FirstName = cp.rr_firstName__c,
                        LastName = cp.rr_lastName__c,
                        VTR4_Username__c = cp.rr_Username__c,
                        VTR4_IsLockedPatient__c = (cp.VTR2_Patient_Status__c!=null && cp.VTR2_Patient_Status__c != VT_R4_ConstantsHelper_AccountContactCase.CASE_PRE_CONSENT),
                        Phone = cp.VTD1_Patient_Phone__c,
                        VTR5_PatientPhone__c = cp.VTD1_Patient_Phone__c,
                        RecordTypeId = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PATIENT,
                        VTR2_Doc_Language__c = cp.VTD2_Language__c,
                        //SH-15912 Additional Language Mapping
                        VT_R5_Secondary_Preferred_Language__c =  cp.VT_R5_Secondary_Preferred_Language__c,
                        VT_R5_Tertiary_Preferred_Language__c = cp.VT_R5_Tertiary_Preferred_Language__c,
                        VTR2_Primary_Language__c = cp.VTD2_Language__c);
                ContactCandidateWrapper candidateWrapper = new ContactCandidateWrapper(patientContact, acc, cp);
                contactsToInsert.add(patientContact);
                contactWrapperSequence.put(i, candidateWrapper);
                i++;
                VT_R4_PatientConversion.mapAccContact.put(acc.Id, patientContact);
                if (cp.VTR2_Caregiver_Email__c != null) { // MODIFIED VERSION OF CODE TAKEN FROM VT_R3_PublishedAction_CreateCaregivers
                    Contact caregiverContact = new Contact(AccountId = cp.HealthCloudGA__AccountId__c,
                            FirstName = cp.VTR2_Caregiver_First_Name__c,
                            LastName = cp.VTR2_Caregiver_Last_Name__c,
                            VTR4_Username__c = cp.VTR2_Caregiver_Email__c,
                            Email = cp.VTR2_Caregiver_Email__c,
                            Phone = cp.VTR2_Caregiver_Phone__c,
                            RecordTypeId = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_CAREGIVER,
                            VTR2_Primary_Language__c = cp.VTR2_Caregiver_Preferred_Language__c,
                            VTR2_Doc_Language__c = cp.VTR2_Caregiver_Preferred_Language__c,
                            VT_R5_Secondary_Preferred_Language__c = cp.VT_R5_Caregiver_Secondary_Language__c,
                            VT_R5_Tertiary_Preferred_Language__c = cp.VT_R5_Caregiver_Tertiary_Language__c,
                            VTD1_Primary_CG__c = true);
                    candidateWrapper.cgContact = caregiverContact;
                    contactsToInsert.add(caregiverContact);
                    contactWrapperSequence.put(i, candidateWrapper);
                    i++;
                }
                contactCandidateWrappers.add(candidateWrapper);
            } else {
                cp.HealthCloudGA__AccountId__c = acc.Id;
                Contact contact = new Contact(Id = dCase.ContactId, AccountId = acc.Id, VTD1_Clinical_Study_Membership__c = dCase.Id);
                VT_R4_PatientConversion.mapAccContact.put(acc.Id, contact);
                ContactCandidateWrapper candidateWrapper = new ContactCandidateWrapper(contact, acc, cp);
                contactCandidateWrappers.add(candidateWrapper);
            }
        }
        if (!contactsToInsert.isEmpty()) {
            Integer numberInCollection = 1;
            for (Database.SaveResult saveResult : Database.insert(contactsToInsert, VT_R4_PatientConversion.allOrNothing)) {
                ContactCandidateWrapper cpWrapper = contactWrapperSequence.get(numberInCollection);
                if (saveResult.isSuccess()) {
                    cpWrapper.candidate.put('VTR4_StatusCreate__c', STATUS_FINISHED);
                    //contactCandidateWrappers[0].account.VTD2_Patient_s_Contact__c = cpWrapper.contact.Id;
                    cpWrapper.account.VTD2_Patient_s_Contact__c = cpWrapper.contact.Id;
                } else {
                    VT_R4_PatientConversionHelper.logError(saveResult, cpWrapper.candidate, 'VTR4_StatusCreate__c', null);
                }
                numberInCollection++;
            }
        }
        VT_R4_PatientConversion.patientWraps = contactCandidateWrappers;
        System.debug('contacts patientWraps' + contactCandidateWrappers.size());
    }

    public void createPortalUsersForPatients(List<VT_R4_PatientConversion.CPWrapper> cpWrapList, Boolean doInCurrentTransaction) {
        
        /* SH-14562 - Start */
        for (ContactCandidateWrapper wrapper : contactCandidateWrappers) {
            for (VT_R4_PatientConversion.CPWrapper cpw : cpWrapList) {
                if(wrapper.candidate.Id == cpw.cp.Id){
                    wrapper.studyTeamPack = cpw.studyTeamPack;
                    break;
                }
            }
        }
        /* SH-14562 - End */
        
        createPortalUsersForPatientsAndCaregivers(contactCandidateWrappers, doInCurrentTransaction);
    }

    public void createPortalUsersForPatientsAndCaregivers(List<ContactCandidateWrapper> cpWraps, Boolean doInCurrentTransaction) {
        // MODIFIED VERSION OF CODE TAKEN FROM VT_D2_ContactTriggerHelper
        setLocales();
        List<User> usersToInsert = new List<User>();
        List <ContactCandidateWrapper> wrappersToTransfer = new List<ContactCandidateWrapper>();
        List <ContactCandidateWrapper> wrappersToCreateUsers = new List<ContactCandidateWrapper>();

        for (ContactCandidateWrapper c_wrapper : cpWraps) {
            Case dCase = VT_R4_PatientConversion.duplicatesCPcase.get(c_wrapper.candidate.rr_Email__c);
            if (dCase == null){
                wrappersToCreateUsers.add(c_wrapper);
                c_wrapper.user = getUserFromContact(c_wrapper, false);
                if (c_wrapper.cgContact != null) {
                    c_wrapper.cgUser = getUserFromContact(c_wrapper, true);
                }
            } else {
                wrappersToTransfer.add(c_wrapper);
            }
        }
        List<ContactCandidateWrapper> cpWrapsToContinue = new List<ContactCandidateWrapper> ();
         if (!wrappersToCreateUsers.isEmpty()) {
             cpWrapsToContinue.addAll(persistUsers(usersToInsert, wrappersToCreateUsers, doInCurrentTransaction, !doInCurrentTransaction));
        }

        if (!wrappersToTransfer.isEmpty()) {
            cpWrapsToContinue.addAll(wrappersToTransfer);
            //VT_R4_StudyTeamAssignment.assignStudyTeam(wrappersToTransfer, doInCurrentTransaction, !doInCurrentTransaction);
        }
        if (!cpWrapsToContinue.isEmpty()) {
            VT_R4_PatientConversion.reserveSubjectNumbers(cpWrapsToContinue, doInCurrentTransaction, !doInCurrentTransaction); // Added as part of SH-14562, before this was present in assignestudyteam
        }
    }

    public List <ContactCandidateWrapper>  persistUsers(List<User> usersToInsert, List<ContactCandidateWrapper> cpWraps, Boolean doInCurrentTransaction, Boolean usePE) {
        System.debug('cpWraps = ' + cpWraps.size());
        Map <Id, ContactCandidateWrapper> wrappersSuccessMap = new Map <Id, ContactCandidateWrapper>();
        List <ContactCandidateWrapper> wrappersSuccess = new List<ContactCandidateWrapper>();

        Map <String, ContactCandidateWrapper> userIdToWrapperMap = new Map<String, ContactCandidateWrapper>();
        List <User> users = new List<User>();
        for (ContactCandidateWrapper wrapper : cpWraps) {
            if (wrapper.user != null) {
                userIdToWrapperMap.put(wrapper.user.UserName, wrapper);
                users.add(wrapper.user);
            }
            if (wrapper.cgUser != null) {
                userIdToWrapperMap.put(wrapper.cgUser.UserName, wrapper);
                users.add(wrapper.cgUser);
            }
        }
        Integer i = 0;
        try {
            for (Database.SaveResult saveResult : Database.insert(users, VT_R4_PatientConversion.allOrNothing)) {
                User user = users[i];
                ContactCandidateWrapper wrapper = userIdToWrapperMap.get(user.UserName);
                if (saveResult.isSuccess()) {
                    wrappersSuccessMap.put(wrapper.candidate.Id, wrapper);
                } else {
                    for (Database.Error dbError : saveResult.getErrors()) {
                        VT_R4_PatientConversionHelper.logError(saveResult, wrapper.candidate, 'VTR4_StatusCreate__c', null);
                    }
                }
                i++;
            }
        } catch (Exception e) {
            for (ContactCandidateWrapper wrapper : cpWraps) {
                VT_R4_PatientConversionHelper.logError(null, wrapper.candidate, 'VTR4_StatusCreate__c', e);
            }
        }



        for (ContactCandidateWrapper wrapper : wrappersSuccessMap.values()) {
            if (wrapper.user != null) {
                wrapper.contact.VTD1_UserId__c = wrapper.user.Id;
            }
            if (wrapper.cgContact != null && wrapper.cgUser != null) {
                wrapper.cgContact.VTD1_UserId__c = wrapper.cgUser.Id;
            }
        }

        for (ContactCandidateWrapper wrapper : wrappersSuccessMap.values()) {
            wrappersSuccess.add(wrapper);
        }
        if (!wrappersSuccess.isEmpty()) {
            List<VTD1_Chat_Member__c> chatMembersToCreate = new List<VTD1_Chat_Member__c>();
            for (ContactCandidateWrapper wrapper: wrappersSuccess) {
                if (wrapper.candidate.Study__r.VTD1_Patient_Group_Id__c != null) {
                    if (wrapper.user != null) {
                        chatMembersToCreate.add(new VTD1_Chat_Member__c(
                                VTD1_Chat__c = wrapper.candidate.Study__r.VTD1_Patient_Group_Id__c,
                                VTD1_User__c = wrapper.user.Id
                        ));
                    }
                    if (wrapper.cgUser != null) {
                        chatMembersToCreate.add(new VTD1_Chat_Member__c(
                                VTD1_Chat__c = wrapper.candidate.Study__r.VTD1_Patient_Group_Id__c,
                                VTD1_User__c = wrapper.cgUser.Id
                        ));
                    }
                }

            }
            if (!chatMembersToCreate.isEmpty()) {
                insert chatMembersToCreate;
            }
        }
        System.debug('wrappersSuccess size = ' + wrappersSuccess.size());
        /*if (!wrappersSuccess.isEmpty()) {
            //VT_R4_StudyTeamAssignment.assignStudyTeam(wrappersSuccess, doInCurrentTransaction, usePE);
            VT_R4_PatientConversion.reserveSubjectNumbers(wrappersSuccess, false, usePE); // Added as part of SH-14562, before this was present in assignestudyteam
        }*/
        return wrappersSuccess;
    }

    private static User getUserFromContact(ContactCandidateWrapper cpWrapper, Boolean isCG) {
        User newUser = new User();
        if (isCG) {
            cpWrapper.cgUser = newUser;
        } else {
            cpWrapper.user = newUser;
        }
        Contact contact = (isCG ? cpWrapper.cgContact : cpWrapper.contact);
        Id profileId = VT_D1_HelperClass.getProfileIdByName(isCG ? VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME: VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        contactsByIdMap.put(contact.Id, contact);
        HealthCloudGA__CandidatePatient__c cp = cpWrapper.candidate;
        newUser.ContactId = contact.Id;
        newUser.FirstName = contact.FirstName;
        newUser.LastName = contact.LastName;
        newUser.Alias = isCG ? VT_D1_CandidatePatientProcessHandler.getAlias(newUser.FirstName, newUser.LastName) : cp.rr_Alias__c;
        newUser.Username = isCG ? cp.VTR2_Caregiver_Email__c : cp.rr_Username__c;
        newUser.Email = contact.Email;
        newUser.VTR4_Delay_activation_emails_for_PT_CG__c = true;
        newUser.IsActive = !cp.VT_R4_Inactive__c;
        newUser.ProfileId = profileId;
        newUser.EmailEncodingKey = 'ISO-8859-1';
        newUser.CountryCode = cp.HealthCloudGA__Address1Country__c;
        newUser.LanguageLocaleKey = contact.VTR2_Primary_Language__c;
        newUser.VT_R5_Secondary_Preferred_Language__c = isCG ? cp.VT_R5_Caregiver_Secondary_Language__c : cp.VT_R5_Secondary_Preferred_Language__c;
        newUser.VT_R5_Tertiary_Preferred_Language__c = isCG ? cp.VT_R5_Caregiver_Tertiary_Language__c : cp.VT_R5_Tertiary_Preferred_Language__c;
        VT_D2_UserLanguageLocaleAssignment__mdt langCountryLcl = uLocals.get(
                newUser.CountryCode + newUser.LanguageLocaleKey);
        if (langCountryLcl == null) {
            langCountryLcl = uLocals.get(newUser.CountryCode);
        }
        if (langCountryLcl != null) {
            newUser.TimeZoneSidKey = langCountryLcl.VTD2_TimeZoneSidKey__c;
            newUser.VTD2_UserTimezone__c = langCountryLcl.VTD2_TimeZoneSidKey__c;
            newUser.LocaleSidKey = langCountryLcl.VTD2_LocaleSidKey__c;
            newUser.VTD1_Country_Code__c = langCountryLcl.VTD2_CountryCode__c;
        }
        return newUser;
    }

    private static void setLocales() {
        VT_D2_UserLanguageLocaleAssignment__mdt[] uLocals_mdt = [
                SELECT VTD2_LanguageLocaleKey__c,
                        VTD2_TimeZoneSidKey__c,
                        VTD2_CountryCode__c,
                        VTD2_LocaleSidKey__c,
                        VTD2_StdCountryCode__c
                FROM VT_D2_UserLanguageLocaleAssignment__mdt
        ];
        if (uLocals_mdt.size() == 0) {
            throw new DmlException('Please provide values to the User Language Locale Assignment (custom metadata)!');
        }
        String lang;
        for (VT_D2_UserLanguageLocaleAssignment__mdt lcl : uLocals_mdt) {
            if (String.isBlank(lcl.VTD2_LanguageLocaleKey__c)) {
                lang = '';
            } else {
                lang = lcl.VTD2_LanguageLocaleKey__c;
            }
            uLocals.put(lcl.VTD2_StdCountryCode__c + lang, lcl);
        }
    }

    public static void logError(Database.SaveResult saveResult, HealthCloudGA__CandidatePatient__c cp, String block,
            Exception e) {
        Id cpId = cp.Id;
        if (block != 'General Error') {
            cp.put(block, STATUS_FAILED);
        }
        String errorMassage = '';
        if (saveResult != null) {
            for (Database.Error dbE : saveResult.getErrors()) {
                errorMassage += dbE.getStatusCode() + ' ' + dbE.getMessage() + ' ' + dbE.getFields() + '\n';
            }
        }
        if (e != null) {
            errorMassage += e.getMessage() + e.getStackTraceString();
        }
        VT_R4_PatientConversion.conversionErrors.add(new VTR4_Conversion_Log__c(VTR4_Candidate_Patient__c = cpId,
                VTR4_Error_Message__c = errorMassage));
        //TODO: Partial Conversion
        //if (cp.VTR4_StatusCreate__c != STATUS_FINISHED) {
        cp.VTR4_Convertation_Process_Status__c = VT_R4_PatientConversion.PROCESS_STATUS_COMPLETED;
        cp.VTR5_Conversion_Completed__c = System.now();
        VT_R4_PatientConversion.failedCPs.put(cp.Id, cp);
        //}
    }

    public static void findDuplicates(List<HealthCloudGA__CandidatePatient__c> cpList) {
        Map<String, Case> emailCaseMap = new Map<String, Case>();
        for (HealthCloudGA__CandidatePatient__c cp : cpList) {
            emailCaseMap.put(cp.rr_Email__c.toLowerCase(), null);
        }
        List<Account> accounts = [
                SELECT Id,
                        HealthCloudGA__CarePlan__r.Id,
                        HealthCloudGA__CarePlan__r.VTR4_CandidatePatient__c,
                        HealthCloudGA__CarePlan__r.VTD1_Patient_User__r.Email,
                        HealthCloudGA__CarePlan__r.AccountId,
                        HealthCloudGA__CarePlan__r.ContactId,
                        HealthCloudGA__CarePlan__r.VTD1_Patient__c,
                        HealthCloudGA__CarePlan__r.VTD1_Patient_User__c,
                        HealthCloudGA__CarePlan__r.VTD1_Study__c
                FROM Account
                WHERE HealthCloudGA__CarePlan__r.VTD1_Patient_User__r.Email IN :emailCaseMap.keySet()
        ];
        for (Account account : accounts) {
            emailCaseMap.put(account.HealthCloudGA__CarePlan__r.VTD1_Patient_User__r.Email.toLowerCase(),
                    account.HealthCloudGA__CarePlan__r);
        }
        Map<String, Id> duplicatesCPcase = new  Map<String, Id> ();
        for (HealthCloudGA__CandidatePatient__c cp : cpList) {
            String curEmail = cp.rr_Email__c.toLowerCase();
            if (VT_D1_CandidatePatientProcessHandler.isTransferProcess(cp)) {
                duplicatesCPcase.put(curEmail, cp.VTR4_CaseForTransfer__c);
            } else if (emailCaseMap.get(curEmail) != null) {
                cp.VTD1_DuplicatedRecord__c = true;
            }
        }
        if (!duplicatesCPcase.isEmpty()) {
            Map<Id, Case> caseTransferMap  = new Map<Id, Case>([
                        SELECT Id,
                                VTR4_CandidatePatient__c,
                                VTD1_Patient_User__r.Email,
                                AccountId,
                                ContactId,
                                VTD1_Patient__c,
                                VTD1_Patient_User__c,
                                VTD1_Study__c
                        FROM Case
                    WHERE Id IN :duplicatesCPcase.values()
            ]);
            for (String emailString : duplicatesCPcase.keySet()) {
                System.debug('emailString ' + emailString);
                System.debug('duplicatesCPcase.get(emailString) ' + duplicatesCPcase.get(emailString));
                System.debug('caseTransferMap.get(duplicatesCPcase.get(emailString))' + caseTransferMap.get(duplicatesCPcase.get(emailString)));
                VT_R4_PatientConversion.duplicatesCPcase.put(emailString, caseTransferMap.get(duplicatesCPcase.get(emailString)));
            }

        }
    }
    public static void clearStatusFields(List<HealthCloudGA__CandidatePatient__c> cpList) {
        for (HealthCloudGA__CandidatePatient__c cp : cpList) {
            cp.VTR4_StatusCreate__c = '';
            cp.VTR4_StatusDKDALS__c = '';
            cp.VTR4_StatusGPP__c = '';
            cp.VTR4_StatusSharing__c = '';
            cp.VTR4_StatusSTA__c = '';
            cp.VTR4_StatusStudyStratification__c = '';
            cp.VTR4_StatusVisits__c = '';
        }
    }
    public static Boolean checkConversion(List<HealthCloudGA__CandidatePatient__c> cps, Map<Id, HealthCloudGA__CandidatePatient__c> oldMap) {
        System.debug('checkConversion ' + cps.size());
        Boolean isConverted = false;
        List<Id> communityUserIds = new List<Id>();
        List<Id> cpsToSendEmails = new List<Id>();
        Map<Id, Case> casesForSharingMap = new Map<Id, Case>();
        for (HealthCloudGA__CandidatePatient__c cp : cps) {
            System.debug('checkConversion ' + cp);
            if (cp.Conversion__c != STATUS_CONVERTED && !cp.VTD1_DuplicatedRecord__c) {
                Boolean conversionStatuses = (cp.VTR4_StatusCreate__c == STATUS_FINISHED
                        && cp.VTR4_StatusSTA__c == STATUS_FINISHED
                       // && cp.VTR4_StatusVisits__c == STATUS_FINISHED
                );
                if (!Test.isRunningTest()) {
                    if (cp.VTR4_StatusSharing__c == STATUS_FINISHED) {
                        cp.Conversion__c = STATUS_CONVERTED;
                        isConverted = true;
                    } else if (conversionStatuses) {
                        Id caseId = getCaseForSharing(cp);
                        casesForSharingMap.put(caseId, new Case(Id=caseId));
                        cp.Conversion__c = STATUS_CONVERTED;
                    }
                } else {
                    if (conversionStatuses && cp.VTR4_StatusSharing__c == STATUS_FINISHED) {
                        cp.Conversion__c = STATUS_CONVERTED;
                        isConverted = true;
                    }
                }
                if (cp.Conversion__c == STATUS_CONVERTED && !VT_D1_CandidatePatientProcessHandler.isTransferProcess(cp)) {
                    // send emails to related Patient and Caregiver
                    cpsToSendEmails.add(cp.Id);
                }
            }
            if (cp.Conversion__c == STATUS_CONVERTED) {
                cp.VTR4_Convertation_Process_Status__c = VT_R4_PatientConversion.PROCESS_STATUS_COMPLETED;
                cp.VTR5_Conversion_Completed__c = System.now();
            }

        }
        if (!casesForSharingMap.isEmpty()) {
            initiateSharingCreation(casesForSharingMap);
        }
        if (!cpsToSendEmails.isEmpty()) {
            for (User u : [SELECT Id
            FROM User
            WHERE ContactId IN
            (SELECT Id FROM Contact WHERE Account.Candidate_Patient__c IN :cpsToSendEmails)]) {
                communityUserIds.add(u.Id);
            }
            if (!communityUserIds.isEmpty()) {
                new VT_R3_PublishedAction_ResetCommunityPass(communityUserIds).publish();
            }
        }
        return isConverted;
    }
    private static Id getCaseForSharing(HealthCloudGA__CandidatePatient__c cp) {
        if (cp.VTR4_CaseForTransfer__c == null) {
            return cp.VTR4_CaseForSharing__c;
        }
        return getTransferredCase(cp.VTR4_CaseForSharing__c);
    }
    private static Id getTransferredCase(Id contactId) {
        return [SELECT Id FROM Case WHERE ContactId = :contactId ORDER BY CreatedDate DESC LIMIT 1].Id;
    }
    private static void initiateSharingCreation(Map<Id, Case> caseMap) {
        VT_R5_PatientConversionSharing.createConversionShares(caseMap.keySet());
        System.debug('33');
    }

    public static void retryConversionBlocks(List<HealthCloudGA__CandidatePatient__c> cps, Map<Id, HealthCloudGA__CandidatePatient__c> oldMap) {
       /* System.debug('retryConversionBlocks ' + cps.size());
        Set<Id> cpIds = new Set<Id>();
        for (HealthCloudGA__CandidatePatient__c cp : cps) {
            System.debug('retryConversionBlocks ' + cp.VTR4_TriggerVisits__c + ' ' + cp.VTR4_ConversionRetryTriggered__c);
            if (!cp.VTR4_ConversionRetryTriggered__c || cp.Conversion__c == STATUS_CONVERTED) {
                continue;
            }
            cpIds.add(cp.Id);
        }
        System.debug('retryConversionBlocks ' + cpIds.size());
        if (!cpIds.isEmpty()) {
            HealthCloudGA__CandidatePatient__c cp = cps[0];
            if (cp.VTR4_TriggerCreate__c) {
                VT_R4_PatientConversion.convert(cpIds);
            } else {
                List<Case> cases = [
                        SELECT Id,
                                VTR4_CandidatePatient__c,
                                VTD1_Study__c,
                                VTD2_Study_Geography__c,
                                VTD1_Virtual_Site__c,
                                VTD1_Primary_PG__c,
                                VTD1_Secondary_PG__c
                        FROM Case
                        WHERE VTR4_CandidatePatient__c IN :cpIds
                ];
                System.debug('cases = ' + cases.size());
                VT_R4_PatientConversion.cases.addAll(cases);
                VT_R4_PatientConversion.retryInProgress = true;
                VT_R4_PatientConversion.convert(cpIds);
            }
        }*/
    }
    public class ContactCandidateWrapper {

        public Integer retryNumber = 1;
        public Integer maxRetryNumbers = 3;
        public Contact contact;

        public Contact cgContact;
        public Account account;
        public User user;

        public User cgUser;
        public VTD1_Patient__c patient;
        public HealthCloudGA__CandidatePatient__c candidate;
        public VT_R4_StudyTeamAssignment.StudyTeamPack studyTeamPack;
        public Case pCase;
        public String subjectNumber;
        public ContactCandidateWrapper(Contact patientOrCaregiver, Account account, HealthCloudGA__CandidatePatient__c relatedCandidate) {
            this.contact = patientOrCaregiver;
            this.candidate = relatedCandidate;
            this.account = account;

        }
        public ContactCandidateWrapper(Case patientCase, HealthCloudGA__CandidatePatient__c relatedCandidate){
            this.candidate = relatedCandidate;
            this.pCase = patientCase;

        }
    }
}