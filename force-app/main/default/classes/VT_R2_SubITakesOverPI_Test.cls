/**
 * Created by Alexander Komarov on 26.02.2019.
 */

@isTest
private class VT_R2_SubITakesOverPI_Test {
    @testSetup
    private static void setupMethod() {
        VT_R3_GlobalSharing.disableForTest = true;

        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);

        VT_D1_TestUtils.createTestPatientCandidate(1);
        Id studyId = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1].Id;

        User PI3User = VT_D1_TestUtils.createUserByProfile('Primary Investigator', 'PI3');
        User PI4User = VT_D1_TestUtils.createUserByProfile('Primary Investigator', 'PI4');
        Study_Team_Member__c PI2STM = [SELECT Id,User__r.FirstName,Study_Team_Member__c,User__c FROM Study_Team_Member__c WHERE Study__c = :studyId AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName = 'PIBackupUser1'];

        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();
        VT_D1_TestUtils.persistUsers();

        Study_Team_Member__c PI3STM = new Study_Team_Member__c();
        PI3STM.Study__c = study.Id;
        PI3STM.User__c = PI3User.Id;
        PI3STM.Study_Team_Member__c = PI2STM.Id; //stm.id
        PI3STM.VTD1_Active__c = true;
        PI3STM.VTD1_Ready_For_Assignment__c = true;
        PI3STM.RecordTypeId = VT_R4_ConstantsHelper_ProfilesSTM.RECORD_TYPE_ID_STUDY_TEAM_MEMBER_PI;

        insert PI3STM;
        Test.stopTest();

        //DOA Log, Note to File Step Down, 1572, Acknowledgement Letter
        List<VTD1_Regulatory_Document__c> regdocList = new List<VTD1_Regulatory_Document__c>();
        regdocList.add(new VTD1_Regulatory_Document__c(
                VTD1_Document_Type__c = 'DOA Log',
                Level__c = 'Site',
                Name = 'regdoc1'
        ));
        regdocList.add(new VTD1_Regulatory_Document__c(
                VTD1_Document_Type__c = 'Note to File Step Down',
                Level__c = 'Site',
                Name = 'regdoc2'
        ));
        regdocList.add(new VTD1_Regulatory_Document__c(
                VTD1_Document_Type__c = '1572 IRB Acknowledgement Letter',
                Level__c = 'Site',
                Name = 'regdoc3'
        ));
        insert regdocList;

        List<VTD1_Regulatory_Binder__c> regBinderList = new List<VTD1_Regulatory_Binder__c>();
        for (VTD1_Regulatory_Document__c doc : [SELECT Id FROM VTD1_Regulatory_Document__c LIMIT 3]) {
            VTD1_Regulatory_Binder__c regBinder = new VTD1_Regulatory_Binder__c();
            regBinder.VTD1_Regulatory_Document__c = doc.Id;
            regBinder.VTD1_Care_Plan_Template__c = studyId;
            regBinderList.add(regBinder);
        }
        insert regBinderList;

    }

    @IsTest
    private static void beforeInsertTest() {
        VT_R3_GlobalSharing.disableForTest = true;

        Id study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1].Id;
        Test.startTest();
        Study_Team_Member__c pi = [
                SELECT Id,User__r.FirstName,Study_Team_Member__c
                FROM Study_Team_Member__c
                WHERE Study__c = :study AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName = 'PIUser1'
        ];

        // RAJESH :SH-17440 removed the CRA fields mapping from the virtual site.
        /*
         Study_Team_Member__c pi2 = [
                SELECT Id,User__r.FirstName,Study_Team_Member__c
                FROM Study_Team_Member__c
                WHERE Study__c = :study AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName = 'PIBackupUser1'
         ];
         pi.VTR2_Remote_CRA__c = pi2.Id;
         pi.VTR2_Onsite_CRA__c = pi2.Id;
         update pi;
         */

        Virtual_Site__c vs = new Virtual_Site__c(
                Name = 'Virtual Site',
                VTD1_Study__c = study,
                VTD1_Study_Team_Member__c = pi.Id,
                VTD1_Study_Site_Number__c = '123'
        );
        insert vs;
        Test.stopTest();
    }

    @IsTest
    private static void anotherBeforeUpdateTest() {
        VT_R3_GlobalSharing.disableForTest = true;

        Id study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1].Id;

        //RAJESH :SH-17440 removed the CRA fields mapping from the virtual site and STM.
        Study_Team_Member__c pi = [
                SELECT Id,User__r.FirstName,Study_Team_Member__c/*,VTR2_Remote_CRA__r.User__c*/
                FROM Study_Team_Member__c
                WHERE Study__c = :study AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName = 'PIUser1'
        ];
        Study_Team_Member__c pi2 = [
                SELECT Id,User__r.FirstName,Study_Team_Member__c
                FROM Study_Team_Member__c
                WHERE Study__c = :study AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName = 'PIBackupUser1'
        ];
        Study_Team_Member__c pi3 = [
                SELECT Id,User__r.FirstName,Study_Team_Member__c,User__c
                FROM Study_Team_Member__c
                WHERE Study__c = :study AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName = 'PI3'
        ];
        //RAJESH :SH-17440 removed the CRA fields mapping from the virtual site.
        //  pi.VTR2_Remote_CRA__c = pi3.Id;
        //  pi.VTR2_Onsite_CRA__c = pi2.Id;
        //  update pi;

        Virtual_Site__c vs = new Virtual_Site__c(
                Name = 'Virtual Site',
                VTD1_Study__c = study,
                VTD1_Study_Site_Number__c = '123',
                VTD1_Study_Team_Member__c = pi.Id
        );
        insert vs;

        String error;
        Test.startTest();
        try {
            vs.VTR2_PI_Change_Complete__c = true;
            vs.VTD1_Study_Team_Member__c = pi2.Id;
            update vs;
        } catch (Exception e) {
            error = e.getMessage();
        }
        System.assert(error.contains(System.Label.VTR2_NewBackup_Pi_Error));

        vs.VTR2_New_Backup_PI__c = pi3.User__c;
        update vs;
        try {
            VT_R2_SubIOverPIController.execute(vs.Id);
        } catch (Exception e) {
            System.debug(e);
        }

        VTD1_Regulatory_Document__c regDoc4 = new VTD1_Regulatory_Document__c(
                VTD1_Document_Type__c = 'Acknowledgement Letter',
                Level__c = 'Site',
                Name = 'regdoc4'
        );
        insert regDoc4;

        VTD1_Regulatory_Document__c doc = [SELECT Id,VTD1_Document_Type__c FROM VTD1_Regulatory_Document__c WHERE VTD1_Document_Type__c = 'Acknowledgement Letter' LIMIT 1];
        VTD1_Regulatory_Binder__c regBinder = new VTD1_Regulatory_Binder__c(
                VTD1_Regulatory_Document__c = doc.Id,
                VTD1_Care_Plan_Template__c = study
        );
        insert regBinder;

        VT_R2_SubIOverPIController.execute(vs.Id);

        Test.stopTest();
    }

    /*@IsTest
  private static void beforeUpdateTest() {
      VT_R2_HttpCalloutMockImpl httpMock = new VT_R2_HttpCalloutMockImpl();
      Test.setMock(HttpCalloutMock.class, httpMock);
      httpMock.response = new HttpResponse();
      httpMock.response.setStatus('OK');
      httpMock.response.setStatusCode(200);

      Id study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1].Id;
      Study_Team_Member__c pi = [
              SELECT Id,User__r.FirstName,Study_Team_Member__c
              FROM Study_Team_Member__c
              WHERE Study__c = :study AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName = 'PIUser1'
      ];

      Study_Team_Member__c pi2 = [
              SELECT Id,User__r.FirstName,Study_Team_Member__c
              FROM Study_Team_Member__c
              WHERE Study__c = :study AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName = 'PIBackupUser1'
      ];
      User pi3 = [
              SELECT Id,FirstName
              FROM User
              WHERE FirstName = 'PI4'
      ];
      pi.VTR2_Remote_CRA__c = pi2.Id;
      pi.VTR2_Onsite_CRA__c = pi2.Id;
      update pi;

      Virtual_Site__c vs = new Virtual_Site__c();
      vs.Name = 'Virtual Site';
      vs.VTD1_Study__c = study;
      vs.VTD1_Is_Active__c = true;
      vs.VTD1_Study_Team_Member__c = pi.id;
      vs.VTR2_New_Backup_PI__c = pi3.Id;
      insert vs;

      Case cas = [select id,VTD1_Virtual_Site__c,Status from case where VTD1_Study__c = :study LIMIT 1];
      cas.VTD1_Virtual_Site__c = vs.id;
      cas.Status = 'Does Not Qualify';
      update cas;

      vs.VTR2_PI_Change_Complete__c = true;
      vs.VTD1_Study_Team_Member__c = pi2.id;

      Test.startTest();
      update vs;  // right now its working, but ONLY THIS line making 85 soql queries
      Test.stopTest();
  }*/
}