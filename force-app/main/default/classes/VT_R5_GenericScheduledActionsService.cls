/**
 * Created by Alexey Mezentsev on 10/29/2020.
 */

public without sharing class VT_R5_GenericScheduledActionsService extends VT_R5_HerokuScheduler.AbstractScheduler implements VT_R5_HerokuScheduler.HerokuExecutable, Queueable, Database.AllowsCallouts {
    private List<VT_R5_HerokuScheduler.HerokuSchedulerPayload> payloads = new List<VT_R5_HerokuScheduler.HerokuSchedulerPayload>();
    private List<VT_D2_TNCatalogTasks.ParamsHolder> userTasks = new List<VT_D2_TNCatalogTasks.ParamsHolder>();

    public VT_R5_GenericScheduledActionsService() {
    }

    public VT_R5_GenericScheduledActionsService(List<ScheduledActionData> actions) {
        Datetime now = System.now();
        for (ScheduledActionData actionData : actions) {
            payloads.add(new VT_R5_HerokuScheduler.HerokuSchedulerPayload(
                    actionData.scheduledTime <= now ? now.addMinutes(5) : actionData.scheduledTime,
                    'VT_R5_GenericScheduledActionsService',
                    JSON.serialize(actionData))
            );
        }
    }

    public void execute(QueueableContext context) {
        this.schedule(payloads);
    }

    public void testExecute() {
        this.schedule(payloads);
    }

    public void herokuExecute(String payload) {
        List<ScheduledActionData> actionDataList = (List<ScheduledActionData>) JSON.deserialize(payload, List<ScheduledActionData>.class);
        Map<Id, VT_D2_TNCatalogTasks.ParamsHolder> taskToAssignByCaseMap = new Map<Id, VT_D2_TNCatalogTasks.ParamsHolder>();
        for (ScheduledActionData actionData : actionDataList) {
            String actionName = actionData.actionName;
            Id recordId = actionData.recordId;
            if (actionName == 'ContactPatientForRescreening') {
                taskToAssignByCaseMap.put(recordId, new VT_D2_TNCatalogTasks.ParamsHolder(recordId, '227'));
            } else if (actionName == 'Document_FollowUpTask') {
                userTasks.add(new VT_D2_TNCatalogTasks.ParamsHolder(recordId, 'T535'));
            }
        }
        if (!taskToAssignByCaseMap.isEmpty()) {
            VT_R5_ActualVisitProcessesHandler.assignToPGorSCR(taskToAssignByCaseMap);
            userTasks.addAll(taskToAssignByCaseMap.values());
        }
        if (!userTasks.isEmpty()) {
            VT_D2_TNCatalogTasks.generateTask(userTasks);
        }
    }

    public class ScheduledActionData {
        public Datetime scheduledTime;
        public Id recordId;
        public String actionName;

        public ScheduledActionData(Datetime scheduledTime, Id recordId, String actionName) {
            this.scheduledTime = scheduledTime;
            this.recordId = recordId;
            this.actionName = actionName;
        }
    }
}