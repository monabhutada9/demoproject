@isTest
public with sharing class VT_R2_PopulatePatientLanguageBatchTest {

    public static void testUpdate() {
        System.runAs(new User(Id = UserInfo.getUserId())) {
            List<Contact> contacts = [SELECT Id, VTR2_Primary_Language__c, RecordTypeId FROM Contact WHERE RecordTypeId =:VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PATIENT];
            for(Contact con : contacts){
                con.VTR2_Primary_Language__c = null;
            }
            update contacts;
            Test.startTest();
            Database.executeBatch(new VT_R2_PopulatePatientLanguageBatch());
            Test.stopTest();
        }
    }

}