/**
 * Created by User on 19/05/20.
 */
@isTest
public with sharing class VT_D1_PIStudySwitcherRemoteTest {

    public static void testBehavior() {
        User user = [SELECT Id FROM User WHERE VTD1_Profile_Name__c = 'Primary Investigator' ORDER BY CreatedDate DESC LIMIT 1];
        Study_Site_Team_Member__c sstm = [Select id, VTR2_Associated_PI__c, VTR2_Associated_SCr__r.User__c, VTR2_Associated_PI__r.Study__c From Study_Site_Team_Member__c LIMIT 1 ];
        System.debug(sstm);
        System.debug(sstm.VTR2_Associated_SCr__r.User__c);
        System.debug(user);
        System.runAs(user) {
            Test.startTest();
            VT_D1_PIStudySwitcherRemote.getStudies(false);
            VT_D1_PIStudySwitcherRemote.getStudies(true);
            VT_D1_PIStudySwitcherRemote.getMyStudyIds();
            VT_D1_PIStudySwitcherRemote.getMyStudyIds(null);
            VT_D1_PIStudySwitcherRemote.getMyStudyIds(sstm.VTR2_Associated_PI__r.Study__c);
            try {
                VT_D1_PIStudySwitcherRemote.getStudies(null);
            } catch (Exception e) {
                System.assertEquals('System.AuraHandledException', e.getTypeName());
            }
            Test.stopTest();
        }
    }
}