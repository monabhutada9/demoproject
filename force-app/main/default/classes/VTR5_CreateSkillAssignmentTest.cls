/*
Created By - Yogesh More
Created Date - 17/01/2020
Version - 1.0
Story No. - SH-9363/SH-9180
Discription - This is the test class for VTR5_BatchClassCreateSkillAssignment and VTR5_CreateSkillAssignment class.
*/

@isTest
public class VTR5_CreateSkillAssignmentTest{

    // Setup Method to create the set of records.
    @testSetup static void setup(){
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VTR5_SerResSkillAssignForSC.blockSrCreation = true;

        VT_R5_SerResSkillAssignForSC.blockSrCreation = true;

        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();
        Test.stopTest();
        User use = new user();
        User userSysAdmin =  [SELECT ContactId FROM User WHERE Profile.Name = 'System Administrator' AND Id !=: UserInfo.getUserId() AND IsActive = true LIMIT 1];
        System.runAs(userSysAdmin) {
            use = VT_D1_TestUtils.createUserByProfile('Study Concierge', 'TestStudyConcierge@test.com');
            use.VT_R5_Primary_Preferred_Language__c='en_Us';
            use.VT_R5_Secondary_Preferred_Language__c='af';
            use.VT_R5_Tertiary_Preferred_Language__c='bs';
            use.isactive = true;
            insert use;
        }

        Study_Team_Member__c CRA = new Study_Team_Member__c();
        CRA.Study__c=study.Id;
        CRA.User__c=use.Id;
        CRA.VTD1_Active__c=true;
        CRA.VTD1_Ready_For_Assignment__c=true;
        CRA.RecordTypeId =  Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByName().get('SC').getRecordTypeId();

        insert CRA;

        system.debug('CRA==>'+CRA);
        system.debug('use==>'+use);
    }

    // Test Method to cover valid Scenario
    static testMethod void TestMethod1(){
        Test.StartTest();
        User proUser = [Select id,Name,IsActive,VT_R5_Primary_Preferred_Language__c,VT_R5_Secondary_Preferred_Language__c,VT_R5_Tertiary_Preferred_Language__c from User where profile.name = 'Study Concierge' AND VT_R5_Secondary_Preferred_Language__c='af' limit 1];

        User userSysAdmin =  [SELECT ContactId FROM User WHERE Profile.Name = 'System Administrator' AND Id !=: UserInfo.getUserId() AND IsActive = true LIMIT 1];
        System.runAs(userSysAdmin){
            String query = 'Select id,Name,IsActive,VT_R5_Primary_Preferred_Language__c,VT_R5_Secondary_Preferred_Language__c,VT_R5_Tertiary_Preferred_Language__c from User where profile.name = \'Study Concierge\' AND VT_R5_Secondary_Preferred_Language__c=\'af\' limit 1';
            VTR5_BatchClassCreateSkillAssignment batch = new VTR5_BatchClassCreateSkillAssignment(query);

            //Database.executeBatch(batch, 100);

            Database.executeBatch(batch, 100);
        }
        Test.StopTest();

        List<ServiceResource> lstSR = [Select id, (select id,Skill.DeveloperName,Skill.Id from ServiceResourceSkills) from ServiceResource where RelatedRecordId =: proUser.Id];
        System.assert(lstSR.size() > 0);
        System.assertEquals(true, lstSR[0].ServiceResourceSkills.size() > 0);
    }

    // Test Method to cover invalid Scenario
    static testMethod void TestMethod2(){

        Test.StartTest();
        User proUser = [Select id,Name,IsActive,VT_R5_Primary_Preferred_Language__c,VT_R5_Secondary_Preferred_Language__c,VT_R5_Tertiary_Preferred_Language__c from User where profile.name = 'Study Concierge' AND VT_R5_Secondary_Preferred_Language__c='af' limit 1];

        String query = 'Select id,Name,IsActive,VT_R5_Primary_Preferred_Language__c,VT_R5_Secondary_Preferred_Language__c,VT_R5_Tertiary_Preferred_Language__c from User where isactive = false limit 1';
        VTR5_BatchClassCreateSkillAssignment batch = new VTR5_BatchClassCreateSkillAssignment(query);
        Database.executeBatch(batch, 100);
        Test.stopTest();

        List<ServiceResource> lstSR = new List<ServiceResource>();
        lstSR = [Select id, (select id,Skill.DeveloperName,Skill.Id from ServiceResourceSkills) from ServiceResource where RelatedRecordId =: proUser.Id];

        //System.assert(lstSR.size() == 0); -- commented out due to valid scenario


    }

}