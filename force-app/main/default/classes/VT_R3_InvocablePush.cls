/**
 * Created by Alexander Komarov on 22.07.2019.
 */


/**
 * @description Called from flow
 *
 * @see VTR3_Mobile_Push_15_mins_before_Video_Conf.flow
 */

public without sharing class VT_R3_InvocablePush {

    @InvocableMethod
    public static void createPushNotifications(List<Id> conferenceIds) {
        List<Video_Conference__c> videoConferencesToCheck = [
                SELECT Id, (
                        SELECT VTD1_User__r.VTR3_Verified_Mobile_Devices__c
                        FROM Conference_Members__r
                )
                FROM Video_Conference__c
                WHERE Id IN :conferenceIds
        ];
        Set<Id> conferencesWithMobileUsers = new Set<Id>();
        for (Video_Conference__c videoConference : videoConferencesToCheck) {
            if (videoConference.Conference_Members__r != null) {
                for (VTD1_Conference_Member__c conferenceMember : videoConference.Conference_Members__r) {
                    if (conferenceMember.VTD1_User__r != null) {
                        if (String.isNotBlank(conferenceMember.VTD1_User__r.VTR3_Verified_Mobile_Devices__c)) {
                            conferencesWithMobileUsers.add(videoConference.Id);
                            break;
                        }
                    }
                }
            }
        }

        List<Id> conferencesToPush = new List<Id>();

        for (VTD1_Actual_Visit__c av : [
                SELECT
                        Id, (
                        SELECT
                                Id,
                                Scheduled_For__c,
                                VTD1_Scheduled_Visit_End__c
                        FROM Video_Conferences__r
                        ORDER BY CreatedDate DESC
                        LIMIT 1
                )
                FROM VTD1_Actual_Visit__c
                WHERE Id IN (
                        SELECT VTD1_Actual_Visit__c
                        FROM Video_Conference__c
                        WHERE Id IN :conferencesWithMobileUsers
                )
        ]) {
            Id lastScheduledConference = av.Video_Conferences__r[0].Id;
            if (conferencesWithMobileUsers.contains(lastScheduledConference)) {
                conferencesToPush.add(lastScheduledConference);
            }
        }

        if (conferencesToPush.size() > 0) {
            if (conferencesToPush.size() <= ((System.Limits.getLimitMobilePushApexCalls() - System.Limits.getMobilePushApexCalls()) / 2)) {
                List<Video_Conference__c> toPush = [
                        SELECT
                                Name,
                                VTD1_Actual_Visit__c,
                                Scheduled_For__c,
                                VTD1_Scheduled_Visit_End__c
                        FROM Video_Conference__c
                        WHERE Id IN :conferencesToPush
                ];
                sendPushNotificationsSync(toPush);
            } else if (!(System.isFuture() || System.isBatch())) {
                VT_R4_BatchVideoConferencePush batch = new VT_R4_BatchVideoConferencePush(conferencesToPush);
                Database.executeBatch(batch, 5);
            }
        }
    }

    public static void sendPushNotificationsSync(List<Video_Conference__c> conferencesToPush) {
        List<VTD1_General_Settings__mdt> generalSettingsList = [
                SELECT VTD1_Connected_App_API_Name_for_Android__c,
                        VTR3_Connected_App_API_Name_for_iOS__c
                FROM VTD1_General_Settings__mdt
        ];

        List<Id> visitsIds = new List<Id>();
        for (Video_Conference__c v : conferencesToPush) {
            visitsIds.add(v.VTD1_Actual_Visit__c);
        }
        Map<Id, Set<String>> visitToPatientsIds = new Map<Id, Set<String>>();
        Map<Id, String> visitToTranslatedMessage = new Map<Id, String>();

        for (Visit_Member__c vm : [
                SELECT
                        VTD1_Participant_User__c,
                        VTD1_Participant_User__r.LanguageLocaleKey,
                        VTD1_Actual_Visit__c,
                        VTD1_Actual_Visit__r.VTD1_Protocol_Visit__r.VTD1_EDC_Name__c,
                        VTD1_Actual_Visit__r.VTD1_Unscheduled_Visit_Type__c,
                        VTD1_Actual_Visit__r.Name,
                        VTD1_Is_internal__c
                FROM Visit_Member__c
                WHERE VTD1_Actual_Visit__c IN :visitsIds AND (VTD1_Member_Type__c = 'Patient' OR VTD1_Member_Type__c = 'Caregiver')
        ]) {
            if (!visitToPatientsIds.containsKey(vm.VTD1_Actual_Visit__c)) {
                visitToPatientsIds.put(vm.VTD1_Actual_Visit__c, new Set<String>());
            }
            if (vm.VTD1_Is_internal__c) {
                if (!visitToTranslatedMessage.containsKey(vm.VTD1_Actual_Visit__c)) {
                    String visitName = VT_D1_HelperClass.getActualVisitName(vm.VTD1_Actual_Visit__r, vm.VTD1_Participant_User__r.LanguageLocaleKey);
                    String translatedLabel = VT_D1_TranslateHelper.getLabelValue(Label.VTR4_TelevisitInMinutes, vm.VTD1_Participant_User__r.LanguageLocaleKey);
                    visitToTranslatedMessage.put(vm.VTD1_Actual_Visit__c, translatedLabel.replace('#1', visitName));
                }
            }
            visitToPatientsIds.get(vm.VTD1_Actual_Visit__c).add(String.valueOf(vm.VTD1_Participant_User__c));
        }
        for (Video_Conference__c v : conferencesToPush) {
            Id visit = v.VTD1_Actual_Visit__c;
            String message = visitToTranslatedMessage.get(visit);
            Long visitEndDateTime = v.VTD1_Scheduled_Visit_End__c.getTime();
            Long visitStartDateTime = v.Scheduled_For__c.getTime();
            sendPushNotification(generalSettingsList, visitToPatientsIds.get(visit), visit, visitEndDateTime, visitStartDateTime, message);
        }


    }

    private static void sendPushNotification(List<VTD1_General_Settings__mdt> settings, Set<String> usersIds, Id actualVisit, Long visitStartDateTimeInc, Long visitEndDateTimeInc, String message) {

        String appAPINameIos = settings[0].VTR3_Connected_App_API_Name_for_iOS__c;
        String appAPINameAndroid = settings[0].VTD1_Connected_App_API_Name_for_Android__c;

        Messaging.PushNotification msgIos = new Messaging.PushNotification();
        Messaging.PushNotification msgAndroid = new Messaging.PushNotification();
        Map<String, String> pushDataIos = new Map<String, String>();
        Map<String, Object> androidPayload = new Map<String, Object>();
        String visitStartDateTime = String.valueOf(visitStartDateTimeInc);
        String visitEndDateTime = String.valueOf(visitEndDateTimeInc);

        String pushMessage = message;


        pushDataIos.put('visitId', actualVisit);
        pushDataIos.put('visitStartDateTime', visitStartDateTime);
        pushDataIos.put('visitEndDateTime', visitEndDateTime);

        androidPayload.put('visitId', actualVisit);
        androidPayload.put('visitEndDateTime', visitEndDateTime);
        androidPayload.put('visitStartDateTime', visitStartDateTime);
        androidPayload.put('message', pushMessage);

        msgAndroid.setPayload(androidPayload);


        Map<String, Object> payload = Messaging.PushNotificationPayload.apple(pushMessage, '', null, pushDataIos);
        msgIos.setPayload(payload);

        Set<String> users = usersIds;


        try {
            msgIos.send(appAPINameIos, users);
            system.debug('Push notification was sent');
            System.debug('msg' + msgIos);
            System.debug('users' + users);
            System.debug('payload' + payload);
        } catch (Exception e) {
            system.debug('Error occurred when sending Push notification to IOS');
        }

        try {
            msgAndroid.send(appAPINameAndroid, users);
            System.debug('Push notification was sent');
            System.debug('msg' + msgAndroid);
            System.debug('users' + users);
            System.debug('androidPayload' + androidPayload);

        } catch (Exception e) {
            System.debug('Error occurred when sending Push notification to Android');
        }
    }
}