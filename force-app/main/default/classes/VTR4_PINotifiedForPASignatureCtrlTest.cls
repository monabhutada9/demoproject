/**
 * Created by maksimfesenko on 12/8/20.
 */

@IsTest
private class VTR4_PINotifiedForPASignatureCtrlTest {

    @TestSetup
    private static void setupData() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);

        Virtual_Site__c virtualSite = new Virtual_Site__c(
            VTD1_PI_User__c = UserInfo.getUserId(),
            VTD1_Study__c = study.Id,
            VTD1_Study_Site_Number__c = '1'
        );
        insert virtualSite;

        Case patient = new Case(
            VTD1_Patient_User__c = UserInfo.getUserId(),
            VTD1_Study__c = study.Id
        );
        insert patient;

        insert new VTD1_Document__c(
            VTD1_Site__c = virtualSite.Id,
            VTD1_Clinical_Study_Membership__c = patient.Id
        );
    }

    @IsTest
    private static void testCoverage() {
        Id documentId = [SELECT Id FROM VTD1_Document__c LIMIT 1].Id;

        System.Test.startTest();

        VTR4_PINotifiedForPASignatureController controller = new VTR4_PINotifiedForPASignatureController();
        controller.documentId = documentId;
        String lastName = controller.getPiLastName();
        String protocolName = controller.getProtocolNickName();

        System.Test.stopTest();

        User user = [SELECT LastName FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        HealthCloudGA__CarePlanTemplate__c study = [SELECT VTD1_Protocol_Nickname__c FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];

        System.assertEquals(user.LastName, lastName);
        System.assertEquals(study.VTD1_Protocol_Nickname__c, protocolName);
    }
}