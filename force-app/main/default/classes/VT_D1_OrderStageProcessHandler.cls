/**
 * Created by Leonid Bartenev
 */

public class VT_D1_OrderStageProcessHandler {

    @Future(Callout = true)
    public static void createPatientDeliveryRecordsFuture(Set<Id> orderStageIds){
        createPatientDeliveryRecords(orderStageIds);
    }
    
    public static void createPatientDeliveryRecords(Set<Id> orderStageIds) {
        //prepare list of processed fields:
        Set<String> processedFields = new Set<String>();
        List<Schema.FieldSetMember> fieldMembers = SObjectType.VTD1_OrderStage__c.FieldSets.Fields_to_Order.getFields();
        for(FieldSetMember fsm : fieldMembers) processedFields.add(fsm.getFieldPath());
        Set<String> queryFields = new Set<String>(processedFields);
        queryFields.add(VTD1_OrderStage__c.VTD1_ShipmentId__c + '');
        queryFields.add(VTD1_OrderStage__c.VTD1_IntegrationId__c + '');
        
        //query order stages with fields:
        String query =
                ' SELECT ' + String.join(new List<String>(queryFields), ',') +
                        ' FROM VTD1_OrderStage__c ' +
                        ' WHERE Id IN (\'' + String.join(new List<Id>(orderStageIds), '\',\'') + '\')';
        List<VTD1_OrderStage__c> orderStagesList = Database.query(query);
        
        //prepare id sets:
        Set<String> shipmentIds = new Set<String>();
        Set<String> integrationIds = new Set<String>();
        for (VTD1_OrderStage__c orderStageItem : orderStagesList) {
            shipmentIds.add(orderStageItem.VTD1_ShipmentId__c);
            integrationIds.add(orderStageItem.VTD1_IntegrationId__c);
        }

        System.debug('SHIPMENT IDS: ' + shipmentIds);
        System.debug('INT IdS: ' + integrationIds);
        
        //prepare orders maps:
        List<VTD1_Order__c> orderList = [
                SELECT Id,
                        VTD1_TrackingNumber__c,
                        VTD1_IntegrationId__c,
                        VTD1_ShipmentId__c
                FROM VTD1_Order__c
                WHERE VTD1_IntegrationId__c IN: integrationIds
                OR VTD1_ShipmentId__c IN: shipmentIds
                ORDER BY VTD1_ShipmentId__c NULLS FIRST
        ];
        System.debug('ORDERS: ' + orderList);
        Map<String, List<VTD1_Order__c>> ordersByShipmentIdMap = new Map<String, List<VTD1_Order__c>>();
        Map<String, List<VTD1_Order__c>> ordersByIntegrationIdMap = new Map<String, List<VTD1_Order__c>>();
        for(VTD1_Order__c order : orderList){
            if(order.VTD1_ShipmentId__c != null) {
                List<VTD1_Order__c> itemOrders = ordersByShipmentIdMap.get(order.VTD1_ShipmentId__c);
                if(itemOrders == null) itemOrders = new List<VTD1_Order__c>();
                itemOrders.add(order);
                ordersByShipmentIdMap.put(order.VTD1_ShipmentId__c, itemOrders);
            }
            if(order.VTD1_IntegrationId__c != null && order.VTD1_ShipmentId__c == null) {
                List<VTD1_Order__c> itemOrders = ordersByIntegrationIdMap.get(order.VTD1_IntegrationId__c);
                if(itemOrders == null) itemOrders = new List<VTD1_Order__c>();
                itemOrders.add(order);
                ordersByIntegrationIdMap.put(order.VTD1_IntegrationId__c, itemOrders);
            }
        }
    
        //process order stages:
        List<VTD1_Order__c> ordersForUpdate = new List<VTD1_Order__c>();
        Map<Id, VTD1_OrderStage__c> orderStagesByOrderIdMap = new Map<Id, VTD1_OrderStage__c>();
        for (VTD1_OrderStage__c orderStageItem : orderStagesList) {
            List<VTD1_Order__c> orderItemList = ordersByShipmentIdMap.get(orderStageItem.VTD1_ShipmentId__c);
            System.debug('ORDER ITEM LIST: ' + orderItemList);
            VTD1_Order__c orderItem;
            if(orderItemList == null) {
                orderItemList = ordersByIntegrationIdMap.get(orderStageItem.VTD1_IntegrationId__c);
                System.debug('ORDER ITEM LIST: ' + orderItemList);
                if(orderItemList != null){
                    if(orderItemList.size() > 1){
                        System.debug('WARN: Multiple Integration Id');
                        orderStageItem.VTD1_ProcessingResult__c = 'Several deliveries found';
                        continue;
                    }else{
                        orderItem = orderItemList[0];
                    }
                }
            }else{
                if(orderItemList.size() > 1){
                    System.debug('WARN: Multiple Shipment Id');
                    orderStageItem.VTD1_ProcessingResult__c = 'Several deliveries found';
                    continue;
                }else{
                    orderItem = orderItemList[0];
                }
            }
            if(orderItem == null) {
                System.debug('WARN: Delivery not found');
                orderStageItem.VTD1_ProcessingResult__c = 'Delivery not found';
                continue;
            }
            System.debug('ORDER: ' + orderItem);
            orderStagesByOrderIdMap.put(orderItem.Id, orderStageItem);
            try {
                for (String fieldName : processedFields) {
                    System.debug('fieldName=' + fieldName + ', val=' + orderStageItem.get(fieldName));
                    orderItem.put(fieldName, orderStageItem.get(fieldName));
                }
                if (orderItem.VTD1_ChangeSource__c == null) orderItem.VTD1_ChangeSource__c = 'CSM';
                orderItem.VTD1_ShipmentId__c = orderStageItem.VTD1_ShipmentId__c;
                ordersForUpdate.add(orderItem);
            } catch (Exception e) {
                orderStageItem.VTD1_ProcessingResult__c = e.getMessage();
            }
        }
        
        //handle errors:
        System.debug('ORDERS FOR UPDATE: ' + ordersForUpdate);
        List<Database.SaveResult> srList = Database.update(ordersForUpdate, false);
        for (Database.SaveResult sr : srList) {
            System.debug('SR: ' + sr);
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted account. Account ID: ' + sr.getId());
                orderStagesByOrderIdMap.get(sr.getId()).VTD1_ProcessingResult__c = 'OK';
            } else {
                // Operation failed, so get all errors
                for (Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Account fields that affected this error: ' + err.getFields());
                    orderStagesByOrderIdMap.get(sr.getId()).VTD1_ProcessingResult__c = err.getMessage();
                }
            }
        }
        update orderStagesList;
    }

}