/**
 * Created by user on 24.06.2020. not longer needed - switched to batch for conversion instead of platform events
 */

public with sharing class VT_R4_STMAssignmentHandler {
    /*public void processEvents(List <VTR4_New_Transaction__e> events) {
        try {
            List <VT_R4_PatientConversion.CPWrapper> cpWraps = new List<VT_R4_PatientConversion.CPWrapper>();
            for (VTR4_New_Transaction__e event : events) {
                VT_R4_PatientConversion.CPWrapper cpw = (VT_R4_PatientConversion.CPWrapper)JSON.deserialize(event.VTR4_Parameters__c,
                        VT_R4_PatientConversion.CPWrapper.class);
                cpWraps.add(cpw);
            }
            try {
                VT_R4_StudyTeamAssignment.getStudyTeam(cpWraps, true);
            } catch (Exception e) {
                for (VT_R4_PatientConversion.CPWrapper wrapper : cpWraps) {
                    VT_R4_PatientConversionHelper.logError(null, wrapper.cp, 'VTR4_StatusSTA__c', e);
                }
                VT_R4_PatientConversion.saveErrors();
            }
        } catch (Exception e) {
            insert new VTR4_Conversion_Log__c(VTR4_Error_Message__c = e.getMessage()
                    + ' ' + e.getStackTraceString()
                    + ' ' + e.getCause() + e.getLineNumber() + ' ' + e.getTypeName());
        }
    }

    public Object call(String action, Map<String, Object> args) {
        processEvents((List <VTR4_New_Transaction__e>)args.get('events'));
        return null;
    }*/
}