/**
 * @author: Alexander Komarov
 * @date: 02.09.2020
 * @description:
 */

public with sharing class VT_R5_MobileTimezoneUpdater extends VT_R5_MobileRestResponse implements VT_R5_MobileRestRouter.Routable {

    Boolean successUpdate = false;
    String newTimeZone;

    public List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/timezone-update/'
        };
    }

    public void versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        switch on this.request.httpMethod {
            when 'POST' {
                switch on requestVersion {
                    when 'v1' {
                        updateTimezone();
                        return;
                    }
                }
            }
        }
        VT_R5_MobileRestRouter.unsupportedVersionResponse();
        this.addError('unsupported version');
        this.sendResponse();
    }

    private void updateTimezone() {
        try {
            RequestParams requestParams = (RequestParams) JSON.deserialize(request.requestBody.toString(), RequestParams.class);
            successUpdate = updateUserTimezone(requestParams.newTimezone);
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private Map<String, String> getValidTimezones(User u) {
        return VT_D1_CustomMultiPicklistController.getSelectOptionsWithLabel(u, 'TimeZoneSidKey');
    }

    private Boolean updateUserTimezone(String newTimezone) {
        User currentUser = [SELECT Id, TimeZoneSidKey FROM User WHERE Id = :UserInfo.getUserId()];
        if (newTimezone == null) {
            return false;
        } else {
            if (!currentUser.TimeZoneSidKey.equals(newTimezone)) {
                TimeZone timeZone = TimeZone.getTimeZone(newTimezone);
                String gmtShift = timeZone.getDisplayName().substring(1, 10);
                Map<String, String> salesforceTimezones = getValidTimezones(currentUser);
                String timeZoneToUpdateByShift;
                String timeZoneToUpdateByKey;
                for (String zone : salesforceTimezones.keySet()) {
                    if (salesforceTimezones.get(zone).contains(gmtShift)) {
                        timeZoneToUpdateByShift = zone;
                    }
                    if (zone.equals(newTimezone)) {
                        timeZoneToUpdateByKey = zone;
                    }
                }
                currentUser.TimeZoneSidKey = timeZoneToUpdateByKey == null ? timeZoneToUpdateByShift : timeZoneToUpdateByKey;
                this.newTimeZone = currentUser.TimeZoneSidKey;
                update currentUser;
            }
            return true;
        }
    }

    public void initService() {
    }

    private class RequestParams {
        private String newTimezone;
    }
}