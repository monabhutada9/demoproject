@IsTest
public class VT_R5_eCoaDupeAnswerCleanerTest {
    @TestSetup
    static void testSetup() {
        Case cas = new Case(RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VTD1_PCF').getRecordTypeId());
        insert cas;
        List<VTD1_Survey__c> surveys = new List<VTD1_Survey__c>{
            new VTD1_Survey__c(VTD1_CSM__c = cas.Id, VTR5_Response_GUID__c = '1234'),
            new VTD1_Survey__c(VTD1_CSM__c = cas.Id, VTR5_Response_GUID__c = '5678')
        };
        insert surveys;
        insert new List<VTD1_Survey_Answer__c>{
            new VTD1_Survey_Answer__c(VTD1_Survey__c = surveys[0].Id, VT_R5_External_Widget_Data_Key__c = 'abcd'),
            new VTD1_Survey_Answer__c(VTD1_Survey__c = surveys[0].Id, VT_R5_External_Widget_Data_Key__c = 'abcd'),
            new VTD1_Survey_Answer__c(VTD1_Survey__c = surveys[0].Id, VT_R5_External_Widget_Data_Key__c = 'defg'),
            new VTD1_Survey_Answer__c(VTD1_Survey__c = surveys[0].Id, VT_R5_External_Widget_Data_Key__c = 'defg'),
            new VTD1_Survey_Answer__c(VTD1_Survey__c = surveys[0].Id, VT_R5_External_Widget_Data_Key__c = 'hijk'),
            new VTD1_Survey_Answer__c(VTD1_Survey__c = surveys[1].Id, VT_R5_External_Widget_Data_Key__c = 'abcd'),
            new VTD1_Survey_Answer__c(VTD1_Survey__c = surveys[1].Id, VT_R5_External_Widget_Data_Key__c = 'defg'),
            new VTD1_Survey_Answer__c(VTD1_Survey__c = surveys[1].Id, VT_R5_External_Widget_Data_Key__c = 'defg'),
            new VTD1_Survey_Answer__c(VTD1_Survey__c = surveys[1].Id, VT_R5_External_Widget_Data_Key__c = 'hijk'),
            new VTD1_Survey_Answer__c(VTD1_Survey__c = surveys[1].Id, VT_R5_External_Widget_Data_Key__c = 'hijk')
        };
    }

    @IsTest
    static void doTest() {
        VT_R5_eCoaDupeAnswerCleaner cleaner = new VT_R5_eCoaDupeAnswerCleaner();
        cleaner.query = 'SELECT Id FROM VTD1_Survey__c';
        Test.startTest();
        {
            Database.executeBatch(cleaner);
        }
        Test.stopTest();
        for (AggregateResult ar : [SELECT COUNT(Id) cnt, VTD1_Survey__c FROM VTD1_Survey_Answer__c GROUP BY VTD1_Survey__c]) {
            System.assertEquals(3, (Integer) ar.get('cnt'), 'Wrong number of remaining answers');
        }
    }
}