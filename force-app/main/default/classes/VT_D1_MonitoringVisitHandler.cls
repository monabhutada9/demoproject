public without sharing class VT_D1_MonitoringVisitHandler {

    public void onBeforeDelete(List<VTD1_Monitoring_Visit__c> recs) {
        VT_D1_LegalHoldDeleteValidator.validateDeletion(recs);
    }

    public void onBeforeInsert(List<VTD1_Monitoring_Visit__c> recs) {
        setScheduledTimes(recs, null);
        validateUserInSSTM(recs); //Added by Priyanka Ambre : SH-17441 
    }

    public void onBeforeUpdate(List<VTD1_Monitoring_Visit__c> recs, Map<Id, VTD1_Monitoring_Visit__c> oldMap) {
        setScheduledTimes(recs, oldMap);
        validateDateTimeIsAvailableBeforeFlow(recs, oldMap);
    }

    public void onAfterUpdate(List<VTD1_Monitoring_Visit__c> recs, Map<Id, VTD1_Monitoring_Visit__c> newMap,
                                                        Map<Id, VTD1_Monitoring_Visit__c> oldMap) {
        createMilestones(recs, oldMap);
        monitoringVisitsSharing(recs, newMap, oldMap);
    }

    public void onAfterInsert(List<VTD1_Monitoring_Visit__c> recs, Map<Id, VTD1_Monitoring_Visit__c> newMap,
                                                        Map<Id, VTD1_Monitoring_Visit__c> oldMap) {
        monitoringVisitsSharing(recs, newMap, oldMap);
        validateDateTimeIsAvailableBeforeFlow(recs, oldMap);
    }

    private void createMilestones(List<VTD1_Monitoring_Visit__c> recs, Map<Id, VTD1_Monitoring_Visit__c> oldMap) {
        VT_D1_MilestoneCreator milestoneCreator = new VT_D1_MilestoneCreator();
        for (VTD1_Monitoring_Visit__c item : recs) {
            VTD1_Monitoring_Visit__c old = oldMap.get(item.Id);

            if (VT_D1_MilestoneHelper.isVisitCloseout(item) && !VT_D1_MilestoneHelper.isVisitCloseout(old)) {
                milestoneCreator.addPotentialStudyMilestone(item.VTD1_Study__c, VT_R4_ConstantsHelper_AccountContactCase.STUDY_FIRST_VIRTUAL_SITE_CLOSED);
            }
            if (VT_D1_MilestoneHelper.isVisitInitiated(item) && !VT_D1_MilestoneHelper.isVisitInitiated(old)) {
                milestoneCreator.addPotentialStudyMilestone(item.VTD1_Study__c, VT_R4_ConstantsHelper_AccountContactCase.STUDY_FIRST_VIRTUAL_SITE_INITIATED);
                milestoneCreator.addPotentialSiteMilestone(item.VTD1_Virtual_Site__c, VT_R4_ConstantsHelper_AccountContactCase.SITE_VIRTUAL_SITE_INITIATED);
            }
            if (VT_D1_MilestoneHelper.isVisitSelected(item) && !VT_D1_MilestoneHelper.isVisitSelected(old)) {
                milestoneCreator.addPotentialStudyMilestone(item.VTD1_Study__c, VT_R4_ConstantsHelper_AccountContactCase.STUDY_FIRST_VIRTUAL_SITE_SELECTED);
                milestoneCreator.addPotentialSiteMilestone(item.VTD1_Virtual_Site__c, VT_R4_ConstantsHelper_AccountContactCase.SITE_VIRTUAL_SITE_SELECTED);
            }

        }
        milestoneCreator.createMilestones();
    }

    private static void monitoringVisitsSharing(List<VTD1_Monitoring_Visit__c> recs,
                                                Map<Id, VTD1_Monitoring_Visit__c> newMap, 
                                                Map<Id, VTD1_Monitoring_Visit__c> oldMap) {
                                                    
        List<VTD1_Monitoring_Visit__c> toCalcSharing = Trigger.isInsert ? recs : new List<VTD1_Monitoring_Visit__c>();
        if (Trigger.isUpdate) {
            for (VTD1_Monitoring_Visit__c visit : recs) {
                if (visit.VTR2_Visit_CRA__c != oldMap.get(visit.id).VTR2_Visit_CRA__c) {
                    toCalcSharing.add(visit);
                }
            }
        }

        // Conquerors
        // Abhay & Akansksha
        // Jira Ref: SH-17438, SH-17437
        // Description: Added a block to execute sharing for only the relevant CRAs that are part of the SSTMs as well.

        // Update: Commented the craUserIdSet and the condition is changed hence it is not needed anymore. 
        if (!toCalcSharing.isEmpty()) {
            
            // Set<Id> craUserIdSet = new Set<Id>();

            Set<Id> siteIdSet = new Set<Id>();
            Map<Id, VTD1_Monitoring_Visit__c> siteAndMonitorVisitMap = new Map<Id, VTD1_Monitoring_Visit__c>();
            List<VTD1_Monitoring_Visit__c> monitorVisitListToShare = new List<VTD1_Monitoring_Visit__c>();
            for(VTD1_Monitoring_Visit__c mvObj: toCalcSharing) {

                // craUserIdSet.add(mvObj.VTR5_VisitCRAUser__c);

                siteIdSet.add(mvObj.VTD1_Virtual_Site__c);
                siteAndMonitorVisitMap.put(mvObj.VTD1_Virtual_Site__c, mvObj);
            }
            for(Study_Site_Team_Member__c sstmObj : [SELECT Id, 
                                                            VTR5_Associated_CRA__c,
                                                            VTR5_Associated_CRA__r.User__c, 
                                                            VTD1_SiteID__c 
                                                        FROM Study_Site_Team_Member__c 
                                                        WHERE VTD1_SiteID__c IN: siteIdSet

                                                        // Conquerors
                                                        // Abhay
                                                        // Jira Ref: SH-17438
                                                        // Description: Changing the where condition for this SOQL query
                                                        // AND VTR5_Associated_CRA__r.User__c IN: craUserIdSet
                                                        AND VTR5_Associated_CRA__r.User__c <> NULL

                                                        ]) {
                
                if(siteAndMonitorVisitMap.containsKey(sstmObj.VTD1_SiteID__c)) {
                    monitorVisitListToShare.add(siteAndMonitorVisitMap.get(sstmObj.VTD1_SiteID__c));
                }
            }
            if(!monitorVisitListToShare.isEmpty()) {
                VT_R3_GlobalSharing.doSharing(monitorVisitListToShare);
            }
            // End of code: Conquerors
        }
    }

    private void setScheduledTimes(List<VTD1_Monitoring_Visit__c> recs, Map<Id, VTD1_Monitoring_Visit__c> oldMap) {
        List<VTD1_Monitoring_Visit__c> toSetTimes = new List<VTD1_Monitoring_Visit__c>();

        for (VTD1_Monitoring_Visit__c item : recs) {
            if ((oldMap == null && item.VTD1_Visit_Start_Date__c != null) ||
                    (oldMap != null && item.VTD1_Visit_Start_Date__c != oldMap.get(item.Id).VTD1_Visit_Start_Date__c)) {
                toSetTimes.add(item);
            }
        }

        if (!toSetTimes.isEmpty()) {
            new VT_R2_MonitoringVisitTimeCalculator().calculateTimes(toSetTimes);
        }
    }

    private Boolean isReadyForFlow(VTD1_Monitoring_Visit__c visit) {
        if ((Trigger.isInsert
                && visit.VTD1_Visit_Start_Date__c != null
                && visit.VTD1_Virtual_Site__c != null
                && visit.VTD1_Study_Site_Visit_Status__c == 'Planned'
                && visit.VTD1_Remote_Monitoring_Visit_Report_Stat__c != 'Approved')
                ||
                (Trigger.isUpdate
                        && visit.VTD1_Visit_Start_Date__c != null
                        && visit.VTD1_Virtual_Site__c != null
                        && visit.VTD1_Study_Site_Visit_Status__c == 'Planned'
                        && visit.VTD1_Remote_Monitoring_Visit_Report_Stat__c != 'Approved'
                        && visit.VTD1_Assigned_PI__c != null
                        && visit.VTD1_Study__c != null
                )) {
            return true;
        } else {
            return false;
        }
    }

    private void validateDateTimeIsAvailableBeforeFlow(List<VTD1_Monitoring_Visit__c> recs, Map<Id, VTD1_Monitoring_Visit__c> oldMap) {
        List<VTD1_Monitoring_Visit__c> visits = new List<VTD1_Monitoring_Visit__c>();
        for (VTD1_Monitoring_Visit__c visit : recs) {
            if (isReadyForFlow(visit) && (oldMap == null || !isReadyForFlow(oldMap.get(visit.Id)))) {
                visits.add(visit);
            }
        }
        VT_R4_ValidationMVScheduled val = new VT_R4_ValidationMVScheduled();
        val.isItAvailableTimeForVisit(visits);
    }
    
    /*******************************************************************************************************
    * @author                   Priyanka Ambre (SH-17441)
    * @description              Validating and updating logged in user as CRA User on Monitoring Visit
    * @param recs      			List of all the Monitoring visit which are inserted
    */
     private void validateUserInSSTM(List<VTD1_Monitoring_Visit__c> recs) { 
        Profile profileCRA = [SELECT Id 
                              FROM Profile 
                              WHERE Name =: VT_R4_ConstantsHelper_Profiles.CRA_PROFILE_NAME 
                              LIMIT 1];
        if(profileCRA != null && UserInfo.getProfileId() == profileCRA.Id) {            
            Map<Id, Study_Team_Member__c> mapSiteToSTM = new Map<Id, Study_Team_Member__c>();

            for(VTD1_Monitoring_Visit__c objVisit : recs) {
                if(objVisit.VTD1_Virtual_Site__c != null) {
                    mapSiteToSTM.put(objVisit.VTD1_Virtual_Site__c, null); //get all the virtual site Ids from given Monitoring visits.
                }
            } 

            //get the STM which have logged in user in User__c field.
            //CRA profile dont have access to SSTM Object but we need SSTM's to get Associated CRA therefore
            //WITH SECURITY_ENFORCED clause is not used.
            List<Study_Site_Team_Member__c> lstSSTMs = [SELECT	Id,
                                                     		VTR5_Associated_CRA__c,
                                                     		VTR5_Associated_CRA__r.User__c, 
                                                     		VTD1_SiteID__c 
                                                     FROM Study_Site_Team_Member__c 
                                                     WHERE VTD1_SiteID__c IN:mapSiteToSTM.keyset()
                                                     AND VTR5_Associated_CRA__c <> NULL
                                                     AND VTR5_Associated_CRA__r.User__c =: UserInfo.getUserId()
                                                    ];

            //As there is no CRUD operation is performed in the for loop therefore SOQL For Loop is not used.             
            for(Study_Site_Team_Member__c objSSTM : lstSSTMs) {
                if(mapSiteToSTM.containsKey(objSSTM.VTD1_SiteID__c) 
                && objSSTM.VTR5_Associated_CRA__r.User__c != NULL) {
                    mapSiteToSTM.put(objSSTM.VTD1_SiteID__c, objSSTM.VTR5_Associated_CRA__r);
                }
            }

            for(VTD1_Monitoring_Visit__c objVisit : recs) {
                Study_Team_Member__c objSTM = mapSiteToSTM.get(objVisit.VTD1_Virtual_Site__c); 

                if(objSTM != null && objSTM.User__c != null) {
                    //Update VTR5_VisitCRAUser__c, VTR2_Visit_CRA__c
                    objVisit.VTR5_VisitCRAUser__c = objSTM.User__c;
                    objVisit.VTR2_Visit_CRA__c = objSTM.Id;
                }else{
                    objVisit.addError(System.Label.VTR5_CRANotSSTMErrorInMV); //If User is not setup as a SSTM on Virtual visit.
                }      
            }    
        }else{

            for(VTD1_Monitoring_Visit__c objVisit : recs) {
                objVisit.addError(System.Label.VTR5_CRAProfileErrorInMV);//If logged in User is not of CRA profile.
            }
        }         
    }
}