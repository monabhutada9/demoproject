public without sharing class VT_D1_FeedCommentProcessMentions extends VT_D1_ProcessMentions {

    private void getRecsWithAtSign() {
        for (sObject obj : this.records) {
            FeedComment item = (FeedComment)obj;
            if (item.CommentBody == null) continue;
            if (item.CommentBody.contains('@') || item.CommentBody.contains('{!')) {
                this.recsWithAtSign.add(obj);
            }
        }
    }

    private void updateRecords() {
        Map<Id, VT_D1_Mention> toUpdMap = VT_D1_MentionHelper.getFinalUpdateMap(this.oooMentions);

        if (!toUpdMap.isEmpty()) {
            List<ConnectApi.BatchResult> batchResponse = ConnectApi.ChatterFeeds.getCommentBatch(Network.getNetworkId(), new List<Id>(toUpdMap.keySet()));
            for (Object result : VT_D1_MentionHelper.getBatchResults(batchResponse)) {
                ConnectApi.Comment existingRec = (ConnectApi.Comment)result;

                ConnectApi.CommentInput updatedRec = new ConnectApi.CommentInput();
                updatedRec.body = VT_D1_MentionHelper.getMessageBodyWithAppendedBackups(existingRec.Id, existingRec.body.messageSegments, toUpdMap);
                ConnectApi.ChatterFeeds.updateComment(Network.getNetworkId(), existingRec.Id, updatedRec);
            }
        }
    }
}