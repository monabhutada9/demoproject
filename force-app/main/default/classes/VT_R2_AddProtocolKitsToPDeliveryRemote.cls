/**
 * Created by shume on 02.04.2019.
 */

public without sharing class VT_R2_AddProtocolKitsToPDeliveryRemote {
    public class KitsException extends Exception {
    }

    @AuraEnabled
    public static List<VTD1_Protocol_Kit__c> getProtocolKits (Id deliveryId) {
        if (deliveryId == null) {
            return null;
        }
        VTD1_Protocol_Delivery__c pd = [SELECT Care_Plan_Template__c
                                        FROM VTD1_Protocol_Delivery__c
                                        WHERE Id=:deliveryId];
        if (pd == null || pd.Care_Plan_Template__c==null) {
            return null;
        }
        return [SELECT Id, Name, VTD1_Kit_Type__c, VTD1_Care_Plan_Template__c, VTD1_Protocol_Delivery__c
                    FROM VTD1_Protocol_Kit__c
                    WHERE VTD1_Protocol_Delivery__c=null AND VTD1_Care_Plan_Template__c=:pd.Care_Plan_Template__c];
    }

    @AuraEnabled
    public static void addKitsRemote (List<Id> kitsToAdd, String studyId, String deliveryId) {
        try {
            if (kitsToAdd == null || studyId == null) {
                throw new KitsException('No Kits to Add to Delivery');
            }
            List<VTD1_Protocol_Kit__c> kitsInBase = [SELECT Id, VTD1_Protocol_Delivery__c
                                                        FROM VTD1_Protocol_Kit__c
                                                        WHERE Id in :kitsToAdd AND VTD1_Protocol_Delivery__c=null
                                                        AND VTD1_Care_Plan_Template__c=:studyId
                                                        FOR UPDATE ];
            if (kitsInBase.size() != kitsToAdd.size()) {
                throw new AuraHandledException('Chosen Protocol Kits were changed while you were reviewing them! ' +
                        'Please choose kits to add to Protocol Delivery again.');
            }
            for (VTD1_Protocol_Kit__c kit : kitsInBase) {
                kit.VTD1_Protocol_Delivery__c = deliveryId;
            }
            update kitsInBase;
        } catch (Exception e) {
            String errorMsg = e.getMessage();
            if (errorMsg.contains(Label.VTR2_KitsCombinedError)) {
                errorMsg = Label.VTR2_KitsCombinedError;
            }
            if(!Test.isRunningTest()) throw new AuraHandledException(errorMsg);
        }
    }
}