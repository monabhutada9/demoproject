/**
* @author: Carl Judge
* @date: 10-Sep-19
**/
public without sharing class VT_R2_SurveyAnswerHandler {
    public void onBeforeInsert(List<VTD1_Survey_Answer__c> recs) {
        setQuestionContent(recs);
    }
    public void onBeforeUpdate(List<VTD1_Survey_Answer__c> recs, Map<Id, VTD1_Survey_Answer__c> oldMap) {
        setAnswerFields(recs, oldMap);
        setTimestamp(recs, oldMap);
    }
    public void onAfterInsert(List<VTD1_Survey_Answer__c> recs) {
        createGroupsAndGroupAnswers(recs);
    }
    public void onAfterUpdate(List<VTD1_Survey_Answer__c> recs, Map<Id, VTD1_Survey_Answer__c> oldMap) {
        updateScoresOnGroupAnswers(recs, oldMap);
    }
    /**
     * Sets current GMT+0 date/time in field @see VTR4_AnswerTimestamp__c if answer (@see VTD1_Answer__c field) was changed by Patient or Caregiver
     * Used for mobile offline -> online sync
     *
     * @since 4.0
     * @param recs from @see VT_D1_SurveyAnswerTrigger trigger - Trigger.new
     * @param oldMap from @see VT_D1_SurveyAnswerTrigger trigger - Trigger.oldMap
     */
    private void setTimestamp(List<VTD1_Survey_Answer__c> recs, Map<Id, VTD1_Survey_Answer__c> oldMap) {
        Set<String> validProfilesForTimestamp = new Set<String>{
                VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME, VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME
        };
        String currentUserProfile = [SELECT Profile.Name FROM User WHERE Id = :UserInfo.getUserId()].Profile.Name;
        if (!validProfilesForTimestamp.contains(currentUserProfile)) return;
        for (VTD1_Survey_Answer__c answer : recs) {
            if (needTimestamp(answer, oldMap.get(answer.Id))) {
                answer.VTR4_AnswerTimestamp__c = System.now();
            }
        }
    }
    /**
     * Compare answers and decide if a timestamp is needed.
     *
     * @param newAnswer @see VTD1_Survey_Answer__c new answer from trigger
     * @param oldAnswer @see VTD1_Survey_Answer__c old answer from trigger
     *
     * @return Boolean, true if answers are different (ignoring letters case) else - false.
     */
    private Boolean needTimestamp(VTD1_Survey_Answer__c newAnswer, VTD1_Survey_Answer__c oldAnswer) {
        if (newAnswer.VTD1_Answer__c == null) {
            return !(oldAnswer.VTD1_Answer__c == null);
        } else {
            return oldAnswer.VTD1_Answer__c == null ? true : !(newAnswer.VTD1_Answer__c.equalsIgnoreCase(oldAnswer.VTD1_Answer__c));
        }
    }
    private void setQuestionContent(List<VTD1_Survey_Answer__c> recs) {
        List<Id> protocolIds = new List<Id>();
        for (VTD1_Survey_Answer__c item : recs) {
            protocolIds.add(item.VTD1_Question__c);
        }

        Map<Id, VTD1_Protocol_ePro_Question__c> qContentMap = new Map<Id, VTD1_Protocol_ePro_Question__c>();
        if (!protocolIds.isEmpty()) {
            qContentMap = new Map<Id, VTD1_Protocol_ePro_Question__c>([
                SELECT Id, VTR2_QuestionContentLong__c, VTR4_QuestionContentRichText__c
                FROM VTD1_Protocol_ePro_Question__c
                WHERE Id in :protocolIds
            ]);
        }

        for (VTD1_Survey_Answer__c item : recs) {
            if (qContentMap.containsKey(item.VTD1_Question__c)) {
                item.VTR2_Question_content__c = qContentMap.get(item.VTD1_Question__c).VTR2_QuestionContentLong__c;
                item.VTR4_QuestionContentRichText__c = qContentMap.get(item.VTD1_Question__c).VTR4_QuestionContentRichText__c;
            } else {
                item.VTR2_Question_content__c = item.VTR5_External_Question__c;
                item.VTR4_QuestionContentRichText__c = item.VTR5_External_Question__c;
            }
        }
    }
    private void createGroupsAndGroupAnswers(List<VTD1_Survey_Answer__c> recs) {
        List<Id> questionIds = new List<Id>();
        List<Id> eDiaryIds = new List<Id>();
        for (VTD1_Survey_Answer__c answer : recs) {
            questionIds.add(answer.VTD1_Question__c);
            eDiaryIds.add(answer.VTD1_Survey__c);
        }
        Map<Id, Set<VTR2_Protocol_eDiary_Group__c>> questionToProtocolGroups = new Map<Id, Set<VTR2_Protocol_eDiary_Group__c>>();
        List<Id> protocolGroupIds = new List<Id>();
        for (VTR2_Protocol_eDiary_Group_Question__c groupQuestion : [
                SELECT VTR2_Protocol_eDiary_Group__r.Id, VTR2_Protocol_eDiary_Group__r.Name, VTR2_Protocol_eDiary_Question__c
                FROM VTR2_Protocol_eDiary_Group_Question__c
                WHERE VTR2_Protocol_eDiary_Question__c IN :questionIds
        ]) {
            if (!questionToProtocolGroups.containsKey(groupQuestion.VTR2_Protocol_eDiary_Question__c)) {
                questionToProtocolGroups.put(groupQuestion.VTR2_Protocol_eDiary_Question__c, new Set<VTR2_Protocol_eDiary_Group__c>());
            }
            questionToProtocolGroups.get(groupQuestion.VTR2_Protocol_eDiary_Question__c).add(groupQuestion.VTR2_Protocol_eDiary_Group__r);
            protocolGroupIds.add(groupQuestion.VTR2_Protocol_eDiary_Group__r.Id);
        }
        if (!questionToProtocolGroups.isEmpty()) {
            // get existing groups
            Map<String, VTR2_Patient_eDiary_Group__c> groupMap = new Map<String, VTR2_Patient_eDiary_Group__c>(); // key is eDiary ID + protocol Group ID
            for (VTR2_Patient_eDiary_Group__c existingGroup : [
                    SELECT VTR2_eDiary__c, VTR2_Protocol_eDiary_Group__c
                    FROM VTR2_Patient_eDiary_Group__c
                    WHERE VTR2_eDiary__c IN :eDiaryIds
                    AND VTR2_Protocol_eDiary_Group__c IN :protocolGroupIds
            ]) {
                String compositeKey = '' + existingGroup.VTR2_eDiary__c + existingGroup.VTR2_Protocol_eDiary_Group__c;
                groupMap.put(compositeKey, existingGroup);
            }
            // create new groups if needed
            List<VTR2_Patient_eDiary_Group__c> newGroups = new List<VTR2_Patient_eDiary_Group__c>();
            for (VTD1_Survey_Answer__c answer : recs) {
                if (questionToProtocolGroups.containsKey(answer.VTD1_Question__c)) {
                    for (VTR2_Protocol_eDiary_Group__c protocolGroup : questionToProtocolGroups.get(answer.VTD1_Question__c)) {
                        String compositeKey = '' + answer.VTD1_Survey__c + protocolGroup.Id;
                        if (!groupMap.containsKey(compositeKey)) {
                            VTR2_Patient_eDiary_Group__c newGroup = new VTR2_Patient_eDiary_Group__c(
                                    VTR2_eDiary__c = answer.VTD1_Survey__c,
                                    VTR2_Protocol_eDiary_Group__c = protocolGroup.Id,
                                    Name = protocolGroup.Name
                            );
                            groupMap.put(compositeKey, newGroup);
                            newGroups.add(newGroup);
                        }
                    }
                }
            }
            if (!newGroups.isEmpty()) {
                insert newGroups;
            }
            // create group answers
            List<VTR2_Patient_eDiary_Group_Answer__c> groupAnswers = new List<VTR2_Patient_eDiary_Group_Answer__c>();
            for (VTD1_Survey_Answer__c answer : recs) {
                if (questionToProtocolGroups.containsKey(answer.VTD1_Question__c)) {
                    for (VTR2_Protocol_eDiary_Group__c protocolGroup : questionToProtocolGroups.get(answer.VTD1_Question__c)) {
                        String compositeKey = '' + answer.VTD1_Survey__c + protocolGroup.Id;
                        if (groupMap.containsKey(compositeKey)) {
                            groupAnswers.add(new VTR2_Patient_eDiary_Group_Answer__c(
                                    VTR2_eDiary_Answer__c = answer.Id,
                                    VTR2_Patient_eDiary_Group__c = groupMap.get(compositeKey).Id,
                                    VTR2_Score__c = answer.VTD1_Score__c
                            ));
                        }
                    }
                }
            }
            insert groupAnswers;
        }
    }
    private void setAnswerFields(List<VTD1_Survey_Answer__c> recs, Map<Id, VTD1_Survey_Answer__c> oldMap) {
        for (VTD1_Survey_Answer__c item : recs) {
            if (item.VTR2_English_Answer__c != oldMap.get(item.Id).VTR2_English_Answer__c) {
                item.VTR2_Is_Answered__c = !String.isBlank(item.VTD1_Answer__c);
                item.VTD1_Answer_Number__c = item.VTR2_English_Answer__c != null && item.VTR2_English_Answer__c.isNumeric() ?
                        Integer.valueOf(item.VTR2_English_Answer__c) : null;
            }
        }
    }
    private void updateScoresOnGroupAnswers(List<VTD1_Survey_Answer__c> recs, Map<Id, VTD1_Survey_Answer__c> oldMap) {
        Map<Id, VTD1_Survey_Answer__c> scoreHasChanged = new Map<Id, VTD1_Survey_Answer__c>();
        for (VTD1_Survey_Answer__c answer : recs) {
            if ((oldMap == null && answer.VTD1_Score__c != null) ||
                    (oldMap != null && answer.VTD1_Score__c != oldMap.get(answer.Id).VTD1_Score__c)) {
                scoreHasChanged.put(answer.Id, answer);
            }
        }
        if (!scoreHasChanged.isEmpty()) {
            List<VTR2_Patient_eDiary_Group_Answer__c> groupAnswers = [
                    SELECT Id, VTR2_eDiary_Answer__c
                    FROM VTR2_Patient_eDiary_Group_Answer__c
                    WHERE VTR2_eDiary_Answer__c IN :scoreHasChanged.keySet()
            ];
            for (VTR2_Patient_eDiary_Group_Answer__c groupAnswer : groupAnswers) {
                groupAnswer.VTR2_Score__c = scoreHasChanged.get(groupAnswer.VTR2_eDiary_Answer__c).VTD1_Score__c;
            }
            update groupAnswers;
        }
    }
}