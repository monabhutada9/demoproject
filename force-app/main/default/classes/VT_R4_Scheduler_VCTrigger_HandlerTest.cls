/**

 * Created by Denis Belkovskii on 5/17/2020.

 */
@isTest
public with sharing class VT_R4_Scheduler_VCTrigger_HandlerTest {

    public static void testSchedulerVideoConferenceTrigger(){
        VTR4_ExternalScheduler__c testSetting = new VTR4_ExternalScheduler__c(VTR4_Active__c = true);
        insert testSetting;
        
        List<DomainObjects.User_t> domainTestUsers = new List<DomainObjects.User_t>();
        for(Integer i=0; i<10; i++){
            DomainObjects.User_t userPatient = new DomainObjects.User_t();
            domainTestUsers.add(userPatient);
        }


        List<DomainObjects.VTD1_Actual_Visit_t> domainActualVisits = new List<DomainObjects.VTD1_Actual_Visit_t>();
        for(Integer i=0; i<5; i++){
            DomainObjects.VTD1_Actual_Visit_t domainActualVisit= new DomainObjects.VTD1_Actual_Visit_t();
            domainActualVisit.setVTR5_PatientCaseId(new DomainObjects.Case_t());
            domainActualVisits.add(domainActualVisit);
        }


        List<DomainObjects.Visit_Member_t> domainVisitMembers = new List<DomainObjects.Visit_Member_t>();

            for(Integer i=0; i<10; i++){
            DomainObjects.Visit_Member_t domainVisitMember = new DomainObjects.Visit_Member_t();
            domainVisitMember.addVTD1_Participant_User(domainTestUsers[i]);
            if(i< 5) {
               domainVisitMember.addVTD1_Actual_Visit(domainActualVisits[i]);


            }
            else {
               domainVisitMember.addVTD1_Actual_Visit(domainActualVisits[i-5]);
            }
            domainVisitMembers.add(domainVisitMember);

            }

        List<Video_Conference__c> videoConferences = new List<Video_Conference__c>();
        List<VTD1_Actual_Visit__c> actualVisits = new List<VTD1_Actual_Visit__c>();
        for(Integer i=0; i<5; i++){
            Video_Conference__c videoConference = new Video_Conference__c();
            VTD1_Actual_Visit__c actualVisit = (VTD1_Actual_Visit__c) domainActualVisits[i].persist();
            videoConference.VTD1_Actual_Visit__c = actualVisit.Id;
            actualVisits.add(actualVisit);
            videoConferences.add(videoConference);
        }
        
        for(VTD1_Actual_Visit__c av: actualVisits){
            av.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED;
        }
        update actualVisits;
        insert videoConferences;

            List<VTR4_Scheduler_Video_Conference__c> schedulerVCs = new List<VTR4_Scheduler_Video_Conference__c>();
        for(Integer i=0; i<5; i++){
            VTR4_Scheduler_Video_Conference__c schedulerVC = new VTR4_Scheduler_Video_Conference__c(
                    VTR4_ReadyToStart__c = true,
                    VTR4_Video_Conference__c = videoConferences[i].Id
            );
            if(i<3){
                schedulerVC.VTR4_ReadyToEnd__c = false;
                schedulerVC.VTR4_ReadyToStart__c = true;
                }
                else{
                schedulerVC.VTR4_ReadyToEnd__c = true;

                }
                schedulerVCs.add(schedulerVC);
            }



            Integer numberOfSchVCBeforeTrigger = schedulerVCs.size();
            Test.startTest();
            insert schedulerVCs;
            Test.stopTest();
            Integer numberOfSchVCAfterTrigger = [SELECT COUNT() FROM VTR4_Scheduler_Video_Conference__c];
        System.assertEquals(numberOfSchVCBeforeTrigger, 5);
        System.assertEquals(5, numberOfSchVCAfterTrigger);
    }
}