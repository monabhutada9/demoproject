public with sharing class VT_D2_PIPatientPCFCommentsController {
    public class PCFComment{
        public VTD1_PCF_Patient_Contact_Form_Comment__c parentPCFComment;
        public List<VTD1_PCF_Patient_Contact_Form_Comment__c> childPCFComments;
        public String reply;
        public Boolean showReply;
    }

    public class PCFCommentWrapper implements Comparable {

        public VTD1_PCF_Patient_Contact_Form_Comment__c pcfComment;

        public PCFCommentWrapper(VTD1_PCF_Patient_Contact_Form_Comment__c pcfCom) {
            pcfComment = pcfCom;
        }

        public Integer compareTo(Object compareTo) {
            PCFCommentWrapper compareToPCFComment = (PCFCommentWrapper)compareTo;

            if (pcfComment.VTD2_Parent_PCF_Comment__c == null){
                return pcfComment.CreatedDate < compareToPCFComment.pcfComment.CreatedDate ? 1 : -1;
            } else {
                return pcfComment.CreatedDate < compareToPCFComment.pcfComment.CreatedDate ? -1 : 1;
            }
        }
    }

    @AuraEnabled
    public static String getPcfComments(Id pcfId) {
        try {
            List<VTD1_PCF_Patient_Contact_Form_Comment__c> comments = [
                    SELECT Id, Name, Comment__c, CreatedBy.Name, CreatedBy.SmallPhotoUrl, CreatedDate, VTD2_Parent_PCF_Comment__c
                    FROM VTD1_PCF_Patient_Contact_Form_Comment__c
                    WHERE Patient_Contact_Form__r.RecordTypeId IN :VT_D1_HelperClass.getRecordTypePCF()
                    AND Patient_Contact_Form__c = :pcfId
                    ORDER BY VTD2_Parent_PCF_Comment__c DESC
            ];

            Map<String, List<VTD1_PCF_Patient_Contact_Form_Comment__c>> parentPCFCommentIdToChildPCFListMap = new Map<String, List<VTD1_PCF_Patient_Contact_Form_Comment__c>>();
            List<VTD1_PCF_Patient_Contact_Form_Comment__c> parentPCFCommentList = new List<VTD1_PCF_Patient_Contact_Form_Comment__c>();
            for(VTD1_PCF_Patient_Contact_Form_Comment__c comment: comments){
                if (comment.VTD2_Parent_PCF_Comment__c == null){
                    parentPCFCommentIdToChildPCFListMap.put(comment.Id, null);
                    parentPCFCommentList.add(comment);
                } else {
                    List<VTD1_PCF_Patient_Contact_Form_Comment__c> childPCFCommentList;
                    if(parentPCFCommentIdToChildPCFListMap.get(comment.VTD2_Parent_PCF_Comment__c) == null) {
                        childPCFCommentList = new List<VTD1_PCF_Patient_Contact_Form_Comment__c>();
                    } else{
                        childPCFCommentList = parentPCFCommentIdToChildPCFListMap.get(comment.VTD2_Parent_PCF_Comment__c);
                    }
                    childPCFCommentList.add(comment);
                    parentPCFCommentIdToChildPCFListMap.put(comment.VTD2_Parent_PCF_Comment__c, childPCFCommentList);
                }
            }

            List<PCFCommentWrapper> pcfCommentWrapperList = new List<PCFCommentWrapper>();
            for (VTD1_PCF_Patient_Contact_Form_Comment__c pcf: parentPCFCommentList){
                PCFCommentWrapper pcfCommentWrapper = new PCFCommentWrapper(pcf);
                pcfCommentWrapperList.add(pcfCommentWrapper);
            }
            pcfCommentWrapperList.sort();

            List<PCFComment> pcfCommentList = new List<PCFComment>();
            for (PCFCommentWrapper pcfCommentWrapper : pcfCommentWrapperList){
                String parentPCFCommentId = pcfCommentWrapper.pcfComment.Id;
                PCFComment pcfComment = new PCFComment();
                pcfComment.parentPCFComment = pcfCommentWrapper.pcfComment;

                List<VTD1_PCF_Patient_Contact_Form_Comment__c> childPCFComments = parentPCFCommentIdToChildPCFListMap.get(parentPCFCommentId);
                if (childPCFComments == null){
                    pcfComment.childPCFComments = null;
                } else {
                    List<PCFCommentWrapper> pcfCommentWrapperChildList = new List<PCFCommentWrapper>();
                    for (VTD1_PCF_Patient_Contact_Form_Comment__c pcf: childPCFComments){
                        PCFCommentWrapper pcfCommentWrapperChild = new PCFCommentWrapper(pcf);
                        pcfCommentWrapperChildList.add(pcfCommentWrapperChild);
                    }
                    pcfCommentWrapperChildList.sort();
                    List<VTD1_PCF_Patient_Contact_Form_Comment__c> childPCFCommentsSorted = new List<VTD1_PCF_Patient_Contact_Form_Comment__c>();
                    for (PCFCommentWrapper pcfCommentWrapperChild : pcfCommentWrapperChildList){
                        childPCFCommentsSorted.add(pcfCommentWrapperChild.pcfComment);
                    }
                    pcfComment.childPCFComments = childPCFCommentsSorted;
                }
                pcfComment.showReply = false;
                pcfCommentList.add(pcfComment);
            }
            return JSON.serialize(pcfCommentList);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static void addPCFComment(String pcfId, String newComment, String parentPCFId) {
        try {
            VTD1_PCF_Patient_Contact_Form_Comment__c pcfComment = new VTD1_PCF_Patient_Contact_Form_Comment__c();
            pcfComment.Comment__c = newComment;
            pcfComment.Patient_Contact_Form__c = pcfId;
            pcfComment.VTD2_Parent_PCF_Comment__c = parentPCFId;
            insert pcfComment;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
}