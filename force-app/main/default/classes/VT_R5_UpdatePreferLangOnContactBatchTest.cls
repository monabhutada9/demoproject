/*
 * Author: Sunil BP.
 * Date: 19 Aug 2020
 * Jira Issue: SH-14661
 * Desription: Test class for VTR5_UpdatePreferredLangOnContactBatch
*/

@IsTest
public class VT_R5_UpdatePreferLangOnContactBatchTest {
    @testSetup
    private static void setupMethod() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }
    @isTest
    public static void testLanguage(){
        List<Contact> lstcon = [SELECT Id, AccountId, RecordType.Name, VTR2_Primary_Language__c, VTD1_Clinical_Study_Membership__c, VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__c, VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c, VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_DefaultIRBLanguage__c FROM Contact WHERE RecordType.Name = 'Patient'];	
        system.debug('lstcon.size'+lstcon.size());
       	DomainObjects.Contact_t contactCaregiverT = new DomainObjects.Contact_t()
                .setVTD1_RelationshiptoPatient('Parent')
                .setEmail('test@email.com')
                .setFirstName('Tom')
                .setLastName('Jary')
                .setPhone('654654654')
                .setAccountId(lstcon[0].AccountId)
            	.setVTR2_Primary_Language('en_US')
                .setRecordTypeName('Caregiver');
        Contact conCaregiver = (Contact) contactCaregiverT.persist();
        conCaregiver.VTD1_Clinical_Study_Membership__c = lstcon[0].VTD1_Clinical_Study_Membership__c;
        update conCaregiver;
        Virtual_Site__c site = new Virtual_Site__c();
        site.Id = lstcon[0].VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__c;
        site.VTR5_IRBApprovedLanguages__c = 'zh_TW';
        site.VTR5_DefaultIRBLanguage__c = 'zh_TW';
        update site;
        test.startTest();
        Database.executeBatch(new VT_R5_UpdatePreferredLangOnContactBatch());
        test.stopTest();
        for(Contact con: [SELECT Id, VTR2_Primary_Language__c FROM Contact WHERE RecordType.Name = 'Patient' OR RecordType.Name = 'Caregiver']){
            system.assertEquals('zh_TW', con.VTR2_Primary_Language__c, 'Preferd language not updated correctly');
        }
    }
}