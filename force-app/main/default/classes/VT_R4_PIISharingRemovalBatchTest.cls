/**
* @author: Olga Baranova
* @date: 10-feb-2020
**/

@isTest
public with sharing class VT_R4_PIISharingRemovalBatchTest {

    @testSetup
    static void setup() {
        List<User> users = new List<User>();
        users.add(new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'Project Lead'].Id,
                FirstName = 'UserPL',
                LastName = 'L',
                Email = VT_D1_TestUtils.generateUniqueUserName(),
                Username = VT_D1_TestUtils.generateUniqueUserName(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                IsActive = true
        ));
        users.add(new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'Monitoring Report Reviewer'].Id,
                FirstName = 'UserMRR',
                LastName = 'L',
                Email = VT_D1_TestUtils.generateUniqueUserName(),
                Username = VT_D1_TestUtils.generateUniqueUserName(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                IsActive = true
        ));
        users.add(new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'Virtual Trial Study Lead'].Id,
                FirstName = 'UserVTSL',
                LastName = 'L',
                Email = VT_D1_TestUtils.generateUniqueUserName(),
                Username = VT_D1_TestUtils.generateUniqueUserName(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                IsActive = true
        ));
        users.add(new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'Project Management Analyst'].Id,
                FirstName = 'UserPMA',
                LastName = 'L',
                Email = VT_D1_TestUtils.generateUniqueUserName(),
                Username = VT_D1_TestUtils.generateUniqueUserName(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                IsActive = true
        ));
        users.add(new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'Regulatory Specialist'].Id,
                FirstName = 'UserRS',
                LastName = 'L',
                Email = VT_D1_TestUtils.generateUniqueUserName(),
                Username = VT_D1_TestUtils.generateUniqueUserName(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                IsActive = true
        ));
        insert users;

    }

    @isTest
    private static void doTest() {
        Case pcf = new Case(RecordTypeId = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF);
        insert pcf;
        List<String> profiles = new List<String>{
                'Monitoring Report Reviewer',
                'Project Lead',
                'Virtual Trial Study Lead',
                'Regulatory Specialist',
                'Project Management Analyst'
        };
        List<CaseShare> caseShares = new List<CaseShare>();
        for (User usrIds : [SELECT Id FROM User WHERE Profile.Name IN :profiles AND (
                FirstName = 'UserPL' OR
                FirstName = 'UserMRR' OR
                FirstName = 'UserVTSL' OR
                FirstName = 'UserPMA' OR
                FirstName = 'UserRS')]) 
        {
            caseShares.add(new CaseShare(
                    CaseId = pcf.Id,
                    CaseAccessLevel = 'Edit',
                    UserOrGroupId = usrIds.id));
        }
        System.debug('caseShares.size: ' + caseShares.size() + ' caseShares: ' + caseShares);
        insert caseShares;
        Test.startTest();
        Database.executeBatch(new VT_R4_PIISharingRemovalBatch());
        Test.stopTest();
    }
}