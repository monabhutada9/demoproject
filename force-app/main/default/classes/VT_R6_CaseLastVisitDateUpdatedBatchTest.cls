@IsTest
public class VT_R6_CaseLastVisitDateUpdatedBatchTest {

    @IsTest
    static void testCaseLastVisitDateUpdatedBatch() {
        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor');
        Account account = (Account) patientAccount.persist();

        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(patientAccount)
                .setVTD1_SC_Tasks_Queue_Id(UserInfo.getUserId());

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
                .setFirstName('Patient')
                .setLastName('Patient')
                .setAccountId(account.Id);

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        patientUser.persist();

        Case patientCase = (Case) new DomainObjects.Case_t()
                .addPIUser(new DomainObjects.User_t())
                .addVTD1_Patient_User(patientUser)
                .addContact(patientContact)
                .setRecordTypeByName('CarePlan')
                .setAccountId(account.Id)
                .addStudy(study)
                .persist();

        Case caseRecord = [SELECT VTD1_Study__c,VTR5_LastVisitCompleteDate__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        System.assertEquals(null, caseRecord.VTR5_LastVisitCompleteDate__c);

        VTD1_Actual_Visit__c aVisit1 = new VTD1_Actual_Visit__c(
                VTD1_Case__c = caseRecord.Id,
                VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT,
                VTR2_Televisit__c = false
        );
        insert  aVisit1;

        aVisit1.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED;
        update aVisit1;

        caseRecord = [SELECT VTD1_Study__c,VTR5_LastVisitCompleteDate__c FROM Case WHERE Id =: caseRecord.Id];
        System.assertNotEquals(null, caseRecord.VTR5_LastVisitCompleteDate__c);

        caseRecord.VTR5_LastVisitCompleteDate__c = null;
        update caseRecord;

        caseRecord = [SELECT VTD1_Study__c,VTR5_LastVisitCompleteDate__c FROM Case WHERE Id =: caseRecord.Id];
        System.assertEquals(null, caseRecord.VTR5_LastVisitCompleteDate__c);

        Test.startTest();
            Database.executeBatch(new VT_R6_CaseLastVisitDateUpdatedBatch());
        Test.stopTest();

        caseRecord = [SELECT VTD1_Study__c,VTR5_LastVisitCompleteDate__c FROM Case WHERE Id =: caseRecord.Id];
        System.assertNotEquals(null, caseRecord.VTR5_LastVisitCompleteDate__c);
    }

}