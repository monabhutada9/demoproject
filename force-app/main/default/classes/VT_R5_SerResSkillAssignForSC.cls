public class VT_R5_SerResSkillAssignForSC {
    /* method : afterUpdateSkillAssignment(List<user> userList,Map<Id,User> oldMap)
* params : userList,oldMap- Both param come from VT_D1_UserTrigger in after update scenario
* Description :this method prepare set of user id if any language,additional prefered alngugae,profile and isactive get updated.
*/ 
    public static Boolean blockSrCreation=false;
    public static void afterUpdateSerResSkillAssignment(List<User> UserList ,Map<Id,User> oldMap){
        List<String> profileList =Label.VTR5_ProfileName.split(',');
        Map<Id,Set<String>> userIdToOldLangMap = new Map<Id,Set<String>>();
        Map<Id,Set<String>> userIdToNewLangMap = new Map<Id,Set<String>>();
        Map<Id,String> userIdToNameMap = new Map<Id,String>();
        for(user us :UserList){  
            
            if(!blockSrCreation && us.isActive && profileList.contains(us.VTD1_Profile_Name__c)){
                Set<String> tempNewLang = new Set<String>();
                Set<String> tempOldLang = new Set<String>();
                if(oldMap.get(us.Id).VT_R5_Primary_Preferred_Language__c != us.VT_R5_Primary_Preferred_Language__c){
                    
                    tempNewLang.add(us.VT_R5_Primary_Preferred_Language__c);
                    tempOldLang.add(oldMap.get(us.Id).VT_R5_Primary_Preferred_Language__c);
                } 
                if(oldMap.get(us.Id).VT_R5_Secondary_Preferred_Language__c != us.VT_R5_Secondary_Preferred_Language__c){
                    tempNewLang.add(us.VT_R5_Secondary_Preferred_Language__c);
                    tempOldLang.add(oldMap.get(us.Id).VT_R5_Secondary_Preferred_Language__c);
                }
                if(oldMap.get(us.Id).VT_R5_Tertiary_Preferred_Language__c != us.VT_R5_Tertiary_Preferred_Language__c){
                    tempNewLang.add(us.VT_R5_Tertiary_Preferred_Language__c);
                    tempOldLang.add(oldMap.get(us.Id).VT_R5_Tertiary_Preferred_Language__c);
                    
                }
                if((oldMap.get(us.Id).profileId  != us.profileId) ||(OldMap.get(us.Id).isActive != us.isActive) ){
                    tempNewLang.add(us.VT_R5_Primary_Preferred_Language__c);
                    tempNewLang.add(us.VT_R5_Secondary_Preferred_Language__c);
                    tempNewLang.add(us.VT_R5_Tertiary_Preferred_Language__c);   
                }
                
                if(tempOldLang != null && tempOldLang.size()>0){
                    userIdToOldLangMap.put(us.Id,tempOldLang);
                    userIdToNameMap.put(us.id,us.VTD1_Full_Name__c);
                }
                if(tempNewLang != null && tempNewLang.size()>0){
                    userIdToNewLangMap.put(us.Id,tempNewLang);
                    userIdToNameMap.put(us.id,us.VTD1_Full_Name__c);
                }
            }
        }
        String userIdToNewLangMapJSONStr = Json.serialize(userIdToNewLangMap);        
        String userIdToOldLangMapJSONStr = Json.serialize(userIdToOldLangMap);
        
        if(userIdToNameMap != null && userIdToNameMap.size()>0){
            createUpdateServResSkillAssignment(userIdToNewLangMapJSONStr,userIdToOldLangMapJSONStr,userIdToNameMap,false);
        }
        
    }    
    /* method :createServiceResourse(Set<Id> userId)
* params :userId-after insert scenario on VT_D1_UserTrigger )
* Description :prepare list of user id for SC user.
*/    
    public static void createServiceResourse(List<User> userList){
        Map<Id,Set<String>> userIdToNewLangMap = new Map<Id,Set<String>>();
        Map<Id,String> userIdToNameMap = new Map<Id,String>();
        List<String> profileList =Label.VTR5_ProfileName.split(',');
        for(User us :userList){
            
            if(!blockSrCreation && us.isActive && profileList.contains(us.VTD1_Profile_Name__c)){
                Set<String> languageOfUeser = new Set<String>();
                if(us.VT_R5_Primary_Preferred_Language__c != null && us.VT_R5_Primary_Preferred_Language__c!=''){
                    languageOfUeser.add(us.VT_R5_Primary_Preferred_Language__c);   
                }
                if(us.VT_R5_Secondary_Preferred_Language__c != null && us.VT_R5_Secondary_Preferred_Language__c!=''){
                    languageOfUeser.add(us.VT_R5_Secondary_Preferred_Language__c);
                }
                if(us.VT_R5_Tertiary_Preferred_Language__c != null && us.VT_R5_Tertiary_Preferred_Language__c!=''){
                    languageOfUeser.add(us.VT_R5_Tertiary_Preferred_Language__c);
                }
                userIdToNewLangMap.put(us.Id,languageOfUeser);
                userIdToNameMap.put(us.id,us.VTD1_Full_Name__c);
                
            }
        }
        
        String userIdToNewLangMapJSONStr = Json.serialize(userIdToNewLangMap);
        if(userIdToNewLangMap != null && userIdToNewLangMap.size()>0){
            createUpdateServResSkillAssignment(userIdToNewLangMapJSONStr,null,userIdToNameMap,true);
        }
    }
    /* method :CreateUpdateServResSkillAssignment(Set<Id> userId,Set<String> toDeleteUserSkill,set<Id> existingServResorForUserId)
* params :All param come from afterUpdateSkillAssignment method or createServiceResourse(after insert scenario)
* Description :onchang of language,additional prefered alngugae,profile and isActive  field it will 
* create/update service resourse and assign all skill( langugae) of a user to service resourse.
*/     
    @future
    public static void createUpdateServResSkillAssignment(String userIdToNewLangMapJSON,String userIdToOldLangMapJSON,Map<Id,String> userIdToNameMap,Boolean isInsert){
        Map<String,Set<String>> userIdToOldLangMap = new Map<String,Set<String>>();
        Map<String,Set<String>> userIdToNewLangMap = new Map<String,Set<String>>();
        Set<String>  allLanguageForSkillMatch = new Set<String>();
        Map<Id,ServiceResource> userIdToExistingServiceResourseMap = new Map<Id,ServiceResource>();
        Map<String,Id> skillDevNameToServResSkillIdMap = new Map<String,Id>();
        if(String.isNotBlank(userIdToNewLangMapJSON)){
            userIdToNewLangMap = (Map<String,Set<String>>) JSON.deserialize(userIdToNewLangMapJSON, Map<String,Set<String>>.class); }
        if(String.isNotBlank(userIdToOldLangMapJSON)){
            userIdToOldLangMap = (Map<String,Set<String>>) JSON.deserialize(userIdToOldLangMapJSON, Map<String,Set<String>>.class);}
        
        List<ServiceResource> serviceResourseToCreate = new List<ServiceResource>();
        if(userIdToNewLangMap != null && userIdToNewLangMap.size()>0){
            for(Id usrId :userIdToNewLangMap.keySet()){
                allLanguageForSkillMatch.addAll(userIdToNewLangMap.get(usrId));
            }
        }
        
        if(!isInsert){
            for(ServiceResource serv : [Select id,RelatedRecordId,(Select id,Skill.DeveloperName from ServiceResourceSkills) from ServiceResource where RelatedRecordId IN: userIdToNameMap.keySet()]){
                userIdToExistingServiceResourseMap.put(serv.RelatedRecordId,serv); 
            }		   
        }
        for(Id usId :userIdToNameMap.keySet()){
            if(isInsert ||(!isInsert &&   !userIdToExistingServiceResourseMap.containsKey(usId))){
                ServiceResource servResor = new ServiceResource();
                servResor.Name='SR-SC'+'_'+userIdToNameMap.get(usId);
                servResor.RelatedRecordId=usId;
                servResor.IsActive=true;
                servResor.ResourceType='A'; 
                servResor.Description='Service Resourse for'+' '+userIdToNameMap.get(usId);
                serviceResourseToCreate.add(servResor);
            }  
        }
        
        if(serviceResourseToCreate != Null && serviceResourseToCreate.size()>0){    
            insert serviceResourseToCreate;
        }
        
        if(!isInsert  &&  userIdToExistingServiceResourseMap != null && userIdToExistingServiceResourseMap.size()>0) {
            serviceResourseToCreate.addAll(userIdToExistingServiceResourseMap.values());      
        }
        
        
        List<ServiceResourceSkill> ServiceResourceSkillToDelete = new List<ServiceResourceSkill>();
        List<ServiceResourceSkill> skillAssignToServResourseList = new List<ServiceResourceSkill>(); 
        for(skill skl: [Select id,DeveloperName from skill where DeveloperName IN :allLanguageForSkillMatch] ){
            skillDevNameToServResSkillIdMap.put(skl.DeveloperName,skl.Id);
        }
        
        for(ServiceResource src :serviceResourseToCreate){
            Set<Id> existingSkillId = new Set<Id>();
            for(ServiceResourceSkill srs:src.ServiceResourceSkills){
                
                if(!isInsert && userIdToOldLangMap != null && userIdToOldLangMap.size()>0 && userIdToOldLangMap.get(src.RelatedRecordId).contains(srs.Skill.DeveloperName)){
                    ServiceResourceSkillToDelete.add(srs);
                }
                else{
                    existingSkillId.add(srs.Skill.Id);
                }
                
            }
            
            for(String skillDevName : userIdToNewLangMap.get(src.RelatedRecordId)){
                if(userIdToNewLangMap != null && userIdToNewLangMap.size()>0 && !existingSkillId.contains(skillDevNameToServResSkillIdMap.get(skillDevName)) && skillDevNameToServResSkillIdMap.containsKey(skillDevName) ) { 
                    ServiceResourceSkill srk =  new ServiceResourceSkill();
                    srk.ServiceResourceId=src.Id;
                    srk.SkillId=skillDevNameToServResSkillIdMap.get(skillDevName);
                    srk.EffectiveStartDate=date.today();
                    skillAssignToServResourseList.add(srk);
                }
            }	           
        }           
        
        if(ServiceResourceSkillToDelete != null && ServiceResourceSkillToDelete.size()>0){
            delete ServiceResourceSkillToDelete;
        } 
        
        if(skillAssignToServResourseList != null && skillAssignToServResourseList.size()>0){
            insert skillAssignToServResourseList;
        } 
    }
    
    
}