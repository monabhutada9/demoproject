public interface VT_R5_SearchFilter {
    /**
     * Must return list of Custom Object fields.Used for building query
     *
     * @return  list of fields
     */
    List<String> getFields();

    /**
     * Must return type of Custom Object
     *
     * @return - SObject type
     */
    SObjectType getSObjectType();

    /**
     *  This method must generate filter string for search query
     *
     * @return - filter string for query
     */
    String getFilterString();
}