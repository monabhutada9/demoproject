public without sharing class VT_D1_ePROSchedulingTaskHelper {

    public class ePROSchedulingException extends Exception{}

    public class TaskTemplate {
        @InvocableVariable(Label = 'Survey ID' Required = true)
        public Id surveyId;

        @InvocableVariable(Label = 'Subject' Required = true)
        public String subject;

        @InvocableVariable(Label = 'Category' Required = true)
        public String category;

        @InvocableVariable(Label = 'Due Date' Required = true)
        public Date dueDate;

        @InvocableVariable(Label = 'My Task List Redirect' Required = true)
        public String redirect;

        @InvocableVariable(Label = 'Unique Code' Required = true)
        public String uniqueCode;

        @InvocableVariable(Label = 'RecordType API Name' Required = true)
        public String recordTypeDeveloperName;

        @InvocableVariable(Label = 'Subject Parameters' Required = true)
        public String subjectParameters;
    }

    private List<TaskTemplate> templates;
    private Map<Id, Set<Id>> receiversBySurvey = new Map<Id, Set<Id>>();
    private Map<Id, VTD1_Survey__c> surveysById = new Map<Id, VTD1_Survey__c>();

    @InvocableMethod
    public static void sendPatientCaregiverTasks(List<TaskTemplate> templates) {
        new VT_D1_ePROSchedulingTaskHelper(templates).execute();
    }

    public VT_D1_ePROSchedulingTaskHelper(List<TaskTemplate> templates) {
        this.templates = templates;
    }

    private void execute() {
        querySurveys();
        Map<String, Schema.RecordTypeInfo> recTypInfoMap = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName();
        List<Task> tasks = new List<Task>();

        for (TaskTemplate template : this.templates) {
            Id recTypeId;
            if (recTypInfoMap.containsKey(template.recordTypeDeveloperName)) {
                recTypeId = recTypInfoMap.get(template.recordTypeDeveloperName).getRecordTypeId();
            } else {
                throw new ePROSchedulingException('Invalid Task RecordType API Name: ' + template.recordTypeDeveloperName);
            }

            if (this.receiversBySurvey.containsKey(template.surveyId)) {
                Set<Id> receivers = this.receiversBySurvey.get(template.surveyId);
                for (Id uId : receivers) {
                    VTD1_Survey__c survey = this.surveysById.get(template.surveyId);
                    tasks.add(new Task(
                        ActivityDate = template.dueDate,
                        OwnerId = uId,
                        Subject = template.subject,
                        WhatId = template.surveyId,
                        VTD2_My_Task_List_Redirect__c = template.redirect,
                        VTD2_Task_Unique_Code__c = template.uniqueCode,
                        Category__c = template.category,
                        HealthCloudGA__CarePlanTemplate__c = survey.VTD1_CSM__r.VTD1_Study__c,
                        RecordTypeId = recTypeId,
                        VTD2_Subject_Parameters__c = template.subjectParameters,
                        VTD1_Caregiver_Clickable__c = survey.VTD1_Caregiver_User_Id__c != null,
                        VTD1_Case_lookup__c = survey.VTD1_CSM__c
                    ));
                }
            }
        }

        if (! tasks.isEmpty()) { insert tasks; }
    }

    public void querySurveys() {
        List<Id> surveyIds = new List<Id>();
        for (TaskTemplate item : this.templates) {
            surveyIds.add(item.surveyId);
        }

        for (VTD1_Survey__c item : [
            SELECT Id, VTD1_Patient_User_Id__c,
                VTD1_Caregiver_User_Id__c,
                VTD1_Protocol_ePRO__r.VTD1_Subject__c,
                VTD1_Protocol_ePRO__r.VTD1_Caregiver_on_behalf_of_Patient__c,
                VTD1_CSM__r.VTD1_ePro_can_be_completed_by_Caregiver__c,
                VTD1_CSM__r.VTD1_Study__c,
                VTD1_CSM__c
            FROM VTD1_Survey__c
            WHERE Id IN :surveyIds
        ]) {
            Set<Id> receivers = new Set<Id>();
            if (toAddPatient(item)) {
                receivers.add(item.VTD1_Patient_User_Id__c);
            } else if (toAddCaregiver(item)) {
                receivers.add(item.VTD1_Caregiver_User_Id__c);
            }
            this.receiversBySurvey.put(item.Id, receivers);
            this.surveysById.put(item.Id, item);
        }
    }

    private Boolean toAddPatient(VTD1_Survey__c survey) {
        return
            survey.VTD1_Patient_User_Id__c != null &&
            survey.VTD1_Protocol_ePRO__r.VTD1_Subject__c != 'Caregiver'
        ;
    }

    private Boolean toAddCaregiver(VTD1_Survey__c survey) {
        return
            survey.VTD1_Caregiver_User_Id__c != null && (
                survey.VTD1_Protocol_ePRO__r.VTD1_Subject__c == 'Caregiver' || (
                    survey.VTD1_Protocol_ePRO__r.VTD1_Caregiver_on_behalf_of_Patient__c ||
                    survey.VTD1_CSM__r.VTD1_ePro_can_be_completed_by_Caregiver__c
                )
            )
        ;
    }
}