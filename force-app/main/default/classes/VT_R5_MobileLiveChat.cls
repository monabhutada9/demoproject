/**
 * @author: Alexander Komarov
 * @date: 07.05.2020
 * @description: service for mobile app, returns live chat configuration for particular user
 *
 * ACTIVE VERSION => v2
 *
 */
public without sharing class VT_R5_MobileLiveChat extends VT_R5_MobileRestResponse implements VT_R5_MobileRestRouter.Routable {

    private static String userLanguageKey = 'en_US';

    public List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/live-chat-settings/'
        };
    }

    public void versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        get_v1();
                        return;
                    }
                    when 'v2' {
                        get_v2();
                        return;
                    }
                }
            }
        }
        VT_R5_MobileRestRouter.unsupportedVersionResponse();
        this.addError('unsupported version');
        this.sendResponse();
    }
    public void initService() {
    }
    private void get_v1() {
        try {
            this.buildResponseWOAI(new LiveChatSettings());
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private void get_v2() {
        try {
            this.buildResponseWOAI(new LiveChatSettings(UserInfo.getUserId()));
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private class LiveChatSettings {
        private String deploymentId;
        private List<String> buttonIds;
        private String orgId;
        private String chatApiEndpoint;
        private String visitorName;
        private String visitorEmail;
        private List<ChatButton> chatButtons;

        LiveChatSettings() {
            buttonIds = new List<String>();
            setVisitorInfo(UserInfo.getUserId());
            setLiveChatValues();
            setLiveChatButtons(VT_D1_SkillsBasedRouting.getChatbuttonIdMap(), null);
        }

        LiveChatSettings(Id userId) {
            chatButtons = new List<ChatButton>();
            setVisitorInfo(userId);
            setLiveChatValues();
            Map<String, Id> skillBasedButtons = (Map<String, Id>)new VT_Stubber.ResultStub('skillBasedButtons',
                    VT_D1_SkillsBasedRouting.getChatbuttonIdMap()
            ).getResult();
            setLiveChatButtons(skillBasedButtons, getLanguageKeyByButtonIds(skillBasedButtons.values()));
        }

        private void setLiveChatValues() {
            this.orgId = VT_D1_SkillsBasedRouting.getOrganizationId();
            this.deploymentId = VT_D1_SkillsBasedRouting.getLiveChatDeploymentId();
            this.chatApiEndpoint = VT_D1_SkillsBasedRouting.getLiveChatUrl();
        }

        private void setVisitorInfo(Id userId) {
            User visitor = [SELECT Name, Email, LanguageLocaleKey, Contact.VTR2_Primary_Language__c FROM User WHERE Id = :userId LIMIT 1];
            userLanguageKey = visitor.Contact.VTR2_Primary_Language__c;
            this.visitorEmail = visitor.Email;
            this.visitorName = visitor.Name;
        }

        private void setLiveChatButtons(Map<String, Id> skillBasedButtons, Map<String, String> languageByButtonIds) {
            for (String order : new List<String>{'primary', 'secondary', 'tertiary'}) {
                if (skillBasedButtons.containsKey(order)) {
                    String longId = skillBasedButtons.get(order);
                    if (languageByButtonIds == null) {
                        this.buttonIds.add(longId.substring(0, 15));
                    } else {
                        this.chatButtons.add(new ChatButton(longId, languageByButtonIds.get(longId)));
                    }
                }
            }
        }

        private Map<String, String> getLanguageKeyByButtonIds(List<Id> buttonIds) {
            List<LiveChatButton> liveChatButtons = (List<LiveChatButton>) new VT_Stubber.ResultStub('liveChatButtons' ,
            [
                    SELECT Id, DeveloperName
                    FROM LiveChatButton
                    WHERE Id IN :buttonIds
            ]).getResult();
            Map<String, String> languageKeyByButtonIds = new Map<String, String>();
            for (LiveChatButton chatButton : liveChatButtons) {
                String language = chatButton.DeveloperName.substring(
                        chatButton.DeveloperName.indexOf('_') + 1, chatButton.DeveloperName.length()
                );
                languageKeyByButtonIds.put(chatButton.Id, language);
            }
            return languageKeyByButtonIds;
        }
    }

    private class ChatButton {
        private String buttonId;
        private String languageKey;
        private String language;

        ChatButton(String longId, String languageKey) {
            this.buttonId = longId.substring(0, 15);
            this.languageKey = languageKey;
            this.language = VT_D1_TranslateHelper.getLabelValue('VTR5_' + languageKey);
        }
    }
}