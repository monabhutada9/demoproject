/**
 * @description Common use Utilities
 * @author Ruslan Mullayanov
 */

public with sharing class VT_Utilities {
    private static Map<String, SObjectType> schemaGlobalDescribe = Schema.getGlobalDescribe();

    public static Boolean isAccessibleAndUpdatable(SObjectField field) {
        return VT_Utilities.isAccessible(field) && VT_Utilities.isUpdateable(field);
    }

    public static Boolean isAccessible(String sObjectTypeName, String fieldName){
        return VT_Utilities.isAccessible(getSObjectFieldByName(getSObjectTypeByName(sObjectTypeName), fieldName));
    }
    public static Boolean isAccessible(String sObjectTypeName){
        return VT_Utilities.isAccessible(getSObjectTypeByName(sObjectTypeName));
    }
    public static Boolean isAccessible(SObjectType sObjectType) {
        return sObjectType.getDescribe().isAccessible();
    }
    public static Boolean isAccessible(SObjectField field) {
        return field.getDescribe().isAccessible();
    }
    
    public static Boolean isCreateable(String sObjectTypeName){
        return VT_Utilities.isCreateable(getSObjectTypeByName(sObjectTypeName));
    }
    public static Boolean isCreateable(SObjectType sObjectType) {
        return sObjectType.getDescribe().isAccessible();
    }
    public static Boolean isCreateable(SObjectField field) {
        return field.getDescribe().isCreateable();
    }
    
    public static Boolean isUpdateable(String sObjectTypeName){
        return VT_Utilities.isUpdateable(getSObjectTypeByName(sObjectTypeName));
    }
    public static Boolean isUpdateable(SObjectType sObjectType) {
        return sObjectType.getDescribe().isAccessible();
    }
    public static Boolean isUpdateable(SObjectField field) {
        return field.getDescribe().isUpdateable();
    }
    
    public static Boolean isDeletable(String sObjectTypeName){
        return VT_Utilities.isDeletable(getSObjectTypeByName(sObjectTypeName));
    }
    public static Boolean isDeletable(SObjectType sObjectType) {
        return sObjectType.getDescribe().isAccessible();
    }
    
    private static SObjectType getSObjectTypeByName(String sObjectTypeName) {
        return schemaGlobalDescribe.get(sObjectTypeName);
    }

    private static SObjectField getSObjectFieldByName(SObjectType sObjectType, String fieldName) {
        return sObjectType.getDescribe().fields.getMap().get(fieldName);
    }
}