/**
 * Created by MPlatonov on 31.07.2020.
 */

public without sharing class VT_R5_VisitScheduler {
    class ConferenceQueueable implements Queueable {
        final List<Video_Conference__c> conferences;
        final Map <Id, List <VTD1_Conference_Member__c>> conferenceMembersMap;
        ConferenceQueueable(List<Video_Conference__c> conferences, Map <Id, List <VTD1_Conference_Member__c>> conferenceMembersMap) {
            this.conferences = conferences;
            this.conferenceMembersMap = conferenceMembersMap;
        }
        public void execute(QueueableContext queueableContext) {
            Database.upsert(conferences, false);
            List <VTD1_Conference_Member__c> conferenceMembersToInsert = new List <VTD1_Conference_Member__c>();
            for (Video_Conference__c conference : conferences) {
                List <VTD1_Conference_Member__c> conferenceMembers = conferenceMembersMap.get(conference.VTD1_Actual_Visit__c);
                if (conferenceMembers != null) {
                    for (VTD1_Conference_Member__c conferenceMember : conferenceMembers) {
                        conferenceMember.VTD1_Video_Conference__c = conference.Id;
                        conferenceMembersToInsert.add(conferenceMember);
                    }
                }
            }
            if (!conferenceMembersToInsert.isEmpty()) {
                insert conferenceMembersToInsert;
            }
        }
    }
    static Map <String, Decimal> buffersMap = new Map <String, Decimal>();
    static {
        List <VTD1_CalendarBufferForScheduledVisit__mdt> bufferForScheduledVisits = [select Id, VTD1_ProfileId__c, VTD1_VisitType__c, VTD1_Buffer__c from VTD1_CalendarBufferForScheduledVisit__mdt];
        for (VTD1_CalendarBufferForScheduledVisit__mdt bufferForScheduledVisit : bufferForScheduledVisits) {
            buffersMap.put(bufferForScheduledVisit.VTD1_ProfileId__c + '_' + bufferForScheduledVisit.VTD1_VisitType__c, bufferForScheduledVisit.VTD1_Buffer__c);
        }
    }
    public static void scheduleVisits(List <VTD1_Actual_Visit__c> visits, Map <Id, VTD1_Actual_Visit__c> oldMap) {
        System.debug('VT_R5_VisitScheduler.scheduleVisits ' + visits.size());
        List <VTD1_Actual_Visit__c> filteredVisits = new List <VTD1_Actual_Visit__c>();
        List <VTD1_Actual_Visit__c> rescheduledVisits = new List <VTD1_Actual_Visit__c>();
        Map <Id, List <Event>> visitIdToBufferEventMap = new Map <Id, List <Event>>();
        for (VTD1_Actual_Visit__c visit : visits) {
            VTD1_Actual_Visit__c oldVisit = oldMap.get(visit.Id);
            System.debug('visit = ' + visit.VTD1_Status__c + ' ' + visit.VTD1_Scheduled_Date_Time__c);
            System.debug('oldVisit = ' + oldVisit.VTD1_Status__c + ' ' + oldVisit.VTD1_Scheduled_Date_Time__c);
            if (visit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED && (visit.VTD1_Scheduled_Date_Time__c != oldVisit.VTD1_Scheduled_Date_Time__c ||
                    (oldVisit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_MISSED && oldVisit.VTD1_Scheduled_Date_Time__c != null))) {
                filteredVisits.add(visit);
                if (oldVisit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED ||
                        oldVisit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED ||
                        (oldVisit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_MISSED && oldVisit.VTD1_Scheduled_Date_Time__c != null)) {
                    rescheduledVisits.add(visit);
                }
            }
        }
        System.debug('filteredVisits ' + filteredVisits.size());
        System.debug('rescheduledVisits ' + rescheduledVisits.size());
        if (filteredVisits.isEmpty()) {
            return;
        }
        System.debug('schedule more 1 visit');
        Map <Id, Event> visitIdToEventMap = new Map <Id, Event>();
        Map <Id, Event> newVisitIdToEventMap = new Map <Id, Event>();
        if (rescheduledVisits != null) {
            List <Event> events = [select Id, VTD1_Actual_Visit__c, StartDateTime, EndDateTime, VTD1_IsBufferEvent__c from Event where VTD1_Actual_Visit__c in :rescheduledVisits and IsChild = false];
            System.debug('events exist = ' + events.size());
            for (Event event : events) {
                if (!event.VTD1_IsBufferEvent__c) {
                    visitIdToEventMap.put(event.VTD1_Actual_Visit__c, event);
                } else {
                    List <Event> bufferEvents = visitIdToBufferEventMap.get(event.VTD1_Actual_Visit__c);
                    if (bufferEvents == null) {
                        bufferEvents = new List <Event>();
                        visitIdToBufferEventMap.put(event.VTD1_Actual_Visit__c, bufferEvents);
                    }
                    bufferEvents.add(event);
                }
            }
        }
        List <Event> events = new List <Event>();
        List <EventRelation> eventRelations = new List <EventRelation>();
        filteredVisits = [
                select Id, RecordTypeId, VTD1_Event_Subject__c, VTD1_Scheduled_Date_Time__c, VTD1_Visit_Duration__c,
                        VTD1_Case__c, Unscheduled_Visits__c, VTD1_Unscheduled_Visit_Duration__c, VTD1_VisitType_SubType__c, VTR2_Televisit__c, (
                        Select Id, VTD1_Actual_Visit__c, VTD1_Participant_User__c, VTD1_Participant_User__r.ProfileId,
                                VTD1_Name_HHN_Phleb__c, VTD1_External_Participant_Email__c
                        from Visit_Members__r
                )
                from VTD1_Actual_Visit__c
                where Id in :filteredVisits
        ];
        List <Video_Conference__c> conferences = [select Id, VTD1_Actual_Visit__c from Video_Conference__c where VTD1_Actual_Visit__c in : filteredVisits];
        Map <Id, Video_Conference__c> visitToConferencesMap = new Map <Id, Video_Conference__c>();
        Map <Id, List <VTD1_Conference_Member__c>> conferenceMembersMap = new Map <Id, List <VTD1_Conference_Member__c>>();
        List <VTD1_Conference_Member__c> conferenceMembersToInsert = new List <VTD1_Conference_Member__c>();
        for (Video_Conference__c conference : conferences) {
            visitToConferencesMap.put(conference.VTD1_Actual_Visit__c, conference);
        }
        System.debug('conferences exist = ' + conferences);
        conferences = new List <Video_Conference__c>();
        for (VTD1_Actual_Visit__c visit : filteredVisits) {
            System.debug('next visit = ' + visit);
            Decimal visitDuration = visit.RecordTypeId == VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_ACTUAL_VISIT_UNSCHEDULED ?
                    visit.VTD1_Unscheduled_Visit_Duration__c : visit.VTD1_Visit_Duration__c;
            Video_Conference__c conference = visitToConferencesMap.get(visit.Id);
            List <VTD1_Conference_Member__c> members = null;
            if (conference != null) {
                conference.Scheduled_For__c = visit.VTD1_Scheduled_Date_Time__c;
                conferences.add(conference);
            } else if (visit.VTR2_Televisit__c) {
                Id caseId = visit.VTD1_Case__c;
                if (caseId == null) {
                    caseId = visit.Unscheduled_Visits__c;
                }
                conference = new Video_Conference__c(VTD1_Actual_Visit__c = visit.Id, Scheduled_For__c = visit.VTD1_Scheduled_Date_Time__c, VTD1_Clinical_Study_Membership__c = caseId);
                visitToConferencesMap.put(conference.VTD1_Actual_Visit__c, conference);
                conferences.add(conference);
                members = new List <VTD1_Conference_Member__c>();
                conferenceMembersMap.put(visit.Id, members);
            }
            Event event = visitIdToEventMap.get(visit.Id);
            if (event != null) {
                event.StartDateTime = visit.VTD1_Scheduled_Date_Time__c;
                event.EndDateTime = visit.VTD1_Scheduled_Date_Time__c.addMinutes(Math.Round(visitDuration));
                List <Event> bufferEvents = visitIdToBufferEventMap.get(event.VTD1_Actual_Visit__c);
                if (bufferEvents != null) {
                    for (Event bufferEvent : bufferEvents) {
                        Integer bufferEventDuration = (Integer) (bufferEvent.EndDateTime.getTime() - bufferEvent.StartDateTime.getTime()) / 60000;
                        bufferEvent.StartDateTime = event.EndDateTime;
                        bufferEvent.EndDateTime = event.EndDateTime.addMinutes(bufferEventDuration);
                        events.add(bufferEvent);
                    }
                }
            } else {
                for (Visit_Member__c member : visit.Visit_Members__r) {
                    if (member.VTD1_Participant_User__c == null) {
                        if (members != null) {
                            members.add(new VTD1_Conference_Member__c(VTD1_Visit_Member__c = member.Id, VTD1_HHN_Phlebotomist_User__c = member.VTD1_Name_HHN_Phleb__c, VTD1_External_Participant_Email__c = member.VTD1_External_Participant_Email__c));
                        }
                    } else {
                        if (members != null) {
                            members.add(new VTD1_Conference_Member__c(VTD1_Visit_Member__c = member.Id, VTD1_User__c = member.VTD1_Participant_User__c, VTD1_External_Participant_Email__c = member.VTD1_External_Participant_Email__c));
                        }
                        event = visitIdToEventMap.get(visit.Id);
                        if (event == null) {
                            event = new Event(VTD1_Actual_Visit__c = visit.Id, Subject = visit.VTD1_Event_Subject__c, OwnerId = member.VTD1_Participant_User__c, StartDateTime = visit.VTD1_Scheduled_Date_Time__c, EndDateTime = visit.VTD1_Scheduled_Date_Time__c.addMinutes(Math.Round(visitDuration)));
                            visitIdToEventMap.put(visit.Id, event);
                            newVisitIdToEventMap.put(visit.Id, event);
                        }
                        Decimal bufferValue = buffersMap.get(member.VTD1_Participant_User__r.ProfileId + '_' + visit.VTD1_VisitType_SubType__c);
                        if (bufferValue != null) {
                            events.add(new Event(WhatId = visit.Id, VTD1_Actual_Visit__c = visit.Id, Subject = visit.VTD1_Event_Subject__c, OwnerId = member.VTD1_Participant_User__c, StartDateTime = event.EndDateTime, EndDateTime = event.EndDateTime.addMinutes(Math.Round(bufferValue)), VTD1_IsBufferEvent__c = true));
                        }
                    }
                }
            }
        }
        events.addAll(visitIdToEventMap.values());
        if (!events.isEmpty()) {
            upsert events;
        }
        for (Event event : events) {
            System.debug('event after upsert ' + event);
        }
        for (VTD1_Actual_Visit__c visit : filteredVisits) {
            Event event = newVisitIdToEventMap.get(visit.Id);
            if (event == null)
                continue;
            eventRelations.add(new EventRelation(EventId = event.Id, RelationId = event.VTD1_Actual_Visit__c, IsParent = true, IsWhat = true));
            for (Visit_Member__c member : visit.Visit_Members__r) {
                if (member.VTD1_Participant_User__c == event.OwnerId)
                    continue;
                if (member.VTD1_Participant_User__c != null) {
                    eventRelations.add(new EventRelation(EventId = event.Id, RelationId = member.VTD1_Participant_User__c, IsInvitee = true, IsParent = false));
                }
            }
        }
        System.debug('eventRelations size = ' + eventRelations.size());
        System.debug('eventRelations ' + eventRelations);
        if (!eventRelations.isEmpty()) {
            insert eventRelations;
        }
        if (!conferences.isEmpty()) {
            System.enqueueJob(new ConferenceQueueable(conferences, conferenceMembersMap));
        }
    }
    public static void onAddVisitMembers(List <Visit_Member__c> visitMembers) {
        Map <Id, Video_Conference__c> visitIdToConferenceMap = new Map <Id, Video_Conference__c>();
        for (Visit_Member__c member : visitMembers) {
            if (member.VTD1_Actual_Visit__c != null) {
                visitIdToConferenceMap.put(member.VTD1_Actual_Visit__c, null);
            }
        }
        List <Video_Conference__c> conferences = [select Id, VTD1_Actual_Visit__c from Video_Conference__c where VTD1_Actual_Visit__c in : visitIdToConferenceMap.keySet()];
        for (Video_Conference__c conference : conferences) {
            visitIdToConferenceMap.put(conference.VTD1_Actual_Visit__c, conference);
        }
        System.debug('conferences = ' + conferences.size());
        if (visitIdToConferenceMap.isEmpty()) {
            return;
        }
        visitMembers = [select Id, VTD1_Actual_Visit__c, VTD1_Actual_Visit__r.VTR2_Televisit__c, VTD1_Participant_User__c, VTD1_Name_HHN_Phleb__c, VTD1_External_Participant_Email__c
            from Visit_Member__c where VTD1_Actual_Visit__r.VTD1_Status__c = :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED
            and VTD1_Actual_Visit__r.VTR2_Televisit__c = true and Id in : visitMembers];
        List <VTD1_Conference_Member__c> conferenceMembers = new List <VTD1_Conference_Member__c>();
        for (Visit_Member__c member : visitMembers) {
            Video_Conference__c conference = visitIdToConferenceMap.get(member.VTD1_Actual_Visit__c);
            if (conference != null) {
                if (member.VTD1_Participant_User__c == null) {
                    conferenceMembers.add(new VTD1_Conference_Member__c(VTD1_Video_Conference__c = conference.Id, VTD1_Visit_Member__c = member.Id, VTD1_HHN_Phlebotomist_User__c = member.VTD1_Name_HHN_Phleb__c, VTD1_External_Participant_Email__c = member.VTD1_External_Participant_Email__c));
                } else {
                    conferenceMembers.add(new VTD1_Conference_Member__c(VTD1_Video_Conference__c = conference.Id, VTD1_Visit_Member__c = member.Id, VTD1_User__c = member.VTD1_Participant_User__c, VTD1_External_Participant_Email__c = member.VTD1_External_Participant_Email__c));
                }
            }
        }
        if (!conferenceMembers.isEmpty())
            insert conferenceMembers;
    }
}