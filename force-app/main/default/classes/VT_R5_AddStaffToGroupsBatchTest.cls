@IsTest
private class VT_R5_AddStaffToGroupsBatchTest {
    @TestSetup
    static void testSetup() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.stopTest();
    }

    @IsTest
    static void doTest() {
        Test.startTest();
        Database.executeBatch(new VT_R5_AddStaffToGroupsBatch());
        Test.stopTest();
    }
}