/**
* @author: Carl Judge
* @date: 03-Sep-20
* @description:
**/

@IsTest
private class VT_R5_SharingFixerTest {
    @TestSetup
    static void setup() {
        DomainObjects.Account_t account_t = new DomainObjects.Account_t().setRecordTypeByName('Sponsor');
        DomainObjects.Study_t study = new DomainObjects.Study_t()
            .setMaximumDaysWithoutLoggingIn(1)
            .addAccount(account_t);
        study.persist();

        Account account = (Account) account_t.toObject();

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
            .setFirstName('PatientDevices')
            .setLastName('PatientDevices')
            .setAccountId(account.Id);

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
            .addContact(patientContact)
            .setProfile('Patient')
            .setUsername('VT_R5_RestPatientDevicesTest@test.com');
        patientUser.persist();

        Case patientCase = (Case) new DomainObjects.Case_t()
            .addPIUser(new DomainObjects.User_t())
            .addVTD1_Patient_User(patientUser)
            .setStudy(((HealthCloudGA__CarePlanTemplate__c) study.toObject()).Id)
            .setContactId(((User) patientUser.toObject()).ContactId)
            .setRecordTypeByName('CarePlan')
            .setAccountId(account.Id)
            .persist();
    }

    @IsTest
    static void doTest() {
        Test.startTest();
        SchedulableContext sc = null;
        VT_R5_SharingFixer fixer = new VT_R5_SharingFixer();
        fixer.nextRunIntervalMinutes = 10;
        fixer.execute(sc);
        Test.stopTest();
    }

    @IsTest
    static void doTest2() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c ORDER BY CreatedDate DESC LIMIT 1];
        User patient = [SELECT Id FROM User WHERE Profile.Name = 'Patient' ORDER BY CreatedDate DESC LIMIT 1];
        insert new VTD1_Study_Sharing_Configuration__c(VTD1_User__c = patient.Id, VTD1_Study__c = study.Id, VTD1_Access_Level__c = 'Edit');
        Test.stopTest();
    }
}