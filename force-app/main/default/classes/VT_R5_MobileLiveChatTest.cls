/**
 * @description :
 *
 * @author : Alexander Komarov
 *
 * @date : 06.07.2020
 */
@IsTest
public with sharing class VT_R5_MobileLiveChatTest {

    public static void firstTest() {
        Test.startTest();
        User toRun = [SELECT Id, VT_R5_Tertiary_Preferred_Language__c, VT_R5_Secondary_Preferred_Language__c FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        System.runAs(toRun) {
            RestContext.request = new RestRequest();
            RestContext.response = new RestResponse();
            RestContext.request.requestURI = '/patient/v1/live-chat-settings/';
            RestContext.request.httpMethod = 'GET';
            VT_R5_MobileRestRouter.doGET();
        }
        Test.stopTest();
    }

    public static void secondTest() {
        Test.startTest();
        User toRun = [SELECT Id, VT_R5_Tertiary_Preferred_Language__c, VT_R5_Secondary_Preferred_Language__c FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];

        Id itButtonId = fflib_IDGenerator.generate(LiveChatButton.getSObjectType());
        Id deButtonId = fflib_IDGenerator.generate(LiveChatButton.getSObjectType());
        Id en_USButtonId = fflib_IDGenerator.generate(LiveChatButton.getSObjectType());

        VT_Stubber.applyStub('liveChatButtons',
                new List<LiveChatButton>{
                        new LiveChatButton(Id = itButtonId, DeveloperName = 'some-id_it'),
                        new LiveChatButton(Id = deButtonId, DeveloperName = 'some-id-2_de'),
                        new LiveChatButton(Id = en_USButtonId, DeveloperName = 'some-id-3_en_US')
                });

        VT_Stubber.applyStub('skillBasedButtons',
                new Map<String, Id>{
                        'secondary' => itButtonId,
                        'tertiary' => deButtonId,
                        'primary' => en_USButtonId
                });

        System.runAs(toRun) {
            RestContext.request = new RestRequest();
            RestContext.response = new RestResponse();
            RestContext.request.requestURI = '/patient/v2/live-chat-settings/';
            RestContext.request.httpMethod = 'GET';
            VT_R5_MobileRestRouter.doGET();
        }
        Test.stopTest();

    }

    public static void thirdTest() {
        Test.startTest();
        User toRun = [SELECT Id, VT_R5_Tertiary_Preferred_Language__c, VT_R5_Secondary_Preferred_Language__c FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        System.runAs(toRun) {
            RestContext.request = new RestRequest();
            RestContext.response = new RestResponse();
            RestContext.request.httpMethod = 'GET';
            RestContext.request.requestURI = '/patient/v0/live-chat-settings/';//unsupported version
            VT_R5_MobileRestRouter.doGET();
        }
        Test.stopTest();
    }
}