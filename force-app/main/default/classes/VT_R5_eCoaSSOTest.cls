/**
* @author: Olya Baranova
* @date: 17-Jul-20
* @description:
**/

@IsTest
public with sharing class VT_R5_eCoaSSOTest {

    @TestSetup
    private static void setupMethod() {

        eCOA_IntegrationDetails__c customSetting = new eCOA_IntegrationDetails__c();
        customSetting.eCOA_OrgGuid__c = 'org_altavoz';
        customSetting.Cognito_UserPoolId__c = 'us-east-1_RpJI95p9g';
        customSetting.Cognito_ClientId__c = '7s29ancc3i683c6fcme3rm2m0f';
        customSetting.VTR5_certificateName__c = 'ecoa_sso';
        customSetting.VTR5_eCoa_IssuerGuid__c = 'issuer_97cde1e1-958d-4af7-a193-99c0f4fbf536';
        customSetting.VTR5_eCoaEnvironment__c = 'k9';
        insert customSetting;

        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .setEdiaryTool('eCOA')
                .setEcoaGuid('study_79826f74-6967-4eb0-bc99-f50629e66f0c')
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));
        DomainObjects.VirtualSite_t virtSite = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .setExternalDiaryLanguage('en_US;de;ru')
                .setDefaultExternalDiaryLanguage('de');
        virtSite.persist();

        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t();
        patientAccount.persist();
        //patient
        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
                .setAccountId(patientAccount.id)
                .setFirstName('Morty_the_Patient');
        DomainObjects.User_t userPatient = new DomainObjects.User_t()
                .addContact(patientContact)
                .setEcoaGuid('subject_9ecb75cf-a594-45b8-6667-7d11bd9bd41d')
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        userPatient.persist();
        //caregiver
        DomainObjects.Contact_t caregiverContact = new DomainObjects.Contact_t()
                .setVTD1_RelationshiptoPatient('Parent')
                .setAccountId(patientAccount.id)
                .setEmail(DomainObjects.RANDOM.getEmail())
                .setFirstName('Rick')
                .setLastName('VT_R5_eCoaSSOTest')
                .setRecordTypeName('Caregiver');
        DomainObjects.User_t caregiverUser = new DomainObjects.User_t()
                .addContact(caregiverContact)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME);
        caregiverUser.persist();


        // primary investigator 
        DomainObjects.Account_t PiAccount = new DomainObjects.Account_t()
            												 .setName('PrimaryInvestigator');
        PiAccount.persist();
        DomainObjects.Contact_t PiContact =  new DomainObjects.Contact_t()
            												  .setAccountId(PiAccount.id)
            												  .setEmail(DomainObjects.RANDOM.getEmail())
            												  .setFirstName('Primary')
            												  .setLastName('Investigator')
            												  .SetRecordTypeName('PI');
        PiContact.persist();
        DomainObjects.Address_t PiAddress = new DomainObjects.Address_t()
            												 .setAddressLine1('Akshya Nagar')
            												 .setAddressLine2('1st Block')
            												 .setContact(PiContact.id)
            												 .setPrimary(true)
            												 .setCity('NewYork')
            												 .setState('NewYork')
            												 .SetZipCode('98389')
            												 .setCountry('US');
        
        PiAddress.persist();
        DomainObjects.Phone_t PiPhone = new DomainObjects.Phone_t('989898983')
            											.setContact(PiContact.id)
            											.setType('Mobile')
            											.addAccount(PiAccount)
            											.setPrimaryForPhone(true);
        PiPhone.persist();   


        //case
        DomainObjects.Case_t casePatient = new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .addStudy(study)
                .addUser(userPatient)
                .addContact(patientContact)

                .addAccount(patientAccount)
                .addVTD1_PI_contact(PiContact);

        casePatient.persist();
        patientContact.addVTD1_Clinical_Study_Membership(casePatient);

        Account acc = [SELECT Id,VTD2_Patient_s_Contact__c FROM Account WHERE Id = :patientAccount.id];
        acc.VTD2_Patient_s_Contact__c = patientContact.id;
        update acc;


        Contact con = [SELECT Id, VTD1_UserId__c,VTD1_Clinical_Study_Membership__c FROM Contact WHERE Id = :patientContact.id];
        con.VTD1_UserId__c = userPatient.id;
        con.VTD1_Clinical_Study_Membership__c=casePatient.Id;

        update con;

    }

    @IsTest
    private static void getPatientToken() {

        Test.startTest();

        User patientUsr = [
                SELECT  ContactId,Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_External_Diary_Languages__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_Default_External_Diary_Language__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_PI_contact__c,Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTR5_eCOA_Consent_Diary_triggered__c,Contact.VTD1_Clinical_Study_Membership__c,
                        LanguageLocaleKey
                FROM User
                WHERE VTR5_eCOA_Guid__c = 'subject_9ecb75cf-a594-45b8-6667-7d11bd9bd41d'
        ];
       
        VT_R5_eCoaSSO.getToken(patientUsr.Id);

        Test.stopTest();
    }

    @IsTest
    private static void getCaregiverToken() {




        Test.startTest();

        User caregiverUsr = [
                SELECT Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_External_Diary_Languages__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_Default_External_Diary_Language__c,
                        LanguageLocaleKey
                FROM User
                WHERE Profile.Name = 'Caregiver'
                AND Contact.Account.VTD2_Patient_s_Contact__r.VTD1_UserId__r.VTR5_eCOA_Guid__c = 'subject_9ecb75cf-a594-45b8-6667-7d11bd9bd41d'
        ];
        VT_R5_eCoaSSO.getToken(caregiverUsr.Id);



        Test.stopTest();
    }
}