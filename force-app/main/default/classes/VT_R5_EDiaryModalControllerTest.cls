/**
 * Created by Maksim Fedarenka on 8/12/2020.
 */

@IsTest
public with sharing class VT_R5_EDiaryModalControllerTest {
    public static void testBehavior() {
        VTD1_Survey__c eDiaryStub = new VTD1_Survey__c();
        VT_R5_EDiaryModalController.AnswersWrapper wrapper = VT_R5_EDiaryModalController.getSurveyAnswers(eDiaryStub);

        System.assert(wrapper != null);
    }
}