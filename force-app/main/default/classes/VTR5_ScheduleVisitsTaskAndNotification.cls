/**
 * Created by Stanislav Frolov on 8/4/2020.
 */

public with sharing class VTR5_ScheduleVisitsTaskAndNotification {

    private static Map<Id, VTD1_Actual_Visit__c> updateVisitsMap = new Map<Id, VTD1_Actual_Visit__c>();
    // SH-13732
    public static Boolean isLastOnboardingVisitComplete = false;
    public static Boolean isLillyStudy = false;

    public class ParamsHolder {
        public String visitCaseId;
        public Integer visitOrderNumber;
        public String caseIdSTM;
        public String patientGuideId;
        public Boolean siteStaffRespForElgl;
        public String scrId;

    }
    private static final String ONBOARDING_VISIT = 'Onboarding Visit';
    private static final String ACTUAL_VISIT = 'Actual Visit';
    private static final String BASELINE_VISIT = 'Baseline Visit';

    // SH-13732
    private static final String PROTOCOL_BASELINE_VISIT = 'Protocol Baseline Visit';
    private static final String PROTOCOL_VISIT = 'Protocol Visit';

    private static final String BASELINE_TN_CODE = '300';
    private static final String ONBOARDING_TN_CODE = '002';

    private static final String ACTUAL_VISIT_QUERY_FIELDS = ' To_Be_Scheduled_Date__c, ' +
            ' VTD2_Schedule_Notification_Time_Formula__c,' +
            ' VTR2_f_ScheduleNotificationPT__c,' +
            ' VTD1_Visit_Type__c, Sub_Type__c,' +
            // SH-13732
            ' VTD1_Visit_Number__c, ' +
            ' VTD2_Baseline_Visit__c ';

    private static final Map<String, String> visitConditions = new Map<String, String>{
            BASELINE_VISIT =>
                    ' AND VTD1_Onboarding_Type__c = \'Baseline Visit\'' +
                            ' AND VTD1_Status__c = \'Future Visit\'',
            ONBOARDING_VISIT =>
                    ' AND VTR2_f_OnbordingVisitOrderNumber__c <> NULL' +
                            ' AND VTD1_Status__c = \'Future Visit\' ' +
                            ' AND VTD1_Onboarding_Type__c NOT IN (\'Baseline Visit\', \'N/A\')',
            ACTUAL_VISIT =>
                    ' AND VTR3_LTFUStudyVisit__c = TRUE' +
                            ' AND VTD1_Status__c <> \'Cancelled\'' +
                            ' AND (' +
                            '(VTD1_Visit_Type__c = \'Labs\' AND Sub_Type__c IN (\'Phlebotomist\', \'PSC\', \'Home Health Nurse\') )' +
                            'OR (VTD1_Visit_Type__c IN (\'Health Care Professional (HCP)\', \'Study Team\') )' +
                            ')',
            PROTOCOL_BASELINE_VISIT =>
                    ' AND VTR3_LTFUStudyVisit__c = TRUE' +
                            ' AND VTD2_Baseline_Visit__c = TRUE ' +
                            ' AND VTD1_Status__c <> \'Cancelled\'' +
                            ' AND (' +
                            '(VTD1_Visit_Type__c = \'Labs\' AND Sub_Type__c IN (\'Phlebotomist\', \'PSC\', \'Home Health Nurse\') )' +
                            'OR (VTD1_Visit_Type__c IN (\'Health Care Professional (HCP)\', \'Study Team\') )' +
                            ')',
            PROTOCOL_VISIT =>
                    ' AND VTR3_LTFUStudyVisit__c = TRUE' +
                            ' AND VTD1_Onboarding_Type__c = \'N/A\'' +
                            ' AND VTD2_Baseline_Visit__c = FALSE ' +
                            ' AND VTD1_Status__c NOT IN (\'Cancelled\', \'Completed\')' +
                            ' AND (' +
                            '(VTD1_Visit_Type__c = \'Labs\' AND Sub_Type__c IN (\'Phlebotomist\', \'PSC\', \'Home Health Nurse\') )' +
                            'OR (VTD1_Visit_Type__c IN (\'Health Care Professional (HCP)\', \'Study Team\') )' +
                            ')'
    };

    public static void doVisitSchedulingAndTasks(List<ParamsHolder> params) {
        if (!(System.isFuture() || System.isBatch() || System.isQueueable()) && !Test.isRunningTest()) {
            doVisitSchedulingAndTasksFuture (JSON.serialize(params));
        } else {
            doVisitSchedulingAndTasksSync(params);
        }
    }

    @Future(callout = true)
    public static void doVisitSchedulingAndTasksFuture (String paramsString) {
        List<ParamsHolder> params = ( List<ParamsHolder>) JSON.deserialize(paramsString, List<ParamsHolder>.class);
        doVisitSchedulingAndTasksSync(params);
    }

    public static void doVisitSchedulingAndTasksSync (List<ParamsHolder> params) {
        Map<String, String> paramsOnCaseId = new Map<String, String>();
        Map<String, Integer> visitOrderNumberOnCaseId = new Map<String, Integer>();
        Map<String, ParamsHolder> caseIdsToCalcAssignee;
        updateVisitsMap = new Map<Id, VTD1_Actual_Visit__c>();
        List<String> stmCaseIds = new List<String>();

        for (ParamsHolder param : params) {
            if (param.visitCaseId != null) {
                if (param.visitOrderNumber != null) {
                    visitOrderNumberOnCaseId.put(param.visitCaseId, ++param.visitOrderNumber);
                } else {
                    if (param.siteStaffRespForElgl != null && param.siteStaffRespForElgl) {
                        paramsOnCaseId.put(param.visitCaseId, param.scrId);
                    } else {
                        if (param.scrId != null && param.patientGuideId != null) {
                            if (caseIdsToCalcAssignee == null) {
                                caseIdsToCalcAssignee = new Map<String, ParamsHolder>();
                            }
                            caseIdsToCalcAssignee.put(param.visitCaseId, param);
                        } else if (param.patientGuideId != null) {
                            paramsOnCaseId.put(param.visitCaseId, param.patientGuideId);
                        } else {
                            paramsOnCaseId.put(param.visitCaseId, param.scrId);
                        }
                    }
                }
            } else if (param.caseIdSTM != null) {
                stmCaseIds.add(param.caseIdSTM);
            }
        }
        if (caseIdsToCalcAssignee != null) {
            paramsOnCaseId.putAll(calculateTaskAssignee(caseIdsToCalcAssignee));
        }

        if (!stmCaseIds.isEmpty()) {
            // Sh-13732
            String VISIT_CONDITION = isLillyStudy
                    ? (isLastOnboardingVisitComplete
                            ? PROTOCOL_BASELINE_VISIT
                            : PROTOCOL_VISIT)
                    : ACTUAL_VISIT;
            List<VTD1_Actual_Visit__c> aVisits = getVisits(stmCaseIds, visitConditions.get(VISIT_CONDITION), ACTUAL_VISIT_QUERY_FIELDS);
            if (!aVisits.isEmpty()) {
                scheduleActualVisits(aVisits);
            }
        }
        if (!paramsOnCaseId.isEmpty()) {
            scheduleVisitsAndGenerateTasks(paramsOnCaseId, BASELINE_VISIT);
        }
        if (!visitOrderNumberOnCaseId.isEmpty()) {
            scheduleVisitsAndGenerateTasks(visitOrderNumberOnCaseId, ONBOARDING_VISIT);
        }
        update updateVisitsMap.values();
    }


    private static void scheduleVisitsAndGenerateTasks(Map<String, Object> casesInfo, String visitType) {
        Map<String, String> assignedTo;
        List<Id> visitIds = new List<Id>();
        for (VTD1_Actual_Visit__c v : getVisits(new List<String>(casesInfo.keySet()), visitConditions.get(visitType), '')) {
            if (visitType == ONBOARDING_VISIT && (Integer) casesInfo.get(v.VTD1_Case__c) != v.VTR2_f_OnbordingVisitOrderNumber__c) {
                continue;
            }
            if (visitType == BASELINE_VISIT && casesInfo.get(v.VTD1_Case__c) != null) {
                String assignee = (String) casesInfo.get(v.VTD1_Case__c);
                if (assignedTo != null) {
                    assignedTo.put(BASELINE_TN_CODE + v.Id, assignee);
                } else {
                    assignedTo = new Map<String, String>{
                            BASELINE_TN_CODE + v.Id => assignee
                    };
                }
            }
            if(!updateVisitsMap.containsKey(v.Id)){
                updateVisitsMap.put(v.Id, v);
            }
            updateVisitsMap.get(v.Id).VTD1_Status__c = 'To Be Scheduled';
            visitIds.add(v.Id);
            //updateVisits.add(v);
        }

        String tnCode = visitType == BASELINE_VISIT ? BASELINE_TN_CODE : ONBOARDING_TN_CODE;
        if(!visitIds.isEmpty()){
            VT_D2_TNCatalogTasks.generateTasks(new List<String>{
                    tnCode
            }, visitIds, null, assignedTo);
        }
    }

    public static void scheduleActualVisits(List<VTD1_Actual_Visit__c> visits) {
        Set<String> phlebotomistFounds;
        Set<String> firstHHNFounds;

        for (VTD1_Actual_Visit__c v : visits) {

            Boolean visitIsChanged = false;
            // SH-13732
            if(v.VTD1_Visit_Number__c == 'v1' && v.VTD2_Baseline_Visit__c) {
                v.VTD1_Schedule_Visit_Task_Date__c = Date.today();//SH-14867 : Populating VTR2_ScheduleVisitTaskPatient__c
                v.VTR2_ScheduleVisitTaskPatient__c = (v.VTR2_f_ScheduleNotificationPT__c != null && v.To_Be_Scheduled_Date__c != null)
                        ? v.To_Be_Scheduled_Date__c - Integer.valueOf(v.VTR2_f_ScheduleNotificationPT__c)
                        : null;
                visitIsChanged = true;
            } else if (v.VTD1_Visit_Type__c == 'Study Team') {
                v.VTD1_Schedule_Visit_Task_Date__c = (v.VTD2_Schedule_Notification_Time_Formula__c != null && v.To_Be_Scheduled_Date__c != null)
                        ? v.To_Be_Scheduled_Date__c - Integer.valueOf(v.VTD2_Schedule_Notification_Time_Formula__c)
                        : null;
                v.VTR2_ScheduleVisitTaskPatient__c = (v.VTR2_f_ScheduleNotificationPT__c != null && v.To_Be_Scheduled_Date__c != null)
                        ? v.To_Be_Scheduled_Date__c - Integer.valueOf(v.VTR2_f_ScheduleNotificationPT__c)
                        : null;
                visitIsChanged = true;
            } else if (v.VTD1_Visit_Type__c == 'Health Care Professional (HCP)') {
                v.VTD1_Schedule_Visit_Task_Date__c = Date.today();
                visitIsChanged = true;
            } else if (v.Sub_Type__c == 'Phlebotomist' && v.VTD1_Visit_Type__c == 'Labs') {
                if (phlebotomistFounds == null) {
                    phlebotomistFounds = new Set<String>();
                }
                if (v.To_Be_Scheduled_Date__c != null) {
                    v.VTD1_Schedule_Visit_Task_Date__c = !phlebotomistFounds.contains(v.VTD1_Case__c)
                            ? v.To_Be_Scheduled_Date__c - 21
                            : v.To_Be_Scheduled_Date__c - 14;
                } else {
                    v.VTD1_Schedule_Visit_Task_Date__c = null;
                }
                phlebotomistFounds.add(v.VTD1_Case__c);
                visitIsChanged = true;
            } else if (v.Sub_Type__c == 'Home Health Nurse' && v.VTD1_Visit_Type__c == 'Labs') {
                if (firstHHNFounds == null) {
                    firstHHNFounds = new Set<String>();
                }
                if (v.To_Be_Scheduled_Date__c != null) {
                    v.VTD1_Schedule_Visit_Task_Date__c = !firstHHNFounds.contains(v.VTD1_Case__c)
                            ? v.To_Be_Scheduled_Date__c - 21
                            : v.To_Be_Scheduled_Date__c - 14;
                } else {
                    v.VTD1_Schedule_Visit_Task_Date__c = null;
                }
                firstHHNFounds.add(v.VTD1_Case__c);
                visitIsChanged = true;
            } else if (v.Sub_Type__c == 'PSC' && v.VTD1_Visit_Type__c == 'Labs') {
                v.VTD1_Schedule_Visit_Task_Date__c = v.To_Be_Scheduled_Date__c - 14;
                visitIsChanged = true;
            }
            if(visitIsChanged){
                updateVisitsMap.put(v.Id, v);
            }
        }
    }

    private static List<VTD1_Actual_Visit__c> getVisits(List<String> caseIds, String additionalConditions, String additionalFields) {
        String query = 'SELECT Id, VTD1_Case__c, VTD1_Status__c, VTR2_f_OnbordingVisitOrderNumber__c' + (String.isNotBlank(additionalFields) ? ', ' : ' ') + additionalFields +
                ' FROM VTD1_Actual_Visit__c ' +
                ' WHERE VTD1_Case__c IN :caseIds ' + additionalConditions;
        return Database.query(query);
    }

    public static Map<String, String> calculateTaskAssignee(Map<String, ParamsHolder> paramsOnCases) {
        Map<String, String> assigneeOnCases = new Map<String, String>();
        String addCond = ' AND VTD1_Onboarding_Type__c = \'Screening\'';
        String addField = ' VTR2_Modality__c';
        for (VTD1_Actual_Visit__c v : getVisits(new List<String>(paramsOnCases.keySet()), addCond, addField)) {
            if (v.VTR2_Modality__c == 'At Location') {
                assigneeOnCases.put(v.VTD1_Case__c, paramsOnCases.get(v.VTD1_Case__c).scrId);
            } else {
                assigneeOnCases.put(v.VTD1_Case__c, paramsOnCases.get(v.VTD1_Case__c).patientGuideId);
            }
        }
        return assigneeOnCases;
    }
    
}