/**
 * @author: Alexander Komarov
 * @date: 07.05.2019
 * @description: REST Resource for mobile app to get study supplies data and perform study supplies actions.
 */
@RestResource(UrlMapping='/Patient/StudySupplies/*')
global with sharing class VT_R3_RestPatientStudySupplies {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());
    public static Boolean formUpdated;
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();
    static Map<String, String> BUTTON_LABELS = new Map<String, String>{
            'Confirm delivery' => System.Label.VTD1_ConfirmDelivery,
            'Confirm Packaging Materials In Hand' => System.Label.VTD1_ConfirmPackagingMaterialsInHand,
            'Request Replacement' => System.Label.VTD1_RequestReplacement,
            'Confirm Pickup' => System.Label.VTD1_ConfirmPickup,
            'Confirm DropOff' => System.Label.VTD1_ConfirmDropOff
    };
    @HttpGet
    global static String getSupplies() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        String temp = request.requestURI.substringAfterLast('/StudySupplies/');
        PatientStudySuppliesResponse responseObject = new PatientStudySuppliesResponse();
        VT_D1_PatientMyStudySuppliesController.StudySupplies wrapperFromController;
        formUpdated = [SELECT VTD1_Form_Updated__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].VTD1_Form_Updated__c;
        try {
            wrapperFromController = VT_D1_PatientMyStudySuppliesController.getStudySuppliesWrapper();
        } catch (Exception e) {
            response.statusCode = 500;
            cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName());
            return JSON.serialize(cr, true);
        }
        Id particularDeliveryId = null;
        Id particularKitId = null;
        if (!String.isBlank(temp)) {
            if (!helper.isIdCorrectType(temp, 'VTD1_Patient_Kit__c')) {
                if (!helper.isIdCorrectType(temp, 'VTD1_Order__c')) {
                    response.statusCode = 400;
                    cr.buildResponse(helper.forAnswerForIncorrectInput());
                    return JSON.serialize(cr, true);
                } else {
                    particularDeliveryId = Id.valueOf(temp);
                }
            } else {
                particularKitId = Id.valueOf(temp);
            }
        }
        try {
            System.debug('forming list: kit id = '+particularKitId + 'delivery id = '+particularDeliveryId);
            responseObject.items = formListResponse(wrapperFromController, particularKitId, particularDeliveryId);
        } catch (Exception e) {
            response.statusCode = 500;
            cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName());
            return JSON.serialize(cr, true);
        }
        responseObject.replacementReasons = wrapperFromController.replacementReasons;
        responseObject.kitTypes = wrapperFromController.kitTypes;
        responseObject.deliveryStatuses = wrapperFromController.deliveryStatuses;
        cr.buildResponse(responseObject);
        return JSON.serialize(cr, true);
    }
    //Confirm delivery;Confirm Packaging Materials In Hand;Request Replacement;Confirm Pickup;Confirm DropOff
    @HttpPost
    global static String doAction(String action, String deliveryId, Boolean isIMP, String reason, String comment, Boolean needPackagingMaterials, Long dateTimeUnix, String kitId) {
        System.debug('GOT DELIVERY ID = '+deliveryId + ' GOT KIT ID = '+kitId);
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        String delName = [SELECT Id, Name FROM VTD1_Order__c WHERE Id = :deliveryId LIMIT 1].Name;
        //check all needed parameters is here
        if (action.equalsIgnoreCase('Confirm Packaging Materials In Hand')) {
            VT_D1_MyStudySuppliesItemController.confPackMatHand(deliveryId, needPackagingMaterials);
            cr.buildResponse(getStudySuppliesList(), System.Label.VTR3_Confirm_Packaging_Materials_Request.replace('#1', delName));
        } else if (action.equalsIgnoreCase('Request Replacement')) {
            Case c = VT_D1_PatientCaregiverBound.getCaseForUser();
            String patientName = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
            if (isIMP) {
                VT_D1_MyStudySuppliesItemController.confReqReplDelivery(c.Id, kitId, delName, patientName, isIMP, reason, comment);
            } else {
                VT_D1_MyStudySuppliesItemController.confReqReplDelivery(c.Id, kitId, delName, patientName, isIMP, null, null);
            }
            //String caseId, String deliveryName, String patientName,
            //            Boolean deliveryIsIMP, String requestReplacementReason, String requestReplacementComments, Id patientKitId
            cr.buildResponse(getStudySuppliesList(), System.Label.VTR3_Kit_Replacement_Request.replace('#1', delName));
        } else if (action.equalsIgnoreCase('Confirm Pickup')) {
            Datetime d = Datetime.newInstance(dateTimeUnix - timeOffset);
            VT_D1_MyStudySuppliesItemController.confPickup(deliveryId, d);
            cr.buildResponse(getStudySuppliesList(), System.Label.VTR3_Success_Confirm_Return_Delivery.replace('#1', delName));
        } else if (action.equalsIgnoreCase('Confirm DropOff')) {
            Datetime d = Datetime.newInstance(dateTimeUnix - timeOffset);
            VT_D1_MyStudySuppliesItemController.confDropOff(deliveryId, d);
            cr.buildResponse(getStudySuppliesList(), System.Label.VTR3_Success_Confirm_Return_Delivery.replace('#1', delName));
        } else if (action.equalsIgnoreCase('Confirm delivery')) {
            response.statusCode = 400;
            return 'Wrong destination. For confirm delivery use /services/apexrest/Patient/StudySupplies/Confirm/*';
        } else {
            response.statusCode = 400;
            return 'No such action';
        }
        return JSON.serialize(cr, true);
    }
    public static PatientStudySuppliesResponse getStudySuppliesList() {
        formUpdated = [SELECT VTD1_Form_Updated__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].VTD1_Form_Updated__c;
        PatientStudySuppliesResponse responseObject = new PatientStudySuppliesResponse();
        VT_D1_PatientMyStudySuppliesController.StudySupplies wrapperFromController = VT_D1_PatientMyStudySuppliesController.getStudySuppliesWrapper();
        responseObject.replacementReasons = wrapperFromController.replacementReasons;
        responseObject.kitTypes = wrapperFromController.kitTypes;
        responseObject.deliveryStatuses = wrapperFromController.deliveryStatuses;
        responseObject.items = formListResponse(wrapperFromController, null, null);
        return responseObject;
    }
    public static List<PatientMobileStudySupply> formListResponse(VT_D1_PatientMyStudySuppliesController.StudySupplies supplies, Id kitId, Id deliveryId) {
        List<PatientMobileStudySupply> result = new List<PatientMobileStudySupply>();
        Boolean searchForDelivery = deliveryId != null;
        Boolean searchForKit = kitId != null;
        Boolean gotNeededOne = false;
        for (VT_D1_PatientMyStudySuppliesController.StudySupplyItem studySupplyItem : supplies.studySupplyList) {
            if (searchForDelivery) {
                if (!studySupplyItem.delivery.Id.equals(deliveryId) || gotNeededOne) {
                    continue;
                } else {
                    gotNeededOne = true;
                }
            }
            Boolean buttonNeed = checkIfButtonNeedForDelivery(studySupplyItem);
            Boolean updateNeed = isUpdateNeed(studySupplyItem);
            if ((studySupplyItem.kitsList != null && !searchForDelivery)) {
                for (VTD1_Patient_Kit__c kit : studySupplyItem.kitsList) {
                    System.debug('kit to process - '+kit);
                    System.debug('current search status got? - '+gotNeededOne + ' searching? - '+searchForKit);
                    if (searchForKit) {
                        System.debug('income id = '+kitId + ' in list id = '+kit.Id + ' equals? = '+kit.Id.equals(kitId));
                        if (!kit.Id.equals(kitId) || gotNeededOne) {
                            continue;
                        } else {
                            gotNeededOne = true;
                        }
                    }
                    String buttonValue = buttonNeed ? determinateButton(studySupplyItem, kit) : '';
                    System.debug('ADDING TO LIST');
                    result.add(new PatientMobileStudySupply(
                            false,
                            studySupplyItem.delivery.Id,
                            kit.Id,
                            kit.RecordType.Name == 'IMP' ? System.Label.VTD1_MedicationKit : kit.VTD1_Kit_Type__c,
                            getKitName(kit),
                            studySupplyItem.delivery.VTD1_Protocol_Delivery__r.VTD1_Description__c,
                            String.valueOf(studySupplyItem.delivery.get('translatedStatus')),
                            studySupplyItem.delivery.LastModifiedDate,
                            kit.VTD1_Recalled__c,
                            studySupplyItem.delivery.VTD1_isPackageDelivery__c,
                            buttonValue,
                            updateNeed,
                            BUTTON_LABELS.get(buttonValue),
                            studySupplyItem.delivery.VTD1_Shipment_Type_formula__c,
                            studySupplyItem.delivery.VTD1_Carrier__c,
                            studySupplyItem.delivery.VTD1_TrackingNumber__c,
                            studySupplyItem.shipmentDate,
                            studySupplyItem.expectedDeliveryDate,
                            getExpectedDeliveryDate(studySupplyItem.delivery.VTD1_ExpectedDeliveryDate__c)
                    ));
                }
            } else if (!searchForKit) {
                String buttonValue = buttonNeed ? determinateButton(studySupplyItem, null) : '';
                result.add(new PatientMobileStudySupply(
                        true,
                        studySupplyItem.delivery.Id,
                        null,
                        null,
                        studySupplyItem.translatedDeliveryName,
                        studySupplyItem.delivery.VTD1_Protocol_Delivery__r.VTD1_Description__c,
                        String.valueOf(studySupplyItem.delivery.get('translatedStatus')),
                        studySupplyItem.delivery.LastModifiedDate,
                        false,
                        studySupplyItem.delivery.VTD1_isPackageDelivery__c,
                        buttonValue,
                        updateNeed,
                        BUTTON_LABELS.get(buttonValue),
                        studySupplyItem.delivery.VTD1_Shipment_Type_formula__c,
                        studySupplyItem.delivery.VTD1_Carrier__c,
                        studySupplyItem.delivery.VTD1_TrackingNumber__c,
                        studySupplyItem.shipmentDate,
                        studySupplyItem.expectedDeliveryDate,
                        getExpectedDeliveryDate(studySupplyItem.delivery.VTD1_ExpectedDeliveryDate__c)
                ));
            }
        }
        return result;
    }
    private static Datetime getExpectedDeliveryDate(Date expectedDeliveryDate) {
        if (expectedDeliveryDate != null) {
            return Datetime.newInstance(expectedDeliveryDate.year(), expectedDeliveryDate.month(), expectedDeliveryDate.day());
        } else return null;
    }
    private static Boolean isUpdateNeed(VT_D1_PatientMyStudySuppliesController.StudySupplyItem item) {
        return item.delivery.VTD1_Status__c == 'Pending Shipment' && !formUpdated && item.delivery.VTR2_Destination_Type__c != 'Site';
    }
    private static String getKitName(VTD1_Patient_Kit__c kitItem) {
        if (kitItem.RecordType.Name == 'Packaging Materials') {
            return System.Label.VTR3_PackagingMaterials;
        } else if (kitItem.VTD1_Protocol_Kit__c != null) {
            return kitItem.VTD1_Protocol_Kit__r.Name;
        } else {
            return kitItem.VTD1_Kit_Name__c;
        }
    }
    private static String determinateButton(VT_D1_PatientMyStudySuppliesController.StudySupplyItem item, VTD1_Patient_Kit__c kitItem) {
        String result = '';
        if (item.delivery.VTD1_Status__c == 'Delivered'
                && (item.delivery.VTR2_Destination_Type__c == 'Site' ? item.delivery.VTR2_KitsGivenToPatient__c : true)
                && item.delivery.VTD1_ExpectedDeliveryDateTime__c == null) {
            result += 'Confirm delivery';
        } else if (item.delivery.VTD1_Status__c == 'Received' || item.confirmPackagingMaterialsInHand) {
            if (item.confirmPackagingMaterialsInHand) {
                result += 'Confirm Packaging Materials In Hand';
            } else if (kitItem != null && kitItem.IMP_Replacement_Forms__r.isEmpty()) {
                result += 'Request Replacement';
            }
        } else if (item.confirmCourierPickup) {
            result += 'Confirm Pickup';
        } else if (item.confirmDropOff) {
            result += 'Confirm DropOff';
        }
        return result;
    }
    private static Boolean checkIfButtonNeedForDelivery(VT_D1_PatientMyStudySuppliesController.StudySupplyItem item) {
        return ((item.delivery.VTD1_Status__c == 'Delivered' && item.delivery.VTD1_ExpectedDeliveryDateTime__c == null)
                || (item.delivery.VTD1_Status__c == 'Pending Shipment' && !formUpdated && item.delivery.VTR2_Destination_Type__c != 'Site')
                || item.delivery.VTD1_Status__c == 'Received'
                || item.confirmPackagingMaterialsInHand
                || !item.confirmCourierPickup
                || !item.confirmDropOff);
    }
    public class PatientMobileStudySupply {
        public Boolean isDelivery;
        public String deliveryId;
        public String kitId;
        public String title;
        public String description;
        public String kitType;
        public String status;
        public Boolean recalled;
        public String button;
        public String buttonLabel;
        public Boolean updateNeed;
        public Long dateLastModified;
        public Boolean isPackageDelivery;
        public String shipmentType;
        public String courierName;
        public String trackingId;
        public String shipDate;
        public String estDeliveryDate;
        public Long expectedDeliveryDate;
        public PatientMobileStudySupply(
                Boolean isDelivery,
                String delId,
                String kitId,
                String kitType,
                String title,
                String descriptionString,
                String status,
                Datetime dateLastModified,
                Boolean recalled,
                Boolean isPackage,
                String button,
                Boolean updateNeed,
                String buttonLabel,
                String shipmentType,
                String courierName,
                String trackingId,
                String shipDate,
                String estDeliveryDate,
                Datetime expectedDeliveryDate
        ) {
            this.isDelivery = isDelivery;
            this.deliveryId = delId;
            this.kitId = kitId == null ? '' : kitId;
            this.kitType = kitType == null ? '' : kitType;
            this.title = title == null ? '' : title;
            this.description = descriptionString == null ? '' : descriptionString;
            this.status = status == null ? '' : status;
            this.dateLastModified = dateLastModified.getTime() + timeOffset;
            this.recalled = recalled;
            this.isPackageDelivery = isPackage;
            this.button = button;
            this.updateNeed = updateNeed;
            this.buttonLabel = buttonLabel;
            this.shipmentType = shipmentType == null ? '' : shipmentType;
            this.courierName = courierName == null ? '' : courierName;
            this.trackingId = trackingId == null ? '' : trackingId;
            this.shipDate = shipDate == null ? '' : shipDate;
            this.estDeliveryDate = estDeliveryDate == null ? '' : estDeliveryDate;
            this.expectedDeliveryDate = expectedDeliveryDate == null ? null : expectedDeliveryDate.getTime();
        }
    }
    public class PatientStudySuppliesResponse {
        private List<Object> items;
        private List<VT_D1_PatientMyStudySuppliesController.Option> deliveryStatuses;
        private List<VT_D1_PatientMyStudySuppliesController.Option> replacementReasons;
        private List<VT_D1_PatientMyStudySuppliesController.Option> kitTypes;
    }
}