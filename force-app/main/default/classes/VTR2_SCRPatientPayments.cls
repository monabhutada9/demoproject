public with sharing class VTR2_SCRPatientPayments {

    public class PatientPaymentsWrapper {
        @AuraEnabled public List<VTD1_Patient_Payment__c> paymentsList { get; set; }
        @AuraEnabled public Integer totalPages { get; set; }
        @AuraEnabled public Decimal totalPaymentIssued { get; set; }
    }

    @AuraEnabled
    public static PatientPaymentsWrapper getPatientPayments(Id caseId, String querySettingsString) {
        PatientPaymentsWrapper ppWrapper = new PatientPaymentsWrapper();
        try {
            VT_TableHelper.TableQuerySettings querySettings = VT_TableHelper.parseQuerySettings(querySettingsString);

            ppWrapper.paymentsList = getPaymentsList(caseId, querySettings);
            ppWrapper.totalPages = getTotalPages(caseId, querySettings);
            ppWrapper.totalPaymentIssued = getTotalPaymentIssued(caseId);
            return ppWrapper;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static void reviewPayment(Id paymentId, Boolean isApprove) {
        try {
            VTD1_Patient_Payment__c payment = [
                    SELECT
                            Id,
                            VTD1_Activity_Complete__c,
                            VTD1_PG_Approved__c
                    FROM VTD1_Patient_Payment__c
                    WHERE Id = :paymentId
            ];
            if (isApprove) {
                payment.VTD1_PG_Approved__c = true;
            } else {
                payment.VTD1_Activity_Complete__c = false;
            }
            update payment;

            List<Task> task = [
                    SELECT
                            Id,
                            Status
                    FROM Task
                    WHERE VTD2_Task_Unique_Code__c = '400'
                        AND WhatId = :paymentId
                        AND OwnerId = :UserInfo.getUserId()
                        AND Status = 'Open'
            ];
            if (task.size() > 0) {
                task[0].Status = 'Completed';
                update task;
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    private static List<VTD1_Patient_Payment__c> getPaymentsList(Id caseId, VT_TableHelper.TableQuerySettings querySettings) {
        String queryOffset = (querySettings.queryOffset != null && querySettings.queryOffset != '') ? ' ' + querySettings.queryOffset : '';
        String queryLimit = (querySettings.queryLimit != null && querySettings.queryLimit != '') ? ' ' + querySettings.queryLimit : '';
        String query = 'SELECT Id, Name, VTD1_Amount__c, VTD1_Amount_Formula__c, VTD1_Payment_Issued__c, VTD1_PG_Approved__c, VTD1_Status__c,' +
                ' VTD1_Patient_Name__c, VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTD1_Reimbursement_Threshold__c,' +
                ' VTD1_Clinical_Study_Membership__r.VTD1_Study__r.Name, VTD1_Activity_Complete__c, VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c, RecordType.Name' +
                ' FROM VTD1_Patient_Payment__c WHERE VTD1_Clinical_Study_Membership__c = :caseId ORDER BY Name ASC' + queryLimit + queryOffset;
        return (List<VTD1_Patient_Payment__c>) Database.query(query);
    }

    private static Integer getTotalPages(Id caseId, VT_TableHelper.TableQuerySettings querySettings) {
        String queryFilter = (querySettings.queryFilter != null && querySettings.queryFilter != '') ? ' AND ' + querySettings.queryFilter : '';
        String query = 'SELECT Count(Id) FROM VTD1_Patient_Payment__c  WHERE VTD1_Clinical_Study_Membership__c = :caseId ' + queryFilter;

        AggregateResult[] ar = Database.query(query);
        Integer totalPages = (Integer) Math.ceil((Decimal) ar[0].get('expr0') / querySettings.entriesOnPage);
        return totalPages;
    }

    private static Decimal getTotalPaymentIssued(Id caseId) {
        AggregateResult[] ar = [
                SELECT SUM(VTD1_Amount_Formula__c)
                FROM VTD1_Patient_Payment__c
                WHERE VTD1_Clinical_Study_Membership__c = :caseId
                AND VTD1_Payment_Issued__c = TRUE
        ];
        if (ar[0].get('expr0') != null) {
            return ((Decimal) ar[0].get('expr0'));
        } else {
            return 0.00;
        }
    }

}