@IsTest
public with sharing class VT_R2_DocuSignRecipientStatusHandlerTest {

    private static final String testEmail = 't@t.com';
    private static final String RECIPIENT_ID = '1DCE2B24-B6D3-4867-930C-AD35F924EF91';

    public static void testTaskAndNotificationCreation() {
        if (!isHandlerActive()) {
            return;
        }
        createTnCatalogRecords();
        DomainObjects.VTD1_Document_t document = new DomainObjects.VTD1_Document_t().setDocumentType(VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_DOA_LOG);
        document.persist();
        User user = (User) new DomainObjects.User_t()
                .setEmail(testEmail)
                .persist();
        Test.startTest();
        new DomainObjects.dsfs_DocuSign_Recipient_Status_t()
                .addDsfs_Parent_Status_Record(new DomainObjects.dsfs_DocuSign_Status_t())
                .setDsfs_DocuSign_Recipient_Id(RECIPIENT_ID)
                .setDsfs_DocuSign_Recipient_Email(testEmail)
                .setVTR2_Document(document.id)
                .persist();
        Test.stopTest();

        System.assertEquals(1, [SELECT count() FROM Task WHERE OwnerId = :user.Id]);
        System.assertEquals(1, [SELECT count() FROM VTD1_NotificationC__c WHERE VTD1_Receivers__c = :user.Id]);
    }

    public static void testClosingTask() {
        if (!isHandlerActive()) {
            return;
        }
        createTnCatalogRecords();
        DomainObjects.VTD1_Document_t document = new DomainObjects.VTD1_Document_t().setDocumentType(VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_DOA_LOG);
        document.persist();
        User user = (User) new DomainObjects.User_t()
                .setEmail(testEmail)
                .persist();
        dsfs__DocuSign_Recipient_Status__c docuSignRecipientStatus = (dsfs__DocuSign_Recipient_Status__c) new DomainObjects.dsfs_DocuSign_Recipient_Status_t()
                .addDsfs_Parent_Status_Record(new DomainObjects.dsfs_DocuSign_Status_t())
                .setDsfs_DocuSign_Recipient_Email(testEmail)
                .setVTR2_Document(document.id)
                .setDsfs_DocuSign_Recipient_Id(user.Id)
                .persist();
        Test.startTest();
        docuSignRecipientStatus.dsfs__Recipient_Status__c = VT_R2_DocuSignRecipientStatusHandler.COMPLETED_STATUS;
        update docuSignRecipientStatus;
        Test.stopTest();

        System.assertEquals(VT_R2_DocuSignRecipientStatusHandler.COMPLETED_STATUS, [SELECT Status FROM Task][0].Status);
    }

    public static void coverageForTriggerWhenHandlerIsDisabled() {
        new DomainObjects.dsfs_DocuSign_Recipient_Status_t()
                .addDsfs_Parent_Status_Record(new DomainObjects.dsfs_DocuSign_Status_t())
                .setDsfs_DocuSign_Recipient_Id(RECIPIENT_ID)
                .persist();
    }

    private static void createTnCatalogRecords() {
        new DomainObjects.VTD2_TN_Catalog_Code_t()
                .setRecordType('Notification')
                .setVTD2_T_Task_Unique_Code(VT_R2_DocuSignRecipientStatusHandler.TN_CATALOG_CODE_FOR_NOTIFICATION)
                .setVTD2_T_Receiver('Task.OwnerId')
                .setVTD2_T_SObject_Name('Task')
                .persist();
        new DomainObjects.VTD2_TN_Catalog_Code_t()
                .setRecordType('Task')
                .setVTD2_T_Task_Unique_Code(VT_R2_DocuSignRecipientStatusHandler.TN_CATALOG_CODE_FOR_TASK)
                .setVTD2_T_Assigned_To_ID('VTD1_Document__c.VTD1_PG_Approver__c')
                .setVTD2_T_Document('VTD1_Document__c.Id')
                .setVTD2_T_Due_Date('today()')
                .setVTD2_T_Related_To_Id('VTD1_Document__c.Id')
                .setVTD2_T_SObject_Name('VTD1_Document__c')
                .persist();
    }

    private static Boolean isHandlerActive() {
        if(HandlerExecutionPool.getInstance()
                .getHandler(dsfs__DocuSign_Recipient_Status__c.class)
                .isHandlerActive(VT_R2_DocuSignRecipientStatusHandler.class)) {
            return true;
        }
        System.debug(LoggingLevel.WARN, VT_R2_DocuSignRecipientStatusHandler.class.getName()
                + ' is inactive. The test VT_R2_DocuSignRecipientStatusHandlerTest will be ignored.');
        return false;
    }
}