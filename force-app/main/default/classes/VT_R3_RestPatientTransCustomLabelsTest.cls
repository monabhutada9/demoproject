/**
 * Created by Rishat Shamratov on 04.10.2019.
 */

@IsTest
public with sharing class VT_R3_RestPatientTransCustomLabelsTest {

    public static void testGetTranslatedCustomLabels() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/Patient/CustomLabels/Translate';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        req.params.put('lang', 'en_US');
        req.params.put('masterLabel', 'PatientCommunity');
        String labelsApiNamesAndTranslatedValuesString = VT_R3_RestPatientTranslatedCustomLabels.getTranslatedCustomLabels();
        Map<String, Object> labelsApiNamesAndTranslatedValuesMap;

        labelsApiNamesAndTranslatedValuesMap = (Map<String, Object>) JSON.deserializeUntyped(labelsApiNamesAndTranslatedValuesString);

        String labelApiNameAndTranslatedValueString = String.valueOf(labelsApiNamesAndTranslatedValuesMap.get('serviceResponse'));
        labelApiNameAndTranslatedValueString = labelApiNameAndTranslatedValueString.replaceAll('^\\W', '').replaceAll('\\W$', '');
        List<String> ls = labelApiNameAndTranslatedValueString.split('=');

        System.assertEquals('Change Password', ls.get(1));

        req.params.put('lang', 'invalidLang');
        req.params.put('masterLabel', 'PatientCommunity');
        String labelsApiNamesAndTranslatedValuesWrongLangString = VT_R3_RestPatientTranslatedCustomLabels.getTranslatedCustomLabels();
        Map<String, Object> labelsApiNamesAndTranslatedValuesWrongLangMap;

        labelsApiNamesAndTranslatedValuesWrongLangMap = (Map<String, Object>) JSON.deserializeUntyped(labelsApiNamesAndTranslatedValuesWrongLangString);
        String labelApiNameAndTranslatedValueWrongLangString = String.valueOf(labelsApiNamesAndTranslatedValuesWrongLangMap.get('serviceResponse'));
        labelApiNameAndTranslatedValueWrongLangString = labelApiNameAndTranslatedValueWrongLangString.replaceAll('^\\W', '').replaceAll('\\W$', '');

        List<String> lsWrongLang = labelApiNameAndTranslatedValueWrongLangString.split(':');

        System.assertEquals('Please, specify one of the valid languages', lsWrongLang.get(0));

        req.params.put('lang', 'en_US');
        req.params.put('masterLabel', 'Comm');
        String labelsApiNamesAndTranslatedValuesWrongMLString = VT_R3_RestPatientTranslatedCustomLabels.getTranslatedCustomLabels();
        Map<String, Object> labelsApiNamesAndTranslatedValuesWrongMLMap;

        labelsApiNamesAndTranslatedValuesWrongMLMap = (Map<String, Object>) JSON.deserializeUntyped(labelsApiNamesAndTranslatedValuesWrongMLString);
        String labelApiNameAndTranslatedValueWrongMLString = String.valueOf(labelsApiNamesAndTranslatedValuesWrongMLMap.get('serviceResponse'));
        labelApiNameAndTranslatedValueWrongMLString = labelApiNameAndTranslatedValueWrongMLString.replaceAll('^\\W', '').replaceAll('\\W$', '');

        List<String> lsWrongML = labelApiNameAndTranslatedValueWrongMLString.split(':');

        System.assertEquals('Please, specify one of the valid master labels', lsWrongML.get(0));

        req.params.put('lang', 'invalidLang');
        req.params.put('masterLabel', 'Comm');
        String labelsApiNamesAndTranslatedValuesWrongLangAndMLString = VT_R3_RestPatientTranslatedCustomLabels.getTranslatedCustomLabels();
        Map<String, Object> labelsApiNamesAndTranslatedValuesWrongLangAndMLMap;

        labelsApiNamesAndTranslatedValuesWrongLangAndMLMap = (Map<String, Object>) JSON.deserializeUntyped(labelsApiNamesAndTranslatedValuesWrongLangAndMLString);
        String labelApiNameAndTranslatedValueWrongLangAndMLString = String.valueOf(labelsApiNamesAndTranslatedValuesWrongLangAndMLMap.get('serviceResponse'));
        labelApiNameAndTranslatedValueWrongLangAndMLString = labelApiNameAndTranslatedValueWrongLangAndMLString.replaceAll('}', '}:,');
        List<String> lsWrongLangAndML = labelApiNameAndTranslatedValueWrongLangAndMLString.split(':');

        System.assertEquals('Available languages, Available master labels', lsWrongLangAndML.get(0) + lsWrongLangAndML.get(2));

    }

    public static void testGetCustomLabelsByMasterLabel() {
        HttpCalloutMock cMock = new CalloutMock();
        Test.setMock(HttpCalloutMock.class, cMock);
        Test.startTest();
        VT_R3_CustomLabelsRenderPageController.getCustomLabelsByMasterLabel();
        Test.stopTest();
    }

    public with sharing class CalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');
            res.setBody('{"size":37,"totalSize":37,"done":true,"queryLocator":null,"entityTypeName":"ExternalString","records":[{"attributes": {"type": "ExternalString","url": "/services/data/v46.0/tooling/sobjects/ExternalString/101g0000001uhnMAAQ"},"Id": "101g0000001uhnMAAQ","MasterLabel": "PatientCommunity Common","Name": "c","Value": "Change Password"}]}');
            return res;
        }
    }
}