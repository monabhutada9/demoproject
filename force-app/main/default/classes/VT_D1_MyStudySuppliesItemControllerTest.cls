@IsTest
private class VT_D1_MyStudySuppliesItemControllerTest {
 	private static Case cas;
 	private static User u;

 	@TestSetup
 	static void setup() {

		Test.startTest();
		HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
		VT_D1_TestUtils.createProtocolDelivery(study.Id);
		//HealthCloudGA__CandidatePatient__c candidatePatient = VT_D1_TestUtils.createPatientCandidate('Mike', 'Birn', uniqueUserName, uniqueUserName, 'mikebirn', 'Converted', study.Id, 'PN', '10203040509876', 'US', 'NY');
		VT_D1_TestUtils.createTestPatientCandidate(1);
		Test.stopTest();

		List<Case> casesList = [SELECT VTD1_Patient_User__c, VTD1_Patient__r.VTD1_Patient_Name__c, VTD1_Study__c
		FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
		if(casesList.size() > 0){
			cas = casesList.get(0);
			VT_R4_GenerateActualVisitsAndKits generator = new VT_R4_GenerateActualVisitsAndKits();
			generator.executeDeliveriesAndKits(new List<Id> {cas.Id});
		}
	}

 	static {
    	List<Case> casesList = [SELECT VTD1_Patient_User__c FROM Case];
	    if(casesList.size() > 0){
	        cas = casesList.get(0);
	        String userId = cas.VTD1_Patient_User__c;
	        u = [SELECT Id, ContactId FROM User WHERE Id =: userId];
	    }
	}

 	@IsTest
    static void confReqReplDeliveryTest(){
		System.assertNotEquals(null, cas);
		System.assertNotEquals(null, u);

		List<VTD1_Patient_Kit__c> kits = [SELECT Id, VTD1_Patient_Delivery__c FROM VTD1_Patient_Kit__c WHERE VTD1_Kit_Type__c = 'IMP'
		AND VTD1_Patient_Delivery__c IN (
				SELECT Id
				FROM VTD1_Order__c
				WHERE VTD1_Case__c = :cas.Id
		) LIMIT 1];
		System.assert(!kits.isEmpty(), 'Kit not found');

		Test.startTest();
		System.runAs(u){
			VT_D1_MyStudySuppliesItemController.confReqReplDelivery(cas.Id, kits[0].Id, 'kitName',
					'patientName', true, 'Damaged', 'requestReplacementComments');
			VT_D1_MyStudySuppliesItemController.confReqReplDelivery(cas.Id, kits[0].VTD1_Patient_Delivery__c, 'deliveryName',
					'patientName', true, 'Damaged', 'requestReplacementComments');

			List<IMP_Replacement_Form__c> replacementFormList = [SELECT Name, VTR2_Replacement_Form_Name__c, VTD1_Replacement_Reason__c, VTD1_Comments__c
																FROM IMP_Replacement_Form__c WHERE VTD1_Case__c = :cas.Id
																AND (VTD1_Replace__c!=NULL OR VTR2_ReplacePackagingMaterials__c!=NULL)];
			System.assertEquals(2, replacementFormList.size());
			for (IMP_Replacement_Form__c replacementForm : replacementFormList) {
				System.assertNotEquals(null, replacementForm.VTR2_Replacement_Form_Name__c);
				System.assertEquals('Damaged', replacementForm.VTD1_Replacement_Reason__c);
				System.assertEquals('requestReplacementComments', replacementForm.VTD1_Comments__c);
			}
		}
		Test.stopTest();
  	}

    @IsTest
    static void confPackMatHandTest() {
		System.assertNotEquals(null, cas);
		System.assertNotEquals(null, u);

		List<VTD1_Order__c> deliveryList = [SELECT Id FROM VTD1_Order__c WHERE VTD1_Case__c =: cas.Id];
		System.assert(deliveryList.size() > 4);

		VTD1_Order__c delivery = deliveryList.get(0);

		Task task = new Task();
		task.OwnerId = cas.VTD1_Patient_User__c;
		task.VTD1_Type_for_backend_logic__c = 'Confirm Packaging Materials in Hand';
		task.Status = 'Open';
		task.WhatId = delivery.Id;
		Test.startTest();
		insert task;

		System.runAs(u) {
			VT_D1_MyStudySuppliesItemController.confPackMatHand(delivery.Id, true);
		}

		List<VTD1_Order__c> deliveryList1 = [SELECT Id FROM VTD1_Order__c WHERE Id =: delivery.Id AND VTD1_isPackagingMaterialsNeeded__c = TRUE];
		System.assert(deliveryList1.size() > 0);

		List<Task> taskList = [SELECT Id FROM Task WHERE Id = :task.Id AND Status = 'Completed'];
		System.assert(taskList.size() > 0);

		System.runAs(u) {
			VT_D1_MyStudySuppliesItemController.confPackMatHand(delivery.Id, false);
		}

		List<VTD1_Order__c> deliveryList2 = [SELECT Id FROM VTD1_Order__c WHERE Id =: delivery.Id AND VTD1_isPackagingMaterialsNeeded__c = FALSE AND VTD1_PatientHasPackagingMaterials__c = true];
		System.assert(deliveryList2.size() > 0);
		Test.stopTest();
	}

	@IsTest
	static void confPickupTest() {
		System.assertNotEquals(null, cas);
		System.assertNotEquals(null, u);

		List<VTD1_Order__c> deliveryList = [SELECT Id FROM VTD1_Order__c WHERE VTD1_Case__c =: cas.Id];
		System.assert(deliveryList.size() > 4);

		VTD1_Order__c delivery = deliveryList.get(0);
		Test.startTest();

		Task task = new Task();
		task.OwnerId = cas.VTD1_Patient_User__c;
		task.VTD1_Type_for_backend_logic__c = 'Confirm Courier Pickup';
		task.Status = 'Open';
		task.WhatId = delivery.Id;
		insert task;

		Datetime dt = Datetime.now();
		System.runAs(u) {
			VT_D1_MyStudySuppliesItemController.confPickup(delivery.Id, dt);
		}

		List<VTD1_Order__c> deliveryList1 = [SELECT Id FROM VTD1_Order__c WHERE Id =: delivery.Id AND VTD1_CourierPickupDateTime__c =: dt];
		System.assert(deliveryList1.size() > 0);

		List<Task> taskList = [SELECT Id FROM Task WHERE Id = :task.Id AND Status = 'Completed'];
		System.assert(taskList.size() > 0);
		Test.stopTest();
	}

	@IsTest
	static void confDropOffTest() {
		System.assertNotEquals(null, cas);
		System.assertNotEquals(null, u);
		Test.startTest();

		List<VTD1_Order__c> deliveryList = [SELECT Id FROM VTD1_Order__c WHERE VTD1_Case__c =: cas.Id];
		System.assert(deliveryList.size() > 4);

		VTD1_Order__c delivery = deliveryList.get(0);

		Task task = new Task();
		task.OwnerId = cas.VTD1_Patient_User__c;
		task.VTD1_Type_for_backend_logic__c = 'Confirm Drop Off of Study Medication';
		task.Status = 'Open';
		task.WhatId = delivery.Id;
		insert task;

		Datetime dt = Datetime.now();
		System.runAs(u) {
			VT_D1_MyStudySuppliesItemController.confDropOff(delivery.Id, dt);
		}

		List<VTD1_Order__c> deliveryList1 = [SELECT Id FROM VTD1_Order__c WHERE Id =: delivery.Id AND VTD1_Drop_Off_IMP_DateTime__c =: dt];
		System.assert(deliveryList1.size() > 0);

		List<Task> taskList = [SELECT Id FROM Task WHERE Id = :task.Id AND Status = 'Completed'];
		System.assert(taskList.size() > 0);
		Test.stopTest();
	}

}