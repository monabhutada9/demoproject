/**
 * Created by user on 05.07.2019.
 */

@IsTest
public with sharing class VT_R2_PCFCase_Test {
    @testSetup
    private static void setupMethod() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
        Id tnTaskRecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Task').getRecordTypeId();
        Id tnNotificationRecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Notification').getRecordTypeId();
        List <VTD2_TN_Catalog_Code__c> tnCatalogCodes = new List<VTD2_TN_Catalog_Code__c>();
        VTD2_TN_Catalog_Code__c tnCatalogCode = new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = '219',
                VTD2_T_Category__c = 'Safety Related',
                VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD1_RTId__c.VTD1_Task_PCF_Task__c',
                VTD2_T_Due_Date__c = 'IF(Case.IsEscalated = TRUE AND NOT(Case.IsEscalated = FALSE) AND CaseId != null AND 2 > 1, NOW+2h, NOW+1d)',
                VTD2_T_My_Task_List_Redirect__c = '"contact-form?pcfId=" & Case.Id & "%amp%caseId=" & Case.VTD1_Clinical_Study_Membership__c',
                VTD2_T_Used_In__c = 'PB: PCF owner change notification',
                VTD2_T_SObject_Name__c = 'Case',
                VTD2_T_Reminder_Date_Time__c = 'TODAY()',
                VTD2_T_Reminder_Set__c = true,
                VTD2_T_Priority__c = 'Normal',
                RecordTypeId = tnTaskRecordTypeId,
                VTD2_T_Type__c = 'PCF(Possible)',
                VTD2_T_Patient__c = 'Case.VTD1_Clinical_Study_Membership__c',
                VTD2_T_Assigned_To_ID__c = 'Case.OwnerId',
                VTD2_T_Care_Plan_Template__c = 'Case.VTD1_Clinical_Study_Membership__r.VTD1_Study__c',
                VTD2_T_Related_To_Id__c = 'Case.Id',
                VTD2_T_Subject__c = '"Review and assess " &Case.CaseNumber & ", a potential AE/SAE for " & FORMAT(Case.CreatedDate-2d, \'EEE, MMM d-yyyy\') & FORMAT(Case.CreatedDate-2m, \'EEE, MMM d-yyyy\') & FORMAT(Case.CreatedDate-2h, \'EEE, MMM d-yyyy\') & LEFT(test, 3)'
        );
        tnCatalogCodes.add(tnCatalogCode);

        tnCatalogCode = new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'N066',
                VTD2_T_Receiver__c = 'Case.OwnerId',
                VTD2_T_Has_direct_link__c = true,
                VTD2_T_Link_to_related_event_or_object__c = '"contact-form?pcfId=" & Case.Id & "%amp%caseId=" & Case.VTD1_Clinical_Study_Membership__c',
                VTD2_T_Type__c = 'General',
                VTD2_T_Title__c = 'Safety',
                VTD2_T_SObject_Name__c = 'Case',
                VTD2_T_Used_In__c = 'Flow: PCF Safety indicator Possible- NotificationC (launched by PB: PCF owner change notification)-for SCR',
                VTD2_T_Message__c = '"Review and assess " & Case.CaseNumber & ", a potential AE/SAE for " & Case.VTD1_Clinical_Study_Membership__r.VTR2_PTNameSubId__c  & FORMAT(Case.CreatedDate-2y, \'EEE, MMM d-yyyy\') & FORMAT(Case.CreatedDate-2M, \'EEE, MMM d-yyyy\') & FORMAT(Case.CreatedDate-2s, \'EEE, MMM d-yyyy\')',
                VTD2_T_Priority__c = 'Normal',
                RecordTypeId = tnNotificationRecordTypeId,
                VTR2_T_CSM__c = 'Case.VTD1_Clinical_Study_Membership__c'
        );
        tnCatalogCodes.add(tnCatalogCode);

        tnCatalogCode = new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'N070',
                VTD2_T_Receiver__c = 'Case.OwnerId',
                VTD2_T_Has_direct_link__c = true,
                VTD2_T_Link_to_related_event_or_object__c = '"contact-form?pcfId=" & Case.Id & "%amp%caseId=" & Case.VTD1_Clinical_Study_Membership__c',
                VTD2_T_Type__c = 'General',
                VTD2_T_Title__c = 'Safety',
                VTD2_T_SObject_Name__c = 'Case',
                VTD2_T_Used_In__c = 'Flow: PCF Safety indicator Possible- NotificationC (launched by PB: PCF owner change notification)-for SCR',
                VTD2_T_Message__c = '"Review and assess " & Case.CaseNumber & ", a potential AE/SAE for " & Case.VTD1_Clinical_Study_Membership__r.VTR2_PTNameSubId__c',
                VTD2_T_Priority__c = 'Normal',
                RecordTypeId = tnNotificationRecordTypeId,
                VTR2_T_CSM__c = 'Case.VTD1_Clinical_Study_Membership__c'
        );
        tnCatalogCodes.add(tnCatalogCode);

        insert tnCatalogCodes;

        Case caseCarePlan = [select Id from Case];
        List <User> users = [select Id, Name from User where FirstName in ('StudyConciergeUser1', 'PIUser1', 'SiteCoordinator1') order by FirstName];
        Case cas = VT_D1_TestUtils.createCase('VTD1_PCF', null, null, null, null, null, null);
        cas.VTD1_Study__c = study.Id;
        cas.VTD1_Clinical_Study_Membership__c = caseCarePlan.Id;
        cas.OwnerId = users[2].Id;
        cas.VTD1_PCF_Safety_Concern_Indicator__c = 'Possible';
        insert cas;
    }
    //COPADO_TEST_RESOLVE
    @isTest
    private static void testReassign() {
        VT_R3_GlobalSharing.disableForTest = true;
        List <User> users = [select Id, Name from User where FirstName in ('StudyConciergeUser1', 'PIUser1', 'SiteCoordinator1') order by FirstName];
        Case cas = [select Id, OwnerId, Status, VTD2_Reason_Safety_Concern_Indicator_Cha__c, VTD1_PCF_Safety_Concern_Indicator__c from Case where RecordType.DeveloperName = 'VTD1_PCF'];

        Test.startTest();

        cas.OwnerId = users[1].Id;
        cas.Status = 'Open';
        cas.VTD2_Reason_Safety_Concern_Indicator_Cha__c = 'Some comments';
        update cas;

        cas.OwnerId = users[0].Id;
        update cas;

        Test.stopTest();

        List <Task> tasks = [select Id, OwnerId, Status from Task];
        //System.debug('tasks = ' + tasks);
        cas.VTD1_PCF_Safety_Concern_Indicator__c = 'No';
        //cas.IsEscalated = true;
        update cas;
        tasks = [select Id, OwnerId, Status from Task];
        //System.debug('tasks = ' + tasks);

        List <VTD1_NotificationC__c> notifications = [select Id, VTD1_Receivers__c, OwnerId from VTD1_NotificationC__c];
        //System.debug('notifications = ' + notifications);
        //System.debug('end of testReassign');
    }

    @isTest
    private static void testReassign1() {
        VTD2_TN_Catalog_Code__c tnCatalogCode = [
                SELECT Id, VTD2_T_Assigned_To_ID__c
                FROM VTD2_TN_Catalog_Code__c
                WHERE VTD2_T_Task_Unique_Code__c = '219'
                LIMIT 1
        ];
        tnCatalogCode.VTD2_T_Assigned_To_ID__c = 'Case.Owner.Id';
        update tnCatalogCode;
        testReassign();
    }

    @isTest
    private static void testReassign2() {
        VTD2_TN_Catalog_Code__c tnCatalogCode = [
                SELECT Id, VTD2_T_Assigned_To_ID__c
                FROM VTD2_TN_Catalog_Code__c
                WHERE VTD2_T_Task_Unique_Code__c = '219'
                LIMIT 1
        ];
        tnCatalogCode.VTD2_T_Assigned_To_ID__c = 'Case.VTD1_PI_user__c';
        update tnCatalogCode;
        testReassign();
    }
}