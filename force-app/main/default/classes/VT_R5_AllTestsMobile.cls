@IsTest
public without sharing class VT_R5_AllTestsMobile {

    public static final String USERNAME = 'VT_R5_AllTestsMobilePatient@test.com';
    public static final String PT_USERNAME = 'VT_R5_AllTestsMobilePatient@test.com';
    public static final String CG_USERNAME = 'VT_R5_AllTestsMobileCG@test.com';
    public static final String PI_USERNAME = 'VT_R5_AllTestsMobilePI@test.com';

    @TestSetup
    static void setup() {
        VT_R3_GlobalSharing.disableForTest = true;

        DomainObjects.Account_t firstPatientAccountT = new DomainObjects.Account_t()
                .setRecordTypeByName('Patient');
        firstPatientAccountT.persist();

        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'))
                .setOriginalName('Study Name')
                .setStudyLogo('<img alt="Study Test Logotype" src="https://nonenone--c.eu31.content.force.com/servlet/servlet.ImageServer?id=0155I000000HjU7&amp;oid=00D1N000001vfO7&amp;lastMod=1592555893000"></img><img alt="Study Test Logotype" src="https://nonenone--c.eu31.content.force.com/servlet/rtaImage?eid=a095I000000eKC7&amp;feoid=00N1N00000PoibO&amp;refid=0EM5I0000009WHJ" style="height: 500px; width: 500px;"></img>')
                .setStudyAdminId(UserInfo.getUserId())
                .setVTD1_SC_Tasks_Queue_Id(UserInfo.getUserId())
                .setEdiaryTool('ECoa');

        DomainObjects.Contact_t firstPatientContact = new DomainObjects.Contact_t()
                .setRecordTypeName('Patient')
                .setAccountId(firstPatientAccountT.id)
                .setLastName('ln')
                .setFirstName('fn')
                .setBirthdate(System.now().addYears(-30).date())
                .setVTR2_Primary_Language('en_US')
                .setVT_R5_Tertiary_Preferred_Language('de')
                .setVT_R5_Secondary_Preferred_Language('it');


        DomainObjects.Address_t firstAddress = new DomainObjects.Address_t()
                .addContact(firstPatientContact)
                .setPrimary(true)
                .setCity('NY')
                .setCountry('US');

        DomainObjects.User_t firstPatientUser = new DomainObjects.User_t()
                .addContact(firstPatientContact)
                .setProfile('Patient')
                .setUsername(USERNAME)
                .setVTR3_UnreadFeeds(fflib_IDGenerator.generate(FeedItem.SObjectType));

        DomainObjects.Contact_t firstCGContact = new DomainObjects.Contact_t()
                .setRecordTypeName('Caregiver')
                .setAccountId(firstPatientAccountT.id)
                .setLastName('lnCG')
                .setFirstName('fnCG')
                .setEmail(DomainObjects.RANDOM.getEmail())
                .setVTD1_RelationshiptoPatient('Parent')
                .setPhone('+1(111)11-11-11')
                .setBirthdate(System.now().addYears(-30).date());

        DomainObjects.User_t firstCGUser = new DomainObjects.User_t()
                .addContact(firstCGContact)
                .setProfile('Caregiver')
                .setUsername('VT_R5_AllTestsMobileCG@test.com');

        new DomainObjects.Phone_t(firstPatientAccountT, String.valueOf(DomainObjects.RANDOM.getInteger(10000000, 99999999)))
                .setType('Mobile')
                .setPrimaryForPhone(true)
                .addContact(firstPatientContact);

        new DomainObjects.Phone_t(firstPatientAccountT, String.valueOf(DomainObjects.RANDOM.getInteger(10000000, 99999999)))
                .setType('Work')
                .setPrimaryForPhone(true)
                .addContact(firstCGContact);

        DomainObjects.User_t piUser = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Primary Investigator')
                .setUsername(PI_USERNAME)
                .setEmail(DomainObjects.RANDOM.getEmail());

        DomainObjects.VTR2_Study_Geography_t studyGeo = new DomainObjects.VTR2_Study_Geography_t()
                .addVTD2_Study(study)
                .setGeographicalRegion('Country')
                .setCountry('US')
                .setDateOfBirthRestriction('Year Only') ;

        DomainObjects.VirtualSite_t virtualSite = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .addStudyGeography(studyGeo)
                .setStudySiteNumber('12345');

        DomainObjects.VTR2_StudyPhoneNumber_t studyPhone = new DomainObjects.VTR2_StudyPhoneNumber_t()
                .setName('1111111111')
                .addVTR2_Study_Geography(studyGeo);

        DomainObjects.Case_t patientCase = new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .setSubject('VT_R5_AllTestsMobile')
                .addPIUser(piUser)
                .addVTD1_Patient_User(firstPatientUser)
                .addStudy(study)
                .addVirtualSite(virtualSite)
                .addContact(firstPatientContact)
                .addStudyGeography(studyGeo)
                .addAccount(firstPatientAccountT)
                //.setStatus(VT_R4_ConstantsHelper_Statuses.CASE_ACTIVE_RANDOMIZED)
                .setVTD1_Subject_ID('VT_R5_AllTestsMobile-id')
                .setVTR5_Internal_Subject_Id('VT_R5_AllTestsMobile-iid')
                .addVTD1_Primary_PG(new DomainObjects.User_t())
                .addSCRUser(new DomainObjects.User_t())
                .addVTD1_PI_contact(new DomainObjects.Contact_t())

        //.addVTR2_StudyPhoneNumber(studyPhone)
//                .addVTD1_PI_contact(new DomainObjects.Contact_t())
//                .addSCRUser(
//                        new DomainObjects.User_t()
//                                .setProfile('Site Coordinator')
//                                .addContact(new DomainObjects.Contact_t())
//                )
        ;
        patientCase.persist();

        new DomainObjects.VTD1_Physician_Information_t()
                .setVTD1_Clinical_Study_Membership(patientCase.id);

        DomainObjects.VTD1_ProtocolVisit_t protocolVisit = new DomainObjects.VTD1_ProtocolVisit_t();

        DomainObjects.VTD1_Actual_Visit_t visit = new DomainObjects.VTD1_Actual_Visit_t()
                .setVTD1_Case(patientCase.id)
                .addVTD1_ProtocolVisit_t(protocolVisit)
                .setVTD1_Status('Completed')
                .setVTD1_Name('Test Visit')
                .setScheduledDateTime(System.now().addDays(-10)) ;

        new DomainObjects.Visit_Member_t()
                .addVTD1_Actual_Visit(visit)
                .addVTD1_Participant_User(firstPatientUser);
        new DomainObjects.Visit_Member_t()
                .addVTD1_Actual_Visit(visit)
                .setVTD1_External_Participant_Type('HHN');
        new DomainObjects.Visit_Member_t()
                .addVTD1_Actual_Visit(visit)
                .addVTD1_Participant_User(firstCGUser);
        new DomainObjects.Visit_Member_t()
                .addVTD1_Actual_Visit(visit)
                .addVTD1_Participant_User(piUser);

        DomainObjects.VTD1_Actual_Visit_t visit2 = new DomainObjects.VTD1_Actual_Visit_t()
                .setVTD1_Case(patientCase.id)
                .setVTD1_Status('To Be Scheduled')
                .setRecordTypeByName('VTD1_Unscheduled')
                .setVTD1_UnscheduledVisitType('Consent')
                .setScheduledDateTime(System.now().addDays(1))
                .setUnscheduledVisitDuration(30);

        new DomainObjects.Visit_Member_t()
                .addVTD1_Actual_Visit(visit2)
                .addVTD1_Participant_User(firstPatientUser);
        new DomainObjects.Visit_Member_t()
                .addVTD1_Actual_Visit(visit2)
                .setVTD1_External_Participant_Type('HHN');
        new DomainObjects.Visit_Member_t()
                .addVTD1_Actual_Visit(visit2)
                .addVTD1_Participant_User(firstCGUser);

        new DomainObjects.Video_Conference_t()
                .addVTD1_Actual_Visit(visit2)
                .setSessionId('sessionId');

//        DomainObjects.VTD1_Actual_Visit_t visit3 = new DomainObjects.VTD1_Actual_Visit_t()
//                .setVTD1_Case(patientCase.id)
//                .setVTD1_Status('Scheduled')
//                .setRecordTypeByName('Study_Team')
//                //.setVTD1_UnscheduledVisitType('Consent')
//                .setScheduledDateTime(System.now().addDays(1))
//                .setUnscheduledVisitDuration(30);
//
//        new DomainObjects.Visit_Member_t()
//                .addVTD1_Actual_Visit(visit3)
//                .addVTD1_Participant_User(firstPatientUser);
//        new DomainObjects.Visit_Member_t()
//                .addVTD1_Actual_Visit(visit3)
//                .setVTD1_External_Participant_Type('HHN');
//        new DomainObjects.Visit_Member_t()
//                .addVTD1_Actual_Visit(visit3)
//                .addVTD1_Participant_User(firstCGUser);

        new DomainObjects.Video_Conference_t()
                .addVTD1_Actual_Visit(visit2)
                .setSessionId('sessionId');

        new DomainObjects.VTD1_NotificationC_t()
                .setVTD1_Receivers(firstPatientUser.id)
                .addCase(patientCase)
                .setMessage('some message')
                .setVTD1_Read(false);

        DomainObjects.VTD1_Order_t order = new DomainObjects.VTD1_Order_t()
                .setVTD1_Status('Delivered')
                .setVTD1_Case(patientCase.id)
                .setVTD1_Expected_Shipment_Date(Date.today())
                .setVTD1_Delivery_Order(1)
                .setVTD1_ShipmentId('shipmentId');
        DomainObjects.VTD1_Patient_Kit_t kit = new DomainObjects.VTD1_Patient_Kit_t()
                .setVTD1_Case(patientCase.id)
                .setVTD2_MaterialId('materialId')
                .addVTD1_Patient_Delivery(order);
        new DomainObjects.VTD1_Order_t()
                .setVTD1_Status('Received')
                .setVTD1_Case(patientCase.id)
                .setVTD1_Expected_Shipment_Date(Date.today())
                .setVTD1_Delivery_Order(1)
                .setVTD1_ShipmentId('shipmentId');

        DomainObjects.VTD1_Protocol_Delivery_t pDelivery = new DomainObjects.VTD1_Protocol_Delivery_t()
                .addCare_Plan_Template(study);
        DomainObjects.VTD1_Protocol_Kit_t pKit = new DomainObjects.VTD1_Protocol_Kit_t()
                .setRecordTypeByName('VTD1_Connected_Devices')
                .addVTD1_Care_Plan_Template(study)
                .addVTD1_Protocol_Delivery(pDelivery);

        DomainObjects.Product2_t device1 = new DomainObjects.Product2_t()
                .setProductName('Device')
                .setRecordTypeByName('VTD1_Connected_Devices')
                .setVTD1_DeviceType('Smart Watch')
                .setVTD1_Manufacturer('Apple')
                .setVTR5_Image('<img alt="User-added image" src="SOME_URL/servlet/rtaImage?eid=01t2g00000072Ri&amp;feoid=00N2g000000vm1Z&amp;refid=0EM2g000000Cyn7"></img>')
                .setVTR5_IsContinous(true);
        DomainObjects.HealthCloudGA_EhrDevice_t ehrDevice = new DomainObjects.HealthCloudGA_EhrDevice_t()
                .addProduct(device1)
                .addVTD1_Care_Plan_Template(study)
                .addVTD1_Protocol_Kit(pKit)
                .setVTD1_Kit_Type('Connected Devices')
                .addMeasurementType('heart_rate');
        DomainObjects.Patient_Device_t patientDevice = new DomainObjects.Patient_Device_t()
                .addEhrDevice(ehrDevice)
                .setModelName('Test Apple Watch')
                .setCaseId(patientCase.id)
                .setRecordTypeByName('VTD1_Connected_Device')
                .setVTR5_Active(true)
                .addVTD1_Patient_Kit(kit);

        DomainObjects.VTD1_Protocol_Delivery_t pDelivery2 = new DomainObjects.VTD1_Protocol_Delivery_t()
                .addCare_Plan_Template(study);
        DomainObjects.VTD1_Protocol_Kit_t pKit2 = new DomainObjects.VTD1_Protocol_Kit_t()
                .setRecordTypeByName('VTD1_Connected_Devices')
                .addVTD1_Care_Plan_Template(study)
                .addVTD1_Protocol_Delivery(pDelivery2);

        DomainObjects.Product2_t device2 = new DomainObjects.Product2_t()
                .setProductName('Device2')
                .setRecordTypeByName('VTD1_Connected_Devices')
                .setVTD1_DeviceType('Weight Scale')
                .setVTD1_Manufacturer('BodyTrace')
                .setVTR5_Image('<img alt="User-added image" src="SOME_URL/servlet/rtaImage?eid=01t2g00000072Ri&amp;feoid=00N2g000000vm1Z&amp;refid=0EM2g000000Cyn7"></img>')
                .setVTR5_IsContinous(true);
        DomainObjects.HealthCloudGA_EhrDevice_t ehrDevice2 = new DomainObjects.HealthCloudGA_EhrDevice_t()
                .addProduct(device2)
                .addVTD1_Care_Plan_Template(study)
                .addVTD1_Protocol_Kit(pKit2)
                .setVTD1_Kit_Type('Connected Devices')
                .addMeasurementType('heart_rate');
        DomainObjects.Patient_Device_t patientDevice2 = new DomainObjects.Patient_Device_t()
                .addEhrDevice(ehrDevice2)
                .setModelName('Test Apple Watch 2')
                .setCaseId(patientCase.id)
                .setRecordTypeByName('VTD1_Connected_Device')
                .setVTR5_Active(true)
                .addVTD1_Patient_Kit(kit);

        new DomainObjects.VTR5_DeviceReadingAddInfo_t()
                .addDeviceId(patientDevice)
                .setVTR5_Reading_ID('someid12')
                .setVTR5_SecondReading_ID('someid21')
                .setVTR5_Archived(true);

        new DomainObjects.VTD1_Document_t()
                .addCase(patientCase)
                .addStudy(study)
                .setRecordTypeId(VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD)
                .setVTD1_Current_Workflow('Medical Records flow')
                .setStatus('Approved')
                .persist();


        patientDevice2.setDeviceKeyId('iqvia12345');
//        update (VTD1_Patient_Device__c) patientDevice2.toObject();

        patientDevice.setDeviceKeyId('123iqvia1234');
//        update (VTD1_Patient_Device__c) patientDevice.toObject();

        insert new VTD1_RTId__c(
                Name = 'TestRecordTypeGeneral',
                VTD1_SC_Payment__c = VT_R4_ConstantsHelper_Tasks.RECORD_TYPE_ID_SC_TASK_PAYMENT
        );

        String RECORD_TYPE_ID_PATIENT_PAYMENT_COMPENSATION = Schema.SObjectType.VTD1_Patient_Payment__c
                .getRecordTypeInfosByDeveloperName().get('VTD1_Compensation').getRecordTypeId();
        VTD1_Protocol_Payment__c protocolPayment = new VTD1_Protocol_Payment__c(VTD1_Currency__c = 'USD');
        insert protocolPayment;

        List<VTD1_Patient_Payment__c> payments = new List<VTD1_Patient_Payment__c>{
                new VTD1_Patient_Payment__c(
                        VTD1_Clinical_Study_Membership__c = patientCase.id,
                        RecordTypeId = RECORD_TYPE_ID_PATIENT_PAYMENT_COMPENSATION,
                        VTD1_Activity_Complete__c = true,
                        VTD1_Payment_Issued__c = true,
                        VTD1_Amount__c = 10,
                        VTD1_Protocol_Payment__c = protocolPayment.Id
                ),
                new VTD1_Patient_Payment__c(
                        VTD1_Clinical_Study_Membership__c = patientCase.id,
                        RecordTypeId = RECORD_TYPE_ID_PATIENT_PAYMENT_COMPENSATION,
                        VTD1_Activity_Complete__c = true,
                        VTD1_Payment_Issued__c = true,
                        VTD1_Amount__c = 30,
                        VTD1_Protocol_Payment__c = protocolPayment.Id
                )
        };
        insert payments;


        update new List<VTD1_Patient_Device__c>{
                (VTD1_Patient_Device__c) patientDevice.toObject(),
                (VTD1_Patient_Device__c) patientDevice2.toObject()
        };

        Contact patientC = (Contact) firstPatientContact.toObject();
        patientC.VTD1_Clinical_Study_Membership__c = patientCase.id;
        patientC.VTD1_UserId__c = firstPatientUser.id;
        Contact cgC = (Contact) firstCGContact.toObject();
        cgC.VTD1_Clinical_Study_Membership__c = patientCase.id;
        update new List<Contact>{
                patientC, cgC
        };

        List<UserShare> us = new List<UserShare>();
        UserShare us1 = new UserShare(UserId = firstPatientUser.id, UserOrGroupId = firstCGUser.id, RowCause = 'Manual', UserAccessLevel = 'Edit');
        UserShare us2 = new UserShare(UserId = firstCGUser.id, UserOrGroupId = firstPatientUser.id, RowCause = 'Manual', UserAccessLevel = 'Edit');
        us.add(us1);
        us.add(us2);
        insert us;

        insert new CaseShare(UserOrGroupId = firstCGUser.id, CaseId = patientCase.id, CaseAccessLevel = 'Edit');

        List <ContactShare> contactShares = new List<ContactShare>();
        contactShares.add(new ContactShare(UserOrGroupId = firstCGUser.id, ContactId = firstPatientContact.id, ContactAccessLevel = 'Edit'));
        contactShares.add(new ContactShare(UserOrGroupId = firstPatientUser.id, ContactId = firstPatientContact.id, ContactAccessLevel = 'Edit'));
        contactShares.add(new ContactShare(UserOrGroupId = firstPatientUser.id, ContactId = firstCGContact.id, ContactAccessLevel = 'Edit'));
        contactShares.add(new ContactShare(UserOrGroupId = firstCGUser.id, ContactId = firstCGContact.id, ContactAccessLevel = 'Edit'));
        insert contactShares;

        List <AccountShare> accountShares = new List<AccountShare>();
        accountShares.add(new AccountShare(UserOrGroupId = firstCGUser.id, AccountId = firstPatientAccountT.id, AccountAccessLevel = 'Edit', OpportunityAccessLevel = 'Edit', ContactAccessLevel = 'Edit', CaseAccessLevel = 'Edit'));
        accountShares.add(new AccountShare(UserOrGroupId = firstPatientUser.id, AccountId = firstPatientAccountT.id, AccountAccessLevel = 'Edit', OpportunityAccessLevel = 'Edit', ContactAccessLevel = 'Edit', CaseAccessLevel = 'Edit'));
        insert accountShares;
        System.debug('LIMIT LLL' + Limits.getDmlStatements());
        System.debug('LIMIT LLL' + Limits.getQueries());

        RestContext.request = new RestRequest();
        RestContext.response = new RestResponse();
    }

    @IsTest
    private static void VT_R3_RestPatientInitialTest() {
        VT_R3_RestPatientInitialTest.firstTest();
    }

    @IsTest
    private static void VT_R5_MobileInitialTest1() {
        VT_R5_MobileInitialTest.firstTest();
    }
    @IsTest
    private static void VT_R5_MobileInitialTest2() {
        VT_R5_MobileInitialTest.secondTest();
    }
    @IsTest
    private static void VT_R5_MobileInitialTest3() {
        VT_R5_MobileInitialTest.thirdTest();
    }

    @IsTest
    private static void VT_R3_RestPeriodicTest1() {
        VT_R3_RestPeriodicTest.firstTest();
    }

    @IsTest
    private static void VT_R3_RestPeriodicTest2() {
        VT_R3_RestPeriodicTest.secondTest();
    }

    @IsTest
    private static void VT_R3_RestPatientVideoConferenceTest() {
        VT_R3_RestPatientVideoConferenceTest.firstTest();
    }
    @IsTest
    private static void VT_R3_RestPatientVideoConferenceTest2() {
        VT_R3_RestPatientVideoConferenceTest.secondTest();
    }
    @IsTest
    private static void VT_R3_RestPatientVideoConferenceTest3() {
        VT_R3_RestPatientVideoConferenceTest.thirdTest();
    }

    /**
     * @description Homepage Tests
     **/
    //    @IsTest
//    private static void VT_R5_MobilePatientHomePageTest1() {
//        VT_R5_MobilePatientHomePageTest.startTest_1();
//    }
//
//    @IsTest
//    private static void VT_R5_MobilePatientHomePageTest2() {
//        VT_R5_MobilePatientHomePageTest.startTest_2();
//    }
//
    @IsTest
    private static void VT_R5_MobilePatientHomePageTest3() {
        VT_R5_MobilePatientHomePageTest.firstTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientHomePageTest4() {
        VT_R5_MobilePatientHomePageTest.secondTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientHomePageTest5() {
        VT_R5_MobilePatientHomePageTest.thirdTest();
    }

    /**
     * @description Calendar and Visits Service Tests
     **/
    @IsTest
    private static void VT_R5_MobilePatientVisitsTest_1() {
        VT_R5_MobilePatientVisitsTest.firstTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientVisitsTest_2() {
        VT_R5_MobilePatientVisitsTest.secondTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientVisitsTest_3() {
        VT_R5_MobilePatientVisitsTest.thirdTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientVisitsTest_4() {
        VT_R5_MobilePatientVisitsTest.forthTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientVisitsTest_5() {
        VT_R5_MobilePatientVisitsTest.fifthTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientVisitsTest_6() {
        VT_R5_MobilePatientVisitsTest.sixthTest();
    }

    @IsTest
    private static void VT_R5_MobilePatientCalendarTest_1() {
        VT_R5_MobilePatientCalendarTest.firstTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientCalendarTest_2() {
        VT_R5_MobilePatientCalendarTest.secondTest();
    }

    /**
     * @description Profile and Profile Photo Tests
     **/
    @IsTest
    private static void getProfile1() {
        VT_R3_RestPatientProfileTest.firstTest();
    }
    @IsTest
    private static void getProfile2() {
        VT_R3_RestPatientProfileTest.secondTest();
    }
    @IsTest
    private static void updatePatientProfile() {
        VT_R3_RestPatientProfileTest.thirdTest();
    }
    @IsTest
    private static void updateProfileCG() {
        VT_R3_RestPatientProfileTest.fourthTest();
    }

    @IsTest
    private static void VT_R5_MobilePatientProfileTest1() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_AllTestsMobileCG@test.com' LIMIT 1];
        System.runAs(toRun) {
            setRestContext('/mobile/v1/profile', 'GET');
            Test.startTest();
            VT_R5_MobileRouter.doGET();
            Test.stopTest();
        }
    }
    @IsTest
    private static void VT_R5_MobilePatientProfileTest2() {
        User toRun = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        System.runAs(toRun) {
            setRestContext('/mobile/v1/profile', 'GET');
            Test.startTest();
            VT_R5_MobileRouter.doGET();
            Test.stopTest();
        }
    }
    @IsTest
    private static void VT_R5_MobilePatientProfileTest3() {
        User toRun = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        System.runAs(toRun) {
            setRestContext('/mobile/v1/profile', 'POST');
            RestContext.request.requestBody = Blob.valueOf('{"shippingSection":{"communicationFromCourierText":true,"communicationFromCourierPhone":true,"communicationFromCourierEmail":true,"addressZipCode":"88888","addressType":"Home","addressState":"NY","addressLineTwo":"88888 Secret Street22245","addressLineThree":"88888 First Avenue","addressLineOne":"88888 Secret Streettt334","addressCountry":"US","addressCity":"NY"},"physicianSection":{"physicians":[{"physicianPhone":"88888","office":"88888111OFFICEasdasdssa","lastName":"WorkLast","middleName":"Middle","firstName":"Work","emailAddress":"88888a2c0e000001qn6hqaw@mail.com"}]},"phonesSection":{"receiveSMSVisitReminders":false,"receiveSMSEdiaryNotifications":false,"receiveNewMessageNotifications":false,"phones":[{"phoneType":"Work","phoneNumber":"88888","isPrimary":false}]},"personalSection":{"username":"Pat' + DomainObjects.RANDOM.getEmail() + '","secondaryPreferredLanguage":"fi","preferredContactMethod":"Email","lastName":"Frame2","language":"de","gender":"Male","tertiaryPreferredLanguage":"ru","firstName":"Cody2","emailAddress":"pat1' + DomainObjects.RANDOM.getEmail() + '","dateOfBirth":"2001-10-05"},"emergencyContactSection":{"emergencyContactPhoneType":"Home","emergencyContactPhoneNumber":"+9 (890) 998-9811","emergencyContactLastName":"Name?","emergencyContactFirstName":"Contact"},"deliveryAvailabilitySection":{"wednesdayTo":"16:00","wednesdayFrom":"08:00","tuesdayTo":"14:00","tuesdayFrom":"08:00","thursdayTo":"19:00","thursdayFrom":"09:00","saturdayTo":"21:00","saturdayFrom":"18:00","mondayTo":"12:00","mondayFrom":"07:00","fridayTo":"19:00","fridayFrom":"10:00"}}');
            Test.startTest();
            VT_R5_MobileRouter.doPOST();
            Test.stopTest();
        }
    }

    @IsTest
    private static void VT_R5_MobilePatientProfilePhotoTest1() {
        VT_R5_MobilePatientProfilePhotoTest.firstTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientProfilePhotoTest2() {
        VT_R5_MobilePatientProfilePhotoTest.secondTest();
    }

    /**
     * @description Study Documents Tests
     **/
    @IsTest
    private static void VT_R5_MobilePatientStudyDocumentsTest1() {
        VT_R5_MobilePatientStudyDocumentsTest.firstTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientStudyDocumentsTest2() {
        VT_R5_MobilePatientStudyDocumentsTest.secondTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientStudyDocumentsTest3() {
        VT_R5_MobilePatientStudyDocumentsTest.thirdTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientStudyDocumentsTest4() {
        VT_R5_MobilePatientStudyDocumentsTest.fourthTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientStudyDocumentsTest5() {
        VT_R5_MobilePatientStudyDocumentsTest.fifthTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientStudyDocumentsTest6() {
        VT_R5_MobilePatientStudyDocumentsTest.sixthTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientStudyDocumentsTest7() {
        VT_R5_MobilePatientStudyDocumentsTest.seventhTest();
    }
    @IsTest
    private static void VT_R3_RestPatientStudyDocumentsTest1() {
        VT_R3_RestPatientStudyDocumentsTest.testBehavior();
    }
    @IsTest
    private static void VT_R3_RestPatientStudyDocumentsTest2() {
        VT_R3_RestPatientStudyDocumentsTest.processDocumentTest();
    }
    @IsTest
    private static void VT_R3_RestPatientStudyDocumentsTest3() {
        VT_R3_RestPatientStudyDocumentsTest.patchDocumentTest();
    }

    /**
     * @description Study Supplies Tests
     **/
    @IsTest
    private static void VT_R5_MobilePatientStudySuppliesTest() {
        VT_R5_MobilePatientStudySuppliesTest.firstTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientStudySuppliesTest2() {
        VT_R5_MobilePatientStudySuppliesTest.secondTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientStudySuppliesTest3() {
        VT_R5_MobilePatientStudySuppliesTest.thirdTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientStudySuppliesTest4() {
        VT_R5_MobilePatientStudySuppliesTest.fourthTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientStudySuppliesTest5() {
        VT_R5_MobilePatientStudySuppliesTest.fifthTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientStudySuppliesTest6() {
        VT_R5_MobilePatientStudySuppliesTest.sixthTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientStudySuppliesTest7() {
        VT_R5_MobilePatientStudySuppliesTest.seventhTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientStudySuppliesTest8() {
        VT_R5_MobilePatientStudySuppliesTest.eighthTest();
    }
    @IsTest
    private static void VT_R3_RestPatientStudySuppliesConfTest1() {
        VT_R3_RestPatientStudySuppliesConfTest.firstTest();
    }
    @IsTest
    private static void VT_R3_RestPatientStudySuppliesConfTest2() {
        VT_R3_RestPatientStudySuppliesConfTest.secondTest();
    }

    @IsTest
    private static void VT_R3_RestPatientStudySuppliesTest1() {
        VT_R3_RestPatientStudySuppliesTest.firstTest();
    }
    @IsTest
    private static void VT_R3_RestPatientStudySuppliesTest2() {
        VT_R3_RestPatientStudySuppliesTest.secondTest();
    }

    /**
     * @description Live Chat Tests
     **/
    @IsTest
    private static void VT_R5_MobileLiveChatTest1() {
        VT_R5_MobileLiveChatTest.firstTest();
    }
    @IsTest
    private static void VT_R5_MobileLiveChatTest2() {
        VT_R5_MobileLiveChatTest.secondTest();
    }
    @IsTest
    private static void VT_R5_MobileLiveChatTest3() {
        VT_R5_MobileLiveChatTest.thirdTest();
    }

    /**
     * @description Payments Tests
     **/
    @IsTest
    private static void VT_R5_MobilePatientPaymentsTest_1() {
        VT_R5_MobilePatientPaymentsTest.firstTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientPaymentsTest_2() {
        VT_R5_MobilePatientPaymentsTest.secondTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientPaymentsTest_3() {
        VT_R5_MobilePatientPaymentsTest.thirdTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientPaymentsTest_4() {
        VT_R5_MobilePatientPaymentsTest.forthTest();
    }

    /**
     * @description LoginFlow Tests
     **/
    @IsTest
    private static void VT_R4_RestMobileLoginFlowTest1() {
        VT_R4_RestMobileLoginFlowTest.firstTest();
    }
    @IsTest
    private static void VT_R4_RestMobileLoginFlowTest2() {
        VT_R4_RestMobileLoginFlowTest.secondTest();
    }
    @IsTest
    private static void VT_R4_RestMobileLoginFlowTest3() {
        VT_R4_RestMobileLoginFlowTest.thirdTest();
    }
    @IsTest
    private static void VT_R4_RestMobileLoginFlowTest4() {
        VT_R4_RestMobileLoginFlowTest.fourthTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientVideoConferenceTest1() {
        VT_R5_MobilePatientVideoConferenceTest.firstTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientVideoConferenceTest2() {
        VT_R5_MobilePatientVideoConferenceTest.secondTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientVideoConferenceTest3() {
        VT_R5_MobilePatientVideoConferenceTest.thirdTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientPhoneSMSTest1() {
        VT_R5_MobilePatientPhoneSMSTest.firstTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientPhoneSMSTest2() {
        VT_R5_MobilePatientPhoneSMSTest.secondTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientPhoneSMSTest3() {
        VT_R5_MobilePatientPhoneSMSTest.thirdTest();
    }
    @IsTest
    private static void VT_R5_MobilePatientPhoneSMSTest4() {
        VT_R5_MobilePatientPhoneSMSTest.fourthTest();
    }

    @IsTest
    private static void VT_R3_RestPatientHomepageTest1() {
        VT_R3_RestPatientHomepageTest.firstTest();
    }
    @IsTest
    private static void VT_R3_RestPatientHomepageTest2() {
        VT_R3_RestPatientHomepageTest.secondTest();
    }
    @IsTest
    private static void VT_R3_RestPatientHomepageTest3() {
        VT_R3_RestPatientHomepageTest.thirdTest();
    }
    @IsTest
    static void VT_R3_RestVisitTimeslotsTest() {
        VT_R3_RestVisitTimeslotsTest.getTimeSlotsTest();
    }
    @IsTest
    static void VT_R5_MobileNewsFeedTest1() {
        VT_R5_MobileNewsFeedTest.firstTest();
    }
    @IsTest
    static void VT_R5_MobileNewsFeedTest2() {
        VT_R5_MobileNewsFeedTest.secondTest();
    }
    @IsTest
    static void VT_R5_MobileNewsFeedTest3() {
        VT_R5_MobileNewsFeedTest.thirdTest();
    }

    private class ProfileCalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');
            res.setBody('{"controllerValues":{},"defaultValue":{"attributes":null,"label":"English","validFor":[],"value":"en_US"},"eTag":"799a39f9755","url":"/services/data/v45.0/ui-api/object-info/Contact/picklist-values/0121N000000oCFsQAM/VTR2_Primary_Language__c","values":[{"attributes":null,"label":"English","validFor":[],"value":"en_US"},{"attributes":null,"label":"Spanish (Spain)","validFor":[],"value":"es"},{"attributes":null,"label":"Italian","validFor":[],"value":"it"},{"attributes":null,"label":"German","validFor":[],"value":"de"},{"attributes":null,"label":"French (France)","validFor":[],"value":"fr"},{"attributes":null,"label":"Dutch","validFor":[],"value":"nl_NL"},{"attributes":null,"label":"French (Canada)","validFor":[],"value":"fr_CA"},{"attributes":null,"label":"Spanish (US)","validFor":[],"value":"es_US"},{"attributes":null,"label":"Japanese","validFor":[],"value":"ja"}]}');
            return res;
        }
    }

    public static void setRestContext(String URI, String method) {
        RestContext.request = new RestRequest();
        RestContext.response = new RestResponse();
        RestContext.request.requestURI = URI;
        RestContext.request.httpMethod = method;
    }
}