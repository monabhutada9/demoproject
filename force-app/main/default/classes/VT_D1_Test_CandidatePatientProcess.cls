/**
 *	@author Jane Ivanova
 *	Test class for Candidate Patient trigger
 *
 */
 
@isTest
private class VT_D1_Test_CandidatePatientProcess {

	//Test candidate patient conversion
    static testMethod void newCandidatePatient() {
    	createStudy('Study 1');
    	
        // TO DO: implement unit test
		HealthCloudGA__CandidatePatient__c cp = new 	HealthCloudGA__CandidatePatient__c();
		
		// Mandatory fields:
		cp.rr_firstName__c = 'First';
		cp.rr_lastName__c = 'Last';
		cp.rr_Email__c = 'firstlast@test.test';
		cp.VTD1_Patient_Phone__c = '12345678900';
		cp.HealthCloudGA__SourceSystemId__c	= 'SOMESFID001';
		
		cp.VTD1_ProtocolNumber__c = 'Study 1';
    }
    
    public static Id createStudy(String name) {
    	HealthCloudGA__CarePlanTemplate__c cpt = new HealthCloudGA__CarePlanTemplate__c();
    	return cpt.Id;	
    }
}