/**
 * Created by user on 12/27/2019.
 */

public with sharing class VT_R4_PatientKitTriggerHandler {
    public static void changeIntegrationId(List<VTD1_Patient_Kit__c> newKits) {
        Set<Id> patientDeliveriesIds = new Set<Id>();
        Set<VTD1_Patient_Kit__c> patientKits = new Set<VTD1_Patient_Kit__c>();
        for (VTD1_Patient_Kit__c patientKit : newKits) {
            if (patientKit.VTD1_IntegrationId__c == null && patientKit.VTD1_Patient_Delivery__c != null) {
                patientDeliveriesIds.add(patientKit.VTD1_Patient_Delivery__c);
                patientKits.add(patientKit);
            }
        }
        if (!patientDeliveriesIds.isEmpty()) {
            Map<Id, VTD1_Order__c> ordersByIds = new Map<Id, VTD1_Order__c>([
                    SELECT VTD1_Case__r.VTD1_Subject_ID__c, RecordTypeId
                    FROM VTD1_Order__c
                    WHERE Id IN :patientDeliveriesIds
            ]);
            for (VTD1_Patient_Kit__c patientKit : patientKits) {
                VTD1_Order__c order = ordersByIds.get(patientKit.VTD1_Patient_Delivery__c);
                if (patientKit.VTD1_Patient_Delivery__r.RecordTypeId != VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_ORDER_PACKING_MATERIALS) {
                    patientKit.VTD1_IntegrationId__c = order.VTD1_Case__r.VTD1_Subject_ID__c
                            + '-' + patientKit.VTD1_Kit_Name__c;
                }
            }
        }
    }
}