/**
* @author: Carl Judge
* @date: 21-Sep-18
* @description: 
**/

public without sharing class VT_D2_VisitConfirmedTemplateController extends VT_D2_VisitEmailsAbstract {

    public Id slotId {get; set;}
    private VTD2_Time_Slot__c slot;
    
    public VTD2_Time_Slot__c getSlot() {
        if (this.slot == null) {
            this.slot = [
                SELECT Id,
                    VTD2_Timeslot_Date_Time__c,
                    VTD2_Actual_Visit__c,
                    VTD2_Actual_Visit__r.Id,
                    VTD2_Actual_Visit__r.VTD1_Case__r.Status,
                    VTD2_Actual_Visit__r.VTD1_Case__r.VTD1_PI_user__r.Title,
                    VTD2_Actual_Visit__r.VTD1_Case__r.VTD1_PI_user__r.Name,
                    VTD2_Actual_Visit__r.VTD1_Case__r.VTD1_PI_user__r.TimeZoneSidKey,
                    VTD2_Actual_Visit__r.Unscheduled_Visits__r.VTD1_PI_user__r.Title,
                    VTD2_Actual_Visit__r.Unscheduled_Visits__r.VTD1_PI_user__r.Name,
                    VTD2_Actual_Visit__r.Unscheduled_Visits__r.VTD1_PI_user__r.TimeZoneSidKey,
                    VTD2_Actual_Visit__r.VTD1_Case__r.Contact.Name,
                    VTD2_Actual_Visit__r.VTD1_Study_name_formula__c,
                    VTD2_Actual_Visit__r.VTD1_Formula_Name__c,
                    VTD2_Actual_Visit__r.VTD1_Protocol_Visit__r.VTD1_Range__c,
                    VTD2_Actual_Visit__r.VTD1_Visit_Duration__c,
                    VTD2_Actual_Visit__r.To_Be_Scheduled_Date__c,
                    VTD2_Actual_Visit__r.RecordType.DeveloperName,
                    VTD2_Actual_Visit__r.VTD2_Case_Contact_Name__c,
                    VTD2_Actual_Visit__r.VTD2_Patient_ID__c
                FROM VTD2_Time_Slot__c
                WHERE Id = :this.slotId
            ];
        }
        return this.slot;
    }

    public override VTD1_Actual_Visit__c getVisit() {
       return getSlot().VTD2_Actual_Visit__r;
    }

    public String getDayText() {
        return getDateText(getSlot().VTD2_Timeslot_Date_Time__c);
    }

    public String getTimeText() {
        Datetime startTime = getSlot().VTD2_Timeslot_Date_Time__c;
        Integer visitDuration = 30;
        if (getSlot().VTD2_Actual_Visit__r.VTD1_Visit_Duration__c != null) {
            visitDuration = getSlot().VTD2_Actual_Visit__r.VTD1_Visit_Duration__c.intValue();
        }
        Datetime endTime = getSlot().VTD2_Timeslot_Date_Time__c.addMinutes(visitDuration);

        return (startTime.format('h:mm aa', getPiTimezone()) + ' - ' + endTime.format('h:mm aa', getPiTimezone())).toLowerCase();
    }

    public String getCalendarUrl() {
        return VisitRequestConfirmSite__c.getInstance().PICalendarURL__c;
    }
}