@isTest
private class VT_R3_DocumentServiceTest {

    @TestSetup
    static void dataSetup() {
        VT_R3_GlobalSharing.disableForTest = true;
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);

        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
//        Test.stopTest();
        Id studyId = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1].Id;

        User PI3User = VT_D1_TestUtils.createUserByProfile('Primary Investigator', 'PI3');
        User PI4User = VT_D1_TestUtils.createUserByProfile('Primary Investigator', 'PI4');
        Study_Team_Member__c PI2STM = [SELECT Id,User__r.FirstName,Study_Team_Member__c,User__c FROM Study_Team_Member__c WHERE Study__c = :studyId AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName = 'PIBackupUser1'];


        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();
        VT_D1_TestUtils.persistUsers();


        Study_Team_Member__c PI3STM = new Study_Team_Member__c();
        PI3STM.Study__c = study.Id;
        PI3STM.User__c = PI3User.Id;
        PI3STM.Study_Team_Member__c = PI2STM.Id; //stm.id
        PI3STM.VTD1_Active__c = true;
        PI3STM.VTD1_Ready_For_Assignment__c = true;
        PI3STM.RecordTypeId = VT_R4_ConstantsHelper_ProfilesSTM.RECORD_TYPE_ID_STUDY_TEAM_MEMBER_PI;

        insert PI3STM;

        //DOA Log, Note to File Step Down, 1572, Acknowledgement Letter

        List<VTD1_Regulatory_Document__c> regdocList = new List<VTD1_Regulatory_Document__c>();

        VTD1_Regulatory_Document__c regDoc1 = new VTD1_Regulatory_Document__c(
                VTD1_Document_Type__c = 'DOA Log',
                Level__c = 'Site',
                Name = 'regdoc1'
        );
        VTD1_Regulatory_Document__c regDoc2 = new VTD1_Regulatory_Document__c(
                VTD1_Document_Type__c = 'Note to File Step Down',
                Level__c = 'Site',
                Name = 'regdoc2'
        );
        VTD1_Regulatory_Document__c regDoc3 = new VTD1_Regulatory_Document__c(
                VTD1_Document_Type__c = '1572 IRB Acknowledgement Letter',
                Level__c = 'Site',
                Name = 'regdoc3'
        );


        regdocList.add(regDoc1);
        regdocList.add(regDoc2);
        regdocList.add(regDoc3);

        insert regdocList;
        Test.stopTest();
        List<VTD1_Regulatory_Binder__c> regBinderList = new List<VTD1_Regulatory_Binder__c>();
        for (VTD1_Regulatory_Document__c doc : [SELECT Id FROM VTD1_Regulatory_Document__c LIMIT 3]) {
            VTD1_Regulatory_Binder__c regBinder = new VTD1_Regulatory_Binder__c();
            regBinder.VTD1_Regulatory_Document__c = doc.Id;
            regBinder.VTD1_Care_Plan_Template__c = studyId;
            regBinderList.add(regBinder);
        }

        insert regBinderList;
    }

    @IsTest
    static void createPlaceholdersBySiteLevelTest() {
        VT_R3_GlobalSharing.disableForTest = true;
        HealthCloudGA__CarePlanTemplate__c study = [select Id from HealthCloudGA__CarePlanTemplate__c];
        System.debug(study);
        List <Study_Team_Member__c> studyTeamMembers = [select Id, Name, User__c from Study_Team_Member__c where Study__c = :study.Id and RecordType.Name = 'PI' and VTD1_PIsAssignedPG__c != null];
        System.debug('limits ' + Limits.getQueries());
        Test.startTest();
        Virtual_Site__c site = new Virtual_Site__c(
                VTD1_Study_Site_Number__c = '12345',
                VTD1_Study__c = study.Id,
                Name = 'REG_TEST-',
                VTD1_Study_Team_Member__c = studyTeamMembers[0].Id,
                VTD1_PI_User__c = studyTeamMembers[0].User__c
        );
        insert site;
        List<VTD1_Regulatory_Binder__c> binders = [SELECT Id, VTD1_Level__c, VTD1_Care_Plan_Template__c FROM VTD1_Regulatory_Binder__c];
        System.debug('limits2 ' + Limits.getQueries());

        VT_R3_DocumentService.reCalculateDocuments(site.Id);
        Test.stopTest();
        List<VTD1_Document__c> placeholderList = [SELECT Id, VTD1_Current_Workflow__c, VTD1_Regulatory_Binder__c, VTD1_Study__c, VTD1_Status__c, VTD1_IsActive__c FROM VTD1_Document__c];

        System.assertEquals('Not Yet Started', placeholderList[0].VTD1_Current_Workflow__c, 'Incorrect VTD1_Current_Workflow__c value');
        System.assertEquals(binders[0].Id, placeholderList[0].VTD1_Regulatory_Binder__c, 'Incorrect VTD1_Regulatory_Binder__c value');
        System.assertEquals(binders[0].VTD1_Care_Plan_Template__c, placeholderList[0].VTD1_Study__c, 'Incorrect VTD1_Study__c value');
        //System.assertEquals('Pending Approval', placeholderList[0].VTD1_Status__c, 'Incorrect VTD1_Status__c value');  //SH-10773
        System.assertEquals(true, placeholderList[0].VTD1_IsActive__c, 'Incorrect VTD1_Status__c value');
    }

    @IsTest
    static void createPlaceholdersBySiteLevelTest2() {
        VT_R3_GlobalSharing.disableForTest = true;
        HealthCloudGA__CarePlanTemplate__c study = [select Id from HealthCloudGA__CarePlanTemplate__c];
        System.debug(study);
        List <Study_Team_Member__c> studyTeamMembers = [select Id, Name, User__c from Study_Team_Member__c where Study__c = :study.Id and RecordType.Name = 'PI' and VTD1_PIsAssignedPG__c != null];
        Test.startTest();
        Virtual_Site__c site = new Virtual_Site__c(
                VTD1_Study_Site_Number__c = '12345',
                VTD1_Study__c = study.Id,
                Name = 'REG_TEST-',
                VTD1_Study_Team_Member__c = studyTeamMembers[0].Id,
                VTD1_PI_User__c = studyTeamMembers[0].User__c
        );
        insert site;
        List<VTD1_Regulatory_Binder__c> binders = [SELECT Id, VTD1_Level__c, VTD1_Care_Plan_Template__c FROM VTD1_Regulatory_Binder__c];


        List<VTD1_Document__c> placeholderList = [
                SELECT Id, VTD1_Current_Workflow__c, VTD1_Regulatory_Binder__c, VTD1_Study__c, VTD1_Status__c, VTD1_IsActive__c
                FROM VTD1_Document__c WHERE VTD1_Site__c = :site.id
        ];
        delete placeholderList;
        VT_R3_DocumentService.reCalculateDocuments(UserInfo.getUserId());//to code coverage
        VT_R3_DocumentService.reCalculateDocuments(site.Id);
        Test.stopTest();
        placeholderList = [
                SELECT Id, VTD1_Current_Workflow__c, VTD1_Regulatory_Binder__c, VTD1_Study__c, VTD1_Status__c, VTD1_IsActive__c
                FROM VTD1_Document__c WHERE VTD1_Site__c = :site.id
        ];
        System.assertNotEquals(placeholderList.size(), 0);
    }
}