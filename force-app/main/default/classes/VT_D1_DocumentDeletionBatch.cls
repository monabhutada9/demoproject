global without sharing class VT_D1_DocumentDeletionBatch implements Database.Batchable<sObject>, Schedulable {

    // to schedule hourly:
    // system.schedule('Delete flagged Documents', '0 0 * * * ?',  new VT_D1_DocumentDeletionBatch());
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new VT_D1_DocumentDeletionBatch());
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        Datetime currentTime = Datetime.now();
        return Database.getQueryLocator([
                SELECT Id, (SELECT ContentDocumentId FROM ContentDocumentLinks)
                FROM VTD1_Document__c
                WHERE VTD1_Flagged_for_Deletion__c = TRUE
                    AND CreatedDate <= : currentTime.addHours(-1)
        ]);
    }

    global void execute(Database.BatchableContext bc, List<VTD1_Document__c> scope){
        List<Id> contentDocIds = new List<Id>();
        for (VTD1_Document__c doc : scope) {
            for (ContentDocumentLink link : doc.ContentDocumentLinks) {
                contentDocIds.add(link.ContentDocumentId);
            }
        }

        VT_D1_ContentDocumentProcessHandler.skipSetFileNamesOnDoc = true;
        delete scope;
        delete [
                SELECT Id
                FROM ContentDocument
                WHERE Id IN :contentDocIds
        ];
    }

    global void finish(Database.BatchableContext bc){
        List<ContentDocumentLink> cdls = [
                SELECT ContentDocumentId
                FROM ContentDocumentLink
                WHERE LinkedEntityId IN (SELECT Id FROM User WHERE UserType = 'Guest')
                    AND SystemModstamp < :Datetime.now().addHours(-12)
                ORDER BY ContentDocumentId ASC
        ];
        Set<Id> idsSet = new Set<Id>();
        for (ContentDocumentLink cdl : cdls) {
            idsSet.add(cdl.ContentDocumentId);
        }
        if (idsSet.isEmpty()) {
            return;
        }
        List<AggregateResult> cvs = [SELECT COUNT(ContentDocumentId), ContentDocument.Id FROM ContentDocumentLink WHERE ContentDocumentId IN :idsSet GROUP BY ContentDocument.Id];
        Set<ContentDocument> cdsToDelete = new Set<ContentDocument>();
        for (AggregateResult cv : cvs) {
            if ((Integer)cv.get('expr0') == 1) cdsToDelete.add(new ContentDocument(Id = (Id)cv.get('Id')));
        }
        if (!cdsToDelete.isEmpty()) {
            delete new List<ContentDocument>(cdsToDelete);
        }
    }
}