/**
 * Created by Alexander Komarov on 14.05.2019.
 */

@RestResource(UrlMapping='/Patient/FileDownload/*')
global with sharing class VT_R3_RestFileDownload {
    @HttpGet
    global static String getResponse() {
        String fileId = RestContext.request.requestURI.substringAfterLast('/');

        ContentVersion cd = [SELECT Id, FileType, VersionData, ContentSize, FileExtension FROM ContentVersion WHERE ContentDocumentId =:fileId  LIMIT 1];

        String fileData = EncodingUtil.base64Encode(cd.VersionData);
        FileObj result = new FileObj(fileData, cd.FileType, cd.FileExtension, cd.ContentSize, fileData.length());


        return JSON.serialize(result);
    }

    private class FileObj {
        public String fileType;
        public String fileExtension;
        public Integer size;
        public String content;
        public Integer sizeBase64;


        public FileObj(String content, String type, String extension, Integer size, Integer sizeBase64) {
            this.content = content;
            this.fileType = type;
            this.fileExtension = extension;
            this.size = size;
            this.sizeBase64 = sizeBase64;
        }
    }
}