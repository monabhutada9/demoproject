public without sharing class VT_D1_StudySharingQueryHelper {
    private static final List<String> OTHERS_FIELDS = new List<String> {
        'VTD1_Project_Lead__r',
        'VTD1_Remote_CRA__r',
        'VTD1_Regulatory_Specialist__r',
        'VTD1_Virtual_Trial_Study_Lead__r',
        'VTD1_PMA__r',
        'VTD1_Monitoring_Report_Reviewer__r',
        'VTD1_Virtual_Trial_head_of_Operations__r'
    };

    public static final Map<String, Integer> ACCESS_PRECEDENCE = new Map<String, Integer> {
        'None' => 0,
        'Read' => 1,
        'Edit' => 2
    };

    //copado
    private Id studyId;
    private Integer totalRecords;
    private List<User> currentUsers = new List<User>();
    private Map<Id, User> other = new Map<Id, User>();
    private Map<Id, VTD1_Study_Sharing_Configuration__c> existingConfigsByUserId = new Map<Id, VTD1_Study_Sharing_Configuration__c>();
    private Map<Id, VT_D1_StudySharingConfiguratorController.ShareConfigItem> shareConfigItems = new Map<Id, VT_D1_StudySharingConfiguratorController.ShareConfigItem>(); // mapped by user ID so we can check against existing shares and see if we missed anyone

    public String getConfigs(Id studyId, String tabId, String jsonParams) {
        System.debug('tabId2'+ tabId);
        this.studyId = studyId;
        getUsersFromStudy(tabId, jsonParams);
        getExistingConfigsByUserId();
        getShareConfigItemsFromUsers(tabId);
        checkForOpenTasks();

        List<VT_D1_StudySharingConfiguratorController.ShareConfigItem> items = this.shareConfigItems.values();
        items.sort();
        VT_D1_StudySharingConfiguratorController.WrapperRecords wrapperRecords = new VT_D1_StudySharingConfiguratorController.WrapperRecords();
        wrapperRecords.shareConfigItems.addAll(items);
        wrapperRecords.totalRecords = this.totalRecords;
        return JSON.serialize(wrapperRecords);
    }

    private void getUsersFromStudy(String tabId, String jsonParams) {
        System.debug('tabId'+ tabId);
        if(tabId == 'patient'){
            getPatientsByAccId(jsonParams);
        } else if (tabId == 'caregiver') {
            getCaregivers(jsonParams);
        } else if (tabId == 'stm') {
            getStudyMembers(jsonParams);
        } else if (tabId == 'other') {
            getOthers(jsonParams);
        }
    }

    private void getPatientsByAccId(String jsonParams) {
        System.debug('studyId=' +  studyId);
        String query = 'SELECT Id, ' +
            'VTD1_Patient_User__r.Id, ' +
            'VTD1_Patient_User__r.Contact.AccountId, ' +
            'VTD1_Patient_User__r.Name, ' +
            'VTD1_Patient_User__r.Profile.Name ' +
            'FROM Case ';
        String filter = 'VTD1_Study__c = \'' + studyId + '\'' +
            'AND VTD1_Patient_User__r.IsActive = TRUE ' +
            ' AND RecordType.DeveloperName = \'CarePlan\'';
        String sObjectName ='Case';
        Map <String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(jsonParams);
        params.put('filter', filter);
        VT_R5_PagingQueryHelper.ResultWrapper wrapper = VT_R5_PagingQueryHelper.query(query, sObjectName, JSON.serialize(params));
        this.totalRecords = wrapper.count;
        for(Object o : wrapper.records) {
            Case cas = (Case)o;
            this.currentUsers.add(cas.VTD1_Patient_User__r);
        }
    }

    private void getCaregivers(String jsonParams) {
        String query = 'SELECT Id, ' +
            'Contact.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__r.Name, ' +
            'Name, ' +
            'Profile.Name ' +
            'FROM User ';
        String filter = 'Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c = \'' + studyId + '\'' +
            ' AND IsActive = TRUE ' +
            ' AND Profile.Name = \'Caregiver\'';
        String sObjectName ='User';
        Map <String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(jsonParams);
        params.put('filter', filter);
        VT_R5_PagingQueryHelper.ResultWrapper wrapper = VT_R5_PagingQueryHelper.query(query, sObjectName, JSON.serialize(params));
        this.totalRecords = wrapper.count;
        for(Object o : wrapper.records) {
            User us = (User)o;
            this.currentUsers.add(us);
        }
    }

    private void getStudyMembers(String jsonParams) {
        String query = 'SELECT Id, ' +
            'User__r.Id, ' +
            'User__r.Profile.Name, ' +
            'User__r.Name ' +
            'FROM Study_Team_Member__c ';
        String filter = 'Study__c = \'' + studyId + '\'' +
            ' AND User__r.IsActive = TRUE';
        String sObjectName ='Study_Team_Member__c';
        Map <String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(jsonParams);
        params.put('filter', filter);
        VT_R5_PagingQueryHelper.ResultWrapper wrapper = VT_R5_PagingQueryHelper.query(query, sObjectName, JSON.serialize(params));
        this.totalRecords = wrapper.count;
        for(Object o : wrapper.records) {
            Study_Team_Member__c stm = (Study_Team_Member__c)o;
            this.currentUsers.add(stm.User__r);
        }
    }

    private void getOthers(String jsonParams) {
        List<User> users = new List<User>();
        String query = 'SELECT ';
        for (String field : OTHERS_FIELDS) {
            query += field + '.Id,'
                    + field + '.Name,'
                    + field + '.Profile.Name,'
                    ;
        }
        query = query.SubstringBeforeLast(',');
        query += ' FROM HealthCloudGA__CarePlanTemplate__c WHERE Id = :studyId';
        HealthCloudGA__CarePlanTemplate__c study = Database.query(query);

        for (String field : OTHERS_FIELDS) {
            if (study.getSobject(field) != null) {
                users.add((User)study.getSobject(field));
            }
        }
        for(User u : users){
            this.other.put(u.Id, u);
        }
        for (Study_Team_Member__c item : [
            SELECT Id, User__r.Id
            FROM Study_Team_Member__c
            WHERE Study__c = :studyId
            AND User__r.IsActive = TRUE
        ]){
            if(this.other.containsKey(item.User__r.Id)){
                this.other.remove(item.User__r.Id);
            }
        }
        this.currentUsers = this.other.values();
        this.totalRecords = this.other.values().size();
    }

    private void getExistingConfigsByUserId() {
        List<VTD1_Study_Sharing_Configuration__c> items = [
            SELECT Id, VTD1_User__c, VTD1_Profile_Name__c, VTD1_Access_Level__c, VTD1_Locked__c,
                VTD1_User__r.Name, VTD1_User__r.Id, VTD1_User__r.Profile.Name
            FROM VTD1_Study_Sharing_Configuration__c
            WHERE VTD1_Study__c = :this.studyId AND VTD1_User__c IN :this.currentUsers
        ];
        for (VTD1_Study_Sharing_Configuration__c item : items) {
            this.existingConfigsByUserId.put(item.VTD1_User__c, item);
        }
    }

    private void getShareConfigItemsFromUsers(String tabId) {
        for (User u : currentUsers) {
            if (u != null) {
                VT_D1_StudySharingConfiguratorController.ShareConfigItem shareConfigItem = new VT_D1_StudySharingConfiguratorController.ShareConfigItem();
                shareConfigItem.userName = u.Name;
                shareConfigItem.profileName = u.Profile.Name;
                if (tabId =='caregiver' && u.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c != null) {
                    shareConfigItem.patientName = u.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__r.Name;
                }

                shareConfigItem.config = this.existingConfigsByUserId.containsKey(u.Id) ?
                    this.existingConfigsByUserId.get(u.Id) :
                    new VTD1_Study_Sharing_Configuration__c(
                        VTD1_User__c = u.Id,
                        VTD1_Study__c = this.studyId,
                        VTD1_Locked__c = false,
                        VTD1_Access_Level__c = 'None'
                    );
                this.shareConfigItems.put(u.Id, shareConfigItem);
            }
        }
    }

    private void checkForOpenTasks() {
        Integer limitRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
        for (Task item : [
            SELECT OwnerId
            FROM Task
            WHERE OwnerId IN :this.shareConfigItems.keySet()
            AND HealthCloudGA__CarePlanTemplate__c = :this.studyId
            AND Status = 'Open'
            ORDER BY CreatedDate DESC
            LIMIT :limitRows
        ]) {
            this.shareConfigItems.get(item.OwnerId).hasOpenTasks = true;
        }
    }
}