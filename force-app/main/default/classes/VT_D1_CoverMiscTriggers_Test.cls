/**
* @author: Carl Judge
* @date: 08-Nov-18
* @description:
**/

@IsTest
public with sharing class VT_D1_CoverMiscTriggers_Test {

    public static void doTest() {
        Account acc = new Account(Name = 'acc');
        insert acc;

        FeedItem fItem = new FeedItem(Body = 'Test', ParentId = acc.Id);
        insert fItem;

        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c();
        insert study;

        VTD1_Study_Stratification__c studyStrat = new VTD1_Study_Stratification__c(VTD1_Study__c = study.Id);
        insert studyStrat;

        Case cas = new Case();
        insert cas;


        insert new FeedComment(
            CommentBody = 'Test',
            FeedItemId = fItem.Id
        );
        insert new VTD1_OrderStage__c(
            VTD1_ExpectedDeliveryDate__c = Date.today(),
            VTD1_ProtocolId__c = '1234',
            VTD1_ShipmentId__c = '1234',
            VTD1_Status__c = 'Cancelled',
            VTD1_SubjectId__c = '1234',
            VTD1_ShipmentName__c = '1234'
        );
        insert new VTD1_Translation__c(
            VTD1_Record_Id__c = acc.Id,
            VTD1_Field_Name__c = 'Name',
            VTD1_Language__c = 'es',
            VTD1_Object_Name__c = 'Account',
            VTD1_Value__c = 'accSpanish'
        );
        insert new VTD1_Patient_Stratification__c(
            VTD1_Patient__c = cas.Id,
            VTD1_Study_Stratification__c = studyStrat.Id
        );
    }
}