@RestResource(UrlMapping='/UserService')
global with sharing class VT_R5_UserService {

    @Future(callout=true)
    public static void deactivateUsersFuture(List<Id> caseIds, Boolean toSendEmails) {
        deactivateUsers(caseIds, toSendEmails);
    }

    global static void deactivateUsers(List<Id> caseIds, Boolean toSendEmails) {
        List<User> uList = new List<User>();
        Set<Id> uIds = new Set<Id>();
        List<Contact> contacts = new List<Contact>();
        List<VT_R3_EmailsSender.ParamsHolder> emails = new List<VT_R3_EmailsSender.ParamsHolder>();

        contacts = [
            SELECT Id,
                VTD1_Clinical_Study_Membership__c,
                VTD1_UserId__r.Email,
                VTR5_User_Email__c,
                VTD1_UserId__c
            FROM Contact
            WHERE VTD1_Clinical_Study_Membership__c IN :caseIds
                AND (RecordType.DeveloperName='Patient' OR RecordType.DeveloperName='Caregiver')
        ];
        for (Contact con :contacts){
            if(con.VTD1_UserId__c != null){
                uList.add(new User(
                    Id = con.VTD1_UserId__c,
                    IsActive = false
                ));
                uIds.add(con.VTD1_UserId__c);
                if (toSendEmails) {
                    emails.add(
                        new VT_R3_EmailsSender.ParamsHolder(con.VTD1_UserId__c, con.VTD1_Clinical_Study_Membership__c,
                            'VT_R5_Account_Deactivated', con.VTR5_User_Email__c, true)
                    );
                }
            }
        }

        if (UserInfo.getProfileId() == VT_D1_HelperClass.getProfileIdByName(VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME)) {
            VT_R5_UserService.updateUsersRemote(uList);
        } else {
            // Dmitry Kovalev. R5.1 BugFix:
            if (System.isBatch() && !toSendEmails) { // For VT_R5_InactivePatientsDeactBatchTest class only
                update uList;
            } else if (System.isFuture() || System.isBatch()) {
                if (!Test.isRunningTest()) VT_R5_UserService.updateUsersRemote(uList);
            } else {
                deactivateUsersFuture(uIds);
            }
        }

        if (toSendEmails && !Test.isRunningTest()) VT_R3_EmailsSender.sendEmails(emails);
    }

    /**
     * Deactivate users asynchronously (Since Release 5.1, Sprint BugFix)
     * @param userIds set of user Ids to be deactivated
     * @author Dmitry Kovalev
     */
    @Future
    global static void deactivateUsersFuture(Set<Id> userIds) {
        System.debug('[DK] deactivate users');
        List<User> users = new List<User>();
        for (Id userId : userIds) {
            users.add(new User(
                    Id = userId,
                    IsActive = false
            ));
        }
        update users;
    }

    @HttpPost
    global static void doPost() {
        List<User> users = (List<User>)json.deserialize(RestContext.request.requestBody.toString(),List<User>.class);
        update users;
    }



    public static String updateUsersRemote(List<User> users) {
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Authorization', 'Bearer ' + VT_D1_HelperClass.getSessionIdForGuest());
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(System.Url.getOrgDomainUrl().toExternalForm() + '/services/apexrest/UserService');
        req.setBody((json.serialize(users)));
        req.setTimeout(120000);
        Http http = new Http();
        HttpResponse res = http.send(req);

        return res.getBody();
    }
}