/**
 * Created by Andrey Pivovarov on 4/24/2020.
 */

global with sharing class VT_R4_RemindersScheduler implements Schedulable {
    private static List <Integer> minuteBounds = new List<Integer>{0, 15, 30, 45, 60};
    private static List <Integer> minuteBoundsOffset = new List<Integer>{15, 30, 45, 0};
    private static List <Integer> hourBoundsOffset = new List<Integer>{0, 0, 0, 1};
    private static Map <Integer, List <Integer>> offsets = new Map<Integer, List <Integer>>{
            0 => new List<Integer>{15, 0},
            15 => new List<Integer>{30, 0},
            30 => new List<Integer>{45, 0},
            45 => new List<Integer>{0, 1}
    };
    private Id jobId;
    private static String nameSchedule = 'SchedulerReminders To Heroku';
    Datetime dt;

    global void execute(SchedulableContext sc) {
        this.jobId = sc.getTriggerId();
        Database.executeBatch(new VT_R4_RemindersBatch(this, dt),200);
    }

    public Datetime getAlignedTimeJob(Datetime dt) {
        if (dt == null) {
            dt = System.now();
        }
        System.debug('dt = ' + dt);
        Integer minutes = dt.minute();
        Integer hours = dt.hour();
        List <Integer> offsets = offsets.get(minutes);
        if (offsets == null) {
            for (Integer i = 0; i < minuteBoundsOffset.size(); i ++) {
                if (minutes >= minuteBounds[i] && minutes < minuteBounds[i + 1]) {
                    offsets = new List<Integer>{minuteBoundsOffset[i], hourBoundsOffset[i]};
                    break;
                }
            }
        }
        System.debug('offsets = ' + offsets);
        System.debug(minutes + ' ' + hours);
        minutes = offsets[0];
        hours += offsets[1];
        if (hours >= 24) {
            hours = 0;
        }
        Time t = Time.newInstance(hours, minutes, 0, 0);
        dt = Datetime.newInstance(dt.date(), t);
        return dt;
    }

    public void scheduleJob(Datetime dt) {
        System.debug('scheduleJob ' + dt + ' ' + this.jobId);
        if (this.jobId != null) {
            System.abortJob(this.jobId);
        }
        if (Test.isRunningTest()) nameSchedule = 'TestSchedulerRemindersHeroku';
        Integer minutes = dt.minute();
        Integer hours = dt.hour();
        String cron = '0 ' + minutes + ' ' + hours + ' * * ?';
        System.debug('cron = ' + cron);
        VT_R4_RemindersScheduler remindersScheduler = new VT_R4_RemindersScheduler();
        remindersScheduler.dt = dt;
        System.schedule(nameSchedule, cron, remindersScheduler);
    }
}