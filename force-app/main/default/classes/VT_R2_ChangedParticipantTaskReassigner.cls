/**
* @author: Carl Judge
* @date: 28-Jun-19
* @description: Reassign open tasks to new Case participants
**/

public without sharing class VT_R2_ChangedParticipantTaskReassigner {

    public static void reassignTasks(Map<Id, Map<Id, Id>> oldToNewIdsByCaseId) {
        system.debug(oldToNewIdsByCaseId);


        List<Id> oldIds = new List<Id>();
        for (Map<Id, Id> item : oldToNewIdsByCaseId.values()) {
            oldIds.addAll(item.keySet());
        }

        List<Task> tasksToUpdate = new List<Task>();
        for (Task item : [
            SELECT Id, OwnerId, WhatId, VTD1_Case_lookup__c
            FROM Task
            WHERE (WhatId IN :oldToNewIdsByCaseId.keySet() OR VTD1_Case_lookup__c IN :oldToNewIdsByCaseId.keySet())
            AND OwnerId IN :oldIds AND IsClosed = FALSE
        ]) {
            Id caseId = oldToNewIdsByCaseId.containsKey(item.WhatId) ? item.WhatId : item.VTD1_Case_lookup__c;
            if (caseId != null && oldToNewIdsByCaseId.containsKey(caseId) &&
                oldToNewIdsByCaseId.get(caseId).containsKey(item.OwnerId) &&
                oldToNewIdsByCaseId.get(caseId).get(item.OwnerId) != null)
            {
                item.OwnerId = oldToNewIdsByCaseId.get(caseId).get(item.OwnerId);
                tasksToUpdate.add(item);
            }
        }

        if (! tasksToUpdate.isEmpty()) { update tasksToUpdate; }
    }
}