public class POCDocumentDelete {

    public static void getDocumentData() { 

        Set<Id> parentDocumentIdSet = new Set<Id>();
        Set<Id> parentSiteIdSet = new Set<Id>();
        Set<Id> parentStudyIdSet = new Set<Id>();

        Map<Id, ContentVersion> contentVersionMap = new Map<Id, ContentVersion>();
        Map<Id, ContentDocumentLink> contentDocumentLinkMap = new Map<Id, ContentDocumentLink>();
        for(ContentVersion contentVersionObj: [SELECT Id, 
                                                     Title, 
                                                     ContentDocumentId 
                                                    FROM ContentVersion
                                                    WHERE isLatest = TRUE]) {
            contentVersionMap.put(contentVersionObj.ContentDocumentId, contentVersionObj);
        }
        if(contentVersionMap.keySet().size() > 0) {
            for(ContentDocumentLink contentDocLinkObj: [SELECT ContentDocument.Title, 
                                                               LinkedEntityId,
                                                               ContentDocumentId
                                                            FROM ContentDocumentLink 
                                                            WHERE ContentDocumentId IN: contentVersionMap.keySet()]) {

                contentDocumentLinkMap.put(contentDocLinkObj.LinkedEntityId, contentDocLinkObj);                                                
            }  
        }
        if(contentDocumentLinkMap.keySet().size() > 0) {
            for(Id fileParentId: contentDocumentLinkMap.keySet()) {
                String parentSobjName = SchemaGlobalDescribe.findObjectNameFromRecordIdPrefix(String.valueOf(fileParentId).subString(0, 3));
                if(parentSobjName == 'VTD1_Document__c') {
                    parentDocumentIdSet.add(fileParentId);
                }
                else if(parentSobjName == 'HealthCloudGA__CarePlanTemplate__c') {
                    parentStudyIdSet.add(fileParentId);
                }
                else if(parentSobjName == 'Virtual_Site__c') {
                    parentSiteIdSet.add(fileParentId);
                }
            }  
        }
    }

    public class FileAndRelatedRecordsWrapper {
        public String StudyName;
        public String SiteName;
        public String SiteNumber;
        public String DeletedDocumentName;
        public String DeletedDocumentNumber;
        public String RoleOfDeleter;
        public String FirstNameOfDeleter;
        public String LastNameOfDeleter;
        public String ResonOfDelete; 
        public String PatientID;
        public String DocumentCategory;
        public String DocumentVersion;
        public DateTime DeletedTime;

        public FileAndRelatedRecordsWrapper(String StudyName, 
                                            String SiteName, 
                                            String SiteNumber, 
                                            String DeletedDocumentName, 
                                            String DeletedDocumentNumber, 
                                            String RoleOfDeleter, 
                                            String FirstNameOfDeleter,
                                            String LastNameOfDeleter, 
                                            String ResonOfDelete, 
                                            String PatientID, 
                                            String DocumentCategory,
                                            String DocumentVersion,
                                            DateTime DeletedTime) {
            this.StudyName = StudyName;
            this.SiteName = SiteName;
            this.DeletedDocumentName = DeletedDocumentName;
            this.DeletedDocumentNumber = DeletedDocumentNumber;
            this.RoleOfDeleter = RoleOfDeleter;
            this.FirstNameOfDeleter = FirstNameOfDeleter;
            this.LastNameOfDeleter = LastNameOfDeleter;
            this.ResonOfDelete = ResonOfDelete;
            this.PatientID = PatientID;
            this.DocumentCategory = DocumentCategory;
            this.DocumentVersion = DocumentVersion;
            this.DeletedTime = DeletedTime;
        }
    }
}