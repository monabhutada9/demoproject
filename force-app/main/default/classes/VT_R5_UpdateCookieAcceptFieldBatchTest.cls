/***********************************************************************
* @author              Aarohi Sanjekar <aarohi.sanjekar@quintiles.com>
* @date                30-Oct-2020
* @group               Cookie Opt-In/Opt Out
* @group-content       http://jira.quintiles.net/browse/SH-19347
* @description         Test class for Batch to update 'Cookies Accepted Details'fields of Contact object for existing records
*/
@isTest(seeAllData = false)
public class VT_R5_UpdateCookieAcceptFieldBatchTest{
    @isTest
    static void testBatchPositive(){
        HealthCloudGA__CarePlanTemplate__c study=VT_D1_TestUtils.prepareStudy(1); 
        List<User> userIds = [SELECT Id FROM User WHERE Profile.Name = 'Patient' LIMIT 1];
        List<Contact> lstContacts = new List<Contact>(); 
        List<case> listOfCases = new List<case>();
        lstContacts.add(new Contact(
            FirstName = 'Test',
            LastName = 'Patient',
            RecordTypeId = VT_D1_ConstantsHelper.RECORD_TYPE_ID_CONTACT_PATIENT,
            VTR3_CookiesAcceptedDetails__c = 'Test',
            VTD1_UserId__c = userIds.get(0).id
        ));
        insert lstContacts;
        Case caseRecordPatient = new Case(ContactId = lstContacts.get(0).Id, VTD1_Study__c = study.Id);
        listOfCases.add(caseRecordPatient);
        insert listOfCases;
        List<Account> lstActs = new List<Account>();
        for(Contact con : lstContacts){
            lstActs.add(new Account(Name = 'Test Patient ' + con.FirstName, VTD2_Patient_s_Contact__c = con.Id));
        }
        insert lstActs;
        Test.startTest();
        VT_R5_UpdateCookieAcceptFieldBatch.runBatch(1);
        Test.stopTest();
        Contact conUpdateList = [SELECT id, VTR3_CookiesAcceptedDetails__c FROM Contact where VTR3_CookiesAcceptedDetails__c=null LIMIT 1];
        System.assertEquals(conUpdateList.VTR3_CookiesAcceptedDetails__c, null);
    }
    @isTest
    static void testBatchNegative(){
        HealthCloudGA__CarePlanTemplate__c study=VT_D1_TestUtils.prepareStudy(1); 
        List<Contact> lstContact = new List<Contact>();
        List<case> listOfCase = new List<case>();
        lstContact.add(new Contact(
            FirstName = 'Test2',
            LastName = 'Patient2',
            RecordTypeId = VT_D1_ConstantsHelper.RECORD_TYPE_ID_CONTACT_PATIENT,
            VTR3_CookiesAcceptedDetails__c = 'TestCookie'
        ));
        insert lstContact;
        Case caseRecordPatient = new Case(ContactId = lstContact.get(0).Id, VTD1_Study__c = study.Id);
        listOfCase.add(caseRecordPatient);
        insert listOfCase;
        List<Account> lstAct = new List<Account>();
        for(Contact con : lstContact){
            lstAct.add(new Account(Name = 'TestCookie ' + con.FirstName, VTD2_Patient_s_Contact__c = con.Id));
        }
        insert lstAct;
        Test.startTest();
        VT_R5_UpdateCookieAcceptFieldBatch.runBatch(1);
        Test.stopTest();
        Contact conUpdateList = [SELECT id, VTR3_CookiesAcceptedDetails__c FROM Contact where VTR3_CookiesAcceptedDetails__c=null LIMIT 1];
        System.assertEquals(conUpdateList.VTR3_CookiesAcceptedDetails__c, null);
    } 
}