/**
 * Created by user on 25.08.2020.
 * This batch finds all VT_R5_General_Phone_Configurations records and delete them.
 */

global with sharing class VT_R5_General_Phone_ConfigurationsBatch implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return database.getQueryLocator([SELECT id from VTR5_GeneralPhoneConfiguration__c]);
    }

    global void execute(Database.BatchableContext bc, List<sObject> records){
        delete records;
    }
    global void finish(Database.BatchableContext bc){
        System.debug('VT_R5_General_Phone_ConfigurationsBatch - finished deletion');
    }
}