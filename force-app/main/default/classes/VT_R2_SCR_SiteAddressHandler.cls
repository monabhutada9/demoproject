public with sharing class VT_R2_SCR_SiteAddressHandler extends Handler {

    public override void onBeforeInsert(Handler.TriggerContext context) {
        AddressInsertValidator validator = new AddressInsertValidator(context);
        validator.validate();
    }

    public override void onAfterUpdate(Handler.TriggerContext context) {
        PrimaryAddressManager manager = new PrimaryAddressManager(context);
        manager.updatePrimaryAddress();

        AddressAfterUpdateValidator validator = new AddressAfterUpdateValidator(context);
        validator.validate();
    }

    public override void onBeforeDelete(Handler.TriggerContext context) {
        AddressDeleteValidator validator = new AddressDeleteValidator(context);
        validator.validate();
    }


    private abstract class AddressValidator {

        private Map<Id, SiteAddresses> siteAddresses;
        private List<ValidationRule> validatorRules;

        public AddressValidator(Handler.TriggerContext context) {
            this.siteAddresses = this.getSiteAddresses(context);
            this.validatorRules = new List<ValidationRule>();
        }

        public void validate() {
            for (ValidationRule validator : this.validatorRules) {
                for (SiteAddresses addresses : this.siteAddresses.values()) {
                    validator.validate(addresses);
                }
            }
        }


        protected abstract Set<Id> getSiteAddressIds(Handler.TriggerContext context);

        protected abstract Map<Id, SiteAddresses> addTriggerContextAddresses(Map<Id, SiteAddresses> siteAddresses, Handler.TriggerContext context);


        private Map<Id, SiteAddresses> getSiteAddresses(TriggerContext context) {

            Map<Id, SiteAddresses> result = new Map<Id, SiteAddresses>();
            Set<Id> changedAddressesSiteIds = this.getSiteAddressIds(context);

            if (!changedAddressesSiteIds.isEmpty()) {
                List<VTR2_Site_Address__c> dbAddresses = this.getDatabaseAddresses(changedAddressesSiteIds);
                result = this.convertAddressesToSiteAddresses(dbAddresses);
                result = this.addTriggerContextAddresses(result, context);
            }

            return result;
        }

        private List<VTR2_Site_Address__c> getDatabaseAddresses(Set<Id> changedAddressesSiteIds) {
            return [
                    SELECT Id
                            , VTR2_Primary__c
                            , VTR2_VirtualSiteLookup__c
                    FROM VTR2_Site_Address__c
                    WHERE VTR2_VirtualSiteLookup__c IN :changedAddressesSiteIds

            ];
        }

        private Map<Id, SiteAddresses> convertAddressesToSiteAddresses(List<VTR2_Site_Address__c> dbAddresses) {

            Map<Id, SiteAddresses> siteAddresses = new Map<Id, SiteAddresses>();

            for (VTR2_Site_Address__c address : dbAddresses) {
                Id siteId = address.VTR2_VirtualSiteLookup__c;
                if (!siteAddresses.containsKey(siteId)) {
                    siteAddresses.put(siteId, new SiteAddresses(siteId));
                }
                siteAddresses.get(siteId).addDbAddress(address);
            }

            return siteAddresses;
        }

    }

    private class AddressInsertValidator extends AddressValidator {

        public AddressInsertValidator(Handler.TriggerContext context) {
            super(context);
            this.validatorRules.add(new InsertPrimaryAddressValidationRule());
        }

        protected override Set<Id> getSiteAddressIds(TriggerContext context) {

            Set<Id> changedAddressesSiteIds = new Set<Id>();

            for (VTR2_Site_Address__c address : (List<VTR2_Site_Address__c>) context.newList) {
                changedAddressesSiteIds.add(address.VTR2_VirtualSiteLookup__c);
            }

            return changedAddressesSiteIds;
        }

        protected override Map<Id, SiteAddresses> addTriggerContextAddresses(Map<Id, SiteAddresses> siteAddresses, Handler.TriggerContext context) {

            for (VTR2_Site_Address__c address : (List<VTR2_Site_Address__c>) context.newList) {
                Id siteId = address.VTR2_VirtualSiteLookup__c;
                if (!siteAddresses.containsKey(siteId)) {
                    siteAddresses.put(siteId, new SiteAddresses(siteId));
                }
                siteAddresses.get(siteId).addTriggerAddress(address);
            }

            return siteAddresses;
        }

    }

    private class AddressAfterUpdateValidator extends AddressValidator {

        public AddressAfterUpdateValidator(Handler.TriggerContext context) {
            super(context);
            this.validatorRules.add(new AfterUpdatePrimaryAddressValidationRule());

        }

        protected override Set<Id> getSiteAddressIds(TriggerContext context) {

            Set<Id> changedAddressesSiteIds = new Set<Id>();
            List<VTR2_Site_Address__c> oldList = (List<VTR2_Site_Address__c>) context.oldList;
            Map<Id, VTR2_Site_Address__c> newMap = (Map<Id, VTR2_Site_Address__c>) context.newMap;

            for (VTR2_Site_Address__c oldAddress : oldList) {
                VTR2_Site_Address__c newAddress = newMap.get(oldAddress.Id);
                changedAddressesSiteIds.add(newAddress.VTR2_VirtualSiteLookup__c);
                changedAddressesSiteIds.add(oldAddress.VTR2_VirtualSiteLookup__c);
            }

            return changedAddressesSiteIds;
        }

        protected override Map<Id, SiteAddresses> addTriggerContextAddresses(Map<Id, SiteAddresses> siteAddresses, Handler.TriggerContext context) {

            for (VTR2_Site_Address__c address : (List<VTR2_Site_Address__c>) context.newList) {
                Id siteId = address.VTR2_VirtualSiteLookup__c;
                if (!siteAddresses.containsKey(siteId)) {
                    siteAddresses.put(siteId, new SiteAddresses(siteId));
                }
                siteAddresses.get(siteId).addTriggerAddress(address);
            }

            for (VTR2_Site_Address__c address : (List<VTR2_Site_Address__c>) context.oldList) {
                VTR2_Site_Address__c newAddress = (VTR2_Site_Address__c) context.newMap.get(address.Id);
                Id siteId = address.VTR2_VirtualSiteLookup__c;
                if (!siteAddresses.containsKey(siteId)) {
                    siteAddresses.put(siteId, new SiteAddresses(siteId));
                }
                siteAddresses.get(siteId).addTriggerAddress(newAddress);
            }

            return siteAddresses;
        }

    }

    private class AddressDeleteValidator extends AddressValidator {

        public AddressDeleteValidator(Handler.TriggerContext context) {
            super(context);
            this.validatorRules.add(new DeletePrimaryAddressValidationRule());
        }

        protected override Set<Id> getSiteAddressIds(TriggerContext context) {

            Set<Id> changedAddressesSiteIds = new Set<Id>();
            for (VTR2_Site_Address__c address : (List<VTR2_Site_Address__c>) context.oldList) {
                if (address.VTR2_Primary__c) {
                    changedAddressesSiteIds.add(address.VTR2_VirtualSiteLookup__c);
                }
            }

            return changedAddressesSiteIds;
        }

        protected override Map<Id, SiteAddresses> addTriggerContextAddresses(Map<Id, SiteAddresses> siteAddresses, Handler.TriggerContext context) {

            for (VTR2_Site_Address__c address : (List<VTR2_Site_Address__c>) context.oldList) {
                Id siteId = address.VTR2_VirtualSiteLookup__c;
                if (!siteAddresses.containsKey(siteId)) {
                    siteAddresses.put(siteId, new SiteAddresses(siteId));
                }
                siteAddresses.get(siteId).addTriggerAddress(address);
            }

            return siteAddresses;
        }

    }


    public interface ValidationRule {
        Boolean validate(SiteAddresses addresses);
    }

    public class InsertPrimaryAddressValidationRule implements ValidationRule {

        public Boolean validate(SiteAddresses addresses) {

            Boolean isValid = false;
            String error;

            Map<Id, VTR2_Site_Address__c> nonTriggerDbAddresses = addresses.dbAddresses.clone();
            for (VTR2_Site_Address__c address : addresses.triggerAddresses) {
                if (nonTriggerDbAddresses.containsKey(address.Id)) {
                    nonTriggerDbAddresses.remove(address.Id);
                }
            }

            Integer triggerPrimaryQuantity = addresses.getTriggerPrimaryQuantity();
            Integer quantityNonTriggerPrimaryAddresses = addresses.getPrimaryQuantity(nonTriggerDbAddresses.values());
            Integer finalPrimaryQuantity = quantityNonTriggerPrimaryAddresses + triggerPrimaryQuantity;

            if (finalPrimaryQuantity > 1) {
                error = Label.VTR2_SCR_One_Primary_Address_Allowed;
            } else if (finalPrimaryQuantity == 0) {
                error = Label.VTR2_SCR_Site_Should_Have_Primary_Address;
            }

            if (error != null) {
                addresses.setErrorToRecords(error);
            } else {
                isValid = true;
            }

            return isValid;
        }
    }

    public class AfterUpdatePrimaryAddressValidationRule implements ValidationRule {

        public Boolean validate(SiteAddresses addresses) {

            Boolean isValid = false;
            String error;

            Integer dbPrimaryQuantity = addresses.getDbPrimaryQuantity();

            if (dbPrimaryQuantity > 1) {
                error = Label.VTR2_SCR_One_Primary_Address_Allowed;
            } else if (dbPrimaryQuantity == 0) {
                error = Label.VTR2_SCR_Site_Should_Have_Primary_Address;
            }

            if (error != null) {
                addresses.setErrorToRecords(error);
            } else {
                isValid = true;
            }

            return isValid;
        }
    }

    public class DeletePrimaryAddressValidationRule implements ValidationRule {

        public Boolean validate(SiteAddresses addresses) {

            Boolean isValid = false;
            String error;

            Integer triggerPrimaryQuantity = addresses.getTriggerPrimaryQuantity();

            if (triggerPrimaryQuantity != 0) {
                error = Label.VTR2_SCR_Site_Should_Have_Primary_Address;
                addresses.setErrorToRecords(error);
            } else {
                isValid = true;
            }

            return isValid;
        }
    }


    private class PrimaryAddressManager {

        Handler.TriggerContext context;

        public PrimaryAddressManager(Handler.TriggerContext context) {
            this.context = context;
        }

        private void updatePrimaryAddress() {

            Set<Id> newPrimaries = new Set<Id>();
            Set<Id> siteIds = new Set<Id>();
            List<VTR2_Site_Address__c> oldList = this.context.oldList;
            Map<Id, VTR2_Site_Address__c> newMap = (Map<Id, VTR2_Site_Address__c>) this.context.newMap;

            for (VTR2_Site_Address__c oldAddress : oldList) {
                VTR2_Site_Address__c newAddress = newMap.get(oldAddress.Id);
                if (!oldAddress.VTR2_Primary__c && newAddress.VTR2_Primary__c
                        && (oldAddress.VTR2_VirtualSiteLookup__c == newAddress.VTR2_VirtualSiteLookup__c)) {
                    newPrimaries.add(newAddress.Id);
                    siteIds.add(newAddress.VTR2_VirtualSiteLookup__c);
                }
            }

            List<VTR2_Site_Address__c> addressesToUpdate = [
                    SELECT Id
                            ,Name
                            , VTR2_Primary__c
                    FROM VTR2_Site_Address__c
                    WHERE Id NOT IN :newPrimaries
                    AND VTR2_VirtualSiteLookup__c IN :siteIds
            ];

            for (VTR2_Site_Address__c address : addressesToUpdate) {
                address.VTR2_Primary__c = false;
            }

            update addressesToUpdate;
        }
    }


    private class SiteAddresses {

        private final Id virtualSiteId;
        private final Map<Id, VTR2_Site_Address__c> dbAddresses;
        private final List<VTR2_Site_Address__c> triggerAddresses;

        public SiteAddresses(Id virtualSiteId) {
            this.virtualSiteId = virtualSiteId;
            this.dbAddresses = new Map<Id, VTR2_Site_Address__c>();
            this.triggerAddresses = new List<VTR2_Site_Address__c>();
        }

        private void addDbAddress(VTR2_Site_Address__c address) {
            dbAddresses.put(address.Id, address);
        }

        private void addTriggerAddress(VTR2_Site_Address__c address) {
            triggerAddresses.add(address);
        }

        private Integer getDbPrimaryQuantity() {
            return getPrimaryQuantity(this.dbAddresses.values());
        }

        private Integer getTriggerPrimaryQuantity() {
            return getPrimaryQuantity(this.triggerAddresses);
        }

        private Integer getPrimaryQuantity(List<VTR2_Site_Address__c> addresses) {
            Integer quantity = 0;
            for (VTR2_Site_Address__c address : addresses) {
                if (address.VTR2_Primary__c && address.VTR2_VirtualSiteLookup__c != null) {
                    quantity++;
                }
            }

            return quantity;
        }

        private void setErrorToRecords(String error) {
            for (VTR2_Site_Address__c address : this.triggerAddresses) {
                address.addError(error);
            }
        }
    }

}