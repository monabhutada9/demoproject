global with sharing class UpdateSignatureType implements Database.Batchable<sObject>{

    global Database.QueryLocator start(Database.BatchableContext batchableContext) {
        return Database.getQueryLocator(
                'SELECT Id, VTD1_Signature_Type__c, VTR4_Signature_Type__c FROM VTD1_Document__c ' +
                'WHERE VTR4_Signature_Type__c = null AND VTD1_Signature_Type__c != null'
        );
    }

    global void execute(Database.BatchableContext batchableContext, List<VTD1_Document__c> documents) {
        for (VTD1_Document__c document : documents){
                document.VTR4_Signature_Type__c = document.VTD1_Signature_Type__c;
        }
        update documents;
    }

    global void finish(Database.BatchableContext batchableContext) {
    }
    //to execute: Database.executeBatch(new UpdateSignatureType(), 200);
}
//