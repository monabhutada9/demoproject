@IsTest
private class GetStatsForVideoConfControllerTest {

    public with sharing class CalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            if (request.getEndpoint().endsWith('/session')) {
                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'application/json');
                response.setBody('["test"]');
                response.setStatus('OK');
                response.setStatusCode(200);
                return response;
            } else if (request.getEndpoint().endsWith('/getListArchived')) {
                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'application/json');
                response.setBody('{"archives":[{"name":"test-name","status":"test-status","reason":"test-reason","createdAt":"1341828183000","duration":"100"}]}');
                response.setStatus('OK');
                response.setStatusCode(200);
                return response;
            } else {
                throw new CalloutException('Unknown callout endpoint');
            }
        }
    }

    public with sharing class InvalidCalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            if (request.getEndpoint().endsWith('/getListArchived')) {
                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'application/json');
                response.setBody('{}');
                response.setStatus('OK');
                response.setStatusCode(200);
                return response;
            } else {
                throw new CalloutException('Unknown callout endpoint');
            }
        }
    }


    @TestSetup
    static void setupData() {
        Test.setMock(HttpCalloutMock.class, new CalloutMock());
        Video_Conference__c vc = new Video_Conference__c(
                SessionId__c = 'test-session-id'
        );

        insert vc;
    }

    @IsTest
    static void getArchivesListBySessionTest_OK() {
        Video_Conference__c videoConference = [SELECT Id FROM Video_Conference__c LIMIT 1];

        Test.setMock(HttpCalloutMock.class, new CalloutMock());
        Test.startTest();
            String status = GetStatsForVideoConfController.getArchivesListBySession(videoConference.Id);
        Test.stopTest();

        System.assertEquals('OK', status, 'Incorrect status');
    }

    @IsTest
    static void getArchivesListBySessionTest_Error() {
        Video_Conference__c videoConference = [SELECT Id FROM Video_Conference__c LIMIT 1];

        Test.setMock(HttpCalloutMock.class, new InvalidCalloutMock());
        Test.startTest();
            String status = GetStatsForVideoConfController.getArchivesListBySession(videoConference.Id);
        Test.stopTest();

        System.assertEquals('ERROR', status, 'Incorrect status');
    }
}