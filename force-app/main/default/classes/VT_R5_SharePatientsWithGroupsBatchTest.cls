/**
* @author: Carl Judge
* @date: 12-Nov-20
**/
@IsTest
public class VT_R5_SharePatientsWithGroupsBatchTest {
    @TestSetup
    private static void testSetup() {
        Test.startTest();
        VT_R3_GlobalSharing.disableForTest = true;
        DomainObjects.Study_t study = new DomainObjects.Study_t()
            .addAccount(new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor'));
        DomainObjects.VirtualSite_t site = new DomainObjects.VirtualSite_t()
            .addStudy(study);
        site.persist();
        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t();
        DomainObjects.User_t userPatient = new DomainObjects.User_t()
            .addContact(patientContact)
            .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        userPatient.persist();
        DomainObjects.Case_t casePatient = new DomainObjects.Case_t()
            .setRecordTypeByName('CarePlan')
            .addStudy(study)
            .addUser(userPatient)
            .addVirtualSite(site)
            .addContact(patientContact);
        casePatient.persist();
        Case cas = [SELECT Id, VTD1_Study__c, VTD1_Virtual_Site__c, VTD1_Primary_PG__c, VTD1_PI_user__c, VTD1_Patient_User__c, Contact.AccountId FROM Case ORDER BY CreatedDate DESC LIMIT 1];
        insert new Case(
            VTD1_Clinical_Study_Membership__c = cas.Id,
            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VTD1_PCF').getRecordTypeId()
        );
        Test.stopTest();
    }

    @IsTest
    static void doTest() {
        Test.startTest();
        Database.executeBatch(new VT_R5_SharePatientsWithGroupsBatch());
        Test.stopTest();
    }
}