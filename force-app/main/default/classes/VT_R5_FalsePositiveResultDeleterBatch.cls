/**
 * @author Dmitry Yartsev
 * @date 26.10.2020
 * @description One time used batch for deleting False Positive Results for Missed eDiaries for Symptom Check (SH-19032)
 */

public with sharing class VT_R5_FalsePositiveResultDeleterBatch implements Database.Batchable<Case> {

    @TestVisible
    private static final String START_QUERY_STUB_NAME = 'VT_R5_FalsePositiveResultDeleterBatch.start';
    private static final String RULE_GUID_FIELD_NAME = String.valueOf(VTD1_Survey__c.VTR5_eCoa_ruleGuid__c);
    private static final String MISSED_STATUS = 'Missed';

    String studyId;
    Set<String> ruleGuidsSet;

    public VT_R5_FalsePositiveResultDeleterBatch(String ruleName, Id studyId) {
        this.studyId = studyId;
        this.ruleGuidsSet = new Set<String>();
        for (AggregateResult ar : [SELECT COUNT(Id), VTR5_eCoa_ruleGuid__c FROM VTD1_Survey__c WHERE VTD1_CSM__r.VTD1_Study__c = :studyId AND VTR5_eCoa_RuleName__c = :ruleName AND VTR5_eCoa_ruleGuid__c != NULL GROUP BY VTR5_eCoa_ruleGuid__c]) {
            this.ruleGuidsSet.add(String.valueOf(ar.get(RULE_GUID_FIELD_NAME)));
        }
    }

    public Iterable<Case> start(Database.BatchableContext cxt) {
        return (Iterable<Case>) new VT_Stubber.ResultStub(
                START_QUERY_STUB_NAME,
                Database.getQueryLocator([
                    SELECT Id,
                    (
                            SELECT VTD1_Status__c, VTR5_eCoa_ruleGuid__c
                            FROM Surveys__r
                            WHERE VTR5_eCoa_ruleGuid__c IN :this.ruleGuidsSet
                            ORDER BY VTD1_Due_Date__c DESC
                    )
                    FROM Case
                    WHERE VTD1_Study__c =: this.studyId
                ])
        ).getResult();
    }

    public void execute(Database.BatchableContext cxt, List<Case> scope) {
        List<VTD1_Survey__c> diariesToDelete = new List<VTD1_Survey__c>();

        for (Case c : scope) {
            Map<String, List<VTD1_Survey__c>> diariesByGuid;
            if (c.Surveys__r == null || c.Surveys__r.isEmpty()) {
                continue;
            }
            diariesByGuid = getDiariesByGuid(c.Surveys__r);
            for (List<VTD1_Survey__c> diaries : diariesByGuid.values()) {
                diariesToDelete.addAll(getDiariesToDelete(diaries));
            }
        }

        if (!diariesToDelete.isEmpty() && !Test.isRunningTest()) {
            delete diariesToDelete;
        }

    }

    public void finish(Database.BatchableContext cxt) {
    }

    private Map<String, List<VTD1_Survey__c>> getDiariesByGuid(List<VTD1_Survey__c> diaries) {
        Map<String, List<VTD1_Survey__c>> diariesByGuid = new Map<String, List<VTD1_Survey__c>>();

        for (VTD1_Survey__c diary : diaries) {
            if (!diariesByGuid.containsKey(diary.VTR5_eCoa_ruleGuid__c)) {
                diariesByGuid.put(diary.VTR5_eCoa_ruleGuid__c, new List<VTD1_Survey__c>());
            }
            diariesByGuid.get(diary.VTR5_eCoa_ruleGuid__c).add(diary);
        }

        return diariesByGuid;
    }

    @TestVisible
    private List<VTD1_Survey__c> getDiariesToDelete(List<VTD1_Survey__c> diaries) {
        List<VTD1_Survey__c> diariesToDelete = new List<VTD1_Survey__c>();
        List<VTD1_Survey__c> missedList = new List<VTD1_Survey__c>();
        Boolean hasDueSoonOrCompleted = false;

        for (VTD1_Survey__c diary : diaries) {
            if (diary.VTD1_Status__c == MISSED_STATUS) {
                missedList.add(diary);
            } else {
                hasDueSoonOrCompleted = true;
            }
        }

        if (hasDueSoonOrCompleted) {
            diariesToDelete.addAll(missedList);
        } else if (!missedList.isEmpty()) {
            missedList.remove(0);
            diariesToDelete.addAll(missedList);
        }

        return diariesToDelete;
    }

}