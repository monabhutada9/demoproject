/**
 * Created by user on 12/26/2019.
 */

public with sharing class VT_R4_PatientDeviceTriggerHandler {


    public static List<VTD1_NotificationC__c> notificationList ;
    public static void changeIntegrationId(List<VTD1_Patient_Device__c> newDevices) {
        Set<Id> patientDeviceIds = new Set<Id>();
        Set<Id> protocolDeviceIds = new Set<Id>();
        List<VTD1_Patient_Device__c> patientDevices = new List<VTD1_Patient_Device__c>();
        for (VTD1_Patient_Device__c patientDevice : newDevices) {
            if ((patientDevice.RecordTypeId.equals(VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_DEVICE_AD_HOC_CONNECTED_DEVICE)
                    || patientDevice.RecordTypeId.equals(VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_DEVICE_AD_HOC_TABLET_DEVICE))
                    && patientDevice.VTD1_IntegrationId__c == null)  {
                patientDeviceIds.add(patientDevice.VTD1_Patient_Delivery__c);
                patientDevices.add(patientDevice);
                protocolDeviceIds.add(patientDevice.VTD1_Protocol_Device__c);
            }
        }
        if (!patientDeviceIds.isEmpty()) {
            Map<Id, VTD1_Order__c> ordersByIds = new Map<Id, VTD1_Order__c>([
                    SELECT VTD1_Case__r.VTD1_Subject_ID__c, RecordTypeId
                    FROM VTD1_Order__c
                    WHERE Id IN : patientDeviceIds
            ]);
            Map<Id, HealthCloudGA__EhrDevice__c> ehrDevicesByIds = new Map<Id, HealthCloudGA__EhrDevice__c>([
                    SELECT Name
                    FROM HealthCloudGA__EhrDevice__c
                    WHERE Id IN : protocolDeviceIds
            ]);
            for (VTD1_Patient_Device__c patientDevice : patientDevices) {
                VTD1_Order__c order = ordersByIds.get(patientDevice.VTD1_Patient_Delivery__c);
                HealthCloudGA__EhrDevice__c protocolDevice = ehrDevicesByIds.get(patientDevice.VTD1_Protocol_Device__c);
                patientDevice.VTD1_IntegrationId__c = order.VTD1_Case__r.VTD1_Subject_ID__c + '-'
                        + protocolDevice.Name;
            }
        }
    }

    /**

   * @description fills Start Date field on the patient devices if the Active flag is set to true

   http://jira.quintiles.net/browse/SH-11824
 */
    public static void fillPatientDevicesStartDate(List<VTD1_Patient_Device__c> newDevices, Map<Id, VTD1_Patient_Device__c> oldMap) {
        for (VTD1_Patient_Device__c device :newDevices) {
            if((oldMap == null || device.VTR5_Active__c != oldMap.get(device.Id).VTR5_Active__c ) && device.VTR5_Active__c == true) {
                device.VTR5_StartDate__c = Datetime.now();
                device.VTR5_EndDate__c = null;
            }
        }
    }
    
    /**
    * @description fills Device Key Id field on the patient devices depending on the fields "IMEI" and "Vendor Reported Serial Number"
    */
    public static void fillDeviceKeyId(List<VTD1_Patient_Device__c> newDevices, Map<Id, VTD1_Patient_Device__c> oldMap) {
        List<VTD1_Patient_Device__c> devicesToProcess = new List<VTD1_Patient_Device__c>();
        for (VTD1_Patient_Device__c device :newDevices) {
            if(oldMap == null
                    || oldMap.get(device.Id).VTR5_IMEI__c != device.VTR5_IMEI__c
                    || oldMap.get(device.Id).VTD1_Vendor_Reported_Serial_Number__c != device.VTD1_Vendor_Reported_Serial_Number__c) {
                devicesToProcess.add(device);
            }
        }

        for (VTD1_Patient_Device__c device : devicesToProcess) {
            if(device.VTR5_IMEI__c != null) {
                device.VTR5_DeviceKeyId__c = device.VTR5_IMEI__c;
            } else if (device.VTD1_Vendor_Reported_Serial_Number__c != null) {
                device.VTR5_DeviceKeyId__c = device.VTD1_Vendor_Reported_Serial_Number__c;
            } else {
                device.VTR5_DeviceKeyId__c = null;
            }
        }
    }
  
 /* method : createPatientDeviceNotificationForRetakeReq(List<VTD1_Patient_Device__c> patientDeviceList)
* params : patientDeviceList param come from VT_D1_PatientDeviceTrigger in after update scenario
* Description :this method create notification for patient & caregiver whenever Patient/Caregiver receives request to retake a reading
*/ 
public static void createPatientDeviceNotificationForRetakeReq(List<VTD1_Patient_Device__c> patientDeviceList){
    notificationList=new List<VTD1_NotificationC__c>();


    set<id> ehrDeviceId = new set<id>();
    Map<Id,VTD1_Patient_Device__c> caseIdToPatientDeviceMap = new Map<Id,VTD1_Patient_Device__c>();
    for(VTD1_Patient_Device__c patientDevice :patientDeviceList){
        if(VT_R4_ConstantsHelper_Misc.isRetakeRequest && patientDevice.VTR5_SyncStatus__c =='Retake Requested' && 
            patientDevice.VTD1_Case__c != null &&  patientDevice.VTD1_Protocol_Device__c != null){
              ehrDeviceId.add(patientDevice.VTD1_Protocol_Device__c);
			  caseIdToPatientDeviceMap.put(patientDevice.VTD1_Case__c,patientDevice);
                 System.debug('-v--'+ehrDeviceId+'--hr----'+caseIdToPatientDeviceMap);
        }
    }


    if(!caseIdToPatientDeviceMap.isEmpty() && ehrDeviceId != null && ehrDeviceId.size()>0){ 
        Map<Id,HealthCloudGA__EhrDevice__c> ehrDeviceIdToEhrDeviceMap = new Map<Id,HealthCloudGA__EhrDevice__c>( 
                               [Select id ,VTD1_Device_Type__c from HealthCloudGA__EhrDevice__c where 
                                VTD1_Device__r.VTR5_IsContinous__c =false and VTD1_Device_Type__c != null and Id IN:ehrDeviceId WITH SECURITY_ENFORCED]);

            Map<Id,Case> caseIdToCaseMap = new Map<Id,Case>([Select id ,VTD1_Patient_User__c from case where VTD1_Patient_User__c != null and  Id IN:caseIdToPatientDeviceMap.keySet() WITH SECURITY_ENFORCED]);

              System.debug('-v--'+caseIdToCaseMap+'--hr----'+ehrDeviceIdToEhrDeviceMap);
        if(!ehrDeviceIdToEhrDeviceMap.isEmpty() && !caseIdToCaseMap.isEmpty()){
            List<VTD1_NotificationC__c> createNotificationList = new List<VTD1_NotificationC__c>(); 
            for(VTD1_Patient_Device__c patientDevice :caseIdToPatientDeviceMap.values()){

               
                if(caseIdToCaseMap.keySet().contains(patientDevice.VTD1_Case__c) && ehrDeviceIdToEhrDeviceMap.keySet().contains(patientDevice.VTD1_Protocol_Device__c)){
                     System.debug('---'+patientDevice);
                    VTD1_NotificationC__c notification = new  VTD1_NotificationC__c();
                    notification.Link_to_related_event_or_object__c = 'device?deviceId='+patientDevice.Id;
                    notification.HasDirectLink__c = true;
                    notification.VTD1_Receivers__c = caseIdToCaseMap.get(patientDevice.VTD1_Case__c).VTD1_Patient_User__c;
                    notification.OwnerId = caseIdToCaseMap.get(patientDevice.VTD1_Case__c).VTD1_Patient_User__c;
                    notification.Title__c = 'VTR5_StudyDevices';
                    notification.Type__c = 'General';
                    notification.Message__c ='VTR5_PatientDevice_Retake_Notification';
                    notification.VTD1_Parameters__c = getPicklistLabel('product2','VTD1_DeviceType__c', ehrDeviceIdToEhrDeviceMap.get(patientDevice.VTD1_Protocol_Device__c).VTD1_Device_Type__c);  // need to provide device type
                    notification.VTR5_Email_Body__c ='VTR5_PatientDevice_Retake_Notification';
                    notification.VTR5_Email_Body_Parameters__c =getPicklistLabel('product2','VTD1_DeviceType__c', ehrDeviceIdToEhrDeviceMap.get(patientDevice.VTD1_Protocol_Device__c).VTD1_Device_Type__c);  // need to provide device type
                    createNotificationList.add(notification);
                    
                }
            }
                 
                if(createNotificationList != null && createNotificationList.size()>0){
                    insert createNotificationList;  
                    notificationList.addAll(createNotificationList);
                    VT_R5_EmailGeneratorForTaskNotificationC.sendEmailNotification(Json.serialize(notificationList));
                      System.debug('---'+notificationList);
                }
          
        }
    }        
}


private static string getPicklistLabel(String ObjectApiName,String fieldApiName,String pickValue){ 
        String picklistLabel;

        Schema.SObjectType sObj = Schema.getGlobalDescribe().get(objectApiName) ;
        Schema.DescribeSObjectResult sObjResult = sObj.getDescribe() ;
        Map<String,Schema.SObjectField> fields = sObjResult.fields.getMap();
        Schema.DescribeFieldResult fieldResult = fields.get(fieldApiName).getDescribe();
        List<Schema.PicklistEntry> pickListVal = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry ple : pickListVal){  
            if(ple.getValue() ==pickValue){
	            picklistLabel=ple.getLabel();
              } 
        }
           return picklistLabel;
   }
   
  

}