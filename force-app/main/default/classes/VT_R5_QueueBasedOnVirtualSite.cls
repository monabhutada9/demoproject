/***********************************************************************
* @author              Akanksha Singh <akanksha.singh@quintiles.com>
* @date                07-Oct-2020


* @group               SH-17543
* @group-content       http://jira.quintiles.net/browse/SH-17543
* @description         create Queue on insertion of SSTM for related 'Virtual Site' object 
*                      and populate Queue Name to 'CRA Queue' field of 'Virtual Site' object  
*/
/***********************************************************************/

public without sharing class VT_R5_QueueBasedOnVirtualSite {

//Declare Variable to prevent unrequired 'after update' operation
    public static Boolean isAfterUpdate = true;



// Method to create Queue for inserted Virtual Site    
    public static void createQueueBasedOnVirtualSite(List<Virtual_Site__c> virsites) {

        try {   


            String queueName;
            Map<Id,Virtual_Site__c> mapOfVirSiteIdAndVirSiteRec = new Map<Id,Virtual_Site__c>();


            Map<Id, Group> queuesToInsertMap = new Map<Id,Group>();
        
    // Expected CRA Queue field of Virtual Site
            for(Virtual_Site__c virtualsiteRec : virsites ) {  


                queueName = virtualsiteRec.Id + '_CRAQueue';


                mapOfVirSiteIdAndVirSiteRec.put(virtualsiteRec.Id, virtualsiteRec); 
                Group queueObj = new Group();
                queueObj.Name = queueName;
                queueObj.Type='Queue';
                queuesToInsertMap.put(virtualsiteRec.Id, queueObj); 
            }
        
            if(queuesToInsertMap.size() > 0) {



                insert queuesToInsertMap.values();
                updateQueueinVirtualSite(queuesToInsertMap,mapOfVirSiteIdAndVirSiteRec);
            }




        } Catch(Exception ex){
            System.debug('Exception Meesage'+ex.getMessage()+'Line No:'+ex.getLineNumber());
            ErrorLogUtility.logException(ex, null, 'VT_R5_QueueBasedOnVirtualSite');
        }   
    }
    
// Method to update 'CRA Queue'field of Virtual Site with newly created Queue Id    
    public static void updateQueueinVirtualSite(Map<Id, Group> queuesToInsertMap,Map<Id,Virtual_Site__c> mapOfVirSiteIdAndVirSiteRec) {


        
        try{

            List<Virtual_Site__c> virSiteRec = new List<Virtual_Site__c>(mapOfVirSiteIdAndVirSiteRec.values());
            Map<Id,Virtual_Site__c> convertedmapofVirSiteIdToVirtualSite = new Map<Id,Virtual_Site__c>(mapOfVirSiteIdAndVirSiteRec);
            List<Virtual_Site__c> virtualSiteToUpdate = new List<Virtual_Site__c>();
            Map<Id,Group> mapOfVirSiteIdWithQueueRec = new Map<Id,Group>(queuesToInsertMap);

    // Assign Queue Id to CRA Queue field of related Virtual Site. 

            Id virId;


            for(Virtual_Site__c vs: [ SELECT Id,
                                    VTR5_CRA_Queue__c
                                    FROM Virtual_Site__c
                                    WHERE Id IN:mapOfVirSiteIdAndVirSiteRec.keyset() ])  { 


                virId = vs.Id;
                if(mapOfVirSiteIdWithQueueRec.containsKey(virId)) {
                    vs.VTR5_CRA_Queue__c = mapOfVirSiteIdWithQueueRec.get(virId).Id;
                    virtualSiteToUpdate.add(vs); 

                }
            }
        
            if(virtualSiteToUpdate.size() > 0) {

                if (VT_Utilities.isCreateable('Virtual_Site__c')) {
                    update virtualSiteToUpdate;
                    isAfterUpdate = false;
                }
            }
             


        }Catch(Exception ex){
            System.debug('Exception Meesage'+ex.getMessage()+'Line No:'+ex.getLineNumber());
            ErrorLogUtility.logException(ex, null, 'VT_R5_QueueBasedOnVirtualSite');
        }  
    }
}