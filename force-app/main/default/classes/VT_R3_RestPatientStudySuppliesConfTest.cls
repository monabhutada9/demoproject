@isTest
public with sharing class VT_R3_RestPatientStudySuppliesConfTest {

    public static void firstTest() {
        Case cas = [SELECT Id FROM Case WHERE RecordType.Name = 'CarePlan' LIMIT 1];

        String responseString;
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/Patient/StudySupplies/Confirm/error';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = response;

        Test.startTest();
        responseString = VT_R3_RestPatientStudySuppliesConfirm.getConfirmDetails();

        List<VTD1_Order__c> deliveryList = [SELECT Id, VTD1_Status__c, (SELECT Id, Name FROM Patient_Kits__r) FROM VTD1_Order__c WHERE VTD1_Case__c = :cas.Id];
        VTD1_Order__c delivery = deliveryList.get(0);
        RestContext.request.requestURI = '/Patient/StudySupplies/Confirm/' + delivery.Id;
        responseString = VT_R3_RestPatientStudySuppliesConfirm.getConfirmDetails();

        List<VTD1_Patient_Device__c> devices = new List<VTD1_Patient_Device__c>();
        List<VTD1_Patient_Accessories__c> patientAccessoriesList = new List<VTD1_Patient_Accessories__c>();

        VTD1_Patient_Device__c device;
        for (VTD1_Patient_Kit__c pc : delivery.Patient_Kits__r) {
            device = new VTD1_Patient_Device__c(VTD1_Patient_Kit__c = pc.Id, VTD1_Case__c = cas.Id);
            devices.add(device);
        }
        insert devices;
        for (VTD1_Patient_Device__c dev : [SELECT Id FROM VTD1_Patient_Device__c]) {
            patientAccessoriesList.add(new VTD1_Patient_Accessories__c(VTD1_Patient_Device__c = dev.Id, VTD1_Case__c = cas.Id));
        }
        insert patientAccessoriesList;
        responseString = VT_R3_RestPatientStudySuppliesConfirm.getConfirmDetails();

        RestContext.request.requestURI = '/Patient/StudySupplies/Confirm/a1q2a000000Vt2GAAS';
        responseString = VT_R3_RestPatientStudySuppliesConfirm.getConfirmDetails();
        Test.stopTest();
    }


    public static void secondTest() {
        Test.startTest();

        Case cas = [SELECT Id FROM Case WHERE RecordType.Name = 'CarePlan' LIMIT 1];
        String responseString;
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/Patient/StudySupplies/Confirm/err';
        request.httpMethod = 'GET';
        request.params.put('updateDelivery', 'true');
        request.params.put('kitContainsAll', 'true');
        request.params.put('kitContentsDamaged', 'true');
        RestContext.request = request;
        RestContext.response = response;

        responseString = VT_R3_RestPatientStudySuppliesConfirm.confirmDelivery();

        List<VTD1_Order__c> deliveryList = [SELECT Id, VTD1_Status__c, (SELECT Id, Name FROM Patient_Kits__r) FROM VTD1_Order__c WHERE VTD1_Case__c = :cas.Id];

        VTD1_Order__c delivery = deliveryList.get(0);
        RestContext.request.requestURI = '/Patient/StudySupplies/Confirm/' + delivery.Id;
        responseString = VT_R3_RestPatientStudySuppliesConfirm.confirmDelivery();

        VT_R3_RestPatientStudySuppliesConfirm.KitData kitData = new VT_R3_RestPatientStudySuppliesConfirm.KitData();
        kitData.containsAll = true;

        List<VT_R3_RestPatientStudySuppliesConfirm.KitData> kitDataList = new List<VT_R3_RestPatientStudySuppliesConfirm.KitData>();
        kitDataList.add(kitData);

        VT_R3_RestPatientStudySuppliesConfirm.DeliveryData deliveryData = new VT_R3_RestPatientStudySuppliesConfirm.DeliveryData();
        deliveryData.deliveryDate = (Datetime.now() + 1).getTime();
        deliveryData.comment = 'comment';
        deliveryData.kitData = kitDataList;


        RestContext.request.requestBody = Blob.valueof(JSON.serialize(deliveryData));
        responseString = VT_R3_RestPatientStudySuppliesConfirm.confirmDelivery();

        RestContext.request.params.put('updateDelivery', 'true');
        RestContext.request.params.put('kitContainsAll', 'false');
        RestContext.request.params.put('kitContentsDamaged', 'false');

        responseString = VT_R3_RestPatientStudySuppliesConfirm.confirmDelivery();

        RestContext.request.params.put('updateDelivery', 'false');
        RestContext.request.params.put('kitContainsAll', 'false');
        RestContext.request.params.put('kitContentsDamaged', 'false');

        responseString = VT_R3_RestPatientStudySuppliesConfirm.confirmDelivery();

        Test.stopTest();
    }
}