/**
 * Created by Rishat Shamratov on 22.10.2019.
 */

global class VT_R3_CustomLabelsRenderPageController {

    /**
     * Get custom labels by Tooling API request
     *
     * @return Map of strings "API name" to strings "Value"
     */
    public static Map<String, String> getCustomLabelsByMasterLabel() {
        // Get custom label master label (Short description) mask from address line
        String masterLabelMask = ApexPages.currentPage().getParameters().get('masterLabel');

        // Set Tooling API request
        String queryString = '?q=SELECT+Id,+Name,+MasterLabel,+Value+FROM+ExternalString+WHERE+MasterLabel+LIKE+' + masterLabelMask + '+AND+NamespacePrefix+=+null+ORDER+BY+Name';

        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setHeader('Authorization', 'Bearer ' + VT_D1_HelperClass.getSessionIdForGuest());
        req.setEndpoint(Url.getOrgDomainUrl().toExternalForm() + '/services/data/v46.0/tooling/query/' + queryString);
        System.debug('REQUEST: ' + req);

        // Send Tooling API request
        Http http = new Http();
        HttpResponse res = http.send(req);
        System.debug('RESPONSE: ' + res.getStatus() + ' ' + res.getBody());

        // Gather a map with custom label API names for getting their translations on the page
        Map<String, Object> resSer = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        Map<String, String> labelApiNamesMap = new Map<String, String>();
        List<Object> resSerRecords = (List<Object>) resSer.get('records');
        for (Object recObject : resSerRecords) {
            Map<String, Object> resSerRecordsMap = (Map<String, Object>) recObject;
            labelApiNamesMap.put(String.valueOf(resSerRecordsMap.get('Name')), String.valueOf(resSerRecordsMap.get('Value')));
        }

        return labelApiNamesMap;
    }

    /**
     * Get translated content of custom labels from the page
     *
     * @param lang String "language" for custom label translation on the page
     * @param masterLabel String "masterLabel" for custom label translation on the page
     *
     * @return Map of strings "API name" to strings "Translation"
     */
    public static Map<String, String> getLabelsString(String lang, String masterLabel) {
        // Set address line request and getting content string with custom label API names and their translations from Tooling API response
        String labelApiNamesAndTranslatedValuesString;
        try {
            PageReference pageRef = new PageReference('/apex/VT_R3_CustomLabelsRenderPage?lang=' + lang + '&masterLabel=' + masterLabel);
            // Apex tests can not work with PageReference.getContent()
            if (Test.isRunningTest()) {
                labelApiNamesAndTranslatedValuesString = 'VTR3_ChangePassword<:>Change Password<;>';
            } else {
                labelApiNamesAndTranslatedValuesString = pageRef.getContent().toString().trim();
            }
        } catch (Exception e) {
            System.debug(e);
        }

        // Edit the response string
        String regExp = '<;>(\\s+)';
        labelApiNamesAndTranslatedValuesString = labelApiNamesAndTranslatedValuesString.replaceAll(regExp, '<;>');

        // Gather a map with custom label API names and their translations
        Map<String, String> labelApiNamesAndTranslatedValuesMap = new Map<String, String>();
        List<String> labelApiNamesAndTranslatedValuesStringParts = labelApiNamesAndTranslatedValuesString.split('<;>');
        for (String label : labelApiNamesAndTranslatedValuesStringParts) {
            List<String> labelParams = label.split('<:>');
            String labelApiName = labelParams.get(0);
            String labelTranslatedValue = labelParams.get(1);
            labelApiNamesAndTranslatedValuesMap.put(labelApiName, labelTranslatedValue);
        }

        return labelApiNamesAndTranslatedValuesMap;
    }
}