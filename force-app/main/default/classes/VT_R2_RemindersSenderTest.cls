@isTest
private class VT_R2_RemindersSenderTest {

    public class VTD1_TokboxHelperCalloutMockImpl implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setStatusCode(200);
            Video_Conference__c conference = [select Id from Video_Conference__c];
            String body = JSON.serialize(new Set<Id>{conference.Id});
            response.setBody(body);
            return response;
        }
    }

    @testSetup
    static void setup() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }

    @isTest
    static void test() {
        Case cas = [select Id from Case];
        //Video_Conference__c conference = [select Id from Video_Conference__c];
        Test.setMock(HttpCalloutMock.class, new VTD1_TokboxHelperCalloutMockImpl());
        VTD1_ProtocolVisit__c pVisit = new VTD1_ProtocolVisit__c(VTR2_Visit_Participants__c = 'Patient; PI');
        insert pVisit;
        List <VTD1_Actual_Visit__c> listVisits = new List<VTD1_Actual_Visit__c>();
        VTD1_Actual_Visit__c visit = new VTD1_Actual_Visit__c(
                VTD1_Case__c = cas.Id,
                VTD1_Scheduled_Date_Time__c = System.now().addHours(1).addMinutes(1),
                VTD1_Status__c = 'Scheduled',
                VTD1_Protocol_Visit__c = pVisit.Id
        );
        listVisits.add(visit);
        Test.startTest();
        //insert visit;
        insert listVisits;

        VTR2_Reminders__mdt reminder = new VTR2_Reminders__mdt();
        reminder.Email_Template__c = 'ReminderPatientPSC1HourBefore';
        reminder.Hours__c = 1;
        reminder.Profile_Name__c = 'PatientCaregiver';
        reminder.TN_Cataloge_Notification__c = 'N052';
        reminder.Sub_Type__c = 'PSC';
        reminder.Sub_Type_Condition__c = 'equals';

        VTR2_Reminders__mdt reminder2 = new VTR2_Reminders__mdt();
        reminder2.Email_Template__c = 'ReminderExternal1HourBefore';
        reminder2.Hours__c = 1;
        reminder2.Profile_Name__c = 'Other External';

        VTR2_Reminders__mdt reminder3 = new VTR2_Reminders__mdt();
        reminder3.Email_Template__c = 'VTD2_1HourbeforePIEmailnotificationwhenvisithasbeenScheduled_NEW';
        reminder3.Hours__c = 1;
        reminder3.Profile_Name__c = 'Primary Investigator';
        reminder3.TN_Cataloge_Notification__c = 'N054';
        reminder3.Visit_Type__c = 'Study Team';
        reminder3.Visit_Type_Condition__c = 'equals';

        VTR2_Reminders__mdt reminder4 = new VTR2_Reminders__mdt();
        reminder4.Email_Template__c = 'VTR2_ReminderPatient1HourBefore';
        reminder4.Hours__c = 1;
        reminder4.Profile_Name__c = 'PatientCaregiver';
        reminder4.TN_Cataloge_Notification__c = 'N052';
        reminder4.Sub_Type__c = 'PSC';
        reminder4.Sub_Type_Condition__c = 'not equals';

        List<VTR2_Reminders__mdt> remindersList = new List<VTR2_Reminders__mdt>();
        remindersList.add(reminder);
        remindersList.add(reminder2);
        remindersList.add(reminder3);
        remindersList.add(reminder4);

        VT_R2_RemindersSender.reminders = remindersList;

        VT_R2_RemindersSender.sendReminders(new List <VT_R2_RemindersSender.ParamsHolder>{new VT_R2_RemindersSender.ParamsHolder(visit.Id, 1)});
        Test.stopTest();

        try{
            VT_R2_RemindersSender.reminders = null;
            VT_R2_RemindersSender.sendReminders(new List <VT_R2_RemindersSender.ParamsHolder>{new VT_R2_RemindersSender.ParamsHolder(visit.Id, 1)});

        }catch(Exception e){
            
        }
        /*VTD1_ProtocolVisit__c pVisit = new VTD1_ProtocolVisit__c(VTR2_Visit_Participants__c = 'Patient; Patient Guide; PI');
        insert pVisit;
        VTD1_Actual_Visit__c visit = new VTD1_Actual_Visit__c(
                VTD1_Case__c = cas.Id,
                //VTD1_Scheduled_Date_Time__c = System.now().addHours(1).addMinutes(1),
                VTD1_Status__c = 'To Be Scheduled',
                VTD1_Protocol_Visit__c = pVisit.Id
        );
        insert visit;
        visit.VTD1_Scheduled_Date_Time__c = System.now().addHours(1).addMinutes(1);
        visit.VTD1_Status__c = 'Scheduled';
        Test.startTest();
        update visit;
        Test.stopTest();
        VT_R2_RemindersSender.sendReminders(new List <VT_R2_RemindersSender.ParamsHolder>{new VT_R2_RemindersSender.ParamsHolder(visit.Id)});
        */
    }
}