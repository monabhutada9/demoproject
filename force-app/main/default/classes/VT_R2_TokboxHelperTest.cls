/**
 * Created by user on 25.09.2019.
 */
@IsTest
public with sharing class VT_R2_TokboxHelperTest {
    public class VTD1_TokboxHelperCalloutMockImpl implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setStatusCode(200);
            Video_Conference__c conference = [SELECT Id FROM Video_Conference__c];
            String body = JSON.serialize(new Map<String, Object>{ 'message' => new List<Id>{ conference.Id } });
            response.setBody(body);
            return response;
        }
    }
    @TestSetup
    private static void setupMethod() {
        Video_Conference__c conference = new Video_Conference__c(SessionId__c = 'qwerty');
        insert conference;
    }
    @IsTest
    public static void test() {
        Video_Conference__c conference = [SELECT Id FROM Video_Conference__c];
        Test.setMock(HttpCalloutMock.class, new VTD1_TokboxHelperCalloutMockImpl());
        VTD1_TokboxHelper.session(new Set<Id>{conference.Id});
    }
}