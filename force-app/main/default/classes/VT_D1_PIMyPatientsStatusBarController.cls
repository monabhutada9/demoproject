/**
 * @author ?
 * @author Ruslan Mullayanov
 */
public class VT_D1_PIMyPatientsStatusBarController {

    public static List<String> activeCaseStatusList = new List<String>{
            'Pre-Consent',
            'Consented',
            'Screened',
            'Washout/Run-In',
            'Active/Randomized'
    };
    public static List<String> notActiveCaseStatusList = new List<String>{
            'Does Not Qualify',
            'Screen Failure',
            'Washout/Run-In Failure',
            'Dropped',
            'Dropped Treatment,In F/Up',
            'Dropped Treatment,Compl F/Up',
            'Completed',
            'Compl Treatment,In F/Up',
            'Compl Treatment,Dropped F/Up'
    };

    @AuraEnabled(Cacheable=true)
    public static String getStatusBar(Id studyId) {
        try {
            Map<String, Integer> activeStatusToQuantityListMap = new Map<String, Integer>();
            Map<String, Integer> notActiveStatusToQuantityListMap = new Map<String, Integer>();
            Integer quantityActivePatients = 0;
            Integer quantityNotActivePatients = 0;

            List<Id> myStudyIds = VT_D1_PIStudySwitcherRemote.getMyStudyIds(studyId);
            String caseRecordTypeId = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN;
            Id currentUserId = UserInfo.getUserId();
            List<AggregateResult> arList = getAggregateResultsOfCases(caseRecordTypeId, currentUserId, myStudyIds);
            if (!arList.isEmpty()) {
                for (AggregateResult ar : arList) {
                    String status = String.valueOf(ar.get('Status'));
                    Integer quantity = Integer.valueOf(ar.get('Quantity'));
                    if (activeCaseStatusList.contains(status)) {
                        activeStatusToQuantityListMap.put(status, quantity);
                        quantityActivePatients += quantity;
                    } else if (notActiveCaseStatusList.contains(status)) {
                        notActiveStatusToQuantityListMap.put(status, quantity);
                        quantityNotActivePatients += quantity;
                    }
                }
            }
            return JSON.serialize(new PatientStatusBar(activeStatusToQuantityListMap, notActiveStatusToQuantityListMap, quantityActivePatients, quantityNotActivePatients));
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    private static List<PatientStatusItem> getNotActivePatientStatusList(Map<String, Integer> notActiveStatusToQuantityListMap, Integer quantityNotActivePatients) {
        List<PatientStatusItem> notActivePatientStatusList = new List<PatientStatusItem>();
        for (String status : notActiveStatusToQuantityListMap.keySet()) {
            PatientStatusItem patientStatusItem = new PatientStatusItem();
            patientStatusItem.status = status;
            patientStatusItem.quantity = notActiveStatusToQuantityListMap.get(status);
            Decimal percentageOfTotal = patientStatusItem.quantity * 100 / quantityNotActivePatients;
            patientStatusItem.percentageOfTotal = percentageOfTotal.round(System.RoundingMode.HALF_EVEN);
            notActivePatientStatusList.add(patientStatusItem);
        }
        return notActivePatientStatusList;
    }

    private static List<PatientStatusItem> getActivePatientStatusList(Map<String, Integer> activeStatusToQuantityListMap, Integer quantityActivePatients) {
        List<PatientStatusItem> activePatientStatusList = new List<PatientStatusItem>();
        for (String status : activeStatusToQuantityListMap.keySet()) {
            PatientStatusItem patientStatusItem = new PatientStatusItem();
            patientStatusItem.status = status;
            patientStatusItem.quantity = activeStatusToQuantityListMap.get(status);
            Decimal percentageOfTotal = patientStatusItem.quantity * 100 / quantityActivePatients;
            patientStatusItem.percentageOfTotal = percentageOfTotal.round(System.RoundingMode.HALF_EVEN);
            activePatientStatusList.add(patientStatusItem);
        }
        return activePatientStatusList;
    }

    private static List<AggregateResult> getAggregateResultsOfCases(String caseRecordTypeId, Id currentUserId, List<Id> myStudyIds) {
        return [
                SELECT Status
                        , COUNT(Id) Quantity
                FROM Case
                WHERE RecordTypeId = :caseRecordTypeId
                    AND (
                            VTD1_PI_user__c = :currentUserId
                                OR VTD1_Backup_PI_User__c = :currentUserId
                                OR VTR2_SiteCoordinator__c = :currentUserId
                                OR VTR2_Backup_Site_Coordinator__c = :currentUserId
                    )
                    AND VTD1_Study__c IN :myStudyIds
                GROUP BY Status
        ];
    }

    public class PatientStatusBar {
        public List<PatientStatusItem> activePatientStatusList;
        public List<PatientStatusItem> notActivePatientStatusList;
        public Integer quantityActivePatientStatusList;
        public Integer quantityNotActivePatientStatusList;

        public PatientStatusBar(Map<String, Integer> activeStatusToQuantityListMap, Map<String, Integer> notActiveStatusToQuantityListMap, Integer quantityActivePatients, Integer quantityNotActivePatients) {
            this.activePatientStatusList = getActivePatientStatusList(activeStatusToQuantityListMap, quantityActivePatients);
            this.quantityActivePatientStatusList = quantityActivePatients;
            this.notActivePatientStatusList = getNotActivePatientStatusList(notActiveStatusToQuantityListMap, quantityNotActivePatients);
            this.quantityNotActivePatientStatusList = quantityNotActivePatients;
        }
    }

    public class PatientStatusItem {
        public String status;
        public Integer quantity;
        public Decimal percentageOfTotal;
    }
}