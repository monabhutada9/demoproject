/**
 * @author: Aliaksandr Vabishchevich
 * @date: 29-Jul-2020
 * @description: Web Service for receiving Signant File requests from Mulesoft
 */

@RestResource(UrlMapping='/SignantJsonProcessingService/*')
global with sharing class VT_R5_SignantJsonProcessingService {
    @HttpPost
    global static void processJson() {
        RestRequest req = RestContext.request;
        Map<String, Object> parsedRequest = (Map<String, Object>) JSON.deserializeUntyped(req.requestBody.toString());

        List<VTR5_Signant_File__c> signantFiles = new List<VTR5_Signant_File__c>();
        for (Object sigObj : (List<Object>) parsedRequest.get('subjects')) {
            Map<String, Object> o = (Map<String, Object>) sigObj;

            signantFiles.add(new VTR5_Signant_File__c(
                    Name = String.valueOf(o.get('fileName')),
                    VTR5_XMLContent__c = String.valueOf(o.get('content')),
                    VTR5_DateReceived__c = Datetime.now()
            ));
        }

        Database.insert(signantFiles, false);
//        Database.executeBatch(new VT_R5_SignantFilesProcessingBatch(), 200);
    }
}