/**
 * @author: Dmitry Yartsev
 * @date: 27.04.2020
 * @description: Test class for VT_R4_PatientFilesDownloadBlocker
 */

@IsTest
public with sharing class VT_R4_PatientFilesDownloadBlockerTest {
    private static final String TESTED_CLASS_NAME = 'VT_R4_PatientFilesDownloadBlocker';
    private static final Id CONTENT_VERSION_ID = fflib_IDGenerator.generate(ContentVersion.getSObjectType());
    private static final List<Id> CONTENT_VERSION_IDS = new List<Id>{ CONTENT_VERSION_ID };
    private static final Id CONTENT_DOCUMENT_ID = fflib_IDGenerator.generate(ContentDocument.getSObjectType());
    private static final Id DOCUMENT_ID = fflib_IDGenerator.generate(VTD1_Document__c.getSObjectType());
    private static final String ALLOW_SOQL = 'SOQL context must be allowed';
    private static final String MUST_BE_FORBIDDEN = 'Downloading must be forbidden for the current user';
    private static final String MUST_BE_ALLOWED = 'Downloading must be allowed for the current user';

    public static void downloadFileSoql() {
        Sfc.ContentDownloadHandler handler = new VT_R4_PatientFilesDownloadBlocker()
                .getContentDownloadHandler(CONTENT_VERSION_IDS, Sfc.ContentDownloadContext.SOQL);
        System.assert(handler.isDownloadAllowed, ALLOW_SOQL);
    }

    public static void patientFileDownloadingIsNotAllowedForStudy() {
        getDocQueryStub().applyStub();
        getDocLinksQueryStub().applyStub();
        getFilteredDocsQueryStub(false).applyStub();

        System.runAs(getCRAUser()) {
            Sfc.ContentDownloadHandler handler = new VT_R4_PatientFilesDownloadBlocker()
                    .getContentDownloadHandler(CONTENT_VERSION_IDS, Sfc.ContentDownloadContext.CONTENT);
            System.assert(!handler.isDownloadAllowed, MUST_BE_FORBIDDEN);
        }
        System.runAs(getCMUser()) {
            Sfc.ContentDownloadHandler handler = new VT_R4_PatientFilesDownloadBlocker()
                    .getContentDownloadHandler(CONTENT_VERSION_IDS, Sfc.ContentDownloadContext.CONTENT);
            System.assert(!handler.isDownloadAllowed, MUST_BE_FORBIDDEN);
        }
        System.runAs(getPGUser()) {
            Sfc.ContentDownloadHandler handler = new VT_R4_PatientFilesDownloadBlocker()
                    .getContentDownloadHandler(CONTENT_VERSION_IDS, Sfc.ContentDownloadContext.CONTENT);
            System.assert(handler.isDownloadAllowed, MUST_BE_ALLOWED);
        }
    }

    public static void patientFileDownloadingIsAllowedForStudy() {
        getDocQueryStub().applyStub();
        getDocLinksQueryStub().applyStub();
        getFilteredDocsQueryStub(true).applyStub();

        System.runAs(getCRAUser()) {
            Sfc.ContentDownloadHandler handler = new VT_R4_PatientFilesDownloadBlocker()
                    .getContentDownloadHandler(CONTENT_VERSION_IDS, Sfc.ContentDownloadContext.CONTENT);
            System.assert(handler.isDownloadAllowed, MUST_BE_ALLOWED);
        }
        System.runAs(getCMUser()) {
            Sfc.ContentDownloadHandler handler = new VT_R4_PatientFilesDownloadBlocker()
                    .getContentDownloadHandler(CONTENT_VERSION_IDS, Sfc.ContentDownloadContext.CONTENT);
            System.assert(handler.isDownloadAllowed, MUST_BE_ALLOWED);
        }
        System.runAs(getPGUser()) {
            Sfc.ContentDownloadHandler handler = new VT_R4_PatientFilesDownloadBlocker()
                    .getContentDownloadHandler(CONTENT_VERSION_IDS, Sfc.ContentDownloadContext.CONTENT);
            System.assert(handler.isDownloadAllowed, MUST_BE_ALLOWED);
        }
    }

    private static User getCRAUser() {
        return getUserWithProfile('CRA');
    }

    private static User getCMUser() {
        return getUserWithProfile('Monitor');
    }

    private static User getPGUser() {
        return getUserWithProfile('Patient Guide');
    }

    private static User getUserWithProfile(String profileName) {
        String userName = VT_D1_TestUtils.generateUniqueUserName();
        return new User(
                ProfileId = VT_D1_HelperClass.getProfileIdByName(profileName),
                Username = userName,
                LastName = userName,
                Email = userName,
                Alias = 'Test',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                TimeZoneSidKey = 'America/Los_Angeles'
        );
    }

    private static QueryBuilder.StubbedQueryBuilder getDocQueryStub() {
        return new QueryBuilder()
                .buildStub()
                .addStubToSObject(new ContentVersion(ContentDocumentId = CONTENT_DOCUMENT_ID))
                .namedQueryStub(TESTED_CLASS_NAME + '.getDocumentIdFromCVId');
    }

    private static QueryBuilder.StubbedQueryBuilder getDocLinksQueryStub() {
        return new QueryBuilder()
                .buildStub()
                .addStubToList(new List<ContentDocumentLink>{
                        new ContentDocumentLink(LinkedEntityId = DOCUMENT_ID)
                })
                .namedQueryStub(TESTED_CLASS_NAME + '.getDocumentLinks');
    }

    private static QueryBuilder.StubbedQueryBuilder getFilteredDocsQueryStub(Boolean isDownloadAvailableForStudy) {
        return new QueryBuilder()
                .buildStub()
                .addStubToList(isDownloadAvailableForStudy
                        ? new List<VTD1_Document__c>()
                        : new List<VTD1_Document__c>{ new VTD1_Document__c() }
                )
                .namedQueryStub(TESTED_CLASS_NAME + '.getFilteredRelatedDocuments');
    }
}