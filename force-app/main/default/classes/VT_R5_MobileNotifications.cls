/**
 * @author: Alexander Komarov
 * @date: 16.04.2020
 * @description: service for mobile app, provides functionality for notifications
 *
 * ACTIVE VERSION => v1
 *
 */

public with sharing class VT_R5_MobileNotifications {
//        extends VT_R5_MobileRestResponse implements VT_R5_MobileRestRouter.Routable {
//    static Integer timeOffset;
//    private class MobileNotification {
//        public String id;
//        public String title;
//        public String message;
//        public String relatedObject;
//        public Long notificationDate;
//        public Boolean read;
//        public String visitId;
//        public String videoSessionId;
//        public String patientName;
//        public MobileNotification(VTD1_NotificationC__c serverNotificationC) {
//            this.id = serverNotificationC.Id;
//            this.title = serverNotificationC.Title__c;
//            this.message = serverNotificationC.Message__c;
//            this.notificationDate = serverNotificationC.CreatedDate.getTime();
//            this.read = serverNotificationC.VTD1_Read__c;
//            this.visitId = serverNotificationC.VTD2_VisitID__c;
//            if (serverNotificationC.Link_to_related_event_or_object__c.equals('detail/device-list')) {
//                this.relatedObject = 'device-list';
//            } else {
//                this.relatedObject = serverNotificationC.Link_to_related_event_or_object__c;
//            }
//            if (serverNotificationC.Type__c != null) {
//                if (serverNotificationC.Type__c.equals('Messages')) {
//                    this.relatedObject = 'messages';//TODO: finally do a picklist on object?
//                    if (serverNotificationC.Number_of_unread_messages__c > 1) {
//                        this.message = System.Label.VT_D1_YouHaveMessages
//                                + ' '
//                                + String.valueOf(serverNotificationC.Number_of_unread_messages__c)
//                                + ' '
//                                + System.Label.VT_D1_NewMessages;
//                    } else {
//                        this.message = System.Label.VT_D1_OneNewMessage;
//                    }
//                    if (!serverNotificationC.VTD1_Read__c) {
//                        this.notificationDate = serverNotificationC.VTD2_New_Message_Added_DateTme__c.getTime() + timeOffset;
//                    }
//                }
//            }
//        }
//    }
//    public List<String> getMapping() {
//        return new List<String>{
//                '/patient/{version}/notifications/'
//        };
//    }
//    public void versionRoute(Map<String, String> parameters) {
//        initService();
//        String requestVersion = parameters.get('version');
//        switch on this.request.httpMethod {
//            when 'GET' {
//                switch on requestVersion {
//                    when 'v1' {
//                        get_v1();
//                        return;
//                    }
//                }
//            }
//            when 'PATCH' {
//                switch on requestVersion {
//                    when 'v1' {
//                        patch_v1();
//                        return;
//                    }
//                }
//            }
//            when 'DELETE' {
//                switch on requestVersion {
//                    when 'v1' {
//                        delete_v1();
//                        return;
//                    }
//                }
//            }
//        }
//        VT_R5_MobileRestRouter.unsupportedVersionResponse();
//        this.addError('unsupported version');
//        this.sendResponse();
//    }
//    public void initService() {
//        timeOffset = UserInfo.getTimeZone().getOffset(System.now());
//    }
//    private void get_v1() {
//        List<VTD1_NotificationC__c> notificationCs = VT_D1_CustomNotificationsController.getNotificationsForCurrentUser();
//        try {
//            this.buildResponse(convertNotifications(notificationCs));
//        } catch (Exception e) {
//            this.showErrorBanner();
//            this.sendResponse(500, 'internal server error');
//            return;
//        }
//        this.sendResponse();
//    }
//    private void delete_v1() {
//        String originalRequestBody = request.requestBody.toString();
//        if (String.isBlank(originalRequestBody)) sendResponse(400, 'incorrect input');
//        Database.DeleteResult[] deleteResults = deleteNotifications(originalRequestBody);
//        this.processDatabaseResults(deleteResults);
//        this.sendResponse();
//    }
//    private void patch_v1() {
//        String originalRequestBody = request.requestBody.toString();
//        if (String.isBlank(originalRequestBody)) sendResponse(400, 'incorrect input');
//        Database.SaveResult[] updateResult = markNotificationsRead(originalRequestBody);
//        this.processDatabaseResults(updateResult);
//        this.sendResponse();
//    }
//    private Database.DeleteResult[] deleteNotifications(String originalRequestBody) {
//        if (String.isBlank(originalRequestBody)) sendResponse(400, 'incorrect input');
//        List<String> clearIds = getClearIds(originalRequestBody);
//        VTD1_NotificationC__c[] notesForDelete = [SELECT Id FROM VTD1_NotificationC__c WHERE Id IN :clearIds];
//        return Database.delete(notesForDelete, false);
//    }
//    private Database.SaveResult[] markNotificationsRead(String originalRequestBody) {
//        List<String> clearIds = getClearIds(originalRequestBody);
//        VTD1_NotificationC__c[] notificationsToUpdate = new List<VTD1_NotificationC__c>();
//        for (VTD1_NotificationC__c notification : [SELECT Id, VTD1_Read__c FROM VTD1_NotificationC__c WHERE Id IN :clearIds]) {
//            if (!notification.VTD1_Read__c) {
//                notificationsToUpdate.add(new VTD1_NotificationC__c(
//                        Id = notification.Id,
//                        VTD1_Read__c = true
//                ));
//            }
//        }
//        return Database.update(notificationsToUpdate, false);
//    }
//    private List<String> getClearIds(String originalRequestBody) {
//        List<String> clearIds = new List<String>();
//        for (String str : originalRequestBody.split(',')) {
//            str = str.remove(' ').remove('"').remove('\'');
//            clearIds.add(str);
//        }
//        return clearIds;
//    }
//    private static List<MobileNotification> convertNotifications(List<VTD1_NotificationC__c> dbNotifications) {
//        List<MobileNotification> result = new List<MobileNotification>();
//        Set<Id> dubbedNotifications = new Set<Id>();
//        Set<Id> notificationsIds = new Set<Id>();
//        for (VTD1_NotificationC__c dbNotification : dbNotifications) {
//            notificationsIds.add(dbNotification.Id);
//            if (String.isNotBlank(dbNotification.VTR2_PatientNotificationId__c)) {
//                dubbedNotifications.add(dbNotification.Id);
//            }
//        }
//        Map<Id, Id> actualVisitToNotification = new Map<Id, Id>();
//        for (VTD1_NotificationC__c n : [
//                SELECT Id, VTD2_VisitID__c
//                FROM VTD1_NotificationC__c
//                WHERE Id IN :notificationsIds
//        ]) {
//            if (String.isNotBlank(n.VTD2_VisitID__c)) {
//                actualVisitToNotification.put(n.VTD2_VisitID__c, n.Id);
//            }
//        }
//        Map<Id, String> notificationIdToVideoSession = new Map<Id, String>();
//        for (Video_Conference__c vc : [
//                SELECT Id, SessionId__c, VTD1_Actual_Visit__c
//                FROM Video_Conference__c
//                WHERE VTD1_Actual_Visit__c IN :actualVisitToNotification.keySet()
//        ]) {
//            if (String.isNotBlank(vc.SessionId__c)) {
//                notificationIdToVideoSession.put(actualVisitToNotification.get(vc.VTD1_Actual_Visit__c), vc.SessionId__c);
//            }
//        }
//        String patientName;
//        if (dubbedNotifications.size() > 0) {
//            Id patientAccount = [SELECT Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].Contact.AccountId;
//            patientName = [SELECT Name FROM Contact WHERE Id IN (SELECT ContactId FROM Case WHERE AccountId = :patientAccount) LIMIT 1].Name;
//        }
//        for (VTD1_NotificationC__c serverNotificationC : dbNotifications) {
//            MobileNotification convertedNotification = new MobileNotification(serverNotificationC);
//            if (dubbedNotifications.contains(serverNotificationC.Id)) {
//                convertedNotification.patientName = patientName;
//            }
//            if (notificationIdToVideoSession.containsKey(serverNotificationC.Id)) {
//                convertedNotification.videoSessionId = notificationIdToVideoSession.get(serverNotificationC.Id);
//            }
//            result.add(convertedNotification);
//        }
//        System.debug('_');
//        return result;
//    }

}