/**
 * Created by Dmitry Yartsev on 30.08.2020.
 */

@IsTest
private class VT_R5_GVEmailTemplateControllerTest {

    private static final Id CONF_ID = fflib_IDGenerator.generate(Video_Conference__c.getSObjectType());
    private static final Id CONF_MEMBER_ID = fflib_IDGenerator.generate(VTD1_Conference_Member__c.getSObjectType());
    private static final Id GENERAL_VISIT_MEMBER_ID = fflib_IDGenerator.generate(VTR4_General_Visit_Member__c.getSObjectType());

    @IsTest
    private static void testTelevisit() {
        applyConfMemberStub();
        VT_R5_GVEmailTemplateController controller = new VT_R5_GVEmailTemplateController();
        controller.relToId = CONF_MEMBER_ID;
        controller.recipId = UserInfo.getUserId();
        System.assertEquals(true, controller.isTelevisit);
        System.assertNotEquals(null, controller.externalMemberName);
        System.assertNotEquals(null, controller.generalVisitName);
        System.assertNotEquals(null, controller.participationReason);
        System.assertNotEquals(null, controller.scheduledDay);
        System.assertNotEquals(null, controller.scheduledDate);
        System.assertNotEquals(null, controller.scheduledTime);

        applyMetadataQueryStubForTelevisitUrl();
        System.assertNotEquals(null, controller.televisitUrl);
    }

    @IsTest
    private static void testNotTelevisit() {
        applyGenVisitMemberStub();
        VT_R5_GVEmailTemplateController controller = new VT_R5_GVEmailTemplateController();
        controller.relToId = GENERAL_VISIT_MEMBER_ID;
        controller.recipId = UserInfo.getUserId();
        System.assertEquals(false, controller.isTelevisit);
        System.assertNotEquals(null, controller.externalMemberName);
        System.assertNotEquals(null, controller.generalVisitName);
        System.assertNotEquals(null, controller.participationReason);
        System.assertNotEquals(null, controller.scheduledDay);
        System.assertNotEquals(null, controller.scheduledDate);
        System.assertNotEquals(null, controller.scheduledTime);
    }

    private static void applyConfMemberStub() {
        VTD1_Conference_Member__c member = new VTD1_Conference_Member__c(
            Id = CONF_MEMBER_ID,
            VTR4_General_Visit_Member__r = getGenVisitMember(),
            VTD1_Video_Conference__c = CONF_ID,
            VTD1_Video_Conference__r = new Video_Conference__c(
                Id = CONF_ID,
                SessionId__c = 'Test'
            )
        );
        VT_Stubber.applyStub('VT_R5_GVEmailTemplateController.confMember', new List<VTD1_Conference_Member__c>{ member });
    }

    private static void applyGenVisitMemberStub() {
        VTR4_General_Visit_Member__c member = getGenVisitMember();
        VT_Stubber.applyStub('VT_R5_GVEmailTemplateController.gVisitMember', new List<VTR4_General_Visit_Member__c>{ member });
    }

    private static VTR4_General_Visit_Member__c getGenVisitMember() {
        return new VTR4_General_Visit_Member__c(
            Id = GENERAL_VISIT_MEMBER_ID,
            VTR4_External_Member_Name__c = 'Test',
            VTR4_General_Visit__r = new VTR4_General_Visit__c(
                Name = 'Test',
                VTR4_Scheduled_Date_Time__c = Datetime.now()
            ),
            VTR4_ReasonForParticipation__c = 'Test'
        );
    }

    private static void applyMetadataQueryStubForTelevisitUrl() {
        new QueryBuilder()
            .buildStub()
            .addStubToList(new List<VTD1_General_Settings__mdt> {
            new VTD1_General_Settings__mdt(
                VTD1_Video_Heroku_App_Url__c = 'http://example.herokuapp.com',
                VTR5_VideoHerokuAesKey__c = '3AHWTi6vWtvWRQqP5xCXgAPWOB1TqwsQ1IxKlydHRrY='
            )
        })
        .namedQueryStub('ExternalVideoController.metadataQuery')
        .applyStub();
    }

}