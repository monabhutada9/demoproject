/**
 * Created by user on 04-Aug-20.
 */
@IsTest
public with sharing class VT_R5_StringConditionParserTest {
	public static void test() {
		Map<Integer, Boolean> testAnswerMap = new Map<Integer, Boolean>{
				1 => true,
				2 => false,
				3 => true,
				4 => false
		};
		System.assert(VT_R5_StringConditionParser.evaluate('1 and ( 2 or 3) ', testAnswerMap));
		System.assert(!VT_R5_StringConditionParser.evaluate('1 and ( 2 or 5) ', testAnswerMap));
		System.assert(!VT_R5_StringConditionParser.evaluate('1 and ( 2 or 5) \n ', testAnswerMap));
		System.assert(VT_R5_StringConditionParser.evaluate('1\n ', testAnswerMap));
		System.assert(!VT_R5_StringConditionParser.evaluate('1\n 2', testAnswerMap));
		System.assert(!VT_R5_StringConditionParser.evaluate('1 \r\n 2', testAnswerMap));
		System.assert(!VT_R5_StringConditionParser.evaluate('false'));
		System.assert(VT_R5_StringConditionParser.evaluate('true'));
		System.assert(VT_R5_StringConditionParser.evaluate('true and false or true'));
		System.assert(!VT_R5_StringConditionParser.evaluate('true  false or true'));
	}
}