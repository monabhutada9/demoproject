/*
 * Created by shume on 23.01.2019.
 */

public with sharing class VT_D2_Batch_ApexPushNotification implements Database.Batchable<SObject>,
        Database.AllowsCallouts {
    private Set<Id> notsIdSet;
    public VT_D2_Batch_ApexPushNotification(Set<Id> notsIdSet) {
        this.notsIdSet = notsIdSet;
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
                SELECT Id,
                        VTD1_Receivers__c,
                        Title__c,
                        Message__c,
                        VTD2_Title_Parameters__c,
                        VTD1_Parameters__c,
                        VTR2_PatientNotificationId__c,
                        Type__c,
                        Link_to_related_event_or_object__c,
                        Number_of_unread_messages__c,
                        VTD2_VisitID__c,
                        VTR3_Notification_Type__c,
                        OwnerId
                FROM VTD1_NotificationC__c
                WHERE Id IN:notsIdSet
        ]);
    }

    public void execute(Database.BatchableContext bc, List<VTD1_NotificationC__c> notificationList) {
        VT_D1_TranslateHelper.translate(notificationList, true);
        VT_D1_NotificationCHandler.sendPushNotificationsToMobileDevices(notificationList);
    }
    public void finish(Database.BatchableContext bc) {
    }
}