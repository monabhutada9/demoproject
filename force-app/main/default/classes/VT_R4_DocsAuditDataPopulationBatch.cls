/**
 * @description Populates the VTD1_Document__c.VTR4_AuditStudy__c, VTD1_Document__c.VTR4_AuditSite__c document
 * fields, using the following scheme:
 * document->case->site->study or document.oldSite->study or document->oldStudy
 *
 * @jira SH-11425 Create batch to populate Study & Virtual Site Direct Link on Patient-Level Documents
 * @author Created by Maksim Fedarenka on 21-May-20.
 */
//TODO run once and move to destructive changes with VT_R4_DocsAuditDataPopulationBatchTest
public with sharing class VT_R4_DocsAuditDataPopulationBatch implements Database.Batchable<SObject> {
	/**
	 * Launches the execution of the batch
	 * @param batchSize Number of records to be passed into the execute method for batch processing.
	 */
	public static void run(Integer batchSize) {
		Database.executeBatch(new VT_R4_DocsAuditDataPopulationBatch(), batchSize);
	}
 
	public Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator ([
				SELECT Id, VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__c,
					VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTD1_Study__c,
					VTD1_Site__r.Id, VTD1_Site__r.VTD1_Study__c, VTD1_Study__c, VTD2_Eligibility_Assesment_For__c
				FROM VTD1_Document__c
				WHERE VTR4_AuditStudy__c = NULL AND VTR4_AuditSite__c = NULL
		]);
	}

	public void execute(Database.BatchableContext bc, List<VTD1_Document__c> scope) {
		VT_R4_DocumentService.populateStudyAndVirtualSite(scope);
		update scope;
	}

	public void finish(Database.BatchableContext bc) {
		System.debug('VT_R4_DocsAuditDataPopulationBatch::finish');
	}
}