@IsTest
public class VT_D1_MedicalRecordsListTest {
//    @TestSetup in VT_R5_AllTests7


    public static void getRecordsTest() {
//        User piUser;
        List<Case> casesList = [SELECT VTD1_PI_user__c FROM Case];
        System.assert(casesList.size() > 0);
        if(casesList.size() > 0){
            Case cas = casesList.get(0);
//            String userId = cas.VTD1_PI_user__c;
//            piUser = [SELECT Id, ContactId FROM User WHERE Id =: userId];

            //String pcfId = VT_D1_CommunityChat.createPcf(cas.Id, 'subject', 'description', null, 'Possible');

            VTD1_Document__c doc = new VTD1_Document__c();
            doc.VTD1_Clinical_Study_Membership__c = cas.Id;
            doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD;
            insert doc;

            Test.startTest();

            ContentVersion contentVersion = new ContentVersion();
            contentVersion.PathOnClient = 'test.txt';
            contentVersion.Title = 'Test file';
            contentVersion.VersionData = Blob.valueOf('Test Data');
            insert contentVersion;

            ContentVersion contentVersion1 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id=:contentVersion.Id];

            ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
            contentDocumentLink.LinkedEntityId = doc.Id;
            contentDocumentLink.ContentDocumentId = contentVersion1.ContentDocumentId;
            contentDocumentLink.ShareType = 'I';
            contentDocumentLink.Visibility = 'AllUsers';
            insert contentDocumentLink;

            Case c = VT_D1_MedicalRecordsList.getCase();
            List<VTD1_Document__c> documentList = VT_D1_MedicalRecordsList.getRecords(cas.Id);
            ContentDocumentLink contentDocumentLink1 = VT_D1_MedicalRecordsList.getFileByRecord(doc.Id);
            ContentVersion contentVersion2 = VT_D1_MedicalRecordsList.getDocumentByRecord(doc.Id);
            VTD1_Document__c doc1 = VT_D1_MedicalRecordsList.getRecordByFile(contentVersion1.ContentDocumentId);
            //String siteURL = VT_D1_MedicalRecordsList.getSiteURL();

            VT_D1_MedicalRecordsList.certifyDoc(cas.Id, doc.Id);
            Test.stopTest();
        }
    }
}