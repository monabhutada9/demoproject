/**
 * Created by Alexander Komarov on 07.03.2019.
 */

public without sharing class VT_R2_PatientHistoryHandler implements Cache.CacheBuilder {
    public static List<VTR6_PatientFieldHistoryTemporary__c> historyTempRecords = new List<VTR6_PatientFieldHistoryTemporary__c>();
    private List<String> fieldsToTrack = new List<String>();                          // List of fields.
    private Map<Id, List<String>> mapIdToFieldsList = new Map<Id, List<String>>();    // SObjects Ids to Fields.
    private Map<Id, Id> mapOfRelevantObjects = new Map<Id, Id>();                     // SObject Id to Patient Id map.
    private static Map<Id, Id> objToPatientIdCache = new Map<Id, Id>();
    private static Set<Id> nonPatientIds = new Set<Id>();
    Map<String, List<String>> fieldsToTrackByObject = new Map<String, List<String>>();  // for test only

    public Object doLoad(String CacheKey) {
        String objectType = CacheKey.split('PatientHistoryHandler_')[1];
        List<String> fieldsToTrack = new List<String>();
        if (objectType.equalsIgnoreCase('User')) {
            String userFields = [
                    SELECT User_Fields__c
                    FROM VT_R2_Patient_History_Tracking_Settings__mdt
                    WHERE User_Fields__c != NULL
                    LIMIT 1
            ].User_Fields__c;
            fieldsToTrack.addAll(userFields.split(','));
        } else {
            List<VT_R2_Patient_History_Tracking_Settings__mdt> testing = [
                    SELECT Object__r.QualifiedApiName, Field__r.QualifiedApiName
                    FROM VT_R2_Patient_History_Tracking_Settings__mdt
                    WHERE Object__r.QualifiedApiName = :objectType
            ];
            for (VT_R2_Patient_History_Tracking_Settings__mdt mtd : testing) {
                fieldsToTrack.add(mtd.Field__r.QualifiedApiName);
            }
        }
        return fieldsToTrack;
    }

    public void onAfterInsert(List<SObject> recs) {
        loadFieldsSet(recs.get(0));                     // tracking fields of current object -> fieldsToTrack field
        getObjectsToPatientIdMap(recs);                 // SObject Id -> to patient id -> mapOfRelevantObjects field

        if (mapOfRelevantObjects != null) {
            for (SObject sobj : recs) {
                if (!mapOfRelevantObjects.containsKey(sobj.Id)) continue;
                String valueOfField = '';
                List<String> fieldsList = new List<String>();
                for (String field : fieldsToTrack) {
                    valueOfField = String.valueOf(sobj.get(field));
                    if (!String.isEmpty(valueOfField)) {
                        fieldsList.add(field);
                    }
                } if (!fieldsList.isEmpty()) {
                    mapIdToFieldsList.put(sobj.Id, fieldsList);
                }
            }
            if (!mapIdToFieldsList.isEmpty()) {
                processInsertOrDelete(recs, 'Insert');
            }
        }
    }

    public void onAfterUpdate(List<SObject> recs, Map<Id, SObject> oldMap) {
        System.debug('start CpuTime ' + Limits.getCpuTime());

        loadFieldsSet(recs.get(0));
        System.debug('after load CpuTime ' + Limits.getCpuTime());

        getObjectsToPatientIdMap(recs); //SObject Id -> to patient id
        System.debug('after get obj CpuTime ' + Limits.getCpuTime());

        List<String> fieldsList = new List<String>();

        if (mapOfRelevantObjects != null) {
            for (SObject sobj : recs) {
                if (!mapOfRelevantObjects.containsKey(sobj.Id)) continue;
                SObject oldObj = oldMap.get(sobj.Id);
                String valueOfFieldNew = '';
                String valueOfFieldOld = '';
                fieldsList.clear();

                for (String field : fieldsToTrack) {
                    valueOfFieldNew = String.valueOf(sobj.get(field));
                    valueOfFieldOld = String.valueOf(oldObj.get(field));
                    if (!String.isEmpty(valueOfFieldOld)) {
                        if (!String.isEmpty(valueOfFieldNew)) {
                            if (!valueOfFieldNew.equalsIgnoreCase(valueOfFieldOld)) {
                                fieldsList.add(field);
                            }
                        } else {
                            fieldsList.add(field);
                        }
                    } else {
                        if (!String.isEmpty(valueOfFieldNew)) {
                            fieldsList.add(field);
                        }
                    }
                }
                if (!fieldsList.isEmpty()) {
                    mapIdToFieldsList.put(sobj.Id, fieldsList);
                }
            }
            System.debug('before process CpuTime ' + Limits.getCpuTime());

            if (!mapIdToFieldsList.isEmpty()) {
                processUpdate(recs, oldMap);
            }
        }
        System.debug('after all CpuTime ' + Limits.getCpuTime());
    }

    public void onBeforeDelete(List<SObject> recs) {
        loadFieldsSet(recs.get(0));
        getObjectsToPatientIdMap(recs); //SObject Id -> to patient id

        if (mapOfRelevantObjects != null) {
            for (SObject sobj : recs) {

                if (!mapOfRelevantObjects.containsKey(sobj.Id)) continue;
                String valueOfField = '';
                List<String> fieldsList = new List<String>();
                for (String field : fieldsToTrack) {

                    valueOfField = String.valueOf(sobj.get(field));
                    if (!String.isEmpty(valueOfField)) {
                        fieldsList.add(field);
                    }
                } if (!fieldsList.isEmpty()) {
                    mapIdToFieldsList.put(sobj.Id, fieldsList);
                }
            }
            if (!mapIdToFieldsList.isEmpty()) {
                processInsertOrDelete(recs, 'Delete');
            }
        }
    }

    private void processUpdate(List<SObject> recs, Map<Id, SObject> oldMap) {
        List<VTR6_PatientFieldHistoryTemporary__c> historyToInsert = new List<VTR6_PatientFieldHistoryTemporary__c>();

        for (SObject obj : recs) {
            if (mapIdToFieldsList.containsKey(obj.Id)) {
                String objType = obj.getSObjectType().getDescribe().getName();
                for (String field : mapIdToFieldsList.get(obj.Id)) {
                    VTR6_PatientFieldHistoryTemporary__c pfh = new VTR6_PatientFieldHistoryTemporary__c();
                    pfh.VTR6_ObjectType__c = objType;
                    pfh.VTR6_Object_Id__c = obj.Id;
                    pfh.VTR6_Field__c = field;
                    pfh.VTR6_OldValue__c = String.valueOf(oldMap.get(obj.Id).get(field)); // update
                    pfh.VTR6_NewValue__c = String.valueOf(obj.get(field));
                    pfh.VTR6_OperationType__c = 'Update';
                    pfh.VTR6_PatientId__c = mapOfRelevantObjects.get(obj.Id);
                    pfh.VTR6_IsDeleted__c = false;
                    pfh.VTR6_DateofChange__c = System.now();
                    pfh.VTR6_Hash__c = generateHash(objType, field);
                    pfh.VTR6_UserId__c = UserInfo.getUserId();
                    historyToInsert.add(pfh);
                }
            }

        }
        if (!historyToInsert.isEmpty()) {
            if (System.isBatch() && VT_R4_PatientConversion.patientConversionInProgress) {
                historyTempRecords.addAll(historyToInsert);
            } else {
                insert historyToInsert;
            }
        }
    }

    private void processInsertOrDelete(List<SObject> recs, String operation) {
        List<VTR6_PatientFieldHistoryTemporary__c> historyToInsert = new List<VTR6_PatientFieldHistoryTemporary__c>();
//        System.debug('processInsertOrDelete* ' + recs + ' - ' + operation);

        for (SObject obj : recs) {
            System.debug('obj.Id = ' + obj.Id + ' ' + mapIdToFieldsList.get(obj.Id));
            if (mapIdToFieldsList.containsKey(obj.Id)) {
                for (String field : mapIdToFieldsList.get(obj.Id)) {
                    String objType = getTypeOfObject(obj);
                    VTR6_PatientFieldHistoryTemporary__c pfh = new VTR6_PatientFieldHistoryTemporary__c();
                    pfh.VTR6_ObjectType__c = objType;
                    pfh.VTR6_Object_Id__c = obj.Id;
                    pfh.VTR6_Field__c = field;
                    pfh.VTR6_OperationType__c = operation;
                    pfh.VTR6_PatientId__c = mapOfRelevantObjects.get(obj.Id);
                    pfh.VTR6_DateofChange__c = System.now();
                    pfh.VTR6_Hash__c = generateHash(objType, field);
                    pfh.VTR6_UserId__c = UserInfo.getUserId();
                    if (operation.equals('Insert')) {
                        pfh.VTR6_OldValue__c = null;
                        pfh.VTR6_NewValue__c = String.valueOf(obj.get(field));
                        pfh.VTR6_IsDeleted__c = false;
                    } else if (operation.equals('Delete')) {
                        pfh.VTR6_OldValue__c = String.valueOf(obj.get(field));
                        pfh.VTR6_NewValue__c = null;
                        pfh.VTR6_IsDeleted__c = true;
                    }
                    historyToInsert.add(pfh);
                }
            }
        }

        if (!historyToInsert.isEmpty()) {
            if (System.isBatch() && VT_R4_PatientConversion.patientConversionInProgress) {
                System.debug('processInsertOrDelete* isBatch' + System.isBatch() + ' - ' + VT_R4_PatientConversion.patientConversionInProgress);
                historyTempRecords.addAll(historyToInsert);
            } else {
                insert historyToInsert;
            }
        }
    }

    private String getTypeOfObject(SObject so) {
        return so.getSObjectType().getDescribe().getName();
    }

    private void getObjectsToPatientIdMap(List<SObject> objects) {
        List<SObject> possiblePatientRelatedObjs = new List<SObject>();
        for (SObject obj : objects) {
            if (!nonPatientIds.contains(obj.Id)) {
                possiblePatientRelatedObjs.add(obj);
            }
        }

        if (!possiblePatientRelatedObjs.isEmpty() &&
                !objToPatientIdCache.keySet().containsAll(new Map<Id, SObject>(possiblePatientRelatedObjs).keySet()))
        {
            queryObjects(possiblePatientRelatedObjs);
        }

        Map<Id, Id> mapObjectsToPatientId = new Map<Id, Id>();
        for (SObject obj : objects) {
            if (objToPatientIdCache.containsKey(obj.Id)) {
                mapObjectsToPatientId.put(obj.Id, objToPatientIdCache.get(obj.Id));
            }
        }
        this.mapOfRelevantObjects = mapObjectsToPatientId.size() > 0 ? mapObjectsToPatientId : null;
    }

    private void queryObjects(List<SObject> objects) {
        SObject instance = objects.get(0);

        if (instance instanceof Contact) {
            for (Contact con : [
                    SELECT Id, RecordType.Name
                    FROM Contact
                    WHERE Id IN :objects
                    AND RecordType.Name = 'Patient'
            ]) {
                objToPatientIdCache.put(con.Id, con.Id);
            }
        } else if (instance instanceof Address__c) {
            for (Address__c address : [
                    SELECT Id, VTD1_Contact__c, VTD1_Contact__r.RecordType.Name
                    FROM Address__c
                    WHERE Id IN :objects
                    //         AND VTD1_Contact__r.RecordType.Name = 'Patient'
            ]) {
                if (address.VTD1_Contact__r.RecordType.Name == 'Patient') {
                    objToPatientIdCache.put(address.Id, address.VTD1_Contact__c);
                } else {
                    nonPatientIds.add(address.Id);
                }
            }
        } else if (instance instanceof Account) {
            for (Account acc : [
                    SELECT Id, VTD2_Patient_s_Contact__c, VTD2_Patient_s_Contact__r.RecordTypeId
                    FROM Account
                    WHERE Id IN :objects
                    //       AND VTD2_Patient_s_Contact__r.RecordTypeId = :VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PATIENT
            ]) {
                if (acc.VTD2_Patient_s_Contact__r.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PATIENT) {
                    objToPatientIdCache.put(acc.Id, acc.VTD2_Patient_s_Contact__c);
                } else {
                    nonPatientIds.add(acc.Id);
                }
            }
        } else if (instance instanceof Case) {
            for (SObject sobj : objects) {
                Case cas = (Case) sobj;
                if (cas.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN) {
                    objToPatientIdCache.put(cas.Id, cas.ContactId);
                }
            }
        } else if (instance instanceof VT_D1_Phone__c) {
            for (SObject sobj : objects) {
                VT_D1_Phone__c phone = (VT_D1_Phone__c) sobj;
                if (phone.VTR4_ContactType__c == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
                    objToPatientIdCache.put(phone.Id, phone.VTD1_Contact__c);
                } else {
                    nonPatientIds.add(phone.Id);
                }
            }
        } else if (instance instanceof User) {
            for (SObject sobj : objects) {
                User user = (User) sobj;
                if (user.VTD1_Profile_Name__c == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
                    objToPatientIdCache.put(user.Id, user.ContactId);
                } else {
                    nonPatientIds.add(user.Id);
                }
            }

        } else if (instance instanceof VTD1_Physician_Information__c) {
            for (VTD1_Physician_Information__c pi : [
                    SELECT Id, VTD1_Clinical_Study_Membership__r.ContactId, VTD1_Clinical_Study_Membership__r.Contact.RecordType.Id
                    FROM VTD1_Physician_Information__c
                    WHERE Id IN :objects
                    //       AND VTD1_Clinical_Study_Membership__r.Contact.RecordType.Id = :VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PATIENT
            ]) {
                if (pi.VTD1_Clinical_Study_Membership__r.Contact.RecordType.Id == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PATIENT) {
                    objToPatientIdCache.put(pi.Id, pi.VTD1_Clinical_Study_Membership__r.ContactId);
                } else {
                    nonPatientIds.add(pi.Id);
                }
            }
        }
    }

    private void loadFieldsSet(SObject so) {
        //form set of fields to track
        String objectType = getTypeOfObject(so);
        String key = 'PatientHistoryHandler_' + objectType;

        // fix for error with creating users in SeeAllData tests. Vlad Tyazhov
        // OrgCacheException: Failed Cache.Org.get(): Org cache is only supported in Data Silo tests
        if (!Test.isRunningTest()) {
            this.fieldsToTrack = (List<String>) Cache.Org.get(VT_R2_PatientHistoryHandler.class, key);

        } else if (fieldsToTrackByObject.get(objectType) != null) {  // for tests only - store in attr instead of cache
            this.fieldsToTrack = fieldsToTrackByObject.get(objectType);

        } else {
            this.fieldsToTrack = (List<String>) doLoad(key);
            fieldsToTrackByObject.put(objectType, this.fieldsToTrack.clone());
        }
    }

    private String generateHash(String o, String s) {
        Blob targetBlob = Blob.valueOf(o + s);
        Blob hash = Crypto.generateDigest('MD5', targetBlob);
        return EncodingUtil.base64Encode(hash);
    }
}