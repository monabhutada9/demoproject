public with sharing class VT_D1_PIMyPatientDocumentsController {

    @AuraEnabled
    public static PatientDocumentsWithAdditionalInformation getPatientDocuments(String caseId) {
        MyPatientDocumentsFactory factory = new MyPatientDocumentsFactory();
        factory.addAlgorithm(new PiPatientDocumentsSelector());
        factory.addAlgorithm(new ScrPatientDocumentsSelector());
        return factory.getPatientDocuments(caseId);
    }

    @AuraEnabled
    public static Boolean updateDocumentsStatuses(List<VTD1_Document__c> documents) {
        MyPatientDocumentsFactory factory = new MyPatientDocumentsFactory();
        factory.addAlgorithm(new PiPatientDocumentsSelector());
        factory.addAlgorithm(new ScrPatientDocumentsSelector());
        return factory.updateDocumentsStatuses(documents);
    }

    @AuraEnabled
    public static ContentVersion getDocumentByRecord(Id recordId) {
        MyPatientDocumentsFactory factory = new MyPatientDocumentsFactory();
        factory.addAlgorithm(new PiPatientDocumentsSelector());
        factory.addAlgorithm(new ScrPatientDocumentsSelector());
        return factory.getDocumentByRecord(recordId);
    }

    @AuraEnabled
    public static Boolean ifThereIsPendingApprovalProcess(Id caseId) {
        MyPatientDocumentsFactory factory = new MyPatientDocumentsFactory();
        factory.addAlgorithm(new PiPatientDocumentsSelector());
        factory.addAlgorithm(new ScrPatientDocumentsSelector());
        return factory.ifThereIsPendingApprovalProcess(caseId);
    }

    @AuraEnabled
    public static String getSiteURL() {
        return Url.getSalesforceBaseUrl().toExternalForm();
    }

    @AuraEnabled
    public static Boolean isSandbox(){
        return String.isNotEmpty(VT_D1_HelperClass.getSandboxPrefix());
    }

    @AuraEnabled
    public static void approveMedicalRecords(List<VTD1_Document__c> documents){
        List<Approval.ProcessWorkitemRequest> appList = new List<Approval.ProcessWorkitemRequest>();
        Map<Id,Id> docToApprovalMap = new Map<Id, Id>();
        List<ProcessInstanceWorkItem> appProcessList = [SELECT Id, ProcessInstance.TargetObjectId,OriginalActorId  FROM ProcessInstanceWorkItem WHERE ProcessInstance.TargetObjectId IN :documents];

        for(ProcessInstanceWorkItem pwi: appProcessList){
            if(pwi.OriginalActorId == UserInfo.getUserId() || Test.isRunningTest()) {
                docToApprovalMap.put(pwi.ProcessInstance.TargetObjectId, pwi.Id);
            }
        }

        for(VTD1_Document__c doc :documents) {
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            req.setComments('Approved by SCR');
            req.setAction('Approve');
            Id workItemId = docToApprovalMap.get(doc.Id);
            if(workItemId == null){
                doc.addError('Error Occured in Trigger');
            }
            else{
                req.setWorkitemId(workItemId);
                appList.add(req);
            }
        }
        List<Approval.ProcessResult> result =  Approval.process(appList);
    }

    interface MyPatientDocumentsSelector {

        Boolean isSuitableProfile();

        PatientDocumentsWithAdditionalInformation getPatientDocuments(Id caseId);

        Boolean updateDocumentsStatuses(List<VTD1_Document__c> documents);

        ContentVersion getDocumentByRecord(Id recordId);

        Boolean ifThereIsPendingApprovalProcess(Id caseId);
    }

    public abstract class ProfileMyPatientDocumentsSelector implements MyPatientDocumentsSelector {

        protected abstract Id getProfileId();

        protected abstract PatientDocumentsWithAdditionalInformation getPatientDocuments(Id caseId);

        protected abstract Boolean updateDocumentsStatuses(List<VTD1_Document__c> documents);

        protected abstract ContentVersion getDocumentByRecord(Id recordId);

        protected abstract Boolean ifThereIsPendingApprovalProcess(Id caseId);

        public Boolean isSuitableProfile() {
            return UserInfo.getProfileId() == this.getProfileId();
        }
    }

    public class MyPatientDocumentsFactory {

        public List<MyPatientDocumentsSelector> algorithms;

        public MyPatientDocumentsFactory() {
            this.algorithms = new List<MyPatientDocumentsSelector>();
        }

        public PatientDocumentsWithAdditionalInformation getPatientDocuments(String caseId) {
            PatientDocumentsWithAdditionalInformation result;
            for (MyPatientDocumentsSelector algorithm : this.algorithms) {
                if (algorithm.isSuitableProfile()) {
                    result = algorithm.getPatientDocuments(caseId);
                    break;
                }
            }
            return result;
        }

        public Boolean updateDocumentsStatuses(List<VTD1_Document__c> documents) {
            Boolean isSuccess;
            for (MyPatientDocumentsSelector algorithm : this.algorithms) {
                if (algorithm.isSuitableProfile()) {
                    isSuccess = algorithm.updateDocumentsStatuses(documents);
                    break;
                }
            }
            return isSuccess;
        }

        public ContentVersion getDocumentByRecord(Id recordId) {
            ContentVersion contentVersion;
            for (MyPatientDocumentsSelector algorithm : this.algorithms) {
                if (algorithm.isSuitableProfile()) {
                    contentVersion = algorithm.getDocumentByRecord(recordId);
                    break;
                }
            }
            return contentVersion;
        }

        public Boolean ifThereIsPendingApprovalProcess(Id caseId) {
            Boolean hasProcess;
            for (MyPatientDocumentsSelector algorithm : this.algorithms) {
                if (algorithm.isSuitableProfile()) {
                    hasProcess = algorithm.ifThereIsPendingApprovalProcess(caseId);
                    break;
                }
            }
            return hasProcess;
        }

        public void addAlgorithm(MyPatientDocumentsSelector MyPatientDocuments) {
            this.algorithms.add(MyPatientDocuments);
        }
    }

    public without sharing class PiPatientDocumentsSelector extends ProfileMyPatientDocumentsSelector {

        protected override Id getProfileId() {
            return getPiProfileId();
        }

        public override PatientDocumentsWithAdditionalInformation getPatientDocuments(Id caseId) {
            List<Case> cases = getCaseById(caseId);
            if (cases.isEmpty()) {
                return null;
            }
            Case caseRecord = cases[0];
            List<VTD1_Document__c> docs = getDocuments(caseRecord);
            return new PatientDocumentsWithAdditionalInformation(docs, caseRecord);
        }

        public override Boolean updateDocumentsStatuses(List<VTD1_Document__c> documents) {
            try {
                List<Database.SaveResult> saveResults = Database.update(documents);
                Boolean isSuccess;
                for (Database.SaveResult saveResult : saveResults) {
                    if (!saveResult.isSuccess()) {
                        isSuccess = saveResult.isSuccess();
                        break;
                    } else {
                        isSuccess = saveResult.isSuccess();
                    }
                }
                return isSuccess;
            } catch (Exception exc) {
                throw new AuraHandledException(exc.getMessage());
            }
        }

        public override ContentVersion getDocumentByRecord(Id recordId) {
            return VT_D1_MedicalRecordsList.getDocumentByRecord(recordId);
        }

        public override Boolean ifThereIsPendingApprovalProcess(Id caseId) {
            List<ProcessInstance> processInstances = getProcessInstances(caseId);
            Boolean hasProcess = !(processInstances.isEmpty());
            return hasProcess;
        }

        private List<VTD1_Document__c> getDocuments(Case caseRecord) {
            List<VTD1_Document__c> docs = [
                    SELECT
                            Id,
                            VTD1_FileNames__c,
                            VTD1_Nickname__c,
                            VTD1_Comment__c,
                            VTD1_Comments__c,
                            VTD1_Version__c,
                            VTD1_Status__c,
                            VTD1_Deletion_Reason__c,
                            VTD1_Does_file_needs_deletion__c,
                            VTD1_Why_file_is_irrelevant__c,
                            VTD1_PI_Comment__c,
                            VTD1_PG_Approved__c,
                            VTD1_Why_File_is_Relevant__c,
                            VTD1_IsArchived__c,
                            VTD1_Archived_Date_Stamp__c,
                            RecordType.Name
                            , (
                            SELECT
                                    ContentDocumentId
                                    , ContentDocument.LatestPublishedVersion.VersionNumber
                            FROM ContentDocumentLinks
                            LIMIT 1
                    )
                    FROM VTD1_Document__c
                    WHERE VTD1_Clinical_Study_Membership__c = :caseRecord.Id
                    AND (RecordType.DeveloperName IN ('VTD1_Medical_Record', 'Medical_Record_Rejected'))
                    AND (VTD1_Deletion_Reason__c != 'Relevant, Uploaded in Error')
                    ORDER BY VTD1_Status__c
            ];
            return docs;
        }

        private List<Case> getCaseById(Id caseId) {
            return [
                    SELECT Id
                            , VTD1_Study__r.Name
                            , Contact.Name
                    FROM Case
                    WHERE Id = :caseId
            ];
        }

        private List<ProcessInstance> getProcessInstances(Id caseId) {
            return [
                    SELECT
                            Id
                            , TargetObjectId
                            , IsDeleted
                            , Status
                    FROM ProcessInstance
                    WHERE
                    IsDeleted = FALSE
                    AND TargetObjectId = :caseId
                    AND Status = 'Pending'
                    ORDER BY CreatedDate
                            DESC
                    LIMIT 1
            ];
        }

        private Id getPiProfileId() {
            return [SELECT Id FROM Profile WHERE Name = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME][0].Id;
        }
    }

    public without sharing class ScrPatientDocumentsSelector extends ProfileMyPatientDocumentsSelector {

        protected override Id getProfileId() {
            return getScrProfileId();
        }

        public override PatientDocumentsWithAdditionalInformation getPatientDocuments(Id caseId) {
            List<Case> cases = getCaseById(caseId);
            if (cases.isEmpty()) {
                return null;
            }
            Case caseRecord = cases[0];
            List<VTD1_Document__c> docs = getDocuments(caseRecord);

            return new PatientDocumentsWithAdditionalInformation(docs, caseRecord);
        }

        public override Boolean updateDocumentsStatuses(List<VTD1_Document__c> documents) {
            try {
                List<Database.SaveResult> saveResults = Database.update(documents);
                Boolean isSuccess;
                for (Database.SaveResult saveResult : saveResults) {
                    if (!saveResult.isSuccess()) {
                        isSuccess = saveResult.isSuccess();
                        break;
                    } else {
                        isSuccess = saveResult.isSuccess();
                    }
                }
                return isSuccess;
            } catch (Exception exc) {
                throw new AuraHandledException(exc.getMessage());
            }
        }

        public override ContentVersion getDocumentByRecord(Id recordId) {
            return VT_D1_MedicalRecordsList.getDocumentByRecord(recordId);
        }

        public override Boolean ifThereIsPendingApprovalProcess(Id caseId) {
            List<ProcessInstance> processInstances = getProcessInstances(caseId);
            Boolean hasProcess = !(processInstances.isEmpty());
            return hasProcess;
        }

        private List<VTD1_Document__c> getDocuments(Case caseRecord) {
            List<VTD1_Document__c> docs = [
                    SELECT
                            Id,
                            VTD1_FileNames__c,
                            VTD1_Nickname__c,
                            VTD1_Comment__c,
                            VTD1_Comments__c,
                            VTD1_Version__c,
                            VTD1_Status__c,
                            VTD1_Deletion_Reason__c,
                            VTD1_Does_file_needs_deletion__c,
                            VTD1_Why_file_is_irrelevant__c,
                            VTD1_PI_Comment__c,
                            VTD1_PG_Approved__c,
                            VTD1_Why_File_is_Relevant__c,
                            VTD1_IsArchived__c,
                            VTD1_Archived_Date_Stamp__c,
                            RecordType.Name
                            , (
                            SELECT
                                    ContentDocumentId
                                    , ContentDocument.LatestPublishedVersion.VersionNumber
                            FROM ContentDocumentLinks
                            LIMIT 1
                    )
                    FROM VTD1_Document__c
                    WHERE VTD1_Clinical_Study_Membership__c = :caseRecord.Id
                    AND (RecordType.DeveloperName IN ('VTD1_Medical_Record', 'Medical_Record_Rejected'))
                    AND (VTD1_Deletion_Reason__c != 'Relevant, Uploaded in Error')
                    ORDER BY VTD1_Status__c
            ];
            return docs;
        }

        private List<ProcessInstance> getProcessInstances(Id caseId) {
            return [
                    SELECT
                            Id
                            , TargetObjectId
                            , IsDeleted
                            , Status
                    FROM ProcessInstance
                    WHERE
                    IsDeleted = FALSE
                    AND TargetObjectId = :caseId
                    AND Status = 'Pending'
                    ORDER BY CreatedDate
                            DESC
                    LIMIT 1
            ];
        }

        private List<Case> getCaseById(Id caseId) {
            return [
                    SELECT Id
                            , VTD1_Study__r.Name
                            , Contact.Name
                    FROM Case
                    WHERE Id = :caseId
            ];
        }

        private Id getScrProfileId() {
            return [SELECT Id FROM Profile WHERE Name = :VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME][0].Id;
        }
    }

    public class PatientDocument {
        @AuraEnabled public VTD1_Document__c document;
        @AuraEnabled public String downloadUrl;
        @AuraEnabled public Boolean showStatusDetailInformation;
        @AuraEnabled public String archivedDate;
        @AuraEnabled public String version;

        public PatientDocument(VTD1_Document__c doc) {
            String sandboxPrefix = Url.getCurrentRequestUrl().getPath().toLowerCase().split('/')[1];
            if (sandboxPrefix == 'pi' || sandboxPrefix == 'scr'){
                sandboxPrefix = '/' + sandboxPrefix;
            }
            else{
                sandboxPrefix = '';
            }
            this.document = doc;
            this.showStatusDetailInformation = false;
            if (doc.ContentDocumentLinks.size() > 0) {
                this.version = doc.ContentDocumentLinks[0].ContentDocument.LatestPublishedVersion.VersionNumber + '.0';
                this.downloadUrl = Url.getSalesforceBaseUrl().toExternalForm()
                        + sandboxPrefix
                        + '/sfc/servlet.shepherd/document/download/'
                        + doc.ContentDocumentLinks[0].ContentDocumentId;
            } else {
                this.version = '';
                this.downloadUrl = '';
            }

            if (doc.VTD1_Archived_Date_Stamp__c != null) {
                Integer d = doc.VTD1_Archived_Date_Stamp__c.day();
                Integer m = doc.VTD1_Archived_Date_Stamp__c.month();
                Integer y = doc.VTD1_Archived_Date_Stamp__c.year();
                Datetime dt = Datetime.newInstance(y, m, d);
                this.archivedDate = dt.format('dd-MMM-YYYY');
            }
        }
    }

    public class PatientDocumentsWithAdditionalInformation {
        @AuraEnabled public List<PatientDocument> documents;
        @AuraEnabled public Case caseRecord;

        public PatientDocumentsWithAdditionalInformation(List<VTD1_Document__c> docs, Case caseRecord) {
            List<PatientDocument> patientDocuments = new List<PatientDocument>();
            for (VTD1_Document__c doc : docs) {
                PatientDocument patientDocument = new PatientDocument(doc);
                patientDocuments.add(patientDocument);
            }
            this.documents = patientDocuments;
            this.caseRecord = caseRecord;
        }
    }
}