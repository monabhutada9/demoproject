/**
 * Created by user on 4/1/2019.
 */

public with sharing class VT_R2_OrderTriggerHandler {

    public static void insertTypes(List<VTD1_Order__c> patientDeliveries) {
        Set<Id> protocolDeliveriesIds = new Set<Id>();
        Set<Id> parentDeliveries = new Set<Id>();
        Map<Id, VTD1_Order__c> parentDeliveriesMap = new Map<Id, VTD1_Order__c>();
        for (VTD1_Order__c order : patientDeliveries) {
            if (Trigger.isInsert) {
                if (order.VTD1_Protocol_Delivery__c != null) {
                    protocolDeliveriesIds.add(order.VTD1_Protocol_Delivery__c);
                } else if (order.VTD1_Patient_Delivery__c != null) {
                    parentDeliveries.add(order.VTD1_Patient_Delivery__c);
                }
                if ((order.RecordTypeId == VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_ORDER_AD_HOC_DELIVERY
                    || order.RecordTypeId == VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_ORDER_AD_HOC_REPLACEMENT_DELIVERY)
                        && order.VTD1_Kit_Type__c == 'IMP/Lab') {
                    order.VTD1_isLab__c = true;
                    order.VTD1_isIMP__c = true;
                }
            }
        }
        if (!parentDeliveries.isEmpty()) {
            parentDeliveriesMap = new Map<Id, VTD1_Order__c> ([SELECT Id, VTD1_Protocol_Delivery__c,
                                                                VTD1_Protocol_Delivery__r.VTR2_Destination_Type__c
                                                                FROM VTD1_Order__c
                                                                WHERE Id IN :parentDeliveries]);
            for (VTD1_Order__c parentOrder : parentDeliveriesMap.values()) {
                protocolDeliveriesIds.add(parentOrder.VTD1_Protocol_Delivery__c);
            }
        }
        if (!protocolDeliveriesIds.isEmpty()) {
            Set<Id> caseIdsSite = new Set<Id>();
            Set<Id> caseIdsPatient = new Set<Id>();
            Map<Id, VTD1_Protocol_Delivery__c> desttypesPDid = new Map<Id, VTD1_Protocol_Delivery__c> (
            [   SELECT Id, VTR2_Destination_Type__c
                FROM VTD1_Protocol_Delivery__c
                WHERE Id in:protocolDeliveriesIds
            ]);
            for (VTD1_Order__c order : patientDeliveries) {
                String destination_Type;
                if (order.VTD1_Patient_Delivery__c != null && parentDeliveriesMap.get(order.VTD1_Patient_Delivery__c) != null) {
                    destination_Type = 'Patient'; //parentDeliveriesMap.get(order.VTD1_Patient_Delivery__c).VTD1_Protocol_Delivery__r.VTR2_Destination_Type__c;
                } else if (desttypesPDid.get(order.VTD1_Protocol_Delivery__c) != null) {
                    destination_Type = desttypesPDid.get(order.VTD1_Protocol_Delivery__c).VTR2_Destination_Type__c;
                }
                if (destination_Type != null) {
                    if (destination_Type == 'Patient') {
                        caseIdsPatient.add(order.VTD1_Case__c);
                    } else if (destination_Type == 'Site') {
                        caseIdsSite.add(order.VTD1_Case__c);
                    }
                }
            }
            Map<Id, Id> caseIdprimaryAddressId = new Map<Id, Id>();
            if (!caseIdsSite.isEmpty()) {
                for (Case c : [SELECT Id, VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__c, ContactId
                                FROM Case
                                WHERE Id in:caseIdsSite AND VTD1_Virtual_Site__c != null
                                ]) {
                    caseIdprimaryAddressId.put(c.Id, c.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__c);
                }
            }
            Map<Id, Id> caseAddressesIds = new Map <Id, Id>();
            if (!caseIdsPatient.isEmpty()) {
                for (Address__c a : [select Id, VTD1_Contact__r.VTD1_Clinical_Study_Membership__c
                                    from Address__c
                                    where VTD1_Primary__c = true
                                        and VTD1_Contact__r.VTD1_Clinical_Study_Membership__c In:caseIdsPatient]) {
                    caseAddressesIds.put(a.VTD1_Contact__r.VTD1_Clinical_Study_Membership__c, a.id);
                }
            }

            for (VTD1_Order__c item : patientDeliveries) {
                String destination_Type;
                if (item.VTD1_Patient_Delivery__c != null && parentDeliveriesMap.get(item.VTD1_Patient_Delivery__c) != null) {
                    destination_Type = 'Patient'; //parentDeliveriesMap.get(item.VTD1_Patient_Delivery__c).VTD1_Protocol_Delivery__r.VTR2_Destination_Type__c;
                } else if (desttypesPDid.get(item.VTD1_Protocol_Delivery__c) != null)  {
                    destination_Type = desttypesPDid.get(item.VTD1_Protocol_Delivery__c).VTR2_Destination_Type__c;
                }

                if (destination_Type == null) continue;

                item.VTR2_Destination_Type__c = destination_Type;

                if (item.VTR2_Destination_Type__c == 'Patient' ) {
                   item.VTR2_Patient_Shipping_Address__c = caseAddressesIds.get(item.VTD1_Case__c);
                } else if (item.VTR2_Destination_Type__c == 'Site') {
                    item.VTR2_Site_Shipping_Address__c = caseIdprimaryAddressId.get(item.VTD1_Case__c);
                }
            }
        }
    }

    public static void updateDelivery (List<VTD1_Order__c> newDeliveries, Map<Id, VTD1_Order__c> oldDeliveries) {
        Set<Id> ordersCaseIds = new Set<Id>();
        List<VTD1_Order__c> orders = new List<VTD1_Order__c>();
        for (VTD1_Order__c order: newDeliveries) {
            if (Trigger.isInsert || order.VTD1_Status__c != oldDeliveries.get(order.Id).VTD1_Status__c) {
                order.VTR2_Status_Date__c = System.now();
            }
            if (Trigger.isInsert && !order.RecordTypeId.equals(VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_ORDER_PACKING_MATERIALS)
                    && order.VTD1_IntegrationId__c == null) {
                ordersCaseIds.add(order.VTD1_Case__c);
                orders.add(order);
            }
        }
        if (!ordersCaseIds.isEmpty()) {
            Map<Id, Case> casesByIds = new Map<Id, Case>([
                    SELECT VTD1_Subject_ID__c
                    FROM Case
                    WHERE Id IN : ordersCaseIds
            ]);
            for (VTD1_Order__c order : orders) {
                Case myCase = casesByIds.get(order.VTD1_Case__c);
                order.VTD1_IntegrationId__c = myCase.VTD1_Subject_ID__c + '-' + order.Name;
            }
        }
    }


    public static void sendConfirmShipmentDetails (List<VTD1_Order__c> newList, Map<Id, VTD1_Order__c> oldMap) {
        Map<Id, List<VTD1_Order__c>> ordersToCasesIdsMap = new Map<Id, List<VTD1_Order__c>>();
        Set<String> kitTypesDevices = new Set<String>{
                'Connected Devices',
                'Study Hub Tablet'
        };
        String impLabType = 'IMP/Lab';
        String welcomeType = 'Welcome to Study Package';
        for (VTD1_Order__c order : newList) {
            if (order.VTD1_Status__c == 'Pending Shipment'
                && oldMap.get(order.Id).VTD1_Status__c != order.VTD1_Status__c
                && (
                    (kitTypesDevices.contains(order.VTD1_Kit_Type__c)
                        || order.VTD1_containsDevice__c
                        || order.VTD1_containsTablet__c)
                    || (order.VTD1_Kit_Type__c == welcomeType
                        && order.VTD1_isWelcomeToStudyPackage__c)
                    || (order.VTD1_Kit_Type__c == impLabType
                        && order.VTR2_Destination_Type__c == 'Patient'
                        && (
                            (order.VTD1_isIMP__c
                                || order.VTD1_isLab__c)
                            || order.VTD1_Shipment_Type_formula__c == System.Label.VTD2_PackagingMaterials)
                        )
                )
            ) {
                if (!ordersToCasesIdsMap.containsKey(order.VTD1_Case__c)) {
                    ordersToCasesIdsMap.put(order.VTD1_Case__c, new List<VTD1_Order__c>());
                }
                ordersToCasesIdsMap.get(order.VTD1_Case__c).add(order);
            }
        }
        if (!ordersToCasesIdsMap.isEmpty()) {
            String templateDevName = 'ConfirmStudyShipAddressKitreadytobeship';
            List<VT_R3_EmailsSender.ParamsHolder> emailsPHList = new List<VT_R3_EmailsSender.ParamsHolder>();
            for (Account acc : [
                    SELECT Id,
                            HealthCloudGA__CarePlan__c,
                            (SELECT Id,
                                    RecordTypeId
                            FROM Contacts
                            WHERE (RecordType.Name = 'Patient'
                                OR (RecordType.Name = 'Caregiver' AND VTD1_Primary_CG__c = TRUE))
                            )
                    FROM Account
                    WHERE HealthCloudGA__CarePlan__c IN :ordersToCasesIdsMap.keySet()
            ]) {
                for (Contact cont : acc.Contacts) {
                    for (VTD1_Order__c order : ordersToCasesIdsMap.get(acc.HealthCloudGA__CarePlan__c)) {
                        emailsPHList.add(
                                new VT_R3_EmailsSender.ParamsHolder(
                                        cont.Id,
                                        order.Id,
                                        templateDevName
                                )
                        );
                        if (order.VTD1_Kit_Type__c == impLabType
                                && (order.VTD1_Shipment_Type_formula__c == System.Label.VTD2_PackagingMaterials
                                || order.VTD1_isPackageDelivery__c)
                        ) {
                            order.VTD1_Confirm_PM_Address_task_sent__c = Date.today();
                        } else if (order.VTD1_Kit_Type__c != impLabType) {
                            order.VTR2_WelcomeDeviceConfirmAddressTaskSent__c = Date.today();
                            order.VTD1_Invocable_process__c = true;
                        }
                    }
                }
            }
            if (!emailsPHList.isEmpty()) {
                VT_R3_EmailsSender.sendEmails(emailsPHList);
            }
        }
    }
}