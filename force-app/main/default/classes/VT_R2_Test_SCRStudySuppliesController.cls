/**
 * Created by user on 11.04.2019.
 */

@isTest
public with sharing class VT_R2_Test_SCRStudySuppliesController {
    @testSetup
    static void setup() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        /*List <Study_Team_Member__c> studyTeamMembers = [select Id, RecordType.Name from Study_Team_Member__c];
        for (Study_Team_Member__c studyTeamMember : studyTeamMembers) {
            System.debug('stm = ' + studyTeamMembers);
        }*/
        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }
    @isTest
    public static void test() {
        VT_R2_SCRPatientStudySuppliesController.getOptions();
        Case caseObj = [select Id from Case];
        VTD1_Patient_Kit__c kit = new VTD1_Patient_Kit__c(VTD1_Case__c = caseObj.Id);
        insert kit;
        VT_R2_SCRPatientStudySuppliesController.getStudySupplies(caseObj.Id);
    }
}