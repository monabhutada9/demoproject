/**
 * Created by shume on 26/12/2019.
 */

public with sharing class VT_R4_StudyTeamAssignment {
    private static List<HealthCloudGA__CandidatePatient__c> candidatesWithOneKeyFieldList = new List<HealthCloudGA__CandidatePatient__c>();
    private static List<HealthCloudGA__CandidatePatient__c> candidatesWithPIuserList = new List<HealthCloudGA__CandidatePatient__c>();
    private static List<HealthCloudGA__CandidatePatient__c> candidatesForDefaultFlowList = new List<HealthCloudGA__CandidatePatient__c>();
    private static Set<String> oneKeyFieldIds = new Set<String>();
    private static Set<String> refferedPIuserIds = new Set<String>();
    private static Set<Id> referredFlowStudyIds = new Set<Id>();
    private static Set<Id> defaultFlowStudyIds = new Set<Id>();
    private static Set<String> patientStatesForSOQL = new Set<String>();
    private static Map<String, Study_Team_Member__c> PIsByReferredUserMap = new Map<String, Study_Team_Member__c>();
    private static Map<String, List<Study_Team_Member__c>> PIsByStudyStateMap = new Map<String, List<Study_Team_Member__c>>();
    private static Map<String, Study_Team_Member__c> PIsByStudyRescueMap = new Map<String, Study_Team_Member__c>();
    private static Map<Id, Study_Team_Member__c> studyMembersToUpdateMap = new Map<Id, Study_Team_Member__c>();
    private static Map <Id, Integer> indexStudyTeamMemberMap = new Map<Id, Integer>();
    private static Map <Id, Integer> indexStudyTeamMemberRelationships1Map = new Map<Id, Integer>();
    private static Map <Id, List <Study_Site_Team_Member__c>> sortedPGBySecondaryLastAssignment = new Map<Id, List<Study_Site_Team_Member__c>>();
    private static Map <Id, Study_Team_Member__c> skipPrevSecondaryPGMap = new Map<Id, Study_Team_Member__c>();

    public static  Map<Id, StudyTeamPack> studyTeamByCandidateIdsMap = new Map<Id, StudyTeamPack>();

    /*  SH-14562 - This method finds the STM.
        This code copied from assignStudyTeam method, As a part of SH-14562 story we need to 
        find STM before the contact and user creation to validate and assign virtual site IRB language.
    */
    public static void getStudyTeam(List<VT_R4_PatientConversion.CPWrapper> cpWrapList, Boolean doInCurrentTransaction){
        List<Study_Team_Member__c> studyTeamMembers;
        Study_Team_Member__c primaryStudyMemberPI;
        StudyTeamPack studyTeamForCandidate;
        List<HealthCloudGA__CandidatePatient__c> cps = new List<HealthCloudGA__CandidatePatient__c>();
        for (VT_R4_PatientConversion.CPWrapper wrapper : cpWrapList) {
            if (wrapper.cp.VTR5_Study_Site_Number__c != null && wrapper.cp.VT_R4_VirtualSite__c == null) {
                continue;
            }
            cps.add(wrapper.cp);
        }
        divideCandidatesByFlow(cps);
        studyTeamMembers = getStudyTeamMembers();
        populatePIsMaps(studyTeamMembers);
        if (!candidatesWithPIuserList.isEmpty()) {
            for (HealthCloudGA__CandidatePatient__c candidate : candidatesWithPIuserList) {
                Id virtualSiteForCP = candidate.VT_R4_VirtualSite__c;
                Id piID = candidate.VTR2_Pi_Id__c;
                primaryStudyMemberPI = PIsByStudyRescueMap.get(virtualSiteForCP + '_' + piID);
                system.debug('primaryStudyMemberPI $$' + primaryStudyMemberPI);
                if(primaryStudyMemberPI != null){
                    studyTeamForCandidate = createStudyTeamPack(primaryStudyMemberPI);
                    studyTeamByCandidateIdsMap.put(candidate.Id, studyTeamForCandidate);
                }
            }
        }
        if (!candidatesWithOneKeyFieldList.isEmpty()) {
            for (HealthCloudGA__CandidatePatient__c candidate : candidatesWithOneKeyFieldList) {
                if (candidate.VT_R4_VirtualSite__c != null) {
                    primaryStudyMemberPI = PIsByReferredUserMap.get(candidate.VT_R4_VirtualSite__c + '_' + candidate.VTD1_One_Key_ID__c);
                } else {
                    primaryStudyMemberPI = PIsByReferredUserMap.get(candidate.Study__c + '_' + candidate.VTD1_One_Key_ID__c);
                }
                if(primaryStudyMemberPI != null){
                    studyTeamForCandidate = createStudyTeamPack(primaryStudyMemberPI);
                    studyTeamByCandidateIdsMap.put(candidate.Id, studyTeamForCandidate);
                }
                System.debug('OneKeyFieldFlow $$$: ' + candidate.Id);
            }
        }
        if (!candidatesForDefaultFlowList.isEmpty()) {
            List<Study_Team_Member__c> possibleMembersPI1;
            Map <String, Integer> indexMap = new Map<String, Integer>();
            for (HealthCloudGA__CandidatePatient__c candidate : candidatesForDefaultFlowList) {
                //System.debug('def flow ' + candidate);
                String key = candidate.Study__c + candidate.HealthCloudGA__Address1State__c;
                possibleMembersPI1 = PIsByStudyStateMap.get(key);
                if (possibleMembersPI1 == null)
                    continue;
                Integer index = indexMap.get(key);
                if (index == null || index == possibleMembersPI1.size() - 1) {
                    index = 0;
                } else {
                    index ++;
                }
                indexMap.put(key, index);
                primaryStudyMemberPI = possibleMembersPI1[index];
                System.debug('primaryStudyMemberPI = ' + primaryStudyMemberPI);
                studyTeamForCandidate = createStudyTeamPack(primaryStudyMemberPI);
                studyTeamByCandidateIdsMap.put(candidate.Id, studyTeamForCandidate);
            }
        }
        for (VT_R4_PatientConversion.CPWrapper wrapper : cpWrapList) {
            wrapper.cp.VTR4_StatusSTA__c = VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_FINISHED;
        }
        update studyMembersToUpdateMap.values();
        VT_R4_PatientConversion.createAccountContactPatient(cpWrapList, true);
        ///cpWraps[0].candidate.VTR4_StatusSTA__c = VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_FINISHED;
        ///VT_R4_PatientConversion.createCases(cpWraps, doInCurrentTransaction, usePE);
        //VT_R4_PatientConversion.reserveSubjectNumbers(cpWraps, false, usePE);
    }

    private static void divideCandidatesByFlow(List<HealthCloudGA__CandidatePatient__c> candidatesList) {
        for (HealthCloudGA__CandidatePatient__c candidate : candidatesList) {
            if (candidate.VTR2_Pi_Id__c != null) {
                refferedPIuserIds.add(candidate.VTR2_Pi_Id__c);
                referredFlowStudyIds.add(candidate.Study__c);
                candidatesWithPIuserList.add(candidate);
            } else if (candidate.VTD1_One_Key_ID__c != null) {
                oneKeyFieldIds.add(candidate.VTD1_One_Key_ID__c);
                referredFlowStudyIds.add(candidate.Study__c);
                candidatesWithOneKeyFieldList.add(candidate);
            } else {
                defaultFlowStudyIds.add(candidate.Study__c);
                patientStatesForSOQL.add('%' + candidate.HealthCloudGA__Address1State__c + '%');
                candidatesForDefaultFlowList.add(candidate);
            }
        }
    }

    private static List<Study_Team_Member__c> getStudyTeamMembers() {
        List<Study_Team_Member__c> result = [
                SELECT
                        Id,
                        User__r.People_Soft_ID__c,
                        Study__c,
                        VTD1_PI_License__c,
                        User__c,
                        VTD1_UserContact__c,
                        VTD1_Number_of_Patients__c,
                        VTD1_VirtualSite__c,
                        VTD1_VirtualSite__r.VTR5_IRBApprovedLanguages__c,
                        VTD1_VirtualSite__r.VTR5_DefaultIRBLanguage__c,
                        VTD1_Last_Assigned_Patient__c, (
                        SELECT
                                Id,
                                VTD1_UserContact__c,
                                User__c,
                                VTD1_Last_Assigned_Patient__c
                        FROM Study_Team_Members__r
                        WHERE VTD1_Ready_For_Assignment__c = TRUE
                        ORDER BY VTD1_Last_Assigned_Patient__c
                ), (
                        SELECT
                                Id,
                                VTD1_Associated_PG__c,
                                VTD1_Associated_PG__r.User__c,
                                VTD1_Associated_PG__r.VTD1_Last_Assigned_Patient__c,
                                VTD1_Associated_PG__r.VTD1_Secondary_Last_Assigned_Patient__c
                        FROM Study_Team_Member_Relationships1__r
                        WHERE VTD1_Ready_For_Assignment__c = TRUE
                        ORDER BY VTD1_Associated_PG__r.VTD1_Last_Assigned_Patient__c
                ), (
                        SELECT
                                Id,
                                VTR2_Associated_SCr__r.User__c,
                                VTR2_SCr_type__c
                        FROM Study_Site_Team_Members1__r
                )
                FROM Study_Team_Member__c
                WHERE ((
                        Study__c IN :referredFlowStudyIds
                        AND (User__r.People_Soft_ID__c IN :oneKeyFieldIds OR User__c IN :refferedPIuserIds))
                OR (
                        Study__c IN :defaultFlowStudyIds
                        AND VTD1_Backup_PI__c = FALSE
                        AND VTD1_PI_License__c LIKE :patientStatesForSOQL
                        AND VTD1_Ready_For_Assignment__c = TRUE
                        AND VTD1_Type__c = 'Primary Investigator'
                        ))
                AND VTD1_VirtualSite__c != NULL
                ORDER BY Study__c, VTD1_Last_Assigned_Patient__c
        ];
        Set <Id> sstmIds = new Set<Id>();
        for (Study_Team_Member__c stm : result) {
            for (Study_Site_Team_Member__c sstm : stm.Study_Team_Member_Relationships1__r) {
                sstmIds.add(sstm.Id);
            }
        }
        List <Study_Site_Team_Member__c> sstms = [
                SELECT
                        Id,
                        VTD1_Associated_PI__c,
                        VTD1_Associated_PG__c,
                        VTD1_Associated_PG__r.User__c,
                        VTD1_Associated_PG__r.VTD1_Last_Assigned_Patient__c,
                        VTD1_Associated_PG__r.VTD1_Secondary_Last_Assigned_Patient__c
                FROM Study_Site_Team_Member__c
                WHERE Id IN : sstmIds
                ORDER BY VTD1_Associated_PI__c, VTD1_Associated_PG__r.VTD1_Secondary_Last_Assigned_Patient__c
        ];
        for (Study_Site_Team_Member__c sstm : sstms) {
            List <Study_Site_Team_Member__c> sstmsByPI = sortedPGBySecondaryLastAssignment.get(sstm.VTD1_Associated_PI__c);
            if (sstmsByPI == null) {
                sstmsByPI = new List<Study_Site_Team_Member__c>();
                sortedPGBySecondaryLastAssignment.put(sstm.VTD1_Associated_PI__c, sstmsByPI);
            }
            sstmsByPI.add(sstm);
        }
        return result;
    }

    private static void populatePIsMaps(List<Study_Team_Member__c> studyTeamMembers) {
        Map <Id, Study_Team_Member__c> IdToPIMap = new Map<Id, Study_Team_Member__c>();
        for (Study_Team_Member__c stmPI : studyTeamMembers) {
            String referredUserPIId = stmPI.User__c;
            String referredSoftId = stmPI.User__r.People_Soft_ID__c;
            if (refferedPIuserIds.contains(referredUserPIId)) {
                PIsByStudyRescueMap.put(stmPI.VTD1_VirtualSite__c + '_' + referredUserPIId, stmPI);
            }
            if (referredSoftId != null && oneKeyFieldIds.contains(referredSoftId)) {
                PIsByReferredUserMap.put(stmPI.VTD1_VirtualSite__c + '_' + referredSoftId, stmPI);
                PIsByReferredUserMap.put(stmPI.Study__c + '_' + referredSoftId, stmPI);
            }
            if (stmPI.VTD1_PI_License__c != null) {
                List<String> licenses = new List<String>(stmPI.VTD1_PI_License__c.split(','));
                Id studyId = stmPI.Study__c;
                for (String license : licenses) {
                    String key = studyId + license;
                    if (PIsByStudyStateMap.containsKey(key)) {
                        PIsByStudyStateMap.get(key).add(stmPI);
                    } else {
                        PIsByStudyStateMap.put(key, new List<Study_Team_Member__c>{stmPI});
                    }
                }
            }
        }
    }

    private static StudyTeamPack createStudyTeamPack(Study_Team_Member__c primaryStudyMemberPI) {
        Id virtualSiteId;
        Id PI1UserId;
        Id PI1ContactId;
        Id PI2UserId;
        Id PI2ContactId;
        Id PG1UserId;
        Id PG2UserId;
        Id SCR1UserId;
        Id SCR2UserId;
        StudyTeamPack result;
        Study_Team_Member__c relatedStudyMemberPG1 = new Study_Team_Member__c();
        // find Virtual Site
        virtualSiteId = primaryStudyMemberPI.VTD1_VirtualSite__c;
        // find PI1
        PI1UserId = primaryStudyMemberPI.User__c;
        PI1ContactId = primaryStudyMemberPI.VTD1_UserContact__c;
        if (primaryStudyMemberPI.VTD1_Number_of_Patients__c == null) {
            primaryStudyMemberPI.VTD1_Number_of_Patients__c = 1;
        } else {
            primaryStudyMemberPI.VTD1_Number_of_Patients__c++;
        }
        primaryStudyMemberPI.VTD1_Last_Assigned_Patient__c = System.now();
        studyMembersToUpdateMap.put(primaryStudyMemberPI.Id, primaryStudyMemberPI);
        // find PI 2
        List<Study_Team_Member__c> possibleMembersPI2 = primaryStudyMemberPI.Study_Team_Members__r;
        System.debug('possibleMembersPI2 = ' + possibleMembersPI2 + ' for primary ' + primaryStudyMemberPI);
        if (!possibleMembersPI2.isEmpty()) {
            Integer index = indexStudyTeamMemberMap.get(primaryStudyMemberPI.Id);
            if (index == null || index == possibleMembersPI2.size() - 1) {
                index = 0;
            } else {
                index ++;
            }
            indexStudyTeamMemberMap.put(primaryStudyMemberPI.Id, index);
            Study_Team_Member__c relatedStudyMemberPI2 = primaryStudyMemberPI.Study_Team_Members__r[index];
            System.debug('relatedStudyMemberPI2 = ' + relatedStudyMemberPI2);
            PI2UserId = relatedStudyMemberPI2.User__c;
            PI2ContactId = relatedStudyMemberPI2.VTD1_UserContact__c;
            relatedStudyMemberPI2.VTD1_Last_Assigned_Patient__c = System.now();
            studyMembersToUpdateMap.put(relatedStudyMemberPI2.Id, relatedStudyMemberPI2);
        }
        // find PGs
        List<Study_Site_Team_Member__c> possibleMembersPGs = primaryStudyMemberPI.Study_Team_Member_Relationships1__r;
        System.debug('possibleMembersPGs = ' + possibleMembersPGs.size());
        if (!possibleMembersPGs.isEmpty()) {
            // find PG1
            Integer index = indexStudyTeamMemberRelationships1Map.get(primaryStudyMemberPI.Id);
            if (index == null || index == possibleMembersPGs.size() - 1) {
                index = 0;
            } else {
                index ++;
            }
            System.debug('indexPG1 = ' + index);
            indexStudyTeamMemberRelationships1Map.put(primaryStudyMemberPI.Id, index);
            relatedStudyMemberPG1 = possibleMembersPGs[index].VTD1_Associated_PG__r;
            PG1UserId = relatedStudyMemberPG1.User__c;
            relatedStudyMemberPG1.VTD1_Last_Assigned_Patient__c = System.now();
            studyMembersToUpdateMap.put(relatedStudyMemberPG1.Id, relatedStudyMemberPG1);
            Study_Team_Member__c relatedStudyMemberPG2 = skipPrevSecondaryPGMap.get(primaryStudyMemberPI.Id);
            if (relatedStudyMemberPG2 == null) {
                List <Study_Site_Team_Member__c> possibleSecondaryMembersPGs = sortedPGBySecondaryLastAssignment.get(primaryStudyMemberPI.Id);
                if (possibleSecondaryMembersPGs != null) {
                    System.debug('possibleSecondaryMembersPGs = ' + possibleSecondaryMembersPGs.size());
                    relatedStudyMemberPG2 = possibleSecondaryMembersPGs[index].VTD1_Associated_PG__r;
                    if (relatedStudyMemberPG1.Id == relatedStudyMemberPG2.Id) {
                        System.debug('same PG');
                        skipPrevSecondaryPGMap.put(primaryStudyMemberPI.Id, relatedStudyMemberPG2);
                        if (possibleSecondaryMembersPGs.size() > 1) {
                            Integer nextIndex = index + 1;
                            if (nextIndex >= possibleSecondaryMembersPGs.size()) {
                                nextIndex = 0;
                            }
                            relatedStudyMemberPG2 = possibleSecondaryMembersPGs[nextIndex].VTD1_Associated_PG__r;
                        } else {
                            relatedStudyMemberPG2 = null;
                        }
                    }
                }
            }
            if (relatedStudyMemberPG2 != null) {
                PG2UserId = relatedStudyMemberPG2.User__c;
                relatedStudyMemberPG2.VTD1_Secondary_Last_Assigned_Patient__c = System.now();
                studyMembersToUpdateMap.put(relatedStudyMemberPG2.Id, relatedStudyMemberPG2);
            }
            System.debug('relatedStudyMemberPG1 = ' + relatedStudyMemberPG1);
            System.debug('relatedStudyMemberPG2 = ' + relatedStudyMemberPG2);
        }
        // find SCRs
        List<Study_Site_Team_Member__c> possibleMembersSCRs = primaryStudyMemberPI.Study_Site_Team_Members1__r;
        for (Study_Site_Team_Member__c siteTeamMember : possibleMembersSCRs) {
            if (SCR1UserId == null && siteTeamMember.VTR2_SCr_type__c == 'Primary') {
                SCR1UserId = siteTeamMember.VTR2_Associated_SCr__r.User__c;
            } else if (SCR2UserId == null && siteTeamMember.VTR2_SCr_type__c == 'Backup') {
                SCR2UserId = siteTeamMember.VTR2_Associated_SCr__r.User__c;
            }
        }
        result = new StudyTeamPack(virtualSiteId, primaryStudyMemberPI, PI2UserId, PI2ContactId, relatedStudyMemberPG1, PG2UserId, SCR1UserId, SCR2UserId);
        System.debug('stm pack = ' + result);
        return result;
    }

    public class StudyTeamPack {
        public Id virtualSiteId;
        public Id primaryStmPI;
        public Id relatedStmPG1;
        public Id PI1UserId;
        public Id PI1ContactId;
        public Id PI2UserId;
        public Id PI2ContactId;
        public Id PG1UserId;
        public Id PG2UserId;
        public Id SCR1UserId;
        public Id SCR2UserId;
        public String IRBApprovedLanguages;
        public String DefaultIRBLanguage;

        public StudyTeamPack(
                Id virtualSiteId,
                Study_Team_Member__c primaryStudyMemberPI,
                Id PI2UserId, Id PI2ContactId,
                Study_Team_Member__c relatedStudyMemberPG1,
                Id PG2UserId,
                Id SCR1UserId, Id SCR2UserId) {
            this.virtualSiteId = virtualSiteId;
            this.primaryStmPI = primaryStudyMemberPI.Id;
            this.relatedStmPG1 = relatedStudyMemberPG1.Id;
            this.PI1UserId = primaryStudyMemberPI.User__c;
            this.PI1ContactId = primaryStudyMemberPI.VTD1_UserContact__c;
            this.PI2UserId = PI2UserId;
            this.PI2ContactId = PI2ContactId;
            if (relatedStudyMemberPG1 != null) {
                this.PG1UserId = relatedStudyMemberPG1.User__c;
            }
            this.PG2UserId = PG2UserId;
            this.SCR1UserId = SCR1UserId;
            this.SCR2UserId = SCR2UserId;
            this.IRBApprovedLanguages = primaryStudyMemberPI.VTD1_VirtualSite__r.VTR5_IRBApprovedLanguages__c;
            this.DefaultIRBLanguage = primaryStudyMemberPI.VTD1_VirtualSite__r.VTR5_DefaultIRBLanguage__c;
        }
    }

    public class AssignmentHistoryWrapper {
        public VTD1_Assignment_History__c ah;
        public HealthCloudGA__CandidatePatient__c cp;
        public AssignmentHistoryWrapper(VTD1_Assignment_History__c ah, HealthCloudGA__CandidatePatient__c cp) {
            this.ah = ah;
            this.cp = cp;
        }
    }
}