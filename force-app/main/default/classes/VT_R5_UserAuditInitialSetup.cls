/**
 * @author Aliaksandr Vabishchevich
 * @date 08-Oct-20
 * @description Initial setup for User Audit report
 * deprecated class
 */


public with sharing class VT_R5_UserAuditInitialSetup {
    public static void createUserAuditRecordsForUsers() {}  // deprecated
}