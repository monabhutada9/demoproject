/**
 * Created by User on 19/05/21.
 */
@isTest
private with sharing class VT_D1_OrderIntegrationTest {

    @testSetup
    private static void init(){
        getOrder();
    }
    @isTest
    private static void sendPatientDeliveryToCSMTest(){
        VTD1_Order__c order = [SELECT Id FROM VTD1_Order__c LIMIT 1];
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        VT_D1_OrderIntegration.sendPatientDeliveryToCSM(order.Id, '1.1');
        Test.stopTest();
    }
    @isTest
    private static void sendPatientDeliveryToCSMFutureTest(){
        VTD1_Order__c order = getOrder();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        VT_D1_OrderIntegration.sendPatientDeliveryToCSMFuture(order.Id, '1.1');
        Test.stopTest();
    }
    @isTest
    private static void sendAdHocReplacementToCSMTest(){
        VTD1_Order__c order = [SELECT Id FROM VTD1_Order__c LIMIT 1];
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        VT_D1_OrderIntegration.sendAdHocReplacementToCSM(order.Id);
        Test.stopTest();
    }
    @isTest
    private static void sendAdHocReplacementToCSMFutureTest(){
        VTD1_Order__c order = [SELECT Id FROM VTD1_Order__c LIMIT 1];
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        VT_D1_OrderIntegration.sendAdHocReplacementToCSMFuture(order.Id);
        Test.stopTest();
    }
    @isTest
    private static void sprocessAdHocReplacementResponseTest(){
        VTD1_Order__c order = [SELECT Id FROM VTD1_Order__c LIMIT 1];
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        VT_D1_OrderIntegration.ReplacementResponse resp = new VT_D1_OrderIntegration.ReplacementResponse();
        resp.protocolId = 'test';
        resp.subjectId = 'test';
        resp.shipmentId = 'test';
        resp.shipmentName = 'test';
        resp.kits = new List<VT_D1_OrderIntegration.Kit>();
        resp.auditLog = null;
        VT_D1_OrderIntegration.processAdHocReplacementResponse(order.Id,resp);
        Test.stopTest();
    }

    private static VTD1_Order__c getOrder(){
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );
        DomainObjects.User_t user = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5));
        Case cas = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .addStudy(study)
                .addUser(user)
                .setVTD1_Subject_ID('subjectId')
                .persist();
        VTD1_Order__c order = (VTD1_Order__c) new DomainObjects.VTD1_Order_t()
                .setVTD1_Status('Shipped')
                .setVTD1_Case(cas.Id)
                .setVTD1_Expected_Shipment_Date(Date.today())
                .setVTD1_Delivery_Order(1)
                .setVTD1_ShipmentId('shipmentId')
                .persist();
        return order;
    }

    public class MockHttpResponseGenerator implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            VT_D1_OrderIntegration.ReplacementResponse resp = new VT_D1_OrderIntegration.ReplacementResponse();
            resp.protocolId = 'test';
            resp.subjectId = 'test';
            resp.shipmentId = 'test';
            resp.shipmentName = 'test';
            resp.kits = new List<VT_D1_OrderIntegration.Kit>();
            resp.auditLog = null;
            res.setBody(JSON.serialize(resp));
            return res;
        }
    }
}