/**
 * Created by Andrey Pivovarov on 6/18/2020.
 */

public without sharing class VT_R5_CaseProcessHandler extends Handler {

    private static final String PRE_CONSENT = VT_R4_ConstantsHelper_AccountContactCase.CASE_PRE_CONSENT;
    private static final String CONSENTED = VT_R4_ConstantsHelper_AccountContactCase.CASE_CONSENTED;
    private static final String SCREENED = VT_R4_ConstantsHelper_AccountContactCase.CASE_SCREENED;
    private static final String SCREEN_FAILURE = VT_R4_ConstantsHelper_AccountContactCase.CASE_SCREEN_FAILURE;
    private static final String WASHOUT_RUN_IN = VT_R4_ConstantsHelper_AccountContactCase.CASE_WASHOUT_RUN_IN;
    private static final String WASHOUT_RUN_IN_FAILURE = VT_R4_ConstantsHelper_AccountContactCase.CASE_WASHOUT_RUN_IN_FAILURE;
    private static final String ACTIVE_RANDOMIZED = VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED;


    private static final String COMPLETED = VT_R4_ConstantsHelper_AccountContactCase.CASE_COMPLETED;
    private static final String ID_CAREPLAN = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN;
    private static final String ID_CASE_SPF = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_SPF;
    private static final String ID_ACTUAL_VISIT_UNSCHEDULED = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_ACTUAL_VISIT_UNSCHEDULED;
    private static final String ID_ACTUAL_VISIT_LABS = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_ACTUAL_VISIT_LABS;



    public static Boolean skipFlows = false;
    private static List<VT_D2_TNCatalogTasks.ParamsHolder> userTasks = new List<VT_D2_TNCatalogTasks.ParamsHolder>();
    private static List<VT_D2_TNCatalogNotifications.ParamsHolder> notifications = new List<VT_D2_TNCatalogNotifications.ParamsHolder>();
    private static List<VT_R3_EmailsSender.ParamsHolder> emailParamsHolders = new List<VT_R3_EmailsSender.ParamsHolder>();

    public static Set<Id> caseMapIds = new Set<Id>();
    public static Map<Id, Case> caseMap = new Map<Id, Case>();
    public static Map<Id, VTD2_Study_Geography__c> studyGeographyMap = new Map<Id, VTD2_Study_Geography__c>();

    public override void onBeforeUpdate(Handler.TriggerContext context) {
        executeCaseProcessBeforeUpdate(context.newMap, context.oldMap);
    }
    public override void onAfterUpdate(Handler.TriggerContext context) {
        executeCaseProcessAfterUpdate(context.newMap, context.oldMap);
    }
    public override void onBeforeInsert(Handler.TriggerContext context) {
        executeCaseProcessBeforeInsert(context.newMap);
    }
    public override void onAfterInsert(Handler.TriggerContext context) {
        executeCaseProcessAfterInsert(context.newMap);
    }

    public static Map<Id, Case> getCaseMap(Map<Id, SObject> newCases) {
        if (!caseMapIds.containsAll(newCases.keySet())) {
            caseMapIds.addAll(newCases.keySet());
            caseMap = new Map<Id, Case>([
                    SELECT VTD1_Study__r.VTD1_Study_Admin__c, VTD1_Study__r.VTD2_TMA_Eligibility_Review_Options__c,
                            VTD2_Study_Geography__r.VTD2_TMA_Review_Required__c, VTD1_Study__r.VTD2_TMA_Eligibility_Review_Required__c,
                            VTD1_Virtual_Site__r.VTD2_TMA_Review_Required__c, VTD1_Study__r.VTD2_Washout_Run_In__c, VTD1_PI_user__r.Email,
                            VTD1_Study__r.VTR2_Site_Staff_Responsible_for_Elgbl__c, VTD1_Study__r.VTD1_Randomized__c,
                            VTD2_Monitoring_Visit__r.VTD1_Site_Visit_Type__c, VTD1_Document__r.VTD1_Regulatory_Document_Type__c,
                            VTD1_Study__r.VTD2_Rescreening_Wait_Period__c, VTD1_Clinical_Study_Membership__r.RecordTypeId,
                            VTD1_Study__r.VTD2_Maximum_Rescreening_Attempts__c, VTD1_Study__r.VTD2_Rescreening_Allowed__c
                    FROM Case
                    WHERE Id IN :caseMapIds
            ]);
        }
        return caseMap;
    }

    public static void getStudyGeographyMap(List<Case> newCases) {
        Set<Id> studyGeographiesIds = new Set<Id>();
        for (Case cs : (List<Case>) newCases) {
            studyGeographiesIds.add(cs.VTD2_Study_Geography__c);
        }
        studyGeographyMap = new Map<Id, VTD2_Study_Geography__c>([
                SELECT VTD2_Current_Patients__c, VTD2_TMA_Number__c
                FROM VTD2_Study_Geography__c
                WHERE Id IN :studyGeographiesIds
        ]);
    }

    public static void executeCaseProcessBeforeUpdate(Map<Id, SObject> newCases, Map<Id, SObject> oldCases) {
        Map<Id, Case> currentCases = getCaseMap(newCases);
        List<Case> css = newCases.values();
        List<VTD1_Case_Status_History__c> caseStatusHistories = new List<VTD1_Case_Status_History__c>();
        List<Case> caseStudyGeographies = new List<Case>();
        List<Case> caseActualVisit = new List<Case>();
        Map<Id, VT_D2_TNCatalogTasks.ParamsHolder> taskToAssignByCaseMap = new Map<Id, VT_D2_TNCatalogTasks.ParamsHolder>();

        List<VT_R5_GenericScheduledActionsService.ScheduledActionData> scheduledActions = new List<VT_R5_GenericScheduledActionsService.ScheduledActionData>();
        for (Case newCase : css) {
            Case oldCase = (Case) oldCases.get(newCase.Id);
            if (newCase.Status != oldCase.Status) {
                /**Create Case Status History*/
                caseStatusHistories.add(new VTD1_Case_Status_History__c(
                        VTD1_Case__c = newCase.Id,
                        VTD1_Is_Active__c = true,
                        VTD1_Status__c = newCase.Status));
                /**Task for the Patient Guide to re-submit the randomization*/
                if (newCase.Status == WASHOUT_RUN_IN_FAILURE) {
                    taskToAssignByCaseMap.put(newCase.Id, new VT_D2_TNCatalogTasks.ParamsHolder(newCase.Id, '301'));
                    // Flow.Interview.Create_Task_for_SCR_or_PG
                }
                if ((newCase.Status == SCREEN_FAILURE
                        || newCase.Status == WASHOUT_RUN_IN_FAILURE)
                        && newCase.VTD2_Rescreening_Attempts__c < currentCases.get(newCase.Id).VTD1_Study__r.VTD2_Maximum_Rescreening_Attempts__c
                        && currentCases.get(newCase.Id).VTD1_Study__r.VTD2_Rescreening_Allowed__c && !newCase.VTD2_Do_Not_Contact_for_Rescreening__c
                        && newCase.VTD2_Failure_Date__c != null) {
                    // Flow.Interview.VTR4_Delay_task_creation_to_contact_Patient_for_rescreening
                    Datetime delayTo = newCase.VTD2_Failure_Date__c
                            + (Integer) (currentCases.get(newCase.Id).VTD1_Study__r.VTD2_Rescreening_Wait_Period__c != null ?
                            currentCases.get(newCase.Id).VTD1_Study__r.VTD2_Rescreening_Wait_Period__c : 0);
                    scheduledActions.add(new VT_R5_GenericScheduledActionsService.ScheduledActionData(
                            delayTo.addHours(5), newCase.Id, 'ContactPatientForRescreening')
                    );
                }
            }
            /**RAND2 Randomization Results*/

            if (newCase.Status != oldCase.Status || newCase.VTD1_Eligibility_Status__c != oldCase.VTD1_Eligibility_Status__c
                    || newCase.VTR2_WashoutRunInOutcome__c != oldCase.VTR2_WashoutRunInOutcome__c) {
                if (newCase.VTD2_Baseline_Visit_Complete__c && newCase.VTD1_Study__c != null
                        && currentCases.get(newCase.Id).VTD1_Study__r.VTD1_Randomized__c == 'Yes'

                        && ((newCase.VTD1_Eligibility_Status__c == 'Eligible' && newCase.Status == SCREENED

                        && !currentCases.get(newCase.Id).VTD1_Study__r.VTD2_Washout_Run_In__c)
                        || (newCase.Status == WASHOUT_RUN_IN && newCase.VTR2_WashoutRunInOutcome__c == 'Successful'
                        && currentCases.get(newCase.Id).VTD1_Study__r.VTD2_Washout_Run_In__c))) {
                    taskToAssignByCaseMap.put(newCase.Id, new VT_D2_TNCatalogTasks.ParamsHolder(newCase.Id, '226'));

                    // Flow.Interview.Create_Task_for_SCR_or_PG
                }
            }
            if ((newCase.VTD1_Eligibility_Status__c != oldCase.VTD1_Eligibility_Status__c
                    && newCase.VTD1_Eligibility_Status__c == 'Ineligible' && !newCase.VTR2_Eligibility_Conducted_Outside_SH__c)
                    || ((newCase.Status != oldCase.Status || newCase.VTR2_WashoutRunInOutcome__c != oldCase.VTR2_WashoutRunInOutcome__c)
                    && newCase.Status == WASHOUT_RUN_IN && newCase.VTR2_WashoutRunInOutcome__c == 'Failure')) {
                newCase.VTD2_Failure_Date__c = System.now().date();
                caseActualVisit.add(newCase);
            }
            /**Case - Reschedule Transferred Patient's Visits, Case -Patient Transfer Date Update
             * | working with 'When PI contact is changed update PI User'*/
            if (newCase.VTD1_Virtual_Site__c != oldCase.VTD1_Virtual_Site__c) {


                userTasks.add(new VT_D2_TNCatalogTasks.ParamsHolder(newCase.Id, '218'));
                if (newCase.VTD1_Virtual_Site__c != null) {
                    newCase.Patient_Transfer_Date__c = System.now().date();
                }
            }
            /* Commented this code because it always fails: Case_Update_PI flow updates the same case in trigger.isbefore context: SELF_REFERENCE_FROM_TRIGGER error
            The same logic is implemented in VT_R2_CasePiContactUserSyncer.syncIds method.

            //When PI contact is Changed update PI User | working dont need advanced
            if (newCase.VTD1_PI_contact__c != oldCase.VTD1_PI_contact__c
                    || newCase.VTD1_Backup_PI_Contact__c != oldCase.VTD1_Backup_PI_Contact__c) {
                Map<String, Object> paramsMap = new Map<String, Object>();
                paramsMap.put('varCaseID', newCase.Id);
                if(VT_R5_CaseProcessHandler.skipFlows == false){
                    Flow.Interview.Case_Update_PI fireFlow = new Flow.Interview.Case_Update_PI(paramsMap);
                    if (!Test.isRunningTest()) {
                        fireFlow.start();
                    }
                }
            }
            */
            /**Case - update Termination Reason Description and Termination Reason Description*/
            if (newCase.VTD1_Reason_Code__c != oldCase.VTD1_Reason_Code__c) {
                newCase.VTD1_Termination_Reason_Description__c = newCase.VTD1_Reason_Code__c;
            }
            if (newCase.VTR3_Dropped_OtherReason_Description__c != oldCase.VTR3_Dropped_OtherReason_Description__c) {
                newCase.VTD1_Termination_OtherReason_Description__c = newCase.VTR3_Dropped_OtherReason_Description__c;
            }
            /**RAND2 Increment Number of Patients can i set Countries*/
//            TODO move to patient conversation
            if ((!newCase.VTD2_TMA_Review_Required__c ||
                    newCase.VTD2_Study_Geography__c != oldCase.VTD2_Study_Geography__c)
                    && (newCase.RecordTypeId == ID_CAREPLAN && newCase.VTD1_Study__c != null
                    && newCase.VTD2_Study_Geography__c != null
                    && currentCases.get(newCase.Id).VTD1_Study__r.VTD2_TMA_Eligibility_Review_Options__c == 'Countries'
                    && currentCases.get(newCase.Id).VTD2_Study_Geography__r.VTD2_TMA_Review_Required__c)) {
                caseStudyGeographies.add(newCase);
//            } else if (newCase.VTD2_Geography_Flow_is_Complete__c != oldCase.VTD2_Geography_Flow_is_Complete__c
//                    && (newCase.RecordTypeId == ID_CAREPLAN && newCase.VTD1_Study__c != null
//                    && newCase.VTD2_Geography_Flow_is_Complete__c &&
//                    currentCases.get(newCase.Id).VTD1_Study__r.VTD2_TMA_Eligibility_Review_Options__c == 'Sites'
//                    && currentCases.get(newCase.Id).VTD1_Study__r.VTD2_TMA_Eligibility_Review_Required__c && newCase.VTD1_Virtual_Site__c != null
//                    && currentCases.get(newCase.Id).VTD1_Virtual_Site__r.VTD2_TMA_Review_Required__c)) {
//                newCase.VTD2_TMA_Review_Required__c = true;
//            }
            } else if (!newCase.VTD2_TMA_Review_Required__c && (newCase.RecordTypeId == ID_CAREPLAN && newCase.VTD1_Study__c != null
                    && currentCases.get(newCase.Id).VTD1_Study__r.VTD2_TMA_Eligibility_Review_Options__c == 'Sites'
                    && currentCases.get(newCase.Id).VTD1_Study__r.VTD2_TMA_Eligibility_Review_Required__c && newCase.VTD1_Virtual_Site__c != null
                    && currentCases.get(newCase.Id).VTD1_Virtual_Site__r.VTD2_TMA_Review_Required__c)) {
                newCase.VTD2_TMA_Review_Required__c = true;
            }
        }

        if (!scheduledActions.isEmpty()) {
            new VT_R5_GenericScheduledActionsEvent(scheduledActions).publish();
        }

        if (!taskToAssignByCaseMap.isEmpty()) {
            VT_R5_ActualVisitProcessesHandler.assignToPGorSCR(taskToAssignByCaseMap);
            userTasks.addAll(taskToAssignByCaseMap.values());
        }
        if (!caseActualVisit.isEmpty()) {
            createActualVisits(caseActualVisit);
        }
        if (!caseStatusHistories.isEmpty()) {
            insert caseStatusHistories;
        }
        if (!caseStudyGeographies.isEmpty()) {
            List<Case> cases = updateCaseStudyGeography(caseStudyGeographies);
        }
    }

    public static void executeCaseProcessAfterUpdate(Map<Id, SObject> newCases, Map<Id, SObject> oldCases) {
        Map<Id, Case> currentCases = getCaseMap(newCases);
        Map<Id, Case> actualVisitsCase = new Map<Id, Case>();

        Set<Id> caseIdsForUpdateContact = new Set<Id>();
        Set<Id> caseIdsWithReconciledPatients = new Set<Id>();
        Set<Id> accountIdsForStudyCompletionEmail = new Set<Id>();
        List<Case> css = newCases.values();
        List<Case> casesForScreenFailureEmail = new List<Case>();
        List<Case> casesForRandomizationEmail = new List<Case>();

        List<VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder> svtanParams = new List<VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder>();
        List<VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder> paramsNoEnrollmentDate = new List<VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder>();
        List<VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder> paramsWithEnrollmentDate = new List<VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder>();
        for (Case newCase : css) {
            Case oldCase = (Case) oldCases.get(newCase.Id);
            if (newCase.Status != oldCase.Status) {
                /**PI Notification when Status = Pre Consent*/
                if (newCase.Status == PRE_CONSENT
                        && newCase.VTD1_PI_user__c != null) {
                    notifications.add(new VT_D2_TNCatalogNotifications.ParamsHolder(newCase.Id, 'N058'));
                }
                /**Notify PG when Clinical Study Membership Status Changes*/
                if (newCase.RecordTypeId == ID_CAREPLAN
                        && newCase.VTD1_Primary_PG__c != null) {
                    notifications.add(new VT_D2_TNCatalogNotifications.ParamsHolder(newCase.Id, 'P404'));
                }
                /**Study Completion Email 3*/
                if (newCase.Status == COMPLETED) {
                    accountIdsForStudyCompletionEmail.add(newCase.AccountId);
                    // Flow.Interview.VTD1_Study_Completion_Email_2
                }
                /**NotificationC when Patient Status is Screen Failure | Washout/Run-In Failure*/
                if (newCase.Status == SCREEN_FAILURE
                        || newCase.Status == WASHOUT_RUN_IN_FAILURE) {
                    notifications.add(new VT_D2_TNCatalogNotifications.ParamsHolder(newCase.Id, 'N073'));
                    casesForScreenFailureEmail.add(newCase);
                }
                if (newCase.Status == ACTIVE_RANDOMIZED) {
                    /**Community Home UI switch for Patient*/
                    caseIdsForUpdateContact.add(newCase.Id);
                    /**Notify PI about successful randomization*/
                    if (newCase.VTD1_PI_user__c != null /*&& currentCases.get(newCase.Id).VTD1_Study__r.VTD1_Randomized__c == 'Yes'*/) {
                        notifications.add(new VT_D2_TNCatalogNotifications.ParamsHolder(newCase.Id, 'N077'));
                        casesForRandomizationEmail.add(newCase);
                    }

                }
            }
            /**Count Number of Reconciled Patients*/
            if (newCase.VTD1_Drug_Reconciliation_Process__c != oldCase.VTD1_Drug_Reconciliation_Process__c
                    && newCase.VTD1_Drug_Reconciliation_Process__c == 'Complete') {
                caseIdsWithReconciledPatients.add(newCase.Id);
                // Flow.Interview.Count_Number_of_Reconciled_Patients
            }
            /**Cases - Initiate Tasks*/
            if (newCase.VTD1_Reconsent_Needed__c != oldCase.VTD1_Reconsent_Needed__c
                    && !newCase.VTD1_Reconsent_Needed__c && !newCase.VTD1_Reconsent_Refused__c
                    && newCase.VTD1_Study__c != null
                    && currentCases.get(newCase.Id).VTD1_Study__r.VTD1_Study_Admin__c != null) {
                userTasks.add(new VT_D2_TNCatalogTasks.ParamsHolder(newCase.Id, 'T539'));
            }
            /**Schedule Visit Task & generate Baseline Visit*/
            if (newCase.VTD1_Study__c != null &&
                    (   (!currentCases.get(newCase.Id).VTD1_Study__r.VTD2_Washout_Run_In__c
                            && (newCase.Status == SCREENED
                            || newCase.Status == CONSENTED)
                            && newCase.VTD1_Eligibility_Status__c == 'Eligible'
                            && (oldCase.VTD1_Eligibility_Status__c != 'Eligible' || isStatusChanged(newCase, oldCase)))
                            ||
                            (currentCases.get(newCase.Id).VTD1_Study__r.VTD2_Washout_Run_In__c
                                    && newCase.Status == WASHOUT_RUN_IN
                                    && newCase.VTR2_WashoutRunInOutcome__c == 'Successful'
                                    && (oldCase.VTR2_WashoutRunInOutcome__c != 'Successful' || isStatusChanged(newCase, oldCase)))
                    )
                    ) {
                VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder svtan = new VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder();
                svtan.visitCaseId = newCase.Id;
                svtan.siteStaffRespForElgl = currentCases.get(newCase.Id).VTD1_Study__r.VTR2_Site_Staff_Responsible_for_Elgbl__c;
                svtan.scrId = newCase.VTR2_SiteCoordinator__c;
                svtanParams.add(svtan);
            }
            /**Schedule Visits Task & Notification 2*/
            if ((newCase.VTD1_Enrollment_Date__c != null
                    && newCase.VTD1_Enrollment_Date__c != oldCase.VTD1_Enrollment_Date__c)
                    || // SH-13732 Added for Study with Baseline visit after Active/Randomized
                    (newCase.VTR5_LastOnboardingVisitCompleteDate__c != null
                            && oldCase.Status != newCase.Status
                            && newCase.Status == ACTIVE_RANDOMIZED
                            && newCase.VTR5_Baseline_Visit_After_A_R_Flag__c
                    )
                    ) {
                if (!newCase.VTR5_Baseline_Visit_After_A_R_Flag__c) {
                    VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder svtan = new VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder();
                    svtan.caseIdSTM = newCase.Id;
                    svtanParams.add(svtan);
                }
                // SH-13732
                else {
                    VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder param = new VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder();
                    param.caseIdSTM = newCase.Id;
                    if (newCase.VTD1_Enrollment_Date__c != null) {
                        paramsWithEnrollmentDate.add(param);
                    } else {
                        paramsNoEnrollmentDate.add(param);
                    }
                }
            }
            /**Start Up - Case 1*/
            if (newCase.VTD1_Sponsor_Response_Received__c != oldCase.VTD1_Sponsor_Response_Received__c
                    && newCase.VTD1_Sponsor_Response_Received__c && newCase.RecordTypeId == ID_CASE_SPF) {
                if (newCase.VTD1_Document__c != null && currentCases.get(newCase.Id).VTD1_Document__r.VTD1_Regulatory_Document_Type__c == 'VTSIF') {
                    userTasks.add(new VT_D2_TNCatalogTasks.ParamsHolder(newCase.Id, 'T605'));
                } else if (newCase.VTD1_Document__c != null
                        && currentCases.get(newCase.Id).VTD1_Document__r.VTD1_Regulatory_Document_Type__c == 'Site Activation Approval Form') {
                    userTasks.add(new VT_D2_TNCatalogTasks.ParamsHolder(newCase.Id, 'T608'));
                } else if (newCase.VTD2_Monitoring_Visit__c != null
                        && currentCases.get(newCase.Id).VTD2_Monitoring_Visit__r.VTD1_Site_Visit_Type__c == 'Selection') {
                    userTasks.add(new VT_D2_TNCatalogTasks.ParamsHolder(newCase.Id, 'T607'));
                }
            }
            /**Update Preferred Lab Visit*/
            if (newCase.Preferred_Lab_Visit__c != null && newCase.Preferred_Lab_Visit__c != oldCase.Preferred_Lab_Visit__c
                    && (newCase.Preferred_Lab_Visit__c == 'Home Health Nurse' || newCase.Preferred_Lab_Visit__c == 'PSC'
                    || newCase.Preferred_Lab_Visit__c == 'Phlebotomist')) {
                actualVisitsCase.put(newCase.Id, newCase);
            }
        }
        if (!caseIdsWithReconciledPatients.isEmpty()) {
            countNumberOfReconciledPatients(caseIdsWithReconciledPatients);
        }
        if (!accountIdsForStudyCompletionEmail.isEmpty()) {
            createStudyCompletionEmail(accountIdsForStudyCompletionEmail);
        }
        if (!casesForRandomizationEmail.isEmpty()) {
            notifyPIAboutRandomization(casesForRandomizationEmail);
        }
        if (!caseIdsForUpdateContact.isEmpty()) {
            updateAccsContacts(caseIdsForUpdateContact);
        }
        if (!casesForScreenFailureEmail.isEmpty()) {
            notifyPatientScreenFailure(casesForScreenFailureEmail);
        }
        if (!actualVisitsCase.isEmpty()) {
            updatePreferredLabVisit(actualVisitsCase);
        }
        if(!svtanParams.isEmpty()){
            VTR5_ScheduleVisitsTaskAndNotification.doVisitSchedulingAndTasks(svtanParams);
        }
        if(!paramsNoEnrollmentDate.isEmpty()){
            VTR5_ScheduleVisitsTaskAndNotification.isLillyStudy = true;
            VTR5_ScheduleVisitsTaskAndNotification.isLastOnboardingVisitComplete = true;
            VTR5_ScheduleVisitsTaskAndNotification.doVisitSchedulingAndTasksSync(paramsNoEnrollmentDate);
        }
        if(!paramsWithEnrollmentDate.isEmpty()){
            VTR5_ScheduleVisitsTaskAndNotification.isLillyStudy = true;
            VTR5_ScheduleVisitsTaskAndNotification.isLastOnboardingVisitComplete = false;
            VTR5_ScheduleVisitsTaskAndNotification.doVisitSchedulingAndTasksSync(paramsWithEnrollmentDate);
        }
        sendTaskNotificationAndEmail();
    }

    public static void executeCaseProcessBeforeInsert(Map<Id, SObject> newCases) {
        for (Case newCase : (List<Case>) newCases.values()) {
            /**RAND2 Randomization Results | 2 and 3 diamonds*/
            if ((newCase.VTD1_Eligibility_Status__c == 'Ineligible' && !newCase.VTR2_Eligibility_Conducted_Outside_SH__c)
                    || (newCase.Status == WASHOUT_RUN_IN && newCase.VTR2_WashoutRunInOutcome__c == 'Failure')) {
                newCase.VTD2_Failure_Date__c = System.now().date();
            }
        }
    }

    public static void executeCaseProcessAfterInsert(Map<Id, SObject> newCases) {
        Map<Id, Case> currentCases = getCaseMap(newCases);
        List<Case> caseStudyGeographies = new List<Case>();
        List<Case> casesToUpdate = new List<Case>();
        List<Case> css = newCases.values();
        List<VTD1_Case_Status_History__c> caseStatusHistories = new List<VTD1_Case_Status_History__c>();
        List<Case> caseActualVisit = new List<Case>();

        for (Case newCase : css) {
            Case cs = new Case(Id = newCase.Id);
            /**RAND2 Randomization Results | 2 and 3 diamond*/
            if ((newCase.VTD1_Eligibility_Status__c == 'Ineligible' && !newCase.VTR2_Eligibility_Conducted_Outside_SH__c)
                    || (newCase.Status == WASHOUT_RUN_IN && newCase.VTR2_WashoutRunInOutcome__c == 'Failure')) {
                caseActualVisit.add(newCase);
            }
            /**PI Notification when Status = Pre Consent*/
            if (newCase.Status == PRE_CONSENT
                    && newCase.VTD1_PI_user__c != null) {
                notifications.add(new VT_D2_TNCatalogNotifications.ParamsHolder(newCase.Id, 'N058'));
            }
            /**Create Case Status History*/
            caseStatusHistories.add(new VTD1_Case_Status_History__c(
                    VTD1_Case__c = newCase.Id,
                    VTD1_Is_Active__c = true,
                    VTD1_Status__c = newCase.Status));
            /**RAND2 Increment Number of Patients*/
            if ((!newCase.VTD2_TMA_Review_Required__c
                    && newCase.RecordTypeId == ID_CAREPLAN && newCase.VTD1_Study__c != null
                    && newCase.VTD2_Study_Geography__c != null
                    && currentCases.get(newCase.Id).VTD1_Study__r.VTD2_TMA_Eligibility_Review_Options__c == 'Countries'
                    && currentCases.get(newCase.Id).VTD2_Study_Geography__r.VTD2_TMA_Review_Required__c)) {
                cs.VTD2_Study_Geography__c = newCase.VTD2_Study_Geography__c;
                caseStudyGeographies.add(cs);
            } else if (!newCase.VTD2_TMA_Review_Required__c && (newCase.RecordTypeId == ID_CAREPLAN && newCase.VTD1_Study__c != null
                    && currentCases.get(newCase.Id).VTD1_Study__r.VTD2_TMA_Eligibility_Review_Options__c == 'Sites'
                    && currentCases.get(newCase.Id).VTD1_Study__r.VTD2_TMA_Eligibility_Review_Required__c && newCase.VTD1_Virtual_Site__c != null
                    && currentCases.get(newCase.Id).VTD1_Virtual_Site__r.VTD2_TMA_Review_Required__c)) {
                cs.VTD2_TMA_Review_Required__c = true;
                casesToUpdate.add(cs);
            }
        }
        if (!caseActualVisit.isEmpty()) {
            createActualVisits(caseActualVisit);
        }
        if (!caseStudyGeographies.isEmpty()) {
            casesToUpdate.addAll(updateCaseStudyGeography(caseStudyGeographies));
        }
        if (!casesToUpdate.isEmpty()) {
            update casesToUpdate;
        }
        if (!caseStatusHistories.isEmpty()) {
            insert caseStatusHistories;
        }
    }

    public static void updatePreferredLabVisit(Map<Id, Case> newCases) {
        List<VTD1_Actual_Visit__c> avToUpdate = new List<VTD1_Actual_Visit__c>();
        List<VTD1_Actual_Visit__c> actualVisits = [
                SELECT Sub_Type__c, VTR2_Modality__c, VTD1_Unscheduled_Visit_Type__c, VTR2_SubtypeUpdate__c, RecordTypeId,
                        Unscheduled_Visits__c, VTD1_Case__c
                FROM VTD1_Actual_Visit__c
                WHERE VTD1_Case__c IN :newCases.keySet()
                OR Unscheduled_Visits__c IN :newCases.keySet()
                FOR UPDATE
        ];
        for (VTD1_Actual_Visit__c actualVisit : actualVisits) {
            if (actualVisit.VTR2_SubtypeUpdate__c) {
                if ((newCases.get(actualVisit.VTD1_Case__c) != null
                        && newCases.get(actualVisit.VTD1_Case__c).Preferred_Lab_Visit__c == 'Home Health Nurse')
                        || (newCases.get(actualVisit.Unscheduled_Visits__c) != null
                        && newCases.get(actualVisit.Unscheduled_Visits__c).Preferred_Lab_Visit__c == 'Home Health Nurse')) { //Update Preferred Lab Visit
                    if (actualVisit.RecordTypeId == ID_ACTUAL_VISIT_LABS
                            || (actualVisit.RecordTypeId == ID_ACTUAL_VISIT_UNSCHEDULED
                            && actualVisit.VTD1_Unscheduled_Visit_Type__c == 'Labs')) {
                        if (actualVisit.RecordTypeId == ID_ACTUAL_VISIT_UNSCHEDULED) {
                            actualVisit.VTR2_Modality__c = 'At Home';
                        }
                        actualVisit.Sub_Type__c = 'Home Health Nurse';
                    }
                } else if ((newCases.get(actualVisit.VTD1_Case__c) != null
                        && newCases.get(actualVisit.VTD1_Case__c).Preferred_Lab_Visit__c == 'PSC')
                        || (newCases.get(actualVisit.Unscheduled_Visits__c) != null
                        && newCases.get(actualVisit.Unscheduled_Visits__c).Preferred_Lab_Visit__c == 'PSC')) {
                    if (actualVisit.RecordTypeId == ID_ACTUAL_VISIT_LABS
                            || (actualVisit.RecordTypeId == ID_ACTUAL_VISIT_UNSCHEDULED
                            && actualVisit.VTD1_Unscheduled_Visit_Type__c == 'Labs')) {
                        if (actualVisit.RecordTypeId == ID_ACTUAL_VISIT_UNSCHEDULED) {
                            actualVisit.VTR2_Modality__c = 'At Location';
                        }
                        actualVisit.Sub_Type__c = 'PSC';
                    }
                } else if ((newCases.get(actualVisit.VTD1_Case__c) != null
                        && newCases.get(actualVisit.VTD1_Case__c).Preferred_Lab_Visit__c == 'Phlebotomist')
                        || (newCases.get(actualVisit.Unscheduled_Visits__c) != null
                        && newCases.get(actualVisit.Unscheduled_Visits__c).Preferred_Lab_Visit__c == 'Phlebotomist')) {
                    if (actualVisit.RecordTypeId == ID_ACTUAL_VISIT_LABS
                            || (actualVisit.RecordTypeId == ID_ACTUAL_VISIT_UNSCHEDULED
                            && actualVisit.VTD1_Unscheduled_Visit_Type__c == 'Labs')) {
                        if (actualVisit.RecordTypeId == ID_ACTUAL_VISIT_UNSCHEDULED) {
                            actualVisit.VTR2_Modality__c = 'At Home';
                        }
                        actualVisit.Sub_Type__c = 'Phlebotomist';
                    }
                }
                avToUpdate.add(actualVisit);
            }
        }
        if (!avToUpdate.isEmpty()) {
            update avToUpdate;
        }
    }

    public static void updateAccsContacts(Set<Id> caseIds) {
        List<Contact> accsContacts = [
                SELECT VTD1_FullHomePage__c
                FROM Contact
                WHERE Account.HealthCloudGA__CarePlan__c IN :caseIds
                FOR UPDATE
        ];
        for (Contact ct : accsContacts) {
            ct.VTD1_FullHomePage__c = true;
        }
        if (!accsContacts.isEmpty()) {
            update accsContacts;
        }
    }

    public static void notifyPIAboutRandomization(List<Case> newCases) {
        for (Case cs : newCases) {
            VT_R3_EmailsSender.ParamsHolder emailParamsHolder = new VT_R3_EmailsSender.ParamsHolder();
            emailParamsHolder.recipientId = cs.VTD1_PI_user__c;
            emailParamsHolder.templateDevName = 'Patient_Randomized_notification';
            emailParamsHolder.relatedToId = cs.Id;
            emailParamsHolders.add(emailParamsHolder);
        }
    }

    public static void notifyPatientScreenFailure(List<Case> newCases) {
        for (Case cs : newCases) {
            VT_R3_EmailsSender.ParamsHolder emailParamsHolder = new VT_R3_EmailsSender.ParamsHolder();
            emailParamsHolder.recipientId = cs.VTD1_Patient_User__c;
            emailParamsHolder.templateDevName = 'VTR3_Patient_is_ineligible_for_the_study';
            emailParamsHolder.relatedToId = cs.Id;
            emailParamsHolders.add(emailParamsHolder);
        }
    }

    /**rewrited flow 'Case - Increment Patients Number'*/
    public static List<Case> updateCaseStudyGeography(List<Case> caseStudyGeographies) {
        getStudyGeographyMap(caseStudyGeographies);
        List<Case> casesToUpdate = new List<Case>();
        Map<Id,VTD2_Study_Geography__c> studyGeographiesToUpdateMap = new Map<Id,VTD2_Study_Geography__c>();
        for (Case cs : caseStudyGeographies) {
            VTD2_Study_Geography__c studyGeography = studyGeographyMap.get(cs.VTD2_Study_Geography__c);
            if (studyGeography != null && studyGeography.VTD2_TMA_Number__c <= studyGeography.VTD2_Current_Patients__c + 1 && !studyGeographiesToUpdateMap.containsKey(studyGeography.Id)) {
                cs.VTD2_TMA_Review_Required__c = true;
                casesToUpdate.add(cs);
                studyGeography.VTD2_Current_Patients__c = studyGeography.VTD2_Current_Patients__c + 1;
                studyGeographiesToUpdateMap.put(studyGeography.Id,studyGeography);
            }
        }
        update studyGeographiesToUpdateMap.values();
        return casesToUpdate;
    }

    private static void createActualVisits(List<Case> caseActualVisit) {
        List<VTD1_Actual_Visit__c> actualVisits = new List<VTD1_Actual_Visit__c>();
        for (Case cs : caseActualVisit) {
            if (cs.VTD1_Eligibility_Status__c == 'Ineligible') {
                actualVisits.add(new VTD1_Actual_Visit__c(
                        Name = 'Screening Result Visit',
                        RecordTypeId = ID_ACTUAL_VISIT_UNSCHEDULED,
                        Unscheduled_Visits__c = cs.Id,
                        VTD1_Reason_for_Request__c = 'Screening Result Visit',
                        VTD1_Unscheduled_Visit_Duration__c = 45,
                        VTD1_Unscheduled_Visit_Type__c = 'Screening Results Visit',
                        VTR2_Modality__c = 'At Home'));
            }
            if (cs.Status == WASHOUT_RUN_IN) {
                actualVisits.add(new VTD1_Actual_Visit__c(
                        Name = 'Washout/Run-In Results Visit',
                        RecordTypeId = ID_ACTUAL_VISIT_UNSCHEDULED,
                        Unscheduled_Visits__c = cs.Id,
                        VTD1_Reason_for_Request__c = 'Washout/Run-In Results Visit',
                        VTD1_Unscheduled_Visit_Duration__c = 45,
                        VTD1_Unscheduled_Visit_Type__c = 'Screening Results Visit',
                        VTR2_Modality__c = 'At Home'));
            }
        }
        if (!actualVisits.isEmpty()) {
            insert actualVisits;
        }
    }

    private static void sendTaskNotificationAndEmail() {
        if (!userTasks.isEmpty()) {
            VT_D2_TNCatalogTasks.generateTask(userTasks);
        }
        if (!notifications.isEmpty()) {
            VT_D2_TNCatalogNotifications.generateNotification(notifications);
        }
        if (!emailParamsHolders.isEmpty() && !Test.isRunningTest()) {
            VT_R3_EmailsSender.sendEmails(emailParamsHolders);
        }
        notifications.clear();
        userTasks.clear();
        emailParamsHolders.clear();
    }

    private static Boolean isStatusChanged (Case newCase, Case oldCase) {
        return (newCase.Status != oldCase.Status);
    }

    private static void countNumberOfReconciledPatients(Set<Id> caseIdsWithReconciledPatients) {
        Set<Id> piUsersIds = new Set<Id>();
        Set<Id> studyIds = new Set<Id>();
        for (Id caseId : caseIdsWithReconciledPatients) {
            Case cas = caseMap.get(caseId);
            piUsersIds.add(cas.VTD1_PI_user__c);
            studyIds.add(cas.VTD1_Study__c);
        }
        List<Study_Team_Member__c> piMembers = [
                SELECT User__c, Study__c, VTD1_Number_of_Reconciled_Patients__c FROM Study_Team_Member__c
                WHERE User__c IN :piUsersIds AND Study__c IN :studyIds
        ];
        for (Study_Team_Member__c member : piMembers) {
            VT_D2_TNCatalogTasks.ParamsHolder taskForPI = new VT_D2_TNCatalogTasks.ParamsHolder();
            VT_D2_TNCatalogNotifications.ParamsHolder notificationForPi = new VT_D2_TNCatalogNotifications.ParamsHolder();
            taskForPI.tnCode = 'T615';
            taskForPI.sourceId = member.Study__c;
            taskForPI.assignedToID = member.User__c;
            userTasks.add(taskForPI);
            notificationForPi.tnCode = 'N565';
            notificationForPi.sourceId = member.Study__c;
            notificationForPi.Receiver = member.User__c;
            notifications.add(notificationForPi);
            VT_R3_EmailsSender.ParamsHolder emailParamsHolder = new VT_R3_EmailsSender.ParamsHolder();
            emailParamsHolder.recipientId = member.User__c;
            emailParamsHolder.templateDevName = 'VTD2_Study_Drug_Reconciliation_Log_Ready_for_Signature';
            emailParamsHolder.relatedToId = member.Id;
            emailParamsHolders.add(emailParamsHolder);
            member.VTD1_Number_of_Reconciled_Patients__c++;
        }
        update piMembers;
    }

    private static void createStudyCompletionEmail(Set<Id> accountIds) {
        List<Contact> contacts = [SELECT VTD1_Clinical_Study_Membership__c FROM Contact WHERE AccountId IN :accountIds];
        for (Contact con : contacts) {
            VT_R3_EmailsSender.ParamsHolder emailParamsHolder = new VT_R3_EmailsSender.ParamsHolder();
            emailParamsHolder.recipientId = con.Id;
            emailParamsHolder.templateDevName = 'VTR3_StudyParticipationComplete';
            emailParamsHolder.relatedToId = con.VTD1_Clinical_Study_Membership__c;
            emailParamsHolders.add(emailParamsHolder);
        }
    }
}