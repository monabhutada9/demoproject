global class VT_D1_ActionItemServices {
	private class CreateActionItemRequest{
		//String UserID;//ignore
		//String UserPassword;
		String ProtocolSiteId;
		String AlertId;
		String Severity;
		String DueDate;
		String ActionRequired;
		String AssignedTo;
		String ReferenceId;
		String CreatedBy;
	}
	
	private class RetrieveListOfActionItemRequest{
		String ProtocolNumber;
		String SiteNumber;
		String Status;
		String ReferenceId;
	}
	
	private class RetrieveAuthorisedAssigneesRequest{
		String UserID;
		String SiteNumber;
		
	}
	
	private class ErrorResponse {
		String ErrorMessage;
		
		ErrorResponse(String errorText) {
			ErrorMessage = errorText;
		}
	}

	global class ActionItem{
		webservice String AlertId{get;set;}
		webservice String ActionItemId{get;set;}
		webservice String ReferenceId{get;set;}
		webservice String Title{get;set;}
		webservice String Category{get;set;}
		webservice String CreatedById{get;set;}
		webservice String CreatedByFirstName{get;set;}
		webservice String CreatedByLastName{get;set;}
		webservice String AssignedTo{get;set;}
		webservice String AISeverity{get;set;}
		webservice String Comments{get;set;}
		webservice String DueDate{get;set;}
		webservice String CreatedDate{get;set;}
		webservice String Author{get;set;}
		webservice String PIFirstName{get;set;}
		webservice String PILastName{get;set;}
		webservice String AssignedCPMFirstName{get;set;}
		webservice String AssignedCPMLastName{get;set;}
		webservice String SiteName{get;set;}
		webservice String Status{get;set;}
		webservice String ResolutionComments{get;set;}
		webservice String ResolutionDate{get;set;}
		webservice String UpdatedDate{get;set;}
		webservice String AssignedToFirstName{get;set;}
		webservice String AssignedToLastName{get;set;}
		
		
		public ActionItem(String alId, String actId){
			AlertId=alId;
			ActionItemId=actId;
		}
		public ActionItem(){
			
		}
	}
	
	global class ActionItemDetailsWrapper{
		webservice String ProtocolSiteId{get;set;}
		webservice String Country{get;set;}
		webservice List<ActionItem> actionItem{get;set;}
	}
	
	global class ListOfActionItemDetailsWrapper{
		webservice List<ActionItemDetailsWrapper> ActionItemDetails{get;set;}
	}
	
	
	///////////////////////////////////////////
	
	global class AuthorisedAssignees{
		webservice ProtocolSiteDetailWrapper ProtocolSiteDetailList;
		webservice AuthorisedAssigneesDetailsWrapper AuthorisedAssigneesDetails;
	}
	
	global class ProtocolSiteDetailWrapper{
		webservice List<ProtocolSiteDetail> ProtocolSiteDetail{get;set;}
		//webservice AuthorisedAssigneesDetailsWrapper AuthorisedAssigneesDetails{get;set;}
	}
	
	global class ProtocolSiteDetail{
		webservice String ProtocolSiteId{get;set;}
		webservice String Country{get;set;}
	}
	
	//////////////////////////////
	global class AuthorisedAssigneesDetailsWrapper{
		webservice String OwnerEmployeeId{get;set;}
		webservice List<AuthorisedAssigneeWrapper> AuthorisedAssignee{get;set;}
	}
	
	global class AuthorisedAssigneeWrapper{
		webservice String AssigneeId{get;set;}
		webservice String EmployeeId{get;set;}
		webservice String AssigneeFirstName{get;set;}
		webservice String AssigneeLastName{get;set;}
		webservice String AssigneeFullName{get;set;}
	}
	/*
	webservice static String getContextOrganizationId(String protocolId) {

		String currentOrgId = UserInfo.getOrganizationId();
		System.debug('!!! currentOrgId='+currentOrgId);

	  	return currentOrgId;
	}
	*/
	
	public static Date getDateFromString(String strDate){
		//format 2018-10-08
		List<String> parts=strDate.split('-');
		Integer i=0;
		Integer year, month, day;
		for(String p:parts){
			if(i==0) year=Integer.valueOf(p);
			if(i==1) month=Integer.valueOf(p);
			if(i==2){
				if(p.length()>=2) p=p.substring(0,2);
				day=Integer.valueOf(p);
			}
			i++;
		}
		system.debug('!!! year='+year+', month='+month+', day='+day);
		Date d=Date.newInstance(year, month, day);
		system.debug('!!! d='+d);
		return d;
	}
	
	webservice static String createActionItem(String ProtocolSiteId, String AlertId, String Severity, String DueDate, String ActionRequired, String AssignedTo, String ReferenceId, String CreatedBy){
		
		Datetime requestReceivedTimestamp = Datetime.now();
		HTTPResponse response = new HTTPResponse();
		
		VTD1_Action_Item__c newAI = new VTD1_Action_Item__c();
		
		//Check Protocol Site Id and log error in data received
		CreateActionItemRequest createActionItemRequest = new CreateActionItemRequest();
		createActionItemRequest.ProtocolSiteId = ProtocolSiteId;
		createActionItemRequest.AlertId = AlertId;
		createActionItemRequest.Severity = Severity;
		createActionItemRequest.DueDate = DueDate;
		createActionItemRequest.ActionRequired = ActionRequired;
		createActionItemRequest.AssignedTo = AssignedTo;
		createActionItemRequest.ReferenceId = ReferenceId;
		createActionItemRequest.CreatedBy = CreatedBy;
		
		if(String.isBlank(ProtocolSiteId)){
			//need to return Error
			VTD1_IntegrationInbound_Log__c log = 
				VT_D1_HTTPConnectionHandler.createIntegrationInboundLogRecord(
					System.JSON.serializePretty(createActionItemRequest)
					, null
					, 'Create Action Item'
					, requestReceivedTimestamp
					, 'ProtocolSiteId cannot be empty'
					);		
			insert log;
			
			//ErrorResponse er = new ErrorResponse('ProtocolSiteId cannot be empty');
			//return System.JSON.serializePretty(er);
			return null;
		}
		
		try {
			//find Visrual Site
			Virtual_Site__c vsite = getVSiteByProtocolSiteId(ProtocolSiteId);
			
			//need to return Error
			if(vsite==null || vsite.VTD1_Study__c==null) {
				VTD1_IntegrationInbound_Log__c log = 
					VT_D1_HTTPConnectionHandler.createIntegrationInboundLogRecord(
						System.JSON.serializePretty(createActionItemRequest)
						, null
						, 'Create Action Item'
						, requestReceivedTimestamp
						, 'Virtual Site is not found or it is not linked with Study.'
						);		
				insert log;
				
				//ErrorResponse er = new ErrorResponse('Virtual Site is not found');
				//return System.JSON.serializePretty(er);
				return null;
			}
			
			Id vsiteId=vsite.Id;
			Id studyId=vsite.VTD1_Study__c;
			
			newAI.VTD1_SiteId__c = vsiteId;
			newAI.VTD1_Study__c = studyId;
			newAI.VTD1_AlertId__c = AlertId;
			newAI.VTD1_Action_Item_Description__c = ActionRequired;
			newAI.VTD1_Severity_Description__c = Severity;
			if(!String.isBlank(DueDate)){
				//Date dDueDate=getDateFromString(DueDate);
				//newAI.VTD1_Due_Date__c=dDueDate;
				
				DateTime dtDueDate = DateTime.valueOfGmt((dueDate.replaceAll('T', ' ')));
				newAI.VTD1_Due_Date__c= date.valueOf(dtDueDate);
				
			}
			newAI.OwnerId = AssignedTo;
			newAI.VTD1_ReferenceId__c = ReferenceId;// - Ref Id for the type of issue ?
			newAI.VTD1_Author_Qid__c = CreatedBy;// - Unique CTMS UserID of the AI creator ?
			
			insert newAI;

			if(!Test.isRunningTest()) {
				//Log interaction
				VTD1_IntegrationInbound_Log__c log =
						VT_D1_HTTPConnectionHandler.createIntegrationInboundLogRecord(
								System.JSON.serializePretty(createActionItemRequest)
								, newAI.Id
								, 'Create Action Item'
								, requestReceivedTimestamp
								, null
						);
				insert log;
			}
			return newAI.Id;
		
		} catch (Exception e) {
			VTD1_IntegrationInbound_Log__c log = 
				VT_D1_HTTPConnectionHandler.createIntegrationInboundLogRecord(
					System.JSON.serializePretty(createActionItemRequest)
					, null
					, 'Create Action Item'
					, requestReceivedTimestamp
					, e.getMessage()
					);		
			insert log;
			return null;
		}
		
		
	}

	webservice static ListOfActionItemDetailsWrapper retrieveListOfActionItem(String ProtocolNumber, String SiteNumber, String AIStatus, String ReferenceId){
	
		Datetime requestReceivedTimestamp = Datetime.now();
		RetrieveListOfActionItemRequest retrieveRequest = new RetrieveListOfActionItemRequest();
		retrieveRequest.ProtocolNumber=ProtocolNumber;
		retrieveRequest.SiteNumber=SiteNumber;
		retrieveRequest.Status=AIStatus;
		retrieveRequest.ReferenceId=ReferenceId;
		
		List<ActionItemDetailsWrapper> actItemWrapList = new List<ActionItemDetailsWrapper>();
		
		Virtual_Site__c vsite = getVSiteBySiteNumber(SiteNumber);
		
		if(vsite==null){
//			VTD1_IntegrationInbound_Log__c log = 
//				VT_D1_HTTPConnectionHandler.createIntegrationInboundLogRecord(
//					System.JSON.serializePretty(retrieveRequest)
//					, null
//					, requestReceivedTimestamp
//					, 'Virtual Site is not found'
//					);		
//			insert log;
			
			return null;
		}
		
		List<VTD1_Action_Item__c> aiList= new List<VTD1_Action_Item__c>();
		
		String queryAI = 'select Id, VTD1_AlertId__c, VTD1_ReferenceId__c, VTD1_Author_Qid__c, OwnerId, VTD1_Author_First_Name__c, VTD1_Author_Last_Name__c, Owner.FirstName, Owner.LastName,  VTD1_Severity_Description__c, CreatedDate, VTD1_Due_Date__c,  VTD1_SiteId__r.VTD1_Study_Team_Member__r.User__r.FirstName, VTD1_SiteId__r.VTD1_Study_Team_Member__r.User__r.LastName, VTD1_SiteId__r.Name, VTD1_Status_Description__c, VTD1_Resolution_Description__c, VTD1_Resolution_Date__c, VTD1_Action_Item_Note_Description__c, LastModifiedDate from VTD1_Action_Item__c';
		queryAI += ' where VTD1_SiteId__r.VTD1_Study_Site_Number__c=:SiteNumber';
		
		if(!String.isBlank(ProtocolNumber)) queryAI +=' and VTD1_SiteId__r.VTD1_Study__r.Name=:ProtocolNumber';
		if(!String.isBlank(ReferenceId)) queryAI +=' and VTD1_ReferenceId__c=:ReferenceId';
		if(!String.isBlank(AIStatus)) queryAI +=' and VTD1_Status_Description__c=:AIStatus';
 		
		ailist = Database.query(queryAI);
		
		List<ActionItem> actItemList = new List<ActionItem>();
		
		if(!aiList.isEmpty()){
			
			for(VTD1_Action_Item__c aitem:aiList){
				ActionItem ai = new ActionItem();
				ai.AlertId=aitem.VTD1_AlertId__c;
				ai.ActionItemId=aitem.Id;
				ai.ReferenceId=aitem.VTD1_ReferenceId__c;
				ai.Title='';
				ai.Category='';
				if(!String.isBlank(aitem.VTD1_Author_Qid__c))
					ai.CreatedById = aitem.VTD1_Author_Qid__c;
				else
					ai.CreatedById = aitem.OwnerId;
				ai.CreatedByFirstName = aitem.VTD1_Author_First_Name__c;
				ai.CreatedByLastName = aitem.VTD1_Author_Last_Name__c;
				ai.AssignedTo = aitem.OwnerId;
				ai.AssignedToFirstName = aitem.Owner.FirstName;
				ai.AssignedToLastName = aitem.Owner.LastName;
				ai.AISeverity = aitem.VTD1_Severity_Description__c;
				ai.Comments = aitem.VTD1_Action_Item_Note_Description__c;
				if(aitem.VTD1_Due_Date__c!=null){
					Date d=aitem.VTD1_Due_Date__c;
					Datetime dt=datetime.newInstance(d.year(), d.month(),d.day());
					ai.DueDate=String.valueOf(dt.format('MM/dd/yyyy'));
					//ai.DueDate = String.valueOf(Datetime.valueOf((aitem.VTD1_Due_Date__c)).format('MM/dd/yyyy'));//String.valueOf(aitem.VTD1_Due_Date__c);
				}
				
				DateTime createdDate=aitem.CreatedDate;
				ai.CreatedDate=String.valueOf(createdDate.format('MM/dd/yyyy'));
				//ai.CreatedDate = String.valueOf(Datetime.valueOf((aitem.CreatedDate)).format('MM/dd/yyyy'));//String.valueOf(aitem.CreatedDate);
				
				if(!String.isBlank(aitem.VTD1_Author_First_Name__c))
					ai.Author = aitem.VTD1_Author_First_Name__c;
				if(!String.isBlank(aitem.VTD1_Author_Last_Name__c)){
					if(!String.isBlank(ai.Author))
						ai.Author = ai.Author + ' ';
					ai.Author = ai.Author + aitem.VTD1_Author_Last_Name__c;
				}
					
				ai.PIFirstName = aitem.VTD1_SiteId__r.VTD1_Study_Team_Member__r.User__r.FirstName;
				ai.PILastName = aitem.VTD1_SiteId__r.VTD1_Study_Team_Member__r.User__r.LastName;
				ai.AssignedCPMFirstName='';
				ai.AssignedCPMLastName='';
				ai.SiteName = aitem.VTD1_SiteId__r.Name;
				ai.Status = aitem.VTD1_Status_Description__c;
				ai.ResolutionComments = aitem.VTD1_Resolution_Description__c;
				if(aitem.VTD1_Resolution_Date__c!=null){
					Date resolutionDate=aitem.VTD1_Resolution_Date__c;
					Datetime rdt=datetime.newInstance(resolutionDate.year(), resolutionDate.month(),resolutionDate.day());
					ai.ResolutionDate=String.valueOf(rdt.format('MM/dd/yyyy'));
					//ai.ResolutionDate = String.valueOf(Datetime.valueOf((aitem.VTD1_Resolution_Date__c)).format('MM/dd/yyyy'));//String.valueOf((aitem.VTD1_Resolution_Date__c));//String.valueOf((aitem.VTD1_Resolution_Date__c).format('yyyy-MM-dd'));
				}
				
				
				Datetime udt=aitem.LastModifiedDate;
				ai.UpdatedDate=String.valueOf(udt.format('MM/dd/yyyy'));
				//ai.UpdatedDate = String.valueOf(Datetime.valueOf((aitem.LastModifiedDate)).format('MM/dd/yyyy HH:mm:ss'));//String.valueOf(aitem.LastModifiedDate);
				actItemList.add(ai);
			}
		}

		ActionItemDetailsWrapper aiWrap= new ActionItemDetailsWrapper();
		aiWrap.ProtocolSiteId = vsite.Id;
		aiWrap.Country = vsite.VTD1_Country_Code__c;
		aiWrap.actionItem = actItemList;
		
		actItemWrapList.add(aiWrap);
		
		ListOfActionItemDetailsWrapper ListOfActionItemDetails = new ListOfActionItemDetailsWrapper();
		ListOfActionItemDetails.ActionItemDetails = actItemWrapList;
		
//		VTD1_IntegrationInbound_Log__c log = 
//				VT_D1_HTTPConnectionHandler.createIntegrationInboundLogRecord(
//					System.JSON.serializePretty(retrieveRequest)
//					, System.JSON.serializePretty(ListOfActionItemDetails)
//					, requestReceivedTimestamp
//					, null
//					);		
//		insert log;
		
		return ListOfActionItemDetails;//actItemWrapList;//ai;
	}
	
	webservice static AuthorisedAssignees retrieveAuthorisedAssignees(String UserID, String SiteNumber){
		Datetime requestReceivedTimestamp = Datetime.now();
		RetrieveAuthorisedAssigneesRequest retrieveRequest = new RetrieveAuthorisedAssigneesRequest();
		retrieveRequest.UserID=UserID;
		retrieveRequest.SiteNumber=SiteNumber;
		
		
		AuthorisedAssignees assigneesObject = new AuthorisedAssignees();
		
		ProtocolSiteDetailWrapper protSiteDetWrap = new ProtocolSiteDetailWrapper();
		List<ProtocolSiteDetail> ProtocolSiteDetailList = new List<ProtocolSiteDetail>();
		ProtocolSiteDetail detail = new ProtocolSiteDetail();
		
		//Id vsiteId = getVSiteIdBySiteNumber(SiteNumber);
		
		Virtual_Site__c site = getVSiteBySiteNumber(SiteNumber);
		
		if(site==null){
			//VTD1_IntegrationInbound_Log__c log = 
			//	VT_D1_HTTPConnectionHandler.createIntegrationInboundLogRecord(
			//		System.JSON.serializePretty(retrieveRequest)
			//		, null
			//		, requestReceivedTimestamp
			//		, 'Virtual Site is not found'
			//		);		
			//insert log;
			return null;	
		}	
		
		detail.ProtocolSiteId = site.Id;
		detail.country = site.VTD1_Country_Code__c;
		
		ProtocolSiteDetailList.add(detail);
		
		protSiteDetWrap.ProtocolSiteDetail = ProtocolSiteDetailList;
				
		assigneesObject.ProtocolSiteDetailList = protSiteDetWrap;
		
		AuthorisedAssigneesDetailsWrapper assigneesWrap= new AuthorisedAssigneesDetailsWrapper();
		assigneesWrap.OwnerEmployeeId='';
		assigneesWrap.AuthorisedAssignee = getAssignees(site, UserID);//getPGsBySite(site.VTD1_Study_Team_Member__c, assigneesWrap);
		
		assigneesObject.AuthorisedAssigneesDetails = assigneesWrap;
		
		//VTD1_IntegrationInbound_Log__c log = 
		//		VT_D1_HTTPConnectionHandler.createIntegrationInboundLogRecord(
		//			System.JSON.serializePretty(retrieveRequest)
		//			, System.JSON.serializePretty(assigneesObject)
		//			, requestReceivedTimestamp
		//			, null
		//			);		
		//	insert log;
		
		return assigneesObject;
	}
	
	public static Virtual_Site__c getVSiteByProtocolSiteId(String ProtocolSiteId){
		if(String.isBlank(ProtocolSiteId)){
			//need to return Error
			return null;
		}
		
		//find Visrual Site
		Virtual_Site__c vsite;
		if(!String.isBlank(ProtocolSiteId)){
			List<Virtual_Site__c> vsiteList = [select Id, VTD1_Study__c from Virtual_Site__c where Id=:ProtocolSiteId];
			
			if(!vsiteList.isEmpty())
				vsite = vsiteList[0];
		}
		
		return vsite;
		
	}
	
	public static Id getVSiteIdBySiteNumber(String SiteNumber){
		if(String.isBlank(SiteNumber)){
			//need to return Error
			return null;
		}
		
		//find Visrual Site
		Id vsiteId;
		if(!String.isBlank(SiteNumber)){
			List<Virtual_Site__c> vsiteList = [select Id from Virtual_Site__c where VTD1_Study_Site_Number__c=:SiteNumber];
			
			if(!vsiteList.isEmpty())
				vsiteId = vsiteList[0].Id;
		}
		
		return vsiteId;
		
	}
	
	public static Virtual_Site__c getVSiteBySiteNumber(String SiteNumber){
		if(String.isBlank(SiteNumber)){
			//need to return Error
			return null;
		}
		
		//find Visrual Site
		Virtual_Site__c vsite;
		if(!String.isBlank(SiteNumber)){
			List<Virtual_Site__c> vsiteList = [select Id, VTD1_Country_Code__c, VTD1_Study_Team_Member__c, VTD1_Study__c 
												from Virtual_Site__c where VTD1_Study_Site_Number__c=:SiteNumber];
			
			if(!vsiteList.isEmpty())
				vsite = vsiteList[0];
		}
		
		return vsite;
		
	}
	
	public static List<AuthorisedAssigneeWrapper> getAssignees(Virtual_Site__c site, String qUserId){
		List<AuthorisedAssigneeWrapper> AuthorisedAssigneeList = new List<AuthorisedAssigneeWrapper>();
		getPGsBySite(site.VTD1_Study_Team_Member__c, AuthorisedAssigneeList);
		getCM(site.VTD1_Study__c, qUserId, AuthorisedAssigneeList);
		
		return AuthorisedAssigneeList;
	}
	
	public static List<AuthorisedAssigneeWrapper> getPGsBySite(Id studyTeamMemberId, List<AuthorisedAssigneeWrapper> AuthorisedAssigneeList){
		if(studyTeamMemberId==null) return AuthorisedAssigneeList;		
		
		List<Study_Team_Member__c> sTeamMembers = [select Id, VTD1_PIsAssignedPG__r.Id, VTD1_PIsAssignedPG__r.VTD1_QId__c,
														VTD1_PIsAssignedPG__r.FirstName, VTD1_PIsAssignedPG__r.LastName,
														VTD1_PIsAssignedPG__r.VTD1_Full_Name__c
														from Study_Team_Member__c where														
														Id=:studyTeamMemberId and VTD1_PIsAssignedPG__c!=null];
														
		if(sTeamMembers.isEmpty()) return AuthorisedAssigneeList;
		
		
		Study_Team_Member__c teamMemberItem = sTeamMembers[0];		
		
		AuthorisedAssigneeWrapper assignee = new AuthorisedAssigneeWrapper();
		assignee.AssigneeId = teamMemberItem.VTD1_PIsAssignedPG__r.VTD1_QId__c;
		assignee.EmployeeId = teamMemberItem.VTD1_PIsAssignedPG__r.Id;
		assignee.AssigneeFirstName = teamMemberItem.VTD1_PIsAssignedPG__r.FirstName;
		assignee.AssigneeLastName = teamMemberItem.VTD1_PIsAssignedPG__r.LastName;
		assignee.AssigneeFullName = teamMemberItem.VTD1_PIsAssignedPG__r.VTD1_Full_Name__c;
		
		AuthorisedAssigneeList.add(assignee);
		
		//teamMemberItem.VTD1_PIsAssignedPG__r.Id;
		
		/*
		List<Study_Site_Team_Member__c> ssTeamMembers = [select Id, VTD1_Associated_PG__c, VTD1_Associated_PG__r.User__c,
														VTD1_Associated_PG__r.User__r.FirstName, 
														VTD1_Associated_PG__r.User__r.LastName, VTD1_PG_Name__c
														from Study_Site_Team_Member__c where														
														VTD1_SiteID__c=:vsiteId];
														
		if(ssTeamMembers.isEmpty()) return null;
		
		List<AuthorisedAssigneeWrapper> AuthorisedAssigneeList = new List<AuthorisedAssigneeWrapper>();
		for(Study_Site_Team_Member__c teamMemberItem:ssTeamMembers){
			AuthorisedAssigneeWrapper assignee = new AuthorisedAssigneeWrapper();
			assignee.AssigneeId = teamMemberItem.VTD1_Associated_PG__r.User__c;
			//assignee.EmployeeId='';
			assignee.AssigneeFirstName = teamMemberItem.VTD1_Associated_PG__r.User__r.FirstName;
			assignee.AssigneeLastName = teamMemberItem.VTD1_Associated_PG__r.User__r.LastName;
			assignee.AssigneeFullName = teamMemberItem.VTD1_PG_Name__c;
			
			AuthorisedAssigneeList.add(assignee);
		}
		*/
		return AuthorisedAssigneeList;
	}
	
	public static void getCM(Id studyId, String qUserId, List<AuthorisedAssigneeWrapper> AuthorisedAssigneeList){
		List<Study_Team_Member__c> sTeamMembers = [select Id, User__c, User__r.VTD1_QId__c,
														User__r.FirstName, User__r.LastName,
														User__r.VTD1_Full_Name__c
														from Study_Team_Member__c where	
														Study__c=:studyId and													
														User__r.VTD1_QId__c=:qUserId];
		if(sTeamMembers.isEmpty()) return;
				
		Study_Team_Member__c teamMemberItem = sTeamMembers[0];		
		
		AuthorisedAssigneeWrapper assignee = new AuthorisedAssigneeWrapper();
		assignee.AssigneeId = teamMemberItem.User__r.VTD1_QId__c;
		assignee.EmployeeId = teamMemberItem.User__c;
		assignee.AssigneeFirstName = teamMemberItem.User__r.FirstName;
		assignee.AssigneeLastName = teamMemberItem.User__r.LastName;
		assignee.AssigneeFullName = teamMemberItem.User__r.VTD1_Full_Name__c;
		
		AuthorisedAssigneeList.add(assignee);
		
		return;
	}
}