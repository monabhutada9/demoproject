@IsTest
private class VT_R5_SkillAndChatButtonsBatchTest {
    @IsTest
    static void setupTestData() {
        List<HealthCloudGA__CarePlanTemplate__c> aList = new List<HealthCloudGA__CarePlanTemplate__c>();
        for (Integer i = 0; i < 1; i++) {
            HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c(
                    Name = 'STUDY' + i,
                    HealthCloudGA__Active__c = true,
                    City_for_return__c = 'PUNE',
                    Days_After_Lab_Collection_Before_PI_Revi__c = '2',
                    State_or_return__c = 'GB-ENG',
                    VTD1_Address_Line_1__c = 'QIVIA',
                    VTD1_Address_Line_2__c = 'PUNE',
                    VTD1_All_Sites_Selected__c = false,
                    VTD1_Bulk_Upload_Flag__c = false,
                    VTD1_Business_Days_Before_Shipment_Remin__c = '5',
                    VTD1_Carematix_Bulk_Upload__c = false,
                    VTD1_DeliveryTeamGroupId__c = '123',
                    VTD1_First_Site_Activated__c = false,
                    VTD1_ID_Required_for_IMP_Delivery__c = false,
                    VTD1_ID_Required_for_Lab_Delivery__c = false,
                    VTD1_IMP_Return_Interval__c = 'Every 2nd Kit',
                    VTD1_Integron_Bulk_Upload_Flag__c = false,
                    VTD1_Is_Test__c = false,
                    VTD1_Lab_Confirmation_Period__c = '5',
                    VTD1_Lab_Return_Interval__c = 'Never',
                    VTD1_Lab_Return_Process__c = 'Packaging Materials in Hand',
                    VTD1_Legal_Hold__c = false,
                    VTD1_Max_Number_IMP_Deliveries__c = 11.0,
                    VTD1_Max_Number_Lab_Deliveries__c = 1.0,
                    VTD1_Maximum_Days_Without_Logging_in__c = 1.0,
                    VTD1_Name__c = 'QIVIA',
                    VTD1_PD_Form_Processing_Timeframe__c = 0.0,
                    VTD1_PG_Study_Group_Id__c = '123',
                    VTD1_PIGroupId__c = '123',
                    VTD1_Patient_Community_URL__c = 'https://3dev5-iqviavirtualtrials.cs45.force.com/patient',
                    VTD1_Patient_Group_Id__c = '123',
                    VTD1_Peds_study__c = false,
                    VTD1_Phase_IV_PI_Approval_Needed__c = false,
                    VTD1_Placebo_Controlled__c = false,
                    //VTD1_Primary_Sponsor__c = '123',
                    VTD1_ProtocolAmendment_DocumentsUploaded__c = false,
                    VTD1_Ready_For_Outbound__c = false,
                    VTD1_Recruitment_Hold__c = false,
                    VTD1_Recruitment_goals_have_been_met__c = false,
                    VTD1_Require_ID_Verification_for_Deliver__c = false,
                    VTD1_Return_IMP_To__c = 'IQVIA',
                    VTD1_Return_Process__c = 'Packaging Materials in Hand',
                    VTD1_SCGroupId__c = '123',
                    VTD1_SC_Tasks_Queue_Id__c = '12123',
                    VTD1_Study_Admin_Setup_Complete__c = false,
                    VTD1_Study_Closeout_Initiated__c = false,
                    VTD1_Study_Library__c = 'iqviavirtualtrials--3dev5--c.visualforce.com/patient/s/about-study',
                    VTD1_Study_Type__c = 'Interventional',
                    VTD1_Study_Uses_Connected_Device_Kits__c = false,
                    VTD1_Study_Uses_IMP_Kits__c = false,
                    VTD1_Study_Uses_Lab_Kits__c = false,
                    VTD1_VTSL_Confirmation_Setup_Complete__c = false,
                    VTD1_Virtual_Study_Type__c = 'Virtual',
                    VTD1_ZIP__c = '1234',
                    VTD1_isBulkUpoadDateUpdated__c = false,
                    When_Does_Patient_Receive_Kit__c = 'Prior to day 0',
                    VDT2_Send_Visit_Reminders__c = false,
                    VTD2_CM_Group_Id__c = '123',
                    VTD2_Lab_Modality_Configurable__c = false,
                    VTD2_Maximum_Days_Since_Referral__c = 1.0,
                    VTD2_Rescreening_Allowed__c = false,
                    VTD2_Send_New_In_App_Messages__c = false,
                    VTD2_Send_Overdue_Notifications__c = false,
                    VTD2_Sponsor_PD_Notification__c = 'Monthly',
                    VTD2_TMA_Blinded_for_Study__c = false,
                    VTD2_TMA_Eligibility_Review_Required__c = false,
                    VTD2_TMA_PD_Review_Required__c = false,
                    VTD2_TMA_Queue_ID__c = '1233',
                    VTD2_Washout_Run_In__c = false,
                    VTR2_CRA_Group_Id__c = '123',
                    VTR2_Caregiver__c = false,
                    VTR2_EligibilityOutsideSH__c = false,
                    VTR2_FirstVisitStatus__c = 'To Be Scheduled',
                    VTR2_REGroupId__c = '123',
                    VTR2_REQueueId__c = '123',
                    VTR2_SCR_Group_Id__c = '123',
                    VTR2_Send_New_Messages__c = false,
                    VTR2_Send_eDiary_Notifications__c = false,
                    VTR2_Site_Staff_Responsible_for_Elgbl__c = false,
                    VTR3_AddressLine3__c = 'PUNE',
                    VTR3_Countryforreturn__c = 'GB',
                    VTR3_LTFU__c = false,
                    VTR5_MRR_Blinded_for_Study__c = false
            );
            aList.add(study);
        }
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CalloutMock());
        insert aList;
        Test.stopTest();
    }

    @IsTest
    static void doTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CalloutMock());
        VTR5_SkillAndChatButtonBatch.runBatch(false);
        Test.stopTest();
    }

    public with sharing class CalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            if (request.getEndpoint().endsWith('/composite')) {
                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'application/json');
                response.setBody('{' +
                                    '"compositeResponse": [{' +
                                        '"body": {' +
                                            '"id": "001R00000033JNuIAM",' +
                                            '"success": true,' +
                                            '"errors": []' +
                                        '},' +
                                        '"httpHeaders": {' +
                                            '"Location": "/services/data/v38.0/composite/sobjects"' +
                                        '},' +
                                        '"httpStatusCode": 201,' +
                                        '"referenceId": "SkillRecord"' +
                                    '}]' +
                                '}');
                response.setStatusCode(200);
                return response;
            } else {
                throw new CalloutException('Unknown callout endpoint');
            }
        }
    }
}