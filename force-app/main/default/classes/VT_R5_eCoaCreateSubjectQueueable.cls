/**
* @author: Carl Judge
* @date: 22-Sep-20
* @description: Queueable to execute ecoa subject creations
**/

public without sharing class VT_R5_eCoaCreateSubjectQueueable extends VT_R3_AbstractChainableQueueable implements Database.AllowsCallouts {
    private static final List<String> USER_RETRY_ERRORS = new List<String>{
            'unable to obtain exclusive access to this record'
    };
    private List<VT_R5_eCoaCreateSubject> actions;

    public VT_R5_eCoaCreateSubjectQueueable(List<VT_R5_eCoaCreateSubject> actions) {
        this.actions = actions;
    }
    public VT_R5_eCoaCreateSubjectQueueable() {
        this.actions = new List<VT_R5_eCoaCreateSubject>();
    }

    public void addAction(VT_R5_eCoaCreateSubject action) {
        if (actions.size() == 100 || Test.isRunningTest()) {
            VT_R5_eCoaCreateSubjectQueueable newQueueable = new VT_R5_eCoaCreateSubjectQueueable(actions);
            addToChain(newQueueable);
            actions = new List<VT_R5_eCoaCreateSubject>();
        }
        actions.add(action);
    }

    public override Type getType() {
        return VT_R5_eCoaCreateSubjectQueueable.class;
    }

    public override void executeLogic() {
        System.debug('Processing ' + actions.size() + ' create subject actions');

        List<VTD1_Integration_Log__c> logs = new List<VTD1_Integration_Log__c>();
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        List<User> users = new List<User>();

        for (VT_R5_eCoaCreateSubject action : actions) {
            action.setSkipLogging(true);
            action.setHoldEmail(true);
            action.setSkipUserUpdate(true);

            try {
                action.execute();
            } catch (Exception e) {
                System.debug(e.getMessage());
            }
            VTD1_Integration_Log__c log = action.getLog();
            if (log != null) {
                logs.add(log);
            }
            Messaging.SingleEmailMessage email = action.getEmail();
            if (email != null) {
                emails.add(email);
            }
            User subjectUser = action.getSubjectUser();
            if (subjectUser != null) {
                users.add(subjectUser);
            }
        }

        if (!Test.isRunningTest()) {
            if (!logs.isEmpty()) {
                insert logs;
            }
            if (!emails.isEmpty()) {
                Messaging.sendEmail(emails);
            }
        }
        if (!users.isEmpty()) {

            List<Database.SaveResult> results = Database.update(users, false);

            processUserSaveResults(results, users);
        }
    }

    private void processUserSaveResults(List<Database.SaveResult> results, List<User> users) {
        List<User> recsToRetry = new List<User>();
        for (Integer i = 0; i < results.size(); i++) {
            if (!results[i].isSuccess() && VT_D1_AsyncDML.isRetryDML(results[i].getErrors(), USER_RETRY_ERRORS).isRetry) {
                recsToRetry.add(users[i]);
            }
        }
        if (!recsToRetry.isEmpty()) {

            VT_D1_AsyncDML asyncDML = new VT_D1_AsyncDML(users, DMLOperation.UPD)

                    .setAllOrNothing(false)
                    .setRetryErrors(USER_RETRY_ERRORS)
                    .setLoggingEnabled(true)
                    .setClassForLogging(this.getType().getName());
            this.addToChain(asyncDML);
        }
    }
}