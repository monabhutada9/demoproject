/**
* @author: Carl Judge
* @date: 18-Apr-20
* @description: Helper to build queries for eCOA callouts. Based on export from eCOA attached to Jira SH-10214
**/

public class VT_R5_eCoaQueryBuilder {
    // these are case sensitive on the API side
    public enum Direction {ascending, descending}
    public enum SubstringOption {startsWith, endsWith, contains, exact}
    public enum TimeRange {today, yesterday, thisMonth, lastMonth, thisYear, lastYear}

    String type = 'TypeDbQueryDesc';
    List<String> fields = new List<String>();
    List<TypeDbSort> sorts = new List<TypeDbSort>();
    List<TypeDbFilter> filters = new List<TypeDbFilter>();
    Integer offset = 0;

    public Integer pageSize = VT_R4_ConstantsHelper_Protocol_ePRO.DIARY_SETTINGS.VTR5_DiaryResponsesDefaultPageSize__c.intValue();


    public static VT_R5_eCoaQueryBuilder getDefaultQuery() {
        return new VT_R5_eCoaQueryBuilder()
            .addSort(new VT_R5_eCoaQueryBuilder.TypeDbSort('recordTimeUtc').setDirection(Direction.ascending));
    }

    public VT_R5_eCoaQueryBuilder addField(String field) {
        this.fields.add(field);
        return this;
    }

    public VT_R5_eCoaQueryBuilder addSort(TypeDbSort dbSort) {
        this.sorts.add(dbSort);
        return this;
    }

    public VT_R5_eCoaQueryBuilder addFilter(TypeDbFilter filter) {
        this.filters.add(filter);
        return this;
    }


    public VT_R5_eCoaQueryBuilder setOffset(Integer offset) {
        this.offset = offset;
        return this;
    }

    public VT_R5_eCoaQueryBuilder setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
        return this;
    }


    public String toJSON() {
        return JSON.serialize(this, true);
    }


    public class TypeDbSort {
        String type = 'TypeDbSort';
        String field;
        String direction; // default ascending if null
        Boolean nullsFirst;

        public TypeDbSort(String field) {
            this.field = field;
        }

        public TypeDbSort setDirection(Direction direction) {
            this.direction = direction.name();
            return this;
        }

        public TypeDbSort setNullsFirst(Boolean nullsFirst) {
            this.nullsFirst = nullsFirst;
            return this;
        }
    }

    public abstract class TypeDbFilter {
        protected String type;
        protected String field;

        public TypeDbFilter(String field) {
            this.field = field;
        }
    }

    public class TypeDbFilterNumberRange extends TypeDbFilter {
        Long min;
        Boolean minInclusive; // defaults true if null
        Long max;
        Boolean maxInclusive; // defaults true if null
        Boolean invert; // defaults false if null

        public TypeDbFilterNumberRange(String field) {
            super(field);
            this.type = 'TypeDbFilterNumberRange';
        }

        public TypeDbFilterNumberRange setMin(Long min) {
            this.min = min;
            return this;
        }

        public TypeDbFilterNumberRange setMinInclusive(Boolean minInclusive) {
            this.minInclusive = minInclusive;
            return this;
        }

        public TypeDbFilterNumberRange setMax(Long max) {
            this.max = max;
            return this;
        }

        public TypeDbFilterNumberRange setMaxInclusive(Boolean maxInclusive) {
            this.maxInclusive = maxInclusive;
            return this;
        }

        public TypeDbFilterNumberRange setInvert(Boolean invert) {
            this.invert = invert;
            return this;
        }
    }

    public class TypeDbFilterNumberIncludes extends TypeDbFilter {
        List<Long> values;
        Boolean invert;

        public TypeDbFilterNumberIncludes(String field, List<Long> values) {
            super(field);
            this.type = 'TypeDbFilterNumberIncludes';
            this.values = values;
        }

        public TypeDbFilterNumberIncludes setInvert(Boolean invert) {
            this.invert = invert;
            return this;
        }
    }

    public class TypeDbFilterStringIncludes extends TypeDbFilter {
        List<String> values;
        Boolean invert;
        Boolean ignoreCase;

        public TypeDbFilterStringIncludes(String field, List<String> values) {
            super(field);
            this.type = 'TypeDbFilterStringIncludes';
            this.values = values;
        }

        public TypeDbFilterStringIncludes setInvert(Boolean invert) {
            this.invert = invert;
            return this;
        }

        public TypeDbFilterStringIncludes setIgnoreCase(Boolean ignoreCase) {
            this.ignoreCase = ignoreCase;
            return this;
        }
    }

    public class TypeDbFilterStringSearch extends TypeDbFilter {
        String value;
        Boolean invert;
        Boolean ignoreCase;
        SubstringOption subString; // defaults to 'contains' if null

        public TypeDbFilterStringSearch(String field, String value) {
            super(field);
            this.type = 'TypeDbFilterStringSearch';
            this.value = value;
        }

        public TypeDbFilterStringSearch setInvert(Boolean invert) {
            this.invert = invert;
            return this;
        }

        public TypeDbFilterStringSearch setIgnoreCase(Boolean ignoreCase) {
            this.ignoreCase = ignoreCase;
            return this;
        }

        public TypeDbFilterStringSearch setSubstringOption(SubstringOption subStringOption) {
            this.subString = subString;
            return this;
        }
    }

    public class TypeDbFilterBoolean extends TypeDbFilter {
        Boolean value;

        public TypeDbFilterBoolean(String field, Boolean value) {
            super(field);
            this.type = 'TypeDbFilterBoolean';
            this.value = value;
        }
    }

    public class TypeDbFilterTimeNamedRange extends TypeDbFilter {
        TimeRange value;
        String timeZone;

        public TypeDbFilterTimeNamedRange(String field, TimeRange value, String timeZone) {
            super(field);
            this.type = 'TypeDbFilterTimeNamedRange';
            this.value = value;
            this.timeZone = timeZone;
        }
    }

    public class TypeDbFilterTimeDayRange extends TypeDbFilter {
        TimeRange value;
        String timeZone;
        Integer newest; // max days ago
        Integer oldest; // min days ago

        public TypeDbFilterTimeDayRange(String field, String timeZone) {
            super(field);
            this.type = 'TypeDbFilterTimeDayRange';
            this.timeZone = timeZone;
        }

        public TypeDbFilterTimeDayRange setMaxDaysAgo(Integer maxDaysAgo) {
            this.newest = maxDaysAgo;
            return this;
        }

        public TypeDbFilterTimeDayRange setMinDaysAgo(Integer minDaysAgo) {
            this.oldest = minDaysAgo;
            return this;
        }
    }
}