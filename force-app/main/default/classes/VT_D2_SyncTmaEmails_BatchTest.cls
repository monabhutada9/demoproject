/**
* @author: Carl Judge
* @date: 25-Jan-19
* @description: Test for VT_D2_SyncTmaEmails_Batch
**/

@IsTest
public class VT_D2_SyncTmaEmails_BatchTest {

    @TestSetup
    private static void setupMethod() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            Test.startTest();
            Profile prof = [SELECT Id FROM Profile WHERE Name = 'Therapeutic Medical Advisor'];
            User tmaUser = new User(
                    ProfileId = prof.Id,
                    FirstName = 'tmaUser',
                    LastName = 'tmaUser',
                    Email = VT_D1_TestUtils.generateUniqueUserName(),
                    Username = VT_D1_TestUtils.generateUniqueUserName(),
                    CompanyName = 'TEST',
                    Title = 'title',
                    Alias = 'alias',
                    TimeZoneSidKey = 'America/Los_Angeles',
                    EmailEncodingKey = 'UTF-8',
                    LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_US',
                    IsActive = true
            );
            insert tmaUser;

            insert new Study_Team_Member__c(
                    Study__c = study.Id,
                    User__c = tmaUser.Id
            );
        }
    }

    @IsTest
    private static void doTest() {
        User tmaUser = [SELECT Id, Email FROM User WHERE Username LIKE '%@iqviavttest.com' AND FirstName = 'tmaUser'];
        tmaUser.Email = VT_D1_TestUtils.generateUniqueUserName();
        update tmaUser;

        String CRON_EXP = '0 0 0 15 3 ? *';
        Test.startTest();
        System.schedule('SyncTMAEmail',  CRON_EXP, new VT_D2_SyncTmaEmails_Batch());
        Test.stopTest();
    }
}