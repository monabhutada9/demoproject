/**
* @author: Ruslan Mullayanov
* @description: Returns list of Studies with Study Team, grouped by Study Team Members profiles, + other roles
**/
public with sharing class VT_D2_StudyTeamByStudyController {
    public class StudyTeam {
        public List<Study_Team_Member__c> PIs;
        public List<Study_Team_Member__c> PGs;
        public List<Study_Team_Member__c> SCs;
        public List<Study_Team_Member__c> CMs;
        public List<Study_Team_Member__c> TMAs;
        public Map<String, List<User>> otherRoles;
    }
    public class Study {
        public String id;
        public String name;
    }
    @AuraEnabled(Cacheable=true)
    public static String getMyStudies() {
        try {
            Id currentUserId = UserInfo.getUserId();
            List<Study> studies = new List<Study>();
            List<AggregateResult> arList = [
                    SELECT Study__c studyId,
                            Study__r.Name studyName
                FROM Study_Team_Member__c
                WHERE User__c =: currentUserId
                    OR Study__r.VTD1_Project_Lead__c =: currentUserId
                    OR Study__r.VTD1_Regulatory_Specialist__c =: currentUserId
                    OR Study__r.VTD1_Virtual_Trial_Study_Lead__c =: currentUserId
                    OR Study__r.VTD1_Virtual_Trial_head_of_Operations__c =: currentUserId
                    OR Study__r.VTD1_Study_Admin__c =: currentUserId
                GROUP BY Study__c, Study__r.Name
                ORDER BY Study__r.Name ASC
            ];
            if (!arList.isEmpty()) {
                for (AggregateResult ar : arList) {
                    Study study = new Study();
                    study.id = String.valueOf(ar.get('studyId'));
                    study.name = String.valueOf(ar.get('studyName'));
                    studies.add(study);
                }
            } else {
                throw new NullPointerException();
            }
            return JSON.serialize(studies);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
    @AuraEnabled(Cacheable=true)
    public static String getStudyTeam(Id studyId) {
        try {
            if (studyId == null) throw new NullPointerException();
            StudyTeam studyTeam = new StudyTeam();
            studyTeam.PIs = new List<Study_Team_Member__c>();
            studyTeam.PGs = new List<Study_Team_Member__c>();
            studyTeam.SCs = new List<Study_Team_Member__c>();
            studyTeam.CMs = new List<Study_Team_Member__c>();
            studyTeam.TMAs = new List<Study_Team_Member__c>();
            List<Study_Team_Member__c> stmList = [
                    SELECT Id,
                            User__c,
                            User__r.Name,

                            VTD1_Type__c,
                			VTD1_Active__c
                    FROM Study_Team_Member__c
                    WHERE Study__c = :studyId and VTD1_Active__c=true

                    ORDER BY User__r.Name
            ];
            for (Study_Team_Member__c stm : stmList) {
                if (stm.VTD1_Type__c== VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) {
                    studyTeam.PIs.add(stm);
                } else if (stm.VTD1_Type__c== VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME) {
                    studyTeam.PGs.add(stm);
                } else if (stm.VTD1_Type__c== VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME) {
                    studyTeam.SCs.add(stm);
                } else if (stm.VTD1_Type__c== VT_R4_ConstantsHelper_ProfilesSTM.CM_PROFILE_NAME) {
                    studyTeam.CMs.add(stm);
                } else if (stm.VTD1_Type__c== VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME) {
                    studyTeam.TMAs.add(stm);
                }
            }
            studyTeam.otherRoles = getOtherRolesMap(studyId);
            return JSON.serialize(studyTeam);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
    private static Map<String, List<User>> getOtherRolesMap(Id studyId) {
        List<String> otherSTMRoles = new List<String>{
                VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME,
                VT_R4_ConstantsHelper_ProfilesSTM.MRR_PROFILE_NAME,
                VT_R4_ConstantsHelper_ProfilesSTM.VTSL_PROFILE_NAME,
                VT_R4_ConstantsHelper_ProfilesSTM.PL_PROFILE_NAME,
                VT_R4_ConstantsHelper_ProfilesSTM.RS_PROFILE_NAME,
                VT_R4_ConstantsHelper_ProfilesSTM.PMA_PROFILE_NAME
        };
        List<Study_Team_Member__c> stmList = [
                SELECT Id,
                        User__c,
                        VTD1_Type__c
                FROM Study_Team_Member__c
                WHERE Study__c = :studyId
                AND VTD1_Type__c
                        IN :otherSTMRoles
        ];
        HealthCloudGA__CarePlanTemplate__c studyWithParticipants = [
                SELECT Id,
                        VTD1_Remote_CRA__c,
                        VTD1_Virtual_Trial_Study_Lead__c,
                        VTD1_Project_Lead__c,
                        VTD1_Regulatory_Specialist__c,
                        VTD1_Virtual_Trial_head_of_Operations__c
                FROM HealthCloudGA__CarePlanTemplate__c
                WHERE Id = :studyId
        ];
        Set<Id> userIdSet = new Set<Id>();
        for (Study_Team_Member__c stm : stmList) {
            userIdSet.add(stm.User__c);
            }
        userIdSet.add(studyWithParticipants.VTD1_Remote_CRA__c);
        userIdSet.add(studyWithParticipants.VTD1_Virtual_Trial_Study_Lead__c);
        userIdSet.add(studyWithParticipants.VTD1_Project_Lead__c);
        userIdSet.add(studyWithParticipants.VTD1_Regulatory_Specialist__c);
        userIdSet.add(studyWithParticipants.VTD1_Virtual_Trial_head_of_Operations__c);
        if (userIdSet.isEmpty()) return null;
        Map<Id, User> userMap = new Map<Id, User>([
                SELECT Id,
                        Name,
                        VTD1_Profile_Name__c
                FROM User
                WHERE Id
                        IN :userIdSet
                ORDER BY Name ASC
        ]);
        Map<String, List<User>> roleToUserMap = new Map<String, List<User>>();
        for (Id userId : userMap.keySet()) {
            if (!roleToUserMap.containsKey(userMap.get(userId).VTD1_Profile_Name__c)) {
                roleToUserMap.put(userMap.get(userId).VTD1_Profile_Name__c, new List<User>{
                        userMap.get(userId)
                });
            } else {
                List<User> users = roleToUserMap.get(userMap.get(userId).VTD1_Profile_Name__c);
                users.add(userMap.get(userId));
                roleToUserMap.put(userMap.get(userId).VTD1_Profile_Name__c, users);
            }
        }
        return roleToUserMap;
    }
}