@IsTest
public with sharing class VT_D1_CommunityChangePasswordTest {

    public static void getUserSuccessTest() {
        User user = (User) new DomainObjects.User_t().persist();

        User fetchedUser;


        Test.startTest();
        System.runAs(user) {
            fetchedUser = VT_D1_CommunityChangePasswordController.getUser();
        }
        Test.stopTest();

        System.assertEquals(user.Id, fetchedUser.Id);
    }
}