/**
 * Created by Yevhen Kharchuk on 16-Mar-20.
 */

public without sharing class VT_R4_PatientTransferCloneDocuments implements Database.Batchable<SObject>, Database.Stateful {
    private static Set<String> MEDICAL_DOCUMENT_REC_TYPES = new Set<String>{
            'VTD2_Certification',
            'VTD1_Medical_Record',
            'Medical_Record_Archived',
            'Medical_Record_Rejected',
            'Medical_Record_Release_Form',
            'VTR3_VisitDocument'
    };
    private Id oldCaseId;
    private Id newCaseId;
    private Map<Id, VTD1_Document__c> docIdToClonedDoc;

    public VT_R4_PatientTransferCloneDocuments(Id oldCaseId, Id newCaseId) {
        this.oldCaseId = oldCaseId;
        this.newCaseId = newCaseId;
        this.docIdToClonedDoc = new Map<Id, VTD1_Document__c>();
    }

    public Database.QueryLocator start(Database.BatchableContext param1) {
        return getTransferredCaseDocumentsQL(this.oldCaseId);
    }

    public void execute(Database.BatchableContext param1, List<SObject> documents) {
        List<VTD1_Document__c> forInsert = new List<VTD1_Document__c>();
        for (VTD1_Document__c doc : (List<VTD1_Document__c>) documents) {
            VTD1_Document__c clonedDoc = doc.clone();
            clonedDoc.VTD1_Clinical_Study_Membership__c = newCaseId;
            List<VTD1_Document__c> certs = [SELECT Id FROM VTD1_Document__c WHERE Id = :doc.VTD2_Certification__c];
            if (!certs.isEmpty()) {
                clonedDoc.VTD2_Certification__c = certs[0].Id;
            }
            docIdToClonedDoc.put(doc.Id, clonedDoc);
            forInsert.add(clonedDoc);
        }

        insert forInsert;
    }

    public void finish(Database.BatchableContext param1) {
        Map<Id, VTD1_Document__c> documents = getTransferredCaseDocuments(oldCaseId); 
        if (!Test.isRunningTest()) {
            Database.executeBatch(new VT_R4_PatientTransferCloneFiles(
                    this.docIdToClonedDoc, this.newCaseId
            ), 1);
        }
    }

    private Database.QueryLocator getTransferredCaseDocumentsQL(Id oldCaseId) {
        return Database.getQueryLocator(getQueryString());
    }

    private Map<Id, VTD1_Document__c> getTransferredCaseDocuments(Id oldCaseId) {
        return new Map<Id, VTD1_Document__c>((VTD1_Document__c[]) Database.query(getQueryString()));
    }

    private String getQueryString() {
        return 'SELECT Document_Name__c, Id, Name, Reason__c, RecordTypeId, Site_RSU_NS__c, TMF_Clock__c,' +
                'VTD1_Approval_Date__c, VTD1_Approval_Letter__c, VTD1_Archived_Date_Stamp__c,' +
                'VTD1_Clinical_Study_Membership__c, VTD1_Comment__c, VTD1_Comments__c, VTD1_Current_Version__c,' +
                'VTD1_Current_Workflow__c, VTD1_Deletion_Date__c, VTD1_Deletion_Reason__c, VTD1_Deletion_User__c,' +
                'VTD1_DFDocumentClass__c, VTD1_DFDocumentGroup__c, VTD1_Document_Locked__c, VTD1_Document_Name__c,' +
                'VTD1_Document_Type__c, VTD1_DocuSignStatus__c, VTD1_Does_file_needs_deletion__c, VTD1_EDP_Package__c,' +
                'VTD1_EDPRC_Approval_Date__c, VTD1_Eligibility_Assessment_Status__c, VTD1_Eligibility_Contest_Review__c,' +
                'VTD1_FileNames__c, VTD1_Files_have_not_been_edited__c, VTD1_Flagged_for_Deletion__c,' +
                'VTD1_Full_Approval_Date_Time__c, VTD1_Full_Version__c, VTD1_IsActive__c, VTD1_IsArchived__c,' +
                'VTD1_isArchivedVersion__c, VTD1_ISF__c, VTD1_ISF_Complete__c, VTD1_ISF_Complete_Date__c,' +
                'VTD1_Lifecycle_State__c, VTD1_Name__c, VTD1_Nickname__c, VTD1_Number__c, VTD1_Number_Before_Decimal__c,' +
                'VTD1_PG_Approved__c, VTD1_PG_AssesmentApproved__c, VTD1_PG_Rejected__c,VTR4_IRB_Approved__c,' +
                'VTD1_PI_Approved__c, VTD1_PI_Comment__c, VTD1_PI_Rejected__c, VTR4_Reconsent_Complete__c,' +
                'VTD1_Previous_Version__c, VTD1_Protocol_Amendment__c, VTD1_Regulatory_Binder__c,' +
                'VTD1_Regulatory_Document_Type__c, VTD1_RSU__c, VTD1_SC_Task_Owner__c, VTD1_Send_to_DF__c,' +
                'VTD1_Signature_Date__c, VTD1_Signature_Type__c, VTD1_Site__c, VTD1_Site_EDP__c, VTD1_Site_IRB_Flag__c,' +
                'VTD1_Site_RSU__c, VTD1_Site_RSU_Complete__c, VTD1_Site_RSU_Complete_Date__c, VTD1_Status__c,' +
                'VTD1_Study__c, VTD1_Study_IRB_Flag__c, VTD1_Study_IRB_Package__c, VTD1_Study_RSU__c,' +
                'VTD1_Study_RSU_Complete__c, VTD1_Study_RSU_Complete_Date__c, VTD1_Submission_Date__c,' +
                'VTD1_Submission_type__c, VTD1_TMF__c, VTD1_TMF_Complete__c, VTD1_TMF_Complete_Date_Time__c,' +
                'VTD1_Uploading_Not_Finished__c, VTD1_Version__c, VTD1_Version_Amendments__c,' +
                'VTD1_Why_file_is_irrelevant__c, VTD1_Why_File_is_Relevant__c, VTD2_Associated_Medical_Record_id__c,' +
                'VTD2_Associated_Medical_Records__c, VTD2_Certification__c, VTD2_Document_NameEAF__c,' +
                'VTD2_Document_Record_Type__c, VTD2_EAFStatus__c, VTD2_Eligibility_Assesment_For__c,' +
                'VTD2_Email_Patient_Record_ID__c, VTD2_Email_PI_LN__c, VTD2_Email_Protocol_Nickname__c,' +
                'VTD2_EmailPatientFullName__c, VTD2_EmailPatientId__c, VTD2_Final_Eligibility_Decision__c,' +
                'VTD2_Final_Review_Comments__c, VTD2_FirstLast_Name__c, VTD2_isTMAApprove__c, VTD2_Patient__c,' +
                'VTD2_Patient_ID__c, VTD2_Patient_Status__c, VTD2_PI_Decision_Comment__c,' +
                'VTD2_PI_for_PA_Signature_Page__c, VTD2_PI_Signature_required__c, VTD2_Previous_Owner__c,' +
                'VTD2_Previous_PI_Decision__c, VTD2_Redacted__c, VTD2_Sent_for_Signature__c, VTD2_Status_Formula__c,' +
                'VTD2_Template__c, VTD2_Template_ID__c, VTD2_TMA__c, VTD2_TMA_Comments__c,' +
                'VTD2_TMA_Eligibility_Decision__c, VTD2_TMA_Start_Date__c, VTR2_1572__c, VTR2_Description__c,' +
                'VTR2_Geography__c, VTR2_Language__c, VTR2_Way_to_Send_with_DocuSign__c,' +
                'VTR3_Category__c, VTR3_Category_add__c, VTR3_Created_By__c,VTR3_CreatedProfile__c, VTR3_Type__c,' +
                'VTR3_Upload_File_Completed__c, VTR4_ChangesAppliedToAllPatients__c' +
                ', (SELECT Id, ContentDocument.LatestPublishedVersionId FROM ContentDocumentLinks) ' +
                'FROM VTD1_Document__c ' +
                'WHERE VTD1_Clinical_Study_Membership__c = :oldCaseId ' +
                'AND RecordType.DeveloperName IN :MEDICAL_DOCUMENT_REC_TYPES';
    }
}