/**
 * Created by shume on 04.02.2019.
 */

@IsTest
private class VT_D1_ProtocolDeviationsControllerTest {
    @testSetup
    private static void setupMethod() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VTD1_Protocol_Deviation__c p = new VTD1_Protocol_Deviation__c(VTD1_PD_Status__c = 'Draft', VTD1_StudyId__c = study.Id);
        insert p;
    }

    @IsTest
    static void testBehavior() {
        HealthCloudGA__CarePlanTemplate__c s = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        User PIuser = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Primary Investigator' AND ContactId != null AND IsActive = true LIMIT 1];
        List<AggregateResult> arList = [
                SELECT Study__c studyId, Study__r.Name studyName, VTD1_Backup_PI__c isBackupPI
                FROM Study_Team_Member__c
                WHERE
                        VTD1_Type__c = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME
                GROUP BY Study__c, Study__r.Name, VTD1_Backup_PI__c
                ORDER BY Study__r.Name ASC
        ];
        System.debug(arList);
        VTD1_Protocol_Deviation__c p = [SELECT Id, VTD1_StudyId__c FROM VTD1_Protocol_Deviation__c];
        System.debug(p);
        Test.startTest();
        List<VT_D1_ProtocolDeviationsControllerRemote.ProtocolWrapper> protocols;
        System.runAs(PIuser) {
            VT_D1_ProtocolDeviationsControllerRemote.protocolsInfo pi =
                    VT_D1_ProtocolDeviationsControllerRemote.getProtocolDeviations(s.Id);
            protocols = pi.protocols;
            try {
                VT_D1_ProtocolDeviationsControllerRemote.saveDraftRemote(protocols[0].pd);
            } catch (Exception e) {
            }
            try {
                VT_D1_ProtocolDeviationsControllerRemote.acknowledgeRemote(protocols[0].pd);
            } catch (Exception e) {
            }
        }
        try {
            VTD1_Protocol_Deviation__c pd = new VTD1_Protocol_Deviation__c(Id = protocols[0].pd.Id,
                    VTD1_PD_Status__c = 'Confirmed', VTD2_PI_acknowledgment__c = false);
            update pd;
            VT_D1_ProtocolDeviationsControllerRemote.saveDraftRemote(protocols[0].pd);
        } catch (Exception e) {
        }
        System.runAs(PIuser) {
            try {
                VT_D1_ProtocolDeviationsControllerRemote.acknowledgeRemote(protocols[0].pd);
            } catch (Exception e) {
            }
        }
        Test.stopTest();
    }

    @IsTest static void testSaveDraftRemote() {
        Test.startTest();
        VTD1_Protocol_Deviation__c notInsertedDeviation = new VTD1_Protocol_Deviation__c();
        VTD1_Protocol_Deviation__c insertedDeviation = (VTD1_Protocol_Deviation__c) new DomainObjects.VTD1_Protocol_Deviation_t().persist();
        try {
            VT_D1_ProtocolDeviationsControllerRemote.getProtocolDeviations(null);
        } catch(Exception ex) {
        }
        VT_D1_ProtocolDeviationsControllerRemote.saveDraftRemote(notInsertedDeviation);
        VT_D1_ProtocolDeviationsControllerRemote.saveDraftRemote(insertedDeviation);
        Test.stopTest();
    }

    @IsTest static void testAcknowledgeRemote() {
        Test.startTest();
        VTD1_Protocol_Deviation__c insertedDeviation = new VTD1_Protocol_Deviation__c();
        insertedDeviation = [SELECT CreatedDate, Id FROM VTD1_Protocol_Deviation__c LIMIT 1][0];
        VT_D1_ProtocolDeviationsControllerRemote.acknowledgeRemote(insertedDeviation);
        VT_D1_ProtocolDeviationsControllerRemote.ProtocolWrapper protocolWrapper = new VT_D1_ProtocolDeviationsControllerRemote.ProtocolWrapper(insertedDeviation);
        String result = protocolWrapper.reportedToIRBEC;
        result = protocolWrapper.reportedToSponsor;
        protocolWrapper.reportedToIRBEC = 'true';
        Test.stopTest();
    }
}