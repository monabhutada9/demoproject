/**
 * Created by MPlatonov on 01.02.2019.
 */

global without sharing class VT_D2_TNCatalogNotifications {
    public static Boolean bulkMode;
    public static Boolean futureMode;
    public static List <ParamsHolder> holderList;

    public static Integer CHUNK_SIZE = 50;

    public interface Listener {
        void notificationsCreated(List <VTD1_NotificationC__c> notifications);
    }

    public static List <Listener> listeners = new List<Listener>();

    public class ParamsHolder {
        @InvocableVariable(Label = 'TN Code' Required = false)
        public String tnCode;

        @InvocableVariable(Label = 'TN Codes' Required = false)
        public List <String> tnCodes;

        @InvocableVariable(Label = 'Record ID' Required = false)
        public String sourceId;

        @InvocableVariable(Label = 'Record IDs' Required = false)
        public List <String> sourceIds;

        @InvocableVariable(Label = 'Subject parameter ID 1' Required = false)
        public String subjectParamId_1;

        @InvocableVariable(Label = 'Subject parameter ID 2' Required = false)
        public String subjectParamId_2;

        @InvocableVariable(Label = 'Subject parameter ID 3' Required = false)
        public String subjectParamId_3;

        @InvocableVariable(Label = 'Subject parameter ID 4' Required = false)
        public String subjectParamId_4;

        @InvocableVariable(Label = 'Subject parameter ID 5' Required = false)
        public String subjectParamId_5;

        @InvocableVariable(Label = 'Link to related event' Required = false)
        public String linkToRelatedEvent;

        @InvocableVariable(Label = 'Receiver' Required = false)
        public String Receiver;

        @InvocableVariable(Label = 'Receivers' Required = false)
        public List <String> Receivers;

        public ParamsHolder() {
        }

        public ParamsHolder(String sourceId, String tnCode) {
            this.sourceId = sourceId;
            this.tnCode = tnCode;
        }
    }
    public static Map <String, Object> settingsMap = new Map<String, Object>();
    static {
        VTD1_RTId__c settings = VTD1_RTId__c.getInstance();
        Map<String, Schema.SObjectField> infoMap = Schema.SObjectType.VTD1_RTId__c.fields.getMap();
        for (String name : infoMap.keySet()) {
            settingsMap.put(name.toLowerCase(), settings.get(name));
        }
    }

    class NotificationsCreator implements Queueable {
        final List <VTD1_NotificationC__c> notifications;
        final Integer index;
        public NotificationsCreator(List <VTD1_NotificationC__c> notifications, Integer index) {
            this.notifications = notifications;
            this.index = index;
        }
        public void execute(QueueableContext queueableContext) {
            Integer endIndex = index + CHUNK_SIZE;
            if (endIndex > notifications.size()) {
                endIndex = notifications.size();
            }
            List <VTD1_NotificationC__c> chunk = new List<VTD1_NotificationC__c>();
            for (Integer i = index; i < endIndex; i ++) {
                chunk.add(notifications[i]);
            }
            insert chunk;
            if (endIndex != notifications.size()) {
                System.enqueueJob(new NotificationsCreator(notifications, index + CHUNK_SIZE));
            }
        }
    }

    private static final Map <String, String> tnFieldNamesMapping = new Map<String, String>();
    private static final List <String> tnCatalogFieldNames = new List<String>();
    static {
        tnFieldNamesMapping.put('VTD2_T_Task_Unique_Code__c', 'VDT2_Unique_Code__c');
        tnFieldNamesMapping.put('VTD2_T_Message__c', 'Message__c');
        tnFieldNamesMapping.put('VTD2_T_Message_Parameters__c', 'VTD1_Parameters__c');
        tnFieldNamesMapping.put('VTD2_T_Receiver__c', 'VTD1_Receivers__c');
        tnFieldNamesMapping.put('VTD2_T_Has_direct_link__c', 'HasDirectLink__c');
        tnFieldNamesMapping.put('VTD2_T_DoNotSendEmail__c', 'VTD2_DoNotSendEmail__c');
        tnFieldNamesMapping.put('VTD2_T_Link_to_related_event_or_object__c', 'Link_to_related_event_or_object__c');
        tnFieldNamesMapping.put('VTD2_T_Type__c', 'Type__c');
        tnFieldNamesMapping.put('VTD2_T_Title__c', 'Title__c');
        tnFieldNamesMapping.put('VTD2_T_Care_Plan_Template__c', 'VTR2_Study__c');
        tnFieldNamesMapping.put('VTD2_T_Visit_Id__c', 'VTD2_VisitID__c');
        tnFieldNamesMapping.put('VTR2_T_CSM__c', 'VTD1_CSM__c');
        tnFieldNamesMapping.put('VTD2_T_DoNotDuplicate__c', 'VTD2_DoNotDuplicate__c');
        tnFieldNamesMapping.put('VTR3_T_Title_Parameters__c', 'VTD2_Title_Parameters__c');
        tnFieldNamesMapping.put('VTR4_T_Number_of_unread_messages__c', 'Number_of_unread_messages__c');
        tnFieldNamesMapping.put('VTR4_T_Read__c', 'VTD1_Read__c');
        tnFieldNamesMapping.put('VTD2_T_Assigned_To_ID__c', 'OwnerId');
        //SH-8206 Added by Warriors        
        tnFieldNamesMapping.put('VTR5_T_Email_Subject__c', 'VTR5_Email_Subject__c');
        tnFieldNamesMapping.put('VTR5_T_Email_Subject_Parameters__c', 'VTR5_Email_Subject_Parameters__c');
        tnFieldNamesMapping.put('VTR5_T_Email_Body__c', 'VTR5_Email_Body__c');
        tnFieldNamesMapping.put('VTR5_T_Email_Body_Parameters__c', 'VTR5_Email_Body_Parameters__c');

        for (String name : tnFieldNamesMapping.keySet()) {
            tnCatalogFieldNames.add(name);
        }
    }

    class PostToChatterJob implements Queueable {
        final List <VTD1_NotificationC__c> postToChatterNotifications;
        public PostToChatterJob(final List <VTD1_NotificationC__c> postToChatterNotifications) {
            this.postToChatterNotifications = postToChatterNotifications;
        }
        public void execute(QueueableContext queueableContext) {
            VT_D1_TranslateHelper.translate(postToChatterNotifications);
            if (!postToChatterNotifications.isEmpty()) {
                List<VT_D2_PostToChatter.ChatterPostTemplate> chatterTemplates = new List<VT_D2_PostToChatter.ChatterPostTemplate>();
                for (VTD1_NotificationC__c notification : postToChatterNotifications) {
                    VT_D2_PostToChatter.ChatterPostTemplate template = new VT_D2_PostToChatter.ChatterPostTemplate();
                    template.targetId = notification.VTD1_Receivers__c;
                    template.message = notification.Message__c;
                    chatterTemplates.add(template);
                }
                VT_D2_PostToChatter.postToChatter(chatterTemplates);
            }
        }
    }

    public class TNCatalogDescriptorElement {
        public final String fieldName;
        public final Object value;
    }

    public class TNCatalogDescriptor {
        public final String SObjectName;
        public final Map <String, Object> params = new Map<String, Object>();
        public final String recordTypeName;
        public final Boolean passIfEmptyRecipient;
        public String query;
        public List <String> queryParts;
        public final String tnCode;
        public TNCatalogDescriptor(String SObjectName, String recordTypeName, Boolean passIfEmptyRecipient, String tnCode) {
            this.sObjectName = SObjectName;
            this.recordTypeName = recordTypeName;
            this.passIfEmptyRecipient = passIfEmptyRecipient;
            this.tnCode = tnCode;
        }
    }

    public class QueryItem {
        TNCatalogDescriptor tnCatalogDescriptor;
        List <Id> sourceIds;
    }

    public static List <VTD1_NotificationC__c> flushNotifications() {
        if (holderList == null)
            return null;
        bulkMode = false;
        return generateNotificationProcess(holderList);
    }

    @InvocableMethod
    public static void generateNotification(List <ParamsHolder> params) {
        if (bulkMode != null && bulkMode) {
            if (holderList == null) {
                holderList = new List <ParamsHolder>();
            }
            holderList.addAll(params);
            return;
        }
        generateNotificationProcess(params);
    }

    public static List <VTD1_NotificationC__c> generateNotificationProcess(List <ParamsHolder> params) {
        List <String> tnCatalogCodes = new List<String>();
        List <Id> sourceIds = new List<Id>();
        List <String> subjectParamsIds = new List<String>();
        List <String> sObjectNames = new List<String>();
        Map <String, String> subjectParams = new Map<String, String>();
        Map <String, String> linkToRelatedEventMap = new Map<String,String>();
        Map <String, List <Id>> receiverMap = new Map<String,List <Id>>();
        
        
        for (ParamsHolder paramsHolder : params) {
            if (paramsHolder.tnCode != null && paramsHolder.Receivers == null) {
                tnCatalogCodes.add(paramsHolder.tnCode);
            }
            if (paramsHolder.sourceId != null) {
                sourceIds.add(paramsHolder.sourceId);
            }
            if (paramsHolder.sourceIds != null) {
                for (String sourceId : paramsHolder.sourceIds) {
                    sourceIds.add(sourceId);
                    if (paramsHolder.tnCode != null) {
                        tnCatalogCodes.add(paramsHolder.tnCode);
                    }
                }
            }
            if (paramsHolder.tnCodes != null) {
                for (String tnCode : paramsHolder.tnCodes) {
                    tnCatalogCodes.add(tnCode);
                }
            }
            subjectParams.put('8', paramsHolder.subjectParamId_1);
            subjectParams.put('VTD2_T_Subject_Parameter_Field_2__c', paramsHolder.subjectParamId_2);
            subjectParams.put('VTD2_T_Subject_Parameter_Field_3__c', paramsHolder.subjectParamId_3);
            subjectParams.put('VTD2_T_Subject_Parameter_Field_4__c', paramsHolder.subjectParamId_4);
            subjectParams.put('VTD2_T_Subject_Parameter_Field_5__c', paramsHolder.subjectParamId_5);
            if (paramsHolder.linkToRelatedEvent != null)
                linkToRelatedEventMap.put(paramsHolder.tnCode + paramsHolder.sourceId, paramsHolder.linkToRelatedEvent);
            if (paramsHolder.Receiver != null) {
                List <Id> ids = receiverMap.get(paramsHolder.tnCode + paramsHolder.sourceId);
                if (ids == null) {
                    ids = new List <Id>();
                    receiverMap.put(paramsHolder.tnCode + (Id)paramsHolder.sourceId, ids);
                }
                //receiverMap.put(paramsHolder.tnCode + paramsHolder.sourceId, paramsHolder.Receiver);
                ids.add(paramsHolder.Receiver);
            } else if (paramsHolder.Receivers != null) {
                Integer index = 0;
                for (String receiver : paramsHolder.Receivers) {
                    List <Id> ids = receiverMap.get(paramsHolder.tnCode + paramsHolder.sourceIds[index]);
                    if (ids == null) {
                        ids = new List <Id>();
                        receiverMap.put(paramsHolder.tnCode + paramsHolder.sourceIds[index], ids);
                    }
                    //receiverMap.put(paramsHolder.tnCode + paramsHolder.sourceId, paramsHolder.Receiver);
                    ids.add(paramsHolder.Receivers[index]);
                    index ++;
                }
            }
        }
        return generateNotifications(tnCatalogCodes, sourceIds, subjectParams, linkToRelatedEventMap, receiverMap, null);
    }

    public static List <VTD1_NotificationC__c> generateNotifications(List <String> tnCatalogCodes, List <Id> sourceIds, Map <String, String> subjectParams) {
        if (bulkMode != null && bulkMode) {
            if (holderList == null) {
                holderList = new List <ParamsHolder>();
            }
            for (Integer i = 0; i < tnCatalogCodes.size(); i ++) {
                ParamsHolder holder = new ParamsHolder();
                holder.tnCode = tnCatalogCodes[i];
                holder.sourceId = sourceIds[i];
                holderList.add(holder);
            }
            return null;
        } else {
            return generateNotifications(tnCatalogCodes, sourceIds, subjectParams, null, null, null);
        }
    }

    public static void generateNotificationsAsync(List <String> tnCatalogCodes, List <Id> sourceIds
            , Map <String, String> subjectParams, Map <String, String> linkToRelatedEventMap
            , Map<String, List<Id>> receiverMap, Map <Id, Map<String, String>> messageParams, Boolean autoInsert) {

        if (!System.isFuture() && !System.isBatch() && !System.isQueueable()) {

        String tnCatalogCodesJSON = JSON.serialize(tnCatalogCodes);
        String sourceIdsJSON = JSON.serialize(sourceIds);
        String subjectParamsJSON = subjectParams != null ? JSON.serialize(subjectParams) : null;
        String linkToRelatedEventMapJSON = linkToRelatedEventMap != null ? JSON.serialize(linkToRelatedEventMap) : null;
        String receiverMapJSON = receiverMap != null ? JSON.serialize(receiverMap) : null;
        String messageParamsJSON = messageParams != null ? JSON.serialize(messageParams) : null;
        generateNotificationsFuture(tnCatalogCodesJSON, sourceIdsJSON, subjectParamsJSON, linkToRelatedEventMapJSON
            , receiverMapJSON, messageParamsJSON, autoInsert);
    }

    }


    @Future
    public static void generateNotificationsFuture(String tnCatalogCodesJSON, String sourceIdsJSON
            , String subjectParamsJSON, String linkToRelatedEventMapJSON, String receiverMapJSON, String messageParamsJSON, Boolean autoInsert) {
        List <String> tnCatalogCodes = (List <String>)JSON.deserialize(tnCatalogCodesJSON, List <String>.class);
        List <Id> sourceIds = (List <String>)JSON.deserialize(sourceIdsJSON, List <Id>.class);
        Map <String, String> subjectParams = subjectParamsJSON != null ? (Map <String, String>)JSON.deserialize(subjectParamsJSON, Map <String, String>.class) : null;
        Map <String, String> linkToRelatedEventMap = linkToRelatedEventMapJSON != null ? (Map <String, String>)JSON.deserialize(linkToRelatedEventMapJSON, Map <String, String>.class) : null;
        Map <String, List<Id>> receiverMap = receiverMapJSON != null ? (Map<String, List<Id>>)JSON.deserialize(linkToRelatedEventMapJSON, Map<String, List<Id>>.class) : null;
        List <VTD1_NotificationC__c> notifications = generateNotifications(tnCatalogCodes, sourceIds, subjectParams
                , linkToRelatedEventMap, receiverMap, null, autoInsert);
        for (Listener listener : listeners) {
            listener.notificationsCreated(notifications);
        }
    }

    public static List <VTD1_NotificationC__c> generateNotifications(List <String> tnCatalogCodes, List <Id> sourceIds, Map <String, String> subjectParams, Map <String, String> linkToRelatedEventMap, Map<String, List<Id>> receiverMap) {
        return generateNotifications(tnCatalogCodes, sourceIds, subjectParams, linkToRelatedEventMap, receiverMap, null);
    }

    public static List <VTD1_NotificationC__c> generateNotifications(List <String> tnCatalogCodes, List <Id> sourceIds, Map <String, String> subjectParams, Map <String, String> linkToRelatedEventMap
            , Map<String, List<Id>> receiverMap, Map <Id, Map<String, String>> messageParams) {

        return generateNotifications(tnCatalogCodes, sourceIds, subjectParams, linkToRelatedEventMap, receiverMap, messageParams, true);

    }

    public static List <VTD1_NotificationC__c> generateNotifications(List <String> tnCatalogCodes, List <Id> sourceIds
            , Map <String, String> subjectParams, Map <String, String> linkToRelatedEventMap
            , Map<String, List<Id>> receiverMap, Map <Id, Map<String, String>> messageParams, Boolean autoInsert) {
        Map <String, List <Id>> sourceIdsByCodeMap = new Map<String, List<Id>>();
        Map <String, TNCatalogDescriptor> codeToTnCatalogDescriptorMap = new Map<String, TNCatalogDescriptor>();
        Map <String, List <QueryItem>> queryItemMap = new Map<String, List<QueryItem>>();
        Map <String, Set <String>> queryFieldsMap = new Map<String, Set<String>>();

        for (Integer i = 0; i < tnCatalogCodes.size(); i++) {
            String code = tnCatalogCodes[i];
            codeToTnCatalogDescriptorMap.put(code, null);
            List <Id> ids = sourceIdsByCodeMap.get(code);
            if (ids == null) {
                ids = new List<Id>();
            }
            ids.add(sourceIds[i]);
            sourceIdsByCodeMap.put(code, ids);
        }
        List <String> codes = new List<String>();
        for (String code : sourceIdsByCodeMap.keySet()) {
            codes.add('\'' + code + '\'');
        }
        List <SObject> catalogs = loadTNCatalogs(codes);
        for (SObject sobj : catalogs) {
            VTD2_TN_Catalog_Code__c tnCatalog = (VTD2_TN_Catalog_Code__c) sobj;
            String sObjectName = (String) tnCatalog.get('VTD2_T_SObject_Name__c');
            Boolean passIfEmptyRecipient = (Boolean)tnCatalog.get('VTR2_Pass_if_empty_recipient__c');
            TNCatalogDescriptor catalogDescriptor = new TNCatalogDescriptor(sObjectName, tnCatalog.RecordType.Name, passIfEmptyRecipient, tnCatalog.VTD2_T_Task_Unique_Code__c);
            VT_D2_TNCatalogUtils.catalogueType = catalogDescriptor.recordTypeName;
            List <String> queryParts = new List<String>();
            //catalogDescriptor.queryParts = new List<String>();
            Set <String> fields = new Set<String>();
            //queryParts.add('select ');
            List <QueryItem> queryItems = queryItemMap.get(sObjectName);
            if (queryItems == null) {
                queryItems = new List<QueryItem>();
                queryItemMap.put(sObjectName, queryItems);
            }

            QueryItem queryItem = new QueryItem();
            queryItem.tnCatalogDescriptor = catalogDescriptor;
            queryItem.sourceIds = sourceIdsByCodeMap.get(tnCatalog.VTD2_T_Task_Unique_Code__c);
            queryItems.add(queryItem);

            for (String catalogFieldName : tnCatalogFieldNames) {
                Object value = tnCatalog.get(catalogFieldName);
                if (catalogFieldName == 'VTD2_T_Task_Unique_Code__c') {
                    catalogDescriptor.params.put(catalogFieldName, value);
                }/* else if (catalogFieldName == 'VTD2_T_Due_Date__c') {
                    catalogDescriptor.params.put(catalogFieldName, VT_D2_TNCatalogUtils.parseDate((String) value));
                } else if (catalogFieldName == 'VTD2_T_Reminder_Date_Time__c') {
                    catalogDescriptor.params.put(catalogFieldName, VT_D2_TNCatalogUtils.parseDatetime((String) value));
                } else if (catalogFieldName == 'VTD2_T_Task_Record_Type_ID__c') {
                    String strValue = (String) value;
                    List <String> parts = strValue.split('\\.');
                    Id recordTypeId = (Id) settingsMap.get(parts[parts.size() - 1].toLowerCase());
                    catalogDescriptor.params.put(catalogFieldName, recordTypeId);
                }*/ else if (value instanceof Boolean) {
                    catalogDescriptor.params.put(catalogFieldName, value);
                } else {
                    List <VT_D2_TNCatalogUtils.ExpressionComponent> components = VT_D2_TNCatalogUtils.parseExpression(String.valueOf(value), sObjectName, fields, queryParts, false);
                    catalogDescriptor.params.put(catalogFieldName, components);
                }
            }

            Set <String> queryFields = queryFieldsMap.get(sObjectName);
            if (queryFields == null) {
                queryFields = new Set<String>();
                queryFieldsMap.put(sObjectName, queryFields);
            }

            for (String field : queryParts) {
                queryFields.add(field.replace(',', ''));
            }

            //queryItem.fieldNames.addAll(queryParts);

            //queryParts.add(' from ' + sObjectName);
            catalogDescriptor.query = 'select ' + String.join(queryParts, '') + ' from ' + sObjectName;
            codeToTnCatalogDescriptorMap.put((String) catalogDescriptor.params.get('VTD2_T_Task_Unique_Code__c'), catalogDescriptor);
        }

        List <VTD1_NotificationC__c> notifications = new List<VTD1_NotificationC__c>();
        List <VTD1_NotificationC__c> postToChatterNotifications = new List<VTD1_NotificationC__c>();
        List <VTD1_NotificationC__c> customNotifications = new List<VTD1_NotificationC__c>();

        for (String sObjectName : queryItemMap.keySet()) {
            List <QueryItem> items = queryItemMap.get(sObjectName);
            List <String> fieldsList = new List<String>();
            fieldsList.addAll(queryFieldsMap.get(sObjectName));
            Set <String> fieldsSet = new Set<String>();
            for(String field : fieldsList){
                fieldsSet.add(field.toLowerCase());
            }
//            String query = 'select ' + String.join(fieldsList, ',') + ' from ' + sObjectName;
            String query = 'select ' + String.join(new List<String>(fieldsSet), ',') + ' from ' + sObjectName; // BUG FIX SH-5298
            Set <Id> idSet = new Set<Id>();
            for (QueryItem item : items) {
                idSet.addAll(item.sourceIds);
            }
            List <String> whereClause = new List<String>();
            for (String id : idSet) {
                whereClause.add('\'' + id + '\'');
            }
            query += ' where Id in (' + String.join(whereClause, ',') + ')';
            Map <Id, SObject> dataMap = new Map<Id, SObject>(Database.query(query));
            for (QueryItem item : items) {
                TNCatalogDescriptor tnCatalogDescriptor = item.tnCatalogDescriptor;
                VT_D2_TNCatalogUtils.catalogueType = tnCatalogDescriptor.recordTypeName;
                for (Id id : item.sourceIds) {
                    SObject sobj = dataMap.get(id);
                    if (sobj == null) {


                        continue;
                    }
                    VTD1_NotificationC__c notification = new VTD1_NotificationC__c(OwnerId = UserInfo.getUserId());
                    List <String> subjectParameters = new List<String>();
                    for (String tnFieldName : tnCatalogDescriptor.params.keySet()) {
                        Object value = null;
                        Object params = tnCatalogDescriptor.params.get(tnFieldName);
                        if (params != null && tnFieldName.indexOf('VTD2_T_Subject_Parameter_Field') >= 0) {
                            value = VT_D2_TNCatalogUtils.evalExpression((List <VT_D2_TNCatalogUtils.ExpressionComponent>)params, sobj, false, false);
                            if (value != null) {
                                subjectParameters.add(subjectParams.get(tnFieldName) + value);
                            }
                        } else if (params instanceof List <VT_D2_TNCatalogUtils.ExpressionComponent>) {
                            value = VT_D2_TNCatalogUtils.evalExpression((List <VT_D2_TNCatalogUtils.ExpressionComponent>)params, sobj, false, false);
                        } else {
                            value = tnCatalogDescriptor.params.get(tnFieldName);
                        }
                        if (value != null) {
                            String taskFieldName = tnFieldNamesMapping.get(tnFieldName);
                            if (taskFieldName != null) {
                                try{
                                    notification.put(taskFieldName, value);
                                }
                                catch (SObjectException e) {
                                    if (e.getMessage() == 'Illegal assignment from String to Decimal') {
                                        notification.put(taskFieldName, decimal.valueOf((String)value));
                                    }
                                }
                            }
                        }
                    }
                    if (messageParams != null && messageParams.get(id) != null && messageParams.get(id).get(tnCatalogDescriptor.tnCode) != null) {
                        notification.VTD1_Parameters__c = messageParams.get(id).get(tnCatalogDescriptor.tnCode);
                    }
                    String key = tnCatalogDescriptor.tnCode + sobj.Id;
                    if(linkToRelatedEventMap != null && linkToRelatedEventMap.containsKey(key)) {
                        notification.Link_to_related_event_or_object__c  = linkToRelatedEventMap.get(key);
                    }
                    if(receiverMap != null && receiverMap.containsKey(key)) {
                        List <Id> receiverIds = receiverMap.get(key);
                        for (Id receiverId : receiverIds) {
                            if (tnCatalogDescriptor.recordTypeName != 'CustomNotification') {
                                VTD1_NotificationC__c notificationClone = notification.clone();
                                notificationClone.VTD1_Receivers__c = receiverId;
                                notificationClone.OwnerId = receiverId;
                                if (tnCatalogDescriptor.passIfEmptyRecipient && String.isEmpty(notificationClone.VTD1_Receivers__c)) {
                                    continue;
                                }
                                if (tnCatalogDescriptor.recordTypeName == 'Notification') {
                                    notifications.add(notificationClone);
                                } else if (tnCatalogDescriptor.recordTypeName == 'PostToChatter'){
                                    postToChatterNotifications.add(notificationClone);
                                }
                            } else {
                                notification.VTD1_Receivers__c = notification.VTD1_Receivers__c != null ? notification.VTD1_Receivers__c + ';' + receiverId : receiverId;
                            }
                        }
                        if (tnCatalogDescriptor.recordTypeName == 'CustomNotification') {
                            customNotifications.add(notification);
                        }
                    } else {
                        if (tnCatalogDescriptor.passIfEmptyRecipient && String.isEmpty(notification.VTD1_Receivers__c)) {
                            continue;
                        }
                        if (tnCatalogDescriptor.recordTypeName == 'Notification') {
                            notifications.add(notification);
                        } else if (tnCatalogDescriptor.recordTypeName == 'PostToChatter'){
                            postToChatterNotifications.add(notification);
                        } else if (tnCatalogDescriptor.recordTypeName == 'CustomNotification') {
                            customNotifications.add(notification);

                        }

                        }
                    }
                }
            }


        if (customNotifications != null && !customNotifications.isEmpty()) {
            sendCustomNotifications(customNotifications);
        }
        if (System.isBatch() || System.isFuture()) {
            VT_D1_TranslateHelper.translate(postToChatterNotifications);
            if (!postToChatterNotifications.isEmpty()) {
                List<VT_D2_PostToChatter.ChatterPostTemplate> chatterTemplates = new List<VT_D2_PostToChatter.ChatterPostTemplate>();
                for (VTD1_NotificationC__c notification : postToChatterNotifications) {
                    VT_D2_PostToChatter.ChatterPostTemplate template = new VT_D2_PostToChatter.ChatterPostTemplate();
                    template.targetId = notification.VTD1_Receivers__c;
                    template.message = notification.Message__c;
                    chatterTemplates.add(template);
                }
                VT_D2_PostToChatter.postToChatter(chatterTemplates);
            }
        }
        if (futureMode != null) {
            addNotificationFuture(JSON.serialize(notifications));
        } else if (autoInsert) {
            //insert notifications;
            insertNotifications(notifications);
        }
        if (!System.isBatch() && !System.isFuture()) {
            postToChatter(JSON.serialize(postToChatterNotifications));
        }
        return notifications;
    }

    @future
    private static void addNotificationFuture(String notificationJSON) {
        List <VTD1_NotificationC__c> notifications = (List <VTD1_NotificationC__c>)JSON.deserialize(notificationJSON, List <VTD1_NotificationC__c>.class);
        insert notifications;
    }


    private static void insertNotifications(List <VTD1_NotificationC__c> notifications) {
        if (notifications.size() < CHUNK_SIZE) {

            insert notifications;

        } else if(Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()){

            System.enqueueJob(new NotificationsCreator(notifications, 0));

        } else if(!System.isBatch() && !System.isFuture()){
            addNotificationFuture(JSON.serialize(notifications));
        }
    }


    @future(callout=true)
    private static void postToChatter(String notificationJSON) {
        List <VTD1_NotificationC__c> postToChatterNotifications = (List <VTD1_NotificationC__c>)JSON.deserialize(notificationJSON, List <VTD1_NotificationC__c>.class);

        // SH-12601 Update notifications and task creation to avoid notifying inactive users
        removeInactiveReceivers(postToChatterNotifications);

        VT_D1_TranslateHelper.translate(postToChatterNotifications);
        if (!postToChatterNotifications.isEmpty()) {
            List<VT_D2_PostToChatter.ChatterPostTemplate> chatterTemplates = new List<VT_D2_PostToChatter.ChatterPostTemplate>();
            for (VTD1_NotificationC__c notification : postToChatterNotifications) {
                VT_D2_PostToChatter.ChatterPostTemplate template = new VT_D2_PostToChatter.ChatterPostTemplate();
                template.targetId = notification.VTD1_Receivers__c;
                template.message = notification.Message__c;
                chatterTemplates.add(template);
            }
            if (!Test.isRunningTest()) {
                VT_D2_PostToChatter.postToChatter(chatterTemplates);
            }
        }
    }

    private static List <SObject> loadTNCatalogs(List <String> codes) {
        String query = 'select RecordType.Name,' + String.join(tnCatalogFieldNames, ',') +
                + ',VTD2_T_SObject_Name__c, VTR2_Pass_if_empty_recipient__c from VTD2_TN_Catalog_Code__c where VTD2_T_Task_Unique_Code__c in (' + String.join(codes, ',') + ')';
        return Database.query(query);
    }

    public static void sendCustomNotifications (List<VTD1_NotificationC__c> customNotifications) {
        if (System.isBatch() || System.isFuture()) {
            if (Trigger.isExecuting) {
                VT_R5_CustomNotifPlatformEventSender sender = new VT_R5_CustomNotifPlatformEventSender(JSON.serialize(customNotifications));
                sender.publish();
            } else {
            VT_D1_TranslateHelper.translate(customNotifications);
            List<VT_R3_CustomNotification.NotificationWrapper> customNotifList = new List<VT_R3_CustomNotification.NotificationWrapper>();
            for (VTD1_NotificationC__c notification : customNotifications) {
                if (notification.VTD1_Receivers__c != null) {
                    customNotifList.add(new VT_R3_CustomNotification.NotificationWrapper(
                            notification.VTD1_Receivers__c.split(';'),
                            notification.Title__c,
                            notification.Message__c,
                            notification.Link_to_related_event_or_object__c
                    ));
                }
            }
            if (!Test.isRunningTest()) {
                VT_R3_CustomNotification.sendCustomNotifications(customNotifList);
            }
            }
        } else {
            
            sendCustomNotificationsFuture(JSON.serialize(customNotifications));
        }
    }

    @Future(Callout=true)
    public static void sendCustomNotificationsFuture (String notificationsJSON) {
        List<VTD1_NotificationC__c> notifList = (List<VTD1_NotificationC__c>)JSON.deserialize(notificationsJSON, List<VTD1_NotificationC__c>.class);




        // SH-12601 Update notifications and task creation to avoid notifying inactive users
        removeInactiveReceivers(notifList);




        sendCustomNotifications(notifList);
    }

    /**
     * @description Removes notifications with inactive users
     * @author Maksim Fedarenka
     * @jira SH-12601 Update notifications and task creation to avoid notifying inactive users
     * @param notificationList List of notifications to be processed
     */
    private static void removeInactiveReceivers(List<VTD1_NotificationC__c> notificationList) {
        Set<Id> userIdSet = new Set<Id>();
        for (VTD1_NotificationC__c notification : notificationList) {
            if (notification.VTD1_Receivers__c != null) {
                userIdSet.addAll((List<Id>) notification.VTD1_Receivers__c.split(';'));
            }
        }

        Set<Id> inactiveUserIdSet = getInactiveUserIdSet(userIdSet);

        for (Integer i = notificationList.size() - 1; i >= 0; i--) {
            VTD1_NotificationC__c notification = notificationList.get(i);
            if (notification.VTD1_Receivers__c != null) {
                // removes inactive receivers from the VTD1_Receivers__c (semicolon separated string)
                String receiverIdString = '';
                List<String> receiverIdList = notification.VTD1_Receivers__c.split(';');
                for (Id receiverId : (List<Id>) receiverIdList) {
                    // checks if the receiver isActive
                    if (!inactiveUserIdSet.contains(receiverId)) {
                        receiverIdString += receiverId + ';';
                    }
                }
                receiverIdString = receiverIdString.removeEndIgnoreCase(';');
                notification.VTD1_Receivers__c = receiverIdString;
                if (String.isEmpty(notification.VTD1_Receivers__c)) {
                    // if VTD1_Receivers__c is Empty - removes the notification from the list
                    notificationList.remove(i);
                }
            }
        }
    }

    /**
     * @description Returns set of inactive user ids
     * @param userIdSet set of user ids to check if they're active
     * @return inactive User id set
     */
    public static Set<Id> getInactiveUserIdSet(Set<Id> userIdSet) {
        if (userIdSet.isEmpty()) {
            return new Set<Id>();
        }

        return new Map<Id, User>([
                SELECT Id
                FROM User
                WHERE Id IN :userIdSet AND IsActive = FALSE
        ]).keySet();
    }

    /*public static void test_N003() {
        Map <String, String> params = new Map<String, String>();
        generateNotifications(new List<String>{'N003'}, new List<Id>{'a1U3D000001Ko0cUAC'}, params, null, null);
    }

    public static void test_N001() {
        Map <String, String> params = new Map<String, String>();
        generateNotifications(new List<String>{'N001','N001'}, new List<Id>{'a1s0n000000N7zZAAS', 'a1s0n000000N80UAAS'}, params, null, null);
    }*/

    /*public static void test_N002() {
        Map <String, String> params = new Map<String, String>();
        generateNotifications(new List<String>{'N002'}, new List<Id>{'a1P2a000000rf4v'}, params, null, null);
    }

    public static void test_N006() {
        Map <String, String> params = new Map<String, String>();
        generateNotifications(new List<String>{'N006'}, new List<Id>{'a1n2a000000JEi6'}, params, null, null);
    }

    public static void test_N030() {
        Map <String, String> params = new Map<String, String>();
        generateNotifications(new List<String>{'N030'}, new List<Id>{'a1P2a000000jswH'}, params, null, null);
    }

    public static void test_N033() {
        Map <String, String> params = new Map<String, String>();
        generateNotifications(new List<String>{'N033'}, new List<Id>{'5002a000004bcUi'}, params, null, null);
    }

    public static void test_N036() {
        Map <String, String> params = new Map<String, String>();
        generateNotifications(new List<String>{'N036'}, new List<Id>{'a1v2a0000008MAr'}, params, null, null);
    }

    public static void test_N045() {
        Map <String, String> params = new Map<String, String>();
        generateNotifications(new List<String>{'N045'}, new List<Id>{'a1P2a000000rZKSEA2'}, params, null, null);
    }

    public static void test_N027() {
        Map <String, String> params = new Map<String, String>();
        generateNotifications(new List<String>{'N027'}, new List<Id>{'5002a0000055dQv'}, params, null, null);
    }

    public static void test_N060() {
        Map <String, String> params = new Map<String, String>();
        generateNotifications(new List<String>{'N060'}, new List<Id>{'a1P2a000000rfUL'}, params, null, null);
    }

    public static void test_P074() {
        Map <String, String> params = new Map<String, String>();
        generateNotifications(new List<String>{'P074'}, new List<Id>{'a1g2a000000gg0i'}, params, null, null);
    }*/
}