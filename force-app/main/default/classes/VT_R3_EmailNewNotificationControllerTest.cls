@IsTest
public with sharing class VT_R3_EmailNewNotificationControllerTest {
	static User patientUser;
	public static void Test() {
		String username = VT_D1_TestUtils.generateUniqueUserName();
		Test.startTest();
		{
			HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
			VT_D1_TestUtils.createPatientCandidate('Ter', 'kin', username, username, 'terkin',
					'Converted', study.Id, 'PN', '11-11-11', 'US', 'NY');
		}
		Test.stopTest();
		patientUser = [SELECT Id FROM User WHERE Username = :username];

		VT_R3_EmailNewNotificationController emailController = new VT_R3_EmailNewNotificationController();
		emailController.firstName = 'Ter';
		emailController.lastName = 'kin';
		emailController.profileName = 'patient';
		emailController.userLanguage = 'en_US';
		System.assertNotEquals(null, emailController.getSalutation());
		System.assertNotEquals(null, emailController.getBodyText());
		System.assertNotEquals(null, emailController.getLinkLabel());
		System.assertNotEquals(null, emailController.sincerely);
		System.assertNotEquals(null, emailController.closingMail);
		System.assertEquals(null, emailController.getLinkToStudyHub());
	}
}