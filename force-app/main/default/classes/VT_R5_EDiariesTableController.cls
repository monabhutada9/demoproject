/** Modified Sonam: SH-20622 Add the Total score filter field */
/** Modified Conquerors, Priyanka: SH-21178 Added Response Based Filters on My Patients for eCOA diaries */
public with sharing class VT_R5_EDiariesTableController {
    @AuraEnabled
    public List<VTR5_eDiary_Filter__c> lstediaryFilters;//Added by :Conquerors, Priyanka Ambre SH-21178
    @AuraEnabled
    public List<ColumnWrapper> columns;
    @AuraEnabled
    public List<ColumnWrapper> statuses;
    @AuraEnabled
    public List<ColumnWrapper> virtualsite;
    @AuraEnabled
    public List<ColumnWrapper> ediaryName;
    @AuraEnabled
    public List<EDiaryWrapper> items;
    @AuraEnabled
    public Map<String, Object> params;
    @AuraEnabled
    public Boolean isGuidFilter;
    @AuraEnabled
    public Map<String, String> eDiaryStatusMap = new Map<String, String>{
            'missed' => VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_MISSED,
            'dueSoon' => VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_DUE_SOON,
            'completed' => VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_COMPLETED,
            'readyToStart' => VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_READY_TO_START,
            'inProgress' => VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_IN_PROGRESS,
            'reviewRequired' => VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED,
            'reviewed' => VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED,
            'eDiaryMissed' => VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_EDIARY_MISSED
    };

    private static final String VTR5_EXTERNAL = 'VTR5_External';
    private static final String EPRO = 'ePRO';
    private static final String NOT_STARTED = 'Not Started';
    private static List<String> virtualsiteFilterList = new List<String>();
    private static List<String> eDiaryNameFiltersList = new List<String>();
    //Added by :Conquerors, Priyanka Ambre SH-21178
    //START
    private static final String responseEidaryName  = 'VTR5_eDiary_Name__c';
    private static final String responseExternalWidgetKey  = 'VTR5_External_Widget_Key__c';
    private static final String responseAnswerContent  = 'VTR5_Answer_Content__c';
    private static final String ecoa = 'eCOA';
    //END

    private static String customOrder;
    private static String customSort;
    private static final Map<String, String> soqlOrderMap = new Map<String, String>{
            'patientName' => 'VTD1_Patient_User_Id__r.Name',
            'patientId' => 'VTD1_CSM__r.VTD1_Subject_ID__c',
            'eDiaryName' => 'Name',
            'dueDate' => 'VTD1_Due_Date__c',
            'virtualSites' => 'VTD1_CSM__r.VTD1_Virtual_Site__r.Name',
            'eDiaryStatus' => 'VTD1_Status__c',
            'dateCompleted' => 'VTD1_Completion_Time__c',
            'totalScore' => 'VTR5_Total_Score_Display__c'  // added the total score field for soql SH-20622 Sonam
    };
    private final static List<String> eDiaryFields = new List<String>{
            'Id',
            'Name',
            'RecordType.DeveloperName',
            'VTR5_Reviewed__c',
            'VTD1_Due_Date__c',
            'VTR5_MissedReason__c',
            'VTR5_CompletedbyCaregiver__c',
            'VTR2_Reviewer_User__c',
            'VTD1_Status__c',
            'VTD1_Completion_Time__c',
            'VTR2_Total_Score__c',
            'VTD1_Protocol_ePRO__c',
            'VTR4_Percentage_Completed__c',
            'VTD1_CSM__c',
            'VTD1_CSM__r.ContactId',
            'VTD1_CSM__r.Contact.Name',
            'VTD1_CSM__r.VTD1_Study__c',
            'VTD1_CSM__r.VTD1_Subject_ID__c',
            'VTD1_CSM__r.VTD1_Primary_PG__c',
            'VTD1_CSM__r.VTR2_SiteCoordinator__c',
            'VTD1_CSM__r.VTD1_Virtual_Site__c',
            'VTD1_CSM__r.VTD1_Virtual_Site__r.Name',
            'VTR5_Total_Score_Display__c' // added new field for total score Sonam SH-20622
    };

    @AuraEnabled
    public static String updateEdiary(VTD1_Survey__c eDiary) {

        try {
            if (eDiary.VTR5_Reviewed__c && eDiary.RecordType.DeveloperName == 'ePRO') {
                eDiary.VTD1_Status__c = eDiary.VTR4_Percentage_Completed__c == 100
                        ? VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED
                        : VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED_PARTIALLY_COMPLETED;
            }
            update eDiary;
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return String.valueOf(true);
    }

    
    /*******************************************************************************************************
    * @author                   Priyanka Ambre (SH-21178)
    * @description              Method return all the eDiary Filter records for the specified Study.
    * @param recs      			Map of String, Object which is sent from component.
    */
    @AuraEnabled(cacheable=true)
    public static List<VTR5_eDiary_Filter__c> getEdiaryFilters(string studyId){
        if (studyId != Null) {
                List<VTR5_eDiary_Filter__c> lstEdiaryFilterRecords = [SELECT   Id, 
                                                                               VTR5_Study__c,
                                                                               VTR5_External_Widget_Key__c,
                                                                               VTR5_Label_Name__c,
                                                                               VTR5_Answer_Content__c,
                                                                               VTR5_eDiary_Name__c
                                                                       FROM    VTR5_eDiary_Filter__c 
                                                                       WHERE   VTR5_Study__c =: studyId 
                                                                       AND     VTR5_Study__r.VTR5_eDiaryTool__c =: ecoa
                                                                       WITH SECURITY_ENFORCED LIMIT : Limits.getLimitQueryRows()];
                return  lstEdiaryFilterRecords;
            
        }
        return null;
    }
	
    /*******************************************************************************************************
    * @author                   Priyanka Ambre (SH-21178)
    * @description              Method returns the set of ediary Ids, which are associated with VTD1_Survey_Answer__c
                                Ids which is associated to the selected response fields.
    * @param recs      			Map of String, Object of selected response.
    */
    public static Set<Id> getEdiaryIds(Map<String, Object> responseMap){
        if(responseMap.get(responseExternalWidgetKey) != null && responseMap.get(responseEidaryName) != null 
        && responseMap.get(responseAnswerContent) != null){
            Set<Id> setEdiaryIds = new Set<Id>();
            List<VTD1_Survey_Answer__c> lstAnswers = [SELECT    Id,
                                                                VTD1_Survey__c,
                                                                VTD1_Answer__c
                                                    FROM VTD1_Survey_Answer__c
                                                    WHERE VT_R5_External_Widget_Data_Key__c  =: (String.valueOf(responseMap.get(responseExternalWidgetKey)))
                                                    AND VTD1_Survey__r.Name =: String.valueOf(responseMap.get(responseEidaryName))
                                                    WITH SECURITY_ENFORCED LIMIT :Limits.getLimitQueryRows()]; 
            
            //Checked for VTD1_Answer__c as Long text Area field cannot be used in WHERE Clause.
            for(VTD1_Survey_Answer__c objAnswer : lstAnswers){
            if(responseMap.get(responseAnswerContent) != null && 
                objAnswer.VTD1_Answer__c.containsIgnoreCase(String.valueOf(responseMap.get(responseAnswerContent)))){
                    setEdiaryIds.add(objAnswer.VTD1_Survey__c);
                }
            } 
            return setEdiaryIds;
        }
        return null;
    }

    @AuraEnabled
    public static VT_R5_EDiariesTableController getEdiaryRecords(String listViewId, String jsonParams, String searchWord, Boolean firstLoad) {
        String userProfileName = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        customOrder = null;

        VT_R5_EDiariesTableController result = new VT_R5_EDiariesTableController();
        result.initColumns();
        result.initStatuses();
        result.isGuidFilter = false;
        result.params = (Map<String, Object>) JSON.deserializeUntyped(jsonParams);
       // result.lstediaryFilters =  getEdiaryFilters(result.params); //Added by :Conquerors, Priyanka Ambre SH-21178
        List<String> guidIds = getGuidIds(result, listViewId);
        Set<Id> setEdiaryIds = new Set<Id>();//Added by :Conquerors, Priyanka Ambre SH-21178
        result.initVirtualSiteAndEdiaryName(result.params);
        String eDiaryQuery = getEDiaryQuery() +
                generateQueryFilter(searchWord, result.params, guidIds, listViewId, firstLoad, setEdiaryIds) +//Added setEdiaryIds parameter by :Conquerors, Priyanka Ambre SH-21178
                ' WITH SECURITY_ENFORCED ' +  // added the security enforced checkbox SH-20622 Sonam
            	generateOrder(result.params.get('order')) +
                generateLimits(result.params.get('limit'));

        //VT_R5_PagingQueryHelper.ResultWrapper wrapperEdiaries = VT_R5_PagingQueryHelper.query(eDiaryQuery, 'VTD1_Survey__c', jsonParams);
        List<VTD1_Survey__c> eDiaryList = Database.query(eDiaryQuery);
        Map<Id, List<VTD1_Protocol_ePro_Question__c>> surveyQuestionsMap = getQuestionsForSurveys(eDiaryList);
        Map<Id, String> phoneMap = getPhoneNumbers(eDiaryList);
        Map<Id, Datetime> dateMapOfLastVisit = getDatesOfLastVisit(eDiaryList);

        result.items = new List<EDiaryWrapper>();
        Integer MAX_REASON_LEN = 40;
        for (VTD1_Survey__c eDiary : eDiaryList) {
            EDiaryWrapper eDiaryWrapper = new EDiaryWrapper();
            eDiaryWrapper.eDiary = eDiary;
            eDiaryWrapper.canUserReview = canUserReviewDiary(eDiary, userProfileName);
            eDiaryWrapper.needScoring = needScoring(surveyQuestionsMap.get(eDiary.Id));
            eDiaryWrapper.phone = phoneMap.get(eDiary.VTD1_CSM__r.ContactId);
            eDiaryWrapper.completedDateOfVisit = dateMapOfLastVisit.get(eDiary.VTD1_CSM__c) != null ? dateMapOfLastVisit.get(eDiary.VTD1_CSM__c) : null;
            eDiaryWrapper.reasonShort = String.isNotBlank(eDiary.VTR5_MissedReason__c) && eDiary.VTR5_MissedReason__c.length() > MAX_REASON_LEN
                    ? eDiary.VTR5_MissedReason__c.left(MAX_REASON_LEN) + '..'
                    : eDiary.VTR5_MissedReason__c;
            eDiaryWrapper.virtualSiteShort = String.isNotBlank(eDiary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name) && eDiary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name.length() > MAX_REASON_LEN
                    ? eDiary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name.left(MAX_REASON_LEN) + '...'
                    : eDiary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name;
            eDiaryWrapper.ediaryNameShort = String.isNotBlank(eDiary.Name) && eDiary.Name.length() > MAX_REASON_LEN
                    ? eDiary.Name.left(MAX_REASON_LEN) + '...'
                    : eDiary.Name;
            result.items.add(eDiaryWrapper);
        }
        if (customOrder == 'phoneNumber') {
            sortByPhoneNumber(result);
        } else {
            result.items.sort();
        }
        return result;
    }

    private static Map<Id, List<VTD1_Protocol_ePro_Question__c>> getQuestionsForSurveys(List<VTD1_Survey__c> surveys) {
        Set<Id> protocolIds = new Set<Id>();
        Map<Id, List<VTD1_Protocol_ePro_Question__c>> result = new Map<Id, List<VTD1_Protocol_ePro_Question__c>>();
        for (VTD1_Survey__c survey : surveys) {
            if (survey.VTD1_Protocol_ePRO__c != null) {
                protocolIds.add(survey.VTD1_Protocol_ePRO__c);
            }
        }
        if (protocolIds.isEmpty()) {
            return result;
        }
        Map<Id, VTD1_Protocol_ePRO__c> protocols = new Map<Id, VTD1_Protocol_ePRO__c>([
                SELECT Id, (
                        SELECT VTR2_Scoring_Type__c, VTR2_AllowOverridingAutoscore__c
                        FROM Protocol_ePro_Questions__r
                )
                FROM VTD1_Protocol_ePRO__c
                WHERE Id IN :protocolIds
        ]);
        for (VTD1_Survey__c survey : surveys) {
            if (survey.VTD1_Protocol_ePRO__c != null && protocols.get(survey.VTD1_Protocol_ePRO__c) != null) {
                result.put(survey.Id, protocols.get(survey.VTD1_Protocol_ePRO__c).Protocol_ePro_Questions__r);
            }
        }
        return result;
    }

    private static Boolean canUserReviewDiary(VTD1_Survey__c survey, String userProfileName) {
        if (!Test.isRunningTest() && !VT_R4_ConstantsHelper_ProfilesSTM.SITE_STAFF.contains(userProfileName)) {
            return false;
        }



        Boolean reviewedAccessible = VT_Utilities.isAccessibleAndUpdatable(VTD1_Survey__c.VTR5_Reviewed__c);
        Boolean statusAccessible = VT_Utilities.isAccessibleAndUpdatable(VTD1_Survey__c.VTD1_Status__c);
        Boolean scoreAccessible = VT_Utilities.isAccessible(VTD1_Survey_Answer__c.VTD1_Score__c);


        Boolean fieldsAccessible = reviewedAccessible && statusAccessible && scoreAccessible;
        Boolean reviewRequired = (survey.VTD1_Status__c == VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED
                || survey.VTD1_Status__c == VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_COMPLETED)
                && !survey.VTR5_Reviewed__c;






        return fieldsAccessible && reviewRequired;
    }

    private static Boolean needScoring(List<VTD1_Protocol_ePro_Question__c> questions) {
        if (questions == null || questions.isEmpty()) {
            return false;
        }
        for (VTD1_Protocol_ePro_Question__c question : questions) {
            if (question.VTR2_Scoring_Type__c == 'Auto' && question.VTR2_AllowOverridingAutoscore__c || question.VTR2_Scoring_Type__c == 'Manual') {
                return true;
            }
        }
        return false;
    }

    private static void sortByPhoneNumber(VT_R5_EDiariesTableController data) {
        Map<String, List<EDiaryWrapper>> byPhoneMap = new Map<String, List<EDiaryWrapper>>();
        List<EDiaryWrapper> sortedItems = new List<EDiaryWrapper>();
        for (EDiaryWrapper ew : data.items) {
            if (!byPhoneMap.containsKey(ew.phone)) {
                byPhoneMap.put(ew.phone, new List<EDiaryWrapper>());
            }
            byPhoneMap.get(ew.phone).add(ew);
        }
        List<String> phones = new List<String>(byPhoneMap.keySet());
        phones.sort();
        if (customSort == 'asc') {
            for (String phone : phones) {
                if (phone != null) {
                    sortedItems.addAll(byPhoneMap.get(phone));
                }
            }
        } else {
            for (Integer i = phones.size() - 1; i >= 0; i--) {
                if (phones[i] != null) {

                    List<EDiaryWrapper> diariesByPhone = byPhoneMap.get(phones[i]);
                    for (Integer j = diariesByPhone.size() - 1; i >= 0; i--) {
                        sortedItems.add(diariesByPhone[j]);
                    }
                }
            }
        }
        if (byPhoneMap.containsKey(null)) {
            sortedItems.addAll(byPhoneMap.get(null));
        }
        data.items = sortedItems;

    }

    private static List<String> getGuidIds(VT_R5_EDiariesTableController data, String listViewId) {
        List<String> result = new List<String>();
        if (String.isNotBlank(listViewId)) {
            List<Attachment> attachments = [SELECT Id, Body FROM Attachment WHERE ParentId = :listViewId];
            if (attachments.isEmpty()) {
                return null;
            }
            data.isGuidFilter = true;
            String body = attachments.isEmpty() ? '' : attachments.get(0).Body.toString().remove('\r');
            Integer pos = 0;
            while (pos <= body.length()) {
                Integer nextNewLine = body.indexOf('\n', pos);
                if (nextNewLine == -1) {
                    nextNewLine = body.length();
                }
                String rowString = body.substring(pos, nextNewLine);
                if (String.isNotBlank(rowString)) {
                    result.add(rowString);
                }
                pos = nextNewLine + 1;
            }
            return result;

        }

        return null;
    }

    private static String getEDiaryQuery() {
        return 'SELECT ' + String.join(eDiaryFields, ',') + ' FROM VTD1_Survey__c ';
    }

    private static String generateQueryFilter(String searchWord, Map <String, Object> params, List<String> guidIds, String listViewId, Boolean firstLoad, Set<Id> setEdiaryIds) {
        String filter = 'WHERE (RecordType.DeveloperName = :VTR5_EXTERNAL OR RecordType.DeveloperName = :EPRO) AND VTD1_Status__c <> :NOT_STARTED ';
        if (guidIds != null) {
            filter += 'AND VTR5_Response_GUID__c IN :guidIds ';
        }
        if (String.isNotBlank(listViewId)) {
            generatePreFilter(listViewId, params, firstLoad);
        }
        if (String.isNotBlank(searchWord)) {
            Set<String> searchFields = new Set<String>{
                    'VTD1_Patient_User_Id__r.Name', 'Name', 'VTD1_CSM__r.VTD1_Subject_ID__c'
            };
            searchWord = searchWord.remove('\'');
            filter += 'AND (';
            for (String searchField : searchFields) {
                filter += searchField + ' LIKE \'' + searchWord + '%\' OR ';
            }
            filter = filter.substringBeforeLast(' OR ') + ')';
        }

        if (params.containsKey('filterMap')) {
            Map<String, Object> filterMap = (Map<String, Object>) params.get('filterMap');
            if (filterMap.containsKey('dueDate') && filterMap.get('dueDate') != null) {
                Date dueDate = Date.valueOf((String) filterMap.get('dueDate'));
                Datetime dueDateTime = Datetime.newInstanceGmt(dueDate, Time.newInstance(0, 0, 0, 0));
                filter += ' AND (VTD1_Due_Date__c >= ' + dueDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ') + ' and VTD1_Due_Date__c < ' + dueDateTime.addHours(24).format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ') + ')';
            }

            if (filterMap.containsKey('studyId') && filterMap.get('studyId') != null) {
                filter += ' AND VTD1_CSM__r.VTD1_Study__c = \'' + ((String) filterMap.get('studyId')) + '\'';
                //Added by :Conquerors, Priyanka Ambre SH-21178	
                //START	
                if(params != null && params.containsKey('selectedResponse') &&  params.get('selectedResponse') != null){	
                    Map<String, Object> selectedResMap = (Map<String, Object>)params.get('selectedResponse');	
                    setEdiaryIds.addAll(getEdiaryIds(selectedResMap)); 	
                    filter += 'AND  Id IN : setEdiaryIds';	
                }	
                //END
            }

            if (filterMap.containsKey('markReviewedFilter')) {
                String markReviewedFilter = (String) filterMap.get('markReviewedFilter');
                filter += String.isNotBlank(markReviewedFilter) ? ' AND (VTR5_Reviewed__c = ' + markReviewedFilter + ')' : '';
            }

            if (filterMap.containsKey('statusFilters')) {
                List<Object> statusFilters = (List<Object>) filterMap.get('statusFilters');
                if (statusFilters != null && !statusFilters.isEmpty()) {
                    String statusFiltersStr = '(\'' + String.join(statusFilters, '\',\'') + '\')'; // ('str1','str2','str3')
                    filter += ' AND (VTD1_Status__c IN ' + statusFiltersStr + ')';
                }
            }

            if (filterMap.containsKey('virtualSitesFilters')) {
                for (Object virtualsite : (List<Object>) filterMap.get('virtualSitesFilters')) {
                    virtualsiteFilterList.add(String.valueof(virtualsite));
                }
                if (!virtualsiteFilterList.isEmpty()) {
                    String virtualsiteFilters = 'virtualsiteFilterList';
                    filter += ' AND (VTD1_CSM__r.VTD1_Virtual_Site__r.Name IN:' + virtualsiteFilters + ')';
                }
            }

            if (filterMap.containsKey('eDiaryNameFilters')) {
                for (Object ediary : (List<Object>) filterMap.get('eDiaryNameFilters')) {
                    eDiaryNameFiltersList.add(String.valueof(ediary));
                }
                if (!eDiaryNameFiltersList.isEmpty()) {
                    String eDiaryNameFilters = 'eDiaryNameFiltersList';
                    filter += ' AND (Name IN :' + eDiaryNameFilters + ')';
                }
            }
            
            // Start SH-20622: Sonam added the filter for total score field
            if (filterMap.containsKey('totalScoreValue') && filterMap.get('totalScoreValue') != null) {
                String totalScoreFilter = '\'' + (String) filterMap.get('totalScoreValue') + '%\'';
                    filter += ' AND (VTR5_Total_Score_Display__c Like ' + totalScoreFilter + ')';
            }
            //End SH-20622
        }
        return filter;
    }

    private static void generatePreFilter(String listViewId, Map <String, Object> params, Boolean firstLoad) {
        List<VTR5_DiaryListView__c> dlvList = [Select Id, VTR5_Filter__c from VTR5_DiaryListView__c where Id = :listViewId];
        if (!dlvList.isEmpty() && String.isNotBlank(dlvList.get(0).VTR5_Filter__c)) {
            String preFilter = dlvList.get(0).VTR5_Filter__c;
            Map<String, Object> preFilterMap = new Map<String, Object>();
            try {
                preFilterMap = (Map<String, Object>) JSON.deserializeUntyped(preFilter);
            } catch (Exception ex) {
                System.debug('JSON.deserialize error, ' + ex.getStackTraceString());
            }
            if (preFilterMap.isEmpty()) {
                return;
            }
            Map<String, Object> filterMap = params.containsKey('filterMap') ? (Map<String, Object>) params.get('filterMap') : new Map<String, Object>();
            if (preFilterMap.get('name') != null) {
                try {
                    List<Object> eDiaryNameFiltersObjList = (List<Object>) filterMap.get('eDiaryNameFilters');
                    List<String> eDiaryNameFiltersStrList = new List<String>();
                    for (Object filterObj : eDiaryNameFiltersObjList) {
                        eDiaryNameFiltersStrList.add(String.valueOf(filterObj));
                    }
                    Set<String> eDiaryNameFiltersStrSet = new Set<String>(eDiaryNameFiltersStrList);
                    if (eDiaryNameFiltersStrSet == null) {
                        eDiaryNameFiltersStrSet = new Set<String>();
                    }
                    eDiaryNameFiltersStrSet.add(String.valueOf(preFilterMap.get('name')));
                    eDiaryNameFiltersStrList = new List<String>();
                    eDiaryNameFiltersStrList.addAll(eDiaryNameFiltersStrSet);
                    filterMap.put('eDiaryNameFilters', eDiaryNameFiltersStrList);
                } catch (Exception e) {
                    System.debug('Name filter error: ' + e.getMessage() + '\n>> stack trace:\n' + e.getStackTraceString());
                }
            }
            if (firstLoad && preFilterMap.get('studyId') != null) {
                filterMap.put('studyId', preFilterMap.get('studyId'));
            }
            if (firstLoad && preFilterMap.get('status') != null && String.isNotBlank((String) preFilterMap.get('status'))) {
                List<Object> filterStatus = new List<Object>{
                        preFilterMap.get('status')
                };
                filterMap.put('statusFilters', filterStatus);
            }
            params.put('filterMap', filterMap);
        }
    }

    private static String generateOrder(Object order) {
        String orderString = ' ORDER BY VTD1_Due_Date__c DESC NULLS LAST, VTD1_Patient_User_Id__r.Name ASC NULLS LAST, VTD1_Status__c ASC NULLS LAST, VTR5_Total_Score_Display__c NULLS LAST'; // Sonam SH-20622 Modified the query and add total score field
        Map<String, Object> orderMap = new Map<String, Object>();
        if (order != null) {
            orderMap = (Map<String, Object>) order;
        }
        if (orderMap != null && orderMap.containsKey('columnName') && orderMap.containsKey('sorting')) {
            String columnName = (String) orderMap.get('columnName');
            String sorting = (String) orderMap.get('sorting');
            if (soqlOrderMap.containsKey(columnName)) {
                orderString = ' ORDER BY ' + soqlOrderMap.get(columnName) + ' ' + sorting + ' NULLS LAST';
                if (columnName != 'patientName') {
                    orderString += ', ' + soqlOrderMap.get('patientName') + ' ASC';
                }
                if (columnName != 'dueDate') {
                    orderString += ', ' + soqlOrderMap.get('dueDate') + ' DESC';
                }
                
                // Start SH-20622: Sonam Added the sorting for total score field
                if (columnName != 'totalScore') {
                   orderString += ', ' + soqlOrderMap.get('totalScore') + ' ASC';
                }
                // End SH-20622
            }
            customOrder = columnName;
            customSort = sorting;
        }
        return orderString;
    }

    private static String generateLimits(Object limitParam) {
        String limitStr = (String) limitParam;
        return String.isNotBlank(limitStr) && limitStr.isNumeric()
                ? ' LIMIT ' + limitStr
                : ' LIMIT ' + Limits.getLimitQueryRows();
    }

    private void initColumns() {
        this.columns = new List<ColumnWrapper>();
        this.columns.add(new ColumnWrapper(System.Label.VTD1_Name, 'patientName'));
        this.columns.add(new ColumnWrapper(System.Label.VTD2_PatientID, 'patientId'));
        this.columns.add(new ColumnWrapper(System.Label.VTR2_PhoneNumber, 'phoneNumber'));
        this.columns.add(new ColumnWrapper(System.Label.VTR5_EdiaryLastName, 'eDiaryName'));
        this.columns.add(new ColumnWrapper(System.Label.VT_D1_ePRO_DueDate, 'dueDate'));
        this.columns.add(new ColumnWrapper(System.Label.VT_R2_Status, 'eDiaryStatus'));
        this.columns.add(new ColumnWrapper(System.Label.VTR5_EdiaryDateCompleted, 'dateCompleted'));
        this.columns.add(new ColumnWrapper(System.Label.VTR5_TotalScore, 'totalScore')); // SH-20622: Harshita
        this.columns.add(new ColumnWrapper(System.Label.VTR5_EdiaryMarkReviewed, 'markReviewed'));
        this.columns.add(new ColumnWrapper(System.Label.VTR5_EdiaryMissedReason, 'missedReason'));
        this.columns.add(new ColumnWrapper(System.Label.VTR2_Site, 'virtualSites')); // SH-20122 Sonam: Changed the place
        this.columns.add(new ColumnWrapper(System.Label.VTR5_EdiaryDateOfLastVisit, 'dateOfLastVisit')); // SH-20122 Sonam: Changed the place
        
    }

    private void initStatuses() {
        this.statuses = new List<ColumnWrapper>();
        Set<String> allowedStatuses = new Set<String>(eDiaryStatusMap.values());
        for (Schema.PicklistEntry pickListVal : VTD1_Survey__c.VTD1_Status__c.getDescribe().getPicklistValues()) {
            if (allowedStatuses.contains(pickListVal.getValue())) {
                this.statuses.add(new ColumnWrapper(pickListVal.getLabel(), pickListVal.getValue()));
            }
        }
    }

    /*
     * @Parameter -Map <String, Object> params
     * @Description - Getting All ediary name and virtual site name in ColumnWrapper to show
     * in edairyName and site filter respectively on UI
     */
    private void initVirtualSiteAndEdiaryName(Map <String, Object> params) {
        String filter = 'WHERE VTD1_Status__c <> :NOT_STARTED ';
        List<Object> virtualSiteFiltersList = new List<Object>();
        if (params.containsKey('filterMap')) {
            Map<String, Object> filterMap = (Map<String, Object>) params.get('filterMap');
            if (filterMap.containsKey('studyId') && filterMap.get('studyId') != null) {
                filter += ' AND VTD1_CSM__r.VTD1_Study__c = \'' + ((String) filterMap.get('studyId')) + '\'';
            }
            if (filterMap.containsKey('virtualSitesFilters')) {
                virtualSiteFiltersList = (List<Object>) filterMap.get('virtualSitesFilters');
            }
        }
        String eDiaryQuery = getEDiaryQuery() + filter;
        this.virtualsite = new List<ColumnWrapper>();
        Set<String> allowedVirtualSite = new Set<String>();
        this.ediaryName = new List<ColumnWrapper>();
        Set<String> ediaryNameSet = new Set<String>();
        String strVirtualSiteName;
        String streDiaryName;
        Integer MAX_NAME_LEN = 40;
        for (VTD1_Survey__c eDiary : Database.query(eDiaryQuery)) {
            if (String.isNotBlank(ediary.VTD1_CSM__r.VTD1_Virtual_Site__c) &&
                    !allowedVirtualSite.contains(ediary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name)) {
                allowedVirtualSite.add(ediary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name);
                if (ediary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name.length() > MAX_NAME_LEN) {
                    strVirtualSiteName = ediary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name.left(MAX_NAME_LEN) + '...';
                    this.virtualsite.add(new ColumnWrapper(strVirtualSiteName,
                            ediary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name));
                } else {
                    this.virtualsite.add(new ColumnWrapper(ediary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name,
                            ediary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name));
                }
            }
            if (!ediaryNameSet.contains(ediary.Name)) {
                if (virtualSiteFiltersList.isEmpty()) {
                    ediaryNameSet.add(ediary.Name);
                    if (ediary.Name.length() > MAX_NAME_LEN) {
                        streDiaryName = ediary.Name.left(MAX_NAME_LEN) + '...';
                        this.ediaryName.add(new ColumnWrapper(streDiaryName, ediary.Name));
                    } else {
                        this.ediaryName.add(new ColumnWrapper(ediary.Name, ediary.Name));
                    }
                } else if (!virtualSiteFiltersList.isEmpty() && virtualSiteFiltersList.contains(eDiary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name)) {
                    ediaryNameSet.add(ediary.Name);
                    if (ediary.Name.length() > MAX_NAME_LEN) {
                        streDiaryName = ediary.Name.left(MAX_NAME_LEN) + '...';
                        this.ediaryName.add(new ColumnWrapper(streDiaryName, ediary.Name));
                    } else {
                        this.ediaryName.add(new ColumnWrapper(ediary.Name, ediary.Name));
                    }
                }
            }
        }
        this.virtualsite.sort();
        this.ediaryName.sort();
    }


    public class ColumnWrapper implements Comparable {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String columnName;

        public ColumnWrapper(String label, String columnName) {
            this.label = label;
            this.columnName = columnName;
        }
        public Integer compareTo(Object compareTo) {
            ColumnWrapper columnToCompare = (ColumnWrapper) compareTo;
            String string1 = columnToCompare.columnName.toUpperCase();
            String string2 = columnName.toUpperCase();
            return string2.CompareTo(string1);
        }
    }

    public class EDiaryWrapper implements Comparable {
        @AuraEnabled
        public VTD1_Survey__c eDiary;
        @AuraEnabled
        public Datetime completedDateOfVisit;
        @AuraEnabled
        public String phone;
        @AuraEnabled
        public String reasonShort;
        @AuraEnabled
        public Boolean canUserReview = false;
        @AuraEnabled
        public Boolean needScoring = false;
        @AuraEnabled
        public String virtualSiteShort;
        @AuraEnabled
        public String ediaryNameShort;

        public Integer compareTo(Object compareTo) {
            EDiaryWrapper eDiaryToCompare = (EDiaryWrapper) compareTo;
            if (customOrder == 'dateOfLastVisit') {
                if (customSort == 'asc') {
                    return completedDateOfVisit < eDiaryToCompare.completedDateOfVisit ? 1 : 0;
                }
                if (customSort == 'desc') {
                    return completedDateOfVisit > eDiaryToCompare.completedDateOfVisit ? 1 : 0;
                }
            }
            return 0;
        }
    }

    public without sharing class GodModeSubClass { //separate subclass to ignore sharing for getting visits that are not shared to users due to regular sharing issues/ SH-15366
        public List<AggregateResult> queryArWithoutSharing(Set<Id> caseIds, Boolean isUncsh) {
            return isUncsh ? [Select max(VTD1_Visit_Completion_Date__c)d, Unscheduled_Visits__c from VTD1_Actual_Visit__c where VTD1_Visit_Completion_Date__c != null and Unscheduled_Visits__c in:caseIds group by Unscheduled_Visits__c]
                    : [Select max(VTD1_Visit_Completion_Date__c)d, VTD1_Case__c from VTD1_Actual_Visit__c where VTD1_Visit_Completion_Date__c != null and VTD1_Case__c in:caseIds group by VTD1_Case__c];
        }
    }

    private static Map<Id, String> getPhoneNumbers(List<VTD1_Survey__c> eDiaryList) {
        Map<Id, String> result = new Map<Id, String>();
        for (VTD1_Survey__c eDiary : eDiaryList) {
            result.put(eDiary.VTD1_CSM__r.ContactId, null);
        }

        for (VT_D1_Phone__c phone : [SELECT PhoneNumber__c, VTD1_Contact__c FROM VT_D1_Phone__c WHERE VTD1_Contact__c = :result.keySet() AND IsPrimaryForPhone__c = TRUE]) {
            result.put(phone.VTD1_Contact__c, phone.PhoneNumber__c);
        }
        return result;
    }

    private static Map<Id, DateTime> getDatesOfLastVisit(List<VTD1_Survey__c> eDiaryList) {
        Map<Id, DateTime> result = new Map<Id, DateTime>();
        for (VTD1_Survey__c eDiary : eDiaryList) {
            result.put(eDiary.VTD1_CSM__c, null);
        }

        List<AggregateResult> maxProtocolDates = new GodModeSubClass().queryArWithoutSharing(result.keySet(), false);
        for (AggregateResult ar : maxProtocolDates) {
            Id caseId = (Id) ar.get('VTD1_Case__c');
            Datetime complDate = (Datetime) ar.get('d');
            result.put(caseId, complDate);
        }
        List<AggregateResult> maxUnschDates = new GodModeSubClass().queryArWithoutSharing(result.keySet(), true);
        for (AggregateResult ar : maxUnschDates) {
            Id caseId = (Id) ar.get('Unscheduled_Visits__c');
            Datetime complDate = (Datetime) ar.get('d');
            if (result.get(caseId) != null && result.get(caseId) < complDate) {
                result.put(caseId, complDate);
            }
        }
        return result;
    }
}