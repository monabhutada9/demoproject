public without sharing class VT_D1_SurveyAnswerCreator {

    public static void createAnswers(List<VTD1_Survey__c> surveys) {
        Map<Id, List<VTD1_Protocol_ePro_Question__c>> questionMap = getQuestions(surveys);

        List<VTD1_Survey_Answer__c> answers = new List<VTD1_Survey_Answer__c>();
        for (VTD1_Survey__c item : surveys) {
            if (questionMap.containsKey(item.VTD1_Protocol_ePRO__c)) {
                for (VTD1_Protocol_ePro_Question__c question : questionMap.get(item.VTD1_Protocol_ePRO__c)) {
                    answers.add(new VTD1_Survey_Answer__c(
                            VTD1_Survey__c = item.Id,
                            VTD1_Question__c = question.Id
                    ));
                }
            }
        }
        if (!answers.isEmpty()) {
            insert answers;
        }
    }

    public static void createAnswers2(List<VTD1_Survey__c> newSurveys, Map<Id, VTD1_Survey__c> oldSurveys) {
        Map<Id, List<VTD1_Protocol_ePro_Question__c>> questionMap = getQuestions(newSurveys);
        List<VTD1_Protocol_ePro_Question__c> questions = new List<VTD1_Protocol_ePro_Question__c>();
        for (Id key : questionMap.keySet()) {
            questions.addAll(questionMap.get(key));
        }
        Map<Id, List<VTD1_Survey_Answer__c>> oldAnswersBySurveyId = new Map<Id, List<VTD1_Survey_Answer__c>>();
        for (VTD1_Survey_Answer__c answer : [SELECT Id, VTD1_Survey__c, VTD1_Question__c FROM VTD1_Survey_Answer__c WHERE VTD1_Survey__c IN :newSurveys AND VTD1_Question__c IN :questions]) {
            if (oldAnswersBySurveyId.keySet().contains(answer.VTD1_Survey__c)) {
                oldAnswersBySurveyId.get(answer.VTD1_Survey__c).add(answer);
            } else {
                oldAnswersBySurveyId.put(answer.VTD1_Survey__c, new List<VTD1_Survey_Answer__c>{
                        answer
                });
            }
        }
        List<VTD1_Survey_Answer__c> answers = new List<VTD1_Survey_Answer__c>();
        Boolean isNeedNewAnswer;
        for (VTD1_Survey__c item : newSurveys) {
            if (item.VTD1_Status__c == 'In Progress' && oldSurveys.get(item.Id).VTD1_Status__c != 'In Progress') {
                if (questionMap.containsKey(item.VTD1_Protocol_ePRO__c)) {
                    for (VTD1_Protocol_ePro_Question__c question : questionMap.get(item.VTD1_Protocol_ePRO__c)) {
                        isNeedNewAnswer = true;
                        if (oldAnswersBySurveyId.containsKey(item.Id)) {
                            for (VTD1_Survey_Answer__c answer : oldAnswersBySurveyId.get(item.Id)) {
                                if (answer.VTD1_Question__c == question.Id) {
                                    isNeedNewAnswer = false;
                                    break;
                                }
                            }
                        }
                        if (isNeedNewAnswer) {
                            answers.add(new VTD1_Survey_Answer__c(
                                    VTD1_Survey__c = item.Id,
                                    VTD1_Question__c = question.Id
                            ));
                        }
                    }
                }
            }
        }
        if (!answers.isEmpty()) {
            insert answers;
        }
    }


    private static Map<Id, List<VTD1_Protocol_ePro_Question__c>> getQuestions(List<VTD1_Survey__c> surveys) {
        List<Id> templateIds = new List<Id>();
        for (VTD1_Survey__c item : surveys) {
            templateIds.add(item.VTD1_Protocol_ePRO__c);
        }

        Map<Id, List<VTD1_Protocol_ePro_Question__c>> questionMap = new Map<Id, List<VTD1_Protocol_ePro_Question__c>>();
        for (VTD1_Protocol_ePro_Question__c item : [
                SELECT Id, VTD1_Protocol_ePRO__c
                FROM VTD1_Protocol_ePro_Question__c
                WHERE VTD1_Protocol_ePRO__c IN :templateIds
        ]) {
            if (!questionMap.containsKey(item.VTD1_Protocol_ePRO__c)) {
                questionMap.put(item.VTD1_Protocol_ePRO__c, new List<VTD1_Protocol_ePro_Question__c>());
            }
            questionMap.get(item.VTD1_Protocol_ePRO__c).add(item);
        }

        return questionMap;
    }
}