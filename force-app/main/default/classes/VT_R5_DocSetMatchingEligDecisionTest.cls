/**
 * Created by Denis Belkovskii
 * on 9/15/2020.
 */

@IsTest
public class VT_R5_DocSetMatchingEligDecisionTest {
    @TestSetup
    static void setup() {
        DomainObjects.VTD1_Document_t firstDocument = new DomainObjects.VTD1_Document_t()
                .setRecordTypeId(Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName()
                        .get('VTD1_Patient_eligibility_assessment_form').getRecordTypeId()
                );
        new DomainObjects.Task_t()
                .addDocument(firstDocument)
                .setVTD1_Type_for_backend_logic('TMA task');
        firstDocument.persist();
        DomainObjects.Case_t domainCase = new DomainObjects.Case_t()
                .addSCRUser(new DomainObjects.User_t())
                .addVTD1_Patient(new DomainObjects.VTD1_Patient_t());
        new DomainObjects.VTD1_Actual_Visit_t()
                .addVTD1_Case(domainCase)
                .setVTR2_Modality('At Location')
                .persist();
        new DomainObjects.VTD1_Document_t()
                .addCase(domainCase)
                .setRecordTypeId(VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE)
                .persist();
    }

    @IsTest
    static void testHandler() {
        Test.startTest();
        VTD1_Document__c firstDocument = [
                SELECT Id, VTD2_EAFStatus__c, RecordType.DeveloperName, VTD1_Clinical_Study_Membership__c
                FROM VTD1_Document__c
                WHERE RecordType.DeveloperName = 'VTD1_Patient_eligibility_assessment_form' 
                AND VTD1_Clinical_Study_Membership__c = NULL
                LIMIT 1
        ];
        firstDocument.VTD1_Eligibility_Assessment_Status__c = 'Eligible';
        update firstDocument;
        VTD1_Document__c firstDocumentAfterUpdate = [
                SELECT  VTD2_EAFStatus__c,
                        RecordType.DeveloperName,
                        VTD1_Eligibility_Assessment_Status__c
                FROM VTD1_Document__c
                WHERE RecordType.DeveloperName = 'VTD1_Patient_eligibility_assessment_form'
                AND VTD1_Clinical_Study_Membership__c = NULL
                LIMIT 1
        ];
        System.assertEquals('Signed by PI', firstDocumentAfterUpdate.VTD2_EAFStatus__c);

        firstDocument.VTD2_TMA_Eligibility_Decision__c = 'Eligible';
        update firstDocument;
        VTD1_Document__c firstDocumentAfterSecondUpdate = [
                SELECT Id, VTD2_EAFStatus__c, RecordType.DeveloperName
                FROM VTD1_Document__c
                WHERE RecordType.DeveloperName = 'VTD1_Patient_eligibility_assessment_form'
                AND VTD1_Clinical_Study_Membership__c = NULL
                LIMIT 1
        ];
        System.assertEquals('Signed by TMA', firstDocumentAfterSecondUpdate.VTD2_EAFStatus__c);

        Task testTmaTask = [
                SELECT Status FROM Task WHERE Document__c = :firstDocumentAfterSecondUpdate.Id
        ];
        System.assertEquals('Completed', testTmaTask.Status);

        VTD1_Document__c secondDocument = [
                SELECT  VTD1_Clinical_Study_Membership__r.VTD1_Eligibility_Status__c,
                        VTD1_Eligibility_Assessment_Status__c
                FROM VTD1_Document__c
                WHERE VTD1_Clinical_Study_Membership__c != NULL
                LIMIT 1
        ];
        secondDocument.VTD1_Eligibility_Assessment_Status__c = 'Eligible';
        update secondDocument;
        VTD1_Document__c secondDocumentAfterUpdate = [
                SELECT  VTD1_Clinical_Study_Membership__r.VTD1_Eligibility_Status__c,
                        VTD1_Eligibility_Assessment_Status__c,
                        VTD2_Final_Eligibility_Decision__c,
                        VTD2_EAFStatus__c
                FROM VTD1_Document__c
                WHERE VTD1_Clinical_Study_Membership__c != NULL
        ];
        System.assertEquals(secondDocumentAfterUpdate.VTD1_Clinical_Study_Membership__r.VTD1_Eligibility_Status__c,
                secondDocumentAfterUpdate.VTD1_Eligibility_Assessment_Status__c);
        System.assertEquals('Completed', secondDocumentAfterUpdate.VTD2_EAFStatus__c);
        System.assertEquals(secondDocumentAfterUpdate.VTD2_Final_Eligibility_Decision__c,
                secondDocumentAfterUpdate.VTD1_Eligibility_Assessment_Status__c);

        secondDocumentAfterUpdate.VTD2_TMA_Eligibility_Decision__c = 'Ineligible';
        update secondDocumentAfterUpdate;
        VTD1_Document__c secondDocumentAfterSecondUpdate = [
                SELECT  VTD1_Clinical_Study_Membership__r.VTD1_Eligibility_Status__c,
                        VTD1_Clinical_Study_Membership__r.VTD2_TMA_Review_Required__c,
                        VTD1_Eligibility_Assessment_Status__c,
                        VTD2_TMA_Eligibility_Decision__c,
                        VTD2_Final_Eligibility_Decision__c,
                        VTD2_EAFStatus__c
                FROM VTD1_Document__c
                WHERE VTD1_Clinical_Study_Membership__c != NULL
        ];
        System.assertEquals(secondDocumentAfterSecondUpdate.VTD1_Clinical_Study_Membership__r.VTD1_Eligibility_Status__c,
                secondDocumentAfterSecondUpdate.VTD2_Final_Eligibility_Decision__c);

        secondDocumentAfterUpdate.VTD1_Clinical_Study_Membership__r.VTD2_TMA_Review_Required__c = true;
        secondDocumentAfterUpdate.VTD2_TMA_Eligibility_Decision__c = null;
        update secondDocumentAfterUpdate.VTD1_Clinical_Study_Membership__r;
        update secondDocumentAfterUpdate;
        secondDocumentAfterSecondUpdate.VTD2_TMA_Eligibility_Decision__c = 'Ineligible';
        secondDocumentAfterSecondUpdate.VTD2_Final_Eligibility_Decision__c = null;
        update secondDocumentAfterSecondUpdate;
        Test.stopTest();
    }
}