public with sharing class StudyTeamMembersUploadController {
    
    private static List<List<String>> parsedCsv = new List<List<String>>();
    private static Map<String, Id> userMap = new Map<String, Id>();

    @auraEnabled
    public static String processUpload(Id docId, Id studyId) {
        getParsedCsvFromDocId(docId);

        Set<String> emails = new Set<String>();
        for (List<String> row : parsedCsv) {
            emails.addAll(row);
        }

        if (!emails.isEmpty()) {
            getUsersByEmail(emails);
            createTeamMembers(studyId);
        }

        deleteContentDoc(docId);
        return '';
    }

    private static void getParsedCsvFromDocId(Id docId) {
        parsedCsv = CSVReader.Parse([SELECT VersionData FROM ContentVersion WHERE ContentDocumentId = :docId LIMIT 1].VersionData.toString());
    }

    private static void deleteContentDoc(Id docId) {
        delete [SELECT Id FROM ContentDocument WHERE Id = :docId];
    }

    private static void getUsersByEmail(Set<String> emails) {
        for (User item : [SELECT Id, Email FROM User WHERE Email IN :emails]) {
            userMap.put(item.Email, item.Id);
        }
    }
    
    private static void createTeamMembers(Id studyId) {
        // create members for 2nd column first
        Set<String> secondColEmails = new Set<String>();
        for (List<String> row : parsedCsv) {
            if (row.size() == 2) {
                secondColEmails.add(row[1]);
            }
        }
        // mapped by email address so we can link to column 1 users later
        Map<String, Study_Team_Member__c> masterMap = new Map<String, Study_Team_Member__c>();
        for (String email : secondColEmails) {
            if (userMap.containsKey(email)) {
                masterMap.put(email, new Study_Team_Member__c(
                    Study__c = studyId,
                    User__c = userMap.get(email)
                ));
            }
        }
        if (!masterMap.isEmpty()) { insert masterMap.values(); }

        // now for 1st col
        List<Study_Team_Member__c> newMembers = new List<Study_Team_Member__c>();
        for (List<String> row : parsedCsv) {
            if (!row.isEmpty() && userMap.containsKey(row[0]) && !masterMap.containsKey(row[0])) {
                Study_Team_Member__c newMember = new Study_Team_Member__c(
                    Study__c = studyId,
                    User__c = userMap.get(row[0])
                );
                if (row.size() == 2 && masterMap.containsKey(row[1])) {
                    newMember.Study_Team_Member__c = masterMap.get(row[1]).Id;
                }
                newMembers.add(newMember);
            }
        }
        if (!newMembers.isEmpty()) { insert newMembers; }
    }
}