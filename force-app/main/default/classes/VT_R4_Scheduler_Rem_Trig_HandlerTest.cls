/**
 * Created by Belkovskii Denis on 5/15/2020.
 */
@isTest 
public with sharing class VT_R4_Scheduler_Rem_Trig_HandlerTest {

    public static void testRemindersTrigger(){
        VTR4_ExternalScheduler__c testSetting = new VTR4_ExternalScheduler__c(
                VTR4_RemindersIsActive__c = true
        );
        insert testSetting;
        List<VTD1_Actual_Visit__c> actualVisits = new List<VTD1_Actual_Visit__c>();
        Case patientCase = new Case();
        insert patientCase;
        for(Integer i=0; i<21; i++){
            VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c(
                    VTR5_PatientCaseId__c = patientCase.Id
            );
            actualVisits.add(actualVisit);
        } 
        insert actualVisits;
        List<VTR4_Scheduler_Reminders__c> schReminders = new List<VTR4_Scheduler_Reminders__c>();
        for(Integer i=0; i<20; i++){
            VTR4_Scheduler_Reminders__c testReminder = new VTR4_Scheduler_Reminders__c();
            testReminder.VTR4_Actual_Visit__c = actualVisits[i].Id;
            testReminder.VTR4_Hours_Before__c = i;
            testReminder.VTR4_ReadyToStart__c = true;
            schReminders.add(testReminder);


        }
        insert schReminders;
        Integer numberOfSchRemindersAfterTrigger = [SELECT COUNT() FROM VTR4_Scheduler_Reminders__c];
        System.assertEquals(20, numberOfSchRemindersAfterTrigger);
    }
}