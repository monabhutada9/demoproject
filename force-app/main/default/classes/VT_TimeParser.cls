/**
 * Created by Dmitry Kovalev on 11.08.2020.
 */

public without sharing class VT_TimeParser {
    /* Internal Exceptions */
    public class InvalidTimeFormatException extends Exception {}
    public class InvalidTimeRangeException extends Exception {}

    /* Attributes/Constants */
    public Integer hour {
        get;
        set {
            if (value < 0 || value > 23) {
                throw new InvalidTimeRangeException();
            } else {
                hour = value;
            }
        }
    }
    public Integer minute {
        get;
        set {
            if (value < 0 || value > 59) {
                throw new InvalidTimeRangeException();
            } else {
                minute = value;
            }
        }
    }
    public Integer second {
        get;
        set {
            if (value < 0 || value > 59) {
                throw new InvalidTimeRangeException();
            } else {
                second = value;
            }
        }
    }
    public Integer millisecond {
        get;
        set {
            if (value < 0 || value > 999) {
                throw new InvalidTimeRangeException();
            } else {
                millisecond = value;
            }
        }
    }

    public static final String PATTERN_12HOUR_CLOCK = '^(\\d{1,2})(?::(\\d{2}))?(?::(\\d{2}))?(?:.(\\d{3}))?[ ]?([aApP][mM])$';
    public static final String PATTERN_24HOUR_CLOCK = '^(\\d{1,2}):(\\d{2})(?::(\\d{2}))?(?:.(\\d{3}))?$';

    /* Constructors */
    public VT_TimeParser() {
        this.hour = 0;
        this.minute = 0;
        this.second = 0;
        this.millisecond = 0;
    }

    public VT_TimeParser(Integer hour, Integer minute, Integer second, Integer millisecond) {
        this(hour, minute, second);
        this.millisecond = millisecond;
    }

    public VT_TimeParser(Integer hour, Integer minute, Integer second) {
        this(hour, minute);
        this.second = second;
    }

    public VT_TimeParser(Integer hour, Integer minute) {
        this(hour);
        this.minute = minute;
    }

    public VT_TimeParser(Integer hour) {
        this();
        this.hour = hour;
    }

    public VT_TimeParser(String timeStr) {
        this(parseTime(timeStr));
    }

    public VT_TimeParser(DateTime datetimeObj) {
        this(datetimeObj, true);
    }

    public VT_TimeParser(DateTime datetimeObj, Boolean isGMT) {
        if (isGMT) {
            this(datetimeObj.timeGmt());
        } else {
            this(datetimeObj.time());
        }
    }

    public VT_TimeParser(Time timeObj) {
        this(timeObj.hour(), timeObj.minute(), timeObj.second(), timeObj.millisecond());
    }

    public VT_TimeParser(VT_TimeParser timeObj) {
        this(timeObj.hour, timeObj.minute, timeObj.second, timeObj.millisecond);
    }

    /**
     * <p>This static method returns 24-hour clock time object based on taken valid time string.</p>
     * <p>Examples of valid time formatting:</p>
     * <ul>
     *     <li>Only hours: 3AM (only with </li>
     *     <li>Hours and minutes: 3:30AM</li>
     *     <li>The same +seconds: 3:30:00AM</li>
     *     <li>The same again +milliseconds: 3:30:00.468AM</li>
     *     <li>With whitespace: 3:30 AM</li>
     *     <li>12:30AM as 00:30</li>
     *     <li>12:30PM as 12:30</li>
     *     <li>24-hour range: 00:00:00.000 – 23:59:59.999</li>
     * </ul>
     *
     * @param timeStr String - time in 12-hour or 24-hour clock format (with seconds and milliseconds)
     *
     * @return Time24HourClock object
     */
    public static VT_TimeParser parseTime(String timeStr) {
        VT_TimeParser result;
        timeStr = timeStr.toUpperCase();
        if (timeStr.contains('AM') || timeStr.contains('PM')) {
            result = parseTimeWithPattern(timeStr, PATTERN_12HOUR_CLOCK);
        } else {
            result = parseTimeWithPattern(timeStr, PATTERN_24HOUR_CLOCK);
        }
        return result;
    }

    private static VT_TimeParser parseTimeWithPattern(String timeStr, String patternStr) {
        Pattern timePattern = Pattern.compile(patternStr);
        Matcher timeMatcher = timePattern.matcher(timeStr);

        if (!timeMatcher.matches()) {
            throw new InvalidTimeFormatException();
        }

        Integer hour = Integer.valueOf(timeMatcher.group(1));
        Integer minute = Integer.valueOf(timeMatcher.group(2) != null ? timeMatcher.group(2) : '0');
        Integer second = Integer.valueOf(timeMatcher.group(3) != null ? timeMatcher.group(3) : '0');
        Integer millisecond = Integer.valueOf(timeMatcher.group(4) != null ? timeMatcher.group(4) : '0');

        // According to: https://en.wikipedia.org/wiki/12-hour_clock
        if (timeMatcher.groupCount() == 5) {
            Boolean isAM = timeMatcher.group(5) == 'AM';
            hour = (hour == 12) ? 0 : hour;
            hour = isAM ? hour : hour + 12;
        }

        return new VT_TimeParser(hour, minute, second, millisecond);
    }

    /* Convert to Minutes */
    public static Integer toMinutes(Time timeObj) {
        return new VT_TimeParser(timeObj).toMinutes();
    }

    public static Integer toMinutes(VT_TimeParser timeObj) {
        return timeObj.toMinutes();
    }

    public Integer toMinutes() {
        return this.hour * 60 + this.minute;
    }

    /* Convert to Time object */
    public static Time toTime(VT_TimeParser timeObj) {
        return timeObj.toTime();
    }

    public Time toTime() {
        return Time.newInstance(this.hour, this.minute, this.second, this.millisecond);
    }

    /* to string */
    public override String toString() {
        return leftPad(this.hour) + ':' + leftPad(this.minute);
    }

    private String leftPad(Integer n) {
        return String.valueOf(n).leftPad(2, '0');
    }
}