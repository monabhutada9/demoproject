/**
* Controller for VT_R5_StudyNotificationComponent.
* Handles Translation for Study device and shows Message for others in the email
*
* @author OF
*/

public class VT_R5_StudyNotificationEmailController {
    public Id notificationId {get;set;}

    public VTD1_NotificationC__c getnotificationRecord() {
        try{

            VTD1_NotificationC__c translatedNotification = [

                    SELECT Id, Title__c, OwnerId, VTD2_Title_Parameters__c, Link_to_related_event_or_object__c,
                            CreatedDate, VTD2_New_Message_Added_DateTme__c, FORMAT(CreatedDate) CreatedDateF,
                            FORMAT(VTD2_New_Message_Added_DateTme__c) VTD2_New_Message_Added_DateTme__cF, Message__c,
                            VTD1_Read__c, Number_of_unread_messages__c, Type__c, HasDirectLink__c, isActualVisitLink__c,



                            VTD1_Parameters__c, VTD1_CSM__c, VTD2_VisitID__c, VTR2_PatientNotificationId__c,
                            VDT2_Unique_Code__c, VTR4_CurrentPatientCase__r.VTD2_Study_Phone_Number__c
                    FROM VTD1_NotificationC__c WHERE Id = : notificationId LIMIT 1


            ];

            if (hasLabelAsMessageBody(translatedNotification.Message__c)) {
                VT_D1_TranslateHelper.translate(translatedNotification,true);

            }

            if(String.isNotBlank(translatedNotification.Message__c)){
                translatedNotification.Message__c = translatedNotification.Message__c.replaceAll(' ', '&nbsp;') // SH-19814
                                                                                 .replaceAll('\n', '<br>');
        }

            return translatedNotification;
        } catch(Exception e) {
            System.debug('Exception: ' + e);
            return null;
        }
    }

    // SH-19553 Study Notification Email template show custom label api name instead of value
    public Boolean hasLabelAsMessageBody(String message) {
        if (!message.contains(' ')) {
            try {
                new PageReference('/apex/VT_D2_CustomLabelRenderPage?labelName=' + message + '&lang=en_US').getContent();
                return true;
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }
}