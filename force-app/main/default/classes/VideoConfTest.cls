/**
 * Created by User on 19/05/15.
 */
@isTest
private with sharing class VideoConfTest {
    @isTest
    private static void doTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockSendVideoConfSignalHttpResponseGenerator());
        insert new Video_Conference__c();
        Test.stopTest();
    }

    public class MockSendVideoConfSignalHttpResponseGenerator implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('["test"]');
            res.setStatusCode(200);
            return res;
        }
    }
}