/**
* @author: Carl Judge
* @date: 24-Nov-18
**/
public without sharing class VT_D2_StudyTriggerHandler {
    public static Boolean byPassRequiredFields = test.isRunningTest();
    public static final List<String> STUDY_PARTICIPANT_FIELDS = new List<String>{
            'VTD1_Project_Lead__c',
            'VTD1_Virtual_Trial_Study_Lead__c',
            'VTD1_Regulatory_Specialist__c',
            'VTD1_Remote_CRA__c',
            'VTD1_Virtual_Trial_head_of_Operations__c'
    };

    public void onAfterUpdate(List<HealthCloudGA__CarePlanTemplate__c> studies, Map<Id, HealthCloudGA__CarePlanTemplate__c> oldMap) {
        System.debug('inside onAfterUpdate');
        VT_R2_DocumentSharingTriggerHelper.processStudies(studies, oldMap);
        updateGlobalTaskOwner(studies, oldMap);
        updateCertifyDocVisibility(studies, oldMap);
        processChangedParticipants(studies, oldMap);
        VT_R5_CustomAuditHandler_Misc.onAfterUpdateStudy(studies, oldMap);
        rebuildSharings(studies, oldMap);
    }
    public void onBeforeInsert(List<HealthCloudGA__CarePlanTemplate__c> studies) {
        validateRecueStudyRequiredFields(studies, !byPassRequiredFields);
    }
    public void onAfterInsert(Map<Id, HealthCloudGA__CarePlanTemplate__c> newMap) {
        createStudySkillAndButtonCaller(newMap.keySet());
        VT_R5_ShareGroupCreator.createGroups(newMap.values());
    }
    public void onBeforeUpdate(List<HealthCloudGA__CarePlanTemplate__c> studies, map<Id, HealthCloudGA__CarePlanTemplate__c> oldMap) {
        List<HealthCloudGA__CarePlanTemplate__c> lstStudies = new List<HealthCloudGA__CarePlanTemplate__c>();
        for (HealthCloudGA__CarePlanTemplate__c study : studies) {
            if (!study.VT_R4_RescueTelVisitStudy__c && study.VT_R4_RescueTelVisitStudy__c != oldMap.get(study.Id).VT_R4_RescueTelVisitStudy__c) {
                lstStudies.add(study);
            }
        }
        validateRecueStudyRequiredFields(lstStudies, !test.isRunningTest());
    }
    private void processChangedParticipants(List<HealthCloudGA__CarePlanTemplate__c> studies, Map<Id, HealthCloudGA__CarePlanTemplate__c> oldMap) {
        List<VT_R3_UpdateAccessStudy.StudyUser> studyUsersAdded = new List<VT_R3_UpdateAccessStudy.StudyUser>();
        List<VT_R3_UpdateAccessStudy.StudyUser> studyUsersRemoved = new List<VT_R3_UpdateAccessStudy.StudyUser>();
        for (HealthCloudGA__CarePlanTemplate__c study : studies) {
            for (String field : STUDY_PARTICIPANT_FIELDS) {
                Object newVal = study.get(field);
                Object oldVal = oldMap.get(study.Id).get(field);
                if (oldVal != newVal) {
                    if (newVal != null) {
                        studyUsersAdded.add(new VT_R3_UpdateAccessStudy.StudyUser(study.Id, (Id) newVal));
                    }
                    if (oldVal != null) {
                        studyUsersRemoved.add(new VT_R3_UpdateAccessStudy.StudyUser(study.Id, (Id) oldVal));
                    }
                }
            }
        }
        if (!studyUsersAdded.isEmpty()) {
            VT_R3_UpdateAccessStudy.studyParticipantSharingOnAdd(studyUsersAdded);
        }
        if (!studyUsersRemoved.isEmpty()) {
            VT_R3_UpdateAccessStudy.studyParticipantSharingOnRemove(studyUsersRemoved);
        }
    }
    private void updateCertifyDocVisibility(List<HealthCloudGA__CarePlanTemplate__c> studies, Map<Id, HealthCloudGA__CarePlanTemplate__c> oldMap) {
        Map<Id, String> certifyDocNameMap = new Map<Id, String>();
        for (HealthCloudGA__CarePlanTemplate__c study : studies) {
            if (study.VTD2_Certification_form_Medical_Records__c != oldMap.get(study.Id).VTD2_Certification_form_Medical_Records__c &&
                    study.VTD2_Certification_form_Medical_Records__c != null) {
                certifyDocNameMap.put(study.Id, study.VTD2_Certification_form_Medical_Records__c);
            }
        }
        if (!certifyDocNameMap.isEmpty()) {
            List<ContentDocumentLink> linksToUpdate = new List<ContentDocumentLink>();
            List<Id> studyIds = new List<Id>(certifyDocNameMap.keySet());
            for (ContentDocumentLink link : [
                    SELECT Id, Visibility,LinkedEntityId,ContentDocument.Title
                    FROM ContentDocumentLink
                    WHERE LinkedEntityId IN :studyIds
            ]) {
                if (certifyDocNameMap.get(link.LinkedEntityId) == link.ContentDocument.Title && link.Visibility != 'AllUsers') {
                    link.Visibility = 'AllUsers';
                    linksToUpdate.add(link);
                }
            }
            if (!linksToUpdate.isEmpty()) {
                update linksToUpdate;
            }
        }
    }
    private void updateGlobalTaskOwner(List<HealthCloudGA__CarePlanTemplate__c> studies, Map<Id, HealthCloudGA__CarePlanTemplate__c> oldMap) {
        Map<Id, Id> studyOldGPlmap = new Map<Id, Id>(); //map to store old values VTD1_Project_Lead__c on Study
        Map<Id, Id> studyOldGVTSLMap = new Map<Id, Id>(); //map to store old values VTD1_Virtual_Trial_Study_Lead__c on Study
        Set<Id>changedStudies = new Set<Id>();
        Set<Id> taskOwners = new Set<Id>();
        for (HealthCloudGA__CarePlanTemplate__c study : studies) {
            if (oldMap.get(study.Id).VTD1_Project_Lead__c != null && study.VTD1_Project_Lead__c != oldMap.get(study.Id).VTD1_Project_Lead__c) {
                studyOldGPlmap.put(study.Id, oldMap.get(study.Id).VTD1_Project_Lead__c);
                changedStudies.add(study.Id);
            }
            if (oldMap.get(study.Id).VTD1_Virtual_Trial_Study_Lead__c != null && study.VTD1_Virtual_Trial_Study_Lead__c != oldMap.get(study.Id).VTD1_Virtual_Trial_Study_Lead__c) {
                studyOldGVTSLMap.put(study.Id, oldMap.get(study.Id).VTD1_Virtual_Trial_Study_Lead__c);
                changedStudies.add(study.Id);
            }
        }
        taskOwners.addAll(studyOldGPlmap.values());
        taskOwners.addAll(studyOldGVTSLMap.values());
        if (studyOldGPlmap.keySet().size() == 0 && studyOldGVTSLMap.keySet().size() == 0) return;
        List<Task> taskToSelect = [
                SELECT Id, OwnerId, HealthCloudGA__CarePlanTemplate__c
                FROM Task
                WHERE Status != 'Completed'
                AND HealthCloudGA__CarePlanTemplate__c
                IN :changedStudies and OwnerId in: taskOwners
        ];
        Map<Id, List<Task>> taskMap = new Map<Id, List<Task>>();
        for (Task tsk : taskToSelect) {
            if (!taskMap.keySet().contains(tsk.HealthCloudGA__CarePlanTemplate__c)) taskMap.put(tsk.HealthCloudGA__CarePlanTemplate__c, new List<Task>());
            taskMap.get(tsk.HealthCloudGA__CarePlanTemplate__c).add(tsk);
        }
        List<Task> tasksToUpdate = new List<Task>();
        for (HealthCloudGA__CarePlanTemplate__c study : studies) {
            List<Task> taskList = new List <Task>();
            if (taskMap.get(study.Id) != null)taskList = taskMap.get(study.Id);
            for (Task tsk : taskList) {
                if (tsk.OwnerId != null && tsk.OwnerId == studyOldGPlmap.get(study.Id)) {
                    tsk.OwnerId = Id.valueOf(study.VTD1_Project_Lead__c);
                    tasksToUpdate.add(tsk);
                }
                if (tsk.OwnerId != null && tsk.OwnerId == studyOldGVTSLMap.get(study.Id)) {
                    tsk.OwnerId = Id.valueOf(study.VTD1_Virtual_Trial_Study_Lead__c);
                    tasksToUpdate.add(tsk);
                }
            }
        }
        System.debug('tasksToUpdate ' + tasksToUpdate);
        update tasksToUpdate;

    }

    public static void validateRecueStudyRequiredFields(List< HealthCloudGA__CarePlanTemplate__c> studies, boolean byPassStudyValidationForTest) {
        if (byPassStudyValidationForTest) {
            for (HealthCloudGA__CarePlanTemplate__c study : studies) {
                if (!study.VT_R4_RescueTelVisitStudy__c && (study.VTD1_Carematix_Bulk_Upload__c == null || study.VTD1_Study_Uses_Connected_Device_Kits__c == null ||
                        study.VTD1_Name__c == null || study.VTD1_Address_Line_1__c == null || study.VTR3_Countryforreturn__c == null ||
                        study.State_or_return__c == null || study.City_for_return__c == null || study.VTD1_ZIP__c == null || study.VTD1_DeliveryTeamGroupId__c == null ||
                        study.VTD1_Patient_Group_Id__c == null || study.VTD1_PIGroupId__c == null || study.VTR2_SCR_Group_Id__c == null || study.VTD1_SCGroupId__c == null ||
                        study.VTD2_CM_Group_Id__c == null || study.VTD1_PG_Study_Group_Id__c == null || study.VTR2_REGroupId__c == null || study.VTR2_CRA_Group_Id__c == null || study.VTD1_Community_Id__c == null
                )) {
                    study.addError('Please add required fields related to IMP Returns, Lab Returns, Return Shipping Label. Communication Groups and Community');
                }
            }
        }
    }


    /* Author: Shailesh B
    * method : createStudySkillAndButtonCaller(Set<Id> newSet) {
    * param : Set<Id> newSet
    * Description : This method id used to call queueable when the context is not in async transaction
                    which creates chat button and skills
    * return :  void
    */
    public void createStudySkillAndButtonCaller(Set<Id> newSet) {
        System.debug('Future context ' + System.isFuture());
        System.debug('Map >>> ' + newSet);
        if (!System.isFuture() && !System.isBatch() && !System.isQueueable()
                && !newSet.isEmpty()) {
            VTR5_CreateStudySkillQueueable asyncHandlerCaller = new VTR5_CreateStudySkillQueueable(newSet);
            Id jobId = System.enqueueJob(asyncHandlerCaller);
            System.debug('###Id jobId  ' + jobId);

        }
    }



    private static void rebuildSharings(List<HealthCloudGA__CarePlanTemplate__c> studies, Map<Id, HealthCloudGA__CarePlanTemplate__c> oldMap) {
        //select only studies with changed fields of users lookups
        List<Id> studyIds = new List<Id>();
        List<Id> studyIdsNotOnHold = new List<Id>();
        for (HealthCloudGA__CarePlanTemplate__c study : studies) {
            if (study.VTD1_Remote_CRA__c != oldMap.get(study.Id).VTD1_Remote_CRA__c ||
                    study.VTD1_Project_Lead__c != oldMap.get(study.Id).VTD1_Project_Lead__c ||
                    study.VTD1_Virtual_Trial_Study_Lead__c != oldMap.get(study.Id).VTD1_Virtual_Trial_Study_Lead__c) {
                studyIds.add(study.Id);
            }
            if (oldMap.get(study.Id).VTD1_Recruitment_Hold__c &&
                    !study.VTD1_Recruitment_Hold__c) {
                studyIdsNotOnHold.add(study.Id);
            }
        }

        if (!studyIdsNotOnHold.isEmpty()) {
            VT_D2_StudyUpdateHelper.convertCandidatePatientsOnHold(studyIdsNotOnHold);
        }
        //Query Action Items for rebuild sharing:
        if (!studyIds.isEmpty()) {
            Map<Id, VTD1_Action_Item__c> aiMap = new Map<Id, VTD1_Action_Item__c>([
                    SELECT Id
                    FROM VTD1_Action_Item__c
                    WHERE VTD1_Study__c IN:studyIds
            ]);
            //Rebuild sharing for action items
            VT_D1_ActionItemSharingHandler.rebuildSharingForActionItems(aiMap.keySet());

        }
    }
}