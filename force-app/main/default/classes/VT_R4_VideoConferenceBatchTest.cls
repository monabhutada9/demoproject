/**
 * Created by Andrey Pivovarov on 4/20/2020.
 */

@IsTest
private class VT_R4_VideoConferenceBatchTest {
    @TestSetup
    static void setup() {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));
        DomainObjects.Case_t patientCase = new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .addStudy(study);
        patientCase.persist();
        List<Video_Conference__c> videoConferences= new List<Video_Conference__c>();
        List<VTD1_Actual_Visit__c> actualVisits = new List<VTD1_Actual_Visit__c>();
        Datetime curDate = System.now();
        for (Integer i = 0; i < 20; i++) {
            VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c(
                    Name = 'Test' + i,
                    VTD1_Scheduled_Date_Time__c = curDate.addMinutes(15),
                    VTR5_PatientCaseId__c = patientCase.Id
            );
            actualVisits.add(actualVisit);
        }
        insert actualVisits;
        for (Integer i = 0; i < 10; i++) {
            videoConferences.add(new Video_Conference__c(
                    Scheduled_For__c = curDate.addMinutes(30),
                    VTD1_Actual_Visit__c = actualVisits[i].Id
            ));
            videoConferences.add(new Video_Conference__c(
                    Scheduled_For__c = curDate.addMinutes(20),
                    VTD1_Actual_Visit__c = actualVisits[i+10].Id
            ));
        }
        insert videoConferences;
        VTR4_Scheduler_Video_Conference__c svc = new VTR4_Scheduler_Video_Conference__c(
                VTR4_Video_Conference__c = videoConferences[0].Id,
                VTR4_Scheduled_for__c = videoConferences[0].Scheduled_For__c,
                VTR4_ReadyToStart__c = true);
        insert svc;
        VTR4_Scheduler_PushNotifications__c sPn = new VTR4_Scheduler_PushNotifications__c(
                VTR4_Video_Conference__c = videoConferences[0].Id,
                VTR4_Scheduled_for__c = videoConferences[0].Scheduled_For__c
        );
        insert sPn;
    }

    @IsTest
    static void videoConferenceBatchTest() {
        Datetime curDate = System.now().addMinutes(-5);
        Datetime nextDate = curDate.addHours(1);
        System.debug('between ' + curDate + ' and ' + nextDate);
        List<Video_Conference__c> videoConferences = [
                SELECT Scheduled_For__c, VTD1_Actual_Visit__c
                FROM Video_Conference__c
                WHERE Scheduled_For__c >: curDate AND Scheduled_For__c <: nextDate];
        videoConferences[0].Scheduled_For__c = curDate.addMinutes(14);
        System.debug('conference* ' + videoConferences[0]);
        System.debug('size*VC ' + videoConferences);
        update videoConferences;
        Test.startTest();

        VT_R4_VideoConferenceScheduler videoScheduler = new VT_R4_VideoConferenceScheduler();
        Datetime dt = videoScheduler.getAlignedTimeJob(System.now());

        Database.executeBatch(new VT_R4_VideoConferenceBatch(videoScheduler, dt),200);

        Test.stopTest();
        List<VTR4_Scheduler_Video_Conference__c> svc = [
                SELECT VTR4_Scheduled_for__c, VTR4_Video_Conference__r.Scheduled_For__c, VTR4_Video_Conference__r.Id,
                        VTR4_ReadyToStart__c, VTR4_ReadyToEnd__c
                FROM VTR4_Scheduler_Video_Conference__c
        ];
        System.debug(svc[0].VTR4_Video_Conference__r.Scheduled_For__c + ' *and* ' + svc[0].VTR4_Scheduled_for__c
                + ' ' + svc[0].VTR4_ReadyToStart__c + ' ' + svc[0].VTR4_ReadyToEnd__c);
        System.assertEquals(svc[0].VTR4_Video_Conference__r.Scheduled_For__c, svc[0].VTR4_Scheduled_for__c);
        System.assertEquals(svc[0].VTR4_Video_Conference__r.Id, svc[0].VTR4_Video_Conference__c);
    }

    @IsTest
    static void pushNotificationsTest() {
        Test.startTest();

        VT_R4_PushNotificationsScheduler pushScheduler = new VT_R4_PushNotificationsScheduler();
        Datetime dt = pushScheduler.getAlignedTimeJob(System.now());

        Database.executeBatch(new VT_R4_PushNotificationsBatch(pushScheduler, dt),200);

        Test.stopTest();
        List<Video_Conference__c> videoConferences = [
                SELECT Scheduled_For__c,
                (SELECT VTR4_Scheduled_for__c FROM Schedulers_Push_Notifications__r)
                FROM Video_Conference__c
        ];
        System.debug('pushSize ' + videoConferences.size());
        System.debug('vidConf ' + videoConferences[0].Schedulers_Push_Notifications__r[0].VTR4_Scheduled_for__c + ' -- ' +
                videoConferences[0].Scheduled_For__c.addMinutes(-15));
        System.assertEquals(videoConferences[0].Schedulers_Push_Notifications__r[0].VTR4_Scheduled_for__c,
                videoConferences[0].Scheduled_For__c.addMinutes(-15));
    }
}