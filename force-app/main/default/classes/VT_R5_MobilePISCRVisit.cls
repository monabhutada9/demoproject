/**
 * Created by Yuliya Yakushenkova on 10/28/2020.
 */

public class VT_R5_MobilePISCRVisit extends VT_R5_MobileRestResponse implements VT_R5_MobileLibrary.Routable {

    private transient RequestParams requestParams = new RequestParams();

    public List<String> getMapping() {
        return new List<String>{
                '/mobile/{version}/visit/{id}',
                '/mobile/{version}/visit/{id}/{action}'
        };
    }

    public VT_R5_MobileRestResponse versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        String requestAction = parameters.get('action');
        String requestId = parameters.get('id');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        if (requestAction == 'participants') {
                            return get_v1_participants(requestId);
                        } if (requestAction == 'timeslots') {
                            return get_v1_time_slots(requestId);
                        } else {
                            return get_v1_visit(requestId);
                        }
                    }
                }
            }
            when 'POST' {
                switch on requestVersion {
                    when 'v1' {
                        switch on requestAction {
                            when 'reschedule' {
                                return post_v1_reschedule(requestId);
                            }
                            when 'cancel' {
                                return post_v1_cancel(requestId);
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    public void initService() {
        parseBody();
        parseParams();
    }

    public Map<String, String> getMinimumVersions() {
        return null;
    }

    public VT_R5_MobileRestResponse get_v1_visit(String visitId) {
        return this.buildResponse(
                VT_R5_PISCRCalendarService.getInstance()
                        .getVisitAndMembersByVisitIdAndUserId(visitId, UserInfo.getUserId())
        );
    }

    public VT_R5_MobileRestResponse get_v1_participants(String visitId) {
        return this.buildResponse(
                VT_R5_PISCRCalendarService.getInstance()
                        .getAllPossibleParticipantsByVisitId(visitId, UserInfo.getUserId())
        );
    }

    public VT_R5_MobileRestResponse get_v1_time_slots(String visitId) {
        return this.buildResponse(
                new VT_R5_VisitTimeSlotsService.DateTimeSlotsHelper(visitId, requestParams.members)
                        .getDateTimeSlots()
        );
    }

    public VT_R5_MobileRestResponse post_v1_reschedule(String visitId) {
        try {
            VT_R5_PISCRCalendarService.Visit visit =
                    VT_R5_PISCRCalendarService.getConvertedVisit(JSON.serialize(requestParams.visit));
            List<VT_R5_PISCRCalendarService.Participant> externalParticipants =
                    VT_R5_PISCRCalendarService.getConvertedParticipants(JSON.serialize(requestParams.visit.externalParticipants));

            VT_R5_PISCRCalendarService.getInstance()
                    .rescheduleVisit(visitId, visit, externalParticipants, requestParams.visit.members);

            return this.buildResponse('GOOD'); // TODO: add label
        } catch (DmlException e) {
            e.setMessage('BAD');
            return this.buildResponse(e); // TODO: add label
        } catch (VT_R5_PISCRCalendarService.CalendarServiceException e) {
            e.setMessage('Datetime not available');
            return this.buildResponse(e); // TODO: add label
        }
    }

    public VT_R5_MobileRestResponse post_v1_cancel(String visitId) {
        try {
            VT_R5_PISCRCalendarService.Visit visit =
                    VT_R5_PISCRCalendarService.getConvertedVisit(JSON.serialize(requestParams.visit));

            VT_R5_PISCRCalendarService.getInstance().cancelVisit(visitId, visit);
            return this.buildResponse('GOOD'); // TODO: add label
        } catch (DmlException e) {
            e.setMessage('BAD');
            return this.buildResponse(e); // TODO: add label
        }
    }

    private void parseBody() {
        if (request.requestBody != null && request.requestBody.size() > 0) {
            this.requestParams = (RequestParams) JSON.deserialize(request.requestBody.toString(), RequestParams.class);
        }
    }

    private void parseParams() {
        if (request.params.containsKey('members')) this.requestParams.setMembers(request.params.get('members'));
        if (request.params.containsKey('preferredDate')) this.requestParams.setPreferredDate(request.params.get('preferredDate'));
    }

    public class RequestParams {
        private Date preferredDate;
        private Visit visit;
        private Set<Id> members = new Set<Id>();


        private void setMembers(String members) {
            this.members = new Set<Id>();
            for (String member : members.split(';')) {
                this.members.add(member);
            }
        }

        private void setPreferredDate(String preferredDate) {
            this.preferredDate = Date.valueOf(preferredDate);
        }
    }

    public class Visit {
        private String name;
        private String reason;
        private String duration;
        private String instructions;
        private Set<Id> members = new Set<Id>();
        private String scheduledDateTime;
        private List<ExternalParticipant> externalParticipants;
    }

    public class ExternalParticipant {
        private String name;
        private String email;
    }
}