/*
@@ Jira Ticket:   SH-7764/8501
@@ Modified By:   Eashan Parlewar
@@ Modified Date: 5/4/2020
@@ Coments:       Updated getChatbuttonIdMap() funtion to get the livechatbuttonid for particular skill.
*/

global without sharing class VT_D1_SkillsBasedRouting {
//  @AuraEnabled
//  public static void routeByUser1() {
//      Case c = VT_D1_CaseByCurrentUser.getCase().caseObj;
//      if (c!=null) {
//          route(new List<String>{c.Id});
//      }
//  }
//  @AuraEnabled
//  public static void routeByUser() {
//          route(new List<String>{'5003D000002VCrcQAG'});
//  }
    @AuraEnabled(Cacheable=true)
    public static String getOrganizationId() {
        return UserInfo.getOrganizationId().mid(0, 15);
    }
    @AuraEnabled(Cacheable=true)
    public static String getLiveChatDeploymentId() {
        List<LiveChatDeployment> deployments = [SELECT Id, MasterLabel FROM LiveChatDeployment WHERE DeveloperName ='VTD1_CustomerCommunity' LIMIT 1];
        String deploymentId = (!deployments.isEmpty()? String.valueOf(deployments[0].Id).mid(0, 15):null);
        return deploymentId;
    }
    @AuraEnabled(Cacheable=true)
    public static String getLiveChatUrl(){
        List<VTD1_General_Settings__mdt> generalSettings = [SELECT VTD2_Live_Agent_URL__c FROM
            VTD1_General_Settings__mdt];
        return !generalSettings.isEmpty()? generalSettings[0].VTD2_Live_Agent_URL__c : null;
    }
    /*
    @@ Eashan Parlewar: Get chat button id based on community user's language preferences.
    @@ Modified Date:   5/4/2020
    */
    public static Map<string,Id> getChatbuttonIdMap(){
        String ButtonId;
        Map <string,Id> ButtonIdMap=new Map<string,Id>();
        User currentUser = [
            SELECT Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c,
            Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c,
            LanguageLocaleKey,VT_R5_Secondary_Preferred_Language__c,VT_R5_Tertiary_Preferred_Language__c    
            FROM User
            WHERE Id = :UserInfo.getUserId()
        ];
        system.debug('#### inside if study id '+currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c);
        system.debug('#### languages '+currentUser.LanguageLocaleKey+' '+currentUser.VT_R5_Secondary_Preferred_Language__c+' '+currentUser.VT_R5_Tertiary_Preferred_Language__c);
            map<id,LiveChatButtonSkill> SkillChatButtonWithChatButtonMap= new map<id,LiveChatButtonSkill>();
            map<id,skill> skillMap=new map<id,skill>([select id,DeveloperName from skill 
                                                      where DeveloperName=:currentUser.LanguageLocaleKey 
                                                      or DeveloperName=:currentUser.VT_R5_Secondary_Preferred_Language__c
                                                      or DeveloperName=:currentUser.VT_R5_Tertiary_Preferred_Language__c
                                                      or DeveloperName=:currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c]);
            system.debug('#### skillMap '+skillMap.values());
            String PrimarySkill;
            String SecondarySkill;
            String TertiarySkill;
            String StudySkill;
            for(Skill sk:skillMap.values()){
                if(sk.DeveloperName==currentUser.LanguageLocaleKey ){
                    system.debug('##### inside FirstlanguageSkill if');
                    PrimarySkill=sk.id;
                }
                if(sk.DeveloperName==currentUser.VT_R5_Secondary_Preferred_Language__c){
                    system.debug('##### inside SecondlanguageSkill if');
                    SecondarySkill=sk.id;
                }
                if(sk.DeveloperName==currentUser.VT_R5_Tertiary_Preferred_Language__c){
                    system.debug('##### inside ThirdLanguageSkill if');
                    TertiarySkill=sk.id;
                }   
                if(sk.DeveloperName==string.valueOf(currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c)){
                    system.debug('##### inside StudySkill if');
                    StudySkill=sk.id;
                }
            }
            system.debug('##### PrimarySkill '+PrimarySkill);
            system.debug('##### SecondarySkill '+SecondarySkill);
            system.debug('##### TertiarySkill '+TertiarySkill);
            system.debug('##### StudySkill '+StudySkill);
            set<id> skillIdSet=new set<id>();
            if(PrimarySkill!=null)
                skillIdSet.add(PrimarySkill);
            if(SecondarySkill!=null)
                skillIdSet.add(SecondarySkill);
            if(TertiarySkill!=null)
                skillIdSet.add(TertiarySkill);
            if(StudySkill!=null)
                skillIdSet.add(StudySkill);
            system.debug('##### skillIdSet '+skillIdSet);
            map<id,LiveChatButton>  ChatButtonMap=new map<id,LiveChatButton>([select id,skillId,skill.developername from LiveChatButton 
                                                                              where skillId in:skillIdSet]);
            system.debug('##### ChatButtonMap '+ChatButtonMap.values());
            map<id,LiveChatButtonSkill> SkillChatButtonMap= new map<id,LiveChatButtonSkill>([select id,buttonId,SkillId,skill.developername from LiveChatButtonSkill
                                                                                            where buttonId in:ChatButtonMap.keySet()
                                                                                            and skillId in:skillIdSet]);
            system.debug('##### SkillChatButtonMap '+SkillChatButtonMap.values());
            for(LiveChatButtonSkill LCBS : SkillChatButtonMap.values()){
                SkillChatButtonWithChatButtonMap.put(LCBS.buttonId,LCBS);
            }
            system.debug('##### SkillChatButtonWithChatButtonMap '+SkillChatButtonWithChatButtonMap);
            for(LiveChatButton LCB : ChatButtonMap.values()){
                if(LCB.skillId==StudySkill && SkillChatButtonWithChatButtonMap.containsKey(LCB.id) 
                  && SkillChatButtonWithChatButtonMap.get(LCB.id).SkillId==PrimarySkill){
                      ButtonIdMap.put('primary',String.valueOf(LCB.id).mid(0, 15));
                }else if(LCB.skillId==PrimarySkill && SkillChatButtonWithChatButtonMap.containsKey(LCB.id) 
                  && SkillChatButtonWithChatButtonMap.get(LCB.id).SkillId==StudySkill){
                      ButtonIdMap.put('primary',String.valueOf(LCB.id).mid(0, 15));
                }
                if(LCB.skillId==StudySkill && SkillChatButtonWithChatButtonMap.containsKey(LCB.id) 
                  && SkillChatButtonWithChatButtonMap.get(LCB.id).SkillId==SecondarySkill){
                     ButtonIdMap.put('secondary',String.valueOf(LCB.id).mid(0, 15));
                }else if(LCB.skillId==SecondarySkill && SkillChatButtonWithChatButtonMap.containsKey(LCB.id) 
                  && SkillChatButtonWithChatButtonMap.get(LCB.id).SkillId==StudySkill){
                     ButtonIdMap.put('secondary',String.valueOf(LCB.id).mid(0, 15));
                }
                if(LCB.skillId==StudySkill && SkillChatButtonWithChatButtonMap.containsKey(LCB.id) 
                  && SkillChatButtonWithChatButtonMap.get(LCB.id).SkillId==TertiarySkill){
                     ButtonIdMap.put('tertiary',String.valueOf(LCB.id).mid(0, 15));
                }else if(LCB.skillId==TertiarySkill && SkillChatButtonWithChatButtonMap.containsKey(LCB.id) 
                  && SkillChatButtonWithChatButtonMap.get(LCB.id).SkillId==StudySkill){
                     ButtonIdMap.put('tertiary',String.valueOf(LCB.id).mid(0, 15));
                }
    }
            return ButtonIdMap;
    }
    public static String getButtonIdByDevName(String devName) {
        LiveChatButton button = [SELECT Id FROM LiveChatButton WHERE DeveloperName = :devName LIMIT 1];
        return button.Id;
    }
/*
  @InvocableMethod
  public static void route(List<String> cases) {
     List<Case> caseObjects = [SELECT Id, VTD1_Study__r.Name FROM Case WHERE Id in :cases];
     for (Case caseObj : caseObjects) {
         // Add SkillsBased PendingServiceRouting
         PendingServiceRouting psrObj = new PendingServiceRouting(
             CapacityWeight = 1,
             IsReadyForRouting = FALSE,
             RoutingModel  = 'MostAvailable',
             RoutingPriority = 1,
             RoutingType = 'SkillsBased',
             ServiceChannelId = getChannelId('Case_Channel'),
             WorkItemId = caseObj.Id
             );
         insert psrObj;
         psrObj = [select id, IsReadyForRouting from PendingServiceRouting where id = : psrObj.id];
         if (caseObj.VTD1_Study__r.Name!=null) {
             // Now add SkillRequirement(s)
             SkillRequirement srObj = new SkillRequirement(
                 RelatedRecordId = psrObj.id,
                 SkillId = getSkillId(caseObj.VTD1_Study__r.Name)
                 //,SkillLevel = 5
                 );
             insert srObj;
             // Update PendingServiceRouting as IsReadyForRouting
             psrObj.IsReadyForRouting = TRUE;
             update psrObj;
         }
      }
      return;
  }
*/
//  public static String getChannelId(String channelName) {
//      ServiceChannel channel = [Select Id From ServiceChannel Where DeveloperName = :channelName];
//      return channel.Id;
//  }
//
//  public static String getSkillId(String skillName) {
//      Skill skill = [Select Id From Skill Where MasterLabel = :skillName];
//      return skill.Id;
//  }
//  public static String getButtonIdBySkillId(String skillId) {
//      LiveChatButton button = [Select Id From LiveChatButton Where SkillId = :skillId Limit 1];
//      return button.Id;
//  }
//  public static String getButtonIdByLabel(String label) {
//      LiveChatButton button = [Select Id From LiveChatButton Where MasterLabel = :label Limit 1];
//      return button.Id;
//  }
    public class ScUsersAndSkills{
        @AuraEnabled
        public  List<String> listOfScUsers {get;set;}
        @AuraEnabled
        public String skillName{get;set;}
    }
    @AuraEnabled(Cacheable=true)
    public static ScUsersAndSkills getScUsersAndSkills(Id chatButtonId){
        List<String> listOfScUsers;
        String skillName;
        ScUsersAndSkills wrapperResult;
        String buttonDeveloperName;
        List<String> buttonLabel;
        try{
            listOfScUsers = new List<String>();
            skillName = null;
            buttonDeveloperName = null;
            wrapperResult = new ScUsersAndSkills();
            buttonLabel = new List<String>();
            buttonDeveloperName = [select id, SkillId, DeveloperName from LiveChatButton where id = :chatButtonId].DeveloperName;
            buttonLabel = buttonDeveloperName.split('_', 2);
            for(Study_Team_Member__c member :[select User__r.FirstName, User__r.LastName from Study_Team_Member__c where RecordType.DeveloperName = 
                                              'SC' and Study__c = :buttonLabel[0]] ){
                                                  listOfScUsers.add(member.User__r.FirstName + ' ' + member.User__r.LastName);
                                              }
            wrapperResult.listOfScUsers = listOfScUsers;
            wrapperResult.skillName = VT_D1_TranslateHelper.getLabelValue('VTR5_'+buttonLabel[1], UserInfo.getLanguage());
        }catch(Exception e){
        }
        return wrapperResult;
    }
}