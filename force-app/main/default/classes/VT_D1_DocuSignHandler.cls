/**
 * Created by user on 16.07.2018.
 */

public with sharing class VT_D1_DocuSignHandler {
public static Map<Id,VTD1_Monitoring_Visit__c> mapOfMonitoringVisit;
static List<VT_D2_TNCatalogTasks.ParamsHolder> paramsList = new List <VT_D2_TNCatalogTasks.ParamsHolder>();

    public static void onDocuSignStatusUpdateAfter(List<dsfs__DocuSign_Status__c> docuSignStatuses, Map<Id, dsfs__DocuSign_Status__c> oldMap) {
        
        Set<Id> allDocIds = new Set<Id>();
        Set<Id> completedDocIds = new Set<Id>();
        Set<Id> monitoringVisistIds = new Set<Id>();

        for (dsfs__DocuSign_Status__c docuSignStatus : docuSignStatuses){
            allDocIds.add(docuSignStatus.VTD1_Document__c);
            if (docuSignStatus.VTD1_Monitoring_Visit__c != null && docuSignStatus.dsfs__Envelope_Status__c == 'Completed') {
                monitoringVisistIds.add(docuSignStatus.VTD1_Monitoring_Visit__c);
            }
            if(null != oldMap && !oldMap.isEmpty()) {
                if (docuSignStatus.dsfs__Envelope_Status__c == 'Completed' && oldMap.get(docuSignStatus.Id).dsfs__Envelope_Status__c != 'Completed') {
                    completedDocIds.add(docuSignStatus.VTD1_Document__c);
                }
            }
        }

//        if (! allDocIds.isEmpty()) { processAllDocIds(allDocIds); }
        if (! allDocIds.isEmpty()) { processAllDocIds(completedDocIds); }
        if (! monitoringVisistIds.isEmpty()) { processMonitoringVisitIds(monitoringVisistIds); }
        if (! completedDocIds.isEmpty()) { processCompletedDocIds(completedDocIds); }
    }

    private static void processCompletedDocIds(Set<Id> completedDocIds) {
        List<Case> casesToUpdate = new List<Case>();
        for (VTD1_Document__c item : [
            SELECT VTD1_Clinical_Study_Membership__c, VTD1_Site__c
            FROM VTD1_Document__c
            WHERE Id IN : completedDocIds
            AND RecordTypeId = :VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_NOTE_OF_TRANSFER
            AND VTD1_Document_Type__c = :VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_TARGET_NOTE_OF_TRANSFER
            AND VTD1_Clinical_Study_Membership__c != null
            AND VTD1_Site__c != null
        ]) {
            casesToUpdate.add(new Case(
                Id = item.VTD1_Clinical_Study_Membership__c,
                VTD1_Virtual_Site__c = item.VTD1_Site__c,
                VTR2_New_Virtual_Site__c = null,
                VTR2_Patient_Transferred_At_Least_Once__c = true
            ));
        }
        if (! casesToUpdate.isEmpty()) { update casesToUpdate; }
    }

    private static void processMonitoringVisitIds(Set<Id> monitoringVisistIds) {
        List <VTD1_Monitoring_Visit__c> monitoringVisits =
        [select Id, VTD2_MRR_Approved__c from VTD1_Monitoring_Visit__c
        where Id in : monitoringVisistIds and VTD1_Site_Visit_Type__c in ('Selection Visit', 'Initiation Visit')];

        if (monitoringVisits.isEmpty())
            return;

        for (VTD1_Monitoring_Visit__c monitoringVisit : monitoringVisits) {
            monitoringVisit.VTD2_MRR_Approved__c = true;
        }
        update monitoringVisits;
    }

    private static void processAllDocIds(Set<Id> docIds) {
        List<Id> caseIds= new List<Id>();

        Id rtIdCertification = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON;
        List<Id> certifiedDocs = new List<Id>();

        List <VTD1_Document__c> documents = [select Id,
            VTD1_Eligibility_Assessment_Status__c,
            RecordTypeId,
            VTD1_Clinical_Study_Membership__c, VTD2_PI_Decision_Comment__c,
            VTD2_TMA_Comments__c
        from VTD1_Document__c
        where Id in : docIds and
        RecordTypeId = :rtIdCertification];
        for (VTD1_Document__c document : documents) {
            if (document.RecordTypeId == rtIdCertification) {
                document.VTD1_Files_have_not_been_edited__c = true;
                certifiedDocs.add(document.Id);
                caseIds.add(document.VTD1_Clinical_Study_Membership__c);
            }
        }
        if (certifiedDocs.size() != 0 && caseIds.size() != 0) {
            List <VTD1_Document__c> documentsForCertification = [select Id,
                VTD1_Eligibility_Assessment_Status__c,
                RecordTypeId,
                VTD1_Clinical_Study_Membership__c,
                VTD1_Files_have_not_been_edited__c
            from VTD1_Document__c
            where VTD2_Certification__c IN :certifiedDocs
            and (RecordType.DeveloperName like '%Medical_Record%' or RecordType.DeveloperName like '%VisitDocument%')
            and VTD1_Clinical_Study_Membership__c in :caseIds];
            for (VTD1_Document__c doc : documentsForCertification) {
                if (!doc.VTD1_Files_have_not_been_edited__c) {
                    doc.VTD1_Files_have_not_been_edited__c = true;
                }
                documents.add(doc);
            }
        }
        
        update documents;
    }

    @AuraEnabled
    public static String sendDocWithDocuSign(Id docId) {
        String error;
        VTD1_Document__c document = [SELECT VTD1_Protocol_Amendment__c, VTD1_Regulatory_Document_Type__c, VTD1_Site__c, RecordTypeId, VTR4_IRB_Approved__c FROM VTD1_Document__c WHERE Id = :docId];
        if (VT_D1_DocumentCHandler.isPASignaturePage(document) && document.VTR4_IRB_Approved__c == false) {
            return 'Approval not received';
        }
        List<ContentDocumentLink> cdl = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId = :docId];
        if (cdl.size() == 0) {
            error = 'Sending not available';
        } else {
            try {
                new VT_D2_QAction_CertifyDocuments(docId).executeFuture();
            } catch (Exception e) {
                throw new AuraHandledException(e.getMessage() + '\n' +  e.getStackTraceString());
            }
        }
        return error;
    }

    @AuraEnabled
    public static String getDocuSignStatuses(Id docId) {
        try {
            List<dsfs__DocuSign_Status__c> statuses = [SELECT Id, Name FROM dsfs__DocuSign_Status__c WHERE VTD1_Document__c =: docId ORDER BY Id DESC LIMIT 1];
            return JSON.serialize(statuses);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
     
    /***********************************************************************
* @author              IQVIA  
* JIRA                 SH - 21445
* Param                docuSignNewMap : Trigger.Newmap newly updated record.
docuSignOldMap : Trigger.oldmap old records.
isInsert       : Is Insert operation.    
* @date                26-Nov-2020
* @group               As a part of improvement converting process builder to trigger.
* @description         Converted Process builder Monitoring Visit DocuSign Status on Object DocuSign Status to trigger
*                      impleted status change lifecycle of monitoring visit object
*/
    
    
    public static void monitorVisitStatusChanger(Map<Id,dsfs__DocuSign_Status__c> docuSignNewMap,
                                                 Map<Id,dsfs__DocuSign_Status__c> docuSignOldMap, 
                                                 Boolean isInsert )
    {       
        Savepoint sp ;   
        Map<Id,List<Task>> mapOfTask = new  Map<Id,List<Task>>();   
        Set<Id> setOfMonitorVisit = new Set<Id>();
        List<sObject> lstSObj = new List<sObject>();
        try{
            
            sp = Database.setSavepoint();

            for(dsfs__DocuSign_Status__c DocuSign : docuSignNewMap.values())
            {
                if(DocuSign.VTD1_Monitoring_Visit__c != null)
                {
                    setOfMonitorVisit.add(DocuSign.VTD1_Monitoring_Visit__c);
                }
            }

            mapOfMonitoringVisit = new Map<Id,VTD1_Monitoring_Visit__c>([
                SELECT Id, 
                VTD1_Remote_Monitoring_Visit_Report_Stat__c, 
                VTD1_Study_Site_Visit_Status__c,
                VTR2_Visit_CRA__c, 
                VTR5_SignMVReportTaskSent__c, 
                VTR2_Visit_CRA__r.User__c,
                VTR2_CRA_Full_Name__c, 
                VTR2_Visit_CRA__r.User__r.Email, 
                Follow_Up_Letter_Status__c,
                VTD1_Followup_Letter_Date__c, 
                VTD1_Virtual_Site__r.VTR3_Study_Geography__r.VTR3_Primary_RS__r.Email,
                VTR5_FollowUpLetterSubmitted__c
                FROM
                VTD1_Monitoring_Visit__c 
                WHERE 
                Id IN : setOfMonitorVisit
            ]);
            
            Map<Id,Task> mapOfVisitTask = new  Map<Id,Task>( [
                SELECT Id,
                Status, WhatId,
                VTD2_Task_Unique_Code__c
                FROM 
                Task 
                WHERE
                whatId IN : setOfMonitorVisit OR
                whatId IN : docuSignNewMap.keySet()
            ]);
            
            if(!mapOfVisitTask.isEmpty()){                                     
                for(Task objTask : mapOfVisitTask.values())
                {
                    if(!mapOfTask.containsKey(objTask.whatId))
                    { 
                        mapOfTask.put(objTask.whatId,new List<Task>());
                    }
                    mapOfTask.get(objTask.whatId).add(objTask);
                }
            }
            
            for(dsfs__DocuSign_Status__c DocuSign : docuSignNewMap.values())
            {
                if(mapOfMonitoringVisit.containsKey(DocuSign.VTD1_Monitoring_Visit__c))
                {                 
                    //MV Report Declined Visit CRA
                    if(((!isInsert && DocuSign.dsfs__Envelope_Status__c == 'Declined' &&  docuSignOldMap.get(DocuSign.Id).dsfs__Envelope_Status__c != 'Declined') ||
                        (isInsert && DocuSign.dsfs__Envelope_Status__c == 'Declined')) && mapOfMonitoringVisit.get(DocuSign.VTD1_Monitoring_Visit__c).VTD1_Remote_Monitoring_Visit_Report_Stat__c != 'Approved')
                    {
                        lstSObj.addAll(updateMonitoringVisitRejected(DocuSign, mapOfMonitoringVisit.get(DocuSign.VTD1_Monitoring_Visit__c)));
                    }
                    
                    //MV Report Approved Visit CRA
                    else if(((!isInsert && DocuSign.dsfs__Envelope_Status__c == 'Completed' && docuSignOldMap.get(DocuSign.Id).dsfs__Envelope_Status__c != 'Completed') ||
                             (isInsert && DocuSign.dsfs__Envelope_Status__c == 'Completed')) && mapOfMonitoringVisit.get(DocuSign.VTD1_Monitoring_Visit__c).VTD1_Remote_Monitoring_Visit_Report_Stat__c != 'Approved')
                    {
                        lstSObj.addAll(updateMonitoringVisitApproved(DocuSign, mapOfMonitoringVisit.get(DocuSign.VTD1_Monitoring_Visit__c)));
                    }
                    
                    //FUL: Approved Visit CRA
                    else if(DocuSign.dsfs__Envelope_Status__c == 'Completed' && mapOfMonitoringVisit.get(DocuSign.VTD1_Monitoring_Visit__c).VTD1_Remote_Monitoring_Visit_Report_Stat__c == 'Approved')
                    {
                        lstSObj.addAll(updateVisitFollowUpStatus(DocuSign, mapOfMonitoringVisit.get(DocuSign.VTD1_Monitoring_Visit__c), mapOfTask));
                    }
                    
                    //FUL: Submitted Visit CRA
                    else if(DocuSign.dsfs__Envelope_Status__c == 'Sent')
                    {
                        lstSObj.addAll(updateMonitoringVisitSubmitted(DocuSign, mapOfMonitoringVisit.get(DocuSign.VTD1_Monitoring_Visit__c), mapOfTask));
                    }
                    
                    //FUL : Rejected Visit CRA
                    else if(DocuSign.dsfs__Envelope_Status__c == 'Declined' &&  mapOfMonitoringVisit.get(DocuSign.VTD1_Monitoring_Visit__c).VTD1_Remote_Monitoring_Visit_Report_Stat__c == 'Approved')
                    {
                        lstSObj.addAll(updateFollowUpStatusRejected(DocuSign, mapOfMonitoringVisit.get(DocuSign.VTD1_Monitoring_Visit__c)));
                    }                
                }      
                
            }
            if(!lstSObj.isEmpty())
            {   
                update lstSObj;
            }
            
            if(!paramsList.isEmpty())
            {
                VT_D2_TNCatalogTasks.generateTask(paramsList);
            }            
        }
        catch(exception ex)
        {
            System.debug('Error=>'+ex);
            Database.rollback(sp);
            new ErrorLogUtility(ex, null, 'VT_D1_DocuSignHandler').saveErrorLog();
        }
    }
    
/***********************************************************************
* @author              IQVIA
* JIRA                 SH - 21445 
* Param                DocuSign : New Docusign record.
* mVisit               Monitoring visit record.
* @date                30-Nov-2020
* @group               As a part of improvement converted Process builder Monitoring Visit
*                      DocuSign Status on Object DocuSign Status to trigger
* @description         Sets the monitoring visit status to rejected and emails CRA for rejection.
*/
    
    private static List<VTD1_Monitoring_Visit__c> updateMonitoringVisitRejected(dsfs__DocuSign_Status__c DocuSign, 
                                                                                
                                                                                VTD1_Monitoring_Visit__c mVisit)
    {
        List<VTD1_Monitoring_Visit__c> lstMonitorVisit = new List<VTD1_Monitoring_Visit__c>(); 
        
        VT_D2_TNCatalogTasks.ParamsHolder params;  
        
        if(
            DocuSign.VTD1_Monitoring_Visit__c != null
            && DocuSign.dsfs__Envelope_Status__c == 'Declined' 
            && mVisit.VTD1_Remote_Monitoring_Visit_Report_Stat__c != 'Approved' 
            && mVisit.VTD1_Study_Site_Visit_Status__c != 'Visit Completed' 
            && mVisit.VTR2_Visit_CRA__c != null
        )
        {
            
            mVisit.VTD1_Remote_Monitoring_Visit_Report_Stat__c = 'Rejected';
            mVisit.VTR5_SignMVReportTaskSent__c = false;
            lstMonitorVisit.add(mVisit);
            
            //Task to CRA
            params = new VT_D2_TNCatalogTasks.ParamsHolder(String.valueOf(DocuSign.Id), '503');
            paramsList.add(params);
            //VT_D2_TNCatalogTasks.generateTask(paramsList);
            
            //Email to CRA
            /* To Do: Flow will be replaced by Apex in near upcoming release. 
            Flow configuration needs to be changed for sender email address.*/

            if(!Test.isRunningTest()){
                Map<String, Object> paramsMap = new Map<String, Object>();
                paramsMap.put('varCRA',mVisit.VTR2_Visit_CRA__r.User__c);
                paramsMap.put('varMonitVisitId', mVisit.Id);
                paramsMap.put('varCRAFirstName', mVisit.VTR2_CRA_Full_Name__c);
                Flow.Interview.Email_to_CRA_on_rejected_Monitoring_Report eventflow = new Flow.Interview.Email_to_CRA_on_rejected_Monitoring_Report(paramsMap);
                eventflow.start();
            }
            
            
        }
        return lstMonitorVisit;
    }
    
/***********************************************************************
* @author              IQVIA RDS
* JIRA                 SH - 21445 
* Param                DocuSign : New Docusign record.
* mVisit               Monitoring visit record. 
* @date                30-Nov-2020
* @group               As a part of improvement converted Process builder Monitoring Visit
*                      DocuSign Status on Object DocuSign Status to trigger
* @description         Sets the monitoring visit status to approved and emails CRA for status approved.
*/
    
    private static List<VTD1_Monitoring_Visit__c> updateMonitoringVisitApproved(dsfs__DocuSign_Status__c DocuSign,
                                                                                VTD1_Monitoring_Visit__c mVisit)
    {
        List<VTD1_Monitoring_Visit__c> lstMonitorVisit = new List<VTD1_Monitoring_Visit__c>();
        if(
            DocuSign.VTD1_Monitoring_Visit__c != null 
            && DocuSign.dsfs__Envelope_Status__c == 'Completed'               
            && mVisit.VTD1_Remote_Monitoring_Visit_Report_Stat__c != 'Approved' 
            && mVisit.VTD1_Study_Site_Visit_Status__c != 'Visit Completed' 
            && mVisit.VTR2_Visit_CRA__c != null)
        {
           
            // update visit
            mVisit.VTD1_Remote_Monitoring_Visit_Report_Stat__c = 'Approved';
            mVisit.VTR5_SignMVReportTaskSent__c = false;
            lstMonitorVisit.add(mVisit);
            
            //Email: Report Approved to CRA, PMA
            /* To Do: Flow will be replaced by Apex in near upcoming release. 
            Flow configuration needs to be changed for sender email address.*/

            if(!Test.isRunningTest()){
                Map<String, Object> paramsMap = new Map<String, Object>();
                paramsMap.put('monitoringVisitId',mVisit.Id);
                paramsMap.put('reportNumber', mVisit.Name);
                paramsMap.put('emailCRA', mVisit.VTR2_Visit_CRA__r.User__r.Email);
                
                Flow.Interview.Monitoring_Visit_Email_to_CRA eventflow = new Flow.Interview.Monitoring_Visit_Email_to_CRA(paramsMap);
                eventflow.start();
            }
        }
        
        return lstMonitorVisit;
    }
    
/***********************************************************************
* @author              IQVIA
* JIRA                 SH - 21445
* Param                DocuSign : New Docusign record.
* mVisit               Monitoring visit record.
* mapOfTask            map of tasks.
* @date                30-Nov-2020
* @group               As a part of improvement converted Process builder Monitoring Visit
*                      DocuSign Status on Object DocuSign Status to trigger
* @description         Sets the monitoring visit Follow up status to approved and updates task as completed
*/
    
    
    private static List<sObject> updateVisitFollowUpStatus(dsfs__DocuSign_Status__c DocuSign, 
                                                           VTD1_Monitoring_Visit__c mVisit, 
                                                           Map<Id,List<Task>> mapOfTask)
    {
        List<sObject> lstObj = new List<sObject>();
        
        
        if(DocuSign.VTD1_Monitoring_Visit__c != null &&
           DocuSign.dsfs__Envelope_Status__c == 'Completed' &&
           mVisit.VTD1_Remote_Monitoring_Visit_Report_Stat__c == 'Approved' &&
           mVisit.VTD1_Study_Site_Visit_Status__c == 'Visit Completed' &&
           mVisit.Follow_Up_Letter_Status__c != 'Approved' &&
           mVisit.VTR2_Visit_CRA__c != null)
        {    
            //Complete Follow Up Letters
            if(mapOfTask.containsKey(DocuSign.Id)){
                for(Task task1 : mapOfTask.get(DocuSign.Id))
                {
                    if( task1.Status != 'Completed' && task1.VTD2_Task_Unique_Code__c == '102')
                    {
                        task1.Status = 'Completed';
                        
                        lstObj.add(task1);
                        
                    }
                }
            }
            
            //Follow Up Letter: Approved            
            mVisit.Follow_Up_Letter_Status__c = 'Approved';
            mVisit.VTD1_Followup_Letter_Date__c =  system.today(); 
            lstObj.add(mVisit);
            
            if(mapOfTask.containsKey(mVisit.Id))
            {
                //Complete 103 Task add
                for(Task task1 : mapOfTask.get(mVisit.Id))
                {
                    
                    if( task1.Status  != 'Completed' && task1.VTD2_Task_Unique_Code__c == '103')
                    {
                        task1.Status = 'Completed';
                        
                        lstObj.add(task1);
                        
                    }
                }
            }
        }
        
        return lstObj;
    }
    
/***********************************************************************
* @author              IQVIA
* JIRA                 SH - 21445
* Param                DocuSign : New Docusign record.
* mVisit               Monitoring visit record. 
* mapOfTask            map of task
* @date                30-Nov-2020
* @group               As a part of improvement converted Process builder Monitoring Visit
*                      DocuSign Status on Object DocuSign Status to trigger
* @description         Sets the monitoring visit status to  completed and updates task completed
*/
    
    private static List<sObject> updateMonitoringVisitSubmitted(dsfs__DocuSign_Status__c DocuSign, 
                                                                VTD1_Monitoring_Visit__c mVisit, 
                                                                Map<Id,List<Task>> mapOfTask)
    {
        List<sObject> lstObj = new List<sObject>();
        
        if(DocuSign.VTD1_Monitoring_Visit__c !=null 
           && DocuSign.dsfs__Envelope_Status__c == 'Sent' 
           && mVisit.VTD1_Study_Site_Visit_Status__c == 'Visit Completed' 
           && mVisit.VTD1_Remote_Monitoring_Visit_Report_Stat__c == 'Approved' 
           && mVisit.Follow_Up_Letter_Status__c != 'Approved' 
           && mVisit.VTR2_Visit_CRA__c != null)
        {            
            //Folow Up Letter Status = Submitted
            mVisit.Follow_Up_Letter_Status__c = 'Submitted';
            lstObj.add(mVisit);
            
            
            if(mapOfTask.containsKey(mVisit.Id))
            {
                //Complete 103 Task add
                for(Task task1 : mapOfTask.get(mVisit.Id))
                {
                    if( task1.Status  != 'Completed' && (task1.VTD2_Task_Unique_Code__c == '103' || task1.VTD2_Task_Unique_Code__c == '102' ))
                    {
                        task1.Status = 'Completed';
                        
                        lstObj.add(task1);
                        
                    }
                }
            }
            
            //Regulatory Specialist Email Follow Up Letter
            /* To Do: Flow will be replaced by Apex in near upcoming release. 
            Flow configuration needs to be changed for sender email address.*/

            if(!Test.isRunningTest()){
                Map<String, Object> paramsMap = new Map<String, Object>();
                paramsMap.put('monitoringVisitId',mVisit.Id);
                paramsMap.put('regulatorySpecialistEmail', mVisit.VTD1_Virtual_Site__r.VTR3_Study_Geography__r.VTR3_Primary_RS__r.Email);
                //paramsMap.put('emailCRA',  mVisit.VTR2_Visit_CRA__r.User__r.Email);
                Flow.Interview.Monitoring_Visit_Follow_Up_Letter_Submission eventflow = new Flow.Interview.Monitoring_Visit_Follow_Up_Letter_Submission(paramsMap);
                eventflow.start();
            }
        }
        
        return lstObj;       
    }
    
/***********************************************************************
* @author              IQVIA
* JIRA                  SH - 21445
* Param                DocuSign : New Docusign record.
* mVisit                Monitoring visit record. 
* @date                30-Nov-2020
* @group               As a part of improvement converted Process builder Monitoring Visit
*                      DocuSign Status on Object DocuSign Status to trigger
* @description         Sets the monitoring visit follow up status  to rejected and generates task with 103 code
*/
    
    
    private static List<VTD1_Monitoring_Visit__c> updateFollowUpStatusRejected(dsfs__DocuSign_Status__c DocuSign, 
                                                                               
                                                                               VTD1_Monitoring_Visit__c mVisit)
    {
        List<VTD1_Monitoring_Visit__c> lstMonitorVisit = new List<VTD1_Monitoring_Visit__c>();
        VT_D2_TNCatalogTasks.ParamsHolder params; 
        
        if(DocuSign.VTD1_Monitoring_Visit__c != null 
           &&  DocuSign.dsfs__Envelope_Status__c == 'Declined' 
           &&  mVisit.VTD1_Remote_Monitoring_Visit_Report_Stat__c == 'Approved'
           &&  mVisit.VTD1_Study_Site_Visit_Status__c == 'Visit Completed'
           &&  mVisit.VTR2_Visit_CRA__c != null)
        {
            //Follow Up Letter: Rejected
            
            mVisit.Follow_Up_Letter_Status__c = 'Rejected';
            mVisit.VTR5_FollowUpLetterSubmitted__c = false;
            lstMonitorVisit.add(mVisit);
            
            //Task to CRA
            params = new VT_D2_TNCatalogTasks.ParamsHolder(String.valueOf(DocuSign.Id), '103');
            paramsList.add(params);
            //VT_D2_TNCatalogTasks.generateTask(paramsList);
            
        }
        
        return lstMonitorVisit;
    }
}