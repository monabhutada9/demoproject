public abstract without sharing class VT_D1_ProcessMentions {

    protected List<sObject> records = new List<sObject>();
    protected List<sObject> recsWithAtSign = new List<sObject>();
    protected List<VT_D1_Mention> parsedMentions = new List<VT_D1_Mention>();
    protected List<VT_D1_Mention> oooMentions = new List<VT_D1_Mention>();
    protected Map<Id,Set<Id>> gfcMentions = new Map<Id,Set<Id>>();

    public void doProcess(List<sObject> recs) {
        if (recs.isEmpty()) return;
        this.records = recs;
        getRecsWithAtSign();
        if (!this.recsWithAtSign.isEmpty()) {
            parse();
            VT_D1_MentionHelper.getOooUserIds(this.parsedMentions);
            for (VT_D1_Mention mention : this.parsedMentions) {
                if (mention.oooUserIds != null && !mention.oooUserIds.isEmpty()) {
                    this.oooMentions.add(mention);
                }
            }

            if (!this.oooMentions.isEmpty()) {
                new VT_D1_MentionBackupFinder(this.oooMentions).getBackupUsers();
                updateRecords();
            }

            //IQVIAVT-665 - Add PI to PCF Chat members when he is @mentioned in Case (PCF) chatter feed
            new VT_D1_PcfMembersFromMentionCreator().addMembersFromMentions(this.parsedMentions);
        }
    }

    public void doProcessGFCMentions(List<sObject> recs) {
        if (recs.isEmpty()) return;
        this.records = recs;
        getRecsWithAtSign();
        if (!this.recsWithAtSign.isEmpty()) {
            parse();
            Set<Id> parentIds = new Set<Id>();
            for (VT_D1_Mention mention : this.parsedMentions) {
                parentIds.add(mention.parentId);
            }
            Map<Id, Case> casesWithGFCRecordType = new Map<Id, Case>([
                    SELECT Id
                    FROM Case
                    WHERE RecordType.DeveloperName = 'VT_R2_General_Contact_Form'
                    AND Id IN:parentIds
            ]);
            Map<Id, Set<Id>> caseWithMentions = new Map<Id, Set<Id>>();
            for (VT_D1_Mention mention : this.parsedMentions) {
                Id caseId = mention.parentId;
                if (!casesWithGFCRecordType.containsKey(caseId)) {
                    continue;
                }
                for (Id userId : mention.getUserIds()) {
                    if (!caseWithMentions.containsKey(caseId)) {
                        caseWithMentions.put(caseId, new Set<Id>{ userId });
                    } else {
                        Set<Id> usersId = caseWithMentions.get(caseId);
                        usersId.add(userId);
                        caseWithMentions.put(caseId, usersId);
                    }
                }
            }
            System.debug('caseWithMentions ' + caseWithMentions);
            VT_D1_MentionHelper.addSharingToCaseGFC(caseWithMentions);
        }
    }

    private abstract void getRecsWithAtSign();

    private void parse() {
        this.parsedMentions = (List<VT_D1_Mention>)JSON.deserialize(
            JSON.serialize(this.recsWithAtSign), List<VT_D1_Mention>.class
        );
        System.debug('parsedMentions ' + this.parsedMentions);
    }

    private abstract void updateRecords();

}