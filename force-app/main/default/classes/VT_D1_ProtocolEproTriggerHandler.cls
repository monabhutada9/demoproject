public without sharing class VT_D1_ProtocolEproTriggerHandler {

    private List<VTD1_Protocol_ePRO__c> satisfactionSurveys = new List<VTD1_Protocol_ePRO__c>();

    public void onBeforeUpdate(List<VTD1_Protocol_ePRO__c> recs, Map<Id, VTD1_Protocol_ePRO__c> oldMap) {
        validateUpdates(recs, oldMap);
    }

    public void onAfterInsert(List<VTD1_Protocol_ePRO__c> recs) {
        getSatisfactionSurveys(recs);

        if (! this.satisfactionSurveys.isEmpty()) {
            validateNewSatisfactionSurveys();
            createDefaultPscQuestions();
        }
    }

    private void validateUpdates(List<VTD1_Protocol_ePRO__c> recs, Map<Id, VTD1_Protocol_ePRO__c> oldMap) {
        List<VTD1_Protocol_ePRO__c> recsToValidate = new List<VTD1_Protocol_ePRO__c>();
        List<String> updateableFields = getUpdateableFields();
        for (VTD1_Protocol_ePRO__c item : recs) {
            for (String field : updateableFields) {
                if (item.get(field) != oldMap.get(item.Id).get(field)) {
                    recsToValidate.add(item);
                    break;
                }
            }
        }

        if (! recsToValidate.isEmpty()) {
            Set<Id> inProgressIds = VT_D1_Protocol_ePRO_Update_Verifier.getInprogressIds(recsToValidate);

            for (VTD1_Protocol_ePRO__c item : recsToValidate) {
                if (inProgressIds.contains(item.Id)) {
                    item.addError(VT_D1_Protocol_ePRO_Update_Verifier.ERROR_MESSAGE);
                }
            }
        }
    }

    private List<String> getUpdateableFields() {
        List<String> fields = new List<String>();

        for (Schema.SObjectField field : VTD1_Protocol_ePRO__c.getSObjectType().getDescribe().fields.getMap().values()) {
            DescribeFieldResult fieldDescribe = field.getDescribe();
            if (fieldDescribe.isUpdateable() && fieldDescribe.getName() != 'VTD1_isArchivedVersion__c') {
                fields.add(fieldDescribe.getName());
            }
        }

        return fields;
    }

    private void getSatisfactionSurveys(List<VTD1_Protocol_ePRO__c> recs) {
        Id recTypeId = Schema.SObjectType.VTD1_Protocol_ePRO__c.getRecordTypeInfosByDeveloperName().get('SatisfactionSurvey').getRecordTypeId();

        for (VTD1_Protocol_ePRO__c item : recs) {
            if (item.RecordTypeId == recTypeId) { this.satisfactionSurveys.add(item); }
        }
    }

    private void validateNewSatisfactionSurveys() {
        List<Id> studyIds = new List<Id>();
        for (VTD1_Protocol_ePRO__c item : this.satisfactionSurveys) {
            studyIds.add(item.VTD1_Study__c);
        }

        Map<String, Integer> countMap = new Map<String, Integer>();
        for (AggregateResult item : [
            SELECT COUNT(Id) cnt, VTD1_Study__c, VTD1_Type__c
            FROM VTD1_Protocol_ePRO__c
            WHERE VTD1_Study__c IN :studyIds
            AND RecordType.DeveloperName = 'SatisfactionSurvey'
            GROUP BY VTD1_Study__c, VTD1_Type__c
        ]) {
            countMap.put((String)item.get('VTD1_Study__c') + (String)item.get('VTD1_Type__c'), (Integer)item.get('cnt'));
        }

        for (VTD1_Protocol_ePRO__c item : this.satisfactionSurveys) {
            if (countMap.get('' + item.VTD1_Study__c + item.VTD1_Type__c) > 1) {
                item.addError('Only one Satisfaction Survey Protocol ePRO of type ' + item.VTD1_Type__c + ' can be created for each study.');
            }
        }
    }

    private void createDefaultPscQuestions() {
        List<VTD1_Protocol_ePro_Question__c> defaultQuestions = new List<VTD1_Protocol_ePro_Question__c>();

        for (VTD1_Protocol_ePRO__c item : this.satisfactionSurveys) {
            if (item.VTD1_Type__c == 'PSC') {
                defaultQuestions.add(new VTD1_Protocol_ePro_Question__c(
                    VTD1_Protocol_ePRO__c = item.Id,
                    VTR4_Number__c = '1',
                    VTD1_Default_PSC_Question__c = true,
                    VTD1_Type__c = 'Drop Down',
                    VTD1_Options__c = 'Yes;No',
                    VTR2_QuestionContentLong__c = VT_R4_ConstantsHelper_Protocol_ePRO.PSC_EPRO_DEFAULT_QUESTION
                ));
            }
        }

        if (! defaultQuestions.isEmpty()) { insert defaultQuestions; }
    }
}