/**
 * Created by Oleh Berehovskyi on 26-Dec-19.
 */
public with sharing class VT_R4_TranslationAssociationHandler extends Handler {
    private Map<String, Set<Id>> strategyToRecordsIds = new Map<String, Set<Id>>();
    private static final String CLASSNAME = VT_R4_TranslationAssociationHandler.class.getName();
    public override void onBeforeInsert(TriggerContext context) {
//        System.debug(LoggingLevel.INFO, 'onBeforeInsert context: ' + JSON.serialize(context));
        final List<VTD1_Translation__c> filteredRecords =
                this.filterByNonPopulatedField(context.newList, VTD1_Translation__c.VTR4_User__c);
        this.updateStudyLookup(filteredRecords);
    }
    public override void onBeforeUpdate(TriggerContext context) {
//        System.debug(LoggingLevel.INFO, 'onBeforeUpdate context: ' + JSON.serialize(context));
        final List<VTD1_Translation__c> filteredRecords = this.filterByChangedRecordId(
                context.newList,
                context.oldList
        );
        this.updateStudyLookup(filteredRecords);
    }
    private List<VTD1_Translation__c> filterByNonPopulatedField(
            List<VTD1_Translation__c> translations,
            SObjectField field
    ) {
        final List<VTD1_Translation__c> filteredRecords = new List<VTD1_Translation__c>();
        for (VTD1_Translation__c translation : translations) {
            if (!translation.isSet(field)) {
                filteredRecords.add(translation);
            }
        }
        return filteredRecords;
    }
    private List<VTD1_Translation__c> filterByChangedRecordId(
            List<VTD1_Translation__c> newRecords,
            List<VTD1_Translation__c> oldRecords
    ) {
        final Map<Id, VTD1_Translation__c> oldRecs = new Map<Id, VTD1_Translation__c>(oldRecords);
        final Map<Id, VTD1_Translation__c> newRecs = new Map<Id, VTD1_Translation__c>(newRecords);
        final List<VTD1_Translation__c> translations = new List<VTD1_Translation__c>();
        for (VTD1_Translation__c newRecord : newRecs.values()) {
            final Boolean isRecordIdChanged = newRecord.VTD1_Record_Id__c
                    != oldRecs.get(newRecord.Id).VTD1_Record_Id__c;
            if (isRecordIdChanged) {
                translations.add(newRecord);
            }
        }
        return translations;
    }
    // method is public to use it in script ran on Production to set study lookup for old translation records
    public void updateStudyLookup(List<VTD1_Translation__c> translations) {
        this.allocateRecords(translations);
//        System.debug(LoggingLevel.DEBUG, this.strategyToRecordsIds);
        final Map<Id, Id> associatedWithStudies = this.getAssociatedWithStudies();
        this.setStudyLookup(translations, associatedWithStudies);
    }
    private void allocateRecords(List<VTD1_Translation__c> translations) {
        for (VTD1_Translation__c translation : translations) {
            final String strategyName = new StrategyFactory().getSuitableStrategyName(translation);
            if (strategyName == null || translation == null) {
                continue;
            }
            Set<Id> existingRecordsIds;
            if (this.strategyToRecordsIds.keySet().contains(strategyName)) {
                existingRecordsIds = this.strategyToRecordsIds.get(strategyName);
                existingRecordsIds.add(translation.VTD1_Record_Id__c);
            } else {
                existingRecordsIds = new Set<Id>{
                        translation.VTD1_Record_Id__c
                };
            }
            this.strategyToRecordsIds.put(strategyName, existingRecordsIds);
        }
    }
    private Map<Id, Id> getAssociatedWithStudies() {
        final Map<Id, Id> recordIdToStudyId = new Map<Id, Id>();
        for (String strategyName : this.strategyToRecordsIds.keySet()) {
            final Type strategyType = Type.forName(CLASSNAME + '.' + strategyName);
            final ISearchStrategy strategy = (ISearchStrategy) strategyType.newInstance();
            recordIdToStudyId.putAll(
                    (Map<Id, Id>) strategy.findStudies(this.strategyToRecordsIds.get(strategyName))
            );
        }
        return recordIdToStudyId;
    }
    private void setStudyLookup(List<VTD1_Translation__c> translations, Map<Id, Id> recordIdToStudyId) {
        for (VTD1_Translation__c translation : translations) {
            if (translation.isSet(VTD1_Translation__c.VTR4_User__c)) {
                translation.VTR4_Study__c = null;
            } else {
                translation.VTR4_Study__c = recordIdToStudyId.get(translation.VTD1_Record_Id__c);
            }
        }
    }
    private static String getSObjectTypeNameById(SObject record) {
        final Id recordId = Id.valueOf(((VTD1_Translation__c) record).VTD1_Record_Id__c);
        return String.valueOf(recordId.getSobjectType());
    }
    private static Map<Id, Id> getRecordIdToStudyIdMap(List<SObject> records, String studyLookup) {
        final Map<Id, Id> recordIdToStudyId = new Map<Id, Id>();
        for (SObject record : records) {
            String studyId = (Id) getFieldRefValue(record, studyLookup);
            recordIdToStudyId.put(record.Id, studyId);
        }
        return recordIdToStudyId;
    }
    private static Object getFieldRefValue(SObject record, String fieldRef) {
        Object fieldRefValue;
        if (isCrossObj(fieldRef)) {
            fieldRefValue = getCrossObjFieldRefValue(record, fieldRef);
        } else {
            fieldRefValue = record.get(fieldRef.split('\\.')[0]);
        }
        return fieldRefValue;
    }
    private static Boolean isCrossObj(String fieldRef) {
        return fieldRef.split('\\.').size() > 1;
    }
    private static Object getCrossObjFieldRefValue(SObject record, String fieldRef) {
        SObject obj = record;
        List<String> fieldRefChain = fieldRef.split('\\.');
        for (Integer i = 0; i < fieldRefChain.size() - 1; i++) {
            obj = obj.getSObject(fieldRefChain[i]);
        }
        return obj.get(fieldRefChain[fieldRefChain.size() - 1]);
    }
    private interface ISearchStrategy {
        Object findStudies(Object searchParams);
    }
    public with sharing class GenericStrategy implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object params) {
            final QueryParams queryParams = (QueryParams) params;
            final QueryBuilder qb = new QueryBuilder(queryParams.sObjectType)
                    .namedQueryRegister(queryParams.stubQuery)
                    .addConditions()
                    .add(new QueryBuilder.InCondition('Id').inCollection((Set<Id>) queryParams.recordsIds))
                    .endConditions();
            if (String.isNotBlank(queryParams.crossObjLookup)) {
                qb.addField(queryParams.crossObjLookup);
            } else {
                qb.addField(queryParams.lookup);
            }
            return getRecordIdToStudyIdMap(qb.toList(),
                    String.isBlank(queryParams.crossObjLookup) ?
                            queryParams.lookup.getDescribe().getName() :
                            queryParams.crossObjLookup
            );
        }
    }
    public with sharing class CaseStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            Case.SObjectType,
                            Case.VTD1_Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class StudyTeamMemberStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            Study_Team_Member__c.SObjectType,
                            Study_Team_Member__c.Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class StudyGeographyStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD2_Study_Geography__c.SObjectType,
                            VTD2_Study_Geography__c.VTD2_Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class ProtocolAmendmentStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_Protocol_Amendment__c.SObjectType,
                            VTD1_Protocol_Amendment__c.VTD1_Study_ID__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class ProtocolDeviationStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_Protocol_Deviation__c.SObjectType,
                            VTD1_Protocol_Deviation__c.VTD1_StudyId__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class ProtocolVisitStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_ProtocolVisit__c.SObjectType,
                            VTD1_ProtocolVisit__c.VTD1_Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class VisitStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            HealthCloudGA__CarePlanTemplateTask__c.SObjectType,
                            HealthCloudGA__CarePlanTemplateTask__c.Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class StudyNotificationStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTR3_Study_Notification__c.SObjectType,
                            VTR3_Study_Notification__c.VTR3_Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class PrimaryObjectiveStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            Objective__c.SObjectType,
                            Objective__c.Study_for_Primary_Objective__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class RegulatoryBinderStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_Regulatory_Binder__c.SObjectType,
                            VTD1_Regulatory_Binder__c.VTD1_Care_Plan_Template__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class LibraryStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_Document__c.SObjectType,
                            VTD1_Document__c.VTD1_Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class ProtocolPaymentStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_Protocol_Payment__c.SObjectType,
                            VTD1_Protocol_Payment__c.VTD1_Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class VirtualSiteStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            Virtual_Site__c.SObjectType,
                            Virtual_Site__c.VTD1_Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class PreScreeningCriteriaStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTR2_Pre_Screening_Criteria__c.SObjectType,
                            VTR2_Pre_Screening_Criteria__c.VTR2_Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class ThirdPartyVendorStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            X3rd_Party_Vendors__c.SObjectType,
                            X3rd_Party_Vendors__c.Care_Plan_Template__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class MonitoringVisitStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_Monitoring_Visit__c.SObjectType,
                            VTD1_Monitoring_Visit__c.VTD1_Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class ProtocolDeliveryStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_Protocol_Delivery__c.SObjectType,
                            VTD1_Protocol_Delivery__c.Care_Plan_Template__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class ProtocolKitStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_Protocol_Kit__c.SObjectType,
                            VTD1_Protocol_Kit__c.VTD1_Care_Plan_Template__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class ProtocolDeviceStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            HealthCloudGA__EhrDevice__c.SObjectType,
                            HealthCloudGA__EhrDevice__c.VTD1_Care_Plan_Template__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class ProtocolAccessoryStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_Protocol_Accessories__c.SObjectType,
                            VTD1_Protocol_Accessories__c.VTD1_Care_Plan_Template__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class LabKitContentStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_Lab_Kit_Contents__c.SObjectType,
                            VTD1_Lab_Kit_Contents__c.VTD1_Care_Plan_Template__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class ContentItemStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_Protocol_LKC__c.SObjectType,
                            VTD1_Protocol_LKC__c.VTD1_Care_Plan_Template__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class ProtocolLabSampleStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_Protocol_Lab_Sample__c.SObjectType,
                            VTD1_Protocol_Lab_Sample__c.VTD1_Care_Plan_Template__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class ProtocolEDiaryStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_Protocol_ePRO__c.SObjectType,
                            VTD1_Protocol_ePRO__c.VTD1_Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class StudyMilestoneStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_Study_Milestone__c.SObjectType,
                            VTD1_Study_Milestone__c.VTD1_Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class SubjectNumeratorStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_SubjectIdNumerator__c.SObjectType,
                            VTD1_SubjectIdNumerator__c.VTD1_Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class StudyStratificationStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_Study_Stratification__c.SObjectType,
                            VTD1_Study_Stratification__c.VTD1_Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class TMATaskStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD2_TMA_Task__c.SObjectType,
                            VTD2_TMA_Task__c.VTD2_Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class TreatmentArmConfigStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTR2_Treatment_Arm_Sharing_Configuration__c.SObjectType,
                            VTR2_Treatment_Arm_Sharing_Configuration__c.VTR2_Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class KnowledgeStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            Knowledge__kav.SObjectType,
                            Knowledge__kav.VTD2_Study__c,
                            this.getFullName()
                    )
            );
        }
    }
    /*public with sharing class TemplateContainerStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTR4_TemplateContainer__c.SObjectType,
                            VTR4_TemplateContainer__c.VTR4_Study__c,
                            this.getFullName()
                    )
            );
        }
    }*/
    public with sharing class ProtocolEProQuestionStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTD1_Protocol_ePro_Question__c.SObjectType,
                            'VTD1_Protocol_ePRO__r.VTD1_Study__c',
                            this.getFullName()
                    )
            );
        }
    }
    public with sharing class ProtocolEInstructionStrategy extends Reflective implements ISearchStrategy {
        public Map<Id, Id> findStudies(Object recordsIds) {
            return new GenericStrategy().findStudies(
                    new QueryParams(
                            (Set<Id>) recordsIds,
                            VTR2_eDiaryInstructions__c.SObjectType,
                            'VTR2_Protocol_eDiary__r.VTD1_Study__c',
                            this.getFullName()
                    )
            );
        }
    }
    private class StrategyFactory {
        private final Map<String, String> strategies = new Map<String, String>{
                'Case' => 'CaseStrategy',
                'Study_Team_Member__c' => 'StudyTeamMemberStrategy',
                'VTD2_Study_Geography__c' => 'StudyGeographyStrategy',
                'VTD1_Protocol_Amendment__c' => 'ProtocolAmendmentStrategy',
                'VTD1_Protocol_Deviation__c' => 'ProtocolDeviationStrategy',
                'VTD1_ProtocolVisit__c' => 'ProtocolVisitStrategy',
                'HealthCloudGA__CarePlanTemplateTask__c' => 'VisitStrategy',
                'VTR3_Study_Notification__c' => 'StudyNotificationStrategy',
                'Objective__c' => 'PrimaryObjectiveStrategy',
                'VTD1_Regulatory_Binder__c' => 'RegulatoryBinderStrategy',
                'VTD1_Document__c' => 'LibraryStrategy',
                'VTD1_Protocol_Payment__c' => 'ProtocolPaymentStrategy',
                'Virtual_Site__c' => 'VirtualSiteStrategy',
                'VTR2_Pre_Screening_Criteria__c' => 'PreScreeningCriteriaStrategy',
                'X3rd_Party_Vendors__c' => 'ThirdPartyVendorStrategy',
                'VTD1_Monitoring_Visit__c' => 'MonitoringVisitStrategy',
                'VTD1_Protocol_Delivery__c' => 'ProtocolDeliveryStrategy',
                'VTD1_Protocol_Kit__c' => 'ProtocolKitStrategy',
                'HealthCloudGA__EhrDevice__c' => 'ProtocolDeviceStrategy',
                'VTD1_Protocol_Accessories__c' => 'ProtocolAccessoryStrategy',
                'VTD1_Lab_Kit_Contents__c' => 'LabKitContentStrategy',
                'VTD1_Protocol_LKC__c' => 'ContentItemStrategy',
                'VTD1_Protocol_Lab_Sample__c' => 'ProtocolLabSampleStrategy',
                'VTD1_Protocol_ePRO__c' => 'ProtocolEDiaryStrategy',
                'VTD1_Study_Milestone__c' => 'StudyMilestoneStrategy',
                'VTD1_SubjectIdNumerator__c' => 'SubjectNumeratorStrategy',
                'VTD1_Study_Stratification__c' => 'StudyStratificationStrategy',
                'VTD2_TMA_Task__c' => 'TMATaskStrategy',
                'VTR2_Treatment_Arm_Sharing_Configuration__c' => 'TreatmentArmConfigStrategy',
                'Knowledge__kav' => 'KnowledgeStrategy',
//                'VTR4_TemplateContainer__c' => 'TemplateContainerStrategy',
                'VTD1_Protocol_ePro_Question__c' => 'ProtocolEProQuestionStrategy',
                'VTR2_eDiaryInstructions__c' => 'ProtocolEInstructionStrategy'
        };
        private String getSuitableStrategyName(SObject record) {
            final String sObjectTypeName = VT_R4_TranslationAssociationHandler.getSObjectTypeNameById(record);
            final String strategyName = this.strategies.get(sObjectTypeName);
            if (String.isEmpty(strategyName)) {
                System.debug(LoggingLevel.WARN, 'No search strategy was found for ' + sObjectTypeName
                        + '. Please check your StrategyFactory.strategies map.'
                );
            }
            return strategyName;
        }
    }
    private virtual class QueryParams {
        public Object recordsIds;
        public SObjectType sObjectType;
        public SObjectField lookup;
        public String crossObjLookup;
        public String stubQuery;
        QueryParams(SObjectType sObjectType, SObjectField lookup) {
            this.sObjectType = sObjectType;
            this.lookup = lookup;
        }
        QueryParams(Object recordsIds, SObjectType sObjectType, SObjectField lookup, String stubQuery) {
            this(sObjectType, lookup);
            this.recordsIds = recordsIds;
            this.stubQuery = stubQuery;
        }
        QueryParams(SObjectType sObjectType, String crossObjLookup) {
            this.sObjectType = sObjectType;
            this.crossObjLookup = crossObjLookup;
        }
        QueryParams(Object recordsIds, SObjectType sObjectType, String crossObjLookup, String stubQuery) {
            this(sObjectType, crossObjLookup);
            this.recordsIds = recordsIds;
            this.stubQuery = stubQuery;
        }
    }
    private abstract class Reflective {
        private virtual String getName() {
            return String.valueOf(this).substringBefore(':');
        }
        private virtual String getFullName() {
            return VT_R4_TranslationAssociationHandler.CLASSNAME + '.' + getName();
        }
    }
}