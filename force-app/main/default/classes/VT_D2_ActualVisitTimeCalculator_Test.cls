/**
* @author: Carl Judge
* @date: 22-Jan-19
* @description: Test class for VT_D2_ActualVisitTimeCalculator
**/

@IsTest
private class VT_D2_ActualVisitTimeCalculator_Test {

    static List<String> profileNames = new List<String>{
        VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME
    };

    @testSetup
    private static void setupMethod() {
        Map<String, User> testUsers = new Map<String, User>();

        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();
        for (String profileName : profileNames) {
            testUsers.put(profileName, VT_D1_TestUtils.createUserByProfile(profileName, profileName.replace(' ','') + 'calcTestUser'));
        }
        VT_D1_TestUtils.persistUsers();

        Case pcf = (Case) new DomainObjects.Case_t()
                .setRecordTypeId(VT_R4_ConstantsHelper_RecordTypes.RECORD_TYPE_ID_CASE_PCF)
                .persist();

        VTD1_Actual_Visit__c visit = new VTD1_Actual_Visit__c(
                VTR5_PatientCaseId__c = pcf.Id
        );
        insert visit;

        List<Visit_Member__c> members = new List<Visit_Member__c>();
        for (String profileName : profileNames) {
            if (testUsers.containsKey(profileName)) {
                members.add(new Visit_Member__c(
                    VTD1_Actual_Visit__c = visit.Id,
                    VTD1_Participant_User__c = testUsers.get(profileName).Id
                ));
            }
        }
        insert members;
    }

    @isTest
    private static void doTest() {
        VTD1_Actual_Visit__c visit = [SELECT Id FROM VTD1_Actual_Visit__c];
        visit.VTD1_Scheduled_Date_Time__c = Datetime.now();
        update visit;
    }
}