/**
 * @author: Alexander Komarov
 * @date: 03.12.2020
 * @description:
 */

public class VT_R5_MobileNewsFeed extends VT_R5_MobileRestResponse implements VT_R5_MobileLibrary.Routable {


    public List<String> getMapping() {
        return new List<String>{
                '/mobile/{version}/newsfeed',
                '/mobile/{version}/newsfeed/{id}'
        };
    }

    public VT_R5_MobileRestResponse versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        String requestId = parameters.get('id');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        return get_v1(requestId);
                    }
                }
            }
        }

        return null;
    }


    private VT_R5_MobileRestResponse get_v1(String requestId) {
        VT_R5_MobileNewsFeedService newsFeedService = new VT_R5_MobileNewsFeedService();
        if (requestId == null) {
            NewsFeedListWrapper wrapper = parseParams(this.request);
            Case patientCase = VT_R5_PatientQueryService.getCaseByCurrentUser();
            newsFeedService.setListQueryParams(wrapper.queryLimit, wrapper.queryOffset, UserInfo.getLanguage(), patientCase.VTD1_Study__c, patientCase.Status);
            wrapper.items = newsFeedService.getNewsFeedList();
            wrapper.totalNews = newsFeedService.countArticles();
            this.buildResponse(wrapper);
        } else {
            this.buildResponse(newsFeedService.getNewsFeedItem(requestId));
        }
        return this;
    }


    public void initService() {
    }

    public Map<String, String> getMinimumVersions() {
        return null;
    }

    private static NewsFeedListWrapper parseParams(RestRequest request) {
        NewsFeedListWrapper wrapper = new NewsFeedListWrapper();
        if (request.params.containsKey('offset')) {
            wrapper.queryOffset = Integer.valueOf(request.params.get('offset'));
        }
        if (request.params.containsKey('limit')) {
            wrapper.queryLimit = Integer.valueOf(request.params.get('limit'));
        }
        return wrapper;
    }

    private class NewsFeedListWrapper {
        List<VT_R5_MobileNewsFeedService.NewsFeedItem> items;
        Integer queryOffset = 0;//DEFAULT VALUE
        Integer queryLimit = 40;//DEFAULT VALUE
        Integer totalNews;
    }
}