/**
 * Created by Dmitry Yartsev on 14.06.2019.
 */
@IsTest
private class VT_R2_StudySiteTeamMemberHandlerTest {

    @TestSetup
    static void testSetup() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        User SCRUser = VT_D1_TestUtils.createUserByProfile('Site Coordinator', 'SCRTestUser');
        User PiUser = VT_D1_TestUtils.createUserByProfile('Primary Investigator', 'PiTestUser');
        // Jira Ref: SH-17543 Akanksha Singh
        // Created user with CRA profile
        User CRAUser = VT_D1_TestUtils.createUserByProfile('CRA', 'CRATestUser');       
        VT_D1_TestUtils.persistUsers();
        
        // Jira Ref: SH-17543 Akanksha Singh
        // Created Virtual Site
        Virtual_Site__c virtualSite = new Virtual_Site__c(Name = 'TestSite', VTD1_Study__c= study.Id,VTD1_Study_Site_Number__c = '1');
        insert virtualSite;
        //End of Jira Ref: SH-17543 Akanksha Singh
        
        Test.stopTest();
        // Jira Ref: SH-17543 Akanksha Singh
        // Created list of STM to reduce multiple insert operation
        List<Study_Team_Member__c> stmList = new List<Study_Team_Member__c>();
        Study_Team_Member__c stmPI = new Study_Team_Member__c(User__c = PiUser.Id, Study__c = study.Id);
        stmList.add(stmPI);
        Study_Team_Member__c stmSCR = new Study_Team_Member__c(User__c = SCRUser.Id, Study__c = study.Id);
        stmList.add(stmSCR);
        // Jira Ref: SH-17543 Akanksha Singh to add CRA STM on study
        Study_Team_Member__c stmCRA = new Study_Team_Member__c(User__c = CRAUser.Id, Study__c = study.Id);
        stmList.add(stmCRA);
        insert stmList;
        
        // Jira Ref: SH-17543 Akanksha Singh
        // Commented unrequired insert statement
        //insert stmSCR;
        //insert stmPI;
        List<Study_Site_Team_Member__c> sstmList = new List<Study_Site_Team_Member__c>();
        sstmList.add(new Study_Site_Team_Member__c(VTR2_Associated_PI__c = stmPI.Id, VTR2_Associated_SCr__c = stmSCR.Id, VTR2_SCr_type__c = 'Primary'));
        sstmList.add(new Study_Site_Team_Member__c(VTR2_Associated_PI__c = stmPI.Id, VTR2_Associated_SCr__c = stmSCR.Id, VTR2_SCr_type__c = 'Backup'));
        // Jira Ref: SH-17543 Akanksha Singh
        // Added CRA SSTM on site
        sstmList.add(new Study_Site_Team_Member__c(VTR5_Associated_CRA__c = stmCRA.Id,VTD1_SiteID__c = virtualSite.Id));
        //End of Jira Ref: SH-17543 Akanksha Singh
        insert sstmList;
        
    }

    @IsTest static void testValidateSCRsCount() {
        Study_Team_Member__c stmPI = [SELECT Id FROM Study_Team_Member__c WHERE User__r.FirstName = 'PiTestUser' LIMIT 1];
        Study_Team_Member__c stmSCR = [SELECT Id FROM Study_Team_Member__c WHERE User__r.FirstName = 'SCRTestUser' LIMIT 1];
        List<Study_Site_Team_Member__c> sstmList = new List<Study_Site_Team_Member__c>();
        sstmList.add(new Study_Site_Team_Member__c(VTR2_Associated_PI__c = stmPI.Id, VTR2_Associated_SCr__c = stmSCR.Id, VTR2_SCr_type__c = 'Primary'));
        sstmList.add(new Study_Site_Team_Member__c(VTR2_Associated_PI__c = stmPI.Id, VTR2_Associated_SCr__c = stmSCR.Id, VTR2_SCr_type__c = 'Backup'));
        Test.startTest();
        try {
            insert sstmList;
        } catch (DmlException e) {
            System.assertEquals(null, sstmList[0].Id);
            System.assertEquals(null, sstmList[1].Id);
        }
        Test.stopTest();
    }

    @IsTest static void testDelete() {
        List<Study_Site_Team_Member__c> sstmList = [SELECT Id FROM Study_Site_Team_Member__c];
        Test.startTest();
        update sstmList;
        delete sstmList;
        Test.stopTest();
    }

    
    // Jira Ref: SH-17543 Akanksha Singh
    // Test method to verify duplicate CRA prevention functionality
    @IsTest static void checkDuplicateCRASSTMTest() {

        Map<Id, Study_Site_Team_Member__c> mapSSTMIdwithSSTMRec = new Map<Id, Study_Site_Team_Member__c>();
        //Fetch Virtual site
        List<Virtual_Site__c> oldVS = [SELECT Id,
                                       VTR5_CRA_Queue__c
                                       FROM Virtual_Site__c 
                                       WHERE Name = 'TestSite'];
        Id virId = oldVS[0].Id;
        
        //Fetch existing CRA of related virtual site
        List<Study_Site_Team_Member__c> sstmList=   [SELECT Id,
                                                    VTR5_Associated_CRA__c,
                                                    VTD1_SiteID__c 
                                                    FROM Study_Site_Team_Member__c
                                                    WHERE VTD1_SiteID__c =:  virId
                                                    ];

        Id existingSiteId = sstmList[0].VTD1_SiteID__c;
        Id existingCRAId = sstmList[0].VTR5_Associated_CRA__c; 

        //Create duplicate CRA for same site
        Study_Site_Team_Member__c sstm = new Study_Site_Team_Member__c();
        sstm.VTD1_SiteID__c = existingSiteId;
        sstm.VTR5_Associated_CRA__c = existingCRAId;
        Test.startTest();

        // to catch error, 'allOrNone' attribute of Database.Insert should be false.
        Database.SaveResult result = Database.insert(sstm,false);
        Test.stopTest();
        //Compare Error message 
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals(System.Label.VTR5_Duplicate_CRA_Error,
                             result.getErrors()[0].getMessage());
        
    }
    //End of Jira Ref: SH-17543 Akanksha Singh
    
}