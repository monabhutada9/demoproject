/**
 * Created by user on 17-Sep-20.
 */

@IsTest
private class VT_R5_CustomAuditHandler_STMTest {

    @TestSetup
    private static void setup() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();

        User portalAccountOwner = createPortalAccountOwner();
        createCommunityUser(portalAccountOwner, 'PIVS@gmail.com');
        createCommunityUser(portalAccountOwner, 'PINoVS@gmail.com');
        Test.stopTest();


        Id profileId = [SELECT Id FROM Profile WHERE Name = 'Study Admin'].Id;
        User u = new User(
            Alias = 'standt', Email = 'standarduser@testorg.com', EmailEncodingKey = 'UTF-8', LastName = 'Testing',
            LanguageLocaleKey = 'ru', ProfileId = profileId, TimeZoneSidKey = 'Europe/Minsk', LocaleSidKey = 'ru_RU',
            Username = 'usr' + Datetime.now().getTime() + '@test.com'
        );
        insert u;
    }
    @IsTest
    private static void updateStmVirtualSite() {
        HealthCloudGA__CarePlanTemplate__c studyVS = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c WHERE VTD1_Name__c = 'Virtual Site Study' LIMIT 1];
        Virtual_Site__c virtualSite2 = [SELECT Id FROM Virtual_Site__c WHERE Name = 'vs2' LIMIT 1];
        User userPIVS = [SELECT Id FROM User WHERE VTD1_Profile_Name__c = 'Primary Investigator' AND Email = 'PIVS@gmail.com' LIMIT 1];
        System.debug('kdkkd ' + userPIVS);
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setName('Study Name')
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'));
        System.debug('**01:' + Limits.getQueries());

        DomainObjects.StudyTeamMember_t stmRemoteCra1 = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA'))
                .addStudy(study);
        stmRemoteCra1.persist();
        DomainObjects.StudyTeamMember_t stmRemoteCra2 = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA'))
                .addStudy(study);
        stmRemoteCra1.persist();
        System.debug('**11:' + Limits.getQueries());

        DomainObjects.StudyTeamMember_t onsiteCra = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA'))
                .addStudy(study);
        onsiteCra.persist();
        DomainObjects.StudyTeamMember_t onsiteCra2 = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA'))
                .addStudy(study);
        onsiteCra.persist();

        System.debug('**21 :' + Limits.getQueries());
        new DomainObjects.StudyTeamMember_t()
                .setUserId(userPIVS.Id)
                .setStudyId(studyVS.Id)
                .setOnsiteCraId(onsiteCra.Id)
                .setRemoteCraId(stmRemoteCra1.Id)
                .setRecordTypeByName('PI')
                .persist();
        Test.startTest();

        Study_Team_Member__c stm = [
                SELECT  Id,
                        Study__c,
                        Study_Team_Member__c,
                        User__c
                FROM Study_Team_Member__c
                WHERE User__c =: userPIVS.Id
                AND Study__c =: studyVS.Id
                LIMIT 1
        ];


        stm.VTD1_VirtualSite__c = virtualSite2.Id;
        update stm;
        List<VTR5_SiteStudyAccessAudit__c> siteStudyAccessAudits = [
                SELECT Id, VTR5_StudyName__c, VTR5_StudyAccessStatus__c, VTR5_StudyDeactivatedDate__c, VTR5_StudyDeactivatedBy__c
                FROM VTR5_SiteStudyAccessAudit__c
        ];
        System.debug(siteStudyAccessAudits+ ' ()()');
        System.assertEquals(siteStudyAccessAudits.size() != null, true);
        stm.VTR2_Remote_CRA__c = stmRemoteCra2.id;
        stm.VTR2_Onsite_CRA__c = onsiteCra2.id;
        update stm;
        System.debug('**21 :' + Limits.getQueries());
        stm.VTD1_VirtualSite__c = null;
        update stm;
        delete stm;
        Test.stopTest();

    }
    @IsTest
    private static void deleteStmWithSite() {
        HealthCloudGA__CarePlanTemplate__c studyVS = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c WHERE VTD1_Name__c = 'Virtual Site Study' LIMIT 1];
        Virtual_Site__c virtualSite2 = [SELECT Id FROM Virtual_Site__c WHERE Name = 'vs2' LIMIT 1];
        User userPIVS = [SELECT Id FROM User WHERE VTD1_Profile_Name__c = 'Primary Investigator' AND Email = 'PIVS@gmail.com' LIMIT 1];
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setName('Study Name')
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'));
        System.debug('**01:' + Limits.getQueries());

        DomainObjects.StudyTeamMember_t stmRemoteCra1 = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA'))
                .addStudy(study);
        stmRemoteCra1.persist();

        System.debug('**11:' + Limits.getQueries());
        DomainObjects.StudyTeamMember_t onsiteCra = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA'))
                .addStudy(study);
        onsiteCra.persist();
        System.debug('**21 :' + Limits.getQueries());
        new DomainObjects.StudyTeamMember_t()
                .setUserId(userPIVS.Id)
                .setStudyId(studyVS.Id)
                .setOnsiteCraId(onsiteCra.Id)
                .setVirtualSiteId(virtualSite2.Id)
                .setRemoteCraId(stmRemoteCra1.Id)
                .setRecordTypeByName('PI')
                .persist();
        Test.startTest();

        Study_Team_Member__c stm = [
                SELECT  Id,
                        Study__c,
                        Study_Team_Member__c,
                        User__c
                FROM Study_Team_Member__c
                WHERE User__c =: userPIVS.Id
                AND Study__c =: studyVS.Id
                LIMIT 1
        ];
        List<VTR5_SiteStudyAccessAudit__c> siteStudyAccessAudits = [
                SELECT Id, VTR5_StudyName__c, VTR5_StudyAccessStatus__c, VTR5_StudyDeactivatedDate__c, VTR5_StudyDeactivatedBy__c
                FROM VTR5_SiteStudyAccessAudit__c
        ];
        System.debug(siteStudyAccessAudits+ ' ()()');
        System.assertEquals(siteStudyAccessAudits.size() != null, true);

        System.debug('**21 :' + Limits.getQueries());

        delete stm;
        Test.stopTest();

    }

    @IsTest
    private static void deleteStmPositive() {

        HealthCloudGA__CarePlanTemplate__c studyVS = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c WHERE VTD1_Name__c = 'Virtual Site Study' LIMIT 1];
        HealthCloudGA__CarePlanTemplate__c studyNoVS = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c WHERE VTD1_Name__c = 'No Virtual Site Study' LIMIT 1];
        Virtual_Site__c virtualSite = [SELECT Id FROM Virtual_Site__c LIMIT 1];
        Virtual_Site__c virtualSite2 = [SELECT Id FROM Virtual_Site__c WHERE Name = 'vs2' LIMIT 1];
        System.debug('** :' + Limits.getQueries());
//        User userSCR = [SELECT Id FROM User WHERE VTD1_Profile_Name__c = 'Site Coordinator' LIMIT 1];
        User userPIVS = [SELECT Id FROM User WHERE VTD1_Profile_Name__c = 'Primary Investigator' AND Email = 'PIVS@gmail.com' LIMIT 1];
        System.debug('**1 :' + Limits.getQueries());

        User userPl = (User) new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PL_PROFILE_NAME)
                .persist();

        // create Project Lead and Primary Investigator STMs
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setName('Study Name')
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'));
        System.debug('**2:' + Limits.getQueries());

        DomainObjects.StudyTeamMember_t stmRemoteCra1 = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA'))
                .addStudy(study);
        stmRemoteCra1.persist();
        System.debug('**3:' + Limits.getQueries());

        DomainObjects.StudyTeamMember_t stmRemoteCra2 = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA'))
                .addStudy(study);
        stmRemoteCra2.persist();
        System.debug('**4 :' + Limits.getQueries());

        DomainObjects.StudyTeamMember_t stmOnsiteCra1 = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA')
                )
                .addStudy(study);
        stmOnsiteCra1.persist();
        System.debug('**4 :' + Limits.getQueries());

        DomainObjects.StudyTeamMember_t stmOnsiteCra2 = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                .setProfile('CRA'))
                .addStudy(study);
        stmOnsiteCra2.persist();
        System.debug('**5 :' + Limits.getQueries());



        new DomainObjects.StudyTeamMember_t()
                .setUserId(userPL.Id)
                .setStudyId(studyVS.Id)
                .setIsActive(true)
                .persist();

        Test.startTest();
        System.debug('here:22');

        new DomainObjects.StudyTeamMember_t()
                .setUserId(userPIVS.Id)
                .setStudyId(studyVS.Id)
                .setVirtualSiteId(virtualSite.Id)
                .setRecordTypeByName('PI')


                .setRemoteCraId(stmRemoteCra1.Id)
                .setOnsiteCraId(stmOnsiteCra1.Id)
                .persist();
        System.debug('**6 :' + Limits.getQueries());

        Study_Team_Member__c stm = [SELECT Id, VTR2_Remote_CRA__c, VTR2_Onsite_CRA__c FROM Study_Team_Member__c WHERE User__c = :userPIVS.Id LIMIT 1];
        stm.VTR2_Remote_CRA__c = stmRemoteCra2.Id;
        stm.VTR2_Onsite_CRA__c = stmOnsiteCra1.Id;
        new DomainObjects.StudyTeamMember_t()
                .setUserId(userPIVS.Id)
                .setStudyId(studyNoVS.Id)
                .setRecordTypeByName('PI')
                .persist();
        System.debug('**7 :' + Limits.getQueries());



        // validate for PL (Virtual Site)
        Study_Team_Member__c stmPL = [SELECT Id, Study__c, VTD1_Type__c FROM Study_Team_Member__c WHERE User__c = :userPL.Id LIMIT 1];
        List<VTR5_UserAudit__c> userAuditsPL = [SELECT Id, VTR5_UserProfileName__c FROM VTR5_UserAudit__c WHERE User__c = :userPL.Id];
        List<VTR5_SiteStudyAccessAudit__c> siteStudyAccessAuditsPL = [
                SELECT Id, VTR5_StudyName__c, VTR5_StudyAccessStatus__c
                FROM VTR5_SiteStudyAccessAudit__c
                WHERE User_Audit__r.User__c = :userPL.Id
        ];


        System.debug('**8 :' + Limits.getQueries());

        System.assertNotEquals(stmPL.VTD1_Type__c, 'Primary Investigator');



        System.assertEquals(1, userAuditsPL.size());
        System.assertEquals(1, siteStudyAccessAuditsPL.size());
        System.assertEquals(stmPL.Study__c, siteStudyAccessAuditsPL[0].VTR5_StudyName__c);
        System.assertEquals(true, siteStudyAccessAuditsPL[0].VTR5_StudyAccessStatus__c);

        // validate for PI (Virtual Site)

        Study_Team_Member__c stmPI = [
                SELECT Id, CreatedDate, VTD1_Type__c, VTD1_VirtualSite__c
                FROM Study_Team_Member__c
                WHERE User__c = :userPIVS.Id AND VTR2_Remote_CRA__c != NULL
                LIMIT 1];

        List<VTR5_UserAudit__c> userAuditsPi = [SELECT Id, VTR5_UserProfileName__c FROM VTR5_UserAudit__c WHERE User__c = :userPIVS.Id];
        List<VTR5_SiteStudyAccessAudit__c> siteStudyAccessAuditsPi = [
                SELECT Id, VTR5_StudyAccessStatus__c, VTR5_SiteAccessStatus__c, VTR5_AssignedDateStudy__c, VTR5_AssignedDateSite__c
                FROM VTR5_SiteStudyAccessAudit__c
                WHERE User_Audit__r.User__c = :userPIVS.Id
                LIMIT 1
        ];
        System.assertEquals(1, userAuditsPi.size());
        System.assertEquals(1, siteStudyAccessAuditsPi.size());
        System.assertEquals(true, siteStudyAccessAuditsPi[0].VTR5_StudyAccessStatus__c);
        System.assertEquals(true, siteStudyAccessAuditsPi[0].VTR5_SiteAccessStatus__c);
//        System.assertEquals(stmPI.CreatedDate, siteStudyAccessAuditsPi[0].VTR5_AssignedDateStudy__c);
//        System.assertEquals(stmPI.CreatedDate, siteStudyAccessAuditsPi[0].VTR5_AssignedDateSite__c);


        System.debug('*9 :' + Limits.getQueries());

        stmPI.VTD1_VirtualSite__c = virtualSite2.Id;
        update stmPI;
        stmPI.VTD1_VirtualSite__c = null;
        update stmPI;

//         validate for PI (No Virtual Site)
//        Study_Team_Member__c stmPINOVS = [SELECT Id, CreatedDate, VTD1_Type__c FROM Study_Team_Member__c WHERE User__c = :userPIVS.Id AND Id != :stmPI.Id];
        List<VTR5_UserAudit__c> userAuditsPINoVS = [SELECT Id, VTR5_UserProfileName__c FROM VTR5_UserAudit__c WHERE User__c = :userPIVS.Id];
        List<VTR5_SiteStudyAccessAudit__c> siteStudyAccessAuditsPiNoVS = [
                SELECT Id, VTR5_StudyAccessStatus__c, VTR5_SiteAccessStatus__c, VTR5_AssignedDateStudy__c, VTR5_AssignedDateSite__c
                FROM VTR5_SiteStudyAccessAudit__c
                WHERE User_Audit__r.User__c = :userPIVS.Id
        ];
        System.assertEquals(1, userAuditsPINoVS.size());
        System.assertEquals(1, siteStudyAccessAuditsPi.size());
        System.assertEquals(true, siteStudyAccessAuditsPiNoVS[0].VTR5_StudyAccessStatus__c);


        System.debug('*9 :' + Limits.getQueries());

        Test.stopTest();
    }
/*
    @IsTest
    private static void deleteStmPositive2(){

        User userStudyAdmin = [SELECT Id, Name FROM User WHERE Profile.Name = 'Study admin' AND IsActive = TRUE LIMIT 1];
//        User userPL = [SELECT Id FROM User WHERE VTD1_Profile_Name__c = :'Project Lead' LIMIT 1];

        Test.startTest();
        System.runAs(userStudyAdmin) {
            DomainObjects.Study_t study_t = new DomainObjects.Study_t();
            DomainObjects.StudyTeamMember_t stm_t = new DomainObjects.StudyTeamMember_t()
                    .addStudy(study_t)

                    .addUser(new DomainObjects.User_t()
                    .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PL_PROFILE_NAME))
                    .setIsActive(true);


            Study_Team_Member__c stm = (Study_Team_Member__c) stm_t.persist();
            delete stm;
        }
        Test.stopTest();


        List<VTR5_UserAudit__c> userAudits = [SELECT Id, VTR5_UserProfileName__c FROM VTR5_UserAudit__c LIMIT 1];

        List<VTR5_SiteStudyAccessAudit__c> siteStudyAccessAudits = [
                SELECT Id, VTR5_StudyName__c, VTR5_StudyAccessStatus__c, VTR5_StudyDeactivatedDate__c, VTR5_StudyDeactivatedBy__c
                FROM VTR5_SiteStudyAccessAudit__c
        ];

        System.assertEquals(1, userAudits.size());
        System.assertEquals(1, siteStudyAccessAudits.size());
        System.assertEquals(userStudyAdmin.Name, siteStudyAccessAudits[0].VTR5_StudyDeactivatedBy__c);
        System.assertEquals(Date.today(), siteStudyAccessAudits[0].VTR5_StudyDeactivatedDate__c.date());
        System.assertEquals(false, siteStudyAccessAudits[0].VTR5_StudyAccessStatus__c);
        //@todo write sae for site deactivation field


    }*/

    @IsTest
    static void updateSsaaBackupPisAfterAddingToPrPi() {
        HealthCloudGA__CarePlanTemplate__c studyVS = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c WHERE VTD1_Name__c = 'Virtual Site Study' LIMIT 1];
        Virtual_Site__c virtualSite = [SELECT Id FROM Virtual_Site__c LIMIT 1];
        User userPIVS = [SELECT Id FROM User WHERE VTD1_Profile_Name__c = 'Primary Investigator' AND Email = 'PIVS@gmail.com' LIMIT 1];
        User userPIVSBackUP = [SELECT Id FROM User WHERE VTD1_Profile_Name__c = 'Primary Investigator' AND Email = 'PINoVS@gmail.com' LIMIT 1];
        System.debug('** :' + Limits.getQueries());
        Test.startTest();
        new DomainObjects.StudyTeamMember_t()
                .setUserId(userPIVS.Id)
                .setStudyId(studyVS.Id)
                .setVirtualSiteId(virtualSite.Id)
                .setRecordTypeByName('PI')
                .persist();
        System.debug('**1 :' + Limits.getQueries());
        Study_Team_Member__c stmPr = [
                SELECT  Id,
                        Study__c,
                        VTD1_VirtualSite__c,
                        User__c
                FROM Study_Team_Member__c
                WHERE User__c =: userPIVS.Id
                AND Study__c =: studyVS.Id
                AND VTD1_VirtualSite__c =: virtualSite.Id
                LIMIT 1
        ];
        new DomainObjects.StudyTeamMember_t()
                .setUserId(userPIVSBackUP.Id)
                .setStudyId(studyVS.Id)
                .setRecordTypeByName('PI')
                .persist();
        Study_Team_Member__c stmBackUp = [
                SELECT  Id,
                        Study__c,
                        Study_Team_Member__c,
                        User__c
                FROM Study_Team_Member__c
                WHERE User__c =: userPIVSBackUP.Id
                AND Study__c =: studyVS.Id
                LIMIT 1
        ];
        stmBackUp.Study_Team_Member__c = stmPr.Id;
        update stmBackUp;
        System.debug('**2 :' + Limits.getQueries());
        Test.stopTest();
        List<VTR5_SiteStudyAccessAudit__c> siteStudyAccessAudits = [
                SELECT Id, VTR5_StudyName__c, VTR5_StudyAccessStatus__c, VTR5_StudyDeactivatedDate__c, VTR5_StudyDeactivatedBy__c
                FROM VTR5_SiteStudyAccessAudit__c
        ];
        System.debug(siteStudyAccessAudits+ ' ()()');
        System.assertEquals(siteStudyAccessAudits.size() != null, true);
        stmBackUp.Study_Team_Member__c = null;
        update stmBackUp;
        System.debug('**3:' + Limits.getQueries());

    }
//
    private static User createPortalAccountOwner() {
        UserRole portalRole = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role', PortalType='None' );
        insert portalRole;
        System.debug('portalRole is ' + portalRole);
        Profile sysAdminProfile = [Select Id from Profile where name = 'System Administrator' LIMIT 1];
        System.debug('4');

        User portalAccountOwner = (User) new DomainObjects.User_t()
                .setProfile(sysAdminProfile.Id)
                .setUserRoleId(portalRole.Id)
                .persist();

        return portalAccountOwner;
    }

    private static void createCommunityUser(User portalAccountOwner, String email) {
        System.runAs ( portalAccountOwner ) {
            DomainObjects.Account_t account_t = new DomainObjects.Account_t().setRecordTypeByName('Sponsor');

            //Create account
            Account portalAccount = new Account(
                    Name = 'portalAccount',
                    OwnerId = portalAccountOwner.Id
            );
            Database.insert(portalAccount);
            //Create contact
            Contact portalContact = new Contact(
                    FirstName = 'portalContactFirst',
                    Lastname = 'portalContactLast',
                    AccountId = portalAccount.Id,
                    Email = 'portalContact' + System.currentTimeMillis() + '@test.com'
            );
            Database.insert(portalContact);
            User communityUser = new User(
                    ProfileId = [SELECT Id FROM Profile WHERE Name = 'Primary investigator'].Id,
                    FirstName = 'CommunityUserFirst',
                    LastName = 'CommunityUserLast',
                    Email = email,
                    Username = 'community.user.' + System.currentTimeMillis() + '@test.com',
                    Title = 'Title',
                    Alias = 'Alias',
                    TimeZoneSidKey = 'America/Los_Angeles',
                    EmailEncodingKey = 'UTF-8',
                    LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_US',
                    ContactId = portalContact.id
            );
            Database.insert(communityUser);
            DomainObjects.Study_t studyWithVirtualSite1 = new DomainObjects.Study_t()
                    .setName('Virtual Site Study')
                    .addAccount(account_t);
            studyWithVirtualSite1.persist();
            DomainObjects.Study_t studyWithVirtualSite2 = new DomainObjects.Study_t()
                    .setName('Virtual Site Study 2')
                    .addAccount(account_t);
            studyWithVirtualSite2.persist();

            DomainObjects.Study_t studyWithoutVirtualSite = new DomainObjects.Study_t()
                    .setName('No Virtual Site Study')
                    .addAccount(account_t);
            studyWithoutVirtualSite.persist();

            DomainObjects.VirtualSite_t vs_1 = new DomainObjects.VirtualSite_t()
                    .setName('vs1')
                    .addStudy(studyWithVirtualSite1);
            vs_1.persist();
            DomainObjects.VirtualSite_t vs_2 = new DomainObjects.VirtualSite_t()
                    .addStudy(studyWithVirtualSite2)
                    .setName('vs2');
            vs_2.persist();
        }
    }


}