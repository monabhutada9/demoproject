@IsTest
public with sharing class VT_R4_SCRNavigationPathHeaderCtrlTest {
    public static void testGetCaseIdByDocumentId() {
        VTD1_Document__c document = (VTD1_Document__c) new DomainObjects.VTD1_Document_t().persist();
        VTD1_Document__c foundDocument = VT_R4_SCRNavigationPathHeaderController.getCaseIdByDocumentId(document.Id);

        System.assert(foundDocument != null);
        System.assert(foundDocument.Id == document.Id);
    }

    public static void testGetLabelTranslated() {
        System.assert(VT_R4_SCRNavigationPathHeaderController.getLabelTranslated('TESTING') == 'Testing');
    }

    public static void testGetCaseIdByTreatmentArmId() {
        DomainObjects.Case_t caseRecord = new DomainObjects.Case_t();
        VTR2_Treatment_Arm__c treatmentArm = (VTR2_Treatment_Arm__c) new DomainObjects.VTR2_Treatment_Arm_t()
                .setStartDate(Date.today().addDays(-7))
                .setEndDate(Date.today().addDays(7))
                .addCase(caseRecord)
                .persist();
        System.assert(VT_R4_SCRNavigationPathHeaderController.getCaseIdByTreatmentArmId('test/' + treatmentArm.Id) != null);
    }
}