/*
Created By - Yogesh More
Created Date - 17/01/2020
Version - 1.0
Story No. - SH-9363/SH-9180
Discription - Create Service Resource and Service Resource skill based on user language.
*/

/*
Method:- 
CreateSkillAssignments – This method creates the Service Resource and Service Resource skill record based 
                         on user language and studies. This method ready for invoke from anywhere like anonymous window, 
                         class or batch class. Also, we have written batch class if we have bulk of users record to process.
                         To execute this method, use following code format.

// execute from class or anonymous window                     
List<user> lstUser = [Select id,Name,IsActive,VT_R5_Primary_Preferred_Language__c,VT_R5_Secondary_Preferred_Language__c,VT_R5_Tertiary_Preferred_Language__c from User where profile.Name = 'Study Concierge'];
String result = VTR5_CreateSkillAssignment.CreateSkillAssignments(lstUser);
System.debug('result==>'+result);

// execute from batch class window
String query = 'Select id,Name,IsActive,VT_R5_Primary_Preferred_Language__c,VT_R5_Secondary_Preferred_Language__c,VT_R5_Tertiary_Preferred_Language__c from User where profile.Name = \'Study Concierge\'';         
VTR5_BatchClassCreateSkillAssignment batch = new VTR5_BatchClassCreateSkillAssignment(query);
Database.executeBatch(batch, 100);
*/

public class VTR5_CreateSkillAssignment{
    
    public static string CreateSkillAssignments(List<user> lstUser){
        Savepoint sp = Database.setSavepoint();
        try{        
        List<ServiceResource> serviceResourseToCreate = new List<ServiceResource>();
        Set<String> allLanguageForSkillMatch = new Set<String>();
        Map<String, Skill> mapOfSkill = new Map<String, Skill>();
        List<ServiceResourceSkill> skillAssignToServResourseList = new List<ServiceResourceSkill>();
        Map<Id,Set<String>> userMapWithSkills = new Map<Id,Set<String>>();
        
        Map<String, ServiceResource> mapOfExistingSR = new Map<String, ServiceResource>();
        Map<String, ServiceResourceSkill> mapOfExistingSRS = new Map<String, ServiceResourceSkill>();
        List<ServiceResource> existingSR = new List<ServiceResource>();
        
        // Getting existing service resources and skill.
        existingSR = [Select Id, RelatedRecordId, (select id,Skill.DeveloperName,Skill.Id from ServiceResourceSkills) from ServiceResource where RelatedRecordId IN : lstUser];
        for(ServiceResource sr : existingSR){
            mapOfExistingSR.put(sr.RelatedRecordId, sr);
            for(ServiceResourceSkill srs : sr.ServiceResourceSkills){
                mapOfExistingSRS.put(sr.RelatedRecordId+'-'+srs.Skill.DeveloperName, srs);
            }
        }
        
        // Creating Service Resource record
        for(user us : lstUser){            
            Set<String>  languageOfUeser= New Set<String>();             
            if(!mapOfExistingSR.containsKey(us.Id)){
                ServiceResource servResor = new ServiceResource();
                servResor.Name='SR-SC-'+us.Name;
                servResor.RelatedRecordId=us.Id;
                servResor.IsActive=true;
                servResor.ResourceType='A';     
                serviceResourseToCreate.add(servResor);
            }
                if(us.VT_R5_Primary_Preferred_Language__c!= null && us.VT_R5_Primary_Preferred_Language__c!='')
                languageOfUeser.add(us.VT_R5_Primary_Preferred_Language__c);
                
                if(us.VT_R5_Secondary_Preferred_Language__c != null && us.VT_R5_Secondary_Preferred_Language__c!='')
                languageOfUeser.add(us.VT_R5_Secondary_Preferred_Language__c);
                    
                if(us.VT_R5_Tertiary_Preferred_Language__c != null && us.VT_R5_Tertiary_Preferred_Language__c!='')
                languageOfUeser.add(us.VT_R5_Tertiary_Preferred_Language__c);
                
                allLanguageForSkillMatch.addAll(languageOfUeser);                        
                userMapWithSkills.put(us.id,languageOfUeser);
        }
        
        // DML on Service Resource object
        System.debug('serviceResourseToCreate*****2'+serviceResourseToCreate);              
        if(serviceResourseToCreate != Null && serviceResourseToCreate.size()>0){
            insert serviceResourseToCreate;
        }        
        System.debug('serviceResourseToCreate*****2'+serviceResourseToCreate);
        
        // getting study team member for resource skill creation based on their study.
        For(Study_Team_Member__c stm: [Select id,User__c,Study__c,Study__r.Name from Study_Team_Member__c
                                       where User__c IN : lstUser and Study__c != null] ){
                userMapWithSkills.get(stm.User__c).add(stm.Study__c);
                allLanguageForSkillMatch.add(stm.Study__c);
        }                              
                                       
        //getting skill records based on user language
        System.debug('allLanguageForSkillMatch*****2'+allLanguageForSkillMatch);
        List<Skill> skillList = [Select id,DeveloperName from skill where DeveloperName IN : allLanguageForSkillMatch];
        System.debug('skillList*****2'+skillList);
        for(Skill skl : skillList){
            mapOfSkill.put(skl.DeveloperName, skl);
        }     
        
        System.debug('existingSR===>'+existingSR);
        if(existingSR.size()>0){
            serviceResourseToCreate.addAll(existingSR);
        }
        
        System.debug('serviceResourseToCreate===>'+serviceResourseToCreate);
        System.debug('userMapWithSkills===>'+userMapWithSkills);
        // Creating Service Resource skill record
        for(ServiceResource rSkil : serviceResourseToCreate){
            if(userMapWithSkills.containskey(rSkil.RelatedRecordId) && userMapWithSkills.get(rSkil.RelatedRecordId) != null){
                for(String str : userMapWithSkills.get(rSkil.RelatedRecordId)){
                    if(mapOfSkill.containsKey(str) && !mapOfExistingSRS.containsKey(rSkil.RelatedRecordId+'-'+str)){
                        ServiceResourceSkill srk =  new ServiceResourceSkill();
                        srk.ServiceResourceId = rSkil.Id;
                        srk.SkillId = mapOfSkill.get(str).id;
                        srk.EffectiveStartDate=date.today();
                        skillAssignToServResourseList.add(srk);
                    }
                }
            } 
        }
        
        // DML on Service Resource Skill object
        system.debug('skillAssignToServResourseList*****'+skillAssignToServResourseList);
        if(skillAssignToServResourseList != null && skillAssignToServResourseList.size()>0){
            insert skillAssignToServResourseList;
        }
        return 'Success';
    }
    catch(Exception ex){
        Database.rollback(sp);
        return 'Error Message =>'+ex.getMessage()+'<br/>Line Number - '+ex.getLineNumber()+'<br/>User List - '+String.ValueOf(lstUser);
    }
    }
}