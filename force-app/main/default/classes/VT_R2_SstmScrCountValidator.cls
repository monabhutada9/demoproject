/**
 * Created by Dmitry Yartsev on 11.06.2019.
 */

public with sharing class VT_R2_SstmScrCountValidator {
    public static void validateSCRsCount(List<Study_Site_Team_Member__c> members, Map<Id, Study_Site_Team_Member__c> oldMembersMap) {
        Set<Id> membersPIIds = new Set<Id>();
        final Set<String> SCR_TYPES = new Set<String>{
                VT_R4_ConstantsHelper_Misc.SSTM_SCR_TYPE_PRIMARY,
                VT_R4_ConstantsHelper_Misc.SSTM_SCR_TYPE_BACKUP
        };

        for (Study_Site_Team_Member__c member : members) {
            if(!String.isBlank(member.VTR2_Associated_PI__c) && !membersPIIds.contains(member.VTR2_Associated_PI__c)) {
                membersPIIds.add(member.VTR2_Associated_PI__c);
            }
        }
        Map<Id, Map<String, Integer>> countSCRBySTMMap = new Map<Id, Map<String, Integer>>();
        for (Study_Team_Member__c memberPI : [SELECT Id, (select id, VTR2_SCr_type__c from
                Study_Site_Team_Members1__r WHERE VTR2_SCr_type__c IN :SCR_TYPES)
        FROM Study_Team_Member__c
        WHERE id IN :membersPIIds
        ORDER BY Name ASC]) {
            Integer countPrimary = 0;
            Integer countBackup = 0;
            countSCRBySTMMap.put(memberPI.Id, new Map<String, Integer>());
            for (Study_Site_Team_Member__c studySiteTeamMember : memberPI.Study_Site_Team_Members1__r) {
                if (studySiteTeamMember.VTR2_SCr_type__c == 'Primary') {
                    countPrimary += 1;
                } else if (studySiteTeamMember.VTR2_SCr_type__c == 'Backup') {
                    countBackup += 1;
                }
            }
            countSCRBySTMMap.get(memberPI.Id).put('countPrimary', countPrimary);
            countSCRBySTMMap.get(memberPI.Id).put('countBackup', countBackup);
        }
        if (countSCRBySTMMap.size() > 0) {
            for (Study_Site_Team_Member__c member : members) {
                if(oldMembersMap == null || (member.VTR2_SCr_type__c != oldMembersMap.get(member.Id).VTR2_SCr_type__c) || (member.VTR2_Associated_PI__c != oldMembersMap.get(member.Id).VTR2_Associated_PI__c) ||
                        (member.VTR2_Associated_SCr__c != oldMembersMap.get(member.Id).VTR2_Associated_SCr__c && oldMembersMap.get(member.Id).VTR2_Associated_SCr__c == null)) {
                    if (member.VTR2_SCr_type__c == 'Primary' && countSCRBySTMMap.get(member.VTR2_Associated_PI__c).get('countPrimary') > 0) {
                        member.VTR2_SCr_type__c.addError('Only one Primary SCR can be assigned to the site');
                        countSCRBySTMMap.get(member.VTR2_Associated_PI__c).put('countPrimary', countSCRBySTMMap.get(member.VTR2_Associated_PI__c).get('countPrimary') + 1);
                    } else if (member.VTR2_SCr_type__c == 'Backup' && countSCRBySTMMap.get(member.VTR2_Associated_PI__c).get('countBackup') > 0) {
                        member.VTR2_SCr_type__c.addError('Only one Backup SCR can be assigned to the site');
                        countSCRBySTMMap.get(member.VTR2_Associated_PI__c).put('countBackup', countSCRBySTMMap.get(member.VTR2_Associated_PI__c).get('countBackup') + 1);
                    }
                }
            }
        }
    }
}