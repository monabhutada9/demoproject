/**
 * Created by Oleh Berehovskyi on 03-Apr-19.
 */

public with sharing class VTR2_SObjectUtils {

//    public static Map<String, String> getPicklistEntryMap(SObjectType type, String picklistName) {
//        Schema.DescribeSObjectResult sObjectDescribe = Schema.getGlobalDescribe()
//                .get(String.valueOf(type))
//                .getDescribe();
//        Map<String, Schema.SObjectField> fieldMap = sObjectDescribe.fields.getMap();
//        List <Schema.PicklistEntry> picklistEntries = fieldMap.get(picklistName)
//                .getDescribe()
//                .getPicklistValues();
//        Map<String, String> values = new Map<String, String>();
//        for (Schema.PicklistEntry entry: picklistEntries) {
//            values.put(entry.label, entry.value);
//        }
//        return values;
//    }

    public static Map<String, String> getPicklistEntryMapByRecordTypeId(SObjectType type, Id recordTypeId, String picklistName) {
            String endpoint = Url.getOrgDomainUrl().toExternalForm();
            endpoint += '/services/data/v45.0';
            endpoint += '/ui-api/object-info/{0}/picklist-values/{1}/{2}';
            endpoint = String.format(endpoint, new String[]{
                    String.valueOf(type), recordTypeId, String.valueOf(picklistName)
            });
            EncodingUtil.urlEncode(endpoint, 'UTF-8');
            HttpRequest request = new HttpRequest();
            request.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Accept', 'application/json');
            request.setEndpoint(endpoint);
            request.setMethod('GET');
            Http http = new Http();
            HttpResponse response = http.send(request);
            System.debug('response: ' + response.getBody());
            Map<String, String> picklistEntryMap = new Map<String, String>();
            Map<String, Object> root = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            List<Object> picklistValues = (List<Object>) root.get('values');
            for (Object picklistValue : picklistValues) {
                Map<String, Object> picklistValueMap = (Map<String, Object>) picklistValue;
                picklistEntryMap.put((String) picklistValueMap.get('label'), (String) picklistValueMap.get('value'));
            }
            return picklistEntryMap;
//        }
    }

}