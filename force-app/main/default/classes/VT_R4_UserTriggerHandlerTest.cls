@IsTest
public class VT_R4_UserTriggerHandlerTest {

    
    @testSetup
    public static void setup(){

        
        DomainObjects.Study_t study = new DomainObjects.Study_t()
            .setName('Study Name')
            .setMaximumDaysWithoutLoggingIn(1)
            .setEdiaryTool('eCOA')
            .setEcoaGuid('study_79826f74-6967-4eb0-bc99-f50629e66f0c')
            .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));

        study.persist();

        VT_R5_ShareGroupMembershipService.disableMembership = True;
        DomainObjects.VirtualSite_t virtSite = new DomainObjects.VirtualSite_t()
            .addStudy(study)
            .setEcoaGuid('sitertyuio56789045678yui');
        
        virtSite.persist();
        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t();
        patientAccount.persist();
        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
            .setAccountId(patientAccount.id)
            .setFirstName('Morty_the_Patient');

            patientContact.persist();
        DomainObjects.User_t userPatient = new DomainObjects.User_t()
            .addContact(patientContact)
            .setEcoaGuid('subject_9ecb75cf-a594-45b8-6667-7d11bd9bd41d')
            .setActive(true)
            .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        
        userPatient.persist();
        
        DomainObjects.Case_t casePatient = new DomainObjects.Case_t()
            .setRecordTypeByName('CarePlan')
            .addStudy(study)
            .addVirtualSite(virtSite)
            .addUser(userPatient)
            .addContact(patientContact)
            .addAccount(patientAccount);
        
        casePatient.persist();
        
        contact con = new contact();
          con.id=patientContact.id;
          con.VTD1_UserId__c = userPatient.id;
          con.VTD1_Clinical_Study_Membership__c=casePatient.Id;
          update con;


        Address__c address1 = new Address__c();
        address1.Name='Street5';
        address1.City__c = 'City';
        address1.Country__c='US';
        address1.State__c  = 'NY';
        address1.VTD1_Contact__c = con.Id;
        address1.VTD1_Primary__c  = true;
        insert address1;

        
        

        VTR5_ePROClinRORuleset__c eproruleSet = New VTR5_ePROClinRORuleset__c();
        eproruleSet.VTR5_ProtocolNumber__c =study.Id;
        eproruleSet.VTR5_Country__c='US';
        eproruleSet.VTR5_State__c='NY';
        eproruleSet.VTR5_ePRO_GUID__c='rtuoi45678';

        eproruleSet.VTR5_ePRO_ScheduleName__c ='test schedule';
        insert eproruleSet;
        VTR5_ePROClinRORuleset__c eproruleSet1 = New VTR5_ePROClinRORuleset__c();
        eproruleSet1.VTR5_ProtocolNumber__c =study.id;
        eproruleSet1.VTR5_SubsetImmuno__c='Styuio56';
        eproruleSet1.VTR5_ePRO_GUID__c='rtuoi45678';
        eproruleSet1.VTR5_ePRO_ScheduleName__c ='test schedule';
        insert eproruleSet1;
      
    }
    @isTest
    public static void eproUserFieldSWithoutEproRecordTest() { 
        case cas =[Select id,VTD1_Study__c,VTD1_Virtual_Site__c,VTD1_Patient_User__c from case];
       
        user us =[Select id,ContactId,VTR5_Create_Subject_in_eCOA__c,Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c,Contact.VTD1_Clinical_Study_Membership__c 
                  from user where Id =:cas.VTD1_Patient_User__c Limit 1];
        User adminUser = [Select id,Name from USer where isActive =true and Profile.Name = 'System Administrator' limit 1];
        Map<id,user> idtouserold = new Map<id,user>();
        idtouserold.put(us.id,us);
        
        us.VTR5_Create_Subject_in_eCOA__c=true;
        System.runAs(adminUser){
            update us;
        }
        List<User> userList = new List<User>();
        userList.add(us);
        Test.startTest();

        VT_D1_UserTriggerHandler.setClinroAndEproUserFields(userList,idtouserold); 
       
        User currentuser = [Select id,VTR5_ePRO_GUID__c,VTR5_ePRO_ScheduleName__c,VTR5_Create_Subject_in_eCOA__c
                            from user where id =:us.Id and VTR5_ePRO_GUID__c =null Limit 1 ];
        System.assertEquals(null, currentuser.VTR5_ePRO_GUID__c);
        Test.stopTest(); 
    }
     @isTest
    public static void eproUserFieldSubsetimmunoTest() { 
        
        HealthCloudGA__CarePlanTemplate__c studysub =[Select id,VTD1_Name__c from HealthCloudGA__CarePlanTemplate__c];
        studysub.VTR5_eCOA_Ruleset_Criteria__c='Subset Immuno/Safety';
        studysub.OwnerId = Userinfo.getUserId();
        update studysub;

        case cas =[Select id,VTD1_Study__c,VTD1_Virtual_Site__c,VTD1_Patient_User__c from case where VTD1_Study__c  =: studysub.Id ];
        cas.VTR5_SubsetImmuno__c='Styuio56';
        update cas;

        user us =[Select id,ContactId,VTR5_Create_Subject_in_eCOA__c,Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c,Contact.VTD1_Clinical_Study_Membership__c 
                  from user where Id =:cas.VTD1_Patient_User__c Limit 1];
        User adminUser = [Select id,Name from USer where isActive =true and Profile.Name = 'System Administrator' limit 1];
        Map<id,user> idtouserold = new Map<id,user>();
        idtouserold.put(us.id,us);
        us.VTR5_Create_Subject_in_eCOA__c=true;   
        System.runAs(adminUser)
        {
            update us;
        }
        List<User> userList = new List<User>();
        userList.add(us);
        
        Test.startTest();
        VT_D1_UserTriggerHandler.setClinroAndEproUserFields(userList,idtouserold);
        User currentuser = [Select id,VTR5_ePRO_GUID__c,VTR5_ePRO_ScheduleName__c,VTR5_Create_Subject_in_eCOA__c
                            from user where id =:us.Id Limit 1 ];
         System.assertEquals('rtuoi45678', currentuser.VTR5_ePRO_GUID__c);
        Test.stopTest(); 
    }
    
    @isTest
    public static void setClinroAndEproUserFieldStateAddressTest() { 
        
        HealthCloudGA__CarePlanTemplate__c studyRec =[Select id,VTD1_Name__c from HealthCloudGA__CarePlanTemplate__c];
        studyRec.VTR5_eCOA_Ruleset_Criteria__c='States Address';
        update studyRec;
       
        case cas =[Select id,VTD1_Study__c,VTD1_Virtual_Site__c,VTD1_Patient_User__c from case];

        user us =[Select id,ContactId,VTR5_Create_Subject_in_eCOA__c,Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c,Contact.VTD1_Clinical_Study_Membership__c 
                  from user where Id =:cas.VTD1_Patient_User__c Limit 1];
        User adminUser = [Select id,Name from USer where isActive =true and Profile.Name = 'System Administrator' limit 1];
        Map<id,user> idtouserold = new Map<id,user>(); 
        idtouserold.put(us.id,us);
      
        us.VTR5_Create_Subject_in_eCOA__c=true;
        System.runAs(adminUser){
            update us;
        }
        List<User> userList = new List<User>();
        userList.add(us);
       
        Test.startTest();
        VT_D1_UserTriggerHandler.setClinroAndEproUserFields(userList,idtouserold);
        User currentuser = [Select id,VTR5_ePRO_GUID__c,VTR5_ePRO_ScheduleName__c,VTR5_Create_Subject_in_eCOA__c
                            from user where id =:us.Id Limit 1 ];
        System.assertEquals('rtuoi45678', currentuser.VTR5_ePRO_GUID__c);
     

        Test.stopTest(); 

    }
    @IsTest
    public static void verifyWelcomeEmailForRescueStudyUsers() {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
            .setName('Study Name')
            .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'))
            .setRescueStudy(true);
        
        DomainObjects.User_t piUser = new DomainObjects.User_t()
            .addContact(new DomainObjects.Contact_t())
            .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME);
        
        Study_Team_Member__c member = (Study_Team_Member__c) new DomainObjects.StudyTeamMember_t()
            .setRecordTypeByName('PI')
            .addUser(piUser)
            .addStudy(study)
            .persist();
        
        User oldUser = new User(Id = member.User__c);
        User newUser = oldUser.clone(true, true, true, true);
        newUser.IsActive = true;
        
        VT_D1_UserTriggerHandler.welcomeEmailForRescueStudyUsers(
            new List<User>{newUser}, new Map<Id, User>{oldUser.Id => oldUser}, new Map<Id, User>{newUser.Id => newUser}
        );
    }
    
    


}