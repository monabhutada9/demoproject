/**
* @author: Carl Judge
* @date: 14-Sep-18
* @description: 
**/

public class VT_D2_VisitRequestedTemplate_Day {

    public String dateText {get; set;}
    public TimeSlot[] timeSlots {get; set;}

    public VT_D2_VisitRequestedTemplate_Day(String dateText) {
        this.dateText = dateText;
        this.timeSlots = new List<TimeSlot>();
    }

    public class TimeSlot {
        public String slotText {get; set;}
        public String slotUrl {get; set;}

        public TimeSlot(String slotText, String slotUrl) {
            this.slotText = slotText;
            this.slotUrl = slotUrl;
        }
    }

}