/**
* @author: Carl Judge
* @date: 11-Jun-20
* @description: body for eCOA response export request
**/

public class VT_R5_RequestBuilder_eCoaResponseExport implements VT_D1_RequestBuilder {
    public String orgGuid = eCOA_IntegrationDetails__c.getInstance().eCOA_OrgGuid__c;
    public String studyGuid;
    public Object queryDesc;
    public String format = VT_R4_ConstantsHelper_Protocol_ePRO.DIARY_SETTINGS.VTR5_DiaryResponseFormat__c;

    public VT_R5_RequestBuilder_eCoaResponseExport(String studyGuid, VT_R5_eCoaQueryBuilder queryDesc) {
        this.studyGuid = studyGuid;
        this.queryDesc = queryDesc;
    }

    public String buildRequestBody() {
        return JSON.serialize(this, true);
    }
}