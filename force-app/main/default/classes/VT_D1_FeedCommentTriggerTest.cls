/**
 * Created by User on 19/06/30.
 */
@isTest
public with sharing class VT_D1_FeedCommentTriggerTest {

    public static void doTest(){
        Account acc = new Account(Name = 'acc');
        insert acc;

        FeedItem fItem = new FeedItem(Body = 'Test', ParentId = acc.Id);
        insert fItem;

        insert new FeedComment(
                CommentBody = 'Test',
                FeedItemId = fItem.Id
        );
        List<FeedComment> feedCommentList = [SELECT Id FROM FeedComment];
        System.assertNotEquals(0,feedCommentList.size());

        delete feedCommentList;
        List<FeedComment> feedCommentListAfterDelete = [SELECT Id FROM FeedComment];
        System.assertEquals(0,feedCommentListAfterDelete.size());
    }
}