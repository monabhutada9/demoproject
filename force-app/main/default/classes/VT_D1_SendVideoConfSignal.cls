public with sharing class VT_D1_SendVideoConfSignal {
    @InvocableMethod
    public static void sendSignalToNotConnectedParticipants(List<Id> videoConfIdsList){
    	if(videoConfIdsList==null || videoConfIdsList.isEmpty()) return;
    	
    	Id videoConfId=videoConfIdsList[0];
    	if(String.isBlank(videoConfId)) return;
	
		List<VTD1_Video_Conf_Connection__c> connectionsList = [select Id, VTD1_Participant__c from VTD1_Video_Conf_Connection__c where
																VTD1_Video_ConfId__c = :videoConfId and																
																VTD1_Time_Connection__c!=null and
																VTD1_Time_Disconnection__c=null];

		//if(connectionsList.isEmpty()) return;
		
		set<Id> connectedParticipantsSetIds = new set<Id>();
		if(!connectionsList.isEmpty()){
			for(VTD1_Video_Conf_Connection__c conItem:connectionsList){
				connectedParticipantsSetIds.add(conItem.VTD1_Participant__c);
			}
		}
		
		
		//get List of participants not connnected to conference yet
		List<VTD1_Conference_Member__c> membersList=[select Id, VTD1_User__c from VTD1_Conference_Member__c where
    												 VTD1_Video_Conference__c=:videoConfId and
    												 VTD1_User__c!=null and
    												 VTD1_User__c not in:connectedParticipantsSetIds];

		if(membersList.isEmpty()) return;
		
		set<Id> notConnectedParticipantsSetIds = new set<Id>();
		for(VTD1_Conference_Member__c vcmItem:membersList){
			notConnectedParticipantsSetIds.add(vcmItem.VTD1_User__c);
		}
		
		if(!notConnectedParticipantsSetIds.isEmpty()){
			VT_D1_VideoConfSignalHandler.sendSignal(notConnectedParticipantsSetIds, true);
		}
		
    }
}