/**
 * Created by Dmitry Kovalev on 03.09.2020.
 */
@IsTest
public class VT_R5_IRBLangConfigurationTriggerTest {
    @TestSetup
    private static void setup() {
        HealthCloudGA__CarePlanTemplate__c study =
            (HealthCloudGA__CarePlanTemplate__c) new DomainObjects.Study_t()
                .persist();
    }

    @IsTest
    public static void doTest() {
        Id studyId = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1].Id;
        VTR5_IRBLanguageConfiguration__c config;
        List<VTR5_IRBLanguageConfiguration__c> configs;

        // Test Insert
        config = new VTR5_IRBLanguageConfiguration__c(
            VTR5_Country__c = 'US',
            VTR5_DefaultIRBLanguage__c = 'en_US',
            VTR5_IRBApprovedLanguages__c = 'en_US;pt_BR',
            VTR5_Study__c = studyId
        );
        insert config;
        configs = getConfigsForStudy(studyId);
        checkConfigs(config, configs);

        // Test Update
        config.VTR5_Country__c = 'BR';
        config.VTR5_DefaultIRBLanguage__c = 'pt_BR';
        update config;
        configs = getConfigsForStudy(studyId);
    }

    private static List<VTR5_IRBLanguageConfiguration__c> getConfigsForStudy(Id studyId) {
        return [
            SELECT Id, VTR5_Country__c, VTR5_DefaultIRBLanguage__c
            FROM VTR5_IRBLanguageConfiguration__c
            WHERE VTR5_Study__c = :studyId
        ];
    }

    private static void checkConfigs(VTR5_IRBLanguageConfiguration__c expected,
            List<VTR5_IRBLanguageConfiguration__c> actual) {
        System.assertEquals(1, actual.size());
        System.assertEquals(expected.VTR5_Country__c, actual[0].VTR5_Country__c);
        System.assertEquals(expected.VTR5_DefaultIRBLanguage__c, actual[0].VTR5_DefaultIRBLanguage__c);
    }
}