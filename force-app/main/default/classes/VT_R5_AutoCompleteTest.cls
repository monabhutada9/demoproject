@isTest
public class VT_R5_AutoCompleteTest {

    static testmethod void fetchFieldDescribeTest(){

        Test.startTest();
        Map < String, Map < String, String >> fieldValueMap =VT_R5_AutoComplete.fetchFieldDescribe('VTR2_Primary_Language__c','contact','');

        HealthCloudGA__CarePlanTemplate__c carePlan = new HealthCloudGA__CarePlanTemplate__c(
            Name = 'eCOA CarePlan',
            VTR5_eCOA_Guid__c = 'study_79826f74-6967-4eb0-bc99-f50629e66f0c',
            VTR5_eDiaryTool__c = 'eCOA'

        );
        insert carePlan;

        Virtual_Site__c site = new Virtual_Site__c(
            VTD1_Study__c = carePlan.Id,
            VTR5_IRBApprovedLanguages__c = 'en_US;de;ru',
            VTR5_DefaultIRBLanguage__c = 'de',
            VTD1_Study_Site_Number__c = '123'
        );
        insert site;

        Map < String, Map < String, String >> fieldMapVirtualSite =VT_R5_AutoComplete.fetchFieldDescribe('VTR2_Primary_Language__c','contact',site.Id);
        Test.stopTest();
    }
}