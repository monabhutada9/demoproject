@IsTest
public class VH_VT_R2_SstmSharingHandlerTest {
    
    @TestSetup
    static void testSetup() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        User SCRUser = VT_D1_TestUtils.createUserByProfile('Site Coordinator', 'SCRTestUser');
        User PiUser = VT_D1_TestUtils.createUserByProfile('Primary Investigator', 'PiTestUser');
        VT_D1_TestUtils.persistUsers();
        Test.stopTest();
        Study_Team_Member__c stmPI = new Study_Team_Member__c(User__c = PiUser.Id, Study__c = study.Id);
        Study_Team_Member__c stmSCR = new Study_Team_Member__c(User__c = SCRUser.Id, Study__c = study.Id);
        insert stmPI;
        insert stmSCR;
        List<Study_Site_Team_Member__c> sstmList = new List<Study_Site_Team_Member__c>();
        sstmList.add(new Study_Site_Team_Member__c(VTR2_Associated_PI__c = stmPI.Id, VTR2_Associated_SCr__c = stmSCR.Id, VTR2_SCr_type__c = 'Primary'));
        sstmList.add(new Study_Site_Team_Member__c(VTR2_Associated_PI__c = stmPI.Id, VTR2_Associated_SCr__c = stmSCR.Id, VTR2_SCr_type__c = 'Backup'));
        insert sstmList;
    }

    @IsTest static void testValidateSCRsCount() {
        Study_Team_Member__c stmPI = [SELECT Id FROM Study_Team_Member__c WHERE User__r.FirstName = 'PiTestUser' LIMIT 1];
        Study_Team_Member__c stmSCR = [SELECT Id FROM Study_Team_Member__c WHERE User__r.FirstName = 'SCRTestUser' LIMIT 1];
        List<Study_Site_Team_Member__c> sstmList = new List<Study_Site_Team_Member__c>();
        sstmList.add(new Study_Site_Team_Member__c(VTR2_Associated_PI__c = stmPI.Id, VTR2_Associated_SCr__c = stmSCR.Id, VTR2_SCr_type__c = 'Primary'));
        sstmList.add(new Study_Site_Team_Member__c(VTR2_Associated_PI__c = stmPI.Id, VTR2_Associated_SCr__c = stmSCR.Id, VTR2_SCr_type__c = 'Backup'));
        Test.startTest();
        try {
            insert sstmList;
        } catch (DmlException e) {
            System.assertEquals(null, sstmList[0].Id);
            System.assertEquals(null, sstmList[1].Id);
        }
        Test.stopTest();
    }

    @IsTest static void testDelete() {
        List<Study_Site_Team_Member__c> sstmList = [SELECT Id FROM Study_Site_Team_Member__c];
        Test.startTest();
        update sstmList;
        delete sstmList;
        Test.stopTest();
    }
}