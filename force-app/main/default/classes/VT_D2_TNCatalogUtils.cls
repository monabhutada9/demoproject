/**
 * Created by MPlatonov on 25.01.2019.
 */

public with sharing class VT_D2_TNCatalogUtils {
    public static final String TYPE_TEXT = 'TEXT';
    public static final String TYPE_FIELD = 'FIELD';
    public static final String TYPE_FUNCTION_LEFT = 'LEFT';
    public static final String TYPE_FUNCTION_RIGHT = 'RIGHT';
    public static final String TYPE_FUNCTION_IF = 'IF';
    public static final String TYPE_FUNCTION_PREDICATE = 'PREDICATE';
    public static final String TYPE_FUNCTION_ISBLANK = 'ISBLANK';
    public static final String TYPE_FUNCTION_NOT = 'NOT';
    public static final String TYPE_FUNCTION_NOW = 'NOW';
    public static final String TYPE_FUNCTION_TODAY = 'TODAY';
    public static final String TYPE_FUNCTION_FORMAT = 'FORMAT';

    public static final String TYPE_OPERATOR_EQUALS = '=';
    public static final String TYPE_OPERATOR_NOT_EQUALS1 = '!=';
    public static final String TYPE_OPERATOR_NOT_EQUALS2 = '<>';
    public static final String TYPE_OPERATOR_MORE = '>';
    public static final String TYPE_OPERATOR_LESS = '<';
    public static final String TYPE_OPERATOR_MORE_EQUALS = '>=';
    public static final String TYPE_OPERATOR_LESS_EQUALS = '<=';
    public static final String TYPE_OPERATOR_PLUS = '+';
    public static final String TYPE_OPERATOR_MINUS = '-';
    public static final String TYPE_OPERATOR_AND = 'AND';
    public static final String TYPE_OPERATOR_OR = 'OR';

    public static final List <String> COMP_OPERATORS = new List<String>();
    static {
        COMP_OPERATORS.add(TYPE_OPERATOR_NOT_EQUALS1);
        COMP_OPERATORS.add(TYPE_OPERATOR_NOT_EQUALS2);
        COMP_OPERATORS.add(TYPE_OPERATOR_EQUALS);
        COMP_OPERATORS.add(TYPE_OPERATOR_MORE_EQUALS);
        COMP_OPERATORS.add(TYPE_OPERATOR_LESS_EQUALS);
        COMP_OPERATORS.add(TYPE_OPERATOR_MORE);
        COMP_OPERATORS.add(TYPE_OPERATOR_LESS);
    }

    public static final List <String> LOGIC_OPERATORS = new List<String>{TYPE_OPERATOR_AND, TYPE_OPERATOR_OR};
    public static final List <String> ARITHMETIC_OPERATORS = new List<String>{TYPE_OPERATOR_PLUS};
    public static final Set <String> ARITHMETIC_OPERATORS_SET = new Set<String>{TYPE_OPERATOR_PLUS, TYPE_OPERATOR_MINUS};

    public static Map <String, Object> settingsMap = new Map<String, Object>();
    public static String catalogueType = '';
    static {
        VTD1_RTId__c settings = VTD1_RTId__c.getInstance();
        Map<String, Schema.SObjectField> infoMap = Schema.SObjectType.VTD1_RTId__c.fields.getMap();
        for (String name : infoMap.keySet()) {
            settingsMap.put(name.toLowerCase(), settings.get(name));
        }
    }

    public class ExpressionComponent {
        public final String type;
        public final String text;
        public final List <ExpressionComponent> components;
        public final List <Object> args;
        public ExpressionComponent(String type, String text, List <ExpressionComponent> components, List <Object> args) {
            this.type = type;
            this.text = text;
            this.components = components;
            this.args = args;
        }
    }

    // find appropriate closed bracket in the text for opened bracket
    public static Integer findCloseBracketPosition(String text, Integer openBracketPosition) {
        Integer openBracketsCount = 1;
        Integer curPos = openBracketPosition;
        while (openBracketsCount > 0) {
            Integer testPos1 = text.indexOf('(', curPos + 1);
            Integer testPos2 = text.indexOf(')', curPos + 1);
            if (testPos1 < 0 && testPos2 < 0) {
                break;
            }else if (testPos1 >=0 && testPos1 < testPos2) {
                openBracketsCount++;
                curPos = testPos1;
            } else if (testPos2 < testPos1 || testPos1 < 0) {
                openBracketsCount--;
                curPos = testPos2;
            }
        }
        if (curPos > openBracketPosition) {
            return curPos;
        }
        else {
            return -1;
        }
    }

    // find appropriate comma position in the text
    public static Integer findCommaPosition(String text, Integer startPosition, Integer openBracketsCount) {
        Integer curPos = startPosition;
        Integer attempts = 0;
        while ((curPos >= 0 || openBracketsCount > 0) && attempts < 100) {
            attempts ++;
            Integer testPos1 = text.indexOf('(', curPos + 1);
            Integer testPos2 = text.indexOf(')', curPos + 1);
            Integer testPos3 = text.indexOf(',', curPos + 1);
            if (openBracketsCount == 1 && testPos3 < testPos1 && testPos3 < testPos2) {
                testPos3 = text.indexOf(',', curPos + 1);
                curPos = testPos3;
                break;
            }

            if (testPos1 >= 0 && testPos1 < testPos2) {
                openBracketsCount ++;
                curPos = testPos1;
            } else if (testPos2 >= 0 && testPos2 < testPos1) {
                openBracketsCount --;
                curPos = testPos2;
            }
            if (openBracketsCount == 1) {
                testPos3 = text.indexOf(',', curPos + 1);
                curPos = testPos3;
                break;
            }
        }
        return curPos;
    }

    public static List <ExpressionComponent> parsePredicate(String text, String sObjectName, Set <String> fields, List <String> queryParts, Boolean isForOwner) {
        List <ExpressionComponent> components = new List<ExpressionComponent>();
        Integer operatorPosistion = -1;
        Integer rightOperandPosition = -1;
        String compOperator = null;
        String logicOperator = null;

        if (text.toUpperCase().trim().indexOf('(') == 0) {
            Integer openBracketPosition = text.indexOf('(');
            Integer closeBracketPosition = findCloseBracketPosition(text.substring(openBracketPosition), openBracketPosition);
            return parsePredicate(text.trim().substring(openBracketPosition + 1, closeBracketPosition), sObjectName, fields, queryParts, isForOwner);
        } else {
            for (String operator : LOGIC_OPERATORS) {
                Integer curPosistion = text.indexOf(operator);
                if (curPosistion >= 0 && (curPosistion < operatorPosistion || operatorPosistion < 0)) {
                    logicOperator = operator;
                    operatorPosistion = curPosistion;
                }
            }

            if (logicOperator != null) {
                List <ExpressionComponent> leftOperand = parsePredicate(text.substring(0, operatorPosistion), sObjectName, fields, queryParts, isForOwner);
                List <ExpressionComponent> rightOperand = parsePredicate(text.substring(operatorPosistion + logicOperator.length()), sObjectName, fields, queryParts, isForOwner);
                components.add(new ExpressionComponent(logicOperator, null, null, new List <Object> {leftOperand, rightOperand}));
                return components;
            }
        }
        if (text.toUpperCase().trim().indexOf(TYPE_FUNCTION_ISBLANK) == 0) {
            Integer openBracketPosition = text.indexOf('(');
            Integer closeBracketPosition = findCloseBracketPosition(text, openBracketPosition);
            List <ExpressionComponent> innerComponents = parseExpression(text.substring(openBracketPosition + 1, closeBracketPosition), sObjectName, fields, queryParts, isForOwner);
            components.add(new ExpressionComponent(TYPE_FUNCTION_ISBLANK, null, null, new List <Object> {innerComponents}));
        } else if (text.toUpperCase().trim().indexOf(TYPE_FUNCTION_NOT) == 0) {
            Integer openBracketPosition = text.indexOf('(');
            Integer closeBracketPosition = findCloseBracketPosition(text, openBracketPosition);
            List <ExpressionComponent> innerComponents = parsePredicate(text.substring(openBracketPosition + 1, closeBracketPosition), sObjectName, fields, queryParts, isForOwner);
            components.add(new ExpressionComponent(TYPE_FUNCTION_NOT, null, null, new List <Object> {innerComponents}));
        } else {
            for (String operator : COMP_OPERATORS) {
                operatorPosistion = text.indexOf(operator);
                if (operatorPosistion >= 0) {
                    compOperator = operator;
                    break;
                }
            }

            if (compOperator != null) {
                List <ExpressionComponent> leftOperand = parseExpression(text.substring(0, operatorPosistion), sObjectName, fields, queryParts, isForOwner);
                List <ExpressionComponent> rightOperand = parseExpression(text.substring(operatorPosistion + compOperator.length()), sObjectName, fields, queryParts, isForOwner);
                components.add(new ExpressionComponent(compOperator, null, null, new List <Object>{leftOperand, rightOperand}));
            }
        }
        return components;
    }

    public static List <ExpressionComponent> parseExpression(String text, String sObjectName, Set <String> fields, List <String> queryParts, Boolean isForOwner) {
        List <ExpressionComponent> components = new List<ExpressionComponent>();
        if (text == null) {
            return null;
        }
        List <String> formulaKeyWords = new List <String>{TYPE_FUNCTION_LEFT, TYPE_FUNCTION_RIGHT, TYPE_FUNCTION_IF, TYPE_FUNCTION_ISBLANK, TYPE_FUNCTION_NOW, TYPE_FUNCTION_TODAY, TYPE_FUNCTION_FORMAT, TYPE_OPERATOR_PLUS, TYPE_OPERATOR_MINUS};
        for (String testKeyWord : formulaKeyWords) {
            Integer position = text.toUpperCase().trim().indexOf(testKeyWord);
            if (position == 0) {
                Integer openBracketPosition = text.indexOf('(', position);
                Integer closeBracketPosition = findCloseBracketPosition(text, openBracketPosition);
                if (text.toUpperCase().trim().indexOf(TYPE_FUNCTION_IF) == 0) {
                    Integer commaPosition = findCommaPosition(text, 0, 0);
                    List <ExpressionComponent> predicateExpression = parsePredicate(text.substring(openBracketPosition + 1, commaPosition), sObjectName, fields, queryParts, false);
                    Integer commaPosition2 = findCommaPosition(text, commaPosition, 1);
                    List <ExpressionComponent> positiveExpression = parseExpression(text.substring(commaPosition + 1, commaPosition2), sObjectName, fields, queryParts, isForOwner);
                    List <ExpressionComponent> negativeExpression = parseExpression(text.substring(commaPosition2 + 1, closeBracketPosition), sObjectName, fields, queryParts, isForOwner);
                    components.add(new ExpressionComponent(TYPE_FUNCTION_IF, null, null, new List <Object>{predicateExpression, positiveExpression, negativeExpression}));
                    List <ExpressionComponent> innerComponents = null;
                    Integer ampPosition = text.indexOf('&', closeBracketPosition);
                    if (ampPosition >= 0) {
                        innerComponents = parseExpression(text.substring(ampPosition + 1), sObjectName, fields, queryParts, isForOwner);
                    }
                    if (innerComponents != null) {
                        components.addAll(innerComponents);
                    }
                    return components;
                }
                Integer operatorPosistion = -1;
                Integer rightOperandPosition = -1;
                String arithmeticOperator = null;
                for (String operator : ARITHMETIC_OPERATORS) {
                    Integer curPosistion = text.indexOf(operator);
                    if (curPosistion >= 0 && (curPosistion < operatorPosistion || operatorPosistion < 0)) {
                        arithmeticOperator = operator;
                        operatorPosistion = curPosistion;
                    }
                }
                if (arithmeticOperator != null) {
                    List <ExpressionComponent> leftOperand = parseExpression(text.substring(0, operatorPosistion), sObjectName, fields, queryParts, isForOwner);
                    List <ExpressionComponent> rightOperand = parseExpression(text.substring(operatorPosistion + arithmeticOperator.length()), sObjectName, fields, queryParts, isForOwner);
                    components.add(new ExpressionComponent(arithmeticOperator, null, null, new List <Object> {leftOperand, rightOperand}));
                    return components;
                }
                if (text.toUpperCase().trim().indexOf(TYPE_FUNCTION_ISBLANK) == 0) {
                    List <ExpressionComponent> innerComponents = parseExpression(text.substring(openBracketPosition + 1, closeBracketPosition), sObjectName, fields, queryParts, isForOwner);
                    components.add(new ExpressionComponent(testKeyWord, null, innerComponents, null));
                } else if (text.toUpperCase().trim().indexOf(TYPE_FUNCTION_FORMAT) == 0) {
                    String formatStr = text.substring(openBracketPosition + 1, closeBracketPosition);
                    List <String> parts = formatStr.split(',');
                    List <String> partsJoin = new List<String>();
                    Boolean isJoin = false;
                    String firstPart = null;
                    String lastPart = null;
                    for (String part : parts) {
                        Integer pos = part.indexOf('\'');
                        if (pos >= 0 && part.indexOf('\'', pos + 1) < 0) {
                            if (!isJoin) {
                                isJoin = true;
                                firstPart = part;
                            } else {
                                isJoin = false;
                                lastPart = part;
                            }

                        }
                        if (!isJoin) {
                            if (lastPart != null) {
                                partsJoin[partsJoin.size() - 1] += ',' + lastPart.replace('\'', '');
                            } else {
                                partsJoin.add(part.replace('\'', ''));
                            }
                            lastPart = null;
                        } else {
                            if (firstPart != null) {
                                partsJoin.add(part.replace('\'', ''));
                            } else {
                                partsJoin[partsJoin.size() - 1] += ',' + lastPart.replace('\'', '');
                            }
                            firstPart = null;
                        }
                    }
                    parts = partsJoin;
                    List <List <ExpressionComponent>> args = new List<List<ExpressionComponent>>();
                    List <String> operatorParts = null;
                    for (String part : parts) {
                        for (String operator : ARITHMETIC_OPERATORS_SET) {
                            operatorParts = part.split('\\' + operator);
                            if (operatorParts.size() > 1) {
                                Integer charCode = operatorParts[1].trim().charAt(0);
                                // check if is it a digit
                                if (charCode >= 48 && charCode <= 57) {
                                    List <ExpressionComponent> innerComponents1 = parseExpression(operatorParts[0], sObjectName, fields, queryParts, isForOwner);
                                    List <ExpressionComponent> innerComponents2 = parseExpression(operatorParts[1], sObjectName, fields, queryParts, isForOwner);
                                    ExpressionComponent arithmeticComponent = new ExpressionComponent(operator, null, null, new List <Object>{
                                            innerComponents1, innerComponents2
                                    });
                                    args.add(new List <ExpressionComponent>{arithmeticComponent});
                                    break;
                                } else {
                                    args.add(parseExpression(part, sObjectName, fields, queryParts, isForOwner));
                                    break;
                                }
                            }
                        }
                        if (operatorParts.size() == 1)
                            args.add(parseExpression(part, sObjectName, fields, queryParts, isForOwner));
                    }
                    components.add(new ExpressionComponent(testKeyWord, null, null, args));
                    List <ExpressionComponent> innerComponents = null;
                    Integer ampPosition = text.indexOf('&', closeBracketPosition);
                    if (ampPosition >= 0) {
                        innerComponents = parseExpression(text.substring(ampPosition + 1), sObjectName, fields, queryParts, isForOwner);
                        components.addAll(innerComponents);
                    }
                    return components;
                } else if (text.toUpperCase().trim().indexOf(TYPE_FUNCTION_NOW) == 0 || text.toUpperCase().trim().indexOf(TYPE_FUNCTION_TODAY) == 0) {
                    components.add(new ExpressionComponent(testKeyWord, null, null, null));
                    List <ExpressionComponent> innerComponents = null;
                    Integer ampPosition = text.indexOf('&', 0);
                    if (ampPosition >= 0) {
                        innerComponents = parseExpression(text.substring(ampPosition + 1), sObjectName, fields, queryParts, isForOwner);
                        components.addAll(innerComponents);
                    }
                    return components;
                } else {
                    operatorPosistion = -1;
                    Integer commaPosition = text.indexOf(',', openBracketPosition);
                    for (Integer i = closeBracketPosition - 1; i >= 0; i --) {
                        if (text.substring(i, i + 1) == ',') {
                            commaPosition = i;
                            break;
                        }
                    }
                    List <ExpressionComponent> innerComponents = parseExpression(text.substring(openBracketPosition + 1, commaPosition), sObjectName, fields, queryParts, isForOwner);
                    closeBracketPosition = text.indexOf(')', commaPosition);
                    Integer argValue = Integer.valueOf(text.substring(commaPosition + 1, closeBracketPosition).trim());
                    ExpressionComponent component = new ExpressionComponent(testKeyWord, null, innerComponents, new List <Integer>{argValue});
                    components.add(component);
                    text = text.substring(closeBracketPosition + 1);
                }
                return components;
            }
        }

        Integer ampPosition = text.indexOf('&');
        String leftOperand = null;
        String rightOperand = null;
        if (ampPosition >= 0) {
            leftOperand = text.substring(0, ampPosition);
            rightOperand = text.substring(ampPosition + 1);
        } else {
            leftOperand = text;
        }
        if (leftOperand.indexOf(sObjectName + '.') >= 0 && leftOperand.indexOf('$Setup') < 0) {
            Integer delimeterPosition = leftOperand.indexOf('.');
            String fieldName = leftOperand.substring(delimeterPosition + 1);
            components.add(new ExpressionComponent(TYPE_FIELD, fieldName, null, null));
            String fieldOwnerName = null;
            String fieldOwnerProfileName = null;
            if (isForOwner) {
                fieldOwnerName = fieldName.trim();
                fieldOwnerProfileName = fieldName.trim();
                if (fieldName.trim().endsWith('__c')) {
                    fieldOwnerName = fieldOwnerName.substring(0, fieldOwnerName.length() - 1) + 'r.isActive';
                    fieldOwnerProfileName = fieldOwnerProfileName.substring(0, fieldOwnerProfileName.length() - 1) + 'r.Profile.Name';
                } else if (fieldName.trim().endsWith('.Id')) {
                    fieldOwnerName = fieldOwnerName.substring(0, fieldOwnerName.length() - 2) + 'isActive';
                    fieldOwnerProfileName = fieldOwnerProfileName.substring(0, fieldOwnerProfileName.length() - 2) + 'Profile.Name';
                } else if (fieldName.trim().endsWith('Id')) {
                    fieldOwnerName = fieldOwnerName.substring(0, fieldOwnerName.length() - 2) + '.isActive';
                    fieldOwnerProfileName = fieldOwnerProfileName.substring(0, fieldOwnerProfileName.length() - 2) + '.Profile.Name';
                }
                if (fieldOwnerName.substring(0, 1) == '.') {
                    fieldOwnerName = fieldOwnerName.substring(1);
                }
                if (fieldOwnerProfileName.substring(0, 1) == '.') {
                    fieldOwnerProfileName = fieldOwnerProfileName.substring(1);
                }
            }
            if (!fields.contains(fieldName.trim().toLowerCase()) || (isForOwner && !fields.contains(fieldOwnerName))) {
                if (!fields.contains(fieldName.trim().toLowerCase())) {
                    if (fields.isEmpty()) {
                        queryParts.add(fieldName.trim());
                    } else {
                        queryParts.add(',' + fieldName.trim());
                    }
                }
                if (fieldOwnerName != null) {
                    queryParts.add(',' + fieldOwnerName + ',' + fieldOwnerProfileName);
                    fields.add(fieldOwnerName);
                    fields.add(fieldOwnerProfileName);
                }
                fields.add(fieldName.trim().toLowerCase());
            }
        } else {
            components.add(new ExpressionComponent(TYPE_TEXT, leftOperand.trim().replace('"', ''), null, null));
        }

        if (rightOperand != null) {
            components.addAll(parseExpression(rightOperand, sObjectName, fields, queryParts, isForOwner));
        }
        return components;
    }
    public static Boolean evalPredicate(List <ExpressionComponent> components, SObject sobj, Boolean isForOwnerActive, Boolean isForOwnerProfileName) {
        for (ExpressionComponent component : components) {
            if (component.type == TYPE_FUNCTION_ISBLANK) {
                Object content = evalExpression((List <ExpressionComponent>)component.args[0], sobj, isForOwnerActive, isForOwnerProfileName);
                return content == null;
            } else if (component.type == TYPE_OPERATOR_EQUALS || component.type == TYPE_OPERATOR_NOT_EQUALS1 || component.type == TYPE_OPERATOR_NOT_EQUALS2) {
                Object result1 = evalExpression((List <ExpressionComponent>)component.args[0], sobj, isForOwnerActive, isForOwnerProfileName);
                Object result2 = evalExpression((List <ExpressionComponent>)component.args[1], sobj, isForOwnerActive, isForOwnerProfileName);
                if (result1 == 'null') result1 = null;
                if (result2 == 'null') result2 = null;
                if (('' + result1).toLowerCase() == 'true') {
                    result1 = true;
                } else if (('' + result1).toLowerCase() == 'false') {
                    result1 = false;
                }
                if (('' + result2).toLowerCase() == 'true') {
                    result2 = true;
                } else if (('' + result2).toLowerCase() == 'false') {
                    result2 = false;
                }
                return component.type == TYPE_OPERATOR_EQUALS ? result1 == result2 : result1 != result2;
            } else if (component.type == TYPE_FUNCTION_NOT) {
                Boolean result = evalPredicate((List <ExpressionComponent>)component.args[0], sobj, isForOwnerActive, isForOwnerProfileName);
                return !result;
            } else if (component.type == TYPE_OPERATOR_AND || component.type == TYPE_OPERATOR_OR) {
                Boolean result1 = evalPredicate((List <ExpressionComponent>)component.args[0], sobj, isForOwnerActive, isForOwnerProfileName);
                Boolean result2 = evalPredicate((List <ExpressionComponent>)component.args[1], sobj, isForOwnerActive, isForOwnerProfileName);
                if (component.type == TYPE_OPERATOR_AND)
                    return result1 && result2;
                else
                        return result1 || result2;
            }
        }
        return false;
    }

    public static Object evalExpression(List <ExpressionComponent> components, SObject sobj, Boolean isForOwnerActive, Boolean isForOwnerProfileName) {
        return evalExpression(components, sobj, new List <Object>(), isForOwnerActive, isForOwnerProfileName);
    }

    private static Object evalExpression(List <ExpressionComponent> components, SObject sobj, List <Object> parts, Boolean isForOwnerActive, Boolean isForOwnerProfileName) {
        for (ExpressionComponent component : components) {
            if (component.type.toUpperCase() == TYPE_FIELD && component.type.toUpperCase() != TYPE_FUNCTION_LEFT) {
                parts.add(getFieldValue(sobj, component.text, isForOwnerActive, isForOwnerProfileName));
            } else if (component.type.toUpperCase() == TYPE_FUNCTION_LEFT) {
                String value = (String)evalExpression(component.components, sobj, isForOwnerActive, isForOwnerProfileName);
                if (value == null)
                    return '';
                if (value.length() > (Integer)component.args[0]) {
                    value = value.substring(0, (Integer)component.args[0]);
                }
                parts.add(value);
            }  else if (component.type == TYPE_OPERATOR_PLUS) {
                Object result1 = evalExpression((List <ExpressionComponent>)component.args[0], sobj, isForOwnerActive, isForOwnerProfileName);
                Object result2 = evalExpression((List <ExpressionComponent>)component.args[1], sobj, isForOwnerActive, isForOwnerProfileName);
                if (result1 instanceof Date && result2 instanceof String) {
                    Date d = (Date)result1;
                    String inc = (String)result2;
                    String incValue = inc.substring(0, inc.length() - 1);
                    String incType = inc.substring(inc.length() - 1);
                    if (incType == 'd') {
                        return d.addDays(Integer.valueOf(incValue));
                    }
                } else if (result1 instanceof Datetime && result2 instanceof String) {
                    Datetime dt = (Datetime)result1;
                    String inc = (String)result2;
                    String incValue = inc.substring(0, inc.length() - 1);
                    String incType = inc.substring(inc.length() - 1);
                    if (incType == 'h') {
                        return dt.addHours(Integer.valueOf(incValue));
                    } else if (incType == 'd') {
                        return dt.addDays(Integer.valueOf(incValue));
                    } else if (incType == 'm') {
                        return dt.addMinutes(Integer.valueOf(incValue));
                    }
                }
            } else if (component.type.toUpperCase() == TYPE_FUNCTION_IF) {
                Boolean result = evalPredicate((List <ExpressionComponent>) component.args[0], sobj, false, false);
                if (result) {
                    parts.add(evalExpression((List <ExpressionComponent>) component.args[1], sobj, isForOwnerActive, isForOwnerProfileName));
                } else {
                    parts.add(evalExpression((List <ExpressionComponent>) component.args[2], sobj, isForOwnerActive, isForOwnerProfileName));
                }
            } else if (component.type.toUpperCase() == TYPE_FUNCTION_NOW) {
                parts.add(Datetime.now());
            } else if (component.type.toUpperCase() == TYPE_FUNCTION_TODAY) {
                parts.add(System.today());
            } else if (component.type.toUpperCase() == TYPE_FUNCTION_FORMAT) {
                Object dt = null;
                List <ExpressionComponent> argComponents = (List <ExpressionComponent>)component.args[0];
                if (ARITHMETIC_OPERATORS_SET.contains(argComponents[0].type)) {
                    List <ExpressionComponent> components1 = (List <ExpressionComponent>)argComponents[0].args[0];
                    List <ExpressionComponent> components2 = (List <ExpressionComponent>)argComponents[0].args[1];
                    dt = evalExpression((List <ExpressionComponent>) components1, sobj, isForOwnerActive, isForOwnerProfileName);
                    String inc = (String)evalExpression((List <ExpressionComponent>) components2, sobj, isForOwnerActive, isForOwnerProfileName);
                    Integer incValue = Integer.valueOf(inc.substring(0, inc.length() - 1));
                    String incType = inc.substring(inc.length() - 1);
                    if (argComponents[0].type == TYPE_OPERATOR_MINUS) {
                        incValue = -incValue;
                    }
                    if (incType.equals('d')) {
                        dt = ((Datetime)dt).addDays(incValue);
                    } else if (incType.equals('M')) {
                        dt = ((Datetime)dt).addMonths(incValue);
                    } else if (incType.equals('y')) {
                        dt = ((Datetime)dt).addYears(incValue);
                    } else if (incType.equals('m')) {
                        dt = ((Datetime)dt).addMinutes(incValue);
                    } else if (incType.equals('h')) {
                        dt = ((Datetime)dt).addHours(incValue);
                    } else if (incType.equals('s')) {
                        dt = ((Datetime)dt).addSeconds(incValue);
                    }
                } else {
                    dt = evalExpression((List <ExpressionComponent>) component.args[0], sobj, isForOwnerActive, isForOwnerProfileName);
                }
                Object formatExpression = evalExpression((List <ExpressionComponent>) component.args[1], sobj, isForOwnerActive, isForOwnerProfileName);
                Object timeZone = component.args.size() > 2 ? evalExpression((List <ExpressionComponent>) component.args[2], sobj, isForOwnerActive, isForOwnerProfileName) : null;
                if (dt != null) {
                    String dateString = '';
                    if (timeZone != null) {
                        dateString = ((Datetime) dt).format((String) formatExpression, (String) timeZone);
                    } else {
                        dateString =((Datetime) dt).format((String) formatExpression);
                    }
                    parts.add(dateString + (catalogueType =='Notification'? '#date' : ''));
                    if (component.components != null) {
                        parts.add(evalExpression(component.components, sobj, parts, isForOwnerActive, isForOwnerProfileName));
                    }
                }
            } else if (component.text.trim().indexOf('$Setup') >= 0) {
                List <String> partsLocal = component.text.split('\\.');
                Object settingsValue = settingsMap.get(partsLocal[partsLocal.size() - 1].toLowerCase());
                parts.add(settingsValue);
            } else {
                parts.add(component.text.replace('%amp%', '&'));
            }
        }
        if (parts.size() == 1) {
            return parts[0];
        } else {
            for (Integer i = 0; i < parts.size(); i ++) {
                Object obj = parts[i];
                if (obj instanceof Datetime) {
                    obj = ((Datetime)obj).format('M/d/yyyy hh:mm a');
                    parts[i] = obj;
                }
            }
            List <String> strValues = new List<String>();
            for (Object obj : parts) {
                strValues.add('' + (obj != null ? obj : ''));
            }
            return String.join(strValues, '');
        }
    }

    public static Object getFieldValue(SObject sobj, String fieldName, Boolean isForOwnerActive, Boolean isForOwnerProfileName) {
        if (fieldName == null)
            return null;
        List <String> fieldParts = fieldName.split('\\.');
        SObject prevObj = sobj;
        for (Integer i = 0; i < fieldParts.size(); i ++) {
            if (prevObj == null) {
                break;
            } else {
                if (i < fieldParts.size() - 1) {
                    prevObj = prevObj.getSObject(fieldParts[i]);
                } else {
                    if (isForOwnerActive) {
                        String ownerFieldName = fieldParts[fieldParts.size() - 1].trim();
                        if (ownerFieldName == 'Id') {
                            if (fieldParts.size() > 1) {
                                ownerFieldName = fieldParts[fieldParts.size() - 2].trim();
                            } else {
                                return prevObj.get('IsActive');
                            }
                        }
                        SObject ownerObject = null;
                        if (ownerFieldName.endsWith('__c')) {
                            ownerObject = prevObj.getSObject(ownerFieldName.substring(0, ownerFieldName.length() - 1) + 'r');
                        } else if (ownerFieldName.endsWith('.Id')) {
                            ownerObject = prevObj.getSObject(ownerFieldName.substring(0, ownerFieldName.length() - 3));
                        } else if (ownerFieldName.endsWith('Id')) {
                            ownerObject = prevObj.getSObject(ownerFieldName.substring(0, ownerFieldName.length() - 2));
                        } else {
                            return prevObj.get('IsActive');
                        }
                        if (ownerObject != null) {
                            return ownerObject.get('IsActive');
                        }
                    } else if (isForOwnerProfileName){
                        String ownerFieldName = fieldParts[fieldParts.size() - 1].trim();
                        if (ownerFieldName == 'Id') {
                            if (fieldParts.size() > 1) {
                                ownerFieldName = fieldParts[fieldParts.size() - 2].trim();
                            } else {
                                return prevObj.getSObject('Profile').get('Name');
                            }
                        }
                        SObject ownerObject = null;
                        if (ownerFieldName.endsWith('__c')) {
                            ownerObject = prevObj.getSObject(ownerFieldName.substring(0, ownerFieldName.length() - 1) + 'r');
                        } else if (ownerFieldName.endsWith('.Id')) {
                            ownerObject = prevObj.getSObject(ownerFieldName.substring(0, ownerFieldName.length() - 3));
                        } else if (ownerFieldName.endsWith('Id')) {
                            ownerObject = prevObj.getSObject(ownerFieldName.substring(0, ownerFieldName.length() - 2));
                        } else {
                            return prevObj.getSObject('Profile').get('Name');
                        }
                        if (ownerObject != null) {
                            return ownerObject.getSObject('Profile').get('Name');
                        }
                    } else {
                        fieldName = fieldParts[i];
                        return prevObj.get(fieldName.trim());
                    }
                }
            }
        }
        return null;
    }

    public static Date parseDate(String s) {
        if (s != null && (s.toLowerCase() == 'today()' || s.toLowerCase() == 'now()')) {
            return System.today();
        } else {
            return null;
        }
    }

    public static Datetime parseDatetime(String s) {
        if (s != null && s.toLowerCase() == 'now()') {
            return System.now();
        } else {
            return null;
        }
    }
}