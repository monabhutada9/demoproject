public class VT_R2_SCPatientEDiary {

    private static Map<String, String> STATUS_LABELS = new Map<String, String>{ // map status API name to custom label for some statues
            'Not Started' => Label.VTR2_Future,
            'Review Required' => Label.VTR2_Completed,
            'Reviewed' => Label.VTR2_Completed,
            'Reviewed (Partially Completed)' => Label.VTR2_Completed
    };
    private static String statusApiName;
    private static String statusLbl;

    @AuraEnabled
    public static SurveysWrapper getSurveys() {
        List<VTD1_Survey__c> surveys = getSurveysRemote();
        System.debug(surveys);
        SurveysWrapper sw = new SurveysWrapper();
        sw.surveys = surveys;
        Long offset = (Datetime.newInstance(Datetime.now().date(), Datetime.now().time()).getTime() -
                Datetime.newInstance(Datetime.now().dateGmt(), Datetime.now().timeGmt()).getTime()) / (60 * 60 * 1000);
        sw.timeZoneOffset = offset;
        return sw;
    }

    public static List<VTD1_Survey__c> getSurveysRemote() {
//        Id currentUserId = UserInfo.getUserId();
//        Id patientId;
//        String profileName = VT_D1_PatientCaregiverBound.getUserProfileName(currentUserId);
//        String profileCG = VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME;
//        if (profileName == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
//            patientId = currentUserId;
//        } else if (profileName == profileCG) {
//            patientId = VT_D1_PatientCaregiverBound.findPatientOfCaregiver(currentUserId);
//        } else {
//            return new List<VTD1_Survey__c>();
//        }
//
//        List<Case> cases = [
//                SELECT Id
//                FROM Case
//                WHERE RecordType.Name = :VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN
//                    AND ContactId IN (
//                            SELECT ContactId
//                            FROM User
//                            WHERE Id = :patientId
//                    )
//        ];
//        System.debug(cases);
//        Datetime now = Datetime.now();
//        String queryStr = 'SELECT Id, Name, VTD1_Protocol_ePRO__r.Name, ' +
//                'VTD1_Status__c, ' +
//                'toLabel(VTD1_Status__c) lblStatus, ' +
//                'VTD1_Protocol_ePRO__c, ' +
//                'VTD1_Due_Date__c, ' +
//                'VTD1_Trigger__c, ' +
//                'VTD1_Start_Time__c' +
//                ' FROM VTD1_Survey__c WHERE VTD1_CSM__c in :cases AND VTD1_Date_Available__c<:now ';
//        System.debug('!!!!' + queryStr);
//        if (profileName == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
//            queryStr = queryStr +
//                    ' AND VTD1_Protocol_ePRO__r.VTD1_Subject__c !=:profileCG';
//        } else {
//            queryStr = queryStr + ' AND (VTD1_Caregiver_User_Id__c != NULL ' +
//                    ' OR VTD1_Protocol_ePRO__r.VTD1_Subject__c =:profileCG)';
//        }
//        queryStr = queryStr + '  ORDER BY VTD1_Due_Date__c';
//
//        System.debug('!!!!' + queryStr);

        String queryStr = 'SELECT ' +
                'Id, ' +
                'Name, ' +
                'VTD1_Protocol_ePRO__r.Name, ' +
                'VTD1_Status__c, ' +
                'VTD1_Protocol_ePRO__c, ' +
                'VTD1_Due_Date__c, ' +
                'VTD1_Trigger__c, ' +
                'VTD1_Start_Time__c, ' +
                'VTD1_CSM__c ' +
                'FROM VTD1_Survey__c ' +
                'WHERE VTD1_Start_Time__c <= TODAY ' +
                'ORDER BY VTD1_Due_Date__c';
        List<VTD1_Survey__c> surveys = Database.query(queryStr);
        VT_D1_TranslateHelper.translate(surveys);
        for (VTD1_Survey__c s : surveys) {
            s.Name = s.VTD1_Protocol_ePRO__r.Name;
        }
        return surveys;
    }


    @AuraEnabled
    public static List<VTD1_Protocol_ePro_Question__c> getQuestionsAnswers(Id surveyId, Id protocolId) {
        List<VTD1_Protocol_ePro_Question__c> questions = [
                SELECT
                        Id,
                        VTR2_QuestionContentLong__c,
                        VTD1_Type__c,
                        VTD1_Options__c,
                        VTD1_Number__c,
                        VTR4_Number__c,
                        VTD1_Group_number__c, (
                            SELECT
                                    Id,
                                    VTD1_Answer__c
                            FROM Survey_Answers__r
                            WHERE VTD1_Survey__c = :surveyId
                            LIMIT 1
                        )
                FROM VTD1_Protocol_ePro_Question__c
                WHERE VTD1_Protocol_ePRO__c = :protocolId
                ORDER BY VTD1_Group_number__c,
                         VTR4_Number__c
        ];
        System.debug('!!!  surveyId ' + surveyId + ' protocolId ' + protocolId);
        VT_D1_TranslateHelper.translate(questions);
        return questions;
    }

    @AuraEnabled
    public static SavedResultsWrapper saveAnswers(String answers, Id surveyId, Boolean submit, String surveyStatus) {
        List<AnswerWrapper> answersList = (List<AnswerWrapper>) JSON.deserialize(answers, List<AnswerWrapper>.class);
        if (answersList == null) {
            throw new AuraHandledException(System.Label.VT_D1_ImpossibleToSave);
        }
        SavedResultsWrapper resultsWrapper = new SavedResultsWrapper();
        setStatusInfo(surveyId);

        if (statusApiName != null) {
            resultsWrapper.sStatus = statusApiName;
            resultsWrapper.lblStatus = statusLbl;
            if (statusApiName != 'In Progress') {
                resultsWrapper.errorMsg = System.Label.VT_D1_CantSubmit;
                return resultsWrapper;
            }
        }
        System.debug('answers  size' + answersList.size());

        List<VTD1_Survey_Answer__c> anss = new List<VTD1_Survey_Answer__c>();

        for (AnswerWrapper an : answersList) {
            System.debug('an.answerId ' + an.answerId + ' na.VTD1_Question__c ' + an.questionId + ' an.answer ' + an.answer);
            VTD1_Survey_Answer__c na = new VTD1_Survey_Answer__c();
            na.Id = an.answerId;
            na.VTD1_Question__c = an.questionId;
            na.VTD1_Answer__c = an.answer;
            if (an.answerId == null) {
                na.VTD1_Survey__c = surveyId;
            }
            anss.add(na);
        }

        List<VTD1_Survey_Answer__c> newAnswers = new List<VTD1_Survey_Answer__c>();
        try {
            Database.UpsertResult[] results = Database.upsert(anss);
            for (Integer index = 0, size = results.size(); index < size; index++) {
                if (results[index].isSuccess()) {
                    if (results[index].isCreated()) {
                        VTD1_Survey_Answer__c na = new VTD1_Survey_Answer__c();
                        na.VTD1_Question__c = anss[index].VTD1_Question__c;
                        na.VTD1_Answer__c = results[index].getId();
                        newAnswers.add(na);
                    }
                }
            }
        } catch (DmlException e) {
            System.debug('!!! Error while updaing answers!');
        }

        if (submit) {
            VTD1_Survey__c sv = new VTD1_Survey__c();
            sv.Id = surveyId;
            sv.VTD1_Completion_Time__c = Date.today();
            sv.VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED;
            try {
                update sv;
                setStatusInfo(surveyId);
                resultsWrapper.sStatus = statusApiName;
                resultsWrapper.lblStatus = statusLbl;
            } catch (DmlException e) {
                newAnswers = null;
                System.debug('!!! Error while submitting survey!');
                throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
            }
        }

        resultsWrapper.answers = newAnswers;
        System.debug('!!! ' + resultsWrapper.answers);
        return resultsWrapper;
    }

    @AuraEnabled
    public static SurveyWrapper updateDateTime(Id surveyId, String status) {
        SurveyWrapper surveyWrapper = new SurveyWrapper();
        setStatusInfo(surveyId);
        VTD1_Survey__c s = new VTD1_Survey__c();
        if (!(statusApiName == 'In Progress' || statusApiName == 'Not Started')) {
            surveyWrapper.errorMsg = System.Label.VT_D1_CantSubmit;
            s.VTD1_Status__c = statusApiName;
            surveyWrapper.sv = s;
            surveyWrapper.lblStatus = statusLbl;
            return surveyWrapper;
        }
        s.Id = surveyId;
        s.VTD1_Start_Time__c = Datetime.now();
        if (status == 'Not Started') {
            s.VTD1_Status__c = 'In Progress';
        }
        try {
            update s;
            setStatusInfo(surveyId);
            s.VTD1_Status__c = statusApiName;
            surveyWrapper.lblStatus = statusLbl;
        } catch (DmlException e) {
            System.debug('!!! Error while updating date of survey start!');
        }
        surveyWrapper.sv = s;
        return surveyWrapper;
    }

    private static void setStatusInfo(Id surveyId) {
        String status;
        List<VTD1_Survey__c> svInDB = [
                SELECT VTD1_Status__c
                FROM VTD1_Survey__c
                WHERE Id = :surveyId
        ];
        if (svInDB.size() != 0) {
            statusApiName = (String) svInDB[0].get('VTD1_Status__c');

            if (STATUS_LABELS.containsKey(statusApiName)) {
                statusLbl = STATUS_LABELS.get(statusApiName);
            } else {
                List<Schema.PicklistEntry> PicklistEntries = VTD1_Survey__c.VTD1_Status__c.getDescribe().getPicklistValues();
                Map<String, String> ApiToLabel = new Map<String, String>();
                for (Schema.PicklistEntry pe : PicklistEntries) {
                    ApiToLabel.put(pe.getValue(), pe.getLabel());
                }
                statusLbl = ApiToLabel.get(statusApiName);
            }
        }
    }

    public with sharing class AnswerWrapper {
        public String questionId;
        public String answerId;
        public String answerType;
        public String answer;
    }

    public class SurveysWrapper {
        @AuraEnabled public List<VTD1_Survey__c> surveys;
        @AuraEnabled public Long timeZoneOffset;
    }

    public class SavedResultsWrapper {
        @AuraEnabled public List<VTD1_Survey_Answer__c> answers;
        @AuraEnabled public String sStatus;
        @AuraEnabled public String errorMsg;
        @AuraEnabled public String lblStatus;
    }
    public class SurveyWrapper {
        @AuraEnabled public VTD1_Survey__c sv;
        @AuraEnabled public String errorMsg;
        @AuraEnabled public String lblStatus;
    }
}