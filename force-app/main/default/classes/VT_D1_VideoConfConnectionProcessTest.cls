/**
 * Created by User on 19/05/14.
 */
@isTest
public with sharing class VT_D1_VideoConfConnectionProcessTest {

    public static void doTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockSendVideoConfSignalHttpResponseGenerator());
        Video_Conference__c videoConference = new Video_Conference__c();
        insert videoConference;

        VTD1_Video_Conf_Connection__c videoConfConnection = new VTD1_Video_Conf_Connection__c();
        videoConfConnection.VTD1_Video_ConfId__c = videoConference.Id;
        videoConfConnection.VTD1_Time_Connection__c = Datetime.now();
        insert videoConfConnection;

        videoConfConnection.VTD1_Time_disconnection__c = Datetime.now().addDays(1);
        
        update videoConfConnection;
        delete videoConfConnection;
        Test.stopTest();
    }

    public class MockSendVideoConfSignalHttpResponseGenerator implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('["test"]');
            res.setStatusCode(200);
            return res;
        }
    }
}