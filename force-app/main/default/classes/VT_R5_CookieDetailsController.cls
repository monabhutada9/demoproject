/*
** Author:              IQVIA Strikers ( Eashan Parlewar )
** Date:                02-Nov-2020
** Group:               http://jira.quintiles.net/browse/SH-19347
** Group-content:       Cookies Opt IN for Community users.
** Description:         Controller to get the details of Cookies from the metadata.
*/
public with sharing class VT_R5_CookieDetailsController {
    public static String domainName;
    public static String ckwrp;
    //public String PUBLISH_STATUS_ONLINE = 'Online';
    /*
    ** Description: Get the details of functional cookies from custom metadata.
    ** Return: return json string.
    */
    @AuraEnabled( Cacheable=true )
    public static String getfunctionalcookiesdata() {
        try {
            list<VTR5_Community_Cookie_List__mdt> CookielistMdt = [ select id, MasterLabel, VTR5_Path__c, VTR5_Domain__c, VTR5_Type__c, VTR5_Sandbox_Domain__c 
                                                                    from VTR5_Community_Cookie_List__mdt where VTR5_Type__c=:'Functional' ];
            Organization org     = [ SELECT IsSandbox FROM Organization LIMIT 1 ]; // To check if the apex class is running in Sandbox.
            if( !Test.isRunningTest() ) { domainName = String.valueOf(Network.getLoginUrl(Network.getNetworkId())); }
            String removeString1 = 'https://';
            domainName           = domainName.replace( removeString1, '' );
            domainName           = domainName.substringBefore('/');
            /*
            ** Create the JSON with the details of cookie ( i.e. Name, Path and Domain ). This JSON is forwarded to the custom component 
            ** in string format.
            */
            ckwrp = '{ "cookies" : [';
            for( integer i = 0; i < CookielistMdt.size(); i++ ) {
                ckwrp += '{ "Name":"'+CookielistMdt[i].MasterLabel+'","Path":"'+CookielistMdt[i].VTR5_Path__c+'","Domain":"';
                if( org.IsSandbox && CookielistMdt[i].VTR5_Sandbox_Domain__c != null ) {
                    ckwrp += CookielistMdt[i].VTR5_Sandbox_Domain__c+'" }'; 
                } else if( CookielistMdt[i].VTR5_Domain__c == '.' ) { 
                    ckwrp += '.'+domainName+'" }';
                } else if( CookielistMdt[i].VTR5_Domain__c == null ) {
                    ckwrp += domainName+'" }';
                }
                if( i < CookielistMdt.size()-1 ) {
                    ckwrp +=',';
                }
            }
            ckwrp += ']}';
            if( Test.isRunningTest()  && domainName.containsIgnoreCase('test.com') ) { throw new VT_D1_Test_DataFactory.MyException('testing'); }
            return ckwrp; // return JSON.
        } catch ( Exception e ) { 
            // Log the exception in the Salesforce org.
            ErrorLogUtility.logException( e, ErrorLogUtility.ApplicationArea.APP_AREA_DELETE_COOKIES, VT_R5_CookieDetailsController.class.getName() );
            throw new AuraHandledException( 'Something went wrong: '+ e.getMessage() );
        }
    }
    /*
    ** Description: Get the details of required cookies from custom metadata.
    ** Return: return list of required cookies in JSON format.
    */

    @AuraEnabled (Cacheable=true )
    public static String getRequiredCookiesData(String lang) {
        try {
            cookieBannerWrapper wrapperResult = new cookieBannerWrapper();
            wrapperResult.cookiesAuthenticatedUsers = new list<VTR5_Community_Cookie_List__mdt>();
            wrapperResult.cookiesNonAuthenticatedUsers = new list<VTR5_Community_Cookie_List__mdt>();

            for(VTR5_Community_Cookie_List__mdt cookieData: [ select id, MasterLabel, VTR5_Path__c, VTR5_Domain__c, VTR5_Type__c, VTR5_Sandbox_Domain__c, VTR5_Custom_label_for_Duration__c, 
            VTR5_Custom_label_for_Description__c, VTR5_Only_Required_ForAuthenticatedUsers__c from VTR5_Community_Cookie_List__mdt where VTR5_Type__c=:'Required' ORDER BY MasterLabel ASC ]){

                if(cookieData.VTR5_Custom_label_for_Duration__c != null){
                    cookieData.VTR5_Custom_label_for_Duration__c    = VT_D1_TranslateHelper.getLabelValue(cookieData.VTR5_Custom_label_for_Duration__c, lang);
                }
                if(cookieData.VTR5_Custom_label_for_Description__c != null){
                    cookieData.VTR5_Custom_label_for_Description__c = VT_D1_TranslateHelper.getLabelValue(cookieData.VTR5_Custom_label_for_Description__c, lang);
                }
                

                if(cookieData.VTR5_Only_Required_ForAuthenticatedUsers__c){
                    wrapperResult.cookiesAuthenticatedUsers.add(cookieData);
                }else{
                    wrapperResult.cookiesNonAuthenticatedUsers.add(cookieData);
                }
                
            }

            List<VTD1_General_Settings__mdt> listOfMetadata = [Select id, 	VTR5_CookiesPolicyDate__c from VTD1_General_Settings__mdt];
            Date CookieLastUpdatedDate = listOfMetadata[0].VTR5_CookiesPolicyDate__c;
            if(!listOfMetadata.isEmpty() && CookieLastUpdatedDate != null){
                wrapperResult.CookieLastUpdatedDate = CookieLastUpdatedDate;
                wrapperResult.cookieUpdatedYear = CookieLastUpdatedDate.year();
                wrapperResult.cookieUpdatedMonth = CookieLastUpdatedDate.month()-1;
                wrapperResult.cookieUpdatedDay= CookieLastUpdatedDate.day();

            }
            ckwrp = JSON.serialize(wrapperResult);
            if( Test.isRunningTest() && 'testDomain'.equals(domainName)) { 
                throw new VT_D1_Test_DataFactory.MyException('testing'); 
            }

            return ckwrp; // return JSON.
        } catch ( Exception e ) { 
            // Log the exception in the Salesforce org.
            ErrorLogUtility.logException( e, ErrorLogUtility.ApplicationArea.APP_AREA_DELETE_COOKIES, VT_R5_CookieDetailsController.class.getName() );
            throw new AuraHandledException( 'Something went wrong: '+ e.getMessage() );
        }
    }


    public class cookieBannerWrapper{
        list<VTR5_Community_Cookie_List__mdt> cookiesAuthenticatedUsers;
        list<VTR5_Community_Cookie_List__mdt> cookiesNonAuthenticatedUsers;

        Date CookieLastUpdatedDate ;
        integer cookieUpdatedYear;
        integer cookieUpdatedMonth;
        integer cookieUpdatedDay;
    }


}