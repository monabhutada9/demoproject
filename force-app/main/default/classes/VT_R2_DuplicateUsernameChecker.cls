/**
* @author: Carl Judge
* @date: 08-Jul-19
* @description: Check for duplicate usernames. Uses code from https://salesforce.stackexchange.com/questions/117664/is-there-any-api-that-allows-me-to-check-if-a-username-is-in-use
**/

@RestResource(UrlMapping='/DuplicateUsernameChecker')
global without sharing class VT_R2_DuplicateUsernameChecker {

    @HttpPost
    global static Boolean doPost(String fullUserName) {
        Boolean result = false;

        if (fullUserName != null) {
            User user = new User();
            user.FirstName = 'any';
            user.LastName = 'any';
            user.Alias = 'any';
            user.Email = 'any@email.com';
            user.Username = fullUserName;
            user.CommunityNickname = fullUserName.length()>40?fullUserName.substring(0, 40):fullUserName;
            user.LocaleSidKey = 'en_US';
            user.LanguageLocaleKey = 'en_US';
            user.EmailEncodingKey = 'UTF-8';
            user.TimeZoneSidKey = 'America/Los_Angeles';
            user.ProfileId = [SELECT Id, Name FROM Profile WHERE Name='Standard User' LIMIT 1].Id;

            Savepoint sp = Database.setSavepoint();

            try {
                insert user;
            }
            catch(DmlException ex) {
                if(ex.getDmlStatusCode(0) == StatusCode.DUPLICATE_USERNAME.name()) {
                    result = true;
                }
            }
            finally {
                Database.rollback(sp);
            }
        }

        return result;
    }
}