/**
 * Created by Danylo Belei on 23.05.2019.
 * Refactored by Maksim Fedarenka on 19.06.2020
 */

@IsTest
public with sharing class VT_D1_HelperClassTest {

    public static void testBehaviour() {
        HealthCloudGA__CarePlanTemplate__c study = (HealthCloudGA__CarePlanTemplate__c) new DomainObjects.Study_t()
            .setMaximumDaysWithoutLoggingIn(1)
            .addAccount(new DomainObjects.Account_t()
                .setRecordTypeId(VT_R4_ConstantsHelper_RecordTypes.RECORD_TYPE_ID_ACCOUNT_SPONSOR)
            )
            .persist();

        DomainObjects.Case_t case_t = new DomainObjects.Case_t()
            .setRecordTypeId(VT_R4_ConstantsHelper_RecordTypes.RECORD_TYPE_ID_CASE_PCF);
        VTD1_Actual_Visit__c visit = (VTD1_Actual_Visit__c) new DomainObjects.VTD1_Actual_Visit_t()
            .addVTD1_Case(case_t)
            .addVTD1_ProtocolVisit_t(
                new DomainObjects.VTD1_ProtocolVisit_t()
                    .setStudy(study.Id)
            )
            .persist();

        Set<Id> idTestSetPCF = VT_D1_HelperClass.getRecordTypePCFSetId();
        Set<Id> idTestSetCarePlan = VT_D1_HelperClass.getRecordTypeCarePlanSetId();

        VT_D1_HelperClass.getActualVisitName(visit, VT_D1_HelperClass.getUserProfileName(UserInfo.getUserId()));
//        VT_D1_HelperClass.sendEmailNotifyToAdmin(
//            new List<User>{ new User(Id = fflib_IDGenerator.generate(User.getSObjectType()), Email = Crypto.getRandomInteger() + '@shtest.com' ) },
//            new EmailTemplate(Body = 'test email body')
//        );
        VT_D1_HelperClass.getOrgWideEmailAddress();

        System.assertEquals('System Administrator', VT_D1_HelperClass.getProfileName(UserInfo.getProfileId()));
        System.assertEquals(UserInfo.getProfileId(), VT_D1_HelperClass.getProfileId('System Administrator'));
        System.assert(!idTestSetPCF.isEmpty());
        System.assert(!idTestSetCarePlan.isEmpty());
        System.assert(VT_D1_HelperClass.isValidId(VT_D1_HelperClass.getIdsFromObjs(new List<User>{ new User(Id = fflib_IDGenerator.generate(User.getSObjectType())) }).get(0)));
        System.assert(!VT_D1_HelperClass.isValidId('invalidId'));
        System.assertEquals('test', VT_D1_HelperClass.getCrossObjectField(new User(Contact = new Contact(FirstName = 'test')), 'Contact.FirstName'));
        System.assertEquals('test', VT_D1_HelperClass.getPatientDeliveryName(new VTD1_Order__c(VTD1_Protocol_Delivery__r = new VTD1_Protocol_Delivery__c(), Name = 'test')));
        System.assertEquals('test_test', VT_D1_HelperClass.conditionEcoaString('test*test'));
        System.assertEquals(0, VT_D1_HelperClass.getBackupStmsForPIs(fflib_IDGenerator.generate(Study_Team_Member__c.getSObjectType())).size());
        System.assertNotEquals(null, VT_D1_HelperClass.getRTMap().get('VTD1_Protocol_Kit__c').get('VTD1_Welcome_to_Study_Package'));
        System.assertNotEquals(null, VT_D1_HelperClass.getRTId('VTD1_Protocol_Kit__c', 'VTD1_Welcome_to_Study_Package'));
        System.assertNotEquals(null, VT_D1_HelperClass.getRecordTypeContactForm());
        System.assertNotEquals(null, VT_D1_HelperClass.getProfileIdByName('Patient'));
        System.assertNotEquals(null, VT_D1_HelperClass.getSessionIdForGuest());
        System.assertNotEquals(null, VT_D1_HelperClass.getSandboxPrefix());
        System.assertNotEquals(null, VT_D1_HelperClass.getProfileNameOfCurrentUser());
        System.assertEquals(3.35, VT_D1_HelperClass.avg(new List<Decimal> {3.4242, 3.2727}));
        System.assertEquals(14, VT_D1_HelperClass.getAllFields(ApexClass.getSObjectType()).size());
        System.assertEquals(null, VT_D1_HelperClass.getEmailTemplate('template'));
    }
}