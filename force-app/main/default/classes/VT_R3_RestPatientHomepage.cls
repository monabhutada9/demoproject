/**
 * Created by Alexander Komarov on 24.05.2019.
 */

@RestResource(UrlMapping='/Patient/Homepage/*')
global with sharing class VT_R3_RestPatientHomepage {
    static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();
    private static final String VISIT_TYPE = Label.VTD2_VisitType;
    private static final String BANNER = 'banner';

    private static Map<String, String> DESTINATIONS = new Map<String, String>{
            'full-profile' => 'Profile',
            'my-study-supplies' => 'StudySupplies',
            'calendar' => 'Calendar',
            'my-diary' => 'Ediary',
            'regulatory-binder' => 'RegulatoryBinder'
    };

    private static Map<String, String> TIMELINE_ITEMS_TYPES = new Map<String, String>{
            Label.VTD2_VisitType => 'Visit',
            Label.VTD2_MilestoneType => 'Milestone',
            Label.VTR5_Intervention => 'Intervention'
    };

    @HttpGet
    global static String getHomepageInfo() {
//        String patientOrCG = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId() LIMIT 1].Name;
        Boolean isCaregiver = VT_D1_HelperClass.getProfileNameOfCurrentUser().equals('Caregiver');

        Case currentCase;
        HealthCloudGA__CarePlanTemplate__c currentStudy;
        if (isCaregiver) {
            Id caseId = VT_D1_PatientCaregiverBound.getPatientCase(UserInfo.getUserId()).Id;
            currentCase = [
                    SELECT Id, VTD1_Study__c, Status, Contact.VTD1_FullHomePage__c,
                            VTD1_Primary_PG__r.MediumPhotoUrl, VTD1_Primary_PG__r.Name, VTD1_PI_user__r.MediumPhotoUrl,
                            VTD1_PI_contact__r.Name, VTR2_SiteCoordinator__r.MediumPhotoUrl, VTR2_SiteCoordinator__r.Name,
                            VTR2_StudyPhoneNumber__r.Name,VTD1_PI_user__c
                    FROM Case
                    WHERE Id = :caseId
                    LIMIT 1
            ];
            currentStudy = [
                    SELECT Id, VTD1_Study_Logo__c, VTD1_Protocol_Nickname__c, Name, VTD1_Study_Library__c, VTR3_LTFU__c
                    FROM HealthCloudGA__CarePlanTemplate__c
                    WHERE Id = :currentCase.VTD1_Study__c
                    LIMIT 1
            ];
        } else {
            currentCase = [
                    SELECT Id, VTD1_Study__c, Status, Contact.VTD1_FullHomePage__c,
                            VTD1_Primary_PG__r.MediumPhotoUrl, VTD1_Primary_PG__r.Name, VTD1_PI_user__r.MediumPhotoUrl,
                            VTD1_PI_contact__r.Name, VTR2_SiteCoordinator__r.MediumPhotoUrl, VTR2_SiteCoordinator__r.Name,
                            VTR2_StudyPhoneNumber__r.Name,VTD1_PI_user__c
                    FROM Case
                    WHERE VTD1_Patient_User__c = :UserInfo.getUserId()
                    AND VTD1_Study__c != NULL
                    AND RecordTypeId = :VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN
                    LIMIT 1
            ];
            currentStudy = [
                    SELECT Id, VTD1_Study_Logo__c, VTD1_Protocol_Nickname__c, Name, VTD1_Study_Library__c, VTR3_LTFU__c
                    FROM HealthCloudGA__CarePlanTemplate__c
                    WHERE Id = :currentCase.VTD1_Study__c
                    LIMIT 1
            ];
        }

        if (currentCase.Contact.VTD1_FullHomePage__c) {
            PostRandomizationHomepage prh = new PostRandomizationHomepage();
            TaskListSection mtls;
            try {
                mtls = formTaskListSection(isCaregiver);
            } catch (Exception e) {
                System.debug('Exception: ' + e.getMessage() + ' ' + e.getStackTraceString());
                mtls = getEmptyTasklistSection();
            }
            TimelineSection tls = formTimelineSection(currentStudy);
            StudyInfoSection sis = formStudyInfoSection(currentCase, currentStudy);
            MyStudyTeamSection msts = formMyStudyTeamSection(currentCase);
            prh.studyInfoSection = sis;
            prh.myStudyTeamSection = msts;
            prh.timelineSection = tls;
            prh.taskListSection = mtls;

            cr.buildResponse(prh);
            return JSON.serialize(cr, true);

        } else {
            PreRandomizationHomepage prh = new PreRandomizationHomepage();

            WelcomeSection ws = formWelcomeSection(currentStudy, currentCase);
            UpNextSection uns = formUpNextSection();
            MyStudyTeamSection msts = formMyStudyTeamSection(currentCase);

            prh.welcomeSection = ws;
            prh.upNextSection = uns;
            prh.myStudyTeamSection = msts;

            cr.buildResponse(prh);
            return JSON.serialize(cr, true);
        }
    }


    private static TaskListSection formTaskListSection(Boolean isCaregiver) {
        List<Task> openTasks = VT_D1_TaskListController.getTasks('Open', null);
        List<Task> closedTasks = VT_D1_TaskListController.getTasks('Completed', null);
        TaskListSection tls = new TaskListSection();
        tls.openTasks = formTasks(openTasks);
        tls.closedTasks = formTasks(closedTasks);
        if (isCaregiver) {
            Id accId = [SELECT Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].Contact.AccountId;
            Case c = [
                    SELECT VTD1_Patient__r.VTD1_First_Name__c, VTD1_Patient__r.VTD1_Last_Name__c
                    FROM Case
                    WHERE AccountId = :accId
                    AND RecordTypeId = :VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN
            ];
            tls.patientFirstName = c.VTD1_Patient__r.VTD1_First_Name__c;
            tls.patientLastName = c.VTD1_Patient__r.VTD1_Last_Name__c;
        }
        tls.openTasksCount = openTasks.size();

        List<Id> newTasksIds = new List<Id>();
        Datetime previousSuccessUserLoginTime = VT_R3_RestHelper.getLastLoginDatetime();

        for (Task t : [SELECT Id FROM Task WHERE CreatedDate > :previousSuccessUserLoginTime AND Status = 'Open']) {
            newTasksIds.add(t.Id);
        }

        tls.newTasksIds = newTasksIds;

        return tls;
    }

    private static TimelineSection formTimelineSection(HealthCloudGA__CarePlanTemplate__c s) {
        TimelineSection tls = new TimelineSection();
        List<TimelineObj> result = new List<VT_R3_RestPatientHomepage.TimelineObj>();
        List<VT_D1_CommunityTimelineController.TimelineItem> temp = new List<VT_D1_CommunityTimelineController.TimelineItem>();
        try {
            temp = VT_D1_CommunityTimelineController.getTimeLineItems(null);
        } catch (Exception e) {
            System.debug('VT_D1_CommunityTimelineController.getTimeLineItems() failed. ' + e.getMessage() + e.getStackTraceString());
        }
        for (VT_D1_CommunityTimelineController.TimelineItem tli : temp) {
            TimelineObj tlo = new TimelineObj();
            tlo.itemTypeTranslated = tli.itemType == null ? '' : tli.itemType;
            tlo.itemType = TIMELINE_ITEMS_TYPES.get(tli.itemType);
            tlo.title = tli.label == null ? '' : tli.label;
            tlo.modality = tli.modality == null ? '' : tli.modality;
            tlo.translatedModality = tli.translatedModality == null ? '' : tli.translatedModality;
            tlo.statusLabel = tli.status == null ? '' : tli.status;
            tlo.status = tli.statusValue == null ? '' : tli.statusValue;
            tlo.subLabel = tli.subLabel == null ? '' : tli.subLabel;
            tlo.lineStatus = tli.lineStatus == null ? '' : tli.lineStatus;
            tlo.itemDatetime = tli.itemDateTime == null ? 0L : tli.itemDateTime.getTime() + timeOffset;
            tlo.itemId = tli.id == null ? '' : tli.id;
            System.debug('details here ' + tli);
            System.debug('details here ' + tli.noDetails);
            if (tli.itemType == VISIT_TYPE) {
                if (tli.visitWindowRaw != null && !tli.visitWindowRaw.isEmpty()) {
                    tlo.itemDatesRange = new List<Long>();
                    for (Datetime dt : tli.visitWindowRaw) {
                        tlo.itemDatesRange.add(dt.getTime());
                    }
                }
                // per SH-19559
                if (!tlo.status.equals(VT_R4_ConstantsHelper_Statuses.ACTUAL_VISIT_STATUS_SCHEDULED)) tlo.itemDatetime = null;
                tlo.noDetails = tli.noDetails == null ? false : tli.noDetails;
//                if (s.VTR3_LTFU__c) {
//                    System.debug('s.VTR3_LTFU__c' + s.VTR3_LTFU__c);
//                    System.debug('tli.labelVisitNamePTandCG' + tli.labelVisitNamePTandCG);
//
//                    tlo.title = tli.labelVisitNamePTandCG == null ? '' : tli.labelVisitNamePTandCG;
//                }
            }
            tlo.icon = tli.icon == null ? '' : tli.icon;
            tlo.description = tli.description == null ? '' : tli.description;
            tlo.visitType = tli.visitType == null ? '' : tli.visitType;
            result.add(tlo);
        }
        tls.timelineSection = result;
        return tls;
    }

    private static StudyInfoSection formStudyInfoSection(Case c, HealthCloudGA__CarePlanTemplate__c s) {
        StudyInfoSection sis = new StudyInfoSection();
        sis.logo = String.isNotBlank(s.VTD1_Study_Logo__c) ? s.VTD1_Study_Logo__c.substring(s.VTD1_Study_Logo__c.indexOf('src=') + 5, s.VTD1_Study_Logo__c.indexOf('"></img>')).replace('amp;', '') : null;
        sis.studyDetailsLink = new LinkObj(System.Label.VT_D1_ViewStudyDetails, 'StudyDetails');
        sis.requestReplacementLink = new LinkObj(System.Label.VTD1_UploadMedicalRecords, 'MedicalRecords');
        sis.callSupportLink = String.isNotBlank(c.VTR2_StudyPhoneNumber__r.Name) ? new LinkObj(System.Label.VTD1_CallSupportCenter, c.VTR2_StudyPhoneNumber__r.Name) : null;
        return sis;
    }

    private static List<TaskObj> formTasks(List<Task> tasks) {
        List<TaskObj> result = new List<VT_R3_RestPatientHomepage.TaskObj>();
        for (Task t : tasks) {
            TaskObj to = new TaskObj();
            to.title = t.Subject;
            to.setDueDate(t.ActivityDate, t.VTR5_EndDate__c);
            to.taskType = t.Type;
            to.caregiverClickable = t.VTD1_Caregiver_Clickable__c;
            to.setRedirectValue(t.VTD2_My_Task_List_Redirect__c);
            to.id = t.Id;
            to.isRequestVisitRedirect = t.VTD2_isRequestVisitRedirect__c;
            to.status = t.Status;
            to.relevantObjId = t.WhatId;
            to.category = t.Category__c;
            result.add(to);
        }
        return result;
    }

    private static MyStudyTeamSection formMyStudyTeamSection(Case c) {
        MyStudyTeamSection msts = new MyStudyTeamSection();
        List<TeammateObj> team = new List<VT_R3_RestPatientHomepage.TeammateObj>();
        String temp = '';
        if (String.isNotBlank(c.VTD1_Primary_PG__r.Name)) {
            TeammateObj pg = new TeammateObj(c.VTD1_Primary_PG__r.Name, System.Label.VT_D1_TeamPatientGuide);
            if (!Test.isRunningTest()) temp = ConnectApi.UserProfiles.getPhoto(null, c.VTD1_Primary_PG__c).standardEmailPhotoUrl;
            pg.memberPhoto = temp.contains('default_profile') ? '' : temp;
            team.add(pg);
        }
        if (String.isNotBlank(c.VTR2_SiteCoordinator__r.Name)) {
            TeammateObj scr = new TeammateObj(c.VTR2_SiteCoordinator__r.Name, System.Label.VTR2_ProfileSiteCoordinator);
            if (!Test.isRunningTest()) temp = ConnectApi.UserProfiles.getPhoto(null, c.VTR2_SiteCoordinator__c).standardEmailPhotoUrl;
            scr.memberPhoto = temp.contains('default_profile') ? '' : temp;
            team.add(scr);
        }
        if (String.isNotBlank(c.VTD1_PI_contact__r.Name)) {
            TeammateObj pi = new TeammateObj(c.VTD1_PI_contact__r.Name, System.Label.VT_D1_TeamStudyDoctor);
            if (!Test.isRunningTest()) temp = ConnectApi.UserProfiles.getPhoto(null, c.VTD1_PI_user__c).standardEmailPhotoUrl;
            pi.memberPhoto = temp.contains('default_profile') ? '' : temp;
            team.add(pi);
        }

        msts.team = team;
        return msts;
    }

    private static UpNextSection formUpNextSection() {
        UpNextSection uns = new UpNextSection();
        uns.visit = new VisitObj();
        VisitObj vo = new VisitObj();
        try {
            VT_D1_UpNextVisit.ActualVisitWrapper visit = VT_D1_UpNextVisit.getVisit();
            vo.id = visit.actualVisit.Id;
            vo.scheduled = visit.actualVisit.VTD1_Scheduled_Date_Time__c == null ? false : true;
            if (vo.scheduled) {
                vo.visitDate = visit.actualVisit.VTD1_Scheduled_Date_Time__c.getTime() + timeOffset;
            } else {
                vo.visitDate = null;
            }
            vo.visitType = visit.actualVisit.VTD1_Protocol_Visit__r.VTD1_VisitType__c;
            vo.visitName = visit.visitName;
            uns.visit = vo;
        } catch (Exception e) {
            System.debug('No appropriate visit for Up Next Section');
        }
        return uns;
    }

    private static WelcomeSection formWelcomeSection(HealthCloudGA__CarePlanTemplate__c s, Case c) {
        WelcomeSection ws = new WelcomeSection();
        LinkObj link = new LinkObj(System.Label.VT_D1_ViewStudyDetails, 'StudyDetails');
        ws.logo = String.isNotBlank(s.VTD1_Study_Logo__c) ? s.VTD1_Study_Logo__c.substring(s.VTD1_Study_Logo__c.indexOf('src=') + 5, s.VTD1_Study_Logo__c.indexOf('"></img>')).replace('amp;', '') : null;
        if (UserInfo.getLanguage() == 'ja') {
            ws.header = s.VTD1_Protocol_Nickname__c + ' ' + Label.VT_D1_Welcome;
            ws.welcomeText = System.Label.VT_D1_YourStudyNumber.substringBefore('{') + ' ' + s.Name + 'です。 ' + System.Label.VT_D1_StudyHubIs.replace('\n', '');
        } else {
            ws.header = System.Label.VT_D1_Welcome + ' ' + System.Label.VT_D1_CommunityHomeWelcome_The + ' ' + s.VTD1_Protocol_Nickname__c + ' ' + System.Label.VT_D1_Study;
            ws.welcomeText = System.Label.VT_D1_YourStudyNumber.substringBefore('{') + ' ' + s.Name + '. ' + System.Label.VT_D1_StudyHubIs.replace('\n', '');
        }
        ws.studyDetailsLink = link;
        ws.callSupportLink = String.isNotBlank(c.VTR2_StudyPhoneNumber__r.Name) ? new LinkObj(System.Label.VTD1_CallSupportCenter, c.VTR2_StudyPhoneNumber__r.Name) : null;

        return ws;
    }

    @TestVisible
    private static TaskListSection getEmptyTasklistSection() {
        TaskListSection taskListSection = new TaskListSection();
        taskListSection.openTasks = new List<TaskObj>();
        taskListSection.closedTasks = new List<TaskObj>();
        taskListSection.openTasksCount = 0;
        taskListSection.newTasksIds = new List<Id>();
        return taskListSection;
    }

    @TestVisible
    private class TaskListSection {
        public List<TaskObj> openTasks;
        public List<TaskObj> closedTasks;
        public String patientFirstName;
        public String patientLastName;
        public Integer openTasksCount;
        public List<Id> newTasksIds;

    }

    private class TaskObj {
        public String title;
        public Long dueDate;
        public String taskType;
        public Boolean caregiverClickable;
        public String redirectValue;
        public String id;
        public Boolean isRequestVisitRedirect;
        public String status;
        public String relevantObjId;
        public String category;
        public String eCOABanner;

        public void setDueDate(Date activityDate, Datetime dueDate) {
            Long dueTime;
            if (activityDate != null) {
                Time emptyTime = Time.newInstance(0, 0, 0, 0);
                dueTime = Datetime.newInstance(activityDate, emptyTime).getTime();
            } else if (dueDate != null) {
                dueTime = dueDate.getTime();
            } else return;
            this.dueDate = dueTime + timeOffset;
        }

        public void setRedirectValue(String redirect) {
            if (redirect == null) {
                redirectValue = '';
                return;
            } else if (redirect.contains(BANNER)) {
                String baseRedirect = redirect;
                redirect = redirect.substring(0, redirect.indexOf(BANNER) - 1);
                setECOABanner(EncodingUtil.urlDecode(baseRedirect.substring(baseRedirect.indexOf('=') + 1), 'UTF-8'));
            }
            redirectValue = DESTINATIONS.containsKey(redirect) ? DESTINATIONS.get(redirect) : redirect;
        }

        public void setECOABanner(String banner) {
            this.eCOABanner = banner;
        }
    }

    private class TimelineSection {
        List<TimelineObj> timelineSection;
    }

    private class TimelineObj {
        public String itemType;
        public String itemTypeTranslated;
        public String title;
        public String modality;
        public String translatedModality;
        public String statusLabel;
        public String status;
        public String subLabel;
        public String lineStatus;
        public Long itemDatetime;
        public String itemId;
        public Boolean noDetails;
        public String icon;
        public String description;
        public String visitType;
        public List<Long> itemDatesRange;
    }

    private class StudyInfoSection {
        public String logo;
        public LinkObj studyDetailsLink;
        public LinkObj requestReplacementLink;
        public LinkObj callSupportLink;
        public final String startChatLabel = System.Label.VTD1_StartChat;
    }

    private class PostRandomizationHomepage {
        public final String version = 'PostRandomizedHomepage';
        public final String userName = UserInfo.getFirstName();
        public TimelineSection timelineSection;
        public TaskListSection taskListSection;
        public StudyInfoSection studyInfoSection;
        public MyStudyTeamSection myStudyTeamSection;
    }

    private class PreRandomizationHomepage {
        public final String version = 'PreRandomizedHomepage';
        public final String userName = UserInfo.getFirstName();
        public WelcomeSection welcomeSection;
        public UpNextSection upNextSection;
        public MyStudyTeamSection myStudyTeamSection;
    }

    private class MyStudyTeamSection {
        public List<TeammateObj> team;
    }

    private class TeammateObj {
        public String memberName;
        public String memberPhoto;
        public String memberRole;

        public TeammateObj(String n, String r) {
            this.memberName = n;
            this.memberRole = r;
        }
    }

    private class UpNextSection {
        public VisitObj visit;
    }

    private class VisitObj {
        public String id;
        public Boolean scheduled;
        public String visitType;
        public String visitName;
        public Long visitDate;
    }

    private class WelcomeSection {
        public String logo;
        public String header;
        public String welcomeText;
        public LinkObj studyDetailsLink;
        public LinkObj callSupportLink;
    }

    private class LinkObj {
        public String linkLabel;
        public String linkDestination;

        public LinkObj(String label, String dest) {
            this.linkLabel = label;
            this.linkDestination = dest;
        }
    }
}