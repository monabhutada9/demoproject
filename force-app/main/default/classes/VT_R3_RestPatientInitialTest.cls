@isTest
public with sharing class VT_R3_RestPatientInitialTest {
    public static void firstTest() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_AllTestsMobilePatient@test.com' LIMIT 1];
        System.runAs(toRun) {
            Test.startTest();
            VT_R3_RestPatientInitial.getInitialData();
            Test.stopTest();
        }
    }
}