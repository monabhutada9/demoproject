/**
 * Created by Yulia Yakushenkova on 08.05.2020.
 * That class, used for patient profile data, is initialized with data for current user and can update data for patient.
 * You can insert your own data in constructor and/or add new method, but please make it non-static if possible.
 * The class used in several places of project.
 */

public class VT_R5_PatientProfileService {

    public User usr;
    public Contact contact;
    public VT_D1_Phone__c phone;
    public Address__c address;

    public Case cse;

    static String FULL_DATE_OF_BIRTH = 'Full Date of Birth';
    static String YEAR_ONLY = 'Year Only';

    public VT_R5_PatientProfileService(Contact patientContact, User patientUsr,
            VT_D1_Phone__c phone, Address__c address) {

        this.contact = patientContact;
        this.usr = patientUsr;
        this.phone = phone;
        this.address = address;
    }

    public VT_R5_PatientProfileService() {
        usr = [
                SELECT
                        Id, ContactId,
                        LanguageLocaleKey,
                        VT_R5_Secondary_Preferred_Language__c,
                        VT_R5_Tertiary_Preferred_Language__c,
                        VTD1_Filled_Out_Patient_Profile__c,
                        Profile.Name
                FROM User
                WHERE Id = :UserInfo.getUserId()
        ];

        if (usr.ContactId != null) {
            List<Contact> contacts = [
                    SELECT
                            Id, AccountId,
                            FirstName, LastName,
                            VTD1_MiddleName__c,
                            Preferred_Contact_Method__c,
                            HealthCloudGA__Gender__c,
                            VTR2_Primary_Language__c,
                            VT_R5_Secondary_Preferred_Language__c,
                            VT_R5_Tertiary_Preferred_Language__c,
                            VTD1_Clinical_Study_Membership__c,
                            Birthdate,
                            VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__c,
                            VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR3_Study_Geography__r.VT_R3_Date_Of_Birth_Restriction__c
                    FROM Contact
                    WHERE Id = :usr.ContactId
            ];
            if (!contacts.isEmpty()) contact = contacts[0];

            List<VT_D1_Phone__c> contactPhones = [
                    SELECT
                            Id, PhoneNumber__c, IsPrimaryForPhone__c
                    FROM VT_D1_Phone__c
                    WHERE VTD1_Contact__c = :usr.ContactId
            ];
            if (!contactPhones.isEmpty()) phone = contactPhones[0];

            List<Address__c> addresses = [
                    SELECT

                            Id,
                            toLabel(State__c) stateLabel,
                            State__c,
                            ZipCode__c,
                            Country__c

                    FROM Address__c
                    WHERE VTD1_Contact__c = :usr.ContactId
            ];
            if (!addresses.isEmpty()) address = addresses[0];


            List<Case> cases = [
                    SELECT Id, VTD2_Patient_Status__c, Status
                    FROM Case
                    WHERE Id = :contact.VTD1_Clinical_Study_Membership__c
            ];
            if (!cases.isEmpty()) cse = cases[0];

        }
    }

//    public String getDateOfBirth() {
//        String dobRestriction = getDateOfBirthRestriction();
//        if (String.isBlank(dobRestriction)) {
//            dobRestriction = FULL_DATE_OF_BIRTH;
//        }
//        if (contact.Birthdate != null) {
//            return dobRestriction.equals(YEAR_ONLY) ?
//                    String.valueOf(contact.Birthdate.year()) : String.valueOf(contact.Birthdate);
//        }
//        return '';
//    }

    public String getDateOfBirthRestriction() {
        if (contact != null &&
                contact.VTD1_Clinical_Study_Membership__r != null &&
                contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r != null &&
                contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR3_Study_Geography__r != null &&
                contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR3_Study_Geography__r.VT_R3_Date_Of_Birth_Restriction__c != null &&
                contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR3_Study_Geography__r.VT_R3_Date_Of_Birth_Restriction__c.equals('Year Only')) {
            return YEAR_ONLY;
        } else {
            return FULL_DATE_OF_BIRTH;
        }
    }

    public void updatePatientProfile(String phoneNumberForCreate, String stateForCreate, String zipCodeForCreate) {
        HandlerExecutionPool.getInstance().addHandlerToDisabledList('VT_R4_PatientFieldLockService');
        update contact;

        usr.VTD1_Filled_Out_Patient_Profile__c = true;
        /* SH-9701 start, Added by warriors team*/
        usr.LanguageLocaleKey = contact.VTR2_Primary_Language__c;
        usr.VT_R5_Secondary_Preferred_Language__c = contact.VT_R5_Secondary_Preferred_Language__c;
        usr.VT_R5_Tertiary_Preferred_Language__c = contact.VT_R5_Tertiary_Preferred_Language__c;
        /* SH-9701 end*/
        update usr;

        if (phone != null) {
            phone.IsPrimaryForPhone__c = true;
            update phone;
        } else {
            insert new VT_D1_Phone__c(
                    VTD1_Contact__c = contact.Id,
                    Account__c = contact.AccountId,
                    IsPrimaryForPhone__c = true,
                    PhoneNumber__c = phoneNumberForCreate
            );
        }

        if (address != null) {
            update address;
        } else if (usr.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
            insert new Address__c(
                    VTD1_Contact__c = contact.Id,
                    VTD1_Patient__c = contact.AccountId,
                    State__c = stateForCreate,
                    ZipCode__c = zipCodeForCreate
            );
        }
        HandlerExecutionPool.getInstance().removeHandlerFromDisabledList('VT_R4_PatientFieldLockService');

        System.debug('HELLO COPADO');

    }
}