@IsTest
public class VT_R2_SCR_SiteAddressHandlerTest {
//    @TestSetup
//    static void setup() {
//        VT_R3_GlobalSharing.disableForTest = true;
//
//        DomainObjects.Study_t study = new DomainObjects.Study_t()
//                .setName('Study Name')
//                .addAccount(new DomainObjects.Account_t()
//                        .setRecordTypeByName('Sponsor'));
//
//        DomainObjects.StudyTeamMember_t stmOnsiteCra = new DomainObjects.StudyTeamMember_t()
//                .setRecordTypeByName('CRA')
//                .addUser(new DomainObjects.User_t()
//                        .setProfile('CRA')
//                )
//                .addStudy(study);
//        stmOnsiteCra.persist();
//
//
//
//
//        DomainObjects.StudyTeamMember_t stmRemoteCra = new DomainObjects.StudyTeamMember_t()
//                .setRecordTypeByName('CRA')
//                .addUser(new DomainObjects.User_t()
//                        .setProfile('CRA'))
//                .addStudy(study);
//        stmRemoteCra.persist();
//
//        DomainObjects.StudyTeamMember_t stm = new DomainObjects.StudyTeamMember_t()
//                .setRecordTypeByName('SCR')
//                .addUser(new DomainObjects.User_t()
//                        .addContact(new DomainObjects.Contact_t())
//                        .setProfile('Site Coordinator'))
//                .setOnsiteCraId(stmOnsiteCra.id)
//                .setRemoteCraId(stmRemoteCra.id)
//                .addStudy(study);
//        Test.startTest();
//        DomainObjects.VirtualSite_t virtualSiteWithoutAddresses = new DomainObjects.VirtualSite_t()
//                .addStudyTeamMember(stm)
//                .setStudySiteNumber('12345')
//                .addStudy(study)
//                .setName('VS without addresses');
////        virtualSiteWithoutAddresses.persist();
//
//
//        DomainObjects.VirtualSite_t virtualSiteWithAddresses = new DomainObjects.VirtualSite_t()
//                .addStudyTeamMember(stm)
//                .setStudySiteNumber('12345')
//                .addStudy(study)
//                .setName('VS with addresses');
////        virtualSiteWithAddresses.persist();
//
//
//
//
//        new DomainObjects.VTR2_Site_Address_t()
//                .addVirtualSite(virtualSiteWithAddresses)
//                .setPrimary(true)
//                .persist();
//        new DomainObjects.VTR2_Site_Address_t()
//                .addVirtualSite(virtualSiteWithAddresses)
//                .setPrimary(false)
//                .persist();
//        Test.stopTest();
//        }


    public static void insertAddressSuccess() {
        Id vsId = [SELECT Id FROM Virtual_Site__c WHERE Name = 'VS with addresses'].Id;

        System.assertEquals(1, [SELECT Id FROM VTR2_Site_Address__c WHERE VTR2_VirtualSiteLookup__c = :vsId AND VTR2_Primary__c = TRUE].size());
        System.assertEquals(1, [SELECT Id FROM VTR2_Site_Address__c WHERE VTR2_VirtualSiteLookup__c = :vsId AND VTR2_Primary__c = FALSE].size());
        System.assertEquals(2, [SELECT Id FROM VTR2_Site_Address__c WHERE VTR2_VirtualSiteLookup__c = :vsId].size());
    }


    public static void updateAddressSuccess() {
        Id vsId = [SELECT Id FROM Virtual_Site__c WHERE Name = 'VS with addresses'].Id;
        VTR2_Site_Address__c address = [SELECT Id FROM VTR2_Site_Address__c WHERE VTR2_VirtualSiteLookup__c = :vsId AND VTR2_Primary__c = TRUE];

        String dmlMessage;
        try {
            address.VTR2_AddressLine1__c = 'New Address Line 1';
            update address;
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }

        System.assertEquals(null, dmlMessage);
    }


    public static void updateAddressFail() {
        Id vsId = [SELECT Id FROM Virtual_Site__c WHERE Name = 'VS with addresses'].Id;
        VTR2_Site_Address__c firstAddress = [SELECT Id FROM VTR2_Site_Address__c WHERE VTR2_VirtualSiteLookup__c = :vsId AND VTR2_Primary__c = TRUE];

        String dmlMessage;
        try {
            firstAddress.VTR2_Primary__c = false;
            update firstAddress;
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }

        System.assertEquals(Label.VTR2_SCR_Site_Should_Have_Primary_Address, dmlMessage);
    }


    public static void updateSecondAddressSuccess() {
        Id vsId = [SELECT Id FROM Virtual_Site__c WHERE Name = 'VS with addresses'].Id;
        VTR2_Site_Address__c firstAddress = [
                SELECT Id FROM VTR2_Site_Address__c WHERE VTR2_VirtualSiteLookup__c = :vsId AND VTR2_Primary__c = TRUE
        ];
        VTR2_Site_Address__c secondAddress = [
                SELECT Id FROM VTR2_Site_Address__c WHERE VTR2_VirtualSiteLookup__c = :vsId AND VTR2_Primary__c = FALSE
        ];

        String dmlMessage;
        try {
            secondAddress.VTR2_Primary__c = true;
            update secondAddress;
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }

        System.assertEquals(null, dmlMessage);
        System.assertEquals(1, [SELECT Id FROM VTR2_Site_Address__c WHERE Id = :secondAddress.Id AND VTR2_Primary__c = TRUE].size());
        System.assertEquals(1, [SELECT Id FROM VTR2_Site_Address__c WHERE Id = :firstAddress.Id AND VTR2_Primary__c = FALSE].size());
    }


    public static void deleteAddressSuccess() {
        Id vsId = [SELECT Id FROM Virtual_Site__c WHERE Name = 'VS with addresses'].Id;
        VTR2_Site_Address__c secondAddress = [
                SELECT Id FROM VTR2_Site_Address__c WHERE VTR2_VirtualSiteLookup__c = :vsId AND VTR2_Primary__c = FALSE
        ];

        String dmlMessage;
        try {
            delete secondAddress;
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }

        System.assertEquals(null, dmlMessage);
        System.assertEquals(1, [
                SELECT Id FROM VTR2_Site_Address__c WHERE VTR2_VirtualSiteLookup__c = :vsId
        ].size());
    }


    public static void deleteAddressFail() {
        Id vsId = [SELECT Id FROM Virtual_Site__c WHERE Name = 'VS with addresses'].Id;
        VTR2_Site_Address__c firstAddress = [
                SELECT Id FROM VTR2_Site_Address__c WHERE VTR2_VirtualSiteLookup__c = :vsId AND VTR2_Primary__c = TRUE
        ];

        String dmlMessage;
        try {
            delete firstAddress;
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }

        System.assertEquals(Label.VTR2_SCR_Site_Should_Have_Primary_Address, dmlMessage);
        System.assertEquals(2, [
                SELECT Id FROM VTR2_Site_Address__c WHERE VTR2_VirtualSiteLookup__c = :vsId
        ].size());
    }

    public static void insertFirstAddressFail() {
        Id vsId = [SELECT Id FROM Virtual_Site__c WHERE Name = 'VS without addresses'].Id;

        String dmlMessage;
        try {
        new DomainObjects.VTR2_Site_Address_t()
                    .setVirtualSiteId(vsId)
                    .setPrimary(false)
                .persist();
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }

        System.assertEquals(0, [
                SELECT Id FROM VTR2_Site_Address__c WHERE VTR2_VirtualSiteLookup__c = :vsId
        ].size());
        System.assertEquals(Label.VTR2_SCR_Site_Should_Have_Primary_Address, dmlMessage);
    }


    public static void insertSecondAddressFail() {
        Id vsId = [SELECT Id FROM Virtual_Site__c WHERE Name = 'VS without addresses'].Id;

        new DomainObjects.VTR2_Site_Address_t()
                .setVirtualSiteId(vsId)
                .setPrimary(true)
                .persist();

        String dmlMessage;
        try {
        new DomainObjects.VTR2_Site_Address_t()
                    .setVirtualSiteId(vsId)
                .setPrimary(true)
                .persist();
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }



        System.assertEquals(1, [
                SELECT Id FROM VTR2_Site_Address__c WHERE VTR2_VirtualSiteLookup__c = :vsId
        ].size());
        System.assertEquals(Label.VTR2_SCR_One_Primary_Address_Allowed, dmlMessage);


    }

}