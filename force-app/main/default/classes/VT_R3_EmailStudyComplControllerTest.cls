/**
 * Created by Redvik on 30-Oct-19.
 */
@isTest
public with sharing class VT_R3_EmailStudyComplControllerTest {

	public static void testGenerateMessage() {

		Test.startTest();
		HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
		study.VTD1_Protocol_Nickname__c = 'test study name';
		update study;
		//HealthCloudGA__CandidatePatient__c candidatePatient = VT_D1_TestUtils.createPatientCandidate(
		//		'Ter', 'kin', VT_D1_TestUtils.generateUniqueUserName(), 'terkin@test.com', 'terkin', 'Converted', study.Id, 'PN', '11-11-11', 'US', 'NY');
		VT_D1_TestUtils.createTestPatientCandidate(1);
		Test.stopTest();

		Case carePlan = [
				SELECT Id, VTD1_Primary_PG__c, VTD1_Secondary_PG__c, VTR2_SiteCoordinator__c, VTR2_Backup_Site_Coordinator__c
				FROM Case
				WHERE RecordType.DeveloperName = 'CarePlan'
		];

		VT_R3_EmailStudyCompleteParController controller = new VT_R3_EmailStudyCompleteParController();
		controller.casId = carePlan.Id;
		System.assertEquals('https://www.clinicalresearch.com/', controller.url);
		System.assertEquals('CP1', controller.getFirstName());
		System.assertNotEquals(null, controller.getStudyParticipationComplete());
		System.assertNotEquals(null, controller.getContactTheStudyTeam());

	}
}