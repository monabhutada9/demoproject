/**
 * @description :Technical class-router for mobile HTTP requests
 *
 * @author : Alexander Komarov
 *
 * @date : 21.10.2020
 */
@RestResource(UrlMapping='/mobile/*')
global with sharing class VT_R5_MobileRouter {
    @HttpGet
    global static void doGET() {
        execute();
    }
    @HttpPost
    global static void doPOST() {
        execute();
    }
    @HttpPatch
    global static void doPATCH() {
        execute();
    }
    @HttpPut
    global static void doPUT() {
        execute();
    }
    @HttpDelete
    global static void doDELETE() {
        execute();
    }

    private static Boolean match(String requestURI, String routableURI) {
        List<String> routableURIList = routableURI.split('/');
        List<String> requestURIList = requestURI.split('/');
        if (routableURIList.size() != requestURIList.size()) {
            return false;
        } else {
            for (Integer i = 0; i < routableURIList.size(); i++) {
                if (!routableURIList.get(i).contains('{') && routableURIList.get(i) != requestURIList.get(i)) {
                    return false;
                }
            }
        }
        System.debug('matched ' + requestURI + ' and ' + routableURI);
        return true;
    }
    private static Map<String, String> getParameters(String requestURI, String routableURI) {
        List<String> requestURIList = requestURI.split('/');
        List<String> routableURIList = routableURI.split('/');
        Map<String, String> result = new Map<String, String>();
        for (Integer i = 0; i < routableURIList.size(); i++) {
            if (routableURIList.get(i).contains('{')) {
                result.put(routableURIList.get(i).substring(1, routableURIList.get(i).length() - 1), requestURIList.get(i));
            }
        }
        return result;
    }
    private static void execute() {
        System.debug('before routing' + System.Limits.getCpuTime());
        VT_R5_MobileLibrary library = new VT_R5_MobileLibrary();
        Boolean routed = false;
        String requestURI = RestContext.request.requestURI;
        VT_R5_MobileRestResponse response;

        for (VT_R5_MobileLibrary.Routable r : library.getRoutes()) {
            if (routed) break;
            for (String mapping : r.getMapping()) {
                if (match(requestURI, mapping)) {
                    if (routed) {
                        break;
                    } else {
                        routed = true;
                    }
                    System.debug('after routing' + System.Limits.getCpuTime());
                    try {
                        response = r.versionRoute(getParameters(requestURI, mapping));
                    } catch (Exception e) {
                        response = new VT_R5_MobileRestResponse();//log this
                        response.buildResponseWOAI(e);
                    }
                }
            }
        }
        if (response == null || !routed) {
            unsupportedVersionResponse();
        } else {
            response.sendResponse();
        }
    }
    public static void unsupportedVersionResponse() {
        RestContext.response.statusCode = 500;
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf('{"error":"error_unsupported_version"}');
    }
}