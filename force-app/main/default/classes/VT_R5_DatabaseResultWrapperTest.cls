/**
* @author: Carl Judge
* @date: 03-Oct-20
**/
@IsTest
private class VT_R5_DatabaseResultWrapperTest {
    @IsTest
    private static void doTest() {
        Account acc = new Account(Name = 'TestAcc');
        List<Database.SaveResult> insResults = Database.insert(new List<Account>{acc});
        List<VT_R5_DatabaseResultWrapper> insWrapped = VT_R5_DatabaseResultWrapper.wrapResults(insResults);
        System.assert(insWrapped[0] instanceof VT_R5_DatabaseResultWrapper.WrappedSaveResult);
        System.assert(insWrapped[0].isSuccess());
        System.assert(insWrapped[0].getErrors().isEmpty());
        System.assert(insWrapped[0].getId() == acc.Id);
        System.assert(insWrapped[0].isCreated() == null);

        acc.Name = 'TestAccount';
        List<Database.SaveResult> updResults = Database.update(new List<Account>{acc});
        List<VT_R5_DatabaseResultWrapper> updWrapped = VT_R5_DatabaseResultWrapper.wrapResults(updResults);
        System.assert(updWrapped[0] instanceof VT_R5_DatabaseResultWrapper.WrappedSaveResult);
        System.assert(updWrapped[0].isSuccess());
        System.assert(updWrapped[0].getErrors().isEmpty());
        System.assert(updWrapped[0].getId() == acc.Id);
        System.assert(updWrapped[0].isCreated() == null);

        acc.Name = 'TestAcc';
        Account acc2 = new Account(Name = 'TestAcc2');
        List<Database.UpsertResult> upsResults = Database.upsert(new List<Account>{acc, acc2});
        List<VT_R5_DatabaseResultWrapper> upsWrapped = VT_R5_DatabaseResultWrapper.wrapResults(upsResults);
        System.assert(upsWrapped[0] instanceof VT_R5_DatabaseResultWrapper.WrappedUpsertResult);
        System.assert(upsWrapped[0].isSuccess());
        System.assert(upsWrapped[0].getErrors().isEmpty());
        System.assert(upsWrapped[0].getId() == acc.Id);
        System.assert(upsWrapped[0].isCreated() == false);
        System.assert(upsWrapped[1] instanceof VT_R5_DatabaseResultWrapper.WrappedUpsertResult);
        System.assert(upsWrapped[1].isSuccess());
        System.assert(upsWrapped[1].getErrors().isEmpty());
        System.assert(upsWrapped[1].getId() == acc2.Id);
        System.assert(upsWrapped[1].isCreated() == true);

        List<Database.DeleteResult> delResults = Database.delete(new List<Account>{acc});
        List<VT_R5_DatabaseResultWrapper> delWrapped = VT_R5_DatabaseResultWrapper.wrapResults(delResults);
        System.assert(delWrapped[0] instanceof VT_R5_DatabaseResultWrapper.WrappedDeleteResult);
        System.assert(delWrapped[0].isSuccess());
        System.assert(delWrapped[0].getErrors().isEmpty());
        System.assert(delWrapped[0].getId() == acc.Id);
        System.assert(delWrapped[0].isCreated() == null);

        List<Database.UndeleteResult> undResults = Database.undelete(new List<Account>{acc});
        List<VT_R5_DatabaseResultWrapper> undWrapped = VT_R5_DatabaseResultWrapper.wrapResults(undResults);
        System.assert(undWrapped[0] instanceof VT_R5_DatabaseResultWrapper.WrappedUndeleteResult);
        System.assert(undWrapped[0].isSuccess());
        System.assert(undWrapped[0].getErrors() == null); // for some reason undelete result errors are null rather than an empty list - see documentation
        System.assert(undWrapped[0].getId() == acc.Id);
        System.assert(undWrapped[0].isCreated() == null);
    }
}