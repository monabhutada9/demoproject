// created generic class for delete the object and create audit trail for that
//Sonam SH-18779
public without sharing class VT_R5_DeleteHandler {
    
    @AuraEnabled
    public static void deleteTheRecord(String recordId, String obj, String deletionReason) {
        System.debug('enter in the method');
        System.debug('recordi id--'+ recordId);
        System.debug('deletion reason'+ deletionReason);
        if(recordId == null || deletionReason == null) {
            return;
        }

        Map<String,Schema.SObjectType> mapOfSobject = Schema.getGlobalDescribe(); 
        Schema.SObjectType sobjType = mapOfSobject.get(Obj); 
        Schema.DescribeSObjectResult describeResult = sobjType.getDescribe(); 
        Map<String,Schema.SObjectField> fieldsMap = describeResult.fields.getMap(); 
        System.debug('fieldmap--'+ fieldsMap);
        System.debug('id field--'+ fieldsMap.get('Id'));

        

        if(recordId != null || deletionReason != null) {
            try{
                VTR5_Document_Audit_Trail__c docAudit = new VTR5_Document_Audit_Trail__c();
                docAudit.VTR5_Reason_for_Deletion__c = deletionReason;
                
                insert docAudit;
                System.debug('reached here--'+ docAudit);
            } catch(Exception ex) {
                System.debug('exception is--'+ ex.getMessage());
            }
            
        }
    }
}