public without sharing class VT_D2_StudyVisits {

    public static final Map<String, Integer> CURRENT_STATUS_PRIORITY = new Map<String, Integer>{
            VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED => 1,
            VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED => 2,
            VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED => 3,
            VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_MISSED => 4
    };

    public class CaseVisit implements Comparable {
        VTD1_Actual_Visit__c data;
        String taskId;
        List<String> participants;

        public Integer compareTo(Object compareTo) {
            CaseVisit compareToItem = (CaseVisit)compareTo;
            if (CURRENT_STATUS_PRIORITY.containsKey(this.data.VTD1_Status__c)) {
                if (this.data.VTD1_Status__c == compareToItem.data.VTD1_Status__c) {
                    if (this.data.RecordTypeId == VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_ACTUAL_VISIT_UNSCHEDULED
                            && this.data.VTR2_Status_changed_date__c != null) {
                        return this.data.VTR2_Status_changed_date__c < compareToItem.data.VTR2_Status_changed_date__c ? 1 : -1;
                    } /*else if (this.data.VTD1_Visit_Number__c != null) {
                        return this.data.VTD1_Visit_Number__c < compareToItem.data.VTD1_Visit_Number__c ? -1 : 1;
                    } */else {
                        return 0;
                    }
                } else {
                    return CURRENT_STATUS_PRIORITY.get(this.data.VTD1_Status__c) < CURRENT_STATUS_PRIORITY.get(compareToItem.data.VTD1_Status__c) ? -1 : 1;
                }
            } else if (this.data.RecordTypeId == VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_ACTUAL_VISIT_UNSCHEDULED
                    && this.data.VTR2_Status_changed_date__c != null) {
                return this.data.VTR2_Status_changed_date__c < compareToItem.data.VTR2_Status_changed_date__c ? 1 : -1;
            /*} else if (this.data.VTD1_Visit_Number__c != null)  {
                return this.data.VTD1_Visit_Number__c < compareToItem.data.VTD1_Visit_Number__c ? -1 : 1;
            */} else {
                return 0;
            }
        }
    }

    public class CaseVisitList {
        String header;
        List<CaseVisit> caseVisits;

        public CaseVisitList(String header, List<CaseVisit> caseVisits){
            this.header = header;
            this.caseVisits = caseVisits;
        }
    }

    @AuraEnabled
    public static String getVisits(String caseId, String visitType) {
        try {
            caseId = VT_D2_CreatePCFFromCaseController.getCaseId(caseId);
            List<String> validStatus = new List<String> {
                    VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT,
                    VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED,
                    VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED
            };

            validStatus.addAll(CURRENT_STATUS_PRIORITY.keySet());

            String query = 'SELECT Id, Name, RecordTypeId, VTD1_Status__c, VTD1_Onboarding_Type__c, VTD1_Visit_Number__c, ' +
                    'VTD1_Visit_Name__c, To_Be_Scheduled_Date__c, VTD1_Scheduled_Date_Time__c, Patient_Name_Formula__c, ' +
                    'VTD1_Date_for_Timeline__c, VTD1_Visit_Completion_Date__c, VTR2_Status_changed_date__c, ' +
                    'VTR2_Modality__c, VTR2_Loc_Name__c, VTR2_Independent_Rater_Flag__c' +
                    ' FROM VTD1_Actual_Visit__c' +
                    ' WHERE ' + (visitType == 'Ad-Hoc' ? 'Unscheduled_Visits__c' : 'VTD1_Case__c') + ' = : caseId' +
                    ' AND VTD1_Status__c IN :validStatus AND VTR3_LTFUStudyVisit__c = true' +
                    (visitType != 'Ad-Hoc' ? ' ORDER BY VTD1_Protocol_Visit__r.VTD1_VisitNumber__c' : '');

            List<VTD1_Actual_Visit__c> visits = Database.query(query);

            Schema.DescribeFieldResult fieldResult = VTD1_Actual_Visit__c.VTD1_Status__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            Map<String, String> visitIdToTaskIdMap = new Map<String, String>();
            AggregateResult[] groupedResults = [SELECT MAX(Id) Id, WhatId FROM Task WHERE WhatId IN: visits GROUP BY WhatId];
            for (AggregateResult ar : groupedResults) {
                visitIdToTaskIdMap.put(String.valueOf(ar.get('WhatId')), String.valueOf(ar.get('Id')));
            }

            Map<Id, List<String>> participantsMap = new Map<Id, List<String>>();
            for (VTD1_Actual_Visit__c item : visits) {
                if (item.VTD1_Status__c == 'To Be Scheduled') {
                    participantsMap.put(item.Id, new List<String>());
                }
            }
            if (! participantsMap.isEmpty()) {
                Boolean isTMA = VT_D1_HelperClass.getUserProfileName(UserInfo.getUserId()) == VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME;
                for (Visit_Member__c item : [
                        SELECT VTD1_Actual_Visit__c,
                                Visit_Member_Name__c,
                                VTD1_Member_Type__c,
                                VTD1_Participant_User__c,
                                VTD1_Participant_User__r.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c,
                                VTD1_Participant_User__r.Profile.Name
                        FROM Visit_Member__c
                        WHERE VTD1_Actual_Visit__c IN :participantsMap.keySet()
                        ORDER BY VTD2_Sort_Order_for_Study_Visits__c, VTD1_Member_Type__c
                ]) {
                    if (isTMA && item.VTD1_Participant_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
                        participantsMap.get(item.VTD1_Actual_Visit__c).add(item.VTD1_Participant_User__r.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c + ', ' + item.VTD1_Member_Type__c);
                    } else if (isTMA && item.VTD1_Participant_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME) {
                        participantsMap.get(item.VTD1_Actual_Visit__c).add(item.VTD1_Participant_User__c + ', ' + item.VTD1_Member_Type__c);
                    } else {
                    participantsMap.get(item.VTD1_Actual_Visit__c).add(item.Visit_Member_Name__c + ', ' + item.VTD1_Member_Type__c);
                }
            }
            }

            List<CaseVisit> scheduledVisits = new List<CaseVisit>();
            List<CaseVisit> toBeRescheduledVisits = new List<CaseVisit>();
            List<CaseVisit> toBeScheduledVisits = new List<CaseVisit>();
            List<CaseVisit> missedVisits = new List<CaseVisit>();
            List<CaseVisit> currentVisits = new List<CaseVisit>();
            List<CaseVisit> futureVisits = new List<CaseVisit>();
            List<CaseVisit> canceledVisits = new List<CaseVisit>();
            List<CaseVisit> completedVisits = new List<CaseVisit>();
            for (VTD1_Actual_Visit__c visit : visits) {
                CaseVisit cv = new CaseVisit();
                cv.data = visit;
                cv.taskId = visitIdToTaskIdMap.get(visit.Id);
                cv.participants = participantsMap.get(visit.Id);

                if (CURRENT_STATUS_PRIORITY.containsKey(visit.VTD1_Status__c)) {
                    currentVisits.add(cv);
                } else if (visit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT) {
                    futureVisits.add(cv);
                } else if (visit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED) {
                    for( Schema.PicklistEntry pickListVal : ple){
                        if (pickListVal.getValue() == visit.VTD1_Status__c) {
                            cv.data.VTD1_Status__c = pickListVal.getLabel();
                        }
                    }
                    canceledVisits.add(cv);
                } else if (visit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED) {
                    completedVisits.add(cv);
                }
            }
            currentVisits.sort();
            futureVisits.sort();
            canceledVisits.sort();
            completedVisits.sort();

            List<CaseVisitList> caseVisits = new List<CaseVisitList>();
            caseVisits.add(new CaseVisitList(Label.VT_D2_StudyVisits_CurrentVisits, currentVisits));
            if (visitType != 'Ad-Hoc') {
                caseVisits.add(new CaseVisitList(Label.VT_D2_StudyVisits_FutureVisits, futureVisits));
            }
            caseVisits.add(new CaseVisitList(Label.VT_R2_StudyVisits_CanceledVisits, canceledVisits));
            caseVisits.add(new CaseVisitList(Label.VT_D2_StudyVisits_CompletedVisits, completedVisits));

            return JSON.serialize(caseVisits);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static void cancelVisit(Id visitId, String chosenOption, String removeReason) {
        VT_D1_ActualVisitsHelper.removeVisit(visitId, chosenOption, removeReason);
    }

    @AuraEnabled
    public static String getAdHocRecTypeId() {
        return Schema.SObjectType.VTD1_Actual_Visit__c.getRecordTypeInfosByDeveloperName().get('VTD1_Unscheduled').getRecordTypeId();
    }

    @AuraEnabled
    public static String getInitData(){
        InitData initData = new InitData();
        initData.reasonForRemovalLength = Schema.SObjectType.VTD1_Actual_Visit__c.fields.VTR2_ReasonToRemove__c.getLength();
        initData.adHocRecTypeId = getAdHocRecTypeId();
        return JSON.serialize(initData);
    }

    public class InitData{
        String adHocRecTypeId;
        Integer reasonForRemovalLength;
    }
    
    @AuraEnabled(Cacheable=true)
    public static String getCurrentUserProfile() { //SH-9842
        return VT_D1_HelperClass.getUserProfileName(UserInfo.getUserId());
    }
}