/**
* @author: Carl Judge
* @date: 14-Sep-20
**/

public without sharing class VT_R5_SiteParticipantSharingHandler {
    private static VT_R5_SiteMemberCalculator beforeUpdateCalc;
    private static Set<Id> changedMemberSiteIds;

    public static void handleAfterInsert(List<Virtual_Site__c> sites) {
        if (Test.isRunningTest() && VT_R3_GlobalSharing.disableForTest) { return; }
        VT_R5_SiteMemberCalculator memberCalc = new VT_R5_SiteMemberCalculator();
        memberCalc.initForSites(new Map<Id,Virtual_Site__c>(sites).keySet());
        List<VT_R5_SiteMemberCalculator.SiteUser> siteUsers = memberCalc.getSiteUsers();
        if (!siteUsers.isEmpty()) {
            VT_R5_ShareGroupMembershipService.addSiteMembers(siteUsers);
            VT_R5_SharingChainFactory.createFromChangedSiteUsers(siteUsers, null).enqueue();
        }
    }

    public static void handleBeforeUpdate(List<Virtual_Site__c> sites, Map<Id, Virtual_Site__c> oldMap) {
        if (Test.isRunningTest() && VT_R3_GlobalSharing.disableForTest) { return; }
        changedMemberSiteIds = getChangedMemberSiteIds(sites, oldMap);
        if (!changedMemberSiteIds.isEmpty()) {
            beforeUpdateCalc = new VT_R5_SiteMemberCalculator();
            beforeUpdateCalc.initForSites(changedMemberSiteIds);
        }
    }

    public static void handleAfterUpdate(List<Virtual_Site__c> sites, Map<Id, Virtual_Site__c> oldMap) {
        if (Test.isRunningTest() && VT_R3_GlobalSharing.disableForTest) { return; }
        if (beforeUpdateCalc != null && changedMemberSiteIds != null && !changedMemberSiteIds.isEmpty()) {
            VT_R5_SiteMemberCalculator afterUpdateCalc = new VT_R5_SiteMemberCalculator();
            afterUpdateCalc.initForSites(changedMemberSiteIds);
            List<VT_R5_SiteMemberCalculator.SiteUser> addedUsers = beforeUpdateCalc.getNonMembers(afterUpdateCalc.getSiteUsers());
            List<VT_R5_SiteMemberCalculator.SiteUser> removedUsers = afterUpdateCalc.getNonMembers(beforeUpdateCalc.getSiteUsers());

            System.debug('ADDED USERS:' + addedUsers);
            System.debug('REMOVED USERS:' + removedUsers);

            if (!addedUsers.isEmpty() || !removedUsers.isEmpty()) {
                VT_R5_SharingChainFactory.createFromChangedSiteUsers(addedUsers, removedUsers).enqueue();
            }
            if (!addedUsers.isEmpty()) {
                VT_R5_ShareGroupMembershipService.addSiteMembers(addedUsers);
            }
            if (!removedUsers.isEmpty()) {
                VT_R5_ShareGroupMembershipService.removeSiteMembers(removedUsers);
            }
            beforeUpdateCalc = null;
        }
    }
    
    private static Set<Id> getChangedMemberSiteIds(List<Virtual_Site__c> sites, Map<Id, Virtual_Site__c> oldMap) {
        Set<Id> siteIds = new Set<Id>();
        for (Virtual_Site__c site : sites) {
            Virtual_Site__c old = oldMap.get(site.Id);
            if (site.VTD1_Study_Team_Member__c != old.VTD1_Study_Team_Member__c
                || site.VTR2_Backup_PI_STM__c != old.VTR2_Backup_PI_STM__c
                //STARt RAJESH :SH-17440 :CRA fields are removed from the Virtual site so will not be changes hereafter.
                /*
                || site.VTD1_Remote_CRA__c != old.VTD1_Remote_CRA__c
                || site.VTR2_Onsite_CRA__c != old.VTR2_Onsite_CRA__c
				*/
                //END:SH-17440
                || site.VTR2_Primary_SCR__c != old.VTR2_Primary_SCR__c)
            {
                siteIds.add(site.Id);
            }
        }
        return siteIds;
    }
}