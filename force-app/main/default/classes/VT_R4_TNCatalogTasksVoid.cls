/**
 * Created by user on 2/13/2020.
 */

public with sharing class VT_R4_TNCatalogTasksVoid {
    public class ParamsHolder {
        @InvocableVariable(Label = 'TN Code' Required = true)
        public String tnCode;

        @InvocableVariable(Label = 'Record ID' Required = false)
        public String sourceId;

        @InvocableVariable(Label = 'Assigned To IDs' Required = true)
        public List <String> assignedToIDs;

        @InvocableVariable(Label = 'Is Notification?' Description = 'Unchecked if generate Tasks, checked if generate Notification.' Required = true)
        public Boolean isNotification;
    }
    @InvocableMethod
    public static void generateTasksOrNotifications(List<ParamsHolder> params) {
        List<VT_D2_TNCatalogTasks.ParamsHolder> tasks = new List<VT_D2_TNCatalogTasks.ParamsHolder>();
        List<String> tnCatalogNotificationCodes = new List<String>();
        List<Id> sourceNotificationIds = new List<Id>();
        Map<String, List<Id>> receiversMap = new Map<String, List<Id>>();
        for (ParamsHolder param : params) {
            if (param.isNotification) {
                tnCatalogNotificationCodes.add(param.tnCode);
                sourceNotificationIds.add(param.sourceId);
                receiversMap.put(param.tnCode + (Id)param.sourceId, param.assignedToIDs);
            } else {
                List<String> sourceIds = new List<String>();
                for (Integer i = 0; i < param.assignedToIDs.size(); i++) {
                    sourceIds.add(param.sourceId);
                }
                VT_D2_TNCatalogTasks.ParamsHolder task = new VT_D2_TNCatalogTasks.ParamsHolder();
                task.tnCode = param.tnCode;
                task.sourceIds = sourceIds;
                task.assignedToIDs = param.assignedToIDs;
                tasks.add(task);
            }
        }
        if (!tasks.isEmpty()) {
            VT_D2_TNCatalogTasks.generateTask(tasks);
        }
        if (!tnCatalogNotificationCodes.isEmpty()) {
            VT_D2_TNCatalogNotifications.generateNotifications(tnCatalogNotificationCodes, sourceNotificationIds, new Map<String, String>(), null, receiversMap, null);
        }
    }

}