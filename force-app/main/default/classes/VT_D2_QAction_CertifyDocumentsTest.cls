@IsTest
public class VT_D2_QAction_CertifyDocumentsTest {

    public with sharing class CalloutMock implements HttpCalloutMock {
        private Integer finalResponseCode;
        public CalloutMock(Integer finalResponseCode) {
            this.finalResponseCode = finalResponseCode;
        }

        public HttpResponse respond(HttpRequest req) {
            String endPoint = req.getEndpoint();
            HttpResponse res = new HttpResponse();

            if (endPoint.contains('/oauth/token')) {
                VT_D2_DocuSignAuthHelper.TokenResponse tokenResponse = new VT_D2_DocuSignAuthHelper.TokenResponse();
                tokenResponse.access_token = 'dsToken';
                tokenResponse.expires_in = 3600;
                res.setBody(JSON.serialize(tokenResponse));
                res.setStatusCode(200);
            } else if (endPoint.contains('/oauth/userinfo')) {
                VT_D2_DocuSignAuthHelper.UserInfoAccount infoAcc = new VT_D2_DocuSignAuthHelper.UserInfoAccount();
                infoAcc.account_id = 'accId';
                infoAcc.account_name = VTD2_Docusign_API_Settings__c.getInstance().VTD2_Account_Name__c;
                infoAcc.base_uri = 'baseUri';
                VT_D2_DocuSignAuthHelper.UserInfoResponse infoResponse = new VT_D2_DocuSignAuthHelper.UserInfoResponse();
                infoResponse.accounts = new List<VT_D2_DocuSignAuthHelper.UserInfoAccount>{
                        infoAcc
                };
                res.setBody(JSON.serialize(infoResponse));
                res.setStatusCode(200);
            } else if (endPoint.contains('baseUri')) {
                res.setBody(JSON.serialize('body'));
                res.setStatusCode(this.finalResponseCode);
            }

            return res;
        }
    }

    @TestSetup
    private static void setupMethod() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        //VT_D1_TestUtils.createPatientCandidate('Mike', 'Birn', 'mikebirn@test.com', 'mikebirn@test.com', 'mikebirn', 'Converted', study.Id, 'PN', '11-11-11', 'US', 'NY');
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
        study.VTD2_Certification_form_Medical_Records__c = 'certDoc';
        update study;

        List<Case> casesList = [SELECT Id, VTD1_Study__c, VTD1_Primary_PG__c FROM Case];

        Case cas = casesList.get(0);

        VTD1_Document__c doc = new VTD1_Document__c();
        doc.VTD1_Study__c = cas.VTD1_Study__c;
        doc.VTD1_Clinical_Study_Membership__c = cas.Id;
        doc.VTD1_Uploading_Not_Finished__c = true;
        insert doc;
        doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE;
        update doc;

        system.debug(doc.Id);

        List<ContentVersion> contentVersions = new List<ContentVersion>();
        contentVersions.add(getContentVersion('test file'));
        contentVersions.add(getContentVersion('certDoc'));
        insert contentVersions;
        Map<Id, ContentVersion> cvMap = new Map<Id, ContentVersion>([
            SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN :contentVersions
        ]);

        List<ContentDocumentLink> cdLinks = new List<ContentDocumentLink>();
        cdLinks.add(getContentDocumentLink(doc.Id, cvMap.get(contentVersions[0].Id).ContentDocumentId));
        cdLinks.add(getContentDocumentLink(study.Id, cvMap.get(contentVersions[1].Id).ContentDocumentId));
        insert cdLinks;

        VTD2_Docusign_API_Settings__c settings = new VTD2_Docusign_API_Settings__c();
        settings.VTD2_Account_Name__c = 'accName';
        insert settings;
    }

    @IsTest
    private static void testFailNull() {
        Test.startTest();
        doTest(null);
        Test.stopTest();
    }

    @IsTest
    private static void testSuccess() {
        Test.startTest();
        doTest(201);
        Test.stopTest();
    }

    @IsTest
    private static void testFail500() {
        Test.startTest();
        doTest(500);
        Test.stopTest();
    }

    @IsTest
    private static void testFail400() {
        Test.startTest();
        doTest(400);
        Test.stopTest();
    }

    @IsTest
    private static void testCertifyMedicalRecord() {
        VTD1_Document__c doc = [
                SELECT Id, VTD1_Clinical_Study_Membership__c, VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c
                FROM VTD1_Document__c
                ORDER BY CreatedDate DESC
                LIMIT 1
        ];
        Test.startTest();
        User u = [SELECT Id FROM User WHERE Id = :doc.VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c];
        Task task = new Task();
        task.Status = 'In Progress';
        task.Document__c = doc.Id;
        task.VTD1_Type_for_backend_logic__c = 'Eligibility Asessment Task';
        task.OwnerId = u.Id;
        insert task;
            Test.setMock(HttpCalloutMock.class, new CalloutMock(201));
            System.runAs(u) {
                    VT_D2_MedicalRecordsCertifyDocsHelper.certifyDoc(doc.VTD1_Clinical_Study_Membership__c, doc.Id);
            }
            Test.stopTest();
    }

    private static void doTest(Integer finalResponseCode) {
        VTD1_Document__c doc = [SELECT Id, VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c FROM VTD1_Document__c ORDER BY CreatedDate DESC LIMIT 1];
        User u = [SELECT Id FROM User WHERE Id = :doc.VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c];
        Test.setMock(HttpCalloutMock.class, new CalloutMock(finalResponseCode));
        System.runAs(u) {
            new VT_D2_QAction_CertifyDocuments(doc.Id).executeFuture();

        }
    }

    private static ContentVersion getContentVersion(String title) {
        return new ContentVersion(
            PathOnClient = 'test.txt',
            Title = title,
            VersionData = Blob.valueOf('Test Data')
        );
    }

    private static ContentDocumentLink getContentDocumentLink(Id parentId, Id contentDocumentId) {
        return new ContentDocumentLink(
            LinkedEntityId = parentId,
            ContentDocumentId = contentDocumentId,
            ShareType = 'I',
            Visibility = 'AllUsers'
        );
    }
}