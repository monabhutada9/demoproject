/**
 * Created by Leonid Bartenev
 *
 * Batch for processing Actions Queue.
 *
 * For start processing of queue execute code:
 *
 * VT_D1_Batch_QueueActionsProcessing.run();
 *
 */

public class VT_D1_Batch_QueueActionsProcessing implements Database.Batchable<SObject>, Database.AllowsCallouts{

    @TestVisible
    private static String cronId;
    public static final Integer REPEAT_INTERVAL_MINUTES = VT_D1_AbstractAction.ACTION_QUEUE_SETTINGS.VT_D1_Batch_Processing_Interval_Minutes__c.intValue();
    public static final Integer SCOPE_SIZE = VT_D1_AbstractAction.ACTION_QUEUE_SETTINGS.VT_D1_Batch_Scope_Size__c.intValue();
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
                SELECT Id, VTD1_Action_Class__c, VTD1_Action_JSON__c 
                FROM VTD1_Queue_Action__c 
                WHERE VTD1_Execution_Time__c <=: Datetime.now()
                ORDER BY VTD1_Execution_Time__c ASC 
        ]);
    }
    
    public void execute(Database.BatchableContext bc, List<VTD1_Queue_Action__c> queueActions) {
        List<VTD1_Queue_Action__c> processedQueueActions = new List<VTD1_Queue_Action__c>();
        for(VTD1_Queue_Action__c queueAction : queueActions){
            VT_D1_AbstractAction.loadAction(queueAction).execute();
            processedQueueActions.add(queueAction);
        }
        delete processedQueueActions;
    }
    
    public void finish(Database.BatchableContext bc) {
        if(!Test.isRunningTest() && cronId == null){
            run();
        }
    }
    
    public static void run(){
        cronId = System.scheduleBatch(new VT_D1_Batch_QueueActionsProcessing(), 'Queue Actions Processing ' + System.now(), REPEAT_INTERVAL_MINUTES, 1);
    }
}