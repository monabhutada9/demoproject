/**
 * Created by Yuliya Yakushenkova on 9/21/2020.
 */

@IsTest
public with sharing class VT_R5_MobileECoaSSOTest {
    public static void testGetECoaSSO_v1() {
        RestContext.response = new RestResponse();
        RestContext.request = new RestRequest();
        RestContext.request.requestURI = '/patient/v1/ecoa-link/';
        RestContext.request.httpMethod = 'GET';
        VT_R5_MobileRestRouter.doGET();
    }
    public static void testGetECoaSSOInvalid() {
        RestContext.response = new RestResponse();
        RestContext.request = new RestRequest();
        RestContext.request.requestURI = '/patient/v0/ecoa-link/';
        RestContext.request.httpMethod = 'GET';
        RestContext.request.requestBody = Blob.valueOf('Unsupported');
        VT_R5_MobileRestRouter.doGET();
    }
}