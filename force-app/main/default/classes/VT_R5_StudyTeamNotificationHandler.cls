/**
 * Created by K.Latysh on 22-Oct-20.
 */

public with sharing class VT_R5_StudyTeamNotificationHandler {

    public void onAfterInsert(List<VTR5_StudyTeamNotification__c> recs) {


        new VT_R5_ConfigurableNotificationService().configureNotifications(recs);
        this.validateActiveNotifications(recs);
    }
    public void onAfterUpdate(List<VTR5_StudyTeamNotification__c> recs, Map<Id,VTR5_StudyTeamNotification__c> recsOld) {
        new VT_R5_ConfigurableNotificationService().configureNotifications(recs);


        this.validateActiveNotifications(recs);
    }

    public void onBeforeInsert(List<VTR5_StudyTeamNotification__c> recs) {
        System.debug('onBeforeInsert');
        for(VTR5_StudyTeamNotification__c studyTeamNotification : recs) {
            if (studyTeamNotification.VTR5_NotificationActive__c == true) {
                studyTeamNotification.VTR5_DateOfDeactivation__c = null;
            }
        }
    }
    public void onBeforeUpdate(List<VTR5_StudyTeamNotification__c> recs, Map<Id,VTR5_StudyTeamNotification__c> recsOld) {
        System.debug('onBeforeUpdate');
        Datetime dateTimeNow = Datetime.now();
        for(VTR5_StudyTeamNotification__c studyTeamNotification : recs){
            if((recsOld.get(studyTeamNotification.Id).VTR5_NotificationActive__c == true) &&studyTeamNotification.VTR5_NotificationActive__c == false){
                studyTeamNotification.VTR5_DateOfDeactivation__c = dateTimeNow;
            }
            if(studyTeamNotification.VTR5_NotificationActive__c == true){
                studyTeamNotification.VTR5_DateOfDeactivation__c = null;
            }
        }
    }

    /**

       * Get list of Study Notifications and check the Studies has only 1 active Notification

       * @author Igor Shamenok
       * @param studyNotifications
       * @date 22-Oct-2020
       * @issue SH-18689
       */

    /*private void validateActiveNotifications(List<VTR5_StudyTeamNotification__c> studyNotifications) {

        List<Id> involvedStudiesId = new List<Id>();
        for (VTR5_StudyTeamNotification__c studyNot : studyNotifications) {
            if(!involvedStudiesId.contains(studyNot.VTR5_Study__c)) {
                involvedStudiesId.add(studyNot.VTR5_Study__c);
            }
        }
        List<VTR5_StudyTeamNotification__c> activeNots = [
                SELECT  Id,
                        VTR5_NotificationActive__c,
                        VTR5_Study__c
                FROM    VTR5_StudyTeamNotification__c
                WHERE   VTR5_NotificationActive__c = TRUE


                AND     VTR5_Study__c IN :involvedStudiesId
        ];
        List<Id> studiesWithActiveNots = new List<Id>();
        List<Id> studiesWithMultipleActiveNots = new List<Id>();
        for(VTR5_StudyTeamNotification__c studyNot : activeNots) {
            if(!studiesWithActiveNots.contains(studyNot.VTR5_Study__c)) {
                studiesWithActiveNots.add(studyNot.VTR5_Study__c);
            } else if(studiesWithActiveNots.contains(studyNot.VTR5_Study__c)) {
                studiesWithMultipleActiveNots.add(studyNot.VTR5_Study__c);
            }
        }
        if(!studiesWithMultipleActiveNots.isEmpty()) {
            for(Id studyId : studiesWithMultipleActiveNots) {
                for(VTR5_StudyTeamNotification__c notification : studyNotifications) {
                    if(notification.VTR5_Study__c == studyId
                            && notification.VTR5_NotificationActive__c == true


                        ) {
                        notification.addError('There can only be one active notification at a time.');
                    }
                }
            }
        }

    }*/

    private void validateActiveNotifications(List<VTR5_StudyTeamNotification__c> studyNotifications) {
        List<Id> involvedStudiesId = new List<Id>();
        for (VTR5_StudyTeamNotification__c studyNot : studyNotifications) {
            if(!involvedStudiesId.contains(studyNot.VTR5_Study__c)) {
                involvedStudiesId.add(studyNot.VTR5_Study__c);
            }
        }
        List<VTR5_StudyTeamNotification__c> activeNots = [
                SELECT  Id,
                        VTR5_NotificationActive__c,
                        VTR5_Study__c
                FROM    VTR5_StudyTeamNotification__c
                WHERE   (VTR5_NotificationActive__c = TRUE OR VTR5_ActiveOfSubmitBanner__c = TRUE)
                AND     VTR5_Study__c IN :involvedStudiesId
        ];
        if(activeNots.size() > 1){
            for(VTR5_StudyTeamNotification__c notification : studyNotifications) {
                if(notification.VTR5_NotificationActive__c || notification.VTR5_ActiveOfSubmitBanner__c){
                    notification.addError('There can only be one active notification at a time.');
                }
            }
        }

    }
}