/**
* @author: Carl Judge
* @date: 17-Jul-19
**/

@isTest
public with sharing class VT_R2_GlobalAdhocSharingTest {

    public static void doTest() {
        Case carePlan = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];

        VT_R2_GlobalAdhocSharing.ParamsHolder patientRelatedParam = new VT_R2_GlobalAdhocSharing.ParamsHolder();
        patientRelatedParam.caseId = carePlan.Id;
        patientRelatedParam.isPatientRelatedObject = true;
        VT_R2_GlobalAdhocSharing.ParamsHolder studyRelatedParam = new VT_R2_GlobalAdhocSharing.ParamsHolder();
        studyRelatedParam.caseId = carePlan.Id;
        studyRelatedParam.isPatientRelatedObject = false;
        List<VT_R2_GlobalAdhocSharing.ParamsHolder> params = new List<VT_R2_GlobalAdhocSharing.ParamsHolder>{patientRelatedParam, studyRelatedParam};

        List<List<User>> users = VT_R2_GlobalAdhocSharing.getActiveUsers(params);
        System.assert(users != null);
        System.assert(users.size() == 2);
        System.assert(!users[0].isEmpty());
        System.assert(!users[1].isEmpty());
    }
}