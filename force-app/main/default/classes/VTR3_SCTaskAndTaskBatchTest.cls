@IsTest
public with sharing class VTR3_SCTaskAndTaskBatchTest {
	public static void testBehavior() { 
		Case ptCase = new Case(
				RecordTypeId = VT_R4_ConstantsHelper_RecordTypes.RECORD_TYPE_ID_CASE_PCF
		);
		insert ptCase;

		VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c(
				VTR5_PatientCaseId__c = ptCase.Id
		);
		insert actualVisit;

		VTD1_Document__c document = new VTD1_Document__c();
		insert document;

		VTD1_SC_Task__c scTask = new VTD1_SC_Task__c(
				VTD1_Actual_Visit__c = actualVisit.Id,
				VTD1_Document__c = document.Id
		);
		insert scTask;

		Task task = new Task(WhatId = scTask.Id);
		insert task;

		Test.startTest();
		VTR3_SCTaskAndTaskBatch batch = new VTR3_SCTaskAndTaskBatch();
		Database.executeBatch(batch);
		Test.stopTest();
	}
}