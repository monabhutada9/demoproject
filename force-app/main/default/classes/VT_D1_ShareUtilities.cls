public class VT_D1_ShareUtilities {

    public static String getShareObjectName(String objName) {
        return isCustomObject(objName) ? objName.replace('__c', '__Share') : objName + 'Share';
    }

    public static String getShareObjectParentField(String objName) {
        return isCustomObject(objName) ? 'Parent' : objName;
    }

    public static String getShareObjectAccessLevelField(String objName) {
        return isCustomObject(objName) ? 'AccessLevel' : objName + 'AccessLevel';
    }
    public static List<String> getShareObjectAccessLevelFields(String objName) {
        List<String> fields;
        if (objName != 'Account') {
            fields = new List<String>{getShareObjectAccessLevelField(objName)};
        } else {
            fields = new List<String> {
                'AccountAccessLevel',
                'OpportunityAccessLevel',
//                'CaseAccessLevel',    During the Account sharing process Cases are excluded. They need to be shared separately
                'ContactAccessLevel'
            };
        }
        return fields;
    }

    public static Boolean isCustomObject(String objName) {
        return objName.contains('__c');
    }

    public static String getFieldNameFromPath(String field) {
        if (field != null && !field.equalsIgnoreCase('Id')) {
            if (field.endsWith('__r')) {
                field = field.removeEnd('r') + 'c';
            } else if(! field.endsWith('__c')) {
                field += 'Id';
            }
        }
        return field;
    }
}