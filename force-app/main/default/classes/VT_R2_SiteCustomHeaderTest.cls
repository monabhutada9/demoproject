@IsTest
public with sharing class VT_R2_SiteCustomHeaderTest {

    public static void getCurrentUserProfileSuccessTest() {
        Test.startTest();
        String result = VT_R2_SiteCustomHeaderController.getCurrentUserProfile();
        Test.stopTest();

        System.debug('result: ' + result);

        System.assertEquals('System Administrator', result);
    }

    public static void getReconciliationLogIdSuccessTest() {
        Test.startTest();
        String result = VT_R2_SiteCustomHeaderController.getReconciliationLogId();
        Test.stopTest();

        System.assertEquals(null, result);
    }
}