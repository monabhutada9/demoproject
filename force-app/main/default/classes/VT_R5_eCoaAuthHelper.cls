/**
* @author: Carl Judge
* @date: 13-Apr-20


* @description: Get parameters to authenticate eCOA REST requests. No longer in normal use as we are using mulesoft for this

**/

public class VT_R5_eCoaAuthHelper {
    private static String token;
    private static String sessionId;
    private static String cookie;
    private static eCOA_IntegrationDetails__c integrationDetails = eCOA_IntegrationDetails__c.getInstance();

    public static String getToken() {
        if (token == null) {
            getTokenAndSessionId();
        }
        return token;
    }
    public static String getSessionId() {
        if (sessionId == null) {
            getTokenAndSessionId();
        }
        return sessionId;
    }
    public static String getCookie() {
        if (cookie == null) {
            getLoginCookie();
        }
        return cookie;
    }

    private static void getTokenAndSessionId() {
        token = (String)Cache.Org.get('eCoaToken');
        sessionId = (String)Cache.Org.get('eCoaSessionId');
        if (token == null || sessionId == null) {
            getTokenAndSessionIdFromApi();
        }
    }

    private static void getTokenAndSessionIdFromApi() {
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint('callout:eCOA_Cognito_Credential/token');
        req.setTimeout(120000);
        req.setHeader('Content-Type', 'application/json');
        req.setBody('{' +
            '"username": "{!HTMLENCODE($Credential.Username)}",' +
            '"password": "{!HTMLENCODE($Credential.Password)}",' +
            '"userPoolId": "' + integrationDetails.Cognito_UserPoolId__c + '",' +
            '"clientId": "' + integrationDetails.Cognito_ClientId__c + '"' +
        '}');
        Http http = new Http();
        HttpResponse res = http.send(req);

        Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        Map<String, Object> tokenMap = (Map<String, Object>)responseMap.get('token');
        Map<String, Object> payloadMap = (Map<String, Object>)tokenMap.get('payload');



        sessionId = responseMap.get('sessionId').toString();
        token = tokenMap.get('jwtToken').toString();
        Long tokenExpiresSecs = (Long)payloadMap.get('exp');
        Long nowSecs = Datetime.now().getTime() / 1000;

        Integer expiresIn = (tokenExpiresSecs - nowSecs).intValue();
        addValueToCache('eCoaToken', token, expiresIn);
        addValueToCache('eCoaSessionId', sessionId, expiresIn);
    }


    private static void getLoginCookie() {
        cookie = (String)Cache.Org.get('eCoaCookie');
        if (cookie == null) {
            doLogin();
        }
    }

    private static void doLogin() {
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setTimeout(120000);
        req.setEndpoint(integrationDetails.eCOA_Endpoint__c + '/api/v1/userLogin');

        req.setHeader('Authorization', 'Bearer ' + getToken());
        req.setHeader('Av-Origin-Guid', 'origin_' + getSessionId());
        req.setHeader('X-Av-Session-Id', getSessionId());

        Http http = new Http();
        HttpResponse res = http.send(req);

        Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        Long maxAge = (Long)responseMap.get('maxAge');
        cookie = res.getHeader('set-cookie');
        cookie = cookie.substringBefore(';');
        Integer expiresIn = (maxAge / 1000).intValue();
        addValueToCache('eCoaCookie', cookie, expiresIn);
    }

    private static void addValueToCache(String key, String val, Integer expiresInSeconds) {
        Cache.Org.put(key, val, expiresInSeconds - 30); // set cache TTL to 30 seconds less than expiry to be safe
    }


    /* Code for anon access to test system
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint('callout:eCOA_Cognito_Credential/token');
        req.setTimeout(120000);
        req.setHeader('Content-Type', 'application/json');
        req.setBody('{' +
            '"username": "{!HTMLENCODE($Credential.Username)}",' +
            '"password": "{!HTMLENCODE($Credential.Password)}",' +
            '"userPoolId": "us-east-1_JH0eYLvMN",' +
            '"clientId": "4fq61s4c5qed0jmvcieogv5ecr"' +
            '}');
        Http http = new Http();
        HttpResponse res = http.send(req);

        Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        Map<String, Object> tokenMap = (Map<String, Object>)responseMap.get('token');
        Map<String, Object> payloadMap = (Map<String, Object>)tokenMap.get('payload');
        String sessionId = responseMap.get('sessionId').toString();
        String token = tokenMap.get('jwtToken').toString();

        req = new HttpRequest();
        req.setMethod('GET');
        req.setTimeout(120000);
        req.setEndpoint('https://api.test.altavozclinical.com/api/v1/userLogin');
        req.setHeader('Authorization', 'Bearer ' + token);
        req.setHeader('Av-Origin-Guid', 'origin_' + sessionId);
        req.setHeader('X-Av-Session-Id', sessionId);
        http = new Http();
        res = http.send(req);

        responseMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        Long maxAge = (Long)responseMap.get('maxAge');
        String cookie = res.getHeader('set-cookie');
        cookie = cookie.substringBefore(';');

        Map<String, String> headerMap = new Map<String, String>{
            'content-type' => 'application/json',
            'Authorization' => 'Bearer ' + token,
            'Av-Origin-Guid' => 'origin_' + sessionId,
            'Cookie' => cookie
        };

        String endpoint = 'https://api.test.altavozclinical.com/api/v1/orgs/:orgGuid/studies/:studyGuid/versionsQuery';
        String method = 'POST';
        String action = 'eCOA versionsQuery';

        system.debug(
            new VT_R5_eCoaApiRequest()
                .setStudyGuid('study_f76fbda9-05d0-4a32-84a3-9f114b53c8f0')
                .setEndpoint(endpoint)
                .setMethod(method)
                .setAction(action)
                .setSkipLogging(true)
                .setHeaders(headerMap)
                .setBody(new VT_R5_RequestBuilder_StudyVersion())
                .send()
        );
     */

}