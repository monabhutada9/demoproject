/**
 * Created by user on 23-Apr-20.
 */

@IsTest
public with sharing class VT_R4_StorageLimitsNotificationTest {

    public static void testLimitStorageNotif() {
        VT_R4_StorageLimitsNotification controller = new VT_R4_StorageLimitsNotification();
        Test.StartTest();
        String scheduledDate = '0 0 23 * * ?';
        String jobId = system.schedule('LimitsNotification', scheduledDate, controller);
        Test.stopTest();
        System.assert(jobId instanceof Id);
    }
}