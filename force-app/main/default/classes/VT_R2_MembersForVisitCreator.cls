/**
* @author: Carl Judge
* @date: 19-Feb-19
* @description: Create visit members for new visits with protocol visits
**/

public without sharing class VT_R2_MembersForVisitCreator {
    private Map<String, Set<String>> participantsToFields = new Map<String, Set<String>>{
        'PI' => new Set<String>{ 'VTD1_PI_user__c' },
        'Patient Guide' => new Set<String>{ 'VTD1_Primary_PG__c' },
        'Patient' => new Set<String>{ 'VTD1_Patient_User__c' },
        'Site Coordinator' => new Set<String>{ 'VTR2_SiteCoordinator__c' }
    };

    public List<VTD1_Actual_Visit__c> visitsWithProtocols;
    public Map<Id, VTD1_ProtocolVisit__c> pVisitMap = new Map<Id, VTD1_ProtocolVisit__c>();
    public Map<Id, Case> caseMap = new Map<Id, Case>();
    private Map<Id, Id> patientCaregiverMap = new Map<Id, Id>();
    private Map<Id, User> userMap = new Map<Id, User>();

    public VT_R2_MembersForVisitCreator(List<VTD1_Actual_Visit__c> visitsWithProtocols) {
        this.visitsWithProtocols = visitsWithProtocols;
    }

    public static void createMembers(List<VTD1_Actual_Visit__c> visits) {
        List<VTD1_Actual_Visit__c> visitsWithProtocols = new List<VTD1_Actual_Visit__c>();
        for (VTD1_Actual_Visit__c item : visits) {
            if (item.VTD1_Protocol_Visit__c != null) { visitsWithProtocols.add(item); }
        }

        if (!visitsWithProtocols.isEmpty()) { new VT_R2_MembersForVisitCreator(visitsWithProtocols).execute(); }
    }

    public void execute() {
        getProtocolVisits();
        getCases();
        if (! this.caseMap.isEmpty()) {
            getUsers();
            createNewMembers();
        }
    }

    private void getProtocolVisits() {
        if (!this.pVisitMap.isEmpty()) {
            return;
        }
        List<Id> pVisitIds = new List<Id>();
        for (VTD1_Actual_Visit__c item : this.visitsWithProtocols) {
            pVisitIds.add(item.VTD1_Protocol_Visit__c);
        }

        this.pVisitMap = new Map<Id, VTD1_ProtocolVisit__c>([
            SELECT Id, VTR2_Visit_Participants__c FROM  VTD1_ProtocolVisit__c WHERE Id IN :pVisitIds
        ]);
    }

    private void getCases() {
        if (!this.caseMap.isEmpty()) {
            return;
        }
        Set<String> fields = new Set<String>();
        List<Id> caseIds = new List<Id>();
        for (VTD1_Actual_Visit__c item : this.visitsWithProtocols) {
            for (String participant : getParticipantsForVisit(item)) {
                if (this.participantsToFields.containsKey(participant)) {
                    fields.addAll(this.participantsToFields.get(participant));
                }
            }
            caseIds.add(item.VTD1_Case__c);
        }

        if (! fields.isEmpty()) {
            String query = 'SELECT Id,' + String.join(new List<String>(fields), ',') + ' FROM Case WHERE Id IN : caseIds';
            system.debug(query);

            for (Case item : Database.query(query)) {
                this.caseMap.put(item.Id, item);
            }
        }
    }

    private void getUsers() {
        List<Id> userIds = new List<Id>();
        for (VTD1_Actual_Visit__c visit : this.visitsWithProtocols) {
            if (this.caseMap.containsKey(visit.VTD1_Case__c)) {
                Case cas = this.caseMap.get(visit.VTD1_Case__c);
                for (String participant : getParticipantsForVisit(visit)) {
                    if (this.participantsToFields.containsKey(participant)) {
                        for (String field : this.participantsToFields.get(participant)) {
                            Object userId = cas.get(field);
                            if (userId != null) {
                                userIds.add((Id)userId);
                            }
                        }
                    }
                }
            }
        }

        if (! userIds.isEmpty()) {
            Map<Id, Id> patientAccountIdMap = new Map<Id, Id>();

            for (User item : [
                SELECT Id, Contact.AccountId, Email, Profile.Name
                FROM User
                WHERE Id IN :userIds
            ]) {
                if (item.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
                    patientAccountIdMap.put(item.Contact.AccountId, item.Id);
                }
                userMap.put(item.Id, item);
            }

            if (! patientAccountIdMap.isEmpty()) {
                for (User item : [
                    SELECT Id, Contact.AccountId, Email, Profile.Name
                    FROM User
                    WHERE Contact.AccountId IN : patientAccountIdMap.keySet()
                    AND Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME
                    AND Contact.VTD1_Primary_CG__c = true
                ]) {
                    this.patientCaregiverMap.put(patientAccountIdMap.get(item.Contact.AccountId), item.Id);
                    userMap.put(item.Id, item);
                }
            }
        }
    }


    private void createNewMembers() {
        List<Visit_Member__c> newMembers = new List<Visit_Member__c>();

        for (VTD1_Actual_Visit__c visit : this.visitsWithProtocols) {
            if (this.caseMap.containsKey(visit.VTD1_Case__c)) {
                Case cas = this.caseMap.get(visit.VTD1_Case__c);
                for (String participant : getParticipantsForVisit(visit)) {
                    if (this.participantsToFields.containsKey(participant)) {
                        for (String field : this.participantsToFields.get(participant)) {
                            Object userId = cas.get(field);
                            if (userId != null) {
                                newMembers.add(getNewMember((Id) userId, visit.Id));
                                if (participant == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME &&
                                    this.patientCaregiverMap.containsKey((Id)userId))
                                {
                                    newMembers.add(getNewMember(this.patientCaregiverMap.get((Id)userId), visit.Id));
                                }
                            }
                        }
                    }
                }
            }
        }

        if (! newMembers.isEmpty()) { insert newMembers; }
    }

    private Visit_Member__c getNewMember(Id userId, Id visitId) {
        return new Visit_Member__c(
                RecordTypeId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_VISIT_MEMBER_REGULAR,
                VTD1_Actual_Visit__c = visitId,
                VTD1_Participant_User__c = userId,
                VTR2_DoNotSendNewMemberEmail__c = true,
                VTD1_External_Participant_Email__c = userMap.get(userId).Email,
                VTD1_External_Participant_Type__c = userMap.get(userId).Profile.Name
        );
    }

    private List<String> getParticipantsForVisit(VTD1_Actual_Visit__c visit) {
        String participants = this.pVisitMap.get(visit.VTD1_Protocol_Visit__c).VTR2_Visit_Participants__c;
        return participants != null ? participants.split(';') : new List<String>();
    }
}