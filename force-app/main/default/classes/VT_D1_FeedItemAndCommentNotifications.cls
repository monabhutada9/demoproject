public without sharing class VT_D1_FeedItemAndCommentNotifications {
    public static final Set<String> CASE_PROFILES = new Set<String>{
            'Primary Investigator',
            'Patient',
            'Caregiver',
            'Site Coordinator'
    };
    private static final List<String> CHAT_PROFILES = new List<String>{
            'Primary Investigator',
            'Patient Guide',
            'Patient',
            'Caregiver',
            'Site Coordinator'
    };
    private static List<Profile> profiles = [SELECT Id FROM Profile WHERE Name IN :CASE_PROFILES];
    private static Map<Id, FeedItem> feedItemMap = new Map<Id, FeedItem>();
    private static Map<Id, Set<VTD1_PCF_Chat_Member__c>> pcfChatMembersMap = new Map<Id, Set<VTD1_PCF_Chat_Member__c>>();
    private static Map<Id, VTD1_NotificationC__c> notificationMap = new Map<Id, VTD1_NotificationC__c>();
    private List<SObject> recs = new List<SObject>();
    private List<SObject> recsToProcess = new List<SObject>();
    private List<Id> caseIds = new List<Id>();
    private Map <Id, Id> userIdToCaseIdMap = new Map<Id, Id>();
    private List<Id> chatIds = new List<Id>();
    private Map<Id, List<Id>> parentToUserIdsMap = new Map<Id, List<Id>>();
    private Map<Id, Id> userIdToStudyIdMap = new Map<Id, Id>();
    private List<Id> userIds = new List<Id>();
    private Set<Id> piUserIds = new Set<Id>();
    public void updateNotifications(List<SObject> recs) {
        this.recs = recs;
        System.debug('***getCpuTime()*** Start' + Limits.getCpuTime());
        getCaseAndChatIds();
        System.debug('***getCpuTime()*** After getCaseAndChatIds()' + Limits.getCpuTime());
        getUserIds();
        System.debug('***getCpuTime()*** After getUserIds()' + Limits.getCpuTime());
        if (!this.userIds.isEmpty()) {
            getPiUserIds();
            System.debug('***getCpuTime()*** After getPiUserIds()' + Limits.getCpuTime());
            updateNotifications();
            System.debug('***getCpuTime()*** After updateNotifications()' + Limits.getCpuTime());
            updateUnreadFeedsCount();
            System.debug('***getCpuTime()*** After updateUnreadFeedsCount()' + Limits.getCpuTime());
        }
    }
    private void getCaseAndChatIds() {
        for (SObject item : this.recs) {
            if (item instanceof FeedItem || item instanceof FeedComment) {
                Id parentId = (Id) item.get('ParentId');
                if (parentId != null) {
                    String pType = parentId.getSobjectType().getDescribe().getName();
                    if (pType == 'Case') {
                        this.caseIds.add(parentId);
                    }
                    if (pType == 'VTD1_Chat__c') {
                        this.chatIds.add(parentId);
                    }
                    this.recsToProcess.add(item);
                }
            }
        }
    }
    private void getUserIds() {
        getParentToUserIdsForCase();
        getParentToUserIdsForChat();
        System.debug(this.parentToUserIdsMap);
        Set<Id> visiblePosts = getVisiblePosts();
        Map <String, List <Id>> chatIdToUserIdsMap = new Map<String, List <Id>>();
        Map <String, Id> caseIdToUserIdMap = new Map<String, Id>();
        for (SObject item : this.recs) {
            Id parentId = (Id) item.get('ParentId');
            System.debug('chat parentId' + parentId);
            if (this.parentToUserIdsMap.containsKey(parentId) && (item instanceof FeedItem || item instanceof FeedComment)) {
                        ConnectApi.FeedBody body;
                Boolean isForInternalUsers = false;
                Boolean isNotInVisiblePosts = false;
                if (item instanceof FeedItem) {
                    if (((FeedItem) item).Visibility == 'InternalUsers') {
                        isForInternalUsers = true;
                    } else if (!Test.isRunningTest()) {
                        body = ConnectApi.ChatterFeeds.getFeedElement(null, item.Id).body;
                    }
                } else if (!Test.isRunningTest()) {
                    Id parentFeedId = ConnectApi.ChatterFeeds.getComment(null, item.Id).feedElement.Id;
                    if (!visiblePosts.contains(parentFeedId)) {
                        isNotInVisiblePosts = true;
                    } else {
                        body = ConnectApi.ChatterFeeds.getFeedElement(null, parentFeedId).body;
                    }
                }
                for (Id uId : this.parentToUserIdsMap.get(parentId)) {
                    Boolean has_mention = false;
                        System.debug('chat parentId' + parentId + parentId.getSobjectType().getDescribe().getName());
                        if (parentId.getSobjectType().getDescribe().getName() == 'VTD1_Chat__c') {
                            has_mention = true;
                            if (uId != item.get('CreatedById')) {
                                List <Id> userIds = chatIdToUserIdsMap.get(parentId);
                                if (userIds == null) {
                                    userIds = new List<Id>();
                                    chatIdToUserIdsMap.put(parentId, userIds);
                                }
                                userIds.add(uId);
//                            System.debug('add chat ' + parentId + ' for ' + uId);
                            }
                        } else if (parentId.getSobjectType().getDescribe().getName() == 'Case' && uId != item.get('CreatedById')) {
                            caseIdToUserIdMap.put(parentId, uId);
                            userIdToCaseIdMap.put(uId, parentId);
//                        System.debug('add case ' + parentId + ' for ' + uId);
                        }
                        if (item instanceof FeedItem) {
                        if (isForInternalUsers) {
                                continue;
                            }
                    } else if (!Test.isRunningTest()) {
                        if (isNotInVisiblePosts) {
                                    continue;
                                }
                        }
                        if (body != null) {
                            for (ConnectApi.MessageSegment messageSegment : body.messageSegments) {
                                if (messageSegment instanceof ConnectApi.MentionSegment) {
                                    ConnectApi.MentionSegment mentionSegment = (ConnectApi.MentionSegment) messageSegment;
                                    if (mentionSegment.record.id == uId) {
                                    has_mention = true;
                                    }
                                }
                            }
                        }
                    if (has_mention && uId != (Id) item.get('CreatedById')) {
                            // don't notify message creator
                            this.userIds.add(uId);
                        }
                    }
                }
            }
        if (!userIdToCaseIdMap.isEmpty()) {
            Map <Id, Case> casePCFIdToPatientCaseMap = new Map<Id, Case>([SELECT Id, VTD1_Clinical_Study_Membership__c FROM Case WHERE Id IN :userIdToCaseIdMap.values()]);
            for (Id userId : userIdToCaseIdMap.keySet()) {
                Id casePCF = userIdToCaseIdMap.get(userId);
                userIdToCaseIdMap.put(userId, casePCFIdToPatientCaseMap.get(casePCF).VTD1_Clinical_Study_Membership__c);
            }
        }
        if (!chatIdToUserIdsMap.isEmpty()) {
            /*List <HealthCloudGA__CarePlanTemplate__c> studies = [
                    select Id, VTD1_PIGroupId__c, VTD1_Patient_Group_Id__c, VTD1_DeliveryTeamGroupId__c, VTR2_SCR_Group_Id__c
                    from HealthCloudGA__CarePlanTemplate__c
                    where VTD1_PIGroupId__c in :chatIdToUserIdMap.keySet() or VTD1_Patient_Group_Id__c in :chatIdToUserIdMap.keySet() or
                    VTD1_DeliveryTeamGroupId__c in :chatIdToUserIdMap.keySet() or VTR2_SCR_Group_Id__c in :chatIdToUserIdMap.keySet()
            ];
            System.debug('studies found = ' + studies.size());
            for (HealthCloudGA__CarePlanTemplate__c study : studies) {
                String chatId = null;
                if (chatIdToUserIdMap.containsKey(study.VTD1_PIGroupId__c)) {
                    chatId = study.VTD1_PIGroupId__c;
                } else if (chatIdToUserIdMap.containsKey(study.VTD1_Patient_Group_Id__c)) {
                    chatId = study.VTD1_Patient_Group_Id__c;
                } else if (chatIdToUserIdMap.containsKey(study.VTD1_DeliveryTeamGroupId__c)) {
                    chatId = study.VTD1_DeliveryTeamGroupId__c;
                } else if (chatIdToUserIdMap.containsKey(study.VTR2_SCR_Group_Id__c)) {
                    chatId = study.VTR2_SCR_Group_Id__c;
                }
                System.debug('chat id = ' + chatId);
                if (chatId != null) {
                    Id userId = chatIdToUserIdMap.get(chatId);
                    userIdToStudyIdMap.put(userId, study.Id);
                }
            }*/
            Map <Id, VTD1_Chat__c> chatsMap = new Map<Id, VTD1_Chat__c>([SELECT Id, VTR2_Study__c FROM VTD1_Chat__c WHERE Id IN :chatIdToUserIdsMap.keySet()]);
            for (Id chatId : chatIdToUserIdsMap.keySet()) {
                List <Id> userIds = chatIdToUserIdsMap.get(chatId);
                VTD1_Chat__c chat = chatsMap.get(chatId);
                if (chat != null) {
                    for (Id userId : userIds) {
                        userIdToStudyIdMap.put(userId, chat.VTR2_Study__c);
                    }
                }
            }
        }
        if (!caseIdToUserIdMap.isEmpty()) {
            List <Case> cases = [SELECT Id, VTD1_Clinical_Study_Membership__r.VTD1_Study__c FROM Case WHERE Id IN :caseIdToUserIdMap.keySet()];
            for (Case caseObj : cases) {
//                System.debug('next caseObj ' + caseObj.VTD1_Clinical_Study_Membership__r.VTD1_Study__c);
                Id userId = caseIdToUserIdMap.get(caseObj.Id);
                userIdToStudyIdMap.put(userId, caseObj.VTD1_Clinical_Study_Membership__r.VTD1_Study__c);
            }
        }
    }
    private void getParentToUserIdsForCase() {
        List<VTD1_PCF_Chat_Member__c> items = getPCFChatMembers(caseIds);
//        System.debug('Got items: ' + items);
        for (VTD1_PCF_Chat_Member__c item : items) {
            if (!this.parentToUserIdsMap.containsKey(item.VTD1_PCF__c)) {
                this.parentToUserIdsMap.put(item.VTD1_PCF__c, new List<Id>());
            }
            this.parentToUserIdsMap.get(item.VTD1_PCF__c).add(item.VTD1_User_Id__c);
        }
    }
    private void getParentToUserIdsForChat() {
        if (chatIds.isEmpty()) {
            return;
        }
        for (VTD1_Chat_Member__c item : [
                SELECT VTD1_User__c, VTD1_Chat__c
                FROM VTD1_Chat_Member__c
                WHERE VTD1_Chat__c IN :this.chatIds
                AND VTD1_User__r.Profile.Name IN :CHAT_PROFILES
        ]) {
            if (!this.parentToUserIdsMap.containsKey(item.VTD1_Chat__c)) {
                this.parentToUserIdsMap.put(item.VTD1_Chat__c, new List<Id>());
            }
            this.parentToUserIdsMap.get(item.VTD1_Chat__c).add(item.VTD1_User__c);
        }
    }
    private void getPiUserIds() {
        this.piUserIds = new Map<Id, User>([
                SELECT Id
                FROM User
                WHERE Id IN :this.userIds
                AND Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME
        ]).keySet();
    }
    private void updateNotifications() {
        fillNotificationsMap(this.userIds);
        for (Id uId : this.userIds) {
            Id studyId = userIdToStudyIdMap.get(uId);
            Id caseId = userIdToCaseIdMap.get(uId);
//            System.debug(caseId + ' ' + studyId);
            VTD1_NotificationC__c n = notificationMap.containsKey(uId) ?
                    notificationMap.get(uId) : new VTD1_NotificationC__c(VTD1_Receivers__c = uId, Type__c = 'Messages', VTR2_Study__c = studyId, VTD1_CSM__c = caseId);
//            System.debug('notificationMap.values() ' + n);
            n.Number_of_unread_messages__c = n.Number_of_unread_messages__c == null ? 1 : n.Number_of_unread_messages__c + 1;
            n.VTD1_Read__c = false;
            n.VTR5_HideNotification__c = false;
            n.VTD2_New_Message_Added_DateTme__c = Datetime.now();
            n.Message__c = getMessageText(uId);// + ' ' + n.Number_of_unread_messages__c;
            n.VTD2_DoNotDuplicate__c = true;
            n.Title__c = 'VTD1_Messages';
            n.VTD1_CSM__c = caseId;
            notificationMap.put(uId, n);
        }
        System.debug('$$$notificationMap.size(): ' + notificationMap.size());
        System.debug('***getCpuTime()*** before upsert notifications: ' + Limits.getCpuTime());
        if (notificationMap.size() > 50) {
            Database.executeBatch(new VT_R4_MassUpsertSObjectBatch(notificationMap.values()));
        } else {
        upsert notificationMap.values();
    }
    }
    private Set<Id> getVisiblePosts() {
        Set<Id> postsSet = new Set<Id>();
        List<Id> postsIds = new List<Id>();
        for (SObject item : this.recs) {
            Id parentId = (Id) item.get('ParentId');
            if (this.parentToUserIdsMap.containsKey(parentId)) {
                for (Id uId : this.parentToUserIdsMap.get(parentId)) {
                    if (item instanceof FeedComment) {
                        postsIds.add(((FeedComment) item).FeedItemId);
                    }
                }
            }
        }
        for (FeedItem fItem : getVisiblePostsIds(postsIds)) {
            if (fItem != null) {
                postsSet.add(fItem.Id);
            }
        }
        return postsSet;
    }
    private String getMessageText(Id uId) {
        return this.piUserIds.contains(uId) ? Label.VTD2_NewMessagesInChat : 'VTD2_NewMessagesInChat';
    }
    private List<FeedItem> getVisiblePostsIds(List<Id> postsIds) {
        if (!feedItemMap.keySet().containsAll(postsIds)) {
            feedItemMap.putAll(new Map<Id, FeedItem>([SELECT Id, Visibility FROM FeedItem WHERE Id IN :postsIds AND Visibility = 'AllUsers']));
        }
        List<FeedItem> feedItemList = new List<FeedItem>();
        for (Id postId : postsIds) {
            if (feedItemMap.containsKey(postId)) {
                feedItemList.add(feedItemMap.get(postId));
            }
        }
        return feedItemList;
    }
    public void updateUnreadFeedsCount() {
        Set<Id> usersIds = new Set<Id>();
        for (List<Id> i : this.parentToUserIdsMap.values()) {
            usersIds.addAll(i);
        }
        Map<Id, User> usersInitial = new Map<Id, User>([SELECT Id, VTR3_UnreadFeeds__c FROM User WHERE Id IN :usersIds]);
//        System.debug('usersInitial: ' + usersInitial);
        for (SObject so : recsToProcess) {
            String feedId;
            if (so instanceof FeedItem) {
                feedId = so.Id;
            } else if (so instanceof FeedComment) {
                feedId = String.valueOf(so.get('FeedItemId'));
            }
//            System.debug('current object: ' + so.Id);
            Id parentItem = (Id) so.get('ParentId');
            //if (String.valueOf(parentItem.getSobjectType()).equals('VTD1_Chat__c')) return;
            List<Id> currentUsersIds = this.parentToUserIdsMap.get(parentItem);
//            System.debug('currentUsersIds: ' + currentUsersIds);
            for (Id i : currentUsersIds) {
//                System.debug('current user: ' + i);
                if (!i.equals(UserInfo.getUserId())) {
                    User current = usersInitial.get(i);
                    String unreadFeeds = current.VTR3_UnreadFeeds__c;
                    if (unreadFeeds != null) {
                        //if (unreadFeeds.contains(parentItem)) continue; <- Bad?
                        if (unreadFeeds.contains(feedId)) continue; // should work now
                        current.VTR3_UnreadFeeds__c = unreadFeeds + feedId + ';';
                    } else {
                        current.VTR3_UnreadFeeds__c = feedId + ';';
                    }
                }
            }
        }
        try {
            System.debug('userInitial size$$$: ' + usersInitial.size());
            System.debug('***getCpuTime()*** $$$ before users update: ' + Limits.getCpuTime());
            if (usersInitial.size() > 50) {
                Database.executeBatch(new VT_R4_MassUpsertSObjectBatch(usersInitial.values()));
            } else {
            update usersInitial.values();
            }
        } catch (Exception e) {
            System.debug('unread feeds update failed' + e);
        }
    }
    public List<VTD1_PCF_Chat_Member__c> getPCFChatMembers(List<Id> caseIds) {
        if (!pcfChatMembersMap.keySet().containsAll(caseIds)) {
            List<VTD1_PCF_Chat_Member__c> chatMemberList = [
                    SELECT VTD1_User_Id__c, VTD1_PCF__c
                    FROM VTD1_PCF_Chat_Member__c
                    WHERE VTD1_PCF__c IN :caseIds AND VTD1_User_Id__r.ProfileId IN :profiles
            ];
            for (VTD1_PCF_Chat_Member__c chatMember : chatMemberList) {
                if (!pcfChatMembersMap.containsKey(chatMember.VTD1_PCF__c)) {
                    pcfChatMembersMap.put(chatMember.VTD1_PCF__c, new Set<VTD1_PCF_Chat_Member__c>());
                }
                pcfChatMembersMap.get(chatMember.VTD1_PCF__c).add(chatMember);
            }
        }
        List<VTD1_PCF_Chat_Member__c> pcfChatMemberList = new List<VTD1_PCF_Chat_Member__c>();
        for (Id caseId : caseIds) {
            if (pcfChatMembersMap.containsKey(caseId)) {
                pcfChatMemberList.addAll(pcfChatMembersMap.get(caseId));
            }
        }
        return pcfChatMemberList;
    }

    private static void fillNotificationsMap(List<Id> userIds){
        if (userIds.isEmpty()) {
            return;
        }

        for (VTD1_NotificationC__c item : [
                SELECT Id,
                        Number_of_unread_messages__c,
                        VTD2_DoNotDuplicate__c,
                        VTD1_Receivers__c,
                        VTD1_Read__c,
                        VTD2_New_Message_Added_DateTme__c,
                        Type__c, VTD1_CSM__c
                FROM VTD1_NotificationC__c
                WHERE OwnerId IN :userIds AND Type__c = 'Messages'
                FOR UPDATE
        ]) {
            notificationMap.put(item.VTD1_Receivers__c, item);
        }
    }
}