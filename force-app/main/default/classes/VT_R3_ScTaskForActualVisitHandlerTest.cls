@IsTest
public without sharing class VT_R3_ScTaskForActualVisitHandlerTest {

    public static void testScCreationForStudyTeamOrHomeHealthNurseVisitType() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];

        Case caseRecord = [SELECT Id FROM Case WHERE  RecordType.DeveloperName = 'VTD1_PCF' LIMIT 1];

        Test.startTest();
            insert new VTD1_RTId__c(VTD1_SC_Task_Cancellation__c = '0121N000000oCINQA2');

            VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c(VTD1_Independent_Rater_Flag__c = true, VTD1_VisitType__c = 'Labs', VTR2_Visit_Participants__c = 'Patient Guide');
            insert protocolVisit;
            insert new VTD2_TN_Catalog_Code__c(VTR2_Name_ID__c = 'VTD1_Actual_Visit__c.VTD1_Visit_Name__c', VTD2_T_Task_Unique_Code__c = 'T529', VTD2_T_Category__c = 'Visit Related', VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD1_RTId__c.VTD1_SC_Task_Cancellation__c', VTD2_T_Due_Date__c = 'NOW()', VTD2_T_Actual_Visit__c = 'VTD1_Actual_Visit__c.Id', VTD2_T_SObject_Name__c = 'VTD1_Actual_Visit__c', RecordTypeId = VTD2_TN_Catalog_Code__c.getSObjectType().getDescribe().getRecordTypeInfosByName().get('SCTask').getRecordTypeId(), VTD2_T_Type__c = 'Cancellation', VTD2_T_Patient__c = 'IF (ISBLANK(VTD1_Actual_Visit__c.VTD1_Case__c ),VTD1_Actual_Visit__c.Unscheduled_Visits__c ,VTD1_Actual_Visit__c.VTD1_Case__c )', VTD2_T_Care_Plan_Template__c = 'IF (ISBLANK(VTD1_Actual_Visit__c.VTD1_Case__c ), VTD1_Actual_Visit__c.Unscheduled_Visits__r.VTD1_Study__c ,VTD1_Actual_Visit__c.VTD1_Case__r.VTD1_Study__c )', VTD2_T_Assigned_To_ID__c = 'IF (ISBLANK(VTD1_Actual_Visit__c.VTD1_Case__c ), VTD1_Actual_Visit__c.Unscheduled_Visits__r.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c ,VTD1_Actual_Visit__c.VTD1_Case__r.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c)', VTR3_T_Name__c = 'VTD1_Actual_Visit__c.VTD1_Visit_Name__c', VTD2_T_Subject__c = '"Contact 3rd party to cancel remaining visits for "&VTD1_Actual_Visit__c.VTD2_PatientName_PatientId__c');
            VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c(VTR2_Independent_Rater_Flag__c = true, Sub_Type__c = 'Home Health Nurse', VTD1_Case__c = caseRecord.Id, VTD1_Status__c = 'Requested', VTD1_Protocol_Visit__c = protocolVisit.Id, VTD1_Patient_Removed_Flag__c = true);
            insert actualVisit;
            List<VTD1_SC_Task__c> tasks = [SELECT Name, VTD1_Patient__c, VTD1_Subject__c, VTD1_Due_Date__c, OwnerId, RecordTypeId, VTD1_Actual_Visit__c, VTD1_CarePlanTemplate__c, VTD1_Type__c, VTD2_Category__c FROM VTD1_SC_Task__c WHERE VTD1_Type__c='Cancellation'];
            System.assertEquals(1, tasks.size());
            VTD1_SC_Task__c task = tasks[0];
            System.assertEquals(actualVisit.Id, task.Name);
            //System.assertEquals(user.Id, task.OwnerId);
            System.assertEquals('0121N000000oCINQA2', task.RecordTypeId);
            System.assertEquals(actualVisit.Id, task.VTD1_Actual_Visit__c);
            System.assertEquals(study.Id, task.VTD1_CarePlanTemplate__c);
            System.assertEquals(System.today(), task.VTD1_Due_Date__c);
            System.assertEquals(caseRecord.Id, task.VTD1_Patient__c);
            System.assert(task.VTD1_Subject__c.contains('Contact 3rd party to cancel remaining visits for '));
            System.assertEquals('Cancellation', task.VTD1_Type__c);
            System.assertEquals('Visit Related', task.VTD2_Category__c);
        Test.stopTest();
    }

    public static void testScCreationForHomeHealthNurseVisitType() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];

        Case caseRecord = [SELECT Id FROM Case WHERE  RecordType.DeveloperName = 'VTD1_PCF' LIMIT 1];
        Test.startTest();
            insert new VTD1_RTId__c(VTD1_SC_Task_Cancellation__c = '0121N000000oCINQA2');

            VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c(VTD1_Independent_Rater_Flag__c = true, VTD1_VisitType__c = 'Labs', VTR2_Visit_Participants__c = 'Patient Guide');
            insert protocolVisit;
            insert new VTD2_TN_Catalog_Code__c(VTR2_Name_ID__c = 'VTD1_Actual_Visit__c.VTD1_Visit_Name__c', VTD2_T_Task_Unique_Code__c = 'T529', VTD2_T_Category__c = 'Visit Related', VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD1_RTId__c.VTD1_SC_Task_Cancellation__c', VTD2_T_Due_Date__c = 'NOW()', VTD2_T_Actual_Visit__c = 'VTD1_Actual_Visit__c.Id', VTD2_T_SObject_Name__c = 'VTD1_Actual_Visit__c', RecordTypeId = VTD2_TN_Catalog_Code__c.getSObjectType().getDescribe().getRecordTypeInfosByName().get('SCTask').getRecordTypeId(), VTD2_T_Type__c = 'Cancellation', VTD2_T_Patient__c = 'IF (ISBLANK(VTD1_Actual_Visit__c.VTD1_Case__c ),VTD1_Actual_Visit__c.Unscheduled_Visits__c ,VTD1_Actual_Visit__c.VTD1_Case__c )', VTD2_T_Care_Plan_Template__c = 'IF (ISBLANK(VTD1_Actual_Visit__c.VTD1_Case__c ), VTD1_Actual_Visit__c.Unscheduled_Visits__r.VTD1_Study__c ,VTD1_Actual_Visit__c.VTD1_Case__r.VTD1_Study__c )', VTD2_T_Assigned_To_ID__c = 'IF (ISBLANK(VTD1_Actual_Visit__c.VTD1_Case__c ), VTD1_Actual_Visit__c.Unscheduled_Visits__r.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c ,VTD1_Actual_Visit__c.VTD1_Case__r.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c)', VTR3_T_Name__c = 'VTD1_Actual_Visit__c.VTD1_Visit_Name__c', VTD2_T_Subject__c = '"Contact 3rd party to cancel remaining visits for "&VTD1_Actual_Visit__c.VTD2_PatientName_PatientId__c');
            VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c(Sub_Type__c = 'Home Health Nurse', VTD1_Case__c = caseRecord.Id, VTD1_Status__c = 'Requested', VTD1_Protocol_Visit__c = protocolVisit.Id, VTD1_Patient_Removed_Flag__c = true);
            insert actualVisit;
            List<VTD1_SC_Task__c> tasks = [SELECT Name, VTD1_Patient__c, VTD1_Subject__c, VTD1_Due_Date__c, OwnerId, RecordTypeId, VTD1_Actual_Visit__c, VTD1_CarePlanTemplate__c, VTD1_Type__c, VTD2_Category__c FROM VTD1_SC_Task__c WHERE VTD1_Type__c='Cancellation'];
            System.assertEquals(1, tasks.size());
            VTD1_SC_Task__c task = tasks[0];
            System.assertEquals(actualVisit.Id, task.Name);
            //System.assertEquals(user.Id, task.OwnerId);
            System.assertEquals('0121N000000oCINQA2', task.RecordTypeId);
            System.assertEquals(actualVisit.Id, task.VTD1_Actual_Visit__c);
            System.assertEquals(study.Id, task.VTD1_CarePlanTemplate__c);
            System.assertEquals(System.today(), task.VTD1_Due_Date__c);
            System.assertEquals(caseRecord.Id, task.VTD1_Patient__c);
            System.assert(task.VTD1_Subject__c.contains('Contact 3rd party to cancel remaining visits for '));
            System.assertEquals('Cancellation', task.VTD1_Type__c);
            System.assertEquals('Visit Related', task.VTD2_Category__c);
        Test.stopTest();
    }

    public static void testScCreationForPhlebotomistVisitType() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];

        Case caseRecord = [SELECT Id FROM Case WHERE  RecordType.DeveloperName = 'VTD1_PCF' LIMIT 1];
        Test.startTest();
            insert new VTD1_RTId__c(VTD1_SC_Task_Cancellation__c = '0121N000000oCINQA2');

            VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c(VTD1_Independent_Rater_Flag__c = true, VTD1_VisitType__c = 'Labs', VTR2_Visit_Participants__c = 'Patient Guide');
            insert protocolVisit;
            insert new VTD2_TN_Catalog_Code__c(VTR2_Name_ID__c = 'VTD1_Actual_Visit__c.VTD1_Visit_Name__c', VTD2_T_Task_Unique_Code__c = 'T529', VTD2_T_Category__c = 'Visit Related', VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD1_RTId__c.VTD1_SC_Task_Cancellation__c', VTD2_T_Due_Date__c = 'NOW()', VTD2_T_Actual_Visit__c = 'VTD1_Actual_Visit__c.Id', VTD2_T_SObject_Name__c = 'VTD1_Actual_Visit__c', RecordTypeId = VTD2_TN_Catalog_Code__c.getSObjectType().getDescribe().getRecordTypeInfosByName().get('SCTask').getRecordTypeId(), VTD2_T_Type__c = 'Cancellation', VTD2_T_Patient__c = 'VTD1_Actual_Visit__c.VTD1_Case__r.Id', VTD2_T_Care_Plan_Template__c = 'IF (ISBLANK(VTD1_Actual_Visit__c.VTD1_Case__c ), VTD1_Actual_Visit__c.Unscheduled_Visits__r.VTD1_Study__c ,VTD1_Actual_Visit__c.VTD1_Case__r.VTD1_Study__c )', VTD2_T_Assigned_To_ID__c = 'IF (ISBLANK(VTD1_Actual_Visit__c.VTD1_Case__c ), VTD1_Actual_Visit__c.Unscheduled_Visits__r.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c ,VTD1_Actual_Visit__c.VTD1_Case__r.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c)', VTR3_T_Name__c = 'VTD1_Actual_Visit__c.VTD1_Visit_Name__c', VTD2_T_Subject__c = '"Contact 3rd party to cancel remaining visits for "&VTD1_Actual_Visit__c.VTD2_PatientName_PatientId__c');
            VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c(Sub_Type__c = 'Phlebotomist', VTD1_Case__c = caseRecord.Id, VTD1_Status__c = 'Requested', VTD1_Protocol_Visit__c = protocolVisit.Id, VTD1_Patient_Removed_Flag__c = true);
            insert actualVisit;
            List<VTD1_SC_Task__c> tasks = [SELECT Name, VTD1_Patient__c, VTD1_Subject__c, VTD1_Due_Date__c, OwnerId, RecordTypeId, VTD1_Actual_Visit__c, VTD1_CarePlanTemplate__c, VTD1_Type__c, VTD2_Category__c FROM VTD1_SC_Task__c WHERE VTD1_Type__c='Cancellation'];
            System.assertEquals(1, tasks.size());
            VTD1_SC_Task__c task = tasks[0];
            System.assertEquals(actualVisit.Id, task.Name);
            //System.assertEquals(user.Id, task.OwnerId);
            System.assertEquals('0121N000000oCINQA2', task.RecordTypeId);
            System.assertEquals(actualVisit.Id, task.VTD1_Actual_Visit__c);
            System.assertEquals(study.Id, task.VTD1_CarePlanTemplate__c);
            System.assertEquals(System.today(), task.VTD1_Due_Date__c);
            System.assertEquals(caseRecord.Id, task.VTD1_Patient__c);
            System.assert(task.VTD1_Subject__c.contains('Contact 3rd party to cancel remaining visits for '));
            System.assertEquals('Cancellation', task.VTD1_Type__c);
            System.assertEquals('Visit Related', task.VTD2_Category__c);
        Test.stopTest();
    }

    public static void testScCreationForHealthCareProfessionalVisitType() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];

        Case caseRecord = [SELECT Id FROM Case WHERE  RecordType.DeveloperName = 'VTD1_PCF' LIMIT 1];
        Test.startTest();
            insert new VTD1_RTId__c(VTD1_SC_Task_Cancellation__c = '0121N000000oCINQA2');

            VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c(VTD1_VisitType__c = 'Health Care Professional (HCP)', VTR2_Visit_Participants__c = 'Patient Guide');
            insert protocolVisit;
            insert new VTD2_TN_Catalog_Code__c(VTR2_Name_ID__c = 'VTD1_Actual_Visit__c.VTD1_Visit_Name__c', VTD2_T_Task_Unique_Code__c = 'T529', VTD2_T_Category__c = 'Visit Related', VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD1_RTId__c.VTD1_SC_Task_Cancellation__c', VTD2_T_Due_Date__c = 'NOW()', VTD2_T_Actual_Visit__c = 'VTD1_Actual_Visit__c.Id', VTD2_T_SObject_Name__c = 'VTD1_Actual_Visit__c', RecordTypeId = VTD2_TN_Catalog_Code__c.getSObjectType().getDescribe().getRecordTypeInfosByName().get('SCTask').getRecordTypeId(), VTD2_T_Type__c = 'Cancellation', VTD2_T_Patient__c = 'VTD1_Actual_Visit__c.VTD1_Case__r.Id', VTD2_T_Care_Plan_Template__c = 'IF (ISBLANK(VTD1_Actual_Visit__c.VTD1_Case__c ), VTD1_Actual_Visit__c.Unscheduled_Visits__r.VTD1_Study__c ,VTD1_Actual_Visit__c.VTD1_Case__r.VTD1_Study__c )', VTD2_T_Assigned_To_ID__c = 'IF (ISBLANK(VTD1_Actual_Visit__c.VTD1_Case__c ), VTD1_Actual_Visit__c.Unscheduled_Visits__r.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c ,VTD1_Actual_Visit__c.VTD1_Case__r.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c)', VTR3_T_Name__c = 'VTD1_Actual_Visit__c.VTD1_Visit_Name__c', VTD2_T_Subject__c = '"Contact 3rd party to cancel remaining visits for "&VTD1_Actual_Visit__c.VTD2_PatientName_PatientId__c');
            VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c(VTD1_Case__c = caseRecord.Id, VTD1_Status__c = 'Requested', VTD1_Protocol_Visit__c = protocolVisit.Id, VTD1_Patient_Removed_Flag__c = true);
            insert actualVisit;
            List<VTD1_SC_Task__c> tasks = [SELECT Name, VTD1_Patient__c, VTD1_Subject__c, VTD1_Due_Date__c, OwnerId, RecordTypeId, VTD1_Actual_Visit__c, VTD1_CarePlanTemplate__c, VTD1_Type__c, VTD2_Category__c FROM VTD1_SC_Task__c WHERE VTD1_Type__c='Cancellation'];
            System.assertEquals(1, tasks.size());
            VTD1_SC_Task__c task = tasks[0];
            System.assertEquals(actualVisit.Id, task.Name);
            //System.assertEquals(user.Id, task.OwnerId);
            System.assertEquals('0121N000000oCINQA2', task.RecordTypeId);
            System.assertEquals(actualVisit.Id, task.VTD1_Actual_Visit__c);
            System.assertEquals(study.Id, task.VTD1_CarePlanTemplate__c);
            System.assertEquals(System.today(), task.VTD1_Due_Date__c);
            System.assertEquals(caseRecord.Id, task.VTD1_Patient__c);
            System.assert(task.VTD1_Subject__c.contains('Contact 3rd party to cancel remaining visits for '));
            System.assertEquals('Cancellation', task.VTD1_Type__c);
            System.assertEquals('Visit Related', task.VTD2_Category__c);
        Test.stopTest();
    }

    public static void testScCreationForPersonalServiceCenterVisitType() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];

        Case caseRecord = [SELECT Id FROM Case WHERE  RecordType.DeveloperName = 'VTD1_PCF' LIMIT 1];
        Test.startTest();
            insert new VTD1_RTId__c(VTD1_SC_Task_Cancellation__c = '0121N000000oCINQA2');

            VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c(VTD1_Independent_Rater_Flag__c = true, VTR2_Visit_Participants__c = 'Patient Guide');
            insert protocolVisit;
            insert new VTD2_TN_Catalog_Code__c(VTR2_Name_ID__c = 'VTD1_Actual_Visit__c.VTD1_Visit_Name__c', VTD2_T_Task_Unique_Code__c = 'T529', VTD2_T_Category__c = 'Visit Related', VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD1_RTId__c.VTD1_SC_Task_Cancellation__c', VTD2_T_Due_Date__c = 'NOW()', VTD2_T_Actual_Visit__c = 'VTD1_Actual_Visit__c.Id', VTD2_T_SObject_Name__c = 'VTD1_Actual_Visit__c', RecordTypeId = VTD2_TN_Catalog_Code__c.getSObjectType().getDescribe().getRecordTypeInfosByName().get('SCTask').getRecordTypeId(), VTD2_T_Type__c = 'Cancellation', VTD2_T_Patient__c = 'VTD1_Actual_Visit__c.VTD1_Case__r.Id', VTD2_T_Care_Plan_Template__c = 'IF (ISBLANK(VTD1_Actual_Visit__c.VTD1_Case__c ), VTD1_Actual_Visit__c.Unscheduled_Visits__r.VTD1_Study__c ,VTD1_Actual_Visit__c.VTD1_Case__r.VTD1_Study__c )', VTD2_T_Assigned_To_ID__c = 'IF (ISBLANK(VTD1_Actual_Visit__c.VTD1_Case__c ), VTD1_Actual_Visit__c.Unscheduled_Visits__r.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c ,VTD1_Actual_Visit__c.VTD1_Case__r.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c)', VTR3_T_Name__c = 'VTD1_Actual_Visit__c.VTD1_Visit_Name__c', VTD2_T_Subject__c = '"Contact 3rd party to cancel remaining visits for "&VTD1_Actual_Visit__c.VTD2_PatientName_PatientId__c');
            VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c(Sub_Type__c = 'PSC', VTD1_Case__c = caseRecord.Id, VTD1_Status__c = 'Requested', VTD1_Protocol_Visit__c = protocolVisit.Id, VTD1_Patient_Removed_Flag__c = true);
            insert actualVisit;
            List<VTD1_SC_Task__c> tasks = [SELECT Name, VTD1_Patient__c, VTD1_Subject__c, VTD1_Due_Date__c, OwnerId, RecordTypeId, VTD1_Actual_Visit__c, VTD1_CarePlanTemplate__c, VTD1_Type__c, VTD2_Category__c FROM VTD1_SC_Task__c WHERE VTD1_Type__c='Cancellation'];
            System.assertEquals(1, tasks.size());
            VTD1_SC_Task__c task = tasks[0];
            System.assertEquals(actualVisit.Id, task.Name);
            //System.assertEquals(user.Id, task.OwnerId);
            System.assertEquals('0121N000000oCINQA2', task.RecordTypeId);
            System.assertEquals(actualVisit.Id, task.VTD1_Actual_Visit__c);
            System.assertEquals(study.Id, task.VTD1_CarePlanTemplate__c);
            System.assertEquals(System.today(), task.VTD1_Due_Date__c);
            System.assertEquals(caseRecord.Id, task.VTD1_Patient__c);
            System.assert(task.VTD1_Subject__c.contains('Contact 3rd party to cancel remaining visits for '));
            System.assertEquals('Cancellation', task.VTD1_Type__c);
            System.assertEquals('Visit Related', task.VTD2_Category__c);
        Test.stopTest();
    }
}