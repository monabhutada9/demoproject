/**
 * Created by user on 13-Jan-20.
 */

@IsTest
private class VT_R4_VisitMemberNotificationContrTest {
	@TestSetup
	static void setupMethod(){
		Test.startTest();
		VT_R3_GlobalSharing.disableForTest = true;

		DomainObjects.Account_t patientAccount = new DomainObjects.Account_t().setRecordTypeByName('Sponsor');
		Account account = (Account) patientAccount.persist();

		DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
				.setFirstName('Patient')
				.setLastName('Patient')
				.setAccountId(account.Id);

		DomainObjects.User_t patientUser = new DomainObjects.User_t()
				.addContact(patientContact)
				.setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);

		DomainObjects.Case_t patientCase = new DomainObjects.Case_t()
				.addVTD1_Primary_PG(new DomainObjects.User_t())
				.addPIUser(new DomainObjects.User_t())
				.addVTD1_Patient_User(patientUser)
				.addContact(patientContact)
				.setRecordTypeByName('CarePlan')
				.setAccountId(account.Id);

		DomainObjects.VTD1_ProtocolVisit_t protocolVisit = new DomainObjects.VTD1_ProtocolVisit_t();

		DomainObjects.VTD1_Actual_Visit_t actualVisit = new DomainObjects.VTD1_Actual_Visit_t()
				.addVTD1_Case(patientCase)
				.addVTD1_ProtocolVisit_t(protocolVisit);

		new DomainObjects.Visit_Member_t()
				.addVTD1_Actual_Visit(actualVisit)
				.addVTD1_Participant_User(patientUser)
				.persist();
		new DomainObjects.Visit_Member_t()
				.addVTD1_Actual_Visit(actualVisit)
				.setVTD1_External_Participant_Type('HHN')
				.persist();
		Test.stopTest();
	}

	@IsTest
	static void emailTextTest() {
		VT_R4_VisitMemberNotificationController controller = new VT_R4_VisitMemberNotificationController();
		List<Visit_Member__c> visitMemberList = [SELECT Id, VTD1_Member_Type__c, VTD1_Actual_Visit__c FROM Visit_Member__c];

		Test.startTest();
		for(Visit_Member__c member : visitMemberList){
			System.debug('Visit mamber type ' + member.VTD1_Member_Type__c);
			controller.visitMemberId = member.Id;
			controller.allLabels = 'VT_R2_VisitEmailSalutaion,'
					+'{\'linkText\':\'VTR2_VisitStudyHub\';\'linkURL\':\'{!$Setup.VTD1_RTId__c.VTD2_Patient_Community_URL__c}\'},';
			controller.getEmailTextList();
			controller.getConferenceMember(member.Id);
		}
		System.debug('controller.replaceFields ' + controller.replaceFields('Hello #Visit_participant,' +
				'#scheduled_date,#scheduled_time, #timezone', 'en_US'));
		Test.stopTest();
	}


	@IsTest
	static void testBehaviour() {
		VTD1_Actual_Visit__c visit = [SELECT Id FROM VTD1_Actual_Visit__c LIMIT 1];
		VT_R4_VisitMemberNotificationController controller = new VT_R4_VisitMemberNotificationController();
		controller.visitId = visit.Id;

		Test.startTest();
		System.assert(controller.getVisitMember() != null);
		Test.stopTest();

	}
}