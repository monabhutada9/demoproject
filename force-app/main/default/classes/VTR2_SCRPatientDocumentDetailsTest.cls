@IsTest
private class VTR2_SCRPatientDocumentDetailsTest {

    public with sharing class CalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest param1) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"controllerValues":{"Medical Records flow":0},"defaultValue":{"attributes":null,"label":"Pending Certification","validFor":[0],"value":"Pending Certification"},"eTag":"13d3354a9bb61c834d711380fa5fbf85","url":"/services/data/v45.0/ui-api/object-info/VTD1_Document__c/picklist-values/0121N000000oCGqQAM/VTD1_Status__c","values":[{"attributes":null,"label":"Approved","validFor":[0],"value":"Approved"},{"attributes":null,"label":"Pending Approval","validFor":[0],"value":"Pending Approval"},{"attributes":null,"label":"Rejected","validFor":[0],"value":"Rejected"},{"attributes":null,"label":"Pending Certification","validFor":[0],"value":"Pending Certification"}]}');
            res.setStatusCode(200);
            return res;
        }
    }

    @TestSetup
    static void testBehavior() {
        Case caseRecord = (Case) new DomainObjects.Case_t().persist();
        VTD1_Document__c document = new VTD1_Document__c (
                RecordTypeId = '0121N000000oCGqQAM',
                VTD1_Status__c = 'Approved'
        );
        insert document;
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.PathOnClient = 'test.txt';
        contentVersion.Title = 'Test file';
        contentVersion.VersionData = Blob.valueOf('Test Data');
        insert contentVersion;
        Id contentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id].ContentDocumentId;

        Test.startTest();
        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = document.Id;
        contentDocumentLink.ContentDocumentId = contentDocumentId;
        contentDocumentLink.ShareType = 'I';
        contentDocumentLink.Visibility = 'AllUsers';
        insert contentDocumentLink;
        Test.stopTest();
    }

    @IsTest
    static void getUnsupportedFormatWarningAsBase64PdfTest(){
        Test.startTest();
        VTR2_SCRPatientDocumentDetails.getUnsupportedFormatWarningAsBase64Pdf();
        Test.stopTest();
    }
    @IsTest
    static void getVisitsTest(){
        Id cas = [SELECT Id FROM Case LIMIT 1].Id;
        VTR2_SCRPatientDocumentDetails.getVisitPicklistEntries(VTR2_SCRPatientDocumentDetails.getVisits(cas));
    }


    @IsTest
    static void getDocumentTest_Positive() {
        Id docId = [SELECT Id FROM VTD1_Document__c].Id;

        Test.startTest();
        VTD1_Document__c document = VTR2_SCRPatientDocumentDetails.getDocument(docId);
        Test.stopTest();

        System.assertEquals('0121N000000oCGqQAM', document.RecordTypeId, 'Incorrect Document Name');
        System.assertEquals(docId, document.Id, 'Incorrect Document Id');
    }

    @IsTest
    static void getDocumentTest_Negative() {
        Id docId = [SELECT Id FROM VTD1_Document__c].Id;
        docId = String.valueOf(docId).substring(0, 14) + '-';

        Test.startTest();
        VTD1_Document__c document = VTR2_SCRPatientDocumentDetails.getDocument(docId);
        Test.stopTest();

        System.assertEquals(null, document, 'Incorrect Document Name');
    }

    @IsTest
    static void getContentDocumentLinkTest_Positive() {
        Id docId = [SELECT Id FROM VTD1_Document__c].Id;

        Test.startTest();
        ContentDocumentLink contentDocumentLink = VTR2_SCRPatientDocumentDetails.getContentDocumentLink(docId);
        Test.stopTest();

        System.assertEquals(docId, contentDocumentLink.LinkedEntityId, 'Incorrect contentDocumentLink');
    }

    @IsTest
    static void getContentDocumentLinkTest_Negative() {
        Id docId = [SELECT Id FROM VTD1_Document__c].Id;
        docId = String.valueOf(docId).substring(0, 14) + '-';

        Test.startTest();
        ContentDocumentLink contentDocumentLink = VTR2_SCRPatientDocumentDetails.getContentDocumentLink(docId);
        Test.stopTest();

        System.assertEquals(null, contentDocumentLink, 'Incorrect contentDocumentLink');
    }

    @IsTest
    static void getContentAsPDFTest() {
        Id docId = [SELECT Id FROM VTD1_Document__c].Id;
        ContentDocumentLink link = [SELECT ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :docId];

        Test.startTest();
        String documentData = VTR2_SCRPatientDocumentDetails.getContentAsPDF(link);
        Test.stopTest();

        System.assertEquals('TestData', documentData, 'Incorrect documentData');
    }

    @IsTest
    static void getDocumentByIdTest() {
        Id docId = [SELECT Id FROM VTD1_Document__c].Id;
        Test.setMock(HttpCalloutMock.class, new CalloutMock());
        Test.startTest();
        VTR2_SCRPatientDocumentDetails.DocumentData wrapper = VTR2_SCRPatientDocumentDetails.getDocumentById(docId);
        Test.stopTest();
        String str = 'TestData';
        System.assertEquals(str.length(), wrapper.pdfData.length(), 'Incorrect documentData');
    }

    @IsTest
    static void updateDocumentTest_Positive() {
        Test.startTest();
        Contact con = new Contact(LastName = 'Test');
        insert con;
        Profile pr = [SELECT Name FROM Profile WHERE Name = :VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME];
        String scrUserName = 'scr_testName';
        User us = new User(
                ProfileId = pr.Id,
                ContactId = con.Id,
                Email = scrUserName + '@test.com',
                Username = scrUserName + '@test.com' + '_user',
                LastName = 'Test',
                Alias = 'Test',
                CommunityNickname = 'Test',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US'
        );
        insert us;
        VTD1_Document__c document = [SELECT RecordTypeId, VTD1_Status__c FROM VTD1_Document__c LIMIT 1];
        System.runAs(us) {
            try {
                Boolean IsSuccess = VTR2_SCRPatientDocumentDetails.updateDocument(document);
                System.assertEquals(true, IsSuccess);
            } catch (Exception e) {
            }
        }
        Test.stopTest();
    }

    @IsTest
    static void updateDocumentTest_WrongProfile_Negative() {
        Profile pr = [SELECT Name FROM Profile WHERE Name = 'Standard User'];
        String scrUserName = 'scr_testName';
        User us = new User(
                ProfileId = pr.Id,
                Email = scrUserName + '@test.com',
                Username = scrUserName + '@test.com' + '_user',
                LastName = 'Test',
                Alias = 'Test',
                CommunityNickname = 'Test',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US'
        );
        Test.startTest();
        insert us;
        Test.stopTest();
        VTD1_Document__c document = [SELECT RecordTypeId, VTD1_Status__c FROM VTD1_Document__c LIMIT 1];

        System.runAs(us) {

            try {
                Boolean IsSuccess = VTR2_SCRPatientDocumentDetails.updateDocument(document);
            } catch (Exception exc) {
                System.assertEquals('Script-thrown exception', exc.getMessage());
            }

        }
    }

    @IsTest
    static void getPicklistEntriesTest() {
        VTD1_Document__c document = [SELECT RecordTypeId, VTD1_Status__c FROM VTD1_Document__c LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new CalloutMock());
        Test.startTest();
        List<VTR2_SCRPatientDocumentDetails.PicklistEntry> picklist = VTR2_SCRPatientDocumentDetails.getPicklistEntries(
                VTD1_Document__c.SObjectType,
                document.RecordTypeId,
                'VTD1_Status__c'
        );
        Test.stopTest();

        System.assertEquals(4, picklist.size());
    }

    @IsTest
    static void getProfileNameOfCurrentUserTest() {
        Test.startTest();
        Profile pr = [SELECT Name FROM Profile WHERE Name = 'Standard User'];
        String scrUserName = 'scr_testName';
        User us = new User(
                ProfileId = pr.Id,
                Email = scrUserName + '@test.com',
                Username = scrUserName + '@test.com' + '_user',
                LastName = 'Test',
                Alias = 'Test',
                CommunityNickname = 'Test',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US'
        );
        insert us;
        System.runAs(us) {


            String profileName = VTR2_SCRPatientDocumentDetails.getProfileNameOfCurrentUser();


            System.assertEquals(pr.Name, profileName, 'Incorrect profile name');
        }
        Test.stopTest();
    }

    @IsTest
    static void sendDocWithDocuSignTest() {
        VTD1_Document__c document = [SELECT RecordTypeId, VTD1_Status__c FROM VTD1_Document__c LIMIT 1];

        Test.startTest();
        String error = VTR2_SCRPatientDocumentDetails.sendDocWithDocuSign(document.Id);
        Test.stopTest();

        System.assertEquals(null, error, 'Incorrect error value');
    }

    @IsTest
    static void sendDocWithDocuSignTest_EmptyDocId() {
        Test.startTest();
        try {
            String error = VTR2_SCRPatientDocumentDetails.sendDocWithDocuSign(null);
        } catch (Exception exc) {
            System.assertEquals('Script-thrown exception', exc.getMessage(), 'Incorrect exception value');
        }
        Test.stopTest();
    }

    @IsTest
    static void getAndCreateDocuSignEnvelopeIdTest() {
        VTD1_Document__c document = [SELECT RecordTypeId, VTD1_Status__c FROM VTD1_Document__c LIMIT 1];

        Test.startTest();
        String docuSignEnvelopeId = VTR2_SCRPatientDocumentDetails.getAndCreateDocuSignEnvelopeId(document.Id);
        List<dsfs__DocuSign_Envelope__c> envelopes = [
                SELECT
                        Id,
                        dsfs__DocuSign_Email_Subject__c,
                        dsfs__Source_Object__c,
                        dsfs__DocumentID__c
                FROM dsfs__DocuSign_Envelope__c
        ];
        Test.stopTest();

        for (dsfs__DocuSign_Envelope__c envelope : envelopes) {
            System.assertEquals('Documents for your DocuSign Signature', envelope.dsfs__DocuSign_Email_Subject__c, 'Incorrect DocuSign_Email_Subject__c value');
            System.assertEquals(document.Id, envelope.dsfs__Source_Object__c, 'Incorrect Source_Object__c value');
            System.assertEquals('Select Document ID', envelope.dsfs__DocumentID__c, 'Incorrect dsfs__DocumentID__c value');
        }
    }

    @IsTest
    static void testApprovalProcess_Approve(){
        Test.startTest();
        VT_R3_GlobalSharing.disableForTest = true;
        User userSCR = (User) new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME)
                .addContact(new DomainObjects.Contact_t().setLastName('TestSCR'))
                .persist();

        User userPG = (User) new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME)
                .persist();

        User userPI = (User) new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME)
                .addContact(new DomainObjects.Contact_t().setLastName('TestSCR').setEmail('test@test.com'))
                .persist();
        Case cas = [SELECT Id FROM Case LIMIT 1];
        cas.VTD1_PI_user__c = userPI.Id;
        update cas;

        VTD1_Document__c doc = [SELECT RecordTypeId, VTD1_Status__c FROM VTD1_Document__c LIMIT 1];

        doc.VTD1_Clinical_Study_Membership__c = cas.Id;

        doc.VTD1_Status__c = 'Pending Approval';
        doc.VTD1_Files_have_not_been_edited__c = true;
        doc.VTR2_SCR_Approver__c = userSCR.Id;
        doc.VTD1_PG_Approver__c = userPG.Id;
        update doc;

        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting approval');
        req.setObjectId(doc.Id);
        Approval.ProcessResult result = Approval.process(req);
        VTR2_SCRPatientDocumentDetails.closeApprovalProcess(doc, 'Approved');
        List<ProcessInstanceWorkitem> res = [SELECT Id FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId = :doc.Id];
        Test.stopTest();
    }

    @IsTest
    static void testApprovalProcess_Reject(){
        Test.startTest();
        VT_R3_GlobalSharing.disableForTest = true;
        User userSCR = (User) new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME)
                .addContact(new DomainObjects.Contact_t().setLastName('TestSCR'))
                .persist();

        User userPG = (User) new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME)
                .persist();

        User userPI = (User) new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME)
                .addContact(new DomainObjects.Contact_t().setLastName('TestSCR').setEmail('test@test.com'))
                .persist();

        Case cas = [SELECT Id FROM Case LIMIT 1];
        cas.VTD1_PI_user__c = userPI.Id;
        update cas;

        VTD1_Document__c doc = [SELECT RecordTypeId, VTD1_Status__c FROM VTD1_Document__c LIMIT 1];
        doc.VTD1_Clinical_Study_Membership__c = cas.Id;
        doc.VTD1_Status__c = 'Pending Approval';
        doc.VTD1_Files_have_not_been_edited__c = true;
        doc.VTR2_SCR_Approver__c = userSCR.Id;
        doc.VTD1_PG_Approver__c = userPG.Id;
        update doc;

        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting approval');
        req.setObjectId(doc.Id);
        VTR2_SCRPatientDocumentDetails.closeApprovalProcess(doc, 'Rejected');
        Test.stopTest();
    }

    @Future
    static void insertUser() {
        Profile pr = [SELECT Name FROM Profile WHERE Name = :VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME];
        String scrUserName = 'scr_testName';
        User us = new User(
                ProfileId = pr.Id,
                Email = scrUserName + '@test.com',
                Username = scrUserName + '@test.com' + '_user',
                LastName = 'Test',
                Alias = 'Test',
                CommunityNickname = 'Test',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                UserRoleId = null
        );
        insert us;
    }
}