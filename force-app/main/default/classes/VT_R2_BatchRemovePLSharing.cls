/**
* @author: Carl Judge
* @date: 25-Sep-19
* @description: 
**/

global without sharing class VT_R2_BatchRemovePLSharing implements Database.Batchable<sObject>, Database.Stateful {

    public static final List<String> SHARE_NAMES = new List<String>{
        'AccountShare',
        'CaseShare',
        'UserShare'
    };
    public List<String> shareNames;

    global Database.QueryLocator start(Database.BatchableContext bc) {
        if (shareNames == null) { shareNames = SHARE_NAMES.clone(); }
        String shareName = shareNames.remove(0);
        String query = 'SELECT Id FROM ' + shareName + ' WHERE UserOrGroup.Profile.Name = \'Project Lead\'';
        if (shareName == 'VTD1_Document__Share') {
            query += ' AND (Parent.RecordType.DeveloperName = \'VTD1_Patient_eligibility_assessment_form\' OR Parent.RecordType.DeveloperName = \'Patient_Eligibility_Assessment_Form_Rejected\')';
        }
        if (shareName == 'UserShare') {
            query += ' AND User.Profile.Name = \'Patient\'';
        }
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<sObject> scope){
        Database.delete(scope, false);
    }

    global void finish(Database.BatchableContext bc){
        if (! shareNames.isEmpty()) {
            launchNextBatch();
        }
    }

    private void launchNextBatch() {
        VT_R2_BatchRemovePLSharing nextIteration = new VT_R2_BatchRemovePLSharing();
        nextIteration.shareNames = shareNames;
        Database.executeBatch(nextIteration);
    }
}