public with sharing class VT_D1_PIElectronicDataCaptureController {

    @AuraEnabled(cacheable=true)
    public static String getEDC() {
        try {
            return getElectronicDataCaptureList();
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    //----------------------------- HELPERS ----------------------------------//

    private static String getElectronicDataCaptureList() {
        Map<String, String> studyIdToVirtualSiteIdMap = new Map<String, String>();
        Map<String, String> studyIdToStudyNameMap = new Map<String, String>();
        Set<Id> virtualSiteIdSet = new Set<Id>();
		List<Study_Team_Member__c> studyTeamMembList = getStudyTeamMemberList();
		if(Test.isRunningTest()){
			studyTeamMembList = new QueryWithoutSharingTest().getStudyTeamMemberList();
		}
        for (Study_Team_Member__c studyTeamMember : studyTeamMembList) {
            virtualSiteIdSet.add(studyTeamMember.VTD1_VirtualSite__c);
            studyIdToVirtualSiteIdMap.put(studyTeamMember.Study__r.Id, studyTeamMember.VTD1_VirtualSite__c);
            studyIdToStudyNameMap.put(studyTeamMember.Study__r.Id, studyTeamMember.Study__r.Name);
        }

        Map<String, VTD1_Site_Data__c> virtualSiteIdToSiteDataMap = getElectronicDataCaptureList(virtualSiteIdSet);

        return getElectronicDataCaptureList(studyIdToVirtualSiteIdMap, studyIdToStudyNameMap, virtualSiteIdToSiteDataMap);
    }
    private static String getElectronicDataCaptureList(Map<String, String> studyIdToVirtualSiteIdMap, Map<String, String> studyIdToStudyNameMap, Map<String, VTD1_Site_Data__c> virtualSiteIdToSiteDataMap) {
        List<ElectronicDataCapture> electronicDataCaptureList = new List<ElectronicDataCapture>();
        for (String studyId : studyIdToVirtualSiteIdMap.keySet()) {
            String virtualSiteId = studyIdToVirtualSiteIdMap.get(studyId);
            ElectronicDataCapture edc = new ElectronicDataCapture();
            edc.studyName = studyIdToStudyNameMap.get(studyId);
            edc.siteData = virtualSiteIdToSiteDataMap.get(virtualSiteId);
            electronicDataCaptureList.add(edc);
        }
        return JSON.serialize(electronicDataCaptureList);
    }
    private static Map<String, VTD1_Site_Data__c> getElectronicDataCaptureList(Set<Id> virtualSiteIdSet) {
        Map<String, VTD1_Site_Data__c> virtualSiteIdToSiteDataMap = new Map<String, VTD1_Site_Data__c>();
		List<VTD1_Site_Data__c> siteDateList = getSiteDataList(virtualSiteIdSet);
        for (VTD1_Site_Data__c sd : siteDateList) {
            VTD1_Site_Data__c currentSiteData = virtualSiteIdToSiteDataMap.get(sd.VTD1_Site__c);
            if (currentSiteData == null || sd.LastModifiedDate > currentSiteData.LastModifiedDate) {
                virtualSiteIdToSiteDataMap.put(sd.VTD1_Site__c, sd);
            }
        }
        return virtualSiteIdToSiteDataMap;
    }
    //------------------------------- SOQL ------------------------------------//

	without sharing class QueryWithoutSharingTest{
		private List<Study_Team_Member__c> getStudyTeamMemberList() {
			return [
					SELECT Id
							, VTD1_VirtualSite__c
							, Study__r.Name
							, Study__r.Id
					FROM Study_Team_Member__c
					WHERE User__c = :UserInfo.getUserId()
			];
		}
	}
    private static List<VTD1_Site_Data__c> getSiteDataList(Set<Id> virtualSiteIdSet) {
        return [
                SELECT Id
                        , VTD1_Action_Required_Forms__c
                        , VTD1_Open_Queries__c
                        , VTD1_Site__c
                        , LastModifiedDate
                FROM VTD1_Site_Data__c
                WHERE VTD1_Site__c IN:virtualSiteIdSet
        ];
    }
    private static List<Study_Team_Member__c> getStudyTeamMemberList() {
        return [
                SELECT Id
                        , VTD1_VirtualSite__c
                        , Study__r.Name
                        , Study__r.Id
                FROM Study_Team_Member__c
                WHERE User__c = :UserInfo.getUserId()
        ];
    }

    //----------------------------- WRAPPERS ----------------------------------//

    public class ElectronicDataCapture {
        public String studyName;
        public VTD1_Site_Data__c siteData;
    }
}