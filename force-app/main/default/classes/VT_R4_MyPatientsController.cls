/**
 * Created by user on 20-Dec-19.
 */

public with sharing class VT_R4_MyPatientsController {

    /* was created in dev environment and accidentally deployed as the part of  the optimization story US-0004177
    public class Patient {
        public Case cas;
        public User u;
        public HealthCloudGA__CandidatePatient__c cand;
        public String study;
        public String firstName;
        public String lastName;
        public String status;

        public Patient(Case cas) {
            this.cas = cas;
            this.study = cas.VTD1_Study__r.Name;
            this.firstName = getNonNullString(cas.VTD1_Patient__r.VTD1_First_Name__c);
            this.lastName = getNonNullString(cas.VTD1_Patient__r.VTD1_Last_Name__c);
            this.status = cas.Status;
        }

        public Patient(HealthCloudGA__CandidatePatient__c cand) {
            this.cand = cand;
            this.study = cand.Study__r.Name;
            this.firstName = getNonNullString(cand.rr_firstName__c);
            this.lastName = getNonNullString(cand.rr_lastName__c);
            this.status = cand.Conversion__c;
        }

        private String getNonNullString(String s) {
            return s != null ? s : '';
        }
    }

    @AuraEnabled(Cacheable=true)
    public static String getMyPatients(Id studyId) {
        MyPatientsFactory factory = new MyPatientsFactory();
        factory.addAlgorithm(new PiMyPatientsSelector());
        factory.addAlgorithm(new ScrMyPatientsSelector());
        return factory.getMyPatients(studyId);
        //return null;
    }

    public class MyPatientsFactory {

        public List<MyPatientsSelector> algorithms;

        public MyPatientsFactory() {
            this.algorithms = new List<MyPatientsSelector>();
        }

        public String getMyPatients(String studyId) {
            List<Patient> result = new List<Patient>();
            for (MyPatientsSelector algorithm : this.algorithms) {
                if (algorithm.isRequiredProfile()) {
                    result = algorithm.getMyPatients(studyId);
                    break;
                }
            }
            return JSON.serialize(result);
        }

        public void addAlgorithm(MyPatientsSelector myPatients) {
            this.algorithms.add(myPatients);
        }
    }

    interface MyPatientsSelector {

        Boolean isRequiredProfile();

        List<Patient> getMyPatients(Id studyId);
    }

    public abstract class ProfileMyPatientsSelector implements MyPatientsSelector {

        public List<Patient> getMyPatients(Id studyId) {
            try {
                List<Patient> myPatientList = new List<Patient>();

                List<Case> caseList = this.getPatients(studyId);

                List<String> patientUserIdList = new List<String>();
                for (Case cas : caseList) {
                    patientUserIdList.add(cas.VTD1_Patient_User__c);
                }
                Map<String, User> userIdToUserMap = new Map<String, User>();
                List<User> lstUser = [SELECT Id, FullPhotoUrl FROM User WHERE Id IN :patientUserIdList];
                for (User u : lstUser) {
                    userIdToUserMap.put(u.Id, u);
                }

                for (Case cas : caseList) {
                    Patient myPatient = new Patient(cas);
                    myPatient.u = userIdToUserMap.get(cas.VTD1_Patient_User__c);
                    myPatientList.add(myPatient);
                }

                for (HealthCloudGA__CandidatePatient__c cand : this.getCandidatePatients(studyId)) {
                    myPatientList.add(new Patient(cand));
                }

                return myPatientList;
            } catch (Exception e) {
                throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
            }

        }

        public Boolean isRequiredProfile() {
            return UserInfo.getProfileId() == this.getProfileId();
        }

        protected abstract Id getProfileId();

        protected abstract List<Case> getPatients(String studyId);

        protected virtual List<HealthCloudGA__CandidatePatient__c> getCandidatePatients(String studyId) {
            return new List<HealthCloudGA__CandidatePatient__c>();
        }
    }

    public with sharing class PiMyPatientsSelector extends ProfileMyPatientsSelector {

        protected override Id getProfileId() {
            return [SELECT Id FROM Profile WHERE Name = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME][0].Id;
        }

        protected override List<Case> getPatients(String studyId) {
            List<Id> myStudyIds = VT_D1_PIStudySwitcherRemote.getMyStudyIds(studyId);
            System.debug('myStudyIds: '+myStudyIds);

            return [
                    SELECT Id, CaseNumber,
                            VTD1_Patient_User__c,
                            VTD1_Patient__c,
                            VTD1_Patient__r.VTD1_First_Name__c,
                            VTD1_Patient__r.VTD1_Last_Name__c,
                            VTD1_Subject_ID__c,
                            VTD1_Randomized_Date1__c,
                            VTD1_Study__r.Name,
                            VTD1_Primary_PG__c,
                            VTD1_Primary_PG__r.Name,
                            VTD1_Primary_PG__r.VTD2_Email_Formula__c,
                            VTD1_Primary_PG__r.VTD2_Phone_Formula__c,
                            VTD1_Secondary_PG__r.Name,
                            VTD1_Secondary_PG__r.VTD2_Email_Formula__c,
                            VTD1_Secondary_PG__r.VTD2_Phone_Formula__c,
                            VTD1_Backup_PI_User__r.Name,
                            VTD1_Reconsent_Needed__c,
                            Status
                    FROM Case
                    WHERE RecordTypeId = :VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN
//                    AND ((VTD1_PI_user__c = :UserInfo.getUserId() OR VTD1_Backup_PI_User__c = :UserInfo.getUserId())
//                    OR (VTD1_PI_user__c = null AND VTD1_Backup_PI_User__c = null))
                    AND (VTD1_PI_user__c != null AND VTD1_Backup_PI_User__c != null)
                    AND VTD1_Study__c IN (SELECT Study__c FROM Study_Team_Member__c WHERE User__c = :UserInfo.getUserId())
                    AND VTD1_Patient__c != NULL
                    AND VTD1_Study__c IN :myStudyIds
            ];
        }
    }

    public with sharing class ScrMyPatientsSelector extends ProfileMyPatientsSelector {

        protected override Id getProfileId() {
            return [SELECT Id FROM Profile WHERE Name = :VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME][0].Id;
        }

        protected override List<Case> getPatients(String studyId) {
            List<Id> myStudyIds = VT_D1_PIStudySwitcherRemote.getMyStudyIds(studyId);
            return [
                    SELECT Id, CaseNumber,
                            VTD1_Patient_User__c,
                            VTD1_Patient__c,
                            VTD1_Patient__r.VTD1_First_Name__c,
                            VTD1_Patient__r.VTD1_Last_Name__c,
                            VTD1_Subject_ID__c,
                            VTD1_Randomized_Date1__c,
                            VTD1_Study__r.Name,
                            VTD1_Primary_PG__c,
                            VTD1_Primary_PG__r.Name,
                            VTD1_Primary_PG__r.VTD2_Email_Formula__c,
                            VTD1_Primary_PG__r.VTD2_Phone_Formula__c,
                            VTD1_Secondary_PG__r.Name,
                            VTD1_Secondary_PG__r.VTD2_Email_Formula__c,
                            VTD1_Secondary_PG__r.VTD2_Phone_Formula__c,
                            VTD1_Backup_PI_User__r.Name,
                            VTD1_Reconsent_Needed__c,
                            Status
                    FROM Case
                    WHERE RecordTypeId = :VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN
                    AND VTD1_Patient__c != NULL
                    AND VTD1_Study__c IN :myStudyIds
            ];
        }

        protected override List<HealthCloudGA__CandidatePatient__c> getCandidatePatients(String studyId) {
            List<Id> myStudyIds = VT_D1_PIStudySwitcherRemote.getMyStudyIds(studyId);
            return [
                    SELECT Id, Study__r.Name, rr_firstName__c, rr_lastName__c, Conversion__c
                    FROM HealthCloudGA__CandidatePatient__c
                    WHERE Study__c = :myStudyIds
                    AND Conversion__c != :VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_CONVERTED
                    AND (VTR2_Created_By_User__c = :UserInfo.getUserId() OR CreatedById = :UserInfo.getUserId())
            ];
        }
    }
*/
}