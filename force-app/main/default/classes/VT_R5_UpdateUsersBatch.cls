/**
 * Created by Alexey Mezentsev on 4/21/2020.
 */

public class VT_R5_UpdateUsersBatch implements Database.Batchable<SObject> {
    private List<User> usersToUpdate;

    public VT_R5_UpdateUsersBatch(List<User> usersToUpdate) {
        this.usersToUpdate = usersToUpdate;
    }

    public Iterable<SObject> start(Database.BatchableContext bc) {
        return usersToUpdate;
    }

    public void execute(Database.BatchableContext bc, List<User> usersToUpdate) {
        update usersToUpdate;
    }

    public void finish(Database.BatchableContext bc) {
    }
}