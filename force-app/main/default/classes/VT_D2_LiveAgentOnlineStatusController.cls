/**
* @author: Carl Judge
* @date: 09-Jan-19
* @description: Controller for VT_D2_LiveAgentOnlineStatus page
* @Modified BY: Eashan Parlewar
* @Modified Date: 5/4/2020
* @Comments: Added attributes for Secondary and tertiary chat button ids.
**/

public with sharing class VT_D2_LiveAgentOnlineStatusController {

    public String liveChatButtonId      {get; set;}
    public String SecChatButtonId       {get; set;}
    public String TerChatButtonId       {get; set;}
    public String liveChatDeploymentId  {get; set;}
    public String organizationId        {get; set;}
    public String liveAgentUrl          {get; set;}

    public VT_D2_LiveAgentOnlineStatusController() {
        /*
        ** Get Button Ids based on patient or caregiver's language preference.
        */
        map<string,Id> ButtonIdMap=VT_D1_SkillsBasedRouting.getChatbuttonIdMap();
        system.debug('#### ButtonIdMap '+ButtonIdMap.values());
        if(ButtonIdMap.containskey('primary')){
            this.liveChatButtonId=String.valueOf(ButtonIdMap.get('primary')).mid(0, 15);
        }else{
            this.liveChatButtonId='Null';
        }

        if(ButtonIdMap.containskey('secondary')){
            this.SecChatButtonId=String.valueOf(ButtonIdMap.get('secondary')).mid(0, 15);
        }else{
            this.SecChatButtonId='Null';
        }

        if(ButtonIdMap.containskey('tertiary')){
            this.TerChatButtonId=String.valueOf(ButtonIdMap.get('tertiary')).mid(0, 15);
        }else{
            this.TerChatButtonId='Null';
        }

        system.debug('##### liveChatButtonId '+this.liveChatButtonId);
        system.debug('##### SecChatButtonId '+this.SecChatButtonId);
        system.debug('##### TerChatButtonId '+this.TerChatButtonId);
        this.liveChatDeploymentId = VT_D1_SkillsBasedRouting.getLiveChatDeploymentId();
        this.organizationId = VT_D1_SkillsBasedRouting.getOrganizationId();
        this.liveAgentUrl = getLiveAgentUrl();
    }

    private static String getLiveAgentUrl() {
        String liveAgentUrl;
        for (VTD1_General_Settings__mdt item : [SELECT VTD2_Live_Agent_URL__c FROM VTD1_General_Settings__mdt]) {
            liveAgentUrl = item.VTD2_Live_Agent_URL__c;
        }
        return liveAgentUrl;
    }
}