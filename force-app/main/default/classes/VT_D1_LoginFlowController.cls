public without sharing class VT_D1_LoginFlowController {
    // Who can access this ApexPage
    private static final Set<String> HIPAA_PROFILES = VT_R4_ConstantsHelper_ProfilesSTM.HIPAA_PROFILES;

    // Verification constants
    private static final String USER_AGENT_MOBILE_APP_IDENTIFIER = 'IQVIAMOBILE';
    private static final Integer VERIFICATION_CODE_LENGTH = 6;
    private static final Integer VERIFICATION_CODE_TTL_MINUTES = 15; // 15 minutes
    private static final String VERIFICATION_CODE_SESSION_IDENTIFIER = 'VerificationCode';
    private static final String VERIFICATION_CODE_DATE_SESSION_IDENTIFIER = 'VerificationCodeCreatedDate';




     /**Removing concept of Default and Preffered Language only User Language is used */
    //private static final String DEFAULT_LANGUAGE = 'en_US';
   // public String defaultArticleLanguage { get; private set; }
  //  public String preferredArticleLanguage { get; private set; }



    public Boolean showIRBRelatedError { get; private set; }

    // Articles
    public Boolean showPrivacyNotice { get; private set; }
    public Boolean showHIPAANotice { get; private set; }

    // Internal object state, retrieved from database
    @TestVisible private User currentUser;
    @TestVisible private List<VTR3_Temporary_SObject__c> cookieInfo;
    public Knowledge__kav privacyNotice { get; private set; }
    public Knowledge__kav hipaaNotice { get; private set; }




    // Multi-factor authentication variables
    //private String verificationCode;
    //private Datetime verificationCodeExpired;
    private Boolean verificationSuccessfull = false;

    public Boolean showVerificationRequired { get; private set; }
    public Boolean showVerificationRequiredError { get; private set; }
    public String codeToCompare { get; set; }
    public String CookieMetadata { get;set; }

    // Mobile app variables
    public String deviceUniqueCode { get; private set; }

    // Session Partition Cache
    Cache.SessionPartition sessionPart = Cache.Session.getPartition('local.default');

    public VT_D1_LoginFlowController() {
        if(!Test.isRunningTest()) {
            this.CookieMetadata = VT_R5_CookieDetailsController.getfunctionalcookiesdata();
        }
        this.currentUser = getRequiredUserInfo(UserInfo.getUserId());
        this.cookieInfo = getCookieInfoForContact(this.currentUser.ContactId);
        this.showIRBRelatedError = false;
       // setArticleLanguages();
    }

    public String getStudyPhoneNumber() {
        return this.currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD2_Study_Phone_Number__c;
    }

    public String getPageLanguage () {
        /**Removing concept of Default and Preffered Language only User Language is used -SH- 14527 */
        /*
        if (this.showIRBRelatedError) {
            return DEFAULT_LANGUAGE;
        } else if (this.preferredArticleLanguage != null) {
            return this.preferredArticleLanguage;
        }
        */
        return this.currentUser.LanguageLocaleKey;
    }

    // Adding Cancel button Hide if Patient and Caregiver logges in - SH-14755
    public Boolean gethideCancelButton() {
        return HIPAA_PROFILES.contains(currentUser.Profile.Name) ? true:false;
    }

    @TestVisible private User getRequiredUserInfo(Id userId) {
        // DX TODO: Rewrite onto VT_Stubber later
        return (User) new QueryBuilder(User.class)
            .addFields(new Set<String> {
                'Id',
                'Profile.Name',
                'LanguageLocaleKey',
                'ContactId',
                'Email',
                'User.HIPAA_Accepted__c',
                'First_Log_In__c',
                'VTD1_Privacy_Notice_Last_Accepted__c',
                'VTR3_Verified_Mobile_Devices__c',
                'VTR4_Is_Activated__c',
                'VTR5_Create_Subject_in_eCOA__c',
                'Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c',
                'Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTD1_Study_Phone_Number__c',
                'Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_DefaultIRBLanguage__c',
                'Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c',
                'Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eDiaryTool__c',
                'Contact.VTD1_Clinical_Study_Membership__r.VTD2_Study_Phone_Number__c'
            })
            .addConditions()
            .add(new QueryBuilder.CompareCondition('Id').eq(userId))
            .endConditions()
            .namedQueryRegister('VT_D1_LoginFlowController.currentUser')
            .toSObject();
    }




    @TestVisible private List<VTR3_Temporary_SObject__c> getCookieInfoForContact(Id contactId) {
        // DX TODO: Rewrite onto VT_Stubber later
        return (List<VTR3_Temporary_SObject__c>) new QueryBuilder(VTR3_Temporary_SObject__c.class)
            .addField(VTR3_Temporary_SObject__c.VTR3_SObjectID__c)
            .addField(VTR3_Temporary_SObject__c.VTR4_CookieAcceptedText__c)
            .addConditions()
            .add(new QueryBuilder.CompareCondition('VTR3_SObjectID__c').eq(contactId))
            .add(new QueryBuilder.NullCondition('VTR4_CookieAcceptedText__c').notNull())
            .setConditionOrder('1 AND 2')
            .endConditions()
            .addOrderDesc('CreatedDate')
            .namedQueryRegister('VT_D1_LoginFlowController.cookieInfo')
            .toList();
    }

    /**
     * Set article language based on IRB languages and user language for patient/caregiver
     * Removing the concept of Default and Preffered language , only user language is used - SH-14527
     */
    @TestVisible private void setArticleLanguages() {
        /*
          this.defaultArticleLanguage = DEFAULT_LANGUAGE;


        if (HIPAA_PROFILES.contains(currentUser.Profile.Name)) {
            Virtual_Site__c site = this.currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r;
            String userLocale = this.currentUser.LanguageLocaleKey;
            if (site.VTR5_IRBApprovedLanguages__c != null && site.VTR5_DefaultIRBLanguage__c != null) {
                if (site.VTR5_IRBApprovedLanguages__c.split(';').contains(userLocale)) {
                    this.preferredArticleLanguage = userLocale;
                    this.defaultArticleLanguage = null;
                } else {
                    this.defaultArticleLanguage = site.VTR5_DefaultIRBLanguage__c;
                }
            } else {
                this.preferredArticleLanguage = userLocale;
            }


        }
       */
    }

    /**
    * Get an appropriate article based on type and language
    * @param articleType A sting of article type (Privacy_Notice/HIPPA)
    * @return Knowledge__kav article
    */
    @TestVisible private Knowledge__kav getArticle(String articleType) {
        Knowledge__kav article;
        /** Commenting  */
        /*
        if (this.preferredArticleLanguage != null) {
            article = queryArticle(articleType, this.preferredArticleLanguage);
        }
        if (article == null && this.defaultArticleLanguage != null) {
            article = queryArticle(articleType, this.defaultArticleLanguage);

    }
        if (article == null && HIPAA_PROFILES.contains(this.currentUser.Profile.Name)) {
            this.showIRBRelatedError = true;
        }

        */

        if (this.currentUser.LanguageLocaleKey != null) {
            article = queryArticle(articleType, this.currentUser.LanguageLocaleKey);
        }
        if (article == null && HIPAA_PROFILES.contains(this.currentUser.Profile.Name)) {
            this.showIRBRelatedError = true;
        }

        System.debug('article ' + article + 'this.showIRBRelatedErrorthis.showIRBRelatedError ' + this.showIRBRelatedError );
        return article;

    }

    /**
     * Query single article
     * @param articleType A sting of article type (Privacy_Notice/HIPPA)
     * @param lang A sting of article language
     * @return Knowledge__kav found article or null
     */
    @TestVisible private Knowledge__kav queryArticle(String articleType, String lang) {
        for (Knowledge__kav article : queryArticles(articleType, lang)) {
            if (article.Language == lang) {
                return article;
            }
        }
        return null;
    }

    /**
    * Query articles
    * @param articleType A sting of article type (Privacy_Notice/HIPPA)
    * @param lang A sting of article language
    * @return List<Knowledge__kav> list of articles
    */
    @TestVisible private List<Knowledge__kav> queryArticles(String articleType, String lang) {
        QueryBuilder.Condition condition;
        if (HIPAA_PROFILES.contains(this.currentUser.Profile.Name)) {
            condition = new QueryBuilder.CompareCondition(Knowledge__kav.VTD2_Study__c).eq(this.currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c);
        } else if (this.currentUser.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME) {
            condition = new QueryBuilder.CompareCondition(Knowledge__kav.Primary_For_SCR__c).eq(true);
        } else if (this.currentUser.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) {
            condition = new QueryBuilder.CompareCondition(Knowledge__kav.Primary_For_PI__c).eq(true);
        }
  


        return (List<Knowledge__kav>) new QueryBuilder(Knowledge__kav.class)
            .addFields('VTD1_Content__c, Language, Title')
            .addConditions()
            .add(new QueryBuilder.CompareCondition(Knowledge__kav.IsLatestVersion).eq(true))
            .add(new QueryBuilder.CompareCondition(Knowledge__kav.PublishStatus).eq('Online'))
            .add(new QueryBuilder.CompareCondition(Knowledge__kav.Language).eq(lang))
            .add(new QueryBuilder.SimpleCondition('VTD2_Type__c INCLUDES(\'' + articleType + '\')'))
            .add(condition)
            .setConditionOrder('1 AND 2 AND 3 AND 4 AND 5')
            .endConditions()
            .addOrderDesc(Knowledge__kav.LastModifiedDate)
            .namedQueryRegister('VT_D1_LoginFlowController.queryArticles')
            .toList();


    }

    public PageReference accept() {
        if (this.showVerificationRequired) {
            if (checkVerificationCodeExpired()) {
                this.showVerificationRequiredError = true;
                this.codeToCompare = '';
                sendVerificationCode();
                return null;
            } else {
                if (getCachedVerificationCode() == this.codeToCompare) {
                    this.verificationSuccessfull = true;
                    if (this.deviceUniqueCode != null && this.currentUser.VTR3_Verified_Mobile_Devices__c.indexOf(this.deviceUniqueCode) == -1) {
                        this.currentUser.VTR3_Verified_Mobile_Devices__c += this.deviceUniqueCode + ';';
                    }
                    update this.currentUser;
                } else {
                    this.showVerificationRequiredError = true;
                    this.codeToCompare = '';
                    return null;
                }
            }
        } else if (this.showPrivacyNotice) {
            this.currentUser.VTD1_Privacy_Notice_Last_Accepted__c = System.now();
        } else if (this.showHIPAANotice) {
            this.currentUser.HIPAA_Accepted__c = true;
            this.currentUser.HIPAA_Accepted_Date_Time__c = System.now();
        }

        if (this.verificationSuccessfull &&
                (this.currentUser.First_Log_In__c != null || (this.currentUser.First_Log_In__c == null &&
                        ((HIPAA_PROFILES.contains(this.currentUser.Profile.Name) && this.currentUser.HIPAA_Accepted__c) ||
                                (!HIPAA_PROFILES.contains(this.currentUser.Profile.Name) && this.currentUser.VTD1_Privacy_Notice_Last_Accepted__c != null))))) {

            if (this.deviceUniqueCode != null && this.currentUser.VTR3_Verified_Mobile_Devices__c.indexOf(this.deviceUniqueCode) == -1) {
                this.currentUser.VTR3_Verified_Mobile_Devices__c += this.deviceUniqueCode + ';';
            }
        }

        PageReference nextPage = findWhatsNext();

        if (allAccepted() && HIPAA_PROFILES.contains(this.currentUser.Profile.Name) && !this.currentUser.VTR4_Is_Activated__c) {
            this.currentUser.VTR4_Is_Activated__c = true;
        }
        update this.currentUser;

        return nextPage;
    }

    public PageReference findWhatsNext() {
        try {
            String userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
            if (userAgent != null && userAgent.indexOf(USER_AGENT_MOBILE_APP_IDENTIFIER) != -1) {
                return Auth.SessionManagement.finishLoginFlow();
            }
        } catch (Exception e) {
            System.debug(e.getStackTraceString() + ' ' + e.getMessage());
        }

        this.showVerificationRequired = false;
        this.showPrivacyNotice = false;
        this.showHIPAANotice = false;
        setCookie();

        if (needVerificationRequired()) {
            this.showVerificationRequired = true;
            if (checkVerificationCodeExpired()) {
                sendVerificationCode();
            }

        } else if (needPrivacyNotice()) {
            this.showPrivacyNotice = true;
            this.privacyNotice = getArticle('Privacy_Notice');

        } else if (needHIPAANotice()) {
            this.showHIPAANotice = true;
            this.hipaaNotice = getArticle('HIPAA');
        }

        if (allAccepted()) {
            return Auth.SessionManagement.finishLoginFlow();
        }
        return null;
    }

    public PageReference cancel() {
        return new PageReference(Site.getBaseUrl() + '/secur/logout.jsp');
    }

    /**
     * @description Check if multi-factor authentication is needed.
     * @return true if needed
     */
    private Boolean needVerificationRequired() {

        if (this.currentUser.First_Log_In__c == null) {
            return false;
        }

        if (this.currentUser.VTR3_Verified_Mobile_Devices__c == null) {
            this.currentUser.VTR3_Verified_Mobile_Devices__c = '';
        }
        if (!this.verificationSuccessfull && this.currentUser.VTR3_Verified_Mobile_Devices__c.indexOf('AUTOTEST') == -1) {
            String userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
            if (userAgent != null && userAgent.indexOf(USER_AGENT_MOBILE_APP_IDENTIFIER) != -1) {
                String regExp = '^.*(?=' + USER_AGENT_MOBILE_APP_IDENTIFIER + ')' + USER_AGENT_MOBILE_APP_IDENTIFIER + '\\s';
                this.deviceUniqueCode = userAgent.replaceAll(regExp, '');
            }

            if ((this.deviceUniqueCode != null && this.currentUser.VTR3_Verified_Mobile_Devices__c.indexOf(this.deviceUniqueCode) == -1)) {
                return true;
            }
        }
        return false;

    }

    /**
     * @description Check if code exists and valid
     *
     * @return Boolean
     */
    public Boolean checkVerificationCodeExpired() {
        //this.sessionPart.get(VERIFICATION_CODE_SESSION_IDENTIFIER);
        Datetime codeCreated = (Datetime) this.sessionPart.get(VERIFICATION_CODE_DATE_SESSION_IDENTIFIER);
        if (this.sessionPart.get(VERIFICATION_CODE_SESSION_IDENTIFIER) == null) return true;
        if (codeCreated == null || codeCreated.addMinutes(VERIFICATION_CODE_TTL_MINUTES) < System.now()) {
            return true;
        } else return false;
    }

    /**
     * @description Send verification code via email and save it to session cache.
     */
    public void sendVerificationCode() {

        String verificationCode = '';

        for (Integer i = 0; i < VERIFICATION_CODE_LENGTH; i++) {
            verificationCode += Integer.valueof(Math.random() * 10);
        }

        //Datetime verificationCodeExpired = System.now().addMinutes(15);

        List<OrgWideEmailAddress> owEmails = [select Id from OrgWideEmailAddress order by CreatedDate limit 1];

        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.optOutPolicy = 'FILTER';
        if (owEmails.size() > 0) {
            message.setOrgWideEmailAddressId(owEmails[0].Id);
        }
        message.setSaveAsActivity(false);
        message.setTargetObjectId(this.currentUser.Id);
        message.setSubject(System.Label.VTR3_VerificationRequired);
        message.plainTextBody = System.Label.VTR3_VerificationCode + ' ' + verificationCode;
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>{
                message
        };
        Messaging.sendEmail(messages);
        addToSessionCache(VERIFICATION_CODE_SESSION_IDENTIFIER, verificationCode, VERIFICATION_CODE_TTL_MINUTES);
        addToSessionCache(VERIFICATION_CODE_DATE_SESSION_IDENTIFIER, System.now(), VERIFICATION_CODE_TTL_MINUTES);

    }

    private Boolean needPrivacyNotice() {
        return this.currentUser.VTD1_Privacy_Notice_Last_Accepted__c == null;
    }

    private Boolean needHIPAANotice() {
        return HIPAA_PROFILES.contains(this.currentUser.Profile.Name)
                && !this.currentUser.HIPAA_Accepted__c;
    }

    private Boolean allAccepted() {
        return !this.showVerificationRequired
                && !this.showPrivacyNotice
                && !this.showHIPAANotice;
    }

    private void addToSessionCache(String key, Object value, Integer expiresInMinutes) {
        this.sessionPart.put(key, value, expiresInMinutes * 60);
    }

    private String getCachedVerificationCode() {
        return (String) this.sessionPart.get(VERIFICATION_CODE_SESSION_IDENTIFIER);
    }

    private void setCookie() {
        String userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
        if ((userAgent == null || (userAgent != null && userAgent.indexOf(USER_AGENT_MOBILE_APP_IDENTIFIER) == -1)) && !this.cookieInfo.isEmpty()) {
            setCookieFuture(this.currentUser.ContactId, this.cookieInfo[0].VTR4_CookieAcceptedText__c, JSON.serialize(this.cookieInfo));
            this.cookieInfo.clear();
        }
    }

    @Future
    private static void setCookieFuture(Id ContactId, String cookiesDetails, String cookieInfo) {
        update new Contact(Id = ContactId, VTR3_CookiesAcceptedDetails__c = cookiesDetails);
        Database.delete((List<VTR3_Temporary_SObject__c>) JSON.deserialize(cookieInfo, List<VTR3_Temporary_SObject__c>.class), false);
    }
}