/**
* @author: N.Arbatskiy
* @date: 05-May-20
* @description: Class with methods templates for processing eCOA callouts.
**/
public abstract class VT_R5_eCoaAbstractAction extends VT_D1_AbstractAction {
    protected Boolean skipLogging = false;
    private Boolean sendFailMail = false;
    private Boolean holdEmail = false; // generate email if required, but dont send it
    private VTD1_Integration_Log__c log;
    private Messaging.SingleEmailMessage email;

    public VTD1_Integration_Log__c getLog() {
        return log;
    }

    public Messaging.SingleEmailMessage getEmail() {
        return email;
    }

    public VT_R5_eCoaAbstractAction setHoldEmail(Boolean holdEmail) {
        this.holdEmail = holdEmail;
        return this;
    }

    /**
     * @description: This method should be overridden in a class which extends
     * VT_R5_eCoaAbstractAction in order to process an actual callout.
     * @return VT_D1_HTTPConnectionHandler.Result in order to be passed
     * as a parameter to @method processResponse.
     *
     */
    protected abstract VT_D1_HTTPConnectionHandler.Result doCallout();
    /**
     * @description: Set the skipLogging class variable. Note that this alone will not skip logging, the subclass must also pass this variable to the VT_R5_eCoaApiRequest
     * @param skipLogging skip logging if true
     * @return the instance of the class
     */
    public VT_R5_eCoaAbstractAction setSkipLogging(Boolean skipLogging) {
        this.skipLogging = skipLogging;
        return this;
    }
    /**
     * @description: Processes the response.
     * @param result is used for setting statuses and messages.
     */
    protected virtual void processResponse(VT_D1_HTTPConnectionHandler.Result result) {
        if (result != null) {
            log = result.log;
            HttpResponse response = result.httpResponse;
            if (response == null) {
                setStatus(STATUS_FAILED);
                setMessage(result.log.VTD1_ErrorMessage__c);
                if (getMessage() != null && getMessage().startsWith('Callout loop not allowed')) {
                    addToQueue();
                    if (isLastAttempt()) {
                        handleFailure(result);
                        this.sendFailMail = true;
                    }
                } else {
                    handleFailure(result);
                    this.sendFailMail = true;
                }
            } else if (response.getStatusCode() >= 500 && response.getStatusCode() < 600) {
                setStatus(STATUS_FAILED);
                setMessage('Server return status code: ' + response.getStatusCode() + ' ' + response.getStatus() + '; Body: ' + response.getBody());
                addToQueue();
                if (isLastAttempt()) {
                    handleFailure(result);
                    this.sendFailMail = true;
                }
            } else if (response.getStatusCode() >= 200 && response.getStatusCode() <= 202) {
                setStatus(STATUS_SUCCESS);
                //executes on success if any logic is implemented in a callout class
                handleSuccess(result);
            } else {
                setStatus(STATUS_FAILED);
                setMessage('Server return: ' + response.getStatusCode() + ' ' + response.getStatus() + ', Body: ' + response.getBody());
                handleFailure(result);
                this.sendFailMail = true;
            }
            handleFinal(result);
        }
    }
    /**
    * @description: Processes the successful/failure response.
    * @param result is used for any further processing.
    * Could be overridden in order to implement any desired logic in a callout class on success/failure.
    */
    protected virtual void handleSuccess(VT_D1_HTTPConnectionHandler.Result result) {}
    protected virtual void handleFailure(VT_D1_HTTPConnectionHandler.Result result) {}
    protected virtual void handleFinal(VT_D1_HTTPConnectionHandler.Result result) {
        if (!this.skipLogging && result.log.Id == null) {
            insert result.log;
        }

        if (this.sendFailMail) {
            sendFailEmail(result);
        }
    }

    /**
    * @description: Sends an email eCOA Admin on failure.
    */
    protected void sendFailEmail(VT_D1_HTTPConnectionHandler.Result result) {
        String ecoaAdminEmail = Test.isRunningTest() ? 'test@maillinator.com' :
            eCOA_IntegrationDetails__c.getInstance().API_Admin_Email__c;
        if (!String.isEmpty(ecoaAdminEmail)) {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = ecoaAdminEmail.split('[,;]');
            message.subject = 'eCOA Callout Failed - ' + result.log.VTD1_Action__c + ' - ' + System.Url.getSalesforceBaseUrl().getHost().substringBefore('.');
            message.plainTextBody = 'Error: ' + getMessage() + '\nLog ID: ' + result.log.Id;

            if (!holdEmail) {
                Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>{
                    message
                };
                Messaging.sendEmail(messages);
            } else {
                email = message;
            }
        }
    }
    /**
    * In this way @method processResponse is called automatically in any class
    * which extends VT_R5_eCoaAbstractAction.
    */
    public override void execute() {
        processResponse(doCallout());
    }
}