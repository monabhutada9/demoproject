@IsTest
private class VT_D2_SomeFilesListByRecordTest {

    public class MockSomeFileListHttpResponseGenerator implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            /*
            ContentDocument contentDocument;
            List<ContentDocument> contentDocumentList = [SELECT Id FROM ContentDocument];
            if (contentDocumentList.size() > 0) {
                contentDocument = contentDocumentList.get(0);
            }
*/
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            if (req.getMethod() == 'GET') {
                res.setBody('{"records":[{"ContentDocumentId":"031g000002AFMk4AAG"}]}');
                res.setStatusCode(200);
            }

            if (req.getMethod() == 'DELETE') {
                /*
                delete contentDocument;
                res.setStatusCode(204);
                */
            }

            return res;
        }
    }

    @testSetup
    static void setup() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.startTest();
        //HealthCloudGA__CandidatePatient__c candidatePatient = VT_D1_TestUtils.createPatientCandidate('Mike', 'Birn', 'mikebirn@test.com', 'mikebirn@test.com', 'mikebirn', 'Converted', study.Id, 'PN', '11-11-11', 'US', 'NY');
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }

    @IsTest
    static void getDocsTest() {
        Test.startTest();
        List<Case> casesList = [SELECT Id, VTD1_Study__c FROM Case];
        System.assert(casesList.size() > 0);
        if (casesList.size() > 0) {
            Case cas = casesList.get(0);

            VTD1_Document__c doc = new VTD1_Document__c();
            doc.VTD1_Study__c = cas.VTD1_Study__c;
            doc.VTD1_Clinical_Study_Membership__c = cas.Id;
            doc.VTD1_Uploading_Not_Finished__c = true;
            insert doc;

            ContentVersion contentVersion = new ContentVersion();
            contentVersion.PathOnClient = 'test.txt';
            contentVersion.Title = 'Test file';
            contentVersion.VersionData = Blob.valueOf('Test Data');
            insert contentVersion;

            ContentVersion contentVersion1 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];

            ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
            contentDocumentLink.LinkedEntityId = doc.Id;
            contentDocumentLink.ContentDocumentId = contentVersion1.ContentDocumentId;
            contentDocumentLink.ShareType = 'I';
            contentDocumentLink.Visibility = 'AllUsers';
            insert contentDocumentLink;

            List<VTD1_Document__c> documentList = VT_D1_SomeFilesListByRecord.getDocs(cas.Id);
            System.assertEquals(1, documentList.size());

            VT_D1_SomeFilesListByRecord.finishSession(documentList);

            List<VTD1_Document__c> documentList1 = [SELECT VTD1_Uploading_Not_Finished__c, VTD1_Files_have_not_been_edited__c FROM VTD1_Document__c];
            System.assertEquals(1, documentList1.size());

            if(documentList1.size() > 0) {
                VTD1_Document__c doc1 = documentList1.get(0);
                System.assertEquals(false, doc1.VTD1_Uploading_Not_Finished__c);
                System.assertEquals(true, doc1.VTD1_Files_have_not_been_edited__c);
            }

            Case cas1 = VT_D1_SomeFilesListByRecord.getCase(cas.Id);
            System.assertNotEquals(null, cas1);

            List<ContentDocument> contentDocumentList = [SELECT Id FROM ContentDocument];
            System.assertEquals(1, contentDocumentList.size());
            Test.stopTest();

        }
    }

    @IsTest
    static void doDeleteTest() {

        List<Case> casesList = [SELECT Id, VTD1_Study__c FROM Case];
        Case cas;
        System.assert(casesList.size() > 0);
        if (casesList.size() > 0) {
            cas = casesList.get(0);

            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockSomeFileListHttpResponseGenerator());
            List<VTD1_Document__c> documentList2 = VT_D1_SomeFilesListByRecord.doDelete(NULL, cas.Id);
            Test.stopTest();
        }

       // System.assertEquals(0, documentList2.size());
        List<ContentDocument> contentDocumentList1 = [SELECT Id FROM ContentDocument];
        System.assertEquals(0, contentDocumentList1.size());
    }
}