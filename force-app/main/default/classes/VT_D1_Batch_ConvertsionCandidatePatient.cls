public class VT_D1_Batch_ConvertsionCandidatePatient {
    // new batch - VT_R5_ConversionBatch.cls
    /*
    //private set<String> candidatePatientIdSet;
    private Set<Id> candidatePatientIdSet;

    public VT_D1_Batch_ConvertsionCandidatePatient(Set<Id> strCandidatePatientIdSet) {
        candidatePatientIdSet = strCandidatePatientIdSet;
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
                SELECT Id, rr_Email__c, Study__c, VTR4_CaseForTransfer__c
                FROM HealthCloudGA__CandidatePatient__c
                WHERE Id IN:candidatePatientIdSet
        ]);
    }
    
    public void execute(Database.BatchableContext bc, List<HealthCloudGA__CandidatePatient__c> cPatientsList) {
        VT_R4_PatientConversionHelper.findDuplicates(cPatientsList);
        Set<Id> candidatePatientIdsToBeConverted = new Set<Id>();
        for (HealthCloudGA__CandidatePatient__c cp : cPatientsList) candidatePatientIdsToBeConverted.add(cp.Id);
        VT_R3_GlobalSharing.doSynchronous = true;
        VT_R4_PatientConversion.convert(candidatePatientIdsToBeConverted);

        //VT_D1_CandidatePatientProcessHandler.conversionCPSync(candidatePatientIdsToBeConverted);
    }
    
    public void finish(Database.BatchableContext bc) {
        Map<Id, Case> casesMap = new Map<Id, Case>([
                SELECT Id
                FROM Case
                WHERE Account.Candidate_Patient__c IN: candidatePatientIdSet
                    AND RecordType.DeveloperName =: VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN
        ]);
        Database.executeBatch(new VT_D2_Batch_SendSubjectStatus(casesMap.keySet()), 1);

        List<VTR6_PatientFieldHistoryTemporary__c> historyTempToInsert = VT_R2_PatientHistoryHandler.historyTempRecords;
        if (!historyTempToInsert.isEmpty()) {
            insert historyTempToInsert;
        }
    }*/
}