/**
 * @author Leonid Bartenev, Ruslan Mullayanov
 */

public without sharing class VT_D1_PIStudySwitcherRemote {
    
    public class StudyItem{
        public String id;
        public String name;
        public String role;
        public String status;
        public StudyItem(String id, String name, String role, String status){
            this.id = id;
            this.name = name;
            this.role = role;
            this.status = status;
        }
    }
    public class StudyTeamMember {
        public String studyId;
        public String studyName;
        public String studyStatus;
        public Boolean isBackupPI;
        public StudyTeamMember(String studyId, String studyName, String studyStatus, Boolean isBackupPI){
            this.studyId = studyId;
            this.studyName = studyName;
            this.studyStatus = studyStatus;
            this.isBackupPI = isBackupPI;
        }
    }
    
    @AuraEnabled(Cacheable=true)
    public static String getStudies(Boolean withoutAllOption){
        try{
            Set<StudyItem> studyItems = new Set<StudyItem>();
            studyItems.add(new StudyItem(null, (withoutAllOption? Label.VTD2_Select : Label.VTD2_AllMyStudies), null, null));
            String currentUserProfile = VT_D1_HelperClass.getUserProfileName(UserInfo.getUserId());
            for (StudyTeamMember stm : getSTMList()) {
                String role;
                if (currentUserProfile == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) {
                    role = stm.isBackupPI? Label.VTD2_SubInvestigator : Label.VTD2_PrimaryInvestigator;
                }
                studyItems.add(new StudyItem(stm.studyId, stm.studyName, role, stm.studyStatus));
            }
            return JSON.serialize(studyItems);
        } catch (Exception e){
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    private static List<StudyTeamMember> getSTMList(){
        return getSTMList(UserInfo.getUserId());
    }

    private static List<StudyTeamMember> getSTMList(Id userId){
        Map<Id, StudyTeamMember> idToStmMap = new Map<Id, StudyTeamMember>();
        try{
            if (VT_D1_HelperClass.getUserProfileName(userId) == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) {
                List<Study_Site_Team_Member__c> sstmList = [
                    SELECT VTR2_Associated_PI3__c, VTR2_Associated_PI3__r.Study__c, VTR2_Associated_PI3__r.Study__r.Name, VTR2_Associated_SubI__c, VTR2_Associated_PI3__r.Study__r.VTD1_StudyStatus__c
                    FROM Study_Site_Team_Member__c
                    WHERE VTR2_Associated_SubI__c = :userId
                ];
                if (sstmList.size() > 0) {
                    for (Study_Site_Team_Member__c sstm : sstmList) {
                        if (!idToStmMap.containsKey(sstm.VTR2_Associated_PI3__r.Study__c)) {
                            idToStmMap.put(sstm.VTR2_Associated_PI3__r.Study__c, new StudyTeamMember(
                                    sstm.VTR2_Associated_PI3__r.Study__c,
                                    sstm.VTR2_Associated_PI3__r.Study__r.Name,
                                    sstm.VTR2_Associated_PI3__r.Study__r.VTD1_StudyStatus__c,
                                    true
                            ));
                        }
                    }
                }
            }
            List<AggregateResult> arList = [
                 SELECT Study__c studyId, Study__r.Name studyName, Study__r.VTD1_StudyStatus__c studyStatus, VTD1_Backup_PI__c isBackupPI
                 FROM Study_Team_Member__c
                 WHERE User__c =: userId
                 AND (VTD1_Type__c =: VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME OR VTD1_Type__c =: VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME)
                 AND Study__c NOT IN :idToStmMap.keySet()
                GROUP BY Study__c, Study__r.Name, Study__r.VTD1_StudyStatus__c, VTD1_Backup_PI__c
                ORDER BY Study__r.Name ASC, VTD1_Backup_PI__c DESC
            ];
            if (!arList.isEmpty()) {
                for (AggregateResult ar : arList) {
                    if (!idToStmMap.containsKey(String.valueOf(ar.get('studyId')))) {
                        idToStmMap.put(String.valueOf(ar.get('studyId')), new StudyTeamMember(
                                String.valueOf(ar.get('studyId')),
                                String.valueOf(ar.get('studyName')),
                                String.valueOf(ar.get('studyStatus')),
                                Boolean.valueOf(ar.get('isBackupPI'))
                        ));
                    }
                }
            }
        } catch (Exception e){
            System.debug('ERROR: ' + e.getMessage() + '\n' + e.getStackTraceString());
        }
        return idToStmMap.values();
    }

    public static List<Id> getUserStudyIds(Id userId){
        List<Id> studyIds = new List<Id>();
        try{
            for (StudyTeamMember stm : getSTMList(userId)) {
                studyIds.add(stm.studyId);
            }
            System.debug('Study Ids: '+studyIds);
        } catch (Exception e){
            System.debug('ERROR: ' + e.getMessage() + '\n' + e.getStackTraceString());
        }
        return studyIds;
    }

    public static List<Id> getMyStudyIds(){
        return getUserStudyIds(UserInfo.getUserId());
    }

    public static List<Id> getMyStudyIds(Id studyId){
	    System.debug('studyId:'+studyId);
	    List<Id> myStudyIds = new List<Id>();
	    try{
	        myStudyIds = getMyStudyIds();
	    	if (!String.isBlank(studyId) && myStudyIds.contains(studyId)) {
	        	return new List<Id>{studyId};
	        }
    	} catch (Exception e){
            System.debug('ERROR: ' + e.getMessage() + '\n' + e.getStackTraceString());
        }
        return myStudyIds;
    }
}