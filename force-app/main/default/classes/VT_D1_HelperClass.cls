global without sharing class VT_D1_HelperClass {
    global static Map<String, Map<String,Id>> rtMap;
    global static Map<String, Map<String,Id>> rtIdsMap = new Map<String, Map<String,Id>>();
    global static Map<Id, String> profileIdToNameMap = new Map<Id, String>();
    global static Map<String, Id> profileNameToIdMap = new Map<String, Id>();
    global static Map<String, Map<String,Id>> getRTMap(){
        if(rtMap != null) return rtMap;
        rtMap = new Map<String, Map<String,Id>>();
        for(RecordType rt : [SELECT DeveloperName,Id,SobjectType FROM RecordType]){
            if(!rtMap.containsKey(rt.SobjectType)) rtMap.put(rt.SobjectType, new Map<String, Id>());
            rtMap.get(rt.SobjectType).put(rt.DeveloperName, rt.Id);
        }
        return rtMap;
    }

    public class RecordTypeCache implements Cache.CacheBuilder {
        public Object doLoad(String key) {
            Map<String,RecordTypeInfo> mapRtInfo =
                    ((SObject)(Type.forName('Schema.' + key.split(':').get(1)).newInstance())).getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName();
            Map<String, Id> mapRtToId = new Map<String, Id>();
            for(String developerName : mapRtInfo.keySet()) {
                mapRtToId.put(developerName, mapRtInfo.get(developerName).getRecordTypeId());
            }
            return mapRtToId;
        }
    }
    global static Id getRTId(String SobjectType, String recordTypeDeveloperName) {
        if (String.isEmpty(SobjectType) || String.isEmpty(recordTypeDeveloperName)) {
            return null;
        }
        if (!rtIdsMap.containsKey(SobjectType)) {
            String key = String.join(new String[] { 'RecordTypeInfos', SobjectType }, ':');
            rtIdsMap.put(SobjectType, (Map<String, Id>) (!Test.isRunningTest()? Cache.Org.get(RecordTypeCache.class, key) : new RecordTypeCache().doLoad(key)));
        }
        return rtIdsMap.get(SobjectType).get(recordTypeDeveloperName);
    }

    public class ProfileCacheException extends Exception {
    }

    public class ProfileCache implements Cache.CacheBuilder {
        public Object doLoad(String key) {
            Map<Id, String> idToNameMap = new Map<Id, String>();
            Map<String, Id> nameToIdMap = new Map<String, Id>();
            for (Profile p : [SELECT Id, Name FROM Profile]) {
                idToNameMap.put(p.Id, p.Name);
                nameToIdMap.put(p.Name, p.Id);
            }
            if (key.equals('profileId')) {
                return idToNameMap;
            } else if (key.equals('profileName')) {
                return nameToIdMap;
            } else {
                throw new ProfileCacheException('Unknown key provided for ProfileCache');
            }
        }
    }

    global static Id getProfileId(String profileName) {
        String key = 'profileName';
        if (profileNameToIdMap.isEmpty()) {
            profileNameToIdMap = (Map<String, Id>) (!Test.isRunningTest()? Cache.Org.get(ProfileCache.class, key) : new ProfileCache().doLoad(key));
        }
        return profileNameToIdMap.get(profileName);
    }

    global static String getProfileName(Id profileId) {
        String key = 'profileId';
        if (profileIdToNameMap.isEmpty()) {
            profileIdToNameMap = (Map<Id, String>) (!Test.isRunningTest()? Cache.Org.get(ProfileCache.class, key) : new ProfileCache().doLoad(key));
        }
        return profileIdToNameMap.get(profileId);
    }

    global static List<RecordType> RecordTypePCF;
    global static List<RecordType> RecordTypeContactFormList;
    global static OrgWideEmailAddress orgWideEmailAddress;
    global static Map<String, EmailTemplate> emailTemplateMap = new Map<String, EmailTemplate>();

    global static List<RecordType> getRecordTypePCF(){

        if(RecordTypePCF != null) return RecordTypePCF;
        String DevNameRT = '%'+VT_R4_ConstantsHelper_AccountContactCase.RT_PCF+'%';// %VTD1_PCF%
        RecordTypePCF = new List<RecordType>();
        RecordTypePCF =  [ SELECT DeveloperName,Id,IsActive,Name,SobjectType
        FROM RecordType
        WHERE (SobjectType = :VT_R4_ConstantsHelper_AccountContactCase.SOBJECT_CASE )
        AND ((DeveloperName LIKE :DevNameRT) )];
        return RecordTypePCF;
    }

    global static List<RecordType> getRecordTypeContactForm(){

        if(RecordTypeContactFormList != null) return RecordTypeContactFormList;
        RecordTypeContactFormList =  [ SELECT DeveloperName,Id,IsActive,Name,SobjectType
        FROM RecordType
        WHERE (SobjectType = :VT_R4_ConstantsHelper_AccountContactCase.SOBJECT_CASE )
        AND DeveloperName in (:VT_R4_ConstantsHelper_AccountContactCase.RT_PCF, :VT_R4_ConstantsHelper_AccountContactCase.RT_GCF)];
        return RecordTypeContactFormList;
    }

    global static Set<Id> RecordTypePCFSetId;
    global static Set<Id> getRecordTypePCFSetId(){
        if(RecordTypePCFSetId != null) return RecordTypePCFSetId;

        RecordTypePCFSetId = new Set<Id>();
        List<RecordType> RecordTypePCFList = new List<RecordType>();

        RecordTypePCFList = VT_D1_HelperClass.getRecordTypePCF();

        RecordTypePCFSetId = (new Map<Id,RecordType>(RecordTypePCFList)).keySet();

        return RecordTypePCFSetId;
    }

    global static List<RecordType> RecordTypeCarePlan;
    global static List<RecordType> getRecordTypeCarePlan(){

        if(RecordTypeCarePlan != null) return RecordTypeCarePlan;
        String DevNameRT = VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN;
        RecordTypeCarePlan = new List<RecordType>();
        RecordTypeCarePlan =  [ SELECT DeveloperName,Id,IsActive,Name,SobjectType
        FROM RecordType
        WHERE (SobjectType = :VT_R4_ConstantsHelper_AccountContactCase.SOBJECT_CASE )
        AND ((DeveloperName LIKE :DevNameRT) )];
        return RecordTypeCarePlan;
    }

    global static Set<Id> RecordTypeCarePlanSetId;
    global static Set<Id> getRecordTypeCarePlanSetId(){
        if(RecordTypeCarePlanSetId != null) return RecordTypeCarePlanSetId;

        RecordTypeCarePlanSetId = new Set<Id>();
        List<RecordType> RecordTypeCarePlanList = new List<RecordType>();

        RecordTypeCarePlanList = VT_D1_HelperClass.getRecordTypeCarePlan();

        RecordTypeCarePlanSetId = (new Map<Id,RecordType>(RecordTypeCarePlanList)).keySet();

        return RecordTypeCarePlanSetId;
    }

//    public static void sendEmailNotifyToAdmin (List <sObject> objectList, EmailTemplate template) {
//        if (objectList.isEmpty()) return;
//        List <Messaging.SingleEmailMessage> emails = new List  <Messaging.SingleEmailMessage> ();
//
//        List<User> adminList = [select Id, Email from User where Profile.Name = 'System Administrator'];
//
//        if(adminList.isEmpty()) return;
//        List<List<String>> arrayList = new List<List<String>>();
//        List<List<String>> listOfEmailPacksStrings = new List<List<String>>();
//        List<String> adminEmailList = new List<String>();
//        for(User admin:adminList){
//            if (adminEmailList.size() == 100) {
//                listOfEmailPacksStrings.add(adminEmailList);
//                adminEmailList = new List<String>();
//            }
//            if(!String.isBlank(admin.Email))
//                adminEmailList.add(admin.Email);
//        }
//
//        if (!adminEmailList.isEmpty()) {
//            listOfEmailPacksStrings.add(adminEmailList);
//        }
//        if(listOfEmailPacksStrings.isEmpty()) return;
//
//        for (sObject objItem :  objectList) {
//            for (List<String>  adminEmails : listOfEmailPacksStrings) {
//                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage ();
//                email.saveAsActivity = false;
//                email.setToAddresses(adminEmails);
//                email.setPlainTextBody(template.Body.replace('{!Case.Id}', String.valueOf(objItem.get('Id'))));
//                emails.add(email);
//            }
//        }
//        List<Messaging.SendEmailResult> results = Messaging.sendEmail(emails);
//    }

    /* because we cant do:
        String name = record.get('parentId__r.Name');
        Instead:
        getCrossObjectField(record, 'parentId__r.Name');
     */
    global static Object getCrossObjectField(SObject rec, String fieldPath) {
        return getValueFromSplitFieldPath(rec, fieldPath.split('\\.'));
    }

    global static Object getValueFromSplitFieldPath(SObject currentSObject, List<String> splitFieldPath) {
        if (currentSObject == null) {
            return null;
        } else if (splitFieldPath.size() == 1) {
            return currentSObject.get(splitFieldPath[0]);
        } else {
            String nextSObjectName = splitFieldPath.remove(0);
            SObject nextSObject = currentSObject.getSObject(nextSObjectName);
            return getValueFromSplitFieldPath(nextSObject, splitFieldPath);
        }
    }

//    global static Map<String, Id> profileIdMap;
    global static Id getProfileIdByName(String name) {
//        if (profileIdMap == null) {
//            profileIdMap = new Map<String, Id>();
//            List<Profile> profileList = [SELECT Id, Name FROM Profile];
//            for (Profile item : profileList) {
//                profileIdMap.put(item.Name, item.Id);
//            }
//        }
//        return profileIdMap.get(name);
        return getProfileId(name);
    }

    global static String getUserProfileName(Id userId) {
        if (UserInfo.getUserId()==userId) {
            return getProfileName(UserInfo.getProfileId());
        } else {
            User user = [SELECT Profile.Name FROM User WHERE Id = :userId LIMIT 1];
            return user.Profile.Name;
        }
    }

    private static String sessionIdForGuestTestValue; // if needed, can set this in a test class before running something with another user
    global static String getSessionIdForGuest() {
        VTD1_FileUploadUserCredentials__c creds = VTD1_FileUploadUserCredentials__c.getInstance();

        Auth.JWT jwt = new Auth.JWT();
        jwt.setSub(creds.username__c);
        jwt.setAud(creds.login_url__c);
        jwt.setIss(creds.client_id__c);

        if (! Test.isRunningTest()) {
            //Create the object that signs the JWT bearer token
            Auth.JWS jws = new Auth.JWS(jwt, creds.certificate_name__c);

            //POST the JWT bearer token
            Auth.JWTBearerTokenExchange bearer = new Auth.JWTBearerTokenExchange(creds.login_url__c + '/services/oauth2/token', jws);

            //Get the access token
            return bearer.getAccessToken();
        } else {
            return sessionIdForGuestTestValue != null ? sessionIdForGuestTestValue : UserInfo.getSessionId();
        }
    }

    global static String getActualVisitName(VTD1_Actual_Visit__c visit) {
        return getActualVisitName(visit, UserInfo.getLanguage());
    }

    global static String getActualVisitName(VTD1_Actual_Visit__c visit, String lang) {
        String visitName = visit.Name;

        if (visit.VTD1_Protocol_Visit__r.VTD1_EDC_Name__c != null) {
            visitName = visit.VTD1_Protocol_Visit__r.VTD1_EDC_Name__c;
        } else {
            String type = visit.VTD1_Unscheduled_Visit_Type__c;
            String visitNameLabel = '';
            if (type == 'Screen Failure' || visit.Name == 'Ad Hoc Visit') {
                visitNameLabel = 'VTD2_AdHocVisit';
            } else if (type == 'Re Screen Visit') {
                visitNameLabel = 'VTD2_ReScreenVisit';
            } else if (type == 'End of Study' && visit.Name == 'End of Study') {
                visitNameLabel = 'VTR3_EndOfStudy';
            } else if (type == 'Screening Results Visit' && visit.Name == 'Screening Result Visit') {
                visitNameLabel = 'VTR3_ScreeningResultVisit';
            } else if (type == 'Screening Results Visit' && visit.Name == 'Washout/Run-In Results Visit') {
                visitNameLabel = 'VTR3_WashoutRunInResultsVisit';
            }

            if (visitNameLabel!='') {
                visitName = VT_D1_TranslateHelper.getLabelValue(visitNameLabel, lang);
            }
        }
        return visitName;
    }

    global static String getPatientDeliveryName(VTD1_Order__c delivery) {
        return delivery.VTD1_Protocol_Delivery__r.Name != null ? delivery.VTD1_Protocol_Delivery__r.Name : delivery.Name;
    }

    public static Map<Id, Study_Team_Member__c> getBackupStmsForPIs(List<Id> stmPIIdList) {
        Map<Id, Study_Team_Member__c> piIdSTMBackupPIMap = new Map<Id, Study_Team_Member__c>();
        List<Study_Team_Member__c> stmList = [SELECT Id, Study_Team_Member__c, User__c, User__r.ContactId FROM Study_Team_Member__c WHERE Study_Team_Member__c IN : stmPIIdList];

        for (Study_Team_Member__c stm : stmList) {
            piIdSTMBackupPIMap.put(stm.Study_Team_Member__c, stm);
        }
        return piIdSTMBackupPIMap;
    }

    public static Map<Id, Study_Team_Member__c> getBackupStmsForPIs(Id stmPIId) {
        return getBackupStmsForPIs(new List<Id>{stmPIId});
    }

    public static String getSandboxPrefix(){
        String sandboxPrefix = Url.getCurrentRequestUrl().getPath().toLowerCase().split('/')[1];
        if (sandboxPrefix == 'pi' || sandboxPrefix == 'scr' || sandboxPrefix == 'patient'){
            sandboxPrefix = '/' + sandboxPrefix;
        }
        else{
            sandboxPrefix = '';
        }
        return sandboxPrefix;
    }

    public static OrgWideEmailAddress getOrgWideEmailAddress() {
        if (orgWideEmailAddress != null) {
            return orgWideEmailAddress;
        }
        List <OrgWideEmailAddress> owEmailAddresses = [SELECT Id FROM OrgWideEmailAddress ORDER BY CreatedDate LIMIT 1];
        if (!owEmailAddresses.isEmpty()) {
            orgWideEmailAddress = owEmailAddresses[0];
        }
        return orgWideEmailAddress;
    }

    public static EmailTemplate getEmailTemplate(String developerName) {
        EmailTemplate template = emailTemplateMap.get(developerName);
        if (template == null) {
            List <EmailTemplate> templates = [select Id, Subject from EmailTemplate where DeveloperName = :developerName];
            if (!templates.isEmpty()) {
                template = templates[0];
                emailTemplateMap.put(developerName, template);
            }
        }
        return template;
    }

    public static String getProfileNameOfCurrentUser() {
        return getProfileName(UserInfo.getProfileId());
    }
    
    public static List<Id> getIdsFromObjs(List<SObject> objs) {
        Set<Id> objIds = new Set<Id>();
        for (SObject obj : objs) {
            if (obj != null && obj.Id != null) {
                objIds.add(obj.Id);
            }
        }
        return new List<Id>(objIds);
    }

    public static Boolean isValidId(String inputId) {
        try {
            if (inputId instanceof Id
                    && Pattern.compile('[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}').matcher(inputId).matches()){
                Id recordId = Id.valueOf(inputId.left(15));
                recordId.getSobjectType();
                String checksum = ((String)recordId).right(3);
                if (inputId.length()==15 || (inputId.length()==18 && inputId.right(3).equals(checksum))) {
                    // is valid Salesforce Id
                    return true;
                }
            }
        } catch (Exception e){
            System.debug('ERROR: '+e.getMessage());
        }
        // is not valid Salesforce Id
        return false;
    }

    // Calculate avg of numbers list
    public static Decimal avg(List<Decimal> nums) {
        Decimal total = 0.0;
        for (Decimal n : nums) {
            total += n;
        }
        return (total / nums.size()).setScale(2, RoundingMode.CEILING);
    }

    /*
        Conditions a string using the same method eCOA does to create API names. For example it would convert TargetDate to target_date
     */
    public static String conditionEcoaString(String inputString) {
        return inputString
            .replaceAll('([^A-Z])([A-Z])([^A-Z])', '$1_$2$3')
            .toLowerCase()                  // make name lower case
            .replaceAll('[^a-z0-9]', '_')   // replace everything thats not a-z or 0-9 with underscores
            .replaceAll('[_]+', '_')        // replace multiple underscores with a single underscore
            .replaceAll('^[_]*', '')        // trim leading underscores
            .replaceAll('[_]*$', '')        // trim trailing underscores
            .replaceAll('^(\\d)', '_$1');   // add an underscore to a leading number
    }

    public static List<String> getAllFields(SObjectType sObjType) {
        return new List<String>(sObjType.getDescribe().fields.getMap().keySet());
    }

    @InvocableMethod(Label = 'Do Nothing')
    public static void doNothing() {}
    
    public static Object stringToEnum(String enumString, Type enumType) {
        Type cType = Type.forName(String.format('List<{0}>', new List<String>{ enumType.getName() }));
        return ((List<Object>) JSON.deserialize(String.format('["{0}"]', new List<String>{ enumString }), cType))[0];
    }
}