/**
 * @author: Dmitry Yartsev
 * @date: 26.06.2020
 * @description: Controller for view study specific Privacy Notice and EULA
 */
public without sharing class VT_R4_PrivacyEulaViewController {

    private static Map<String, String> SUPPORTED_ARTICLE_TYPES_TO_PATH_MAP = new Map<String, String> {
            '/eula' => 'EULA',
            '/policy' => 'Privacy_Policy'
    };

    private static final Set<String> IRB_PROFILES = new Set<String>{
            'Patient',
            'Caregiver'
    };

    public String getVFContent() {
        return getContent(
                ApexPages.currentPage().getParameters().get('articleId') == null
                        ? ApexPages.currentPage().getParameters().get('studyId')
                        : ApexPages.currentPage().getParameters().get('articleId'),
                ApexPages.currentPage().getParameters().get('language'),
                SUPPORTED_ARTICLE_TYPES_TO_PATH_MAP.get(Test.isRunningTest() ? '/eula' : Site.getPathPrefix())
        );
    }

    /**
     * Get article content based on study Id, language and type
     *
     * @param recordId Id of master article or related study
     * @param lang article language
     * @param type article type
     *
     * @return article content, empty string if no articles found, null if recordId/lang/type is missing
     */

    @AuraEnabled
    public static String getContent(Id recordId, String lang, String type) {
        if (!SUPPORTED_ARTICLE_TYPES_TO_PATH_MAP.values().contains(type) || recordId == null || lang == null) {
            return null;
        }

        QueryBuilder.Condition idCondition;
        if (recordId.getSobjectType() == HealthCloudGA__CarePlanTemplate__c.getSObjectType()) {
            idCondition = new QueryBuilder.CompareCondition(Knowledge__kav.VTD2_Study__c).eq(recordId);
        } else {
            idCondition = new QueryBuilder.CompareCondition(Knowledge__kav.KnowledgeArticleId).eq(recordId);
        }
        
        try {
            List<Knowledge__kav> articlesList = (List<Knowledge__kav>) new QueryBuilder(Knowledge__kav.class)
                    .namedQueryRegister('VT_R4_PrivacyEulaViewController.getContent')
                    .addField(Knowledge__kav.VTD1_Content__c)
                    .addConditions()
                    .add(new QueryBuilder.CompareCondition(Knowledge__kav.IsLatestVersion).eq(true))
                    .add(new QueryBuilder.CompareCondition(Knowledge__kav.PublishStatus).eq('Online'))
                    .add(new QueryBuilder.CompareCondition(Knowledge__kav.Language).eq(lang))
                    .add(new QueryBuilder.SimpleCondition(String.join(new String[] { 'VTD2_Type__c INCLUDES(', type, ')' }, '\'')))
                    .add(idCondition)
                    .setConditionOrder('1 AND 2 AND 3 AND 4 AND 5')
                    .endConditions()
                    .addOrderDesc(Knowledge__kav.LastModifiedDate)
                    .toList();
            return articlesList.isEmpty()
                    ? ''
                    : articlesList.get(0).VTD1_Content__c;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getTypeName() + '\n' + e.getStackTraceString());
        }

    }

    @AuraEnabled
    public static Boolean isPi() {
        String currentUserProfile = VT_D1_HelperClass.getUserProfileName(UserInfo.getUserId());
        if ((currentUserProfile == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) ||
                (currentUserProfile == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME)) {
            return true;
        }
        return false;
    }

    /**
    * Get articles for Terms and Conditions page
    *
    * @param studies A JSON string with list of study Ids
    *
    * @return JSON string with list of articles
    */
    @AuraEnabled
    public static String getArticles(String studies) {
        try {
            User currentUser;
            List<Id> studyIds;

            if (studies != null) { // FOR PI/SCR
                studyIds = (List<Id>) System.JSON.deserialize(studies, List<Id>.class);
                currentUser = new User(LanguageLocaleKey = 'en_US'); // SH-16603
            }

            if (studyIds == null) { // FOR PT/CG
                studyIds = new List<Id>();
                currentUser = (User) new QueryBuilder(User.class)
                        .namedQueryRegister('VT_R4_PrivacyEulaViewController.getUser')
                        .addField('Profile.Name')
                        .addField('Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c')
                        .addField('Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c')
                        .addField('Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_DefaultIRBLanguage__c')
                        .addField('LanguageLocaleKey')
                        .addConditions()
                        .add(new QueryBuilder.CompareCondition(User.Id).eq(UserInfo.getUserId()))
                        .endConditions()
                        .toSObject();
                studyIds.add(currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c);
            }

            System.debug('Study IDs: ' + studyIds + '; Current user: ' + currentUser);
            return JSON.serialize(getArticlesContent(currentUser, studyIds));
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    private static ArticlesContent getArticlesContent(User user, List<Id> studyIds) {
        if (studyIds == null) {
            return new ArticlesContent();
        }
        /** Removing the logic of default and Preffered Language , only User Language will be looked upon -SH- 14527 
         * 
        Map<String, String> articleLanguagesMap = getArticleLanguagesMap(user);

        Map<Id, Knowledge__kav> articlesToMasterIdMap = new Map<Id, Knowledge__kav>();
        if (articleLanguagesMap.get('preferredArticleLanguage') != null) {
            for(Knowledge__kav article : queryArticles(articleLanguagesMap.get('preferredArticleLanguage'), studyIds, articlesToMasterIdMap.keySet())) {
                articlesToMasterIdMap.put(article.MasterVersionId, article);
            }
        }
        if (articleLanguagesMap.get('defaultArticleLanguage') != null) {
            for(Knowledge__kav article : queryArticles(articleLanguagesMap.get('defaultArticleLanguage'), studyIds, articlesToMasterIdMap.keySet())) {
                articlesToMasterIdMap.put(article.MasterVersionId, article);
            }
        }
        */

        Map<Id, Knowledge__kav> articlesToMasterIdMap = new Map<Id, Knowledge__kav>();
        if(user.LanguageLocaleKey != null){
            for(Knowledge__kav article : queryArticles(user.LanguageLocaleKey, studyIds, articlesToMasterIdMap.keySet())) {
                articlesToMasterIdMap.put(article.MasterVersionId, article);
            }
        }


        List<Knowledge__kav> eulaArticles = new List<Knowledge__kav>();
        List<Knowledge__kav> ppArticles = new List<Knowledge__kav>();

        for (Knowledge__kav art : articlesToMasterIdMap.values()) {
            if (art.VTD2_Type__c == 'EULA') {
                eulaArticles.add(art);
            }
            if (art.VTD2_Type__c == 'Privacy_Policy') {
                ppArticles.add(art);
            }
        }
        return new ArticlesContent(eulaArticles, ppArticles);
    }

    /**
    * Get article language based on IRB languages and user language for patient/caregiver, en_US for the rest
    *
    * @param currentUser current user
    *
    * @return language map
    */
    /** Removing the logic of default and Preffered Language , only User Language will be looked upon -SH- 14527 
    private static Map<String, String> getArticleLanguagesMap(User currentUser) {
        Map<String, String> langMap = new Map<String, String>();
       
        langMap.put('defaultArticleLanguage', 'en_US');
        if (currentUser != null && IRB_PROFILES.contains(currentUser.Profile.Name)) {
            if (currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c != null
                    && currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_DefaultIRBLanguage__c != null) {
                if (currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c.split(';').contains(currentUser.LanguageLocaleKey)) {
                    langMap.put('preferredArticleLanguage', currentUser.LanguageLocaleKey);
                    langMap.put('defaultArticleLanguage', null);
                } else {
                    langMap.put('defaultArticleLanguage', currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_DefaultIRBLanguage__c);
                }
            } else {
                langMap.put('preferredArticleLanguage', currentUser.LanguageLocaleKey);
            }
        }
        

        

        return langMap;
    }*/

    /**
    * Query articles
    *
    * @param lang language
    * @param studyIds List of study Ids
    * @param masterArticleIds List of articles master versions Ids
    *
    * @return List<Knowledge__kav>
    */
    private static List<Knowledge__kav> queryArticles(String lang, List<Id> studyIds, Set<Id> masterArticleIds) {

        return (List<Knowledge__kav>) new QueryBuilder(Knowledge__kav.class)
                .namedQueryRegister('VT_R4_PrivacyEulaViewController.queryArticles')
                .addFields('VTD1_Content__c, Language, Title, VTD2_Study__c, MasterVersionId, VTD2_Type__c')
                .addConditions()
                .add(new QueryBuilder.CompareCondition(Knowledge__kav.IsLatestVersion).eq(true))
                .add(new QueryBuilder.CompareCondition(Knowledge__kav.PublishStatus).eq('Online'))
                .add(new QueryBuilder.CompareCondition(Knowledge__kav.Language).eq(lang))
                .add(new QueryBuilder.InCondition(Knowledge__kav.MasterVersionId).notIn(masterArticleIds))
                .add(new QueryBuilder.InCondition(Knowledge__kav.VTD2_Study__c).inCollection(studyIds))
                .add(new QueryBuilder.SimpleCondition(String.join(
                        new String[] {
                                'VTD2_Type__c INCLUDES(',
                                String.join(SUPPORTED_ARTICLE_TYPES_TO_PATH_MAP.values(), '\',\''),
                                ')'
                        },
                        '\''
                )))
                .setConditionOrder('1 AND 2 AND 3 AND 4 AND 5 AND 6')
                .endConditions()
                .toList();
    }

    private class ArticlesContent {
        List<Knowledge__kav> eulaArticles;
        List<Knowledge__kav> ppArticles;
        private ArticlesContent (){
            this.eulaArticles = new List<Knowledge__kav>();
            this.ppArticles = new List<Knowledge__kav>();
        }
        private ArticlesContent (List<Knowledge__kav> eulaArticles, List<Knowledge__kav> ppArticles){
            this.eulaArticles = eulaArticles;
            this.ppArticles = ppArticles;
        }
    }
}