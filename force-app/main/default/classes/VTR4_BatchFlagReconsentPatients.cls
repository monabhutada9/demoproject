/**
 * Created by shume on 27/02/2020.
 */

public  without sharing class VTR4_BatchFlagReconsentPatients implements Database.Batchable<SObject> {

    private Set<Id> siteIds;

    public VTR4_BatchFlagReconsentPatients(Set<Id> siteIds) {
        this.siteIds = siteIds;
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
                SELECT Id, VTD1_Reconsent_Needed__c
                FROM Case
                WHERE VTD1_Virtual_Site__c IN :siteIds
                AND VTD1_isFitForReconsent__c = true AND VTD1_Reconsent_Needed__c = false
        ]);
    }

    public void execute(Database.BatchableContext bc, List<Case> pCases) {
        for (Case c : pCases) {
            c.VTD1_Reconsent_Needed__c = true;
        }
        Database.update(pCases, false);
    }

    public void finish(Database.BatchableContext bc) {
    }
}