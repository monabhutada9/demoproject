/**
* @author: Carl Judge
* @date: 22-Oct-18
* @description:
**/
@IsTest
private class VT_D1_ePRO_Test {
    @testSetup
    private static void setupMethod() {
        Test.startTest();
        DomainObjects.Study_t domainStudy = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );
        HealthCloudGA__CarePlanTemplate__c study = (HealthCloudGA__CarePlanTemplate__c) domainStudy.persist();
        study.VTD1_End_Date__c = Date.today().addMonths(3);
        update study;

        DomainObjects.VirtualSite_t virtSite = new DomainObjects.VirtualSite_t()
                .addStudy(domainStudy)
                .setExternalDiaryLanguage('en_US;de;ru')
                .setDefaultExternalDiaryLanguage('de');
        virtSite.persist();

        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t()
                .setName('Patient Account for Patient User');
        patientAccount.persist();

        //patient
        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
                .setAccountId(patientAccount.id)
                .setFirstName('PatientUserWithPatientRecordType');
        DomainObjects.User_t userPatient = new DomainObjects.User_t()
                .addContact(patientContact)
                .setEcoaGuid('subject_9ecb75cf-a594-45b8-6667-7d11bd9bd41d')
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        userPatient.persist();

        //caregiver
        DomainObjects.User_t caregiverUser = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t()
                        .setFirstName('CaregiverTestUser')
                        .setLastName('VT_D1_ePRO_Test')
                        .setEmail(DomainObjects.RANDOM.getEmail())
                        .setRecordTypeName(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME)
                        .setAccountId(patientAccount.id)
                )
                .setUsername('CaregiverTestUser@caregiver.com')
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME);
        caregiverUser.persist();

        //case
        DomainObjects.Case_t casePatient = new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')//CarePlan
                .setStatus('Pre-consent')
                .addStudy(domainStudy)
                .addUser(userPatient)
                .addContact(patientContact)
                .addAccount(patientAccount)
                .addVTD1_Primary_PG(new DomainObjects.User_t())
                .addVTD1_PI_contact(new DomainObjects.Contact_t());
        casePatient.persist();
        patientContact.addVTD1_Clinical_Study_Membership(casePatient);

        Contact con = [SELECT Id, VTD1_UserId__c FROM Contact WHERE Id = :patientContact.id];
        con.VTD1_UserId__c = userPatient.id;
        update con;

        Case cas = [SELECT Id, VTD1_ePro_can_be_completed_by_Caregiver__c, VTR5_SubsetSafety__c, VTR5_SubsetImmuno__c FROM Case WHERE Contact.FirstName = 'PatientUserWithPatientRecordType'];
        cas.VTD1_ePro_can_be_completed_by_Caregiver__c = true;
        cas.VTR5_SubsetSafety__c = 'testSubSafety';
        cas.VTR5_SubsetImmuno__c = 'testImmuno';
        update cas;

        VTD1_ProtocolVisit__c pVisit = new VTD1_ProtocolVisit__c(
                VTD1_Study__c = study.Id,
                VTD1_VisitDuration__c = '1 hour',
                VTR2_Visit_Participants__c = VTD1_ProtocolVisit__c.VTR2_Visit_Participants__c.getDescribe().getPicklistValues()[0].getValue()
        );
        insert pVisit;

        Id satisfactionSurveyRecTypeId = Schema.SObjectType.VTD1_Protocol_ePRO__c.getRecordTypeInfosByDeveloperName().get('SatisfactionSurvey').getRecordTypeId();
        Id eProRecTypeId = Schema.SObjectType.VTD1_Protocol_ePRO__c.getRecordTypeInfosByDeveloperName().get('ePRO').getRecordTypeId();
        Id externalRecTypeId = Schema.SObjectType.VTD1_Protocol_ePRO__c.getRecordTypeInfosByDeveloperName().get('VTR5_External').getRecordTypeId();
        List<VTD1_Protocol_ePRO__c> pEpros = new List<VTD1_Protocol_ePRO__c>();
        // satisfaction ePRO
        pEpros.add(new VTD1_Protocol_ePRO__c(
                VTD1_Study__c = study.Id,
                RecordTypeId = satisfactionSurveyRecTypeId,
                VDD1_Scoring_Window__c = 10,
                VTD1_Type__c = 'PSC',
                VTR2_Protocol_Reviewer__c = 'Patient Guide',
                VTD1_Response_Window__c = 10,
                VTD1_Subject__c = 'Patient'
        ));
        // event epro
        pEpros.add(new VTD1_Protocol_ePRO__c(
                VTD1_Study__c = study.Id,
                RecordTypeId = eProRecTypeId,
                VTD1_Event__c = pVisit.Id,
                VTD1_Trigger__c = 'Event',
                VTR4_EventType__c = 'Visit Completion',
                VTD1_Safety_Keywords__c = 'Test',
                VDD1_Scoring_Window__c = 10,
                VTD1_Reminder_Window__c = 2,
                VTR2_Protocol_Reviewer__c = 'Patient Guide',
                VTD1_Response_Window__c = 10,
                VTD1_Subject__c = 'Patient'
        ));
        // daily epro
        pEpros.add(new VTD1_Protocol_ePRO__c(
                VTD1_Study__c = study.Id,
                RecordTypeId = eProRecTypeId,
                VTD1_First_Available__c = pVisit.Id,
                VTD1_Period__c = 'Daily',
                VTD1_Trigger__c = 'Periodic',
                VDD1_Scoring_Window__c = 10,
                VTR2_Protocol_Reviewer__c = 'Patient Guide',
                VTD1_Response_Window__c = 10,
                VTD1_Subject__c = 'Patient'
        ));
        // weekly epro
        pEpros.add(new VTD1_Protocol_ePRO__c(
                VTD1_Study__c = study.Id,
                RecordTypeId = eProRecTypeId,
                VTD1_First_Available__c = pVisit.Id,
                VTD1_Period__c = 'Weekly',
                VTD1_Trigger__c = 'Periodic',
                VDD1_Scoring_Window__c = 10,
                VTR2_Protocol_Reviewer__c = 'Patient Guide',
                VTD1_Response_Window__c = 10,
                VTD1_Subject__c = 'Patient'
        ));
        // monthly epro
        pEpros.add(new VTD1_Protocol_ePRO__c(
                VTD1_Study__c = study.Id,
                RecordTypeId = eProRecTypeId,
                VTD1_First_Available__c = pVisit.Id,
                VTD1_Period__c = 'Monthly',
                VTD1_Trigger__c = 'Periodic',
                VDD1_Scoring_Window__c = 10,
                VTR2_Protocol_Reviewer__c = 'Patient Guide',
                VTD1_Response_Window__c = 10,
                VTD1_Subject__c = 'Patient'
        ));
        // every x days epro
        pEpros.add(new VTD1_Protocol_ePRO__c(
                VTD1_Study__c = study.Id,
                RecordTypeId = eProRecTypeId,
                VTD1_First_Available__c = pVisit.Id,
                VTD1_Period__c = 'Every X Days',
                VTD1_Day_Frequency__c = 3,
                VTD1_Trigger__c = 'Periodic',
                VDD1_Scoring_Window__c = 10,
                VTR2_Protocol_Reviewer__c = 'Patient Guide',
                VTD1_Response_Window__c = 10,
                VTD1_Subject__c = 'Patient'
        ));
        // specific days repeated epro
        pEpros.add(new VTD1_Protocol_ePRO__c(
                VTD1_Study__c = study.Id,
                RecordTypeId = eProRecTypeId,
                VTD1_First_Available__c = pVisit.Id,
                VTD1_Period__c = 'Specific Days Repeated',
                Days__c = 'Monday;Tuesday',
                VTD1_Trigger__c = 'Periodic',
                VDD1_Scoring_Window__c = 10,
                VTR2_Protocol_Reviewer__c = 'Patient Guide',
                VTD1_Response_Window__c = 10,
                VTD1_Subject__c = 'Patient'
        ));
        //triggered after case status update
        pEpros.add(new VTD1_Protocol_ePRO__c(
                VTD1_Study__c = study.Id,
                RecordTypeId = eProRecTypeId,
                VTR4_EventType__c = 'Patient Milestone Achieved',
                VTR4_PatientMilestone__c = 'Consented',
                VTR4_EventTime__c = System.now().time(),
                VTR4_EventOffset__c = 10,
                VTR4_EventOffsetPeriod__c = 'Days',
                VTD1_Period__c = 'Specific Days Repeated',
                Days__c = 'Monday;Tuesday',
                VTD1_Trigger__c = 'Event',
                VDD1_Scoring_Window__c = 10,
                VTR2_Protocol_Reviewer__c = 'Patient Guide',
                VTD1_Response_Window__c = 10,
                VTD1_Subject__c = 'Patient'
        ));
        // external visit update
        pEpros.add(new VTD1_Protocol_ePRO__c(
                Name = 'external visit update',
                VTR5_eCOAEventName__c = 'test name1',
                VTR5_eCOADiaryGUID__c = 'diary_844f6c3f-a72e-4f6d-af0c-fc9d5c1caf3b',
                VTD1_Study__c = study.Id,
                RecordTypeId = externalRecTypeId,
                VTD1_Event__c = pVisit.Id,
                VTD1_Trigger__c = 'Event',
                VTR4_EventType__c = 'Visit Completion',
                VTR2_Protocol_Reviewer__c = 'Patient Guide',
                VTD1_Subject__c = 'Patient',
                VTR5_SubsetSafetyID__c = 'testSubSafety',
                VTR5_SubsetImmunoID__c = null
        ));
        //external triggered after case status update
        pEpros.add(new VTD1_Protocol_ePRO__c(
                Name = 'external case update',
                VTR5_eCOAEventName__c = 'test name',
                VTR5_eCOADiaryGUID__c = 'diary_844f6c3f-a72e-4f6d-af0c-fc9d5c1caf3c',
                VTD1_Study__c = study.Id,
                RecordTypeId = externalRecTypeId,
                VTR4_EventType__c = 'Patient Milestone Achieved',
                VTR4_PatientMilestone__c = 'Consented',
                VTR2_Protocol_Reviewer__c = 'Patient Guide',
                VTD1_Subject__c = 'Patient',
                VTR5_SubsetSafetyID__c = 'testSubSafety',
                VTR5_SubsetImmunoID__c = null
        ));
        insert pEpros;

        List<VTR2_eDiaryInstructions__c> instrList = new List<VTR2_eDiaryInstructions__c>();
        Map<Id, VTR2_Protocol_eDiary_Scale__c> scalesByProtocolId = new Map<Id, VTR2_Protocol_eDiary_Scale__c>();
        for (VTD1_Protocol_ePRO__c item : pEpros) {
            scalesByProtocolId.put(item.Id, new VTR2_Protocol_eDiary_Scale__c(
                    VTR2_Protocol_eDiary__c = item.Id
            ));
            instrList.add(new VTR2_eDiaryInstructions__c(VTR2_Protocol_eDiary__c = item.Id));
        }
        insert instrList;
        insert scalesByProtocolId.values();

        List<VTR2_ProtocoleDiaryScaleResponse__c> responses = new List<VTR2_ProtocoleDiaryScaleResponse__c>();
        for (VTR2_Protocol_eDiary_Scale__c scale : scalesByProtocolId.values()) {
            responses.add(new VTR2_ProtocoleDiaryScaleResponse__c(
                    VTR2_Protocol_eDiary_Scale__c = scale.Id,
                    VTR2_Response__c = 'Test',
                    VTR2_Score__c = 1
            ));
        }
        insert responses;
        Map<Id, VTD1_Protocol_ePro_Question__c> questionsByEpoId = new Map<Id, VTD1_Protocol_ePro_Question__c>();
        for (VTD1_Protocol_ePRO__c item : pEpros) {
            questionsByEpoId.put(item.Id, new VTD1_Protocol_ePro_Question__c(
                    VTD1_Protocol_ePRO__c = item.Id,
                    VTD1_Safety_Threshold_Lower__c = 4,
                    VTR2_Scoring_Type__c = 'Auto',
                    VTD1_Type__c = 'Radio',
                    VTR2_Protocol_eDiary_Scale__c = scalesByProtocolId.get(item.Id).Id,
                    VTR2_Auto_Score_Default__c = 1
            ));
        }
        insert questionsByEpoId.values();
        Map<Id, VTR2_Protocol_eDiary_Group__c> groupsByEproId = new Map<Id, VTR2_Protocol_eDiary_Group__c>();
        for (VTD1_Protocol_ePRO__c item : pEpros) {
            groupsByEproId.put(item.Id, new VTR2_Protocol_eDiary_Group__c(
                    VTR2_Protocol_eDiary__c = item.Id,
                    VTR2_Number_Within_Page__c = 1,
                    VTR2_Page_Number__c = 1
            ));
        }
        insert groupsByEproId.values();
        List<VTR2_Protocol_eDiary_Group_Question__c> groupQuestions = new List<VTR2_Protocol_eDiary_Group_Question__c>();
        for (VTD1_Protocol_ePRO__c item : pEpros) {
            groupQuestions.add(new VTR2_Protocol_eDiary_Group_Question__c(
                    VTR2_Protocol_eDiary_Group__c = groupsByEproId.get(item.Id).Id,
                    VTR2_Protocol_eDiary_Question__c = questionsByEpoId.get(item.Id).Id
            ));
        }
        insert groupQuestions;

        insert new VTD1_Actual_Visit__c(
                VTD1_Protocol_Visit__c = pVisit.Id,
                Sub_Type__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_SUBTYPE_PSC,
                VTD1_Case__c = cas.Id
        );

    }
    @isTest
    private static void testEvent() {
        VTD1_Actual_Visit__c visit = [SELECT Id FROM VTD1_Actual_Visit__c];
        visit.VTD1_Scheduled_Date_Time__c = Datetime.now();
        visit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED;
        update visit;
        Test.startTest();
        VTD1_Survey__c eventSurvey = [SELECT Id, VTD1_Protocol_ePRO__c, VTD1_CSM__c, VTD1_CSM__r.Contact.AccountId, VTR2_DocLanguage__c, VTD1_Protocol_ePRO__r.Name FROM VTD1_Survey__c WHERE VTD1_Protocol_ePRO__r.VTD1_Type__c = 'PSC'];
        VT_D1_eProHomeController.getEdairyInstructions(new List<VTD1_Survey__c>{
                eventSurvey
        });
        VTD1_Protocol_ePRO__c protocolEPRO = [select Id, Name, (select Id, VTR2_Content__c from Protocol_eDiary_Instructions__r) from VTD1_Protocol_ePRO__c where Id = :eventSurvey.VTD1_Protocol_ePRO__c];
        eventSurvey.VTD1_Protocol_ePRO__r = protocolEPRO;
        eventSurvey.VTD1_Protocol_ePRO__c = protocolEPRO.Id;
        VT_D1_eProHomeController.translateSurveysAndInstrustions(new List<VTD1_Survey__c>{
                eventSurvey
        });
        VTD1_Survey_Answer__c eventSurveyAnswer = [SELECT Id, VTD1_Question__c FROM VTD1_Survey_Answer__c WHERE VTD1_Survey__c = :eventSurvey.Id LIMIT 1];
        eventSurveyAnswer.VTR2_English_Answer__c = 'Test';
        eventSurveyAnswer.VTD1_Score__c = 1;
        update eventSurveyAnswer;
        eventSurvey.VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED;
        eventSurvey.VTR2_DocLanguage__c = 'es';
        update eventSurvey;
        eventSurvey.VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED;
        // test protocol epro updates
        String error = '';
        try {
            update new VTD1_Protocol_ePRO__c(Id = eventSurvey.VTD1_Protocol_ePRO__c, VTD1_Type__c = 'Non-PSC');
        } catch (Exception e) {
            error = e.getMessage();
        }
        system.debug(error);
        System.assert(error.contains(VT_D1_Protocol_ePRO_Update_Verifier.ERROR_MESSAGE));
        error = '';
        try {
            update new VTD1_Protocol_ePro_Question__c(Id = eventSurveyAnswer.VTD1_Question__c, VTD1_Type__c = 'Radio');
        } catch (Exception e) {
            error = e.getMessage();
        }
        System.assert(error.contains(VT_D1_Protocol_ePRO_Update_Verifier.ERROR_MESSAGE));
        // add translations
        List<VTD1_Translation__c> translations = new List<VTD1_Translation__c>{
                new VTD1_Translation__c (
                        VTD1_Value__c = 'sp1;sp2;sp3',
                        VTD1_Object_Name__c = 'VTD1_Protocol_ePro_Question__c', VTD1_Language__c = 'es',
                        VTD1_Record_Id__c = eventSurveyAnswer.VTD1_Question__c, VTD1_Field_Name__c = 'VTD1_Options__c'),
                new VTD1_Translation__c (
                        VTD1_Value__c = 'Spanish', VTD1_Field_Name__c = 'VTR2_QuestionContentLong__c',
                        VTD1_Object_Name__c = 'VTD1_Protocol_ePro_Question__c', VTD1_Language__c = 'es',
                        VTD1_Record_Id__c = eventSurveyAnswer.VTD1_Question__c)
        };
        insert translations;
        // test task epro task creation
        VT_D1_ePROSchedulingTaskHelper.TaskTemplate tTemplate = new VT_D1_ePROSchedulingTaskHelper.TaskTemplate();
        tTemplate.surveyId = eventSurvey.Id;
        tTemplate.subject = 'Test';
        tTemplate.dueDate = Date.today();
        tTemplate.recordTypeDeveloperName = 'SimpleTask';
        VT_D1_ePROSchedulingTaskHelper.sendPatientCaregiverTasks(new VT_D1_ePROSchedulingTaskHelper.TaskTemplate[]{
                tTemplate
        });
        // Test VT_D1_eProHomeController
        User u = [SELECT Id FROM User WHERE Username = 'CaregiverTestUser@caregiver.com'];
        insert new AccountShare(
                UserOrGroupId = u.Id,
                AccountId = eventSurvey.VTD1_CSM__r.Contact.AccountId,
                AccountAccessLevel = 'Edit',
                CaseAccessLevel = 'Edit',
                OpportunityAccessLevel = 'Edit',
                ContactAccessLevel = 'Edit'
        );
        System.runAs(u) {
            VT_D1_eProHomeController.getSurveys();
            VT_D1_eProHomeController.getSurveysForPatient();
            eventSurvey.VTD1_Status__c = 'Ready to Start';
            VT_D1_eProHomeController.getQuestionsAnswersForPTCG(eventSurvey.Id, eventSurvey.VTD1_Protocol_ePRO__c,  eventSurvey.VTD1_Status__c);
            VT_D1_eProHomeController.getQuestionsAnswers(eventSurvey.Id, eventSurvey.VTD1_Protocol_ePRO__c, eventSurvey.VTD1_Status__c);
            VT_D1_eProHomeController.getQuestionsAnswers(eventSurvey.Id, eventSurvey.VTD1_Protocol_ePRO__c);
            VT_D1_eProHomeController.updateDateTime(eventSurvey.Id, VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED);
            eventSurvey.VTD1_Status__c = 'Not Started';
            update eventSurvey;
            VT_D1_eProHomeController.updateDateTime(eventSurvey.Id, 'Not Started');
            List<VT_D1_eProHomeController.AnswerWrapper> answers = new List<VT_D1_eProHomeController.AnswerWrapper>();
            VT_D1_eProHomeController.AnswerWrapper answer = new VT_D1_eProHomeController.AnswerWrapper();
            System.debug('eventSurveyAnswer.VTD1_Question__c ' + eventSurveyAnswer.VTD1_Question__c);
            answer.questionId = eventSurveyAnswer.VTD1_Question__c;
            answer.answer = 'test';
            answers.add(answer);
            system.debug(answers);
            eventSurvey.VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_IN_PROGRESS;
            update eventSurvey;
            VT_D1_eProHomeController.saveAnswers(JSON.serialize(answers), eventSurvey.Id, true);
        }
        Test.stopTest();
    }
    @isTest
    private static void testPeriodic() {
        Test.startTest();
        VTD1_Actual_Visit__c visit = [SELECT Id FROM VTD1_Actual_Visit__c];
        visit.VTD1_Scheduled_Date_Time__c = Datetime.now();
        visit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED;
        update visit;
        VTD1_Survey_Answer__c periodicSurveyAnswer = [SELECT Id, VTD1_Survey__c FROM VTD1_Survey_Answer__c WHERE VTD1_Survey__r.VTD1_Protocol_ePRO__r.VTD1_Safety_Keywords__c = null LIMIT 1];
        VTD1_Survey__c periodicSurvey = [SELECT Id FROM VTD1_Survey__c WHERE Id = :periodicSurveyAnswer.VTD1_Survey__c];
        periodicSurveyAnswer.VTR2_English_Answer__c = '1';
        update periodicSurveyAnswer;
        periodicSurvey.VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED;
        update periodicSurvey;
        Test.stopTest();
    }
    @isTest
    private static void testClone() {
        // test clone protocol epro controller
        Integer currentCount = [SELECT COUNT() FROM VTR2_eDiaryInstructions__c];
        Test.startTest();
        VT_D1_Protocol_ePRO_Cloner_Controller.go([SELECT Id FROM VTD1_Protocol_ePRO__c WHERE VTD1_Trigger__c = 'Event' LIMIT 1].Id, null);
        Test.stopTest();
        System.assertEquals(currentCount + 1, [SELECT COUNT() FROM VTR2_eDiaryInstructions__c]);
    }
    @isTest

    private static void testUpdateCase() {
        Test.startTest();
        Case cas = [SELECT Id, Status, VTR5_SubsetSafety__c FROM Case WHERE VTR5_SubsetSafety__c != NULL LIMIT 1];
        cas.Status = 'Consented';
        update cas;
        Test.stopTest();
    }

    @isTest
    //SH-19377: temporary workaround for "Oxygen Level and Heart Rate" diaries
    private static void oxygenCloningTest() {
        Test.startTest();
        Case cas = [SELECT Id, Status, VTR5_SubsetSafety__c FROM Case LIMIT 1];
        insert new VTD1_Survey__c(VTR5_eCoa_RuleName__c = 'Oxygen Level and Heart Rate', Name = 'Oxygen Level and Heart Rate', VTD1_CSM__c = cas.Id);
        system.assertEquals(3, [select count() from VTD1_Survey__c where name ='Oxygen Level and Heart Rate']);
        Test.stopTest();
    }
}