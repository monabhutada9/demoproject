@IsTest
public class VTR2_BatchNoneLoggedInPatientCheckTest {
    private static Id accountRecordTypeIdOfSponsor = Account.getSObjectType().getDescribe().getRecordTypeInfosByName().get('Sponsor').getRecordTypeId();
    private static Id caseRecordTypeIdOfPcf = Case.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('VTD1_PCF').getRecordTypeId();
    private static Id stmRecordTypeIdOfRe = Study_Team_Member__c.getSObjectType().getDescribe().getRecordTypeInfosByName().get('RE').getRecordTypeId();

    @IsTest
    public static void testPositiveBatchExecution() {
        generateTestData(true);

        Test.startTest();
        Database.executeBatch(new VT_D1_batch_None_Logged_In_Patient_Check());
        Test.stopTest();
        System.assertEquals(1, [SELECT Id FROM Task].size());
    }

    @IsTest
    public static void testNegativeBatchExecutionWithoutStm() {
        generateTestData(false);

        Test.startTest();
        Database.executeBatch(new VT_D1_batch_None_Logged_In_Patient_Check());
        Test.stopTest();
        System.assertEquals(0, [SELECT Id FROM Task].size());
    }

    private static void generateTestData(Boolean createStudyTeamMember) {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeId(accountRecordTypeIdOfSponsor)
                );
        DomainObjects.User_t user = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5));
        new DomainObjects.Case_t()
                .setRecordTypeId(caseRecordTypeIdOfPcf)
                .addStudy(study)
                .addUser(user)
                .persist();

        if (createStudyTeamMember) {
            new DomainObjects.StudyTeamMember_t()
                    .setRecordTypeId(stmRecordTypeIdOfRe)
                    .addUser(user)
                    .addStudy(study)
                    .persist();
        }
    }
}