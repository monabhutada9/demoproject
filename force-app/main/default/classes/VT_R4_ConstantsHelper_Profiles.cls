/**
 * THIS CLASS IS DEPRECATED! DON'T USE IT!
 *
 * Use the following instead:
 * @see VT_R4_ConstantsHelper_ProfilesSTM
 */
public with sharing class VT_R4_ConstantsHelper_Profiles {
    public static final String PG_PROFILE_NAME = 'Patient Guide';
    public static final String PI_PROFILE_NAME = 'Primary Investigator';
    public static final String PATIENT_PROFILE_NAME = 'Patient';
    public static final String CAREGIVER_PROFILE_NAME = 'Caregiver';
    public static final Set<String> HIPAA_PROFILES = new Set<String>{PATIENT_PROFILE_NAME, CAREGIVER_PROFILE_NAME};
    public static final String TMA_PROFILE_NAME = 'Therapeutic Medical Advisor';
    public static final String SC_PROFILE_NAME = 'Study Concierge';
    public static final String CM_PROFILE_NAME = 'Monitor';
    public static final String CRA_PROFILE_NAME = 'CRA';
    public static final String SCR_PROFILE_NAME = 'Site Coordinator';
    public static final String MRR_PROFILE_NAME = 'Monitoring Report Reviewer';
    public static final String RS_PROFILE_NAME = 'Regulatory Specialist';
    public static final String AUDITOR_PROFILE_NAME = 'Auditor';
}