/**
* Created by Leonid Bartenev
*/

public class VT_D1_RequestBuilder_Randomize implements VT_D1_RequestBuilder{
    
    // Data classes: ---------------------------------------------------------------------------------------------------
    private class SubjectToRandomize {
        public String protocolId;
        public String subjectId;
        public String subjectStatus;
        public List<Stratification> stratification;
        public AuditLog auditLog;
    }
    
    private class Stratification {
        String Key;
        String Value;
    }
    
    public class AuditLog {
        public String UserId;
        public Datetime Timestamp;
        
        public AuditLog(){
            UserId = UserInfo.getUserId();
            Timestamp = Datetime.now();
        }
    }
    
    Id caseId;
    
    public VT_D1_RequestBuilder_Randomize(Id caseId){
        this.caseId = caseId;
    }
    
    // Build logic: ----------------------------------------------------------------------------------------------------
    public String buildRequestBody() {
        Case c = [
            SELECT Id, VTD1_Study__c,ContactId,VTD1_ContactGender__c, VTD1_Study__r.Name, VTD1_Subject_ID__c, Status
            FROM Case
            WHERE Id = :caseId
        ];
        SubjectToRandomize subject = new SubjectToRandomize();
        subject.protocolId = c.VTD1_Study__r.Name;
        subject.subjectId = c.VTD1_Subject_ID__c;
        subject.subjectStatus = c.Status;
        subject.stratification = getStratification(c);
        subject.auditLog = new AuditLog();
        String jsonSubject = JSON.serializePretty(subject, true);
        return jsonSubject;
    }
    
    // auxiliary methods: ----------------------------------------------------------------------------------------------
    public static List<Stratification> getStratification(Case c) {
        List<Stratification> stratList = new List<Stratification>();
        
        List<VTD1_Patient_Stratification__c> psList = [
            SELECT Id, Value__c, VTD1_Study_Stratification__r.Name
            FROM VTD1_Patient_Stratification__c
            WHERE VTD1_Patient__c = :c.Id
            AND Value__c!=NULL
            AND VTD1_Study_Stratification__r.Name!=NULL
        ];
        
        //if (psList.isEmpty()) return stratList;
        for (VTD1_Patient_Stratification__c psItem : psList) {
            Stratification strat = new Stratification();
            strat.Key = psItem.VTD1_Study_Stratification__r.Name;
            strat.Value = psItem.Value__c;
            stratList.add(strat);
        }
        
        // To add additional stratification parameters based on conditions  
        List<Stratification> additionalStratifications;
        additionalStratifications = getAdditionalStratifications(c);
        if(additionalStratifications != null && !additionalStratifications.isEmpty()){
            system.debug('-->'+additionalStratifications);
            stratList.addAll(additionalStratifications);
        }
        
        return stratList;
    }
    private static List<Stratification> getAdditionalStratifications(Case c){
        List<Stratification> stratList = new List<Stratification>(); 
        VTR5_Sponsor_Specific_Protocol_Config__mdt sponsorMappings;
        Study_Stratification_Code__mdt studyStratificationCodeMapping;
        try{if(Test.isRunningTest()){
            sponsorMappings = [Select Id, VTR5_eCoA_DiagnosisDate_month_datakey__c, VTR5_eCoA_DiagnosisDate_year_datakey__c, 
                               DeveloperName FROM VTR5_Sponsor_Specific_Protocol_Config__mdt 
                               WHERE VTR5_eCoA_DiagnosisDate_month_datakey__c != null AND VTR5_eCoA_DiagnosisDate_year_datakey__c != NULL ];
        }else{
            //get Custom metadata based on Study Id
            Map<String,VTR5_Sponsor_Specific_Protocol_Config__mdt> studyIdToSponsorSpecificProtocolMap = VT_R5_eCoaSSO.getSponsorSpecificProtocol();
            sponsorMappings = studyIdToSponsorSpecificProtocolMap.get(c.VTD1_Study__c);
        }
            //map to get study stratification codes
            Map<String, Map<String,String>> mapOfStudyStratification = new Map<String, Map<String, String>>();
            
            List<Study_Stratifications__mdt> studyStratification = [select DeveloperName, (Select DeveloperName,Code__c from Study_Stratification_Codes__r) from Study_Stratifications__mdt where Sponsor_Protocol__r.DeveloperName =: c.VTD1_Study__c];

            for(Study_Stratifications__mdt strat : studyStratification){
                if(!mapOfStudyStratification.containsKey(strat.DeveloperName)){
                    mapOfStudyStratification.put(strat.DeveloperName.toLowerCase(),new Map<string,String>());
                }
                List<Study_Stratification_Code__mdt> stratCodes = strat.Study_Stratification_Codes__r;
                for(Study_Stratification_Code__mdt stratCode : stratCodes){
                    mapOfStudyStratification.get(strat.DeveloperName.toLowerCase()).put(stratCode.DeveloperName.toLowerCase(),stratCode.Code__c);
                }
            }
             if(sponsorMappings.Pass_Stratification_During_Randomization__c){
				//Query to get Survey Answers record
           		List<VTD1_Survey_Answer__c> surveyAnswers = new List<VTD1_Survey_Answer__c>();    
                if(sponsorMappings.VTR5_eCoA_DiagnosisDate_month_datakey__c != NULL && sponsorMappings.VTR5_eCoA_DiagnosisDate_year_datakey__c != NULL){
                    surveyAnswers =[SELECT Id,Name,Question__c,VTD1_Answer__c,VTD1_Question__c,VTD1_Survey__r.VTD1_CSM__c,VT_R5_External_Widget_Data_Key__c 
                                                            FROM VTD1_Survey_Answer__c 
                                                            WHERE VTD1_Survey__r.VTD1_CSM__c =: c.Id
                                                            AND (VT_R5_External_Widget_Data_Key__c = : sponsorMappings.VTR5_eCoA_DiagnosisDate_month_datakey__c OR VT_R5_External_Widget_Data_Key__c = : sponsorMappings.VTR5_eCoA_DiagnosisDate_year_datakey__c)];
                }
				List<Address__c> listOfAddress = [SELECT Id,VTD1_Primary__c,State__c,VTD1_Contact__c
                                                 FROM Address__c 
                                                 WHERE VTD1_Primary__c = true AND VTD1_Contact__c =: c.ContactId LIMIT 1];
                List<String> stratificationKeys = new List<String>();
                stratificationKeys = sponsorMappings.Stratifiction_Parameter_Keys__c.split(';');
                for(String strKey : stratificationKeys ){
                   // Logic for DiagnosisDate
                    if(strKey.equalsIgnoreCase(VT_R4_ConstantsHelper_Integrations.STRINGDIAGNOSISDATE) && !surveyAnswers.isEmpty()){
                        String strDiagnosisDate = VT_R4_ConstantsHelper_Integrations.STRINGDIAGNOSISDATE;
                        Date randomizedDate = System.today();
                        Integer diabetesMonth = null;
                        Integer diabetesYear = null;
                        Integer diagnosisDateValue = null;
                        
                        for(VTD1_Survey_Answer__c surAnswers : surveyAnswers){
                           if(surAnswers.VT_R5_External_Widget_Data_Key__c == sponsorMappings.VTR5_eCoA_DiagnosisDate_month_datakey__c){
                                diabetesMonth = Integer.ValueOf(surAnswers.VTD1_Answer__c);
                             }
                            else if(surAnswers.VT_R5_External_Widget_Data_Key__c == sponsorMappings.VTR5_eCoA_DiagnosisDate_year_datakey__c){
                                diabetesYear = Integer.ValueOf(surAnswers.VTD1_Answer__c);
                            }  
                             if(randomizedDate != null && diabetesMonth != null && diabetesYear != null){
                                diagnosisDateValue = VT_D1_RequestBuilder_Randomize.processDiagnosisDateKey(randomizedDate,diabetesMonth,diabetesYear);
                            }
                         }
                         
                        if(strDiagnosisDate != null){
                                Stratification strat = new Stratification();
                                strat.Key = strDiagnosisDate;
                                strat.Value = String.ValueOf(diagnosisDateValue);
                                stratList.add(strat);
                            }
                    }else if(strKey.equalsIgnoreCase(VT_R4_ConstantsHelper_Integrations.STRINGSTATE)){
                         String contactStateAddres='';
                        if(!listOfAddress.isEmpty() && listOfAddress[0].State__c != null){
                            contactStateAddres = listOfAddress[0].State__c.toLowerCase();
                            String stratCode = mapOfStudyStratification.get(strKey.toLowerCase()).get(contactStateAddres);
                            if(stratCode != null && stratCode != ''){
                                Stratification strat = new Stratification();
                                strat.Key = strKey; 
                                strat.Value = stratCode;
                                stratList.add(strat);    
                            }
                        } 
                    }else if(strKey.equalsIgnoreCase(VT_R4_ConstantsHelper_Integrations.STRINGGENDER)){
                        if(c.VTD1_ContactGender__c !=null){
                            String contactGender=c.VTD1_ContactGender__c.toLowerCase();
                            String stratCode = mapOfStudyStratification.get(strKey.toLowerCase()).get(contactGender);
                            Stratification strat = new Stratification();
                            strat.Key = strKey;
                            strat.Value = stratCode;
                            stratList.add(strat);
                        }
                    }
                }
            }
           }catch (Exception e) {
               System.debug('Error :' +e.getMessage());
           }

        return stratList;
    }
    
    /*  Description: Method will perform calculations and return DiagnosisDate key 1/2 integer value.
*   Parameter  : Randomize Date, string month, String year.
*   return     : Integer key value
*/
    Public static integer processDiagnosisDateKey(Date randomizeReqDate, Integer eCoaMonth, Integer eCoaYear){
        Integer diagnosisDateParam = null;
        String eCoaDateString = eCoaMonth+'/'+VT_R4_ConstantsHelper_Integrations.STRINGDEFAULTDATE+'/'+eCoaYear;//eCoaMonth+'01/'+ eCoaMonth+'/'+eCoaYear; //set same day value to eCoaDate from Randomization for camparison.   
        date eCoaDate = date.parse(eCoaDateString);
        Integer monthsBetween = eCoaDate.monthsBetween(randomizeReqDate);
        Integer years = monthsBetween/12;
        if(years < 1){
            diagnosisDateParam = 1;
        }else if(years >= 1){
            diagnosisDateParam = 2;
        }
        return diagnosisDateParam;
    }
}