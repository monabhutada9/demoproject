/**
 * Created by Maksim Fedarenka on 11-May-20.
 */

@IsTest
public class VTR4_BatchFlagReconsentPatientsTest {
	private static final String CASE_STATUS_CONSENTED = 'Consented';
	private static final String RECORD_TYPE_CARE_PLAN = 'CarePlan';
	
	public static void testBehavior() {
		Case testCase = getCase();
		testCase.Status = CASE_STATUS_CONSENTED;
		update testCase;
		Set<Id> siteIds = new Set<Id>{testCase.VTD1_Virtual_Site__c};
		Test.startTest();
		VTR4_BatchFlagReconsentPatients batch = new VTR4_BatchFlagReconsentPatients(siteIds);
		Database.executeBatch(batch, 200);
		Test.stopTest();


	}

	private static Case getCase() {
		return [
				SELECT Id, VTD1_Virtual_Site__c, VTD1_isFitForReconsent__c, RecordType.DeveloperName, VTD1_Study__c,
						Status, VTD1_Reconsent_Needed__c
				FROM Case
				WHERE RecordType.DeveloperName = :RECORD_TYPE_CARE_PLAN
				LIMIT 1
		];
	}
}