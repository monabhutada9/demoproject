/**
* @author: Carl Judge
* @date: 19-Nov-19
**/

@IsTest
public with sharing class VT_R3_AsyncDMLTest {
    public static void testInsert() {
        List<Account> accs = new List<Account>{new Account(Name = 'Test')};

        Test.startTest();
        new VT_R3_AsyncDML(accs, 'INSERT').publish();
        Test.stopTest();
        System.assertEquals(1, [SELECT COUNT() FROM Account]);
    }

    public static void testUpdate() {
        Account acc = new Account(Name = 'Test');
        insert acc;
        acc.Name = 'Test1';

        Test.startTest();
        new VT_R3_AsyncDML(new List<Account>{acc}, 'UPDATE').publish();
        Test.stopTest();
        System.assertEquals('Test1', [SELECT Name FROM Account WHERE Id = :acc.Id].Name);
    }

    public static void testUpsert() {
        Account acc = new Account(Name = 'Test');
        insert acc;
        acc.Name = 'Test1';
        Account acc2 = new Account(Name = 'Test2');

        Test.startTest();
        new VT_R3_AsyncDML(new List<Account>{acc, acc2}, 'UPSERT', true).publish();
        Test.stopTest();
    }

    public static void testDelete() {
        Account acc = new Account(Name = 'Test');
        insert acc;

        Test.startTest();
        new VT_R3_AsyncDML(new List<Account>{acc}, 'DELETE').publish();
        Test.stopTest();
        System.assertEquals(0, [SELECT COUNT() FROM Account]);
    }

    public static void testUndelete() {
        Account acc = new Account(Name = 'Test');
        insert acc;
        delete acc;

        Test.startTest();
        new VT_R3_AsyncDML(new List<Account>{acc}, 'UNDELETE').publish();
        Test.stopTest();
        System.assertEquals(1, [SELECT COUNT() FROM Account]);
    }

    public static void testBatch() {
        List<Account> accs = new List<Account>{
            new Account(Name = 'Test'),
            new Account(Name = 'Test2'),
            new Account(Name = 'Test3')
        };

        Test.startTest();
        VT_R3_AsyncDML.doBatchedDML(accs, 'INSERT', 2);
        Test.stopTest();
    }
}