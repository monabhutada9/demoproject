/**
 * Created by Alexander Komarov on 25.11.2019.
 */

@IsTest
public class VT_R3_RestPatientVideoConferenceTest {
    public static void firstTest() {
        setRestContext('/Patient/VideoConference/Token', 'GET');
        VT_R2_HttpCalloutMockImpl httpMock = new VT_R2_HttpCalloutMockImpl();
        Test.setMock(HttpCalloutMock.class, httpMock);
        httpMock.response = new HttpResponse();
        httpMock.response.setStatus('OK');
        httpMock.response.setStatusCode(200);
        httpMock.response.setBody('{"message":"SOMETOKEN"}');

        Test.startTest();
        VT_R3_RestPatientVideoConference.getVideoMethod();
        setRestContext('/Patient/VideoConference/Token?sessionId=sessionId', 'GET');
        RestContext.request.params.put('sessionId', 'sessionId');
        VT_R3_RestPatientVideoConference.getVideoMethod();
        Test.stopTest();
    }
    public static void secondTest() {
        VTD1_Actual_Visit__c av = [SELECT Id FROM VTD1_Actual_Visit__c LIMIT 1];

        Test.startTest();
        setRestContext('/Patient/VideoConference/SessionId', 'GET');
        RestContext.request.params.put('visitId', '123');
        VT_R3_RestPatientVideoConference.getVideoMethod();

        setRestContext('/Patient/VideoConference/SessionId', 'GET');
        RestContext.request.params.put('visitId', UserInfo.getUserId());
        VT_R3_RestPatientVideoConference.getVideoMethod();

        setRestContext('/Patient/VideoConference/SessionId?visitId=', 'GET');
        RestContext.request.params.put('visitId', av.Id);
        VT_R3_RestPatientVideoConference.getVideoMethod();

        delete [SELECT Id FROM Video_Conference__c LIMIT 1];

        setRestContext('/Patient/VideoConference/SessionId?visitId=', 'GET');
        RestContext.request.params.put('visitId', av.Id);
        VT_R3_RestPatientVideoConference.getVideoMethod();
        Test.stopTest();
    }
    public static void thirdTest() {
        Test.startTest();
        setRestContext('/Patient/VideoConference/Info', 'GET');
        VT_R3_RestPatientVideoConference.getVideoMethod();
        setRestContext('/Patient/VideoConference/Invalid', 'GET');
        VT_R3_RestPatientVideoConference.getVideoMethod();
        Test.stopTest();
    }

    static void setRestContext(String URI, String method) {
        RestContext.request = new RestRequest();
        RestContext.response = new RestResponse();
        RestContext.request.requestURI = URI;
        RestContext.request.httpMethod = method;
    }
}