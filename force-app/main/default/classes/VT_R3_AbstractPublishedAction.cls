/**
* @author: Carl Judge
* @date: 02-Sep-19
* @description: A class extending this can be published on the event bus and so executed in a fresh transaction.
* Note that the executing user will be automated process and that limits are the same as a standard synchronous
* transaction (though async actions can be launched from there)
**/

public abstract class VT_R3_AbstractPublishedAction {
    /**
     * @return the class of the concrete subclass
     */
    public abstract Type getType();
    /**
     * Logic to be executed when the action is published
     */
    public abstract void execute();

    /**
     * Call this method to publish the class instance on the event bus. The trigger for VTR3_PublishedAction__e will
     * execute the logic 
     */
    public void publish() {
        EventBus.publish(new VTR3_PublishedAction__e(
            VTR3_ActionJSON__c = JSON.serialize(this),
            VTR3_ActionType__c = getType().getName()
        ));
    }
}