/**
 * Created by Yulia Yakushenkova on 17.04.2020.
 */

public abstract class VT_RemoteCall {

    public static final String METHOD_POST = 'POST';
    public static final String METHOD_GET = 'GET';
    public static final String METHOD_PUT = 'PUT';

    protected String endPointURL;
    protected String httpMethod;
    protected Map<String, String> headersMap = new Map<String, String>();

    protected Boolean successResponse = false;
    protected String errorMessage;

    public abstract Type getType();
    protected abstract String buildRequestBody();
    protected abstract Object parseResponse(String responseBody);

    public Object execute() {
        HttpRequest req = new HttpRequest();
        req.setMethod(httpMethod);
        req.setEndpoint(endPointURL);
        for (String headerName : headersMap.keySet()) {
            req.setHeader(headerName, headersMap.get(headerName));
        }

        try {
            String requestBody = buildRequestBody();
            if (requestBody != null) req.setBody(requestBody);

            Http http = new Http();
            HttpResponse res = http.send(req);

            if (res.getStatusCode() == 200 || res.getStatusCode() == 201) {
                successResponse = true;
            } else {
                errorMessage = res.getStatus();
            }
            System.debug('RESPONSE STATUS: ' + res.getStatusCode() + ' ' + res.getStatus());
            return parseResponse(res.getBody());
        } catch (Exception ex) {
            errorMessage = ex.getMessage() + ' ' + ex.getStackTraceString();
            System.debug('ERROR: ' + errorMessage);
        }
        return null;
    }
}