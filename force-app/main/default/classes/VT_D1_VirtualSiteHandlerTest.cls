/**
 * Created by User on 19/06/04.
 */
@isTest
public with sharing class VT_D1_VirtualSiteHandlerTest {


    public static void onBeforeDeleteTest(){

        Test.startTest();

        Virtual_Site__c virtualSite = [SELECT Id, VTD1_Study_Site_Number__c, VTD1_Study_Team_Member__c FROM Virtual_Site__c WHERE VTD1_Study_Site_Number__c = '12345' LIMIT 1];
        new VT_D1_VirtualSiteHandler().onBeforeDelete(new List<Virtual_Site__c>{virtualSite});
        Test.stopTest();
    }

    public static void onBeforeInsertTest(){
//        Virtual_Site__c virtualSite = [SELECT Id, VTD1_Study_Site_Number__c FROM Virtual_Site__c WHERE VTD1_Study_Site_Number__c = '12345' LIMIT 1];
        Test.startTest();
        
        Virtual_Site__c virtualSite = [SELECT Id, VTD1_Study_Site_Number__c, VTD1_Study_Team_Member__c FROM Virtual_Site__c WHERE VTD1_Study_Site_Number__c = '12345' LIMIT 1];
        new VT_D1_VirtualSiteHandler().onBeforeInsert(new List<Virtual_Site__c>{virtualSite});
        Test.stopTest();
    }

    public static void onBeforeUpdateTest(){
//        Virtual_Site__c virtualSite = [SELECT Id, VTD1_Study_Site_Number__c FROM Virtual_Site__c WHERE VTD1_Study_Site_Number__c = '12345' LIMIT 1];
        Test.startTest();
        
        Virtual_Site__c virtualSite = [
                SELECT Id,
                        VTD1_Study_Site_Number__c,
                        VTD1_Study_Team_Member__c,
                        VTD1_Study__c,
                        VTR2_PI_Change_Complete__c,
                        VTR3_Update_RB__c,
                        VTR2_Backup_PI_STM__c,
                        
            //START RAJESH:SH-17440
                        /*VTD1_Remote_CRA__c,
                        VTR2_Onsite_CRA__c,*/
            //END:SH-17440
                        VTR2_Primary_SCR__c
                FROM Virtual_Site__c
                WHERE VTD1_Study_Site_Number__c = '12345'
                LIMIT 1
        ];
        new VT_D1_VirtualSiteHandler().onBeforeUpdate(new List<Virtual_Site__c>{virtualSite}, new Map<Id,Virtual_Site__c>{virtualSite.Id => virtualSite});
        Test.stopTest();
    }

    @isTest
    private static void onAfterInsertTest(){
        DomainObjects.VirtualSite_t virtualSite = createVirtualSite();
        Test.startTest();
        Virtual_Site__c vs = (Virtual_Site__c) virtualSite.persist();


        Test.stopTest();
    }
    @isTest

    public static void onAfterUpdateTest(){

        DomainObjects.VirtualSite_t virtualSite = createVirtualSite();

        Virtual_Site__c vs = (Virtual_Site__c) virtualSite.persist();
		//RAJESH :SH-17440 removed the CRA fields mapping from the virtual site.
		

        List<Virtual_Site__c> oldVS = [SELECT VTR2_PrimarySiteAddress__c,
                                       VTR2_Backup_PI_STM__c,
                                       VTR2_Primary_SCR__c,
                                       VTD1_Study_Team_Member__c,
                                       VTD1_Is_Active__c
                                        //START RAJESH:SH-17440
                                       /*, VTD1_Remote_CRA__c, VTR2_Onsite_CRA__c*/
                                       //END:SH-17440
                                       FROM Virtual_Site__c WHERE Id = :virtualSite.Id];
        Test.startTest();

        //START RAJESH SH-17440	
        Study_Site_Team_Member__c obj = VT_D1_TestUtils.createStudySiteTeamMember(oldVS[0].ID,oldVS[0].VTD1_Study_Team_Member__c);	
        insert obj;	
        //END:SH-17440

        

        DomainObjects.VTR2_Site_Address_t address = new DomainObjects.VTR2_Site_Address_t()
                .addVirtualSite(virtualSite)
                .setPrimary(true);
        address.persist();
        oldVS[0].VTR2_PrimarySiteAddress__c = address.Id;
        new VT_D1_VirtualSiteHandler().onAfterUpdate(new List<Virtual_Site__c>{oldVS[0]}, new Map<Id,Virtual_Site__c>{vs.Id => vs}, new Map<Id,Virtual_Site__c>{oldVS[0].Id => oldVS[0]});
        Test.stopTest();
    }

    private static DomainObjects.VirtualSite_t createVirtualSite() {

        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setName('Study Name')
                /*.addPMA(new DomainObjects.User_t()
                        .addContact(new DomainObjects.Contact_t())
                        .setProfile('Site Coordinator'))*/
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));

        DomainObjects.StudyTeamMember_t stmOnsiteCra = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA')
                )
                .addStudy(study);
        stmOnsiteCra.persist();

        DomainObjects.StudyTeamMember_t stmRemoteCra = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA'))
                .addStudy(study);
        stmRemoteCra.persist();
	
        //RAJESH :SH-17440 removed the CRA fields mapping from the virtual site.
        DomainObjects.StudyTeamMember_t stm = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('SCR')
                .addUser(new DomainObjects.User_t()
                        .addContact(new DomainObjects.Contact_t())
                        .setProfile('Site Coordinator'))

            //START RAJESH SH-17440
                //.setOnsiteCraId(stmOnsiteCra.id)
                //.setRemoteCraId(stmRemoteCra.id)
            //END:SH-17440

                .addStudy(study);

        return new DomainObjects.VirtualSite_t()
                .addStudyTeamMember(stm)
                .addStudy(study)
                .setStudySiteNumber('12345');

    }

    // Jira Ref: SH-17543 Akanksha Singh
    // To test 'CRA queue' field modification and Queue creation on Virtual Site Insertion. 
    @isTest
    public static void testVirtualSiteInsertion()
    {
        //Create Virtual Site
        Test.startTest();
        DomainObjects.VirtualSite_t virtualSite = createVirtualSite();
        Virtual_Site__c vs = (Virtual_Site__c) virtualSite.persist();

        //fetch CRA Queue field from Virtual Site after insertion
        List<Virtual_Site__c> oldVS = [SELECT Id,
                                       VTR5_CRA_Queue__c
                                       FROM Virtual_Site__c 
                                       WHERE Id = :virtualSite.Id];
        
        //Fetch Group Id having expected CRA Queue Name of Virtual Site
        String craQueuename = oldVS[0].Id + '_CRAQueue';
        Id testGroupId = [SELECT Id,
                                Name
                          FROM Group
                          WHERE Name =: craQueuename].Id;
        Test.stopTest();

        //Compare 'CRA Queue' field value with Expected Group Id
        System.assertequals(testGroupId,oldVS[0].VTR5_CRA_Queue__c);
        
    }
}