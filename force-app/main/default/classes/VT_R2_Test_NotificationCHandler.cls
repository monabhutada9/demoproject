/**
 * Created by MPlatonov on 07.02.2019.
 */

@isTest
private class VT_R2_Test_NotificationCHandler {
    private static final String VTR2_eDiarySMSOptOutCode = '333';
    private static final String VTR2_NewMessagesSMSOptOutCode = '444';
    private static final String VTR2_VisitRemindersSMSOptOutCode = '555';

    @testSetup
    static void setup() {
        Test.startTest();
        System.debug('setup begin');
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.stopTest();
        study = [
                select VTR2_Send_eDiary_Notifications__c, VTR2_Send_New_Messages__c, VDT2_Send_Visit_Reminders__c,
                        VTR2_eDiarySMSOptOutCode__c, VTR2_NewMessagesSMSOptOutCode__c, VTR2_VisitRemindersSMSOptOutCode__c, Name
                from HealthCloudGA__CarePlanTemplate__c
                where Id = :study.Id
        ];
        study.VTR2_Send_eDiary_Notifications__c = true;
        study.VTR2_Send_New_Messages__c = true;
        study.VDT2_Send_Visit_Reminders__c = true;
        study.VTD1_Patient_Community_URL__c = 'https://intcopy-iqviavirtualtrials.cs47.force.com/patient';
        study.VTR2_eDiarySMSOptOutCode__c = VTR2_eDiarySMSOptOutCode;
        study.VTR2_NewMessagesSMSOptOutCode__c = VTR2_NewMessagesSMSOptOutCode;
        study.VTR2_VisitRemindersSMSOptOutCode__c = VTR2_VisitRemindersSMSOptOutCode;
        update study;
        VT_D1_TestUtils.createTestPatientCandidate(1);
    }
    @isTest
    public static void test() {
        VT_R3_GlobalSharing.disableForTest = true;
        setUpMCSettings();
        System.debug('test begin');
        Case caseObj = [
                select Id, VTD1_Patient_User__c, VTD1_PI_user__c, ContactEmail,
                        VTR2_Receive_SMS_eDiary_Notifications__c, VTR2_Receive_SMS_Visit_Reminders__c, VTR2_SiteCoordinator__c
                from Case
        ];
        caseObj.VTR2_Receive_SMS_eDiary_Notifications__c = true;
        caseObj.VTR2_Receive_SMS_Visit_Reminders__c = true;
        update caseObj;

        List <Study_Team_Member__c> studyTeamMembers = [select Id, Name, RecordType.Name, Study__r.Name, User__c from Study_Team_Member__c];
        System.debug('studyTeamMembers = ' + studyTeamMembers);
        for (Study_Team_Member__c studyTeamMember : studyTeamMembers) {
            System.debug('stm = ' + studyTeamMember.Name + ' ' + studyTeamMember.RecordType.Name + ' ' + studyTeamMember.Study__r.Name + ' ' + studyTeamMember.User__c);
        }
        Test.startTest();
        List <Account> accounts = [select Id, Name from Account where Name = 'CP1 CP1L'];
        for (Account account : accounts) {
            System.debug('acc = ' + account);
        }
        Id accountId = accounts[0].Id;
        String cgUserName = 'CG_' + accounts[0].Name;

        Id rtId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId();

        System.debug('creating contact for account = ' + accountId);
        String patientEmail = '' + caseObj.ContactEmail;
        String cgEmail = patientEmail.substring(0, patientEmail.length() - 10) + '0' + patientEmail.substring(patientEmail.length() - 10);
        Contact contact = new Contact(FirstName = cgUserName, VTD1_EmailNotificationsFromCourier__c = true,
                LastName = 'L', Email = cgEmail, RecordTypeId = rtId, AccountId = accountId, VTD1_Clinical_Study_Membership__c = caseObj.Id, VTD1_Primary_CG__c = true);
        insert contact;
        System.debug('contact = ' + contact);

        Profile profile = [select Id from Profile where Name = 'Caregiver'];

        User cgUser = new User(
                ProfileId = profile.Id,
                FirstName = cgUserName,
                LastName = 'L',
                Email = cgEmail,
                Username = VT_D1_TestUtils.generateUniqueUserName(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/New_York',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                IsActive = true,
                ContactId = contact.Id
        );
        insert cgUser;

        User user = [select Id from User where FirstName = 'SiteCoordinator1'];
        caseObj.VTR2_SiteCoordinator__c = user.Id;
        update caseObj;

        System.debug('caseObj = ' + caseObj);

        List <Id> userIds = new List<Id>{
                caseObj.VTD1_PI_user__c, caseObj.VTR2_SiteCoordinator__c
        };

        List <Contact> contacts = [
                select VTR2_Receive_SMS_New_Message__c, AccountId
                from Contact
                where Id in (select ContactId from User where Id in :userIds)
        ];
        for (Contact c : contacts) {
            c.VTR2_Receive_SMS_New_Message__c = true;
        }
        update contacts;

        List <VT_D1_Phone__c> phones = new List<VT_D1_Phone__c>();
        phones.add(new VT_D1_Phone__c(VTD1_Contact__c = contact.Id, Account__c = accountId, PhoneNumber__c = '(555) 789-3215', Type__c = 'Mobile'));
        phones.add(new VT_D1_Phone__c(VTD1_Contact__c = contacts[0].Id, Account__c = contacts[0].AccountId, PhoneNumber__c = '(555) 789-3216', Type__c = 'Mobile'));
        phones.add(new VT_D1_Phone__c(VTD1_Contact__c = contacts[1].Id, Account__c = contacts[1].AccountId, PhoneNumber__c = '(555) 789-3222', Type__c = 'Mobile'));
        insert phones;

        phones = [select Id, PhoneNumber__c, VTR2_OptInFlag__c, VTR2_SMS_Opt_In_Status__c from VT_D1_Phone__c];
        for (VT_D1_Phone__c phone : phones) {
            phone.VTR2_OptInFlag__c = true;
        }


        Test.stopTest();
        update phones;
        phones = [select Id, PhoneNumber__c, VTR2_OptInFlag__c, VTR2_SMS_Opt_In_Status__c from VT_D1_Phone__c];
        System.debug('phones after insert = ' + phones);

        List <VTR2_InboundSMS__c> inboundSMS = new List<VTR2_InboundSMS__c>();
        for (VT_D1_Phone__c phone : phones) {
            inboundSMS.add(new VTR2_InboundSMS__c(VTR2_MessageText__c = 'YES', VTR2_PhoneNumber__c = phone.PhoneNumber__c));
        }

        insert inboundSMS;

        List <VTD1_NotificationC__c> notifications = new List<VTD1_NotificationC__c>();
        VTD1_NotificationC__c notification = new VTD1_NotificationC__c(
                Title__c = 'eDiary',
                Message__c = 'Test message 1',
                OwnerId = caseObj.VTD1_Patient_User__c,
                VTD1_Receivers__c = caseObj.VTD1_Patient_User__c
        );
        notification = new VTD1_NotificationC__c(
                Title__c = 'visit',
                Message__c = 'Test message 2',
                OwnerId = caseObj.VTD1_Patient_User__c,
                VTD1_Receivers__c = caseObj.VTD1_Patient_User__c
        );
        notifications.add(notification);
        notification = new VTD1_NotificationC__c(
                Title__c = 'message',
                Message__c = 'Test message 3',
                OwnerId = caseObj.VTD1_Patient_User__c,
                VTD1_Receivers__c = caseObj.VTD1_Patient_User__c
        );
        notifications.add(notification);
        notification = new VTD1_NotificationC__c(
                Title__c = 'eDiary',
                Message__c = 'Test message 4',
                OwnerId = cgUser.Id,
                VTD1_Receivers__c = cgUser.Id
        );
        notifications.add(notification);
        notification = new VTD1_NotificationC__c(
                Title__c = 'message',
                Message__c = 'Test message 5',
                OwnerId = caseObj.VTD1_PI_user__c,
                VTD1_Receivers__c = caseObj.VTD1_PI_user__c,
                VTD1_CSM__c = caseObj.Id
        );
        notifications.add(notification);

        notification = new VTD1_NotificationC__c(
                Title__c = 'message',
                Message__c = 'Test message 6',
                OwnerId = caseObj.VTR2_SiteCoordinator__c,
                VTD1_Receivers__c = caseObj.VTR2_SiteCoordinator__c,
                VTD1_CSM__c = caseObj.Id
        );
        notifications.add(notification);
        notification = new VTD1_NotificationC__c(
                VTR3_Notification_Type__c = 'eCoa Notification',
                Title__c = 'message',
                Message__c = 'Test message 6',
                OwnerId = caseObj.VTR2_SiteCoordinator__c,
                VTD1_Receivers__c = caseObj.VTR2_SiteCoordinator__c,
                VTD1_CSM__c = caseObj.Id
        );
        notifications.add(notification);

        VT_Stubber.applyStub(
                'VT_R2_NotificationCHandler.receiversMap',
                new Map<Id, User>([
                        SELECT Id,
                                IsActive,
                                Profile.Name,
                                ContactId,
                                Contact.VTD2_Send_notifications_as_emails__c,
                                Contact.VTD1_Clinical_Study_Membership__c,
                                Contact.VTD1_Clinical_Study_Membership__r.Status,
                                VTR3_Verified_Mobile_Devices__c
                        FROM User
                        ORDER BY CreatedDate DESC LIMIT 50
                ])
        );

        insert notifications;

        notification = notifications[1];
        notification.Number_of_unread_messages__c = 3;
        update notification;
        notification.Number_of_unread_messages__c = 4;
        update notification;
    }

    static void setUpMCSettings() {
        List<VTR2_McloudSettings__mdt> mcloudSettings = new List<VTR2_McloudSettings__mdt>();
        mcloudSettings.add(new VTR2_McloudSettings__mdt(VTR2_OutboundMessageKey_API_Triggered__c = 'NjU6Nzg6MA',
                VTR2_RestURI__c = 'https://mc2gwv28hdsr86gd-sldnjzvzzgm.rest.marketingcloudapis.com/',
                VTR2_ShortCode__c = '99808'));
        VT_R2_McloudBroadcaster.setMCsettings(mcloudSettings);
    }
}