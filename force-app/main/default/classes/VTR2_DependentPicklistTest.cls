/**
 * Created by Danylo Belei on 13.05.2019.
 */

@IsTest
public with sharing class VTR2_DependentPicklistTest {

    public static void testBehavior() {
        String objectName = 'Case';
        String fieldName = 'Status';
        String dependentpicklistName = 'VTD1_Reason_Code__c';

        Test.startTest();
        //VTR2_DependentPicklist picklist = new VTR2_DependentPicklist();
        VTR2_DependentPicklist.Picklist newPicklist = VTR2_DependentPicklist.getDependentPicklist(objectName, fieldName, dependentpicklistName);

        Test.stopTest();

        System.assertEquals(getPickListValuesIntoList().size(), newPicklist.options.size());
    }

    private static List<String> getPickListValuesIntoList() {
        List<String> pickListValuesList = new List<String>();
        Schema.DescribeFieldResult fieldResult = Case.Status.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry pickListVal : ple) {
            pickListValuesList.add(pickListVal.getLabel());
        }
        return pickListValuesList;

    }
}