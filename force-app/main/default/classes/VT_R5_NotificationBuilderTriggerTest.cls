/**
 * Created by Dmitry Kovalev on 21.08.2020.
 * Release 5.1 Sprint 1
 */

@IsTest
public class VT_R5_NotificationBuilderTriggerTest {
    private final static String EXPIRATION_ALERT = 'Expiration Alert';
    private final static String DAILY_ALERT = 'Daily Alert';
    private final static String OPERATION_TYPE = 'BEFORE the Expiration Date';
    private final static String RECIPIENT_SITE_STAFF = 'Site Staff';
    private final static String RECIPIENT_PT_CG = 'Patient/Caregiver';

    @IsTest
    public static void onAfterInsertTest() {
        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c();
        insert study;

        List<VTR5_eDiaryNotificationBuilder__c> builders = new List<VTR5_eDiaryNotificationBuilder__c> {
            createBuilder(DAILY_ALERT, study.Id, RECIPIENT_SITE_STAFF),
            createBuilder(DAILY_ALERT, study.Id, RECIPIENT_PT_CG),
            createBuilder(EXPIRATION_ALERT, study.Id, RECIPIENT_SITE_STAFF),
            createBuilder(EXPIRATION_ALERT, study.Id, RECIPIENT_PT_CG)
        };
        insert builders;

        builders[0].VTR5_RecipientType__c = RECIPIENT_PT_CG;
        builders[1].VTR5_RecipientType__c = RECIPIENT_SITE_STAFF;
        builders[2].VTR5_eDiaryName__c = 'Test';
        update builders;
    }

    private static VTR5_eDiaryNotificationBuilder__c createBuilder(String recordType, Id studyId, String recipientType) {
        VTR5_eDiaryNotificationBuilder__c builder = new VTR5_eDiaryNotificationBuilder__c();
        builder.VTR5_eDiaryName__c = 'Test eDiary';
        builder.VTR5_Study__c = studyId;
        builder.VTR5_RecipientType__c = recipientType;
        if ((new Set<String> { EXPIRATION_ALERT, DAILY_ALERT }).contains(recordType)) {
            builder.RecordTypeId = Schema.SObjectType.VTR5_eDiaryNotificationBuilder__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
            if (recordType == EXPIRATION_ALERT) {
                builder.VTR5_OperationType__c = OPERATION_TYPE;
            }
        }

        // FIXME: error: INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST, bad value for restricted picklist field: Patient/Caregiver
        /*if (recipientType == RECIPIENT_SITE_STAFF) {
            builder.VTR5_RecipientType__c = recipientType;
        }*/
        return builder;
    }
}