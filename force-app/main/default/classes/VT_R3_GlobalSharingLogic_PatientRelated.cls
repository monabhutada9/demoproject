/**
* @author: Carl Judge
* @date: 27-Aug-19
* @description: Calculates sharing details for patient cases and any related object that has the same sharing logic.
* Can be extended for objects that have the same logic with some small differences
**/

public abstract without sharing class VT_R3_GlobalSharingLogic_PatientRelated extends VT_R3_AbstractGlobalSharingLogic {
    /* Profiles MUST be in this set to get direct (non group) sharing to patients */
    public static final Set<String> PATIENT_SHARE_PROFILES = new Set<String> {
        VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME
    };

    protected Set<Id> carePlanIds;
    protected Map<Id, Set<Id>> recordToCaseIdsMap = new Map<Id, Set<Id>>(); // normally this is a 1:1 relationship but Accounts can be related to more than 1 Case
    protected List<Case> cases = new List<Case>();
    protected Map<Id, Set<Id>> patientToCaregiverIds = new Map<Id, Set<Id>>();
    protected Set<Id> cgPatientIds = new Set<Id>();
    protected Map<Id, List<ShareDetail>> shareDetailsByCaseId = new Map<Id, List<ShareDetail>>();

    public void setCarePlanIds(Set<Id> carePlanIds) {
        this.carePlanIds = carePlanIds;
    }

    public override void executeLogic() {
        if (carePlanIds != null) {
            Set<Id> recIdsFromCarePlans = getRecordIdsFromCarePlanIds(carePlanIds);
            init(recIdsFromCarePlans, includedUserIds, scopeRecordIds, removeOnly);
        }
        super.executeLogic();
    }

    protected virtual override void addShareDetailsForRecords() {
        fillRecordToCasesIdMap();
        List<Id> caseIds = new List<Id>();
        for (Set<Id> caseIdSet : recordToCaseIdsMap.values()) {
            caseIds.addAll(caseIdSet);
        }
        fillShareDetailsByCaseId(caseIds);
        addShareDetailsForRelatedRecords();
    }

    protected override Iterable<Object> getUserBatchIterable() {
        return (Iterable<Object>) Database.getQueryLocator(getUserQuery());
    }

    protected override void doUserQueryPrep() {
        // sort the userIds by profile
        groupUserIdsByProfile(PATIENT_SHARE_PROFILES);
        // if we have users from profiles that need patient sharing...
        if (!profileToUserIds.isEmpty()) {
            // if we have caregivers, get their patient IDs
            if (profileToUserIds.containsKey(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME)) {
                getPatientIdsForCGs();
            }
        }
    }

    protected override List<SObject> executeUserQuery() { 
        if (!profileToUserIds.isEmpty()) {
            String query = getUserQuery() + ' LIMIT ' + getAdjustedQueryLimit();
            return Database.query(query);
        } else {
            return null;
        }
    }

    protected abstract String getUserQuery();

    /**
    * Given a list of case IDs and custom metadata for a certain object, record all IDs for this object which are related
    * the case IDs
    *
    * @param caseIds - Set of case IDs
    * @return Set<Id> of record IDs
    */
    @TestVisible
    protected abstract Set<Id> getRecordIdsFromCarePlanIds(Set<Id> caseIds);

    /**
     * Calculate all ShareDetails for some records
     * @param caseIds - List of case IDs
     */
    protected void fillShareDetailsByCaseId(List<Id> caseIds) {
        queryCases(caseIds);
        getCaregiversForPatients();
        getShareDetailsByCaseId();
    }

    /**
     * Map record IDs by CaseID
     */
    protected abstract void fillRecordToCasesIdMap();

    protected void addToRecordToCaseIdsMap(Id recId, Id caseId) {
        if (!recordToCaseIdsMap.containsKey(recId)) {
            recordToCaseIdsMap.put(recId, new Set<Id>());
        }
        recordToCaseIdsMap.get(recId).add(caseId);
    }

    protected void queryCases(List<Id> caseIds) {
        cases = [
            SELECT Id,
                VTD1_Study__c,
                VTD1_Patient_User__c,
                VTD1_Virtual_Site__c,
                VTD1_Patient_User__r.Contact.AccountId,
                VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c,
                VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c
            FROM Case
            WHERE Id IN :caseIds
        ];
    }

    protected void getCaregiversForPatients() {
        List<Id> patientIds = new List<Id>();
        for (Case cas : cases) {
            if (cas.VTD1_Patient_User__c != null) {
                patientIds.add(cas.VTD1_Patient_User__c);
            }
        }
        for (User caregiver : [
            SELECT Id, Contact.AccountId, Contact.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c
            FROM User
            WHERE Profile.Name = 'Caregiver'
            AND Contact.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c IN :patientIds
        ]) {
            if (!patientToCaregiverIds.containsKey(caregiver.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c)) {
                patientToCaregiverIds.put(caregiver.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c, new Set<Id>());
            }
            patientToCaregiverIds.get(caregiver.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c).add(caregiver.Id);
        }
        cgPatientIds = patientToCaregiverIds.keySet();
    }

    protected virtual void getShareDetailsByCaseId() {
        for (Case cas : cases) {
            addCaseShareDetail(cas, cas.VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c);
            addCaseShareDetail(cas, cas.VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c);
            if (patientToCaregiverIds.containsKey(cas.VTD1_Patient_User__c)) {
                for (Id cgId : patientToCaregiverIds.get(cas.VTD1_Patient_User__c)) {
                    addCaseShareDetail(cas, cgId);
                }
            }
        }
    }

    protected void addCaseShareDetail(Case cas, Id userOrGroupId) {
        if (userOrGroupId != null) {
            ShareDetail detail = new ShareDetail();
            detail.userOrGroupId = userOrGroupId;
            detail.studyId = cas.VTD1_Study__c;
            detail.accessLevel = 'Edit';
            if (!shareDetailsByCaseId.containsKey(cas.Id)) {
                shareDetailsByCaseId.put(cas.Id, new List<ShareDetail>());
            }
            shareDetailsByCaseId.get(cas.Id).add(detail);
        }
    }

    protected void addShareDetailsForRelatedRecords() {
        for (Id recId : recordToCaseIdsMap.keySet()) {
            for (Id caseId : recordToCaseIdsMap.get(recId)) {
                if (shareDetailsByCaseId.containsKey(caseId)) {
                    for (ShareDetail detail : shareDetailsByCaseId.get(caseId)) {
                        addShareDetail(recId, detail.userOrGroupId, detail.studyId, detail.accessLevel);
                    }
                }
            }
        }
    }

    private void getPatientIdsForCGs() {
        for (User usr : [
            SELECT Id, Contact.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c
            FROM User
            WHERE Id IN :profileToUserIds.get(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME)
        ]) {
            Id ptId = usr.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c;
            if (!patientToCaregiverIds.containsKey(ptId)) {
                patientToCaregiverIds.put(ptId, new Set<Id>());
            }
            patientToCaregiverIds.get(ptId).add(usr.Id);
        }
        cgPatientIds = patientToCaregiverIds.keySet();
    }

    protected void addUserShareDetailsForCase(Id recId, Case cas) {
        if (patientIds.contains(cas.VTD1_Patient_User__c)) {
            addShareDetail(recId, cas.VTD1_Patient_User__c, cas.VTD1_Study__c, 'Edit');
        }
        if (patientToCaregiverIds.containsKey(cas.VTD1_Patient_User__c)) {
            for (Id cgId : patientToCaregiverIds.get(cas.VTD1_Patient_User__c)) {
                addShareDetail(recId, cgId, cas.VTD1_Study__c, 'Edit');
            }
        }
    }

    protected virtual override void reset() {
        super.reset();
        carePlanIds = null;
        recordToCaseIdsMap.clear();
        cases.clear();
        patientToCaregiverIds.clear();
        cgPatientIds.clear();
        shareDetailsByCaseId.clear();
    }
}