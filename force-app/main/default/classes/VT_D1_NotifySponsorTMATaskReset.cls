global class VT_D1_NotifySponsorTMATaskReset implements Database.Batchable<SObject>, Database.AllowsCallouts {
    //FR–PD–251	Study Hub will send daily reminder notifications to the Project Lead 
    //until the Notify Sponsor/TMA task has been marked as completed	

    global final String query;

    global VT_D1_NotifySponsorTMATaskReset(String q) {
        query = q;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Task> scope) {
        /*    List<Task> lstTsk = new List<Task>();

                for(sobject s : scope){
                    Task tsk = (Task)s;
                   tsk.IsReminderSet = true;
                   tsk.ReminderDateTime = System.now();
                   lstTsk.add(tsk);
                }
                update lstTsk;*/


        List<VT_R3_CustomNotification.NotificationWrapper> notificationList = new List<VT_R3_CustomNotification.NotificationWrapper>();


        Set<Id> protocolDeviationIds = new Set<Id>();

        for (Task t : scope) {
            protocolDeviationIds.add(t.WhatId);
        }

        System.debug(protocolDeviationIds);
        Map<Id, VTD1_Protocol_Deviation__c> pdMap = new Map<Id, VTD1_Protocol_Deviation__c>([SELECT Id, Name, VTD1_Primary_Investigator_ID__r.Name, VTD1_StudyId__r.Name, VTD1_Site_Subject_Level__c, VTD2_Subject_ID__c  FROM VTD1_Protocol_Deviation__c WHERE Id IN :protocolDeviationIds]);

        for (Task tsk : scope) {
            if (pdMap.containsKey(tsk.WhatId)) {
                VTD1_Protocol_Deviation__c pd = pdMap.get(tsk.WhatId);
                String pdName = pd.Name;
                String targetName = pd.VTD1_Primary_Investigator_ID__c != null ? pd.VTD1_Primary_Investigator_ID__r.Name : 'Primary Investigator';
                String studyNickname = pd.VTD1_StudyId__c != null ? pd.VTD1_StudyId__r.Name + ' ' : '';
                String selectedLevel = pd.VTD1_Site_Subject_Level__c != null ? pd.VTD1_Site_Subject_Level__c : '';

                if (pd.VTD1_Site_Subject_Level__c == 'Site') {
                    selectedLevel = 'site';
                } else if (pd.VTD1_Site_Subject_Level__c == 'Subject') {
                    selectedLevel = 'patient';
                    targetName = pd.VTD2_Subject_ID__c != null ? pd.VTD2_Subject_ID__c : '';
                }

                notificationList.add(new VT_R3_CustomNotification.NotificationWrapper(
                        new List<Id>{
                                tsk.OwnerId
                        },
                        'Protocol Deviation',
                        'The \'Notify Sponsor\' task has been open for ' + tsk.CreatedDate.date().daysBetween(System.today()) +
                                ' days. Please notify Sponsor of ' + pdName +
                                ', a ' + selectedLevel + ' level protocol deviation for ' + targetName +
                                ' on The ' + studyNickname + 'Study, and close the task.',
                        tsk.Id
                ));
            }

        }
        if (!Test.isRunningTest()) {
            VT_R3_CustomNotification.sendCustomNotifications(notificationList);
        }

    }


    global void finish(Database.BatchableContext BC) {
    }

}