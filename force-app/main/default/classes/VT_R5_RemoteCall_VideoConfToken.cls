/**
 * Created by Yuliya Yakushenkova on 10/6/2020.
 */

public with sharing class VT_R5_RemoteCall_VideoConfToken extends VT_RemoteCall {

    private String sessionId;

    public VT_R5_RemoteCall_VideoConfToken(String sessionId) {
        endPointURL = ExternalVideoController.getHerokuAppURL() + '/api/token';
        httpMethod = METHOD_POST;
        this.sessionId = sessionId;
    }

    public override Type getType() {
        return VT_R5_RemoteCall_VideoConfToken.class;
    }

    protected override String buildRequestBody() {
        return JSON.serialize(new RequestData(sessionId));
    }

    protected override Object parseResponse(String responseBody) {
        return (Token) JSON.deserialize(responseBody, Token.class);
    }

    public class Token {
        public String message;

        public String getMessage() {
            return message;
        }
    }

    private class RequestData {
        public String sessionId;

        private RequestData(String sessionId) {
            this.sessionId = sessionId;
        }
    }
}