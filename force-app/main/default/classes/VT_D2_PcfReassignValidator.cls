/**
* @author: Carl Judge
* @date: 03-Dec-18
* @description: Validate a change of PCF ownership by certain profiles
**/

public without sharing class VT_D2_PcfReassignValidator {
    
    public static final Map<Id, List<String>> VALID_PARENT_FIELDS_BY_PROFILE = new Map<Id, List<String>>{
        VTD1_RTId__c.getInstance().VTD2_Profile_SC__c => new List<String>{
            'VTD1_Primary_PG__c',
            'VTD1_Secondary_PG__c',
            'VTR2_SiteCoordinator__c'
        },
        VTD1_RTId__c.getInstance().VTD2_Profile_PG__c => new List<String>{
            'VTD1_Primary_PG__c',
            'VTD1_Secondary_PG__c',
            'VTD1_PI_user__c',
            'VTR2_SiteCoordinator__c'
        },
        VTD1_RTId__c.getInstance().VTD2_Profile_PI__c => new List<String>{
            'VTD1_Primary_PG__c',
            'VTD1_Secondary_PG__c',
            'VTR2_SiteCoordinator__c'
        },
        VTD1_RTId__c.getInstance().VTD2_Profile_SCR__c => new List<String>{
            'VTD1_Primary_PG__c',
            'VTD1_Secondary_PG__c',
            'VTD1_PI_user__c'
        }
    };
	
    private Id currentUserProfileId = UserInfo.getProfileId();
    private List<Case> casesToValidate = new List<Case>();
    private Map<Id, Id> studyToScQueueIdMap = new Map<Id, Id>();
    private Map<Id, Case> parentMap = new Map<Id, Case>();
    private Map<Id, Set<Id>> groupMemberMap = new Map<Id, Set<Id>>();

    public void validate(List<Case> cases, Map<Id, Case> oldMap) {
        getCasesToValidate(cases, oldMap);

        system.debug(this.casesToValidate);
        if (! this.casesToValidate.isEmpty()) {
            getParentMap();
            getGroupMemberMap();

            system.debug(this.parentMap);
            system.debug(this.groupMemberMap);

            for (Case caseToValidate : this.casesToValidate) {
                system.debug(ownerIsStudyPoolOrMember(caseToValidate));
                if (! ownerIsStudyPoolOrMember(caseToValidate)) {
                    Boolean valid = false;
                    for (String field : VALID_PARENT_FIELDS_BY_PROFILE.get(this.currentUserProfileId)) {
                        if (ownerMatchesValidField(caseToValidate, field)) {
                            valid = true;
                            break;
                        }
                    }

                    if (! valid) {
                        caseToValidate.addError(Label.VT_D1ReassingErrorGeneral);
                    }
                }
            }
        }
    }

    private void getCasesToValidate(List<Case> cases, Map<Id, Case> oldMap){
        system.debug('GETTING CASES TO VALIDATE');

        Set<Id> pcfRecTypeIds = new Set<Id>{
            Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VTD1_PCF').getRecordTypeId(),
            Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VTD1_PCF_Read_Only').getRecordTypeId()
        };


        for (Case item : cases) {
            system.debug(item.VTD1_PCF_Safety_Concern_Indicator__c == 'No');
            system.debug(pcfRecTypeIds.contains(item.RecordTypeId));
            system.debug(item.RecordTypeId);
            system.debug(VALID_PARENT_FIELDS_BY_PROFILE);
            system.debug(this.currentUserProfileId);
            system.debug(VALID_PARENT_FIELDS_BY_PROFILE.containsKey(this.currentUserProfileId));
            system.debug(item.OwnerId != oldMap.get(item.Id).OwnerId);

            if (item.VTD1_PCF_Safety_Concern_Indicator__c == 'No' &&
                pcfRecTypeIds.contains(item.RecordTypeId) &&
                VALID_PARENT_FIELDS_BY_PROFILE.containsKey(this.currentUserProfileId) &&
                item.OwnerId != oldMap.get(item.Id).OwnerId)
            {
                this.casesToValidate.add(item);
            }
        }
    }

    private void getParentMap() {
        List<Id> parentIds = new List<Id>();
        for (Case item : this.casesToValidate) {
            if (item.VTD1_Clinical_Study_Membership__c != null) {
                parentIds.add(item.VTD1_Clinical_Study_Membership__c);
            }
        }

        String query = 'SELECT Id, VTD1_Study__c, VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c, ' +
            String.join(VALID_PARENT_FIELDS_BY_PROFILE.get(this.currentUserProfileId), ',') +
            ' FROM Case' +
            ' WHERE Id IN :parentIds';

        for (Case item : Database.query(query)) {
            if (item.VTD1_Study__c != null &&
                item.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c != null)
            {
                this.studyToScQueueIdMap.put(item.VTD1_Study__c, item.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c);
            }

            this.parentMap.put(item.Id, item);
        }
    }

    private void getGroupMemberMap() {
        List<Id> userIds = new List<Id>();
        for (Case item : this.casesToValidate) {
            userIds.add(item.OwnerId);
        }

        if (! this.studyToScQueueIdMap.isEmpty()) {
            for (GroupMember item : [
                SELECT GroupId, UserOrGroupId
                FROM GroupMember
                WHERE GroupId IN :this.studyToScQueueIdMap.values()
                AND UserOrGroupId IN :userIds
            ]) {
                if (! this.groupMemberMap.containsKey(item.GroupId)) {
                    this.groupMemberMap.put(item.GroupId, new Set<Id>());
                }
                this.groupMemberMap.get(item.GroupId).add(item.UserOrGroupId);
            }
        }
    }

    private Boolean ownerIsStudyPoolOrMember(Case cas) {
        Boolean ownerIsStudyPoolOrMember = false;

        Case parent = this.parentMap.get(cas.VTD1_Clinical_Study_Membership__c);
        if (parent != null) {
            Id groupId = this.studyToScQueueIdMap.get(parent.VTD1_Study__c);
            
            ownerIsStudyPoolOrMember = groupId == cas.OwnerId || (this.groupMemberMap.containsKey(groupId) && this.groupMemberMap.get(groupId).contains(cas.OwnerId));
        }

        return ownerIsStudyPoolOrMember;
    }

    private Boolean ownerMatchesValidField(Case cas, String field) {
        Boolean ownerMatchesValidField = false;

        Case parent = this.parentMap.get(cas.VTD1_Clinical_Study_Membership__c);
        system.debug(parent);
        system.debug(cas.ownerId);
        if (parent != null) {
            system.debug(parent.get(field));
            ownerMatchesValidField = (Id)parent.get(field) == cas.OwnerId;
        }
        return ownerMatchesValidField;
    }
}