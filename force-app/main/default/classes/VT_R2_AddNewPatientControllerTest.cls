/**
 * @author Ruslan Mullayanov
 * @description Tests for covering VT_R2_AddNewPatientController
 */
@IsTest
public with sharing class VT_R2_AddNewPatientControllerTest {
    private static Id piUserId;
    private static Id virtualSiteId;
    private static Id studyId;

    static {
        List<Case> cases = [SELECT VTD1_PI_user__c FROM Case LIMIT 1];
        if (!cases.isEmpty()) {
            piUserId = cases[0].VTD1_PI_user__c;
        }
        List<Virtual_Site__c> virtualSites = [SELECT Id FROM Virtual_Site__c LIMIT 1];
        if (!virtualSites.isEmpty()) {
            virtualSiteId = virtualSites[0].Id;
        }
        List<HealthCloudGA__CarePlanTemplate__c> studyList = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        if (!studyList.isEmpty()) {
            studyId = studyList[0].Id;
        }
    }

    /*******************************************************************************************************
    * @author                   Akanksha Singh (SH-19460)
    * @description              Test setup to get the Primary PI and Studies associated to the logged in Primaru PI User, Backup PI and Sub-I user.
    */
    @TestSetup
    static void testSetup() {


		Test.startTest();


        //Sharing records should be created for test data
        VT_R3_GlobalSharing.disableForTest = true;
        
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        User SCRUser = VT_D1_TestUtils.createUserByProfile('Site Coordinator', 'SCRTestUser');
        User primaryPiUser = VT_D1_TestUtils.createUserByProfile('Primary Investigator', 'PiTestUser');
        User backupPiUser = VT_D1_TestUtils.createUserByProfile('Primary Investigator', 'BackupPiTestUser');
        User subIPiUser = VT_D1_TestUtils.createUserByProfile('Primary Investigator', 'subIPiTestUser');
        VT_D1_TestUtils.persistUsers();
        
        // Created Virtual Site
        Virtual_Site__c virtualSite = new Virtual_Site__c(Name = 'TestSite', VTD1_Study__c= study.Id,VTD1_Study_Site_Number__c = '1');
        insert virtualSite;
        
        // Created list of STM to reduce multiple insert operation
        List<Study_Team_Member__c> stmList = new List<Study_Team_Member__c>();
        Study_Team_Member__c stmPrimaryPI = new Study_Team_Member__c(User__c = primaryPiUser.Id, Study__c = study.Id,VTD1_VirtualSite__c = virtualSite.Id);
        insert stmPrimaryPI;
        Study_Team_Member__c stmSCR = new Study_Team_Member__c(User__c = SCRUser.Id, Study__c = study.Id);
        stmList.add(stmSCR);
        Study_Team_Member__c stmBackupPi = new Study_Team_Member__c(User__c = backupPiUser.Id, Study__c = study.Id,Study_Team_Member__c = stmPrimaryPI.Id,VTD1_VirtualSite__c = virtualSite.Id);
        stmList.add(stmBackupPi);
        Study_Team_Member__c stmSubIPi = new Study_Team_Member__c(User__c = subIPiUser.Id, Study__c = study.Id);
        stmList.add(stmSubIPi);
        insert stmList;
        List<HealthCloudGA__CarePlanTemplate__Share> shareRec = new List<HealthCloudGA__CarePlanTemplate__Share>();
        
        //As share records won't be created automatically in test classes, need to insert share record manually.
        for(Study_Team_Member__c stm : stmList){
        HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
        objVirtualShare.ParentId = virtualSite.VTD1_Study__c;
        objVirtualShare.UserOrGroupId = stm.User__c;
        objVirtualShare.AccessLevel = 'Edit';
        shareRec.add(objVirtualShare);
        }
        insert shareRec;
        
        List<Study_Site_Team_Member__c> sstmList = new List<Study_Site_Team_Member__c>();
        sstmList.add(new Study_Site_Team_Member__c(VTR2_Associated_PI__c = stmPrimaryPI.Id, 
                                                    VTR2_Associated_SCr__c = stmSCR.Id,
                                                    VTR2_Associated_PI3__c =  stmPrimaryPI.Id,
                                                    VTR2_Associated_SubI__c = stmSubIPi.Id,
                                                    VTD1_SiteID__c = virtualSite.Id,
                                                    VTR2_SCr_type__c = 'Primary'));
        insert sstmList;
        


        Test.stopTest();


    }
    // Jira Ref: SH-19460 End

    // Jira Ref: SH-19460 Akanksha Singh
    // added '@IsTest' for testmethod to increase code coverage. Eariler code coverage was 0%.
    @IsTest
    public static void createCandidatePatientTest() {
        VT_R2_AddNewPatientController.PatientInfo patientInfo = new VT_R2_AddNewPatientController.PatientInfo();
        patientInfo.virtualSiteId = virtualSiteId;
        patientInfo.firstName = 'First';
        patientInfo.lastName = 'Last';
        patientInfo.addressCountry = 'US';
        patientInfo.preferredLanguage = 'en_US';
        patientInfo.addressLine1 = 'addressLine1';
        patientInfo.addressLine2= 'addressLine2';
        patientInfo.stateProvince = 'NY';
        patientInfo.zipCode = '123456';
        patientInfo.email = VT_D1_TestUtils.generateUniqueUserName();
        patientInfo.phone = '1234567890';
        patientInfo.phoneType = 'Mobile';
        VT_R2_AddNewPatientController.CaregiverInfo caregiverInfo = new VT_R2_AddNewPatientController.CaregiverInfo();
        caregiverInfo.firstName = 'FirstCG';
        caregiverInfo.lastName = 'LastCG';
        caregiverInfo.preferredLanguage = 'en_US';
        caregiverInfo.email = VT_D1_TestUtils.generateUniqueUserName();
        caregiverInfo.phone = '9876543210';
        caregiverInfo.phoneType = 'Mobile';
        patientInfo.cg = caregiverInfo;
        //VT_R2_HttpCalloutMockImpl httpMock = new VT_R2_HttpCalloutMockImpl();
        //httpMock.response = new HttpResponse();
        //httpMock.response.setBody('patientId');
        //httpMock.response.setStatus('OK');
        //httpMock.response.setStatusCode(200);
        Test.startTest();
        VT_R3_GlobalSharing.disableForTest = true;
        //Test.setMock(HttpCalloutMock.class, httpMock);
        //VT_R2_AddNewPatientController.createCandidatePatientRemote(System.JSON.serialize(patientInfo));
        //VT_R2_AddNewPatientController.doPost();
        VT_R2_AddNewPatientController.createCandidatePatient(patientInfo);
        //VT_R2_AddNewPatientController.getVirtualSiteWrapperRemote(virtualSiteId,piUserId,studyId);
        Test.stopTest();
    }

    // Jira Ref: SH-19460 Akanksha Singh
    // added '@IsTest' for testmethod to increase code coverage. Eariler code coverage was 0%.
    @IsTest
    public static void preScreeningTest() {
        Test.startTest();
        VT_R2_AddNewPatientController.getInputSelectOptions('HealthCloudGA__CandidatePatient__c', 'VTD2_Language__c');
        VT_R2_AddNewPatientController.getPreScreeningCriteriaListRemote(virtualSiteId);
        VT_R2_AddNewPatientController.createPreScreening(virtualSiteId, null);
        VT_R2_AddNewPatientController.createPreScreening(virtualSiteId, 'failedCriteria');
        Test.stopTest();
    }

    // Jira Ref: SH-19460 Akanksha Singh
    // added '@IsTest' for testmethod to increase code coverage. Eariler code coverage was 0%.
    @IsTest
    public static void getPIAndStudyInfoListTest() {
        Test.startTest();
        List<Study_Site_Team_Member__c> sstmList = [SELECT VTR2_Associated_SCr__r.User__c, VTR2_Associated_PI__r.User__c
        FROM Study_Site_Team_Member__c
        WHERE VTR2_Associated_SCr__r.User__c != NULL
        AND VTR2_Associated_PI__c != NULL
            AND VTR2_Associated_PI__r.Study__c = :studyId
            LIMIT 1
        ];
        System.assert(!sstmList.isEmpty());
        User scrUser = [SELECT Id FROM User WHERE Id = :sstmList[0].VTR2_Associated_SCr__r.User__c LIMIT 1];
//        insert new HealthCloudGA__CarePlanTemplate__Share(
//                UserOrGroupId = scrUser.Id,
//                ParentId = studyId,
//                AccessLevel = 'Read'
//        );
        
        VT_R2_AddNewPatientController.getVirtualSiteWrapperRemote(virtualSiteId, sstmList[0].VTR2_Associated_PI__r.User__c, studyId);
        System.runAs(scrUser) {
            VT_R2_AddNewPatientController.getPIAndStudyInfoList();
        }
        Test.stopTest();
    }

    // Jira Ref: SH-19460 Akanksha Singh
    // added '@IsTest' for testmethod to increase code coverage. Eariler code coverage was 0%.
    @IsTest
    public static void getVirtualSiteRemoteTest() {
        Test.startTest();
        VT_R2_AddNewPatientController.getVirtualSiteRemote(null, piUserId, studyId);
        Test.stopTest();
    }

    // Jira Ref: SH-19460 Akanksha Singh
    // added '@IsTest' for testmethod to increase code coverage. Eariler code coverage was 0%.
    @IsTest
    public static void isEmailUsedTest() {
        VT_R2_HttpCalloutMockImpl httpMock = new VT_R2_HttpCalloutMockImpl();
        Test.setMock(HttpCalloutMock.class, httpMock);
        httpMock.response = new HttpResponse();
        httpMock.response.setBody('true');
        httpMock.response.setStatus('OK');
        httpMock.response.setStatusCode(200);
        Test.startTest();
        System.assert(VT_R2_AddNewPatientController.isEmailUsed(UserInfo.getUserName()));
        Test.stopTest();
    }

    // Jira Ref: SH-19460 Akanksha Singh
    // added '@IsTest' for testmethod to increase code coverage. Eariler code coverage was 0%.
    @IsTest
    public static void isMobileUsedTest() {
        Test.startTest();
        VT_D1_Phone__c phone = new VT_D1_Phone__c();
        phone.PhoneNumber__c = '+1 (123) 456-7890';
        phone.Type__c = 'Mobile';
        phone.Account__c = [SELECT Id FROM Account LIMIT 1].Id;
        insert phone;
        
        System.assert(VT_R2_AddNewPatientController.isMobileUsed('+1(123)45678-90'));
        Test.stopTest();
    }

    /*******************************************************************************************************
    * @author                   Akanksha Singh (SH-19460)
    * @description              test method to get the Primary PI and Studies associated to the logged in sub-I User.
    */
    @IsTest static void getPIAndStudyInfoForPITest() {

        //Sharing records should be created for test data
        VT_R3_GlobalSharing.disableForTest = true;

        Test.startTest(); 
        //Fetch virtual site
        Virtual_Site__c vs = [SELECT Id, 
                              VTD1_Study__c
                              FROM Virtual_Site__c
                              WHERE Name = 'TestSite'
                              LIMIT 1];
        
        //Fetch study site team member associated with retrieved Virtual site
        List<Study_Site_Team_Member__c> sstmList = [SELECT Id,
                                                    VTR2_Associated_SubI__r.User__c,
                                                    VTR2_Associated_PI__c,
                                                    VTR2_Associated_SCr__c,
                                                    VTR2_Associated_PI3__c,
                                                    VTR2_Associated_SubI__c,
                                                    VTR2_Associated_PI3__r.User__c,
                                                    VTR2_Associated_PI3__r.VTD1_UserName__c
                                                    FROM Study_Site_Team_Member__c
                                                    WHERE VTR2_Associated_SubI__r.User__c != NULL
                                                    AND VTR2_Associated_PI3__c != NULL
                                                    AND VTD1_SiteID__r.Name = 'TestSite'
                                                    LIMIT 1
                                                    ];
        System.assert(!sstmList.isEmpty());
        
        User subIUser = [SELECT Id FROM User WHERE Id = :sstmList[0].VTR2_Associated_SubI__r.User__c LIMIT 1];
        
        List<VT_R2_AddNewPatientController.PIUserInfo> result1;
        
        //For sub-I logged in user, Primary PI user should be the returned 
        System.runAs(subIUser) {
            result1 = VT_R2_AddNewPatientController.getPIAndStudyInfoForPI();
        }
        Test.stopTest();
        //Expected: for sub-I logged in user, Primary PI User Info should be returned which will be 'Associated PI3' field value of SSTM.
        System.AssertEquals(result1[0].Id, sstmList[0].VTR2_Associated_PI3__r.User__c);
        
    }
    
    /*******************************************************************************************************
    * @author                   Akanksha Singh (SH-19460)
    * @description              test method to get the Primary PI and Studies associated to the logged in Primary PI User.
    */
    @IsTest static void getPIAndStudyInfoForPITestforPrimaryPi() {

        //Sharing records should be created for test data
        VT_R3_GlobalSharing.disableForTest = true;

        List<String> priPIId = new  List<String>();  
        Test.startTest(); 
        Virtual_Site__c vs = [SELECT Id, 
                              VTD1_Study__c
                              FROM Virtual_Site__c
                              WHERE Name = 'TestSite'
                              LIMIT 1];
        
        List<Study_Site_Team_Member__c> sstmList = [SELECT Id,
                                                    VTR2_Associated_SubI__r.User__c,
                                                    VTR2_Associated_PI__c,
                                                    VTR2_Associated_SCr__c,
                                                    VTR2_Associated_PI3__c,
                                                    VTR2_Associated_SubI__c,
                                                    VTR2_Associated_PI3__r.User__c,
                                                    VTR2_Associated_PI3__r.VTD1_UserName__c
                                                    FROM Study_Site_Team_Member__c
                                                    WHERE VTR2_Associated_SubI__r.User__c != NULL
                                                    AND VTR2_Associated_PI3__c != NULL
                                                    AND VTD1_SiteID__r.Name = 'TestSite'
                                                    LIMIT 1
                                                    ];
        System.assert(!sstmList.isEmpty());

        //As share records won't be created automatically in test classes, need to insert share record manually.
        HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
        objVirtualShare.ParentId = vs.VTD1_Study__c;
        objVirtualShare.UserOrGroupId = sstmList[0].VTR2_Associated_PI3__r.User__c;
        objVirtualShare.AccessLevel = 'Edit';
        insert objVirtualShare;
        
        User primaryPiUser = [SELECT Id FROM User WHERE Id = :sstmList[0].VTR2_Associated_PI3__r.User__c LIMIT 1];
        
        List<VT_R2_AddNewPatientController.PIUserInfo> result2;
        
        //For Primary PI user, Primary PI should be the returned 
        System.runAs(primaryPiUser) {
            result2 = VT_R2_AddNewPatientController.getPIAndStudyInfoForPI();
        }
        Test.stopTest();
        
        //Expected: for Primary PI logged in user, Primary PI User Info should be returned which will be 'Associated PI3' field value of SSTM.
        System.AssertEquals(result2[0].Id, primaryPiUser.Id);
        
    }
    
    /*******************************************************************************************************
    * @author                   Akanksha Singh (SH-19460)
    * @description              test method to get the Primary PI and Studies associated to the logged in Backup PI User.
    */
    @IsTest static void getPIAndStudyInfoForPITestforBackupPI() {

        //Sharing records should be created for test data
        VT_R3_GlobalSharing.disableForTest = true;

        List<String> priPIId = new  List<String>();  
        Test.startTest(); 
        Virtual_Site__c vs = [SELECT Id, 
                              VTD1_Study__c
                              FROM Virtual_Site__c
                              WHERE Name = 'TestSite'
                              LIMIT 1];
        
        List<Study_Site_Team_Member__c> sstmList = [SELECT Id,
                                                    VTR2_Associated_SubI__r.User__c,
                                                    VTR2_Associated_PI__c,
                                                    VTR2_Associated_SCr__c,
                                                    VTR2_Associated_PI3__c,
                                                    VTR2_Associated_SubI__c,
                                                    VTR2_Associated_PI3__r.User__c,
                                                    VTR2_Associated_PI3__r.VTD1_UserName__c
                                                    FROM Study_Site_Team_Member__c
                                                    WHERE VTR2_Associated_SubI__r.User__c != NULL
                                                    AND VTR2_Associated_PI3__c != NULL
                                                    AND VTD1_SiteID__r.Name = 'TestSite'
                                                    LIMIT 1
                                                    ];
        System.assert(!sstmList.isEmpty());
        
        //As share records won't be created automatically in test classes, need to insert share record manually.
        HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
        objVirtualShare.ParentId = vs.VTD1_Study__c;
        objVirtualShare.UserOrGroupId = sstmList[0].VTR2_Associated_PI3__r.User__c;
        objVirtualShare.AccessLevel = 'Edit';
        insert objVirtualShare;
        
        User primaryPiUser = [SELECT Id FROM User WHERE Id = :sstmList[0].VTR2_Associated_PI3__r.User__c LIMIT 1];
        
        List<Study_Team_Member__c> backuppiList = [SELECT Id,
                                                    User__c,
                                                    Name,
                                                    Study_Team_Member__c,
                                                    Study_Team_Member__r.User__c
                                                    FROM Study_Team_Member__c
                                                    WHERE Study_Team_Member__r.User__c = : primaryPiUser.Id
                                                    AND VTD1_VirtualSite__c =: vs.Id ];

        User backupPiUser = [SELECT Id FROM User WHERE Id = :backuppiList[0].User__c LIMIT 1];

        List<VT_R2_AddNewPatientController.PIUserInfo> result3;
        
        //For Backup PI user, Primary PI should be the returned 
        System.runAs(backupPiUser) {
            result3 = VT_R2_AddNewPatientController.getPIAndStudyInfoForPI();
        }
        Test.stopTest();
        
        //Expected: for Backup PI logged in user, Primary PI User Info should be returned.
        System.AssertEquals(result3[0].Id, primaryPiUser.Id);
        
    }
    
    // Jira Ref: SH-19460 Akanksha Singh
    // added testmethod to increase code coverage. Eariler code coverage was 0%.
    // will check Profile of logged in user
    @IsTest
    static void fetchProfileInfotest() {
        Test.startTest();

        Virtual_Site__c vs = [SELECT Id, 
                              VTD1_Study__c
                              FROM Virtual_Site__c
                              WHERE Name = 'TestSite'
                              LIMIT 1];

        List<Study_Site_Team_Member__c> sstmList = [SELECT Id,
                                                    VTR2_Associated_SubI__r.User__c,
                                                    VTR2_Associated_SCr__r.User__c,
                                                    VTR2_Associated_PI__c,
                                                    VTR2_Associated_SCr__c,
                                                    VTR2_Associated_PI3__c,
                                                    VTR2_Associated_SubI__c,
                                                    VTR2_Associated_PI3__r.User__c,
                                                    VTR2_Associated_PI3__r.VTD1_UserName__c
                                                    FROM Study_Site_Team_Member__c
                                                    WHERE VTR2_Associated_SCr__r.User__c != NULL
                                                    AND VTR2_Associated_SCr__c != NULL
                                                    AND VTD1_SiteID__r.Name = 'TestSite'
                                                    LIMIT 1
                                                    ];
        User scrUser = [SELECT Id FROM User WHERE Id = :sstmList[0].VTR2_Associated_SCr__r.User__c LIMIT 1];
        User piUser = [SELECT Id FROM User WHERE Id = :sstmList[0].VTR2_Associated_PI3__r.User__c LIMIT 1];
        //for SCR user
        String result;
        String ExpectedResult = 'External User';
        System.runAs(scrUser) {
        result = VT_R2_AddNewPatientController.fetchProfileInfo();
        }
        System.AssertEquals(ExpectedResult, result);

        //for PI user
        String Piresult;
        String PIExpectedResult = 'Internal User';
        System.runAs(piUser) {
        Piresult = VT_R2_AddNewPatientController.fetchProfileInfo();
        }
        Test.stopTest();
        System.AssertEquals(PIExpectedResult, Piresult);
    }

    // Jira Ref: SH-19460 Akanksha Singh
    // added testmethod to increase code coverage. Eariler code coverage was 0%.
    // To check callout functionality
    @IsTest
    public static void createCandidatePatientRemoteTest() {
        VT_R2_AddNewPatientController.PatientInfo patientInfo = new VT_R2_AddNewPatientController.PatientInfo();
        patientInfo.virtualSiteId = virtualSiteId;
        patientInfo.firstName = 'First';
        patientInfo.lastName = 'Last';
        patientInfo.addressCountry = 'US';
        patientInfo.preferredLanguage = 'en_US';
        patientInfo.addressLine1 = 'addressLine1';
        patientInfo.addressLine2= 'addressLine2';
        patientInfo.stateProvince = 'NY';
        patientInfo.zipCode = '123456';
        patientInfo.email = VT_D1_TestUtils.generateUniqueUserName();
        patientInfo.phone = '1234567890';
        patientInfo.phoneType = 'Mobile';
        String patientInfoStrigyfy = JSON.serialize(patientInfo);
        VT_R2_AddNewPatientController.CaregiverInfo caregiverInfo = new VT_R2_AddNewPatientController.CaregiverInfo();
        caregiverInfo.firstName = 'FirstCG';
        caregiverInfo.lastName = 'LastCG';
        caregiverInfo.preferredLanguage = 'en_US';
        caregiverInfo.email = VT_D1_TestUtils.generateUniqueUserName();
        caregiverInfo.phone = '9876543210';
        caregiverInfo.phoneType = 'Mobile';
        patientInfo.cg = caregiverInfo;
        VT_R2_HttpCalloutMockImpl httpMock = new VT_R2_HttpCalloutMockImpl();
        httpMock.response = new HttpResponse();
        httpMock.response.setBody('patientId');
        httpMock.response.setStatus('OK');
        httpMock.response.setStatusCode(200);
        Test.startTest();
        VT_R3_GlobalSharing.disableForTest = true;
        Test.setMock(HttpCalloutMock.class, httpMock);
        String response = VT_R2_AddNewPatientController.createCandidatePatientRemote(System.JSON.serialize(patientInfo));
        String expectedValue = 'patientId';
        System.assertEquals(expectedValue,response);
        Test.stopTest();
    }

    //Jira Ref : SH-19460 End
    
}