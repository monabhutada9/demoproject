@IsTest
public with sharing class VT_R3_AddStudyDocumentsControllerTest {

    @TestSetup
    private static void setup() {
        DomainObjects.User_t ptUserT = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile(VT_R4_ConstantsHelper_Profiles.PATIENT_PROFILE_NAME);
        User ptUser = (User) ptUserT.persist();

        User cgUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile(VT_R4_ConstantsHelper_Profiles.CAREGIVER_PROFILE_NAME)
                .persist();

        Case caseRecord = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .addAccount(new DomainObjects.Account_t())
                .addUser(ptUserT)
                .persist();
        
        caseRecord.VTD1_Randomized_ID__c = 't';
        caseRecord.Status = 'Active/Randomized';
        caseRecord.VTD1_End_of_study_visit_completed__c = true;
        caseRecord.Subject = 't';
        update caseRecord;

        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c(VTD1_VisitType__c = 'Labs', VTR2_Visit_Participants__c = 'Patient Guide');
        insert protocolVisit;
        VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c(
                Sub_Type__c = 'Home Health Nurse', VTD1_Case__c = caseRecord.Id,
                VTD1_Status__c = 'Requested', VTD1_Protocol_Visit__c = protocolVisit.Id,
                VTD1_Patient_Removed_Flag__c = true, VTR5_PatientCaseId__c = caseRecord.Id
        );
        insert actualVisit;
        VTD1_ProtocolVisit__c protocolVisit2 = new VTD1_ProtocolVisit__c(VTD1_EDC_Name__c = 'Labs 21', VTD1_Independent_Rater_Flag__c = true ,VTD1_VisitType__c = 'Labs', VTR2_Visit_Participants__c = 'Patient Guide');
        insert protocolVisit2;
        insert new VTD2_TN_Catalog_Code__c(VTR2_Name_ID__c = 'VTD1_Actual_Visit__c.VTD1_Visit_Name__c', VTD2_T_Task_Unique_Code__c = 'T52', VTD2_T_Category__c = 'Visit Related', VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD1_RTId__c.VTD1_SC_Task_Cancellation__c', VTD2_T_Due_Date__c = 'NOW()', VTD2_T_Actual_Visit__c = 'VTD1_Actual_Visit__c.Id', VTD2_T_SObject_Name__c = 'VTD1_Actual_Visit__c', RecordTypeId = VTD2_TN_Catalog_Code__c.getSObjectType().getDescribe().getRecordTypeInfosByName().get('SCTask').getRecordTypeId(), VTD2_T_Type__c = 'Cancellation', VTD2_T_Patient__c = 'IF (ISBLANK(VTD1_Actual_Visit__c.VTD1_Case__c ),VTD1_Actual_Visit__c.Unscheduled_Visits__c ,VTD1_Actual_Visit__c.VTD1_Case__c )', VTD2_T_Care_Plan_Template__c = 'IF (ISBLANK(VTD1_Actual_Visit__c.VTD1_Case__c ), VTD1_Actual_Visit__c.Unscheduled_Visits__r.VTD1_Study__c ,VTD1_Actual_Visit__c.VTD1_Case__r.VTD1_Study__c )', VTD2_T_Assigned_To_ID__c = 'IF (ISBLANK(VTD1_Actual_Visit__c.VTD1_Case__c ), VTD1_Actual_Visit__c.Unscheduled_Visits__r.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c ,VTD1_Actual_Visit__c.VTD1_Case__r.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c)', VTR3_T_Name__c = 'VTD1_Actual_Visit__c.VTD1_Visit_Name__c', VTD2_T_Subject__c = '"Contact 3rd party to cancel remaining visits for "&VTD1_Actual_Visit__c.VTD2_PatientName_PatientId__c');

        VTD1_Actual_Visit__c actualVisit2 = new VTD1_Actual_Visit__c(
                Sub_Type__c = 'Home Health Nurse', VTD1_Case__c = caseRecord.Id, VTD1_Status__c = 'Requested',
                VTD1_Protocol_Visit__c = protocolVisit2.Id, VTD1_Patient_Removed_Flag__c = true,
                VTR5_PatientCaseId__c = caseRecord.Id
        );
        insert actualVisit2;
    }

    @IsTest static void testGetActualVisitsByCaseId() {
        Case caseRecord = [SELECT Id, VTD1_Patient_User__c FROM Case LIMIT 1];
        User patient = [SELECT Id FROM User WHERE Id = :caseRecord.VTD1_Patient_User__c];

        VT_D1_TestUtils.loadProfiles();
        User CG = VT_D1_TestUtils.createUserByProfile('Caregiver', 'CaregiverTest');
        Contact cgCon = new Contact(FirstName = CG.FirstName, LastName = 'L', VTD1_Clinical_Study_Membership__c = caseRecord.Id);
        insert cgCon;
        CG.ContactId = cgCon.Id;
        insert CG;
        insert new CaseShare(CaseId = caseRecord.Id, UserOrGroupId = CG.Id, CaseAccessLevel = 'Edit');

        Test.startTest();
        System.runAs(patient){
            VT_R3_AddStudyDocumentsController.getProfileNameOfCurrentUser();
            List<VTD1_Actual_Visit__c> actualVisitList =  VT_R3_AddStudyDocumentsController.getVisits(caseRecord.Id);
            try {
                VT_R3_AddStudyDocumentsController.getActualVisitById(caseRecord.Id);
            } catch (Exception ex) {}
        }
        System.runAs(CG){
            List<VTD1_Actual_Visit__c> actualVisitList =  VT_R3_AddStudyDocumentsController.getVisits(caseRecord.Id);
            try {
                VT_R3_AddStudyDocumentsController.getActualVisitById(null);
                VT_R3_AddStudyDocumentsController.getActualVisitById(caseRecord.Id);
            } catch (Exception ex) {}
        }
        Test.stopTest();
    }

    @IsTest static void testSaveStudyDocument() {
        Blob beforeblob = Blob.valueOf('Unit Test Attachment Body');
        ContentVersion cv = new ContentVersion();
        cv.Title = 'test content trigger';
        cv.PathOnClient ='test';
        cv.VersionData = beforeblob;
        insert cv;
        VTD1_Document__c docuSignDoc = new VTD1_Document__c(
                VTD1_Nickname__c = 'Certification form'
        );
        insert docuSignDoc;

        List<VTD1_Actual_Visit__c> visits = [SELECT Id FROM VTD1_Actual_Visit__c LIMIT 1];

        Test.startTest();

        Case caseRecord = [SELECT Id, VTD1_Patient_User__c FROM Case LIMIT 1];
        VT_R3_AddStudyDocumentsController.saveStudyDocument(caseRecord.Id, visits[0].Id, docuSignDoc.Id, 'nickName', 'comments', null);

        User patient = [SELECT Id, Name, ContactId, AccountId, Contact.AccountId FROM User WHERE Id = :caseRecord.VTD1_Patient_User__c];
        System.runAs(patient){
            //VTD1_Document__c doc = [SELECT Id FROM VTD1_Document__c LIMIT 1];
            VT_R3_AddStudyDocumentsController.saveStudyDocument(caseRecord.Id, visits[0].Id, docuSignDoc.Id, 'nickName', 'comments', null);
            VT_R3_AddStudyDocumentsController.saveStudyDocument(caseRecord.Id, null, docuSignDoc.Id, 'nickName', 'comments');
        }

        User scrUsr = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Site Coordinator' AND IsActive = TRUE LIMIT 1];
        System.runAs(scrUsr){
            VTD1_Document__c cert = new VTD1_Document__c(
                    VTD1_Nickname__c = 'Certification form'
            );
            insert cert;
            VT_R3_AddStudyDocumentsController.saveStudyDocument(caseRecord.Id, visits[0].Id, cert.Id, 'nickName', 'comments', 'Visit Document');
        }

        Test.stopTest();
    }

    @IsTest static void testremoteDocument() {
        Blob beforeblob = Blob.valueOf('Unit Test Attachment Body');
        Case caseRecord = [SELECT Id, VTD1_Patient_User__c FROM Case LIMIT 1];
        ContentVersion cv = new ContentVersion();
        cv.Title = 'test content trigger';
        cv.PathOnClient ='test';
        cv.VersionData = beforeblob;
        insert cv;


        Test.startTest();
        cv = [
                SELECT Id, ContentDocumentId, Title, PathOnClient, VersionData
                FROM ContentVersion
                WHERE Id = :cv.Id
        ];
        insert new ContentDocumentLink(LinkedEntityId = caseRecord.Id, ContentDocumentId = cv.ContentDocumentId);


        VTD1_Document__c document = VT_R3_AddStudyDocumentsController.getRecordByFileAndCase(cv.ContentDocumentId, caseRecord.Id);

        System.assert(document != null, 'VTD1_Document__c record should be created');

        VT_R3_AddStudyDocumentsController.remoteDocument(document.Id);
        try {
            VT_R3_AddStudyDocumentsController.remoteDocument('23412312541');
        } catch (Exception ex) {}

        Test.stopTest();
    }
}