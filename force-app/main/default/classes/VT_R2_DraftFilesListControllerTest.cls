@IsTest
public with sharing class VT_R2_DraftFilesListControllerTest {

    public static void testGetFiles() {
        Account account = (Account) new DomainObjects.Account_t().persist();

        Test.startTest();
        VT_D1_DraftFilesListController.getFiles(account.Id);
        new VT_D1_DraftFilesListController();
        Test.stopTest();
    }
}