@IsTest
private class LightningSharingTest {
	@IsTest
	static void canIEditPermsTest() {
		Contact contact = VT_D1_TestUtils.createContact(null, 'Gena', 'Bobkov', 'Gena', 'en_US', null);
		insert contact;

		Account newAccount = createAccountByAnotherUser();

		Test.startTest();
		LightningSharing.deletePerm(newAccount.Id, contact.Id);
		Boolean canIEditPerms = LightningSharing.canIEditPerms(contact.Id);
		System.assert(canIEditPerms);

		canIEditPerms = LightningSharing.canIEditPerms(newAccount.Id);
		System.assert(canIEditPerms);
		Test.stopTest();
	}

	@IsTest
	public static void getSharingsTest() {
		Contact contact = VT_D1_TestUtils.createContact(null, 'Gena', 'Bobkov', 'Gena', 'en_US', null);
		insert contact;

		String sharings = LightningSharing.getSharings(contact.Id);
		System.assert(!String.isEmpty(sharings));
	}

	@IsTest
	public static void doUpsertPermTest() {
		Account account = new Account(Name = 'Test Patient');
		insert account;

		Account newAccount = createAccountByAnotherUser();

		LightningSharing.doSOSL('Test Patient', 'Account');
		String result = LightningSharing.upsertPerm(UserInfo.getUserId(), account.Id, 'All');
		System.assert(!String.isEmpty(result));
		result = LightningSharing.upsertPerm(UserInfo.getUserId(), newAccount.Id, 'Read');
		System.assert(!String.isEmpty(result));
	}

	private static Account createAccountByAnotherUser() {
		String uniqueUserName = 'usr' + Datetime.now().getTime() + '@test.com';
		Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
		User u = new User(
				Alias = 'standt', Email = 'standarduser@testorg.com', EmailEncodingKey = 'UTF-8', LastName = 'Testing',
				LanguageLocaleKey = 'ru', ProfileId = p.Id, TimeZoneSidKey = 'Europe/Minsk', LocaleSidKey = 'ru_RU',
				Username = uniqueUserName
		);

		Account newAccount = new Account();
		System.runAs(u) {
			newAccount = new Account(Name = 'Test Account');
			insert newAccount;
		}

		return newAccount;
	}
}