public with sharing class VT_D1_PatientMyStudySuppliesController {

    public class StudySupplies {
        public List<StudySupplyItem> studySupplyList;
        public User user;
        public List<Option> deliveryStatuses;
        public List<Option> replacementReasons;
        public List<Option> kitTypes;

        public StudySupplies(List<StudySupplyItem> studySupplyList, User user) {
            this.studySupplyList = studySupplyList;
            this.user = user;
            this.deliveryStatuses = getDeliveryStatuses();
            this.replacementReasons = getReplacementReasons();
            this.kitTypes = getKitTypes();
        }

        private List<Option> getDeliveryStatuses() {
            List<Option> deliveryStatusesList = new List<Option>();
            Map<String, String> deliveryStatusMap = VT_D1_CustomMultiPicklistController.getSelectOptionsWithLabel(new VTD1_Order__c(), 'VTD1_Status__c');
            for (String deliveryStatus : deliveryStatusMap.keySet()) {
                if (deliveryStatus != 'Not Started' && deliveryStatus != 'Approved') {
                    deliveryStatusesList.add(new Option(deliveryStatusMap.get(deliveryStatus), deliveryStatus));
                }
            }
            return deliveryStatusesList;
        }

        private List<Option> getKitTypes() {
            return new List<Option>{
                    new Option(System.Label.VTD2_WelcometoStudyPackage, 'Welcome to Study Package'),
                    new Option(System.Label.VTD2_StudyHubTablet, 'Study Hub Tablet'),
                    new Option(System.Label.VTD2_ConnectedDevices, 'Connected Devices'),
                    new Option(System.Label.VTD2_IMP, 'IMP'),
                    new Option(System.Label.VTD2_Lab, 'Lab')
            };
        }

        private List<Option> getReplacementReasons() {
            List<Option> replacementReasonsList = new List<Option>();
            Map<String, String> replacementReasonOptionsMap = VT_D1_CustomMultiPicklistController.getSelectOptionsWithLabel(new IMP_Replacement_Form__c(), 'VTD1_Replacement_Reason__c');
            for (String value : replacementReasonOptionsMap.keySet()) {
                replacementReasonsList.add(new Option(replacementReasonOptionsMap.get(value), value));
            }
            return replacementReasonsList;
        }
    }

    public class StudySupplyItem {
        public VTD1_Order__c delivery;
        public String translatedDeliveryName;
        public String shipmentDate;
        public String expectedDeliveryDate;
        public Boolean confirmPackagingMaterialsInHand;
        public Boolean confirmCourierPickup;
        public Boolean confirmDropOff;
        public List<VTD1_Patient_Kit__c> kitsList;
    }

    public class Option {
        public String label;
        public String value;

        Option(String label) {
            this.label = label;
            this.value = 'false';
        }

        Option(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }

    @AuraEnabled
    public static String getPatientStudySupplies() {
        try {
            return JSON.serialize(getStudySuppliesWrapper());
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage() + '\n' + ex.getStackTraceString());
        }
    }

    public static StudySupplies getStudySuppliesWrapper() {
        User u = [
                SELECT Id,
                        Profile.Name,
                        VTD1_Form_Updated__c,
                        Contact.VTD1_Clinical_Study_Membership__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__r.Name
                FROM User
                WHERE Id = :UserInfo.getUserId()
        ];
        Map<Id, VTD1_Order__c> delMap = getDeliveriesMap(u.Contact.VTD1_Clinical_Study_Membership__c);
        Map<Id, List<VTD1_Patient_Kit__c>> delIdToKitsMap = getDelIdToKitsMap(delMap.keySet());
        Map<Id, Map<String, Boolean>> delIdToConfirmTasksMap = getDelIdToConfirmTasksMap(delMap);
        return new StudySupplies(getStudySuppliesItems(delMap, delIdToKitsMap, delIdToConfirmTasksMap), u);
    }

    private static Map<Id, VTD1_Order__c> getDeliveriesMap(Id casId) {
        Map<Id, VTD1_Order__c> delMap = new Map<Id, VTD1_Order__c>([
                SELECT Id,
                        Name,
                        RecordType.Name,
                        RecordType.DeveloperName,
                        VTD1_Carrier__c,
                        VTD1_TrackingNumber__c,
                        VTD1_Shipment_Date__c,
                        VTD1_Shipment_Type_formula__c,
                        VTD1_Shipment_Frequency__c,
                        VTD1_ExpectedDeliveryDate__c,
                        VTD1_Status__c,
                        toLabel(VTD1_Status__c) translatedStatus,
                        VTD1_CountIMPPatientDeliveryComma__c,
                        VTD1_Description__c,
                        VTD1_ExpectedDeliveryDateTime__c,
                        VTD1_isIMP__c,
                        VTD1_isPackageDelivery__c,
                        VTD1_isReturnDelivery__c,
                        VTD1_Patient_Delivery__c,
                        VTD1_Patient_Delivery__r.VTD1_Status__c,
                        VTD1_Patient_Delivery__r.VTD1_isIMP__c,
                        VTR2_Destination_Type__c,
                        VTR2_KitsGivenToPatient__c,
                        VTR2_Return_Kit_Type__c,
                        VTR2_For_Return_Kit__c,
                        VTR2_For_Return_Kit__r.VTD1_PatientHasPackagingMaterials__c,
                        VTD1_Kit_Type__c,
                        VTD1_Protocol_Delivery__c,
                        VTD1_Protocol_Delivery__r.Name,
                        VTD1_Protocol_Delivery__r.VTD1_Description__c,
                        toLabel(VTD1_Protocol_Delivery__r.VTD1_Shipment_Type__c),
                        LastModifiedDate
                FROM VTD1_Order__c
                WHERE VTD1_Case__c = :casId
                AND VTD1_Status__c != 'Not Started'
                AND VTD1_Status__c != NULL
                AND (
                        (
                                VTD1_Status__c != 'Pending Return'
                                OR VTD1_isReturnDelivery__c = FALSE
                        )
                        OR (
                                VTD1_Status__c = 'Pending Return'
                                AND VTD1_isReturnDelivery__c = TRUE
                                AND VTD1_Patient_Delivery__r.VTD1_Status__c = 'Received'
                        )
                )
                ORDER BY VTD1_Delivery_Order__c
        ]);
        VT_D1_TranslateHelper.translate(delMap.values());
        return delMap;
    }

    private static Map<Id, List<VTD1_Patient_Kit__c>> getDelIdToKitsMap(Set<Id> delIdsSet) {
        Map<Id, List<VTD1_Patient_Kit__c>> delIdToKitsMap = new Map<Id, List<VTD1_Patient_Kit__c>>();
        List<VTD1_Patient_Kit__c> kitsList = [
                SELECT Id,
                        Name,
                        RecordType.Name,
                        RecordType.DeveloperName,
                        VTD1_Kit_Name__c,
                        VTD1_Kit_Type__c,
                        VTD1_Status__c,
                        toLabel(VTD1_Status__c) translatedStatus,
                        VTD1_Patient_Delivery__c,
                        VTD1_Recalled__c,
                        VTD1_Protocol_Kit__c,
                        VTD1_Protocol_Kit__r.Name,
                        VTR2_IfTemperatureControlled__c,
                        VTR2_ReceivedTemperature__c,
                        LastModifiedDate,
                        (
                            SELECT Name,
                                    VTD1_Replace__c,
                                    VTD1_Replaced_With__c
                            FROM IMP_Replacement_Forms__r
                        )
                FROM VTD1_Patient_Kit__c
                WHERE VTD1_Patient_Delivery__c IN :delIdsSet
        ];
        VT_D1_TranslateHelper.translate(kitsList);
        for (VTD1_Patient_Kit__c kit : kitsList) {
            if (!delIdToKitsMap.containsKey(kit.VTD1_Patient_Delivery__c)) {
                delIdToKitsMap.put(kit.VTD1_Patient_Delivery__c, new List<VTD1_Patient_Kit__c>());
            }
            delIdToKitsMap.get(kit.VTD1_Patient_Delivery__c).add(kit);
        }
        return delIdToKitsMap;
    }

    private static Map<Id, Map<String, Boolean>> getDelIdToConfirmTasksMap(Map<Id, VTD1_Order__c> delMap) {
        String dropOffofStudyMedication = 'Confirm Drop Off of Study Medication';
        String dropOffofLab = 'Confirm Drop Off Lab';
        String packMatInHand = 'Confirm Packaging Materials in Hand';
        String courierPickup = 'Confirm Courier Pickup';
        Set<String> typesForBackendLogic = new Set<String>{
                dropOffofStudyMedication,
                dropOffofLab,
                packMatInHand,
                courierPickup
        };
        Map<Id, Map<String, Boolean>> delIdToConfirmTasksMap = new Map<Id, Map<String, Boolean>>();
        for (AggregateResult ar : [
                SELECT WhatId,
                        VTD1_Type_for_backend_logic__c
                FROM Task
                WHERE VTD1_Type_for_backend_logic__c IN :typesForBackendLogic
                AND Status = 'Open'
                AND WhatId IN :delMap.keySet()
                GROUP BY VTD1_Type_for_backend_logic__c, WhatId
        ]) {
            Id whatId = (Id)ar.get('WhatId');
            if (!delIdToConfirmTasksMap.containsKey(whatId)) {
                delIdToConfirmTasksMap.put(
                        whatId,
                        new Map<String, Boolean>{
                                'confirmPackagingMaterialsInHand' => false,
                                'confirmCourierPickup' => false,
                                'confirmDropOff' => false
                        }
                );
            }
            if (ar.get('VTD1_Type_for_backend_logic__c') == packMatInHand) {
                delIdToConfirmTasksMap.get(whatId).put('confirmPackagingMaterialsInHand', true);
            }
            if (delMap.get(whatId).VTD1_isReturnDelivery__c && delMap.get(whatId).VTD1_Status__c == 'Pending Return') {
                if (ar.get('VTD1_Type_for_backend_logic__c') == courierPickup) {
                    delIdToConfirmTasksMap.get(whatId).put('confirmCourierPickup', true);
                }
                if (delMap.get(whatId).VTD1_Patient_Delivery__r.VTD1_Status__c == 'Received'
                    && (ar.get('VTD1_Type_for_backend_logic__c') == dropOffofStudyMedication || ar.get('VTD1_Type_for_backend_logic__c') == dropOffofLab)
                ) {
                    delIdToConfirmTasksMap.get(whatId).put('confirmDropOff', true);
                }
            }
        }
        return delIdToConfirmTasksMap;
    }

    private static List<StudySupplyItem> getStudySuppliesItems(Map<Id, VTD1_Order__c> delMap, Map<Id, List<VTD1_Patient_Kit__c>> delIdToKitsMap, Map<Id, Map<String, Boolean>> delIdToConfirmTasksMap) {
        List<StudySupplyItem> studySuppliesItems = new List<StudySupplyItem>();
        for (VTD1_Order__c del : delMap.values()) {
            StudySupplyItem item = new StudySupplyItem();
            item.delivery = del;
            item.kitsList = delIdToKitsMap.get(del.Id);
            if (del.Name.contains('Return of IMP')) {
                item.translatedDeliveryName = System.Label.VT_R3_ReturnOfIMP + ' #' + del.Name.split('#')[1];
            } else if (del.Name.contains('Return of Lab')) {
                item.translatedDeliveryName = System.Label.VT_R3_ReturnOfLab + ' #' + del.Name.split('#')[1];
            } else if (del.Name.contains('Return of Connected Devices')) {
                item.translatedDeliveryName = System.Label.VTR4_ReturnOfConnectedDevices;
            } else if (del.Name.contains('Return of Study Hub Tablet')) {
                item.translatedDeliveryName = System.Label.VTR4_ReturnOfStudyHubTablet;
            }
            item.shipmentDate = translateDate(del.VTD1_Shipment_Date__c);
            item.expectedDeliveryDate = translateDate(del.VTD1_ExpectedDeliveryDate__c);
            item.confirmPackagingMaterialsInHand = delIdToConfirmTasksMap.get(del.Id) != null ? delIdToConfirmTasksMap.get(del.Id).get('confirmPackagingMaterialsInHand') : false;
            item.confirmCourierPickup = delIdToConfirmTasksMap.get(del.Id) != null ? delIdToConfirmTasksMap.get(del.Id).get('confirmCourierPickup') : false;
            item.confirmDropOff = delIdToConfirmTasksMap.get(del.Id) != null ? delIdToConfirmTasksMap.get(del.Id).get('confirmDropOff') : false;
            studySuppliesItems.add(item);
        }
        return studySuppliesItems;
    }

    private static String translateDate(Date dateToTranslate) {
        return dateToTranslate == null ? null : VT_D1_TranslateHelper.translate(
                Datetime.newInstance(
                        dateToTranslate.year(),
                        dateToTranslate.month(),
                        dateToTranslate.day()
                ).format('dd-MMM-YYYY')
        );
    }
}