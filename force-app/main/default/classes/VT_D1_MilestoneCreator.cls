public without sharing class VT_D1_MilestoneCreator {

    private Map<String, List<Id>> studyIdsByMilestone = new Map<String, List<Id>>();
    private Map<String, List<Id>> siteIdsByMilestone = new Map<String, List<Id>>();
    public List<Id> studiesWithAllClosedSites = new List<Id>();
    public List<Id> studiesWithClosedAllReasonsSites = new List<Id>();

    private Map<Id, Map<String, SObject>> existingMilestones = new Map<Id, Map<String, SObject>>();

    private List<VTD1_Study_Milestone__c> studyMilestonesToUpsert = new List<VTD1_Study_Milestone__c>();
    private List<VTD1_Site_Milestone__c> siteMilestonesToUpsert = new List<VTD1_Site_Milestone__c>();

    public void addPotentialStudyMilestone(Id studyId, String milestoneDesc) {
        addToIdMap(studyId, milestoneDesc, this.studyIdsByMilestone);
    }
    public void addPotentialSiteMilestone(Id siteId, String milestoneDesc) {
        addToIdMap(siteId, milestoneDesc, this.siteIdsByMilestone);
    }
    private void addToIdMap(Id recId, String milestoneDesc, Map<String, List<Id>> recMap) {
        if (recId != null) {
            if (! recMap.containsKey(milestoneDesc)) {
                recMap.put(milestoneDesc, new List<Id>());
            }
            recMap.get(milestoneDesc).add(recId);
        }
    }

    public void createMilestones() {
        getExistingMileStones();
        generateStudyFirstMilestones();
        generateSiteMilestones();
        generateStudyLastMilestones();

        if (! this.studyMilestonesToUpsert.isEmpty()) { upsert this.studyMilestonesToUpsert; }
        if (! this.siteMilestonesToUpsert.isEmpty()) { upsert this.siteMilestonesToUpsert; }
    }

    private void getExistingMileStones() {
        getExistingStudyMilestones();
        getExistingSiteMilestones();
    }

    private void generateStudyFirstMilestones() {
        for (String milestoneDesc : this.studyIdsByMilestone.keySet()) {
            generateStudyFirstMilestonesFromIdList(this.studyIdsByMilestone.get(milestoneDesc), milestoneDesc);
        }
    }

    private void generateSiteMilestones() {
        for (String milestoneDesc : this.siteIdsByMilestone.keySet()) {
            generateSiteMilestonesFromIdList(this.siteIdsByMilestone.get(milestoneDesc), milestoneDesc);
        }
    }

    private void generateStudyLastMilestones() {
        if (!this.studiesWithClosedAllReasonsSites.isEmpty()) {
            generateStudyLastMilestonesFromStatusHistory();
        }
        if (!this.studiesWithAllClosedSites.isEmpty()) {
            generateLastSubjectOutMileStones();
        }
    }

    private void getExistingStudyMilestones() {
        List<Id> studyIds = getStudyIdsForMilestones();
        if (!studyIds.isEmpty()) {
            for (VTD1_Study_Milestone__c item : [
                SELECT Id, VTD1_Study__c, VTD1_Milestone_Description__c, VTD1_Actual_Date__c
                FROM VTD1_Study_Milestone__c
                WHERE VTD1_Study__c IN :studyIds
            ]) {
                addToExistingMilestones(item.VTD1_Study__c, item);
            }
        }
    }

    private void getExistingSiteMilestones() {
        List<Id> siteIds = getSiteIdsForMilestones();
        if (!siteIds.isEmpty()) {
            for (VTD1_Site_Milestone__c item : [
                SELECT Id, VTD1_Site__c, VTD1_Milestone_Description__c, VTD1_Actual_Date__c
                FROM VTD1_Site_Milestone__c
                WHERE VTD1_Site__c IN :siteIds
            ]) {
                addToExistingMilestones(item.VTD1_Site__c, item);
            }
        }
    }

    private void generateStudyFirstMilestonesFromIdList(List<Id> studyIds, String milestoneDescription) {
        for (Id studyId : studyIds) {
            generateNewStudyMileStone(studyId, milestoneDescription);
        }
    }
    private void generateSiteMilestonesFromIdList(List<Id> siteIds, String milestoneDescription) {
        for (Id siteId : siteIds) {
            generateNewSiteMilestone(siteId, milestoneDescription);
        }
    }

    private void generateStudyLastMilestonesFromStatusHistory() {
        Map<Id, List<CaseHistory>> patientHistoryByStudy = getPatientHistoryByStudyIds(this.studiesWithClosedAllReasonsSites);

        for (Id studyId : this.studiesWithClosedAllReasonsSites) {
            if (patientHistoryByStudy.containsKey(studyId)) {
                Integer cnt = 0;

                for (CaseHistory historyItem : patientHistoryByStudy.get(studyId)) {
                    String newVal = (String)historyItem.NewValue;
                    Date d = historyItem.CreatedDate.dateGmt();

                    if (VT_D1_MilestoneHelper.SUBJECT_ENROLLED_STATUS.contains(newVal)) {
                        generateNewStudyMileStone(studyid, VT_R4_ConstantsHelper_AccountContactCase.LAST_SUBJECT_ENROLLED, d);
                        cnt++;
                    }
                    if (newVal.equals(VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED)) {
                        generateNewStudyMileStone(studyid, VT_R4_ConstantsHelper_AccountContactCase.LAST_SUBJECT_RANDOMIZED, d);
                        cnt++;
                    }
                    if (newVal.equals(VT_R4_ConstantsHelper_AccountContactCase.CASE_SCREENED)) {
                        generateNewStudyMileStone(studyid, VT_R4_ConstantsHelper_AccountContactCase.LAST_SUBJECT_SCREENED, d);
                        cnt++;
                    }

                    if (cnt == 3) { break; }
                }
            }
        }
    }

    private void generateLastSubjectOutMileStones() {
        for (AggregateResult item : [
            SELECT VTD1_Case__r.VTD1_Study__c, MAX(VTD1_Visit_Completion_Date__c) maxDate
            FROM VTD1_Actual_Visit__c
            WHERE VTD1_Case__r.VTD1_Study__c IN :this.studiesWithAllClosedSites
                AND VTD1_Status__c = :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED
                AND VTD1_Visit_Completion_Date__c != NULL
            GROUP BY VTD1_Case__r.VTD1_Study__c
        ]) {
            Datetime dt = (Datetime)item.get('maxDate');
            generateNewStudyMileStone(
                (id)item.get('VTD1_Study__c'),
                VT_R4_ConstantsHelper_AccountContactCase.LAST_SUBJECT_OUT,
                dt.dateGmt()
            );
        }
    }

    private Map<Id, List<CaseHistory>> getPatientHistoryByStudyIds(List<Id> studyIds) {
        Map<Id, List<CaseHistory>> patientHistoryByStudy = new Map<Id, List<CaseHistory>>();
        for (CaseHistory item : [
            SELECT Case.VTD1_Study__c, NewValue, CreatedDate
            FROM CaseHistory
            WHERE Case.VTD1_Study__c IN :studyIds
                AND Field = 'Status'
            ORDER BY CreatedDate DESC
        ]) {
            String newVal = (String)item.NewValue;
            if (VT_D1_MilestoneHelper.SUBJECT_LAST_STATUS.contains(newVal)) {
                if (!patientHistoryByStudy.containsKey(item.Case.VTD1_Study__c)) {
                    patientHistoryByStudy.put(item.Case.VTD1_Study__c, new List<CaseHistory>());
                }
                patientHistoryByStudy.get(item.Case.VTD1_Study__c).add(item);
            }
        }
        return patientHistoryByStudy;
    }

    private List<Id> getStudyIdsForMilestones() {
        return getIdsFromListOfLists(this.studyIdsByMilestone.values());
    }

    private List<Id> getSiteIdsForMilestones() {
        return getIdsFromListOfLists(this.siteIdsByMilestone.values());
    }

    private List<Id> getIdsFromListOfLists(List<List<Id>> listOfLists) {
        List<Id> ids = new List<Id>();
        for (List<Id> idList : listOfLists) {
            if(idList != null) { ids.addAll(idList); }
        }
        return ids;
    }

    private void generateNewStudyMileStone(Id studyId, String milestoneDesc, Date actualDate) {
        if (toCreateMilestone(studyId, milestoneDesc)) {
            VTD1_Study_Milestone__c mStone = new VTD1_Study_Milestone__c(
                VTD1_Study__c = studyId,
                VTD1_Milestone_Description__c = milestoneDesc,
                VTD1_Actual_Date__c = actualDate,
                Id = milestoneExists(studyId, milestoneDesc) ? (Id)this.existingMilestones.get(studyId).get(milestoneDesc).get('Id') : null
            );
            this.studyMilestonesToUpsert.add(mStone);
            addToExistingMilestones(studyId, mStone);
        }
    }
    private void generateNewStudyMileStone(Id studyId, String milestoneDesc) {
        generateNewStudyMileStone(studyId, milestoneDesc, Date.today());
    }

    private void generateNewSiteMilestone(Id siteId, String milestoneDesc) {
        if (toCreateMilestone(siteId, milestoneDesc)) {
            VTD1_Site_Milestone__c mStone = new VTD1_Site_Milestone__c(
                VTD1_Site__c = siteId,
                VTD1_Milestone_Description__c = milestoneDesc,
                VTD1_Actual_Date__c = Date.today(),
                Id = milestoneExists(siteId, milestoneDesc) ? (Id)this.existingMilestones.get(siteId).get(milestoneDesc).get('Id') : null
            );
            this.siteMilestonesToUpsert.add(mStone);
            addToExistingMilestones(siteId, mStone);
        }
    }

    private void addToExistingMilestones(Id recId, SObject milestoneRec) {
        if (!this.existingMilestones.containsKey(recId)) {
            this.existingMilestones.put(recId, new Map<String, SObject>());
        }
        this.existingMilestones.get(recId).put((String)milestoneRec.get('VTD1_Milestone_Description__c'), milestoneRec);
    }

    private Boolean milestoneExists(Id recId, String milestoneDesc) {
        return this.existingMilestones.containsKey(recId) && this.existingMilestones.get(recId).containsKey(milestoneDesc);
    }

    private Boolean toCreateMilestone(Id recId, String milestoneDesc) {
        return ! milestoneExists(recId, milestoneDesc) || this.existingMilestones.get(recId).get(milestoneDesc).get('VTD1_Actual_Date__c') == null;
    }
}