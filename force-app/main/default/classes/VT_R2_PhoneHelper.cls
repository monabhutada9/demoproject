/**
 * Created by MPlatonov on 06.02.2019.
 */
public without sharing class VT_R2_PhoneHelper {
    // validate Patient or PI can not have more than
    //
    // 2 mobile phones
    public static void validateMobilePhoneUnits(List <VT_D1_Phone__c> phones, Map <Id, VT_D1_Phone__c> oldMap) {
        Set <Id> contactIds = new Set<Id>();
        for (VT_D1_Phone__c phone : phones) {
            if (phone.VTD1_Contact__c != null && phone.Type__c == 'Mobile'
                    && !(Trigger.isInsert && phone.VTR4_Conversion_Process__c)) {
                contactIds.add(phone.VTD1_Contact__c);
            }
        }
        if (contactIds.isEmpty()) {
            return;
        }
        List <AggregateResult> results = [select count(Id),
                VTD1_Contact__c from VT_D1_Phone__c where VTD1_Contact__c in : contactIds and Type__c = 'Mobile'
        and VTD1_Contact__r.RecordType.DeveloperName in ('Patient', 'PI','VTR2_SCR', 'Caregiver') group by VTD1_Contact__c having count(Id) > 2];
        System.debug('validatePatientPhones ' + results.size());
        if (!results.isEmpty()) {
            if (contactIds.size() > 1) {
                throw new DmlException(Label.VTR2_UpToTwoMobilePhones);
            } else {
                phones[0].Type__c.addError(Label.VTR2_UpToTwoMobilePhones);
            }
        }
    }
    public static void handleOptInFlag(List <VT_D1_Phone__c> phones, Map <Id, VT_D1_Phone__c> oldMap) {
        for (VT_D1_Phone__c phone : phones) {
            //System.debug('phone = ' + phone + ' ' + oldMap.get(phone.Id));
            if (phone.VTR2_OptInFlag__c && (oldMap == null || !oldMap.get(phone.Id).VTR2_OptInFlag__c)) {
                phone.VTR2_SMS_Opt_In_Status__c = 'Pending';
                System.debug('changed phone = ' + phone);
            }
        }
    }
    public static void validationMobilePhone(List <VT_D1_Phone__c> phones, Map <Id, VT_D1_Phone__c> oldMap){
        Set <String> VTR2_NumberWithoutDigitsName = new Set<String>();
        System.debug('Trigger.new' + phones);
            for (VT_D1_Phone__c vtd1Phone : phones){
                System.debug('trigger phone '+vtd1Phone);
                if(vtd1Phone.Type__c == 'Mobile'){
                    VTR2_NumberWithoutDigitsName.add(vtd1Phone.VTR2_NumberWithoutDigits__c);
                }
            }
        System.debug('in Validation' + VTR2_NumberWithoutDigitsName);
        if(!VTR2_NumberWithoutDigitsName.isEmpty()){
            System.debug('in "if" block');
            List<VT_D1_Phone__c> phoneList = [
                    SELECT  Id,
                            PhoneNumber__c,
                            VTR2_NumberWithoutDigits__c
                    FROM    VT_D1_Phone__c
                    WHERE   VTR2_NumberWithoutDigits__c IN: VTR2_NumberWithoutDigitsName
                    AND     Type__c = 'Mobile'
            ];
            if(!phoneList.isEmpty()){
                System.debug('In phoneList is not empty');
                for(VT_D1_Phone__c phone : phoneList){
                    System.debug('PhoneList ' + phoneList);
                    System.debug('phone :' + phone);
                    for(VT_D1_Phone__c trigersPhone : phones){
                        System.debug('trigersPhone: ' + trigersPhone);
                        System.debug('TrigersList '+phones);
                        if(!(phone.Id==trigersPhone.Id) && phone.VTR2_NumberWithoutDigits__c.equals(trigersPhone.VTR2_NumberWithoutDigits__c)){
                            System.debug('Inter in equals block');
                            trigersPhone.PhoneNumber__c.addError(Label.VTR2_ValidationSameMobileNumber);
                        }
                    }
                }
            }
        }
    }
    public static void setDefaultTypeIfBlank(List<VT_D1_Phone__c> phones) {
        for (VT_D1_Phone__c phone : phones) {
            if (phone.Type__c == null) {
                phone.Type__c = 'Home';
            }
        }
    }
    // TAKEN FROM VT_D1_SendSubject_Phone TRIGGER
    public static void sendSubjectPhone(List<VT_D1_Phone__c> phones, Map<Id, VT_D1_Phone__c> oldMap) {
        Set<String> contactTypes = new Set<String>{'Caregiver', 'Patient'};
        for (VT_D1_Phone__c newPhone : phones) {
            VT_D1_Phone__c oldPhone = oldMap.get(newPhone.Id);
            if ((oldPhone.PhoneNumber__c != newPhone.PhoneNumber__c || oldPhone.Type__c != newPhone.Type__c)
                    && contactTypes.contains(newPhone.VTR4_ContactType__c)
                    && VT_R4_ConstantsHelper_AccountContactCase.SEND_SUBJECT_CASE_STATUSES.contains(newPhone.VTR4_Case_Status__c)
                    && newPhone.VTR4_Case_Id__c != null) {
                VT_D1_QAction_SendSubject action = new VT_D1_QAction_SendSubject(newPhone.VTR4_Case_Id__c);
                if (System.isBatch() || System.isFuture()) {
                    action.addToQueue();
                } else {
                    action.executeFuture();
                }
            }
        }
    }
    public static void validatePrimaryPhone(List <VT_D1_Phone__c> newPhones, Map <Id, VT_D1_Phone__c> oldPhones) {
        List <Id> contactIds = new List <Id>();
        for (VT_D1_Phone__c phone : newPhones) {
            if (phone.VTD1_Contact__c != null && phone.IsPrimaryForPhone__c == true) {
                contactIds.add(phone.VTD1_Contact__c);
            }
        }
        if (!contactIds.isEmpty()) {
            List <AggregateResult> cgPhones = [
                    SELECT COUNT(Id), VTD1_Contact__c
                    FROM VT_D1_Phone__c
                    WHERE VTD1_Contact__c =: contactIds
                    AND IsPrimaryForPhone__c = true
                    GROUP BY VTD1_Contact__c
                    HAVING COUNT(Id) > 1
            ];
            if (!cgPhones.isEmpty()) {
                if (contactIds.size() > 1) {
                    throw new DmlException('PrimaryPhone is limited one');
                } else {
                    newPhones[0].IsPrimaryForPhone__c.addError(Label.VTR4_OnePrimaryPhone);
                }
            }
        }
    }
    public static void dropOptStatusOnNumberChange(List <VT_D1_Phone__c> newPhones, Map <Id, VT_D1_Phone__c> oldPhones) {
        for (VT_D1_Phone__c newPhone : newPhones) {
            VT_D1_Phone__c oldPhone = oldPhones.get(newPhone.Id);
            if ((oldPhone.PhoneNumber__c != newPhone.PhoneNumber__c) && oldPhone.VTR2_SMS_Opt_In_Status__c == 'Pending') {
                newPhone.VTR2_SMS_Opt_In_Status__c = 'Inactive';
                newPhone.VTR2_OptInFlag__c = false;
            }
        }
    }
}