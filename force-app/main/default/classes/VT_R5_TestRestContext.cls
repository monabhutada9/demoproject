/**
 * Created by Yuliya Yakushenkova on 10/8/2020.
 */

@IsTest
public class VT_R5_TestRestContext {

    public static String doTest(User usr, MockRestContext context) {
        System.debug('Mock Rest Test Context: ' + JSON.serializePretty(context));

        Test.startTest();

        RestContext.response = new RestResponse();
        System.runAs(usr) {
            RestContext.request = context.setRestContext();
            context.doRequest();
        }
        Test.stopTest();
        System.debug(RestContext.response.responseBody.toString());
        System.assertNotEquals(500, RestContext.response.statusCode, 'The status code of the response should not be 500');
        return RestContext.response.responseBody.toString();
    }

    public class MockRestContext {
        private String method = 'GET';
        private String URI;
        private Map<String, String> params;
        private Blob body;

        public MockRestContext(String URI, Map<String, String> params) {
            this(null, URI, params, null);
        }

        public MockRestContext(String method, String URI, Map<String, String> params) {
            this(method, URI, params, null);
        }

        public MockRestContext(String method, String URI, Object objectBody) {
            this(method, URI, null, objectBody);
        }

        public MockRestContext(String method, String URI, Map<String, String> params, Object objectBody) {
            this.URI = URI;
            this.params = params;
            if (method != null) this.method = method;
            if (objectBody != null) {
                this.body = Blob.valueOf(JSON.serialize(objectBody, false));
            } else {
                this.body = Blob.valueOf('');
            }
        }

        private RestRequest setRestContext() {
            RestContext.request = new RestRequest();
            RestContext.request.requestURI = URI;
            RestContext.request.httpMethod = method;
            RestContext.request.requestBody = body;

            if (params != null) {
                for (String key : params.keySet()) {
                    RestContext.request.params.put(key, params.get(key));
                }
            }
            return RestContext.request;
        }

        private void doRequest() {
            if (method == 'GET') {
                VT_R5_MobileRouter.doGET();
            } else if (method == 'POST') {
                VT_R5_MobileRouter.doPOST();
            } else if (method == 'PATCH') {
                VT_R5_MobileRouter.doPATCH();
            } else if (method == 'PUT') {
                VT_R5_MobileRouter.doPUT();
            } else if (method == 'DELETE') {
                VT_R5_MobileRouter.doDELETE();
            }
        }
    }
}