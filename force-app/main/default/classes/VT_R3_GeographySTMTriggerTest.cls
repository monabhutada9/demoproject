/**
 *  @author: Galiya Khalikova
 *  @date: 14-Oct-19
 *  @description: Test for VT_R3_Geography_STM_Handler class
 */
@IsTest
private class VT_R3_GeographySTMTriggerTest {
    @testSetup static void setup() {
        DomainObjects.Study_t study = createStudy();
        DomainObjects.VTD2_Study_Geography_t sg = createStudyGeography(study);
        DomainObjects.StudyTeamMember_t rs = createRegulatorySpecialist(study);

        VTR3_Geography_STM__c GSTM = new VTR3_Geography_STM__c(
                Study_Geography__c = sg.Id,
                Is_Primary__c = false,
                VTR3_STM__c = rs.id
        );

        insert GSTM;
    }

    @IsTest
    static void insertFirstFalseGSTMSuccess() {

        System.assertEquals(1, [SELECT Id FROM VTR3_Geography_STM__c].size());
        System.assertEquals(true, [SELECT Is_Primary__c FROM VTR3_Geography_STM__c LIMIT 1].Is_Primary__c);
    }

    @IsTest
    static void insertTwoFalseGSTMSuccess() {

        VTR3_Geography_STM__c gstm_setup = [SELECT Study_Geography__c,VTR3_STM__c FROM VTR3_Geography_STM__c];

        Test.startTest();
        List<VTR3_Geography_STM__c> GSTMs = new List<VTR3_Geography_STM__c>();
        GSTMs.add(new VTR3_Geography_STM__c(
                Study_Geography__c = gstm_setup.Study_Geography__c,
                Is_Primary__c = false,
                VTR3_STM__c = gstm_setup.VTR3_STM__c
        ));

        GSTMs.add(new VTR3_Geography_STM__c(
                Study_Geography__c = gstm_setup.Study_Geography__c,
                Is_Primary__c = false,
                VTR3_STM__c = gstm_setup.VTR3_STM__c
        ));

        insert GSTMs;
        Test.stopTest();

        System.assertEquals(3, [SELECT Id FROM VTR3_Geography_STM__c].size());
        System.assertEquals(true, [SELECT Is_Primary__c FROM VTR3_Geography_STM__c LIMIT 1].Is_Primary__c);
    }

    @IsTest
    static void insertTwoTrueGSTMSuccess() {
        VTR3_Geography_STM__c gstm_setup = [SELECT Study_Geography__c,VTR3_STM__c FROM VTR3_Geography_STM__c];

        Test.startTest();
        VTR3_Geography_STM__c GSTM_true = new VTR3_Geography_STM__c(
                Study_Geography__c = gstm_setup.Study_Geography__c,
                Is_Primary__c = true,
                VTR3_STM__c = gstm_setup.VTR3_STM__c);
        insert GSTM_true;
        Test.stopTest();

        System.assertEquals(2, [SELECT Id FROM VTR3_Geography_STM__c].size());
        System.assertEquals(false, [SELECT Is_Primary__c FROM VTR3_Geography_STM__c Order by CreatedDate LIMIT 1].Is_Primary__c);
    }

    @IsTest
    static void updateGSTMSuccess() {
        VTR3_Geography_STM__c gstm_setup = [SELECT Study_Geography__c,VTR3_STM__c FROM VTR3_Geography_STM__c];

        VTR3_Geography_STM__c gstm_False = new VTR3_Geography_STM__c(
                Study_Geography__c = gstm_setup.Study_Geography__c,
                Is_Primary__c = false,
                VTR3_STM__c = gstm_setup.VTR3_STM__c);
        insert gstm_False;

        VTR3_Geography_STM__c gstm = [
                SELECT Name, Id, Is_Primary__c,VTR3_STM__c
                FROM VTR3_Geography_STM__c
                WHERE Is_Primary__c = false
        ];

        String dmlMessage;
        Test.startTest();
        gstm.Is_Primary__c = true;
        update gstm;
        Test.stopTest();

        System.assertEquals(null, dmlMessage);
        System.assertEquals(1, [
                SELECT Is_Primary__c
                FROM VTR3_Geography_STM__c
                WHERE Is_Primary__c = true
        ].size());
    }

    @IsTest
    static void updateGSTMFail() {

        VTR3_Geography_STM__c gstm = [
                SELECT Name, Id, Is_Primary__c,VTR3_STM__c
                FROM VTR3_Geography_STM__c
                WHERE Is_Primary__c = true
        ];

        String dmlMessage;
        Test.startTest();
        try {
            gstm.Is_Primary__c = false;
            update gstm;

        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        Test.stopTest();

        System.assertEquals(Label.VTR3_GSTM_Should_Have_Primary_RS, dmlMessage);
        System.assertEquals(1, [ SELECT Name, Id, Is_Primary__c,VTR3_STM__c
        FROM VTR3_Geography_STM__c
        WHERE Is_Primary__c = true].size());
    }

    @IsTest
    static void deleteGSTMSuccess() {
        VTR3_Geography_STM__c gstm_setup = [SELECT Study_Geography__c,VTR3_STM__c FROM VTR3_Geography_STM__c];

        VTR3_Geography_STM__c gstm = new VTR3_Geography_STM__c(
                Study_Geography__c = gstm_setup.Study_Geography__c,
                Is_Primary__c = false,
                VTR3_STM__c = gstm_setup.VTR3_STM__c
        );
        insert gstm;

        String dmlMessage;
        Test.startTest();
        delete [SELECT Name, Id, Is_Primary__c,VTR3_STM__c
        FROM VTR3_Geography_STM__c
        WHERE Is_Primary__c = false];
        Test.stopTest();

        System.assertEquals(null, dmlMessage);
        System.assertEquals(1, [
                SELECT Is_Primary__c
                FROM VTR3_Geography_STM__c
        ].size());
    }

    @IsTest
    static void deleteGSTMSFail() {

        String dmlMessage;
        Test.startTest();
        try {
            delete [ SELECT Name, Id, Is_Primary__c,VTR3_STM__c
            FROM VTR3_Geography_STM__c];
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        Test.stopTest();

        System.assertEquals(Label.VTR3_GSTM_Should_Have_Primary_RS, dmlMessage);
        System.assertEquals(1, [
                SELECT Is_Primary__c
                FROM VTR3_Geography_STM__c
        ].size());
    }

    private static DomainObjects.Study_t createStudy() {

        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setName('Study Name')
                /*.addPMA(new DomainObjects.User_t()
                        .addContact(new DomainObjects.Contact_t())
                        .setProfile('Site Coordinator'))*/
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));
        return study;
    }

    private static DomainObjects.StudyTeamMember_t createRegulatorySpecialist (DomainObjects.Study_t study){

        DomainObjects.StudyTeamMember_t regSpec = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('RS')
                .addUser(new DomainObjects.User_t()
                        .setProfile('Regulatory Specialist'))
                .addStudy(study);
        regSpec.persist();
        System.debug('regSpec: ' + regSpec);
        return  regSpec;
    }

    private static DomainObjects.VTD2_Study_Geography_t createStudyGeography(DomainObjects.Study_t study) {

        DomainObjects.VTD2_Study_Geography_t studyGeographyT = new DomainObjects.VTD2_Study_Geography_t(study)
                .addVTD2_Study(study);

        //create study
        //VTD2_Study_Geography__c studyGeographyT = new VTD2_Study_Geography__c();
        //studyGeographyT.VTD2_Study__c = study.id;
        //insert studyGeographyT;

        System.debug('studyGeographyT: ' + studyGeographyT);

        return studyGeographyT;
        //return new DomainObjects.VTD2_Study_Geography_t(study)
        //        .addVTD2_Study(study);
    }

}