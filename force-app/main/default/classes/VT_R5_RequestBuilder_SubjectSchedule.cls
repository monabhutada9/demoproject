/**
* @author: Carl Judge
* @date: 17-Aug-20
**/

public without sharing class VT_R5_RequestBuilder_SubjectSchedule implements VT_D1_RequestBuilder{
    public List<SubjectDetail> subjectDetails = new List<SubjectDetail>();
    public Integer delay;

    public class SubjectDetail {
        Id userId;
        Id clinicalStudyMembership;
        String orgGuid = eCOA_IntegrationDetails__c.getInstance().eCOA_OrgGuid__c;
        String studyGuid;
        String subjectGuid;
        Integer daysBegin = 0;
        Integer daysEnd = VT_R4_ConstantsHelper_Protocol_ePRO.DIARY_SETTINGS.VTR5_ScheduleRequestDaysEnd__c.intValue();
        Boolean stopDiary;
    }

    public VT_R5_RequestBuilder_SubjectSchedule(List<Id> userIds, Boolean isStopEvent, Integer delay) {
        for (User u : [
            SELECT Id,
                Contact.VTD1_Clinical_Study_Membership__c,
                Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eCOA_Guid__c,
                VTR5_eCOA_Guid__c
            FROM User
            WHERE Id IN :userIds
        ]) {
            SubjectDetail detail = new SubjectDetail();
            detail.userId = u.Id;
            detail.clinicalStudyMembership = u.Contact.VTD1_Clinical_Study_Membership__c;
            detail.studyGuid = u.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eCOA_Guid__c;
            detail.subjectGuid = u.VTR5_eCOA_Guid__c;
            detail.stopDiary = isStopEvent;
            this.subjectDetails.add(detail);
        }
        this.delay = delay;
    }

    public String buildRequestBody() {
        return JSON.serialize(this);
    }
}