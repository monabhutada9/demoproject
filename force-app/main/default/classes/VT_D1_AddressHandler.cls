/**
 * Created by user on 06.08.2018.
 */

public with sharing class VT_D1_AddressHandler {
    public static void onAddressUpdateBefore(List <Address__c> addresses, Map <Id, Address__c> oldMap) {
        for (Address__c tNew: addresses) {
            // GET VALUES BEFORE UPDATE (CURRENT VALUES)
            Address__c tOld = oldMap.get(tNew.Id);

            // CHECK IF  ADDRESS HAS BEEN CHANGED
            If(tNew.AddressLastVerified__c == tOld.AddressLastVerified__c && (tNew.Name != tOld.Name ||
                    tNew.City__c != tOld.City__c || tNew.State__c != tOld.State__c || tNew.ZipCode__c != tOld.ZipCode__c)) {
                // SET THE  ADDRESS AS NOT HAVING BEEN VERIFIED
                tNew.Address_Verified__c = FALSE;
                If (tNew.Address_Return_Code__c != NULL) tNew.Address_Return_Code__c = 'Changed';
            }
        }
    }
    public static void onAddressUpdateAfter(List <Address__c> addresses, Map <Id, Address__c> oldMap) {
        System.debug('onAddressUpdateAfter ');
        List <Address__c> filtered = new List<Address__c>();
        Set <Id> contactIds = new Set<Id>();
        for (Address__c address : addresses) {
            if (address.VTD1_Contact__c == null)
                continue;
            Address__c oldAddress = oldMap.get(address.Id);
            if (address.VTD1_AddressType__c != oldAddress.VTD1_AddressType__c ||
                    address.VTD1_Primary__c != oldAddress.VTD1_Primary__c ||
                    address.Name != oldAddress.Name ||
                    address.VTD1_Address2__c != oldAddress.VTD1_Address2__c ||
                    address.City__c != oldAddress.City__c ||
                    address.State__c != oldAddress.State__c ||
                    address.ZipCode__c != oldAddress.ZipCode__c ||
                    address.Country__c != oldAddress.Country__c /*||
                    address.VTD1_Contact__c != oldAddress.VTD1_Contact__c || (
                    (address.VTD1_Contact__c != null && oldAddress.VTD1_Contact__c != null) && (
                            address.VTD1_Contact__r.FirstName != oldAddress.VTD1_Contact__r.FirstName ||
                                    address.VTD1_Contact__r.LastName != oldAddress.VTD1_Contact__r.LastName ||
                                    address.VTD1_Contact__r.Email != oldAddress.VTD1_Contact__r.Email ||
                                    address.VTD1_Contact__r.MobilePhone != oldAddress.VTD1_Contact__r.MobilePhone ||
                                    address.VTD1_Contact__r.Phone != oldAddress.VTD1_Contact__r.Phone
                    ))*/) {
                filtered.add(address);
                contactIds.add(address.VTD1_Contact__c);
            }
        }
        System.debug('contact ids = ' + contactIds);
        List <String> caseStatuses = new List<String>{VT_R4_ConstantsHelper_AccountContactCase.CASE_CONSENTED,
                VT_R4_ConstantsHelper_AccountContactCase.CASE_LOST_TO_FOLLOW_UP,
                VT_R4_ConstantsHelper_AccountContactCase.CASE_SCREENED,
                VT_R4_ConstantsHelper_AccountContactCase.CASE_SCREEN_FAILURE,
                VT_R4_ConstantsHelper_AccountContactCase.CASE_WASHOUT_RUN_IN,
                VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED,
                VT_R4_ConstantsHelper_AccountContactCase.CASE_DROPPED,
                VT_R4_ConstantsHelper_AccountContactCase.CASE_COMPLETED
        };
        List <Case> cases = [select Id from Case where ContactId in : contactIds and Status in : caseStatuses];
        System.debug('cases = ' + cases);
        for (Case caseObj : cases) {
            VT_D1_QAction_SendSubject sendSubjectAction = new VT_D1_QAction_SendSubject(caseObj.Id);
            sendSubjectAction.executeFuture();
        }
    }

//    public static void insertAddresses (List<Address__c> recs, Map<Id, Address__c> oldMap, Map<Id, Address__c> newMap) {
//
//        Set<id> adrIds = new Set<Id>();
//
//        for (Address__c v : recs) {
//            if (Trigger.isUpdate) {
//                if ((oldMap.get(v.id).VTD1_Primary__c != newMap.get(v.id).VTD1_Primary__c)
//                    && (newMap.get(v.id).VTD1_Primary__c)) {
//                adrIds.add(v.Id);
//            }}
//            else adrIds.add(v.Id);
//        }
//
//
//        if (!adrIds.isEmpty()) {
//
//            Map<id, id> caseIds = new Map<Id, Id>();
//            for (Address__c a : [
//                    SELECT VTD1_Contact__r.VTD1_Clinical_Study_Membership__r.Id
//                    FROM Address__c
//                    WHERE Id in:adrIds
//            ]) {
//                caseIds.put(a.VTD1_Contact__r.VTD1_Clinical_Study_Membership__r.Id,a.Id);
//            }
//
//            List<VTD1_Order__c> patientDeliveries = [
//                    SELECT Id, VTR2_Patient_Shipping_Address__c, VTD1_Case__c
//                    FROM VTD1_Order__c
//                    WHERE ((VTD1_Case__c in:caseIds.keySet()) AND
//                    (VTD1_Status__c = 'Not Started' OR VTD1_Status__c = 'Pending Shipment')
//                    AND (VTR2_Destination_Type__c = 'Patient'))
//            ];
//
//            System.debug('~~~~~~~~~~~~~~~~Is It Empty??' + patientdeliveries);
//
//            if (!patientDeliveries.isEmpty()) {
//            for (VTD1_Order__c order : patientDeliveries) {
//                        order.VTR2_Patient_Shipping_Address__c = caseIds.get(order.VTD1_Case__c);
//            }
//
//            update patientDeliveries;}
//        }
//    }
}