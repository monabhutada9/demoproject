/**
 * @author:         Rishat Shamratov
 * @date:           17.07.2020
 * @description:    Controller class for VT_R5_AccountDeactivatedEmail component
 */

public class VT_R5_AccountDeactivatedEmailController {
    public Id patientCaseId { get; set; }

    public class MessageBodyWrapper {
        public String sponsorName { get; set; }
        public String sponsorLogo { get; set; }
        public String noLongerAnActiveParticipant { get; set; }
        public String completionOrDeactivationMessage { get; set; }
    }

    public MessageBodyWrapper getMessageBody() {
        MessageBodyWrapper messBody = new MessageBodyWrapper();
        List<Case> patientCases = [
                SELECT Id,
                        VTD1_Patient_User__r.LanguageLocaleKey,
                        VTD1_Study__c,
                        VTD1_Study__r.VTD1_Protocol_Nickname__c,
                        VTD1_Study__r.VTR5_Completion_or_Deactivation_Message__c,
                        VTD1_Study__r.VTR5_Sponsor_Name__c,
                        VTD1_Study__r.VTR5_Sponsor_Logo__c
                FROM Case
                WHERE Id = :patientCaseId
        ];
        String language = patientCases[0].VTD1_Patient_User__r.LanguageLocaleKey;

        VT_D1_TranslateHelper.translate(patientCases, language);
        Case patientCase = patientCases[0];


        messBody.sponsorName = patientCase.VTD1_Study__r.VTR5_Sponsor_Name__c;
        messBody.sponsorLogo = changeSponsorLogoUrl(patientCase.VTD1_Study__r.VTR5_Sponsor_Logo__c);
        messBody.noLongerAnActiveParticipant = VT_D1_TranslateHelper.getLabelValue('VTR5_NoLongerAnActiveParticipantOnStudy', language).replaceAll('#StudyName', patientCase.VTD1_Study__r.VTD1_Protocol_Nickname__c);
        String translationString = getRecordTranslation(language, patientCase.VTD1_Study__c, 'VTR5_Completion_or_Deactivation_Message__c').VTR2_Value__c;
        messBody.completionOrDeactivationMessage = translationString != null ? translationString : patientCase.VTD1_Study__r.VTR5_Completion_or_Deactivation_Message__c;

        return messBody;
    }

    private VTD1_Translation__c getRecordTranslation(String lang, Id recordId, String recordFieldName) {
        VTD1_Translation__c translation = new VTD1_Translation__c();
        try{
            translation = [
                    SELECT Id,
                        VTD1_Field_Name__c,
                        VTD1_Language__c,
                        VTD1_Object_Name__c,
                        VTD1_Record_Id__c,
                        VTD1_Value__c,
                        VTR2_Value__c
                FROM VTD1_Translation__c
                WHERE VTD1_Language__c = :lang
                    AND VTD1_Record_Id__c = :recordId
                    AND VTD1_Field_Name__c = :recordFieldName
                LIMIT 1
            ];
        } catch(Exception e) {
            System.debug(e);
        }

        return translation;
    }

    private String changeSponsorLogoUrl (String sponsorLogoUrl){
        String newSponsorLogoUrl;
        try {
            Site pubAccImaSite = [SELECT Id FROM Site WHERE Name = 'draftFilesList' LIMIT 1];
            SiteDetail pubAccImaSiteDetail = [SELECT SecureUrl FROM SiteDetail WHERE DurableId = :pubAccImaSite.Id LIMIT 1];
            String sponsorLogoInfo = sponsorLogoUrl.unescapeHtml4().substringBetween('force.com', '"');
            newSponsorLogoUrl = pubAccImaSiteDetail.SecureUrl + sponsorLogoInfo;
            if (Test.isRunningTest()) {
                throw new NoDataFoundException();
            }
        } catch (Exception e) {
            System.debug(e.getMessage());
        }

        return newSponsorLogoUrl;
    }
}