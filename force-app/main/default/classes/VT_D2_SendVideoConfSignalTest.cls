@IsTest
private class VT_D2_SendVideoConfSignalTest {
    public class MockSendVideoConfSignalHttpResponseGenerator implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('["test"]');
            res.setStatusCode(200);
            return res;
        }
    }

    @testSetup
    static void setup() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.startTest();
        //HealthCloudGA__CandidatePatient__c candidatePatient = VT_D1_TestUtils.createPatientCandidate('Mike', 'Birn', 'mikebirn@test.com', 'mikebirn@test.com', 'mikebirn', 'Converted', study.Id, 'PN', '11-11-11', 'US', 'NY');
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }

    @IsTest
    static void sendSignalTest() {
        List<Case> casesList = [SELECT Id, VTD1_Primary_PG__c, VTD1_PI_user__c FROM Case];
        System.assert(casesList.size() > 0);
        if (casesList.size() > 0) {
            Case cas = casesList.get(0);

            Video_Conference__c videoConference = new Video_Conference__c();
            insert videoConference;

            VTD1_Conference_Member__c conferenceMember1 = new VTD1_Conference_Member__c();
            conferenceMember1.VTD1_Video_Conference__c = videoConference.Id;
            conferenceMember1.VTD1_User__c = cas.VTD1_Primary_PG__c;
            insert conferenceMember1;

            VTD1_Conference_Member__c conferenceMember2 = new VTD1_Conference_Member__c();
            conferenceMember2.VTD1_Video_Conference__c = videoConference.Id;
            conferenceMember2.VTD1_User__c = cas.VTD1_PI_user__c;
            insert conferenceMember2;

            VTD1_Video_Conf_Connection__c videoConfConnection = new VTD1_Video_Conf_Connection__c();
            videoConfConnection.VTD1_Video_ConfId__c = videoConference.Id;
            videoConfConnection.VTD1_Time_Connection__c = Datetime.now();
            videoConfConnection.VTD1_Participant__c = cas.VTD1_Primary_PG__c;
            insert videoConfConnection;

            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockSendVideoConfSignalHttpResponseGenerator());
            List<Id> videoConfIdsList = new List<Id>();
            videoConfIdsList.add(videoConference.Id);
            Integer quantityDMLStatementsBefore = Limits.getDMLStatements();
            VT_D1_SendVideoConfSignal.sendSignalToNotConnectedParticipants(videoConfIdsList);
            Integer quantityDMLStatementsAfter = Limits.getDMLStatements();
            System.assertEquals(quantityDMLStatementsBefore + 1, quantityDMLStatementsAfter);
            Test.stopTest();
        }
    }
}