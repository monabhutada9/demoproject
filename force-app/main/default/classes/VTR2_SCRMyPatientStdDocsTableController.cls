public with sharing class VTR2_SCRMyPatientStdDocsTableController {

    @AuraEnabled(Cacheable=true)
    public static Boolean isDownloadAllowed() {

        List<String> allowedProfiles = new List<String>{'Site Coordinator', 'Patient Guide'};

        Profile currentUserProfile = [
                SELECT Name
                FROM Profile
                WHERE Id = :UserInfo.getProfileId()
        ];

        return allowedProfiles.contains(currentUserProfile.Name);
    }

}