/**
* @author: Carl Judge
* @date: 16-Jul-20
* @description: Class to modify a VT_R5_eCoaApiRequest before being sent so that it will work with point to point setup
**/

public without sharing class VT_R5_eCoaPointToPointHelper {
    private static final Map<String, String> ENDPOINT_OVERRIDES = new Map<String, String> {
        VT_R4_ConstantsHelper_Integrations.ENDPOINT_URL_ECOA_RESPONSES => '/api/v1/orgs/:orgGuid/studies/:studyGuid/responseExport/exportLegacy',
        VT_R4_ConstantsHelper_Integrations.ENDPOINT_URL_ECOA_SUBJECT_CREATE => '/api/v1/orgs/:orgGuid/studies/:studyGuid/subjects',
        VT_R4_ConstantsHelper_Integrations.ENDPOINT_URL_ECOA_SUBJECT_UPDATE => '/api/v1/orgs/:orgGuid/studies/:studyGuid/subjects/:subjectGuid',
        VT_R4_ConstantsHelper_Integrations.ENDPOINT_URL_ECOA_SUBJECT_UPDATE_BLIND => '/api/v1/orgs/:orgGuid/studies/:studyGuid/subjects/:subjectGuid/blind',
        VT_R4_ConstantsHelper_Integrations.ENDPOINT_URL_ECOA_SUBJECT_ADD_EVENT => '/api/v1/orgs/:orgGuid/studies/:studyGuid/subjects/:subjectGuid/addEventDate'
    };

    public static VT_D1_HTTPConnectionHandler.Result sendRequest(VT_R5_eCoaApiRequest request) {
        Map<String, String> headerMap = request.getHeaders();
        headerMap.put('Authorization', 'Bearer ' + VT_R5_eCoaAuthHelper.getToken());
        headerMap.put('Av-Origin-Guid', 'origin_' + VT_R5_eCoaAuthHelper.getSessionId());
        headerMap.put('Cookie', VT_R5_eCoaAuthHelper.getCookie());

        String endpoint = request.getEndpoint();
        VT_D1_RequestBuilder body = request.getBody();

        if (ENDPOINT_OVERRIDES.containsKey(endpoint)) {
            request.setEndpoint(eCOA_IntegrationDetails__c.getInstance().eCOA_Endpoint__c + ENDPOINT_OVERRIDES.get(endpoint));
        } else {
            request.setEndpoint(eCOA_IntegrationDetails__c.getInstance().eCOA_Endpoint__c + endpoint);
        }

        if (endpoint == VT_R4_ConstantsHelper_Integrations.ENDPOINT_URL_ECOA_RESPONSES) {
            request.setMethod('GET');
            Map<String, Object> bodyMap = (Map<String, Object>)JSON.deserializeUntyped(body.buildRequestBody());
            String queryDesc = JSON.serialize(bodyMap.get('queryDesc'), true);
            request.setEndpoint(request.getEndpoint() + '?queryDesc=' + EncodingUtil.urlEncode(queryDesc,'UTF-8') + '&format=' + VT_R4_ConstantsHelper_Protocol_ePRO.DIARY_SETTINGS.VTR5_DiaryResponseFormat__c);
            body = null;
        }
        else if (endpoint == VT_R4_ConstantsHelper_Integrations.ENDPOINT_URL_ECOA_SUBJECT_CREATE) {
            VT_D1_HTTPConnectionHandler.Result versionResult = VT_R5_eCoaStudyVersion.getStudyVersion(request.getStudyGuid());
            VT_D1_HTTPConnectionHandler.Result rulesResult = VT_R5_eCoaRules.getStudyRules(request.getStudyGuid());
            Map<String, String> rulesMap = VT_R5_eCoaRuleResponseObject.getRulesMap(rulesResult.httpResponse.getBody());
            body = new BodyExtender(request.getBody())
                .addKeyVal('studyVersionGuid', VT_R5_eCoaStudyVersion.getVersionGuidFromVersionResponse(versionResult))
                .addKeyVal('eproRuleSetGuid', rulesMap.get('epro'))
                .addKeyVal('clinroRuleSetGuid', rulesMap.get('clinro'));
        }
        else if (endpoint == VT_R4_ConstantsHelper_Integrations.ENDPOINT_URL_ECOA_SUBJECT_UPDATE) {
            VT_D1_HTTPConnectionHandler.Result versionResult = VT_R5_eCoaStudyVersion.getStudyVersion(request.getStudyGuid());
            body = new BodyExtender(request.getBody())
                .addKeyVal('studyVersionGuid', VT_R5_eCoaStudyVersion.getVersionGuidFromVersionResponse(versionResult));
        }

        return VT_D1_HTTPConnectionHandler.send(request.getMethod(), request.getMergedEndpoint(), body, request.getAction(), headerMap, request.getSkipLogging());
    }

    public class BodyExtender implements VT_D1_RequestBuilder {
        private Map<String, Object> bodyMap;

        public BodyExtender(VT_D1_RequestBuilder bodyBuilder) {
            this.bodyMap = (Map<String, Object>)JSON.deserializeUntyped(bodyBuilder.buildRequestBody());
        }

        public BodyExtender addKeyVal(String key, Object val){
            this.bodyMap.put(key, val);
            return this;
        }

        public String buildRequestBody() {
            return JSON.serialize(this.bodyMap);
        }
    }
}