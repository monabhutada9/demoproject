/**
 * Created by shume on 8/5/2020.
 * Logic from Process : PCF Safety Concern Indicator Status automation
 */

public without sharing class VT_R5_PCF_Handler extends Handler {

    private static final String ID_CASE_PCF = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF;
    private static final String ID_CASE_PCF_READONLY = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF_READONLY;
    private static final String ID_CAREPLAN = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN;
    private static final String OPEN = VT_R4_ConstantsHelper_AccountContactCase.CASE_OPEN;
    private static final String CLOSED = VT_R4_ConstantsHelper_AccountContactCase.CASE_CLOSED;

    private static final List<String> LAST_MODIFIED_PROFILES = new List<String> {
            VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME,
            VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME
    };
    private static final List<String> LAST_MODIFIED_PROFILES_IDS = new List<String> {
            VT_D1_HelperClass.getProfileIdByName('Primary Investigator'),
            VT_D1_HelperClass.getProfileIdByName('Study Concierge')
    };
    private static final List<String> PROFILES_FOR_NOTIFICATION_C = new List<String>{
            VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME,
            VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME
    };
    private static final List<String> LM_PROFILES_FOR_NOTIFICATION = new List<String>{
            VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME,
            VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME,
            VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME,
            VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME
    };
    public static List<String> tnNotificationCodes = new List<String>();
    public static List<String> tnTaskCodes = new List<String>();
    public static List<Id> notificationSourceIds = new List<Id>();
    public static List<Id> taskSourceIds = new List<Id>();
    private static List<VTD1_PCF_Patient_Contact_Form_Comment__c> pcfPatientContactFormComments = new List<VTD1_PCF_Patient_Contact_Form_Comment__c>();

    protected override void onBeforeInsert(Handler.TriggerContext context){
        /*Case - New PCF`s Status*/
        List<Case> newList = (List<Case>) context.newList;
        Map<Id, Case> clinicalStudyMembershipIds_cases = new Map<Id, Case>();
        Map<Id, Case> owners_cases = new Map<Id, Case>();
        Map<Case, Case> cases_clinicalStudyMemberships = new Map<Case, Case>();
        Map<Case, String> cases_ownersProfileNames = new Map<Case, String>();
        for(Case c: newList){
            if (c.RecordTypeId != VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF) {
                continue;
            }
            if (c.VTD1_Clinical_Study_Membership__c != null) {
                clinicalStudyMembershipIds_cases.put(c.VTD1_Clinical_Study_Membership__c, c);
            }
            owners_cases.put(c.OwnerId, c);
        }
        if (clinicalStudyMembershipIds_cases.isEmpty() && owners_cases.isEmpty()) {
            return;
        }
        for(Case csm : [
              SELECT VTR2_SiteCoordinator__c, VTD1_Primary_PG__c
              FROM Case
              WHERE Id IN :clinicalStudyMembershipIds_cases.keySet()]){
              cases_clinicalStudyMemberships.put(clinicalStudyMembershipIds_cases.get(csm.Id), csm);
        }
        for(User u : [SELECT VTD1_Profile_Name__c FROM User WHERE Id IN :owners_cases.keySet()]){
            cases_ownersProfileNames.put(owners_cases.get(u.Id), u.VTD1_Profile_Name__c);
        }
        for (Case c: newList){
            String newStatus = '';
            String newCallNarrative = '';
            String newOwnerId = '';
            Boolean openAfterReassignment = false;
            if(c.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF && c.Status != 'Closed'){
                newStatus = OPEN;
            }
            if(/*cases_rtNames.get(c) == 'Patient Contact Form'*/
                    c.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF
                    && c.VTD1_PCF_Safety_Concern_Indicator__c != 'No'){
                newCallNarrative = System.UserInfo.getUserId() + ' ' + Date.today() + '\n'
                        + 'VTD1_PCF_Call_Narrative__c';
                openAfterReassignment = true;
            }
            // PCF - Reassign Owner and Notifications: reassign owner
            if(c.VTD1_PCF_Safety_Concern_Indicator__c == 'Possible'
                    && LAST_MODIFIED_PROFILES.contains(cases_ownersProfileNames.get(c))){
                if(cases_clinicalStudyMemberships.get(c).VTR2_SiteCoordinator__c != null){
                    newOwnerId = cases_clinicalStudyMemberships.get(c).VTR2_SiteCoordinator__c;
                } else{
                    newOwnerId = cases_clinicalStudyMemberships.get(c).VTD1_Primary_PG__c;
                }

                if(c.VTD2_PCF_Safety_Concern_Indication_Date__c == null
                        && c.Status == OPEN){
                    c.VTD2_PCF_Safety_Concern_Indication_Date__c = Datetime.now();
                }
            }
            c.Status = !String.isEmpty(newStatus) ? newStatus : c.Status;
            c.VTD1_PCF_Call_Narrative__c = !String.isEmpty(newCallNarrative) ? newCallNarrative : c.VTD1_PCF_Call_Narrative__c;
            c.VTD1_PCF_SC_Open_After_Reassignment__c = openAfterReassignment ? true : false;
            c.OwnerId = !String.isEmpty(newOwnerId) ? (Id) newOwnerId : c.OwnerId;
        }
    }

    protected override void onBeforeUpdate(Handler.TriggerContext context) {
        pcfPatientContactFormComments.clear();
        List<Case> newList = (List<Case>) context.newList;
        Map<Id, Case> oldMap = (Map<Id, Case>) context.oldMap;
        Map<Id, Integer> pcfTaskDueDateUpdate = new Map<Id, Integer> ();
        Map<Id, Case> pcfMap = new Map<Id, Case>();
        for (Case c : newList) {
            if (c.RecordTypeId == ID_CASE_PCF || c.RecordTypeId == ID_CASE_PCF_READONLY) {
                pcfMap.put(c.Id, c);
            }
        }
        if (pcfMap.isEmpty()) {
            return;
        }
        Map<Id, Case> casesMap = getCasesMap(pcfMap.keySet());
        for (Case c : pcfMap.values()) {
            Id userProfileId = UserInfo.getProfileId();
            Case cWithInfo = new Case();
            if(!casesMap.isEmpty()){
                cWithInfo = casesMap.get(c.Id);
            }
            if (c.RecordTypeId == ID_CASE_PCF) {
                /* validation rule: PCF_SCI_Comments_on_Status_Change */
                if (c.VTD1_PCF_Safety_Concern_Indicator__c != oldMap.get(c.Id).VTD1_PCF_Safety_Concern_Indicator__c
                        && (String.isBlank(c.VTD2_Reason_Safety_Concern_Indicator_Cha__c)
                        || c.VTD2_Reason_Safety_Concern_Indicator_Cha__c == 'Required if indicator changed')) {
                    c.VTD2_Reason_Safety_Concern_Indicator_Cha__c.addError('Comments are required when a user changes Safety Concern Indicator.');
                }
                if (c.VTD1_PCF_Safety_Concern_Indicator__c == 'Possible') {
                    if (cWithInfo.VTD1_Clinical_Study_Membership__c != null) {
                        // PCF - Reassign Owner and Notifications: reassign owner
                        if (oldMap.get(c.Id).VTD1_PCF_Safety_Concern_Indicator__c != 'Possible'
                                && LAST_MODIFIED_PROFILES_IDS.contains(userProfileId)) {
                            ///// - validation rule : PCF_SC_SCI_Possible - /////
                            if (userProfileId == VT_D1_HelperClass.getProfileIdByName('Study Concierge')
                                    && c.OwnerId != UserInfo.getUserId()) {
                                c.VTD1_PCF_Safety_Concern_Indicator__c.addError('The Study Concierges can change Safety' +
                                        ' Concern Indicator to \'Possible\' only while they have ownership of the CF');
                            }
                            //////////////////////////////////////////////////////
                            if (cWithInfo.VTD1_Clinical_Study_Membership__r.VTR2_SiteCoordinator__c != null) {
                                c.OwnerId = cWithInfo.VTD1_Clinical_Study_Membership__r.VTR2_SiteCoordinator__c;
                            } else if (cWithInfo.VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c != null) {
                                c.OwnerId = cWithInfo.VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c;
                            }
                        }
                    }
                    if (c.Status == OPEN) {
                        /*PCF Safety Concern Indicator Status automation: Update Assignment Date, Schedule Reminders*/
                        if (c.OwnerId != oldMap.get(c.Id).OwnerId || c.VTD1_Clinical_Study_Membership__c != null) {
                            c.VTR3_PCF_Assignment_Date__c = Datetime.now();
                        }
                        if (c.VTD1_Clinical_Study_Membership__c != null) {
                            if (c.VTD2_PCF_Safety_Concern_Indication_Date__c == null) {
                                c.VTD2_PCF_Safety_Concern_Indication_Date__c = Datetime.now();
                            }
                            if (c.IsEscalated) {
                                c.VTD1_IsEscalatedDateTime__c = System.now();
                                c.VTD1_PCF_Resolution_Deadline__c = c.VTD2_PCF_Safety_Concern_Indication_Date__c.addHours(12);
                                pcfTaskDueDateUpdate.put(c.Id, 2);
                            } else if (cWithInfo.VTD1_Clinical_Study_Membership__r.VTD1_PI_contact__c != null) {
                                c.PCF_SC_Patient_PI_Contact__c = cWithInfo.VTD1_Clinical_Study_Membership__r.VTD1_PI_contact__c;
                                c.VTD1_PCF_Resolution_Deadline__c = c.VTD2_PCF_Safety_Concern_Indication_Date__c.addDays(3);
                                pcfTaskDueDateUpdate.put(c.Id, 24);
                            }
                        }
                    }
                }
                /*PCF Safety Concern Owner Change Immediate Actions: PCF-Safety concern Owner changed*/
                if (c.VTD1_PCF_Safety_Concern_Indicator__c != 'No' && cWithInfo.RecordType.Name != null) {
                    if (cWithInfo.RecordType.Name == 'Patient Contact Form' && cWithInfo.LastModifiedBy.Name != null) {
                        c.VTD1_PCF_Call_Narrative__c = cWithInfo.LastModifiedBy.Name + ' ' + c.LastModifiedDate + '\n'
                                + 'VTD1_PCF_Call_Narrative__c';
                        c.VTD1_PCF_SC_Open_After_Reassignment__c = true;
                    }
                    if (c.Status == OPEN
                            && cWithInfo.RecordType.DeveloperName != null
                            && cWithInfo.RecordType.DeveloperName == 'VTD1_PCF'
                            && c.OwnerId != oldMap.get(c.Id).OwnerId) {
                        c.VTD1_PCF_Reassignment_Date__c = Date.today();
                        c.VTD1_PCF_SC_Open_After_Reassignment__c = false;
                    }
                }
                /*PCF - New Safety Comment*/
                if (c.VTD2_Reason_Safety_Concern_Indicator_Cha__c != oldMap.get(c.Id).VTD2_Reason_Safety_Concern_Indicator_Cha__c
                        && c.VTD2_Reason_Safety_Concern_Indicator_Cha__c != 'Required if indicator changed') {
                    pcfPatientContactFormComments.add(new VTD1_PCF_Patient_Contact_Form_Comment__c(
                            Patient_Contact_Form__c = c.Id,
                            Comment__c = c.VTD2_Reason_Safety_Concern_Indicator_Cha__c,
                            VTD1_PCF_SCI_Change__c = true,
                            VTD1_PCF_SCI_Value_Change__c = c.VTD1_PCF_Safety_Concern_Indicator__c
                    ));
                    c.VTD2_SCI_Reason_Change__c = !c.VTD2_SCI_Reason_Change__c;
                    c.VTD2_Reason_Safety_Concern_Indicator_Cha__c = 'Required if indicator changed';
                }
            }
            if (c.Status != oldMap.get(c.Id).Status) {

                /**PB name : PCF_Page_Access_Management | working dont need advanced*/

                if (c.RecordTypeId == ID_CASE_PCF && c.Status == CLOSED) {
                    c.RecordTypeId = ID_CASE_PCF_READONLY;
                } else if (c.RecordTypeId == ID_CASE_PCF_READONLY && c.Status == OPEN) {
                    c.RecordTypeId = ID_CASE_PCF;
                }
            }
        }
        if(!pcfTaskDueDateUpdate.isEmpty()) {
            taskDueDateUpdate(pcfTaskDueDateUpdate);
        }
    }

    protected override void onAfterInsert(Handler.TriggerContext context) {
        List<Case> newList = (List<Case>) context.newList;
        List<VT_R3_PcfRemindersSender.ParamsHolder> pcfReminders = new List <VT_R3_PcfRemindersSender.ParamsHolder>();
        Map<Id, Case> pcfMap = new Map<Id, Case>();
        Set<Id> caseDoSharing = new Set<Id>();
        for (Case c : newList) {
            if (c.RecordTypeId == ID_CASE_PCF || c.RecordTypeId == ID_CASE_PCF_READONLY) {
                pcfMap.put(c.Id, c);
            }
        }
        if (pcfMap.isEmpty()) {
            return;
        }
        Map<Id, Case> casesMap = getCasesMap(pcfMap.keySet());
        for(Case c: pcfMap.values()){
            Case cWithInfo = new Case();
            if(!casesMap.isEmpty()){
                cWithInfo = casesMap.get(c.Id);
            }
            if (c.RecordTypeId == ID_CASE_PCF) {
                if (c.Status == OPEN
                        && c.VTD1_PCF_Safety_Concern_Indicator__c == 'Possible') {
                    pcfReminders.add(new VT_R3_PcfRemindersSender.ParamsHolder(c.Id));
                }
                if (c.VTD1_PCF_Safety_Concern_Indicator__c == 'Possible'
                        && c.VTD1_Clinical_Study_Membership__c != null
                        && cWithInfo.CreatedBy.VTD1_Profile_Name__c != null
                        && LM_PROFILES_FOR_NOTIFICATION.contains(cWithInfo.CreatedBy.VTD1_Profile_Name__c)) {
                    if (c.CreatedById != cWithInfo.VTD1_Clinical_Study_Membership__r.VTD1_PI_user__c) {
                        tnNotificationCodes.add('N027');
                        notificationSourceIds.add(c.Id);
                    }
                    if (cWithInfo.VTD1_Clinical_Study_Membership__r.VTR2_SiteCoordinator__c != null
                            && c.CreatedById != cWithInfo.VTD1_Clinical_Study_Membership__r.VTR2_SiteCoordinator__c) {
                        tnNotificationCodes.add('N029');
                        notificationSourceIds.add(c.Id);
                    }
                    if (cWithInfo.VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c != null
                            && c.CreatedById != cWithInfo.VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c) {
                        tnNotificationCodes.add('N557');
                        notificationSourceIds.add(c.Id);
                    }
                }
            }


            /**PB name : Cases – New Ad-hoc Case*/

            if (cWithInfo.VTD1_Clinical_Study_Membership__c != null
                    && (cWithInfo.RecordTypeId == ID_CASE_PCF
                    || cWithInfo.RecordTypeId == ID_CASE_PCF_READONLY)
                    && cWithInfo.VTD1_Clinical_Study_Membership__r.RecordTypeId == ID_CAREPLAN) {
                caseDoSharing.add(cWithInfo.Id);
            }
        }
        if (!caseDoSharing.isEmpty()) {
            VT_R3_GlobalSharing.doSharing(caseDoSharing);
        }
        if(!pcfReminders.isEmpty()) {
            VT_R3_PcfRemindersSender.sendReminders(pcfReminders);
        }
        if(!tnNotificationCodes.isEmpty() && !notificationSourceIds.isEmpty()) {
            VT_D2_TNCatalogNotifications.generateNotifications(tnNotificationCodes, notificationSourceIds, null);
        }
    }

    protected override void onAfterUpdate(Handler.TriggerContext context) {
        List<Case> newList = (List<Case>) context.newList;
        Map<Id, Case> pcfMap = new Map<Id, Case>();
        for (Case c : newList) {
            if (c.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF) {
                pcfMap.put(c.Id, c);
            }
        }
        if (pcfMap.isEmpty()) {
            return;
        }
        Map<Id, Case> oldMap = (Map<Id, Case>) context.oldMap;
        List<CaseShare> caseShares = new List<CaseShare>();
        List<VT_R3_EmailsSender.ParamsHolder> senderParams = new List<VT_R3_EmailsSender.ParamsHolder>();
        List <VT_R3_PcfRemindersSender.ParamsHolder> pcfReminders = new List <VT_R3_PcfRemindersSender.ParamsHolder>();
        Map<Id, Case> casesMap = getCasesMap(pcfMap.keySet());
        for (Case c : pcfMap.values()) {
            Case cWithInfo = new Case();
            if(!casesMap.isEmpty()){
                cWithInfo = casesMap.get(c.Id);
            }
            Case oldCase = oldMap.get(c.Id);
            /*PCF owner change notification: (T548) Notification of new PCF SC owner*/
            if(c.OwnerId != oldCase.OwnerId && c.Status == OPEN){
                if(c.OwnerId.getSobjectType() == User.getSObjectType()
                        && (c.VTD1_PCF_Safety_Concern_Indicator__c == 'Confirmed AE'
                            || c.VTD1_PCF_Safety_Concern_Indicator__c == 'Confirmed SAE')){
                    tnTaskCodes.add('T548');
                    taskSourceIds.add(c.Id);
                }
            }
            /* PCF Safety Concern Indicator Status automation */
            if(c.VTD1_PCF_Safety_Concern_Indicator__c != oldCase.VTD1_PCF_Safety_Concern_Indicator__c
                    && cWithInfo.VTD1_PCF_Safety_Concern_Indicator__c != null
                    && cWithInfo.VTD1_PCF_Safety_Concern_Indicator__c == c.VTD1_PCF_Safety_Concern_Indicator__c) {
                if((c.VTD1_PCF_Safety_Concern_Indicator__c == 'Confirmed AE' || c.VTD1_PCF_Safety_Concern_Indicator__c == 'Confirmed SAE')
                        && c.VTD1_Clinical_Study_Membership__c != null) {
                    if(cWithInfo.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTD2_TMA_Queue_ID__c != null){
                        caseShares.add(new CaseShare(CaseId = c.Id,
                                CaseAccessLevel = 'Edit',
                                RowCause = 'Manual',
                                UserOrGroupId = cWithInfo.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTD2_TMA_Queue_ID__c
                        ));
                    }
                    if(cWithInfo.VTD1_Clinical_Study_Membership__r.VTR2_SiteCoordinator__c != null) {
                        tnNotificationCodes.add('N053');
                        notificationSourceIds.add(c.Id);
                    }
                    tnTaskCodes.add('512');
                    taskSourceIds.add(c.Id);
                    tnTaskCodes.add('T546');
                    taskSourceIds.add(c.Id);
                }
            }
            /* END PCF Safety Concern Indicator Status automation */
            if(c.VTD2_Safety_Concern_Reviewed_by_PI__c && !oldCase.VTD2_Safety_Concern_Reviewed_by_PI__c) {
                tnTaskCodes.add('T549');
                taskSourceIds.add(c.Id);
            }
            if(c.Status == OPEN
                    && cWithInfo.RecordType.DeveloperName != null
                    && cWithInfo.Owner.Profile.Name != null
                    && cWithInfo.RecordType.DeveloperName == 'VTD1_PCF'){
                String caseOwnerProfileName = cWithInfo.Owner.Profile.Name;
                /* PCF Safety Concern Owner Change Immediate Actions: Assigned_Potential_AE_SAE_to_PI e-mail*/
                if(c.VTD1_PCF_Safety_Concern_Indicator__c != 'No'
                        && c.OwnerId != oldCase.OwnerId
                        && caseOwnerProfileName == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME){
                    VT_R3_EmailsSender.ParamsHolder emailSenderParamsHolder = new VT_R3_EmailsSender.ParamsHolder();
                    emailSenderParamsHolder.recipientId = c.OwnerId;
                    emailSenderParamsHolder.relatedToId = c.Id;
                    emailSenderParamsHolder.templateDevName = 'PCF_Safety_Concern_is_assigned_to_a_user_Study_Hub';
                    senderParams.add(emailSenderParamsHolder);
                }
                /*PCF Escalation process: e-mail to PI*/
                if(c.VTD1_PCF_Safety_Concern_Indicator__c == 'Possible'
                        && c.LastModifiedById != c.OwnerId
                        && c.IsEscalated == true
                        && oldCase.IsEscalated == false
                        && c.VTD1_Clinical_Study_Membership__c != null){

                    if(caseOwnerProfileName == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME
                            && cWithInfo.VTD1_Clinical_Study_Membership__r.VTD1_PI_contact__c != null){
                        senderParams.add(new VT_R3_EmailsSender.ParamsHolder(
                                cWithInfo.VTD1_Clinical_Study_Membership__r.VTD1_PI_contact__c,
                                c.Id,
                                'PCF_SC_Escalation_email_to_PI'
                        ));
                    }

                    /*PCF Escalation process: NotificationsC/Post to Chatter*/
                    tnNotificationCodes.add( PROFILES_FOR_NOTIFICATION_C.contains(caseOwnerProfileName) ? 'N068' : 'P068' );
                    notificationSourceIds.add(c.Id);
                }
            }
            if(c.Status == OPEN){
                if((c.VTD1_PCF_Safety_Concern_Indicator__c == 'Possible'
                        && (oldCase.VTD1_PCF_Safety_Concern_Indicator__c != 'Possible'
                        || c.IsEscalated != oldCase.IsEscalated
                        || c.OwnerId != oldCase.OwnerId))
                        || (c.VTD1_PCF_Safety_Concern_Indicator__c != 'Possible'
                        && oldCase.VTD1_PCF_Safety_Concern_Indicator__c == 'Possible')){
                    pcfReminders.add(new VT_R3_PcfRemindersSender.ParamsHolder(c.Id));
                }
            }
            if(c.VTD1_PCF_Safety_Concern_Indicator__c == 'Possible'
                    && c.VTD1_Clinical_Study_Membership__c != null
                    && cWithInfo.LastModifiedBy.VTD1_Profile_Name__c != null
                    && cWithInfo.VTD1_Clinical_Study_Membership__r.VTD1_PI_user__c != null
                    && LM_PROFILES_FOR_NOTIFICATION.contains(cWithInfo.LastModifiedBy.VTD1_Profile_Name__c)){
                if(c.CreatedById != cWithInfo.VTD1_Clinical_Study_Membership__r.VTD1_PI_user__c){
                    tnNotificationCodes.add('N027');
                    notificationSourceIds.add(c.Id);
                }
                if(cWithInfo.VTD1_Clinical_Study_Membership__r.VTR2_SiteCoordinator__c != null
                        && c.CreatedById != cWithInfo.VTD1_Clinical_Study_Membership__r.VTR2_SiteCoordinator__c){
                    tnNotificationCodes.add('N029');
                    notificationSourceIds.add(c.Id);
                }
                if(cWithInfo.VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c != null
                        && c.CreatedById != cWithInfo.VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c
                        && cWithInfo.LastModifiedBy.VTD1_Profile_Name__c != null
                        && cWithInfo.LastModifiedBy.VTD1_Profile_Name__c != VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME
                        && cWithInfo.VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c != null){
                    tnNotificationCodes.add('N557');
                    notificationSourceIds.add(c.Id);
                }
            }
        }
        if(!pcfReminders.isEmpty()) {
            VT_R3_PcfRemindersSender.sendReminders(pcfReminders);
        }
        if(!pcfPatientContactFormComments.isEmpty()){
            insert pcfPatientContactFormComments;
        }
        if(!senderParams.isEmpty()){
            VT_R3_EmailsSender.sendEmails(senderParams);
        }
        if(!caseShares.isEmpty()) {
            insert caseShares;
        }
        if(!tnNotificationCodes.isEmpty() && !notificationSourceIds.isEmpty()) {
            VT_D2_TNCatalogNotifications.generateNotifications(tnNotificationCodes, notificationSourceIds, null);
        }
        if(!tnTaskCodes.isEmpty() && !taskSourceIds.isEmpty()) {
            VT_D2_TNCatalogTasks.generateTasks(tnTaskCodes, taskSourceIds, null, null);
        }
        tnNotificationCodes.clear();
        notificationSourceIds.clear();
        tnTaskCodes.clear();
        taskSourceIds.clear();
    }

    public static void taskDueDateUpdate (Map<Id, Integer> pcfTaskDueDateUpdate) {
        List<Task> tasks = [SELECT Id, WhatId FROM Task WHERE WhatId IN :pcfTaskDueDateUpdate.keySet()];
        if(tasks.isEmpty()){
            return;
        }
        List<Task> tasksToUpd = new  List<Task> ();
        for (Task taskToUpd : tasks) {
            Integer hoursIncrement= pcfTaskDueDateUpdate.get(taskToUpd.WhatId);
            tasksToUpd.add(new Task (Id = taskToUpd.Id,
                    ActivityDate = System.now().addHours(hoursIncrement).date(),
                    IsReminderSet = true,
                    ReminderDateTime = System.now().addDays(hoursIncrement)));
        }
        if(!tasksToUpd.isEmpty()) {
            update tasksToUpd;
        }
    }

    public static Map<Id, Case> getCasesMap(Set<Id> caseIdsSet) {
        Map<Id, Case> caseMap = new Map<Id, Case>([
                SELECT Id,
                        VTD1_Clinical_Study_Membership__r.VTD1_PI_contact__c,
                        VTD1_Clinical_Study_Membership__r.VTD1_PI_user__c,
                        VTD1_Clinical_Study_Membership__r.VTR2_SiteCoordinator__c,
                        VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c,
                        VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTD2_TMA_Queue_ID__c,
                        VTD1_Clinical_Study_Membership__r.RecordTypeId,
                        LastModifiedBy.VTD1_Profile_Name__c,
                        LastModifiedBy.Name,
                        Owner.Profile.Name,
                        Owner.Email,
                        RecordType.DeveloperName,
                        RecordType.Name,
                        VTD1_PCF_Safety_Concern_Indicator__c,
                        CreatedBy.VTD1_Profile_Name__c
                FROM Case
                WHERE Id IN :caseIdsSet
        ]);
        return caseMap;
    }
}