/**
 * Created by dmitry on 04.11.2020.
 */

public with sharing class VT_R5_eDiaryAlertsSiteStaffBatch extends VT_R5_AbstractEDiaryAlertsBatch implements Database.AllowsCallouts {

    private static final Id OOO_EVENT_RT_ID = Event.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('OutOfOffice').getRecordTypeId();
    private static final Id SIMPLE_TASK_RT = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('SimpleTask').getRecordTypeId();
    private static final String TASK_STATUS = 'Open';
    private static final String TASK_CATEGORY = 'Follow Up Required';
    private static final String LINK_TO_RELATED_EVENT_OR_OBJECT = 'my-patients?listViewId=';
    private static final String EMAIL_TEMPLATE_NAME = 'Notification_as_Email';
    private static final Set<String> OOO_PROFILES = new Set<String> {
            VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME,
            VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME
    };
    private static final Set<String> COMMUNITY_PROFILES = new Set<String> {
            VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME,
            VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME
    };
    private static final Set<String> DIARY_STATUSES = new Set<String> {
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_DUE_SOON,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_READY_TO_START,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_MISSED,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_EDIARY_MISSED
    };

    private Set<Id> usersForEmail;
    private List<VTD1_NotificationC__c> communityNotificationsForInsert;
    private List<VT_R3_CustomNotification.NotificationWrapper> internalNotificationsForInsert;
    private List<Task> tasksForInsert;


    public VT_R5_eDiaryAlertsSiteStaffBatch(Map<Id, VT_R5_eDiaryAlertsService.AlertRequestInfo> alertsMap,String Recipient_Type) {  //5.7 epic SH-20625
        super(alertsMap,Recipient_Type);  //5.7 epic SH-20625
        this.batchScopeSize = 750;
        this.scopeItemIdFieldName = 'VTD1_Study__c';
        this.usersForEmail = new Set<Id>();
        this.communityNotificationsForInsert = new List<VTD1_NotificationC__c>();
        this.internalNotificationsForInsert = new List<VT_R3_CustomNotification.NotificationWrapper>();
        this.tasksForInsert = new List<Task>();
    }

    protected override Iterable<SObject> getScope() {
        Datetime now = Datetime.now();
        Set<Id> oooUsers = new Set<Id>();
        List<Event> oooEvents = [
                SELECT OwnerId
                FROM Event
                WHERE RecordTypeId = :OOO_EVENT_RT_ID
                    AND Owner.Profile.Name IN :OOO_PROFILES
                    AND StartDateTime <= :now
                    AND EndDateTime > :now
        ];
        for (Event e : oooEvents) {
            oooUsers.add(e.OwnerId);
        }
        
        return Database.getQueryLocator([
                SELECT Id, User__c, User__r.TimeZoneSidKey, User__r.Profile.Name, Study__c,
                        User__r.Contact.VTD2_Send_notifications_as_emails__c,VTD1_Type__c
                FROM Study_Team_Member__c
                WHERE User__r.IsActive = TRUE
                    AND User__c NOT IN :oooUsers
                    AND Study__c IN :this.studyIdToBuildersMap.keySet()
                    AND User__r.Profile.Name IN :VT_R4_ConstantsHelper_ProfilesSTM.SITE_STAFF
        ]);
    }

    protected override Set<Id> getScopeIds(List<SObject> scope) {
        Set<Id> scopeIds = new Set<Id>();
        for (Study_Team_Member__c stm : (List<Study_Team_Member__c>) scope) {
            scopeIds.add(stm.Study__c);
        }

        return scopeIds;
    }

    protected override List<AggregateResult> getAggregatedSurveys(Set<Id> studyIds) {
        return [
                SELECT COUNT(VTD1_Status__c), VTD1_CSM__r.VTD1_Study__c, VTR5_eCoaAlertsKey__c
                FROM VTD1_Survey__c
                WHERE VTD1_CSM__r.VTD1_Study__c IN :studyIds
                    AND VTR5_DiaryStudyKey__c IN :this.studyEDiaryKeys
                    AND VTD1_Status__c IN :DIARY_STATUSES
                    AND VTD1_Due_Date__c IN :this.eDiaryDueDates
                    AND VTD1_Due_Date__c != NULL
                GROUP BY VTR5_eCoaAlertsKey__c, VTD1_CSM__r.VTD1_Study__c
        ];
    }

    protected override void prepareDailyAlerts(SObject scopeItem, VT_R5_eDiaryAlertsService.AlertRequestInfo alertRequestInfo, VTR5_eDiaryNotificationBuilder__c builder) {
        Study_Team_Member__c stm = (Study_Team_Member__c) scopeItem;
        if (builder.VTR5_NotificationRecipient__c.contains(stm.User__r.Profile.Name) && isTimezoneMatched(alertRequestInfo.timezoneOffset, stm.User__r)) {
            addAlertsForInsert(stm.User__r, builder);
        }
    }

    protected override void prepareExpirationAlerts(SObject scopeItem, VT_R5_eDiaryAlertsService.AlertRequestInfo alertRequestInfo, VTR5_eDiaryNotificationBuilder__c builder) {
        Study_Team_Member__c stm = (Study_Team_Member__c) scopeItem;
        Set<String> diaryAlertKeys = this.scopeItemIdToAlertsKeyMap.get(stm.Study__c);

        if (diaryAlertKeys == null) { return; }

        String alertKey = getAlertKey(alertRequestInfo, builder);
        if (diaryAlertKeys.contains(alertKey) && builder.VTR5_NotificationRecipient__c.contains(stm.User__r.Profile.Name)) {
            addAlertsForInsert(stm.User__r, builder);
        }
    }

    protected override VT_R5_eDiaryAlertsBulkInsertRestService.RequestPayload getAlerts() {

        if (this.tasksForInsert.isEmpty()) { return null; }

        //TODO after winter 21 move this temp solution to VT_R5_eDiaryAlertsBulkInsertRestService
        if (!Test.isRunningTest()) { VT_R3_CustomNotification.sendCustomNotifications(this.internalNotificationsForInsert); }

        return new VT_R5_eDiaryAlertsBulkInsertRestService.RequestPayload(
                EMAIL_TEMPLATE_NAME,
                this.usersForEmail,
                this.communityNotificationsForInsert,
                this.tasksForInsert
        );
    }

    private void addAlertsForInsert(User u, VTR5_eDiaryNotificationBuilder__c builder) {
        if (COMMUNITY_PROFILES.contains(u.Profile.Name)) {
            addCommunityNotificationForInsert(u, builder);
            addTaskForInsert(u, builder, false);
        } else {
            addInternalNotificationForInsert(u, builder);
            addTaskForInsert(u, builder, true);
        }
    }

    private void addCommunityNotificationForInsert(User u, VTR5_eDiaryNotificationBuilder__c builder) {
        VTD1_NotificationC__c n = new VTD1_NotificationC__c(
                VTD1_Receivers__c = u.Id,
                OwnerId = u.Id,
                Message__c = builder.VTR5_NotificationMessage__c,
                Title__c = builder.VTR5_Notification_Title__c,
                VTR3_Notification_Type__c = NOTIFICATION_TYPE,
                VTR5_eDiaryNotificationBuilder__c = builder.Id,
                Link_to_related_event_or_object__c = LINK_TO_RELATED_EVENT_OR_OBJECT + builder.VTR5_DiaryListView__c,
                HasDirectLink__c = true
        );

        this.communityNotificationsForInsert.add(n);

        if (u.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME && u.Contact.VTD2_Send_notifications_as_emails__c) {
            this.usersForEmail.add(u.Id);
        }

    }

    private void addInternalNotificationForInsert(User u, VTR5_eDiaryNotificationBuilder__c builder) {
        this.internalNotificationsForInsert.add(new VT_R3_CustomNotification.NotificationWrapper(
                new List<Id>{ u.Id },
                builder.VTR5_Notification_Title__c,
                builder.VTR5_NotificationMessage__c,
                builder.VTR5_DiaryListView__c
        ));
    }

    private void addTaskForInsert(User u, VTR5_eDiaryNotificationBuilder__c builder, Boolean isInternalUser) {
        this.tasksForInsert.add(new Task(
                Status = TASK_STATUS,
                OwnerId = u.Id,
                RecordTypeId = SIMPLE_TASK_RT,
                VTR5_PreventPBInvocation__c = true,
                Category__c = TASK_CATEGORY,
                VTR5_AutocompleteWhenClicked__c = true,
                HealthCloudGA__CarePlanTemplate__c = builder.VTR5_Study__c,
                Subject = builder.VTR5_Notification_Title__c,
                VTD2_My_Task_List_Redirect__c = isInternalUser
                        ? String.valueOf(builder.VTR5_DiaryListView__c)
                        : LINK_TO_RELATED_EVENT_OR_OBJECT + builder.VTR5_DiaryListView__c
        ));
    }
    
    
}