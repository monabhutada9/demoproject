/**
* @author: Carl Judge
* @date: 05-May-20
* @description: Create a Subject (someone who will answer eDiaries) in eCOA
**/


public class VT_R5_eCoaCreateSubject extends VT_R5_eCoaAbstractAction {
    public static Set<Id> createdUserIds = new Set<Id>(); // capture send user IDs to prevent sending the same request more than once due to trigger recursion etc

    private Id userId;
    private Boolean guidExists = false;
    private User subjectUser;
    private Boolean skipUserUpdate = false;

    public VT_R5_eCoaCreateSubject(Id userId) {
        this.userId = userId;
    }

    public VT_R5_eCoaCreateSubject setSkipUserUpdate(Boolean skipUserUpdate) {
        this.skipUserUpdate = skipUserUpdate;
        return this;
    }

    public User getSubjectUser() {
        return subjectUser;
    }

    /**
   * @description: Executes HTTP POST
   * @modified by N.Arbatskiy
   * @return VT_D1_HTTPConnectionHandler.Result in order to be passed as
   * a parameter to @method processResponse(VT_R5_eCoaAbstractAction class).
   */
    public override VT_D1_HTTPConnectionHandler.Result doCallout() {
        String endpoint = VT_R4_ConstantsHelper_Integrations.ENDPOINT_URL_ECOA_SUBJECT_CREATE;
        String method = 'POST';
        String action = 'eCOA Create eCOA Subject';

        if (createdUserIds.contains(userId)) {
            guidExists = true;
            return null;
        }
        createdUserIds.add(userId);
        if (userId != null) {
            User u = [
                    SELECT Id,
                            CountryCode,
                            Phone,
                            Email,
                            TimeZoneSidKey,
                            LocaleSidKey,
                            LanguageLocaleKey,
                            VTR5_eCOA_Guid__c,
                            Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eCOA_Guid__c,
                            Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.Name,
                            Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_eCOA_Guid__c,
                            Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eDiaryTool__c,
                            VTR5_ePRO_GUID__c,
                            VTR5_ClinRO_GUID__c,
                            Contact.VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c
                    FROM User
                    WHERE Id = :userId
            ];
            if (u.VTR5_eCOA_Guid__c == null) {
                return new VT_R5_eCoaApiRequest()
                        .setStudyGuid(u.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eCOA_Guid__c)
                        .setEndpoint(endpoint)
                        .setMethod(method)
                        .setAction(action)
                        .setBody(new VT_R5_RequestBuilder_CreateSubject(u))
                        .setSkipLogging(true)
                        .send();
            } else {
                guidExists = true;
                return null;
            }
        }
        return null;
    }

    public override Type getType() {
        return VT_R5_eCoaCreateSubject.class;
    }

    protected override void handleSuccess(VT_D1_HTTPConnectionHandler.Result result) {
        User subjectUser = new User(
                Id = userId,
                VTR5_eCOA_Guid__c = result.httpResponse.getBody(),
                VTR5_Create_Subject_in_eCOA__c = true
        );
        if (!skipUserUpdate) {
            update subjectUser;
        } else {
            this.subjectUser = subjectUser;
        }
    }

    protected override void handleFailure(VT_D1_HTTPConnectionHandler.Result result) {
        if (guidExists != true) {
            User subjectUser = new User(
                    Id = userId,
                    VTR5_Create_Subject_in_eCOA__c = false
            );
            if (!skipUserUpdate) {
                update subjectUser;
            } else {
                this.subjectUser = subjectUser;
            }
        }
    }
}