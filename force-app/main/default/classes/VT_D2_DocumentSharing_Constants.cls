/**
* @author: Carl Judge
* @date: 21-Dec-18
* @description: Constants for VT_R3_GlobalSharingLogic_Document
**/

public class VT_D2_DocumentSharing_Constants {
    public enum ProfileOrFieldType {MEMBER_PROFILE, STUDY_FIELD, CASE_FIELD}

    public static final String ROW_CAUSE = Schema.VTD1_Document__Share.RowCause.VTD2_DocumentApexShare__c;

    public static final Set<String> NON_PORTAL_PROFILES = new Set<String>{
        VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME,
        'System Administrator',
        VT_R4_ConstantsHelper_ProfilesSTM.AUDITOR_PROFILE_NAME // Added to enable sharing for Auditor Profile, Epic# SH-8126
    };

    public static final Set<Id> MEDICAL_DOCUMENT_RECORDTYPE_IDS = new Set<Id>{
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD,
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_ARCHIVED,
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_REJECTED,
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_RELEASE_FORM        
    };

    public static final Set<String> SITE_RELATED_PROFILES = new Set<String>{
        VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME        
    };

    // Record types here will be shared with the same users as patients
    public static final Set<Id> GROUP_SHARE_RECORDTYPE_IDS = new Set<Id>{
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON,
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT
    };

    //Defines access to documents for study members and study/case participants by record type - FOR NON GROUP SHARING RECORD TYPES
    // RecordTypeId => MEMBER_PROFILE/STUDY_FIELD/CASE_FIELD => Profile/Field => AccessLevel
    public static final Map<Id, Map<ProfileOrFieldType, Map<String, String>>> SHARING_CONFIG = new Map<Id, Map<ProfileOrFieldType, Map<String, String>>>{
        // VTD1_Medical_Record
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD => new Map<ProfileOrFieldType, Map<String, String>> {
            ProfileOrFieldType.MEMBER_PROFILE => new Map<String, String> {
                VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.CM_PROFILE_NAME           => 'Read',
                VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.AUDITOR_PROFILE_NAME      => 'Read'   // Added to enable sharing for Auditor Profile, Epic# SH-8126
            },
            ProfileOrFieldType.STUDY_FIELD => new Map<String, String> {},
            ProfileOrFieldType.CASE_FIELD => new Map<String, String> {
                'VTD1_Patient_User__c'                          => 'Edit',
                'VTD1_PI_user__c'                               => 'Edit',
                'VTD1_Backup_PI_User__c'                        => 'Edit',
                'VTD1_Primary_PG__c'                            => 'Edit',
                'VTD1_Secondary_PG__c'                          => 'Edit'
            }
        },
        // Medical_Record_Archived
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_ARCHIVED => new Map<ProfileOrFieldType, Map<String, String>> {
            ProfileOrFieldType.MEMBER_PROFILE => new Map<String, String> {
                VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.AUDITOR_PROFILE_NAME      => 'Read'  // Added to enable sharing for Auditor Profile, Epic# SH-8126
            },
            ProfileOrFieldType.STUDY_FIELD => new Map<String, String> {},
            ProfileOrFieldType.CASE_FIELD => new Map<String, String> {
                'VTD1_Patient_User__c'                          => 'Edit',
                'VTD1_PI_user__c'                               => 'Edit',
                'VTD1_Backup_PI_User__c'                        => 'Edit',
                'VTD1_Primary_PG__c'                            => 'Edit',
                'VTD1_Secondary_PG__c'                          => 'Edit'
            }
        },
        // Medical_Record_Rejected
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_REJECTED => new Map<ProfileOrFieldType, Map<String, String>> {
            ProfileOrFieldType.MEMBER_PROFILE => new Map<String, String> {
                VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.AUDITOR_PROFILE_NAME      => 'Read'  // Added to enable sharing for Auditor Profile, Epic# SH-8126
            },
            ProfileOrFieldType.STUDY_FIELD => new Map<String, String> {},
            ProfileOrFieldType.CASE_FIELD => new Map<String, String> {
                'VTD1_Patient_User__c'                          => 'Edit',
                'VTD1_PI_user__c'                               => 'Edit',
                'VTD1_Backup_PI_User__c'                        => 'Edit',
                'VTD1_Primary_PG__c'                            => 'Edit',
                'VTD1_Secondary_PG__c'                          => 'Edit'
            }
        },
        // Medical_Record_Release_Form
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_RELEASE_FORM => new Map<ProfileOrFieldType, Map<String, String>> {
            ProfileOrFieldType.MEMBER_PROFILE => new Map<String, String> {
                VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.AUDITOR_PROFILE_NAME      => 'Read'  // Added to enable sharing for Auditor Profile, Epic# SH-8126
            },
            ProfileOrFieldType.STUDY_FIELD => new Map<String, String> {},
            ProfileOrFieldType.CASE_FIELD => new Map<String, String> {
                'VTD1_Patient_User__c'                          => 'Edit',
                'VTD1_PI_user__c'                               => 'Edit',
                'VTD1_Backup_PI_User__c'                        => 'Edit',
                'VTD1_Primary_PG__c'                            => 'Edit',
                'VTD1_Secondary_PG__c'                          => 'Edit'
            }
        },
        // VTD1_Patient_eligibility_assessment_form
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE => new Map<ProfileOrFieldType, Map<String, String>> {
            ProfileOrFieldType.MEMBER_PROFILE => new Map<String, String> {
                VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.CM_PROFILE_NAME           => 'Read',
                VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.AUDITOR_PROFILE_NAME      => 'Read'  // Added to enable sharing for Auditor Profile, Epic# SH-8126
            },
            ProfileOrFieldType.STUDY_FIELD => new Map<String, String> {
                'VTD1_Virtual_Trial_head_of_Operations__c'      => 'Edit'
            },
            ProfileOrFieldType.CASE_FIELD => new Map<String, String> {
                'VTD1_PI_user__c'                               => 'Edit',
                'VTD1_Backup_PI_User__c'                        => 'Edit',
                'VTD1_Primary_PG__c'                            => 'Edit',
                'VTD1_Secondary_PG__c'                          => 'Edit'
            }
        },
        // Patient_Eligibility_Assessment_Form_Rejected
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSR => new Map<ProfileOrFieldType, Map<String, String>> {
            ProfileOrFieldType.MEMBER_PROFILE => new Map<String, String> {
                VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.CM_PROFILE_NAME           => 'Read',
                VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.AUDITOR_PROFILE_NAME      => 'Read'  // Added to enable sharing for Auditor Profile, Epic# SH-8126
            },
            ProfileOrFieldType.STUDY_FIELD => new Map<String, String> {
                'VTD1_Virtual_Trial_head_of_Operations__c'      => 'Edit'
            },
            ProfileOrFieldType.CASE_FIELD => new Map<String, String> {
                'VTD1_PI_user__c'                               => 'Edit',
                'VTD1_Backup_PI_User__c'                        => 'Edit',
                'VTD1_Primary_PG__c'                            => 'Edit',
                'VTD1_Secondary_PG__c'                          => 'Edit'
            }
        },
        // VTD1_Regulatory_Document
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT => new Map<ProfileOrFieldType, Map<String, String>> {
            ProfileOrFieldType.MEMBER_PROFILE => new Map<String, String> {
                VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.CM_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME           => 'Read',
                VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.RS_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.MRR_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.PMA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.AUDITOR_PROFILE_NAME      => 'Read'  // Added to enable sharing for Auditor Profile, Epic# SH-8126
            },
            ProfileOrFieldType.STUDY_FIELD => new Map<String, String> {
                'VTD1_Project_Lead__c'                          => 'Edit',
                'VTD1_Virtual_Trial_Study_Lead__c'              => 'Edit',
                'VTD1_Regulatory_Specialist__c'                 => 'Edit',
                'VTD1_Virtual_Trial_head_of_Operations__c'      => 'Edit'
            },
            ProfileOrFieldType.CASE_FIELD => new Map<String, String> {

            }
        },
        // VTD2_PA_Signature_Page
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PA_SIGNATURE_PAGE => new Map<ProfileOrFieldType, Map<String, String>> {
            ProfileOrFieldType.MEMBER_PROFILE => new Map<String, String> {
                VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.CM_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.MRR_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.PMA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.AUDITOR_PROFILE_NAME      => 'Read'  // Added to enable sharing for Auditor Profile, Epic# SH-8126
            },
            ProfileOrFieldType.STUDY_FIELD => new Map<String, String> {
                'VTD1_Virtual_Trial_head_of_Operations__c'      => 'Edit'
            },
            ProfileOrFieldType.CASE_FIELD => new Map<String, String> {
                'VTD1_Patient_User__c'                          => 'Edit',
                'VTD1_PI_user__c'                               => 'Edit',
                'VTD1_Backup_PI_User__c'                        => 'Edit',
                'VTD1_Primary_PG__c'                            => 'Edit',
                'VTD1_Secondary_PG__c'                          => 'Edit'
            }
        },
        // VTR2_Note_of_Transfer
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_NOTE_OF_TRANSFER => new Map<ProfileOrFieldType, Map<String, String>> {
            ProfileOrFieldType.MEMBER_PROFILE => new Map<String, String> {
                VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.CM_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.RS_PROFILE_NAME           => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.MRR_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.PMA_PROFILE_NAME          => 'Edit',
                VT_R4_ConstantsHelper_ProfilesSTM.AUDITOR_PROFILE_NAME      => 'Read'  // Added to enable sharing for Auditor Profile, Epic# SH-8126
            },
            ProfileOrFieldType.STUDY_FIELD => new Map<String, String> {
                'VTD1_Project_Lead__c'                          => 'Edit',
                'VTD1_Virtual_Trial_Study_Lead__c'              => 'Edit',
                'VTD1_Regulatory_Specialist__c'                 => 'Edit',
                'VTD1_PMA__c'                                   => 'Edit',
                'VTD1_Monitoring_Report_Reviewer__c'            => 'Edit',
                'VTD1_Virtual_Trial_head_of_Operations__c'      => 'Edit'
            },
            ProfileOrFieldType.CASE_FIELD => new Map<String, String> {
                'VTD1_Patient_User__c'                          => 'Edit',
                'VTD1_PI_user__c'                               => 'Edit',
                'VTD1_Backup_PI_User__c'                        => 'Edit',
                'VTD1_Primary_PG__c'                            => 'Edit',
                'VTD1_Secondary_PG__c'                          => 'Edit'
            }
        },
        // VTR2_Informed_Consent_Guide
        VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_INFORMED_CONSENT_GUIDE => new Map<ProfileOrFieldType, Map<String, String>> {
            ProfileOrFieldType.MEMBER_PROFILE => new Map<String, String> {

            },
            ProfileOrFieldType.STUDY_FIELD => new Map<String, String> {

            },
            ProfileOrFieldType.CASE_FIELD => new Map<String, String> {
                'VTD1_Patient_User__c'                          => 'Edit'
            }
        }
    };
}