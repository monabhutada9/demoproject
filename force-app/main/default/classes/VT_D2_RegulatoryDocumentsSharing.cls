/**
* @author: Carl Judge
* @date: 23-Nov-18
* @description: Calculates sharing for regulatory documents. Covers scenarios when documents are inserted, and also when study members and participants are added/removed
**/

public without sharing class VT_D2_RegulatoryDocumentsSharing {
    /* deleted in US-0001772, but somehow was found in orgs again

    public class StudyUser {
        public Id studyId;
        public Id userId;

        public StudyUser(Id studyId, Id userId) {
            this.studyId = studyId;
            this.userId = userId;
        }
    }

    public static final List<String> MEMBER_PROFILES = new List<String>{
        'Therapeutic Medical Advisor',
        'Study Concierge',
        'Monitor',
        'Patient Guide'
    };

    public static final List<String> PARTICIPANT_FIELDS = new List<String>{
        'VTD1_Project_Lead__c',
        'VTD1_Regulatory_Specialist__c',
        'VTD1_Virtual_Trial_Study_Lead__c',
        'VTD1_Virtual_Trial_head_of_Operations__c'
    };

    
    public static void calcSharingForNewDocuments(List<VTD1_Document__c> docs) {
        Set<Id> docIds = new Map<Id, VTD1_Document__c>(docs).keySet();
        if (System.isFuture() || System.isBatch()) {
            calcSharingFromNewDocIds(docIds);
        } else {
            calcSharingFromNewDocIdsFuture(docIds);
        }
    }

    public static void calcSharingFromStudyUsers(List<StudyUser> usersToAdd, List<StudyUser> usersToRemove) {
        if (System.isFuture() || System.isBatch()) {
            if (usersToAdd != null && ! usersToAdd.isEmpty()) { addStudyUserShares(usersToAdd); }
            if (usersToRemove != null && ! usersToRemove.isEmpty()) { removeStudyUserShares(usersToRemove); }
        } else {
            calcSharingFromStudyUsersFuture(getSerializedStudyUsers(usersToAdd), getSerializedStudyUsers(usersToRemove));
        }
    }
    
    
    @Future
    private static void calcSharingFromNewDocIdsFuture(Set<Id> docIds) {
        calcSharingFromNewDocIds(docIds);
    }
    private static void calcSharingFromNewDocIds(Set<Id> docIds) {
        String query = 'SELECT Id,VTD1_Study__c';
        for (String field : PARTICIPANT_FIELDS) {
            query += ',' + 'VTD1_Study__r.' + field;
        }
        query += ' FROM VTD1_Document__c' +
            ' WHERE VTD1_Study__c != null' +
            ' AND RecordTypeId = \'' + VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT + '\'' +
            ' AND Id IN :docIds';

        List<VTD1_Document__c> docs = Database.query(query);
        Map<Id, List<Study_Team_Member__c>> membersMap = getMembersMap(docs);

        List<VTD1_Document__Share> newShares = new List<VTD1_Document__Share>();
        for (VTD1_Document__c doc : docs) {
            if (membersMap.containsKey(doc.VTD1_Study__c)) {
                for (Study_Team_Member__c member : membersMap.get(doc.VTD1_Study__c)) {
                    newShares.add(getNewShare(doc.Id, member.User__c));
                }
            }

            for (String field : PARTICIPANT_FIELDS) {
                Id userId = (Id)VT_D1_HelperClass.getCrossObjectField(doc, 'VTD1_Study__r.' + field);
                if (userId != null) {
                    newShares.add(getNewShare(doc.Id, userId));
                }
            }
        }
        if (! newShares.isEmpty()) { Database.insert(newShares, false); }
    }
    
    @Future
    private static void calcSharingFromStudyUsersFuture(String serializedUsersToAdd, String serializedUsersToRemove) {
        if (! String.isBlank(serializedUsersToAdd)) {
            List<StudyUser> usersToAdd = (List<StudyUser>)JSON.deserialize(serializedUsersToAdd, List<StudyUser>.class);
            if (! usersToAdd.isEmpty()) { addStudyUserShares(usersToAdd); }
        }

        if (serializedUsersToRemove != null) {
            List<StudyUser> usersToRemove = (List<StudyUser>)JSON.deserialize(serializedUsersToRemove, List<StudyUser>.class);
            if (! usersToRemove.isEmpty()) { removeStudyUserShares(usersToRemove); }
        }
    }

    private static void addStudyUserShares(List<StudyUser> studyUsers) {
        Map<Id, List<Id>> studyToDocIdsMap = new Map<Id, List<Id>>();
        for (StudyUser item : studyUsers) {
            studyToDocIdsMap.put(item.studyId, new List<Id>());
        }

        for (VTD1_Document__c item : [SELECT Id, VTD1_Study__c FROM VTD1_Document__c WHERE VTD1_Study__c IN :studyToDocIdsMap.keySet()]) {
            studyToDocIdsMap.get(item.VTD1_Study__c).add(item.Id);
        }

        List<VTD1_Document__Share> newShares = new List<VTD1_Document__Share>();
        for (StudyUser studyUser : studyUsers) {
            for (Id docId : studyToDocIdsMap.get(studyUser.studyId)) {
                newShares.add(getNewShare(docId, studyUser.userId));
            }
        }
        if (! newShares.isEmpty()) { Database.insert(newShares, false); }
    }

    private static void removeStudyUserShares(List<StudyUser> studyUsers) {
        List<Id> studyIds = new List<Id>();
        List<Id> userIds = new List<Id>();
        Set<String> uniqueKeys = new Set<String>();

        for (StudyUser item : studyUsers) {
            studyIds.add(item.studyId);
            userIds.add(item.userId);
            uniqueKeys.add('' + item.studyId + item.userId);
        }

        List<VTD1_Document__Share> sharesToDelete = new List<VTD1_Document__Share>();
        for (VTD1_Document__Share item : [
            SELECT Id, UserOrGroupId, Parent.VTD1_Study__c
            FROM VTD1_Document__Share
            WHERE UserOrGroupId IN :userIds
            AND Parent.VTD1_Study__c IN :studyIds
            AND RowCause = :Schema.VTD1_Document__Share.RowCause.VTD2_RegulatoryDocumentShare__c
        ]) {
            if (uniqueKeys.contains('' + item.Parent.VTD1_Study__c + item.UserOrGroupId)) {
                sharesToDelete.add(item);
            }
        }

        if (! sharesToDelete.isEmpty()) { delete sharesToDelete; }
    }

    private static String getSerializedStudyUsers(List<StudyUser> studyUsers) {
        return studyUsers != null && ! studyUsers.isEmpty() ? JSON.serialize(studyUsers) : null;
    }
    
    private static VTD1_Document__Share getNewShare(Id docId, Id userId) {
        return new VTD1_Document__Share(
            ParentId = docId,
            UserOrGroupId = userId,
            AccessLevel = 'Edit',
            RowCause = Schema.VTD1_Document__Share.RowCause.VTD2_RegulatoryDocumentShare__c
        );
    }

    private static Map<Id, List<Study_Team_Member__c>> getMembersMap(List<VTD1_Document__c> docs) {
        List<Id> studyIds = new List<Id>();
        for (VTD1_Document__c item : docs) {
            studyIds.add(item.VTD1_Study__c);
        }

        Map<Id, List<Study_Team_Member__c>> membersMap = new Map<Id, List<Study_Team_Member__c>>();

        for (Study_Team_Member__c item : [
            SELECT Id, Study__c, User__c
            FROM Study_Team_Member__c
            WHERE Study__c IN :studyIds
            AND User__r.Profile.Name IN :MEMBER_PROFILES
        ]) {
            if (! membersMap.containsKey(item.Study__c)) {
                membersMap.put(item.Study__c, new List<Study_Team_Member__c>());
            }

            membersMap.get(item.Study__c).add(item);
        }

        return membersMap;
    }*/
}