public without sharing class VT_D1_UserProfileMenuController {
    @AuraEnabled(Cacheable=true)
    public static User getUser() {
        return [SELECT Id, Name, FullPhotoUrl FROM User WHERE Id = :UserInfo.getUserId()];
    }
    
    @AuraEnabled(Cacheable=true)
    public static String getBaseUrl() {
        return Site.getBaseUrl();
    }
}