/**
 * @author: Dmitry Kovalev
 * @date: 20.07.2020
 * @description: Test class for VT_R5_IRBLanguageProcessor
 */

@isTest
public with sharing class VT_R5_IRBLanguageProcessorTest {
    private static final String TESTED_CLASS_NAME = 'VT_R5_IRBLanguageProcessor';

    private static final Id STUDY_A = generateId(HealthCloudGA__CarePlanTemplate__c.sObjectType);
    private static final Id STUDY_B = generateId(HealthCloudGA__CarePlanTemplate__c.sObjectType);
    private static final Id STUDY_C = generateId(HealthCloudGA__CarePlanTemplate__c.sObjectType);

    /**
     * For scenarios 1 and 2.
     */
    @isTest
    public static void getModifiedVirtualSitesTest() {
        // get fresh records
        List<VTR5_IRBLanguageConfiguration__c> configs = getConfigs();
        List<Virtual_Site__c> sites = getVirtualSites();

        // apply stub
        applyVirtualSitesQueryStub(sites);

        // test
        List<Virtual_Site__c> modifiedSites = VT_R5_IRBLanguageProcessor.getModifiedVirtualSites(configs);

        // assertations
        checkBasicBehavior(modifiedSites);
    }

    /**
     * For scenario 3.
     */
    @isTest
    public static void updateIRBLanguagesOnVirtualSitesTest() {
        // get fresh records
        List<VTR5_IRBLanguageConfiguration__c> configs = getConfigs();
        List<Virtual_Site__c> sites = getVirtualSites();

        // apply stub
        applyIRBConfigurationQueryStub(configs);
        
        // test (this code will be executed after 'insert' operation)
        sites = VT_R5_IRBLanguageProcessor.updateIRBLanguagesOnVirtualSites(sites);

        // modified sites are those sites which has populated languages fields
        List<Virtual_Site__c> modifiedSites = getSitesWithPopulatedLanguages(sites);

        // assertations
        checkBasicBehavior(modifiedSites);
    }

    /**
     * For scenario 4.
     */
    @isTest
    public static void updateIRBLanguagesOnVirtualSitesIfCountryUpdatedTest() {
        // get fresh records
        List<VTR5_IRBLanguageConfiguration__c> configs = getConfigs();
        List<Virtual_Site__c> sites = getVirtualSites();

        // apply stub
        applyIRBConfigurationQueryStub(configs);

        // do prework
        sites = VT_R5_IRBLanguageProcessor.updateIRBLanguagesOnVirtualSites(sites);

        // Create a list with 3 virtual sites: 
        // * Study A site 2 – site without populated country, but with suitable configuration (after some changes)
        // * Study B site 2 – site without populated country and without suitable configuration
        // * Study A site 1 – site with populated country and with suitable configuration and already populated language fields
        Virtual_Site__c studyAsite1 = sites[0];
        sites = getSitesWithoutCountryAndLanguages(sites); // should return 'Study A site 2' and 'Study B site 2'
        sites.add(studyAsite1);

        // clone every site for the oldSites map (simulation of Trigger.oldMap)
        Map<Id, Virtual_Site__c> oldSites = new Map<Id, Virtual_Site__c>();
        for (Virtual_Site__c site : sites) {
            oldSites.put(site.Id, site.clone());
        }

        // update country fields
        sites[0].VTR3_Country__c = 'US'; // from '' to 'US'
        sites[1].VTR3_Country__c = 'US'; // from '' to 'US'
        sites[2].VTR3_Country__c = 'CN'; // 'Study A site 1' from 'US' to 'CN'

        // test (this code will be executed after 'update' operation)
        sites = VT_R5_IRBLanguageProcessor.updateIRBLanguagesOnVirtualSitesIfCountryUpdated(sites, oldSites);
        List<Virtual_Site__c> sitesWithLanguages = getSitesWithPopulatedLanguages(sites);

        // assertations
        System.assertEquals(3, sites.size(), 'The sites list should contain 3 records');
        System.assertEquals(2, sitesWithLanguages.size(), 'The language fileds should be populated only on 2 records');

        checkStudyBHasNoSitesWithPopulatedLanguages(sites);

        // check if values are populated on sites[0]
        checkVirtualSite(sites[0], 'Study A site 2', 'US', STUDY_A, 'en_US', 'en_US;es_US');

        // check if values are not changed on sites[1]
        checkVirtualSite(sites[1], 'Study B site 2', 'US', STUDY_B, '', '');

        // check if values are not changed on site[2]
        checkVirtualSite(sites[2], 'Study A site 1', 'CN', STUDY_A, 'en_US', 'en_US;es_US');
    }

    private static List<VTR5_IRBLanguageConfiguration__c> getConfigs() {
        List<VTR5_IRBLanguageConfiguration__c> configs = new List<VTR5_IRBLanguageConfiguration__c> {
            createIRBConfig(STUDY_A, 'US', 'en_US;es_US', 'en_US'),
            createIRBConfig(STUDY_A, 'CN', 'zh_TW;en_US', 'zh_TW'),
            createIRBConfig(STUDY_A, 'RU', 'ru_RU;en_US', 'ru_RU'),
            createIRBConfig(STUDY_C, 'US', 'en_US', 'en_US')
        };

        return configs;
    }

    private static VTR5_IRBLanguageConfiguration__c createIRBConfig(Id studyId, String country, 
                                                                   String approvedLanguages, String defaultLanguage) {
        /* 
        DK NOTE: Following code doesn't work: 
        VTR5_IRBLanguageConfiguration__c config = new VTR5_IRBLanguageConfiguration__c(
            VTR5_Study__c = studyId,
            VTR5_Country__c = country,
            VTR5_IRBApprovedLanguages__c = approvedLanguages,
            VTR5_DefaultIRBLanguage__c = defaultLanguage
        );      
        config.recalculateFormulas(); // calculation of VTR5_StudyCountryKey__c
        */

        Map<String, Object> configMap = new Map<String, Object>();
        configMap.put('VTR5_Study__c', studyId);
        configMap.put('VTR5_Country__c', country);
        configMap.put('VTR5_StudyCountryKey__c', studyId + country);
        configMap.put('VTR5_IRBApprovedLanguages__c', approvedLanguages);
        configMap.put('VTR5_DefaultIRBLanguage__c', defaultLanguage);
        Object config = JSON.deserialize(JSON.serialize(configMap), VTR5_IRBLanguageConfiguration__c.class);
        return (VTR5_IRBLanguageConfiguration__c) config;
    }

    private static List<Virtual_Site__c> getVirtualSites() {
        return new List<Virtual_Site__c> {
            createVirtualSite('Study A site 1', STUDY_A, 'US'),
            createVirtualSite('Study A site 2', STUDY_A, ''),
            createVirtualSite('Study A site 3', STUDY_A, 'CN'),
            createVirtualSite('Study B site 1', STUDY_B, 'US'),
            createVirtualSite('Study B site 2', STUDY_B, ''),
            createVirtualSite('Study B site 3', STUDY_B, 'CN'),
            createVirtualSite('Study C site 1', STUDY_C, 'US')
        };
    }

    private static Virtual_Site__c createVirtualSite(String name, Id study, String country) {
        return new Virtual_Site__c(
            Id = generateId(Virtual_Site__c.sObjectType),
            Name = name,
            VTD1_Study__c = study,
            VTR3_Country__c = country,
            VTR5_IRBApprovedLanguages__c = '',
            VTR5_DefaultIRBLanguage__c = ''
        );
    }

    private static Id generateId(SObjectType sObjectType) {
        return fflib_IDGenerator.generate(sObjectType);
    }

    private static void applyVirtualSitesQueryStub(List<Virtual_Site__c> sites) {
        // DK FIXME: rewrite onto VT_Stubber later
        new QueryBuilder()
            .buildStub()
            .addStubToList(sites)
            .namedQueryStub(TESTED_CLASS_NAME + '.getModifiedVirtualSites')
            .applyStub();
    }

    private static void applyIRBConfigurationQueryStub(List<VTR5_IRBLanguageConfiguration__c> configs) {
        // DK FIXME: rewrite onto VT_Stubber later
        new QueryBuilder()
            .buildStub()
            .addStubToList(configs)
            .namedQueryStub(TESTED_CLASS_NAME + '.getIRBConfigMap')
            .applyStub();
    }

    /**
     * This assertation method is suitable for scenarios 1-3
     * Scenarios: https://docs.google.com/spreadsheets/d/1ruLLxsoMM5H44mC5Dm1IW5zFs4mU9U_gu7NtqLDdFps/edit#gid=0
     *
     * @param  modifiedSites
     */
    private static void checkBasicBehavior(List<Virtual_Site__c> modifiedSites) {
        System.assertEquals(3, modifiedSites.size(), 'The modifiedSites list should include only 3 records');

        checkVirtualSite(modifiedSites[0], 'Study A site 1', 'US', STUDY_A, 'en_US', 'en_US;es_US');

        checkVirtualSite(modifiedSites[1], 'Study A site 3', 'CN', STUDY_A, 'zh_TW', 'zh_TW;en_US');

        checkVirtualSite(modifiedSites[2], 'Study C site 1', 'US', STUDY_C, 'en_US', 'en_US');

        checkStudyBHasNoSitesWithPopulatedLanguages(modifiedSites);
    }

    private static List<Virtual_Site__c> getSitesWithPopulatedLanguages(List<Virtual_Site__c> sites) {
        List<Virtual_Site__c> result = new List<Virtual_Site__c>();
        for (Virtual_Site__c site : sites) {
            if (!String.isEmpty(site.VTR5_DefaultIRBLanguage__c)) {
                result.add(site);
            }
        }
        return result;
    }

    private static List<Virtual_Site__c> getSitesWithoutCountryAndLanguages(List<Virtual_Site__c> sites) {
        List<Virtual_Site__c> result = new List<Virtual_Site__c>();
        for (Virtual_Site__c site : sites) {
            Boolean siteHasNoCountryAndNoLanguages = String.isEmpty(site.VTR3_Country__c) &&
                String.isEmpty(site.VTR5_DefaultIRBLanguage__c) && String.isEmpty(site.VTR5_IRBApprovedLanguages__c);

            if (siteHasNoCountryAndNoLanguages) {
                result.add(site);
            }
        }
        return result;
    }

    private static void checkVirtualSite(Virtual_Site__c site, String name, String country, Id studyId,
                                         String defaultLanguage, String approvedLanguages) {
        System.assertEquals(name, site.Name);
        System.assertEquals(country, site.VTR3_Country__c);
        System.assertEquals(studyId, site.VTD1_Study__c);
        System.assertEquals(defaultLanguage, site.VTR5_DefaultIRBLanguage__c);
        System.assertEquals(approvedLanguages, site.VTR5_IRBApprovedLanguages__c);
    }

    private static void checkStudyBHasNoSitesWithPopulatedLanguages(List<Virtual_Site__c> sites) {
        Integer countModifiedSitesRelatedToStudyB = 0;
        for (Virtual_Site__c site : sites) {
            Boolean siteHasPopulatedLanguages = !String.isEmpty(site.VTR5_IRBApprovedLanguages__c) ||
                !String.isEmpty(site.VTR5_DefaultIRBLanguage__c);

            if (site.VTD1_Study__c == STUDY_B && siteHasPopulatedLanguages) {
                countModifiedSitesRelatedToStudyB++;
            }
        }

        System.assertEquals(0, countModifiedSitesRelatedToStudyB,
            'The sites list shouldn\'t countain any records related to study B with populated languages fields');
    }
}