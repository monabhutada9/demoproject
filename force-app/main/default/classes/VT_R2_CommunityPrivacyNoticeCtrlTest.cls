@IsTest
private class VT_R2_CommunityPrivacyNoticeCtrlTest {

    @TestSetup
    private static void testSetup(){
        new DomainObjects.User_t()
        .setEmail('test@test1.com')
                .persist();
    }

    @IsTest static void testAccept() {
        User u = [SELECT id from User where Email = 'test@test1.com' LIMIT 1];
        System.runAs(u) {
            Test.startTest();
            try {
                VT_D1_CommunityPrivacyNoticeController.accept();
            } catch (Exception ex) {
            }
            Test.stopTest();
        }
    }
    @IsTest static void testCancel() {
        VT_D1_CommunityPrivacyNoticeController.cancel();
      }

}