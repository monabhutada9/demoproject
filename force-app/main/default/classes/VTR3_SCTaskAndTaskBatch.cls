global class VTR3_SCTaskAndTaskBatch implements Database.Batchable<sObject>, Database.Stateful {

	global Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator(
				'SELECT Id, VTD1_Actual_Visit__c, VTD1_Document__c, VTD1_Patient__c, (SELECT Id, VTD1_Actual_Visit__c, Document__c, VTD1_Case_lookup__c FROM Tasks WHERE What.Type = \'VTD1_SC_Task__c\')FROM VTD1_SC_Task__c'
		);
	}
	global void execute(Database.BatchableContext bc, List<VTD1_SC_Task__c> scope){
		List<Task> taskList = new List<Task>();
		for (VTD1_SC_Task__c scTask : scope) {
			if(scTask.Tasks.size() > 0){
				scTask.Tasks[0].VTD1_Actual_Visit__c = scTask.VTD1_Actual_Visit__c;
				scTask.Tasks[0].Document__c = scTask.VTD1_Document__c;
				scTask.Tasks[0].VTD1_Case_lookup__c = scTask.VTD1_Patient__c;
				taskList.add(scTask.Tasks[0]);
			}
		}
		update taskList;
	}
	global void finish(Database.BatchableContext bc){
		System.debug('records processed. Shazam!');
	}
}