@IsTest
public with sharing class VT_D1_PatientKitsDevAndAccCountingTest {

    public static void recountConnectedDevicesTest(){
       	recountByDevName('VTD1_Connected_Devices');
    }
    
    public static void recountStudyHubTabletTest(){
       	recountByDevName('VTD1_Study_Hub_Tablet');
    }
    static void recountByDevName(String recTypeDevName){
        try {            
            Case cs = new Case(Subject='Test Case');
            insert cs;
            
            Id recTypeId = Schema.SObjectType.VTD1_Protocol_Kit__c.getRecordTypeInfosByDeveloperName().get(recTypeDevName).getRecordTypeId(); //'Study Hub Tablet'

            VTD1_Protocol_Kit__c protKit = new VTD1_Protocol_Kit__c(RecordTypeId = recTypeId);
            insert protKit;
            
            VTD1_Patient_Kit__c patKit = new VTD1_Patient_Kit__c(VTD1_Case__c = cs.Id, VTD1_Protocol_Kit__c = protKit.id);
            insert patKit;
            
            List<VTD1_Patient_Device__c> devices = new List<VTD1_Patient_Device__c>();
            for (Integer a = 0; a < 10; a++) {
                devices.add(new VTD1_Patient_Device__c(VTD1_Patient_Kit__c = patKit.id, VTD1_Case__c = cs.Id));
            }
            insert devices;
            
            List<VTD1_Patient_Accessories__c> accessories = new List<VTD1_Patient_Accessories__c>();
            for (VTD1_Patient_Device__c device : devices) {
                for (Integer a = 0; a < 10; a++) {
                    accessories.add(new VTD1_Patient_Accessories__c(VTD1_Patient_Device__c = device.id, VTD1_Case__c = cs.Id));
                }
            }
            insert accessories;
            
            VT_D1_PatientKitsDevAndAccCounting.recount(new List<Id>{cs.Id});
            
            VTD1_Patient_Kit__c patKitRecounted = [
                SELECT VTD1_Devices_Count__c 
                FROM VTD1_Patient_Kit__c 
                WHERE Id = :patKit.Id
            ];
            System.debug('Devices Count = ' + patKitRecounted.VTD1_Devices_Count__c);
            System.assertEquals(10,patKitRecounted.VTD1_Devices_Count__c);
            
			Map<Id,VTD1_Patient_Device__c> devicesMap = new Map<Id,VTD1_Patient_Device__c>(devices);
			AggregateResult[] groupedResults = [
                SELECT SUM(VTD1_Accessories_Count__c) accessoriesCount
                FROM VTD1_Patient_Device__c 
                WHERE Id IN :devicesMap.keySet()
            ];
            
            System.debug('Accessories Count = ' + (Integer)groupedResults[0].get('accessoriesCount'));
            System.assertEquals(100,(Integer)groupedResults[0].get('accessoriesCount'));
        } catch (Exception e) {
            System.debug(e.getMessage());
        }
    }
}