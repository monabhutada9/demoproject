/**
* @author: Carl Judge
* @date: 31-May-18
* @description: Handler for VTD1_Chat_Member__c trigger. When a user is added or removed as a chat member, share the chat record and add them as a chatter follow
**/

public without sharing class VTD1_ChatMemberHandler {

    public void onAfterInsert(List<VTD1_Chat_Member__c> recs) {
        doAdd(getMap(recs));
    }

    public void onAfterDelete(List<VTD1_Chat_Member__c> recs) {
        doRemove(getMap(recs));
    }

    public void onAfterUndelete(List<VTD1_Chat_Member__c> recs) {
        doAdd(getMap(recs));
    }

    private void doAdd(Map<Id, Set<Id>> chatToUserIdsMap) {
        recalcSharing(chatToUserIdsMap);
        addFollows(chatToUserIdsMap);
    }

    private void doRemove(Map<Id, Set<Id>> chatToUserIdsMap) {
        recalcSharing(chatToUserIdsMap);
        removeFollows(chatToUserIdsMap);
    }

    private void recalcSharing(Map<Id, Set<Id>> chatToUserIdsMap) {
        Set<Id> chatIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        for (Id chatId : chatToUserIdsMap.keySet()) {
            chatIds.add(chatId);
            userIds.addAll(chatToUserIdsMap.get(chatId));
        }
        VT_R5_SharingChainFactory.createSharingQueueable(null, userIds, chatIds, VTD1_Chat__c.getSObjectType()).enqueue();
    }

    private Map<Id, Set<Id>> getMap(List<VTD1_Chat_Member__c> recs) {
        Map<Id, Set<Id>> chatToUserIdsMap = new Map<Id, Set<Id>>();
        for (VTD1_Chat_Member__c item : recs) {
            if (!chatToUserIdsMap.containsKey(item.VTD1_Chat__c)) {
                chatToUserIdsMap.put(item.VTD1_Chat__c, new Set<Id>());
            }
            chatToUserIdsMap.get(item.VTD1_Chat__c).add(item.VTD1_User__c);
        }

        return chatToUserIdsMap;
    }

    private List<Id> getUserIdsFromMap(Map<Id, Set<Id>> chatToUserIdsMap) {
        List<Id> userIds = new List<Id>();
        for (Set<Id> uIds : chatToUserIdsMap.Values()) {
            userIds.addAll(uIds);
        }
        return userIds;
    }

    private void addFollows(Map<Id, Set<Id>> chatToUserIdsMap) {
        Map<Id, User> userMap = new Map<Id, User>([
                SELECT Id, UserType FROM User WHERE Id IN :getUserIdsFromMap(chatToUserIdsMap)
        ]);

        List<EntitySubscription> newSubs = new List<EntitySubscription>();
        for (Id chatId : chatToUserIdsMap.keySet()) {
            for (Id uId : chatToUserIdsMap.get(chatId)) {
                if (userMap.containsKey(uId) && userMap.get(uId).UserType == 'Standard'){
                    newSubs.add(new EntitySubscription(
                            ParentId = chatId,
                            SubscriberId = uId
                    ));
                }
            }
        }

        Database.insert(newSubs, false);
    }

    private void removeFollows(Map<Id, Set<Id>> chatToUserIdsMap) {
        List<EntitySubscription> toDelete = new List<EntitySubscription>();
        for (EntitySubscription item : [
                SELECT Id, ParentId, SubscriberId
                FROM EntitySubscription
                WHERE ParentId IN :chatToUserIdsMap.keySet()
        AND SubscriberId IN :getUserIdsFromMap(chatToUserIdsMap)
        ]) {
            if (chatToUserIdsMap.get(item.ParentId).contains(item.SubscriberId)) {
                toDelete.add(item);
            }
        }

        if (!toDelete.isEmpty()) { delete toDelete; }
    }
}