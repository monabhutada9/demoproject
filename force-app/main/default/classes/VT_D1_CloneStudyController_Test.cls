/**
 * @author: Carl Judge, Dmitry Kovalev
 * @date: 2020-09-14
 * @description: Test class for VT_D1_CloneStudyController Visualforce page controller.
 * @see VT_D1_CloneStudyController
 **/
@IsTest
public without sharing class VT_D1_CloneStudyController_Test {
    public static void cloneActionTest() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM 	HealthCloudGA__CarePlanTemplate__c LIMIT 1];

        // Test
        Test.startTest();
        VT_D1_CloneStudyController controller = new VT_D1_CloneStudyController(new ApexPages.StandardController(study));
        PageReference pageRef = controller.cloneAction();
        Test.stopTest();

        // Check study created
        Integer studyCount = [SELECT COUNT() FROM HealthCloudGA__CarePlanTemplate__c];
        System.assertEquals(2, studyCount);

        // Check clone study name
        HealthCloudGA__CarePlanTemplate__c studyClone = [
            SELECT Id, Name
            FROM HealthCloudGA__CarePlanTemplate__c
            WHERE Id != :study.Id
            LIMIT 1
        ];
        System.assertEquals(controller.inputStudy.Name, studyClone.Name);

        // Check URL created correctly
        Id actualId = Id.valueOf(pageRef.getUrl().substring(1)); // trim slash
        System.assertEquals(studyClone.Id, actualId);

        // Check chats created
        List<VTD1_Chat__c> chats = [SELECT Id, Name FROM VTD1_Chat__c LIMIT 10];
        System.assertEquals(3, chats.size());

        // Check collaboration groups created
        List<CollaborationGroup> collabGroups = [SELECT Id, Name FROM CollaborationGroup LIMIT 10];
        System.assertEquals(2, collabGroups.size());
    }
}