/**
* @author: Carl Judge
* @date: 07-Oct-19
**/

@IsTest
public with sharing class VT_R3_PublishedAction_CreateCGTest {

    public static void doTest() {
        HealthCloudGA__CandidatePatient__c cand = [
            SELECT Id, HealthCloudGA__AccountId__c
            FROM HealthCloudGA__CandidatePatient__c
            LIMIT 1
        ];

        cand.VTR2_Caregiver_First_Name__c = 'cgTestFirstName';
        cand.VTR2_Caregiver_Email__c = 'cgTestEmail@test.com';
        cand.VTR2_Caregiver_Preferred_Language__c = 'en_US';

        VT_R3_PublishedAction_CreateCaregivers action = new VT_R3_PublishedAction_CreateCaregivers(new List<HealthCloudGA__CandidatePatient__c>{cand});
        Test.startTest();
        action.publish();
        Test.stopTest();
    }
}