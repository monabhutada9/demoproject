public with sharing class GetStatsForVideoConfController {

    public class VideoConfArchiveWrapper {

        @AuraEnabled public String name { get; set; }
        @AuraEnabled public String status { get; set; }
        @AuraEnabled public String reason { get; set; }
        @AuraEnabled public String createdAt { get; set; }
        @AuraEnabled public String duration { get; set; }
    }

    @AuraEnabled
    public static String getArchivesListBySession(Id videoConfId) {
        try {
            System.debug('!!! videoConfId=' + videoConfId);
            List<Video_Conference__c> vcList = [
                    SELECT
                            Id,
                            SessionId__c
                    FROM Video_Conference__c
                    WHERE Id = :videoConfId
            ];
            if (vcList == null || vcList.isEmpty()) return 'No record Video Conference found.';
            Video_Conference__c videoConf = vcList[0];
            String sessionId = videoConf.SessionId__c;
            if (String.isBlank(sessionId)) return 'SessionId is null. Stats cannot be received.';
            HttpRequest req = new HttpRequest();
            req.setEndpoint('https://2sg9ju3d4l.execute-api.us-east-2.amazonaws.com/getListArchived');//('https://pwte8ptkc3.execute-api.eu-west-1.amazonaws.com/production/jwt');
            req.setMethod('POST');
            req.setBody('{"sessionId" :\"' + sessionId + '\"}');
            Http http = new Http();
            HttpResponse res = http.send(req);
            String resString = res.getBody();
            System.debug('Res json ' + resString);
            if (String.isBlank(resString)) return 'Empty result.';
            Integer index = resString.indexOf('{"archives":');
            String jsonStr = resString.substring(index + 12, resString.length() - 1);
            System.debug('!!! jsonStr=' + jsonStr);
            List<VideoConfArchiveWrapper> archList = (List<VideoConfArchiveWrapper>) JSON.deserialize(jsonStr, List<VideoConfArchiveWrapper>.class);
            if (archList == null || archList.isEmpty()) return 'List of archives is empty.';
            for (VideoConfArchiveWrapper wcwItem : archList) {
                System.debug('!!! archive = ' + wcwItem);
            }
            VideoConfArchiveWrapper wcw = archList[0];
            Long createdAtTimestamp = Long.valueOf(wcw.createdAt);
            Datetime startDate = Datetime.newInstance(createdAtTimestamp);
            System.debug('!!! startDate=' + startDate);
            //startDate = startDate.addSeconds(createdAtTimestamp);
            videoConf.Call_Start__c = startDate;
            Integer durSeconds = Integer.valueOf(wcw.duration);
            String durationStr = Datetime.valueOf(durSeconds * 1000).formatGmt('HH:mm:ss');
            videoConf.Duration__c = durationStr;
            System.debug('!!! durationStr=' + durationStr);

            /*
            system.debug('dur='+dur);
            Integer durMin = dur/60;
            system.debug('durMin='+durMin);
            Integer durSec = dur-durMin*60;
            system.debug('durSec='+durSec);

            Time durationTime =
            Time.newInstance(12, durMin, durSec, 0);
            videoConf.Duration__c = durationTime;//Time.newInstaince(0, 0, dur, 0);//Time.valueOf(dur);
            */

            update videoConf;
        } catch (Exception exc) {
            return 'ERROR';
        }

        //system.debug('!!! archs='+archList);

        return 'OK';
    }
}