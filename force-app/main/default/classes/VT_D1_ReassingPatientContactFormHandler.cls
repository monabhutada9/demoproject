//called from Case Trigger
public without sharing class VT_D1_ReassingPatientContactFormHandler {
 
 	public class ReassingPatientContactException extends Exception{}
    public VT_D1_ReassingPatientContactFormHandler(List<Case> caselist,Map<ID,Case> oldCaseMap, Map<ID,Case> newCaseMap){
    	boolean ok= false;
    	String userProf;
    	//String ownProf;
    	List<Group> gr;
    	Map<Id,List<Group>> ownGroup;
    	//Map<Id,Profile> ownProfile;
    	Map<Id,List<Group>> ownPool;
    	List<Id> ownIds;
    	List<RecordType>  rt = [SELECT ID, Name FROM RecordType WHERE DeveloperName ='VTD1_PCF' OR DeveloperName ='VTD1_PCF_Read_Only'];
    	if(rt.size()==0)
    		throw new ReassingPatientContactException('You can not reassign, record typs are incorrect, contact system administrator');
    	//List<Case> caseWithRT = [SELECT ID FROM Case WHERE RecordTypeId in :rt AND Id in:caselist];	///todo
    	List<Case> caseWithRT = new List<Case>();
    	Integer i=0;
    	/*for(List<Case> cas:caselist){
    		if(rt.contains(cas){
    		    caseWithRT.add(cas[i]);	
    		}
    		i++;	
    	}*/
    	List<Profile> uProf=[SELECT Name FROM Profile WHERE Id = :userinfo.getProfileId()];
    	if(uProf.size()>0)
    		userProf = uProf[0].Name;
    	else
    		throw new ReassingPatientContactException('You can not reassign, your profile is empty, contact system administrator');	
    	List<VT_D1_ReassignmentRuls__mdt> rules = [SELECT Rule1__c, Rule2__c, Rule3__c FROM VT_D1_ReassignmentRuls__mdt WHERE QualifiedApiName = 'Rules'];
    	if(rules.size()==0)
    		throw new ReassingPatientContactException('You can not reassign, setting is empty, contact system administrator');

//comment from 35 to 106 because  caseWithRT is always empty (because 22 row is commented by Release manager  2/4/2019 11:45 PM)
//    	for(Case c:caseWithRT){
//    		ownIds.add(c.OwnerId);
//    	}
//    	for(Id oId:ownIds){
//				//	A specific Study Concierge in the study’s pool of SCs
//				ownGroup.put(oId, [SELECT DeveloperName,Id,Name,Type FROM Group where Type = 'Queue' AND DeveloperName LIKE '%SC%' AND Id in
//												(SELECT GroupId FROM GroupMember WHERE UserOrGroupId =: oId)]);
//				//ownProfile.put(oId, [SELECT Name FROM Profile WHERE Id = :oId]);
//				ownPool.put(oId, [SELECT DeveloperName,Id,Name,Type FROM Group where Type = 'Queue' AND DeveloperName LIKE '%SC%' AND id=:oId]);
//    	}
//    	/*for(Case c:caseWithRT){
//				ownProfile.put(c.OwnerId, [SELECT Name FROM Profile WHERE Id = :c.OwnerId]);//todo
//    	}*/
//    	for(Case c:caseWithRT){
//	    	//ownProf = ownProfile.get(c.OwnerId).Name;
//    		if((oldCaseMap.get(c.Id).OwnerId != newCaseMap.get(c.Id).OwnerId) /*&& (c.ParentId!= null || c.VTD1_Clinical_Study_Membership__c!=null)*/){
//				if(c.VTD1_PCF_Safety_Concern_Indicator__c!=null && c.VTD1_PCF_Safety_Concern_Indicator__c=='No') {
//					if(userProf==rules[0].Rule1__c){//The Study Concierge can route to:
//						/*if(ownProf == 'Study Concierge'){ //The study’s pool of Study Concierges
//							ok= true;
//						}*/
//						gr = ownPool.get(c.OwnerId);
//						if(gr.size()>0){
//							ok= true;
//						}
//						//check that owner id is queue and Study Concierges
//						gr= ownGroup.get(c.OwnerId);
//						if(!ok && (gr.size()>0 || c.OwnerId==c.VTD1_Primary_PG__c || c.OwnerId==c.VTD1_Secondary_PG__c)){
//									//The patient’s Patient Guide || The patient’s Backup Patient Guide
//							ok= true;
//						}
//					}
//
//					if(userProf==rules[0].Rule2__c){//The Patient Guide can route to:
//						/*if(ownProf == 'Study Concierge'){ //The study’s pool of Study Concierges
//							ok= true;
//						}*/
//						gr = ownPool.get(c.OwnerId);
//						if(gr.size()>0){
//							ok= true;
//						}
//						gr= ownGroup.get(c.OwnerId);
//						//	A specific Study Concierge in the study’s pool of SCs
//						if(!ok && (gr.size()>0 || c.OwnerId==c.VTD1_Secondary_PG__c || c.OwnerId==c.VTD1_PI_contact__c)){
//							//The patient’s Backup Patient Guide || The patient’s Primary Investigator
//							ok= true;
//						}
//					if(userProf==rules[0].Rule3__c)//The Primary Investigator can route to:
//						//The study’s pool of Study Concierges
//						/*if(ownProf == 'Study Concierge'){ //The study’s pool of Study Concierges
//							ok= true;
//						}*/
//						gr = ownPool.get(c.OwnerId);
//						if(gr.size()>0){
//							ok= true;
//						}
//						gr= ownGroup.get(c.OwnerId);
//							//	A specific Study Concierge in the study’s pool of SCs
//						if(!ok && (gr.size()>0 || c.OwnerId==c.VTD1_Primary_PG__c || c.OwnerId==c.VTD1_Secondary_PG__c)){
//									//The patient’s Patient Guide || The patient’s Backup Patient Guide
//							ok= true;
//						}
//					}
//					if(!ok){
//							throw new ReassingPatientContactException('You can not reassign, contact system administrator');
//					}
//				}else{
//						throw new ReassingPatientContactException('You can not reassign in this state of Safety Concern Indicator');
//		    	}
//
//    		}
//	}
}
}