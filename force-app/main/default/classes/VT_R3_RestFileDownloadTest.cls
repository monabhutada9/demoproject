/**
 * Created by user on 02-Jul-19.
 */
@isTest
public with sharing class VT_R3_RestFileDownloadTest {

	public static void getFile(){
//		Create File
		ContentVersion contentVersion = new ContentVersion();
		contentVersion.Title = 'ABC';
		contentVersion.PathOnClient = 'test';
		contentVersion.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');
		insert contentVersion;

		contentVersion = [SELECT Id, ContentDocumentId FROM ContentVersion LIMIT 1];

		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();
		request.requestURI = '/Patient/FileDownload/' + contentVersion.ContentDocumentId;
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response  = response;
		String responseString = VT_R3_RestFileDownload.getResponse();
		System.assert(!String.isEmpty(responseString));
	}
}