/**
 * Created by User on 19/05/14.
 */
@isTest
public with sharing class VT_D1_TreatmentArmSharingControllerTest {

    public static void getRowsTest(){
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];

        Test.startTest();
        System.assertNotEquals(null,VT_D1_TreatmentArmSharingController.getRows(study.Id));
        Test.stopTest();
    }
}