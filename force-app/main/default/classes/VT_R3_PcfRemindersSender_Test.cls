/**
 * Created by Dmitry Yartsev on 30.09.2019.
 */

@isTest
private class VT_R3_PcfRemindersSender_Test {

    private static final String FLOW_CURRENT_ELEMENT_NAME = 'PCF_SC_Reminder_Sender_Wait';

    @testSetup
    static void setup() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
        List <VTD2_TN_Catalog_Code__c> tnCatalogCodes = new List<VTD2_TN_Catalog_Code__c>();
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'N401',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Notification').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Case',
                VTR2_T_CSM__c = 'Case.VTD1_Clinical_Study_Membership__c',
                VTD2_T_Description_Parameters__c = 'test',
                VTD2_T_Link_to_related_event_or_object__c = '"contact-form?pcfId=" & Case.Id & "%amp%caseId=" & Case.VTD1_Clinical_Study_Membership__c',
                VTD2_T_Message__c = 'VTR3_SCMainRemindersText',
                VTD2_T_Title__c = 'Safety',
                VTD2_T_Has_direct_link__c = true,
                VTR2_Pass_if_empty_recipient__c = true,
                VTD2_T_Type__c = 'General'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'N402',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Notification').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Case',
                VTR2_T_CSM__c = 'Case.VTD1_Clinical_Study_Membership__c',
                VTD2_T_Description_Parameters__c = 'test',
                VTD2_T_Link_to_related_event_or_object__c = '"contact-form?pcfId=" & Case.Id & "%amp%caseId=" & Case.VTD1_Clinical_Study_Membership__c',
                VTD2_T_Message__c = 'VTR3_SCOwnerBasedRemindersText',
                VTD2_T_Title__c = 'Safety',
                VTD2_T_Has_direct_link__c = true,
                VTR2_Pass_if_empty_recipient__c = true,
                VTD2_T_Type__c = 'General'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'P401',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('PostToChatter').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Case',
                VTD2_T_Description_Parameters__c = 'test',
                VTD2_T_Message__c = 'VTR3_SCMainRemindersText',
                VTD2_T_Title__c = 'Safety',
                VTR2_Pass_if_empty_recipient__c = true,
                VTD2_T_Type__c = 'General'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'P402',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('PostToChatter').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Case',
                VTD2_T_Description_Parameters__c = 'test',
                VTD2_T_Message__c = 'VTR3_SCOwnerBasedRemindersText',
                VTD2_T_Title__c = 'Safety',
                VTR2_Pass_if_empty_recipient__c = true,
                VTD2_T_Type__c = 'General'
        ));
        insert tnCatalogCodes;
    }

    @isTest
    static void sendNonEscRemindersPITest() {
        Test.startTest();
        Case cas = preparePcf('PIUser1', false);
        processReminders(cas.Id);


        Datetime dt = Datetime.now();
        cas.VTD2_PCF_Safety_Concern_Indication_Date__c = dt.addHours(-72);
        cas.VTR3_PCF_Assignment_Date__c = dt.addHours(-72);
        update cas;
        Test.stopTest();
        processReminders(cas.Id, dt);

        System.assert((Integer)[SELECT COUNT(Id) FROM VTD1_NotificationC__c WHERE VDT2_Unique_Code__c IN ('N401', 'N402')].get(0).get('expr0') > 0);
    }

    @isTest
    static void sendEscRemindersPITest() {
        Test.startTest();
        Case cas = preparePcf('PIUser1', true);
        processReminders(cas.Id);

        Datetime dt = Datetime.now();
        cas.VTD2_PCF_Safety_Concern_Indication_Date__c = dt.addHours(-12);
        cas.VTR3_PCF_Assignment_Date__c = dt.addHours(-12);
        update cas;
        Test.stopTest();

        processReminders(cas.Id, dt);
        System.assert((Integer)[SELECT COUNT(Id) FROM VTD1_NotificationC__c WHERE VDT2_Unique_Code__c IN ('N401', 'N402')].get(0).get('expr0') > 0);
    }

    @isTest
    static void sendNonEscRemindersPGTest() {
        Test.startTest();
        Case cas = preparePcf('PrimaryPGUser1', false);
        processReminders(cas.Id);
        Test.stopTest();

        Datetime dt = Datetime.now();
        cas.VTD2_PCF_Safety_Concern_Indication_Date__c = dt.addHours(-72);
        update cas;
        processReminders(cas.Id, dt);

        System.assert((Integer)[SELECT COUNT(Id) FROM VTD1_NotificationC__c WHERE VDT2_Unique_Code__c IN ('N401', 'N402')].get(0).get('expr0') > 0);
    }

    @isTest
    static void sendEscRemindersPGTest() {
        Test.startTest();
        Case cas = preparePcf('PrimaryPGUser1', true);
        processReminders(cas.Id);
        Test.stopTest();

        Datetime dt = Datetime.now();
        cas.VTD2_PCF_Safety_Concern_Indication_Date__c = dt.addHours(-12);
        cas.VTD1_IsEscalatedDateTime__c = dt.addHours(-12);
        update cas;
        processReminders(cas.Id, dt);

        System.assert((Integer)[SELECT COUNT(Id) FROM VTD1_NotificationC__c WHERE VDT2_Unique_Code__c IN ('N401', 'N402')].get(0).get('expr0') > 0);
    }

    @isTest
    static void safetyConcernEmailControllerPIOwnerTest() {
        Test.startTest();
        Case cas = preparePcf('PIUser1', false);
        Test.stopTest();

        VT_R3_SafetyConcernEmailController c = new VT_R3_SafetyConcernEmailController();
        System.assertEquals(null, c.communityPIUrl);
        System.assertEquals(null, c.getCaseInfo());
        c.relatedTo = cas.Id;
        c.isMainReminder = 'true';
        System.assertNotEquals(null, c.getCaseInfo());
        c.isMainReminder = 'false';
        System.assertNotEquals(null, c.getCaseInfo());
    }

    @isTest
    static void safetyConcernEmailControllerPGOwnerTest() {
        Test.startTest();
        Case cas = preparePcf('PrimaryPGUser1', true);
        Test.stopTest();

        VT_R3_SafetyConcernEmailController c = new VT_R3_SafetyConcernEmailController();
        System.assertEquals(null, c.communityPIUrl);
        System.assertEquals(null, c.getCaseInfo());
        c.relatedTo = cas.Id;
        c.isMainReminder = 'true';
        System.assertNotEquals(null, c.getCaseInfo());
        c.isMainReminder = 'false';
        System.assertNotEquals(null, c.getCaseInfo());
    }

    private static Case preparePcf(String owner, Boolean isEscalated) {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        Case caseCarePlan = [SELECT Id FROM Case LIMIT 1];
        List <User> users = [SELECT Id, Name FROM User WHERE FirstName IN (:owner) ORDER BY FirstName];

        Case cas = VT_D1_TestUtils.createCase('VTD1_PCF', null, null, null, null, null, null);
        cas.VTD1_Study__c = study.Id;
        cas.VTD1_Clinical_Study_Membership__c = caseCarePlan.Id;
        cas.OwnerId = users[0].Id;
        cas.VTD1_PCF_Safety_Concern_Indicator__c = 'Possible';
        cas.isEscalated = isEscalated;
        cas.VTD2_PCF_Safety_Concern_Indication_Date__c = Datetime.now();
        cas.VTR3_PCF_Assignment_Date__c = Datetime.now();
        if (isEscalated) {
            cas.VTD1_IsEscalatedDateTime__c = Datetime.now();
        }
        insert cas;

        return cas;
    }

    private static void processReminders(Id caseId) {
        processReminders(caseId, null);
    }

    private static void processReminders(Id caseId, Datetime runDatetime) {
        List<VT_R3_PcfRemindersSender.ParamsHolder> args = new List<VT_R3_PcfRemindersSender.ParamsHolder>();
        args.add(new VT_R3_PcfRemindersSender.ParamsHolder(caseId, runDatetime));
        VT_R3_PcfRemindersSender.sendReminders(args);
    }
}