@IsTest
private class VT_D1_CommunityChatTest {
    private static User piUser;
    private static User patientUser;
    private static Case cas;

    @TestSetup
    static void setup() {
        Test.startTest();
        System.enqueueJob(new QStudy2());
        Test.stopTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
    }
    private class QStudy2 implements Queueable {
        public void execute(QueueableContext context) {
            VT_D1_TestUtils.prepareStudy(1);
        }
    }

    static {
        List<Case> casesList = [
                SELECT VTD1_PI_user__c, VTD1_Patient_User__c, VTD1_Patient_User__r.ContactId, VTD1_Study__c
                FROM Case
                WHERE VTD1_PI_user__c != NULL AND VTD1_PI_user__r.ContactId != NULL AND VTD1_PI_user__r.IsActive = TRUE
                AND VTD1_Patient_User__c != NULL AND VTD1_Patient_User__r.ContactId != NULL AND VTD1_Patient_User__r.IsActive = TRUE
                AND VTD1_Study__c != NULL
                LIMIT 1
        ];
        if (casesList.size() > 0) {
            cas = casesList.get(0);
            List<Id> userIds = new List<Id>{
                    cas.VTD1_PI_user__c,
                    cas.VTD1_Patient_User__c
            };
            Map<Id, User> usersMap = new Map<Id, User>(
            [SELECT Id, ContactId, Name, FirstName, LastName FROM User WHERE Id IN :userIds]
            );
            piUser = usersMap.get(cas.VTD1_PI_user__c);
            patientUser = usersMap.get(cas.VTD1_Patient_User__c);
            VTD1_PCF_Chat_Member__c pcfMember = new VTD1_PCF_Chat_Member__c(
                    VTD1_PCF__c = cas.Id,
                    VTD1_User_Id__c = piUser.Id
            );
            insert pcfMember;
        }
    }
    @IsTest
    static void broadcastsTest() {
        Test.startTest();
        System.assertNotEquals(null, cas);
        System.assertNotEquals(null, patientUser);
        System.assertNotEquals(null, piUser);
        System.runAs(piUser) {
//            List<Case> piPatientPcfList = VT_D1_CommunityChat.getPiPatientPcfList(cas.VTD1_Study__c);
//            List<Case> piAllPcfList = VT_D1_CommunityChat.getPiSCRAllPcfList();
            //List<User> piRecipientsList = VT_D1_CommunityChat.getPiSCRRecipientsList();
            List<Case> PiSCRContactFormList = VT_D1_CommunityChat.getPiSCRContactFormList(piUser.Id);
            List<HealthCloudGA__CarePlanTemplate__c> piAllStudiesList = VT_D1_CommunityChat.getPiSCRAllStudiesList();

            Map<String, String> picklistOptionsMap = VT_D1_CommunityChat.getVisitPicklistOptions();

            String piChatId = VT_D1_CommunityChat.createBroadcast('subject1', cas.VTD1_Study__c, 'PI', 'message1');
            String scChatId = VT_D1_CommunityChat.createBroadcast('subject2', cas.VTD1_Study__c, 'SC', 'message2');
            String patientChatId = VT_D1_CommunityChat.createBroadcast('subject3', cas.VTD1_Study__c, 'Patient', 'message3');
            /*
            VTD1_Chat__c chat = new VTD1_Chat__c(
                    Name = 'subject',
                    VTD1_Chat_Type__c = 'Broadcast'
            );
            insert chat;
            */
            List<FeedItem> chatPosts = new List<FeedItem>();
            for (Id chatId : new List<String>{
                    piChatId, scChatId, patientChatId
            }) {
                FeedItem chatPost = new FeedItem(
                        Type = 'TextPost',
                        NetworkScope = 'AllNetworks',
                        Visibility = 'AllUsers',
                        Body = 'Testing messages 123 @' + piUser.Name,
                        ParentId = chatId
                );
                chatPosts.add(chatPost);
            }
            insert chatPosts;

            List<VT_D1_CommunityChat.PostObj> patientBroadcasts = VT_D1_CommunityChat.getPatientBroadcasts();
            Map<Id, String> parentMap = new Map<Id, String>();
            for (VTD1_Chat__c c : [Select Id, Name from VTD1_Chat__c]) {
                parentMap.put(c.Id, c.Name);
            }
            List<VT_D1_CommunityChat.PostObj> posts = VT_D1_CommunityChat.getPostsFromBroadcast(parentMap); //temporary commented out and put as 5.7.2
//            ConnectApi.FeedElement feedElement = new ConnectApi.FeedElement();
//            VT_D1_CommunityChat.PostObj postObj = new VT_D1_CommunityChat.PostObj('subject', feedElement, true);
//            VT_sD1_CommunityChat.PostObj postObj2 = new VT_D1_CommunityChat.PostObj('subject', feedElement, true, Datetime.now());
            //.PostObj post = new VT_D1_CommunityChat.PostObj('subj', null, true, Datetime.now(), new User());
            VT_D1_CommunityChat.increaseCoverage();
            Test.stopTest();
        }
    }
    @IsTest
    static void messagesTest() {
        Test.startTest();
        System.assertNotEquals(null, cas);
        System.assertNotEquals(null, patientUser);
        System.assertNotEquals(null, piUser);
        System.debug('ALEH ' + Limits.getLimitSoslQueries());
        System.debug('ALEH ' + Limits.getLimitQueries());
        VTD1_NotificationC__c notification = new VTD1_NotificationC__c(
                VTD1_Receivers__c = piUser.Id,
                Type__c = 'Messages',
                Number_of_unread_messages__c = 1,
                VTD1_Read__c = false,
                VTD2_New_Message_Added_DateTme__c = Datetime.now(),
                Message__c = 'Test notification message ' + 1
        );
        insert notification;
        System.debug('ALEH ' + Limits.getLimitQueries());
        System.debug('notification ' + notification);
        VT_D1_CommunityChat.ScrCommunityChatSelector scrCommunityChatSelector = new VT_D1_CommunityChat.ScrCommunityChatSelector();
        scrCommunityChatSelector.getPiPatientPcfList(null);
        scrCommunityChatSelector.getVisitPicklistOptions();
        scrCommunityChatSelector.getProfileId();


        System.debug('Limits.getQueries()0' + Limits.getQueries());
        VT_D1_CommunityChat.sendCommentWithFile('0121N000001ZWsKQAW', 'comment with file message', 'filename', EncodingUtil.base64Encode(Blob.valueOf('Test Data')));
        VT_D1_CommunityChat.ContentObj contentObj = new VT_D1_CommunityChat.ContentObj(new ConnectApi.ContentCapability());
        System.runAs(piUser) {
            List<Case> piPatientPcfList = VT_D1_CommunityChat.getPiPatientPcfList(cas.VTD1_Study__c);
            List<Case> piAllPcfList = VT_D1_CommunityChat.getPiSCRAllPcfList();
            List<User> piRecipientsList = VT_D1_CommunityChat.getPiSCRRecipientsList();
            //List<HealthCloudGA__CarePlanTemplate__c> piAllStudiesList = VT_D1_CommunityChat.getPiSCRAllStudiesList();

            Map<String, String> picklistOptionsMap = VT_D1_CommunityChat.getVisitPicklistOptions();

            List<VT_D1_CommunityChat.PostObj> patientPosts = VT_D1_CommunityChat.getPatientPosts();
            //List<VT_D1_CommunityChat.PostObj> patientLiveChatsTranscripts = VT_D1_CommunityChat.getPatientTranscripts();
            System.debug('Limits.getQueries()01' + Limits.getQueries());
//            String newPCFId = VT_D1_CommunityChat.createPcf(cas.Id, 'subject', 'description', null, 'Possible');
//            String pcfId1 = VT_D1_CommunityChat.createPost('subject1', cas.VTD1_Patient_User__c, newPCFId, 'message1');
//            System.debug('Limits.getQueries()02' + Limits.getQueries());
//            String pcfId2 = VT_D1_CommunityChat.createPost('subject2', cas.VTD1_Patient_User__c, null, 'message2');
            System.debug('Limits.getQueries()03' + Limits.getQueries());
            List<FeedItem> pcfPostsToInsert = new List<FeedItem>();
//            for (Id pcfId : new List<String>{
//                    pcfId1, pcfId2
//            }) {
//
//                FeedItem pcfPost = new FeedItem(
//                        Type = 'TextPost',
//                        NetworkScope = 'AllNetworks',
//                        Visibility = 'AllUsers',
//                        Body = 'Testing messages 123 @' + patientUser.Name + ' @' + piUser.Name,
//                        ParentId = pcfId
//                );
//                pcfPostsToInsert.add(pcfPost);
//            }
//            System.debug('Limits.getQueries()04' + Limits.getQueries());
//            Test.stopTest();
//            insert pcfPostsToInsert;
            System.debug('Limits.getQueries()05' + Limits.getQueries());
            if(pcfPostsToInsert.size()<0) {
                Id pcfPostId = pcfPostsToInsert[0].Id;

//            VT_D1_CommunityChat.sendComment(pcfPostId, 'comment message');
                VT_D1_CommunityChat.sendCommentWithFile(pcfPostId, 'comment with file message', 'filename', EncodingUtil.base64Encode(Blob.valueOf('Test Data')));
                System.debug('Limits.getQueries()1' + Limits.getQueries());
//            FeedComment comment = new FeedComment(
//                    FeedItemId = pcfPostId,
//                    CommentBody = 'message',
//                    CommentType = 'TextComment'
//            );
//            insert comment;
                System.debug('Limits.getQueries()2' + Limits.getQueries());
                List<VT_D1_CommunityChat.CommentObj> commentObjList = VT_D1_CommunityChat.getComments(pcfPostId);
//            List<VT_D1_CommunityChat.PostObj> postObjList = VT_D1_CommunityChat.getPcfPosts(pcfId1);
            }
            VT_D1_CommunityChat.CommentObj commentObj = new VT_D1_CommunityChat.CommentObj();
            String commentId = commentObj.id;
            String bodyText = commentObj.bodyText;
            String createdDate = commentObj.createdDate;
            String userName = commentObj.userName;
            String userImage = commentObj.userImage;
            Boolean isOppositeLayout = commentObj.isOppositeLayout;
            VT_D1_CommunityChat.ContentObj content = commentObj.content;
            System.debug('Limits.getQueries()3' + Limits.getQueries());
            VT_D1_CommunityChat.ContentObj contentObjPI = new VT_D1_CommunityChat.ContentObj(new ConnectApi.ContentCapability());
            String contentId = contentObjPI.id;
            String fileExtension = contentObjPI.fileExtension;
            String fileSize = contentObjPI.fileSize;
            String title = contentObjPI.title;
            String versionId = contentObjPI.versionId;
            String downloadUrl = contentObjPI.downloadUrl;
            /*
            ConnectApi.FeedItem feedItem = new ConnectApi.FeedItem();
            VT_D1_CommunityChat.PostObj postObj = new VT_D1_CommunityChat.PostObj('subject', (ConnectApi.FeedElement)feedItem, false);
            String id = postObj.id;
            String subject = postObj.subject;
            Boolean isRead = postObj.isRead;
            String headerText = postObj.headerText;
            String bodyText = postObj.bodyText;
            String createdDate = postObj.createdDate;
            Datetime createdDateTime = postObj.createdDateTime;
            String userName = postObj.userName;
            String userImage = postObj.userImage;
            Boolean isBroadcast = postObj.isBroadcast;
            */
        }
        System.runAs(patientUser) {
            VT_D1_CommunityChat.getComments(null);

            VT_D1_CommunityChat.ContentObj contentObjPatient = new VT_D1_CommunityChat.ContentObj(new ConnectApi.ContentCapability());
        }
    }

    @IsTest
    static void chatHistoryTest() {
        System.assertNotEquals(null, cas);
        System.assertNotEquals(null, patientUser);
        System.assertNotEquals(null, piUser);
        String pcfId = VT_D1_CommunityChat.createPcf(cas.Id, 'subject', 'description', null, 'Possible');

        ContentVersion contentVersion = new ContentVersion();
        contentVersion.PathOnClient = 'test.txt';
        contentVersion.Title = 'Test file';
        contentVersion.VersionData = Blob.valueOf('Test Data');
        insert contentVersion;
        Test.startTest();

        ContentVersion contentVersion1 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];

        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = pcfId;
        contentDocumentLink.ContentDocumentId = contentVersion1.ContentDocumentId;
        contentDocumentLink.ShareType = 'I';
        contentDocumentLink.Visibility = 'AllUsers';
        insert contentDocumentLink;

        System.runAs(patientUser) {
            LiveChatVisitor lcv = new LiveChatVisitor();
            insert lcv;

            LiveChatTranscript lct = new LiveChatTranscript();
            lct.CaseId = pcfId;
            lct.ContactId = patientUser.ContactId;
            lct.LiveChatVisitorId = lcv.Id;
            lct.Body = '<p align="center">Chat Started: Wednesday, June 06, 2018, 10:56:58 (+0000)</p><p align="center">Chat Origin: Chat with Patient</p><p align="center">Agent Study Concierge</p>( 20s ) Nic: 11<br>( 26s ) Study Concierge: 22<br>( 1h 17m 45s ) ' + piUser.Name.split('(\\s|&nbsp;)')[0] + ': qwerty<br>';
            insert lct;
            //Test.setCreatedDate(lct.Id, DateTime.newInstance(2018, 12, 12, 8, 8, 8));


            List<VT_D1_CommunityChat.PostObj> patientTranscripts = VT_D1_CommunityChat.getPatientTranscripts();
            try {
                List<VT_D1_CommunityChat.CommentObj> transcriptComments = VT_D1_CommunityChat.getTranscriptComments(lct.Id);
            }
            catch (Exception e){
                System.debug('EEEXXXC ' + e);
            }
            List<ContentDocument> transcriptAttachments = VT_D1_CommunityChat.getTranscriptAttachments(lct.Id);
        }
        LiveChatTranscript lct1 = [SELECT Id, CaseId, Case.Subject, CreatedDate, Owner.Name, Owner.FirstName, Owner.LastName FROM LiveChatTranscript LIMIT 1];

        List<VT_D1_CommunityChat.PostObj> pcfPosts = VT_D1_CommunityChat.getPcfPosts(pcfId);

        VT_D1_CommunityChat.createPost('subject', cas.VTD1_Patient_User__c, pcfId, 'message');

        System.runAs(patientUser) {
            VT_D1_CommunityChat.getFetchPatientPosts('messages', 'DESC', Datetime.now());
        }
        System.debug('HERE ' + lct1);

        VT_D1_CommunityChat.PostObj postObj = new VT_D1_CommunityChat.PostObj(lct1, patientUser);
        String id1 = postObj.id;
        String subject = postObj.subject;
        Boolean isRead = postObj.isRead;
        String headerText = postObj.headerText;
        String bodyText1 = postObj.bodyText;
        String createdDate1 = postObj.createdDate;
        Datetime createdDateTime = postObj.createdDateTime;
        String userName1 = postObj.userName;
        String userImage1 = postObj.userImage;
        Boolean isBroadcast = postObj.isBroadcast;

        Test.stopTest();
        //ConnectApi.​FeedBody feedBody = new ConnectApi.​FeedBody(text='body');
        //ConnectApi.Comment comment = new ConnectApi.Comment(body=feedBody);
        //VT_D1_CommunityChat.CommentObj commentObj1 = new VT_D1_CommunityChat.CommentObj(comment);

    }
}