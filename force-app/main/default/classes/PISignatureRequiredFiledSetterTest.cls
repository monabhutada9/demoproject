/**
 * Created by Eugen Kharchuk on 02-Jul-19.
 */

@IsTest
private class PISignatureRequiredFiledSetterTest {
    private static TestData td;

    @IsTest
    public static void successFieldPopulatingUnitTest() {
        td = generateTestData();
        stubQueries();

        Test.startTest();
        Handler.TriggerContext context = prepareTriggerContext();
        new VT_R2_PISignatureRequiredFieldSetter().onBeforeInsert(context);
        Test.stopTest();

        System.assertEquals(true, ((VTD1_Document__c) context.newList[0]).VTD2_PI_Signature_required__c);
        System.assertEquals(true, ((VTD1_Document__c) context.newList[1]).VTD2_PI_Signature_required__c);
        System.assertEquals(true, ((VTD1_Document__c) context.newList[2]).VTD2_PI_Signature_required__c);
        System.assertEquals(true, ((VTD1_Document__c) context.newList[3]).VTD2_PI_Signature_required__c);
        System.assertEquals(false, ((VTD1_Document__c) context.newList[4]).VTD2_PI_Signature_required__c);
    }

    @IsTest
    public static void successFieldPopulatingIntegrationTest() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.stopTest();

        SObject regBinderWithSignReq = new DomainObjects.VTD1_Regulatory_Binder_t()
                .addStudy(study)
                .addRegDocument(new DomainObjects.VTD1_Regulatory_Document_t()
                        .setDocumentType(VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_DOA_LOG)
                        .setSignatureType('Wet')
                        .setWaySendDocuSign('Manual')
                        .setSignatureRequired(true))
                .persist();

        SObject regBinderWithoutSignReq = new DomainObjects.VTD1_Regulatory_Binder_t()
                .addStudy(study)
                .addRegDocument(new DomainObjects.VTD1_Regulatory_Document_t()
                        .setDocumentType(VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_DOA_LOG)
                        .setWaySendDocuSign('Manual')
                        .setSignatureRequired(false))
                .persist();

        List<VTD1_Document__c> documents = new List<VTD1_Document__c>{
                new VTD1_Document__c(VTD1_Regulatory_Binder__c = regBinderWithSignReq.Id),
                new VTD1_Document__c(VTD1_Regulatory_Binder__c = regBinderWithSignReq.Id),
                new VTD1_Document__c(VTD1_Regulatory_Binder__c = regBinderWithSignReq.Id),
                new VTD1_Document__c(VTD1_Regulatory_Binder__c = regBinderWithoutSignReq.Id),
                new VTD1_Document__c(VTD1_Regulatory_Binder__c = regBinderWithoutSignReq.Id)
        };

        insert documents;

        Integer documentsWithSignatureRequired = new QueryBuilder(VTD1_Document__c.class)
                .addConditions()
                .add(new QueryBuilder.SimpleCondition('VTD2_PI_Signature_required__c = true'))
                .endConditions()
                .toCount();
        System.assertEquals(3, documentsWithSignatureRequired);
    }

    private static TestData generateTestData() {
        td = new TestData();
        td.regDocumentIds = generateRegDocumentIds(2);
        td.regDocuments = generateRegDocuments();
        td.regBinderIds = generateRegBinderIds(5);
        td.regBinders = populateFormulas(generateRegBinders());
        td.documents = generateDocuments();
        return td;
    }

    private static List<Id> generateRegDocumentIds(Integer amount) {
        List<Id> ids = new List<Id>();
        for (Integer i = 0; i < amount; i++) {
            ids.add(generateRegDocumentId());
        }
        return ids;
    }

    private static Id generateRegDocumentId() {
        return fflib_IDGenerator.generate(VTD1_Regulatory_Document__c.getSObjectType());
    }

    private static List<VTD1_Regulatory_Document__c> generateRegDocuments() {
        return new List<VTD1_Regulatory_Document__c>{
                new VTD1_Regulatory_Document__c(Id = td.regDocumentIds[0], VTD1_Signature_Required__c = true),
                new VTD1_Regulatory_Document__c(Id = td.regDocumentIds[1], VTD1_Signature_Required__c = false)
        };
    }

    private static List<Id> generateRegBinderIds(Integer amount) {
        List<Id> ids = new List<Id>();
        for (Integer i = 0; i < amount; i++) {
            ids.add(generateRegBinderId());
        }
        return ids;
    }

    private static Id generateRegBinderId() {
        return fflib_IDGenerator.generate(VTD1_Regulatory_Binder__c.getSObjectType());
    }

    private static List<VTD1_Regulatory_Binder__c> generateRegBinders() {
        List<VTD1_Regulatory_Binder__c> regBinders = new List<VTD1_Regulatory_Binder__c>();
        for (Integer i = 0; i < td.regBinderIds.size() - 1; i++) {
            regBinders.add(new VTD1_Regulatory_Binder__c(
                    Id = td.regBinderIds[i],
                    VTD1_Regulatory_Document__c = td.regDocuments[0].Id,
                    VTD1_Regulatory_Document__r = td.regDocuments[0]));
        }
        regBinders.add(new VTD1_Regulatory_Binder__c(
                Id = td.regBinderIds[td.regBinderIds.size() - 1],
                VTD1_Regulatory_Document__c = td.regDocuments[1].Id,
                VTD1_Regulatory_Document__r = td.regDocuments[1]));

        return regBinders;
    }

    private static List<VTD1_Regulatory_Binder__c> populateFormulas(List<VTD1_Regulatory_Binder__c> regBinders) {
        List<VTD1_Regulatory_Binder__c> regBindersWithFormulaPopulated = new List<VTD1_Regulatory_Binder__c>();
        for (VTD1_Regulatory_Binder__c regBinder : regBinders) {
            Map<String, Object> untypedRegBinder =
                    (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(regBinder));

            untypedRegBinder.put('VTD1_Signature_Required__c',
                    regBinder.VTD1_Regulatory_Document__r.VTD1_Signature_Required__c);

            regBindersWithFormulaPopulated.add(
                    (VTD1_Regulatory_Binder__c) JSON.deserialize(
                            JSON.serialize(untypedRegBinder), VTD1_Regulatory_Binder__c.class)
            );
        }
        return regBindersWithFormulaPopulated;
    }

    private static List<VTD1_Document__c> generateDocuments() {
        List<VTD1_Document__c> documents = new List<VTD1_Document__c>();
        for (VTD1_Regulatory_Binder__c regBinder : td.regBinders) {
            documents.add(new VTD1_Document__c(
                    VTD1_Regulatory_Binder__c = regBinder.Id,
                    VTD1_Regulatory_Binder__r = regBinder));
        }
        return documents;
    }

    private static void stubQueries() {
        new QueryBuilder()
                .buildStub()
                .addStubToMap(new Map<Id, VTD1_Regulatory_Binder__c>(td.regBinders))
                .namedQueryStub('VT_R2_PISignatureRequiredFieldSetter_RegBinders')
                .applyStub();
    }

    private static Handler.TriggerContext prepareTriggerContext() {
        Handler.TriggerContext context = new Handler.TriggerContext();
        context.newList.addAll(td.documents);
        return context;
    }


    private class TestData {
        List<Id> regDocumentIds;
        List<VTD1_Regulatory_Document__c> regDocuments;
        List<Id> regBinderIds;
        List<VTD1_Regulatory_Binder__c> regBinders;
        List<VTD1_Document__c> documents;
    }
}