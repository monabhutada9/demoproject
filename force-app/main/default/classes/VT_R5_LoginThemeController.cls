public without sharing class VT_R5_LoginThemeController {

    public static List<String> getPickListValuesForIMB(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Contact.VTR2_Primary_Language__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getValue());
        }
        return pickListValuesList;
    }

    @AuraEnabled(Cacheable = true)
    public static String changeLang(String browserLang) {
        Map<String, String> builtMap = new Map<String, String>();
        List<String> allLang = getPickListValuesForIMB();
        for (String langKey : allLang){
            String anotherLangKey = langKey;
            if (!langKey.contains('_')) {
                anotherLangKey += '_' + langKey.toUpperCase();
            }
            if (langKey.contains('_')) {
                String[] langParts = langKey.split('_');
                if (langParts[0].toLowerCase() == langParts[1].toLowerCase()) {
                    anotherLangKey = langParts[0];
                }
            }
            builtMap.put(langKey,langKey);
            builtMap.put(anotherLangKey, langKey);
        }
        String targetLanguage = builtMap.get(browserLang);
        return targetLanguage != null
                ? targetLanguage
                : 'en_US';
    }
}