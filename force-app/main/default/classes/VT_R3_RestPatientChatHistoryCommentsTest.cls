@isTest
public with sharing class VT_R3_RestPatientChatHistoryCommentsTest {
	@TestSetup
	static void setup() {

		Test.startTest();
		HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
		VT_D1_TestUtils.createTestPatientCandidate(1);
		Test.stopTest();

		User user = [SELECT Id, ContactId, Name FROM User WHERE Profile.Name = 'Patient' AND IsActive = true AND Name = 'CP1 CP1L' LIMIT 1];

		LiveChatVisitor visitor = new LiveChatVisitor();
		insert visitor;

		Case cs = VT_D1_TestUtils.createCase('VTD1_PCF', user.ContactId, null, null, null, null, null);
		insert cs;

		List<LiveChatTranscript> liveChatTranscriptList = new List<LiveChatTranscript>();
		LiveChatTranscript liveChatTranscript = new LiveChatTranscript();
		liveChatTranscript.LiveChatVisitorId = visitor.Id;
		liveChatTranscript.CaseId = cs.Id;
		liveChatTranscript.ContactId = user.ContactId;
		liveChatTranscript.Body = '<p align="center">Chat Started: Wednesday, June 06, 2018, 10:56:58 (+0000)</p>'
				+ '<p align="center">Chat Origin: Chat with Patient</p>'
				+ '<p align="center">Agent Study Concierge</p>( 20s ) Nic: 11<br>( 26s ) Study Concierge: 22<br>( 1h 17m 45s ) '
				+ user.Name.split('(\\s|&nbsp;)') + ': qwerty<br>';
		liveChatTranscript.Status = 'completed';
		liveChatTranscript.EndTime = System.now();

		liveChatTranscriptList.add(liveChatTranscript);

		LiveChatTranscript liveChatTranscript2 = new LiveChatTranscript();
		liveChatTranscript2.LiveChatVisitorId = visitor.Id;
		liveChatTranscript2.CaseId = cs.Id;
		liveChatTranscript2.ContactId = user.ContactId;
		liveChatTranscript2.Status = 'InProgress';
		liveChatTranscript2.EndTime = System.now();

		liveChatTranscriptList.add(liveChatTranscript2);
		insert liveChatTranscriptList;
		//Create file
		Blob beforeblob = Blob.valueOf('Unit Test Attachment Body');

		ContentVersion cv = new ContentVersion();
		cv.Title = 'test content trigger';
		cv.PathOnClient ='test';
		cv.VersionData = beforeblob;
		insert cv;

		ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];

		ContentDocumentLink contentlink = new ContentDocumentLink();
		contentlink.LinkedEntityId = cs.Id;
		contentlink.ShareType = 'I';
		contentlink.ContentDocumentId = testContent.ContentDocumentId;
		contentlink.Visibility = 'AllUsers';
		insert contentlink;

	}

	@IsTest
	public static void getCommentTestNegative() {
		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();
		request.requestURI = '/Patient/ChatHistories/0050v000002IUM6AAO'; // incorrect ID
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response = response;

		Test.startTest();
		VT_R3_RestPatientChatHistoryComments.getComments();
		System.assertEquals(400, response.statusCode);
		Test.stopTest();

	}


	@IsTest
	public static void getCommentTestPositive1() {
		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response = response;

		Test.startTest();
		List<LiveChatTranscript> liveChatTranscriptList = [SELECT Id FROM LiveChatTranscript ORDER BY CreatedDate DESC];
		RestContext.request.requestURI = '/Patient/ChatHistories/' + liveChatTranscriptList[0].Id;

		User user = [SELECT Id, ContactId, Name FROM User WHERE Profile.Name = 'Patient' AND IsActive = true AND Name = 'CP1 CP1L' LIMIT 1];

		System.runAs(user){
			VT_R3_RestPatientChatHistoryComments.getComments();
		}
		Test.stopTest();

	}


	@IsTest
	public static void getCommentTestPositive2() {
		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response = response;

		Test.startTest();

		List<LiveChatTranscript> liveChatTranscriptList = [SELECT Id FROM LiveChatTranscript ORDER BY CreatedDate DESC];
		RestContext.request.requestURI = '/Patient/ChatHistories/' + liveChatTranscriptList[1].Id;

		User user = [SELECT Id, ContactId, Name FROM User WHERE Profile.Name = 'Patient' AND IsActive = true AND Name = 'CP1 CP1L' LIMIT 1];

		System.runAs(user){
			VT_R3_RestPatientChatHistoryComments.getComments();
		}

		Test.stopTest();

	}
}