public without sharing class VT_R5_AutoComplete {
    @AuraEnabled(cacheable=true)
    public static Map < String, Map < String, String >> fetchFieldDescribe(string fieldApiName, String objectName,String virtualSiteId){
        
        Map < String, String > lableHelptextMap = new Map < String, String >();
        Map < String, String > fieldValueMap = new Map < String, String >();
        Map < String, Map < String, String >> finalMap = new Map < String, Map < String, String >>();
        
        Schema.SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
        Map<String, SObjectField> fieldMap = objectType.getDescribe().fields.getMap();
        Schema.SobjectField theField = fieldMap.get(fieldApiName);
        Schema.DescribeFieldResult fieldResult = fieldMap.get(fieldApiName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<Virtual_Site__c> virtualSiteList = new List<Virtual_Site__c>();
        List<String> approvedLanguages = new List<String>();
       
        if(String.isNotEmpty(virtualSiteId)){
            virtualSiteList = [SELECT Id,VTR5_DefaultIRBLanguage__c,VTR5_IRBApprovedLanguages__c FROM Virtual_Site__c WHERE Id =: virtualSiteId LIMIT 1];
            if(virtualSiteList.size()>0 && String.isNotEmpty(virtualSiteList[0].VTR5_IRBApprovedLanguages__c)){
                system.debug('site.VTR5_IRBApprovedLanguages__c ' + virtualSiteList[0].VTR5_IRBApprovedLanguages__c);
                approvedLanguages = virtualSiteList[0].VTR5_IRBApprovedLanguages__c.split(';');
            }
        }
        for( Schema.PicklistEntry pickListVal : ple){
            if(pickListVal.isActive()){
                if(virtualSiteList.size()>0 && approvedLanguages.size()>0 ){
                    if(approvedLanguages.contains(pickListVal.getValue())){
                        fieldValueMap.put(pickListVal.getValue(),pickListVal.getLabel());
                    }          
                }
                else{
                    fieldValueMap.put(pickListVal.getValue(),pickListVal.getLabel());   
                }  
            }   
        }   
        system.debug('pickListValuesList ' + fieldValueMap);
       
        lableHelptextMap.put('Helptext',fieldResult.getInlineHelpText());
        lableHelptextMap.put('Label',fieldResult.getLabel());
        finalMap.put('fieldValueMap',fieldValueMap);
        finalMap.put('lableHelptextMap',lableHelptextMap);
       
        system.debug('Field_Name ' + finalMap); 
        return finalMap;
    }

}