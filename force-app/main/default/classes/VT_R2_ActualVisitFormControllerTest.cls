/**
 * Created by user on 16-May-19.
 */
@isTest
public with sharing class VT_R2_ActualVisitFormControllerTest {

	@TestSetup
	static void setupMethod(){
		HealthCloudGA__CarePlanTemplate__c testStudy = VT_D1_TestUtils.prepareStudy(1);
		Test.startTest();
		VT_D1_TestUtils.createTestPatientCandidate(1);
		//System.enqueueJob(new ProtocolVisitCreator());
		Test.stopTest();
		Case cs = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];

		Id caseId = cs.Id;
		cs.VTD1_Enrollment_Date__c = Datetime.now().addDays(-5).date();
		update cs;
		VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
		protocolVisit.VTD1_Range__c = 4;
		protocolVisit.VTD1_VisitOffset__c = 2;
		protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
		protocolVisit.VTD1_VisitDuration__c = '30 minutes';
		protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
		protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
		insert protocolVisit;
		VTD1_ProtocolVisit__c pv = [SELECT VTD1_Range__c FROM VTD1_ProtocolVisit__c where id =:protocolVisit.Id];

		VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
		actualVisit.VTD1_Protocol_Visit__c = protocolVisit.Id;
		actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
		actualVisit.VTD1_Case__c = caseId;
		insert actualVisit;
	}

	@isTest
	public static void test (){
		Test.startTest();
		Case cs = [SELECT Id, Preferred_Lab_Visit__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
		VTD1_Actual_Visit__c actualVisit = [SELECT Id FROM VTD1_Actual_Visit__c LIMIT 1];
		String actualVisitString =  VT_R2_ActualVisitFormController.getActualVisit(actualVisit.Id);
		System.assert(String.isNotEmpty(actualVisitString));
		String recordType =  VT_R2_ActualVisitFormController.getRecordTypes();
		System.assert(String.isNotEmpty(recordType));
		cs.Preferred_Lab_Visit__c = 'Home Health Nurse';
		update cs;
		String Preferred_Lab_Visit = VT_R2_ActualVisitFormController.getPreferredSubType(cs.Id);
		System.assertEquals('Home Health Nurse', Preferred_Lab_Visit);
		String PatientPhone = VT_R2_ActualVisitFormController.getPatientPhone(cs.Id);

		System.assertNotEquals(null, PatientPhone);

		List<VT_R2_ActualVisitFormController.InputSelectOption> options = new List<VT_R2_ActualVisitFormController.InputSelectOption>();
		options = VT_R2_ActualVisitFormController.getInputSelectOptions('HealthCloudGA__CandidatePatient__c', 'VTD2_Language__c');
		System.assertNotEquals(0, options.size());
		System.assert(String.isNotEmpty(VT_R2_ActualVisitFormController.getCurrentUserProfile()));
		List<Visit_Member__c> visitMemberList = [SELECT Id FROM Visit_Member__c];
		String visitMemberListString = JSON.serialize(visitMemberList);
		VT_R2_ActualVisitFormController.getDatesTimesMap(visitMemberListString);
		String eventDateTime = '{"Year":2020,"Month":5,"Date":14,"Hour":11,"Minute":18,"Second":54}';
		VT_R2_ActualVisitFormController.createOrUpdateEventRemote(eventDateTime, true, actualVisit.Id,40, 'To Be Rescheduled', true, true);
		try{
			VT_R2_ActualVisitFormController.getPreferredSubType('1');
		}
		catch(Exception e){
			System.debug('#@#@###M### ' + e);
		}

		Test.stopTest();
	}

	@IsTest
	private static void textExceptions() {
		VTD1_Actual_Visit__c actualVisit = [SELECT Id FROM VTD1_Actual_Visit__c LIMIT 1];
		List<Visit_Member__c> visitMemberList = [SELECT Id FROM Visit_Member__c];

		System.Test.startTest();
		VT_R2_ActualVisitFormController.getTimesMap(actualVisit.Id, JSON.serialize(visitMemberList), null);
		System.Test.stopTest();
	}
}