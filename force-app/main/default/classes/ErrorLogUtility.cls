/************************************************************************
 * Name  : ErrorLogUtility
 * Author : Mahesh Shimpi
 * Desc  : This is the Utility class used for creating instance of Error Logs & save them.
 * 
 *
 * Modification Log:
 * ----------------------------------------------------------------------
 * Developer                Date                description
 * ----------------------------------------------------------------------
 * Mahesh Shimpi             11/06/2020           Original 
 * 
 *************************************************************************/
public without sharing class ErrorLogUtility{



    public enum ApplicationArea { APP_AREA_OUTLIER_READING, APP_AREA_PATIENT_CONVERSION, APP_AREA_CHAT_AUTOMATION, APP_AREA_EMAIL_SENDER, APP_AREA_DELETE_COOKIES, APP_AREA_ASYNC_DML }
    private static final Map<ApplicationArea, String> APP_AREA = new Map<ApplicationArea, String> {
        ApplicationArea.APP_AREA_OUTLIER_READING    => 'Reading Outlier Identification Batch',
        ApplicationArea.APP_AREA_PATIENT_CONVERSION => 'Patient Conversion',
        ApplicationArea.APP_AREA_CHAT_AUTOMATION    => 'Chat Automation',
        ApplicationArea.APP_AREA_EMAIL_SENDER       => 'Email Sender',
        ApplicationArea.APP_AREA_DELETE_COOKIES     => 'Delete Cookies',
        ApplicationArea.APP_AREA_ASYNC_DML          => 'Async DML'
    };

    public static final String DML_ERROR = 'DmlError';
    public String recordId;
    public String errorMessage;
    public Integer lineNumber;
    public String stackTrace;
    public String exceptionType;
    public String applicationArea;
    public String className;
    public String recordDetails;
    /* 
     * constructor : ErrorLogUtility(Exception ex, String applicationArea)
     * param : Exception ex - An Exception instance used to populate ErrorLOg fields.
     *         String applicationArea - Specifies an application area for the log.
     *         String className - Specifies the name of the class in which exception/error occured.
     *         String recordDetails - Text area field to store extra information.
     * description : This constructor is used to initialize the instance variable based on
     *               Exception instance & the application area specified.
     */
    public ErrorLogUtility(Exception ex, ApplicationArea appArea, String className, String recordDetails){ 
        if(ex.getTypeName().equalsIgnoreCase('DmlException')){
            // System.debug('ID : ' + ex.getDmlId(0));
            this.recordId = ex.getDmlId(0);
        }
        this.errorMessage = ex.getMessage();
        this.lineNumber = Integer.valueOf(ex.getLineNumber());
        this.exceptionType = ex.getTypeName();
        this.stackTrace = ex.getStackTraceString();
        this.recordDetails = recordDetails;
        if(appArea != null){
            this.applicationArea = APP_AREA.get(appArea); 
        } 
        if(className != null){
            this.className = className;
        }                  
    }

    /* 
     * constructor : ErrorLogUtility(Exception ex, String applicationArea)
     * param : Exception ex - An Exception instance used to populate ErrorLOg fields.
     *         String applicationArea - Specifies an application area for the log.
     *         String className - Specifies the name of the class in which exception/error occured.
     * description : This constructor is used to initialize the instance variable based on
     *               Exception instance & the application area specified.
     */
    public ErrorLogUtility(Exception ex, ApplicationArea appArea, String className){ 
        if(ex.getTypeName().equalsIgnoreCase('DmlException')){
            // System.debug('ID : ' + ex.getDmlId(0));
            this.recordId = ex.getDmlId(0);
        }
        this.errorMessage = ex.getMessage();
        this.lineNumber = Integer.valueOf(ex.getLineNumber());
        this.exceptionType = ex.getTypeName();
        this.stackTrace = ex.getStackTraceString();
        if(appArea != null){
            this.applicationArea = APP_AREA.get(appArea); 
        } 
        if(className != null){
            this.className = className;
        }                  
    }
    /* 
     * constructor : ErrorLogUtility(Database.Error err, String applicationArea)
     * param : Database.Error err - An instance of Database.Error used to populate ErrorLog fields.
     *         String applicationArea - Specifies an application area for the log.
     *         String className - Specifies the name of the class in which exception/error occured.
     * description : This constructor is used to initialize the instance variable based on
     *               Database.Error instance & the application area specified.
     */
    public ErrorLogUtility(Database.Error err, ApplicationArea appArea, String className){
        // this.recordId = err.;
        this.errorMessage = err.getMessage();
        // this.lineNumber = 0;//'what value to populate as status code';
        // this.statusCode = Integer.valueOf(err.getStatusCode());
        this.exceptionType = ErrorLogUtility.DML_ERROR;//'what value to populate as exceptionType';
        // this.stackTrace = String.join(err.getFields(), '; '); //'what value to populate as stackTrace';
        if(appArea != null){
            this.applicationArea = APP_AREA.get(appArea); 
        }     
        if(className != null){
            this.className = className;
        }    
    }
    /* 
     * method : saveErrorLog()
     * param : none
     * description : This method is used to populate & insert the ErrorLOg record
     * return : void
     */
    public void saveErrorLog(){
        Database.insert(new VTR4_Conversion_Log__c(
            VTR5_RecordId__c = this.recordId != null ? this.recordId : '',
            VTR4_Error_Message__c = this.errorMessage,                    
            VTR5_LineNumber__c = this.lineNumber != null ? this.lineNumber : 0,
            VTR5_StackTrace__c = this.stackTrace,
            VTR5_ExceptionType__c = this.exceptionType != null ? this.exceptionType : '',

            VTR5_ApplicationArea__c = applicationArea != null ? this.applicationArea : '',

            VTR5_ClassName__c = this.className,
            VTR5_RecordDetails__c = this.recordDetails !=null ? this.recordDetails : ''
        ));
        System.debug('ErrorLog saved');
    }
    /* 
     * method : logErrorsInBulk(List<Database.Error> errors, ApplicationArea appArea, String className)
     * param : List<Database.Error> errors - List of Database.Error instances
     *         String applicationArea - Specifies an application area for the log.
     *         String className - Specifies the name of the class in which exception/error occured.
     * description : This method is used to populate & insert the ErrorLOg records in bulk.
     * return : void
     */
    /*public static void logErrorsInBulk(List<Database.Error> errors, ApplicationArea appArea, String className){
        if(isErrorUtilityEnabled()){
            List<VTR4_Conversion_Log__c> errorLogs = new List<VTR4_Conversion_Log__c>(); 
            for(Database.Error err : errors){
                errorLogs.add(new VTR4_Conversion_Log__c(
                    VTR4_Error_Message__c = err.getMessage(),                    
                    VTR5_ExceptionType__c = ErrorLogUtility.DML_ERROR,

                    VTR5_ApplicationArea__c = appArea != null ? APP_AREA.get(appArea) : '',

                    VTR5_ClassName__c = className != null ? className : ''
                ));
            }
            Database.insert(errorLogs);
            System.debug('Bulk ErrorLogs saved.');
        }
    }*/
    /* 
     * method : logErrorsInBulk(Database.SaveResult[] srList, List<Sobject> lstRecords,ApplicationArea appArea, String className)
     * param : Database.SaveResult[] srList - SaveResult list.
     * 		   List<Sobject> lstRecords - This is the records which are updated/deleted
     *         String applicationArea - Specifies an application area for the log.
     *         String className - Specifies the name of the class in which exception/error occured.
     * description : This method is used to populate & insert the ErrorLOg records in bulk.
	 * return : void
     */
    public static void logErrorsInBulk(Database.SaveResult[] srList, List<Sobject> dmlRecords,ApplicationArea appArea, String className){
        if(isErrorUtilityEnabled() && !dmlRecords.isEmpty()){
            List<VTR4_Conversion_Log__c> errorLogs = new List<VTR4_Conversion_Log__c>(); 
            Integer recordIdCount = 0;
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    for(Database.Error err : sr.getErrors()) {
                        errorLogs.add(new VTR4_Conversion_Log__c(
                            VTR5_RecordId__c = dmlRecords[recordIdCount].Id !=null ? dmlRecords[recordIdCount].Id : null ,
                            VTR4_Error_Message__c = err.getMessage(),                    
                            VTR5_ExceptionType__c = ErrorLogUtility.DML_ERROR,
                            VTR5_ApplicationArea__c = appArea != null ? APP_AREA.get(appArea) : '',
                            VTR5_ClassName__c = className != null ? className : ''
                        ));
                    }
                }
                recordIdCount++;
            }
            if(errorLogs.size() > 0){
                Database.insert(errorLogs);
            }
        }
    }

    /* Carl Judge - 3 Nov 2020. Added this so that bulk logging can be done from other types of DML results as well as insert (UpsertResult, DeleteResult, UndeleteResult)
     * method : logErrorsInBulk(VT_R5_DatabaseResultWrapper[] srList, List<Sobject> lstRecords,ApplicationArea appArea, String className, Boolean logRecordDetails)
     * param : VT_R5_DatabaseResultWrapper[] srList - Wrapped list of results from a DML operation. User VT_R5_DatabaseResultWrapper.wrapResults() to create
     * 		   List<Sobject> lstRecords - This is the records which are updated/deleted
     *         String applicationArea - Specifies an application area for the log.
     *         String className - Specifies the name of the class in which exception/error occured.
     *         Boolean logRecordDetails - if set to true, then the whole record will be serialized and saved on the error log
     * description : This method is used to populate & insert the ErrorLOg records in bulk.
	 * return : void
     */
    public static void logErrorsInBulk(VT_R5_DatabaseResultWrapper[] srList, List<Sobject> dmlRecords,ApplicationArea appArea, String className, Boolean logRecordDetails){
        if(isErrorUtilityEnabled() && !dmlRecords.isEmpty()){
            List<VTR4_Conversion_Log__c> errorLogs = new List<VTR4_Conversion_Log__c>();
            Integer recordIdCount = 0;
            for (VT_R5_DatabaseResultWrapper sr : srList) {
                if (!sr.isSuccess()) {
                    for(Database.Error err : sr.getErrors()) {
                        SObject record = dmlRecords[recordIdCount];
                        errorLogs.add(new VTR4_Conversion_Log__c(
                            VTR5_RecordId__c = record?.Id,
                            VTR4_Error_Message__c = err.getMessage(),
                            VTR5_ExceptionType__c = ErrorLogUtility.DML_ERROR,
                            VTR5_ApplicationArea__c = appArea != null ? APP_AREA.get(appArea) : '',
                            VTR5_ClassName__c = className != null ? className : '',
                            VTR5_ObjectType__c = record?.getSObjectType().getDescribe().getName(),
                            VTR5_RecordDetails__c = logRecordDetails && record != null ? JSON.serialize(record) : null
                        ));
                    }
                }
                recordIdCount++;
            }
            if(errorLogs.size() > 0){
                Database.insert(errorLogs);
            }
        }
    }
    /* 
     * method : logException(Exception ex, ApplicationArea appArea, String className)
     * param : Exception ex - An Exception instance used to populate ErrorLOg fields.
     *         String applicationArea - Specifies an application area for the log.
     *         String className - Specifies the name of the class in which exception/error occured.
     *         String recordDetails - Text area field to store extra information.
     * description : This method is used to populate & insert the ErrorLOg records in bulk.
     * return : void
     */    
    public static void logException(Exception ex, ApplicationArea appArea, String className, String recordDetails){
        if(isErrorUtilityEnabled()){
            ErrorLogUtility errorLog = new ErrorLogUtility(ex, appArea, className, recordDetails);
            errorLog.saveErrorLog();
        }
    }
    
    /* 
     * method : logException(Exception ex, ApplicationArea appArea, String className)
     * param : Exception ex - An Exception instance used to populate ErrorLOg fields.
     *         String applicationArea - Specifies an application area for the log.
     *         String className - Specifies the name of the class in which exception/error occured.
     * description : This method is used to populate & insert the ErrorLOg records in bulk.
     * return : void
     */    
    public static void logException(Exception ex, ApplicationArea appArea, String className){
        if(isErrorUtilityEnabled()){
            ErrorLogUtility errorLog = new ErrorLogUtility(ex, appArea, className);
            errorLog.saveErrorLog();
        }
    }
    /* 
     * method : logError(Exception ex, ApplicationArea appArea, String className)
     * param : Database.Error err - An instance of Database.Error used to populate ErrorLog fields.
     *         String applicationArea - Specifies an application area for the log.
     *         String className - Specifies the name of the class in which exception/error occured.
     * description : This method is used to populate & insert the ErrorLOg records in bulk.
     * return : void
     */    
    public static void logError(Database.Error err, ApplicationArea appArea, String className){
        if(isErrorUtilityEnabled()){
            ErrorLogUtility errorLog = new ErrorLogUtility(err, appArea, className);
            errorLog.saveErrorLog();
        }        
    }
    /* 
     * method : isErrorUtilityEnabled()
     * param : none
     * description : This method is used to check whether ErrorLogUtility is enabled or disabled
     *               in custom metadata.
     * return : true - If ErrorLogUtility is enabled 
     *          false - If ErrorLogUtility is disabled or if custom metadata doesnot exist.
     */     
    private static Boolean isErrorUtilityEnabled(){
        List<VTD1_General_Settings__mdt> customMetaData = [SELECT VTR5_ErrorLogUtilityControl__c 
                                                            FROM VTD1_General_Settings__mdt LIMIT 1];

        return (!customMetaData.isEmpty()) ? customMetaData.get(0).VTR5_ErrorLogUtilityControl__c : false;

    }
}
// ==================== Usage ===============================
// 1. For Exceptions
// ErrorLogUtility.logException(ex, ErrorLogUtility.ApplicationArea.APP_AREA_OUTLIER_READING, className);
// 2. For Database.Errors
// ErrorLogUtility.logError(err, ErrorLogUtility.ApplicationArea.APP_AREA_OUTLIER_READING, className);
// 3. For Bulk errors update. 
// ErrorLogUtility.logErrorsInBulk(Database.SaveResult[] srList, List<Sobject> dmlRecords,ApplicationArea appArea, String className)
//