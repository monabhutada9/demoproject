/**
 * @description
 *
 * @author : Carl Judge
 *
 * @date : 12-Mar-19
 **/
public without sharing class VT_R2_NotificationCHandler {
    public static Boolean eCoaBulkAlerts = false;
    private static final Id backendLogicUserId = Test.isRunningTest() ? UserInfo.getUserId().substring(0, 15) : VTD1_RTId__c.getInstance().VTR2_Backend_Logic_User_ID__c;
    public static final Set<String> piScrProfiles = new Set<String>{
            VT_D1_ConstantsHelper.PI_PROFILE_NAME,
            VT_D1_ConstantsHelper.SCR_PROFILE_NAME
    };
    private static final Set<String> ptCgProfiles = new Set<String>{
            VT_D1_ConstantsHelper.PATIENT_PROFILE_NAME,
            VT_D1_ConstantsHelper.CAREGIVER_PROFILE_NAME
    };
    private static final Set<String> notificationTypesWithoutEmails = new Set<String>{
            'Messages',
            'Televisit'
    };
    public static final Set<String> allowedCaseStatuses = getPatientStatusesAllowedForNotifications();

    private static Map<Id, User> receiversMap = new Map<Id, User>();
    public static Set<String> excludedToDuplicate;

    public void onBeforeInsert(List <VTD1_NotificationC__c> recs) {
        if (eCoaBulkAlerts) {
            return;
        }
        getStaticReceiversMap(recs);
        excludeNotificationsForInvalidUsers(recs);
        populateCurrentCaseForPatientCG(recs);
    }

    public void onAfterInsert(List <VTD1_NotificationC__c> recs) {
        if (eCoaBulkAlerts) {
            return;
        }
        duplicateForCaregivers(recs);
        sendNotificationsAsEmails(recs);
        sendNotificationsToCGAndPatientsDevices(recs, true);
        VT_R2_NotificationCSMSProcessor.onAfterInsert(recs);
    }

    public void onAfterUpdate(List <VTD1_NotificationC__c> recs, Map <Id, VTD1_NotificationC__c> oldMap) {
        getStaticReceiversMap(recs);
        sendNotificationsToCGAndPatientsDevices(recs, false);
        VT_R2_NotificationCSMSProcessor.onAfterUpdate(recs, oldMap);
    }

    private static Set<String> getPatientStatusesAllowedForNotifications() {
        Set<String> caseStatusesForExclude = new Set<String>{
                'Dropped',
                'Dropped Treatment,Compl F/Up',
                'Completed',
                'Compl Treatment,Dropped F/Up',
                'Closed'
        };
        Set<String> allowedStatuses = new Set<String>();
        for (Schema.PicklistEntry entry : Case.Status.getDescribe().getPicklistValues()) {
            if (!caseStatusesForExclude.contains(entry.value)) {
                allowedStatuses.add(entry.value);
            }
        }
        return allowedStatuses;
    }

    private static void getStaticReceiversMap(List<VTD1_NotificationC__c> notifications) {
        Set<Id> ownerIds = new Set<Id>();

        for (VTD1_NotificationC__c n : notifications) {
            ownerIds.add(n.VTD1_Receivers__c);
        }
        receiversMap.putAll((Map<Id, User>) new VT_Stubber.ResultStub(
                'VT_R2_NotificationCHandler.receiversMap',
                new Map<Id, User>([
                        SELECT IsActive,
                                Profile.Name,
                                ContactId,
                                Contact.VTD2_Send_notifications_as_emails__c,
                                Contact.VTD1_Clinical_Study_Membership__c,
                                Contact.VTD1_Clinical_Study_Membership__r.Status,
                                VTR3_Verified_Mobile_Devices__c, (SELECT VTR5_ConnectionToken__c, VTR5_ServiceType__c, VTR5_User__c FROM MobilePushRegistrations__r ORDER BY CreatedDate DESC LIMIT 50)
                        FROM User
                        WHERE Id IN :ownerIds
                        AND IsActive = TRUE
                        AND (Profile.Name IN :piScrProfiles OR Contact.VTD1_Clinical_Study_Membership__r.Status IN :allowedCaseStatuses)
                ])
        ).getResult());
    }

    private static void excludeNotificationsForInvalidUsers(List<VTD1_NotificationC__c> notifications) {
        for (VTD1_NotificationC__c n : notifications) {
            if (!receiversMap.containsKey(n.VTD1_Receivers__c)) {
                n.VTD1_Receivers__c = backendLogicUserId;
            }
            n.OwnerId = n.VTD1_Receivers__c;
        }
    }

    private static void populateCurrentCaseForPatientCG(List<VTD1_NotificationC__c> notifications) {
        for (VTD1_NotificationC__c n : notifications) {
            if (n.VTR4_CurrentPatientCase__c == null
                    && receiversMap.containsKey(n.VTD1_Receivers__c)
                    && ptCgProfiles.contains(receiversMap.get(n.VTD1_Receivers__c).Profile.Name)) {
                n.VTR4_CurrentPatientCase__c = receiversMap.get(n.VTD1_Receivers__c).Contact.VTD1_Clinical_Study_Membership__c;
            }
        }
    }

    private static void duplicateForCaregivers(List<VTD1_NotificationC__c> recs) {
        List<Id> patientIds = new List<Id>();
        Map<Id, List<VTD1_NotificationC__c>> mapUserNotifications = new Map<Id, List<VTD1_NotificationC__c>>();
        for (VTD1_NotificationC__c n : recs) {
            if (receiversMap.containsKey(n.VTD1_Receivers__c)
                    && receiversMap.get(n.VTD1_Receivers__c).Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
                    && (excludedToDuplicate == null || !excludedToDuplicate.contains(n.VTD1_Receivers__c + '_' + n.Message__c))) {
                if (mapUserNotifications.get(n.VTD1_Receivers__c) == null) {
                    mapUserNotifications.put(n.VTD1_Receivers__c, new List<VTD1_NotificationC__c>());
                }
                mapUserNotifications.get(n.VTD1_Receivers__c).add(n);
                patientIds.add(n.VTD1_Receivers__c);
            }
        }

        if (patientIds.isEmpty()) {
            return;
        }

        Map<Id, Id> patientCaregiverIds = VT_D1_PatientCaregiverBound.findCaregiversOfPatients(patientIds);
        List<VTD1_NotificationC__c> notForCaregivers = new List<VTD1_NotificationC__c>();
        for (Id pId : patientCaregiverIds.keySet()) {
            List<VTD1_NotificationC__c> notList = mapUserNotifications.get(pId);
            if (notList != null) {
                for (VTD1_NotificationC__c n : notList) {
                    if (!(n.VTD2_DoNotDuplicate__c || (n.Title__c != null && n.Title__c.toLowerCase().contains('visit')))) {
                        VTD1_NotificationC__c clonedNotification = n.clone(false, true);
                        clonedNotification.VTD1_Receivers__c = patientCaregiverIds.get(pId);
                        clonedNotification.VTD1_Read__c = false;
                        notForCaregivers.add(clonedNotification);
                    }
                }
            }
        }
        try {
            if (excludedToDuplicate == null) {
                excludedToDuplicate = new Set<String>();
            }
            for (VTD1_NotificationC__c notification : notForCaregivers) {
                excludedToDuplicate.add(notification.VTD1_Receivers__c + '_' + notification.Message__c);
            }
            insert notForCaregivers;
            if (VT_R4_PatientDeviceTriggerHandler.notificationList != null) {
                VT_R4_PatientDeviceTriggerHandler.notificationList.addAll(notForCaregivers);
            }
        } catch (Exception e) {
            System.debug('Error while inserting notifications for caregivers!');
            System.debug(e + e.getStackTraceString());
        }
    }

    private static void sendNotificationsAsEmails(List<VTD1_NotificationC__c> notifications) {
        List<VT_R3_EmailsSender.ParamsHolder> emailsParamsHolder = new List<VT_R3_EmailsSender.ParamsHolder>();
        for (VTD1_NotificationC__c n : notifications) {
            if (!receiversMap.containsKey(n.VTD1_Receivers__c)
                    || !receiversMap.get(n.VTD1_Receivers__c).Contact.VTD2_Send_notifications_as_emails__c
                    || notificationTypesWithoutEmails.contains(n.Type__c)
                    || n.VTD2_DoNotSendEmail__c) {
                continue;
            }

            String templateName;
            if (ptCgProfiles.contains(receiversMap.get(n.VTD1_Receivers__c).Profile.Name)) {
                if (n.VTR3_Notification_Type__c == 'Study Notification') {
                    templateName = 'Study_Notification';
                } else if (n.VTR3_Notification_Type__c == 'eCOA Notification') {
                    templateName = 'VT_R5_eCOA_Notification';
                } else if (n.VTR5_Email_Body__c != 'VTR5_PatientDevice_Retake_Notification') {
                    templateName = 'Notification_as_Email';
                }
            } else if (receiversMap.get(n.VTD1_Receivers__c).Profile.Name == VT_D1_ConstantsHelper.PI_PROFILE_NAME) {
                if (n.VDT2_Unique_Code__c != VTR5_ConnectedDeviceNotifications__c.getInstance().VTR5_PI_Notification_Catalog_Code__c) {
                    templateName = 'Notification_as_Email';
                }
            }

            if (templateName == null) {
                continue;
            }
            VT_R3_EmailsSender.ParamsHolder ph = new VT_R3_EmailsSender.ParamsHolder(n.OwnerId, n.Id, templateName);
            ph.subjectIsStatic = true;
            emailsParamsHolder.add(ph);

        }
        if (!emailsParamsHolder.isEmpty()) {
            VT_R3_EmailsSender.sendEmails(emailsParamsHolder);
        }
    }

    private static Boolean isUserHaveVerifiedIOSDevice(User u) {
        return !(u == null || u.VTR3_Verified_Mobile_Devices__c == null || !(u.VTR3_Verified_Mobile_Devices__c.contains('-')));
    }

    private static void sendNotificationsToCGAndPatientsDevices(List <VTD1_NotificationC__c> triggerNew, Boolean isInsert) {
        System.debug('sendNotificationsToCGAndPatientsDevices');
        List<VTD1_NotificationC__c> notificationsEvents = new List<VTD1_NotificationC__c>();
        Map<Id, VTD1_NotificationC__c> mapNotificationsToSend = new Map<Id, VTD1_NotificationC__c>();
        Map<Id, User> notificationWithUser = new Map<Id, User>();

        for (VTD1_NotificationC__c n : triggerNew) {
            if (receiversMap.containsKey(n.VTD1_Receivers__c) && (isInsert || (n.Type__c == 'Messages' && !n.VTD1_Read__c))) {
                notificationsEvents.add(n);
                if (receiversMap.containsKey(n.OwnerId) && !receiversMap.get(n.OwnerId).MobilePushRegistrations__r.isEmpty()) {
                    mapNotificationsToSend.put(n.Id, n);
                    notificationWithUser.put(n.Id, receiversMap.get(n.OwnerId));
                }
            }
        }
        if (notificationsEvents.size() != 0) {
            sendToNotificationsService(notificationsEvents);
        }
        if (!notificationWithUser.isEmpty()) {
            VT_R5_MobilePushNotifications mobilePushNotifications = new VT_R5_MobilePushNotifications(notificationWithUser, mapNotificationsToSend);
            try {
                mobilePushNotifications.execute();
            } catch (Exception e) {
                //Log this
                System.debug(e.getMessage() + e.getStackTraceString() + e.getLineNumber());
            }
        }
    }


    @Future(Callout=true)
    private static void sendPushFuture(Set<Id> notiIds) {
        List<VTD1_NotificationC__c> notis = [
                SELECT LastModifiedDate,
                        VTR5_HideNotification__c,
                        HasDirectLink__c,
                        Name, CreatedById,
                        OwnerId,isActualVisitLink__c,
                        VTR5_TitleValue__c,
                        VTD2_DoNotSendEmail__c,
                        VTR4_CurrentPatientCase__c,
                        IsDeleted,
                        VTR4_Recurrent_Sequence_Inactive__c,
                        VTR3_Notification_Type__c,
                        Title__c,
                        SystemModstamp,
                        VTD1_Receivers__c,
                        VTD1_CSM__c,
                        VTD2_VisitID__c,
                        VTD2_DoNotDuplicate__c,
                        Type__c,
                        Message__c,
                        CreatedDate,
                        VTD1_Read__c,
                        VTR2_Study__c,
                        Id,
                        VDT2_Unique_Code__c,
                        LastModifiedById,
                        Link_to_related_event_or_object__c,
                        VTD1_Parameters__c,
                        VTD2_Title_Parameters__c,
                        Number_of_unread_messages__c
                FROM VTD1_NotificationC__c
                WHERE Id IN :notiIds
        ];
        VT_D1_TranslateHelper.translate(notis, true);
        VT_D1_NotificationCHandler.sendPushNotificationsToMobileDevices(notis);
    }


    public static void sendToNotificationsService(List<VTD1_NotificationC__c> nots) {
        Map<String, List<List<Id>>> typeToReceivers = new Map<String, List<List<Id>>>();
        for (VTD1_NotificationC__c n : nots) {
            if (!typeToReceivers.containsKey(n.Type__c)) {
                typeToReceivers.put(n.Type__c, new List<List<Id>>());
            }
            if (typeToReceivers.get(n.Type__c).isEmpty() || typeToReceivers.get(n.Type__c)[typeToReceivers.get(n.Type__c).size() - 1].size() >= 300) {
                typeToReceivers.get(n.Type__c).add(new List<Id>());
            }
            typeToReceivers.get(n.Type__c)[typeToReceivers.get(n.Type__c).size() - 1].add(n.OwnerId);
        }
        if (System.isFuture() || System.isBatch() || System.isScheduled()) {
            List<VTR4_CNotification__e> notificationsEvents = new List<VTR4_CNotification__e>();
            for (String notType : typeToReceivers.keySet()) {
                List<Id> receivers = new List<Id>();
                for (List<Id> userIds : typeToReceivers.get(notType)) {
                    receivers.addAll(userIds);
                }
                notificationsEvents.add(new VTR4_CNotification__e(VTR4_Type__c = notType, VTR4_UserId__c = JSON.serialize(receivers)));
            }
            EventBus.publish(notificationsEvents);
        } else {
            List<NotificationData> notificationsData = new List<NotificationData>();
            for (String notType : typeToReceivers.keySet()) {
                for (List<Id> userIds : typeToReceivers.get(notType)) {
                    notificationsData.add(new NotificationData(notType, JSON.serialize(userIds)));
                }
            }
            sendToNotificationsService(new List<NotificationWrapper>{
                    new NotificationWrapper('notification', notificationsData)
            });
        }
    }

    public static void sendToNotificationsService(List<NotificationWrapper> payloads) {
        sendToNotificationsService(JSON.serialize(payloads));
    }

    @Future(Callout = true)
    private static void sendToNotificationsService(String notificationsList) {
        Long timestamp = Datetime.now().getTime();
        String endpointHost = Test.isRunningTest() ? '' : VTR4_HerokuNotificationService__c.getInstance().VTR4_AppUrl__c;
        String token = Test.isRunningTest() ? '' : CustomThemeLayoutController.getNotificationServiceToken(timestamp);
        Map<String, Object> payload = new Map<String, Object>{
                'userId' => UserInfo.getUserId(),
                'ts' => timestamp,
                'data' => (List<NotificationWrapper>) JSON.deserialize(notificationsList, List<NotificationWrapper>.class)
        };
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('token', token);
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(endpointHost.replace('wss', 'https') + '/api/v1/' + UserInfo.getOrganizationId());
        req.setBody(JSON.serialize(payload));
        req.setTimeout(120000);
        try {
            new Http().send(req);
        } catch (Exception e) {
            System.debug(e.getMessage());
        }
    }

    public class NotificationWrapper {
        String event;
        List<NotificationData> payload;

        public NotificationWrapper(String event, List<NotificationData> payload) {
            this.event = event;
            this.payload = payload;
        }
    }

    public class NotificationData {
        String type;
        String users;

        public NotificationData(String type, String users) {
            this.type = type;
            this.users = users;
        }
    }
}