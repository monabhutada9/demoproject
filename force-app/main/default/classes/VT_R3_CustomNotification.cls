/**
 * @author: Dmitry Yartsev
 * date: 08.10.2019
 * @description: Notifications for internal users
 */

public without sharing class VT_R3_CustomNotification {
    //http methods
    private static final String HTTP_GET = 'GET';
    private static final String HTTP_POST = 'POST';

    private static final String API_VERSION = '/services/data/v46.0';
    private static final String CUSTOM_NOTIFICATION_DEV_NAME = 'VTR3_CustomNotification';

    //endpoints
    private static final String CUSTOM_NOTIFICATION_QUERY_ENDPOINT = API_VERSION + '/tooling/query/?q=select+id+from+CustomNotificationType+where+DeveloperName=\'' + CUSTOM_NOTIFICATION_DEV_NAME + '\'';
    private static final String CUSTOM_NOTIFICATION_ACTION_ENDPOINT = API_VERSION + '/actions/standard/customNotificationAction';

    //custom notification type Id
    private static Id customNotifId;

    //custom notification wrapper for InputsWrapper
    public class NotificationWrapper {
        Id customNotifTypeId;
        List<Id> recipientIds;
        String title;
        String body;
        Id targetId;

        public NotificationWrapper (List<Id> recipientIds, String title, String body, Id targetId) {
            this.recipientIds = recipientIds;
            this.title = title;
            this.body = body;
            this.targetId = targetId;
        }
    }

    //custom notification inputs wrapper for request body
    private class InputsWrapper {
        List<NotificationWrapper> inputs;

        private InputsWrapper (List<NotificationWrapper> notifList) {
            this.inputs = notifList;
        }
    }

    /**
     * Send custom notifications to recipients
     *
     * @param recipientIds A list of recipient or recipient type Ids of the notification. Valid recipient or recipient types are:
     *      @see User
     *      @see Account
     *      @see Opportunity
     *      @see Group
     *
     * @param title A notification title
     * @param body A notification body
     * @param targetId A record Id for the target record of the notification
     */
    public static void sendCustomNotifications (List<Id> recipientIds, String title, String body, Id targetId) {
        sendCustomNotifications(new List<NotificationWrapper>{
                new NotificationWrapper(
                        recipientIds,
                        title,
                        body,
                        targetId
                )
        });
    }

    /**
     * Send custom notifications to recipients
     *
     * @param notifList A custom notification wrapper
     *
     * @see NotificationWrapper
     */
    public static void sendCustomNotifications (List<NotificationWrapper> notifList) {
        if (notifList != null && !notifList.isEmpty()) {
            if (System.isBatch() && Trigger.isExecuting) {
                return; //for now this case occurs only while transfer process where document notifications are not needed
            } else if (System.isFuture() || System.isBatch() || System.isQueueable()) {
                sendNotifications(notifList);
            } else {
                sendNotificationsFuture(JSON.serialize(notifList));
            }
        }
    }

    /**
     * Send custom notifications to recipients (future method)
     *
     * @param notifListJSON A string of deserialized list of notification wrappers
     *
     * @see NotificationWrapper
     */
    @Future(Callout=true)
    private static void sendNotificationsFuture (String notifListJSON) {
        List<NotificationWrapper> notifList = (List<NotificationWrapper>)JSON.deserialize(notifListJSON, List<NotificationWrapper>.class);
        sendNotifications(notifList);

    }

    /**
     * Send custom notifications to recipients
     *
     * @param notifList A list of notification wrappers
     *
     * @see NotificationWrapper
     */
    private static void sendNotifications (List<NotificationWrapper> notifList) {
        if (customNotifId == null) {
            customNotifId = getCustomNotifId();
        }
        for (NotificationWrapper notif : notifList) {
            notif.customNotifTypeId = customNotifId;
        }
        String notifListBody = JSON.serialize(new InputsWrapper(notifList));
        doRequest(HTTP_POST, CUSTOM_NOTIFICATION_ACTION_ENDPOINT, notifListBody);
    }

    /**
     * Get custom notification type Id
     *
     * @return Id
     */
    private static Id getCustomNotifId () {
        HttpResponse response = doRequest(HTTP_GET, CUSTOM_NOTIFICATION_QUERY_ENDPOINT, null);
        if (response.getStatusCode() != 200) {
            return null;
        }
        List<Object> records = (List<Object>)((Map<String, Object>)JSON.deserializeUntyped(response.getBody())).get('records');
        if (records.size() > 0) {
            return (Id)((Map<String, Object>)records[0]).get('Id');
        } else {
            return null;
        }
    }

    /**
     * Send http request
     *
     * @param method A string of http methods (GET, POST, etc.)
     * @param endpoint A string of request endpoint
     * @param body A string of request body
     *
     * @return HttpResponse
     *
     * @see httpMethods
     * @see HttpResponse
     */
    private static HttpResponse doRequest (String method, String endpoint, String body) {
        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setMethod(method);
        httpRequest.setHeader('Authorization', 'Bearer ' + VT_D1_HelperClass.getSessionIdForGuest());
        httpRequest.setEndpoint(VTD1_FileUploadUserCredentials__c.getInstance().org_url__c + endpoint);
        if (body != null) {
            httpRequest.setHeader('Content-Type', 'application/json');
            httpRequest.setHeader('Accept', 'application/json');
            httpRequest.setBody(body);
        }
        httpRequest.setTimeout(120000);
        Http http = new Http();
        HttpResponse httpResponse = http.send(httpRequest);
        return httpResponse;
    }

}