/**
 * @author:         Rishat Shamratov
 * @date:           14.08.2020
 * @description:    Test class for VTR5_ScheduleVisitsTaskAndNotification
 */

@IsTest
private class VT_R5_ScheduleVisitsTaskAndNotificaTest {
    @TestSetup
    private static void testSetup() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
        HealthCloudGA__CarePlanTemplate__c study = [
                SELECT Id,
                        VTR3_LTFU__c
                FROM HealthCloudGA__CarePlanTemplate__c
                LIMIT 1
        ];
        study.VTR3_LTFU_Days__c = 90;
        study.VTR3_LTFU__c = true;
        update study;

        User primaryPGUser = [
                SELECT Id
                FROM User
                WHERE VTD1_Profile_Name__c = :VT_R4_ConstantsHelper_Profiles.PG_PROFILE_NAME
                LIMIT 1
        ];

        User scrUser = [
                SELECT Id
                FROM User
                WHERE VTD1_Profile_Name__c = :VT_R4_ConstantsHelper_Profiles.PG_PROFILE_NAME
                LIMIT 1
        ];

        Case carePlan = [
                SELECT Id,
                        VTD1_Primary_PG__c
                FROM Case
                WHERE RecordType.DeveloperName = :VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN
                LIMIT 1
        ];
        carePlan.VTD1_Primary_PG__c = primaryPGUser.Id;
        carePlan.VTR2_SiteCoordinator__c = scrUser.Id;
        update carePlan;

        VTD1_ProtocolVisit__c protocolVisit1 = new VTD1_ProtocolVisit__c(
                VTD1_Onboarding_Type__c = 'Baseline Visit',
                VTD1_Study__c = study.Id,
                VTD1_VisitNumber__c = 'v1',
                VTD1_VisitOffset__c = -1,
                VTD1_VisitType__c = VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_STUDY_TEAM,
                RecordTypeId = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByDeveloperName().get('VTD1_Onboarding').getRecordTypeId(),
                VTR2_Visit_Participants__c = 'Patient; PI'
        );
        insert protocolVisit1;

        VTD1_ProtocolVisit__c protocolVisit2 = new VTD1_ProtocolVisit__c(
                VTD1_Onboarding_Type__c = 'Screening',
                VTD1_Study__c = study.Id,
                VTD1_VisitNumber__c = 'o1',
                VTD1_VisitOffset__c = -1,
                VTD1_VisitType__c = VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_STUDY_TEAM,
                RecordTypeId = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByDeveloperName().get('VTD1_Onboarding').getRecordTypeId(),
                VTR2_Visit_Participants__c = 'Patient; PI'
        );
        insert protocolVisit2;

        VTD1_Actual_Visit__c actualVisit1 = new VTD1_Actual_Visit__c(
                VTD1_Case__c = carePlan.Id,
                VTD1_Protocol_Visit__c = protocolVisit1.Id,
                VTD1_Status__c = VT_R4_ConstantsHelper_Statuses.ACTUAL_VISIT_STATUS_FUTURE_VISIT
        );
        insert actualVisit1;

        VTD1_Actual_Visit__c actualVisit2 = new VTD1_Actual_Visit__c(
                VTD1_Case__c = carePlan.Id,
                VTD1_Protocol_Visit__c = protocolVisit2.Id,
                VTD1_Status__c = VT_R4_ConstantsHelper_Statuses.ACTUAL_VISIT_STATUS_CANCELLED
        );
        insert actualVisit2;

        VTD1_Actual_Visit__c actualVisit3 = new VTD1_Actual_Visit__c(
                VTD1_Case__c = carePlan.Id,
                VTD1_Protocol_Visit__c = protocolVisit2.Id,
                VTD1_Status__c = VT_R4_ConstantsHelper_Statuses.ACTUAL_VISIT_STATUS_FUTURE_VISIT
        );
        insert actualVisit3;
    }

    @IsTest
    private static void doVisitSchedulingAndTasks1Test() {
        Case carePlan = [
                SELECT Id,
                        VTD1_Primary_PG__c,
                        VTR2_SiteCoordinator__c
                FROM Case
                WHERE RecordType.DeveloperName = :VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN
                LIMIT 1
        ];
        VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder params = new VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder();
        params.visitCaseId = carePlan.Id;
        params.patientGuideId = carePlan.VTD1_Primary_PG__c;
        params.scrId = carePlan.VTR2_SiteCoordinator__c;
        List<VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder> paramsList = new List<VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder>();
        paramsList.add(params);
        VTR5_ScheduleVisitsTaskAndNotification.doVisitSchedulingAndTasks(paramsList);
        VTR5_ScheduleVisitsTaskAndNotification.doVisitSchedulingAndTasksFuture(JSON.serialize(paramsList));
    }

    @IsTest
    private static void doVisitSchedulingAndTasks2Test() {
        Case carePlan = [
                SELECT Id,
                        VTD1_Primary_PG__c,
                        VTR2_SiteCoordinator__c
                FROM Case
                WHERE RecordType.DeveloperName = :VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN
                LIMIT 1
        ];
        VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder params = new VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder();
        params.caseIdSTM = carePlan.Id;
        params.patientGuideId = carePlan.VTD1_Primary_PG__c;
        params.scrId = carePlan.VTR2_SiteCoordinator__c;
        List<VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder> paramsList = new List<VTR5_ScheduleVisitsTaskAndNotification.ParamsHolder>();
        paramsList.add(params);
        VTR5_ScheduleVisitsTaskAndNotification.isLillyStudy = false;
        VTR5_ScheduleVisitsTaskAndNotification.doVisitSchedulingAndTasksFuture(JSON.serialize(paramsList));
    }
}