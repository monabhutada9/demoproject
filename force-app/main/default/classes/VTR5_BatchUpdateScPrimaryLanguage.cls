/**
* @author Divya Mirani 
* @description Batch Class to update Primary Chat Language for existing SC users
* @created 4/24/2020
*/
global class VTR5_BatchUpdateScPrimaryLanguage implements Database.Batchable<sObject>{
   global Database.QueryLocator start(Database.BatchableContext BC){
      String query = 'select id , VT_R5_Primary_Preferred_Language__c , Profile.Name from User where VT_R5_Primary_Preferred_Language__c = null' +
          ' and Profile.Name = \'Study Concierge\' and isActive = true';
      return Database.getQueryLocator(query);
   }
    global void execute(Database.BatchableContext BC, List<sObject> listOfUser){
        for(User u : (List<User>)listOfUser){
            if(String.isBlank(u.VT_R5_Primary_Preferred_Language__c)){
                u.VT_R5_Primary_Preferred_Language__c= 'en_US';
            }
        }
        VT_D1_UserTriggerHandler.byPassUserTrigger();
        update listOfUser;
    }
   global void finish(Database.BatchableContext BC){
   }
    public static Id runBatch(Integer batchSize){
        return database.executeBatch(new VTR5_BatchUpdateScPrimaryLanguage(),batchSize);
    }
}