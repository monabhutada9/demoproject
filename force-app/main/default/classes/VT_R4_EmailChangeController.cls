/**
 * Created by IhorSemenko on 22.01.2020.
 */

public class VT_R4_EmailChangeController {

    public String ContactId {get; set;}

    public String getStudyPhone() {
        Contact con = [SELECT VTD1_Clinical_Study_Membership__r.VTR2_StudyPhoneNumber__r.Name FROM Contact where id = :ContactId];
        return con.VTD1_Clinical_Study_Membership__r.VTR2_StudyPhoneNumber__r.Name;
    }
}