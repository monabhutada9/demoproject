/**
 * Created by Vlad Tyazhov on 18.11.2020.
 */

public class VT_R5_HotfixEligibilityStatusBatch implements Database.Batchable<SObject> {
    public final List<String> patientNumbers;

    public VT_R5_HotfixEligibilityStatusBatch(List<String> pn) {
        patientNumbers = pn;
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
                SELECT Id, Status, VTD1_Eligibility_Status__c, VTD1_Screening_Complete__c, VTD2_Baseline_Visit_Complete__c
                FROM Case
                WHERE (NOT(Status = 'Active/Randomized' AND VTD1_Eligibility_Status__c = 'Eligible'))
                AND Status != 'Screen Failure'
                AND VTD1_Subject_ID__c IN :patientNumbers
        ]);
    }

    public void execute(Database.BatchableContext bc, List<Case> cases) {
        System.debug('he: cases.size(): ' + cases.size());
        List<Case> casesToUpdate = new List<Case>();

        // SET ELIGIBILITY STATUS TO CONSENTED
        for (Case c : cases) {
            if (c.VTD1_Eligibility_Status__c == null) {
                c.VTD1_Eligibility_Status__c = 'Eligible';
                casesToUpdate.add(c);
            }
        }
        update casesToUpdate;
        casesToUpdate.clear();

        System.debug('he: after set eligibility status. DML: ' + Limits.getDmlRows() + ', CPU: ' + Limits.getCpuTime() + ', SOQL: ' + Limits.getQueries());

        // SET STATUS TO CONSENTED
        for (Case c : cases) {
            if (c.Status == 'Pre-Consent') {
                c.Status = 'Consented';
                casesToUpdate.add(c);
            }
        }
        update casesToUpdate;
        casesToUpdate.clear();
        System.debug('he: after set consented. DML: ' + Limits.getDmlRows() + ', CPU: ' + Limits.getCpuTime() + ', SOQL: ' + Limits.getQueries());

        // SET STATUS TO SCREENED
        for (Case c : cases) {
            if (c.Status == 'Consented') {
                c.VTD1_Screening_Complete__c = true;
                c.Status = 'Screened';
                casesToUpdate.add(c);
            }
        }
        update casesToUpdate;
        casesToUpdate.clear();
        System.debug('he: after set screened. DML: ' + Limits.getDmlRows() + ', CPU: ' + Limits.getCpuTime() + ', SOQL: ' + Limits.getQueries());

        // SET STATUS TO ACTIVE/RANDOMIZED
        for (Case c : cases) {
            if (c.Status == 'Screened') {
                c.VTD2_Baseline_Visit_Complete__c = true;
                c.Status = 'Active/Randomized';
                casesToUpdate.add(c);
            }
        }
        update casesToUpdate;
        casesToUpdate.clear();
        System.debug('he: after set active/randomized. DML: ' + Limits.getDmlRows() + ', CPU: ' + Limits.getCpuTime() + ', SOQL: ' + Limits.getQueries());
    }

    public void finish(Database.BatchableContext bc) {}
}