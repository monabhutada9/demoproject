/**
* @author: Carl Judge
* @date: 06-Sep-18
* @description: Test for VideoPanelTreeController class
**/

@IsTest
public class VideoPanelTreeController_Test {

    public with sharing class CalloutMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/xml');
            res.setBody(JSON.serialize(new List<String>{'SessionID'}));
            res.setStatusCode(200);
            return res;
        }
    }

    @testSetup
    private static void setupMethod() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();

        Case carePlan = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];

        VTD1_ProtocolVisit__c pVisit = new VTD1_ProtocolVisit__c(
            VTD1_Visit_Checklist__c = 'TEST',
            VTR2_Visit_Participants__c = VTD1_ProtocolVisit__c.VTR2_Visit_Participants__c.getDescribe().getPicklistValues()[0].getValue()
        );
        insert pVisit;

        VTD1_Actual_Visit__c visit = new VTD1_Actual_Visit__c(
            VTD1_Case__c = carePlan.Id,
            VTD1_Protocol_Visit__c = pVisit.Id,
            VTD1_Reason_for_Request__c = 'TestVideoPanel',
            VTD1_Additional_Visit_Checklist__c = 'TEST'
        );
        insert visit;

        User priPG = [SELECT Id, ContactId FROM User WHERE FirstName = 'PrimaryPGUser1'];
        User secPG = [SELECT Id, ContactId FROM User WHERE FirstName = 'SecondaryPGUser1'];
        User priPI = [SELECT Id, ContactId FROM User WHERE FirstName = 'PIUser1'];
        User secPI = [SELECT Id, ContactId FROM User WHERE FirstName = 'PIBackupUser1'];
        User patientUser = [SELECT Id, ContactId FROM User WHERE FirstName = 'CP1' AND Profile.Name = 'Patient' ORDER BY CreatedDate DESC LIMIT 1];

        List<User> users = new List<User>{priPG, secPG, priPI, secPI, patientUser};
        List<Visit_Member__c> visitMembers = new List<Visit_Member__c>();
        for (User item : users) {
            visitMembers.add(new Visit_Member__c(
                VTD1_Participant_User__c = item.Id,
                VTD1_Actual_Visit__c = visit.Id
            ));
        }
        insert visitMembers;
    }

    @isTest
    private static void doTest() {
        HttpCalloutMock cMock = new CalloutMock();
        Test.setMock(HttpCalloutMock.class, cMock);

        VTD1_Actual_Visit__c visit = [SELECT Id FROM VTD1_Actual_Visit__c WHERE VTD1_Reason_for_Request__c = 'TestVideoPanel' LIMIT 1];

        Video_Conference__c conf = new Video_Conference__c(VTD1_Actual_Visit__c = visit.Id);

        Test.startTest();

        insert conf;

        List<VTD1_Conference_Member__c> confMembers = new List<VTD1_Conference_Member__c>();
        for (User item : [
            SELECT Id FROM User
            WHERE FirstName IN ('PrimaryPGUser1','SecondaryPGUser1','PIUser1','PIBackupUser1','CP1')
        ]) {
            confMembers.add(new VTD1_Conference_Member__c(
                VTD1_User__c = item.Id,
                VTD1_Video_Conference__c = conf.Id
            ));
        }
        insert confMembers;


        Id confId = [SELECT Id FROM Video_Conference__c].Id;
        VideoPanelTreeController.getParticipants(confId);
        VideoPanelTreeController.getChecklists(confId);

        Test.stopTest();
    }
}