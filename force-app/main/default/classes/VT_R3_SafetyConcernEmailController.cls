/**
 * Created by Dmitry Yartsev on 02.09.2019.
 */

public with sharing class VT_R3_SafetyConcernEmailController {

    public  Id relatedTo {get; set;}
    public  String isMainReminder {get; set;}

    public String communityPIUrl {get {return VTD1_RTId__c.getInstance().PI_URL__c;}set;}

    public VT_R3_SafetyConcernEmailController() {

    }

    public class CaseWrapper{
        public String piLastName {get; set;}
        public String aeOrSae {get; set;}
        public String subjectId {get; set;}
        public String studyName {get; set;}
        public String owner {get; set;}
        public Integer hours {get; set;}
        public String caseNumber {get; set;}
        public String patientCaseId {get; set;}
    }

    public CaseWrapper getCaseInfo(){
        Datetime dt = Datetime.now();
        CaseWrapper casInfo = new CaseWrapper();
        if(this.relatedTo != null){
            List<Case> caseList = [
                    SELECT Owner.Name, isEscalated, OwnerId, Owner.Profile.Name,
                            VTD1_IsEscalatedDateTime__c,
                            VTR3_PCF_Assignment_Date__c,
                            VTD2_PCF_Safety_Concern_Indication_Date__c,
                            VTD1_Clinical_Study_Membership__c,
                            VTD1_Clinical_Study_Membership__r.CaseNumber,
                            VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c,
                            VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTD1_Protocol_Nickname__c,
                            VTD1_Clinical_Study_Membership__r.VTD1_PI_user__r.LastName
                    FROM Case WHERE Id = :relatedTo];
            if (caseList.size() > 0) {
                Case cas = caseList[0];
                Datetime remindersBaseTime;
                if (isMainReminder == 'false') {
                    if (cas.Owner.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) {
                        remindersBaseTime = cas.VTR3_PCF_Assignment_Date__c;
                    } else {
                        if (cas.isEscalated) {
                            remindersBaseTime = cas.VTD1_IsEscalatedDateTime__c;
                        } else {
                            remindersBaseTime = cas.VTD2_PCF_Safety_Concern_Indication_Date__c;
                        }
                    }
                } else {
                    remindersBaseTime = cas.VTD2_PCF_Safety_Concern_Indication_Date__c;
                }
                String subjectID = cas.VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c == null ? '' : cas.VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c;
                casInfo.hours = (Integer)(dt.getTime().intValue() - remindersBaseTime.getTime().intValue())/3600000;
                casInfo.piLastName = cas.VTD1_Clinical_Study_Membership__r.VTD1_PI_user__r.LastName;
                casInfo.aeOrSae = cas.isEscalated ? System.Label.VTR3_SAE : System.Label.VTR3_AE;
                casInfo.subjectId = subjectID;
                casInfo.studyName = cas.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTD1_Protocol_Nickname__c;
                casInfo.owner = cas.Owner.Name;
                casInfo.caseNumber = cas.VTD1_Clinical_Study_Membership__r.CaseNumber;
                casInfo.patientCaseId = cas.VTD1_Clinical_Study_Membership__c;
            }
            return casInfo;
        }
        return null;
    }
}