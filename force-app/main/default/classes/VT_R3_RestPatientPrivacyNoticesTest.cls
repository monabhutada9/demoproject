@isTest
public with sharing class VT_R3_RestPatientPrivacyNoticesTest {
//    @TestSetup in VT_R5_AllTests7

    public static void getPrivacyNoticesTest() {
        Contact contact = new Contact(FirstName = 'fn', LastName = 'ln');
//        User user = [SELECT Id, ContactId, VTD1_StudyId__c FROM User WHERE Profile.Name = 'Patient' AND IsActive = true AND Name = 'CP1 CP1L' LIMIT 1];

        Knowledge__kav knowledge = new Knowledge__kav(Title = 'knowledge Title',
                UrlName = 'UrlName', VTD2_Type__c = 'Privacy_Notice', Summary = 'Some summary', VTD1_Content__c = 'ARTICLE CONTENT');
        insert knowledge;
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/Patient/PrivacyNotices/';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = response;
//		String responsString = VT_R3_RestPatientPrivacyNotices.getPrivacyNotices();
//		Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(responsString);
//		System.assertEquals('Something went wrong.', responseMap.get('serviceResponse'));
//		System.assertEquals(400, response.statusCode);
        knowledge = [SELECT KnowledgeArticleId FROM Knowledge__kav WHERE Id = :knowledge.Id];
        List<User> listUsers = [SELECT Id, ContactId FROM User WHERE UserPermissionsKnowledgeUser = true AND IsActive = true AND Profile.name = 'System Administrator'];
        if (!listUsers.isEmpty()) {
			User user2 = listUsers.get(0);
            user2.ContactId = contact.Id;
            update user2;
            Case cs = VT_D1_TestUtils.createCase('CarePlan', user2.ContactId, null, null, null, null, null);
            insert cs;
            Test.startTest();
            System.runAs(user2) {
                KbManagement.PublishingService.publishArticle(knowledge.KnowledgeArticleId, true);
                System.debug('Knowledge *** ' + [SELECT Id, Summary, Title, VTD1_Content__c, KnowledgeArticleId, IsLatestVersion, PublishStatus, Language, VTD2_Study__c,VTD2_Type__c  FROM Knowledge__kav WHERE Id = :knowledge.Id]) ;
                String responsString = VT_R3_RestPatientPrivacyNotices.getPrivacyNotices();
            }
            Test.stopTest();
        }

    }

}