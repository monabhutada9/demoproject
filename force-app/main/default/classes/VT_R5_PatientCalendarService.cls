/**
 * Created by Yuliya Yakushenkova on 9/23/2020.
 */

public class VT_R5_PatientCalendarService {

    private static VT_R5_PatientCalendarService instance;

    public static VT_R5_PatientCalendarService getInstance() {
        if (instance == null) return new VT_R5_PatientCalendarService();
        return instance;
    }

    public List<CalendarEvent> getVisitsByRange(Datetime startDatetime, Datetime endDatetime) {
        Case casePatient = VT_R5_PatientQueryService.getCaseByCurrentUser();
        if (casePatient == null) return null;

        List<VTD1_Actual_Visit__c> visits =
                VT_R5_PatientQueryService.getVisitsAndMembersWithinRangeAndByCaseId(casePatient.Id, startDatetime, endDatetime);
        VT_D1_TranslateHelper.translate(visits);

        List<CalendarEvent> calendarEvents = new List<CalendarEvent>();
        for (VTD1_Actual_Visit__c av : visits) {
            CalendarEvent singleVisit = new CalendarEvent(av, av.Visit_Members__r);
            calendarEvents.add(singleVisit);
        }
        return calendarEvents;
    }

    public CalendarEvent getVisitByVisitId(String visitId) {
        VTD1_Actual_Visit__c visit = VT_R5_PatientQueryService.getVisitAndMembersByVisitId(visitId);

        if (visit == null) return null;

        VT_D1_TranslateHelper.translate(visit);

        List<VTD1_Document__c> documents = VT_R5_PatientQueryService.getDocumentsByVisitId(visitId);

        return new CalendarEvent(visit, visit.Visit_Members__r, documents);
    }

    public CalendarData getVisitsWithinByRangeAndByEventIds(Set<Id> eventIds, Datetime startDatetime, Datetime endDatetime) {
        Case casePatient = VT_R5_PatientQueryService.getCaseByCurrentUser();
        if (casePatient == null) return null;

        List<VTD1_Actual_Visit__c> visits = VT_R5_PatientQueryService.getVisitsAndMembersByCaseId(casePatient.Id);

        CalendarData calendar = new CalendarData();
        for (VTD1_Actual_Visit__c visit : visits) {
            Datetime visitDatetime = visit.VTD1_Scheduled_Date_Time__c;
            if (visitDatetime >= startDatetime && visitDatetime <= endDatetime) {
                if (eventIds.contains(visit.Id)) eventIds.remove(visit.Id);
                calendar.eventsWithinRange.add(new CalendarEvent(visit, visit.Visit_Members__r));
            } else if (eventIds.contains(visit.Id)) {
                calendar.eventsOutOfRange.add(new CalendarEvent(visit, visit.Visit_Members__r));
                eventIds.remove(visit.Id);
            }
        }
        calendar.eventsToDelete.addAll(eventIds);
        return calendar;
    }

    public class CalendarData {
        public List<CalendarEvent> eventsWithinRange = new List<CalendarEvent>();
        public List<CalendarEvent> eventsOutOfRange = new List<CalendarEvent>();
        public Set<Id> eventsToDelete = new Set<Id>();
    }

    public class CalendarEvent {

        public String id;
        public String name;
        public Long startDateTime;
        public Long endDateTime;
        public String description;
        public String instructions;
        public String type;
        public String status;
        public String visitChecklist;
        public String modality;
        public String modalityLabel;
        public String location;
        public String address;
        public String phone;
        public String eventType;
        public List<Participant> participants;
        public List<VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper> documents;

        private CalendarEvent(VTD1_Actual_Visit__c actualVisit, List<Visit_Member__c> members, List<VTD1_Document__c> documents) {
            this(actualVisit, members);
            this.documents = getDocuments(documents);
        }

        private CalendarEvent(VTD1_Actual_Visit__c actualVisit, List<Visit_Member__c> members) {
            Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());
            this.id = actualVisit.Id;
            this.name = VT_D1_HelperClass.getActualVisitName(actualVisit);
            this.startDateTime = getConvertedDate(actualVisit.VTD1_Scheduled_Date_Time__c, timeOffset);
            this.endDateTime = getConvertedDate(actualVisit.VTD1_Scheduled_Visit_End_Date_Time__c, timeOffset);
            this.description = getVisitDescription(actualVisit);
            this.instructions = getPreVisitInstructions(actualVisit);
            this.status = actualVisit.VTD1_Status__c;
            this.visitChecklist = getPatientVisitChecklist(actualVisit);
            this.modality = actualVisit.VTR2_Modality__c;
            this.modalityLabel = String.valueOf(actualVisit.get('modalityLabel'));
            this.location = actualVisit.VTR2_Loc_Name__c;
            this.address = actualVisit.VTR2_Loc_Address__c;
            this.phone = actualVisit.VTR2_Loc_Phone__c;
            this.eventType = 'visit';
            setParticipants(members);
        }

        private Long getConvertedDate(Datetime value, Integer timeOffset) {
            if (value != null) return value.getTime() + timeOffset;
            return null;
        }

        private void setParticipants(List<Visit_Member__c> members) {
            this.participants = new List<Participant>();
            for (Visit_Member__c visitMember : members) {
                if (String.isNotBlank(visitMember.Visit_Member_Name__c)) {
                    this.participants.add(
                            new Participant(visitMember.Id, visitMember.Visit_Member_Name__c)
                    );
                }
            }
        }

        private String getVisitDescription(VTD1_Actual_Visit__c visit) {
            String type = visit.VTD1_Unscheduled_Visit_Type__c;
            if (visit.Unscheduled_Visits__c == null && visit.VTD1_Protocol_Visit__c != null) {
                return visit.VTD1_Protocol_Visit__r.VTD1_Visit_Description__c;
            }
            String label;
            if (visit.Name.equals(type) && visit.Name == 'End of Study') {
                label = VT_D1_TranslateHelper.getLabelValue('Label.VTR3_ReconsentRefused');
            } else if (visit.Name.equals(type) && visit.Name == 'Screening Result Visit') {
                label = VT_D1_TranslateHelper.getLabelValue('Label.VTR3_ScreeningResultVisit');
            } else if (type == 'Screening Results Visit' && visit.Name == 'Washout/Run-In Results Visit') {
                label = VT_D1_TranslateHelper.getLabelValue('Label.VTR3_WashoutRunInResultsVisit');
            } else {
                label = visit.VTD1_Reason_for_Request__c;
            }
            return label;
        }

        private String getPreVisitInstructions(VTD1_Actual_Visit__c visit) {
            if (visit.VTD1_Protocol_Visit__c != null) {
                return visit.VTD1_Protocol_Visit__r.VTD1_PreVisitInstructions__c;
            } else {
                return visit.VTD1_Pre_Visit_Instructions__c;
            }
        }

        private String getPatientVisitChecklist(VTD1_Actual_Visit__c visit) {
            if (visit.VTD1_Protocol_Visit__c != null) {
                return visit.VTD1_Protocol_Visit__r.VTD1_Patient_Visit_Checklist__c;
            } else {
                return visit.VTD1_Additional_Patient_Visit_Checklist__c;
            }
        }

        public List<VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper> getDocuments(List<VTD1_Document__c> docs) {
            List<VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper> result = new List<VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper>();
            for (VTD1_Document__c d : docs) {
                VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper mdw =
                        new VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper(d);
                result.add(mdw);
            }
            result = VT_R3_RestPatientStudyDocuments.addDownloadLinks(result);
            return result;
        }
    }

    private class Participant {
        public String id;
        public String name;

        public Participant(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }
}