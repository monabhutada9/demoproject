/**
 * Created by Mikhail Platonov on 09-Apr-20.
 *
 * @description This class is used by VT_R4_GeneralVisitsController.
 */
public without sharing class VT_R4_GeneralVisitsHelper {
    private static UserSearchService userSearchService = new UserSearchService();
    private without sharing class UserSearchService {
        private List<User> getUsersForSearchComponent() {
            String profileName = [SELECT Profile.Name FROM User WHERE Id = :UserInfo.getUserId()].Profile.Name;
            if (profileName == 'System Administrator') {
                List<String> profileNames = new List<String>{
                        VT_D1_ConstantsHelper.PATIENT_PROFILE_NAME,
                        VT_D1_ConstantsHelper.CAREGIVER_PROFILE_NAME
                };
                return findAllUsersByExcludedProfileNames(profileNames);
            } else {
                Set<Id> studyIds = getCurrentUserStudyIds();
                List<HealthCloudGA__CarePlanTemplate__c> studies = getStudiesWithTeam(studyIds);
                return getStudyTeamUsers(studies);
            }
        }
        private List<User> findAllUsersByExcludedProfileNames(List<String> profileNames) {
            return [
                    SELECT Name, SmallPhotoUrl, Title, Email, Profile.Name
                    FROM User
                    WHERE Profile.Name NOT IN :profileNames AND IsActive = TRUE
                    LIMIT 50000
            ];
        }
        private Set<Id> getCurrentUserStudyIds() {
            Set<Id> totalStudyIds = new Set<Id>();
            List<AggregateResult> aggregatedMemberStudies = [
                    SELECT Study__c
                    FROM Study_Team_Member__c
                    WHERE User__c = :UserInfo.getUserId()
                    GROUP BY Study__c
            ];
            Set<Id> studyIds = new Map<Id, HealthCloudGA__CarePlanTemplate__c>([
                    SELECT Id
                    FROM HealthCloudGA__CarePlanTemplate__c
                    WHERE VTD1_Project_Lead__c = :UserInfo.getUserId()
                    //OR VTD1_Monitoring_Report_Reviewer__c = :UserInfo.getUserId()
                    //OR VTD1_PMA__c = :UserInfo.getUserId()
                    OR VTD1_Regulatory_Specialist__c = :UserInfo.getUserId()
                    OR VTD1_Remote_CRA__c = :UserInfo.getUserId()
                    OR VTD1_Study_Admin__c = :UserInfo.getUserId()
                    OR VTD1_Virtual_Trial_head_of_Operations__c = :UserInfo.getUserId()
                    OR VTD1_Virtual_Trial_Study_Lead__c = :UserInfo.getUserId()
            ]).keySet();
            totalStudyIds.addAll(studyIds);
            for (AggregateResult ar : aggregatedMemberStudies) {
                totalStudyIds.add((Id) ar.get('Study__c'));
            }
            return totalStudyIds;
        }
        private List<HealthCloudGA__CarePlanTemplate__c> getStudiesWithTeam(Set<Id> studyIds) {
            return [
                    SELECT VTD1_Project_Lead__c,
                            //VTD1_Monitoring_Report_Reviewer__c,
                            //VTD1_PMA__c,
                            VTD1_Regulatory_Specialist__c,
                            VTD1_Remote_CRA__c,
                            VTD1_Study_Admin__c,
                            VTD1_Virtual_Trial_head_of_Operations__c,
                            VTD1_Virtual_Trial_Study_Lead__c,
                            (
                                    SELECT User__c
                                    FROM Study_Team_Members__r
                            )
                    FROM HealthCloudGA__CarePlanTemplate__c
                    WHERE Id IN :studyIds
            ];
        }
        private List<User> getStudyTeamUsers(List<HealthCloudGA__CarePlanTemplate__c> studies) {
            Set<Id> userIds = new Set<Id>();
            for (HealthCloudGA__CarePlanTemplate__c study : studies) {
                //userIds.add(study.VTD1_Monitoring_Report_Reviewer__c);
                //userIds.add(study.VTD1_PMA__c);
                userIds.add(study.VTD1_Project_Lead__c);
                userIds.add(study.VTD1_Regulatory_Specialist__c);
                userIds.add(study.VTD1_Remote_CRA__c);
                userIds.add(study.VTD1_Study_Admin__c);
                userIds.add(study.VTD1_Virtual_Trial_head_of_Operations__c);
                userIds.add(study.VTD1_Virtual_Trial_Study_Lead__c);
                for (Study_Team_Member__c member : study.Study_Team_Members__r) {
                    userIds.add(member.User__c);
                }
            }
            List<User> users = [SELECT Name, SmallPhotoUrl, Title, Email, Profile.Name FROM User WHERE Id IN :userIds];
            return users;
        }
    }
    public static List<String> getAvailableTimeSlots(List <VTR4_General_Visit_Member__c> members, Integer duration, Date selectedDate) {
        Id generalVisitId = getGeneralVisitId(members);
        List<Id> visitMembersIds = getVisitMemberUserIds(members);
        List<Time> timeList = getTimesOfDay(visitMembersIds, duration);
        List<Event> events = getSelectedDateTimeEvents(selectedDate, timeList, visitMembersIds, generalVisitId);
        List<Time> timeSlots = calculateFreeDayTime(selectedDate, events, timeList, duration);
        return getSlotsAsStrings(timeSlots);
    }
    private static Id getGeneralVisitId(List<VTR4_General_Visit_Member__c> members) {
        return members.isEmpty() ? null : members[0].VTR4_General_Visit__c;
    }
    private static List<Id> getVisitMemberUserIds(List<VTR4_General_Visit_Member__c> members) {
        List <Id> visitMembersIds = new List <Id>();
        for (VTR4_General_Visit_Member__c member : members) {
            visitMembersIds.add(member.VTR4_Participant_User__c);
        }
        return visitMembersIds;
    }
    private static List<Event> getSelectedDateTimeEvents(Date selectedDate, List<Time> timeList, List<Id> visitMembersIds, Id generalVisitId) {
        Time firstDayStartTime = timeList[2];
        Time lastDayEndTime = timeList[3];
        Datetime startDateTime = DateTime.newInstance(selectedDate, firstDayStartTime);
        Datetime endDateTime = DateTime.newInstance(selectedDate, lastDayEndTime);
        System.debug('selectedDate = ' + selectedDate);
        System.debug('!!!!! startDate ' + startDateTime.date());
        System.debug('!!!!! startTime ' + startDateTime.time());
        System.debug('!!!!! startDate ' + endDateTime.date());
        System.debug('!!!!! startTime ' + endDateTime.time());
        String processQuery = 'SELECT StartDateTime, EndDateTime, IsAllDayEvent, OwnerId, Owner.ProfileId '
                + 'FROM Event '
                + 'WHERE (OwnerId IN :visitMembersIds '
                + 'AND (VTR4_General_Visit__c != :generalVisitId OR VTR4_General_Visit__c = null) '
                + 'AND ((StartDateTime >= :startDateTime AND StartDateTime < :endDateTime) '
                + 'OR (EndDateTime > :startDateTime AND EndDateTime <= :endDateTime) '
                + 'OR (StartDateTime <= :startDateTime AND EndDateTime >= :endDateTime))) '
                + 'ORDER BY StartDateTime';
        List<Event> events = DataBase.query(processQuery);
        System.debug('events = ' + events.size());
        for (Event e : events) {
            System.debug('event = ' + e);
        }
        return events;
    }
    private static List<Time> calculateFreeDayTime(Date dt, List<Event> events, List<Time> timeList, Integer duration) {
        VT_D1_ActualVisitsHelper.actualVisitInfo = new VTD1_Actual_Visit__c();
        VT_D1_ActualVisitsHelper.visitDuration = duration;
        Map<Date, Time[]> freeDayTimes = VT_D1_ActualVisitsHelper.calculateFreeDaysTimes(
                events,
                new List <Date>{
                        dt
                },
                timeList[0],
                timeList[1],
                timeList[2],
                timeList[3]);
        if (freeDayTimes.values().isEmpty()) {
            return new List<Time>();
        }
        return freeDayTimes.values()[0];
    }
    private static List<String> getSlotsAsStrings(List<Time> timeSlots) {
        System.debug('timeSlots = ' + timeSlots);
        List <String> slots = new List<String>();
        for (Time t : timeSlots) {
            Date helpDate = Date.newInstance(2000, 1, 1);
            Datetime helpDateTime = DateTime.newInstance(helpDate, t);
            slots.add(VT_D1_TranslateHelper.translate(helpDateTime.format('h:mm a')));
        }
        return slots;
    }
    public static VTR4_General_Visit__c getGeneralVisit(Id visitId) {
        VTR4_General_Visit__c visit = [
                SELECT
                        Id,
                        Name,
                        VTR4_Visit_Method__c,
                        VTR4_Scheduled_Date_Time__c,
                        VTR4_Duration__c,
                        VTR4_Completed_Date_Time__c,
                        VTR4_Status__c, (
                        SELECT
                                Id,
                                VTR4_Participant_User__c,
                                VTR4_Participant_User__r.Name,
                                VTR4_Participant_User__r.Profile.Name,
                                VTR4_External_Member_Name__c,
                                VTR4_External_Participant_Type__c,
                                VTR4_External_Participant_Email__c,
                                VTR4_Member_Type__c,
                                VTR4_User_Name__c
                        FROM General_Visit_Members__r
                )
                FROM VTR4_General_Visit__c
                WHERE Id = :visitId
        ];
        for (VTR4_General_Visit_Member__c member : visit.General_Visit_Members__r) {
            System.debug('m = ' + member + ' ' + member.VTR4_Participant_User__r.Name);
        }
        System.debug(LoggingLevel.INFO, 'visit ' + JSON.serializePretty(visit));
        return visit;
    }
    public static String updateGeneralVisit(VT_R4_GeneralVisitsController.GeneralVisitData data) {
        System.debug(LoggingLevel.INFO, 'visit to update' + JSON.serializePretty(data));
        Savepoint savepoint = Database.setSavepoint();
        try {
            VTR4_General_Visit__c visitExists = getVisitWithMembers(data.visit);
            if (visitExists != null) {
                System.debug('visitExists = ' + visitExists);
                Map <Id, VTR4_General_Visit_Member__c> memberExistsMap = new Map<Id, VTR4_General_Visit_Member__c>();
                for (VTR4_General_Visit_Member__c member : visitExists.General_Visit_Members__r) {
                    memberExistsMap.put(member.Id, member);
                }
                System.debug('to delete 1 = ' + memberExistsMap);
                for (VTR4_General_Visit_Member__c member : data.members) {
                    System.debug('member.Id = ' + member.Id);
                    if (member.Id != null) {
                        memberExistsMap.remove(member.Id);
                    }
                }
                System.debug('to delete 2 = ' + memberExistsMap);
                if (!memberExistsMap.isEmpty()) {
                    delete memberExistsMap.values();
                }
                update data.visit;
                List <VTR4_General_Visit_Member__c> membersToAdd = new List<VTR4_General_Visit_Member__c>();
                for (VTR4_General_Visit_Member__c member : data.members) {
                    if (member.Id == null) {
                        System.debug(LoggingLevel.INFO, 'member ' + JSON.serializePretty(member));
                        member.VTR4_General_Visit__c = data.visit.Id;
                        membersToAdd.add(member);
                    }
                }
                System.debug('to insert = ' + membersToAdd);
                if (!membersToAdd.isEmpty()) {
                    insert membersToAdd;
                }
                return data.visit.Id;
            } else {
                insert data.visit;
                List <VTR4_General_Visit_Member__c> membersToAdd = new List<VTR4_General_Visit_Member__c>();
                for (VTR4_General_Visit_Member__c member : data.members) {
                    System.debug(LoggingLevel.INFO, 'member ' + JSON.serializePretty(member));
                        member.VTR4_General_Visit__c = data.visit.Id;
                        membersToAdd.add(member);
                }
                System.debug('to insert = ' + membersToAdd);
                if (!membersToAdd.isEmpty()) {
                    insert membersToAdd;
                }
                return data.visit.Id;
            }
        } catch (Exception exc) {
            Database.rollback(savepoint);
            System.debug('ERROR with db');
            throw exc;
        }
    }
    private static VTR4_General_Visit__c getVisitWithMembers(VTR4_General_Visit__c visit) {
        List<VTR4_General_Visit__c> visitExists = [
                SELECT Id, (SELECT Id, VTR4_Participant_User__c, VTR4_Participant_User__r.Name FROM General_Visit_Members__r)
                FROM VTR4_General_Visit__c
                WHERE Id = :visit.Id
        ];
        return visitExists.isEmpty() ? null : visitExists[0];
    }
    private static List<Time> getTimesOfDay(List <Id> visitMembersIds, Integer visitDuration) {
        System.debug('getTimesOfDay visitDuration' + visitDuration);
        System.debug('visitMembersIds ' + visitMembersIds);
        List<Time> lst = new List<Time>();
        list<User> listUsers = [
                SELECT Name, ProfileId,
                        VTD1_StartDay__c,
                        VTD1_EndOfDay__c,
                        TimeZoneSidKey
                FROM User
                WHERE Id IN :visitMembersIds AND VTD1_StartDay__c != null AND VTD1_EndOfDay__c != null
        ];
        system.debug('!!!! listUsers for finding day times' + listUsers);
        Datetime helpDTDT = Datetime.newInstanceGmt(Date.today(), Time.newInstance(0, 0, 0, 0));
        Decimal maxStartDay = 0;
        Decimal minEndDay = 24 * 60;
        Decimal currentUserOffset = Decimal.valueOf(UserInfo.getTimeZone().getOffset(helpDTDT) / (1000 * 60));
        System.debug('currentUserOffset ' + currentUserOffset);
        for (User u : listUsers) {
            Timezone tz = Timezone.getTimeZone(u.TimeZoneSidKey);
            Decimal tOffset = Decimal.valueOf(tz.getOffset(helpDTDT) / (1000 * 60));
            System.debug('u.VTD1_EndOfDay__c' + u.VTD1_EndOfDay__c);
            System.debug('tOffset' + tOffset + u.Name);
            System.debug('currentUserOffset' + currentUserOffset);
            if (u.VTD1_StartDay__c != null) {
                if ((Decimal.valueOf(u.VTD1_StartDay__c) * 60 - tOffset + currentUserOffset) > maxStartDay) {
                    maxStartDay = Decimal.valueOf(u.VTD1_StartDay__c) * 60 - tOffset + currentUserOffset;
                }
            }
            if (u.VTD1_EndOfDay__c != null) {
                Integer userBuffer = 0;//buffersMap.get(u.ProfileId) == null ? 0 : buffersMap.get(u.ProfileId);
                System.debug('userBuffer u.VTD1_EndOfDay__c  ' + userBuffer + u.VTD1_EndOfDay__c + ' minEndDay ' + minEndDay);
                System.debug('Decimal.valueOf(u.VTD1_EndOfDay__c)*60 - userBuffer - tOffset + currentUserOffset ' + (Decimal.valueOf(u.VTD1_EndOfDay__c) * 60 - userBuffer - tOffset + currentUserOffset));
                if ((Decimal.valueOf(u.VTD1_EndOfDay__c) * 60 - userBuffer - tOffset + currentUserOffset) < minEndDay) {
                    minEndDay = Decimal.valueOf(u.VTD1_EndOfDay__c) * 60 - userBuffer - tOffset + currentUserOffset;
                }
            }
            System.debug('maxStartDay + minEndDay' + maxStartDay + minEndDay);
        }
        Integer startHour = Integer.valueOf(maxStartDay / 60);
        Integer endHour = Integer.valueOf(minEndDay / 60);
        System.debug('maxStartDay + minEndDay' + maxStartDay + minEndDay);
        Time startTime = Time.newInstance(startHour, Integer.valueOf(maxStartDay - startHour * 60), 0, 0);
        Time endTime;
        if (endHour == 24) {
            endTime = Time.newInstance(23, 45, 0, 0);
        } else {
            endTime = Time.newInstance(endHour, Integer.valueOf(minEndDay - endHour * 60), 0, 0);
        }
        System.debug('startTime endTime' + startTime + endTime);
        Time firstDayStartTime = startTime; // when To Be Rescheduled
        Time lastDayEndTime = endTime; // when To Be Rescheduled
        lst.add(startTime);
        lst.add(endTime);//lst.add(endTime.addMinutes(-(visitDuration)));
        lst.add(firstDayStartTime);
        lst.add(lastDayEndTime);
        system.debug('!! startTime, endTime' + lst);
        return lst;
    }
    public static List<User> getUsersForSearchComponent() {
        return userSearchService.getUsersForSearchComponent();
    }
}