/************************************************************************
* Name  : VT_R5_TaskandNotificationController
* Date  : 03/06/2020
* Author : Yogesh More
* Desc  : This class created to send email notifictaion based on NotificationC object
* It invokes from multiple location.
*
*
* Modification Log:
* ----------------------------------------------------------------------
* Developer                Date                description
* ----------------------------------------------------------------------
* ---ABC-----             3/06/2020           Original 
* 
*************************************************************************/
public class VT_R5_TaskandNotificationController { 
    public static VTR5_ConnectedDeviceNotifications__c CtSetng;
    public static void SendNotificationC_Email(List<VTD1_NotificationC__c> lstNotificationC){
        try{
            Set<Id> userId = new Set<Id>();
            List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
            //System.debug('==>'+CtSetng);
            for(VTD1_NotificationC__c notC : lstNotificationC){            
                If((CtSetng== null) || (notC.VDT2_Unique_Code__c != null && CtSetng != null && notC.VDT2_Unique_Code__c == CtSetng.VTR5_PI_Notification_Catalog_Code__c)){
                    userId.add(notc.OwnerId);
                }
            }
            Map<Id, User> userMap = new Map<Id, User>([SELECT Id, Email, Firstname, Lastname, Profile.Name, LanguageLocaleKey FROM User WHERE Id IN : userId]);
            VT_D1_TranslateHelper.translate(lstNotificationC,true);
            List<OrgWideEmailAddress> owEmailAddresses = [SELECT Id FROM OrgWideEmailAddress  ORDER BY CreatedDate LIMIT 1];
            for(VTD1_NotificationC__c notC : lstNotificationC){
                If((CtSetng== null) || (notC.VDT2_Unique_Code__c != null && CtSetng != null && notC.VDT2_Unique_Code__c == CtSetng.VTR5_PI_Notification_Catalog_Code__c)){                 
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    If(!String.isBlank(NotC.VTR5_Email_Subject__c)){
                        mail.setSubject(notC.VTR5_Email_Subject__c);
                    }
                    else{
                        mail.setSubject(VT_D1_TranslateHelper.getLabelValue('VTD2_NewNotification', userMap.get(notC.OwnerId).LanguageLocaleKey));
                    }
                    mail.setToAddresses(new String[] {userMap.get(notC.OwnerId).email});
                    mail.setHtmlBody(CreateHTMLBody(notC,userMap.get(notC.ownerId)));
                    mail.setOrgWideEmailAddressId(owEmailAddresses[0].Id);
                    emailList.add(mail);
                }
            }
            if(!emailList.isEmpty()){
                System.debug('EmailList ==>'+EmailList.size());
                List<Messaging.SendEmailResult> results = Messaging.sendEmail(EmailList, false);
                for (Messaging.SendEmailResult result : results) {
                    System.debug('result = ' + result.isSuccess());
                    for (Messaging.SendEmailError error : result.errors) {
                        System.debug(error.getMessage());
                    }
                }
            }
        }
        catch(Exception ex){
            System.debug('error==>'+ex.getLineNumber());
            System.debug('error==>'+ex.getMessage());
        }
    }
    public static string CreateHTMLBody(VTD1_NotificationC__c NotC, User use){
        VT_R3_EmailNewNotificationController emailCont = new VT_R3_EmailNewNotificationController();
        emailCont.userLanguage = use.LanguageLocaleKey;
        emailCont.profileName = use.Profile.Name;
        emailCont.lastName = use.LastName;        
        emailCont.firstName = use.FirstName;
        String Body;
        String getBody;
        If(!String.isBlank(NotC.VTR5_Email_Body__c)){
            getBody = NotC.VTR5_Email_Body__c;
        }
        else{
            getBody = emailCont.getBodyText();
        }
        Body='<html><body><p>'+emailCont.getSalutation()+'</p>'+
            '<p>'+getBody+'</p>'+
            '<p><a href='+emailCont.getLinkToStudyHub()+'>'+VT_D1_TranslateHelper.getLabelValue('VTD2_ViewNotificationInStudyHub', use.LanguageLocaleKey)+'</a></p>'+
            '<p>'+emailCont.sincerely+'</p>'+
            '<p>'+emailCont.closingMail+'</p></body></html>';
        return Body;
    }
    @future(callout=true) 
    Public static void sendEmailNotification(String createNotificationJson){
        List<VTD1_NotificationC__c> createNotificationList = new List<VTD1_NotificationC__c>(); 
        if(String.isNotBlank(createNotificationJson)){
            createNotificationList = (List<VTD1_NotificationC__c>)
                JSON.deserialize(createNotificationJson, List<VTD1_NotificationC__c>.class); 
            SendNotificationC_Email(createNotificationList);
        }
    }
}