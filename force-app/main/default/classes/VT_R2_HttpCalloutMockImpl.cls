/**
 * Created by Slav on 19.02.2019.
 */

@IsTest
public class VT_R2_HttpCalloutMockImpl implements HttpCalloutMock {
	public HttpResponse response;
	
	public HttpResponse respond (HttpRequest request) {
		return response;
	}
}