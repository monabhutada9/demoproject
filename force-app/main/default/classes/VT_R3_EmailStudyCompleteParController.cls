/**
 * Created by Redvik on 28.10.2019.
 */

public with sharing class VT_R3_EmailStudyCompleteParController {
	public  Id casId	{get;set;}
	private Case cas;
	private Case cas1;
	private String firstName;
	private String StudyParticipationComplete;
	private String contactTheStudyTeam;
	private String language;
	public String url {get; set;} {url = 'https://www.clinicalresearch.com/';}

	private Case getCase(){
		if(cas != null) return cas;
		cas = [SELECT 	Id,
						Contact.FirstName,
						VTD1_Study__r.VTD1_Protocol_Nickname__c,
						Contact.VTD1_UserId__r.LanguageLocaleKey,
						VTD2_Study_Phone_Number__c
				FROM Case
				WHERE Id = :casId
				LIMIT 1];
		return  cas;
	}

	private String getLanguage(){
		if(language != null) return language;
		return getCase().Contact.VTD1_UserId__r.LanguageLocaleKey;

	}

	public String getFirstName(){
		firstName = getCase().Contact.FirstName;
		return firstName;
	}

	public String getSalutation(){
		String salutation = VT_D1_TranslateHelper.getLabelValue('VT_R2_VisitEmailSalutaion', getLanguage());
		return salutation.replace('#Visit_participant', getFirstName());
	}

	public String getStudyParticipationComplete(){
		String message = VT_D1_TranslateHelper.getLabelValue('VTR3_StudyParticipationComplete', getLanguage());
		VT_D1_TranslateHelper.translate(new List<Case>{cas}, getLanguage());
		return message.replace('#StudyNickname', getCase().VTD1_Study__r.VTD1_Protocol_Nickname__c);
	}

	public String getContactTheStudyTeam(){
		String message = VT_D1_TranslateHelper.getLabelValue('VTR3_ContactTheStudyTeam', getLanguage());
		if(getPhoneNumber() != null){
			message = message.replace('#PhoneNumber', getPhoneNumber());
		}
		return message;
	}

	private String getPhoneNumber(){
		String phoneNumber;
		if (getCase().VTD2_Study_Phone_Number__c != null) {
			phoneNumber = getCase().VTD2_Study_Phone_Number__c;
		}
		else{
			List<VTR2_StudyPhoneNumber__c> phones = [
					SELECT Name
					FROM VTR2_StudyPhoneNumber__c
					WHERE VTR2_Study__c = :getCase().VTD1_Study__c
							AND VTR2_Language__c = :getLanguage()
					ORDER BY CreatedDate DESC
					LIMIT 1
			];
			if(phones.size() > 0) return phones[0].Name;
		}
		return phoneNumber;
	}

}