/**
    MIT License

    Copyright (c) 2018 Alex

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

@IsTest
public with sharing class QueryBuilderTest {
    private final static String FROM_ACCOUNT = 'FROM Account';
    private final static String SELECT_NAME_FROM = 'SELECT Name FROM';
    private final static String SELECT_ID_NAME_FROM = 'SELECT Id, Name FROM';
    private final static String SELECT_ID_FROM = 'SELECT Id FROM';

    @TestSetup
    public static void init() {
        createAccount(1);
    }

    @IsTest
    public static void testFrom1() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                .toList();
        System.assertEquals(1, accounts.size());

        String query = new QueryBuilder(Account.class).toString();
        System.assert(query.contains(FROM_ACCOUNT));

        query = new QueryBuilder(new Account()).toString();
        System.assert(query.contains(FROM_ACCOUNT));

        query = new QueryBuilder(new Account().getSObjectType()).toString();
        System.assert(query.contains(FROM_ACCOUNT));

        query = new QueryBuilder()
                .addFrom('Account')
                .toString();
        System.assert(query.contains(FROM_ACCOUNT));

        query = new QueryBuilder()
                .addFrom(Account.class)
                .toString();
        System.assert(query.contains(FROM_ACCOUNT));

        query = new QueryBuilder()
                .addFrom(new Account())
                .toString();
        System.assert(query.contains(FROM_ACCOUNT));

        query = new QueryBuilder()
                .addFrom(new Account().getSObjectType())
                .toString();
        System.assert(query.contains(FROM_ACCOUNT));
    }

    @IsTest
    public static void testField1() {
        String query = new QueryBuilder(Account.class)
                .addField('Name')
                .toString();
        System.assert(query.contains(SELECT_NAME_FROM));

        query = new QueryBuilder(Account.class)
                .addField(Account.Name)
                .toString();
        System.assert(query.contains(SELECT_NAME_FROM));

        query = new QueryBuilder(Account.class)
                .addField('Id, Name')
                .toString();
        System.assert(query.contains(SELECT_ID_NAME_FROM));

        query = new QueryBuilder(Account.class)
                .addFields(new Account(Name = 'test'))
                .toString();
        System.assert(query.contains(SELECT_NAME_FROM));

        query = new QueryBuilder(Account.class)
                .addFields('Name')
                .toString();
        System.assert(query.contains(SELECT_NAME_FROM));

        query = new QueryBuilder(Account.class)
                .addFields('Id, Name')
                .toString();
        System.assert(query.contains(SELECT_ID_NAME_FROM));

        query = new QueryBuilder(Account.class)
                .addFields('Name, Name')
                .toString();
        System.assert(query.contains(SELECT_NAME_FROM));

        query = new QueryBuilder(Account.class)
                .addFields(new List<String>{'Name'})
                .toString();
        System.assert(query.contains(SELECT_NAME_FROM));

        query = new QueryBuilder(Account.class)
                .addFields(new List<String>{'Id', 'Name'})
                .toString();
        System.assert(query.contains(SELECT_ID_NAME_FROM));

        query = new QueryBuilder(Account.class)
                .addFields(new List<String>{'Name', 'Name'})
                .toString();
        System.assert(query.contains(SELECT_NAME_FROM));

        query = new QueryBuilder(Account.class)
                .addFields(new Set<String>{'Name'})
                .toString();
        System.assert(query.contains(SELECT_NAME_FROM));

        query = new QueryBuilder(Account.class)
                .addFieldsAll()
                .toString();
        System.assert(checkIfQueryContains(query, new Set<String>{'Name', 'Id', 'RecordTypeId'}));

        query = new QueryBuilder(Account.class)
                .addFieldsAll('Account')
                .toString();
        System.assert(checkIfQueryContains(query, new Set<String>{'Name', 'Id', 'RecordTypeId'}));

        query = new QueryBuilder(Account.class)
                .addFieldsAllCreatable()
                .toString();
        System.assert(checkIfQueryContains(query, new Set<String>{'Name', 'Id', 'RecordTypeId'}));
    }

    private static Boolean checkIfQueryContains(String query, Set<String> words) {
        Boolean result = true;
        for (String word : words) {
            if (!query.containsIgnoreCase(word)) {
                result = false;
                break;
            }
        }
        return result;
    }

    @IsTest
    public static void testConditionManager() {
        String query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.SimpleCondition())
                .preview()
                .endConditions()
                .toString();
        System.assert(query.contains(SELECT_ID_FROM));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.SimpleCondition('Name = \'Account-1\''))
                .endConditions()
                .toString();
        System.assert(query.contains('WHERE Name = \'Account-1\''));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.SimpleCondition())
                .endConditions()
                .toString();
        System.assert(query.contains(SELECT_ID_FROM));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.NullCondition('Name').isNull())
                .endConditions()
                .toString();
        System.assert(query.contains('WHERE Name = NULL'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.NullCondition(Account.Name).isNull())
                .endConditions()
                .toString();
        System.assert(query.contains('WHERE Name = NULL'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.NullCondition(Account.Name).notNull())
                .endConditions()
                .toString();
        System.assert(query.contains('WHERE Name != NULL'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.CompareCondition('Name').eq('Account-1'))
                .endConditions()
                .toString();
        System.assert(query.contains('WHERE Name = \'Account-1\''));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.CompareCondition(Account.Name).eq('Account-1'))
                .endConditions()
                .toString();
        System.assert(query.contains('WHERE Name = \'Account-1\''));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.CompareCondition('Name').ne('Account-1'))
                .endConditions()
                .toString();
        System.assert(query.contains('WHERE Name != \'Account-1\''));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.CompareCondition('IsDeleted').eq(false))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE IsDeleted = FALSE'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.CompareCondition('IsDeleted').ne(true))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE IsDeleted != TRUE'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.CompareCondition('NumberOfEmployees').eq(1))
                .endConditions()
                .toString();
        System.assert(query.contains('WHERE NumberOfEmployees = 1'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.CompareCondition('NumberOfEmployees').ne(0))
                .endConditions()
                .toString();
        System.assert(query.contains('WHERE NumberOfEmployees != 0'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.CompareCondition('NumberOfEmployees').gt(0))
                .endConditions()
                .toString();
        System.assert(query.contains('WHERE NumberOfEmployees > 0'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.CompareCondition('NumberOfEmployees').gte(1))
                .endConditions()
                .preview()
                .toString();
        System.assert(query.contains('WHERE NumberOfEmployees >= 1'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.CompareCondition('NumberOfEmployees').lt(0))
                .endConditions()
                .toString();
        System.assert(query.contains('WHERE NumberOfEmployees < 0'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.CompareCondition('NumberOfEmployees').lte(-1))
                .endConditions()
                .preview()
                .toString();
        System.assert(query.contains('WHERE NumberOfEmployees <= -1'));
    }

    @IsTest
    public static void testLikeCondition1() {
        String query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.LikeCondition('Name').likeAnyBoth('ccount-'))
                .endConditions()
                .toString();
        System.assertEquals('SELECT Id FROM Account WHERE Name LIKE \'%ccount-%\'', query);

        query =  new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.LikeCondition(Account.Name).likeAnyBoth('ccount-'))
                .endConditions()
                .toString();
        System.assertEquals('SELECT Id FROM Account WHERE Name LIKE \'%ccount-%\'', query);

        query =  new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.LikeCondition('Name').likeAnyLeft('ccount-1'))
                .endConditions()
                .toString();
        System.assertEquals('SELECT Id FROM Account WHERE Name LIKE \'%ccount-1\'', query);

        query =  new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.LikeCondition('Name').likeAnyRight('Account-'))
                .endConditions()
                .toString();
        System.assertEquals('SELECT Id FROM Account WHERE Name LIKE \'Account-%\'', query);
    }

    @IsTest
    public static void testInCondition01() {
        String query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.InCondition('Id').inCollection(new Set<Id>()))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE Id IN (\'\')'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.InCondition('Id').inCollection(new List<Id>()))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE Id IN (\'\')'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.InCondition('Id').inCollection(new List<Account>()))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE Id IN (\'\')'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.InCondition('Id').inCollection(new Map<Id, SObject>()))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE Id IN (\'\')'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.InCondition('AnnualRevenue').inCollection(new Set<Decimal>{0.0}))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE AnnualRevenue IN (0.0)'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.InCondition('AnnualRevenue').inCollection(new List<Decimal>{0.0}))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE AnnualRevenue IN (0.0)'));

        query = new QueryBuilder(Account.class)
            .addConditions()
            .add(new QueryBuilder.InCondition('AnnualRevenue').inCollection(new Set<Decimal>()))
            .endConditions()
            .toString();
        System.assert(query.containsIgnoreCase('WHERE AnnualRevenue IN ()'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.InCondition('Id').inCollection(new QueryBuilder(Contact.class).addField('AccountId')))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE Id IN (SELECT AccountId FROM Contact)'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.InCondition('Id').notIn(new List<Account>()))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE Id NOT IN (\'\')'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.InCondition('Id').notIn(new QueryBuilder(Contact.class).addField('AccountId')))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE Id NOT IN (SELECT AccountId FROM Contact)'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.InCondition('Name').inCollection(new List<String>{
                        'Account-1'
                }))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE Name IN (\'Account-1\')'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.InCondition(Account.Name).inCollection(new List<String>{
                        'Account-1'
                }))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE Name IN (\'Account-1\')'));

        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.InCondition('Name').inCollection(new Set<String>{
                        'Account-1'
                }))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE Name IN (\'Account-1\')'));

        Set<Id> accIds = new QueryBuilder('Account').toIdSet();
        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.InCondition('Id').inCollection(accIds))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE Id IN (\'001'));
    }

    @IsTest
    public static void testComplexCondition0() {
        createAccount(1);
        QueryBuilder.ComplexCondition complex = new QueryBuilder.ComplexCondition();
        System.assertEquals('', complex.toString());

        complex = new QueryBuilder.ComplexCondition();
        complex.startCondition(new QueryBuilder.CompareCondition('Name').eq('Account-1'));
        String query = new QueryBuilder(Account.class)
                .addConditions()
                .add(complex)
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE (Name = \'Account-1\')'));

        complex = new QueryBuilder.ComplexCondition();
        complex.andCondition(new QueryBuilder.CompareCondition('Name').eq('Account-1'));
        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(complex)
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE (Name = \'Account-1\')'));

        complex = new QueryBuilder.ComplexCondition();
        complex.orCondition(new QueryBuilder.CompareCondition('Name').eq('Account-1'));
        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(complex)
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE (Name = \'Account-1\')'));

        complex = new QueryBuilder.ComplexCondition();
        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.NullCondition('Name').notNull())
                .add(complex)
                .setConditionOrder('1' + complex.addOrderIfNotEmpty('AND 2'))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE Name != NULL'));

        complex = new QueryBuilder.ComplexCondition();
        complex.andCondition(new QueryBuilder.CompareCondition('Name').eq('Account-1'));
        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.NullCondition('Name').notNull())
                .add(complex)
                .setConditionOrder('1' + complex.addOrderIfNotEmpty('AND 2'))
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE Name != NULL AND (Name = \'Account-1\')'));

        Id accId = new QueryBuilder('Account').toSObject().Id;
        complex = new QueryBuilder.ComplexCondition();
        complex.andCondition(new QueryBuilder.CompareCondition('Name').eq('Account-1'));
        complex.andCondition(new QueryBuilder.CompareCondition('Id').eq(accId));
        query = new QueryBuilder(Account.class)
                .addConditions()
                .add(complex)
                .endConditions()
                .toString();
        System.assert(query.containsIgnoreCase('WHERE (Name = \'Account-1\' AND Id = \'001'));
    }

    @IsTest
    public static void testLimit1() {
        String query = new QueryBuilder(Account.class)
                .setLimit(-1)
                .toString();
        System.assert(!query.containsIgnoreCase('LIMIT'));

        query = new QueryBuilder(Account.class)
                .setLimit(1)
                .toString();
        System.assert(query.containsIgnoreCase('LIMIT'));
    }

    @IsTest
    public static void testOrder1() {
        String query = new QueryBuilder(Account.class)
                .addField('Id')
                .addField('Name')
                .addOrderAsc('Id')
                .toString();
        System.assert(query.containsIgnoreCase('ORDER BY Id'));

        query = new QueryBuilder(Account.class)
                .addField('Name')
                .addField('Id')
                .addOrderDesc('Id')
                .toString();
        System.assert(query.containsIgnoreCase('ORDER BY Id DESC'));
    }

    @IsTest
    public static void testPreview() {
        new QueryBuilder(Account.class)
                .preview();
    }

    @IsTest
    public static void testPreviewCount() {
        new QueryBuilder(Account.class)
                .previewCount();
    }

    @IsTest
    public static void testToString() {
        String queryString = new QueryBuilder(Account.class)
                .toString();
        System.assertEquals('SELECT Id FROM Account', queryString);
    }

    @IsTest
    public static void testToCountString() {
        String queryString = new QueryBuilder(Account.class)
                .toStringCount();
        System.assertEquals('SELECT count() FROM Account', queryString);
    }

    @IsTest
    public static void testToCountString2() {
        String queryString = new QueryBuilder(Account.class)
                .addConditions()
                .add(new QueryBuilder.CompareCondition('Name').eq('Account-1'))
                .endConditions()
                .toStringCount();
        System.assertEquals('SELECT count() FROM Account WHERE Name = \'Account-1\'', queryString);
    }

    @IsTest
    public static void testResetResult() {
        QueryBuilder accountQueryBuilder = new QueryBuilder('Account');
        List<Account> accounts = (List<Account>) accountQueryBuilder
                .toList();
        System.assertEquals(1, accounts.size());

        createAccount(2);
        accounts = (List<Account>) accountQueryBuilder.toList();
        System.assertEquals(1, accounts.size());

        accountQueryBuilder.resetResult();
        accounts = (List<Account>) accountQueryBuilder.toList();
        System.assertEquals(2, accounts.size());
    }

    @IsTest
    public static void testToMap1() {
        Map<Id, SObject> accounts = new QueryBuilder('Account')
                .addField('Name')
                .toMap();
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testToMap2() {
        Map<Id, Account> accounts = new Map<Id, Account>();
        new QueryBuilder('Account')
                .addField('Name')
                .toMap(accounts);
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testToSobject() {
        Account account = (Account) new QueryBuilder('Account')
                .toSObject();
        System.assertNotEquals(null, account);
    }

    @IsTest
    public static void testToSobject2() {
        SObject account = (Account) new QueryBuilder('Account')
                .addConditions()
                .add(new QueryBuilder.NullCondition('Name').isNull())
                .endConditions()
                .toSObject();
        System.assertEquals(null, account);
    }

    @IsTest
    public static void testToIdSet() {
        Set<Id> accountIds = new QueryBuilder('Account')
                .toIdSet();
        System.assertEquals(1, accountIds.size());
    }

    @IsTest
    public static void testExtractIds() {
        final Id ACCOUNT_ID = new QueryBuilder('Account').toSObject().Id;
        createContact(ACCOUNT_ID);
        Set<Id> extractedIds = new QueryBuilder('Contact')
                .addField('AccountId')
                .extractIds('AccountId');
        System.assertEquals(1, extractedIds.size());
        System.assert(extractedIds.contains(ACCOUNT_ID));
    }

    @IsTest
    public static void testClone1() {
        QueryBuilder qb1 = new QueryBuilder('Account');
        System.assertEquals('SELECT Id FROM Account', qb1.toString());

        QueryBuilder qb2 = qb1.cloneQueryBuilder();
        qb2.addField('Name');

        qb1.resetResult();
        System.assertEquals('SELECT Id FROM Account', qb1.toString());
        System.assertEquals('SELECT Name FROM Account', qb2.toString());
    }

    @IsTest
    public static void testClone2() {
        QueryBuilder qb1 = new QueryBuilder('Account');
        qb1.toList();

        QueryBuilder qb2 = qb1.cloneQueryBuilder(true);
        createAccount(2);

        System.assertEquals(qb1.toList(), qb2.toList());
    }

    @IsTest
    public static void testStub1() {
        final String EXPECTED_SOQL = 'SELECT Name FROM Account';
        QueryBuilder stubbedQueryBuilder = new QueryBuilder(Account.class)
                .buildStub()
                .addStubToString(EXPECTED_SOQL)
                .applyStub();
        System.assertEquals(EXPECTED_SOQL, stubbedQueryBuilder.toString());

        stubbedQueryBuilder = new QueryBuilder(Account.class)
                .buildStub()
                .addStubToList(new List<Account>{
                        new Account(Name = 'Stubbed-Account')
                })
                .applyStub();

        List<Account> accounts = (List<Account>) stubbedQueryBuilder.toList();
        System.assertEquals(1, accounts.size());
        System.assertEquals('Stubbed-Account', accounts[0].Name);
    }

    @IsTest
    public static void testAddSubquery() {
        String EXPECTED_SOQL = 'SELECT (SELECT Id FROM Contact) FROM Account';
        String stringifiedQueryBuilder = new QueryBuilder(Account.class)
                .addSubQuery(new QueryBuilder(Contact.class))
                .toString();
        System.assertEquals(EXPECTED_SOQL, stringifiedQueryBuilder, 'Incorrect SOQL query');

        EXPECTED_SOQL = 'SELECT (SELECT Id FROM Contacts) FROM Account';
        stringifiedQueryBuilder = new QueryBuilder(Account.class)
                .addSubQuery('SELECT Id FROM Contacts')
                .toString();
        System.assertEquals(EXPECTED_SOQL, stringifiedQueryBuilder, 'Incorrect SOQL query');
    }

    @IsTest
    public static void testAddFieldsAll() {
        new QueryBuilder().addFieldsAll(Account.class);
        new QueryBuilder().addFieldsAll(Account.SObjectType);
        new QueryBuilder().addFieldsAll(new Account());
    }

    @IsTest
    public static void testAddFieldsAllCreatable() {
        String stringifiedQueryBuilder = new QueryBuilder()
                .addFieldsAllCreatable(Account.class)
                .toString();

        List<String> fieldsNames = getParsedFieldName(stringifiedQueryBuilder);
        Set<Schema.SObjectField> sObjectFields = getsObjectFields('Account', fieldsNames);
        for (Schema.SObjectField field: sObjectFields) {
            System.assert(field.getDescribe().isCreateable(), 'Incorrect isCreateable field describe value');
        }

        stringifiedQueryBuilder = new QueryBuilder()
                .addFieldsAllCreatable(Account.SObjectType)
                .toString();
        fieldsNames = getParsedFieldName(stringifiedQueryBuilder);
        sObjectFields = getsObjectFields('Account', fieldsNames);
        for (Schema.SObjectField field: sObjectFields) {
            System.assert(field.getDescribe().isCreateable(), 'Incorrect isCreateable field describe value');
        }

        stringifiedQueryBuilder = new QueryBuilder()
                .addFieldsAllCreatable(new Account())
                .toString();
        fieldsNames = getParsedFieldName(stringifiedQueryBuilder);
        sObjectFields = getsObjectFields('Account', fieldsNames);
        for (Schema.SObjectField field: sObjectFields) {
            System.assert(field.getDescribe().isCreateable(), 'Incorrect isCreateable field describe value');
        }

        stringifiedQueryBuilder = new QueryBuilder(Account.class)
                .addFieldsAllUpdatable()
                .toString();
        fieldsNames = getParsedFieldName(stringifiedQueryBuilder);
        sObjectFields = getsObjectFields('Account', fieldsNames);
        for (Schema.SObjectField field: sObjectFields) {
            System.assert(field.getDescribe().isUpdateable(), 'Incorrect isUpdateable field describe value');
        }

        stringifiedQueryBuilder = new QueryBuilder()
                .addFieldsAllUpdatable(Account.SObjectType)
                .toString();
        fieldsNames = getParsedFieldName(stringifiedQueryBuilder);
        sObjectFields = getsObjectFields('Account', fieldsNames);
        for (Schema.SObjectField field: sObjectFields) {
            System.assert(field.getDescribe().isUpdateable(), 'Incorrect isUpdateable field describe value');
        }

        stringifiedQueryBuilder = new QueryBuilder()
                .addFieldsAllUpdatable(Account.class)
                .toString();
        fieldsNames = getParsedFieldName(stringifiedQueryBuilder);
        sObjectFields = getsObjectFields('Account', fieldsNames);
        for (Schema.SObjectField field: sObjectFields) {
            System.assert(field.getDescribe().isUpdateable(), 'Incorrect isUpdateable field describe value');
        }

        stringifiedQueryBuilder = new QueryBuilder()
                .addFieldsAllUpdatable(new Account())
                .toString();
        fieldsNames = getParsedFieldName(stringifiedQueryBuilder);
        sObjectFields = getsObjectFields('Account', fieldsNames);
        for (Schema.SObjectField field: sObjectFields) {
            System.assert(field.getDescribe().isUpdateable(), 'Incorrect isUpdateable field describe value');
        }
    }

    @IsTest
    public static void testAddConditionsWithOrder() {
        String stringifiedQueryBuilder = new QueryBuilder(Account.class)
                .addConditionsWithOrder('1')
                .toString();

        System.assertEquals('1', stringifiedQueryBuilder, 'Incorrect SOQL query');
    }

    @IsTest
    public static void testSetOffset() {
        new QueryBuilder(Account.class)
                .setOffset(10);
        // Operating with the query offset is not currently implemented
    }

    @IsTest
    public static void testAddOrderAsc() {
        String expectedSOQL = 'SELECT Id FROM Account ORDER BY Name ASC';
        String stringifiedQueryBuilder = new QueryBuilder(Account.class)
                .addOrderAsc(Account.Name)
                .toString();
        System.assertEquals(expectedSOQL, stringifiedQueryBuilder, 'Incorrect SOQL query');


        expectedSOQL = 'SELECT Id FROM Account ORDER BY Name DESC';
        stringifiedQueryBuilder = new QueryBuilder(Account.class)
                .addOrderDesc(Account.Name)
                .toString();
        System.assertEquals(expectedSOQL, stringifiedQueryBuilder, 'Incorrect SOQL query');
    }

    @IsTest
    public static void testAddGroupBy() {
        String expectedSOQL = 'SELECT Id FROM Account GROUP BY Name';
        String stringifiedQueryBuilder = new QueryBuilder(Account.class)
                .addGroupBy('Name')
                .toString();
        System.assertEquals(expectedSOQL, stringifiedQueryBuilder, 'Incorrect SOQL query');

        expectedSOQL = 'SELECT Id FROM Account GROUP BY Name';
        stringifiedQueryBuilder = new QueryBuilder(Account.class)
                .addGroupBy(Account.Name)
                .toString();
        System.assertEquals(expectedSOQL, stringifiedQueryBuilder, 'Incorrect SOQL query');

        expectedSOQL = 'SELECT Id FROM Account GROUP BY Name';
        stringifiedQueryBuilder = new QueryBuilder(Account.class)
                .addGroupBy(new Set<String> {'Name'})
                .toString();
        System.assertEquals(expectedSOQL, stringifiedQueryBuilder, 'Incorrect SOQL query');
    }

    @IsTest
    public static void testSetCheckCRUDAndFLS() {
        new QueryBuilder(Account.class).setCheckCRUDAndFLS(true);

        new QueryBuilder(Account.class).setCheckCRUDAndFLS();

        new QueryBuilder(Account.class).setCheckCRUD();

        new QueryBuilder(Account.class).setCheckCRUD(true);

        new QueryBuilder(Account.class).setCheckFLS();

        new QueryBuilder(Account.class).newQuery();
    }

    @IsTest
    public static void testToQueryLocator() {
        String expectedSoql = 'SELECT Id FROM Account';
        Database.QueryLocator queryLocator = new QueryBuilder(Account.class)
                .toQueryLocator();

        System.assertEquals(expectedSoql, queryLocator.getQuery(), 'Incorrect query of query QueryLocator');
    }

    @IsTest
    public static void testExtractField() {
        List<Object> objects = new QueryBuilder(Account.class)
                .addField('Name')
                .extractField('Name');

        System.assertEquals('["Account-1"]', JSON.serialize(objects), 'Incorrect extracted field object list');

        objects = new QueryBuilder(Account.class)
                .addField('Name')
                .extractField(Account.Name);

        System.assertEquals('["Account-1"]', JSON.serialize(objects), 'Incorrect extracted field object list');
    }

    @IsTest
    public static void testValidate() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                .toList();
        QueryBuilder.SObjectReadValidator sObjectReadValidator = new QueryBuilder.SObjectReadValidator(accounts);
        try {
            sObjectReadValidator.validate();
        } catch (Exception exc) {
            System.assertEquals(null, exc, 'Validation failed with exception: ' + exc.getMessage());
        }
    }

    @IsTest
    public static void testSetRelatedObjectType() {
        QueryBuilder.RecordTypeCondition condition = new QueryBuilder.RecordTypeCondition('Type');
        condition.setRelatedObjectType('Contact');
    }

    //Utility methods
    private static Account createAccount(Integer i) {
        Account result = new Account();
        result.Name = 'Account-' + i;
        result.NumberOfEmployees = 0;
        result.AnnualRevenue = 10.00;
        insert result;
        return result;
    }

    private static Contact createContact(Id accId) {
        Contact result = new Contact();
        result.LastName = 'Contact-' + accId;
        result.AccountId = accId;
        result.Email = 'email' + accId + '@example.com';
        result.Description = accId;
        insert result;
        return result;
    }

    private static Map<String, Schema.SObjectField> getFieldsMapDescribe(String sObjectName) {
        return Schema.getGlobalDescribe()
                .get(sObjectName)
                .getDescribe()
                .fields
                .getMap();
    }

    private static List<String> getParsedFieldName(String stringifiedQueryBuilder) {
        return stringifiedQueryBuilder
                .substringBetween('SELECT ', ' FROM')
                .deleteWhitespace()
                .split('\\,');
    }

    private static Set<Schema.SObjectField> getsObjectFields(String sObjectName, List<String> fieldsNames) {
        Map<String, Schema.SObjectField> fieldMap = getFieldsMapDescribe(sObjectName);
        Set<Schema.SObjectField> sObjectFields = new Set<Schema.SObjectField>();
        for (String fieldName : fieldsNames) {
            SObjectField field = fieldMap.get(fieldName);
            sObjectFields.add(field);
        }
        return sObjectFields;
    }
}