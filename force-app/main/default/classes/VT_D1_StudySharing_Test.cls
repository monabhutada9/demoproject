/**
* @author: Carl Judge
* @date: 11-Sep-18
* @description: Covers VT_D1_StudySharingConfiguratorController, VT_D1_StudySharingConfigurationTrigger,
*   VT_D1_StudySharingConfigurationHandler, VT_D1_StudySharingQueryHelper, VT_D1_StudySharingBatch
**/

@IsTest
private class VT_D1_StudySharing_Test {

    @TestSetup
    private static void setupMethod() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.stopTest();



        Account acc = new Account(Name = 'testAcc');
        insert acc;
        Contact con = new Contact(LastName = 'Caregiver', AccountId = acc.Id);
        insert con;

        User caregiver = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Caregiver'].Id,
            FirstName = 'Caregiver',
            LastName = 'Caregiver',
            Email = VT_D1_TestUtils.generateUniqueUserName(),
            Username = VT_D1_TestUtils.generateUniqueUserName(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            IsActive = true,
            ContactId = con.Id
        );
        insert caregiver;

        /*User patient = [
                SELECT Id, ContactId, Contact.AccountId
                FROM User
                WHERE Username LIKE '%@iqviavttest.com'
                AND Profile.Name = 'Caregiver'
                AND Contact.AccountId != NULL
                ORDER BY CreatedDate DESC
                LIMIT 1
        ];*/

        Case cas = new Case(
            VTD1_Study__c = study.Id,
            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VTD1_PCF').getRecordTypeId(),
            VTD1_Patient_User__c = caregiver.Id
        );
        insert cas;
    }

    @IsTest
    private static void testControllerMethods() {
        Id studyId = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c].Id;
        Test.startTest();
        Map <String, Object> params = new Map<String, Object>();
        params.put('offset', '0');
        params.put('limit', '100');
        String items = VT_D1_StudySharingConfiguratorController.getConfigs(studyId, 'caregiver', JSON.serialize(params));

        VT_D1_StudySharingConfiguratorController.getSessionId();
        VT_D1_StudySharingConfiguratorController.getStudyIsClosed(studyId);


        List<VTD1_Study_Sharing_Configuration__c> configs = new List<VTD1_Study_Sharing_Configuration__c>();
        Id uid;
        Map <String, Object> params2 = (Map<String, Object>)JSON.deserializeUntyped(items);
        System.debug('items= ' + items);
        System.debug('params2= ' + params2);
        /*for (VT_D1_StudySharingConfiguratorController.ShareConfigItem item : items) {
            if (item.config.VTD1_Access_Level__c != 'Edit' && uid == null) {
                item.config.VTD1_Access_Level__c = 'Edit';
                System.debug('FOUND');
                uid = item.config.VTD1_User__c;
            }
            configs.add(item.config);
        }
        VT_D1_StudySharingConfiguratorController.saveConfigs(JSON.serialize(configs));

        insert new Task(
            OwnerId = uid,
            WhatId = studyId,
            Subject = 'SharingTestTask'
        );*/

        //items = VT_D1_StudySharingConfiguratorController.getConfigs(studyId);
        Test.stopTest();
    }

    @IsTest
    private static void testAddAccess() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        User caregiver = [SELECT Id FROM User WHERE Username LIKE '%@iqviavttest.com'
        AND Profile.Name = 'Caregiver'
        AND Contact.AccountId != null];

        VTD1_Study_Sharing_Configuration__c studyConfig = new VTD1_Study_Sharing_Configuration__c(
            VTD1_Study__c = study.Id,
            VTD1_User__c = caregiver.Id,
            VTD1_Access_Level__c = 'Edit'
        );

        Test.startTest();
        insert studyConfig;
        Test.stopTest();
    }

    /*
    @IsTest
    private static void testRemoveAccess() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        User caregiver = [SELECT Id FROM User WHERE Username = 'caregiver@test.com_user'];

        VTD1_Study_Sharing_Configuration__c studyConfig = new VTD1_Study_Sharing_Configuration__c(
            VTD1_Study__c = study.Id,
            VTD1_User__c = caregiver.Id,
            VTD1_Access_Level__c = 'Edit'
        );
        insert studyConfig;

        Test.startTest();
        studyConfig.VTD1_Access_Level__c = 'None';
        update studyConfig;
        Test.stopTest();
    } */
}