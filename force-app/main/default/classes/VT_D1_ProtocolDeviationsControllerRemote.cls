public with sharing class VT_D1_ProtocolDeviationsControllerRemote {
    private static List<String> PICKLIST_FIELDS = new List<String>{
            'VTD1_Site_Subject_Level__c',
            'VTD1_PD_Category__c',
            'VTD1_Deviation_Severity__c',
            'VTR2_Location__c',
            'VTD1_PD_Status__c'
    };

    @AuraEnabled
    public static ProtocolsInfo getProtocolDeviations(Id studyId) {
        ProtocolsInfo pInfo = new ProtocolsInfo();
        try {
            pInfo.protocols = getProtocols(studyId);
            pInfo.picklistsInfo = getPicklistsInfo();
            pInfo.picklistValues = VT_R2_ProtocolDeviationService.getPicklistValues(PICKLIST_FIELDS);
            pInfo.community = getCommunities();
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
        return pInfo;
    }

    @AuraEnabled
    public static String saveDraftRemote(VTD1_Protocol_Deviation__c draft) {
        String error;
        if (isPDWithStatusDraft(draft.Id)) {
            updateDraft(draft);
        } else {
            error = Label.VT_R2_CanNotEdit;
        }
        return error;
    }

    @AuraEnabled
    public static VTD1_Protocol_Deviation__c acknowledgeRemote(VTD1_Protocol_Deviation__c pd) {
        if (isAcknowledgmentFalse(pd.Id)) {
            pd.VTD2_DateAcknowledged__c = Date.today();
            pd.VTD2_PI_acknowledgment__c = true;
            updateDraft(pd);
        } else {
            throw new AuraHandledException(Label.VT_R2_CanNotAcknowledge);
        }
        return pd;
    }

    //---------------------- PRIVATE ---------------------------//

    private static String getCommunities() {
        List<Network> communities = [SELECT id, name FROM Network WHERE Id = :Network.getNetworkId()];
        if(!communities.isEmpty()) {
            return communities[0].Name;
        }else return null;
    }
    private static void updateDraft(VTD1_Protocol_Deviation__c draft) {
        try {
            update draft;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1]
                    .substring(0, e.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1].indexOf(':'))); /*+ '\n' + e.getStackTraceString()); sh-9248 */
        }
    }
    private static PicklistsInfo getPicklistsInfo() {
        PicklistsInfo picklistsInfo = new PicklistsInfo();
        picklistsInfo.authorizedToContinueOptionsList =
                VT_D1_CustomMultiPicklistController.getSelectOptions(new VTD1_Protocol_Deviation__c(),
                        'VTD1_Subject_Authorized_to_Continue_Flag__c');
        return picklistsInfo;
    }
    private static List<ProtocolWrapper> getProtocols(Id studyId) {
        List<Id> myStudyIds = VT_D1_PIStudySwitcherRemote.getMyStudyIds(studyId);
        List<VTD1_Protocol_Deviation__c> lst = getPDs(myStudyIds);

        List<ProtocolWrapper> lstRes = new List<ProtocolWrapper>();
        for (VTD1_Protocol_Deviation__c pd : lst) {
            ProtocolWrapper pw = new ProtocolWrapper(pd);
            lstRes.add(pw);
        }
        return lstRes;
    }

    //--------------------- SOQL ------------------------//

    private static Boolean isAcknowledgmentFalse(Id draftId) {
        List<VTD1_Protocol_Deviation__c> pds = [
                SELECT Id
                FROM VTD1_Protocol_Deviation__c
                WHERE
                        //VTD1_PD_Status__c = 'Confirmed'
                Id = :draftId
                AND VTD2_PI_acknowledgment__c = FALSE
        ];
        return !pds.isEmpty();
    }
    private static Boolean isPDWithStatusDraft(Id draftId) {
        List<VTD1_Protocol_Deviation__c> pdDraftList = [
                SELECT Id
                FROM VTD1_Protocol_Deviation__c
                WHERE VTD1_PD_Status__c = 'Draft'
                    AND Id = :draftId
        ];
        return !pdDraftList.isEmpty();
    }
    private static List<VTD1_Protocol_Deviation__c> getPDs(List<Id> myStudyIds) {
        return [
                SELECT Id
                        , Name
                        , VTD1_StudyId__r.Name
                        , VTD1_PD_Status__c
                        , VTD1_Site_Subject_Level__c
                        , VTD1_PD_Category__c
                        , VTD1_Deviation_Description__c
                        , VTD1_Corrective_Action_Details__c
                        , VTD1_Date_Documented__c
                        , format(VTD1_PD_Confirmed_Date__c)
                        , VTD1_Primary_Investigator_ID__r.Name
                        , VTD1_Virtual_Site__r.Name
                        , VTD1_Site_Status__c
                        , VTD1_Patient_Status__c
                        , VTD1_Subject_Authorized_to_Continue_Flag__c
                        , format(CreatedDate)
                        , VTD1_Documenting_User__c
                        , VTd1_Days_Since_Open__c
                        , VTD1_Reported_to_IRB_EC__c
                        , format(VTD1_Date_Reported_to_IRB_IEC__c)
                        , format(VTD1_IRB_EC_Response_Date__c)
                        , VTD1_Reported_to_Sponsor__c
                        , format(VTD1_Date_Reported_to_Sponsor__c)
                        , VTD1_Deviation_Severity__c, VTD1_Subject_ID__c
                        , format(VTD1_Deviation_Occurred_Date_Key__c)
                        , format(VTD2_DateAcknowledged__c)
                        , VTD2_PI_acknowledgment__c
                        , VTD2_AcknowledgmentComment__c
                        , VTD1_Subject_ID__r.CaseNumber
                        , VTD2_TMA_Comments__c
                        , VTD1_Subject_ID__r.Contact.Name
                        , VTR2_Location__c
                        , VTD1_PD_Data_Tag__c
                FROM VTD1_Protocol_Deviation__c
                WHERE VTD1_StudyId__c IN :myStudyIds
        ];
    }

    //--------------------- WRAPPERS ------------------------//

    public class ProtocolWrapper {
        @AuraEnabled public List<String> itemsVTD1_PD_Data_Tag { get; set; }
        @AuraEnabled public Date dateDocumented { get; set; }
        @AuraEnabled public Date CreatedDate { get; set; }
        @AuraEnabled public VTD1_Protocol_Deviation__c pd { get; set; }
        @AuraEnabled public String reportedToSponsor {
            get {
                return String.valueOf(pd.VTD1_Reported_to_Sponsor__c);
            }
            set {
                pd.VTD1_Reported_to_Sponsor__c = Boolean.valueOf(value);
                reportedToSponsor = value;
            }
        }
        @AuraEnabled public String reportedToIRBEC {
            get { 
                return String.valueOf(pd.VTD1_Reported_to_IRB_EC__c);
            }
            set {
                pd.VTD1_Reported_to_IRB_EC__c = Boolean.valueOf(value);
                reportedToSponsor = value;
            }
        }

        public ProtocolWrapper(VTD1_Protocol_Deviation__c p) {
            pd = p;
            dateDocumented = p.CreatedDate.date();
            CreatedDate = p.CreatedDate.date();
            if( p.VTD1_PD_Data_Tag__c != null) {
                itemsVTD1_PD_Data_Tag = p.VTD1_PD_Data_Tag__c.split('\n');
            }
        }
    }

    public class ProtocolsInfo {
        @AuraEnabled public List<ProtocolWrapper> protocols;
        @AuraEnabled public PicklistsInfo picklistsInfo;
        @AuraEnabled public String community;
        @AuraEnabled public Map<String, List<VT_R2_ProtocolDeviationService.PicklistValueWrapper>> picklistValues;
    }

    public class PicklistsInfo {
        @AuraEnabled public List<String> authorizedToContinueOptionsList;
    }
}