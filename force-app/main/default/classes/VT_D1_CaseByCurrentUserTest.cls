@IsTest
public without sharing class VT_D1_CaseByCurrentUserTest {

    public static void getExistingCaseSuccessTest() {
        Case pCase = [SELECT id, VTD1_Patient_User__c, AccountId FROM Case WHERE Caregiver_s_Name__c != NULL LIMIT 1];
        User patientUser = [SELECT id FROM User WHERE id =:pCase.VTD1_Patient_User__c LIMIT 1];
        User caregiverUser = [SELECT Id FROM User WHERE VTD1_Profile_Name__c = 'Caregiver' AND AccountId =:pCase.AccountId LIMIT 1];

        Test.startTest();
        VT_D1_CaseByCurrentUser.CaseWrapper cwByPatient;
        VT_D1_CaseByCurrentUser.CaseWrapper cwByCaregiver;
        System.runAs(patientUser) {
            cwByPatient = VT_D1_CaseByCurrentUser.getCase();
        }
        System.runAs(caregiverUser) {
            cwByCaregiver = VT_D1_CaseByCurrentUser.getCase();
        }
        Test.stopTest();
        System.assertEquals(pCase.Id, cwByPatient.caseObj.Id);
        System.assertEquals(pCase.Id, cwByCaregiver.caseObj.Id);
    }

    public static void getNewCaseSuccessTest() {
        Case pCase = [SELECT id, VTD1_Patient_User__c, AccountId FROM Case LIMIT 1];
        User patientUser = [SELECT id FROM User WHERE id =:pCase.VTD1_Patient_User__c LIMIT 1];
        VT_D1_CaseByCurrentUser.CaseWrapper cw;

        Test.startTest();
        System.runAs(patientUser) {
            cw = VT_D1_CaseByCurrentUser.getCase();
        }
        Test.stopTest();
        System.assertEquals(pCase.Id, cw.caseObj.Id);
    }
}