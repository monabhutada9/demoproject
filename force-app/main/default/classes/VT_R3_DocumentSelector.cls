/**
 * Created by user on 17-Oct-19.
 */

public with sharing class VT_R3_DocumentSelector {

    public static VTD1_Document__c getDocumentById(Id documentId) {
        List<VTD1_Document__c> documentList = [
            SELECT Id, Name, RecordTypeId, RecordType.Name, VTD1_FileNames__c, VTD1_Nickname__c, VTD1_Comment__c,
                VTD1_Version__c, VTD1_Status__c, VTR3_CreatedProfile__c, VTR3_Category__c, VTR3_Associated_Visit__c
            FROM VTD1_Document__c
            WHERE Id = :documentId];
        if (documentList.size() != 0) {
            return documentList[0];
        } else {
            return null;
        }

    }

}