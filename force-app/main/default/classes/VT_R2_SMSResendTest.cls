/**
 * Created by Danylo Belei on 15.05.2019.
 */

@IsTest
private class VT_R2_SMSResendTest {

    @TestSetup
    private static void testSetup(){
        new DomainObjects.VTR2_SendSMSLog_t()
                .addContact(new DomainObjects.Contact_t())
                .setStatus('Failure 1')
                .persist();

    }

    @isTest
    private static void testBatch() {
        setUpMCSettings();
        Test.startTest();
        Database.executeBatch(new VT_R2_SMSResend());
        Test.stopTest();

        //System.assertEquals()

    }

    @isTest
    private static void testSchedule() {
        setUpMCSettings();
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        System.schedule('SMSResend',CRON_EXP, new VT_R2_SMSResend());
        Test.stopTest();

        //System.assertEquals()

    }
    static void setUpMCSettings() {
        List<VTR2_McloudSettings__mdt> mcloudSettings = new List<VTR2_McloudSettings__mdt>();
        mcloudSettings.add(new VTR2_McloudSettings__mdt(VTR2_OutboundMessageKey_API_Triggered__c = 'NjU6Nzg6MA',
                VTR2_RestURI__c = 'https://mc2gwv28hdsr86gd-sldnjzvzzgm.rest.marketingcloudapis.com/',
                VTR2_ShortCode__c = '99808'));
        VT_R2_McloudBroadcaster.setMCsettings(mcloudSettings);
    }
}