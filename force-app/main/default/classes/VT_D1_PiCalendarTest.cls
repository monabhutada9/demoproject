@IsTest
private class VT_D1_PiCalendarTest {
	@TestSetup
	private static void setup() {
		Test.startTest();
		HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);

		VT_D1_TestUtils.createTestPatientCandidate(1);
		Map<String, VTD1_ProtocolVisit__c> PVsMap = new Map<String, VTD1_ProtocolVisit__c>{
			'Archived' => new VTD1_ProtocolVisit__c(VTD1_Study__c = study.Id, VTD1_isArchivedVersion__c = True, VTR2_Visit_Participants__c = VTD1_ProtocolVisit__c.VTR2_Visit_Participants__c.getDescribe().getPicklistValues()[0].getValue()),
			'notArchived' => new VTD1_ProtocolVisit__c(VTD1_Study__c = study.Id, VTD1_isArchivedVersion__c = False, VTR2_Visit_Participants__c = VTD1_ProtocolVisit__c.VTR2_Visit_Participants__c.getDescribe().getPicklistValues()[0].getValue())
		};
		insert PVsMap.values();
		System.debug('PVsMap: '+PVsMap);
		Test.stopTest();
		Case caseOpen = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];
		User patient = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Patient' AND ContactId != null AND IsActive = true ORDER BY CreatedDate DESC LIMIT 1];
		User PI = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Primary Investigator' AND ContactId != null AND IsActive = true ORDER BY CreatedDate DESC LIMIT 1];
		System.debug('caseOpen: '+caseOpen);

		List<VTD1_Actual_Visit__c> AVs = new List<VTD1_Actual_Visit__c>{
			new VTD1_Actual_Visit__c(VTD1_Case__c = caseOpen.Id, VTD1_Contact__c = patient.ContactId,
				VTD1_Scheduled_Date_Time__c = Datetime.now(),
				//VTD1_Visit_Duration__c = 10,
				VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED,
				VTD1_Protocol_Visit__c = PVsMap.get('Archived').Id),
			/*new VTD1_Actual_Visit__c(VTD1_Case__c = caseOpen.Id, VTD1_Contact__c = patient.ContactId,
				VTD1_Scheduled_Date_Time__c = Datetime.now(),
				//VTD1_Visit_Duration__c = 10,
				OwnerId = PI.Id,
				VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
				VTD1_Protocol_Visit__c = PVsMap.get('Archived').Id), */
			new VTD1_Actual_Visit__c(VTD1_Case__c = caseOpen.Id, VTD1_Contact__c = patient.ContactId,
				VTD1_Scheduled_Date_Time__c = Datetime.now(),
				//VTD1_Visit_Duration__c = 10,
				VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED,
				VTD1_Protocol_Visit__c = PVsMap.get('notArchived').Id),
			new VTD1_Actual_Visit__c(VTD1_Case__c = caseOpen.Id, VTD1_Contact__c = patient.ContactId,
				VTD1_Scheduled_Date_Time__c = Datetime.now(),
				//VTD1_Visit_Duration__c = 10,
				VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED,
				VTD1_Protocol_Visit__c = PVsMap.get('notArchived').Id)
		};

		System.runAs(new User(Id = UserInfo.getUserId())) {
			insert AVs;
			System.debug('AVs: '+AVs);
		}
	}

	private static void extraSetup() {
		Test.startTest();
		List<Event> events = new List<Event>();
		User patient = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Patient' AND ContactId != null AND IsActive = true ORDER BY CreatedDate DESC LIMIT 1];
		User PI = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Primary Investigator' AND ContactId != null AND IsActive = true ORDER BY CreatedDate DESC LIMIT 1];
		HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c ORDER BY CreatedDate DESC LIMIT 1];
		List<Visit_Member__c> VMs = new List<Visit_Member__c>();
		for (VTD1_Actual_Visit__c av : [SELECT Id FROM VTD1_Actual_Visit__c ORDER BY CreatedDate DESC LIMIT 4]) {
			events.add(new Event(HealthCloudGA__CarePlanTemplate__c = study.Id, VTD1_Actual_Visit__c = av.Id, OwnerId = PI.Id, StartDateTime = Datetime.now(), DurationInMinutes = 10));
			VMs.add(new Visit_Member__c(VTD1_Actual_Visit__c = av.Id, VTD1_Participant_User__c = patient.Id, Participant_Contact__c = patient.ContactId));
			VMs.add(new Visit_Member__c(VTD1_Actual_Visit__c = av.Id, VTD1_Participant_User__c = PI.Id, Participant_Contact__c = PI.ContactId));
		}
		insert VMs;

		Id outOfOfficeRTId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('OutOfOffice').getRecordTypeId();
		events.add(new Event(RecordTypeId = outOfOfficeRTId, OwnerId = PI.Id, StartDateTime = Datetime.now(), DurationInMinutes = 10));
		insert events;

		System.debug('VMs: '+VMs);
		System.debug('Events: '+events);
		Test.stopTest();
	}

	@IsTest
	private static void getEventsTest() {
		extraSetup();
		User u = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Primary Investigator' AND ContactId != null AND IsActive = true ORDER BY CreatedDate DESC LIMIT 1];
		System.assertNotEquals(null, u);

		List<VT_D1_PiCalendar.CalendarObj> events;
		System.debug('runAs: '+ u.Id);
			System.runAs(u) {
				events = VT_D1_PiCalendar.getEvents();
				System.debug('events: '+events.size());
			}
			System.assert(!events.isEmpty());
	}
	@IsTest
	private static void cancelVisitRemoteTest() {
		extraSetup();
		User u = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Primary Investigator' AND ContactId != null AND IsActive = true ORDER BY CreatedDate DESC LIMIT 1];
		System.assertNotEquals(null, u);

		//System.debug('runAs: '+ u.Id);
		//System.runAs(u) {
			VTD1_Actual_Visit__c av = [SELECT Id,
					                          VTD1_Status__c, toLabel(VTD1_Status__c) statusLabel
			                           FROM   VTD1_Actual_Visit__c
			                           WHERE  VTD1_Status__c!= :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED
			                           LIMIT 1
			];
			System.debug('Actual Visit: '+av);
			VT_D1_PiCalendar.cancelVisitRemote(av.Id, 'test');
			VTD1_Actual_Visit__c avCancelled = [SELECT Id, VTD1_Reason_for_Cancellation__c FROM VTD1_Actual_Visit__c WHERE Id = :av.Id];
			System.debug('Cancelled: '+av);
		//}
		System.assertEquals('test',avCancelled.VTD1_Reason_for_Cancellation__c);
	}
	@IsTest
	private static void deleteEventTest() {
		extraSetup();
		User u = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Primary Investigator' AND ContactId != null AND IsActive = true ORDER BY CreatedDate DESC LIMIT 1];
		System.assertNotEquals(null, u);

		System.debug('runAs: '+ u.Id);
		System.runAs(u) {
			List<Event> events = [SELECT Id FROM Event WHERE RecordType.DeveloperName = 'OutOfOffice' AND OwnerId = :u.Id];
			System.debug('OutOfOffice Events: ' + events);
			VT_D1_PiCalendar.deleteEvent(events[0].Id);
			List<Event> eventsAfterDeletion = [SELECT Id FROM Event WHERE Id = :events[0].Id];
			System.debug('Events after deletion: ' + eventsAfterDeletion);

			System.assert(!events.isEmpty());
			System.assert(eventsAfterDeletion.isEmpty());
		}
	}
	@IsTest
	private static void upsertEventsTest() {
		extraSetup();
		User u = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Primary Investigator' AND ContactId != null AND IsActive = true ORDER BY CreatedDate DESC LIMIT 1];
		System.assertNotEquals(null, u);

		delete [SELECT Id FROM Event WHERE Subject = 'TestUpsertEvent'];
		VT_D1_PiCalendar.EventObj eventObj = new VT_D1_PiCalendar.EventObj(null,
			'TestUpsertEvent',
			Datetime.now(),
			Datetime.now(),
			'',
			u.Id
		);
		System.debug(eventObj);

		VT_D1_PiCalendar.EventObj newEvent = VT_D1_PiCalendar.upsertEvents(JSON.serialize(eventObj),
			'Event',
			'Subject',
			'StartDateTime',
			'EndDateTime',
			'Description',
			'OwnerId'
		);
		Event ev = [SELECT Id, Subject FROM Event WHERE Id = :newEvent.Id];
		System.debug('New Event: '+ev);
		System.assertEquals('TestUpsertEvent',ev.Subject);
	}
}