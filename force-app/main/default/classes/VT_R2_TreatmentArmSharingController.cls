public with sharing class VT_R2_TreatmentArmSharingController {

    public class ShareConfigItem implements Comparable {
        @AuraEnabled public VTR2_Treatment_Arm_Sharing_Configuration__c config;
        @AuraEnabled public Id userId;
        @AuraEnabled public String userName;
        @AuraEnabled public String profileName;
        @AuraEnabled public String accessLevel;
        @AuraEnabled public Boolean disableNone;
        @AuraEnabled public Boolean disableRead;
        @AuraEnabled public Boolean disableEdit;

        public Integer compareTo(Object compareTo) {
            ShareConfigItem compareToItem = (ShareConfigItem) compareTo;
            if (this.userName != compareToItem.userName) {
                return this.userName < compareToItem.userName ? -1 : 1;
            }
            return 0;
        }
    }

    @AuraEnabled
    public static List<ShareConfigItem> getConfigs(Id studyId) {
        return new VT_R2_TreatmentArmSharingHelper().getConfigs(studyId);
    }

    //SH-15893
    @AuraEnabled
    public static Boolean checkNotSysAdminProfile() {
        User user = [SELECT Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
        return user.Profile.Name != 'System Administrator' ? true : false;
    }

    @AuraEnabled
    public static String getArmAccessByStudy(Id studyId) {
        try {
            VTR2_Treatment_Arm_Sharing_Configuration__c config = [
                    SELECT VTR2_Access_Level__c
                    FROM VTR2_Treatment_Arm_Sharing_Configuration__c
                    WHERE VTR2_Study__c = :studyId AND VTR2_User__c = :UserInfo.getUserId()
            ];
            return config.VTR2_Access_Level__c;
        } catch (Exception e) {
            return 'None';
        }
    }

    @AuraEnabled
    public static String getArmAccessByCase(Id caseId) {
        Id studyId = [SELECT VTD1_Study__c FROM Case WHERE Id = :caseId].VTD1_Study__c;
        String profileName = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;

        List<VTR2_Treatment_Arm_Sharing_Configuration__c> config = [
                SELECT VTR2_Access_Level__c
                FROM VTR2_Treatment_Arm_Sharing_Configuration__c
                WHERE VTR2_Study__c = :studyId AND VTR2_User__c = :UserInfo.getUserId()
        ];

        if (config.size() > 0) {
            return config[0].VTR2_Access_Level__c;
        } else if (profileName == 'System Administrator') {
            return 'Edit';
        } else {
            return 'None';
        }
    }

    @AuraEnabled
    public static void setConfig(Id studyId, Id userId, String accessLevel) {
        try {
            VTR2_Treatment_Arm_Sharing_Configuration__c c = [
                    SELECT Id, VTR2_User__c, VTR2_Study__c, VTR2_Access_Level__c
                    FROM VTR2_Treatment_Arm_Sharing_Configuration__c
                    WHERE VTR2_Study__c = :studyId AND VTR2_User__c = :userId
            ];
            c.VTR2_Access_Level__c = accessLevel;
            update c;
        } catch (Exception e) {
            VTR2_Treatment_Arm_Sharing_Configuration__c c = new VTR2_Treatment_Arm_Sharing_Configuration__c();
            c.VTR2_User__c = userId;
            c.VTR2_Study__c = studyId;
            c.VTR2_Access_Level__c = accessLevel;
            insert c;
        }
    }
}