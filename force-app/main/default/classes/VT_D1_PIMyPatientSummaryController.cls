public with sharing class VT_D1_PIMyPatientSummaryController {

    @AuraEnabled
    public static String getPatientSummary(String caseId) {
        try {
            return new PatientSummary(caseId).toJson();
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static void updateCaseStatus(String caseId, String caseStatus, String reasonCode, String reasonToOther) {
        try {
            Case cas = [SELECT Id, Status, VTR3_Dropped_OtherReason_Description__c FROM Case WHERE Id = :caseId];
            cas.Status = caseStatus;
            if (caseStatus == VT_R4_ConstantsHelper_AccountContactCase.CASE_DROPPED
                    || caseStatus == VT_R4_ConstantsHelper_AccountContactCase.CASE_DROPPED_IN_FOLLOW_UP
                    || caseStatus == VT_R4_ConstantsHelper_AccountContactCase.CASE_DROPPED_COMPL_FOLLOW_UP
                    || caseStatus == VT_R4_ConstantsHelper_AccountContactCase.CASE_COMPL_DROPPED_FOLLOW_UP) {
                cas.VTD1_Reason_Code__c = reasonCode;
                cas.VTR3_Dropped_OtherReason_Description__c = reasonToOther;
            }

            update cas;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static void updateCaseWashoutOutcome(String caseId, String washoutOutcome) {
        try {
            Case cas = [SELECT Id, VTR2_WashoutRunInOutcome__c FROM Case WHERE Id = :caseId];
            cas.VTR2_WashoutRunInOutcome__c = washoutOutcome;

            update cas;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
    //SH-15999
    @AuraEnabled
    public static void updatePatientId(String caseId, String patientId, String reasonString) {
        List<Case> cas = [SELECT Id, VTD1_Subject_ID__c,VTR5_Reason_for_Patient_ID_Change__c FROM Case WHERE Id = :caseId];
        if (cas.isEmpty()) {
            throw new AuraHandledException('You do not have an access to this record.');
        }
        if (cas[0].VTD1_Subject_ID__c != null && String.isNotEmpty(reasonString)) {
            cas[0].VTR5_Reason_for_Patient_ID_Change__c = reasonString;
        }
        cas[0].VTD1_Subject_ID__c = patientId;
        try {
            update cas;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    /**
     * @author Aliaksandr Vabishchevich
     * @date 01-Dec-2020
     * @description Using in VT_D1_PIMyPatientSummary and VTR2_SCRMyPatientSummary components for update the selected field from IRT Tab
     * @param caseId - id of patient case
     * @param apiFieldName - API name of field to update
     * @param fieldValue - field new value
     * @param typeField - field type (either "text" or "date")
     */
    @AuraEnabled
    public static void updateIrtField(Id caseId, String apiFieldName, String fieldValue, String typeField) {
        try {
            Case cs = Database.query('SELECT Status, ' + apiFieldName + ' FROM Case WHERE Id = :caseId');
            if (cs == null || String.isEmpty(fieldValue)) return;
            if (cs.get(apiFieldName) != null) {
                throw new AuraHandledException(Label.VTR5_ValueWasPopulatedBefore);
            }

            if (typeField != 'date') {
                cs.put(apiFieldName, fieldValue);
            } else {
                cs.put(apiFieldName, Date.valueOf(fieldValue));
            }
            if (apiFieldName == 'VTD1_Randomized_Date1__c') {
                cs.Status = VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED;
            }
            update cs;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static void updateCaseEligibilityStatus(String caseId, String eligibilityStatus) {
        try {
            List<Case> casList = [
                    SELECT Id,
                            VTD1_Eligibility_Status__c
                    FROM Case
                    WHERE Id = :caseId AND
                    VTR2_Eligibility_Conducted_Outside_SH__c = TRUE
            ];

            if (!casList.isEmpty()) {
                casList[0].VTD1_Eligibility_Status__c = eligibilityStatus;
                update casList;
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static void createPD(String caseId, String description) {
        try {
            VTD1_Protocol_Deviation__c protocolDeviation = new VTD1_Protocol_Deviation__c();
            protocolDeviation.VTD1_Subject_ID__c = caseId;
            protocolDeviation.VTD1_Deviation_Description__c = description;

            insert protocolDeviation;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static VTD1_Actual_Visit__c createAdhocVisit(String caseId, String actualVisitType,
            String actualVisitModality, String actualVisitActivitiesLabs,
            String actualVisitProceduresOrders, String actualVisitReason) {
        try {
            VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
            actualVisit.RecordTypeId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_ACTUAL_VISIT_UNSCHEDULED;
            actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
            actualVisit.Unscheduled_Visits__c = caseId;
            actualVisit.Name = 'Ad Hoc Visit';
            actualVisit.VTD1_Unscheduled_Visit_Type__c = actualVisitType;
            actualVisit.VTR2_Modality__c = actualVisitModality;
            actualVisit.VTD1_Visit_Activities_Labs__c = actualVisitActivitiesLabs;
            actualVisit.VTD1_Visit_Procedures_Orders__c = actualVisitProceduresOrders;
            actualVisit.VTD1_Reason_for_Request__c = actualVisitReason;
            actualVisit.VTD1_Unscheduled_Visit_Duration__c = 60;

            insert actualVisit;
            return actualVisit;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static String randomizeRemote(Id caseId) {
        return VT_D1_CaseControllerRemote.randomizeRemote(caseId);
    }

    @AuraEnabled
    public static String getLink(Id caseId) {
        return VT_R3_SecurePortalFilesUploadController.generateLink(caseId, 8);
    }

    @AuraEnabled(Cacheable=true)
    public static String getCurrentUserProfile() {
        return VT_D1_HelperClass.getUserProfileName(UserInfo.getUserId());//[SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
    }

    public class PatientSummary {
        public Case cas;
        public User primaryPGUser;
        public User primaryPIUser;
        public User siteCoordinator;
        public User backupSiteCoordinator;
        public User backupPGUser;
        public User backupPIUser;
        public User currentUser;
        public String internalSubjectId;
        public String externalSubjectInputType;
        public String phone;
        public String address;
        public String washoutOutcome = '';
        public String eligibilityStatus = '';  //Added By Mona Bhutada SH-13732
        public String patientId = '';
        public Boolean isViewWashoutOutcome = true;
        public Boolean isVieweligibilityStatus = true;  //Added By Mona Bhutada SH-13732
        public Boolean isTransferInProgress = false;
        public Boolean requiresExternalSubjectId = false;
        public Boolean isPatientIdEditable = false;  // Aliaksandr Vabishchevich SH-19425
        public String isDobFull = 'Full Date of Birth';
        public List<DropdownOption> actualVisitTypeInputSelectRatioElementList;
        public List<DropdownOption> patientStatusInputSelectRatioElementList;
        public List<DropdownOption> patientWashoutOutcomeSelectRatioElementList;
        public List<DropdownOption> patientReasonCodeInputSelectRatioElementList;
        public List<DropdownOption> patientEligibilityStatusInputSelectRatioElementList;

        public PatientSummary(String caseId) {
            this.setCase(caseId);
            if (this.cas == null) {
                return;
            }
            this.populateStudyTeam();
            this.setPhone();
            this.setAddress();
            this.setWashoutOutcome();
            this.setIsViewWashoutOutcome();
            this.setIsDobFull();
            this.setEligibilityStatus();//Added By Mona Bhutada SH-13732
            this.setIsViewEligibilityStatus();//Added By Mona Bhutada SH-13732
            this.setPatientId();
            this.setIsTransferInProgress(caseId);
            this.populateActualVisitTypeOptions();
            this.populatePatientStatusOptions();
            this.populatePatientWashoutOutcomeOptions();
            this.populatePatientReasonCodeOptions();
            this.populatePatientEligibilityStatusOptions();
        }

        private void setCase(String caseId) {
            List<Case> cases = [
                    SELECT
                            Id
                            , VTD1_Primary_PG__c
                            , VTD1_PI_user__c
                            , VTD1_Backup_PI_User__c
                            , VTD1_Secondary_PG__c
                            , VTD1_Randomized_Date1__c
                            ,VTD1_Enrollment_Date__c
                            , VTR2_IsAvailableForRandomize__c
                            , VTD1_Study__r.VTR5_Requires_External_Subject_Id__c
                            , VTD1_Study__r.VTR5_External_Subject_ID_Input_Type__c
                            , VTD1_Study__r.VTR5_IRTSystem__c
                            , VTD1_Study__r.Name
                            , VTD1_Study__r.VTD2_Washout_Run_In__c
                            , VTD1_Study__r.VTR3_LTFU__c
                            , VTD1_SecureConsentID__c
                            , VTD1_Randomized_ID__c
                            , VTD1_Randomization_Status__c
                            , VTD1_Subject_ID__c
                            , VTR5_Internal_Subject_Id__c
                            , VTR2_WashoutRunInOutcome__c
                            , VTR2_SiteCoordinator__c
                            , VTR2_Backup_Site_Coordinator__c
                            , Status
                            , ContactId
                            , VTD1_Reason_Code__c
                            , VTR3_Dropped_OtherReason_Description__c
                            , Contact.Name
                            , Contact.Birthdate
                            , Contact.VTD1_MiddleName__c
                            , Contact.HealthCloudGA__Gender__c
                            , Contact.VTD1_Emergency_Contact_First_Name__c
                            , Contact.VTD1_Emergency_Contact_Last_Name__c
                            , Contact.VTD1_Emergency_Contact_Phone_Number__c
                            , toLabel(Contact.VTR2_Primary_Language__c)
                            , toLabel(Contact.VT_R5_Secondary_Preferred_Language__c)
                            , toLabel(Contact.VT_R5_Tertiary_Preferred_Language__c)
                            , VTD1_Patient_User__r.LastLoginDate
                            , VTD2_PI_Eligibility_Decision__c
                            , VTD2_TMA_Eligibility_Decision__c
                            , VTD2_TMA_Comments__c
                            , VTD2_PI_Final_Eligibility_Decision__c
                            , VTD2_PI_Comments__c
                            , VTD2_Eligibility_Assessment_Form__r.VTD2_Final_Review_Comments__c
                            , VTR2_Eligibility_Conducted_Outside_SH__c
                            , VTD1_Eligibility_Status__c
                            , VTR3_LastDoseDate__c
                            , VTD2_Study_Geography__r.VT_R3_Date_Of_Birth_Restriction__c
                            , VTR5_ConsentDate__c
                            , VTR5_First_Signant_File_Date__c
                            , VTR5_Last_Signant_File_Date__c
                            , VTR5_SubsetSafety__c
                            , VTR5_SubsetImmuno__c
                            , VTR5_Day_1_Dose__c
                            , VTR5_Day_57_Dose__c

                    FROM Case
                    WHERE Id = :caseId
            ];
            if (!cases.isEmpty()) {
                this.cas = cases[0];
                HealthCloudGA__CarePlanTemplate__c s = this.cas.VTD1_Study__r;
                this.internalSubjectId = this.cas.VTR5_Internal_Subject_Id__c;
                this.requiresExternalSubjectId = s.VTR5_Requires_External_Subject_Id__c;
                this.externalSubjectInputType = s.VTR5_External_Subject_ID_Input_Type__c;
                this.isPatientIdEditable = (s.VTR5_Requires_External_Subject_Id__c && s.VTR5_External_Subject_ID_Input_Type__c == 'Manual')
                        || (s.VTR5_IRTSystem__c == 'Signant' && this.cas.VTD1_Subject_ID__c == null);
            }
        }

        private void populateStudyTeam() {
            List<Id> userIds = new List<Id>();
            userIds.add(this.cas.VTD1_Primary_PG__c);
            userIds.add(this.cas.VTD1_PI_user__c);
            userIds.add(this.cas.VTD1_Backup_PI_User__c);
            userIds.add(this.cas.VTR2_SiteCoordinator__c);
            userIds.add(this.cas.VTD1_Secondary_PG__c);
            userIds.add(this.cas.VTR2_Backup_Site_Coordinator__c);
            userIds.add(this.cas.VTD1_Patient_User__c);

            System.debug('populateStudyTeam begin ' + UserInfo.getUserId());
            System.debug('populateStudyTeam 1 ' + userIds);
            System.debug('populateStudyTeam 2 ' + getStudyTeamUsersByUserIds(userIds));
            for (User user : getStudyTeamUsersByUserIds(userIds)) {
                if (user.Id == this.cas.VTD1_Primary_PG__c) {
                    this.primaryPGUser = user;
                } else if (user.Id == this.cas.VTD1_PI_user__c) {
                    this.primaryPIUser = user;
                } else if (user.Id == this.cas.VTD1_Secondary_PG__c) {
                    this.backupPGUser = user;
                } else if (user.Id == this.cas.VTD1_Backup_PI_User__c) {
                    this.backupPIUser = user;
                } else if (user.Id == this.cas.VTR2_Backup_Site_Coordinator__c) {
                    this.backupSiteCoordinator = user;
                } else if (user.Id == this.cas.VTR2_SiteCoordinator__c) {
                    this.siteCoordinator = user;
                } else {
                    this.currentUser = user;
                }
            }
        }

        private void setPhone() {
            List<VT_D1_Phone__c> phones = getPhonesByContactId(this.cas.ContactId);
            if (!phones.isEmpty()) {
                this.phone = phones[0].PhoneNumber__c;
            }
        }

        private void setAddress() {
            List<Address__c> addresses = getAddressesByContactId(this.cas.ContactId);
            if (!addresses.isEmpty()) {
                this.address = ((addresses[0].City__c != null) ? addresses[0].City__c + ', ' : '') + addresses[0].State__c + ', ' + addresses[0].Country__c;
            }
        }

        private void setWashoutOutcome() {
            this.washoutOutcome = this.cas.VTR2_WashoutRunInOutcome__c;
        }

        private void setPatientId() {
            this.patientId = this.cas.VTD1_Subject_ID__c;
        }

        private void setIsViewWashoutOutcome() {
            this.isViewWashoutOutcome = this.cas.VTD1_Study__r.VTD2_Washout_Run_In__c;
        }

        private void setEligibilityStatus() {
            this.isVieweligibilityStatus = this.cas.VTR2_Eligibility_Conducted_Outside_SH__c;
        }

        private void setIsViewEligibilityStatus() {
            this.eligibilityStatus = this.cas.VTD1_Eligibility_Status__c;
        }

        private void setIsDobFull() {
            if (this.cas.VTD2_Study_Geography__r.VT_R3_Date_Of_Birth_Restriction__c != null)
                this.isDobFull = this.cas.VTD2_Study_Geography__r.VT_R3_Date_Of_Birth_Restriction__c;
        }

        private void setIsTransferInProgress(Id caseId) {
            this.isTransferInProgress = !([
                    SELECT Id
                    FROM HealthCloudGA__CandidatePatient__c
                    WHERE VTR4_CaseForTransfer__c = :caseId
                    AND Conversion__c = 'Draft'
            ].isEmpty());
        }

        private void populateActualVisitTypeOptions() {
            DropdownOption dOption = new DropdownOption();
            this.actualVisitTypeInputSelectRatioElementList = dOption.getActualVisitTypeOptions();
        }

        private void populatePatientStatusOptions() {
            DropdownOption dOption = new DropdownOption();
            this.patientStatusInputSelectRatioElementList = dOption.getPatientStatusOptions(this.cas);
        }

        private void populatePatientWashoutOutcomeOptions() {
            DropdownOption dOption = new DropdownOption();
            this.patientWashoutOutcomeSelectRatioElementList = dOption.getPatientWashoutOutcomeOptions(this.cas);
        }

        private void populatePatientReasonCodeOptions() {
            DropdownOption dOption = new DropdownOption();
            this.patientReasonCodeInputSelectRatioElementList = dOption.getPatientReasonCodeOptions(this.cas);
        }

        private void populatePatientEligibilityStatusOptions() {
            DropdownOption dOption = new DropdownOption();
            this.patientEligibilityStatusInputSelectRatioElementList = dOption.getPatientEligibilityStatusOptions(this.cas);
        }

        private List<VT_D1_Phone__c> getPhonesByContactId(Id contactId) {
            return [
                    SELECT PhoneNumber__c
                    FROM VT_D1_Phone__c
                    WHERE VTD1_Contact__c = :contactId
                    AND IsPrimaryForPhone__c = TRUE
            ];
        }

        private List<Address__c> getAddressesByContactId(Id contactId) {
            return [
                    SELECT City__c, toLabel(State__c), toLabel(Country__c)
                    FROM Address__c
                    WHERE VTD1_Contact__c = :contactId
                    AND VTD1_Primary__c = TRUE
            ];
        }

        private List<User> getStudyTeamUsersByUserIds(List<Id> userIds) {
            return [
                    SELECT
                            Id,
                            Name,
                            FullPhotoUrl,
                            VTD2_Email_Formula__c,
                            VTD2_Phone_Formula__c,
                            VTD1_Profile_Name__c
                    FROM User
                    WHERE Id IN :userIds
            ];
        }

        public String toJson() {
            return JSON.serialize(this);
        }
    }

    public class DropdownOption {
        public String key;
        public Boolean value;

        private List<DropdownOption> getActualVisitTypeOptions() {
            VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
            return getInputSelectRatioList(actualVisit, 'VTD1_Unscheduled_Visit_Type__c', actualVisit.VTD1_Unscheduled_Visit_Type__c, true);
        }

        private List<DropdownOption> getPatientStatusOptions(Case cas) {
            List<DropdownOption> patientStatusInputSelectRatioElementList = new List<DropdownOption>();
            for (DropdownOption inputSelectRatioElement : getInputSelectRatioList(cas, 'Status', cas.Status, false)) {
                if (inputSelectRatioElement.key != 'Open' && inputSelectRatioElement.key != 'Closed') {
                    patientStatusInputSelectRatioElementList.add(inputSelectRatioElement);
                }
            }
            return patientStatusInputSelectRatioElementList;
        }

        private List<DropdownOption> getPatientWashoutOutcomeOptions(Case cas) {
            return getInputSelectRatioList(cas, 'VTR2_WashoutRunInOutcome__c', cas.VTR2_WashoutRunInOutcome__c, false);
        }

        private List<DropdownOption> getPatientReasonCodeOptions(Case cas) {
            return getInputSelectRatioList(cas, 'VTD1_Reason_Code__c', cas.VTD1_Reason_Code__c, true);
        }

        private List<DropdownOption> getPatientEligibilityStatusOptions(Case cas) {
            return getInputSelectRatioList(cas, 'VTD1_Eligibility_Status__c', cas.VTD1_Eligibility_Status__c, false);
        }

        private List<DropdownOption> getInputSelectRatioList(SObject sObj, String fieldName, String fieldValue, Boolean addNullKeyIfValueEmpty) {
            Boolean inputSelectRatioElementEmpty = true;
            List<DropdownOption> selectRatioElementList = new List<DropdownOption>();
            List<String> optionsList = VT_D1_CustomMultiPicklistController.getSelectOptions(sObj, fieldName);
            if (addNullKeyIfValueEmpty && inputSelectRatioElementEmpty) {
                DropdownOption inputSelectRatioElement = new DropdownOption();
                inputSelectRatioElement.key = null;
                inputSelectRatioElement.value = true;
                selectRatioElementList.add(inputSelectRatioElement);
            }
            for (String option : optionsList) {
                DropdownOption inputSelectRatioElement = new DropdownOption();
                inputSelectRatioElement.key = option;
                if (fieldValue == option) {
                    inputSelectRatioElement.value = true;
                    inputSelectRatioElementEmpty = false;
                } else {
                    inputSelectRatioElement.value = false;
                }
                selectRatioElementList.add(inputSelectRatioElement);
            }

            return selectRatioElementList;
        }
    }
}