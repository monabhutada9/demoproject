public without sharing class VT_D1_LegalHoldDeleteValidator {

    private static final String LEGAL_HOLD_FIELD = 'VTD1_Legal_Hold__c';
    public static final String ERROR_MSG = 'Cannot delete this record because Study is on Legal Hold';

    public static void validateDeletion(List<SObject> recs) {
        if (recs != null && ! recs.isEmpty()) {
            String objName = recs[0].getSObjectType().getDescribe().getName();
            if (VT_D1_FieldPaths.STUDY_PATHS.containsKey(objName)) {
                String fieldPath = VT_D1_FieldPaths.STUDY_PATHS.get(objName).substringBeforeLast('c') + 'r.' + LEGAL_HOLD_FIELD;
                Map<Id, SObject> recMap = new Map<Id, SObject>(Database.query(
                    'SELECT Id,' + fieldPath + ' FROM ' + objName + ' WHERE Id IN :recs'
                ));

                for (SObject rec : recs) {
                    SObject queriedRec = recMap.get((Id)rec.get('Id'));
                    Object legalHoldVal = VT_D1_HelperClass.getCrossObjectField(queriedRec, fieldPath);
                    if (legalHoldVal != null && (Boolean)legalHoldVal) {
                        rec.addError(ERROR_MSG);
                    }
                }
            }
        }
    }
}