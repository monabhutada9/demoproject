/**
 * Created by Alexander Komarov on 29.04.2019.
 */

@RestResource(UrlMapping='/Patient/ChatHistories/*')
global with sharing class VT_R3_RestPatientChatHistoryComments {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();
    static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());

    static Id currentHistoryId;
    static Id currentHistoryPCFId;

    @HttpGet
    global static String getComments() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;


        Id postId = request.requestURI.substringAfterLast('/');
        if (String.isBlank(postId) || !helper.isIdCorrectType(postId, 'LiveChatTranscript')) {
            response.statusCode = 400;
            cr.buildResponse(helper.forAnswerForIncorrectInput());
            return JSON.serialize(cr, true);
        }

        currentHistoryId = postId;

        ChatHistoriesCommentsResponse result = new ChatHistoriesCommentsResponse() ;
        try {
            result.comments = getHistoryComments();
            result.files = getHistoryFiles();
//            result = getCommentsAndFormResult(postId);
        } catch (Exception e) {
            response.statusCode = 500;
            cr.buildResponse('FAILURE', 'Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName());
            return JSON.serialize(cr, true);
        }
        cr.buildResponse(result);
        return JSON.serialize(cr, true);

    }

    private static List<CommentObj> getHistoryComments() {
        List<CommentObj> comments = new List<CommentObj>();
        List<LiveChatTranscript> LCTs = [
                SELECT Id, CreatedDate, Body, CaseId, Case.Subject,
                        ContactId, Contact.Name, Contact.FirstName, Contact.PhotoUrl,
                        OwnerId, Owner.Name, Owner.FirstName, Owner.LastName, Contact.VTD1_UserId__c, Status
                FROM LiveChatTranscript
                WHERE Id = :currentHistoryId AND CaseId != NULL AND Case.RecordType.Id IN :VT_D1_HelperClass.getRecordTypePCF()
                AND ContactId IN (SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId())
        ];

        currentHistoryPCFId = LCTs[0].CaseId;

        if (!LCTs.isEmpty()) {
            Id studyConciergeId = LCTs[0].OwnerId;
            String studyConciergeName = String.isBlank(LCTs[0].Owner.Name) ? '' : LCTs[0].Owner.Name;
            ConnectApi.Photo patientPhoto;
            ConnectApi.Photo scPhoto;
            if (!Test.isRunningTest()) {
                patientPhoto = ConnectApi.UserProfiles.getPhoto(null, LCTs[0].Contact.VTD1_UserId__c);
                scPhoto = ConnectApi.UserProfiles.getPhoto(null, studyConciergeId);
            }

            Pattern lctPattern = Pattern.compile('(?i)\\(\\s(?:(\\d{1,2})h\\s)?(?:(\\d{1,2})m\\s)?(?:(\\d{1,2})s\\s)?\\)\\s(\\w+)(?:\\s(\\w+))?:\\s(.*?)(?=(?:\\(\\s(?:\\d{1,2}[hms]\\s){1,3}\\)))');

            for (LiveChatTranscript lct : LCTs) {
                if (lct.Status != 'InProgress') {
                    Matcher lctMatcher = lctPattern.matcher(lct.Body + '( 0s )');
                    while (lctMatcher.find()) {
                        Datetime createdDate = lct.CreatedDate;
                        if (lctMatcher.group(1) != null) {
                            createdDate = createdDate.addHours(Integer.valueOf(lctMatcher.group(1)));
                        }
                        if (lctMatcher.group(2) != null) {
                            createdDate = createdDate.addMinutes(Integer.valueOf(lctMatcher.group(2)));
                        }
                        if (lctMatcher.group(3) != null) {
                            createdDate = createdDate.addSeconds(Integer.valueOf(lctMatcher.group(3)));
                        }

                        CommentObj comment = new CommentObj();
                        comment.commentData = createdDate.getTime();//+timeOffset;
                        comment.text = lctMatcher.group(6).replaceAll('<p.*?>', '\n').replaceAll('<.*?>', '');
                        String tempPhotoString;
                        if (lctMatcher.group(5) != null && (lct.Owner.FirstName == lctMatcher.group(4) || 'Study Concierge' == lctMatcher.group(4) + ' ' + lctMatcher.group(5))) {
                            comment.authorName = studyConciergeName;
                            tempPhotoString = scPhoto.standardEmailPhotoUrl;
                            comment.photo = tempPhotoString.contains('default_profile') ? '' : tempPhotoString;//COMMUNITY_URL + studyConcierge.SmallPhotoUrl;
                        } else if (lct.ContactId != null && lctMatcher.group(4) == lct.Contact.Name.split('(\\s|&nbsp;)')[0]) {
                            comment.authorName = lct.Contact.Name;
                            tempPhotoString = patientPhoto.standardEmailPhotoUrl;
                            comment.photo = tempPhotoString.contains('default_profile') ? '' : tempPhotoString;//COMMUNITY_URL + lct.Contact.VTD1_UserId__r.FullPhotoUrl;
                        } else {
                            comment.authorName = lctMatcher.group(4) + (lctMatcher.group(5) != null ? ' ' + lctMatcher.group(5) : '');
                            comment.photo = '';
                        }
                        comments.add(comment);
                    }
                } else {
                    CommentObj comment = new CommentObj();
                    comment.text = 'This transcript is not ready yet. Check back later.';
                    comments.add(comment);
                }
            }
        }
        return comments;
    }

    private static List<CommentFileObj> getHistoryFiles() {
        List<CommentFileObj> filesList = new List<CommentFileObj>();
        List<ContentDocument> documents = new List<ContentDocument>();
        List<ContentDocumentLink> CDLs = new List<ContentDocumentLink>();

        CDLs = [SELECT Id, ContentDocumentId, ShareType FROM ContentDocumentLink WHERE LinkedEntityId = :currentHistoryPCFId];

        if (!CDLs.isEmpty()) {
            System.debug('CDLs' + CDLs);
            List<Id> CDIdList = new List<Id>();
            for (ContentDocumentLink link : CDLs) {
                CDIdList.add(link.ContentDocumentId);
            }
            documents = [SELECT Id, Title, FileExtension, ContentSize FROM ContentDocument WHERE Id IN :CDIdList];
            for (ContentDocument d : documents) {
                filesList.add(new CommentFileObj(d));
            }
        }
        return filesList;
    }

    private class CommentObj {
        public String authorName;
        public String photo;
        public Long commentData;
        public String text;

        public CommentObj() {}
    }

    private class CommentFileObj {
        public String fileExtension;
        public String fileSize;
        public String title;
        public String fileId;


        public CommentFileObj(ContentDocument d) {
            this.fileExtension = d.FileExtension;
            this.fileSize = String.valueOf(d.ContentSize);
            this.title = d.Title;
            this.fileId = d.Id;
        }
    }

    private class ChatHistoriesCommentsResponse {
        private List<CommentObj> comments;
        private List<CommentFileObj> files;
    }
}