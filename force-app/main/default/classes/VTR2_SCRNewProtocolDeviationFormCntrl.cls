/**
 * Created by User on 19/04/05.
 */

public with sharing class VTR2_SCRNewProtocolDeviationFormCntrl {
    private static String TYPE_PI = 'Primary Investigator';
    private static List<String> PICKLIST_FIELDS = new List<String>{
            'VTD1_Site_Subject_Level__c',
            'VTD1_PD_Category__c',
            'VTD1_Deviation_Severity__c',
            'VTR2_Location__c'
    };

    @AuraEnabled
    public static String getPDData() {
        try{
            return  JSON.serialize(new PDFormValueWrapper(getDataFromDB(),VT_R2_ProtocolDeviationService.getPicklistValues(PICKLIST_FIELDS)));
        }catch (Exception e){
            System.debug('AuraHandledException = '+e.getMessage() + '\n' + e.getStackTraceString());
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
    @AuraEnabled
    public static void createNewPDDraftRecord(String pdDraftForm) {
        try{
            List<VTD1_Protocol_Deviation__c> pdfDraft = (List<VTD1_Protocol_Deviation__c>) JSON.deserializeStrict(pdDraftForm,List<VTD1_Protocol_Deviation__c>.class);
            insert pdfDraft;
        }catch (Exception e){
            System.debug('AuraHandledException = '+e.getMessage() + '\n' + e.getStackTraceString());
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

//--------------------------------------- PRIVATE -----------------------------------------//

    private static Map<Id, StudyWrapper> getDataFromDB() {
        GetMethodsWithoutSharing privateClass = new GetMethodsWithoutSharing();
        List<Study_Site_Team_Member__c> sstmList;
        if(Test.isRunningTest()){
            sstmList = privateClass.getStudySiteTeamMemberList();
        } else {
            sstmList = getStudySiteTeamMemberList();
        }
        Map<Id, Study_Site_Team_Member__c> vsIdSSTMMap = createVirtualSiteMap(sstmList);
        List<Case> caseList;
        if(Test.isRunningTest()){
            caseList = privateClass.getCaseList(vsIdSSTMMap.keySet());
        } else {
            caseList = getCaseList(vsIdSSTMMap.keySet());
        }
        Map<Id, StudyWrapper> studyWrapperMap = fillStudyWrapperMap(vsIdSSTMMap, caseList);
        return studyWrapperMap;
    }

    //fill Study => PIUser => Patients
    private static Map<Id, StudyWrapper> fillStudyWrapperMap(Map<Id, Study_Site_Team_Member__c> vsIdSSTMMap, List<Case> caseList) {
        Map<Id, StudyWrapper> studyWrapperMap = new Map<Id, StudyWrapper>();
        Map<Id, Map<Id,PatientWrapper>> virtualSiteIdPatientWrapperMap = createVirtualSiteIdPatientWrapperMap(caseList);
        for (Study_Site_Team_Member__c studySiteTeamMember : vsIdSSTMMap.values()) {
            StudyWrapper studyWrap = getStudyWrapper(studySiteTeamMember,studyWrapperMap);
            PIWrapper piWrap = getPIWrapper(studySiteTeamMember,studyWrap);
            setPatients(virtualSiteIdPatientWrapperMap,studySiteTeamMember,piWrap);
        }
        return studyWrapperMap;
    }

    private static Map<Id, Map<Id,PatientWrapper>>  createVirtualSiteIdPatientWrapperMap(List<Case> caseList) {
        Map<Id, Map<Id,PatientWrapper>> virtualSiteIdPatientWrapperMap = new Map<Id, Map<Id,PatientWrapper>>();//vsId,PatientWrapper map
        for (Case cas : caseList) {
            if(!virtualSiteIdPatientWrapperMap.containsKey(cas.VTD1_Virtual_Site__c)){
                virtualSiteIdPatientWrapperMap.put(cas.VTD1_Virtual_Site__c,new  Map<Id,PatientWrapper>());
            }
            virtualSiteIdPatientWrapperMap.get(cas.VTD1_Virtual_Site__c).put(cas.Id,new PatientWrapper(cas.Id, cas.Contact.Name, cas.VTD1_Virtual_Site__c, cas.VTD1_PI_user__c));
        }
        return virtualSiteIdPatientWrapperMap;
    }

    private static void setPatients(Map<Id, Map<Id,PatientWrapper>> virtualSiteIdPatientWrapperMap,Study_Site_Team_Member__c studySiteTeamMember, PIWrapper piWrap){
        if(virtualSiteIdPatientWrapperMap.containsKey(studySiteTeamMember.VTR2_Associated_PI__r.VTD1_VirtualSite__c)) {
            piWrap.patientMap.putAll(virtualSiteIdPatientWrapperMap.get(studySiteTeamMember.VTR2_Associated_PI__r.VTD1_VirtualSite__c));
        }
    }
    private static PIWrapper getPIWrapper(Study_Site_Team_Member__c studySiteTeamMember,StudyWrapper studyWrap){
        if (!studyWrap.piMap.containsKey(studySiteTeamMember.VTR2_Associated_PI__r.User__c)) {
            studyWrap.piMap.put(studySiteTeamMember.VTR2_Associated_PI__r.User__c,
                    new PIWrapper(studySiteTeamMember.VTR2_Associated_PI__r.User__c, studySiteTeamMember.VTR2_Associated_PI__r.User__r.Name, studySiteTeamMember.VTR2_Associated_PI__r.VTD1_VirtualSite__c, studyWrap.value));
        }
        return studyWrap.piMap.get(studySiteTeamMember.VTR2_Associated_PI__r.User__c);
    }
    private static StudyWrapper getStudyWrapper(Study_Site_Team_Member__c studySiteTeamMember, Map<Id, StudyWrapper> studyWrapperMap){
        Id studyId = studySiteTeamMember.VTR2_Associated_SCr__r.Study__c ;
        String studyName = studySiteTeamMember.VTR2_Associated_SCr__r.Study__r.Name ;
        if (!studyWrapperMap.containsKey(studyId)) {
            studyWrapperMap.put(studyId, new StudyWrapper(studyId, studyName));
        }
        return studyWrapperMap.get(studyId);
    }
    private static Map<Id,Study_Site_Team_Member__c> createVirtualSiteMap(List<Study_Site_Team_Member__c> sstmList) {
        Map<Id,Study_Site_Team_Member__c> virtualSiteMap = new Map<Id,Study_Site_Team_Member__c>();
        for (Study_Site_Team_Member__c sstm : sstmList) {
            if(sstm.VTR2_Associated_PI__r.VTD1_VirtualSite__c != null) {
                virtualSiteMap.put(sstm.VTR2_Associated_PI__r.VTD1_VirtualSite__c, sstm);
            }
        }
        return virtualSiteMap;
    }
//--------------------------------------- SOQL -----------------------------------------//
    private without sharing class  GetMethodsWithoutSharing {
        public List<Study_Site_Team_Member__c> getStudySiteTeamMemberList() {
            return [
                    SELECT ID
                            , VTR2_Associated_PI__r.User__c
                            , VTR2_Associated_PI__r.User__r.Name
                            , VTR2_Associated_PI__r.VTD1_VirtualSite__c
                            , VTR2_Associated_SCr__r.Study__c
                            , VTR2_Associated_SCr__r.Study__r.Name
                    FROM Study_Site_Team_Member__c
                    WHERE VTR2_Associated_SCr__r.User__c = :UserInfo.getUserId()
                    AND VTR2_Associated_PI__r.VTD1_Type__c = :TYPE_PI
            ];
        }
        public List<Case> getCaseList(Set<Id> virtualSiteSet) {
            return [
                    SELECT Id
                            , VTD1_PI_user__c
                            , VTD1_Virtual_Site__c
                            , Contact.Name
                    FROM Case
                    WHERE VTD1_Virtual_Site__c IN :virtualSiteSet
            ];
        }
    }
    public static List<Study_Site_Team_Member__c>  getStudySiteTeamMemberList() {
        return [
                SELECT ID
                        , VTR2_Associated_PI__r.User__c
                        , VTR2_Associated_PI__r.User__r.Name
                        , VTR2_Associated_PI__r.VTD1_VirtualSite__c
                        , VTR2_Associated_SCr__r.Study__c
                        , VTR2_Associated_SCr__r.Study__r.Name
                FROM Study_Site_Team_Member__c
                WHERE VTR2_Associated_SCr__r.User__c = :UserInfo.getUserId()
                AND VTR2_Associated_PI__r.VTD1_Type__c = :TYPE_PI
        ];
    }

    public static List<Case> getCaseList(Set<Id> virtualSiteSet) {
        return [
                SELECT Id
                        , VTD1_PI_user__c
                        , VTD1_Virtual_Site__c
                        , Contact.Name
                FROM Case
                WHERE VTD1_Virtual_Site__c IN :virtualSiteSet
        ];
    }

//-------------------------------------- WRAPPERS --------------------------------------------------//
    class StudyWrapper {
        public Id value { get; set; }//study Id
        public String label { get; set; }//study name
        public Map<Id, PIWrapper> piMap { get; set; }//user Id, PIWrapper

        public StudyWrapper(Id studyId, String studyName) {
            this.value = studyId;
            this.label = studyName;
            piMap = new Map<Id, PIWrapper>();
        }
    }
    class PIWrapper {
        public Id value { get; set; }//user Id
        public String label { get; set; }//user name
        public Id virtualSiteId { get; set; }
        public Id studyId { get; set; }
        public Map<Id, PatientWrapper> patientMap { get; set; }//case Id, PatientWrapper

        public PIWrapper(Id userId, String userName, Id virtualSiteId, Id studyId) {
            this.value = userId;
            this.label = userName;
            this.virtualSiteId = virtualSiteId;
            this.studyId = studyId;
            this.patientMap = new Map<Id, PatientWrapper>();
        }
    }
    class PatientWrapper {
        public Id value { get; set; } //case Id
        public String label { get; set; }//VTD1_PI_user__r.Name on Case
        public String virtualSiteId { get; set; }
        public String userId { get; set; }//VTD1_PI_user__c on Case

        public PatientWrapper(Id caseId, String patientName, Id virtualSiteId, Id userId) {
            this.value = caseId;
            this.label = patientName;
            this.virtualSiteId = virtualSiteId;
            this.userId = userId;
        }
    }

    public class PDFormValueWrapper {
        public  Map<Id, StudyWrapper> records { get;set; }
        public Map<String, List<VT_R2_ProtocolDeviationService.PicklistValueWrapper>> picklistValue { get; set; }

        public PDFormValueWrapper( Map<Id, StudyWrapper> records, Map<String, List<VT_R2_ProtocolDeviationService.PicklistValueWrapper>> picklistValue) {
            this.records = records;
            this.picklistValue = picklistValue;
        }
    }


}