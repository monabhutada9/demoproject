/**

* @author: N.Arbatskiy
* @date: 24-June-20
* @description: Class for processing VT_D1_AbstractAction actions.
* Checks their context and executes them with appropriate method.
**/


public with sharing class VT_R5_eCoaQueueAction {
    public static Set<String> actionDigests = new Set<String>(); // capture hashes of actions to dedupe

    public static void processAction(List<VT_R5_eCoaAbstractAction> actions) {
        if (actions.size() > 0) {
            List<VTD1_Queue_Action__c> insertActions = new List<VTD1_Queue_Action__c>();
            VT_R5_eCoaCreateSubjectQueueable subjectCreator;
            //then it iterates through List<VT_D1_AbstractAction> actions
            //and checks if they're future/batch
            for (VT_R5_eCoaAbstractAction action : actions) {
                String actionDigest = getDigest(action);
                if (!actionDigests.contains(actionDigest)) {
                    actionDigests.add(actionDigest);
                    Boolean addToQueue = false;
                    if (VT_R4_PatientConversion.patientConversionInProgress) {
                        addToQueue = true;
                    } else if (System.Limits.getFutureCalls() < System.Limits.getLimitFutureCalls() && !System.isBatch() && !System.isFuture() && !Test.isRunningTest()) {
                        action.executeFuture();
                    } else if (action instanceof VT_R5_eCoaCreateSubject) {
                        if (subjectCreator == null) subjectCreator = new VT_R5_eCoaCreateSubjectQueueable();
                        subjectCreator.addAction((VT_R5_eCoaCreateSubject) action);
                        if (Limits.getQueueableJobs() == Limits.getLimitQueueableJobs()) {
                            addToQueue = true; // if no queueables left, will try with platform event. Add to custom object queue too just in case PE fails.
                        }
                    } else {
                        addToQueue = true;
                    }
                    if (addToQueue) {
                        VTD1_Queue_Action__c queueAction = action.returnForMassAddToQueue();
                        if (queueAction != null) {insertActions.add(queueAction);}
                    }
                }
            }
            if (insertActions.size() > 0) {
                insert insertActions;
            }
            if (subjectCreator != null) {
                subjectCreator.enqueue();
            }
        }
    }

    public static String getDigest(VT_R5_eCoaAbstractAction action) {
        String actionString = action.toString();
        Blob md5Blob = Crypto.generateDigest('MD5', Blob.valueOf(actionString));
        String md5Hex = EncodingUtil.convertToHex(md5Blob);
        return md5Hex;
    }
}