/**
 * @author: Alexander Komarov
 * @date: 22.10.2020
 * @description:
 */


public without sharing class VT_R5_MobileAuth extends VT_R5_MobileRestResponse implements VT_R5_MobileLibrary.Routable {
    public static final Set<String> AVAILABLE_PROFILES = new Set<String>{
            VT_R4_ConstantsHelper_Profiles.SCR_PROFILE_NAME,
            VT_R4_ConstantsHelper_Profiles.PI_PROFILE_NAME,
            VT_R4_ConstantsHelper_Profiles.PATIENT_PROFILE_NAME,
            VT_R4_ConstantsHelper_Profiles.CAREGIVER_PROFILE_NAME
    };
    private static final Set<String> PT_CG_PROFILES = new Set<String>{
            VT_R4_ConstantsHelper_Profiles.PATIENT_PROFILE_NAME,
            VT_R4_ConstantsHelper_Profiles.CAREGIVER_PROFILE_NAME
    };
    private static final String SF_OLD_PASSWORD_INVALID = 'INVALID_OLD_PASSWORD';
    private static final String SF_NEW_PASSWORD_INVALID = 'INVALID_NEW_PASSWORD';
    private static final String SF_INVALID_SESSION_ID = 'INVALID_SESSION_ID';
    private static final String SF_INVALID_CROSS_REFERENCE_KEY = 'INVALID_CROSS_REFERENCE_KEY';
    private static final String ERROR_MESSAGE = 'ERROR';

    private static final String ACTIVATE_ACTION = 'activate';
    private static final String LOGIN_ACTION = 'login';
    private static final String FORGOT_PASSWORD_ACTION = 'forgot-password';
    private static final String CHANGE_PASSWORD_ACTION = 'change-password';
    private static final String SET_PASSWORD_ACTION = 'set-password';
    private static final String RESEND_CODE = 'resend-code';
    private static final String LOGINFLOW = 'loginflow';

    private static String currentProfileName;

    private static VT_R5_MobileLoginFlow.LoginFlowResponse loginFlowResponse;

    public List<String> getMapping() {
        return new List<String>{
                '/mobile/{version}/login-dictionary',
                '/mobile/{version}/support-centers-info',
                '/mobile/{version}/auth/{action}'
        };
    }

    public Map<String, String> getMinimumVersions() {
        return new Map<String, String>{
                'v1' => '5.5'
        };
    }
    public void initService() {
    }

    public VT_R5_MobileRestResponse versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        String requestAction = parameters.get('action');
        currentProfileName = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        if (this.request.requestURI.contains('login-dictionary')) {
                            return get_v1_dictionary();
                        }
                        if (this.request.requestURI.contains('support-centers-info')) {
                            return get_v1_supportCenterInfo();
                        }
                        if (requestAction.equals(LOGINFLOW) && (AVAILABLE_PROFILES.contains(currentProfileName) || Test.isRunningTest())) {
                            return get_v1_loginflow();
                        }
                    }
                }
            }
            when 'POST' {
                switch on requestVersion {
                    when 'v1' {
                        if (((requestAction.equals(RESEND_CODE) || requestAction.equals(CHANGE_PASSWORD_ACTION)) && !AVAILABLE_PROFILES.contains(currentProfileName) && !Test.isRunningTest())) {
                            return null;
                        }
                        return post_v1(requestAction);
                    }
                }
            }
        }
        return null;
    }

    private VT_R5_MobileRestResponse get_v1_supportCenterInfo() {
        Map<String, String> result = new Map<String, String>();
        for (VTR5_IQVIASupportCentersPhone__mdt record : [SELECT Id,VTR5_Country__c, VTR5_Phones__c FROM VTR5_IQVIASupportCentersPhone__mdt WHERE VTR5_Active__c = TRUE]) {
            if (String.isNotEmpty(record.VTR5_Country__c) && String.isNotEmpty(record.VTR5_Phones__c)) {
                result.put(record.VTR5_Country__c, record.VTR5_Phones__c);
            }
        }
        this.buildResponseWOAI(result);
        return this;
    }

    private VT_R5_MobileRestResponse get_v1_loginflow() {
        if (!this.request.params.containsKey('deviceUniqueCode')) {
            this.sendResponse(400, 'deviceUniqueCode missing');
        } else {
            parsedParams.deviceUniqueCode = request.params.get('deviceUniqueCode');
            loginFlowResponse = VT_R5_MobileLoginFlow.getLoginFlowState();
        }
        return this;
    }
    private VT_R5_MobileAuth get_v1_dictionary() {
        String language = 'en_US';
        if (this.request.params.containsKey('lang')) {
            language = this.request.params.get('lang');
        }
        Map<String, String> dictionaryData = new Map<String, String>();
        dictionaryData = VT_R3_CustomLabelsRenderPageController.getLabelsString(language, '%27%2525MobileLogin%2525%27');
        this.buildResponseWOAI(dictionaryData);
        return this;
    }
    private VT_R5_MobileAuth post_v1(String requestAction) {
        parseParams(this.request.requestBody.toString(), this.request.params);
        if (requestAction == null) {
            return this;
        } else if (requestAction.equals(LOGIN_ACTION) || requestAction.equals(FORGOT_PASSWORD_ACTION) || requestAction.equals(SET_PASSWORD_ACTION)) {
            processAction(requestAction);
        } else if (requestAction.equals(CHANGE_PASSWORD_ACTION)) {
            processChangePassword();
        } else if (requestAction.equals(ACTIVATE_ACTION)) {
            processActivate();
        } else if (requestAction.equals(RESEND_CODE)) {
            VT_R5_MobileLoginFlow.resendCode();
            success = true;
        } else if (requestAction.equals(LOGINFLOW) && !this.isParamsMissed()) {
            loginFlowResponse = VT_R5_MobileLoginFlow.postAction();
        }
        if (success != null && !success) sendResponse(500);
        return this;
    }

    private void processActivate() {
        User activationUser = [SELECT Id, Profile.Name FROM User WHERE Id = :parsedParams.userID];
        LoginFlowRequest_Remote loginReq = new LoginFlowRequest_Remote(
                VT_RemoteCall.METHOD_POST,
                '?activation=true',
                JSON.serialize(parsedParams),
                activationUser.Profile.Name
        );
        System.debug('parsedParams' + parsedParams);
        VT_R5_MobileLoginFlow.LoginFlowResponse resp = (VT_R5_MobileLoginFlow.LoginFlowResponse) loginReq.execute();
        if (resp != null) {
            System.debug(resp);
            if (loginReq.successResponse && (resp.newPassword != null && resp.username != null)) {
                activationUN = resp.username;
                activationPW = resp.newPassword;
                post_v1(LOGIN_ACTION);
            } else {
                loginFlowResponse = resp;
            }
            System.debug('loginFlowResponse' + loginFlowResponse);
        } else {
            this.sendResponse(500, 'internal senver error 544'); //log this
        }

    }
    @TestVisible
    private Boolean isParamsMissed() {
        if (VT_R5_MobileAuth.parsedParams.stage == null || VT_R5_MobileAuth.parsedParams.deviceUniqueCode == null) {
            this.sendResponse(400, 'stage or deviceUniqueCode missing');
            return true;
        } else if (VT_R5_MobileAuth.parsedParams.stage.equals(VT_R5_MobileLoginFlow.VERIFICATION_CODE_STAGE) && VT_R5_MobileAuth.parsedParams.verificationCode == null) {
            this.sendResponse(400, 'verificationCode is missing');
            return true;
        } else if (VT_R5_MobileAuth.parsedParams.stage.equals(VT_R5_MobileLoginFlow.PRIVACY_NOTICES_STAGE) && VT_R5_MobileAuth.parsedParams.isPrivacyNoticesAccepted == null) {
            this.sendResponse(400, 'isPrivacyNoticesAccepted is missing');
            return true;
        } else if (VT_R5_MobileAuth.parsedParams.stage.equals(VT_R5_MobileLoginFlow.AUTHORIZATION_STAGE) && VT_R5_MobileAuth.parsedParams.isAuthorizationAccepted == null) {
            this.sendResponse(400, 'isAuthorizationAccepted is missing');
            return true;
        } else if (VT_R5_MobileAuth.parsedParams.stage.equals(VT_R5_MobileLoginFlow.INITIAL_PROFILE_SETUP_STAGE) && (VT_R5_MobileAuth.parsedParams.firstName == null
                || VT_R5_MobileAuth.parsedParams.lastName == null || VT_R5_MobileAuth.parsedParams.phoneNumber == null || VT_R5_MobileAuth.parsedParams.preferredContactMethod == null)) {
            this.sendResponse(400, 'initial profile data is missing');
            return true;
        } else {
            return false;
        }
    }

    private void processChangePassword() {
        success = false;
        Profile userProfile = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
        HttpResponse resp = performSOAPAction(userProfile.Name, getSoapChangePasswordRequestBody(parsedParams.oldPassword, parsedParams.newPassword));
        System.debug(resp.getBody());
        if (resp.getStatusCode() == 200) {
            message = Label.VT_R4_ChangedPassword;
            success = true;
        } else if (resp.getStatusCode() == 500) {
            String sfExceptionCode = getValueFromSoapResponse(resp.getBody(), 'sf:exceptionCode');
            if (sfExceptionCode.equals(SF_OLD_PASSWORD_INVALID) || sfExceptionCode.equals(SF_NEW_PASSWORD_INVALID) || sfExceptionCode.equals(SF_INVALID_SESSION_ID)) {
                errorCode = sfExceptionCode;
                message = getValueFromSoapResponse(resp.getBody(), 'sf:exceptionMessage');
            }
        }
        if (!success && message == null) message = 'UNKNOWN_EXCEPTION';

    }


    private String sessionId;
    private String userId;
    private Integer attemptsRemaining;
    private String errorCode;
    private String message { get; private set; }
    private Boolean loginFlowFinished;
    private Boolean success;
    private String userFirstName;
    private String userLastName;
    private String userLanguage;
    private Long passwordLastChangedDate;
    private String userUsername;
    private String userProfile;
    private Boolean passwordExpired;

    private static void parseParams(String requestBody, Map<String, String> params) {
        parsedParams = (RequestParams) JSON.deserialize(requestBody, RequestParams.class);
        if (parsedParams.userID == null) parsedParams.userID = UserInfo.getUserId();
        if (params != null && params.containsKey('activation')) parsedParams.isActivationProcess = Boolean.valueOf(params.get('activation'));
        if (params != null && params.containsKey('deviceUniqueCode')) parsedParams.deviceUniqueCode = params.get('deviceUniqueCode');
        if (activationUN != null) parsedParams.username = activationUN;
        if (activationUN != null) parsedParams.password = activationPW;

    }
    private static String activationUN;
    private static String activationPW;

    public static RequestParams parsedParams = new RequestParams();

    public class RequestParams {
        String username;
        String password;
        public String stage;
        public String verificationCode;
        public String deviceUniqueCode;
        public Boolean isPrivacyNoticesAccepted;
        public Boolean isAuthorizationAccepted;
        public String newPassword;
        String oldPassword;
        public String userID;
        public String firstName;
        public String lastName;
        public String middleName;
        public String phoneNumber;
        public String state;
        public String gender;
        public Long dateOfBirth;
        public String birthDate;
        public String zipCode;
        public String preferredContactMethod;
        public String language;
        public String secondaryLanguage;
        public String tertiaryLanguage;
        public Boolean isActivationProcess;
    }

    private static User candidateActionUser;
    private VT_R5_MobileAuth processAction(String action) {
        List<User> userList = [
                SELECT Id,
                        HIPAA_Accepted__c,
                        VTD1_Privacy_Notice_Last_Accepted__c,
                        VTR3_Verified_Mobile_Devices__c,
                        FirstName,
                        LastName,
                        Username,
                        LanguageLocaleKey,
                        VTD1_Filled_Out_Patient_Profile__c,
                        LastPasswordChangeDate,
                        VTR4_Is_Activated__c,
                        First_Log_In__c,
                        Contact.VTR2_Primary_Language__c,
                        Profile.Name
                FROM User
                WHERE Username = :parsedParams.username
                AND Profile.Name IN :AVAILABLE_PROFILES
        ];
        if (userList.size() > 0) {
            candidateActionUser = userList[0];
            HttpResponse resp;
            if (action.equals(LOGIN_ACTION)) {
                resp = performSOAPAction(candidateActionUser.Profile.Name, getSoapLoginRequestBody(parsedParams.username, parsedParams.password));
                processLoginAction(resp);
            } else if (action.equals(FORGOT_PASSWORD_ACTION)) {
                resp = performSOAPAction(candidateActionUser.Profile.Name, getSoapResetPasswordRequestBody(candidateActionUser.Id));
                processForgotPasswordAction(resp);
            } else if (action.equals(SET_PASSWORD_ACTION)) {
                resp = performSOAPAction(candidateActionUser.Profile.Name, getSoapLoginRequestBody(parsedParams.username, parsedParams.oldPassword));
                processSetPasswordAction(resp);
                if (success) {
                    resp = performSOAPAction(candidateActionUser.Profile.Name, getSoapLoginRequestBody(parsedParams.username, parsedParams.newPassword));
                    processLoginAction(resp);
                }
            }
        } else {
            errorCode = 'ACTION_FAILED';
            success = false;
            message = action.equals(LOGIN_ACTION) ? System.Label.VTR3_UsernameOrPassIncorrect : 'INVALID_USERNAME';
        }
        return this;
    }

    private void processSetPasswordAction(HttpResponse loginResp) {
        success = false;
        sessionId = getValueFromSoapResponse(loginResp.getBody(), 'sessionId');
        Boolean isPasswordExpired = Boolean.valueOf(getValueFromSoapResponse(loginResp.getBody(), 'passwordExpired'));
        if (sessionId != null && isPasswordExpired) {
            try {
                HttpResponse resp = performSOAPAction(candidateActionUser.Profile.Name, getSoapSetPasswordRequestBody(parsedParams.newPassword, sessionId, candidateActionUser.Id));
                Integer statusCode = resp.getStatusCode() == null ? 500 : resp.getStatusCode();
                if (statusCode == 200) {
                    success = true;
                } else {
                    message = getValueFromSoapResponse(resp.getBody(), 'faultstring');
                }
            } catch (Exception e) {
                this.buildResponseWOAI(e);
            }
        } else {
            message = 'OLD_PASSWORD_NOT_EXPIRED';
            this.sendResponse(500, 'OLD_PASSWORD_NOT_EXPIRED');
        }
    }

    private void processForgotPasswordAction(HttpResponse resetPasswordResp) {
        success = false;
        if (resetPasswordResp.getStatusCode() == 200) {
            success = true;
        } else if (resetPasswordResp.getStatusCode() == 500) {
            String sfExceptionCode = getValueFromSoapResponse(resetPasswordResp.getBody(), 'sf:exceptionCode');
            if (sfExceptionCode.equals(SF_INVALID_CROSS_REFERENCE_KEY)) {
                errorCode = sfExceptionCode;
                message = getValueFromSoapResponse(resetPasswordResp.getBody(), 'sf:exceptionMessage');
            }
        }
    }

    private void processLoginAction(HttpResponse loginResp) {
        sessionId = getValueFromSoapResponse(loginResp.getBody(), 'sessionId');
        if (sessionId == null) {
            errorCode = 'LOGIN_FAILED';
            message = System.Label.VTR3_UsernameOrPassIncorrect;
            attemptsRemaining = VT_D1_LockedUserHelper.checkForLockedUser(parsedParams.username);
        } else {
            passwordExpired = Boolean.valueOf(getValueFromSoapResponse(loginResp.getBody(), 'passwordExpired'));
            if (passwordExpired) {
                passwordLastChangedDate = candidateActionUser.LastPasswordChangeDate.getTime();
                userUsername = candidateActionUser.Username;
            }
            loginFlowFinished = checkFinishedLoginFlow(candidateActionUser, parsedParams.deviceUniqueCode);
            userId = getValueFromSoapResponse(loginResp.getBody(), 'userId');
            userFirstName = candidateActionUser.FirstName;
            userLastName = candidateActionUser.LastName;
            userProfile = candidateActionUser.Profile.Name;
            if (candidateActionUser.Contact != null) {
                userLanguage = candidateActionUser.Contact.VTR2_Primary_Language__c;
            }
        }
    }

    private static HttpResponse performSOAPAction(String profileName, String body) {
        HttpRequest request = new HttpRequest();
        request.setEndpoint(getCommunityURL(profileName) + '/services/Soap/u/47.0');
        request.setTimeout(60000);
        request.setMethod('POST');
        request.setHeader('SOAPAction', 'login');
        request.setHeader('Accept', 'text/xml');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setBody(body);
        return new Http().send(request);
    }

    private static String getCommunityURL(String profileName) {
        Map<String, String> sandboxPostfixMapping = new Map<String, String>{
                VT_R4_ConstantsHelper_Profiles.SCR_PROFILE_NAME => 'scr',
                VT_R4_ConstantsHelper_Profiles.PI_PROFILE_NAME => 'pi',
                VT_R4_ConstantsHelper_Profiles.PATIENT_PROFILE_NAME => 'patient',
                VT_R4_ConstantsHelper_Profiles.CAREGIVER_PROFILE_NAME => 'patient'
        };

        Map<String, String> prodMapping = new Map<String, String>{
                VT_R4_ConstantsHelper_Profiles.SCR_PROFILE_NAME => 'virtualtrials-scr.solutions.iqvia.com',
                VT_R4_ConstantsHelper_Profiles.PI_PROFILE_NAME => 'virtualtrials-pi.solutions.iqvia.com',
                VT_R4_ConstantsHelper_Profiles.PATIENT_PROFILE_NAME => 'virtualtrials-patient.solutions.iqvia.com',
                VT_R4_ConstantsHelper_Profiles.CAREGIVER_PROFILE_NAME => 'virtualtrials-patient.solutions.iqvia.com'
        };

        List<Organization> organizations = [SELECT Id, IsSandbox FROM Organization];
        String postfix = '/';
        String mainUrl = '';
        if (!organizations.isEmpty()) {
            if (organizations[0].IsSandbox) {
                mainUrl = [SELECT Domain FROM Domain WHERE HttpsOption = 'Community'][0].Domain;
                postfix += sandboxPostfixMapping.get(profileName);
            } else {
                mainUrl = prodMapping.get(profileName);
            }
        }
        return 'https://' + mainUrl + postfix;
    }

    @TestVisible
    private static Boolean checkFinishedLoginFlow(User user, String deviceUniqueCode) {
        return user.VTD1_Privacy_Notice_Last_Accepted__c != null
                && user.VTR3_Verified_Mobile_Devices__c != null
                && (user.VTR3_Verified_Mobile_Devices__c.contains(deviceUniqueCode) || user.VTR3_Verified_Mobile_Devices__c.contains('AUTOTEST'))
                && (PT_CG_PROFILES.contains(user.Profile.Name) ?
                (user.HIPAA_Accepted__c && user.VTD1_Filled_Out_Patient_Profile__c) : true);
    }

    public class LoginFlowRequest_Remote extends VT_RemoteCall {
        private String requestBody;
        private Boolean successResponse;

        public LoginFlowRequest_Remote(String method, String pathURL, String requestBody, String profileName) {
            this.httpMethod = method;
            this.headersMap.putAll(new Map<String, String>{
                    'Authorization' => 'Bearer ' + VT_D1_HelperClass.getSessionIdForGuest(),
                    'Content-Type' => 'application/json'
            });
            this.endPointURL = getCommunityURL(profileName) + '/services/apexrest/mobile/v1/auth/loginflow';
            this.endPointURL += pathURL;
            this.requestBody = requestBody;
        }

        public override Type getType() {
            return LoginFlowRequest_Remote.class;
        }

        public override String buildRequestBody() {
            return requestBody;
        }

        public override Object parseResponse(String responseBody) {
            successResponse = super.successResponse;
            System.debug('responseBody' + responseBody);
            if (successResponse) {
                return (VT_R5_MobileLoginFlow.LoginFlowResponse) JSON.deserialize(responseBody, VT_R5_MobileLoginFlow.LoginFlowResponse.class);
            } else {
                return null;
            }
        }
    }
    private static String getValueFromSoapResponse(String responseBody, String keyField) {
        String fieldOpen = '<' + keyField + '>';
        String fieldClose = '</' + keyField + '>';
        if (responseBody.contains(fieldOpen)) {
            return responseBody.split(fieldOpen)[1].split(fieldClose)[0];
        }
        return null;
    }
    public static String getSoapLoginRequestBody(String username, String password) {
        if (password.contains('&')) password = password.replaceAll('&', '&amp;');
        return '<?xml version="1.0" encoding="utf-8"?>' +
                '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">' +
                '<soapenv:Header>' +
                '<urn:LoginScopeHeader>' +
                '<urn:organizationId>' + UserInfo.getOrganizationId() + '</urn:organizationId>' +
                '</urn:LoginScopeHeader>' +
                '</soapenv:Header>' +
                '<soapenv:Body>' +
                '<urn:login>' +
                '<urn:username>' + username + '</urn:username>' +
                '<urn:password>' + password + '</urn:password>' +
                '</urn:login>' +
                '</soapenv:Body>' +
                '</soapenv:Envelope>';
    }
    public static String getSoapSetPasswordRequestBody(String newPassword, String sessionId, String userId) {
        if (newPassword.contains('&')) newPassword = newPassword.replaceAll('&', '&amp;');
        return '<?xml version="1.0" encoding="utf-8"?>' +
                '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">' +
                '    <soapenv:Header>' +
                '        <urn:SessionHeader>' +
                '            <urn:sessionId>' + sessionId + '</urn:sessionId>' +
                '        </urn:SessionHeader>' +
                '    </soapenv:Header>' +
                '    <soapenv:Body>' +
                '        <urn:setPassword>' +
                '            <urn:userId>' + userId + '</urn:userId>' +
                '            <urn:password>' + newPassword + '</urn:password>' +
                '        </urn:setPassword>' +
                '    </soapenv:Body>' +
                '</soapenv:Envelope>';
    }
    public static String getSoapChangePasswordRequestBody(String oldPassword, String newPassword) {
        if (newPassword.contains('&')) newPassword = newPassword.replaceAll('&', '&amp;');
        if (oldPassword.contains('&')) oldPassword = oldPassword.replaceAll('&', '&amp;');
        return '<?xml version="1.0" encoding="utf-8"?>' +
                '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">' +
                '    <soapenv:Header>' +
                '        <urn:SessionHeader>' +
                '            <urn:sessionId>' + UserInfo.getSessionId() + '</urn:sessionId>' +
                '        </urn:SessionHeader>' +
                '    </soapenv:Header>' +
                '    <soapenv:Body>' +
                '        <urn:changeOwnPassword>' +
                '            <urn:oldPassword>' + oldPassword + '</urn:oldPassword>' +
                '            <urn:newPassword>' + newPassword + '</urn:newPassword>' +
                '        </urn:changeOwnPassword>' +
                '    </soapenv:Body>' +
                '</soapenv:Envelope>';
    }
    public static String getSoapResetPasswordRequestBody(String userId) {
        return '<?xml version="1.0" encoding="utf-8"?>' +
                '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">' +
                '<soapenv:Header>' +
                '<urn:EmailHeader>' +
                '<urn:triggerAutoResponseEmail>false</urn:triggerAutoResponseEmail>' +
                '<urn:triggerOtherEmail>false</urn:triggerOtherEmail>' +
                '<urn:triggerUserEmail>true</urn:triggerUserEmail>' +
                '</urn:EmailHeader>' +
                '<urn:SessionHeader>' +
                '<urn:sessionId>' + VT_D1_HelperClass.getSessionIdForGuest() + '</urn:sessionId>' +
                '</urn:SessionHeader>' +
                '</soapenv:Header>' +
                '<soapenv:Body>' +
                '<urn:resetPassword>' +
                '<urn:userId>' + userId + '</urn:userId>' +
                '</urn:resetPassword>' +
                '</soapenv:Body>' +
                '</soapenv:Envelope>';
    }
    public override void sendResponse() {
        RestContext.response.addHeader('Content-Type', 'application/json');
        if (loginFlowResponse != null) {
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(loginFlowResponse, true));
        } else {
            if (this.errors != null && this.errors.isEmpty()) this.errors = null;
            if (this.request != null) this.request = null;
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(this, true));
        }
    }
}