/***********************************************************************
* @author              Akanksha Singh <akanksha.singh@quintiles.com>
* @date                07-Oct-2020


* @group               SH-17543
* @group-content       http://jira.quintiles.net/browse/SH-17543

* @description         Add SSTM as GroupMember in queue of related Virtual Site
*/

public without sharing class VT_R5_QueueBasedonSSTM {




// Remove all groupMembers and add based on current SSTM    
    @future
    public static void assignMemberToGroup(List<Id> siteIds) {
        try{
            List<QueueSObject> queueSObjectcreation = new List<QueueSObject>();
            Map<Id,Id> siteAndGroupMap = new Map<Id,Id>();


            List<GroupMember> listOfmember = new List<GroupMember>();
            List<GroupMember> removablemember = new List<GroupMember>();

            
            for(Virtual_Site__c siteRec : [ SELECT Id,
                                            VTR5_CRA_Queue__c
                                            FROM Virtual_Site__c

                                            WHERE Id IN: siteIds
                                            WITH SECURITY_ENFORCED]) {


                siteAndGroupMap.put(siteRec.Id, siteRec.VTR5_CRA_Queue__c);                                  
            }

    // Fetch all GroupMember of Queue and delete.
            for(GroupMember grpMem :[SELECT Id,
                                    GroupId,
                                    UserOrGroupId
                                    FROM GroupMember
                                    WHERE GroupId IN: siteAndGroupMap.values()]) {
                removablemember.add(grpMem);
            }
            
            if(removablemember.size() > 0) {


                delete removablemember;


            }

        // Fetch all current sstm to add in Queue as Group Memebr
            for(Study_Site_Team_Member__c sstmVal : [Select Id,
                                                    VTD1_SiteID__c,
                                                    VTD1_SiteID__r.VTR5_CRA_Queue__c,
                                                    VTR5_Associated_CRA__r.User__c
                                                    FROM Study_Site_Team_Member__c
                                                    WHERE VTD1_SiteID__c IN:siteIds 



                                                    AND VTR5_Associated_CRA__c != NULL 
                                                    WITH SECURITY_ENFORCED ]) {
                GroupMember groupRec = new GroupMember();
                groupRec.UserOrGroupId = sstmVal.VTR5_Associated_CRA__r.User__c;
                groupRec.GroupId = sstmVal.VTD1_SiteID__r.VTR5_CRA_Queue__c;
                listOfmember.add(groupRec); 
            }
            
            if(listOfmember.size() > 0) {
                insert listOfmember;



            }

            for(Id VirtualSiteID : siteAndGroupMap.keyset()) {	
                    QueueSObject craTaskObj = new QueueSObject(SobjectType='VTR5_CRA_Task__c', QueueId = siteAndGroupMap.get(VirtualSiteID));
                    queueSObjectcreation.add(craTaskObj); 
                    QueueSObject docTaskObj = new QueueSObject(SobjectType='VTD1_Document__c', QueueId = siteAndGroupMap.get(VirtualSiteID));
                    queueSObjectcreation.add(docTaskObj);    
            }    

            if (queueSObjectcreation.size() > 0) {


                insert queueSObjectcreation;


            }
        }
        Catch(Exception ex){
            System.debug('Exception Meesage'+ex.getMessage()+'Line No:'+ex.getLineNumber());
            ErrorLogUtility.logException(ex, null, 'VT_R5_QueueBasedonSSTM');
        }
    } 





}