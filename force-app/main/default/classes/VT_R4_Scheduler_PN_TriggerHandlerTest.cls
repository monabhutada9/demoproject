/**
 * Created by Belkovskii Denis on 5/15/2020.
 */
@isTest 
public with sharing class VT_R4_Scheduler_PN_TriggerHandlerTest {

    public static void testNotificationsTrigger(){
        VTR4_ExternalScheduler__c testSetting = new VTR4_ExternalScheduler__c(
                VTR4_PushNotificationIsActive__c = true
        );
        insert testSetting;
        List<VTD1_Actual_Visit__c> actualVisits = new List<VTD1_Actual_Visit__c>();

        Case patientCase = new Case();
        insert patientCase;
        for(Integer i=0; i<10; i++){
            VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c(
                    VTR5_PatientCaseId__c = patientCase.Id
            );
            actualVisits.add(actualVisit);
        }
        insert actualVisits;
        List<Video_Conference__c> testVideoConferences = new List<Video_Conference__c>();
        for (Integer i=0; i<10; i++){
            Video_Conference__c vc = new Video_Conference__c();
            vc.VTD1_Actual_Visit__c = actualVisits[i].Id;
            testVideoConferences.add(vc);
        }
        insert testVideoConferences;
        List<VTR4_Scheduler_PushNotifications__c> testSchedulerPushNotifications = new List<VTR4_Scheduler_PushNotifications__c>();
        for (Integer i=0; i<10; i++){
            VTR4_Scheduler_PushNotifications__c testPushNotification = new VTR4_Scheduler_PushNotifications__c(
                    VTR4_ReadyToStart__c = true,
                    VTR4_Video_Conference__c = testVideoConferences[i].Id);
            testSchedulerPushNotifications.add(testPushNotification);
        }
        insert testSchedulerPushNotifications;
        Integer numberOnSchedulersPNAfterInsert = [SELECT COUNT() FROM VTR4_Scheduler_PushNotifications__c];
        System.assertEquals(10, numberOnSchedulersPNAfterInsert);
    }
}