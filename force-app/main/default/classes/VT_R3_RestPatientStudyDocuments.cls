/**
 * Created by Alexander Komarov on 16.10.2019.
 *     DEPRECATED
 */

@RestResource(UrlMapping='/Patient/StudyDocuments/*')
global with sharing class VT_R3_RestPatientStudyDocuments {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();
    private static Set<String> IMAGE_FILE_EXTENSION = new Set<String>{
            'jpg', 'gif', 'png', 'jpeg', 'bmp'
    };
    private static String BIG_PREVIEW = 'big-thumbnail';
    private static String MIDDLE_PREVIEW = 'thumbnail';
    private static String SMALL_PREVIEW = 'tiny-thumbnail';

    static Map<String, Id> docRT = VT_D1_HelperClass.getRTMap().get(VT_R4_ConstantsHelper_Documents.SOBJECT_DOC_CONTAINER);
    static List<Id> medicalRecordTypes = new List<Id>{
            docRT.get('Medical_Record_Archived'),
            docRT.get('Medical_Record_Release_Form'),
            docRT.get('Medical_Record_Rejected'),
            docRT.get('VTD1_Medical_Record')
    };

    static List<Id> visitDocumentRecordTypes = new List<Id>{
            docRT.get('VTR3_VisitDocument')
    };

    @HttpGet
    global static String getMethod() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        Map<String, String> paramsMap = request.params;

        Case currentCase = VT_D1_PatientCaregiverBound.getCaseForUser();
        Id caseId = currentCase.Id;

        String temp = request.requestURI.substringAfterLast('/StudyDocuments/');
        if (String.isBlank(temp)) {
            Integer queryOffset = 0; //DEFAULT VALUE
            Integer queryLimit = 40; //DEFAULT VALUE
            if (request.params.containsKey('offset')) {
                queryOffset = Integer.valueOf(paramsMap.get('offset'));
            }

            if (request.params.containsKey('limit')) {
                queryLimit = Integer.valueOf(paramsMap.get('limit'));
            }

            List<VTD1_Document__c> docs = getDocuments(caseId, queryOffset, queryLimit);

            MobileListDocumentWrapper result = new MobileListDocumentWrapper();
            MobileDocumentWrapperInfo mdwi = new MobileDocumentWrapperInfo();
            mdwi.totalDocs = countDocuments(caseId);
            mdwi.queryOffset = queryOffset;
            mdwi.queryLimit = queryLimit;

            result.metaInfo = mdwi;
            result.documentsList = new List<MobileDocumentWrapper>();

            System.debug('CPU BEFORE' + Limits.getCpuTime());

            for (VTD1_Document__c doc : docs) {
                MobileDocumentWrapper mobileDocumentWrapper = new MobileDocumentWrapper(doc);
                result.documentsList.add(mobileDocumentWrapper);
            }

            try {
                if (!Test.isRunningTest() && !result.documentsList.isEmpty()) {
                    helper.addPreviewsWithBatchCallout(result.documentsList);
                }
            } catch (Exception e) {
                System.debug(e.getStackTraceString() + e.getMessage());
            }


            addFileSizes(result.documentsList);

            System.debug('CPU AFTER' + Limits.getCpuTime());

            cr.serviceResponse = result;

            return JSON.serialize(cr, true);
        } else if (temp.startsWith(VTD1_Document__c.getSObjectType().getDescribe().getKeyPrefix())) {
            Id docId = temp;
            MobileDocumentWrapper result = new MobileDocumentWrapper(getDocumentById(docId));
            result.addDownloadLink();
            try {
                if (!Test.isRunningTest()) {
                    result.filePreviewBase64 = getPreview(result.contentDocumentId, BIG_PREVIEW);
                }
            } catch (Exception e) {
                result.filePreviewStatus = 'NotAvailable';
            }

            addFileSizes(new List<MobileDocumentWrapper>{
                    result
            });
            cr.buildResponse(result);
            return JSON.serialize(cr, true);
        } else if (temp.contains('Visits')) {
            List<VTD1_Actual_Visit__c> visits = VT_R3_AddStudyDocumentsController.getVisits(caseId);
            StudyDocumentsResponse result = new StudyDocumentsResponse();
            result.visits = convertVisits(visits);
            result.caseId = caseId;
            result.networkId = Network.getNetworkId();
            cr.buildResponse(result);
            return JSON.serialize(cr, true);
        }
        response.statusCode = 400;
        cr.buildResponse(helper.forAnswerForIncorrectInput());
        return JSON.serialize(cr, true);

    }

    private static void addFileSizes(List<MobileDocumentWrapper> income) {
        Set<Id> contentDocumentsIds = new Set<Id>();
        Map<Id, Integer> contentDocsToFileSize = new Map<Id, Integer>();
        for (MobileDocumentWrapper mdw : income) {
            if (String.isNotBlank(mdw.contentDocumentId)) {
                contentDocumentsIds.add(mdw.contentDocumentId);
            }
        }
        for (ContentVersion cv : [SELECT Id, ContentSize, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN :contentDocumentsIds AND IsLatest = TRUE ORDER BY CreatedDate DESC]) {
            contentDocsToFileSize.put(cv.ContentDocumentId, cv.ContentSize);
        }
        for (MobileDocumentWrapper mdw : income) {
            if (String.isNotBlank(mdw.contentDocumentId)) {
                mdw.fileSize = contentDocsToFileSize.get(mdw.contentDocumentId);
            }
        }

    }


    @HttpPost
    global static void processDocument(String contentVersionId, String comment, String nickname, String visitId) {
        Id currentCaseId;
        List<Contact> contacts = [SELECT Id, VTD1_Clinical_Study_Membership__c FROM Contact WHERE VTD1_UserId__c = :UserInfo.getUserId()];
        if (!contacts.isEmpty()) {
            currentCaseId = contacts[0].VTD1_Clinical_Study_Membership__c;
        }

        Id documentId = null;
        Id contentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersionId].ContentDocumentId;
        List <ContentDocumentLink> cdls = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId = :contentDocumentId];
        String documentPrefix = VTD1_Document__c.getSObjectType().getDescribe().getKeyPrefix();
        for (ContentDocumentLink cdl : cdls) {
            if (String.valueOf(cdl.LinkedEntityId).startsWith(documentPrefix)) {
                documentId = cdl.LinkedEntityId;
                break;
            }
        }

        VTD1_Document__c document = getDocumentById(documentId);
        document.VTR3_Upload_File_Completed__c = true;
        document.VTR3_CreatedProfile__c = true;
        document.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT;
        document.VTR3_Category__c = 'Uncategorized';
        document.VTD1_Nickname__c = nickname;
        document.VTD1_Comment__c = comment;
        if (String.isNotEmpty(visitId)) {
            document.VTR3_Associated_Visit__c = visitId;
        }
        document.VTD1_Flagged_for_Deletion__c = false;
        update document;
        VT_D2_MedicalRecordsCertifyDocsHelper.certifyDoc(currentCaseId, document.Id);

        MobileDocumentWrapper docToReturn = new MobileDocumentWrapper(document);
        docToReturn.filePreviewStatus = 'NotAvailable';

        docToReturn.addDownloadLink(contentVersionId);
        cr.serviceResponse = docToReturn;

        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(cr, true));
    }

    @HttpPatch
    global static String updateDocument(String comment, String nickname) {
        RestResponse response = RestContext.response;
        RestRequest request = RestContext.request;

        System.debug('LIMITS 1 ' + Limits.getCpuTime() + ' ' + Limits.getHeapSize());
        String temp = request.requestURI.substringAfterLast('/StudyDocuments/');

        System.debug('LIMITS 2 ' + Limits.getCpuTime() + ' ' + Limits.getHeapSize());

        VTD1_Document__c docToUpdate = getDocumentById(temp);
        System.debug('LIMITS 3 ' + Limits.getCpuTime() + ' ' + Limits.getHeapSize());

        String contentDocumentId = docToUpdate.ContentDocumentLinks.isEmpty() ? '' : docToUpdate.ContentDocumentLinks[0].ContentDocumentId;

        String base64BigPreview;
        try {
            if (String.isNotBlank(contentDocumentId) && !Test.isRunningTest()) {
                base64BigPreview = getPreview(contentDocumentId, BIG_PREVIEW);
            }
        } catch (Exception e) {}

        if (comment != null) docToUpdate.VTD1_Comment__c = comment;
        if (nickname != null) docToUpdate.VTD1_Nickname__c = nickname;
        update docToUpdate;
        System.debug('LIMITS 4 ' + Limits.getCpuTime() + ' ' + Limits.getHeapSize());

        MobileDocumentWrapper docToReturn = new MobileDocumentWrapper(getDocumentById(temp));
        System.debug('LIMITS 5 ' + Limits.getCpuTime() + ' ' + Limits.getHeapSize());

        docToReturn.addDownloadLink();
        if (base64BigPreview != null) {
            docToReturn.filePreviewStatus = 'Available';
            docToReturn.filePreviewBase64 = base64BigPreview;
        } else {
            docToReturn.filePreviewStatus = 'NotAvailable';
        }
        cr.buildResponse(docToReturn);
        System.debug('LIMITS 6 ' + Limits.getCpuTime() + ' ' + Limits.getHeapSize());

        return JSON.serialize(cr, true);

    }

    private static List<VisitObj> convertVisits(List<VTD1_Actual_Visit__c> visits) {
        List<VisitObj> result = new List<VisitObj>();
        for (VTD1_Actual_Visit__c v : visits) {
            result.add(new VisitObj(v));
        }
        return result;
    }

    public static Integer countDocuments(Id caseId) {
        return [
                SELECT count()
                FROM VTD1_Document__c
                WHERE VTD1_Clinical_Study_Membership__c = :caseId AND
                (((RecordTypeId IN :medicalRecordTypes) AND (NOT (VTD1_Does_file_needs_deletion__c = 'Yes' AND VTD1_Deletion_Reason__c = 'Not Relevant'))) OR (RecordTypeId IN :visitDocumentRecordTypes))
        ];
    }

    public static String getPreview(String fileId, String previewFormat) {
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setHeader('Authorization', 'Bearer ' + VT_D1_HelperClass.getSessionIdForGuest());
        req.setEndpoint(Url.getOrgDomainUrl().toExternalForm() + '/services/data/v45.0/connect/communities/' + Network.getNetworkId() + '/files/' + fileId + '/previews/' + previewFormat);
        System.debug('REQUEST: ' + req);
        // Send Tooling API request
        Http http = new Http();
        HttpResponse res = http.send(req);
        if (res.getStatusCode() == 200) {
            System.debug('RESPONSE: ' + res.getStatus() + ' ' + res.getBody());
            Map<String, Object> resSer = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
            String filePreviewStatus = String.valueOf(resSer.get('status'));
            if (filePreviewStatus.equals('NotAvailable') || filePreviewStatus.equals('InProgress')) {
                return null;
            } else if (filePreviewStatus.equals('Available')) {
                Object firstURL = ((List<Object>) resSer.get('previewUrls')).get(0);
                Map<String, Object> lastObj = (Map<String, Object>) firstURL;
                System.debug(lastObj.get('previewUrl'));
                String respLink = String.valueOf(lastObj.get('previewUrl'));
                return helper.getBase64Preview2(Url.getOrgDomainUrl().toExternalForm() + respLink);
            }
        }
        return null;

    }

    public static List<VTD1_Document__c> getDocuments(Id caseId, Integer qOffset, Integer qLimit) {
        String queryStr = 'SELECT Id, ' +
                'Name, ' +
                'VTD1_FileNames__c, ' +
                'VTD1_Nickname__c, ' +
                'VTD1_Comment__c, ' +
                'VTD1_Version__c, ' +
                'VTD1_Status__c, ' +
                'toLabel(VTD1_Status__c) translatedStatus, ' +
                'VTD1_Does_file_needs_deletion__c, ' +
                'VTD1_Deletion_Date__c, ' +
                'VTD1_PI_Comment__c, ' +
                'CreatedBy.FirstName, ' +
                'CreatedBy.LastName, ' +
                'CreatedDate, ' +
                'VTR3_Associated_Visit__c, ' +
                'VTR3_Associated_Visit__r.Name, ' +
                'VTR3_Category__c, ' +
                'toLabel(VTR3_Category__c) translatedCategory, ' +
                '(SELECT Id, ContentDocumentId FROM ContentDocumentLinks ORDER BY SystemModstamp DESC LIMIT 1) ' +
                'FROM VTD1_Document__c ' +
                'WHERE VTD1_Clinical_Study_Membership__c = :caseId AND ' +
                '(((RecordTypeId IN :medicalRecordTypes) AND (NOT (VTD1_Does_file_needs_deletion__c = \'Yes\' AND VTD1_Deletion_Reason__c = \'Not Relevant\'))) ' +
                'OR (RecordTypeId IN :visitDocumentRecordTypes)) ' +
                'ORDER BY CreatedDate DESC';

        if (qLimit != null) {
            queryStr += ' LIMIT ' + qLimit;
        }

        if (qOffset != null) {
            queryStr += ' OFFSET ' + qOffset;
        }

        List<VTD1_Document__c> result = Database.query(queryStr);

        return result;
    }

    public static VTD1_Document__c getDocumentById(Id docId) {
        return [
                SELECT
                        Id,
                        Name,
                        VTD1_FileNames__c,
                        VTD1_Nickname__c,
                        VTD1_Comment__c,
                        VTD1_Version__c,
                        VTD1_Status__c,
                        toLabel(VTD1_Status__c) translatedStatus,
                        CreatedBy.FirstName,
                        CreatedBy.LastName,
                        CreatedDate,
                        VTR3_Associated_Visit__c,
                        VTR3_Associated_Visit__r.Name,
                        toLabel(VTR3_Category__c) translatedCategory,
                        VTR3_Upload_File_Completed__c,
                        VTR3_CreatedProfile__c,
                        RecordTypeId,
                        VTD1_Flagged_for_Deletion__c,
                        VTR3_Category__c, (SELECT Id, ContentDocumentId FROM ContentDocumentLinks ORDER BY SystemModstamp DESC LIMIT 1)
                FROM VTD1_Document__c
                WHERE Id = :docId
        ];
    }

    public static List<MobileDocumentWrapper> addDownloadLinks(List<MobileDocumentWrapper> income) {
        Map<Id, MobileDocumentWrapper> contentDocumentIdsToWrappers = new Map<Id, MobileDocumentWrapper>();
        for (MobileDocumentWrapper mdw : income) {
            if (String.isNotBlank(mdw.contentDocumentId)) {
                contentDocumentIdsToWrappers.put(mdw.contentDocumentId, mdw);
            }
        }
        //TODO: logic can be better
        for (ContentVersion cv : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN :contentDocumentIdsToWrappers.keySet() ORDER BY CreatedDate ASC]) {
            VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper current = contentDocumentIdsToWrappers.get(cv.ContentDocumentId);
            current.fileDownloadLink = System.Url.getSalesforceBaseUrl().toExternalForm() + VT_D1_HelperClass.getSandboxPrefix() + '/services/data/v46.0/sobjects/ContentVersion/' + cv.Id + '/VersionData';
        }
        return contentDocumentIdsToWrappers.values();
    }

    private class StudyDocumentsResponse {
        private List<VisitObj> visits;
        private String networkId;
        private String caseId;
    }

    private class VisitObj {
        private String visitName;
        private Long visitDate;
        private String id;

        public VisitObj(VTD1_Actual_Visit__c actualVisit) {
            this.id = actualVisit.Id;
            this.visitName = actualVisit.Name;
            this.visitDate = actualVisit.VTD1_Date_for_Timeline__c == null ? null : actualVisit.VTD1_Date_for_Timeline__c.getTime();
        }
    }

    private class MobileListDocumentWrapper {
        private List<MobileDocumentWrapper> documentsList;
        private MobileDocumentWrapperInfo metaInfo;
    }

    private class MobileDocumentWrapperInfo {
        private Integer totalDocs;
        private Integer queryOffset;
        private Integer queryLimit;
    }

    public class MobileDocumentWrapper {
        private String documentId;
        private String fileName;
        private String fileExtension;
        public String fileDownloadLink;
        private String fileCategory;
        private String fileCategoryTranslated;
        private String fileNickname;
        private String fileComment;
        private String fileStatus;
        private String fileUserFirstName;
        private String fileUserLastName;
        private String fileAssociatedVisitId;
        private String fileAssociatedVisitName;
        private Long fileUploadDate;
        public String filePreviewBase64;
        public String filePreviewStatus;
        public String contentDocumentId;
        private String lastContentVersionId;
        private Boolean isImage;
        private Integer fileSize;
        private String fileType;

        public MobileDocumentWrapper(VTD1_Document__c document) {
            this.documentId = document.Id;
            this.fileName = document.VTD1_FileNames__c;
            this.fileExtension = fileName == null ? '' : fileName.substringAfterLast('.');
            System.debug('HERE' + document);
            System.debug('HERE' + document.ContentDocumentLinks);
            this.contentDocumentId = document.ContentDocumentLinks.isEmpty() ? '' : document.ContentDocumentLinks[0].ContentDocumentId;
            this.fileCategory = document.VTR3_Category__c;
            this.fileCategoryTranslated = String.valueOf(document.get('translatedCategory'));
            this.fileNickname = document.VTD1_Nickname__c;
            this.fileComment = document.VTD1_Comment__c;
            this.fileStatus = String.valueOf(document.get('translatedStatus'));
            this.fileUserFirstName = document.CreatedBy.FirstName;
            this.fileUserLastName = document.CreatedBy.LastName;
            this.fileAssociatedVisitId = document.VTR3_Associated_Visit__c;
            this.fileAssociatedVisitName = document.VTR3_Associated_Visit__r.Name;
            this.fileUploadDate = document.CreatedDate.getTime();
            this.isImage = IMAGE_FILE_EXTENSION.contains(fileExtension);
            this.fileType = VT_R3_RestHelper.FILE_CATEGORIES_MAPPING.get(fileExtension);
        }


        public void addDownloadLink(String contentVersionId) {
            this.fileDownloadLink = System.Url.getSalesforceBaseUrl().toExternalForm() + VT_D1_HelperClass.getSandboxPrefix() + '/services/data/v46.0/sobjects/ContentVersion/' + contentVersionId + '/VersionData';
        }

        public void addDownloadLink() {
            List<ContentVersion> versions = [SELECT Id FROM ContentVersion WHERE ContentDocumentId = :this.contentDocumentId ORDER BY CreatedDate DESC];
            String lastContentVersionId;
            if (!versions.isEmpty()) {
                lastContentVersionId = versions.get(0).Id;
                this.fileDownloadLink = System.Url.getSalesforceBaseUrl().toExternalForm() + VT_D1_HelperClass.getSandboxPrefix() + '/services/data/v46.0/sobjects/ContentVersion/' + lastContentVersionId + '/VersionData';
            }
        }
    }
}