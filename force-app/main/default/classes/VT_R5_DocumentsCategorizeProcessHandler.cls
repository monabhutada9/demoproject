/**
 * Created by Alexey Mezentsev on 11/19/2020.
 * PB Name: "Documents - Categorize" (VTR3_Documents_Categorize) Replaced by Apex SH-16193
 * PB Description: "Create tasks to review the document for PG/SCR, update a category / 21.10.19 SH-4838
 * 28.10.19 FUT 24h for PG/SCR added from PB Document - Medical Records PG/PI Approvers and Notify PT/CG/change a category
 */

public without sharing class VT_R5_DocumentsCategorizeProcessHandler extends Handler {
    private static final String PENDING_CERTIFICATION = VT_R4_ConstantsHelper_Documents.DOCUMENT_PENDING_CERTIFICATION;
    private static final String CERTIFIED = VT_R4_ConstantsHelper_Documents.DOCUMENT_CERTIFIED;
    private static final Set<String> CATEGORIES_FOR_PENDING_CERTIFICATION = new Set<String>{
            'Visit Document',
            'Source Document',
            'Lab Requisition Form'
    };
    private static final Set<String> CATEGORIES_FOR_CERTIFIED = new Set<String>{
            'Visit Document',
            'Source Document',
            'Lab Requisition Form',
            'Signature Page'
    };
    private static List<VT_D2_TNCatalogTasks.ParamsHolder> userTasks = new List<VT_D2_TNCatalogTasks.ParamsHolder>();
    private static List<VTD1_Document__c> documentForCreateTasks = new List<VTD1_Document__c>();
    private static Id fileUploadUserId;

    protected override void onBeforeInsert(Handler.TriggerContext context) {
        for (VTD1_Document__c doc : (List<VTD1_Document__c>) context.newList) {
            if (isDocumentUploadedByPTorCG(doc, null)) {
                doc.VTD1_Status__c = PENDING_CERTIFICATION;
                documentForCreateTasks.add(doc);
            }
            if (isVisitDocCertifiedOrCCF(doc, null)) {
                doc.VTD1_Status__c = CERTIFIED;
            }
        }
    }

    protected override void onAfterInsert(Handler.TriggerContext context) {
        List<VT_R5_GenericScheduledActionsService.ScheduledActionData> scheduledActions = new List<VT_R5_GenericScheduledActionsService.ScheduledActionData>();
        List<VTD1_Document__c> documentsToUpdate = new List<VTD1_Document__c>();
        if (!documentForCreateTasks.isEmpty()) {
            for (VTD1_Document__c doc : documentForCreateTasks) {
                createTasksToCompleteCertification(doc.Id);
            }
            documentForCreateTasks.clear();
        }
        for (VTD1_Document__c doc : (List<VTD1_Document__c>) context.newList) {
            if (isCategoryVisitDocOrSourceDocOrSoCForm(doc, null)) {
                documentsToUpdate.add(new VTD1_Document__c(Id = doc.Id, VTD1_Status__c = PENDING_CERTIFICATION));
            }
            if (isCreatedByUploadUser(doc, null)) {
                documentsToUpdate.add(new VTD1_Document__c(Id = doc.Id, VTD1_Status__c = CERTIFIED));
            }
            if (isFollowUpTaskForPGorSCR(doc, null)) {
                scheduledActions.add(new VT_R5_GenericScheduledActionsService.ScheduledActionData(
                        doc.CreatedDate.addDays(1), doc.Id, 'Document_FollowUpTask')
                );
            }
        }
        if (!scheduledActions.isEmpty()) {
            new VT_R5_GenericScheduledActionsEvent(scheduledActions).publish();
        }
        if (!documentsToUpdate.isEmpty()) {
            update documentsToUpdate;
        }
        generateTasks();
    }

    protected override void onBeforeUpdate(Handler.TriggerContext context) {
        for (VTD1_Document__c doc : (List<VTD1_Document__c>) context.newList) {
            VTD1_Document__c oldDoc = (VTD1_Document__c) context.oldMap.get(doc.Id);
            if (isCategoryChanged(doc, oldDoc)) {
                doc.VTR3_Category__c = doc.VTR3_Category_add__c;
            }
            if (isVisitDocCertifiedOrCCF(doc, oldDoc) || isCreatedByUploadUser(doc, oldDoc)) {
                doc.VTD1_Status__c = CERTIFIED;
            }
            if (isDocumentUploadedByPTorCG(doc, oldDoc)) {
                doc.VTD1_Status__c = PENDING_CERTIFICATION;
                createTasksToCompleteCertification(doc.Id);
            }
            if (isCategoryVisitDocOrSourceDocOrSoCForm(doc, oldDoc)) {
                doc.VTD1_Status__c = PENDING_CERTIFICATION;
            }
        }
    }

    protected override void onAfterUpdate(Handler.TriggerContext context) {
        List<VT_R5_GenericScheduledActionsService.ScheduledActionData> scheduledActions = new List<VT_R5_GenericScheduledActionsService.ScheduledActionData>();
        for (VTD1_Document__c doc : (List<VTD1_Document__c>) context.newList) {
            VTD1_Document__c oldDoc = (VTD1_Document__c) context.oldMap.get(doc.Id);
            if (isFollowUpTaskForPGorSCR(doc, oldDoc)) {
                scheduledActions.add(new VT_R5_GenericScheduledActionsService.ScheduledActionData(
                        doc.CreatedDate.addDays(1), doc.Id, 'Document_FollowUpTask')
                );
            }
        }
        if (!scheduledActions.isEmpty()) {
            new VT_R5_GenericScheduledActionsEvent(scheduledActions).publish();
        }
        generateTasks();
    }

    private static Boolean isCategoryChanged(VTD1_Document__c doc, VTD1_Document__c oldDoc) {
        return doc.VTR3_CreatedProfile__c && doc.VTR3_Category_add__c != oldDoc.VTR3_Category_add__c;
    }

    private static Boolean isDocumentUploadedByPTorCG(VTD1_Document__c doc, VTD1_Document__c oldDoc) {
        return doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT
                && doc.VTR3_Category__c == VT_R4_ConstantsHelper_Documents.DOCUMENT_CATEGORY_UNCATEGORIZED
                && doc.VTR3_CreatedProfile__c && doc.VTR3_Upload_File_Completed__c
                && doc.VTD1_Status__c != CERTIFIED
                && (oldDoc == null
                || !oldDoc.VTR3_Upload_File_Completed__c
                || !oldDoc.VTR3_CreatedProfile__c
                || doc.VTR3_Category__c != oldDoc.VTR3_Category__c);
    }

    private static Boolean isCategoryVisitDocOrSourceDocOrSoCForm(VTD1_Document__c doc, VTD1_Document__c oldDoc) {
        return doc.VTD1_Status__c != CERTIFIED
                && !doc.VTR3_CreatedProfile__c && CATEGORIES_FOR_PENDING_CERTIFICATION.contains(doc.VTR3_Category__c)
                && (oldDoc == null
                || oldDoc.VTR3_CreatedProfile__c
                || doc.VTR3_Category__c != oldDoc.VTR3_Category__c)
                && doc.CreatedById != getFileUploadUserId();
    }

    private static Boolean isVisitDocCertifiedOrCCF(VTD1_Document__c doc, VTD1_Document__c oldDoc) {
        return doc.VTD1_Files_have_not_been_edited__c
                && (doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT
                || doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON)
                && (oldDoc == null
                || !oldDoc.VTD1_Files_have_not_been_edited__c
                || doc.RecordTypeId != oldDoc.RecordTypeId);
    }

    private static Boolean isCreatedByUploadUser(VTD1_Document__c doc, VTD1_Document__c oldDoc) {
        return CATEGORIES_FOR_CERTIFIED.contains(doc.VTR3_Category__c)
                && (oldDoc == null || doc.VTR3_Category__c != oldDoc.VTR3_Category__c)
                && doc.CreatedById == getFileUploadUserId();
    }
    
    private static Boolean isFollowUpTaskForPGorSCR(VTD1_Document__c doc, VTD1_Document__c oldDoc) {
        return (doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD
                || doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT)
                && doc.VTR3_Upload_File_Completed__c
                && !doc.VTD1_Files_have_not_been_edited__c
                && doc.VTR3_CreatedProfile__c
                && (oldDoc == null || !oldDoc.VTR3_Upload_File_Completed__c || !oldDoc.VTR3_CreatedProfile__c);
    }

    private static void createTasksToCompleteCertification(Id docId) {
        userTasks.add(new VT_D2_TNCatalogTasks.ParamsHolder(docId, 'T534'));
        userTasks.add(new VT_D2_TNCatalogTasks.ParamsHolder(docId, 'T533'));
    }

    private static void generateTasks() {
        if (!userTasks.isEmpty()) {
            VT_D2_TNCatalogTasks.generateTask(userTasks);
            userTasks.clear();
        }
    }

    private static Id getFileUploadUserId() {
        if (fileUploadUserId == null && !Test.isRunningTest()) {
            fileUploadUserId = [
                    SELECT Id
                    FROM User
                    WHERE Username = :VTD1_FileUploadUserCredentials__c.getInstance().username__c
            ].Id;
        }
        return fileUploadUserId;
    }
}