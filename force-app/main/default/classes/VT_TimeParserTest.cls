@IsTest
public class VT_TimeParserTest {
    @IsTest
    public static void runTests() {
        constructTest();
        parseTest();
        toMinutesTest();
        toTimeTest();
        exceptionsTest();
    }

    private static void constructTest() {
        System.assertEquals('00:00', (new VT_TimeParser()).toString());

        System.assertEquals('10:00', (new VT_TimeParser(10)).toString());
        System.assertEquals('10:20', (new VT_TimeParser(10, 20)).toString());
        System.assertEquals('10:20', (new VT_TimeParser(10, 20, 33, 333)).toString());

        System.runAs((User) new DomainObjects.User_t().setTimezone(TimeZone.getTimeZone('America/New_York')).persist()) {
            Time localTimeObj = Time.newInstance(12, 34, 0, 0);
            System.assertEquals('12:34', (new VT_TimeParser(localTimeObj)).toString());

            Date localDateObj = Date.newInstance(2020, 8, 10); // 16:34 for America/New_York (GMT-4)
            Datetime localDatetimeObj = Datetime.newInstance(localDateObj, localTimeObj);
            System.assertEquals('16:34', (new VT_TimeParser(localDatetimeObj, true)).toString());
            System.assertEquals('16:34', (new VT_TimeParser(localDatetimeObj).toString()));
            System.assertEquals('12:34', (new VT_TimeParser(localDatetimeObj, false)).toString());
        }
    }

    private static void parseTest() {
        System.assertEquals('07:30', (new VT_TimeParser('07:30')).toString());
        System.assertEquals('07:30', (new VT_TimeParser('7:30')).toString());
        System.assertEquals('07:30', (new VT_TimeParser('7:30 AM')).toString());
        System.assertEquals('19:30', (new VT_TimeParser('7:30 PM')).toString());

        System.assertEquals('23:30', (new VT_TimeParser('23:30')).toString());
        System.assertEquals('11:30', (new VT_TimeParser('11:30 AM')).toString());
        System.assertEquals('23:30', (new VT_TimeParser('11:30 PM')).toString());
        System.assertEquals('00:30', (new VT_TimeParser('12:30 AM')).toString());

        System.assertEquals('10:30', VT_TimeParser.parseTime('10:30').toString());
        System.assertEquals('10:30', VT_TimeParser.parseTime('10:30 AM').toString());
        System.assertEquals('22:30', VT_TimeParser.parseTime('10:30 PM').toString());
    }

    private static void toMinutesTest() {
        System.assertEquals(30, VT_TimeParser.toMinutes(new VT_TimeParser('12:30 AM')));
        System.assertEquals(30, VT_TimeParser.toMinutes(new VT_TimeParser('0:30')));
        System.assertEquals(30, VT_TimeParser.toMinutes(Time.newInstance(0, 30, 0, 0)));
        System.assertEquals(30, new VT_TimeParser(0, 30).toMinutes());
    }

    private static void toTimeTest() {
        Time timeObj = Time.newInstance(12, 34, 56, 789);

        VT_TimeParser timeParserObj = new VT_TimeParser(12, 34, 56, 789);
        System.assertEquals(timeObj, VT_TimeParser.toTime(timeParserObj));

        VT_TimeParser timeParserObjParsed = new VT_TimeParser('12:34:56.789');
        System.assertEquals(timeObj, timeParserObjParsed.toTime());

        System.assertEquals(timeObj, timeParserObj.toTime());
    }

    private static void exceptionsTest() {
        try {
            new VT_TimeParser(24);
            System.assert(false);
        } catch (VT_TimeParser.InvalidTimeRangeException e) { }

        try {
            new VT_TimeParser('24:00');
            System.assert(false);
        } catch (VT_TimeParser.InvalidTimeRangeException e) { }

        try {
            new VT_TimeParser(12, 65);
            System.assert(false);
        } catch (VT_TimeParser.InvalidTimeRangeException e) { }

        try {
            new VT_TimeParser('12:65');
            System.assert(false);
        } catch (VT_TimeParser.InvalidTimeRangeException e) { }

        try {
            new VT_TimeParser(12, 56, 74);
            System.assert(false);
        } catch (VT_TimeParser.InvalidTimeRangeException e) { }

        try {
            new VT_TimeParser(12, 56, 47, 1000);
            System.assert(false);
        } catch (VT_TimeParser.InvalidTimeRangeException e) { }

        try {
            new VT_TimeParser('invalid');
            System.assert(false);
        } catch (VT_TimeParser.InvalidTimeFormatException e) { }
    }
}