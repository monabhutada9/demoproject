/**
* @author: Carl Judge
* @date: 01-Mar-19
* @description: Update and create case assignment history
**/

public without sharing class VT_R2_CaseAssignmentHistoryUpdater extends VT_R3_AbstractPublishedAction{
    private List<Case> cases;

    public VT_R2_CaseAssignmentHistoryUpdater(List<Case> cases) {
        this.cases = cases;
    }

    public override Type getType() {
        return VT_R2_CaseAssignmentHistoryUpdater.class;
    }

    public override void execute() {
        updateHistory(this.cases);
    }

    public static void updateHistory(List<Case> cases) {
        updateOldHistories(cases);
        createNewHistories(cases);
    }

    private static void updateOldHistories(List<Case> cases) {
        List<VTD1_Assignment_History__c> oldHistories = [
            SELECT Id
            FROM VTD1_Assignment_History__c
            WHERE Assignment_History_Status__c = :VT_R4_ConstantsHelper_AccountContactCase.ASSIGNMENT_HISTORY_CURRENT
            AND VTD1_ClinicalStudyMembership__c IN :cases
        ];
        for (VTD1_Assignment_History__c item : oldHistories) {
            item.Assignment_History_Status__c = VT_R4_ConstantsHelper_AccountContactCase.ASSIGNMENT_HISTORY_TRANSFERRED;
        }
        update oldHistories;
    }

    private static void createNewHistories(List<Case> cases) {
        Map<Id, Id> pgCapacityMap = getPgCapacityMap(cases);
        Map<Id, Map<Id, Id>> stmMap = getStmMap(cases);

        List<VTD1_Assignment_History__c> newHistories = new List<VTD1_Assignment_History__c>();
        for (Case item : cases) {
            newHistories.add(new VTD1_Assignment_History__c(
                VTD1_ClinicalStudyMembership__c = item.Id,
                VTR2_PatientGuideCapacity__c = pgCapacityMap.get(item.VTD1_Primary_PG__c),
                Assignment_History_Status__c = VT_R4_ConstantsHelper_AccountContactCase.ASSIGNMENT_HISTORY_CURRENT,
                VTR2_StudyTeamMember__c = stmMap.get(item.VTD1_Study__c).get(item.VTD1_Primary_PG__c),
                VTD2_Study_Team_Member1__c = stmMap.get(item.VTD1_Study__c).get(item.VTD1_PI_user__c)
            ));
        }
        insert newHistories;
    }

    private static Map<Id, Id> getPgCapacityMap(List<Case> cases) {
        List<Id> pgIds = new List<Id>();
        for (Case item : cases) {
            pgIds.add(item.VTD1_Primary_PG__c);
        }

        Map<Id, Id> pgCapacityMap = new Map<Id, Id>();
        for (VTD1_Patient_Guide_Capacity__c item : [
            SELECT Id, User__c FROM VTD1_Patient_Guide_Capacity__c WHERE User__c IN :pgIds
        ]) {
            pgCapacityMap.put(item.User__c, item.Id);
        }

        return pgCapacityMap;
    }

    private static Map<Id, Map<Id, Id>> getStmMap(List<Case> cases) {
        List<Id> userIds = new List<Id>();
        List<Id> studyIds = new List<Id>();
        for (Case item : cases) {
            userIds.add(item.VTD1_Primary_PG__c);
            userIds.add(item.VTD1_PI_user__c);
            studyIds.add(item.VTD1_Study__c);
        }

        Map<Id, Map<Id, Id>> stmMap = new Map<Id, Map<Id, Id>>();
        for (Study_Team_Member__c item : [
            SELECT Id, User__c, Study__c
            FROM Study_Team_Member__c
            WHERE Study__c IN :studyIds
            AND User__c IN :userIds
        ]) {
            if (! stmMap.containsKey(item.Study__c)) { stmMap.put(item.Study__c, new Map<Id, Id>()); }
            stmMap.get(item.Study__c).put(item.User__c, item.Id);
        }

        return stmMap;
    }
}