/**
 * Created by Alexander Komarov on 27.06.2019.
 */
@RestResource(UrlMapping='/Periodic/*')
global without sharing class VT_R3_RestPeriodic {
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();
    static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());
    static String CONSENT_LINK;
    private static final Set<String> AVAILABLE_PROFILES = new Set<String>{
            VT_R4_ConstantsHelper_Profiles.SCR_PROFILE_NAME,
            VT_R4_ConstantsHelper_Profiles.PI_PROFILE_NAME,
            VT_R4_ConstantsHelper_Profiles.PATIENT_PROFILE_NAME,
            VT_R4_ConstantsHelper_Profiles.CAREGIVER_PROFILE_NAME
    };
    @HttpPut
    global static void checkToken() {
        try {
            Profile currentProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
            if (!AVAILABLE_PROFILES.contains(currentProfile.Name)) {
                rejectedAnswer();
            } else {
            String result = UserInfo.getSessionId();
                RestContext.response.headers.put('Content-Type', 'application/json;charset=UTF-8');
                RestContext.response.responseBody = Blob.valueOf('"' + result + '"');
            }
        } catch (Exception e) {
            rejectedAnswer();
        }
    }
    private static void rejectedAnswer() {
        RestContext.response.headers.put('Content-Type', 'application/json;charset=UTF-8');
        RestContext.response.statusCode = 401;
        RestContext.response.responseBody = Blob.valueOf('[{"message":"Session expired or invalid","errorCode":"INVALID_SESSION_ID"}]');
    }
    @HttpGet
    global static String getPeriodicData() {
        PeriodicData periodicData = new PeriodicData();
        periodicData.futureVideoVisits = getVideoVisitsInfo();
        periodicData.lastNotificationUnixTime = getLastNotificationUnixTime();
        periodicData.unreadConversationsCount = getUnreadConversationsCount();
        periodicData.unreadNotificationsCount = getUnreadNotificationsCount();
        cr.buildResponse(periodicData);
        return JSON.serialize(cr, true);
    }
    public static Integer getUnreadNotificationsCount() {
        Id caseId = [
                SELECT Contact.VTD1_Clinical_Study_Membership__c
                FROM User
                WHERE Id = :UserInfo.getUserId()
        ].Contact.VTD1_Clinical_Study_Membership__c;

        Integer counter = [
                SELECT COUNT()
                FROM VTD1_NotificationC__c
                WHERE VTR4_CurrentPatientCase__c = :caseId
                AND VTR5_HideNotification__c = FALSE
                AND ((Type__c = 'Messages' AND Number_of_unread_messages__c > 0) OR (Type__c != 'Messages'))
                AND Type__c != 'Televisit'
                AND (OwnerId = :UserInfo.getUserId() OR VTD1_Receivers__c = :UserInfo.getUserId())
                AND VTD1_Read__c = FALSE
        ];

        return counter;
    }
    public static Long getLastNotificationUnixTime() {
        List<VTD1_NotificationC__c> l = [SELECT LastModifiedDate FROM VTD1_NotificationC__c WHERE VTD1_Receivers__c = :UserInfo.getUserId() ORDER BY LastModifiedDate DESC];
        if (l.size() > 0) {
            return l.get(0).LastModifiedDate.getTime() + timeOffset;
        } else {
            return -1;
        }
    }
    public static Integer getUnreadConversationsCount() {
        String unreadFeedsString = [SELECT VTR3_UnreadFeeds__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].VTR3_UnreadFeeds__c;
        if (unreadFeedsString != null && unreadFeedsString.length() > 3) {
            return unreadFeedsString.split(';').size();
        } else {
            return 0;
        }
    }
    private static List<VideoVisitInfo> getVideoVisitsInfo() {
        String patientId = VT_D1_PatientCaregiverBound.getPatientId();
        String currentUser = UserInfo.getUserId();
        List<VideoVisitInfo> listVVI = new List<VideoVisitInfo>();
        for (VTD1_Actual_Visit__c visit : [
                SELECT Id,
                        Name,
                        VTD1_Unscheduled_Visit_Type__c,
                        VTD1_Onboarding_Type__c,
                        VTD1_Scheduled_Date_Time__c,
                        VTD1_Scheduled_Visit_End_Date_Time__c, (
                        SELECT Id,
                                Scheduled_For__c,
                                VTD1_Scheduled_Visit_End__c
                        FROM Video_Conferences__r
                        ORDER BY CreatedDate
                                DESC
                        LIMIT 1
                ),
                (SELECT Id, VTD1_Participant_User__c FROM Visit_Members__r)
                FROM VTD1_Actual_Visit__c
                WHERE (Unscheduled_Visits__r.VTD1_Patient_User__c = :patientId OR VTD1_Case__r.VTD1_Patient_User__c = :patientId)
        ]) {
            if (visit.VTD1_Scheduled_Date_Time__c == null || visit.VTD1_Scheduled_Visit_End_Date_Time__c == null) {
                continue;
            }
            Boolean memberOfThisVisit = false;
            for (Visit_Member__c visitMember : visit.Visit_Members__r) {
                if (visitMember.VTD1_Participant_User__c != null && visitMember.VTD1_Participant_User__c.equals(currentUser)) {
                    memberOfThisVisit = true;
                }
            }
            if (visit.VTD1_Scheduled_Visit_End_Date_Time__c > System.now() && memberOfThisVisit) {
                VideoVisitInfo vvi = new VideoVisitInfo();
                vvi.visitId = visit.Id;
                vvi.visitName = visit.Name;
                vvi.visitStartDateTime = visit.VTD1_Scheduled_Date_Time__c.getTime();
                vvi.visitEndDateTime = visit.VTD1_Scheduled_Visit_End_Date_Time__c.getTime();
                if (visit.VTD1_Unscheduled_Visit_Type__c == 'Consent' || visit.VTD1_Unscheduled_Visit_Type__c == 'Re-Consent' || visit.VTD1_Onboarding_Type__c == 'Consent') {
                    vvi.eConsentNeed = true;
                    vvi.eConsentLink = getConsentLink(patientId);
                }
                if(!visit.Video_Conferences__r.isEmpty()) {
                    vvi.videoNeed = true;
                }
                listVVI.add(vvi);
            }
        }
        return listVVI;
    }
    private static String getConsentLink(String patientId) {
        if (CONSENT_LINK == null) {
        List<Case>caseList = [
                SELECT Id, VTD1_Virtual_Site__r.VTR5_ConsentPacketLink__c
                FROM Case
                    WHERE VTD1_Patient_User__c = :patientId
        ];
            CONSENT_LINK = caseList.isEmpty() ? '' : caseList[0].VTD1_Virtual_Site__r.VTR5_ConsentPacketLink__c;
        }
        return CONSENT_LINK;
    }
    public class PeriodicData {
        public List<VideoVisitInfo> futureVideoVisits;
        public Long lastNotificationUnixTime;
        public Integer unreadConversationsCount;
        public Integer unreadNotificationsCount;
    }
    public class VideoVisitInfo {
        public String visitId;
        public String visitName;
        public Long visitStartDateTime;
        public Long visitEndDateTime;
        public Boolean eConsentNeed = false;
        public Boolean videoNeed = false;
        public String eConsentLink;
    }
}