public with sharing class VT_D1_UtilityBarVideoController {
    @AuraEnabled
	public static String getSessionIdUserId() {        
		return UserInfo.getSessionId() + ',' + UserInfo.getUserId();
	}
	
	@AuraEnabled
	public static String getWebSocketUrl(){
		return CustomThemeLayoutController.getWebSocketUrl();
	}

	@AuraEnabled
	public static void sendSignal() {        
		List<VTD1_VideoChatSignal__e> eventList = new List<VTD1_VideoChatSignal__e>();	
		VTD1_VideoChatSignal__e videoEvent = new VTD1_VideoChatSignal__e(VTD1_UserId__c = '0053D000000oto8QAA');
		eventList.add(videoEvent);
		List<Database.SaveResult> results = EventBus.publish(eventList);	    
		for (Database.SaveResult result : results) {
			if (!result.isSuccess()) {
			for (Database.Error error : result.getErrors()) {
	            System.debug('Error returned: ' +
	                   error.getStatusCode() +' - '+
	                   error.getMessage());
	          }
	        }
            else system.debug('!!! Success');
		}    
	}

	/**
	* @description The method gets Notification for the end users
	* @author Aleh Lutsevich
	* @return VT_R5_ConfigurableNotificationController.NotificationWrapper
	* @date 24/10/2020
	* @issue SH-17939
	* @test VT_R5_ConfigurableNotificationContrTest
	*/
	@AuraEnabled
	public static List<VT_R5_ConfigurableNotificationController.NotificationWrapper> getModalData() {
		return VT_R5_ConfigurableNotificationController.getInitData();
	}
	
	/**
	* @description The method creates Notification Action for current user
	* @author Aleh Lutsevich
	* @date 24/10/2020
	* @issue SH-17939
	* @test VT_R5_ConfigurableNotificationContrTest
	*/
	@AuraEnabled
	public static void createNotificationAction(String notificationId) {
		VT_R5_ConfigurableNotificationController.createNotificationAction(notificationId);
	}
}