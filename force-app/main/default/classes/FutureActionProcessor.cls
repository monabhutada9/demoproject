/**
 * Created by triestelaporte on 11/6/20.
 *
 * This is the consumer/processor of the future actions.  It could as easily have been a queueable job, which makes it a bit more interruptable.
 *
 */

// We run this with allows callouts, and as stateful, since it's dynamic and can support many different cascading operations, we want to minimize contention and potential for breakage.
public without sharing class FutureActionProcessor implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {

    public Database.QueryLocator start (Database.BatchableContext bc) {
        FutureActionSettings theSettings = FutureActionSettings.getSettings();

        return Database.getQueryLocator([
                SELECT Id,
                        SerializedRecord__c,
                        ActionType__c,
                        EmailUserId__c,
                        EmailWhatId__c,
                        ErrorMessage__c,
                        EmailTemplateId__c,
                        ActionDate__c,
                        ObjectType__c,
                        RetryCount__c
                FROM FutureActionQueueRecord__c
                WHERE ActionDate__c <= :Datetime.now()
                        AND (RetryCount__c < :theSettings.maxRetryCount
                                OR RetryCount__c = NULL)

                ORDER BY RetryCount__c ASC NULLS FIRST, ActionDate__c ASC
        ]);
    }

    public void execute (Database.BatchableContext bc, List<FutureActionQueueRecord__c> ungroupedActionRequests) {

        // first let's sort the two actions types into two separate lists, one for email, and one for sobject upserts.  This enables us to tie errors back to the original action records by index.
        List<SObject> emailActions = new List<SObject>();
        List<FutureActionQueueRecord__c> groupedInsertActions = new List<FutureActionQueueRecord__c>();
        List<FutureActionQueueRecord__c> groupedUpdateActions = new List<FutureActionQueueRecord__c>();
        List<FutureActionQueueRecord__c> groupedDeleteActions = new List<FutureActionQueueRecord__c>();

        // this just groups everything, it's kind of messy so I refactored it into its own method to get it out of the way
        groupAllActions(ungroupedActionRequests, emailActions, groupedInsertActions, groupedUpdateActions, groupedDeleteActions);

        List<SObject> incomingQueueActionsToDelete = new List<SObject>();
        List<SObject> incomingQueueActionsToUpdate = new List<SObject>();

        // let's get the emailactions done first
        processEmailActions(emailActions, incomingQueueActionsToUpdate, incomingQueueActionsToDelete);

        // Now we can do the object dml
        // this stuff looks kind of weird, but it's all due to salesforce not allowing more than 10 different types in a mixed DML operation, and they can be in no more than 10 'chunks' of types.
        // and since we can't update the same record in the same list more than once, we need to keep track of records by id, and finally keep a map of objects by id, so we can lay multiple updates on top of other updates
        List<List<SObject>> recordsToInsert = new List<List<SObject>>{
                new List<SObject>()
        };
        List<Map<Id, SObject>> recordsToUpdate = new List<Map<Id, SObject>>{
                new Map<Id, SObject>()
        };
        List<Map<Id, SObject>> recordsToDelete = new List<Map<Id, SObject>>{
                new Map<Id, SObject>()
        };

        // these do our actual DML
        processInsertActions(groupedInsertActions, recordsToInsert, incomingQueueActionsToDelete, incomingQueueActionsToUpdate);

        processUpdateActions(groupedUpdateActions, recordsToUpdate, incomingQueueActionsToDelete, incomingQueueActionsToUpdate);

        processDeleteActions(groupedDeleteActions, recordsToDelete, incomingQueueActionsToDelete, incomingQueueActionsToUpdate);

        // finally, we can delete the successful actions or update the error messages
        if (!incomingQueueActionsToDelete.isEmpty()) {
            delete incomingQueueActionsToDelete;
        }
        if (!incomingQueueActionsToUpdate.isEmpty()) {
            update incomingQueueActionsToUpdate;
        }
    }

    public void finish (Database.BatchableContext bc) {
        if (!Test.isRunningTest()) {
            scheduleBatchJob();
        }
    }

    public static void scheduleBatchJob () {
        FutureActionSettings theSettings = FutureActionSettings.getSettings();

        System.scheduleBatch(new FutureActionProcessor(), theSettings.jobName, theSettings.delayMinutes, theSettings.batchSize);
    }

    private void groupAllActions (List<FutureActionQueueRecord__c> ungroupedActionRequests, List<FutureActionQueueRecord__c> emailActions, List<FutureActionQueueRecord__c> groupedInsertActions, List<FutureActionQueueRecord__c> groupedUpdateActions, List<FutureActionQueueRecord__c> groupedDeleteActions) {

        Map<String, List<FutureActionQueueRecord__c>> insertActionsByObjectType = new Map<String, List<FutureActionQueueRecord__c>>();
        Map<String, List<FutureActionQueueRecord__c>> updateActionsByObjectType = new Map<String, List<FutureActionQueueRecord__c>>();
        Map<String, List<FutureActionQueueRecord__c>> deleteActionsByObjectType = new Map<String, List<FutureActionQueueRecord__c>>();

        // this puts them in the maps
        for (FutureActionQueueRecord__c actionRequest : ungroupedActionRequests) {
            if (actionRequest.ActionType__c == String.valueOf(FutureActionSettings.ACTION_TYPE.SEND_EMAIL)) {
                emailActions.add(actionRequest);
            } else {
                // this logic groups the actions into sobject types, so we can keep them grouped going forward
                if (actionRequest.ActionType__c == String.valueOf(FutureActionSettings.ACTION_TYPE.CREATE_RECORD)) {
                    if (!insertActionsByObjectType.containsKey(actionRequest.ObjectType__c)) {
                        insertActionsByObjectType.put(actionRequest.ObjectType__c, new List<FutureActionQueueRecord__c>());
                    }
                    insertActionsByObjectType.get(actionRequest.ObjectType__c).add(actionRequest);
                } else if (actionRequest.ActionType__c == String.valueOf(FutureActionSettings.ACTION_TYPE.DELETE_RECORD)) {
                    if (!deleteActionsByObjectType.containsKey(actionRequest.ObjectType__c)) {
                        deleteActionsByObjectType.put(actionRequest.ObjectType__c, new List<FutureActionQueueRecord__c>());
                    }
                    deleteActionsByObjectType.get(actionRequest.ObjectType__c).add(actionRequest);
                } else if (actionRequest.ActionType__c == String.valueOf(FutureActionSettings.ACTION_TYPE.UPDATE_RECORD)) {
                    if (!updateActionsByObjectType.containsKey(actionRequest.ObjectType__c)) {
                        updateActionsByObjectType.put(actionRequest.ObjectType__c, new List<FutureActionQueueRecord__c>());
                    }
                    updateActionsByObjectType.get(actionRequest.ObjectType__c).add(actionRequest);
                }

            }
        }

        // and this takes them from the maps and puts them into their lists in groups
        for (String objectName : insertActionsByObjectType.keySet()) {
            groupedInsertActions.addAll(insertActionsByObjectType.get(objectName));
        }
        for (String objectName : updateActionsByObjectType.keySet()) {
            groupedUpdateActions.addAll(updateActionsByObjectType.get(objectName));
        }
        for (String objectName : deleteActionsByObjectType.keySet()) {
            groupedDeleteActions.addAll(deleteActionsByObjectType.get(objectName));
        }
    }

    private void processInsertActions (List<FutureActionQueueRecord__c> insertActions, List<List<SObject>> recordsToInsert, List<SObject> actionsToDelete, List<SObject> actionsToUpdate) {
        Set<String> insertTypeCounter = new Set<String>();

        for (FutureActionQueueRecord__c insertAction : insertActions) {
            if (insertTypeCounter.size() == 10) {
                recordsToInsert.add(new List<SObject>());
                insertTypeCounter = new Set<String>();
            }
            String objectPayloadString = insertAction.SerializedRecord__c;
            String objectTypeName = insertAction.ObjectType__c;

            insertTypeCounter.add(objectTypeName);

            SObject record = (SObject) JSON.deserialize(objectPayloadString, SObject.class);

            recordsToInsert[recordsToInsert.size() - 1].add(record);
        }
        if (!recordsToInsert[0].isEmpty()) {
            Integer recordInsertIndex = 0;
            for (List<SObject> insertList : recordsToInsert) {
                for (Database.SaveResult sr : Database.insert(insertList, false)) {
                    FutureActionQueueRecord__c insertAction = insertActions[recordInsertIndex];
                    if (sr.isSuccess()) {
                        actionsToDelete.add(insertAction);
                    } else {
                        String errorMessage = '';
                        for (Database.Error emailError : sr.errors) {
                            errorMessage += emailError.message + '; ';
                        }
                        errorMessage = errorMessage.removeEnd('; ');
                        insertAction.ErrorMessage__c = errorMessage;
                        Integer retryCount = Integer.valueOf(insertAction.RetryCount__c);
                        retryCount = (retryCount == null) ? 1 : retryCount + 1;
                        insertAction.RetryCount__c = retryCount;
                        actionsToUpdate.add(insertAction);
                    }
                    recordInsertIndex++;
                }
            }
        }
    }

    private void processUpdateActions (List<FutureActionQueueRecord__c> updateActions, List<Map<Id, SObject>> recordsToUpdate, List<SObject> actionsToDelete, List<SObject> actionsToUpdate) {
        Set<String> updateTypeCounter = new Set<String>();
        Map<Id, List<SObject>> updateActionsByRecordId = new Map<Id, List<SObject>>();

        for (FutureActionQueueRecord__c updateAction : updateActions) {
            if (updateTypeCounter.size() == 10) {
                recordsToUpdate.add(new Map<Id, SObject>());
                updateTypeCounter = new Set<String>();
            }
            String objectPayloadString = updateAction.SerializedRecord__c;
            String objectTypeName = updateAction.ObjectType__c;

            updateTypeCounter.add(objectTypeName);

            SObject newRecord = (SObject) JSON.deserialize(objectPayloadString, SObject.class);

            // this allows us to tie the record id back to multiple field update request records
            if (!updateActionsByRecordId.containsKey(newRecord.Id)) {
                updateActionsByRecordId.put(newRecord.Id, new List<SObject>());
            }
            updateActionsByRecordId.get(newRecord.Id).add(updateAction);

            Map<Id, SObject> innerMap = recordsToUpdate[recordsToUpdate.size() - 1];

            SObject mergeRecord = (SObject) innerMap.get(newRecord.Id);

            if (mergeRecord == null) {
                mergeRecord = newRecord;
            } else {
                Map<String, Object> newRecordAsMap = newRecord.getPopulatedFieldsAsMap();
                System.debug('newRecordAsMap:  ' + JSON.serializePretty(newRecordAsMap));
                for (String fieldName : newRecordAsMap.keySet()) {
                    System.debug('Working on field Name:  ' + fieldName);
                    Object fieldValue = newRecordAsMap.get(fieldName);
                    System.debug('Field Value:  ' + fieldValue);
                    mergeRecord.put(fieldName, fieldValue);
                }
            }

            innerMap.put(mergeRecord.Id, mergeRecord);
        }
        if (!recordsToUpdate[0].isEmpty()) {

            for (Map<Id, SObject> updateMap : recordsToUpdate) {
                Integer recordUpdateIndex = 0;
                for (Database.SaveResult sr : Database.update(updateMap.values(), false)) {
                    SObject recordBeingUpdated = updateMap.values()[recordUpdateIndex];
                    List<FutureActionQueueRecord__c> updateActionsInner = updateActionsByRecordId.get(recordBeingUpdated.Id);
                    if (sr.isSuccess()) {
                        actionsToDelete.addAll(updateActionsInner);
                    } else {
                        String errorMessage = '';
                        for (Database.Error emailError : sr.errors) {
                            errorMessage += emailError.message + '; ';
                        }
                        errorMessage = errorMessage.removeEnd('; ');
                        for (FutureActionQueueRecord__c updateAction : updateActionsInner) {
                            updateAction.ErrorMessage__c = errorMessage;
                            Integer retryCount = Integer.valueOf(updateAction.RetryCount__c);
                            retryCount = (retryCount == null) ? 1 : retryCount + 1;
                            updateAction.RetryCount__c = retryCount;
                        }
                        actionsToUpdate.addAll(updateActions);
                    }
                    recordUpdateIndex++;
                }
            }
        }
    }

    private void processDeleteActions (List<FutureActionQueueRecord__c> deleteActions, List<Map<Id, SObject>> recordsToDelete, List<SObject> actionsToDelete, List<SObject> actionsToUpdate) {
        Set<String> updateTypeCounter = new Set<String>();
        Map<Id, List<SObject>> deleteActionsByRecordId = new Map<Id, List<SObject>>();

        for (FutureActionQueueRecord__c deleteAction : deleteActions) {
            if (updateTypeCounter.size() == 10) {
                recordsToDelete.add(new Map<Id, SObject>());
                updateTypeCounter = new Set<String>();
            }
            String objectPayloadString = deleteAction.SerializedRecord__c;
            String objectTypeName = deleteAction.ObjectType__c;

            updateTypeCounter.add(objectTypeName);

            SObject oldRecord = (SObject) JSON.deserialize(objectPayloadString, SObject.class);

            // this allows us to tie the record id back to multiple field update request records
            if (!deleteActionsByRecordId.containsKey(oldRecord.Id)) {
                deleteActionsByRecordId.put(oldRecord.Id, new List<SObject>());
            }
            deleteActionsByRecordId.get(oldRecord.Id).add(deleteAction);

            Map<Id, SObject> innerMap = recordsToDelete[recordsToDelete.size() - 1];

            innerMap.put(oldRecord.Id, oldRecord);
        }
        if (!recordsToDelete[0].isEmpty()) {

            for (Map<Id, SObject> deleteMap : recordsToDelete) {
                Integer recordUpdateIndex = 0;
                for (Database.DeleteResult sr : Database.delete(deleteMap.values(), false)) {
                    SObject recordBeingDeleted = deleteMap.values()[recordUpdateIndex];
                    List<FutureActionQueueRecord__c> deleteActionsInner = deleteActionsByRecordId.get(recordBeingDeleted.Id);
                    if (sr.isSuccess()) {
                        actionsToDelete.addAll(deleteActionsInner);
                    } else {
                        String errorMessage = '';
                        for (Database.Error emailError : sr.errors) {
                            errorMessage += emailError.message + '; ';
                        }
                        errorMessage = errorMessage.removeEnd('; ');
                        for (FutureActionQueueRecord__c updateAction : deleteActionsInner) {
                            updateAction.ErrorMessage__c = errorMessage;
                            Integer retryCount = Integer.valueOf(updateAction.RetryCount__c);
                            retryCount = (retryCount == null) ? 1 : retryCount + 1;
                            updateAction.RetryCount__c = retryCount;
                        }
                        actionsToUpdate.addAll(deleteActions);
                    }
                    recordUpdateIndex++;
                }
            }
        }
    }

    private void processEmailActions (List<FutureActionQueueRecord__c> emailActions, List<SObject> actionsToUpdate, List<SObject> actionsToDelete) {
        List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();
        for (FutureActionQueueRecord__c emailAction : emailActions) {
            Messaging.SingleEmailMessage newEmail = new Messaging.SingleEmailMessage();
            newEmail.setTemplateId(emailAction.EmailTemplateId__c);
            newEmail.setTargetObjectId(emailAction.EmailUserId__c);
            Id whatId = emailAction.EmailWhatId__c;
            if (whatId != null) {
                newEmail.setWhatId(whatId);
            }
            emailsToSend.add(newEmail);
        }
        if (!emailsToSend.isEmpty()) {
            Integer emailSendIndex = 0;
            for (Messaging.SendEmailResult ser : Messaging.sendEmail(emailsToSend)) {
                FutureActionQueueRecord__c emailAction = emailActions[emailSendIndex];
                if (ser.isSuccess()) {
                    actionsToDelete.add(emailAction);
                } else {
                    String errorMessage = '';
                    for (Messaging.SendEmailError emailError : ser.errors) {
                        errorMessage += emailError.message + '; ';
                    }
                    errorMessage = errorMessage.removeEnd('; ');
                    emailAction.ErrorMessage__c = errorMessage;
                    Integer retryCount = Integer.valueOf(emailAction.RetryCount__c);
                    retryCount = (retryCount == null) ? 1 : retryCount + 1;
                    emailAction.RetryCount__c = retryCount;
                    actionsToUpdate.add(emailAction);
                }
                emailSendIndex++;
            }
        }
    }
}