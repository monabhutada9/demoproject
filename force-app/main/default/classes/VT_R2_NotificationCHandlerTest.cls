/**
 * Created by user on 05.03.2019.
 */

@IsTest
private class VT_R2_NotificationCHandlerTest {
    /*
    // UNUSED
    @TestSetup
    static void testSetup(){
       VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        {
            HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
            VT_D1_TestUtils.createPatientCandidate('Mike', 'Birn',  VT_D1_TestUtils.generateUniqueUserName(), 'mikebirn@test.com', 'mikebirn', 'Converted', study.Id, 'Prot-001', '11-11-11', 'US', 'NY');
        }
        Test.stopTest();
    }

    @IsTest
    static void test() {
        Case cas = [SELECT Id, VTD1_Study__c, AccountId
        FROM Case
        WHERE VTD1_Study__c != null
        AND RecordType.Name = 'CarePlan'];
        Contact patientContact = [SELECT Id FROM Contact WHERE AccountId = :cas.AccountId];
        User patientUser = [SELECT Id, IsActive, Contact.VTD1_Clinical_Study_Membership__r.Status FROM User WHERE ContactId = :patientContact.Id];
        VT_D1_TestUtils.loadProfiles();
        Contact caregiverContact = new Contact(RecordTypeId = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_CAREGIVER,
                AccountId = cas.AccountId, VTD1_Account__c = cas.AccountId,
                FirstName = 'C ' + System.now(), LastName = 'Caregiver', Email = 'testCare@dt.com');
        insert caregiverContact;
        User caregiverUser = VT_D1_TestUtils.createUserByProfile('Caregiver', 'CaregiverCG');
        caregiverUser.ContactId = caregiverContact.Id;
        insert caregiverUser;
        VTD1_NotificationC__c ntc = new VTD1_NotificationC__c(VTD1_Receivers__c = patientUser.Id, OwnerId=patientUser.Id, Number_of_unread_messages__c=2, Title__c='Unread');
        insert ntc;
        testFuture(patientUser.Id);
    }

    @future
    static void testFuture(Id patientUserId) {
        VTD1_NotificationC__c ntc = new VTD1_NotificationC__c(VTD1_Receivers__c = patientUserId, OwnerId = patientUserId, Number_of_unread_messages__c=2, Title__c='Unread');
        insert ntc;
    }
    */
}