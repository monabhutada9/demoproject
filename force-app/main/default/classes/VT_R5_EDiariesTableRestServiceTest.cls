/**
 * Created by Maksim Fedarenka on 12/7/2020.
 */

@IsTest
private class VT_R5_EDiariesTableRestServiceTest {
    private static final Id rtId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('VTR5_External').getRecordTypeId();

    @TestSetup
    static void setup() {
        Test.startTest();
        DomainObjects.Study_t domainStudy = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                )
        ;

        Virtual_Site__c site = (Virtual_Site__c) new DomainObjects.VirtualSite_t()
                .addStudy(domainStudy)
                .setStudySiteNumber('12345')
                .persist();

        VT_R3_GlobalSharing.disableForTest = true;

        HealthCloudGA__CandidatePatient__c candidatePatient = (HealthCloudGA__CandidatePatient__c) new DomainObjects.HealthCloudGA_CandidatePatient_t()
                .setRr_firstName('Mike')
                .setRr_lastName('Birn')
                .setConversion('Converted')
                .setCaregiverEmail(DomainObjects.RANDOM.getEmail())
                .addStudy(domainStudy)
                .persist();
        Test.stopTest();

        Case pCase = DomainObjects.TEST_UTILS.getTestCase();
        pCase.VTD1_Virtual_Site__c = site.Id;
        update pCase;

        List<VTD1_Survey__c> surveys = new List<VTD1_Survey__c>();
        for (Integer i = 0; i < 3; i++) {
            surveys.add(new VTD1_Survey__c(VTD1_CSM__c = pCase.Id, Name = 'TEST SURVEY', RecordTypeId = rtId));
        }
        insert surveys;
    }

    @IsTest
    static void testBehavior() {
        HealthCloudGA__CarePlanTemplate__c study = DomainObjects.TEST_UTILS.getTestStudy();

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/ediaries-table-service';
        req.httpMethod = 'GET';
        req.addParameter('filter', JSON.serialize(new Map<String, Object>{
                'filterMap' => new Map<String, Object>{
                        'studyId' => study.Id
                }
        }));

        RestContext.request = req;
        RestContext.response= res;

        Test.startTest();
        String result = VT_R5_EDiariesTableRestService.getEDiaryAndSiteNames();
        System.assert(result != null);
        Test.stopTest();
    }

    @IsTest
    static void testExceptionHandling() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/ediaries-table-service';
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response= res;

        Test.startTest();
        Map<String, Object> result = (Map<String, Object>) JSON.deserializeUntyped(VT_R5_EDiariesTableRestService.getEDiaryAndSiteNames());
        System.assert(result.get('errors') != null);
        Test.stopTest();
    }
}