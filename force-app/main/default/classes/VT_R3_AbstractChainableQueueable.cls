/**
* @author: Carl Judge
* @date: 27-Aug-19
* @description: A wrapper for Queueables which provides methods to easily create a chain of Queueables, and also automatically queue via platform events if
* maximum queuables for the current transaction has been reached.
**/

public abstract class VT_R3_AbstractChainableQueueable extends VT_R5_TypeSerializable implements Queueable {
    protected VT_R3_AbstractChainableQueueable nextQueueable;
    private VT_R5_TypeSerializable.SerializedItem serializedNextQueueable;

    public static VT_R3_AbstractChainableQueueable reassembleSeparatedChain(List<VT_R5_TypeSerializable.SerializedItem> separatedSerializedQueueables) {
        // combine the list of queueables into a single, chained queueable
        VT_R3_AbstractChainableQueueable chainQueueable;
        for (VT_R5_TypeSerializable.SerializedItem serializedQueueable : separatedSerializedQueueables) {
            VT_R3_AbstractChainableQueueable deSerializedQueueable = (VT_R3_AbstractChainableQueueable) serializedQueueable.deserialize();
            if (chainQueueable == null) {
                chainQueueable = deSerializedQueueable;
            } else {
                chainQueueable.addToChain(deSerializedQueueable);
            }
        }
        return chainQueueable;
    }

    public void execute(QueueableContext context) {
        executeLogic();
        if (nextQueueable != null) {
            nextQueueable.enqueue();
        }
    }

    /**
     * Should contain the main logic of the queueable. Analogous to the execute method in a standard queueable
     */
    public abstract void executeLogic();

    /**
     * In a normal context, enqueues the job if possible. Or, if queueable limit has been reached,
     * enqueue in a new transaction via a platform event.
     * If in test context, execute directly without enqueuing
     */
    public virtual void enqueue() {
        if (Test.isRunningTest()) {
            this.execute(null);
        } else if (Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()) {
            System.enqueueJob(this);
        } else {
            enqueueViaPlatformEvent();
        }
    }

    /**
     * Queues the current instance of this class using a platform event, to get a fresh transaction
     */
    public void enqueueViaPlatformEvent() {
        VT_R3_PublishedAction_ChainQueueable publishedAction = new VT_R3_PublishedAction_ChainQueueable(this);
        publishedAction.publish();
    }

    /**
     * separate a chainableQueueable with nested queueables into a list of separate chainableQueueables.
     * the queueables are serialized and paired with their class type so they can be deserialized into
     * the correct concrete sub-class. For queueables with a deep chain, serializing without separating can cause problems with heap limit
     * @return queuables, separated and serialized
     */
    public List<VT_R5_TypeSerializable.SerializedItem> getSeparatedSerializedItems(){
        VT_R3_AbstractChainableQueueable queueable = this;
        List<VT_R5_TypeSerializable.SerializedItem> serializedQueueablesWithTypes = new List<VT_R5_TypeSerializable.SerializedItem>();
        while (queueable != null) {
            VT_R3_AbstractChainableQueueable clonedQueueable = queueable.clone();
            clonedQueueable.nextQueueable = null;
            serializedQueueablesWithTypes.add((clonedQueueable).serializeWithType());
            queueable = queueable.nextQueueable;
        }
        return serializedQueueablesWithTypes;
    }

    /**
     * Adds another chainable queueable to the end of the chain (nextQueueable class variable is the next link in the chain)
     *
     * @param chainQueueableToAdd - chain queueable to add to the end of the list
     */
    public void addToChain(VT_R3_AbstractChainableQueueable chainQueueableToAdd) {
        getChainEnd().nextQueueable = chainQueueableToAdd;
    }

    public VT_R3_AbstractChainableQueueable getChainEnd() {
        VT_R3_AbstractChainableQueueable currentQueueable = this;
        while (currentQueueable.nextQueueable != null) {
            currentQueueable = currentQueueable.nextQueueable;
        }
        return currentQueueable;
    }

    public void clearNextQueueable() {
        nextQueueable = null;
    }

    public virtual override void actionsBeforeSerialize() {
        super.actionsBeforeSerialize();
        serializeNextQueueable();
    }

    public virtual override void actionsAfterDeserialize() {
        super.actionsAfterDeserialize();
        deserializeNextQueueable();
    }

    protected void serializeNextQueueable() {
        if (nextQueueable != null) {
            serializedNextQueueable = nextQueueable.serializeWithType();
            nextQueueable = null;
        }
    }

    protected void deserializeNextQueueable() {
        if (serializedNextQueueable != null) {
            nextQueueable = (VT_R3_AbstractChainableQueueable) serializedNextQueueable.deserialize();
            serializedNextQueueable = null;
        }
    }
}