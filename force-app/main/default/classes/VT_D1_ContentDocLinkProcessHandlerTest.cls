/**
 * Created by Danylo Belei on 14.05.2019.
 */

@IsTest
public class VT_D1_ContentDocLinkProcessHandlerTest {

//    @TestSetup
//    private static void testSetup(){
//        Test.startTest();
//        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
//        HealthCloudGA__CandidatePatient__c candidatePatient = VT_D1_TestUtils.createPatientCandidate('Mike', 'Birn', 'mikebirn@test.com', 'mikebirn@test.com', 'mikebirn', 'Converted', study.Id, 'PN', '11-11-11', 'US', 'NY');
//        VT_D1_TestUtils.createTestPatientCandidate(1);
//        Test.stopTest();
//    }
//
//    @isTest
//    private static void testInsert() {
//
//        Case cas = [SELECT id, VTD1_Study__c from Case limit 1] ;
//
//            VTD1_Regulatory_Document__c regDoc = new VTD1_Regulatory_Document__c();
//            regDoc.VTD1_Document_Type__c = 'Eligibility Assesment Form With TMA';
//            regDoc.VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI';
//            insert regDoc;
//
//            VTD1_Regulatory_Binder__c regBinder = new VTD1_Regulatory_Binder__c();
//            regBinder.VTD1_Regulatory_Document__c = regDoc.Id;
//            regBinder.VTD1_Care_Plan_Template__c = cas.VTD1_Study__c;
//            insert regBinder;
//
//            VTD1_Document__c doc = new VTD1_Document__c();
//            doc.VTD1_Study__c = cas.VTD1_Study__c;
//            doc.VTD1_Clinical_Study_Membership__c = cas.Id;
//            doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE;
//            doc.VTD1_Regulatory_Binder__c = regBinder.Id;
//
//            insert doc;
//            System.debug(doc.Id);
//
//            ContentVersion contentVersion = new ContentVersion();
//            contentVersion.PathOnClient = 'test.txt';
//            contentVersion.Title = 'Test file';
//            contentVersion.VersionData = Blob.valueOf('Test Data');
//            //contentVersion.ContentDocumentId = doc.Id;
//            insert contentVersion;
//            System.debug(contentVersion);
//
//            ContentDocument contentDocument = [SELECT Id from ContentDocument LIMIT 1];
//
//            ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
//            contentDocumentLink.LinkedEntityId = doc.Id;
//            contentDocumentLink.ContentDocumentId = contentDocument.Id;
//            contentDocumentLink.ShareType = 'I';
//            contentDocumentLink.Visibility = 'AllUsers';
//            insert contentDocumentLink;
//
//    }
//
//    @isTest
//    private static void testUpdate(){
//
//        Case cas = [SELECT id, VTD1_Study__c from Case limit 1] ;
//
//            VTD1_Regulatory_Document__c regDoc = new VTD1_Regulatory_Document__c();
//            regDoc.VTD1_Document_Type__c = 'Eligibility Assesment Form With TMA';
//            regDoc.VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI';
//            insert regDoc;
//
//            VTD1_Regulatory_Binder__c regBinder = new VTD1_Regulatory_Binder__c();
//            regBinder.VTD1_Regulatory_Document__c = regDoc.Id;
//            regBinder.VTD1_Care_Plan_Template__c = cas.VTD1_Study__c;
//            insert regBinder;
//
//            VTD1_Document__c doc = new VTD1_Document__c();
//            doc.VTD1_Study__c = cas.VTD1_Study__c;
//            doc.VTD1_Clinical_Study_Membership__c = cas.Id;
//            doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE;
//            doc.VTD1_Regulatory_Binder__c = regBinder.Id;
//
//            insert doc;
//            System.debug(doc.Id);
//
//            ContentVersion contentVersion = new ContentVersion();
//            contentVersion.PathOnClient = 'test.txt';
//            contentVersion.Title = 'Test file';
//            contentVersion.VersionData = Blob.valueOf('Test Data');
//            insert contentVersion;
//
//            ContentDocument contentDocument = [SELECT Id from ContentDocument LIMIT 1];
//
//            ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
//            contentDocumentLink.LinkedEntityId = cas.Id;
//            contentDocumentLink.ContentDocumentId = contentDocument.Id;
//            contentDocumentLink.ShareType = 'I';
//            contentDocumentLink.Visibility = 'AllUsers';
//            insert contentDocumentLink;
//
//            update contentDocumentLink;
//    }
}