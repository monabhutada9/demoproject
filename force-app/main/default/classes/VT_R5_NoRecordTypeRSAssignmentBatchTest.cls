/**
 * @author: Stanislav Frolov
 * @date: 29.04.2020
 * @description:
*/
@IsTest
public with sharing class VT_R5_NoRecordTypeRSAssignmentBatchTest {
    @TestSetup
    static void setup(){
        DomainObjects.Study_t study = createStudy();
        DomainObjects.VTD2_Study_Geography_t sg = createStudyGeography(study);
        DomainObjects.StudyTeamMember_t rs = createRegulatorySpecialist(study);
        List<VTR3_Geography_STM__c> vtr3GeographySTMS = new List<VTR3_Geography_STM__c>();

        for(Integer i=0; i < 80; i++){
            vtr3GeographySTMS.add(new VTR3_Geography_STM__c(
                    Study_Geography__c = sg.Id,
                    Is_Primary__c = false,
                    VTR3_STM__c = rs.id
            )
            );
        }
        insert vtr3GeographySTMS;
    }
    @IsTest
    static void method(){
        Database.executeBatch(new VT_R5_NoRecordTypeRegSpecAssignmentBatch());
    }
    private static DomainObjects.Study_t createStudy() {

        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setName('Study Name')
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));
        return study;
    }

    private static DomainObjects.StudyTeamMember_t createRegulatorySpecialist (DomainObjects.Study_t study){

        DomainObjects.StudyTeamMember_t regSpec = new DomainObjects.StudyTeamMember_t()
                .addUser(new DomainObjects.User_t()
                        .setProfile('Regulatory Specialist'))
                .addStudy(study);
        regSpec.persist();
        return  regSpec;
    }

    private static DomainObjects.VTD2_Study_Geography_t createStudyGeography(DomainObjects.Study_t study) {

        DomainObjects.VTD2_Study_Geography_t studyGeographyT = new DomainObjects.VTD2_Study_Geography_t(study)
                .addVTD2_Study(study);
        return studyGeographyT;

    }
}