/**
* @author: Carl Judge
* @date: 29-Sep-20
**/

@IsTest
public class VT_R5_SiteMemberCalculatorTest {

    @TestSetup
    static void testSetup() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }


    @IsTest
    public static void doTest() {
        Case cas = [SELECT Id, VTD1_Study__c, VTD1_PI_user__c, VTD1_Virtual_Site__c FROM Case];
        System.debug(cas.VTD1_PI_user__c);

        VT_R5_SiteMemberCalculator siteCalc = new VT_R5_SiteMemberCalculator();
        siteCalc.initForSites(new Set<Id>{cas.VTD1_Virtual_Site__c});
        System.assert(siteCalc.isSiteMember(cas.VTD1_Virtual_Site__c, cas.VTD1_PI_user__c));
        
        VT_R5_SiteMemberCalculator userCalc = new VT_R5_SiteMemberCalculator();
        userCalc.initForUsers(new Set<Id>{cas.VTD1_PI_user__c}, new Set<Id>{cas.VTD1_Study__c});
        System.assert(userCalc.isSiteMember(cas.VTD1_Virtual_Site__c, cas.VTD1_PI_user__c));
    }
}