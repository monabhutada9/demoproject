/**
 * Created by user on 18-Oct-19.
 */

public with sharing class VT_R3_ActualVisitSelector {

    public static VTD1_Actual_Visit__c getActualVisitById(Id actualVisitId) {

        return [
                SELECT Id, Name, VTD1_Case__c, Unscheduled_Visits__c, Unscheduled_Visits__r.VTD1_Study__c, VTD1_Case__r.VTD1_Subject_ID__c, Unscheduled_Visits__r.VTD1_Subject_ID__c, Unscheduled_Visits__r.VTD1_Study__r.VTD1_Name__c, VTD1_Case__r.VTD1_Study__r.VTD1_Name__c,
                        VTD1_Case__r.Contact.Name,
                        Unscheduled_Visits__r.Contact.Name
                FROM VTD1_Actual_Visit__c
                WHERE Id = :actualVisitId
                LIMIT 1
        ];

    }

}