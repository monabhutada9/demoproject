/**
 * Created by user on 07.08.2018.
 */

public with sharing class VT_D1_Batch_PCQC_ProjectLead  implements Database.Batchable<SObject>, Database.AllowsCallouts, Schedulable{

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id, VTD1_Project_Lead__c  from HealthCloudGA__CarePlanTemplate__c where VTD1_Project_Lead__c != null]);
    }

    /**
     * Edited by Galiya Khalikova on 27.12.2019.
     * Edited as part of SH-7022
     * @param BC
     * @param studies List<HealthCloudGA__CarePlanTemplate__c> contains list of Studies
     * Move unconverted Task to TN Catalog
     */
    public void execute(Database.BatchableContext bc, List<HealthCloudGA__CarePlanTemplate__c> studies) {

        List<Id> sourceIds = new List<Id>();
        List <String> tnCatalogCodes = new List<String>();
        for (HealthCloudGA__CarePlanTemplate__c study : studies) {
            sourceIds.add(study.Id);
            tnCatalogCodes.add('T567');
        }

        if (!tnCatalogCodes.isEmpty()) {
            List <Task> newTasks = VT_D2_TNCatalogTasks.generateTasks(tnCatalogCodes, sourceIds, null, null, false);
            DateTime d = datetime.now().addMonths(-1);
            String monthName = d.format('MMMMM');
            for (Integer i = 0; i < sourceIds.size(); i++) {
                String comments = newTasks[i].Subject;
                comments = comments.replace('#monthName', monthName);
                newTasks[i].Subject = comments;
            }
            insert newTasks;
        }
    }

    public void finish(Database.BatchableContext bc) {
    }

    public void execute(SchedulableContext sc) {
        Database.executeBatch(this);
    }
}