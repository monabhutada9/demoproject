public class VT_D1_UpNextVisit {
    @AuraEnabled(Cacheable=true)
    public static ActualVisitWrapper getVisit() {
        ActualVisitWrapper visitWrapper = new ActualVisitWrapper();
        VTD1_Actual_Visit__c visit;
        Case currentCase = VT_D1_CaseByCurrentUser.getCase().caseObj;
        if (currentCase == null) return null;
        List<VTD1_Actual_Visit__c> visits = [
                SELECT Id,
                        Name,
                        VTD1_Visit_Description__c,
                        VTD1_Protocol_Visit__r.VTD1_EDC_Name__c,
                        VTD1_Status__c,
                        toLabel(VTD1_Status__c) statusLabel,
                        VTD1_Scheduled_Date_Time__c,
                        To_Be_Scheduled_Date__c,
                        VTD1_Onboarding_Type__c,
                        VTD1_Protocol_Visit__r.VTD1_Visit_Description__c,
                        VTD1_Protocol_Visit__r.VTD1_VisitType__c,
                        VTD1_Protocol_Visit__r.VTD1_VisitNumber__c,
                        VTD1_Protocol_Visit__r.VTD1_Onboarding_Type__c,
                        VTR2_Modality__c,
                        toLabel(VTR2_Modality__c) modalityLabel,
                        VTR2_Loc_Address__c,
                        VTR2_Loc_Name__c,
                        VTR2_Loc_Phone__c,
                        VTD1_Case__r.VTR3_LTFU__c,
                        VTD1_Case__r.VTD1_Randomized_Date1__c,
                        VTD1_Protocol_Visit__r.RecordTypeId,
                        VTD1_Unscheduled_Visit_Type__c
                FROM VTD1_Actual_Visit__c
                WHERE (
                        VTD1_Case__c = :currentCase.Id OR Unscheduled_Visits__c = :currentCase.Id
                ) AND (
                        VTD1_Status__c IN :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_PATIENT_STATUSES
                ) AND VTR3_LTFUStudyVisit__c = TRUE
                ORDER BY VTD1_Scheduled_Date_Time__c NULLS LAST, To_Be_Scheduled_Date__c NULLS LAST, VTD1_Protocol_Visit__r.VTD1_VisitNumber__c
        ];

        System.debug('visits: -- ' + visits);
        if (!visits.isEmpty()) {
            List<ActualVisitComparable> comparableVisits = new List<ActualVisitComparable>();
            for (VTD1_Actual_Visit__c av : visits) {
                comparableVisits.add(new ActualVisitComparable(av));
            }
            comparableVisits.sort();
            for (ActualVisitComparable avc : comparableVisits) {
                System.debug(avc.visitNumber + ' - ' + avc.scheduledDateTime + ' - ' + avc.visitTargetDate);
            }
            visit = comparableVisits[0].visit;
            System.debug('UpNext visit ' + visit);
            VT_D1_TranslateHelper.translate(visit);
            visitWrapper.actualVisit = visit;
            visitWrapper.visitName = VT_D1_HelperClass.getActualVisitName(visit);
        }
        return visitWrapper;
    }

    @AuraEnabled(Cacheable=true)
    public static String getAvailableDateTimes(Id visitId) {
        return new VT_D1_ActualVisitsHelper().getAvailableDateTimes(visitId);
    }

    @AuraEnabled(Cacheable=true)
    public static String getMembers(Id visitId) {
        List<Visit_Member__c> members = getVisitMembers(visitId);
        VT_D1_TranslateHelper.translate(members);
        List<VisitMemberWrapper> visitMemberWrappers = new List<VisitMemberWrapper>();
        String userProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId() LIMIT 1].Name;
        for (Visit_Member__c member : members) {
            visitMemberWrappers.add(new VisitMemberWrapper(member, userProfile));
        }
        return JSON.serialize(visitMemberWrappers);
    }

    private static List<Visit_Member__c> getVisitMembers(Id visitId) {
        return [
                SELECT Id,
                        VTD1_Actual_Visit__c,
                        Visit_Member_Name__c,
                        VTD1_Participant_User__r.Profile.Name,
                        VTD1_External_Participant_Type__c,
                        VTD1_Member_Type__c,
                        VTD1_Name_HHN_Phleb__c,
                        VTD1_Actual_Visit__r.VTD2_Patient_ID__c,
                        VTD1_Participant_User__r.FirstName,
                        VTD1_Participant_User__r.LastName
                FROM Visit_Member__c
                WHERE VTD1_Actual_Visit__c = :visitId
        ];
    }

    public class VisitMemberWrapper {
        public Visit_Member__c visitMember { set; get; }
        public String visitMemberName { set; get; }

        public VisitMemberWrapper(Visit_Member__c visitMember, String userProfile) {
            this.visitMember = visitMember;
            formulaFieldTranslate(visitMember, userProfile);
        }

        private void formulaFieldTranslate(Visit_Member__c member, String userProfile) {
            VT_D1_TranslateHelper.translate(member.VTD1_Participant_User__r.FirstName);
            VT_D1_TranslateHelper.translate(member.VTD1_Participant_User__r.LastName);
            String nameHHNPhleb = member.VTD1_Name_HHN_Phleb__c == null ? '' : member.VTD1_Name_HHN_Phleb__c;
            if (member.VTD1_Member_Type__c == Label.VTD2_Patient && userProfile == 'Therapeutic Medical Advisor') {
                visitMemberName = nameHHNPhleb + member.VTD1_Actual_Visit__r.VTD2_Patient_ID__c;
            } else if (member.VTD1_Participant_User__r.FirstName != null && member.VTD1_Participant_User__r.LastName != null) {
                visitMemberName = nameHHNPhleb + member.VTD1_Participant_User__r.FirstName + ' ' + member.VTD1_Participant_User__r.LastName;
            } else {
                visitMemberName = nameHHNPhleb;
            }
        }
    }

    public class ActualVisitComparable implements Comparable {
        public VTD1_Actual_Visit__c visit;
        public Datetime scheduledDateTime;
        public Date visitTargetDate;
        public String visitNumber;
        public Boolean isLTFU;

        public ActualVisitComparable(VTD1_Actual_Visit__c av) {
            visit = av;
            scheduledDateTime = av.VTD1_Scheduled_Date_Time__c;
            visitTargetDate = av.To_Be_Scheduled_Date__c;
            visitNumber = av.VTD1_Protocol_Visit__r.VTD1_VisitNumber__c;
            isLTFU = av.VTD1_Case__r.VTR3_LTFU__c;
        }

        public Integer compareTo(Object compareTo) {
            ActualVisitComparable compareToItem = (ActualVisitComparable) compareTo;
            if (scheduledDateTime != compareToItem.scheduledDateTime) {
                if (scheduledDateTime == null) {
                    return 1;
                }
                if (compareToItem.scheduledDateTime == null) {
                    return -1;
                }
                return scheduledDateTime < compareToItem.scheduledDateTime ? -1 : 1;
            } else if (visitTargetDate != compareToItem.visitTargetDate) {
                if (visitTargetDate == null) {
                    return 1;
                }
                if (compareToItem.visitTargetDate == null) {
                    return -1;
                }
                if (isLTFU && visit.VTD1_Case__r.VTD1_Randomized_Date1__c == null) {
                    if (visit.VTD1_Protocol_Visit__r.RecordTypeId == VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_PROTOCOL_VISIT_PROTOCOL) {
                        if (compareToItem.visit.VTD1_Protocol_Visit__r.RecordTypeId == VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_PROTOCOL_VISIT_PROTOCOL) {
                            return visitNumber > compareToItem.visitNumber ? 1 : -1;
                        } else {
                            return 1;
                        }
                    }
                } else {
                    return visitTargetDate < compareToItem.visitTargetDate ? -1 : 1;
                }
            } else if (visitNumber != compareToItem.visitNumber) {
                if (visitNumber == null) {
                    return 1;
                }
                if (compareToItem.visitNumber == null) {
                    return -1;
                }
                return 0;
                //return visitNumber < compareToItem.visitNumber ? -1 : 1;
            }
            return 0;
        }
    }
    public class ActualVisitWrapper {
        @AuraEnabled public String visitName { get; set; }
        @AuraEnabled public VTD1_Actual_Visit__c actualVisit;
    }
}