/**
* @author: Carl Judge
* @date: 21-Dec-18
* @description: 
**/

public without sharing class VT_D2_ProtocolAmendmentTriggerHandler {

    public void onBeforeDelete(List<VTD1_Protocol_Amendment__c> recs) {
        VT_D1_LegalHoldDeleteValidator.validateDeletion(recs);
        updateDocSharesOnDelete(recs);
    }

    public void onAfterUpdate(List<VTD1_Protocol_Amendment__c> recs, Map<Id, VTD1_Protocol_Amendment__c> oldMap) {
        updateDocSharesOnUpdate(recs, oldMap);
        sendAmendmentNotificationsAndTasks(recs, oldMap);
    }

    private void updateDocSharesOnDelete(List<VTD1_Protocol_Amendment__c> recs) {
        List<Id> docIds = new List<Id>();
        for (VTD1_Protocol_Amendment__c item : recs) {
            if (item.VTD1_Study_ID__c != null && item.VTD1_Protocol_Amendment_Document_Link__c != null) {
                docIds.add(item.VTD1_Protocol_Amendment_Document_Link__c);
            }
        }
        if (! docIds.isEmpty()) { updateDocShares(docIds); }
    }

    private void updateDocSharesOnUpdate(List<VTD1_Protocol_Amendment__c> recs, Map<Id, VTD1_Protocol_Amendment__c> oldMap) {
        List<Id> docIds = new List<Id>();
        for (VTD1_Protocol_Amendment__c item : recs) {
            if (item.VTD1_Study_ID__c != null &&
                item.VTD1_Protocol_Amendment_Document_Link__c != oldMap.get(item.Id).VTD1_Protocol_Amendment_Document_Link__c)
            {
                if (item.VTD1_Protocol_Amendment_Document_Link__c != null) {
                    docIds.add(item.VTD1_Protocol_Amendment_Document_Link__c);
                }
                if (oldMap.get(item.Id).VTD1_Protocol_Amendment_Document_Link__c != null) {
                    docIds.add(oldMap.get(item.Id).VTD1_Protocol_Amendment_Document_Link__c);
                }
            }
        }
        if (! docIds.isEmpty()) { updateDocShares(docIds); }
    }

    private void updateDocShares(List<Id> docIds) {
        VT_R3_GlobalSharing.doSharing(docIds);
    }

    public void onAfterInsert(List<VTD1_Protocol_Amendment__c> recs) {
        Map <Id, VTD1_Regulatory_Binder__c> studyIdToBindersMapPASignature = new Map<Id, VTD1_Regulatory_Binder__c>();
        Map <Id, VTD1_Regulatory_Binder__c> studyIdToBindersMapPA = new Map<Id, VTD1_Regulatory_Binder__c>();
        for (VTD1_Protocol_Amendment__c pa : recs) {
            studyIdToBindersMapPASignature.put(pa.VTD1_Study_ID__c, null);
            studyIdToBindersMapPA.put(pa.VTD1_Study_ID__c, null);
        }
        List <VTD1_Regulatory_Binder__c> binders = [
                SELECT Id, VTD1_Care_Plan_Template__c,
                        VTD1_Regulatory_Document__r.Name,
                        VTD1_Regulatory_Document__r.VTD1_Document_Type__c,
                        VTD1_Regulatory_Document__r.VTD1_Signature_Type__c
                FROM VTD1_Regulatory_Binder__c
                WHERE VTD1_Care_Plan_Template__c IN : studyIdToBindersMapPASignature.keySet()
                AND (VTD1_Regulatory_Document__r.VTD1_Document_Type__c =: VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_SIGNATURE_PAGE
                OR VTD1_Regulatory_Document__r.VTD1_Document_Type__c =: VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_PROTOCOL_AMENDMENT)];

        System.debug('binders = ' + binders);

        for (VTD1_Regulatory_Binder__c binder : binders) {
            if (binder.VTD1_Regulatory_Document__r.VTD1_Document_Type__c == VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_SIGNATURE_PAGE) {
                studyIdToBindersMapPASignature.put(binder.VTD1_Care_Plan_Template__c, binder);
            } else {
                studyIdToBindersMapPA.put(binder.VTD1_Care_Plan_Template__c, binder);
            }
        }

        Map <Id, List <Virtual_Site__c>> virtualSiteMap = new Map<Id, List<Virtual_Site__c>>();
        for (VTD1_Protocol_Amendment__c pa : recs) {
            if (studyIdToBindersMapPASignature.get(pa.VTD1_Study_ID__c) == null
                    || studyIdToBindersMapPA.get(pa.VTD1_Study_ID__c) == null) {
                pa.addError(new DmlException(Label.VTR4_NoSignaturePage));
            } else {
                virtualSiteMap.put(pa.VTD1_Study_ID__c, new List<Virtual_Site__c>());
            }
        }

        System.debug('virtualSiteMap = ' + virtualSiteMap);

        if (virtualSiteMap.isEmpty())
            return;

        List <Virtual_Site__c> sites = [SELECT Id, Name, VTD1_Study__c
                                        FROM Virtual_Site__c
                                        WHERE VTD1_Study__c IN : virtualSiteMap.keySet()];
        for (Virtual_Site__c site : sites) {
            virtualSiteMap.get(site.VTD1_Study__c).add(site);
        }

        List <VTD1_Document__c> documents = new List<VTD1_Document__c>();

        for (VTD1_Protocol_Amendment__c pa : recs) {
            List <Virtual_Site__c> sitesByStudy = virtualSiteMap.get(pa.VTD1_Study_ID__c);
            if (sitesByStudy == null)
                continue;
            VTD1_Regulatory_Binder__c binder = studyIdToBindersMapPASignature.get(pa.VTD1_Study_ID__c);
            for (Virtual_Site__c site : sitesByStudy) {
                documents.add(new VTD1_Document__c(
                    VTD1_Study__c = pa.VTD1_Study_ID__c,
                    VTD1_Site__c = site.Id,
                    VTD1_Regulatory_Binder__c = binder.Id,
                    VTD1_Protocol_Amendment__c = pa.Id,
                    VTR4_Signature_Type__c = binder.VTD1_Regulatory_Document__r.VTD1_Signature_Type__c,
                    RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT,
                        VTD1_Name__c = pa.Name + ' - ' + site.Name
                ));
            }
        }
        if (!documents.isEmpty())
            insert documents;
    }

    private void sendAmendmentNotificationsAndTasks (List<VTD1_Protocol_Amendment__c> recs, Map<Id, VTD1_Protocol_Amendment__c> oldMap) {
        List<Id> studies = new List<Id> ();
        List<Id> validAms = new List<Id> ();
        for (VTD1_Protocol_Amendment__c rec : recs) {
            if (rec.VTD1_Study_Admin_Setup_Complete__c && rec.VTD1_VTSL_Setup_Complete_Confirmation__c
                    && (!oldMap.get(rec.Id).VTD1_Study_Admin_Setup_Complete__c
                    || !oldMap.get(rec.Id).VTD1_VTSL_Setup_Complete_Confirmation__c)
                    && rec.VTD1_Study_ID__c != null) {
                studies.add(rec.VTD1_Study_ID__c);
                validAms.add(rec.Id);
            }
        }
        if (studies.isEmpty()) {
            return;
        }
        Map<Id, List<VTD1_Document__c>> siteDocumentMap = new Map<Id, List<VTD1_Document__c>>();
        for (VTD1_Document__c doc : [SELECT Id, VTD1_Site__c, VTD1_Protocol_Amendment__c  FROM VTD1_Document__c
                WHERE RecordTypeId = :VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT
                AND VTD1_Regulatory_Document_Type__c = :VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_SIGNATURE_PAGE
                AND VTR4_IRB_Approved__c = true AND VTD2_EAFStatus__c = :VT_R4_ConstantsHelper_Documents.DOCUMENT_EAF_STATUS_COMPLETED
                AND VTD1_Protocol_Amendment__c IN :validAms AND VTD1_Site__r.VTD1_Study__c IN :studies]) {
            List<VTD1_Document__c> docList = siteDocumentMap.get(doc.VTD1_Site__c);
            if (docList == null) {
                docList = new List<VTD1_Document__c> ();
            }
            docList.add(doc);
            siteDocumentMap.put(doc.VTD1_Site__c, docList);
        }
        if (!siteDocumentMap.isEmpty()) {
            checkReconsentFlagOnCases(siteDocumentMap.keySet());
            sendNotificationsAndTasksToTeamMembers(siteDocumentMap);
        }
    }

    public void checkReconsentFlagOnCases(Set<Id> siteIds) {
        VTR4_BatchFlagReconsentPatients batch = new VTR4_BatchFlagReconsentPatients(siteIds);
        Database.executeBatch(batch, 10);
    }

    public void sendNotificationsAndTasksToTeamMembers(Map<Id, List<VTD1_Document__c>> siteDocumentMap) {
        List<Study_Team_Member__c> stms = [
                SELECT
                        Id, VTD1_VirtualSite__c, (
                        SELECT
                                VTD1_Associated_PG__r.User__c
                        FROM Study_Team_Member_Relationships1__r
                ), (
                        SELECT
                                VTR2_Associated_SCr__r.User__c
                        FROM Study_Site_Team_Members1__r
                )
                FROM Study_Team_Member__c
                WHERE VTD1_VirtualSite__c IN :siteDocumentMap.keySet()
        ];
        if (stms.isEmpty()) {
            return;
        }
        String TN_CATALOG_CODE_FOR_TASK = 'T526';
        Map <String, String> assignedToMap = new Map<String, String>();
        List <Id> sourceIdsTasks = new List <Id>();

        String TN_CATALOG_CODE_FOR_NOTIFICATION = 'N562';
        Map <String, List<Id>> assignedToMapNotification = new Map<String,  List<Id>>();
        List <Id> sourceIdsNots = new List <Id>();

        for (Study_Team_Member__c stm : stms) {
            List<VTD1_Document__c> docList = siteDocumentMap.get(stm.VTD1_VirtualSite__c);
            for (VTD1_Document__c document : docList) {
                String taskKey = TN_CATALOG_CODE_FOR_TASK + document.Id;
                String notificationKey = TN_CATALOG_CODE_FOR_NOTIFICATION + document.Id;
                String assignedToMapValue = assignedToMap.get(taskKey) == null
                        ? '' : assignedToMap.get(taskKey);
                for (Study_Site_Team_Member__c stmPG : stm.Study_Team_Member_Relationships1__r) {
                    sourceIdsTasks.add(document.Id);
                    assignedToMapValue += (String.isEmpty(assignedToMapValue) ?'':',') + stmPG.VTD1_Associated_PG__r.User__c;
                    assignedToMap.put(taskKey, assignedToMapValue);
                }
                for (Study_Site_Team_Member__c stmSCR: stm.Study_Site_Team_Members1__r) {
                    //task
                    sourceIdsTasks.add(document.Id);
                    assignedToMapValue += (String.isEmpty(assignedToMapValue) ?'':',') + stmSCR.VTR2_Associated_SCr__r.User__c;
                    assignedToMap.put(taskKey, assignedToMapValue);
                    //notification
                    sourceIdsNots.add(document.Id);
                    List<Id> scrList = assignedToMapNotification.get(notificationKey);
                    if (scrList == null) {
                        scrList = new List<Id>();
                    }
                    scrList.add(stmSCR.VTR2_Associated_SCr__r.User__c);
                    assignedToMapNotification.put(notificationKey, scrList);
                }
            }
        }
        if (!sourceIdsTasks.isEmpty()) {
            VT_D2_TNCatalogTasks.generateTasks(new List<String>{TN_CATALOG_CODE_FOR_TASK},
                    sourceIdsTasks, null, assignedToMap);
        }
        System.debug(sourceIdsNots);
        System.debug(assignedToMapNotification);
        if (!sourceIdsNots.isEmpty()) {
            VT_D2_TNCatalogNotifications.generateNotifications(new List<String>{TN_CATALOG_CODE_FOR_NOTIFICATION},
                    sourceIdsNots, null, null, assignedToMapNotification);
        }
    }
}