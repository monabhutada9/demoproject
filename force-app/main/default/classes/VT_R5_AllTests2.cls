/**
 * Created by Igor Shamenok on 01-Oct-20.
 */
@IsTest
private with sharing class VT_R5_AllTests2 {
    @TestSetup
    static void setup() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'))
                .setOriginalName('tN')
                .setStudyAdminId(UserInfo.getUserId());

        DomainObjects.User_t piUser = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Primary Investigator')
                .setEmail(DomainObjects.RANDOM.getEmail());

        DomainObjects.Contact_t scrCon = new DomainObjects.Contact_t()
                .setLastName('Con1');
        DomainObjects.User_t scrUser = new DomainObjects.User_t()
                .addContact(scrCon)
                .setProfile('Site Coordinator');

//        DomainObjects.Phone_t phone = new DomainObjects.Phone_t(new DomainObjects.Account_t()
//                .setRecordTypeByName('Sponsor'), '345-6789')
//                .setType('Home')
//                .setPrimaryForPhone(false)
//                .addContact(scrCon);
//        phone.persist();

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Patient');
        DomainObjects.HealthCloudGA_CandidatePatient_t candidatePatient = new DomainObjects.HealthCloudGA_CandidatePatient_t()
                .setConversion('Converted')
                .setCaregiverEmail(DomainObjects.RANDOM.getEmail())
                .addStudy(study);
        candidatePatient.persist();
        Test.stopTest();
        DomainObjects.VirtualSite_t virtualSite = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .setStudySiteNumber('12345');
        DomainObjects.StudyTeamMember_t stmPI = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addVirtualSite(virtualSite)
                .addUser(piUser)
                .setRecordTypeByName('PI');
        stmPI.persist();
        System.debug('CASEES: ' + [SELECT count() FROM Case]);
    }

    @IsTest
    static void VT_D1_CaseByCurrentUserTest1() {
        VT_D1_CaseByCurrentUserTest.getExistingCaseSuccessTest();
    }
    @IsTest
    static void VT_D1_CaseByCurrentUserTest2() {
        VT_D1_CaseByCurrentUserTest.getNewCaseSuccessTest();
    }
    @IsTest
    static void VT_D1_PatientProfileIntialControllerTest() {
        VT_D1_PatientProfileIntialControllerTest.initialProfileTest();
    }
    @IsTest
    static void VT_R2_PatientHistoryHandlerTest() {
        VT_R2_PatientHistoryHandlerTest.firstTest();
    }
    @IsTest
    static void VT_R4_DeleteEDiaryNotificationsBatchTest() {
        VT_R4_DeleteEDiaryNotificationsBatchTest.testBatch();
    }
    @IsTest
    static void VT_R4_PatientFieldLockServiceTest1() {
        VT_R4_PatientFieldLockServiceTest.testVerifyLockedFieldsSoloPatientSet();
    }
    @IsTest
    static void VT_R4_PatientFieldLockServiceTest2() {
        VT_R4_PatientFieldLockServiceTest.testVerifyLockedFieldsPatientSetWithUser();
    }
    @IsTest
    static void VT_R4_PatientFieldLockServiceTest3() {
        VT_R4_PatientFieldLockServiceTest.testVerifyLockedFieldsSoloPatient();
    }
    @IsTest
    static void VT_R4_PatientFieldLockServiceTest4() {
        VT_R4_PatientFieldLockServiceTest.testVerifyLockedFieldsSoloPatientWithUser();
    }
    @IsTest
    static void VT_R4_PatientFieldLockServiceTest5() {
        VT_R4_PatientFieldLockServiceTest.testVerifyLockedFieldsEmptyValues();
    }
    @IsTest
    static void VT_R4_PatientFieldLockServiceTest6() {
        VT_R4_PatientFieldLockServiceTest.testUpdateRestrictedFieldByPgSuccess();
    }
    @IsTest
    static void VT_R4_PatientFieldLockServiceTest7() {
        VT_R4_PatientFieldLockServiceTest.testUpdateRestrictedFieldByPgFail();
    }
    @IsTest
    static void VT_R4_PatientFieldLockServiceTest8() {
        VT_R4_PatientFieldLockServiceTest.testUpdateRestrictedFieldByAdminSuccess();
    }
    @IsTest
    static void VT_R4_PatientFieldLockServiceTest9() {
        VT_R4_PatientFieldLockServiceTest.testUpdateHistoricalDataSuccess();
    }
    @IsTest
    static void VT_R4_PatientFieldLockServiceTest10() {
        VT_R4_PatientFieldLockServiceTest.UserChangeEmailConfirmationTriggerHandlerWithOutCaregiversTest();
    }
    @IsTest
    static void VT_R4_PatientFieldLockServiceTest11() {
        VT_R4_PatientFieldLockServiceTest.UserChangeEmailConfirmationTriggerHandlerCaregiverTest();
    }
    @IsTest
    static void VT_R4_TransferPatientControllerTest1() {
        VT_R4_TransferPatientControllerTest.testGetAllStudies();
    }
    @IsTest
    static void VT_R4_TransferPatientControllerTest2() {
        VT_R4_TransferPatientControllerTest.testFindAvailablePIs();
    }
    @IsTest
    static void VT_R4_TransferPatientControllerTest3() {
        VT_R4_TransferPatientControllerTest.testPrimaryInvestigatorEquality();
    }
    @IsTest
    static void VT_R4_TransferPatientControllerTest4() {
        VT_R4_TransferPatientControllerTest.testFindCriteria();
    }
    @IsTest
    static void VT_R4_TransferPatientControllerTest5() {
        VT_R4_TransferPatientControllerTest.testGetContact();
    }
    @IsTest
    static void VT_R4_TransferPatientControllerTest6() {
        VT_R4_TransferPatientControllerTest.testSaveCandidatePatient();
    }
    @IsTest
    static void VT_R4_TransferPatientControllerTest7() {
        VT_R4_TransferPatientControllerTest.createCandidatePatientTest();
    }
    @IsTest
    static void VTR2_SCRMyPatientFullProfileTest1() {
        VTR2_SCRMyPatientFullProfileTest.getFullProfileTest();
    }
    @IsTest
    static void VTR2_SCRMyPatientFullProfileTest2() {
        VTR2_SCRMyPatientFullProfileTest.updateProfileSuccessTest();
    }
    @IsTest
    static void VTR2_SCRMyPatientFullProfileTest3() {
        VTR2_SCRMyPatientFullProfileTest.saveCaregiverInDBTest();
    }
    @IsTest
    static void VTR2_SCRMyPatientFullProfileTest4() {
        VTR2_SCRMyPatientFullProfileTest.addNewPhysicianTest();
    }
    @IsTest
    static void VTR2_SCRMyPatientFullProfileTest5() {
        VTR2_SCRMyPatientFullProfileTest.checkSuccessEditableFields();
    }
    @IsTest
    static void VT_D1_Test_PatientCaregiverBound() {
        VT_D1_Test_PatientCaregiverBound.test();
    }
}