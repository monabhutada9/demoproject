/**
 * Created by Galiya Khalikova on 10.10.2019.
 */

public with sharing class VT_R3_Geography_STM_Handler extends Handler{

    public override void onBeforeDelete(Handler.TriggerContext context) {
        DeleteValidator validator = new DeleteValidator(context);
        validator.validate();
    }

    public override void onAfterInsert(Handler.TriggerContext context) {
        PrimaryRegulatorySpecialistManager manager = new PrimaryRegulatorySpecialistManager(context);
        manager.insertPrimaryRegulatorySpecialist();
    }

    public override void onAfterUpdate(Handler.TriggerContext context) {
        PrimaryRegulatorySpecialistManager manager = new PrimaryRegulatorySpecialistManager(context);
        manager.updatePrimaryRegulatorySpecialist();

        AfterUpdateValidator validator = new AfterUpdateValidator(context);
        validator.validate();
    }


    private abstract class Validator {

        private Map<Id, GeographySTM> geographySTMMap;
        private List<ValidationRule> validatorRules;

        public Validator(Handler.TriggerContext context) {
            this.geographySTMMap = this.getSiteGeography(context);
            this.validatorRules = new List<ValidationRule>();
        }

        public void validate() {
            for (ValidationRule validator : this.validatorRules) {
                for (GeographySTM geographySTMs : this.geographySTMMap.values()) {
                    validator.validate(geographySTMs);
                }
            }
        }


        protected abstract Set<Id> getSiteGeographyIds(Handler.TriggerContext context);

        protected abstract Map<Id, GeographySTM> addTriggerContext(Map<Id, GeographySTM> geographySTMs, Handler.TriggerContext context);


        private Map<Id, GeographySTM> getSiteGeography(TriggerContext context) {

            Map<Id, GeographySTM> result = new Map<Id, GeographySTM>();
            Set<Id> changedGeographySTMIds = this.getSiteGeographyIds(context);

            if (!changedGeographySTMIds.isEmpty()) {
                List<VTR3_Geography_STM__c> dbGeographySTMs = this.getDatabaseSiteGeography(changedGeographySTMIds);
                result = this.convertGeographySTMTogSTMs(dbGeographySTMs);
                result = this.addTriggerContext(result, context);
            }

            return result;
        }

        private List<VTR3_Geography_STM__c> getDatabaseSiteGeography(Set<Id> changedGeographySTMIds) {
            return [
                    SELECT Id
                            , Is_Primary__c
                            , Study_Geography__c
                            ,RecordTypeId
                            ,RecordType.DeveloperName 

                    FROM VTR3_Geography_STM__c
                    WHERE Study_Geography__c IN :changedGeographySTMIds

            ];
        }

        private Map<Id, GeographySTM> convertGeographySTMTogSTMs(List<VTR3_Geography_STM__c> dbGSTMs) {

            Map<Id, GeographySTM> geographySTMs = new Map<Id, GeographySTM>();

            for (VTR3_Geography_STM__c gSTM : dbGSTMs) {
                Id gSTMId = gSTM.Study_Geography__c;
                if (!geographySTMs.containsKey(gSTMId)) {
                    geographySTMs.put(gSTMId, new GeographySTM(gSTMId));
                }
                geographySTMs.get(gSTMId).addDbGeography(gSTM);
            }

            return geographySTMs;
        }

    }

    private class AfterUpdateValidator extends Validator {

        public AfterUpdateValidator(Handler.TriggerContext context) {
            super(context);
            this.validatorRules.add(new AfterUpdatePrimaryRSValidationRule());

        }

        protected override Set<Id> getSiteGeographyIds(TriggerContext context) {

            Set<Id> changedGeographySTMIds = new Set<Id>();
            List<VTR3_Geography_STM__c> oldList = (List<VTR3_Geography_STM__c>) context.oldList;
            Map<Id, VTR3_Geography_STM__c> newMap = (Map<Id, VTR3_Geography_STM__c>) context.newMap;

            for (VTR3_Geography_STM__c old : oldList) {
                VTR3_Geography_STM__c newGSTM = newMap.get(old.Id);
                changedGeographySTMIds.add(newGSTM.Study_Geography__c);
                changedGeographySTMIds.add(old.Study_Geography__c);
            }

            return changedGeographySTMIds;
        }

        protected override Map<Id, GeographySTM> addTriggerContext(Map<Id, GeographySTM> geographySTMs, Handler.TriggerContext context) {

            for (VTR3_Geography_STM__c gSTM : (List<VTR3_Geography_STM__c>) context.newList) {
                Id gSTMId = gSTM.Study_Geography__c;
                if (!geographySTMs.containsKey(gSTMId)) {
                    geographySTMs.put(gSTMId, new GeographySTM(gSTMId));
                }
                geographySTMs.get(gSTMId).addTriggerGeography(gSTM);
            }

            for (VTR3_Geography_STM__c oldList : (List<VTR3_Geography_STM__c>) context.oldList) {
                VTR3_Geography_STM__c newGeographySTMMap = (VTR3_Geography_STM__c) context.newMap.get(oldList.Id);
                Id gSTMId = oldList.Study_Geography__c;
                if (!geographySTMs.containsKey(gSTMId)) {
                    geographySTMs.put(gSTMId, new GeographySTM(gSTMId));
                }
                geographySTMs.get(gSTMId).addTriggerGeography(newGeographySTMMap);
            }

            return geographySTMs;
        }

    }

    private class DeleteValidator extends Validator {

        public DeleteValidator(Handler.TriggerContext context) {
            super(context);
            this.validatorRules.add(new DeletePrimaryRSValidationRule());
        }

        protected override Set<Id> getSiteGeographyIds(TriggerContext context) {

            Set<Id> changedGeographySTMIds = new Set<Id>();
            for (VTR3_Geography_STM__c oldList : (List<VTR3_Geography_STM__c>) context.oldList) {
                if (oldList.Is_Primary__c) {
                    changedGeographySTMIds.add(oldList.Study_Geography__c);
                }
            }

            return changedGeographySTMIds;
        }

        protected override Map<Id, GeographySTM> addTriggerContext(Map<Id, GeographySTM> geographySTMs, Handler.TriggerContext context) {

            for (VTR3_Geography_STM__c gSTM : (List<VTR3_Geography_STM__c>) context.oldList) {
                Id siteId = gSTM.Study_Geography__c;
                if (!geographySTMs.containsKey(siteId)) {
                    geographySTMs.put(siteId, new GeographySTM(siteId));
                }
                geographySTMs.get(siteId).addTriggerGeography(gSTM);
            }

            return geographySTMs;
        }

    }


    public interface ValidationRule {
        Boolean validate(GeographySTM gSTMs);
    }

    public class AfterUpdatePrimaryRSValidationRule implements ValidationRule {

        public Boolean validate(GeographySTM geographySTMs) {
            Boolean isValid = false;
            String error;

            Integer dbPrimaryQuantity = geographySTMs.getDbPrimaryQuantity();

            if (dbPrimaryQuantity == 0) {
                error = Label.VTR3_GSTM_Should_Have_Primary_RS;
            }

            Integer vtslPrimaryQuantity = geographySTMs.getVTSLPrimaryQuantity();

            
            if (vtslPrimaryQuantity == 0) {
                error = Label.VTR5_GSTM_Should_Have_Primary_VTSL;
            }

            if (error != null) {
                geographySTMs.setErrorToRecords(error);
            } else {
                isValid = true;
            }
            return isValid;
        }
    }

    public class DeletePrimaryRSValidationRule implements ValidationRule {

        public Boolean validate(GeographySTM geographySTMs) {

            Boolean isValid = false;
            String error;

            Integer triggerPrimaryQuantityRS = geographySTMs.getTriggerPrimaryQuantityRS();
            Integer triggerPrimaryQuantityVTSL = geographySTMs.getTriggerPrimaryQuantityVTSL();
            system.debug(logginglevel.error, 'triggerPrimaryQuantityRS '+triggerPrimaryQuantityRS);
            system.debug(logginglevel.error, 'triggerPrimaryQuantityVTSL '+triggerPrimaryQuantityVTSL);

            if (triggerPrimaryQuantityRS != 0&&triggerPrimaryQuantityRS!=null) {
                error = Label.VTR3_GSTM_Should_Have_Primary_RS;
                geographySTMs.setErrorToRecords(error);
            }if (triggerPrimaryQuantityVTSL != 0&&triggerPrimaryQuantityVTSL!=null){
                error = Label.VTR5_GSTM_Should_Have_Primary_VTSL;
                geographySTMs.setErrorToRecords(error);
            } else {
                isValid = true;
            }
            system.debug(logginglevel.error, 'isValid '+isValid);
            return isValid;
        }
    }

    private class PrimaryRegulatorySpecialistManager {

        Handler.TriggerContext context;

        public PrimaryRegulatorySpecialistManager(Handler.TriggerContext context) {
            this.context = context;
        }

        private void insertPrimaryRegulatorySpecialist() {

            List<VTR3_Geography_STM__c> newList = this.context.newList;
            Set<Id> studyGeographyId = new Set<Id>();
            Map<Id, Id> geographyToPrimaryMapRS = new Map<Id, Id>();
            Map<Id, Id> geographyToPrimaryMapVTSL = new Map<Id, Id>();
            Id RS_RecType_Id = Schema.SObjectType.VTR3_Geography_STM__c.getRecordTypeInfosByDeveloperName().get('RS').getRecordTypeId();
            Id VTSL_RecType_Id = Schema.SObjectType.VTR3_Geography_STM__c.getRecordTypeInfosByDeveloperName().get('VTSL').getRecordTypeId();

            
            List<VTR3_Geography_STM__c> rsToUpdate = new List<VTR3_Geography_STM__c>();
            List<VTR3_Geography_STM__c> vtslToUpdate = new List<VTR3_Geography_STM__c>();

            List<VTR3_Geography_STM__c> stmsToUpdate = new List<VTR3_Geography_STM__c>();
            Set<Id> triggerIds = new Map<Id, VTR3_Geography_STM__c>(newList).keySet();

            for (VTR3_Geography_STM__c gstm : newList) {
                studyGeographyId.add(gstm.Study_Geography__c);
            }

            for (VTR3_Geography_STM__c gstm : [
                    SELECT Id
                            ,Study_Geography__c,
                            Is_Primary__c,
                            RecordTypeId,
                            RecordType.DeveloperName 
                    FROM VTR3_Geography_STM__c
                    WHERE Study_Geography__c IN:studyGeographyId
                    AND Is_Primary__c = true
                    AND Id NOT IN :triggerIds
            ]) {
                if(gstm.RecordType.DeveloperName=='RS'||gstm.RecordTypeId==null){
                        geographyToPrimaryMapRS.put(gstm.Study_Geography__c, gstm.Id);
                }else if(gstm.RecordType.DeveloperName=='VTSL'){
                       geographyToPrimaryMapVTSL.put(gstm.Study_Geography__c, gstm.Id);

                }
            }

            for (VTR3_Geography_STM__c gstm : newList) {

                Id currentPrimaryRSId = geographyToPrimaryMapRS.get(gstm.Study_Geography__c);
                Id currentPrimaryVTSLId = geographyToPrimaryMapVTSL.get(gstm.Study_Geography__c);

                if (gstm.Is_Primary__c) {
                    if(gstm.RecordTypeId==RS_RecType_Id||gstm.RecordTypeId==null){
                        if (currentPrimaryRSId != null) {
                             if (triggerIds.contains(currentPrimaryRSId)) {
                                rsToUpdate.add(new VTR3_Geography_STM__c(Id = gstm.Id, Is_Primary__c = false));
                            } else {
                                rsToUpdate.add(new VTR3_Geography_STM__c(Id = currentPrimaryRSId, Is_Primary__c = false));
                                    geographyToPrimaryMapRS.put(gstm.Study_Geography__c, gstm.Id);
                            }
                        }
                    }else if(gstm.RecordTypeId==VTSL_RecType_Id){
                        if (currentPrimaryVTSLId != null) {
                            if (triggerIds.contains(currentPrimaryVTSLId)) {
                                   vtslToUpdate.add(new VTR3_Geography_STM__c(Id = gstm.Id, Is_Primary__c = false));
                           } else {
                                   vtslToUpdate.add(new VTR3_Geography_STM__c(Id = currentPrimaryVTSLId, Is_Primary__c = false));
                                   geographyToPrimaryMapVTSL.put(gstm.Study_Geography__c, gstm.Id);
                           }
                       }
                    }
                }

                if (!gstm.Is_Primary__c && currentPrimaryRSId == null&&(gstm.RecordTypeId==RS_RecType_Id||gstm.RecordTypeId==null)) {
                    rsToUpdate.add(new VTR3_Geography_STM__c(Id = gstm.Id, Is_Primary__c = true));
                    geographyToPrimaryMapRS.put(gstm.Study_Geography__c, gstm.Id);
                }

                if (!gstm.Is_Primary__c && currentPrimaryVTSLId == null&&gstm.RecordTypeId==VTSL_RecType_Id) {
                    vtslToUpdate.add(new VTR3_Geography_STM__c(Id = gstm.Id, Is_Primary__c = true));
                    geographyToPrimaryMapVTSL.put(gstm.Study_Geography__c, gstm.Id);
                }
            }
            if(vtslToUpdate.size()>0)stmsToUpdate.addAll(vtslToUpdate);
            if(rsToUpdate.size()>0)stmsToUpdate.addAll(rsToUpdate);

            if (!stmsToUpdate.isEmpty()) {
                system.debug(logginglevel.error,'stmsToUpdate '+stmsToUpdate);
                update stmsToUpdate;
            }
        }

        private void updatePrimaryRegulatorySpecialist() {
            Id RS_RecType_Id = Schema.SObjectType.VTR3_Geography_STM__c.getRecordTypeInfosByDeveloperName().get('RS').getRecordTypeId();
            Id VTSL_RecType_Id = Schema.SObjectType.VTR3_Geography_STM__c.getRecordTypeInfosByDeveloperName().get('VTSL').getRecordTypeId();
            Set<Id> newPrimariesRS = new Set<Id>();
            Set<Id> newPrimariesVTSL = new Set<Id>();

            Set<Id> studyGeographyId = new Set<Id>();
            List<VTR3_Geography_STM__c> oldList = this.context.oldList;
            Map<Id, VTR3_Geography_STM__c> newMap = (Map<Id, VTR3_Geography_STM__c>) this.context.newMap;

            for (VTR3_Geography_STM__c oldRegulatorySpecialist : oldList) {
                VTR3_Geography_STM__c newRegulatorySpecialist = newMap.get(oldRegulatorySpecialist.Id);
                if (!oldRegulatorySpecialist.Is_Primary__c && newRegulatorySpecialist.Is_Primary__c) {
                    if(oldRegulatorySpecialist.RecordTypeId==null
                    ||oldRegulatorySpecialist.RecordTypeId==RS_RecType_Id){
                            newPrimariesRS.add(newRegulatorySpecialist.Id);
                    }else if(oldRegulatorySpecialist.RecordTypeId==VTSL_RecType_Id){
                            newPrimariesVTSL.add(newRegulatorySpecialist.Id);
                    }
                    system.debug(logginglevel.error,'newPrimariesRS '+newPrimariesRS);
                    system.debug('newPrimariesVTSL '+newPrimariesVTSL);
                    studyGeographyId.add(newRegulatorySpecialist.Study_Geography__c);
                }
            }
            List<VTR3_Geography_STM__c> regulatorySpecialistToUpdate =new List<VTR3_Geography_STM__c>();
            List<VTR3_Geography_STM__c> VTSLSpecialistToUpdate =new List<VTR3_Geography_STM__c>();
            List<VTR3_Geography_STM__c> GSTMsToUpdate =new List<VTR3_Geography_STM__c>();

            if(newPrimariesRS.size()>0){
                regulatorySpecialistToUpdate = [
                    SELECT Id
                            ,Name
                            , Is_Primary__c
                            , RecordTypeId
                    FROM VTR3_Geography_STM__c
                    WHERE Id NOT IN :newPrimariesRS
                    AND (RecordType.DeveloperName='RS' 
                    OR RecordTypeId=NULL) 
                    AND Study_Geography__c IN :studyGeographyId
            ];
            

                for (VTR3_Geography_STM__c regulatorySpecialist : regulatorySpecialistToUpdate) {
                    regulatorySpecialist.Is_Primary__c = false;
                    GSTMsToUpdate.add(regulatorySpecialist);
                }

            }

            if(newPrimariesVTSL.size()>0){
                VTSLSpecialistToUpdate = [
                    SELECT Id
                            ,Name
                            , Is_Primary__c
                            ,RecordTypeId 
                    FROM VTR3_Geography_STM__c
                    WHERE Id NOT IN :newPrimariesVTSL
                    AND RecordType.DeveloperName='VTSL' 
                    AND Study_Geography__c IN :studyGeographyId
            ];
            

                for (VTR3_Geography_STM__c vtslSpecialist : VTSLSpecialistToUpdate) {
                    vtslSpecialist.Is_Primary__c = false;
                    GSTMsToUpdate.add(vtslSpecialist);
                }

            }

            update GSTMsToUpdate;
        }
    }


    private class GeographySTM {

        private final Id studyGeographyId;
        private final Map<Id, VTR3_Geography_STM__c> dbGeography_STMs;
        private final Map<Id, VTR3_Geography_STM__c> vtslGeography_STMs;

        private final List<VTR3_Geography_STM__c> triggerGeography_STMs;
        private final List<VTR3_Geography_STM__c> triggerGeographyRS_STMs;
        private final List<VTR3_Geography_STM__c> triggerGeographyVTSL_STMs;
        private final Id VTSL_RecType_Id;
        private final Id RS_RecType_Id;
        
        public GeographySTM(Id studyGeographyId) {
            this.VTSL_RecType_Id = Schema.SObjectType.VTR3_Geography_STM__c.getRecordTypeInfosByDeveloperName().get('VTSL').getRecordTypeId();
            this.RS_RecType_Id = Schema.SObjectType.VTR3_Geography_STM__c.getRecordTypeInfosByDeveloperName().get('RS').getRecordTypeId();
            this.studyGeographyId = studyGeographyId;
            this.dbGeography_STMs = new Map<Id, VTR3_Geography_STM__c>();
            this.vtslGeography_STMs = new Map<Id, VTR3_Geography_STM__c>();
            this.triggerGeography_STMs = new List<VTR3_Geography_STM__c>();
            this.triggerGeographyRS_STMs = new List<VTR3_Geography_STM__c>();
            this.triggerGeographyVTSL_STMs = new List<VTR3_Geography_STM__c>();

        }

        private void addDbGeography(VTR3_Geography_STM__c geographySTM) {
          //  Id VTSL_RecType_Id = Schema.SObjectType.VTR3_Geography_STM__c.getRecordTypeInfosByDeveloperName().get('VTSL').getRecordTypeId();
          //  Id RS_RecType_Id = Schema.SObjectType.VTR3_Geography_STM__c.getRecordTypeInfosByDeveloperName().get('RS').getRecordTypeId();

            if(geographySTM.RecordTypeId==null||geographySTM.RecordTypeId==RS_RecType_Id){
                dbGeography_STMs.put(geographySTM.Id, geographySTM);
            }else if(geographySTM.RecordTypeId==VTSL_RecTYpe_Id){
                vtslGeography_STMs.put(geographySTM.Id, geographySTM);

            }
        }

        private void addTriggerGeography(VTR3_Geography_STM__c geographySTM) {
            triggerGeography_STMs.add(geographySTM);
            if(geographySTM.RecordTypeId==null||geographySTM.RecordTypeId==RS_RecType_Id){
                    triggerGeographyRS_STMs.add(geographySTM);

            }else if(geographySTM.RecordTypeId==VTSL_RecType_Id){
                    triggerGeographyVTSL_STMs.add(geographySTM);

            }

        }

        private Integer getDbPrimaryQuantity() {
            Id RS_RecType_Id = Schema.SObjectType.VTR3_Geography_STM__c.getRecordTypeInfosByDeveloperName().get('RS').getRecordTypeId();
            Integer quantityN;
            List <VTR3_Geography_STM__c>gstmtoValidate = new List<VTR3_Geography_STM__c>();
            for(VTR3_Geography_STM__c gstm:this.dbGeography_STMs.values()){
                if(gstm.RecordTypeID==null||gstm.RecordTypeId==RS_RecType_Id){
                    gstmtoValidate.add(gstm);
                }
            }
            system.debug(logginglevel.Error, 'gstmtoValidate '+gstmtoValidate);
            
            if(gstmtoValidate.size()>0){
                quantityN=getPrimaryQuantity(gstmtoValidate);
            } 
            //return getPrimaryQuantity(gstmtoValidate);
            return quantityN;
        }
        private Integer getVTSLPrimaryQuantity() {
            Integer quantityN;

            Id VTSL_RecType_Id = Schema.SObjectType.VTR3_Geography_STM__c.getRecordTypeInfosByDeveloperName().get('VTSL').getRecordTypeId();
            List <VTR3_Geography_STM__c>gstmtoValidate = new List<VTR3_Geography_STM__c>();
            for(VTR3_Geography_STM__c gstm:this.vtslGeography_STMs.values()){
                if(gstm.RecordTypeId==VTSL_RecType_Id){
                    gstmtoValidate.add(gstm);
                }
            }
            system.debug(logginglevel.Error, 'gstmtoValidate '+gstmtoValidate);
            if(gstmtoValidate.size()>0){
                quantityN=getPrimaryQuantity(gstmtoValidate);
            } 
           // return getPrimaryQuantity(gstmtoValidate);
           return quantityN;
        }

        private Integer getTriggerPrimaryQuantity() {
            return getPrimaryQuantity(this.triggerGeography_STMs);
        }
        private Integer getTriggerPrimaryQuantityRS() {
            return getPrimaryQuantity(this.triggerGeographyRS_STMs);
        }
        private Integer getTriggerPrimaryQuantityVTSL() {
            return getPrimaryQuantity(this.triggerGeographyVTSL_STMs);
        }

        private Integer getPrimaryQuantity(List<VTR3_Geography_STM__c> gSTMs) {
            Integer quantity = 0;
            for (VTR3_Geography_STM__c gSTM : gSTMs) {
                if (gSTM.Is_Primary__c && gSTM.Study_Geography__c != null) {
                    quantity++;
                }
            }

            return quantity;
        }
       private Integer getPrimaryQuantityRS(List<VTR3_Geography_STM__c> gSTMs) {
        Id RS_RecType_Id = Schema.SObjectType.VTR3_Geography_STM__c.getRecordTypeInfosByDeveloperName().get('RS').getRecordTypeId();

            Integer quantity;
            for (VTR3_Geography_STM__c gSTM : gSTMs) {
                if (gSTM.Is_Primary__c && gSTM.Study_Geography__c != null&&gstm.RecordTypeId==RS_RecType_Id) {
                   if(quantity==null)quantity=0;
                    quantity++;
                }
            }

            return quantity;
        }
        private Integer getPrimaryQuantityVTSL(List<VTR3_Geography_STM__c> gSTMs) {
            Id VTSL_RecType_Id = Schema.SObjectType.VTR3_Geography_STM__c.getRecordTypeInfosByDeveloperName().get('VTSL').getRecordTypeId();

            Integer quantity;
            for (VTR3_Geography_STM__c gSTM : gSTMs) {
                if (gSTM.Is_Primary__c && gSTM.Study_Geography__c != null&&gstm.RecordTypeId==VTSL_recType_Id) {
                    if(quantity==null)quantity=0;
                    quantity++;
                }
            }

            return quantity;
        }

        private void setErrorToRecords(String error) {
            for (VTR3_Geography_STM__c rs : this.triggerGeography_STMs) {
                rs.addError(error);
            }
        }
    }

}