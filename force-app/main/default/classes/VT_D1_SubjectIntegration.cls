/******************************************************
 * Created by Elena Shane
 * Changed by Leonid Bartenev, Jane Ivanova, Ivan Matskevich
 ******************************************************/

public with sharing class VT_D1_SubjectIntegration {
    
    public class SubjectIntegrationException extends Exception {}
    
    public static final String SUBJECT_SERVICE_URL = 'callout:Mulesoft_Subject';
    public static final String SUBJECT_STATUS_SERVICE_URL = 'callout:Mulesoft_Subject_Status';
    public static final String INTEGRATION_ACTION_CREATE_SUBJECT = 'Create Subject';
    public static final String INTEGRATION_ACTION_UPDATE_SUBJECT = 'Update Subject';
    public static final String INTEGRATION_ACTION_UPSERT_SUBJECT = 'Upsert Subject';
    public static final String INTEGRATION_ACTION_SUBJECT_STATUS = 'Subject Status';
    public static final String INTEGRATION_ACTION_RANDOMIZE_SUBJECT = 'Randomize Subject';
    public static final String ENDPOINT_URL_SUBJECT = SUBJECT_SERVICE_URL + '/subject';
    public static final String ENDPOINT_URL_SUBJECT_RANDOMIZE = SUBJECT_SERVICE_URL + '/subject/randomization';
    public static final String ENDPOINT_URL_SUBJECT_STATUS = SUBJECT_STATUS_SERVICE_URL + '/subject/status';
    public static final String POST_METHOD = 'POST';
    public static final String PUT_METHOD = 'PUT';
/*    public static final String PATCH_METHOD = 'PATCH';    */
    
    public class SubjectIntegrationResponseMessage {
        public String protocolId;
        public String subjectId;
        public String message;
        public String status;
        public Integer statusCode;
    }
    /*
    public class UpsertSubjectResponseMessage {
        public String protocolId;
        public String subjectId;
        public String message;
        public String status;
        public Integer statusCode;
    }
    */
    public class RandomizeSubjectResponseMessage {
        public String protocolId;
        public String subjectId;
        public String randomizationId;
        public String message;
        public String status;
        public Integer statusCode;
    }
    
//   public class UpdateSubjectStatusResponse {
//        public UpdateSubjectStatusResponseMessage subject;
//        public HttpResponse httpResponse;
//        public VTD1_Integration_Log__c log;
//        
//        public UpdateSubjectStatusResponse(VT_D1_HTTPConnectionHandler.Result result) {
//            log = result.log;
//            httpResponse = result.httpResponse;
//            if (httpResponse != null) {
//                String body = httpResponse.getBody().replace('\n', ' ').replace('\r', ' ').replace('\r\n', ' ');
//                subject = new UpdateSubjectStatusResponseMessage();
//                try{
//                    subject = (UpdateSubjectStatusResponseMessage) JSON.deserialize(body, UpdateSubjectStatusResponseMessage.class);
//                }catch(Exception e){
//                    String errorMessage = e.getMessage() + '\n' + e.getStackTraceString();
//                    log.VTD1_ErrorMessage__c = errorMessage.mid(0, 32768);
//                    update log;
//                }
//                subject.status = httpResponse.getStatus();
//                subject.statusCode = httpResponse.getStatusCode();
//            }
//        }
//    }

    /*
    public class UpsertSubjectResponse {
        public UpsertSubjectResponseMessage subject;
        public HttpResponse httpResponse;
        public VTD1_Integration_Log__c log;
        
        public UpsertSubjectResponse(VT_D1_HTTPConnectionHandler.Result result) {
            log = result.log;
            httpResponse = result.httpResponse;
            if (httpResponse != null) {
                String body = httpResponse.getBody().replace('\n', ' ').replace('\r', ' ').replace('\r\n', ' ');
                subject = new UpsertSubjectResponseMessage();
                try{
                    subject = (UpsertSubjectResponseMessage) JSON.deserialize(body, UpsertSubjectResponseMessage.class);
                }catch(Exception e){
                    String errorMessage = e.getMessage() + '\n' + e.getStackTraceString();
                    log.VTD1_ErrorMessage__c = errorMessage.mid(0, 32768);
                    update log;
                }
                subject.status = httpResponse.getStatus();
                subject.statusCode = httpResponse.getStatusCode();
            }
        }
    }
    */

    public class RandomizeSubjectResponse {
        public RandomizeSubjectResponseMessage subject;
        public HttpResponse httpResponse;
        public VTD1_Integration_Log__c log;
        
        public RandomizeSubjectResponse(VT_D1_HTTPConnectionHandler.Result result) {
            log = result.log;
            httpResponse = result.httpResponse;
            if (httpResponse != null) {
                String body = httpResponse.getBody().replace('\n', ' ').replace('\r', ' ').replace('\r\n', ' ');
                subject = new RandomizeSubjectResponseMessage();
                try{
                    subject = (RandomizeSubjectResponseMessage) JSON.deserialize(body, RandomizeSubjectResponseMessage.class);
                }catch(Exception e){
                    String errorMessage = e.getMessage() + '\n' + e.getStackTraceString();
                    log.VTD1_ErrorMessage__c = errorMessage.mid(0, 32768);
                    update log;
                }
                subject.status = httpResponse.getStatus();
                subject.statusCode = httpResponse.getStatusCode();
            }
        }
    }   
    
    public class SubjectIntegrationResponse {
        public SubjectIntegrationResponseMessage subject;
        public HttpResponse httpResponse;
        public VTD1_Integration_Log__c log;
        
        public SubjectIntegrationResponse(VT_D1_HTTPConnectionHandler.Result result) {
            log = result.log;
            httpResponse = result.httpResponse;
            if (httpResponse != null) {
                String body = httpResponse.getBody().replace('\n', ' ').replace('\r', ' ').replace('\r\n', ' ');
                subject = new SubjectIntegrationResponseMessage();
                try{
                    subject = (SubjectIntegrationResponseMessage) JSON.deserialize(body, SubjectIntegrationResponseMessage.class);
                }catch(Exception e){
                    String errorMessage = e.getMessage() + '\n' + e.getStackTraceString();
                    log.VTD1_ErrorMessage__c = errorMessage.mid(0, 32768);
                    update log;
                }
                subject.status = httpResponse.getStatus();
                subject.statusCode = httpResponse.getStatusCode();
            }
        }
    }
    
    
    // Send status update to RR
    public static SubjectIntegrationResponse sendSubjectStatus(Id caseId){    
            return new SubjectIntegrationResponse(VT_D1_HTTPConnectionHandler.send(
                    PUT_METHOD,
                    ENDPOINT_URL_SUBJECT_STATUS,
                    new VT_D2_RequestBuilder_SendSubjectStatus(caseId),
                    INTEGRATION_ACTION_SUBJECT_STATUS
            ));          
    }
    
    // Create/update subject callout: ----------------------------------------------------------------------------------
    //public static VT_D1_HTTPConnectionHandler.Result upsertSubject(Id caseId){
    //public static UpsertSubjectResponse upsertSubject(Id caseId){
    public static SubjectIntegrationResponse upsertSubject(Id caseId){
        VT_D1_RequestBuilder_SendSubject requestBuilder = new VT_D1_RequestBuilder_SendSubject(caseId);
        if(requestBuilder.c.VTD1_CreatedInCenduitCSM__c){
            // Update
            return new SubjectIntegrationResponse(VT_D1_HTTPConnectionHandler.send(
                    PUT_METHOD,
                    ENDPOINT_URL_SUBJECT,
                    requestBuilder,
                    INTEGRATION_ACTION_UPDATE_SUBJECT
            ));
        }else{
            // Create
            return new SubjectIntegrationResponse(VT_D1_HTTPConnectionHandler.send(
                    POST_METHOD,
                    ENDPOINT_URL_SUBJECT,
                    requestBuilder,
                    INTEGRATION_ACTION_CREATE_SUBJECT
            ));
            //return new UpsertSubjectResponse(result);
        }
    }
    
    // Randomize callout: ----------------------------------------------------------------------------------------------
    public static RandomizeSubjectResponse randomizeSubject(Id caseId) {
        VT_D1_HTTPConnectionHandler.Result result = VT_D1_HTTPConnectionHandler.send(
                POST_METHOD,
                ENDPOINT_URL_SUBJECT_RANDOMIZE,
                new VT_D1_RequestBuilder_Randomize(caseId),
                INTEGRATION_ACTION_RANDOMIZE_SUBJECT
        );
        return new RandomizeSubjectResponse(result);
    }
    
    
}