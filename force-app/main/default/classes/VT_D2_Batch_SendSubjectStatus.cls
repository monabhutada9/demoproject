/**
 * Created by Leonid Bartenev
 */

public class VT_D2_Batch_SendSubjectStatus implements Database.Batchable<SObject>, Database.AllowsCallouts {
    
    private Set<Id> caseIds;
    
    public VT_D2_Batch_SendSubjectStatus(Set<Id> caseIds){
        this.caseIds = caseIds;
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
                SELECT Id, Status
                FROM Case
                WHERE Id IN:caseIds
        ]);
    }
    
    public void execute(Database.BatchableContext param1, List<Case> cases) {
        for(Case newCase : cases){
            System.debug('SEND STATUS FOR: ' + newCase.Id);
            new VT_D2_QAction_SendSubjectStatus(newCase.Id).execute();
        }
    }
    
    public void finish(Database.BatchableContext param1) {
    }

}