public without sharing class VT_D1_VideoConfSignalHandler {
	
    
    public static void sendSignal(Set<Id> participantsSetIds, Boolean show) {
		VTR4_CNotification__e videoEvent = new VTR4_CNotification__e(
				VTR4_Type__c = 'Televisit',
				VTR4_Message__c = show ? 'Opened' : 'Closed',
				VTR4_UserId__c = JSON.serialize(participantsSetIds)

		);
    	
		Database.SaveResult result = EventBus.publish(videoEvent);
		if (!result.isSuccess()) {
			for (Database.Error error : result.getErrors()) {
				System.debug('Error returned: ' +
						error.getStatusCode() +' - '+
						error.getMessage());
			}
		} else {
			System.debug('!!! Success');
		}
    }

}