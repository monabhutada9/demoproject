/**
 * Created by Andrey Pivovarov on 9/4/2020.
 * @see VT_R5_DocumentCProcessHandlerTest
 * @description rewrited process builder 'Approve medical records 1'
 */

public without sharing class VT_R5_DocumentCMedRecordsHandler extends Handler {
    private static Map<Id, VTD1_Document__c> documentMap = new Map<Id, VTD1_Document__c>();
    private static final String ID_DOCUMENT_MEDICAL_RECORD = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD;
    private static final String ID_DOCUMENT_MEDICAL_RECORD_REJECTED = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_REJECTED;
    private static final String ID_DOCUMENT_MEDICAL_RECORD_ARCHIVED = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_ARCHIVED;
    private static final String ID_DOCUMENT_MEDICAL_RECORD_RELEASE_FORM = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_RELEASE_FORM;
    private static final String ID_DOCUMENT_VISIT_DOCUMENT = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT;
    private static final String DOCUMENT_APPROVED = VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED;
    private static final String DOCUMENT_REJECTED = VT_R4_ConstantsHelper_Documents.DOCUMENT_REJECTED;
    private static final String SCR_PROFILE_NAME = VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME;
    private static final String PG_PROFILE_NAME = VT_R4_ConstantsHelper_Profiles.PG_PROFILE_NAME;
    private static final String SC_PROFILE_NAME = VT_R4_ConstantsHelper_Profiles.SC_PROFILE_NAME;

    private static void getDocuments(Map<Id, SObject> documents) {
        if (!documentMap.keySet().containsAll(documents.keySet())) {
            documentMap = new Map<Id, VTD1_Document__c>([
                    SELECT RecordType.DeveloperName, CreatedBy.Profile.Name, VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c,
                            VTD1_Files_have_not_been_edited__c, VTD1_FileNames__c, VTR3_Category_add__c,
                            VTR3_Upload_File_Completed__c, VTD1_Deletion_Reason__c, VTD1_Status__c, VTD1_Nickname__c
                    FROM VTD1_Document__c WHERE Id IN :documents.keySet()
            ]);
        }
    }

    public override void onBeforeUpdate(Handler.TriggerContext context) {
        beforeUpdateDocument(context);
    }
    public override void onAfterUpdate(Handler.TriggerContext context) {
        afterUpdateDocument(context);
    }
    public override void onBeforeInsert(Handler.TriggerContext context) {
        beforeInsertDocument(context);
    }
    public override void onAfterInsert(Handler.TriggerContext context) {
        afterInsertDocument(context);
    }

    private static void beforeUpdateDocument(Handler.TriggerContext context) {
        getDocuments(context.newMap);
        Map<Id, VTD1_Document__c> oldDocumentsMap = (Map<Id, VTD1_Document__c>) context.oldMap;
        for (VTD1_Document__c document : (List<VTD1_Document__c>) context.newList) {
            VTD1_Document__c oldDocument = oldDocumentsMap.get(document.Id);
            /**Approve medical records 1 | isNew*/
            // 1 diamond
            if (oldDocument.RecordTypeId == ID_DOCUMENT_VISIT_DOCUMENT
                    && document.RecordTypeId == ID_DOCUMENT_MEDICAL_RECORD
                    && documentMap.get(document.Id) != null
                    && documentMap.get(document.Id).CreatedBy.Profile.Name == SCR_PROFILE_NAME) {
                document.VTD1_Current_Workflow__c = 'Medical Records flow';
                document.VTD1_Document_Type__c = 'Medical Record';
                document.VTD1_Files_have_not_been_edited__c = true;
                document.VTD1_Status__c = DOCUMENT_APPROVED;
                document.VTD1_PG_Approved__c = true;
            }
            // 3 diamond
            if (document.VTR3_Category_add__c != oldDocument.VTR3_Category_add__c && document.VTR3_Category_add__c == 'Medical Record') {
                document.RecordTypeId = ID_DOCUMENT_MEDICAL_RECORD;
                document.VTD1_Current_Workflow__c = 'Medical Records flow';
                document.VTD1_Document_Type__c = 'Medical Record';
            }
            if (isRecordTypeContainsMedical(document)) {
                // 6 diamond
                if (document.VTD1_Deletion_Reason__c != oldDocument.VTD1_Deletion_Reason__c
                        && document.VTD1_Deletion_Reason__c == 'Relevant, Uploaded in Error') {
                    document.RecordTypeId = ID_DOCUMENT_MEDICAL_RECORD_ARCHIVED;
                }
                // 7 diamond
                if (document.VTD1_Status__c != oldDocument.VTD1_Status__c && document.VTD1_Status__c == DOCUMENT_REJECTED
                        && document.RecordTypeId != ID_DOCUMENT_MEDICAL_RECORD_REJECTED) {
                    document.RecordTypeId = ID_DOCUMENT_MEDICAL_RECORD_REJECTED;
                }
            }
            // 8 diamond
            if (document.VTD1_Status__c != oldDocument.VTD1_Status__c && oldDocument.VTD1_Status__c == DOCUMENT_REJECTED
                    && document.RecordTypeId == ID_DOCUMENT_MEDICAL_RECORD_REJECTED
                    && document.VTD1_Deletion_Reason__c != 'Relevant, Uploaded in Error') {
                document.RecordTypeId = ID_DOCUMENT_MEDICAL_RECORD;
            }
        }
    }
    private static void afterUpdateDocument(Handler.TriggerContext context) {
        Set<Id> documentIdsToDelete = new Set<Id>();
        Set<Id> documentMedRecordsProcessIds = new Set<Id>();
        Map<Id, VTD1_Document__c> oldDocumentsMap = (Map<Id, VTD1_Document__c>) context.oldMap;
        for (VTD1_Document__c document : (List<VTD1_Document__c>) context.newList) {
            VTD1_Document__c oldDocument = oldDocumentsMap.get(document.Id);
            // 4 diamond
            if (document.VTD1_Status__c != DOCUMENT_APPROVED && documentMap.get(document.Id) != null
                    && documentMap.get(document.Id).CreatedBy.Profile.Name != PG_PROFILE_NAME
                    && documentMap.get(document.Id).CreatedBy.Profile.Name != SCR_PROFILE_NAME
                    && (document.VTD1_Files_have_not_been_edited__c != oldDocument.VTD1_Files_have_not_been_edited__c
                        || document.VTR3_Upload_File_Completed__c != oldDocument.VTR3_Upload_File_Completed__c
                        || document.VTR3_Category_add__c != oldDocument.VTR3_Category_add__c
                        || oldDocument.RecordTypeId != ID_DOCUMENT_MEDICAL_RECORD
                        || oldDocument.VTD1_FileNames__c == null)
                    && document.VTD1_Files_have_not_been_edited__c
                    && document.VTR3_Upload_File_Completed__c
                    && document.VTD1_FileNames__c != null
                    && (document.VTR3_Category_add__c == 'Medical Record'
                        || document.RecordTypeId == ID_DOCUMENT_MEDICAL_RECORD)
                    ) {
                    documentMedRecordsProcessIds.add(document.Id);
            }
            // 5 diamond
            if (document.VTD1_Deletion_Reason__c != oldDocument.VTD1_Deletion_Reason__c
                    && document.VTD1_Deletion_Reason__c == 'Not Relevant') {
                documentIdsToDelete.add(document.Id);
            }
        }
        if (!documentIdsToDelete.isEmpty()) {
            delete [SELECT Id FROM VTD1_Document__c WHERE Id IN :documentIdsToDelete];
        }
        if (!documentMedRecordsProcessIds.isEmpty()) {
            executeMedicalRecordsProcess(documentMedRecordsProcessIds);
        }
    }

    private static void beforeInsertDocument(Handler.TriggerContext context) {
        for (VTD1_Document__c document : (List<VTD1_Document__c>) context.newList) {
            /**Approve medical records 1*/
            // 3 diamond
            if (document.VTR3_Category_add__c == 'Medical Record') {
                document.RecordTypeId = ID_DOCUMENT_MEDICAL_RECORD;
                document.VTD1_Current_Workflow__c = 'Medical Records flow';
                document.VTD1_Document_Type__c = 'Medical Record';
            }
        }
    }
    private static void afterInsertDocument(Handler.TriggerContext context) {
        getDocuments(context.newMap);

        List<VTD1_Document__c> documentsToDelete = new List<VTD1_Document__c>();
        Map<Id,VTD1_Document__c> documentsMapToUpdate = new Map<Id,VTD1_Document__c>();
        Set<Id> documentMedRecordsProcessIds = new Set<Id>();
        for (VTD1_Document__c document : documentMap.values()) {
            VTD1_Document__c docToUpdate = new VTD1_Document__c(Id = document.Id);
            // 1 diamond
            if (document.RecordTypeId == ID_DOCUMENT_MEDICAL_RECORD
                    && (document.CreatedBy.Profile.Name == SCR_PROFILE_NAME
                        || document.CreatedBy.Profile.Name == PG_PROFILE_NAME)) {
                docToUpdate.VTD1_Current_Workflow__c = 'Medical Records flow';
                docToUpdate.VTD1_Document_Type__c = 'Medical Record';
                docToUpdate.VTD1_Files_have_not_been_edited__c = true;
                docToUpdate.VTD1_Status__c = DOCUMENT_APPROVED;
                docToUpdate.VTD1_PG_Approved__c = true;
                documentsMapToUpdate.put(docToUpdate.Id, docToUpdate);
            }
            // 2 diamond
            if (document.RecordTypeId == ID_DOCUMENT_MEDICAL_RECORD
                    && document.CreatedBy.Profile.Name == SC_PROFILE_NAME) {
                docToUpdate.VTD1_Files_have_not_been_edited__c = true;
                docToUpdate.VTD1_Status__c = null;
                documentsMapToUpdate.put(docToUpdate.Id, docToUpdate);
            }
            // 4 diamond
            if ((document.VTR3_Category_add__c == 'Medical Record'
                    || document.RecordTypeId == ID_DOCUMENT_MEDICAL_RECORD)
                    && document.CreatedBy.Profile.Name != SCR_PROFILE_NAME
                    && document.CreatedBy.Profile.Name != PG_PROFILE_NAME
                    && document.VTD1_Files_have_not_been_edited__c
                    && document.VTR3_Upload_File_Completed__c
                    && document.VTD1_FileNames__c != null) {
                documentMedRecordsProcessIds.add(document.Id);
            }
            // 5 diamond
            if (document.VTD1_Deletion_Reason__c == 'Not Relevant'
                    && document.RecordTypeId == ID_DOCUMENT_MEDICAL_RECORD_REJECTED) {
                documentsToDelete.add(document);
            }
            if (isRecordTypeContainsMedical(document)) {
                // 6 diamond
                if (document.VTD1_Deletion_Reason__c == 'Relevant, Uploaded in Error') {
                    docToUpdate.RecordTypeId = ID_DOCUMENT_MEDICAL_RECORD_ARCHIVED;
                    documentsMapToUpdate.put(docToUpdate.Id, docToUpdate);
                }
                // 7 diamond
                if (document.VTD1_Status__c == DOCUMENT_REJECTED
                        && document.RecordTypeId != ID_DOCUMENT_MEDICAL_RECORD_REJECTED) {
                    docToUpdate.RecordTypeId = ID_DOCUMENT_MEDICAL_RECORD_REJECTED;
                    documentsMapToUpdate.put(docToUpdate.Id, docToUpdate);
                }
            }
        }
        if (!documentsToDelete.isEmpty()) {
            delete documentsToDelete;
        }
        if (!documentsMapToUpdate.isEmpty()) {
            update documentsMapToUpdate.values();
        }
        if (!documentMedRecordsProcessIds.isEmpty()) {
            executeMedicalRecordsProcess(documentMedRecordsProcessIds);
        }
    }

    /**rewrited flow 'Medical Records process'*/
    private static void executeMedicalRecordsProcess(Set<Id> docIds) {
        List <String> tnNotificationCodes = new List<String>();
        List <Id> notificationSourceIds = new List<Id>();
        List <String> tnTaskCodes = new List<String>();
        List <Id> taskSourceIds = new List<Id>();
        List<VTD1_Document__c> documentsToUpdate = new List<VTD1_Document__c>();
        for (VTD1_Document__c doc : [
                SELECT Id, VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c, OwnerId
                FROM VTD1_Document__c
                WHERE Id IN :docIds
                AND RecordTypeId =: ID_DOCUMENT_MEDICAL_RECORD]) {
            if (doc.VTD1_Clinical_Study_Membership__r != null &&
                    doc.VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c != null) {
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setObjectId(doc.Id);
                req.setSubmitterId(doc.OwnerId);
                Approval.ProcessResult result = Approval.process(req);
            } else {
                doc.VTD1_Status__c = 'Pending Approval';
                documentsToUpdate.add(doc);
                notificationSourceIds.add(doc.Id);
                tnNotificationCodes.add('N050');
            }
            taskSourceIds.add(doc.Id);
            tnTaskCodes.add('001');
        }
        if (!documentsToUpdate.isEmpty()) {
            update documentsToUpdate;
        }
        if (!tnNotificationCodes.isEmpty() && !notificationSourceIds.isEmpty()) {
            VT_D2_TNCatalogNotifications.generateNotifications(tnNotificationCodes, notificationSourceIds, null);
        }
        if (!tnTaskCodes.isEmpty() && !taskSourceIds.isEmpty()) {
            VT_D2_TNCatalogTasks.generateTasks(tnTaskCodes, taskSourceIds, null, null);
        }
    }

    private static Boolean isRecordTypeContainsMedical(VTD1_Document__c doc) {
        return (doc.RecordTypeId == ID_DOCUMENT_MEDICAL_RECORD
                || doc.RecordTypeId == ID_DOCUMENT_MEDICAL_RECORD_ARCHIVED
                || doc.RecordTypeId == ID_DOCUMENT_MEDICAL_RECORD_REJECTED
                || doc.RecordTypeId == ID_DOCUMENT_MEDICAL_RECORD_RELEASE_FORM);
    }
}