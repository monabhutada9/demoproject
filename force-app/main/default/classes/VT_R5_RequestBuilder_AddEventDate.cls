/**
* @author: Carl Judge
* @date: 12-May-20
**/

public class VT_R5_RequestBuilder_AddEventDate implements VT_D1_RequestBuilder {
    public String timezone;
    public String unixTime;
    public String dateKey;
    public Boolean incer;

    // mulesoft fields
    public String studyGuid;
    public String orgGuid = eCOA_IntegrationDetails__c.getInstance().eCOA_OrgGuid__c;

    public VT_R5_RequestBuilder_AddEventDate(User u, String dateKey, Boolean incer) {
        this.dateKey = VT_D1_HelperClass.conditionEcoaString(dateKey);
        this.incer = incer;
        this.timezone = u.TimeZoneSidKey;
        this.unixTime = String.valueOf(System.now().getTime() / 1000);
        this.studyGuid = u.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eCOA_Guid__c;
    }

    public String buildRequestBody() {
        return JSON.serialize(this);
    }
}