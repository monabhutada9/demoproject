/**
 * @author: Alexander Komarov
 * @date: 08.07.2020
 * @description:
 */

@IsTest
public with sharing class VT_R5_MobileResourcesTest {

    public static void getGuidesTest() {
        Id caseId = fflib_IDGenerator.generate(Case.getSObjectType());
        VT_Stubber.applyStub(
                'CurrentContact',
                new List<Contact>{
                        new Contact(
                                VTD1_Clinical_Study_Membership__c = caseId,
                                VTR2_Primary_Language__c = 'en_US'
                        )
                });
        Id studyId = fflib_IDGenerator.generate(HealthCloudGA__CarePlanTemplate__c.getSObjectType());
        Id geoId = fflib_IDGenerator.generate(VTD2_Study_Geography__c.getSObjectType());
        VT_Stubber.applyStub(
                'CurrentCase',
                new List<Case>{
                        new Case(
                                VTD1_Study__c = studyId,
                                VTD1_Virtual_Site__r = new Virtual_Site__c(VTR3_Study_Geography__c = geoId)
                        )
                });

        Id docId1 = fflib_IDGenerator.generate(VTD1_Document__c.getSObjectType());
        Id docId2 = fflib_IDGenerator.generate(VTD1_Document__c.getSObjectType());
        VT_Stubber.applyStub(
                'infGuidesDocumentsWithDefaults',
                new List<VTD1_Document__c>{
                        new VTD1_Document__c(
                                Id = docId1,
                                VTR2_Language__c = 'en_US',
                                VTR5_DefaultDocument__c = false,
                                RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_INFORMED_CONSENT_GUIDE
                        ),
                        new VTD1_Document__c(
                                Id = docId2,
                                VTR2_Language__c = 'it',
                                VTR5_DefaultDocument__c = true,
                                RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_INFORMED_CONSENT_GUIDE
                        )
                });

        Id contDocId = fflib_IDGenerator.generate(ContentDocument.getSObjectType());
        VT_Stubber.applyStub(
                'infGuidesLinks',
                new List<ContentDocumentLink>{
                        new ContentDocumentLink(
                                LinkedEntityId = docId1,
                                ContentDocumentId = contDocId,
                                ContentDocument = new ContentDocument()
                        )
                });

        VT_Stubber.applyStub(
                'infGuidesContentVersions',
                new List<ContentVersion>{
                        new ContentVersion(
                                ContentDocumentId = contDocId
                        )
                });

        VT_Stubber.applyStub(
                'infGuidesDocuments',
                new List<VTD1_Document__c>{
                        new VTD1_Document__c(
                                Id = docId1,
                                VTR5_DocumentTitle__c = 'someTitle',
                                VTR5_DocumentVersion__c = '1',
                                VTR2_Language__c = 'en_US'
                        )
                });



        RestContext.request = new RestRequest();
        RestContext.response = new RestResponse();
        RestContext.request.requestURI = '/patient/v1/resources/informed-consent-guides/';
        RestContext.request.httpMethod = 'GET';
        RestContext.request.params.put('limit', '10');
        RestContext.request.params.put('offset', '0');
        VT_R5_MobileRestRouter.doGET();
    }
}