/**
* @author: Carl Judge
* @date: 04-Nov-18
* @description: 
**/

@IsTest
private class VT_D1_GridPhonesListController_Test {

    @testSetup
    private static void setupMethod() {
        Account acc = new Account(Name = 'TestAcc');
        insert acc;

        Account accDelete = new Account(Name = 'TestAccDelete');
        insert accDelete;


        Contact con = new Contact(LastName = 'TestCon', AccountId = acc.Id);
        insert con;

        Contact conDelete = new Contact(LastName = 'TestDelete', AccountId = acc.Id);
        insert conDelete;

        Case cas = new Case(AccountId = acc.Id, Subject = 'test');
        insert cas;

        Case casDelete = new Case(AccountId = accDelete.Id, Subject = 'for Delete');
        insert casDelete;

        VT_D1_Phone__c phone = new VT_D1_Phone__c(Account__c = acc.Id, VTD1_Contact__c = con.Id, PhoneNumber__c = '6463097689');
        insert phone;

        VT_D1_Phone__c phoneDelete = new VT_D1_Phone__c(Account__c = accDelete.Id, VTD1_Contact__c = conDelete.Id, PhoneNumber__c = '0101010101');
        insert phoneDelete;
    }

    @isTest
    private static void doTest() {
        Account acc = [SELECT Id FROM Account where Name = 'TestAcc'];
        Contact con = [SELECT Id FROM Contact WHERE LastName = 'TestCon'];
        Case cas = [SELECT Id FROM Case where Subject = 'test'];

        //Account accDelete = [SELECT Id FROM Account where Name = 'TestAccDelete'];
        Contact conDelete = [Select Id FROM Contact WHERE LastName ='TestDelete'];
        Case casDelete = [SELECT Id FROM Case where Subject = 'for Delete'];

        List<VT_D1_GridPhonesListController.VT_D1_GridPhonesWrapper> wrappedPhones;
        List<VT_D1_GridPhonesListController.VT_D1_GridPhonesWrapper> wrappedPhonesForDelete;

        wrappedPhones = VT_D1_GridPhonesListController.getPhonesWrapList(acc.Id, con.Id);
        wrappedPhones = VT_D1_GridPhonesListController.getPhonesWrapList(con.Id, con.Id);
        wrappedPhones = VT_D1_GridPhonesListController.getPhonesWrapList(cas.Id, con.Id);

        wrappedPhonesForDelete = VT_D1_GridPhonesListController.getPhonesWrapList(casDelete.Id, conDelete.Id);
        System.debug(wrappedPhonesForDelete );
        System.debug(wrappedPhones);

        VT_D1_GridPhonesListController.getAccountId(acc.Id);
        VT_D1_GridPhonesListController.getAccountId(con.Id);
        VT_D1_GridPhonesListController.getAccountId(cas.Id);

        VT_D1_GridPhonesListController.getContactId(acc.Id);
        VT_D1_GridPhonesListController.getContactId(con.Id);
        VT_D1_GridPhonesListController.getContactId(cas.Id);

        String s = JSON.serialize(wrappedPhones);
        String sd = JSON.serialize(wrappedPhonesForDelete);
        VT_D1_GridPhonesListController.savePhones(s,sd);
    }
}