public with sharing class VT_R3_CasePatientFlagHandler extends Handler {

    public override void onAfterInsert(Handler.TriggerContext context) {
        updateActualVisitsPatientRemovedFlagToTrueOnCases(context.newList);
    }

    public override void onAfterUpdate(Handler.TriggerContext context) {
        updateActualVisitsPatientRemovedFlagToTrueOnCases(context.newList);
    }

    public static void updateActualVisitsPatientRemovedFlagToTrueOnCases(List<Case> newCases) {
        List<Case> casesWithDroppedStatus = getDroppedCases(newCases);
        if (!casesWithDroppedStatus.isEmpty()) {
            List<VTD1_Actual_Visit__c> actualVisits = getActualVisitsByCases(casesWithDroppedStatus);
            actualVisits = setPatientRemovedFlagToTrue(actualVisits);
            update actualVisits;
        }
    }

    public static List<Case> getDroppedCases( List<Case> cases){
        List<Case> droppedCases = new List<Case>();
        for (Case caseRecord : cases) {
            if (caseRecord.Status == 'Dropped') {
                droppedCases.add(caseRecord);
            }
        }
        return droppedCases;
    }

    private static List<VTD1_Actual_Visit__c> getActualVisitsByCases(List<Case> casesWithDroppedStatus) {
        return [
                SELECT
                        VTD1_Status__c,
                        toLabel(VTD1_Status__c) statusLabel
                FROM VTD1_Actual_Visit__c
                WHERE (VTD1_Case__c IN :casesWithDroppedStatus
                OR Unscheduled_Visits__c IN :casesWithDroppedStatus)
                AND VTD1_Patient_Removed_Flag__c != TRUE
                AND VTD1_Status__c != :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED
                AND VTD1_Status__c != :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED
                AND VTD1_Status__c != :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT
        ];
    }

    private static List<VTD1_Actual_Visit__c> setPatientRemovedFlagToTrue(List<VTD1_Actual_Visit__c> actualVisits) {
        for (VTD1_Actual_Visit__c actualVisit : actualVisits) {
            actualVisit.VTD1_Patient_Removed_Flag__c = true;
        }
        return actualVisits;
    }
}