/**
 * @author: Alexander Komarov
 * @date: 04.03.2020
 * @description:
 */
//Temporary such mapping here, later will be one class-router for all v1 api
@RestResource(UrlMapping='/Patient/v1/Ediaries/*')
global with sharing class VT_R4_MobilePatientEdiary {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();
    private static Set<String> notCompletedStatuses = new Set<String>{
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_NOT_STARTED, VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_IN_PROGRESS, VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_READY_TO_START
    };
    private static Map<String, String> PATIENT_STATUSES_MAPPING_TRANSLATED = new Map<String, String>{
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_NOT_STARTED => Label.VTR4_ReadyToStart,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED => Label.VTR2_Completed,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED => Label.VTR2_Completed,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED_PARTIALLY_COMPLETED => Label.VTR2_Completed,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_READY_TO_START => Label.VTR4_ReadyToStart
    };
    private static Map<String, String> PATIENT_STATUSES_MAPPING = new Map<String, String>{
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_NOT_STARTED => 'VTR4_ReadyToStart',
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED => 'VTR2_Completed',
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED => 'VTR2_Completed',
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED_PARTIALLY_COMPLETED => 'VTR2_Completed'
    };
    private static Set<String> SURVEY_MISSED_COMPLETED_STATUSES = new Set<String>{
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_EDIARY_MISSED,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_COMPLETED,





            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_MISSED,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED_PARTIALLY_COMPLETED





    };
    private static VTD1_Survey__c currentSurvey;
    static RestRequest request = RestContext.request;
    static RestResponse response = RestContext.response;
    static MobilePatientEdiaryResponse mobilePatientEdiaryResponse = new MobilePatientEdiaryResponse();
    @HttpGet
    global static void getMethod() {
        String surveyId = request.requestURI.substringAfterLast('/Ediaries/');
        if (String.isBlank(surveyId)) {
            try {
                addListOfSurveysToResponse();
            } catch (Exception e) {
                sendResponse(500, 'FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName() + '.');
            }
        } else {
            try {
                getSingleSurvey(surveyId);
            } catch (QueryException qe) {
                sendResponse(400, 'No surveys for this patient with such ID - ' + surveyId);
            } catch (Exception e) {
                sendResponse(500, 'FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName() + '.');
            }
        }
        sendResponse();
    }
    @HttpPost
    global static void postMethod() {
        System.debug(JSON.serializePretty(request.requestBody.toString()));
        System.debug(request.requestURI);
        System.debug(request.params);
        String surveyId = request.requestURI.substringAfterLast('/Ediaries/');
        if (String.isBlank(surveyId)) {
            sendResponse(400, helper.forAnswerForIncorrectInput());
        } else {
            getSurvey(surveyId);
            if (!checkStatus()) {
                return;
            }
            MobileEdiaryRequest mobileEdiaryRequest = new MobileEdiaryRequest(request.requestBody.toString());
//            List<CustomAnswerWrapper> answersList = (List<CustomAnswerWrapper>) JSON.deserialize(request.requestBody.toString(), List<CustomAnswerWrapper>.class);
            List<CustomAnswerWrapper> answersList = mobileEdiaryRequest.answers;
            System.debug('DESERIALIZED SUCCESSFULLY' + JSON.serializePretty(mobileEdiaryRequest));
            if (!checkAnswers(answersList, surveyId)) {
                return;
            }
            System.debug('answersList : ' + JSON.serialize(answersList));
            List<String> answersToRemove = getAnswersToRemove(answersList);
            answersList = processTimestampsSecondVersionEspeciallyForCopado(currentSurvey, answersList);
            String answersForController = JSON.serialize(answersList);
            Boolean complete = getCompleteParam();
            try {
                if (complete) {
                    VT_D1_eProHomeController.saveAnswers(answersForController, surveyId, complete, true);
                    deleteRelevantNotifications(surveyId);
                    cr.serviceMessage = System.Label.VTR4_HasBeenCompleted.replace('#1', currentSurvey.Name);
                } else {
                    VT_D1_eProHomeController.saveAndRemoveAnswers(answersForController, JSON.serialize(answersToRemove), surveyId, mobileEdiaryRequest.percentage, true);
                }
            } catch (Exception e) {
                cr.serviceMessage = 'Internal server error.';
                sendResponse(500, 'error updating answers / survey ' + e.getMessage() + ' ' + e.getStackTraceString());
                return;
            }
            getSingleSurvey(surveyId);
            System.debug('RETURNING SURVEY => ' + JSON.serializePretty(mobilePatientEdiaryResponse.surveys[0]));
            System.debug('RETURNING SURVEY => ' + JSON.serializePretty(mobilePatientEdiaryResponse.questionGroups));
            sendResponse();
        }
    }
    private static List<String> getAnswersToRemove(List<CustomAnswerWrapper> answersList) {
        List<String> result = new List<String>();
        Set<String> givenAnswers = new Set<String>();
        for (CustomAnswerWrapper caw : answersList) {
            givenAnswers.add(caw.answerId);
        }
        System.debug('givenAnswers : ' + JSON.serialize(givenAnswers));
        if (currentSurvey.Survey_Answers__r != null) {
            System.debug('currentSurvey.Survey_Answers__r : ' + JSON.serialize(currentSurvey.Survey_Answers__r));
            for (VTD1_Survey_Answer__c sa : currentSurvey.Survey_Answers__r) {
                if (!givenAnswers.contains(sa.Id)) {
                    result.add(sa.Id);
                    System.debug('REMOVING ANSWERS : ' + JSON.serialize(result));
                }
            }
        }
        return result;
    }
    private static void deleteRelevantNotifications(String surveyId) {
        Database.executeBatch(new VT_R4_DeleteEDiaryNotificationsBatch(surveyId));
    }
    private static Boolean getCompleteParam() {
        if (!request.params.containsKey('complete')) {
            return false;
        } else {
            return Boolean.valueOf(request.params.get('complete'));
        }
    }
    private static Boolean checkAnswers(List<CustomAnswerWrapper> answersList, String surveyId) {
        Set<String> answersIds = new Set<String>();
        for (CustomAnswerWrapper caw : answersList) {
            answersIds.add(caw.answerId);
        }
        System.debug('answersIds *** ' + answersIds);
        for (VTD1_Survey_Answer__c answer : [SELECT Id, VTD1_Survey__c FROM VTD1_Survey_Answer__c WHERE Id IN :answersIds]) {
            if (!answer.VTD1_Survey__c.equals(surveyId)) {
                sendResponse(409, 'FAILURE. Some answers not belong to this survey.');
                return false;
            }
        }
        return true;
    }
    private static Boolean checkStatus() {
        if (!notCompletedStatuses.contains(currentSurvey.VTD1_Status__c)) {
            sendResponse(417, 'FAILURE. Survey already completed or missed');
            return false;
        } else {
            return true;
        }
    }
    private static void getSurvey(String surveyId) {
        currentSurvey = [SELECT Id, VTD1_Status__c, Name, VTR4_Percentage_Completed__c, (SELECT Id, VTD1_Answer__c, VTR4_AnswerTimestamp__c FROM Survey_Answers__r) FROM VTD1_Survey__c WHERE Id = :surveyId LIMIT 1];
    }
    private static void getSingleSurvey(String surveyId) {
        VTD1_Survey__c currentSurvey = [
                SELECT
                        Id,
                        toLabel(VTD1_Status__c)lblStatus,
                        VTD1_Due_Date__c,
                        VTR4_Percentage_Completed__c, (
                        SELECT
                                Id,
                                VTR2_Question_content__c,
                                VTD1_Answer__c
                        FROM
                                Survey_Answers__r
                ),
                        VTD1_Protocol_ePRO__c,
                        Name,
                        VTD1_Status__c,
                        VTD1_Protocol_ePRO__r.Name,
                        VTD1_Protocol_ePRO__r.VTD1_Subject__c,
                        VTD1_Protocol_ePRO__r.VTR4_Is_Branching_eDiary__c,
                        VTD1_Protocol_ePRO__r.VTD1_Study__r.VTR5_Display_Completed_Missed_Ediary__c,
                        VTR2_DocLanguage__c
                FROM
                        VTD1_Survey__c
                WHERE
                        Id = :surveyId
                LIMIT 1
        ];
        List<VTD1_Protocol_ePro_Question__c> questions = VT_D1_eProHomeController.getQuestionsAnswers(currentSurvey.Id, currentSurvey.VTD1_Protocol_ePRO__c);
        List<VTR2_eDiaryInstructions__c> instructions = getInstructionsByProtocol(currentSurvey.VTD1_Protocol_ePRO__c);
        String languageToTranslate = getLanguageToTranslate(currentSurvey.VTR2_DocLanguage__c);
        VT_D1_TranslateHelper.translate(instructions, languageToTranslate);

        mobilePatientEdiaryResponse.fill(questions, languageToTranslate);
        mobilePatientEdiaryResponse.lblForSurvey = VT_D1_eProHomeController.getTranslatedLabels(languageToTranslate);
        mobilePatientEdiaryResponse.surveys.add(new MobilePatientEdiary(currentSurvey, instructions));
    }
    private static String getLanguageToTranslate(String docLanguage) {
        String result = UserInfo.getLanguage();
        if (docLanguage != null) {
            result = docLanguage;
        } else {
            String contactDocLang = [
                    SELECT Contact.VTR2_Doc_Language__c
                    FROM User
                    WHERE Id = :UserInfo.getUserId()
            ].Contact.VTR2_Doc_Language__c;
            if (contactDocLang != null) {
                result = contactDocLang;
            }
        }
        return result;
    }
    private static List<VTR2_eDiaryInstructions__c> getInstructionsByProtocol(String protocolId) {
        List<VTR2_eDiaryInstructions__c> instructions = [SELECT Id, VTR2_Content__c, VTR2_Group_number__c FROM VTR2_eDiaryInstructions__c WHERE VTR2_Protocol_eDiary__c = :protocolId];
        return instructions;
    }
    private static void addListOfSurveysToResponse() {
        VT_D1_eProHomeController.SurveysWrapperForPatient surveys = VT_D1_eProHomeController.getSurveysForPatient();
        for (VT_D1_eProHomeController.SurveyWrapper sw : surveys.surveys) {
            System.debug('INCOME SV ' + JSON.serializePretty(sw.sv));
            if (sw.sv.VTD1_CSM__r.VTD1_Study__r.VTR5_Display_Completed_Missed_Ediary__c) {
                mobilePatientEdiaryResponse.surveys.add(new MobilePatientEdiary(sw.sv, sw.instructions));
            } else if (!SURVEY_MISSED_COMPLETED_STATUSES.contains(sw.sv.VTD1_Status__c)) {
                mobilePatientEdiaryResponse.surveys.add(new MobilePatientEdiary(sw.sv, sw.instructions));
            }
        }
    }
    private static List<CustomAnswerWrapper> processTimestampsSecondVersionEspeciallyForCopado(VTD1_Survey__c survey, List<CustomAnswerWrapper> answersList) {
        System.debug('answersList=' + JSON.serializePretty(answersList));
        List<CustomAnswerWrapper> result = new List<CustomAnswerWrapper>();
        Map<Id, Long> answerIdsToTimestamp = new Map<Id, Long>();
        for (VTD1_Survey_Answer__c answer : survey.Survey_Answers__r) {
            answerIdsToTimestamp.put(answer.Id, answer.VTR4_AnswerTimestamp__c == null ? 0L : answer.VTR4_AnswerTimestamp__c.getTime());
        }
        System.debug('answerIdsToTimestamp=' + JSON.serializePretty(answerIdsToTimestamp));
        for (CustomAnswerWrapper caw : answersList) {
            if (caw.answerId == null) {
                System.debug('approved 1 - ' + caw);
                result.add(caw);
                continue;
            }
            if (answerIdsToTimestamp.containsKey(caw.answerId)) {
                Long savedTimestamp = answerIdsToTimestamp.get(caw.answerId);
                System.debug('saved - ' + savedTimestamp + ' caw.ts ' + caw);
                if (savedTimestamp < caw.timestamp) {
                    System.debug('approved 2 - ' + caw);
                    result.add(caw);
                    continue;
                }
            }
            System.debug('rejected - ' + caw);
        }
        return result;
    }
    private static void processResetEdiary(VTD1_Survey__c survey) {
        Boolean gotAtLeastOne = false;
        for (VTD1_Survey_Answer__c sa : survey.Survey_Answers__r) {
            if (String.isNotBlank(sa.VTD1_Answer__c)) {
                sa.VTD1_Answer__c = null;
                sa.VTR2_Is_Answered__c = false;
                gotAtLeastOne = true;
            }
        }
        if (gotAtLeastOne) update survey.Survey_Answers__r;
        survey.VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_READY_TO_START;
        survey.VTR4_Percentage_Completed__c = 0;
        update survey;
    }
    @HttpPut
    global static void putMethod() {
        String toCheck = request.requestURI.substringAfterLast('/Ediaries/Reset/');
        if (String.isBlank(toCheck)) {
            sendResponse(400, helper.forAnswerForIncorrectInput());
        } else {
            if (!helper.isIdCorrectType(toCheck, 'VTD1_Survey__c')) {
                sendResponse(400, 'bad id');
                return;
            }
            getSurvey(toCheck);
            if (!checkStatus()) {
                return;
            }
            processResetEdiary(currentSurvey);
            try {
                getSingleSurvey(toCheck);
            } catch (Exception e) {
                sendResponse(500, 'FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName() + '.');
            }
            sendResponse();
        }
    }
    private class MobilePatientEdiaryResponse {
        public List<String> errors = new List<String>();
        public VT_D1_eProHomeController.LabelsForSurvey lblForSurvey;
        public List<MobilePatientEdiary> surveys = new List<MobilePatientEdiary>();
        public Map<String, MobilePatientEdiaryGroup> questionGroups = new Map<String, MobilePatientEdiaryGroup>();
//        public List<MobilePatientEdiaryQuestion> questionGroups = new List<MobilePatientEdiaryQuestion>();
        public void fill(List<VTD1_Protocol_ePro_Question__c> questions, String languageToTranslate) {
            List<MobilePatientEdiaryQuestion> listOfQuestionsToProcess = formList(questions, languageToTranslate);
            List<MobilePatientEdiaryQuestion> nextListOfQuestionsToProcess = new List<MobilePatientEdiaryQuestion>();
            List<MobilePatientEdiaryQuestion> listOfQuestionsToStructure = new List<MobilePatientEdiaryQuestion>();
            Map<String, MobilePatientEdiaryQuestion> mapIdToQuestion = new Map<String, MobilePatientEdiaryQuestion>();
            Set<String> nextIterationParents = new Set<String>();
            Set<String> currentIterationParents = new Set<String>();
            Boolean firstRun = true;
            while (!listOfQuestionsToProcess.isEmpty()) {
                System.debug('listOfQuestionsToProcess size now = ' + listOfQuestionsToProcess.size());
                for (MobilePatientEdiaryQuestion q : listOfQuestionsToProcess) {
                    System.debug('current Q num =' + q.questionNumber + ' id = ' + q.questionId);
                    if (String.isBlank(q.questionParentId) || currentIterationParents.contains(q.questionParentId)) {
                        System.debug('ADDING TO PROCESS ' + q.questionNumber + ' parent= ' + q.questionParentId);
                        listOfQuestionsToStructure.add(q);
                        nextIterationParents.add(q.questionId);
                    } else {
                        System.debug('ADDING TO NEXT PROCESS ' + q.questionNumber);
                        nextListOfQuestionsToProcess.add(q);
                    }
                    if (firstRun) {
                        mapIdToQuestion.put(q.questionId, q);
                    }
                }
                firstRun = false;
                currentIterationParents.clear();
                currentIterationParents.addAll(nextIterationParents);
                nextIterationParents.clear();
                createBaseStructure(listOfQuestionsToStructure, mapIdToQuestion);
                listOfQuestionsToStructure.clear();
                listOfQuestionsToProcess.clear();
                listOfQuestionsToProcess.addAll(nextListOfQuestionsToProcess);
                nextListOfQuestionsToProcess.clear();
            }
        }
        private void createBaseStructure(
                List<MobilePatientEdiaryQuestion> questions,
                Map<String, MobilePatientEdiaryQuestion> mapIdToQuestion
        ) {
            System.debug('CREATING STRUCTURE FOR ' + questions.size() + ' QUESTIONS ');
            for (MobilePatientEdiaryQuestion q : questions) {
                System.debug('Processing question =' + q);
                MobilePatientEdiaryGroup mobilePatientEdiaryGroup = new MobilePatientEdiaryGroup();
                mobilePatientEdiaryGroup.question = q;
                mobilePatientEdiaryGroup.childQuestions = new Map<String, Map<String, MobilePatientEdiaryGroup>>();
                if (q.containsBranching) {
                    if (String.isNotEmpty(q.questionOptions)) {
                        for (String option : q.questionOptions.split(';')) {
                            mobilePatientEdiaryGroup.childQuestions.put(option, new Map<String, MobilePatientEdiaryGroup>());
                            System.debug('putting option =' + option);
                        }
                    }
                }
                Map<String, MobilePatientEdiaryGroup> groupFromStructure = getGroup(q, mapIdToQuestion);
                System.debug('GOT GROUP ' + groupFromStructure);
                if (groupFromStructure != null) {
                if (q.questionNumber == null) {
                    groupFromStructure.put(q.questionId, mobilePatientEdiaryGroup);
                } else {
                    groupFromStructure.put(q.questionNumber, mobilePatientEdiaryGroup);
                }
            }
        }
        }
        private Map<String, MobilePatientEdiaryGroup> getGroup(MobilePatientEdiaryQuestion question, Map<String, MobilePatientEdiaryQuestion> mapIdToQuestion) {
            Map<String, MobilePatientEdiaryGroup> result = questionGroups;
            if (question.questionParentId == null) {
                System.debug('RETURNING GROUP ' + result);
                return result;
            }
            MobilePatientEdiaryQuestion currentQuestion = question;
            Map<String, MobilePatientEdiaryQuestion> parentIdToItChildQuestion = new Map<String, MobilePatientEdiaryQuestion>();
            while (currentQuestion.questionParentId != null) {
                parentIdToItChildQuestion.put(currentQuestion.questionParentId, currentQuestion);
                currentQuestion = mapIdToQuestion.get(currentQuestion.questionParentId);
            }
            System.debug('max parent = ' + currentQuestion);
            MobilePatientEdiaryQuestion childQuestion;
            while (true) {
                childQuestion = parentIdToItChildQuestion.get(currentQuestion.questionId);
                if (childQuestion == null) {
                    System.debug('BREAKING CYCLE');
                    break;
                }
                MobilePatientEdiaryGroup currentGroup = result.get(currentQuestion.questionNumber);
                System.debug('currentGroup = ' + currentGroup);
                Map<String, Map<String, MobilePatientEdiaryGroup>> currentChildGroup = currentGroup.childQuestions;
                result = currentChildGroup.get(childQuestion.questionResponseTrigger);
                System.debug('current result = ' + result);
                currentQuestion = childQuestion;
            }
            return result;
        }
        private List<MobilePatientEdiaryQuestion> formList(List<VTD1_Protocol_ePro_Question__c> questions, String languageToTranslate) {
            List<MobilePatientEdiaryQuestion> result = new List<MobilePatientEdiaryQuestion>();
            Set<Id> questionsIds = new Set<Id>();
            for (VTD1_Protocol_ePro_Question__c protocolQuestion : questions) {
                questionsIds.add(protocolQuestion.Id);
            }
            getTranslationsMaps(questionsIds, languageToTranslate);
            for (VTD1_Protocol_ePro_Question__c protocolQuestion : questions) {
                result.add(new MobilePatientEdiaryQuestion(protocolQuestion));
            }
            return result;
        }

        private void getTranslationsMaps(Set<Id> questionsIds, String languageToTranslate) {
            for (VTD1_Translation__c translation : [
                    SELECT
                            Id,
                            VTD1_Field_Name__c,
                            VTD1_Record_Id__c,
                            VTD1_Value__c,
                            VTR2_Value__c
                    FROM VTD1_Translation__c
                    WHERE VTD1_Language__c = :languageToTranslate
                    AND VTD1_Object_Name__c = 'VTD1_Protocol_ePro_Question__c'
                    AND VTD1_Record_Id__c IN :questionsIds
            ]) {
                if (translation.VTD1_Field_Name__c.equals('VTD1_Options__c')) questionsOptionsTranslations.put(translation.VTD1_Record_Id__c, translation);
                if (translation.VTD1_Field_Name__c.equals('VTD1_Question_Content__c')) questionsContentsTranslations.put(translation.VTD1_Record_Id__c, translation);
                if (translation.VTD1_Field_Name__c.equals('VTR2_QuestionContentLong__c')) questionsContentsLongTranslations.put(translation.VTD1_Record_Id__c, translation);
            }
    }
    }
    private static Map<Id, VTD1_Translation__c> questionsContentsTranslations = new Map<Id, VTD1_Translation__c>();
    private static Map<Id, VTD1_Translation__c> questionsContentsLongTranslations = new Map<Id, VTD1_Translation__c>();
    private static Map<Id, VTD1_Translation__c> questionsOptionsTranslations = new Map<Id, VTD1_Translation__c>();
    public class Instruction {
        public String content;
        public String groupNumber;
        public Instruction(VTR2_eDiaryInstructions__c dbInstruction) {
            this.content = dbInstruction.VTR2_Content__c;
            if (dbInstruction.VTR2_Group_number__c != null) {
                this.groupNumber = String.valueOf(dbInstruction.VTR2_Group_number__c);
            } else {
                this.groupNumber = '1';
            }
        }
    }
    private class MobilePatientEdiary {
        public String surveyId;
        public String surveyStatus;
        public String surveyStatusLabel;
        public String surveyTitle;
        public String targetSubject;
        public String dueDateLabel;
        public Long dueDate;
        public Integer countQuestions;
        public Decimal percentageCompleted;
        public String protocolId;
        public Boolean containsBranching;
        public List<Instruction> instructions;
        //lblStatus !!
        public MobilePatientEdiary(VTD1_Survey__c dbSurvey, List<VTR2_eDiaryInstructions__c> dbInstructions) {
            this.surveyId = dbSurvey.Id;
            this.surveyTitle = dbSurvey.Name;
            if (PATIENT_STATUSES_MAPPING.containsKey(dbSurvey.VTD1_Status__c)) {
                this.surveyStatus = VT_D1_TranslateHelper.getLabelValue(PATIENT_STATUSES_MAPPING.get(dbSurvey.VTD1_Status__c), 'en_US');
            } else {
                this.surveyStatus = dbSurvey.VTD1_Status__c;
            }
            if (PATIENT_STATUSES_MAPPING_TRANSLATED.containsKey(dbSurvey.VTD1_Status__c)) {
                this.surveyStatusLabel = PATIENT_STATUSES_MAPPING_TRANSLATED.get(dbSurvey.VTD1_Status__c);
            } else {
                this.surveyStatusLabel = String.valueOf(dbSurvey.get('lblStatus'));
            }
            this.targetSubject = dbSurvey.VTD1_Protocol_ePRO__r.VTD1_Subject__c;
            this.dueDateLabel = dbSurvey.VTD1_Due_Date__c.format('dd-MMM-yyyy');
            this.dueDate = dbSurvey.VTD1_Due_Date__c.getTime(); //+ timeOffset
            this.countQuestions = dbSurvey.Survey_Answers__r.size();
            this.percentageCompleted = dbSurvey.VTR4_Percentage_Completed__c;
            this.protocolId = dbSurvey.VTD1_Protocol_ePRO__c;
            this.containsBranching = dbSurvey.VTD1_Protocol_ePRO__r.VTR4_Is_Branching_eDiary__c;
            if (!dbInstructions.isEmpty()) {
                this.instructions = new List<Instruction>();
                for (VTR2_eDiaryInstructions__c dbI : dbInstructions) {
                    this.instructions.add(new Instruction(dbI));
                }
            }
        }
    }
    private class MobilePatientEdiaryGroup {
        public MobilePatientEdiaryQuestion question;
        public Map<String, Map<String, MobilePatientEdiaryGroup>> childQuestions = new Map<String, Map<String, MobilePatientEdiaryGroup>>();
    }
    public class MobilePatientEdiaryQuestion { //implements Comparable
        public String questionId;
        public String questionNumber;
        public String questionContent;
        public String questionContentRich;
        public String questionContentTranslated;
        public String questionType;
        public String questionOptions;
        public String questionOptionsTranslated;
        public String questionGroupNumber;
        public String questionParentId;
        public String questionResponseTrigger;
        public String questionAnswerValue;
        public String questionAnswerId;
        public Boolean containsBranching;
        public MobilePatientEdiaryQuestion(VTD1_Protocol_ePro_Question__c q) {
            this.questionId = q.Id;
            this.questionNumber = q.VTR4_Number__c;
            this.questionContent = q.VTR2_QuestionContentLong__c != null ? q.VTR2_QuestionContentLong__c : q.VTD1_Question_Content__c;
            this.questionContentRich = q.VTR4_QuestionContentRichText__c;
            if (questionsContentsLongTranslations.containsKey(q.Id)) this.questionContentTranslated = getTranslationValue(questionsContentsLongTranslations.get(q.Id));
            if (questionsContentsTranslations.containsKey(q.Id)) this.questionContentTranslated = getTranslationValue(questionsContentsTranslations.get(q.Id));
            this.questionType = q.VTD1_Type__c;
            if (q.VTD1_Options__c != null) {
                this.questionOptions = q.VTD1_Options__c.replace('\r\n', '');
                if (questionsOptionsTranslations.containsKey(q.Id)) this.questionOptionsTranslated = getTranslationValue(questionsOptionsTranslations.get(q.Id));
            }
            this.questionGroupNumber = String.valueOf(q.VTD1_Group_number__c);
            this.questionParentId = q.VTR4_Branch_Parent__c;
            this.questionResponseTrigger = q.VTR4_Response_Trigger__c;
            if (!q.Survey_Answers__r.isEmpty()) {
                this.questionAnswerValue = q.Survey_Answers__r[0].VTD1_Answer__c;
                this.questionAnswerId = q.Survey_Answers__r[0].Id;
            }
            this.containsBranching = q.VTR4_Contains_Branching_Logic__c;
        }
        private String getTranslationValue (VTD1_Translation__c translation) {
            if (translation != null) {
                if (translation.VTR2_Value__c != null) {
                    return translation.VTR2_Value__c;
                } else {
                    return translation.VTD1_Value__c;
                }
            } else return null;
        }
//        public Integer compareTo(Object compareTo) {
//            MobilePatientEdiaryQuestion compareToQ = (MobilePatientEdiaryQuestion) compareTo;
//            String[] questionCompareToParted;
//            try {
//                questionCompareToParted = questionNumber.split('\\.');
//            } catch (Exception e) {
//                System.debug('ATTENTION HERE! THIS MAY BE PROBLEM!');
//                return 1;
//            }
//            String[] questionToCompare;
//            try {
//                questionToCompare = compareToQ.questionNumber.split('\\.');
//            } catch (Exception e) {
//                System.debug('ATTENTION HERE! THIS MAY BE PROBLEM!');
//                return -1;
//            }
//
//
//            Integer returnValue = 0;
//            Integer j = questionCompareToParted.size();
//            Integer k = questionToCompare.size();
//            for (Integer i = 0; i < k && i < j; i++) {
//                Integer compareToInt;
//                Integer toCompareInt;
//                try {
//                    compareToInt = Integer.valueOf(questionCompareToParted[i]);
//                } catch (Exception e) {
//                    returnValue = 1;
//                    System.debug('ATTENTION HERE! THIS MAY BE PROBLEM!');
//                }
//                try {
//                    toCompareInt = Integer.valueOf(questionToCompare[i]);
//                } catch (Exception e) {
//                    System.debug('ATTENTION HERE! THIS MAY BE PROBLEM!');
//                    returnValue = -1;
//                } finally {
//                    if (returnValue != 0) {
//                        return returnValue;
//                    }
//                }
//                if (compareToInt > toCompareInt) {
//                    return 1;
//                } else if (compareToInt < toCompareInt) {
//                    return -1;
//                } else {
//                    continue;
//                }
//            }
//            if (returnValue == 0) {
//                if (j > k) {
//                    return 1;
//                } else if (k > j) {
//                    return -1;
//                } else {
//                    return 0;
//                }
//            }
//            return returnValue;
//        }
    }
    public class CustomAnswerWrapper {
        public String questionId;
        public String answerId;
        public String answer;
        public Long timestamp;
    }
    public class MobileEdiaryRequest {
        public List<CustomAnswerWrapper> answers;
        public Integer percentage;
        public MobileEdiaryRequest() {
        }
        public MobileEdiaryRequest(String s) {
            MobileEdiaryRequest result = (MobileEdiaryRequest) JSON.deserialize(s, MobileEdiaryRequest.class);
            this.answers = result.answers;
            this.percentage = result.percentage;
        }
    }
    private static void sendResponse(Integer code, String error) {
        mobilePatientEdiaryResponse.errors.add(error);
        sendResponse(code);
    }
    private static void sendResponse(Integer code) {
        response.statusCode = code;
        sendResponse();
    }
    private static void sendResponse() {
        response.addHeader('Content-Type', 'application/json');
        cr.buildResponse(mobilePatientEdiaryResponse);
        response.responseBody = Blob.valueOf(JSON.serialize(cr, true));
    }
}