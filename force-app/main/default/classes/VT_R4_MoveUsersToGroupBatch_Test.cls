/**
 * Created by user on 05-Feb-20.
 */
@isTest
public with sharing class VT_R4_MoveUsersToGroupBatch_Test {

	@TestSetup
	private static void setupMethod() {
		User sysAdminUser = (User) new DomainObjects.User_t()
				.setProfile('System Administrator')
				.persist();
		User piUser = (User) new DomainObjects.User_t()
				.addContact(new DomainObjects.Contact_t())
				.setProfile('Primary Investigator')
				.persist();

	}
	@isTest
	public static void moveUsersToGroupTest() {
		Map<String, User> userByProfileNameMap = new Map<String, User>();
		for(User u : [SELECT Id, Username, Profile.Name, FirstName, LastName, VTD1_Profile_Name__c FROM User WHERE Username LIKE '%@scott.com']){
			userByProfileNameMap.put(u.VTD1_Profile_Name__c, u);
		}
		String adminProfName = userByProfileNameMap.containsKey('PT1') ? 'PT1' : 'System Administrator';
		System.debug('userByProfileNameMap ' + userByProfileNameMap);
		Integer sysAdminGroupMemberCount = [
				SELECT COUNT()
				FROM GroupMember
				WHERE Group.Name = 'System Administrator Users'
					AND UserOrGroupId = :userByProfileNameMap.get(adminProfName).Id];
		Integer pIGroupMemberCount = [
				SELECT COUNT()
				FROM GroupMember
				WHERE Group.Name = 'PI Users'
					AND UserOrGroupId = :userByProfileNameMap.get('Primary Investigator').Id];
		System.assertEquals(0,  sysAdminGroupMemberCount);
		System.assertEquals(0,  pIGroupMemberCount);
		Test.startTest();
		VT_R4_MoveUsersToGroupBatch moveUsersToGroupBatch = new VT_R4_MoveUsersToGroupBatch();
		Id batchId = Database.executeBatch(moveUsersToGroupBatch);
		Test.stopTest();
		pIGroupMemberCount = [
				SELECT COUNT()
				FROM GroupMember
				WHERE Group.Name = 'PI Users'
				AND UserOrGroupId = :userByProfileNameMap.get('Primary Investigator').Id];
		System.assertEquals(0,  PIGroupMemberCount);
	}
}