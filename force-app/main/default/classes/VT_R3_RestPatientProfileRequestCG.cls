/**
 * Created by Alexander Komarov on 18.06.2019.
 */

@RestResource(UrlMapping='/Patient/Profile/RequestCG/')
global with sharing class VT_R3_RestPatientProfileRequestCG {
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();


    @HttpPost
    global static String createSCTask() {
        RestResponse response = RestContext.response;
        RestRequest request = RestContext.request;

        String body = request.requestBody.toString();

        try {
            CaregiverData cgd = (CaregiverData) JSON.deserialize(body, CaregiverData.class);
            Case cas = VT_D1_CaseByCurrentUser.getCase().caseObj;
            List <VTD1_SC_Task__c> scTask = VT_D2_TNCatalogTasks.generateSCTasks(new List <String>{'T547'}, new List <Id>{cas.Id}, null, null, false);

            String relationship;
            if (cgd.relationship == 'Other') {
               relationship = cgd.relationshipOther;
            } else {
               relationship = cgd.relationship;
            }

            String comments = scTask[0].VTD1_Comments__c;
            comments = comments.replace('#firstName', cgd.firstName);
            comments = comments.replace('#lastName', cgd.lastName);
            comments = comments.replace('#email', cgd.emailAddress);
            comments = comments.replace('#phone', cgd.phoneNumber);
            comments = comments.replace('#relationship', relationship);
            scTask[0].VTD1_Comments__c = comments;

            insert scTask;
        } catch (Exception e) {
            response.statusCode = 500;
            cr.buildResponse('FAILURE. ' + e.getStackTraceString() + ' ' + e.getMessage(), 'Some text for NOT-success-banner "request caregiver" action');
            return JSON.serialize(cr, true);
        }
        cr.buildResponse('SUCCESS', System.Label.VTR3_Success_Add_Caregiver_Request);
        return JSON.serialize(cr, true);

    }

    public class CaregiverData {
        public String firstName;
        public String lastName;
        public String relationship;
        public String relationshipOther;
        public String emailAddress;
        public String phoneNumber;
    }

    private class CustomResponse {
        public String serviceResponse;
        public String serviceMessage;
    }
}