/**
 * Created by Denis Belkovskii on 5/6/2020.
 */
public class VT_R4_Video_Conf_Connect_Trigger_Handler {
    public static void updateVideoConferences(Set<Id> videoConfIds){
        if(videoConfIds.isEmpty()) return;
        Map<Id, Video_Conference__c> videoConferencesMap = new Map<Id, Video_Conference__c>(
            [SELECT Id, Call_Start__c, Duration__c FROM Video_Conference__c WHERE Id IN :videoConfIds]
        );
        List<Video_Conference__c> videoConferencesToUpdate = new List<Video_Conference__c>();
        List<AggregateResult> vccAggregateResults = [SELECT
                MIN(VTD1_Time_Connection__c) confStart,
                MAX(VTD1_Time_disconnection__c) confEnd,
                VTD1_Video_ConfId__c
        FROM VTD1_Video_Conf_Connection__c
        WHERE VTD1_Video_ConfId__c IN :videoConfIds
        GROUP BY VTD1_Video_ConfId__c];
        for (AggregateResult vcc : vccAggregateResults) {
            Id confId = (Id)vcc.get('VTD1_Video_ConfId__c');
            Video_Conference__c conf = videoConferencesMap.get(confId);
            Datetime confStart = (Datetime)vcc.get('confStart');
            if (conf.Call_Start__c != null && conf.Call_Start__c < confStart) {
                confStart = conf.Call_Start__c;
            }
            Datetime confEnd = (Datetime)vcc.get('confEnd');
            Decimal confDuration = ((confEnd.getTime() - confStart.getTime())/(1000*60.0)).setScale(2);//min
            conf.Call_Start__c = confStart;
            conf.Duration__c = String.valueOf(confDuration);
            videoConferencesToUpdate.add(conf);
        }
        update videoConferencesToUpdate;
    }
    @Future
    public static void archiveVideoConferenceConnections(Set<Id> vccIds){
        if(vccIds.isEmpty()) return;
        List<VTR4_Video_Conference_Connection_History__b> vcConnectionsHistory = new List<VTR4_Video_Conference_Connection_History__b>();
        for(VTD1_Video_Conf_Connection__c vcc : [SELECT
                                                    VTD1_Video_ConfId__c,
                                                    Name,
                                                    VTD1_Participant__c,
                                                    VTD1_Time_Connection__c,
                                                    VTD1_Time_disconnection__c,
                                                    VTD1_External_Id__c
                                                    FROM VTD1_Video_Conf_Connection__c
                                                    WHERE Id IN :vccIds]){
                VTR4_Video_Conference_Connection_History__b vccHistory = new VTR4_Video_Conference_Connection_History__b();
                vccHistory.VTR4_Video_Conference__c = vcc.VTD1_Video_ConfId__c;
                vccHistory.VTR4_VCC_Name__c = vcc.Name;
                vccHistory.VTR4_Participant__c = vcc.VTD1_Participant__c;
                vccHistory.VTR4_Time_Connected__c = vcc.VTD1_Time_Connection__c;
                vccHistory.VTR4_Time_Disconnected__c = vcc.VTD1_Time_disconnection__c;
                vccHistory.VTR4_External_Id__c = vcc.VTD1_External_Id__c;
                vcConnectionsHistory.add(vccHistory);
        }
        Database.insertImmediate(vcConnectionsHistory);
        Database.delete(new List<Id>(vccIds),false);
    }
}