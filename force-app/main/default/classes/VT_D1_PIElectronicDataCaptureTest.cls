@IsTest
public with sharing class VT_D1_PIElectronicDataCaptureTest {

    public static void getEDCTest() {
        User u;
        Test.startTest();
        List<Case> casesList = [SELECT VTD1_PI_user__c FROM Case];
        if(casesList.size() > 0){
            Case cas = casesList.get(0);
            String userId = cas.VTD1_PI_user__c;
            u = [SELECT Id FROM User WHERE Id =: userId];
        }

        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];
        Virtual_Site__c virtualSite = new Virtual_Site__c();
        virtualSite.VTD1_Study__c = study.Id;
        virtualSite.VTD1_Study_Site_Number__c = '12345';
        insert virtualSite;

        VTD1_Site_Data__c siteData = new VTD1_Site_Data__c();
        siteData.VTD1_Action_Required_Forms__c = 10;
        siteData.VTD1_Open_Queries__c = 20;
        siteData.VTD1_Site__c = virtualSite.Id;
        insert siteData;

        List<Study_Team_Member__c> studyTeamMemberList = [SELECT VTD1_VirtualSite__c FROM Study_Team_Member__c WHERE User__c =: u.Id];
        System.assert(studyTeamMemberList.size() > 0);

        Study_Team_Member__c studyTeamMember = studyTeamMemberList.get(0);
        studyTeamMember.VTD1_VirtualSite__c = virtualSite.Id;
        update studyTeamMember;

        System.runAs(u){
            System.debug([SELECT Id, VTD1_VirtualSite__c, Study__r.Name, Study__r.Id, User__c  FROM Study_Team_Member__c]);

            String electronicDataCaptureListString = VT_D1_PIElectronicDataCaptureController.getEDC();
            List<VT_D1_PIElectronicDataCaptureController.ElectronicDataCapture> electronicDataCaptureList = (List<VT_D1_PIElectronicDataCaptureController.ElectronicDataCapture>) JSON.deserialize(electronicDataCaptureListString, List<VT_D1_PIElectronicDataCaptureController.ElectronicDataCapture>.class);

            //System.assertEquals(1, electronicDataCaptureList.size());
            if(electronicDataCaptureList.size() > 0){

            }
        }
        Test.stopTest();

    }
}