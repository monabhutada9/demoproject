/**
* @author: Carl Judge
* @date: 14-Sep-18
* @description: Controller for VT_D2_VisitRequestConfirmation page
**/

public without sharing class VT_D2_VisitRequestConfirmationController {

    public static final String SCHEDULE_ACTION = 'SCHEDULE';
    public static final String RESCHEDULE_ACTION = 'RESCHEDULE';
    public static final String MISSING_ID_ERROR = 'No id param found';
    public static final String INVALID_ID_ERROR = 'id incorrect';
    public static final String MISSING_ACTION_ERROR = 'No action param found';
    public static final String INVALID_ACTION_ERROR = 'Unknown action';

    public class RequestException extends Exception{}

    private Id recordId;
    private VTD1_Actual_Visit__c visit;

    public String action {get; private set;}
    public String errorMsg {get; private set;}
    public String dateText {get; private set;}
    public String slotText {get; private set;}
    public Boolean alreadyScheduled {get; private set; }
    public Boolean hasValidationError {get; private set; }
    public String scheduleError {get; private set; }

    public void onLoad() {
        try {
            getParams();

            if (this.action != null) {
                validateRequest();
                processRequest();
            }
        } catch (Exception e) {
            this.errorMsg = e.getMessage();
            System.debug(this.errorMsg);
        }
    }

    public String getSchedule_action() {
        return SCHEDULE_ACTION;
    }

    private void getParams() {
        String action = ApexPages.currentPage().getParameters().get('action');
        if (action == null) {
            throw new RequestException(MISSING_ACTION_ERROR);
        } else {
            if (action.equalsIgnoreCase(SCHEDULE_ACTION)) {
                this.action = SCHEDULE_ACTION;
            } else if (action.equalsIgnoreCase(RESCHEDULE_ACTION)) {
                this.action = RESCHEDULE_ACTION;
            } else {
                throw new RequestException(INVALID_ACTION_ERROR);
            }
        }

        String recId = ApexPages.currentPage().getParameters().get('id');
        if (recId == null) {
            throw new RequestException(MISSING_ID_ERROR);
        } else if (! (recId instanceof Id)) {
            throw new RequestException(INVALID_ID_ERROR);
        } else {
            this.recordId = recId;
            SObjectType objectType = this.recordId.getSobjectType();

            if (objectType != VTD2_Time_Slot__c.getSObjectType() && (this.action == SCHEDULE_ACTION || objectType != VTD1_Actual_Visit__c.getSObjectType())) {
                throw new RequestException(INVALID_ID_ERROR);
            }
        }
    }

    public VTD1_Actual_Visit__c getVisit() {
        if (this.visit == null) {
            Id recId = this.recordId;
            String idClause = recId.getSobjectType() == VTD1_Actual_Visit__c.getSObjectType() ?
                '= :recId' : 'IN (SELECT VTD2_Actual_Visit__c FROM VTD2_Time_Slot__c WHERE Id = :recId)';

            String query = 'SELECT Id, ' +
                'Name, ' +
                'VTD1_Status__c, ' +
                'VTD1_Case__r.VTD1_Study__c, ' +
                'VTD1_Case__r.VTD1_PI_user__c, ' +
                'VTD1_Case__r.VTD1_PI_user__r.TimeZoneSidKey, ' +
                'Unscheduled_Visits__r.VTD1_Study__c, ' +
                'Unscheduled_Visits__r.VTD1_PI_user__c, ' +
                'Unscheduled_Visits__r.VTD1_PI_user__r.TimeZoneSidKey, ' +
                'VTD1_Visit_Duration__c, ' +
                'VTD1_Scheduled_Date_Time__c, ' +
                'VTD1_VisitType_SubType__c, ' +
                'RecordType.DeveloperName' +
                ' FROM VTD1_Actual_Visit__c' +
                ' WHERE Id ' + idClause;

            System.debug(query);
            system.debug(recId);

            this.visit = Database.query(query);
        }


        return this.visit;
    }

    public Case getCase() {
        VTD1_Actual_Visit__c visit = getVisit();
        return visit.RecordType.DeveloperName == 'VTD1_Unscheduled' ? visit.Unscheduled_Visits__r : visit.VTD1_Case__r;
    }

    public String getCalendarUrl() {
        return VisitRequestConfirmSite__c.getInstance().PICalendarURL__c;
    }

    private void validateRequest() {
        this.hasValidationError = false;

        VTD1_Actual_Visit__c visit = getVisit();

        if (visit.VTD1_Status__c == null) {
            this.hasValidationError = true;
            System.debug('Visit has null status');
        } else if (! visit.VTD1_Status__c.equalsIgnoreCase(VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED) &&
                   ! visit.VTD1_Status__c.equalsIgnoreCase(VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_REQUESTED))
        {
            this.hasValidationError = true;
            System.debug('Visit has invalid status');
        }
    }

    private void processRequest() {
        if (action == SCHEDULE_ACTION) {
            processSchedule();
        } else {
            processReschedule();
        }
    }

    private void processSchedule() {
        List<VTD2_Time_Slot__c> allSlots = [
            SELECT Id, VTD2_Actual_Visit__c, VTD2_Timeslot_Date_Time__c
            FROM VTD2_Time_Slot__c
            WHERE VTD2_Actual_Visit__c = :getVisitId()
        ];

        VTD2_Time_Slot__c chosenSlot;
        for (VTD2_Time_Slot__c item : allSlots) {
            if (item.Id == this.recordId) {
                chosenSlot = item;
                break;
            }
        }

        // if cant find record with this ID
        if (chosenSlot == null) {
            throw new RequestException(INVALID_ID_ERROR);
        }

        chosenSlot.VTD2_Status__c = VT_R4_ConstantsHelper_VisitsEvents.TIMESLOT_CONFIRMED;
        Integer visitDuration = 30;
        if (getVisit().VTD1_Visit_Duration__c != null) {
            visitDuration = getVisit().VTD1_Visit_Duration__c.intValue();
        }
        Datetime startTime;
        Datetime endTime;

        this.alreadyScheduled = getVisit().VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED;

        if (! this.alreadyScheduled) {
            startTime = chosenSlot.VTD2_Timeslot_Date_Time__c;
            endTime = chosenSlot.VTD2_Timeslot_Date_Time__c.addMinutes(visitDuration);

            this.dateText = startTime.format('EEEE, d-MMM-yyyy', getCase().VTD1_PI_user__r.TimeZoneSidKey);
            this.slotText = (startTime.format('h:mm aa', getCase().VTD1_PI_user__r.TimeZoneSidKey)
            + ' - ' + endTime.format('h:mm aa', getCase().VTD1_PI_user__r.TimeZoneSidKey)).toLowerCase();

            List<VTD2_Time_Slot__c> otherProposedSlots = [
                SELECT Id FROM VTD2_Time_Slot__c
                WHERE VTD2_Actual_Visit__c = :chosenSlot.VTD2_Actual_Visit__c
                AND Id != :chosenSlot.Id
                AND VTD2_Status__c = :VT_R4_ConstantsHelper_VisitsEvents.TIMESLOT_PROPOSED
            ];

            for (VTD2_Time_Slot__c item : otherProposedSlots) {
                item.VTD2_Status__c = VT_R4_ConstantsHelper_VisitsEvents.TIMESLOT_REJECTED;
            }

            String errorMessage = updateVisitAfterSchedule(chosenSlot);
            if (errorMessage == null) {
                List<VTD2_Time_Slot__c> slotsToUpdate = new List<VTD2_Time_Slot__c>();
                slotsToUpdate.addAll(otherProposedSlots);
                slotsToUpdate.add(chosenSlot);
                update slotsToUpdate;

            } else {
                scheduleError = errorMessage;
            }
        } else {
            system.debug('before time zone stuff');
            startTime = getVisit().VTD1_Scheduled_Date_Time__c;
            endTime = getVisit().VTD1_Scheduled_Date_Time__c.addMinutes(visitDuration);
            this.dateText = startTime.format('EEEE, d-MMM-yyyy', getCase().VTD1_PI_user__r.TimeZoneSidKey);
            this.slotText = (startTime.format('h:mm aa', getCase().VTD1_PI_user__r.TimeZoneSidKey)
            + ' - ' + endTime.format('h:mm aa', getCase().VTD1_PI_user__r.TimeZoneSidKey)).toLowerCase();
            system.debug('after time zone stuff');
        }
    }

    private void processReschedule() {
        List<VTD2_Time_Slot__c> slots = [
            SELECT Id
            FROM VTD2_Time_Slot__c
            WHERE (Id = :this.recordId OR VTD2_Actual_Visit__c = :this.recordId)
            AND VTD2_Status__c != :VT_R4_ConstantsHelper_VisitsEvents.TIMESLOT_REJECTED
        ];

        if (! slots.isEmpty()) {
            for (VTD2_Time_Slot__c item : slots) {
                item.VTD2_Status__c = VT_R4_ConstantsHelper_VisitsEvents.TIMESLOT_REJECTED;
            }
            update slots;
        }

        Study_Team_Member__c member;
        for (Study_Team_Member__c item : [
            SELECT Id, VTD2_PI_Reschedule__c
            FROM Study_Team_Member__c
            WHERE User__c = :getCase().VTD1_PI_user__c
            AND Study__c = :getCase().VTD1_Study__c
        ]) {
            member = item;
        }

        if (member != null) {
            member.VTD2_PI_Reschedule__c = member.VTD2_PI_Reschedule__c == null ? 1 : member.VTD2_PI_Reschedule__c + 1;
            update member;
        }

        updateVisitAfterReschedule();
    }

    public Id getVisitId() {
        return this.recordId.getSobjectType() == VTD1_Actual_Visit__c.getSObjectType() ? this.recordId : getVisit().Id;
    }

    private String updateVisitAfterSchedule(VTD2_Time_Slot__c chosenSlot) {
        VTD1_Actual_Visit__c visit = getVisit();
        return VT_D1_ActualVisitsHelper.createUpdateEvent(chosenSlot.VTD2_Timeslot_Date_Time__c,
                false, visit.Id, Integer.valueOf(visit.VTD1_Visit_Duration__c), visit.VTD1_VisitType_SubType__c, false, false);

        /*visit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED;
        visit.VTD1_Scheduled_Date_Time__c = chosenSlot.VTD2_Timeslot_Date_Time__c;
        update visit;*/
    }

    private void updateVisitAfterReschedule() {
        VTD1_Actual_Visit__c visit = getVisit();
        visit.VTD1_Status__c = visit.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED ?
            VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED : VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
        //visit.VTD1_Scheduled_Date_Time__c = null;
        visit.VTR2_RequestReschedule__c = true;
        update visit;
    }
}