public with sharing class VT_R2_PopulatePatientLanguageBatch implements Database.Batchable<sObject>{

    public Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id, LanguageLocaleKey, ContactId FROM User WHERE ContactId != null AND Contact.VTR2_Primary_Language__c = null AND (Contact.RecordTypeId = \'' + VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_CAREGIVER + '\' OR Contact.RecordTypeId = \''+ VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PATIENT + '\')';
        System.debug('query' + query);
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<User> scope){
        List<Contact> contactToUpdateList = new List<Contact>();
        String lg;
        for(Integer i = 0; i < scope.size(); i++) {
            if(scope[i].LanguageLocaleKey.length() > 2) {
                lg = 'en_US';
            } else {
                lg = scope[i].LanguageLocaleKey;
            }
            contactToUpdateList.add(
                    new Contact(
                            Id = scope[i].ContactId,
                            VTR2_Primary_Language__c = lg,
                            VTR2_Doc_Language__c = lg
                    )
            );
        }
        update contactToUpdateList;
    }

    public void finish(Database.BatchableContext BC){

    }

}