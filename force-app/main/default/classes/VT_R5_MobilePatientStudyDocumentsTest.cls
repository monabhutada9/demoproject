/**
 * @author: Alexander Komarov
 * @date: 15.10.2020
 * @description:
 */

@IsTest
public with sharing class VT_R5_MobilePatientStudyDocumentsTest {
    public static void firstTest() {
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        Id docId = [SELECT Id FROM VTD1_Document__c LIMIT 1].Id;
        Test.startTest();
        HttpCalloutMock cMock = new previewCallOutMock();
        Test.setMock(HttpCalloutMock.class, cMock);
        ContentVersion contentVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();
        ContentVersion insertedContVersion = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        new DomainObjects.ContentDocumentLink_t()
                .withLinkedEntityId(docId)
                .withContentDocumentId(insertedContVersion.ContentDocumentId)
                .persist();
        insert new VTD1_Document__Share(ParentId = docId, UserOrGroupId = toRun.Id, AccessLevel = 'Edit', RowCause = 'Manual');
        System.runAs(toRun) {
            VT_R5_AllTestsMobile.setRestContext('/patient/v1/study-documents', 'GET');
            VT_R5_MobileRestRouter.doGET();
        }
        Test.stopTest();
    }

    public static void secondTest() {
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        Id docId = [SELECT Id FROM VTD1_Document__c LIMIT 1].Id;
        Test.startTest();
        HttpCalloutMock cMock = new previewCallOutMock();
        Test.setMock(HttpCalloutMock.class, cMock);
        ContentVersion contentVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();
        ContentVersion insertedContVersion = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        new DomainObjects.ContentDocumentLink_t()
                .withLinkedEntityId(docId)
                .withContentDocumentId(insertedContVersion.ContentDocumentId)
                .persist();
        insert new VTD1_Document__Share(ParentId = docId, UserOrGroupId = toRun.Id, AccessLevel = 'Edit', RowCause = 'Manual');
        System.runAs(toRun) {
            VT_R5_AllTestsMobile.setRestContext('/patient/v1/study-documents/' + docId, 'PATCH');
            RestContext.request.requestBody = Blob.valueOf('{"nickname":"newNickName","comment":"newComment"}');
            VT_R5_MobileRestRouter.doPATCH();
        }
    }


    public static void thirdTest() {
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        Id docId = [SELECT Id FROM VTD1_Document__c LIMIT 1].Id;
        Test.startTest();
        ContentVersion contentVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();
        ContentVersion insertedContVersion = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        new DomainObjects.ContentDocumentLink_t()
                .withLinkedEntityId(docId)
                .withContentDocumentId(insertedContVersion.ContentDocumentId)
                .persist();
        insert new VTD1_Document__Share(ParentId = docId, UserOrGroupId = toRun.Id, AccessLevel = 'Edit', RowCause = 'Manual');
        System.runAs(toRun) {
            Id visitId = [SELECT Id FROM VTD1_Actual_Visit__c LIMIT 1].Id;
            VT_R5_AllTestsMobile.setRestContext('/patient/v1/study-documents', 'POST');
            RestContext.request.requestBody = Blob.valueOf('{"contentVersionId":"' + insertedContVersion.Id + '","visitId":"' + visitId + '","nickname":"newNickName","comment":"newComment"}');
            VT_R5_MobileRestRouter.doPOST();
        }
        Test.stopTest();
    }

    public static void fourthTest() {
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(toRun) {
            VT_R5_AllTestsMobile.setRestContext('/patient/v1/study-documents/visits', 'GET');
            VT_R5_MobileRestRouter.doGET();
        }
        Test.stopTest();
    }

    public static void fifthTest() {
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        Id docId = [SELECT Id FROM VTD1_Document__c LIMIT 1].Id;
        Test.startTest();
        HttpCalloutMock cMock = new previewCallOutMock();
        Test.setMock(HttpCalloutMock.class, cMock);
        ContentVersion contentVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();
        ContentVersion insertedContVersion = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        new DomainObjects.ContentDocumentLink_t()
                .withLinkedEntityId(docId)
                .withContentDocumentId(insertedContVersion.ContentDocumentId)
                .persist();
        insert new VTD1_Document__Share(ParentId = docId, UserOrGroupId = toRun.Id, AccessLevel = 'Edit', RowCause = 'Manual');
        System.runAs(toRun) {
            VT_R5_AllTestsMobile.setRestContext('/patient/v1/study-documents/' + docId, 'GET');
            VT_R5_MobileRestRouter.doGET();
        }
        Test.stopTest();
    }
    public static void sixthTest() {
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        Id docId = [SELECT Id FROM VTD1_Document__c LIMIT 1].Id;
        Test.startTest();
        ContentVersion contentVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();
        ContentVersion insertedContVersion = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        new DomainObjects.ContentDocumentLink_t()
                .withLinkedEntityId(docId)
                .withContentDocumentId(insertedContVersion.ContentDocumentId)
                .persist();
        insert new VTD1_Document__Share(ParentId = docId, UserOrGroupId = toRun.Id, AccessLevel = 'Edit', RowCause = 'Manual');
        System.runAs(toRun) {
            VT_R5_AllTestsMobile.setRestContext('/patient/v1/file/' + insertedContVersion.ContentDocumentId, 'GET');
            VT_R5_MobileRestRouter.doGET();
        }
        Test.stopTest();
    }

    public static void seventhTest() {
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        Id docId = [SELECT Id FROM VTD1_Document__c LIMIT 1].Id;
        Test.startTest();
        HttpCalloutMock cMock = new previewCallOutMock();
        Test.setMock(HttpCalloutMock.class, cMock);
        ContentVersion contentVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();
        ContentVersion insertedContVersion = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        new DomainObjects.ContentDocumentLink_t()
                .withLinkedEntityId(docId)
                .withContentDocumentId(insertedContVersion.ContentDocumentId)
                .persist();
        insert new VTD1_Document__Share(ParentId = docId, UserOrGroupId = toRun.Id, AccessLevel = 'Edit', RowCause = 'Manual');
        System.runAs(toRun) {
            VT_R5_AllTestsMobile.setRestContext('/patient/v1/study-documents/' + docId, 'GET');
            VT_R5_MobileRestRouter.doGET();
        }
        Test.stopTest();
    }

    private class previewCallOutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');
            res.setBody('TEXTVALUE');
            return res;
        }
    }
}