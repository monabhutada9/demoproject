/**
 * Created by user on 12/1/2020.
 */

@IsTest
private class UpdateSignatureTypeTest {
    @IsTest
    static void testBehavior() {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );
        DomainObjects.VTD1_Regulatory_Document_t regulatoryDocumentT = new DomainObjects.VTD1_Regulatory_Document_t()
                .setLevel('Study')
                .setWayToSendToDocuSign('Manual')
                .setDocumentType('Site Activation Approval Form')
                .setSiteRSU(true)
                .setSignatureRequired(true)
                .setSignatureType('Wet');
        DomainObjects.VTD1_Regulatory_Binder_t binder = new DomainObjects.VTD1_Regulatory_Binder_t()
                .addRegDocument(regulatoryDocumentT)
                .addStudy(study);
        VTD1_Document__c testDoc = (VTD1_Document__c) new DomainObjects.VTD1_Document_t()
                .addRegBinder(binder)
                .persist();
        VTD1_Document__c doc = [SELECT Id, VTR4_Signature_Type__c
                                    FROM VTD1_Document__c
                                    WHERE Id =: testDoc.Id LIMIT 1
        ];
        doc.VTR4_Signature_Type__c = null;
        update doc;
        Test.startTest();
        UpdateSignatureType obj = new UpdateSignatureType();
        DataBase.executeBatch(obj);
        Test.stopTest();
    }
}