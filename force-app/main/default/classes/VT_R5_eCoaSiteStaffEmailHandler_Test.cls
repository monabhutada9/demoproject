@IsTest
public with sharing class VT_R5_eCoaSiteStaffEmailHandler_Test {
    private static final String RESPONSE_GUID = 'response_guid';
    @TestSetup
    private static void setup() {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'))
                .setName('Study Name');
        DomainObjects.StudyTeamMember_t scrStm = new DomainObjects.StudyTeamMember_t()
            .setRecordTypeByName('SCR')
            .addUser(new DomainObjects.User_t()
                .setProfile('Site Coordinator')
                .addContact(new DomainObjects.Contact_t())
            )
            .addStudy(study);
        scrStm.persist();
        DomainObjects.VirtualSite_t site_t = new DomainObjects.VirtualSite_t()
            .addStudyTeamMember(scrStm)
            .addStudy(study)
            .setStudySiteNumber('0')
            .setEcoaGuid('site_000000000000000000000000000000000000');
        DomainObjects.User_t patientUserT = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile(VT_R4_ConstantsHelper_Profiles.PATIENT_PROFILE_NAME);
        patientUserT.persist();
        Case patientCase = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .addAccount(new DomainObjects.Account_t())
                .setStudy(study.id)
                .addVirtualSite(site_t)
                .addUser(patientUserT)
                .persist();
        VTD1_Survey__c survey = (VTD1_Survey__c) new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(patientCase.Id)
                .setRecordTypeByName('VTR5_External')
                .setVTD1_Due_Date(Datetime.now().addMinutes(-5))
                .setVTD1_Status(VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_COMPLETED)
                .persist();
    }

    @IsTest
    private static void doTest() {
        VTD1_Survey__c survey = [SELECT Id, Name, VTR5_Response_GUID__c FROM VTD1_Survey__c];
        survey.VTR5_Response_GUID__c = RESPONSE_GUID;
        update survey;

        VT_R5_eCoaSiteStaffEmailHandler handler = new VT_R5_eCoaSiteStaffEmailHandler();
        Messaging.InboundEmail inboundEmail = new Messaging.InboundEmail();
        inboundEmail.plainTextBody = RESPONSE_GUID;
        Messaging.InboundEnvelope inboundEnvelope = new Messaging.InboundEnvelope();
        inboundEnvelope.fromAddress = 'eCOA';
        handler.handleInboundEmail(inboundEmail, inboundEnvelope);
    }
}