/**
* @author: Carl Judge
* @date: 24-Jan-19
* @description: Test class for Document Sharing
**/

@IsTest
public class VT_D2_DocumentSharingTest {
    @testSetup
    private static void setupMethod() {
        Test.startTest();
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );


		VT_R3_GlobalSharing.disableForTest = true;
        new DomainObjects.HealthCloudGA_CandidatePatient_t()
                .setCaregiverEmail(DomainObjects.RANDOM.getEmail())

                .setRr_firstName('Mike')
                .setRr_lastName('Birn')
                .setConversion('Converted')
                .addStudy(study)
                .persist();

        Test.stopTest();

        DomainObjects.User_t userPI = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Primary Investigator');
        DomainObjects.StudyTeamMember_t stmPI = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(userPI)
                .setRecordTypeByName('PI');
        stmPI.persist();



        Case carePlan = [SELECT Id, VTD1_Study__c, VTD1_Patient_User__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];
        List<VTD1_Document__c> docs = new List<VTD1_Document__c>();
        for (Id recTypeId : VT_D2_DocumentSharing_Constants.SHARING_CONFIG.keySet()) {
            docs.add(new VTD1_Document__c(
                    VTD1_Clinical_Study_Membership__c = carePlan.Id,
                    RecordTypeId = recTypeId,
                    VTD1_Study__c = carePlan.VTD1_Study__c
            ));
        }
        insert docs;

        List<VTD1_Regulatory_Document__c> regDocs = new List<VTD1_Regulatory_Document__c>{
            new VTD1_Regulatory_Document__c(
                VTD1_Document_Type__c = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_SIGNATURE_PAGE,
                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI'
            ),
            new VTD1_Regulatory_Document__c(
                VTD1_Document_Type__c = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_PROTOCOL_AMENDMENT,
                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI'
            )
        };
        insert regDocs;

        List<VTD1_Regulatory_Binder__c> binders = new List<VTD1_Regulatory_Binder__c>{
            new VTD1_Regulatory_Binder__c(
                VTD1_Care_Plan_Template__c = study.Id,
                VTD1_Regulatory_Document__c = regDocs[0].Id
            ),
            new VTD1_Regulatory_Binder__c(
                VTD1_Care_Plan_Template__c = study.Id,
                VTD1_Regulatory_Document__c = regDocs[1].Id
            )
        };
        insert binders;

    }

    @isTest
    private static void doTest() {
        //start
        Case carePlan = [SELECT Id, VTD1_Study__c, VTD1_Patient_User__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];
        List<VTD1_Document__c> docs = [
                SELECT Id,VTD1_Study__c, VTD1_Clinical_Study_Membership__c
                FROM VTD1_Document__c
                WHERE VTD1_Clinical_Study_Membership__c = :carePlan.Id
        ];
        Test.startTest();
        for (VTD1_Document__c item : docs) {
            item.VTD1_Study__c = carePlan.VTD1_Study__c;
        }
        update docs;

        // Trigger recalc from Case
        Id patientId = carePlan.VTD1_Patient_User__c;
        carePlan.VTD1_Patient_User__c = UserInfo.getUserId();
        update carePlan;
        carePlan.VTD1_Patient_User__c = patientId;
        update carePlan;
        Test.stopTest();
    }


    @isTest
    private static void teststudyRecalc() {
        Case carePlan = [SELECT Id, VTD1_Study__c, VTD1_Patient_User__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];

        // Trigger recalc from Study
        HealthCloudGA__CarePlanTemplate__c study = [
            SELECT Id, VTD1_Virtual_Trial_Study_Lead__c
            FROM HealthCloudGA__CarePlanTemplate__c
            WHERE Id = :carePlan.VTD1_Study__c
        ];
        Id vtslId = study.VTD1_Virtual_Trial_Study_Lead__c;
        study.VTD1_Virtual_Trial_Study_Lead__c = UserInfo.getUserId();
        VT_R3_GlobalSharing.disableForTest = true;
        study.VTD1_Virtual_Trial_Study_Lead__c = vtslId;
        Test.startTest();
        update study;
        Test.stopTest();
    }

    @isTest
    private static void testSiteRecalc() {
        Test.startTest();
        Case carePlan = [SELECT Id, VTD1_Study__c, VTD1_Patient_User__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];

        // Trigger recalc from Virtual Site
        HealthCloudGA__CarePlanTemplate__c study = [
            SELECT Id, VTD1_Virtual_Trial_Study_Lead__c
            FROM HealthCloudGA__CarePlanTemplate__c
            WHERE Id = :carePlan.VTD1_Study__c
        ];
        Study_Team_Member__c piStm = [
                SELECT Id
                FROM Study_Team_Member__c
            WHERE Study__c = :study.Id
            AND User__r.Profile.Name = 'Primary Investigator'
            LIMIT 1
        ];
        Virtual_Site__c vSite = new Virtual_Site__c(VTD1_Study__c = study.Id, VTD1_Study_Site_Number__c = '12345');
        insert vSite;
        vSite.VTD1_Study_Team_Member__c = piStm.Id;
        update vSite;
        Test.stopTest();
    }

    @isTest
    private static void testPaRecalc() {
        Test.startTest();
        Case carePlan = [SELECT Id, VTD1_Study__c, VTD1_Patient_User__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];
        List<VTD1_Document__c> docs = [
                SELECT Id,VTD1_Study__c, VTD1_Clinical_Study_Membership__c
                FROM VTD1_Document__c
                WHERE VTD1_Clinical_Study_Membership__c = :carePlan.Id
        ];
        // Trigger recalc from Protocol Amendment
        HealthCloudGA__CarePlanTemplate__c study = [
            SELECT Id, VTD1_Virtual_Trial_Study_Lead__c
            FROM HealthCloudGA__CarePlanTemplate__c
            WHERE Id = :carePlan.VTD1_Study__c
        ];
        VTD1_Protocol_Amendment__c pa = new VTD1_Protocol_Amendment__c(VTD1_Study_ID__c = study.Id) ;
        insert pa;
        pa.VTD1_Protocol_Amendment_Document_Link__c = docs[0].Id;
        update pa;
        delete pa;
        Test.stopTest();
    }

    @isTest
    private static void testRecalculateSharing() {
        Test.startTest();
        Case carePlan = [SELECT Id, VTD1_Study__c, VTD1_Patient_User__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];
        List<VTD1_Document__c> docs = [
                SELECT Id,VTD1_Study__c, VTD1_Clinical_Study_Membership__c, RecordTypeId
                FROM VTD1_Document__c
                WHERE VTD1_Clinical_Study_Membership__c = :carePlan.Id
        ];
        VT_R3_GlobalSharing.doSharing(docs);
        Test.stopTest();
    }
}