/**
 * Created by Leonid Bartenev
 */

public abstract class VT_D1_AbstractAction {

    public static final String STATUS_SUCCESS = 'Success';
    public static final String STATUS_FAILED = 'Failed';
    public static final String STATUS_NOT_EXECUTED = 'Not Executed';

    public static final VT_D1_ActionQueueSettings__mdt ACTION_QUEUE_SETTINGS = [
            SELECT Id,VT_D1_Batch_Processing_Interval_Minutes__c,
                    VT_D1_Batch_Scope_Size__c,
                    VT_D1_Default_Action_Repeat_Interval_Min__c,
                    VT_D1_Default_Attempts_Count__c
            FROM VT_D1_ActionQueueSettings__mdt
            WHERE DeveloperName = 'Default'
    ];

    private Integer currentAttempt = 1;
    public Integer attemptsCount = ACTION_QUEUE_SETTINGS.VT_D1_Default_Attempts_Count__c.intValue();
    public Integer repeatIntervalMin = ACTION_QUEUE_SETTINGS.VT_D1_Default_Action_Repeat_Interval_Min__c.intValue();
    private transient String status = STATUS_NOT_EXECUTED;
    private String lastError;


    //must be implemented in child
    public abstract void execute();

    //must return class type of concrete child class
    public abstract Type getType();

    public void executeFuture() {
        executeFuture(JSON.serialize(this), getType().getName());
    }

    public void addToQueue() {
        if (status == STATUS_SUCCESS) return ;
        if (status == STATUS_NOT_EXECUTED && currentAttempt > attemptsCount) return;
        if (status == STATUS_FAILED && currentAttempt >= attemptsCount) return;
        if (status != STATUS_NOT_EXECUTED) currentAttempt++;
        VTD1_Queue_Action__c queueAction = new VTD1_Queue_Action__c(
                VTD1_Action_Class__c = getType().getName(),
                VTD1_Action_JSON__c = JSON.serialize(this),
                VTD1_Execution_Time__c = Datetime.now().addMinutes(repeatIntervalMin)
        );
        insert queueAction;
        if (status != STATUS_NOT_EXECUTED) currentAttempt--;
    }

    public VTD1_Queue_Action__c returnForMassAddToQueue() {
        if (status == STATUS_SUCCESS) return null;
        if (status == STATUS_NOT_EXECUTED && currentAttempt > attemptsCount) return null;
        if (status == STATUS_FAILED && currentAttempt >= attemptsCount) return null;
        if (status != STATUS_NOT_EXECUTED) currentAttempt++;
        VTD1_Queue_Action__c queueAction = new VTD1_Queue_Action__c(
                VTD1_Action_Class__c = getType().getName(),
                VTD1_Action_JSON__c = JSON.serialize(this),
                VTD1_Execution_Time__c = Datetime.now().addMinutes(repeatIntervalMin)
        );
        if (status != STATUS_NOT_EXECUTED) currentAttempt--;
        return queueAction;
    }

    public String getStatus() {
        return status;
    }

    protected void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return lastError;
    }

    protected void setMessage(String message) {
        this.lastError = message;
    }

    public static VT_D1_AbstractAction loadAction(VTD1_Queue_Action__c queueAction) {
        return loadAction(queueAction.VTD1_Action_JSON__c, queueAction.VTD1_Action_Class__c);
    }

    public static VT_D1_AbstractAction loadAction(String actionJSON, String className) {
        VT_D1_AbstractAction action = (VT_D1_AbstractAction) JSON.deserialize(actionJSON, Type.forName(className));
        action.status = STATUS_NOT_EXECUTED;
        action.lastError = null;
        return action;
    }

    public Integer getAttemptNumber() {
        return currentAttempt;
    }

    @Future(Callout=true)
    private static void executeFuture(String actionJSON, String className) {
        loadAction(actionJSON, className).execute();
    }

    public Boolean isLastAttempt() {
        return currentAttempt >= attemptsCount;
    }

    public void createTasksTroughTnForCase(String tnRecord, List<String> CaseIds, List<Id> ownerIds, String errorMessage) {
        List<VT_D2_TNCatalogTasks.ParamsHolder> paramsList = new List <VT_D2_TNCatalogTasks.ParamsHolder>();
        VT_D2_TNCatalogTasks.ParamsHolder params = new VT_D2_TNCatalogTasks.ParamsHolder();
        params.tnCode = tnRecord;
        params.sourceIds = CaseIds;
        params.assignedToIDs = ownerIds;
        paramsList.add(params);
        List<Task> task = VT_D2_TNCatalogTasks.generateTask(paramsList);

        for (Integer i = 0; i < ownerIds.size(); i++) {
            String comments = task[i].Description;
            comments = comments.replace('#getMessage', errorMessage);
            task[i].Description = comments;
        }
        update task;
    }
}