/**
 * Created by Maksim Fedarenka on 7/20/2020.
 */

/**
 * @description Collects cases in 'Dropped' or 'Does Not Qualify' statuses, executes the deactivation process of the
 * patients, related to such Cases
 * @jira SH-12593 Create batch to collect patients that we need to deactivate (Historical data)
 */
public with sharing class VT_R5_InactivePatientsDeactivationBatch implements Database.Batchable<SObject> {
    /**
     * Launches the execution of the batch
     * @param batchSize Number of records to be passed into the execute method for batch processing.
     */
    public static void run(Integer batchSize) {
        Database.executeBatch(new VT_R5_InactivePatientsDeactivationBatch(), batchSize);
    }

    public Iterable<SObject> start(Database.BatchableContext bc) {
        System.debug('VT_R5_InactivePatientsDeactivationBatch: the execution has been started');
        return Database.getQueryLocator ([
                SELECT Id
                FROM Case
                WHERE (Status = :VT_R4_ConstantsHelper_Statuses.CASE_DROPPED OR Status = :VT_R4_ConstantsHelper_Statuses.CASE_DOES_NOT_QUALIFY)
                    AND (Contact.RecordType.DeveloperName = :VT_R4_ConstantsHelper_Profiles.PATIENT_PROFILE_NAME OR Contact.RecordType.DeveloperName = :VT_R4_ConstantsHelper_Profiles.CAREGIVER_PROFILE_NAME)
                    AND VTD1_Patient_User__r.IsActive = TRUE
        ]);
    }

    public void execute(Database.BatchableContext bc, List<Case> scope) {
        Map<Id, Case> caseMap = new Map<Id, Case>(scope);
        VT_R5_UserService.deactivateUsers(new List<Id>(caseMap.keySet()), false);
    }

    public void finish(Database.BatchableContext bc) {
        System.debug('VT_R5_InactivePatientsDeactivationBatch: the execution has been completed');
    }
}