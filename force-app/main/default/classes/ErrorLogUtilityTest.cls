/************************************************************************
 * Name  : ErrorLogUtilityTest
 * Author : Mahesh Shimpi
 * Desc  : This is the Test class for ErrorLogUtility class, used for testing all the functionlity.
 * 
 *
 * Modification Log:
 * ----------------------------------------------------------------------
 * Developer                Date                description
 * ----------------------------------------------------------------------
 * Mahesh Shimpi             15/06/2020           Original 
 * 
 *************************************************************************/
@isTest
public class ErrorLogUtilityTest {
    /* 
     * method : testErrorLogUtility1()
     * param : none
     * description : Test method is used to test single exception logging functionlity.
     * return : void
     */
    @isTest
    public static void testErrorLogUtility1() {
        Test.startTest();
        try{
            Id tskId = [select Id from Task Where AccountId = '123456789012345'][0].Id;
        }catch(Exception ex){
            System.debug('Exception occured ' + ex.getMessage());
            ErrorLogUtility.logException(ex, ErrorLogUtility.ApplicationArea.APP_AREA_CHAT_AUTOMATION, ErrorLogUtilityTest.class.getName());
        }
        List<VTR4_Conversion_Log__c> errorLogs1 = [Select Id, VTR5_RecordId__c ,
                                    VTR4_Error_Message__c,                    
                                    VTR5_LineNumber__c,
                                    VTR5_StackTrace__c,
                                    VTR5_ExceptionType__c,
                                    VTR5_ApplicationArea__c,
                                    VTR5_ClassName__c 
                                    FROM VTR4_Conversion_Log__c
                                    WHERE VTR5_ClassName__c =: ErrorLogUtilityTest.class.getName()];
        System.assertEquals(errorLogs1.size(), 1);
        Test.stopTest();
    }
    
    /* 
     * method : testErrorLogUtilityAdditionalInfo()
     * param : none
     * description : Test method is used to test single exception logging functionlity with additional info/record details
     * return : void
     */
    @isTest
    public static void testErrorLogUtilityAdditionalInfo() {
        Test.startTest();
        try{
            Id tskId = [select Id from Task Where AccountId = '123456789012345'][0].Id;
        }catch(Exception ex){
            System.debug('Exception occured ' + ex.getMessage());
            ErrorLogUtility.logException(ex, ErrorLogUtility.ApplicationArea.APP_AREA_CHAT_AUTOMATION, ErrorLogUtilityTest.class.getName(),'Additional Information');
        }
        List<VTR4_Conversion_Log__c> errorLogs1 = [Select Id, VTR5_RecordId__c ,
                                    VTR4_Error_Message__c,                    
                                    VTR5_LineNumber__c,
                                    VTR5_StackTrace__c,
                                    VTR5_ExceptionType__c,
                                    VTR5_ApplicationArea__c,
                                    VTR5_ClassName__c ,
                                    VTR5_RecordDetails__c
                                    FROM VTR4_Conversion_Log__c
                                    WHERE VTR5_ClassName__c =: ErrorLogUtilityTest.class.getName()];
        System.assertEquals(errorLogs1.size(), 1);
        Test.stopTest();
    }
    
    /* 
     * method : testErrorLogUtility2()
     * param : none
     * description : Test method is used to test single error logging functionlity.
     * return : void
     */
    @isTest
    public static void testErrorLogUtility2() {
        Test.startTest();        
        Account[] accts = new List<Account>{ new Account(), new Account(), new Account()};
        Database.SaveResult[] srList = Database.insert(accts, false);
        // Iterate through each returned result
        List<Database.Error> errs = new List<Database.Error>();
        for (Database.SaveResult sr : srList) {
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    errs.add(err);
                }
            }
        }
        ErrorLogUtility.logErrorsInBulk(srList, accts, ErrorLogUtility.ApplicationArea.APP_AREA_OUTLIER_READING, ErrorLogUtilityTest.class.getName());
        List<VTR4_Conversion_Log__c> lstConversionLog = [Select Id, VTR5_RecordId__c ,
                                    VTR4_Error_Message__c,                    
                                    VTR5_LineNumber__c,
                                    VTR5_StackTrace__c,
                                    VTR5_ExceptionType__c,
                                    VTR5_ApplicationArea__c,
                                    VTR5_ClassName__c 
                                    FROM VTR4_Conversion_Log__c
                                    WHERE VTR5_ClassName__c =: ErrorLogUtilityTest.class.getName() ];
        System.assertEquals(lstConversionLog.size(), 3);
    }
    /* 
     * method : testErrorLogUtility3()
     * param : none
     * description : Test method is used to test Bulk error logging functionlity.
     * return : void
     */    
    @isTest
    public static void testErrorLogUtility3() {
        Test.startTest();        
        Account[] accts = new List<Account>{ new Account(), new Account(), new Account()};
        Database.SaveResult[] srList = Database.insert(accts, false);
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    ErrorLogUtility.logError(err, ErrorLogUtility.ApplicationArea.APP_AREA_PATIENT_CONVERSION, ErrorLogUtilityTest.class.getName());
                }
            }
        }
        List<VTR4_Conversion_Log__c> errorLogs = [Select Id, VTR5_RecordId__c ,
                                    VTR4_Error_Message__c,                    
                                    VTR5_LineNumber__c,
                                    VTR5_StackTrace__c,
                                    VTR5_ExceptionType__c,
                                    VTR5_ApplicationArea__c,
                                    VTR5_ClassName__c 
                                    FROM VTR4_Conversion_Log__c
                                    WHERE VTR5_ClassName__c =: ErrorLogUtilityTest.class.getName() ];
        System.assertEquals(errorLogs.size(), 3);
    }
    
    /* 
     * method : testLogErrorsInBulk()
     * param : none
     * description : Test method is used to test Bulk error logging functionlity.
     * return : void
     */    
    @isTest
    private static void testLogErrorsInBulk(){       
        Account[] accts = new List<Account>{ new Account(), new Account(), new Account()};
        Test.startTest();
        	Database.SaveResult[] srList = Database.insert(accts, false);        
        	ErrorLogUtility.logErrorsInBulk(VT_R5_DatabaseResultWrapper.wrapResults(srList),accts,ErrorLogUtility.ApplicationArea.APP_AREA_PATIENT_CONVERSION, ErrorLogUtilityTest.class.getName(), false);
        Test.stopTest();
        List<VTR4_Conversion_Log__c> errorLogs = [Select Id, VTR5_RecordId__c ,
                                    VTR4_Error_Message__c,                    
                                    VTR5_LineNumber__c,
                                    VTR5_StackTrace__c,
                                    VTR5_ExceptionType__c,
                                    VTR5_ApplicationArea__c,
                                    VTR5_ClassName__c 
                                    FROM VTR4_Conversion_Log__c
                                    WHERE VTR5_ClassName__c =: ErrorLogUtilityTest.class.getName()];
        System.assertEquals(errorLogs.size(), 3);
    }
}