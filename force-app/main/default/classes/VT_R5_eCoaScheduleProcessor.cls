/**
* @author: Carl Judge
* @date: 12-Aug-20
**/

public without sharing class VT_R5_eCoaScheduleProcessor {
    private Map<Id, String> userSchedules = new Map<Id, String>();

    public VT_R5_eCoaScheduleProcessor addUserSchedule(Id userId, String schedule) {
        this.userSchedules.put(userId, schedule);
        return this;
    }

    public void processSchedules() {
        if (this.userSchedules.isEmpty()) { return; }

        List<VTD1_Survey__c> surveys = new List<VTD1_Survey__c>();
        List<String> compositeKeys = new List<String>();
        Id externalRecTypeId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('VTR5_External').getRecordTypeId();

        Map<Id, User> userMap = new Map<Id, User>([
            SELECT Id, Contact.VTD1_Clinical_Study_Membership__c, VTR5_eCOA_Guid__c
            FROM User
            WHERE Id IN :userSchedules.keySet()
        ]);

        for (Id userId : userSchedules.keySet()) {
            ScheduleResponse response = (ScheduleResponse)JSON.deserialize(userSchedules.get(userId), ScheduleResponse.class);
            if (response != null && response.eproProtoStatusDef != null && response.eproProtoStatusDef.diaries != null) {
                User usr = userMap.get(userId);
                for (Diary diary : response.eproProtoStatusDef.diaries) {
                    if (!String.isEmpty(diary.ruleGuid)) {
                        String compositeKey = usr.VTR5_eCOA_Guid__c + diary.ruleGuid + diary.windowBegin.valueOf;
                        surveys.add(new VTD1_Survey__c(
                            VTD1_CSM__c = usr.Contact.VTD1_Clinical_Study_Membership__c,
                            VTD1_Patient_User_Id__c = userId,
                            RecordTypeId = externalRecTypeId,
                            Name = diary.ruleName,
                            VTR5_eCoa_ruleGuid__c = diary.ruleGuid,
                            VTD1_Date_Available__c = Datetime.newInstance(diary.windowBegin.valueOf),
                            VTD1_Due_Date__c = Datetime.newInstance(diary.windowEnd.valueOf + 1),
                            VTR5_eCoaCompositeKey__c = compositeKey
                        ));
                        compositeKeys.add(compositeKey);
                    }
                }
            }
        }

        upsert surveys VTR5_eCoaCompositeKey__c;

        /*
        delete [
            SELECT Id
            FROM VTD1_Survey__c
            WHERE RecordTypeId = :externalRecTypeId
            AND VTD1_Status__c != :VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_COMPLETED
            AND VTR5_eCoaCompositeKey__c NOT IN :compositeKeys
            AND VTD1_Patient_User_Id__c IN :userSchedules.keySet()
        ];*/
    }

    private class ScheduleResponse {
        EproProtoStatusDef eproProtoStatusDef;
    }
    private class EproProtoStatusDef {
        List<Diary> diaries;
    }
    private class Diary {
        String ruleGuid;
        String ruleName;
        Window windowBegin;
        Window windowEnd;
    }
    private class Window {
        Long valueOf;
    }
}