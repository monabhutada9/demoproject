/**
 * Created by User on 19/05/13.
 */
@isTest
public with sharing class VT_R2_PIMyPatientEDiaryItemContrlTest {
    public static void getCurrentUserTest() {
        Test.startTest();
        System.assertNotEquals(null, VT_R2_PIMyPatientEDiaryItemController.getCurrentUserInfo());
        Test.stopTest();
    }
    public static void getQuestionsAnswersTest() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id, Name FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        VTD1_Protocol_ePRO__c protocolEPROT = [SELECT Id FROM VTD1_Protocol_ePRO__c LIMIT 1];
        VTD1_Survey__c survey = [SELECT Id FROM VTD1_Survey__c LIMIT 1];
        VTD1_Survey_Answer__c answer = [SELECT Id FROM VTD1_Survey_Answer__c LIMIT 1];
        VTR2_SCRPatientEDiary.AnswerWrapper answ = new VTR2_SCRPatientEDiary.AnswerWrapper();
        answ.answer = 'Test';
        answ.answerId = answer.Id;
        VTR2_SCRPatientEDiary.AnswerWrapper answ2 = new VTR2_SCRPatientEDiary.AnswerWrapper();
        answ2.answer = 'Test2';

        Test.startTest();
        List <VTD1_ProtocolVisit__c> protocolVisits = createTestVisitProtocols(study.Id);
        List <VTD1_Protocol_ePRO__c> protocolEPROS = createEPROtocols(study.Id, protocolVisits);
        List <VTR2_Protocol_eDiary_Group__c> groups = createEDiaryGroups(protocolEPROS);
        List <VTR2_Protocol_eDiary_Scale__c> scales = createEDiaryScales(protocolEPROS);
        List <VTR2_ProtocoleDiaryScaleResponse__c> scaleResponses = createEDiaryScaleResponses(scales);
        List <VTD1_Protocol_ePro_Question__c> protocolEProQuestions = createProtocolEProQuestions(protocolEPROS, groups, scales);

        VT_R2_PIMyPatientEDiaryItemController.getQuestionsAnswers(survey.Id, protocolEPROT.Id);

        VT_R2_PIMyPatientEDiaryItemController.PiMyPatientEDiaryItemSelector pi = new VT_R2_PIMyPatientEDiaryItemController.PiMyPatientEDiaryItemSelector();
        pi.getQuestionsAnswers(survey.Id, protocolEPROT.Id);

        VT_R2_PIMyPatientEDiaryItemController.ScrMyPatientEDiaryItemSelector scr = new VT_R2_PIMyPatientEDiaryItemController.ScrMyPatientEDiaryItemSelector();
        scr.getQuestionsAnswers(survey.Id, protocolEPROT.Id);
        Test.stopTest();
    }

    private static List <VTD1_Protocol_ePro_Question__c> createProtocolEProQuestions(List <VTD1_Protocol_ePRO__c> protocolEPROS, List <VTR2_Protocol_eDiary_Group__c> groups, List <VTR2_Protocol_eDiary_Scale__c> scales) {
        List <VTD1_Protocol_ePro_Question__c> protocolEProQuestions = new List<VTD1_Protocol_ePro_Question__c>();
        for (VTD1_Protocol_ePRO__c protocolEPRO : protocolEPROS) {
            if (protocolEPRO.Name == 'TestAsk3' || protocolEPRO.Name == 'TestAsk4') {
                for (Integer i = 0; i < 3; i++) {
                    VTD1_Protocol_ePro_Question__c question = new VTD1_Protocol_ePro_Question__c();
                    question.VTD1_Protocol_ePRO__c = protocolEPRO.Id;
                    question.VTD1_Type__c = 'Short Text';
                    if (i < 2) {
                        question.VTD1_Group_number__c = 1;
                    } else {
                        question.VTD1_Group_number__c = 2;
                    }
                    question.VTD1_Number__c = (i + 1);
                    question.VTR4_Number__c = String.valueOf(question.VTD1_Number__c);
                    question.VTR2_Protocol_eDiary_Scale__c = scales[0].Id;
                    question.VTR2_Scoring_Type__c = 'Auto';
                    question.VTD1_Type__c = 'Radio';
                    question.VTR2_Auto_Score_Default__c = 4;
                    protocolEProQuestions.add(question);
                }
            } else {
                VTD1_Protocol_ePro_Question__c question = new VTD1_Protocol_ePro_Question__c();
                question.VTD1_Protocol_ePRO__c = protocolEPRO.Id;
                question.VTD1_Type__c = 'Short Text';
                question.VTD1_Safety_Threshold_Lower__c = 3;
                question.VTD1_Safety_Threshold_Upper__c = 7;
                question.VTR2_Protocol_eDiary_Scale__c = scales[1].Id;
                question.VTR2_Scoring_Type__c = 'Auto';
                question.VTD1_Type__c = 'Radio';
                question.VTR2_Auto_Score_Default__c = 4;
                protocolEProQuestions.add(question);
            }
        }
        insert protocolEProQuestions;

        List <VTR2_Protocol_eDiary_Group_Question__c> groupQuestions = new List<VTR2_Protocol_eDiary_Group_Question__c>();
        for (VTD1_Protocol_ePro_Question__c question : protocolEProQuestions) {
            groupQuestions.add(new VTR2_Protocol_eDiary_Group_Question__c(
                    VTR2_Protocol_eDiary_Question__c = question.Id,
                    VTR2_Protocol_eDiary_Group__c = groups[0].Id
            ));
        }
        insert groupQuestions;

        return protocolEProQuestions;
    }

    private static List <VTR2_ProtocoleDiaryScaleResponse__c> createEDiaryScaleResponses(List <VTR2_Protocol_eDiary_Scale__c> scales) {
        List <VTR2_ProtocoleDiaryScaleResponse__c> scaleResponses = new List<VTR2_ProtocoleDiaryScaleResponse__c>();
        Integer num = 1;
        for (VTR2_Protocol_eDiary_Scale__c scale : scales) {
            VTR2_ProtocoleDiaryScaleResponse__c response = new VTR2_ProtocoleDiaryScaleResponse__c(
                    VTR2_Protocol_eDiary_Scale__c = scale.Id,
                    VTR2_Response__c = 'TestResponse_' + num,
                    VTR2_Score__c = 5
            );
            scaleResponses.add(response);
        }
        insert scaleResponses;
        return scaleResponses;
    }

    private static List <VTR2_Protocol_eDiary_Scale__c> createEDiaryScales(List <VTD1_Protocol_ePRO__c> protocolEPROS) {
        List <VTR2_Protocol_eDiary_Scale__c> scales = new List<VTR2_Protocol_eDiary_Scale__c>();
        Integer num = 1;
        for (VTD1_Protocol_ePRO__c protocol : protocolEPROS) {
            VTR2_Protocol_eDiary_Scale__c scale = new VTR2_Protocol_eDiary_Scale__c(
                    Name = 'TestScale_' + (num++),
                    VTR2_Protocol_eDiary__c = protocol.Id
            );
            scales.add(scale);
        }
        insert scales;
        return scales;
    }

    private static List <VTR2_Protocol_eDiary_Group__c> createEDiaryGroups(List <VTD1_Protocol_ePRO__c> protocolEPROS) {
        List <VTR2_Protocol_eDiary_Group__c> groups = new List<VTR2_Protocol_eDiary_Group__c>();
        Integer num = 1;
        for (VTD1_Protocol_ePRO__c protocol : protocolEPROS) {
            VTR2_Protocol_eDiary_Group__c eDiarygroup = new VTR2_Protocol_eDiary_Group__c(
                    Name = 'TestGroup_' + (num++),
                    VTR2_Protocol_eDiary__c = protocol.Id,
                    VTR2_Page_Number__c = 3,
                    VTR2_Number_Within_Page__c = 1
            );
            groups.add(eDiarygroup);
        }
        insert groups;
        return groups;
    }

    private static List <VTD1_Protocol_ePRO__c> createEPROtocols(Id studyId, List <VTD1_ProtocolVisit__c> protocolVisits) {
        List <VTD1_Protocol_ePRO__c> protocols = new List<VTD1_Protocol_ePRO__c>();

        VTD1_Protocol_ePRO__c protocol = new VTD1_Protocol_ePRO__c();
        protocol.VTD1_Study__c = studyId;
        protocol.Name = 'TestAsk1';
        protocol.VTD1_Type__c = 'eDiary';
        protocol.VTD1_Trigger__c = 'Event';
        protocol.VTD1_Period__c = 'Daily';
        protocol.VTD1_Subject__c = 'Patient';
        protocol.VTR4_EventType__c = 'Visit Completion';
        protocol.VTD1_Event__c = protocolVisits[0].Id;
        protocols.add(protocol);

        protocol = new VTD1_Protocol_ePRO__c();
        protocol.VTD1_Study__c = studyId;
        protocol.Name = 'TestAsk2';
        protocol.VTD1_Type__c = 'eDiary';
        protocol.VTD1_Trigger__c = 'Event';
        protocol.VTD1_Period__c = 'Daily';
        protocol.VTD1_Subject__c = 'Patient';
        protocol.VTR4_EventType__c = 'Visit Completion';
        protocol.VTD1_Caregiver_on_behalf_of_Patient__c = true;
        protocol.VTD1_Event__c = protocolVisits[0].Id;
        protocols.add(protocol);

        protocol = new VTD1_Protocol_ePRO__c();
        protocol.VTD1_Study__c = studyId;
        protocol.Name = 'TestAsk3';
        protocol.VTD1_Type__c = 'eDiary';
        protocol.VTD1_Trigger__c = 'Event';
        protocol.VTD1_Period__c = 'Daily';
        protocol.VTD1_Subject__c = 'Patient';
        protocol.VTR4_EventType__c = 'Visit Completion';
        //protocol.VTD1_First_Available__c = protocolVisits[0].Id;
        protocol.VTD1_Event__c = protocolVisits[0].Id;
        protocols.add(protocol);

        protocol = new VTD1_Protocol_ePRO__c();
        protocol.VTD1_Study__c = studyId;
        protocol.Name = 'TestAsk4';
        protocol.VTD1_Type__c = 'eDiary';
        protocol.VTD1_Trigger__c = 'Event';
        protocol.VTD1_Period__c = 'Daily';
        protocol.VTD1_Subject__c = 'Caregiver';
        protocol.VTD1_Event__c = protocolVisits[0].Id;
        protocol.VTD1_Safety_Keywords__c = 'TestWord';
        protocol.VTR4_EventType__c = 'Visit Completion';
        protocols.add(protocol);

        protocol = new VTD1_Protocol_ePRO__c();
        protocol.VTD1_Study__c = studyId;
        protocol.Name = 'TestAsk5';
        protocol.VTD1_Type__c = 'eDiary';
        protocol.VTD1_Trigger__c = 'Event';
        protocol.VTD1_Period__c = 'Daily';
        protocol.VTD1_Subject__c = 'Caregiver';
        protocol.VTD1_Reminder_Window__c = 0.02;
        protocol.VTR4_EventType__c = 'Visit Completion';
        //protocol.VTD1_First_Available__c = protocolVisits[0].Id;
        protocol.VTD1_Event__c = protocolVisits[0].Id;
        protocols.add(protocol);


        for (VTD1_Protocol_ePRO__c protocolEPRO : protocols) {

            if (protocolEPRO.Name == 'TestAsk1')
                protocolEPRO.VDD1_Scoring_Window__c = 0.04; else
                    protocolEPRO.VDD1_Scoring_Window__c = 4;

            if (protocolEPRO.Name == 'TestAsk5')
                protocolEPRO.VTD1_Response_Window__c = 0.04; else
                    protocolEPRO.VTD1_Response_Window__c = 4;
        }

        insert protocols;
        return protocols;
    }

    private static List <VTD1_ProtocolVisit__c> createTestVisitProtocols(Id studyId) {
        List <VTD1_ProtocolVisit__c> protocolVisits = new List<VTD1_ProtocolVisit__c>();
        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Study__c = studyId;
        protocolVisit.RecordTypeId = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByName().get('Onboarding').getRecordTypeId();
        protocolVisit.VTD1_Onboarding_Type__c = 'Initial Greeting';
        protocolVisit.VTD1_VisitType__c = 'Study Team';
        protocolVisit.VTD1_VisitDuration__c = '1 hour';
        protocolVisit.VTD1_Range__c = 1;
        protocolVisit.VTR2_Visit_Participants__c = 'Patient Guide;Patient';
        protocolVisit.VTD1_VisitNumber__c = 'O1';
        protocolVisits.add(protocolVisit);

        protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Study__c = studyId;
        protocolVisit.RecordTypeId = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByName().get('Onboarding').getRecordTypeId();
        protocolVisit.VTD1_Onboarding_Type__c = 'Consent';
        protocolVisit.VTD1_VisitType__c = 'Study Team';
        protocolVisit.VTD1_VisitDuration__c = '15 minutes';
        protocolVisit.VTD1_Range__c = 1;
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisit.VTD1_VisitNumber__c = 'O2';
        protocolVisits.add(protocolVisit);

        protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Study__c = studyId;
        protocolVisit.RecordTypeId = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByName().get('Onboarding').getRecordTypeId();
        protocolVisit.VTD1_Onboarding_Type__c = 'Screening';
        protocolVisit.VTD1_VisitType__c = 'Study Team';
        protocolVisit.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit.VTD1_Range__c = 1;
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisit.VTD1_VisitNumber__c = 'O3';
        protocolVisits.add(protocolVisit);

        protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Study__c = studyId;
        protocolVisit.RecordTypeId = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByName().get('Protocol').getRecordTypeId();
        protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit.VTD1_VisitType__c = 'Labs';
        protocolVisit.VTD1_VisitDuration__c = '15 minutes';
        protocolVisit.VTD1_Range__c = 1;
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisits.add(protocolVisit);

        protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Study__c = studyId;
        protocolVisit.RecordTypeId = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByName().get('Protocol').getRecordTypeId();
        protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit.VTD1_VisitType__c = 'Health Care Professional (HCP)';
        protocolVisit.VTD1_VisitDuration__c = '15 minutes';
        protocolVisit.VTD1_Range__c = 1;
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisits.add(protocolVisit);

        protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Study__c = studyId;
        protocolVisit.RecordTypeId = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByName().get('Protocol').getRecordTypeId();
        protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit.VTD1_VisitType__c = 'Health Care Professional (HCP)';
        protocolVisit.VTD1_VisitDuration__c = '45 minutes';
        protocolVisit.VTD1_Range__c = 1;
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisits.add(protocolVisit);

        insert protocolVisits;
        return protocolVisits;
    }


    public static void flagSurveyAsPossibleConcernTest() {
        VTD1_Survey_Answer__c answer = [SELECT Id FROM VTD1_Survey_Answer__c LIMIT 1];
        VTD1_Survey__c survey = [SELECT Id FROM VTD1_Survey__c LIMIT 1];
        VTR2_SCRPatientEDiary.AnswerWrapper answ = new VTR2_SCRPatientEDiary.AnswerWrapper();
        answ.answer = 'Test';
        answ.answerId = answer.Id;
        VTR2_SCRPatientEDiary.AnswerWrapper answ2 = new VTR2_SCRPatientEDiary.AnswerWrapper();
        answ2.answer = 'Test2';

        User scrUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Site Coordinator')
                .persist();
        User piUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Primary Investigator')
                .persist();
        Test.startTest();
        System.runAs(scrUser) {
            VT_R2_PIMyPatientEDiaryItemController.flagSurveyAsPossibleConcern(survey.Id);
        }
        System.runAs(piUser) {
            VT_R2_PIMyPatientEDiaryItemController.flagSurveyAsPossibleConcern(survey.Id);
        }
        Test.stopTest();
    }
    public static void updateSurveyStatusTest() {
        VTD1_Survey_Answer__c answer = [SELECT Id FROM VTD1_Survey_Answer__c LIMIT 1];
        VTD1_Survey__c survey = [SELECT Id, VTD1_Protocol_ePRO__r.VTD1_Study__c FROM VTD1_Survey__c LIMIT 1];
        VTR2_SCRPatientEDiary.AnswerWrapper answ = new VTR2_SCRPatientEDiary.AnswerWrapper();
        answ.answer = 'Test';
        answ.answerId = answer.Id;
        VTR2_SCRPatientEDiary.AnswerWrapper answ2 = new VTR2_SCRPatientEDiary.AnswerWrapper();
        answ2.answer = 'Test2';

        VT_R2_PIMyPatientEDiaryItemController.ScoresWrapper wrapper = new VT_R2_PIMyPatientEDiaryItemController.ScoresWrapper(answer.Id, '2');
        VT_R2_PIMyPatientEDiaryItemController.ScoresWrapper wrapper2 = new VT_R2_PIMyPatientEDiaryItemController.ScoresWrapper(answer.Id, '2');

        List<Study_Team_Member__c> stms = [
                SELECT User__c, User__r.Profile.Name
                FROM Study_Team_Member__c
                WHERE Study__c = :survey.VTD1_Protocol_ePRO__r.VTD1_Study__c
                AND (User__r.Profile.Name = 'Site Coordinator'
                OR User__r.Profile.Name = 'Primary Investigator')
        ];

        User scrUser = new User();
        User piUser = new User();
        for (Study_Team_Member__c stm : stms) {
            if (stm.User__r.Profile.Name == 'Site Coordinator') {
                scrUser.Id = stm.User__c;
            } else if (stm.User__r.Profile.Name == 'Primary Investigator') {
                piUser.Id = stm.User__c;
            }
        }
        Test.startTest();
        System.runAs(scrUser) {
            VT_R2_PIMyPatientEDiaryItemController.updateSurveyStatus(survey.Id, JSON.serialize(new List<VT_R2_PIMyPatientEDiaryItemController.ScoresWrapper>{
                    wrapper, wrapper2
            }));
        }
        System.runAs(piUser) {
            VT_R2_PIMyPatientEDiaryItemController.updateSurveyStatus(survey.Id, JSON.serialize(new List<VT_R2_PIMyPatientEDiaryItemController.ScoresWrapper>{
                    wrapper, wrapper2
            }));
        }
        VT_R2_PIMyPatientEDiaryItemController.updateSurveyStatus(survey.Id, JSON.serialize(new List<VT_R2_PIMyPatientEDiaryItemController.ScoresWrapper>{
                wrapper, wrapper2
        }));
        Test.stopTest();
    }

    public static void testMissedReason() {
        Id standardSurveyRecordType = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('ePRO').getRecordTypeId();
        List<VTD1_Survey__c> surveys = [
                SELECT Id, VTR5_MissedReason__c, VTD1_Status__c, VTD1_Protocol_ePRO__r.VTD1_Study__c
                FROM VTD1_Survey__c
                WHERE RecordTypeId = :standardSurveyRecordType
                ORDER BY CreatedDate
                LIMIT 2
        ];
        for (VTD1_Survey__c survey : surveys) {
            survey.VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_EDIARY_MISSED;
        }
        update surveys;

        List<Study_Team_Member__c> stms = [
                SELECT User__c, User__r.Profile.Name
                FROM Study_Team_Member__c
                WHERE Study__c = :surveys[0].VTD1_Protocol_ePRO__r.VTD1_Study__c
                AND (User__r.Profile.Name = 'Site Coordinator'
                OR User__r.Profile.Name = 'Primary Investigator')
        ];

        User scrUser = new User();
        User piUser = new User();
        for (Study_Team_Member__c stm : stms) {
            if (stm.User__r.Profile.Name == 'Site Coordinator') {
                scrUser.Id = stm.User__c;
            } else if (stm.User__r.Profile.Name == 'Primary Investigator') {
                piUser.Id = stm.User__c;
            }
        }

        Test.startTest();
        System.runAs(scrUser) {
            VT_R2_PIMyPatientEDiaryItemController.updateMissedReason(surveys[0].Id, 'some Missed reason');
        }
        System.runAs(piUser) {
            VT_R2_PIMyPatientEDiaryItemController.updateMissedReason(surveys[1].Id, 'some Missed reason2');
        }
        Test.stopTest();

        List<VTD1_Survey__c> surveysWithMissedReason = [
                SELECT Id, VTR5_MissedReason__c, VTD1_Status__c
                FROM VTD1_Survey__c
                WHERE Id IN: surveys
                ORDER BY CreatedDate
        ];
        System.assertEquals('some Missed reason', surveysWithMissedReason[0].VTR5_MissedReason__c, 'Diary Missed reason was not updated for SCR');
        System.assertEquals('some Missed reason2', surveysWithMissedReason[1].VTR5_MissedReason__c, 'Diary Missed reason was not updated for PI');
    }

}