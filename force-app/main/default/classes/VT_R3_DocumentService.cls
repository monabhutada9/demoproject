global class VT_R3_DocumentService {

    webService static String reCalculateDocuments(String siteId) {
        List<Virtual_Site__c> siteList = [SELECT Id, VTD1_Study__c, VTD1_EDP_In_Progress__c FROM Virtual_Site__c WHERE Id = :siteId];
        if(siteList.isEmpty()) {
            return 'Success';
        }
        Virtual_Site__c currentSite = siteList[0];
        List<VTD1_Regulatory_Binder__c> binderList = [
                SELECT  Id,
                        VTD1_Care_Plan_Template__c,
                        VTD1_Site_RSU__c
                FROM    VTD1_Regulatory_Binder__c
                WHERE   Id NOT IN (
                        SELECT  VTD1_Regulatory_Binder__c
                        FROM    VTD1_Document__c
                        WHERE   VTD1_Site__c = :siteId
                ) AND VTD1_Care_Plan_Template__c =:currentSite.VTD1_Study__c
                  AND  VTD1_Level__c = 'Site'
        ];
        if(binderList.isEmpty()) {
            return Label.VT_R3_Document_Success_Message;
        }
        Map<Id, Integer> siteId_docCount = VT_D1_DocumentProcessHelper.createPlaceholders(siteList, binderList);
        if(siteId_docCount != null && siteId_docCount.containsKey(currentSite.Id) && siteId_docCount.get(currentSite.Id) > 0) {
            currentSite.VTD1_EDP_In_Progress__c += siteId_docCount.get(currentSite.Id);
            update currentSite;
        }
        return Label.VT_R3_Document_Success_Message;
    }
}