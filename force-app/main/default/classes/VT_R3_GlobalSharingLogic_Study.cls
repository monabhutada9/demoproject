/**
* @author: Carl Judge
* @date: 23-Sep-19
**/

public without sharing class VT_R3_GlobalSharingLogic_Study extends VT_R3_AbstractGlobalSharingLogic {
    public override Type getType() {
        return VT_R3_GlobalSharingLogic_Study.class;
    }

    public override VTR3_GlobalSharingConfig__mdt getConfig() {
        return getConfigForObjectType(HealthCloudGA__CarePlanTemplate__c.getSObjectType().getDescribe().getName());
    }

    protected override void addShareDetailsForRecords() {
        for (VTD1_Study_Sharing_Configuration__c shareConfig : [
            SELECT VTD1_Study__c, VTD1_Access_Level__c, VTD1_User__c
            FROM VTD1_Study_Sharing_Configuration__c
            WHERE VTD1_Study__c IN :recordIds
            AND VTD1_Access_Level__c != 'None'
            AND VTD1_User__r.IsActive = TRUE
        ]) {
            addShareDetail(shareConfig.VTD1_Study__c, shareConfig.VTD1_User__c, shareConfig.VTD1_Study__c, shareConfig.VTD1_Access_Level__c);
        }
    }

    protected override Iterable<Object> getUserBatchIterable() {
        return (Iterable<Object>) Database.getQueryLocator(getUserQuery());
    }

    protected override List<SObject> executeUserQuery() {
        String query = getUserQuery() + ' LIMIT ' + getAdjustedQueryLimit();
        return Database.query(query);
    }

    private String getUserQuery() {
        String query = 'SELECT VTD1_Study__c, VTD1_User__c' +
            ' FROM VTD1_Study_Sharing_Configuration__c' +
            ' WHERE VTD1_User__c IN :includedUserIds';
        if (scopedStudyIds != null) {
            query += ' AND VTD1_Study__c IN :scopedStudyIds';
        }
        System.debug(query);
        return query;
    }

    protected override String getRemoveQuery() {
        return 'SELECT Id' +
            ' FROM HealthCloudGA__CarePlanTemplate__Share' +
            ' WHERE ParentId IN :scopedStudyIds' +
            ' AND UserOrGroupId IN :includedUserIds';
    }

    protected override void createUserShareDetailsFromRecords(List<SObject> records) {
        for (SObject configObj : records) {
            VTD1_Study_Sharing_Configuration__c config = (VTD1_Study_Sharing_Configuration__c)configObj;
            addShareDetail(config.VTD1_Study__c, config.VTD1_User__c, config.VTD1_Study__c, 'Edit');
        }
    }
}