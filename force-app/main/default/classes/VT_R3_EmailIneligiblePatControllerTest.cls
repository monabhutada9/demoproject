/**
 * @author: Rishat Shamratov
 * @date: 31.10.2019
 * @description: Test class for VT_R3_EmailNewNotificationController
 */
@IsTest
public with sharing class VT_R3_EmailIneligiblePatControllerTest {
    public static void renderStoredEmailTemplateTest() {
        DomainObjects.Study_t domainStudy = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .setVTD1_Protocol_Nickname('Test Study')
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );

        DomainObjects.VTR2_Study_Geography_t domainStudyGeography = new DomainObjects.VTR2_Study_Geography_t()
                .addVTD2_Study(domainStudy);

        DomainObjects.VTR2_StudyPhoneNumber_t domainStudyPhoneNumber = new DomainObjects.VTR2_StudyPhoneNumber_t()
                .addVTR2_Study_Geography(domainStudyGeography);
        domainStudyPhoneNumber.persist();

        DomainObjects.User_t domainPatientUser = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);

        DomainObjects.Case_t domainCarePlan = new DomainObjects.Case_t()
                .addStudy(domainStudy)
                .addVTR2_StudyPhoneNumber(domainStudyPhoneNumber)
                .addVTD1_Patient_User(domainPatientUser)
                .setRecordTypeId(VT_R4_ConstantsHelper_RecordTypes.RECORD_TYPE_ID_CASE_CAREPLAN);
        domainCarePlan.persist();

        EmailTemplate emailTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'VTR3_Patient_is_ineligible_for_the_study'];

        Test.startTest();
        Messaging.renderStoredEmailTemplate(emailTemplate.Id, domainPatientUser.Id, domainCarePlan.Id);
        Test.stopTest();
    }
}