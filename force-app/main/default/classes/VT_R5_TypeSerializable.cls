/**
* @author: Carl Judge
* @date: 20-Sep-20
* @description: To support serialize/deserialize of items that need to have their type preserved.
 *  Use before/after methods to serialize/deserialize nested items which need type preserving
**/

public abstract class VT_R5_TypeSerializable {
    public abstract Type getType();

    public virtual void actionsBeforeSerialize(){}

    public virtual void actionsAfterDeserialize(){}

    public SerializedItem serializeWithType() {
        return new SerializedItem(this);
    }

    public class SerializedItem {
        public String serializedItem;
        public String typeString;

        public SerializedItem(VT_R5_TypeSerializable item) {
            item.actionsBeforeSerialize();
            serializedItem = JSON.serialize(item);
            typeString = item.getType().getName();
        }

        public VT_R5_TypeSerializable deserialize() {
            VT_R5_TypeSerializable item = (VT_R5_TypeSerializable) JSON.deserialize(serializedItem, Type.forName(typeString));
            item.actionsAfterDeserialize();
            return item;
        }
    }
}