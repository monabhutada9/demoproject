public class VT_D1_CaseVisitsListController {
	public class CaseVisit {
		VTD1_Actual_Visit__c visit;	
		String taskId;
	}

	@AuraEnabled
	public static String getVisits(string caseId) {
		try{
			List<VTD1_Actual_Visit__c> visits = [
					SELECT Id, Name, VTD1_Status__c, VTD1_Onboarding_Type__c, VTD1_Visit_Number__c, To_Be_Scheduled_Date__c
					FROM VTD1_Actual_Visit__c
					WHERE VTD1_Case__c = :caseId
					ORDER BY VTD1_Visit_Number__c
			];
			System.debug('TEST! ' + visits);

			List<String> visitIds = new List<String>();
			for(VTD1_Actual_Visit__c visit : visits){
				visitIds.add(visit.Id);
			}
			
			Map<String, String> visitIdToTaskIdMap = new Map<String, String>();	
			AggregateResult[] groupedResults = [SELECT MAX(Id) Id, WhatId FROM Task WHERE WhatId IN: visitIds GROUP BY WhatId];
			for(AggregateResult ar : groupedResults) {
				visitIdToTaskIdMap.put(String.valueOf(ar.get('WhatId')), String.valueOf(ar.get('Id')));	
			}
			System.debug(groupedResults);

			List<CaseVisit> caseVisits = new List<CaseVisit>();
			for(VTD1_Actual_Visit__c visit : visits){
				CaseVisit caseVisit = new CaseVisit();
				caseVisit.visit = visit; 
				caseVisit.taskId = visitIdToTaskIdMap.get(visit.Id);

				caseVisits.add(caseVisit); 	
			}
		
			return JSON.serialize(caseVisits);
        }catch (Exception e){
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }    
	}
}