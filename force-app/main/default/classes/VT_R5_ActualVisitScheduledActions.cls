/**
 * Created by Alexey Mezentsev on 9/28/2020.
 */

public without sharing class VT_R5_ActualVisitScheduledActions extends VT_R5_HerokuScheduler.AbstractScheduler implements VT_R5_HerokuScheduler.HerokuExecutable, Queueable, Database.AllowsCallouts {
    private List<VT_R5_HerokuScheduler.HerokuSchedulerPayload> payloads = new List<VT_R5_HerokuScheduler.HerokuSchedulerPayload>();
    private List<VT_D2_TNCatalogTasks.ParamsHolder> userTasks = new List<VT_D2_TNCatalogTasks.ParamsHolder>();
    private List<VT_D2_TNCatalogNotifications.ParamsHolder> userNotifications = new List<VT_D2_TNCatalogNotifications.ParamsHolder>();

    public VT_R5_ActualVisitScheduledActions() {
    }

    public VT_R5_ActualVisitScheduledActions(List<ScheduledActionData> actions) {

        Datetime now = System.now();
        for (ScheduledActionData actionData : actions) {
            payloads.add(new VT_R5_HerokuScheduler.HerokuSchedulerPayload(
                    actionData.scheduledTime <= now ? now.addMinutes(5) : actionData.scheduledTime,

                    'VT_R5_ActualVisitScheduledActions',
                    JSON.serialize(actionData))
            );
        }
    }

    public void execute(QueueableContext context) {
        this.schedule(payloads);
    }

    public void testExecute() {
        this.schedule(payloads);
    }

    public void herokuExecute(String payload) {
        List<ScheduledActionData> actionDataList = (List<ScheduledActionData>) JSON.deserialize(payload, List<ScheduledActionData>.class);
        List<VT_R2_RemindersSender.ParamsHolder> visitReminders = new List<VT_R2_RemindersSender.ParamsHolder>();

        Set<Id> actualVisitIdsForEmail = new Set<Id>();
        Set<Id> actualVisitIdsConsentReconsent = new Set<Id>();

        Set<Id> actualVisitIdsWithLabsPSCCompleted = new Set<Id>();
        for (ScheduledActionData actionData : actionDataList) {
            String actionName = actionData.actionName;
            Id visitId = actionData.visitId;
            if (actionName == 'TimeSlotIsChosen') {
                userTasks.add(new VT_D2_TNCatalogTasks.ParamsHolder(visitId, 'T640'));
            } else if (actionName.startsWith('SendReminder')) {
                if (actionName.contains('Before2Days')) {
                    visitReminders.add(new VT_R2_RemindersSender.ParamsHolder(visitId, 48));
                } else if (actionName.contains('Before2Hours')) {
                    visitReminders.add(new VT_R2_RemindersSender.ParamsHolder(visitId, 2));
                } else if (actionName.contains('Before1Hour')) {
                    visitReminders.add(new VT_R2_RemindersSender.ParamsHolder(visitId, 1));
                }
            } else if (actionName == 'SubTypeToBeScheduled') {
                userNotifications.add(new VT_D2_TNCatalogNotifications.ParamsHolder(visitId, 'N539'));
                userNotifications.add(new VT_D2_TNCatalogNotifications.ParamsHolder(visitId, 'N538'));
                userNotifications.add(new VT_D2_TNCatalogNotifications.ParamsHolder(visitId, 'N030'));
            } else if (actionName == 'LabsPSCCompleted') {
                actualVisitIdsWithLabsPSCCompleted.add(visitId);


            } else if (actionName == 'ConsentReconsentVisitScheduled') { //PB Name: Pop Up Notifications for scheduled visits 6 diamond
                actualVisitIdsConsentReconsent.add(visitId);
            } else if (actionName == 'ConsentScheduled') { // SH-14809 PB Name: Pop Up Notifications for scheduled visits 9 diamond
                userTasks.add(new VT_D2_TNCatalogTasks.ParamsHolder(visitId, '214'));
                userNotifications.add(new VT_D2_TNCatalogNotifications.ParamsHolder(visitId, 'N530'));
                actualVisitIdsForEmail.add(visitId);
            }


        }

        if (!actualVisitIdsWithLabsPSCCompleted.isEmpty()) {
            createTasksForPSCVisitConfirmation(actualVisitIdsWithLabsPSCCompleted);
        }
        if (!userTasks.isEmpty()) {
            VT_D2_TNCatalogTasks.generateTask(userTasks);
        }
        if (!userNotifications.isEmpty()) {
            VT_D2_TNCatalogNotifications.generateNotification(userNotifications);
        }
        if (!visitReminders.isEmpty()) {
            VT_R2_RemindersSender.sendReminders(visitReminders);
        }


        if (!actualVisitIdsForEmail.isEmpty()) {
            emailCountersignPacketForPI(actualVisitIdsForEmail);
        }
        if (!actualVisitIdsConsentReconsent.isEmpty()) {
            runConsentReconsentEvent(actualVisitIdsConsentReconsent);
        }
    }

    public void emailCountersignPacketForPI(Set<Id> actualVisitIdsForEmail) {
        List<VT_R3_EmailsSender.ParamsHolder> paramsHolders = new List<VT_R3_EmailsSender.ParamsHolder>();
        for (VTD1_Actual_Visit__c av : [SELECT VTD1_Case__r.VTD1_PI_user__c, VTD1_Case__c FROM VTD1_Actual_Visit__c
        WHERE Id IN : actualVisitIdsForEmail]) {
            VT_R3_EmailsSender.ParamsHolder emailParamsHolder = new VT_R3_EmailsSender.ParamsHolder();
            emailParamsHolder.recipientId = av.VTD1_Case__r.VTD1_PI_user__c;
            emailParamsHolder.templateDevName = 'VTD2_Countersign_Packet_for_PI';
            emailParamsHolder.relatedToId = av.VTD1_Case__c;
            paramsHolders.add(emailParamsHolder);
        }
        if (!Test.isRunningTest()) VT_R3_EmailsSender.sendEmails(paramsHolders);
    }

    public void runConsentReconsentEvent(Set<Id> actualVisitIdsConsentReconsent) {
        List<VT_R5_RunConsentVisit.ParamsVisit> consentVisitParams = new List<VT_R5_RunConsentVisit.ParamsVisit>();
        for (VTD1_Actual_Visit__c av : [SELECT Id, VTD1_Scheduled_Date_Time__c, Unscheduled_Visits__r.AccountId,
                                                VTD1_Case__r.AccountId, VTD1_Protocol_Visit__c
                                            FROM VTD1_Actual_Visit__c
                                            WHERE Id IN : actualVisitIdsConsentReconsent]) {
            VT_R5_RunConsentVisit.ParamsVisit paramsVisit = new VT_R5_RunConsentVisit.ParamsVisit();
            paramsVisit.scheduledDate = av.VTD1_Scheduled_Date_Time__c;
            paramsVisit.accountId =
                    av.VTD1_Protocol_Visit__c == null ? av.Unscheduled_Visits__r.AccountId
                            : av.VTD1_Case__r.AccountId;
            paramsVisit.visitId = av.Id;
            consentVisitParams.add(paramsVisit);
        }
        if (!consentVisitParams.isEmpty()) {
            VT_R5_RunConsentVisit.sendSignal(consentVisitParams);
        }
    }



    private void createTasksForPSCVisitConfirmation(Set<Id> actualVisitIdsWithLabsPSCCompleted) {
        Id satisfactionSurveyRecordTypeId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('SatisfactionSurvey').getRecordTypeId();
        List<VTD1_Actual_Visit__c> aVisits = [
                SELECT Id, VTD1_Case__r.VTR2_SiteCoordinator__c, Unscheduled_Visits__r.VTR2_SiteCoordinator__c, (
                        SELECT Id, VTD1_Status__c
                        FROM Surveys__r
                        WHERE RecordTypeId = :satisfactionSurveyRecordTypeId
                        LIMIT 1
                )
                FROM VTD1_Actual_Visit__c
                WHERE Id IN :actualVisitIdsWithLabsPSCCompleted
        ];
        for (VTD1_Actual_Visit__c aVisit : aVisits) {
            VTD1_Survey__c satisfactionSurvey = null;
            if (!aVisit.Surveys__r.isEmpty()) {
                satisfactionSurvey = aVisit.Surveys__r[0];
            }
            if (satisfactionSurvey == null
                    || satisfactionSurvey.VTD1_Status__c == 'eDiary Missed'
                    || satisfactionSurvey.VTD1_Status__c == 'Not Started'
                    || satisfactionSurvey.VTD1_Status__c == 'In Progress') {
                if (aVisit.VTD1_Case__c != null) {
                    userTasks.add(new VT_D2_TNCatalogTasks.ParamsHolder(aVisit.Id, '231'));
                    if (aVisit.VTD1_Case__r.VTR2_SiteCoordinator__c != null) {
                        userNotifications.add(new VT_D2_TNCatalogNotifications.ParamsHolder(aVisit.Id, 'N045'));
                    }
                } else if (aVisit.Unscheduled_Visits__c != null) {
                    userTasks.add(new VT_D2_TNCatalogTasks.ParamsHolder(aVisit.Id, '232'));
                    if (aVisit.Unscheduled_Visits__r.VTR2_SiteCoordinator__c != null) {
                        userNotifications.add(new VT_D2_TNCatalogNotifications.ParamsHolder(aVisit.Id, 'N048'));
                    }
                }
            }
        }
    }

    public class ScheduledActionData {
        public Datetime scheduledTime;
        public Id visitId;
        public String actionName;

        public ScheduledActionData(Datetime scheduledTime, Id visitId, String actionName) {
            this.scheduledTime = scheduledTime;
            this.visitId = visitId;
            this.actionName = actionName;
        }
    }
}