/**
* @author: Carl Judge
* @date: 27-Sep-19
* @description: Sharing for ActionItem. Logic taken from VT_D1_ActionItemSharingHandler
**/
public without sharing class VT_R3_GlobalSharingLogic_ActionItem extends VT_R3_AbstractGlobalSharingLogic {
    public static final Set<String> PROFILES_TO_SHARE_WITH = new Set<String> {
        VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.PL_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.VTSL_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.PMA_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.MRR_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.CM_PROFILE_NAME
    };

    public static final Set<String> STUDY_STM_PROFILES = new Set<String> {
        VT_R4_ConstantsHelper_ProfilesSTM.PL_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.VTSL_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.PMA_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.MRR_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.CM_PROFILE_NAME
    };

    public static final Set<String> SITE_STM_PROFILES = new Set<String> {
        VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME
    };

    private Map<Id, Set<Id>> siteToStmUserIds = new Map<Id, Set<Id>>();
    private Set<Id> siteFromStmIds = new Set<Id>();

    public override Type getType() {
        return VT_R3_GlobalSharingLogic_ActionItem.class;
    }

    public override VTR3_GlobalSharingConfig__mdt getConfig() {
        return getConfigForObjectType(VTD1_Action_Item__c.getSObjectType().getDescribe().getName());
    }

    protected override void addShareDetailsForRecords() {
        //generate new sharing:
        List<VTD1_Action_Item__c> aiList = [
            SELECT Id, VTD1_Study__c,
                VTD1_SiteId__c,
                OwnerId,
                VTD1_Study__r.VTD1_Remote_CRA__c,
                VTD1_Study__r.VTD1_Project_Lead__c,
                VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c,
                VTD1_Study__r.VTD1_PMA__c,
                VTD1_Study__r.VTD1_Monitoring_Report_Reviewer__c
            FROM VTD1_Action_Item__c
            WHERE Id IN: recordIds
        ];
        //prepare members maps:
        List<Id> virtualSitesIds  = new List<Id>();
        List<Id> studyIds = new List<Id>();
        for(VTD1_Action_Item__c ai : aiList) {
            if(ai.VTD1_SiteId__c != null) virtualSitesIds.add(ai.VTD1_SiteId__c);
            if(ai.VTD1_Study__c != null) studyIds.add(ai.VTD1_Study__c);
        }
        //site members:
        List<Study_Team_Member__c> members = [
            SELECT Id,VTD1_VirtualSite__c, User__c
            FROM Study_Team_Member__c
            WHERE VTD1_VirtualSite__c IN: virtualSitesIds
            AND RecordType.DeveloperName IN ('PI', 'PG')
        ];
        Map<Id, List<Id>> siteMembersMap = new Map<Id, List<Id>>();
        for(Study_Team_Member__c member : members){
            List<Id> vSiteMembers = siteMembersMap.get(member.VTD1_VirtualSite__c);
            if(vSiteMembers == null) vSiteMembers = new List<Id>();
            vSiteMembers.add(member.User__c);
            siteMembersMap.put(member.VTD1_VirtualSite__c, vSiteMembers);
        }
        //study members:
        members = [
            SELECT Id, Study__c, User__c
            FROM Study_Team_Member__c
            WHERE Study__c IN: studyIds
            AND RecordType.DeveloperName IN ('SC', 'CM', 'PL', 'VTSL','MRR','PMA')
        ];
        Map<Id, List<Id>> studyMembersMap = new Map<Id, List<Id>>();
        for(Study_Team_Member__c member : members){
            List<Id> vSiteMembers = studyMembersMap.get(member.Study__c);
            if(vSiteMembers == null) vSiteMembers = new List<Id>();
            vSiteMembers.add(member.User__c);
            studyMembersMap.put(member.Study__c, vSiteMembers);
        }
        //create sharing:
        for(VTD1_Action_Item__c ai : aiList){
            Set<Id> usersIds = new Set<Id>();
            //add vSite users:
            List<Id> vSiteMembers = siteMembersMap.get(ai.VTD1_SiteId__c);
            if(vSiteMembers != null) usersIds.addAll(vSiteMembers);
            //add study users:
            List<Id> studyMembers = studyMembersMap.get(ai.VTD1_Study__c);
            if (studyMembers != null) usersIds.addAll(studyMembers);
            //add users from study fields:
            if(ai.VTD1_Study__r.VTD1_Remote_CRA__c != null) usersIds.add(ai.VTD1_Study__r.VTD1_Remote_CRA__c);
            if(ai.VTD1_Study__r.VTD1_Project_Lead__c != null) usersIds.add(ai.VTD1_Study__r.VTD1_Project_Lead__c);
            if(ai.VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c != null) usersIds.add(ai.VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c);
            if(ai.VTD1_Study__r.VTD1_PMA__c !=null) usersIds.add(ai.VTD1_Study__r.VTD1_PMA__c);
            if(ai.VTD1_Study__r.VTD1_Monitoring_Report_Reviewer__c !=null) usersIds.add(ai.VTD1_Study__r.VTD1_Monitoring_Report_Reviewer__c);
            System.debug('thi$$$$ ');
            System.debug('ai.VTD1_Study__r.VTD1_Monitoring_Report_Reviewer__c!*!*!**! ' + ai.VTD1_Study__r.VTD1_Monitoring_Report_Reviewer__c);
            //add sharing records for current ai:
            for(Id userId : usersIds){
                addShareDetail(ai.Id, userId, ai.VTD1_Study__c, 'Edit');
            }
        }
    }

    protected override Iterable<Object> getUserBatchIterable() {
        return (Iterable<Object>) Database.getQueryLocator(getUserQuery());
    }

    protected override void doUserQueryPrep() {
        groupUserIdsByProfile();
        if (!profileToUserIds.isEmpty()) {
            queryStms(includedUserIds);
        }
    }

    protected override List<SObject> executeUserQuery() {
        if (!profileToUserIds.isEmpty()) {
            String query = getUserQuery() + ' LIMIT ' + getAdjustedQueryLimit();
            return Database.query(query);
        } else {
            return null;
        }
    }

    private void addNewShareDetail(VTD1_Action_Item__c item, Id userId) {
        addShareDetail(item.Id, userId, item.VTD1_Study__c, 'Edit');
    }

    protected override void createUserShareDetailsFromRecords(List<SObject> records) {
        List<VTD1_Action_Item__c> items = (List<VTD1_Action_Item__c>)records;

        for (VTD1_Action_Item__c item : items) {
            if (item.VTD1_Study__r.VTD1_Remote_CRA__c != null && craIds != null && craIds.contains(item.VTD1_Study__r.VTD1_Remote_CRA__c)) {
                addNewShareDetail(item, item.VTD1_Study__r.VTD1_Remote_CRA__c);
            }
            if (item.VTD1_Study__r.VTD1_Project_Lead__c != null && projLeadIds != null && projLeadIds.contains(item.VTD1_Study__r.VTD1_Project_Lead__c)) {
                addNewShareDetail(item, item.VTD1_Study__r.VTD1_Project_Lead__c);
            }
            if (item.VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c != null && vtslIds != null && vtslIds.contains(item.VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c)) {
                addNewShareDetail(item, item.VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c);
            }
            if (item.VTD1_Study__r.VTD1_PMA__c != null && pmaIds != null && pmaIds.contains(item.VTD1_Study__r.VTD1_PMA__c)) {
                addNewShareDetail(item, item.VTD1_Study__r.VTD1_PMA__c);
            }
            if (item.VTD1_Study__r.VTD1_Monitoring_Report_Reviewer__c != null && mrrIds != null && mrrIds.contains(item.VTD1_Study__r.VTD1_Monitoring_Report_Reviewer__c)) {
                addNewShareDetail(item, item.VTD1_Study__r.VTD1_Monitoring_Report_Reviewer__c);
            }
            if (studyToStmUserIds.containsKey(item.VTD1_Study__c)) {
                for (Id userId : studyToStmUserIds.get(item.VTD1_Study__c)) {
                    addNewShareDetail(item, userId);
                }
            }
            if (siteToStmUserIds.containsKey(item.VTD1_SiteId__c)) {
                for (Id userId : siteToStmUserIds.get(item.VTD1_SiteId__c)) {
                    addNewShareDetail(item, userId);
                }
            }
        }
    }

    private String getUserQuery() {
        String query =
            'SELECT Id,' +
                'VTD1_Study__c,' +
                'VTD1_SiteId__c,' +
                'VTD1_Study__r.VTD1_Remote_CRA__c,' +
                'VTD1_Study__r.VTD1_Project_Lead__c,' +
                'VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c,' +
                'VTD1_Study__r.VTD1_PMA__c,' +
                'VTD1_Study__r.VTD1_Monitoring_Report_Reviewer__c' +
            ' FROM VTD1_Action_Item__c' +
            ' WHERE VTD1_Study__r.VTD1_Remote_CRA__c IN :craIds' +
                ' OR VTD1_Study__r.VTD1_Project_Lead__c IN :projLeadIds' +
                ' OR VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c IN :vtslIds' +
                ' OR VTD1_Study__r.VTD1_PMA__c IN :pmaIds' +
                ' OR VTD1_Study__r.VTD1_Monitoring_Report_Reviewer__c IN :mrrIds' +
                ' OR VTD1_Study__c IN :studyFromStmIds' +
                ' OR VTD1_SiteId__c IN :siteFromStmIds';
        System.debug(query);
        return query;
    }

    protected override String getRemoveQuery() {
        String removeQuery = 'SELECT Id FROM VTD1_Action_Item__Share WHERE ';
        removeQuery += scopedSiteIds != null
            ? 'Parent.VTD1_SiteId__c IN :scopedSiteIds'
            : 'Parent.VTD1_Study__c IN :scopedStudyIds';
        removeQuery += ' AND UserOrGroupId IN :includedUserIds';
        return removeQuery;
    }

    private void queryStms(Set<Id> userIds) {
        String query = 'SELECT Study__c, VTD1_VirtualSite__c, User__c, User__r.Profile.Name FROM Study_Team_Member__c WHERE User__c IN :userIds';
        if (scopedSiteIds != null) {
            query += ' AND VTD1_VirtualSite__c IN :scopedSiteIds';
        } else if (scopedStudyIds != null) {
            query += ' AND Study__c IN :scopedStudyIds';
        }

        for (Study_Team_Member__c stm : Database.query(query)) {
            if (STUDY_STM_PROFILES.contains(stm.User__r.Profile.Name)) {
                if (!studyToStmUserIds.containsKey(stm.Study__c)) {
                    studyToStmUserIds.put(stm.Study__c, new Set<Id>());
                }
                studyToStmUserIds.get(stm.Study__c).add(stm.User__c);
            }

            if (SITE_STM_PROFILES.contains(stm.User__r.Profile.Name)) {
                if (!siteToStmUserIds.containsKey(stm.VTD1_VirtualSite__c)) {
                    siteToStmUserIds.put(stm.VTD1_VirtualSite__c, new Set<Id>());
                }
                siteToStmUserIds.get(stm.VTD1_VirtualSite__c).add(stm.User__c);
            }
        }

        studyFromStmIds = studyToStmUserIds.keySet();
        siteFromStmIds = siteToStmUserIds.keySet();
    }

    protected override void reset() {
        super.reset();
        siteToStmUserIds.clear();
        siteFromStmIds.clear();
    }
}