/**
 * @author: IQVIA Strikers
 * @date: 01.12.2020
 * @description: Test class for VT_R3_EmailsSender. Class is set to SeeAllData = true in order to query UserShare records.
 */

@isTest(SeeAllData = false)
public class VT_R5_EmailsSenderUserShareTest {

    public static User vtslUser;
    public static User patientUser;
    public static Case patientCase; 
    public static User adminUser; 
    

      /*
        **@author:      IQVIA Strikers
        **@date:        12/01/2020
        **@description: This method is to create study, patient user, case, VTSL user, STM.
        */
    @testSetUp
     private static void setupMethod(){
        Id vtslProfileId =[SELECT Id FROM Profile WHERE Name =:VT_R4_ConstantsHelper_ProfilesSTM.VTSL_PROFILE_NAME].id; 
         DomainObjects.Study_t study_t = new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'))
                .setOriginalName('VT_R5_EmailsSenderUserShareTest');
        DomainObjects.Contact_t con_t = new DomainObjects.Contact_t()
                .setLastName('VT_R3_EmailsSenderTest');
        DomainObjects.User_t user_t = new DomainObjects.User_t()
                .addContact(con_t)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        Case c = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName(VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN)
                .addStudy(study_t)
                .addVTD1_Patient_User(user_t)
                .addContact(con_t)
                .persist();
        
       DomainObjects.User_t userVTSL = new DomainObjects.User_t()
            .setProfile(vtslProfileId);
            userVTSL.persist();
        DomainObjects.StudyTeamMember_t stm1 = new DomainObjects.StudyTeamMember_t().setRecordTypeByName('VTSL').addUser(userVTSL).addStudy(Study_t);
        stm1.persist();

        vtslUser = new User(id = userVTSL.record.Id);
        patientUser = new User(id = user_t.record.Id);
    }
    
    public static void populateUsersVariables()
    {        
        adminUser =  [Select id from User where user.profile.name= 'System Administrator'  and isActive = true 
                            and id != :userInfo.getUserId() limit 1];
        patientCase=[select id,VTD1_Patient_User__c from case limit 1];
        patientUser = new User(id = patientCase.VTD1_Patient_User__c);
        patientUser = new User(id = patientCase.VTD1_Patient_User__c);
       	Study_Team_Member__c stm = [select id,User__c from Study_Team_Member__c where recordtype.name='VTSL'];
        vtslUser = new User(id = stm.User__c);
    }

    private static List<User> setupMethodForGroupSharing(){
        Id vtslProfileId =[SELECT Id FROM Profile WHERE Name =:VT_R4_ConstantsHelper_ProfilesSTM.VTSL_PROFILE_NAME].id;
        List<User> vtslUserList = new List<User>();
        for(Integer i = 0; i < 5 ; ++i){
            vtslUserList.add(new User(
            FirstName = 'vtslUserFirstName'+i,
            LastName = 'vtslUserLastName'+i,
            Alias = 'ali'+i,
            email = i+'vtslUserFirstName@scott.com',
            Username = i+'vtslUserFirstName@scott.com',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey ='en_US',
            TimeZoneSidKey = 'America/Los_Angeles',
            IsActive = true,
            UserPermissionsSFContentUser = false,
            profileId = vtslProfileId,
            VTD2_UserTimezone__c = 'America/Los_Angeles'
            ));
        }
        insert vtslUserList;
        return vtslUserList;
    }
    
     /*
        **@author:      IQVIA Strikers
        **@date:        12/01/2020
        **@description: This method is to test sharing for where recepient is patient and sender is VTSL.
        Here pateint user is not shared with VTSL user
        */

    @isTest
    private static void testForVTSLWithoutUserShare(){
        List<UserShare> userShareList = new List<UserShare>();
        //setupMethod();
        populateUsersVariables();
        Test.startTest();
        System.runAs(vtslUser){
            List<VT_R3_EmailsSender.ParamsHolder> paramList= new List<VT_R3_EmailsSender.ParamsHolder>{
                new VT_R3_EmailsSender.ParamsHolder(patientUser.id,patientCase.id,'VT_D2_Registration_Welcome_Template')
            };
            VT_R3_EmailsSender.VT_R5_calculateSharingOfRecipientsWrp emailSender = new 
                VT_R3_EmailsSender.VT_R5_calculateSharingOfRecipientsWrp();
                userShareList = emailSender.createUserSharelist(paramList);
        }
        Test.stopTest();
        System.assertEquals(userShareList.size(), 1);
        System.assertEquals(userShareList[0].UserOrGroupId, vtslUser.id);
        System.assertEquals(userShareList[0].UserOrGroupId, vtslUser.id);
        System.assertEquals(userShareList[0].UserId, patientUser.id);
        System.assertEquals(userShareList[0].UserAccessLevel, 'Read');

    }

     /*
        **@author:      IQVIA Strikers
        **@date:        12/01/2020
        **@description: This method is to test sharing for where recepient is patient and sender is VTSL.
        Here pateint user is directly shared with VTSL user
        */
    
    @isTest
    private static void testForVTSLwithDirectShare(){
        //setupMethod();
        populateUsersVariables();
        List<UserShare> userShareList = new List<UserShare>();
        System.runAs(adminUser){
            UserShare userShareRecord = new UserShare();
            userShareRecord.UserId = patientUser.id;
            userShareRecord.UserOrGroupId = vtslUser.id;
            userShareRecord.UserAccessLevel = 'Read';
            userShareRecord.RowCause = 'Manual';
            insert userShareRecord;
        }
        Test.startTest();
        System.runAs(vtslUser){
            List<VT_R3_EmailsSender.ParamsHolder> paramList= new List<VT_R3_EmailsSender.ParamsHolder>{
                new VT_R3_EmailsSender.ParamsHolder(patientUser.id,patientCase.id,'VT_D2_Registration_Welcome_Template')
            };
            VT_R3_EmailsSender.VT_R5_calculateSharingOfRecipientsWrp emailSender = new 
                VT_R3_EmailsSender.VT_R5_calculateSharingOfRecipientsWrp();
                userShareList = emailSender.createUserSharelist(paramList);
        }
        Test.stopTest();
        System.assertEquals(userShareList.size(), 0);

    }

    /*
        **@author:      IQVIA Strikers
        **@date:        12/01/2020
        **@description: This method is to test sharing for where recepient is patient and sender is VTSL.
        Here pateint user is directly shared with a group that has VTSL user as a member
        */

    @isTest
    private static void testForVTSLWithGroupShare(){ 
        //setupMethod();
        populateUsersVariables();
        List<UserShare> userShareList = new List<UserShare>();
        System.runAs(adminUser){
            Group grp = new Group();
            grp.name = 'Test Group1';
            grp.Type = 'Regular'; 
            Insert grp;     
            GroupMember grpMem2 = new GroupMember();
            grpMem2.UserOrGroupId = vtslUser.Id;
            grpMem2.GroupId = grp.Id;
            Insert grpMem2; 
            UserShare userShareRecord = new UserShare();
            userShareRecord.UserId = patientUser.id;
            userShareRecord.UserOrGroupId = grp.id;
            userShareRecord.UserAccessLevel = 'Read';
            userShareRecord.RowCause = 'Manual';
            insert userShareRecord;
            
        }
        
        Test.startTest();
        System.runAs(vtslUser){
            List<VT_R3_EmailsSender.ParamsHolder> paramList= new List<VT_R3_EmailsSender.ParamsHolder>{
                new VT_R3_EmailsSender.ParamsHolder(patientUser.id,patientCase.id,'VT_D2_Registration_Welcome_Template')
            };
            VT_R3_EmailsSender.VT_R5_calculateSharingOfRecipientsWrp emailSender = new 
                VT_R3_EmailsSender.VT_R5_calculateSharingOfRecipientsWrp();
                userShareList = emailSender.createUserSharelist(paramList);
        }
        Test.stopTest();
        System.assert(userShareList.size()==0);
    }

     /*
        **@author:      IQVIA Strikers
        **@date:        12/01/2020
        **@description: This method is to test sharing for where recepient is patient and sender is VTSL.
        Here pateint user is directly shared with a group that is further shared with subgroup that has VTSL user as a member
        */

    @isTest
    private static void testForVTSLWithSubGroupShare(){ 
        //setupMethod();
        populateUsersVariables();
        List<UserShare> userShareList = new List<UserShare>();
        List<Group> groupList = new List<Group>();
        List<GroupMember> grouListpMember = new List<GroupMember>();
        System.runAs(adminUser){

            for(Integer i = 0; i<2 ; ++i){
                groupList.add(new Group(
                    name = 'Test Group1'+i,
                    Type = 'Regular'
                ));
            }

            insert groupList;
            
            GroupMember grpMem1 = new GroupMember();
            grpMem1.UserOrGroupId = groupList[1].Id;
            grpMem1.GroupId = groupList[0].Id;
            grouListpMember.add(grpMem1);

            GroupMember grpMem2 = new GroupMember();
            grpMem2.UserOrGroupId = vtslUser.Id;
            grpMem2.GroupId = groupList[1].Id;
            grouListpMember.add(grpMem2);

            insert grouListpMember;

            UserShare userShareRecord = new UserShare();
            userShareRecord.UserId = patientUser.id;
            userShareRecord.UserOrGroupId = groupList[0].Id;
            userShareRecord.UserAccessLevel = 'Read';
            userShareRecord.RowCause = 'Manual';
            insert userShareRecord;
            
            
        }
        
        Test.startTest();
        System.runAs(vtslUser){
            List<VT_R3_EmailsSender.ParamsHolder> paramList= new List<VT_R3_EmailsSender.ParamsHolder>{
                new VT_R3_EmailsSender.ParamsHolder(patientUser.id,patientCase.id,'VT_D2_Registration_Welcome_Template')
            };
            VT_R3_EmailsSender.VT_R5_calculateSharingOfRecipientsWrp emailSender = new 
                VT_R3_EmailsSender.VT_R5_calculateSharingOfRecipientsWrp();
            userShareList = emailSender.createUserSharelist(paramList);
        }
        Test.stopTest();
        System.assert(userShareList.size()==0);
    }
    

     /*
        **@author:      IQVIA Strikers
        **@date:        12/02/2020
        **@description: This method is to test sharing for where recepient is VTSLs and sender is a VTSL.
        Here recipient VTSLs user are not shared sender VTSL
        */

    
     @IsTest
    private static void testForGroupSharingWithoutGroupAccess(){
        //setupMethod();
        populateUsersVariables();
        Set<Id> vtslUsersId = new Set<Id>();
        System.runAs(adminUser){
            for(User  u : setupMethodForGroupSharing()){
                vtslUsersId.add(u.id);
            }
            
        }
        Set<Id> recipeintWithoutAccess = new Set<Id>();
        Test.startTest();
        System.runAs(vtslUser){
            VT_R3_EmailsSender.VT_R5_calculateSharingOfRecipientsWrp emailSender = new 
                VT_R3_EmailsSender.VT_R5_calculateSharingOfRecipientsWrp();
            recipeintWithoutAccess = emailSender.checkWithGroupSharing(vtslUsersId);
        }
        Test.stopTest();
        System.assert(recipeintWithoutAccess.size()==5);
        
    }

     /*
        **@author:      IQVIA Strikers
        **@date:        12/02/2020
        **@description: This method is to test sharing for where recepient is VTSLs and sender is a VTSL.
        Here recipient VTSLs user are directly, via group and via subgroup are shared sender VTSL
        */
    

    @IsTest
    private static void testForGroupSharingWithAccess_1(){
        //setupMethod();
        populateUsersVariables();
        Set<Id> vtslUsersId = new Set<Id>();
        List<Group> groupList = new List<Group>();
        List<GroupMember> groupMemberList = new List<GroupMember>();
        List<UserShare> userShareRecords = new List<UserShare>();
        System.runAs(adminUser){
            for(User  u : setupMethodForGroupSharing()){
                vtslUsersId.add(u.id);
            }

            for(Integer i = 0; i<2 ; ++i){
                groupList.add(new Group(
                    name = 'Test Group1'+i,
                    Type = 'Regular'
                ));
            }

            insert groupList;

            
            GroupMember grpMem1 = new GroupMember();
            grpMem1.UserOrGroupId = vtslUser.id;
            grpMem1.GroupId = groupList[0].Id;
            groupMemberList.add(grpMem1);
            
            GroupMember grpMem2 = new GroupMember();
            grpMem2.UserOrGroupId = groupList[1].Id;
            grpMem2.GroupId = groupList[0].Id;
            groupMemberList.add(grpMem2);
            
            GroupMember grpMem3 = new GroupMember();
            grpMem3.UserOrGroupId = vtslUser.id;
            grpMem3.GroupId = groupList[1].Id;
            groupMemberList.add(grpMem3); 

            insert groupMemberList;


            List<Id>vtslUsersList = new List<Id>(vtslUsersId);

            UserShare userShareRecord1 = new UserShare();
            userShareRecord1.UserId = vtslUsersList[0];
            userShareRecord1.UserOrGroupId = vtslUser.id;
            userShareRecord1.UserAccessLevel = 'Read';
            userShareRecord1.RowCause = 'Manual';
            userShareRecords.add(userShareRecord1);
            
            
            
            UserShare userShareRecord2 = new UserShare();
            userShareRecord2.UserId = vtslUsersList[1];
            userShareRecord2.UserOrGroupId = groupList[0].Id;
            userShareRecord2.UserAccessLevel = 'Read';
            userShareRecord2.RowCause = 'Manual';
            userShareRecords.add(userShareRecord2);
            
            
            
            
            UserShare userShareRecord3 = new UserShare();
            userShareRecord3.UserId = vtslUsersList[2];
            userShareRecord3.UserOrGroupId =groupList[0].Id;
            userShareRecord3.UserAccessLevel = 'Read';
            userShareRecord3.RowCause = 'Manual';
            userShareRecords.add(userShareRecord3);

            insert userShareRecords;
            
        }
        Set<Id> recipeintWithoutAccess = new Set<Id>();
        Test.startTest();
        System.runAs(vtslUser){
            VT_R3_EmailsSender.VT_R5_calculateSharingOfRecipientsWrp emailSender = new 
                VT_R3_EmailsSender.VT_R5_calculateSharingOfRecipientsWrp();
            recipeintWithoutAccess = emailSender.checkWithGroupSharing(vtslUsersId);
        }
        Test.stopTest();
        System.assert(recipeintWithoutAccess.size()==2); 
        
    }

     /*
        **@author:      IQVIA Strikers
        **@date:        12/02/2020
        **@description: This method is to test sharing for where recepient is VTSLs and sender is a VTSL.
        Here recipient VTSLs user are directly, via group and via subgroup are shared sender VTSL with changed sequence of 
        userShare record's userId
        */
    
     @IsTest
    private static void testForGroupSharingWithAccess_2(){
        //setupMethod();
        populateUsersVariables();
        Set<Id> vtslUsersId = new Set<Id>();
        List<Group> groupList = new List<Group>();
        List<GroupMember> groupMemberList = new List<GroupMember>();
        List<UserShare> userShareRecords = new List<UserShare>();
        System.runAs(adminUser){
            for(User  u : setupMethodForGroupSharing()){
                vtslUsersId.add(u.id);
            }
            List<Id>vtslUsersList = new List<Id>(vtslUsersId);
            for(Integer i = 0; i<2 ; ++i){
                groupList.add(new Group(
                    name = 'Test Group1'+i,
                    Type = 'Regular'
                ));
            }

            insert groupList;

            
            GroupMember grpMem1 = new GroupMember();
            grpMem1.UserOrGroupId = vtslUser.id;
            grpMem1.GroupId = groupList[0].Id;
            groupMemberList.add(grpMem1);
            
            GroupMember grpMem2 = new GroupMember();
            grpMem2.UserOrGroupId = groupList[1].Id;
            grpMem2.GroupId = groupList[0].Id;
            groupMemberList.add(grpMem2);
            
            GroupMember grpMem3 = new GroupMember();
            grpMem3.UserOrGroupId = vtslUser.id;
            grpMem3.GroupId = groupList[1].Id;
            groupMemberList.add(grpMem3); 

            insert groupMemberList;

            UserShare userShareRecord1 = new UserShare();
            userShareRecord1.UserId = vtslUsersList[0];
            userShareRecord1.UserOrGroupId = vtslUser.id;
            userShareRecord1.UserAccessLevel = 'Read';
            userShareRecord1.RowCause = 'Manual';
            userShareRecords.add(userShareRecord1);
            
            
            
            UserShare userShareRecord2 = new UserShare();
            userShareRecord2.UserId = vtslUsersList[1];
            userShareRecord2.UserOrGroupId = groupList[0].Id;
            userShareRecord2.UserAccessLevel = 'Read';
            userShareRecord2.RowCause = 'Manual';
            userShareRecords.add(userShareRecord2);
            
            
            
            
            UserShare userShareRecord3 = new UserShare();
            userShareRecord3.UserId = vtslUsersList[2];
            userShareRecord3.UserOrGroupId =groupList[0].Id;
            userShareRecord3.UserAccessLevel = 'Read';
            userShareRecord3.RowCause = 'Manual';
            userShareRecords.add(userShareRecord3);

            insert userShareRecords;
        }
        Set<Id> recipeintWithoutAccess = new Set<Id>();
        Test.startTest();
        System.runAs(vtslUser){
            VT_R3_EmailsSender.VT_R5_calculateSharingOfRecipientsWrp emailSender = new 
                VT_R3_EmailsSender.VT_R5_calculateSharingOfRecipientsWrp();
            recipeintWithoutAccess = emailSender.checkWithGroupSharing(vtslUsersId);
        }
        Test.stopTest();
        System.assert(recipeintWithoutAccess.size()==2); 
        
    }

   

    
   
}