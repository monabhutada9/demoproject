public with sharing class VT_D1_CommunicationCommunityController {

    @AuraEnabled
    public static Video_Conference__c getRecord(String recId) {
        return [
            SELECT Id, Name, VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Patient__c,
                VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Patient__r.VTD1_Patient_Name__c
            FROM Video_Conference__c
            WHERE Id = :recId
        ];
    }
}