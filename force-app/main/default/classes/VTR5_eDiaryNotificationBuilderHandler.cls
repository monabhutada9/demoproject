/**
 * Created by Stnanislav Frolov on 11/18/2020.
 */

public with sharing class VTR5_eDiaryNotificationBuilderHandler {

    private final static Id DAILY_RT_ID = Schema.SObjectType.VTR5_eDiaryNotificationBuilder__c.getRecordTypeInfosByName().get('Daily Alert').getRecordTypeId();
    private final static String SITE_STAFF = 'Site Staff';
    private final static String PT_CG = 'Patient/Caregiver';
    private static Map<Id, String> BUILDER_TO_EDIARY_TOOL_MAP ;


    public static void insertDiaryListViews(Map<Id, VTR5_eDiaryNotificationBuilder__c> buildersMap) {
        BUILDER_TO_EDIARY_TOOL_MAP = getBuilderToEDiaryToolMap(buildersMap.keySet());
        Map<Id, VTR5_DiaryListView__c> viewsToInsert = buildDiaryListViews(buildersMap);
        if (!viewsToInsert.isEmpty()) {
            insert viewsToInsert.values();
            List<VTR5_eDiaryNotificationBuilder__c> buildersToUpdate = new List<VTR5_eDiaryNotificationBuilder__c>();
            for (Id builderId : viewsToInsert.keySet()) {
                VTR5_eDiaryNotificationBuilder__c builder = buildersMap.get(builderId).clone(true, false);
                builder.VTR5_DiaryListView__c = viewsToInsert.get(builderId).Id;
                buildersToUpdate.add(builder);
            }
            update buildersToUpdate;
        }
    }

    public static void updateBuildersListView(Map<Id, VTR5_eDiaryNotificationBuilder__c> oldBuilders, Map<Id, VTR5_eDiaryNotificationBuilder__c> newBuilders) {
        BUILDER_TO_EDIARY_TOOL_MAP = getBuilderToEDiaryToolMap(newBuilders.keySet());

        Set<Id> listViewsToDelete = new Set<Id>();
        List<VTR5_DiaryListView__c> listViewsToUpdate = new List<VTR5_DiaryListView__c>();
        Map<Id, VTR5_eDiaryNotificationBuilder__c> buildersListViewsToInsert = new Map<Id, VTR5_eDiaryNotificationBuilder__c>();

        for (VTR5_eDiaryNotificationBuilder__c newBuilder : newBuilders.values()) {
            VTR5_eDiaryNotificationBuilder__c oldBuilder = oldBuilders.get(newBuilder.Id);
            if(newBuilder.VTR5_RecipientType__c == PT_CG && oldBuilder.VTR5_RecipientType__c == PT_CG){
                continue;
            } else if (newBuilder.VTR5_RecipientType__c == PT_CG && oldBuilder.VTR5_RecipientType__c == SITE_STAFF) {
                listViewsToDelete.add(newBuilder.VTR5_DiaryListView__c);
                newBuilder.VTR5_DiaryListView__c = null;
            } else if (newBuilder.VTR5_RecipientType__c == SITE_STAFF && oldBuilder.VTR5_RecipientType__c == PT_CG) {
                buildersListViewsToInsert.put(newBuilder.Id, newBuilder);
            } else if (oldBuilder.VTR5_eDiaryName__c != newBuilder.VTR5_eDiaryName__c
                    || oldBuilder.VTR5_Study__c != newBuilder.VTR5_Study__c
                    || oldBuilder.VTR5_OperationType__c != newBuilder.VTR5_OperationType__c) {
                listViewsToUpdate.add(new VTR5_DiaryListView__c(Id = newBuilder.VTR5_DiaryListView__c, VTR5_Filter__c = createFilter(newBuilder)));
            }
        }
        if(!listViewsToUpdate.isEmpty()){
            update listViewsToUpdate;
        }
        if (!listViewsToDelete.isEmpty()) {
            delete [SELECT Id FROM VTR5_DiaryListView__c WHERE Id IN :listViewsToDelete];
        }
        if (!buildersListViewsToInsert.isEmpty()) {
            Map<Id, VTR5_DiaryListView__c> viewsToInsert = buildDiaryListViews(buildersListViewsToInsert);
            if (!viewsToInsert.isEmpty()) {
                insert viewsToInsert.values();
                for (VTR5_eDiaryNotificationBuilder__c builder : buildersListViewsToInsert.values()) {
                    newBuilders.get(builder.Id).VTR5_DiaryListView__c = viewsToInsert.get(builder.Id).Id;
                }
            }
        }
    }

    private static Map<Id, String> getBuilderToEDiaryToolMap(Set<Id> builderIds) {
        Map<Id, String> builderToEDiaryToolMap = new Map<Id, String>();
        for (VTR5_eDiaryNotificationBuilder__c builder : [SELECT VTR5_Study__r.VTR5_eDiaryTool__c FROM VTR5_eDiaryNotificationBuilder__c WHERE Id IN :builderIds AND VTR5_Study__c != NULL]) {
            builderToEDiaryToolMap.put(builder.Id, builder.VTR5_Study__r.VTR5_eDiaryTool__c);
        }
        return builderToEDiaryToolMap;
    }

    private static String getStatusesString(String operationType, String eDiaryTool) {
        String status;

        if (operationType == 'BEFORE the Expiration Date') {
            if (eDiaryTool == 'eCOA') {
                status = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_DUE_SOON;
            } else if (eDiaryTool == 'Study Hub eDiary') {
                status = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_READY_TO_START;
            }
        } else {
            if (eDiaryTool == 'eCOA') {
                status = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_MISSED;
            } else if (eDiaryTool == 'Study Hub eDiary') {
                status = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_EDIARY_MISSED;
            }
        }

        return status;
    }
    private static Map<Id, VTR5_DiaryListView__c> buildDiaryListViews(Map<Id, VTR5_eDiaryNotificationBuilder__c> buildersMap) {
        Map<Id, VTR5_DiaryListView__c> viewListByBuilderId = new Map<Id, VTR5_DiaryListView__c>();

        for (VTR5_eDiaryNotificationBuilder__c builder : buildersMap.values()) {
            if (builder.VTR5_RecipientType__c != SITE_STAFF) {
                continue;
            }
            viewListByBuilderId.put(builder.Id, new VTR5_DiaryListView__c(VTR5_Filter__c = createFilter(builder)));
        }
        return viewListByBuilderId;
    }
    private static String createFilter(VTR5_eDiaryNotificationBuilder__c builder) {
        Map<String, String> filter = new Map<String, String>();
        filter.put('name', builder.VTR5_eDiaryName__c);
        filter.put('studyId', builder.VTR5_Study__c);
        if (builder.RecordTypeId != DAILY_RT_ID) {
            filter.put('status', getStatusesString(builder.VTR5_OperationType__c, BUILDER_TO_EDIARY_TOOL_MAP.get(builder.Id)));
        }
        return JSON.serialize(filter);
    }
}