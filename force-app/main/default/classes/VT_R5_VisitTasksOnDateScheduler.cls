/**
 * Created by Alexey Mezentsev on 8/19/2020.
 * SH-14596 PB Name: "Schedule Visit Tasks on Date"
 * Test class: VT_R5_ActualVisitProcessesHandlerTest.scheduleBatchVisitTasksOnDate()
 */

global without sharing class VT_R5_VisitTasksOnDateScheduler implements Schedulable {
    // to start scheduler:
    // System.schedule('VisitTasksOnDateScheduler', '0 0 0 * * ?', new VT_R5_VisitTasksOnDateScheduler());
    global void execute(SchedulableContext context) {
        Database.executeBatch(new VT_R5_ScheduleVisitTasksOnDate(), 50);
    }
}