/**
 * Created by Руслан on 20.02.2020.
 */


public with sharing class VT_R4_GeneralContactFormsController {
    @AuraEnabled
    public static List<Case> getCases() {
        return [
                SELECT CaseNumber, Status, VTD1_Study__c, VTD1_Study__r.Name, CreatedDate, VTD1_PCF_Reason_for_Contact__c,Subject
                FROM Case
                WHERE RecordType.DeveloperName = 'VT_R2_General_Contact_Form'
                ORDER BY CreatedDate DESC
        ];
    }

    @AuraEnabled
    public static Id getGcfRecordTypeId() {
        return VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_GCF;
    }
}