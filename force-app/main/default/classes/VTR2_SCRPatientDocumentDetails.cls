public with sharing class VTR2_SCRPatientDocumentDetails {

    private static final Set<String> SUPPORTED_FORMATS = new Set<String>{'pdf', 'txt'};

    @AuraEnabled
    public static DocumentData getDocumentById(Id docId) {
        try {
            DocumentData documentData = new DocumentData();
            documentData.document = getDocument(docId);
            ContentDocumentLink cdl = getContentDocumentLink(docId);
            if (cdl != null) {
                documentData.contentDocumentId = cdl.ContentDocumentId;
                documentData.documentDownloadUrl = Url.getSalesforceBaseUrl().toExternalForm()
                        + VT_D1_HelperClass.getSandboxPrefix()
                        + '/sfc/servlet.shepherd/document/download/' + cdl.ContentDocumentId;
                String fileExtension = cdl.ContentDocument.LatestPublishedVersion.FileExtension;
                if (SUPPORTED_FORMATS.contains(fileExtension)) {
                    documentData.pdfData = getContentAsPDF(cdl);
                } else {
                    documentData.pdfData = getUnsupportedFormatWarningAsBase64Pdf();
                }
            }
            if (documentData.document != null) {
                documentData.documentStatusPicklistEntries = getPicklistEntries(
                        VTD1_Document__c.SObjectType,
                        documentData.document.RecordTypeId,
                        'VTD1_Status__c'
                );
                if (documentData.document.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT
                        && documentData.document.VTR3_CreatedProfile__c
                        && documentData.document.VTR3_Category_add__c == 'Uncategorized'){
                    documentData.documentCategoryPicklistEntries = getPicklistEntries(
                            VTD1_Document__c.SObjectType,
                            documentData.document.RecordTypeId,
                            'VTR3_Category_add__c'
                    );
                    documentData.documentVisitPicklistEntries = getVisitPicklistEntries(getVisits(documentData.document.VTD1_Clinical_Study_Membership__c));
                    documentData.uncategorized = true;
                }
                documentData.dateAdded = documentData.document.CreatedDate.format('dd-MMM-YYYY hh:mm a');
            }
            return documentData;
        } catch (Exception exc) {
            throw new AuraHandledException(exc.getMessage() + ' ' + exc.getStackTraceString());
        }
    }

    @AuraEnabled
    public static Boolean updateDocument(VTD1_Document__c document) {
        try {
            String profileName = getProfileNameOfCurrentUser();
            if (profileName == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME || profileName == VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME) {
                Database.SaveResult saveResult = Database.update(document);
                return saveResult.isSuccess();
            } else {
                throw new AuraHandledException(Label.VT_R2_SCR_PI_canModify);
            }
        } catch (Exception exc) {
            throw new AuraHandledException(exc.getMessage());
        }
    }

    @AuraEnabled
    public static String sendDocWithDocuSign(Id docId) {
        String error;
        try {
            if (getCDL(docId).isEmpty()) {
                error = Label.VT_R2_SendingNotAvailable;
            } else {
                new VT_D2_QAction_CertifyDocuments(docId).executeFuture();
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
        return error;
    }

    @AuraEnabled
    public static String getAndCreateDocuSignEnvelopeId(String documentId) {
        try{
        dsfs__DocuSign_Envelope__c docuSignEnvelope = new dsfs__DocuSign_Envelope__c();
        docuSignEnvelope.dsfs__DocuSign_Email_Subject__c = 'Documents for your DocuSign Signature';
        docuSignEnvelope.dsfs__Source_Object__c = documentId;
        docuSignEnvelope.dsfs__DocumentID__c = 'Select Document ID';
        docuSignEnvelope.dsfs__DocuSign_Email_Message__c = 'I am sending you this request for your electronic signature, please review and electronically sign by following the link below.';
        insert docuSignEnvelope;
        return docuSignEnvelope.Id;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static void closeApprovalProcess(VTD1_Document__c document, String status){
        Map<String,String> statusToActionMap = new Map<String, String>{
                'Approved' => 'Approve',
                'Rejected' => 'Reject'
        };

        List<ProcessInstanceWorkItem> appProcess = [SELECT Id, OriginalActorId FROM ProcessInstanceWorkItem WHERE ProcessInstance.TargetObjectId =:document.Id ];
        List<Approval.ProcessWorkitemRequest> reqList = new List<Approval.ProcessWorkitemRequest>();

        for(ProcessInstanceWorkitem pwi : appProcess) {
            if(pwi.OriginalActorId == UserInfo.getUserId() || Test.isRunningTest()) {
                Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                req.setComments('Edited by SCR');
                req.setAction(statusToActionMap.get(status));

                if (pwi == null) {
                    document.addError('Error Occured in Trigger');
                } else {
                    req.setWorkitemId(pwi.Id);
                    reqList.add(req);
                }
            }
        }
        List<Approval.ProcessResult> result = Approval.process(reqList);


    }

    private static String getSandboxPrefix(){
        String sandboxPrefix = Url.getCurrentRequestUrl().getPath().toLowerCase().split('/')[1];
        if (sandboxPrefix == 'pi' || sandboxPrefix == 'scr' || sandboxPrefix == 'patient'){
            sandboxPrefix = '/' + sandboxPrefix;
        }
        else{
            sandboxPrefix = '';
        }
        return sandboxPrefix;
    }

    //-------------------------- HELPERS ---------------------------//

    @TestVisible
    private static VTD1_Document__c getDocument(Id docId) {
        if (docId == null) return null;
        List<VTD1_Document__c> document = getDocs(docId);
        if (!document.isEmpty()) {
            return document[0];
        } else {
            return null;
        }
    }

    @TestVisible
    private static String getContentAsPDF(ContentDocumentLink cdl) {
        PageReference pageRef = new PageReference(getSandboxPrefix() + '/sfc/servlet.shepherd/document/download/' + cdl.ContentDocumentId); //0692F000000G41eQAC
        String data;
        if (!Test.isRunningTest()) {
            data = EncodingUtil.base64Encode(pageRef.getContentAsPDF());
        } else {
            data = 'TestData';
        }
        return data;
    }

    @TestVisible
    private static ContentDocumentLink getContentDocumentLink(Id docId) {
        if (docId == null) return null;
        List<ContentDocumentLink> cdl = getCDL(docId);
        if (!cdl.isEmpty()) {
            return cdl[0];
        } else {
            return null;
        }
    }

    @TestVisible
    private static String getUnsupportedFormatWarningAsBase64Pdf() {
        String notSupportedMsg = System.Label.VTR2_UnsupportedFormatWarning;
        String data = EncodingUtil.base64Encode(Blob.toPdf(notSupportedMsg));
        return data;
    }

    @TestVisible
    private static List<PicklistEntry> getPicklistEntries(SObjectType type, Id recordTypeId, String picklistName) {
        Map<String, String> picklist = VTR2_SObjectUtils.getPicklistEntryMapByRecordTypeId(type, recordTypeId, picklistName);
        return getConvertedPicklistEntries(picklist);
    }

    @TestVisible
    private static List<PicklistEntry> getVisitPicklistEntries(List<VTD1_Actual_Visit__c> avList) {
        List<PicklistEntry> picklistEntries = new List<PicklistEntry>();
        PicklistEntry emptyPicklistEntry = new PicklistEntry();
        emptyPicklistEntry.label = 'None';
        emptyPicklistEntry.value = null;
        picklistEntries.add(emptyPicklistEntry);
        for (VTD1_Actual_Visit__c av : avList) {
            PicklistEntry picklistEntry = new PicklistEntry();
            picklistEntry.label = av.Name;
            picklistEntry.value = av.Id;
            picklistEntries.add(picklistEntry);
        }
        return picklistEntries;
    }

    @TestVisible
    private static List<PicklistEntry> getConvertedPicklistEntries(Map<String, String> picklistEntriesMap) {
        List<PicklistEntry> picklistEntries = new List<PicklistEntry>();
        for (String picklistLabel : picklistEntriesMap.keySet()) {
            PicklistEntry picklistEntry = new PicklistEntry();
            picklistEntry.label = picklistLabel;
            picklistEntry.value = picklistEntriesMap.get(picklistLabel);
            picklistEntries.add(picklistEntry);
        }
        return picklistEntries;
    }

    @TestVisible
    private static String getProfileNameOfCurrentUser() {
        return getProfiles()[0].Name;
    }

    //---------------------------- SOQL ----------------------------//
    
    @TestVisible
    private static List<VTD1_Actual_Visit__c> getVisits(Id casId) {
        return [
                SELECT Id,
                        Name
                FROM VTD1_Actual_Visit__c
                WHERE VTD1_Case__c = :casId OR Unscheduled_Visits__c = :casId
        ];
    }

    private static List<Profile> getProfiles() {
        return [
                SELECT Name
                FROM Profile
                WHERE Id = : UserInfo.getProfileId()
        ];
    }
    private static List<VTD1_Document__c> getDocs(Id docId) {
        return [
                SELECT
                        Id,
                        VTD1_Does_file_needs_deletion__c,
                        VTD1_Deletion_Reason__c,
                        VTD1_Why_file_is_irrelevant__c,
                        VTD1_Eligibility_Assessment_Status__c,
                        VTD2_Previous_PI_Decision__c,
                        VTD2_Final_Eligibility_Decision__c,
                        VTD2_TMA_Eligibility_Decision__c,
                        VTD2_TMA_Comments__c,
                        VTD2_Final_Review_Comments__c,
                        VTD1_Clinical_Study_Membership__c,
                        VTD1_Clinical_Study_Membership__r.Contact.Name,
                        VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c,
                        VTD1_Study__r.Name,
                        VTD1_Study__r.VTD1_StudyStatus__c,
                        VTD1_Name__c,
                        VTD1_FileNames__c,
                        VTD1_Version__c,
                        VTD1_Nickname__c,
                        VTD1_Status__c,
                        VTD1_Comment__c,
                        VTD2_Patient__r.VTD1_Patient_Name__c,
                        VTD2_Patient__r.Name,
                        RecordTypeId,
                        RecordType.Name,
                        RecordType.DeveloperName,
                        VTD1_Regulatory_Document_Type__c,
                        VTD1_Document_Type__c,
                        VTR3_Category__c,
                        VTR3_Category_add__c,
                        CreatedDate,
                        VTR3_CreatedProfile__c,
                        VTR3_Associated_Visit__c,
                        VTR3_Associated_Visit__r.Name
                FROM VTD1_Document__c
                WHERE Id = :docId
        ];
    }
    private static List<ContentDocumentLink> getCDL(Id docId) {
        return [
                SELECT Id
                        , ContentDocumentId
                        , ContentDocument.LatestPublishedVersion.FileExtension
                        , LinkedEntityId
                FROM ContentDocumentLink
                WHERE LinkedEntityId = :docId
        ];
    }

    //------------------------ WRAPPERS ---------------------------//

    public class PicklistEntry {
        @AuraEnabled public String label;
        @AuraEnabled public String value;
    }
    public class DocumentData {
        @AuraEnabled public VTD1_Document__c document;
        @AuraEnabled public String pdfData;
        @AuraEnabled public String contentDocumentId;
        @AuraEnabled public String documentDownloadUrl;
        @AuraEnabled public String dateAdded;
        @AuraEnabled public Boolean uncategorized = false;
        @AuraEnabled public List<PicklistEntry> documentStatusPicklistEntries;
        @AuraEnabled public List<PicklistEntry> documentCategoryPicklistEntries;
        @AuraEnabled public List<PicklistEntry> documentVisitPicklistEntries;
    }
}