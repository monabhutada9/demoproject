/**
* @author: Tatiana Terekhova
* @date: 27-May-20
* @description: Used by VT_R5_QAction_ContentDocs to send documents to docusign
**/
public class VT_R5_CDocumentsCalloutHelper {
    public static VT_D1_HTTPConnectionHandler.Result sendCertifyDocument(Id mvrId,Id docId,Id dsUserId, String dsSubject,String dsMessage) {
        VT_D2_DocuSignAuthHelper.Result tokenResponse = VT_D2_DocuSignAuthHelper.getAuthToken();
        if (tokenResponse.value == null) {
            insert tokenResponse.calloutResult.log;
            return tokenResponse.calloutResult;
        }
        VT_D2_DocuSignAuthHelper.Result baseUriResponse = VT_D2_DocuSignAuthHelper.getBaseUri(tokenResponse.value);
        if (baseUriResponse.value == null) {
            insert baseUriResponse.calloutResult.log;
            return baseUriResponse.calloutResult;
        }
        String method = 'POST';
        String endpointURL = baseUriResponse.value + '/envelopes';
        VT_R5_RequestBuilder_CDocument_DocuSign  requestBuilder = new VT_R5_RequestBuilder_CDocument_DocuSign(mvrId,docId, dsUserId, dsSubject,dsMessage);
        String action = 'Send DocuSign Envelope';
        Map<String, String> headerMap = new Map<String, String>{
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' + tokenResponse.value
        };
        VT_D1_HTTPConnectionHandler.Result certifyResult = VT_D1_HTTPConnectionHandler.send(method, endpointURL, requestBuilder, action, headerMap);
        return certifyResult;
    }
}