@isTest
public with sharing class VT_R3_RestPatientPaymentsTest {

    //@TestSetup in VT_R5_AllTests7

    public static void getPaymentsTest() {
        VT_R3_RestHelper helper = new VT_R3_RestHelper();
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/Patient/Payments/';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = response;
        String responsString = VT_R3_RestPatientPayments.getPayments();
        System.assert(!String.isEmpty(responsString));

        Case carePlan = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
		Id recordTypeIdPayment = Schema.SObjectType.VTD1_Patient_Payment__c.getRecordTypeInfosByDeveloperName().get('VTD1_Compensation').getRecordTypeId();
		VTD1_Patient_Payment__c patientPayment = new VTD1_Patient_Payment__c(VTD1_Clinical_Study_Membership__c = carePlan.Id, RecordTypeId = recordTypeIdPayment);
        insert patientPayment;

        RestContext.request.requestURI = '/Patient/Payments/' + patientPayment.Id;
        responsString = VT_R3_RestPatientPayments.getPayments();
        System.assert(!String.isEmpty(responsString));

        RestContext.request.requestURI = '/Patient/Payments/0050v0000022C2QAAU'; //incorrect id test
        responsString = VT_R3_RestPatientPayments.getPayments();
        Map<String, Object> resp = (Map<String, Object>) JSON.deserializeUntyped(responsString);
        System.assertEquals(helper.forAnswerForIncorrectInput(), resp.get('serviceResponse'));
        System.assertEquals(400, response.statusCode);
    }

    public static void completePaymentTest() {
        VT_R3_RestHelper helper = new VT_R3_RestHelper();
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/Payments/';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = response;
        String responsString = VT_R3_RestPatientPayments.completePayment();
        Map<String, Object> resp = (Map<String, Object>) JSON.deserializeUntyped(responsString);
        System.assertEquals(helper.forAnswerForIncorrectInput(), resp.get('serviceResponse'));
        System.assertEquals(400, response.statusCode);


        Case carePlan = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        Id recordTypeIdPayment = Schema.SObjectType.VTD1_Patient_Payment__c.getRecordTypeInfosByDeveloperName().get('VTD1_Compensation').getRecordTypeId();
        VTD1_Patient_Payment__c patientPayment = new VTD1_Patient_Payment__c(
                VTD1_Clinical_Study_Membership__c = carePlan.Id,
				RecordTypeId = recordTypeIdPayment,
                VTD1_Activity_Complete__c = false,
                Name = 'TestPaymentt');
        insert patientPayment;
        RestContext.request.requestURI = '/Payments/' + patientPayment.Id;
        responsString = VT_R3_RestPatientPayments.completePayment();
        resp = (Map<String, Object>) JSON.deserializeUntyped(responsString);
        System.assertEquals(System.Label.VTR3_CompletePaymentActivity.replace('#1', patientPayment.Name), resp.get('serviceMessage'));

        patientPayment.VTD1_Activity_Complete__c = true;
        update patientPayment;
        responsString = VT_R3_RestPatientPayments.completePayment();
        resp = (Map<String, Object>) JSON.deserializeUntyped(responsString);
        System.assertEquals('FAILURE. Payment not in "' + System.Label.VTD1_NotStarted + '" status.', resp.get('serviceResponse'));
        System.assertEquals(417, response.statusCode);
    }

    public static void getPaymentsSumTest() {
        Case carePlan = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        VTD1_Protocol_Payment__c protocolPayment = new VTD1_Protocol_Payment__c();
        insert protocolPayment;
		Id recordTypeIdPayment = Schema.SObjectType.VTD1_Patient_Payment__c.getRecordTypeInfosByDeveloperName().get('VTD1_Compensation').getRecordTypeId();
		VTD1_Patient_Payment__c patientPayment = new VTD1_Patient_Payment__c(VTD1_Clinical_Study_Membership__c = carePlan.Id,
                VTD1_Activity_Complete__c = true,
                VTD1_Payment_Issued__c = true,
				RecordTypeId = recordTypeIdPayment,
                VTD1_Protocol_Payment__c = protocolPayment.Id);
        insert patientPayment;
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/Patient/Payments/Sum/';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = response;
        VT_R3_RestPatientPaymentsSum.getPayments();
    }
    //for copado deploy
    //for copado deploy
}