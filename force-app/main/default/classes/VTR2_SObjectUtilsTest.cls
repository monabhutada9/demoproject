@IsTest
private class VTR2_SObjectUtilsTest {

    public with sharing class CalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest param1) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"controllerValues":{"Medical Records flow":0},"defaultValue":{"attributes":null,"label":"Pending Certification","validFor":[0],"value":"Pending Certification"},"eTag":"13d3354a9bb61c834d711380fa5fbf85","url":"/services/data/v45.0/ui-api/object-info/VTD1_Document__c/picklist-values/0121N000000oCGqQAM/VTD1_Status__c","values":[{"attributes":null,"label":"Approved","validFor":[0],"value":"Approved"},{"attributes":null,"label":"Pending Approval","validFor":[0],"value":"Pending Approval"},{"attributes":null,"label":"Rejected","validFor":[0],"value":"Rejected"},{"attributes":null,"label":"Pending Certification","validFor":[0],"value":"Pending Certification"}]}');
            res.setStatusCode(200);
            return res;
        }
    }

    @TestSetup
    static void testSetup() {
        VTD1_Document__c document = new VTD1_Document__c (
                RecordTypeId = '0121N000000oCGqQAM',
                VTD1_Status__c = 'Approved'
        );
        insert document;

        ContentVersion contentVersion = new ContentVersion();
        contentVersion.PathOnClient = 'test.txt';
        contentVersion.Title = 'Test file';
        contentVersion.VersionData = Blob.valueOf('Test Data');
        insert contentVersion;

        Id contentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id].ContentDocumentId;

        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = document.Id;
        contentDocumentLink.ContentDocumentId = contentDocumentId;
        contentDocumentLink.ShareType = 'I';
        contentDocumentLink.Visibility = 'AllUsers';
        insert contentDocumentLink;
    }

    @IsTest
    static void getPicklistEntryMapByRecordTypeIdTest() {
        VTD1_Document__c document = [SELECT RecordTypeId, VTD1_Status__c FROM VTD1_Document__c LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new CalloutMock());
        Test.startTest();
        Map<String, String> picklist = VTR2_SObjectUtils.getPicklistEntryMapByRecordTypeId(
                VTD1_Document__c.SObjectType,
                document.RecordTypeId,
                'VTD1_Status__c'
        );
        Test.stopTest();
        System.assertEquals(4, picklist.size());
    }
}