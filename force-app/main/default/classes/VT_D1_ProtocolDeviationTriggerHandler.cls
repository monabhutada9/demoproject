/**
* @author: Carl Judge
* @date: 26-Dec-18
**/

public without sharing class VT_D1_ProtocolDeviationTriggerHandler {
    public void onBeforeDelete(List<VTD1_Protocol_Deviation__c> recs) {
        VT_D1_LegalHoldDeleteValidator.validateDeletion(recs);
    }

    public void onAfterInsert(List<VTD1_Protocol_Deviation__c> recs) {
        shareDeviations(recs);
    }

    // Conquerors
    // Jira Ref: SH-17437 Akanksha Singh
    // Description: Only Assigned CRA should be able to create Protocol Deviation record
    public void onBeforeInsert(List<VTD1_Protocol_Deviation__c> recs) {
        checkCRAPresenceOnSSTM(recs); 
    }
    // End of code: Conquerors

    public void onAfterUpdate(List<VTD1_Protocol_Deviation__c> recs, Map<Id, VTD1_Protocol_Deviation__c> oldMap) {
        List<VTD1_Protocol_Deviation__c> addedToStudy = new List<VTD1_Protocol_Deviation__c>();
        for (VTD1_Protocol_Deviation__c item : recs) {
            if (item.VTD1_StudyId__c != null && oldMap.get(item.Id).VTD1_StudyId__c == null) {
                addedToStudy.add(item);
            }
        }

        if (! addedToStudy.isEmpty()) { shareDeviations(addedToStudy); }
    }

    private void shareDeviations(List<VTD1_Protocol_Deviation__c> recs) {
        VT_R3_GlobalSharing.doSynchronous = true;
        VT_R3_GlobalSharing.doSharing(recs);
    }

    // Conquerors
    // Jira Ref: SH-17437 Akanksha Singh
    // Description: Only Assigned CRA should be able to create Protocol Deviation record
    private void checkCRAPresenceOnSSTM(List<VTD1_Protocol_Deviation__c> recs) {
        
        Map<Id, Study_Team_Member__c> mapSiteToSTM = new Map<Id, Study_Team_Member__c>();
        
        //Fetch CRA profile information 
        Profile profileCRA = [SELECT Id,
                                     Name 
                              FROM Profile 
                              WHERE Name =: VT_R4_ConstantsHelper_Profiles.CRA_PROFILE_NAME 
                              LIMIT 1];
        
        //If logged in user is CRA, then put Virtual Site in list.                      
        if(profileCRA != null && UserInfo.getProfileId() == profileCRA.Id) {
            
            for(VTD1_Protocol_Deviation__c pdRec : recs) {
                if(pdRec.VTD1_Virtual_Site__c != null) {
                    mapSiteToSTM.put(pdRec.VTD1_Virtual_Site__c, null);  
                }
            } 
            
            //Fetch associated CRA of site 
            for(Study_Site_Team_Member__c lstSSTMs : [SELECT Id,
                                                     		VTR5_Associated_CRA__c,
                                                     		VTR5_Associated_CRA__r.User__c, 
                                                     		VTD1_SiteID__c 
                                                        FROM Study_Site_Team_Member__c 
                                                        WHERE VTD1_SiteID__c IN: mapSiteToSTM.keyset()
                                                        AND VTR5_Associated_CRA__c <> NULL
                                                        AND VTR5_Associated_CRA__r.User__c =: UserInfo.getUserId()
                                                    ]) {
                mapSiteToSTM.put(lstSSTMs.VTD1_SiteID__c, lstSSTMs.VTR5_Associated_CRA__r);
            }

            for(VTD1_Protocol_Deviation__c proDevRec : recs) {
                Study_Team_Member__c objSTM = mapSiteToSTM.get(proDevRec.VTD1_Virtual_Site__c); 
                //if logged in CRA is not SSTM on site
                if(objSTM == null) {
                    proDevRec.addError(System.Label.VTR5_notAssociatedCRA);   
                }
            }  
        }  
    }
    // End of code: Conquerors
}