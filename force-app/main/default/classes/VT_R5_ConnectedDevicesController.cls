/**


 * @author Aliaksandr Vabishchevich
 * @date 06-Apr-20
 * @description Base class for Connected Devices epic
 */


public with sharing class VT_R5_ConnectedDevicesController {
    public static Map<String, List<String>> readingTypesByMeasurement = new Map<String, List<String>>{

            'heart_rate' => new List<String>{'heart_rate'},
            'weight' => new List<String>{'weight'},
            'daily_steps' => new List<String>{'daily_steps'},
            'blood_pressure' => new List<String>{'pressure_sys', 'pressure_dia'}
    };

    private static Map<String, List<Decimal>> limitsByType = new Map<String, List<Decimal>>();
    private static String browserTimezone;
    @TestVisible private static List<VTR5_publicreadings__x> readingsForTest = new List<VTR5_publicreadings__x>();



    // Patient Community, Patient Devices page, get all devices for patient
    // used in lwc: vt_r5_patient_my_devices
    @AuraEnabled
    public static List<VTD1_Patient_Device__c> getDevicesList() {
        try {
            Id caseId = [
                    SELECT Id, Contact.VTD1_Clinical_Study_Membership__c
                    FROM User
                    WHERE Id = :UserInfo.getUserId()
            ].Contact.VTD1_Clinical_Study_Membership__c;
            List<VTD1_Patient_Device__c> devices = executeSoqlDevices(null, caseId, null);
            return devices;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }



    // Patient Community, Device Detail Page, Device info card
    // used in lwc: vt_r5_patient_device_card, vt_r5_patient_device_chart
    @AuraEnabled(Cacheable=true)
    public static VTD1_Patient_Device__c getDeviceInfo(String deviceId) { 
        try {
            List<VTD1_Patient_Device__c> devices = executeSoqlDevices(deviceId, null, null);

            System.assert(devices.size() == 1);
            return devices[0];

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }



    /**
     * @description Patient Community (Device Detail page).
     * Get measurements for single device to display in chart/table view.
     * Used in lwc: vt_r5_patient_device_chart
     * @param deviceKey Filter readings by DeviceKeyId
     * @param measurementType
     * @param minDate Filter readings by min date (used in date filters)
     * @param maxDate
     * @param timezone
     *
     * @return List of Measurements
     */
    @AuraEnabled
    public static List<Measurement> getReadingsPatient(String deviceKey, String measurementType, Datetime minDate, Datetime maxDate, String timezone) {
        try {
            browserTimezone = timezone;
            List<Measurement> measurements = getMeasurements(new Set<String>{deviceKey}, measurementType, minDate, maxDate);
            hideArchivedValues(measurements);  // hide archived measurements from Patient and Caregiver view
            return measurements;



        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }



    /**
     * @description SCR/PI Community, Lightning (Device Detail page).
     * Get measurements for single device to display in chart/table view.
     * Used in lwc: vt_r5_devices_pi_detail_page, vt_r5_lightning_patient_devices_summary
     * @param caseId
     * @param deviceKey Filter readings by DeviceKeyId
     * @param measurementType
     * @param minDate Filter readings by min date (used in date filters)
     * @param maxDate
     * @param timezone
     *
     * @return
     */
    @AuraEnabled
    public static List<Measurement> getReadingsSingleDevice(Id caseId, String deviceKey, String measurementType, Datetime minDate, Datetime maxDate, String timezone) {
        try {
            browserTimezone = timezone;
            limitsByType = generateLimitsByType(caseId, measurementType);
            return getMeasurements(new Set<String>{deviceKey}, measurementType, minDate, maxDate);



        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }



    /**
     * @description SCR/PI Community, Lightning (Devices List page).
     * Get measurements for multiple devices to display in chart/table view.
     * Used in lwc: vt_r5_my_patient_devices, vt_r5_lightning_patient_devices
     * @param caseId
     * @param measurementType
     * @param minDate Filter readings by min date (used in date filters)
     * @param maxDate
     * @param timezone
     *
     * @return
     */


    @AuraEnabled
    public static List<Device> getReadingsMultipleDevices(String caseId, String measurementType, Datetime minDate, Datetime maxDate, String timezone) {
        try {
            browserTimezone = timezone;
            limitsByType = generateLimitsByType(caseId, measurementType);
            List<VTD1_Patient_Device__c> devices = executeSoqlDevices(null, caseId, measurementType);


            List<Device> deviceObjs = new List<Device>();
            Map<String, VTD1_Patient_Device__c> devicesByKey = new Map<String, VTD1_Patient_Device__c>();
            System.debug('cd: devices ' + devices);

            for (VTD1_Patient_Device__c device : devices) {
                if (device.VTR5_DeviceKeyId__c == null) {
                    deviceObjs.add(new Device(device, null));
                } else {
                    devicesByKey.put(device.VTR5_DeviceKeyId__c, device);
                }
            }

            // pre-populate result map with empty lists
            Map<String, List<Measurement>> measurementsByDeviceKey = new Map<String, List<Measurement>>();
            for (String key : devicesByKey.keySet()) {
                measurementsByDeviceKey.put(key, new List<Measurement>());
            }

            // get readings from Heroku DB and group by DeviceKeyId
            List<Measurement> measurements = getMeasurements(devicesByKey.keySet(), measurementType, minDate, maxDate);
            for (Measurement r : measurements) {
                measurementsByDeviceKey.get(r.deviceKey).add(r);
            }

            for (String key : measurementsByDeviceKey.keySet()) {
                deviceObjs.add(new Device(devicesByKey.get(key), measurementsByDeviceKey.get(key)));
            }
            return deviceObjs;



        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }



    private static Map<String, List<Decimal>> generateLimitsByType(Id caseId, String measurementType) {
        List<VTR5_StudyMeasurementsConfiguration__c> studyConfigs = getBoundaries(caseId, measurementType);
        Map<String, List<Decimal>> limitsByType = new Map<String, List<Decimal>>();



        for (VTR5_StudyMeasurementsConfiguration__c c : studyConfigs) {
            // generate min and max value for OutOfRange
            Decimal minValue;
            if (c.VTR5_LowerBoundaryHigh_Alert__c != null) {
                minValue = c.VTR5_LowerBoundaryHigh_Alert__c;
            } else if (c.VTR5_LowBoundary__c != null) {
                minValue = c.VTR5_LowBoundary__c;
            }



            Decimal maxValue;
            if (c.VTR5_UpperBoundaryHighAlert__c != null) {
                maxValue = c.VTR5_UpperBoundaryHighAlert__c;
            } else if (c.VTR5_HighBoundary__c != null) {
                maxValue = c.VTR5_HighBoundary__c;
            }
            limitsByType.put(c.VTR5_MeasurementType__c, new List<Decimal> {minValue, maxValue});
        }

        return limitsByType;
    }


    // SCR/PI Community, get Study Measurement Configuration to display tresholds on chart
    // used in lwc: vt_r5_my_patient_devices
    @AuraEnabled
    public static List<VTR5_StudyMeasurementsConfiguration__c> getBoundaries(String caseId, String measurementType) {
        List<String> readingTypes = readingTypesByMeasurement.get(measurementType);
        try {
            Case cs = [SELECT VTD1_Study__c FROM Case WHERE Id = :caseId];
            List<VTR5_StudyMeasurementsConfiguration__c> studyConfigs = [
                    SELECT VTR5_MeasurementType__c, VTR5_HighBoundary__c, VTR5_LowBoundary__c, VTR5_LowerBoundaryHigh_Alert__c, VTR5_UpperBoundaryHighAlert__c
                    FROM VTR5_StudyMeasurementsConfiguration__c
                    WHERE VTR5_Study__c = :cs.VTD1_Study__c
                    AND VTR5_MeasurementType__c IN :readingTypes
            ];
            return studyConfigs;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }



    /** Retrieve Readings (external object) for specified devices, process and transform to Measurements
     * @param deviceKeys Filter readings by DeviceKeyId of devices
     * @param measurementType Filter readings by MeasurementType
     * @param minDate Filter readings by min date (used in date filters)
     * @param maxDate Filter readings by max date (default - now)
     *
     * @return List of Measurements
     */
    private static List<Measurement> getMeasurements(Set<String> deviceKeys, String measurementType, Datetime minDate, Datetime maxDate) {
        List<VTR5_publicreadings__x> readings = executeSoqlPublicReadings(deviceKeys, measurementType, minDate, maxDate);


        Map<String, Measurement> readingsExtendedMap;
        if (measurementType != 'blood_pressure') {
            readingsExtendedMap = generateMeasurements(readings, measurementType);
        } else {
            readingsExtendedMap = generateMeasurementsForBloodPressure(readings);
        }

        System.debug('readingsExtendedMap ' + readingsExtendedMap);
        List<VTR5_DeviceReadingAddInfo__c> readingsInfoList = [
                SELECT VTR5_Reading_ID__c,

                        CreatedBy.VTD1_Full_Name__c,
                        CreatedDate,
                        VTR5_Comments__c,
                        VTR5_Archived__c
                FROM VTR5_DeviceReadingAddInfo__c
                WHERE VTR5_Reading_ID__c IN :readingsExtendedMap.keySet()
                ORDER BY CreatedDate DESC
        ];


        for (VTR5_DeviceReadingAddInfo__c ri : readingsInfoList) {
            String readingId = ri.VTR5_Reading_ID__c;


            Measurement re = readingsExtendedMap.get(readingId);
            re.comments.add(new Comment(ri));
            re.hasComments = true;
            re.isArchived = re.isArchived || ri.VTR5_Archived__c;



            readingsExtendedMap.put(readingId, re);
        }
        System.debug(readingsExtendedMap.values());

        return readingsExtendedMap.values();
    }



    /**
     * @description Take list of Readings and convert to Measurements (for all types except blood pressure)
     * @param allReadings List of Readings (external object) to process
     * @param measurementType Measurement type of Reading
     *
     * @return Map of ReadingId and Measurement objects
     */
    public static Map<String, Measurement> generateMeasurements(List<VTR5_publicreadings__x> allReadings, String measurementType) {
        Map<String, Measurement> measurementsMap = new Map<String, Measurement>();
        for (VTR5_publicreadings__x reading : allReadings) {
            Measurement m = new Measurement(new List<VTR5_publicreadings__x>{reading}, measurementType);
            measurementsMap.put(m.Id, m);


        }
        return measurementsMap;
    }



    /**
     * @description Take list of Readings, group by measurementId and convert to Measurements (only for blood pressure)
     * @param allReadings List of Readings (external object) to process
     *
     * @return Map of ReadingId and Measurement objects
     */
    public static Map<String, Measurement> generateMeasurementsForBloodPressure(List<VTR5_publicreadings__x> allReadings) {
        Map<String, Measurement> readingsExtendedMap = new Map<String, Measurement>();

        // group readings by measurementId. example: {"measurementId1": {"pressure_sys": R1, "pressure_dia": R2}}
        Map<String, Map<String, VTR5_publicreadings__x>> readingsByMeasurement = new Map<String, Map<String, VTR5_publicreadings__x>>();
        for(VTR5_publicreadings__x r : allReadings){
            String measurementId = r.VTR5_device_key_id__c + '-' + r.VTR5_date__c.getTime();


            if(readingsByMeasurement.containsKey(measurementId)) {
                Map<String, VTR5_publicreadings__x> readingsForMeasurement = readingsByMeasurement.get(measurementId);
                readingsForMeasurement.put(r.VTR5_type__c, r);
                readingsByMeasurement.put(measurementId, readingsForMeasurement);
            } else {
                readingsByMeasurement.put(measurementId, new Map<String, VTR5_publicreadings__x>{r.VTR5_type__c => r});
            }
        }



        // write grouped readings to ReadingExtended object
        for (String measurementId : readingsByMeasurement.keySet()) {
            Map<String, VTR5_publicreadings__x> measurementReadings = readingsByMeasurement.get(measurementId);
            List<VTR5_publicreadings__x> readingsForMeasurement = new List<VTR5_publicreadings__x>();
            if (measurementReadings.containsKey('pressure_sys')) readingsForMeasurement.add(measurementReadings.get('pressure_sys'));
            if (measurementReadings.containsKey('pressure_dia')) readingsForMeasurement.add(measurementReadings.get('pressure_dia'));

            System.debug('measurementReadings ' + measurementReadings);
            Measurement m = new Measurement(readingsForMeasurement, 'blood_pressure');
            readingsExtendedMap.put(m.Id, m);

        }
        
        return readingsExtendedMap;
    }



    /**
     * @description Take reading and check if it's out of range (using limitsByType generated before)
     * @param reading Reading (external object)
     *
     * @return True if reading is not in range specified on Study level
     */


    public static Boolean checkValueOutOfRange(VTR5_publicreadings__x reading) {
        if (reading == null) return false;

        Decimal value = reading.VTR5_value__c;
        List<Decimal> limits = limitsByType.get(reading.VTR5_type__c);
        if (limits == null) return false;



        if (limits[0] != null && value <= limits[0]) return true;  // check if less then lower limit
        if (limits[1] != null && value >= limits[1]) return true;  // check if higher then upper limit
        return false;
    }



    /**
     * @description Remove archived Measurements from list (used to hide them from Patient/Caregiver)
     * @param measurements List of Measurements
     */
    // Remove archived values from readings list
    public static void hideArchivedValues(List<Measurement> measurements) {
        Integer i = 0;
        while (i < measurements.size()) {
            if(measurements.get(i).isArchived) {
                measurements.remove(i);
            } else {
                i++;
            }
        }
    }

    /**
     * @description Retrieves readings for the list of devices
     * @param deviceKeys Filter readings by DeviceKeyId of devices
     * @param measurementType Filter readings by MeasurementType
     * @param minDate Filter readings by min date (used in date filters)
     * @param maxDate Filter readings by max date (default - now)
     *
     * @return List of Readings external objects
     */
    public static List<VTR5_publicreadings__x> executeSoqlPublicReadings(Set<String> deviceKeys, String measurementType, Datetime minDate, Datetime maxDate) {


        List<String> readingTypes = readingTypesByMeasurement.get(measurementType);

        // official workaround for "2000 rows max" limitation: https://help.salesforce.com/articleView?id=000339353&type=1
        List<VTR5_publicreadings__x> readings = new List<VTR5_publicreadings__x>();
        Integer readingsChunkSize;



        while (readingsChunkSize == null || readingsChunkSize == 2000) {
            List<VTR5_publicreadings__x> readingsChunk = new List<VTR5_publicreadings__x>([
                    SELECT ExternalId, VTR5_date__c, VTR5_value__c, VTR5_unit__c, VTR5_type__c, VTR5_device_key_id__c
                    FROM VTR5_publicreadings__x
                    WHERE VTR5_device_key_id__c IN :deviceKeys


                AND VTR5_type__c IN :readingTypes
                AND VTR5_date__c >= :minDate
                    AND VTR5_date__c < :maxDate
                    ORDER BY VTR5_date__c DESC
                    LIMIT 2000
            ]);
            readings.addAll(readingsChunk);


            readingsChunkSize = readingsChunk.size();
            if (readingsChunkSize > 0) {
                maxDate = readingsChunk[readingsChunkSize - 1].VTR5_date__c;
            }
        }

        if(Test.isRunningTest()) {
            return (readingsForTest.size() > 0) ? readingsForTest : null;
        }

        return readings;
    }




    /**
     * @description Creates a DeviceReadingAddInfo record with comment for measurement (SH-8218)
     * @param text Comment text
     * @param deviceKey DeviceKeyId of device to link
     * @param readingIds List of all reading ids related to measurement and comment (2 for blood pressure, 1 for others)
     */
    @AuraEnabled
    public static void createCommentRecord(String text, String deviceKey, List<String> readingIds) {
        try{
            VTR5_DeviceReadingAddInfo__c comment = new VTR5_DeviceReadingAddInfo__c();
            VTD1_Patient_Device__c device = [SELECT Id FROM VTD1_Patient_Device__c WHERE VTR5_DeviceKeyId__c = :deviceKey];
            comment.VTR5_Comments__c = text;
            comment.VTR5_DeviceID__c = device.Id;
            comment.VTR5_DeviceSerialNumber__c = deviceKey;
            comment.VTR5_Reading_ID__c = readingIds[0];
            if (readingIds.size() > 1) {
                comment.VTR5_SecondReading_ID__c = readingIds[1];
            }
            insert comment;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    /**
     * @description: Verify if current user has permissions to comment/archive on readings (SH-8218)
     *
     * @return True if has permissions
     */
    @AuraEnabled
    public static Boolean permittedProfiles() {
        List<String> allowedProfiles = new List<String>{'Patient Guide', 'Site Coordinator', 'Primary Investigator', 'System Administrator'};
        String profileName = [SELECT Id,Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        return allowedProfiles.contains(profileName);
    }

    /**
     * @description: Get PatientDevices records
     *
     * @param deviceId Salesforce Id of device
     * @param caseId Filter devices by patient Case
     * @param measurementType Filter devices by measurementType (heart_rate, blood_pressure, weight, daily_steps)
     *
     * @return List of PatientDevice Records
     */
    private static List<VTD1_Patient_Device__c> executeSoqlDevices(Id deviceId, Id caseId, String measurementType) {
        try {
            String recordType = 'VTD1_Connected_Device';
            String query = 'SELECT Id' +
                    ', Name' +
                    ', VTR5_DeviceKeyId__c' +  // main key for all mappings
                    ', VTD1_Manufacturer__c' +
                    ', VTD1_Model_Name__c' +
                    ', VTR5_LastReadingDate__c' +
                    ', VTR5_Next_Reading_Date__c' +
                    ', VTR5_SyncStatus__c' +
                    ', VTD1_Vendor_Reported_Serial_Number__c' +
                    ', VTR5_IMEI__c' +
                    ', VTD1_Name__c' +
                    ', VTR5_LastDataSync__c' +
                    ', VTD1_Protocol_Device__r.VTR5_Measurement_Type__c' +
                    ', toLabel(VTD1_Protocol_Device__r.VTR5_Measurement_Type__c) typeLabel' +
                    ', VTD1_Protocol_Device__r.VTD1_Device__r.VTR5_IsContinous__c' +
                    ', VTD1_Protocol_Device__r.VTD1_Device__r.VTR5_Image__c' +
                    ' ' +
                    'FROM VTD1_Patient_Device__c ' +
                    'WHERE' +
                    ' RecordType.DeveloperName = :recordType' +
                    ' AND VTD1_Protocol_Device__c != NULL' +
                    ' AND VTD1_Protocol_Device__r.VTD1_Device__c != NULL';
            if (deviceId != null) {
                query += ' AND Id = :deviceId';
            }
            if (measurementType != null) {
                query += ' AND VTD1_Protocol_Device__r.VTR5_Measurement_Type__c INCLUDES (:measurementType)';
            }
            if (caseId != null) {
                query += ' AND VTD1_Case__c = :caseId';
            }
            System.debug(query);
            List<VTD1_Patient_Device__c> devices = Database.query(query);
            return devices;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class Device {
        @AuraEnabled public Id Id { get; set; }
        @AuraEnabled public VTD1_Patient_Device__c info { get; set; }
        @AuraEnabled public List<Measurement> readings { get; set; }

        public Device(VTD1_Patient_Device__c device, List<Measurement> readings) {
            this.Id = device.Id;
            this.info = device;
            this.readings = readings != null ? readings : new List<Measurement>();
        }
    }

    public class Measurement {
        @AuraEnabled public String Id { get; set; }
        @AuraEnabled public String deviceKey { get; set; }

        @AuraEnabled public List<String> ids { get; set; }
        @AuraEnabled public List<Decimal> values { get; set; }
        @AuraEnabled public String valueLabel { get; set; }



        @AuraEnabled public String readingDateLabel { get; set; }
        @AuraEnabled public String readingTimeLabel { get; set; }
        @AuraEnabled public Datetime readingDate { get; set; }
        @AuraEnabled public String unit { get; set; }
        @AuraEnabled public String measurementType { get; set; }



        @AuraEnabled public Boolean isArchived { get; set; }
        @AuraEnabled public Boolean isOutOfRange { get; set; }
        @AuraEnabled public Boolean hasComments { get; set; }
        @AuraEnabled public List<Comment> comments { get; set; }



        public Measurement(List<VTR5_publicreadings__x> readingsForMeasurement, String measurementType) {
            VTR5_publicreadings__x mr = readingsForMeasurement[0];  // main reading
            this.Id = mr.ExternalId;
            this.deviceKey = mr.VTR5_device_key_id__c;
            this.readingDate = mr.VTR5_date__c;

            System.debug('before readingDateLabel ' + mr.VTR5_date__c);
            this.readingDateLabel = mr.VTR5_date__c.format('dd-MMM-yyyy', browserTimezone);
            System.debug('readingDateLabel ' + readingDateLabel);
            this.readingTimeLabel = mr.VTR5_date__c.format('hh:mm a', browserTimezone);
            System.debug('readingTimeLabel ' + readingTimeLabel);
            this.unit = mr.VTR5_unit__c;
            this.measurementType = measurementType;


            this.ids = new List<String>();
            for (VTR5_publicreadings__x r : readingsForMeasurement) {
                this.ids.add(r.ExternalId);
            }

            this.values = new List<Decimal>();



            if (measurementType != 'blood_pressure') {
                this.values.add(mr.VTR5_value__c);
                this.valueLabel = mr.VTR5_value__c + (String.isBlank(mr.VTR5_unit__c) ? '' : ' ' + mr.VTR5_unit__c);
                this.isOutOfRange = checkValueOutOfRange(mr);


            } else {


                this.isOutOfRange = false;
                for (String readingType : new List<String>{'pressure_sys', 'pressure_dia'}) {
                    VTR5_publicreadings__x readingForType;
                    for (VTR5_publicreadings__x r : readingsForMeasurement) {
                        if (r.VTR5_type__c == readingType) readingForType = r;
                    }



                    if (readingForType == null) {
                        this.values.add(-1);
                        continue;
                    }

                    this.values.add(Integer.valueOf(readingForType.VTR5_value__c));

                    this.isOutOfRange = this.isOutOfRange || checkValueOutOfRange(readingForType);
                }
                this.valueLabel = String.join(this.values, '/').replace('-1', '-');
            }



            this.isArchived = false;
            this.hasComments = false;
            this.comments = new List<Comment>();
        }
    }



    public class Comment {
        @AuraEnabled public Id Id { get; set; }
        @AuraEnabled public String text { get; set; }
        @AuraEnabled public String createdBy { get; set; }
        @AuraEnabled public Datetime createdDate { get; set; }
        @AuraEnabled public String createdDateLabel { get; set; }
        @AuraEnabled public String createdTimeLabel { get; set; }

        public Comment(VTR5_DeviceReadingAddInfo__c readingAddInfo) {
            this.Id = readingAddInfo.Id;
            this.text = readingAddInfo.VTR5_Comments__c;
            this.createdBy = readingAddInfo.CreatedBy.VTD1_Full_Name__c;
            this.createdDate = readingAddInfo.CreatedDate;
            this.createdDateLabel = readingAddInfo.CreatedDate.format('dd-MMM-yyyy', browserTimezone);
            this.createdTimeLabel = readingAddInfo.CreatedDate.format('hh:mm a', browserTimezone);


        }
    }




// - - - - - - - - - - - OLD - - - - - - - - - - - //
/*
    @AuraEnabled
    public static List<List<Boundary>> studyConfigList(String caseId, String measurementType) {
        List<String> readingTypes = readingTypesByMeasurement.get(measurementType);
        List<List<Boundary>> configList = new List<List<Boundary>>();
        try {
            Case cs = [SELECT VTD1_Study__c FROM Case WHERE Id = :caseId];
            List<VTR5_StudyMeasurementsConfiguration__c> studyConfigs = [
                    SELECT VTR5_MeasurementType__c,
                            VTR5_HighBoundary__c,
                            VTR5_LowBoundary__c,
                            VTR5_LowerBoundaryHigh_Alert__c,
                            VTR5_UpperBoundaryHighAlert__c
                    FROM VTR5_StudyMeasurementsConfiguration__c
                    WHERE VTR5_Study__c = :cs.VTD1_Study__c
                    AND VTR5_MeasurementType__c IN :readingTypes
            ];
            for(VTR5_StudyMeasurementsConfiguration__c config : studyConfigs) {
                List<Boundary> boundaries = new List<Boundary>();
                if(config.VTR5_LowBoundary__c != null) {
                    Boundary item = new Boundary();
                    item.readingType = config.VTR5_MeasurementType__c;
                    item.type = 'low';
                    item.value = config.VTR5_LowBoundary__c;
                    boundaries.add(item);
                }
                if(config.VTR5_HighBoundary__c != null) {
                    Boundary item = new Boundary();
                    item.readingType = config.VTR5_MeasurementType__c;
                    item.type = 'high';
                    item.value = config.VTR5_HighBoundary__c;
                    boundaries.add(item);
                }
                if(config.VTR5_LowerBoundaryHigh_Alert__c != null) {
                    Boundary item = new Boundary();
                    item.readingType = config.VTR5_MeasurementType__c;
                    item.type = 'low';
                    item.value = config.VTR5_LowerBoundaryHigh_Alert__c;
                    boundaries.add(item);
                }
                if(config.VTR5_LowerBoundaryHigh_Alert__c != null) {
                    Boundary item = new Boundary();
                    item.readingType = config.VTR5_MeasurementType__c;
                    item.type = 'high';
                    item.value = config.VTR5_UpperBoundaryHighAlert__c;
                    boundaries.add(item);
                }
                configList.add(boundaries);
            }
            return configList;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    public class Boundary {
        public String readingType;
        public String type;
        public Decimal value;
    }



    // Get readings from Heroku table through HTTP request
    public static List<Object> sendRequest(String deviceKey, String readingType) {

        String username = '6d10f3af36934616bc932119f4af4c82';
        String password = '44b2c7700343432d8d38df6afe5f5d77';
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(username + ':' + password));
        String API_ENDPOINT = 'https://odata-2-virginia.heroku.com/odata/v4/6d6bfc0325514b5eada2353b83df1c07/public$readings';
//        Date minDate = Date.today().addDays(-4);
        String url = API_ENDPOINT + '?$filter=' +

                'device_key_id+eq+%27' + deviceKey + '%27' +

                '+and+type+eq+%27' + readingType + '%27' +
//                '+and+date+ge+%27' + String.valueOf(minDate) + '%27' +
                '&$select=id,type,date,value,unit' +
                '&$orderby=date' +
                '&$top=10';
        Http http = new Http();
        HttpRequest r = new HttpRequest();
        r.setEndpoint(url);
        r.setHeader('Authorization', authorizationHeader);
        r.setMethod('GET');



        HttpResponse response = http.send(r);
        if (response.getStatusCode() != 200) {
            throw new AuraHandledException('Invalid response ' + response.getStatusCode() + ': ' + response.getBody());
        }
        System.debug(response.getBody());
        // List<VT_R5_ConnectedDevicesJsonParse.Reading> dbRows = VT_R5_ConnectedDevicesJsonParse.parse(response.getBody());
        return null;
    }

 */

}