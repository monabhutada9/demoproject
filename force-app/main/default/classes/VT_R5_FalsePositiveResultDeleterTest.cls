/**
 * @author Dmitry Yartsev
 * @date 26.10.2020
 */

@IsTest
private class VT_R5_FalsePositiveResultDeleterTest {

    private static final String DUE_SOON_STATUS = 'Due Soon';
    private static final String MISSED_STATUS = 'Missed';
    private static final String COMPLETED_STATUS = 'Completed';

    private enum Scenario { DUE_SOON, MISSED, COMPLETED, ALL, EMPTY }

    private static Map<Scenario, Case> scenarioToCase;
    private static VT_R5_FalsePositiveResultDeleterBatch batch;

    static {
        scenarioToCase = getScenarioToCaseMap();
        batch = new VT_R5_FalsePositiveResultDeleterBatch('test', fflib_IDGenerator.generate(HealthCloudGA__CarePlanTemplate__c.getSObjectType()));
    }

    @IsTest
    private static void batchTest() {
        Test.startTest();
        {
            VT_Stubber.applyStub(VT_R5_FalsePositiveResultDeleterBatch.START_QUERY_STUB_NAME, scenarioToCase.values());
            Database.executeBatch(batch);
        }
        Test.stopTest();
    }

    @IsTest
    private static void dueSoonTest() {
        Case cas = scenarioToCase.get(Scenario.DUE_SOON);
        List<VTD1_Survey__c> surveysToDelete = batch.getDiariesToDelete(cas.Surveys__r);
        System.assertEquals(1, surveysToDelete.size(), '1 survey must be deleted');
    }

    @IsTest
    private static void missedTest() {
        Case cas = scenarioToCase.get(Scenario.MISSED);
        List<VTD1_Survey__c> surveysToDelete = batch.getDiariesToDelete(cas.Surveys__r);
        System.assertEquals(2, surveysToDelete.size(), '2 surveys must be deleted');
    }

    @IsTest
    private static void completedTest() {
        Case cas = scenarioToCase.get(Scenario.COMPLETED);
        List<VTD1_Survey__c> surveysToDelete = batch.getDiariesToDelete(cas.Surveys__r);
        System.assertEquals(1, surveysToDelete.size(), '1 survey must be deleted');
    }

    private static Map<Scenario, Case> getScenarioToCaseMap() {
        Map<Scenario, Case> scenarioToCase = new Map<Scenario, Case>();
        scenarioToCase.put(Scenario.ALL, getCaseWithSurveys(getSurveysQueryResult(getSurveysList(Scenario.ALL))));
        scenarioToCase.put(Scenario.DUE_SOON, getCaseWithSurveys(getSurveysQueryResult(getSurveysList(Scenario.DUE_SOON))));
        scenarioToCase.put(Scenario.MISSED, getCaseWithSurveys(getSurveysQueryResult(getSurveysList(Scenario.MISSED))));
        scenarioToCase.put(Scenario.COMPLETED, getCaseWithSurveys(getSurveysQueryResult(getSurveysList(Scenario.COMPLETED))));
        scenarioToCase.put(Scenario.EMPTY, getCaseWithSurveys(null));
        return scenarioToCase;
    }

    private static Case getCaseWithSurveys(Map<String, Object> surveysQueryResult) {
        Map<String, Object> caseFieldMap = new Map<String, Object>{ 'Surveys__r' => surveysQueryResult };
        String caseJSON = JSON.serialize(caseFieldMap);
        return (Case) JSON.deserialize(caseJSON, Case.class);
    }

    private static Map<String, Object> getSurveysQueryResult(List<VTD1_Survey__c> surveys) {
        return new  Map<String, Object>{
                'totalSize' => surveys.size(),
                'done' => true,
                'records' => surveys
        };
    }

    private static List<VTD1_Survey__c> getSurveysList(Scenario sc) {
        List<VTD1_Survey__c> surveysList = new List<VTD1_Survey__c>();
        String ruleGuid = String.valueOf(Crypto.getRandomLong());
        switch on sc {
            when DUE_SOON {
                surveysList.add(new VTD1_Survey__c(VTD1_Status__c = DUE_SOON_STATUS, VTR5_eCoa_ruleGuid__c = ruleGuid));
                surveysList.add(new VTD1_Survey__c(VTD1_Status__c = MISSED_STATUS, VTR5_eCoa_ruleGuid__c = ruleGuid));
                surveysList.add(new VTD1_Survey__c(VTD1_Status__c = COMPLETED_STATUS, VTR5_eCoa_ruleGuid__c = ruleGuid));
            }
            when MISSED {
                surveysList.add(new VTD1_Survey__c(VTD1_Status__c = MISSED_STATUS, VTR5_eCoa_ruleGuid__c = ruleGuid));
                surveysList.add(new VTD1_Survey__c(VTD1_Status__c = MISSED_STATUS, VTR5_eCoa_ruleGuid__c = ruleGuid));
                surveysList.add(new VTD1_Survey__c(VTD1_Status__c = MISSED_STATUS, VTR5_eCoa_ruleGuid__c = ruleGuid));
            }
            when COMPLETED {
                surveysList.add(new VTD1_Survey__c(VTD1_Status__c = COMPLETED_STATUS, VTR5_eCoa_ruleGuid__c = ruleGuid));
                surveysList.add(new VTD1_Survey__c(VTD1_Status__c = MISSED_STATUS, VTR5_eCoa_ruleGuid__c = ruleGuid));
                surveysList.add(new VTD1_Survey__c(VTD1_Status__c = COMPLETED_STATUS, VTR5_eCoa_ruleGuid__c = ruleGuid));
            }
            when ALL {
                surveysList.addAll(getSurveysList(Scenario.DUE_SOON));
                surveysList.addAll(getSurveysList(Scenario.MISSED));
                surveysList.addAll(getSurveysList(Scenario.COMPLETED));
            }
        }
        return surveysList;
    }

}