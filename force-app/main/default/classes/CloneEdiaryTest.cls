@IsTest
public with sharing class CloneEdiaryTest{
    @isTest
    static void testDomainCase() {
        VT_R3_GlobalSharing.disableForTest = false;
        Test.startTest();
        //Create Study
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );

        //Create User
        DomainObjects.User_t user = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5));

        //Create PI User
        DomainObjects.User_t piuser = new DomainObjects.User_t()
                .setProfile('Primary Investigator')
                .setEmail('test@email.com')
                .addContact(new DomainObjects.Contact_t());
            piuser.persist();

        //Create PI STM
        DomainObjects.StudyTeamMember_t stm = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('PI')
                .addUser(piuser)
                .addStudy(study);
             stm.persist();
        
        //Create SCR User
        DomainObjects.User_t scruser = new DomainObjects.User_t()
                .setProfile('Site Coordinator')
                .setEmail('testscr@email.com')
                .addContact(new DomainObjects.Contact_t());
            piuser.persist();

        //Create SCR STM            
        DomainObjects.StudyTeamMember_t stm1 = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('SCR')
                .addUser(scruser)
                .addStudy(study);
                stm1.persist();

        //Create Virtual Site
        DomainObjects.VirtualSite_t virtSite = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .setStudySiteNumber('12345')
                .addStudyTeamMember(stm);
                //.setCountry('US')
                //.setState('NY')
                //.setAddressLine1('Test123');
                virtSite.persist();

        DomainObjects.VTR2_Study_Geography_t studyGeo = new DomainObjects.VTR2_Study_Geography_t()
                .addVTD2_Study(study)
                .setGeographicalRegion('Country')
                .setCountry('US')
                .setDateOfBirthRestriction('Year Only');
        DomainObjects.VTR2_StudyPhoneNumber_t studyPhone = new DomainObjects.VTR2_StudyPhoneNumber_t()
                .addVTR2_Study_Geography(studyGeo);

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t();
        DomainObjects.User_t ptUser = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile('Patient');

          DomainObjects.Case_t patientCase = new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .setSubject('VT_R5_allTest')
                .addPIUser(piUser)
                .addVTD1_Patient_User(ptUser)
                .addStudy(study)
                .addUser(ptUser)
                .addContact(patientContact)
                .addStudyGeography(studyGeo)
                .addVirtualSite(virtSite);
        patientCase.persist();
        DomainObjects.VTD1_Protocol_ePRO_t protocolEPROT = new DomainObjects.VTD1_Protocol_ePRO_t()
                .setRecordTypeByName('SatisfactionSurvey')
                .addVTD1_Study(study)
                .setVTD1_Type('Non-PSC')
                .setVTR4_Is_Branching_eDiary(true)
                .setVTR2_Protocol_Reviewer('Patient Guide')
                .setVTD1_Subject('Patient')
                .setVTD1_Response_Window(10);
        DomainObjects.VTD1_Survey_t survey = new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(patientCase.Id)
                .setName('testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttest')
                .addVTD1_Protocol_ePRO(protocolEPROT);
        survey.persist();

        DomainObjects.VTD1_Survey_Answer_t answer = new DomainObjects.VTD1_Survey_Answer_t()
                .addVTD1_Question(new DomainObjects.VTD1_Protocol_ePro_Question_t()
                        .addVTD1_Protocol_ePRO(new DomainObjects.VTD1_Protocol_ePRO_t()
                                .setRecordTypeByName('SatisfactionSurvey')
                                .addVTD1_Study(study)
                                .setVTD1_Type('PSC')
                                .setVTR2_Protocol_Reviewer('Patient Guide')
                                .setVTD1_Subject('Patient')
                                .setVTD1_Response_Window(10)))
                .addVTD1_Survey(survey);
        answer.persist();      

        

        
        Case ptCase = [SELECT Id, 
                                VTD1_PI_user__c, 
                                VTD1_Study__c, 
                                VTD1_Backup_PI_User__c, 
                                VTD1_PI_contact__c,
                                VTD1_Virtual_Site__c
                        FROM Case 
                        WHERE RecordType.Name = 'CarePlan' LIMIT 1];
        System.debug('ptCase---'+ptCase);
        System.assertNotEquals(null, ptCase.VTD1_Virtual_Site__c);

        List<VTD1_Survey_Answer__c> lstSurveyAnswers ;
        System.runAs(new User(Id = piUser.Id)){
      

        
        /*Case ptCase1   = [SELECT Id, 
                                VTD1_PI_user__c, 
                                VTD1_Study__c, 
                                VTD1_Backup_PI_User__c, 
                                VTD1_PI_contact__c,
                                VTD1_Virtual_Site__c
                        FROM Case 
                        WHERE RecordType.Name = 'CarePlan' LIMIT 1];
        System.debug('ptCase---'+ptCase1);*/
                lstSurveyAnswers = [SELECT    Id,
                                                VTD1_Survey__c,
                                                VTD1_Answer__c
                                                FROM VTD1_Survey_Answer__c];
        }
            System.assertEquals(true, lstSurveyAnswers.size()>0);  
        
    }
    @isTest
    static void testTestUtils(){
         Test.startTest();
         VT_R3_GlobalSharing.disableForTest = false;
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );


		
        new DomainObjects.HealthCloudGA_CandidatePatient_t()
                .setCaregiverEmail(DomainObjects.RANDOM.getEmail())

                .setRr_firstName('Mike')
                .setRr_lastName('Birn')
                .setConversion('Converted')
                .addStudy(study)
                .persist();

        Test.stopTest();

        DomainObjects.User_t userPI = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Primary Investigator');
        DomainObjects.StudyTeamMember_t stmPI = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(userPI)
                .setRecordTypeByName('PI');
        stmPI.persist();

        Case carePlan1 = [SELECT Id, VTD1_Study__c, VTD1_Patient_User__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];


             DomainObjects.VTD1_Protocol_ePRO_t protocolEPROT = new DomainObjects.VTD1_Protocol_ePRO_t()
                .setRecordTypeByName('SatisfactionSurvey')
                .addVTD1_Study(study)
                .setVTD1_Type('Non-PSC')
                .setVTR4_Is_Branching_eDiary(true)
                .setVTR2_Protocol_Reviewer('Patient Guide')
                .setVTD1_Subject('Patient')
                .setVTD1_Response_Window(10);
        DomainObjects.VTD1_Survey_t survey = new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(carePlan1.Id)
                .setName('testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttest')
                .addVTD1_Protocol_ePRO(protocolEPROT);
        survey.persist();

        DomainObjects.VTD1_Survey_Answer_t answer = new DomainObjects.VTD1_Survey_Answer_t()
                .addVTD1_Question(new DomainObjects.VTD1_Protocol_ePro_Question_t()
                        .addVTD1_Protocol_ePRO(new DomainObjects.VTD1_Protocol_ePRO_t()
                                .setRecordTypeByName('SatisfactionSurvey')
                                .addVTD1_Study(study)
                                .setVTD1_Type('PSC')
                                .setVTR2_Protocol_Reviewer('Patient Guide')
                                .setVTD1_Subject('Patient')
                                .setVTD1_Response_Window(10)))
                .addVTD1_Survey(survey);
        answer.persist();  


        Case carePlan2 = [SELECT Id, VTD1_Study__c, VTD1_Patient_User__c, VTD1_PI_user__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];
        List<VTD1_Survey_Answer__c> lstSurveyAnswers ;
        User U = [SELECT Id FROM User WHERE Id =: userPI.Id];
        System.assertEquals(carePlan2.VTD1_PI_user__c, userPI.Id);
        System.runAs(U){
                Case carePlan = [SELECT Id, VTD1_Study__c, VTD1_Patient_User__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];
                System.assertNotEquals(null , carePlan.Id);



                lstSurveyAnswers = [SELECT    Id,
                                                        VTD1_Survey__c,
                                                        VTD1_Answer__c
                                                        FROM VTD1_Survey_Answer__c];
        }
        System.assertEquals(true, lstSurveyAnswers.size()>0);  
    }
}