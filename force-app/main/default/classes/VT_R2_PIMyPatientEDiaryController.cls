public class VT_R2_PIMyPatientEDiaryController {
    private static final Map<String, Set<String>> SURVEY_RECORD_TYPES = new Map<String, Set<String>>{
            'eDiary' => new Set<String>{
                    'ePRO', 'VTR5_External'
            },
            'SourceForm' => new Set<String>{
                    'VTR5_SourceForm'
            }
    };

    @AuraEnabled(Cacheable=false)
    public static SurveysWrapper getSurveys(Id caseId, String type, String sortOrder, String filterVal, String searchParam) {
        MyPatientEDiaryFactory factory = new MyPatientEDiaryFactory();
        factory.addAlgorithm(new PiMyPatientEDiarySelector());
        factory.addAlgorithm(new ScrMyPatientEDiarySelector());
        return factory.getSurveys(caseId, type, sortOrder, filterVal, searchParam);
    }
    @AuraEnabled
    public static void updateSurvey(VTD1_Survey__c survey) {
        update survey;
    }

    public interface MyPatientEDiarySelector {
        Boolean isSuitableProfile();
        SurveysWrapper getSurveys(Id caseId, String type, String sortOrder, String filterVal, String searchParam);
    }
    public abstract class ProfileMyPatientEDiarySelector implements MyPatientEDiarySelector {
        protected abstract Id getProfileId();
        protected abstract SurveysWrapper getSurveys(Id caseId, String type, String sortOrder, String filterVal, String searchParam);

        /* Modifed method to implement changes for eDiary sort,search and filter */
        protected List<VTD1_Survey__c> getSurveysByCaseId(Id caseId, String type, String sortOrder, String filterVal, String searchParam) {
            String orderBy = type == 'eDiary' ? 'VTD1_Due_Date__c ' : 'VTR5_Last_Modified_Externally__c ';
            String sortOrderVal = sortOrder == null ? '' : sortOrder ;
            Set<String> recordTypes = type == 'eDiary' ? SURVEY_RECORD_TYPES.get('eDiary') : SURVEY_RECORD_TYPES.get('SourceForm');
            String ediaryStatusFilter = '';
            List<String> statuses = new List<String>();
            if (filterVal == 'allEdiaryFilter') {
                statuses.add('eDiary Missed');
                statuses.add('Not Started');
                statuses.add('Missed');
                statuses.add('Due Soon');
            } else if (filterVal == 'missedEDiariesFilter') {
                statuses.add('eDiary Missed');
                statuses.add('Missed');
            } else if (filterVal == 'futureEDiariesFilter') {
                statuses.add('Not Started');
                statuses.add('Due Soon');
            }
            ediaryStatusFilter = ' AND VTD1_Status__c IN :statuses ';
            if (filterVal == 'noEdiaryFilter' || String.IsBlank(filterVal)) {
                ediaryStatusFilter = '' ;
            }

            String searchFilter ;

            if (!String.IsBlank(searchParam)) {
                searchParam = String.escapeSingleQuotes(searchParam);
                searchFilter = ' AND Name LIKE ' + '\'' + '%' + searchParam + '%' + '\'' ;
            } else {
                searchFilter = '' ;
            }

            String query = 'SELECT Id' +
                    '            , Name' +
                    '            , VTD1_Protocol_ePRO__r.Name' +
                    '            , VTD1_Status__c, VTD1_Protocol_ePRO__c, VTR5_Clinician__c' +
                    '            , VTD1_Completion_Time__c, VTR5_Last_Modified_Externally__c' +
                    '            , VTR5_MissedReason__c,VTR5_Reviewed__c,RecordTypeId,RecordType.DeveloperName' +
                    '            , VTD1_Due_Date__c, VTD1_Trigger__c, VTR5_WasReadBy__c' +
                    '            , VTD1_Start_Time__c, VTR2_Reviewer_User__c, VTR5_CompletedbyCaregiver__c' +
                    '            , VTD1_CSM__r.VTR2_SiteCoordinator__c' + // [DK] Primary SCR on Patient Case
                    '            , VTD1_CSM__r.VTD1_Virtual_Site__r.VTR2_Primary_SCR__c' +
                    '            , VTD1_CSM__r.VTD1_Study__r.VTR5_EDC_System_Link__c' +
                    '			 , (SELECT VTD1_Answer__c,VTD1_Score__c,VTD1_Question__c,VTD1_Survey__r.Name,VTR5_External_Question__c, VTR5_External_Question_Number__c,RecordType.Name, RecordTypeId from Survey_Answers__r ORDER BY VTR5_External_Question_Number__c)' +
                    '    FROM VTD1_Survey__c' +
                    '    WHERE VTD1_CSM__c = :caseId AND RecordType.DeveloperName IN :recordTypes' + ediaryStatusFilter + searchFilter +
                    '    ORDER BY ' + orderBy + sortOrderVal + ' LIMIT 300';
            return Database.query(query);
        }
        public Boolean isSuitableProfile() {
            return UserInfo.getProfileId() == this.getProfileId();
        }
    }
    public class MyPatientEDiaryFactory {
        public List<MyPatientEDiarySelector> algorithms;
        public MyPatientEDiaryFactory() {
            this.algorithms = new List<MyPatientEDiarySelector>();
        }
        public SurveysWrapper getSurveys(Id caseId, String type, String sortOrder, String filterVal, String searchParam) {
            SurveysWrapper result;
            for (MyPatientEDiarySelector algorithm : this.algorithms) {
                if (algorithm.isSuitableProfile()) {
                    result = algorithm.getSurveys(caseId, type, sortOrder, filterVal, searchParam);
                    break;
                }
            }
            return result;
        }
        public void addAlgorithm(MyPatientEDiarySelector myPatientEDiarySelector) {
            this.algorithms.add(myPatientEDiarySelector);
        }
    }

    public without sharing class PiMyPatientEDiarySelector extends ProfileMyPatientEDiarySelector {
        public override Id getProfileId() {
            return getPiProfileId();
        }
        public override SurveysWrapper getSurveys(Id caseId, String type, String sortOrder, String filterVal, String searchParam) {
            SurveysWrapper sw = new SurveysWrapper();
            Case caseRecord = new Case();
            List<Case> cases = getCaseByCaseId(caseId);
            if (!cases.isEmpty()) {
                caseRecord = cases[0];
                if (type == 'SourceForm' && caseRecord.VTD1_Study__r.VTR5_SourceFormToolSelection__c != 'eCOA ClinRO') {
                    return null;
                }
                sw.caseRecord = caseRecord;
            } else {
                return null;
            }
            List<VTD1_Survey__c> surveys = getSurveysByCaseId(caseId, type, sortOrder, filterVal, searchParam);
            sw.surveys = surveys;
            Long offset = (Datetime.newInstance(Datetime.now().date(), Datetime.now().time()).getTime() - Datetime.newInstance(Datetime.now().dateGmt(), Datetime.now().timeGmt()).getTime()) / (60 * 60 * 1000);
            sw.timeZoneOffset = offset;
            sw.surveyToFilesJson = surveys.isEmpty() ? '' : getFiles(surveys);
            return sw;
        }
        public String getFiles(List<VTD1_Survey__c> surveys) {
            Map<Id, List<ContentDocumentLink>> surveyToFiles = new Map<Id, List<ContentDocumentLink>>();
            Set<Id> surveysIds = new Set<Id>();
            for (VTD1_Survey__c survey : surveys) {
                surveysIds.add(survey.Id);
            }
            List<ContentDocumentLink> links = getContentDocumentLinks(surveysIds);
            for (ContentDocumentLink cdl : links) {
                if (!surveyToFiles.containsKey(cdl.LinkedEntityId)) {
                    surveyToFiles.put(cdl.LinkedEntityId, new List<ContentDocumentLink>());
                }
                surveyToFiles.get(cdl.LinkedEntityId).add(cdl);
            }
            return JSON.serialize(surveyToFiles);
        }
        private List<Case> getCaseByCaseId(Id caseId) {
            return [
                    SELECT Id
                            , VTD1_Study__r.Name
                            , Contact.Name
                            , VTD1_Study__r.VTR5_SourceFormToolSelection__c
                    FROM Case
                    WHERE Id = :caseId
                    LIMIT 1
            ];
        }
        private List<ContentDocumentLink> getContentDocumentLinks(Set<Id> surveysIds) {
            return [
                    SELECT Id
                            , LinkedEntityId
                            , ContentDocumentId
                            , IsDeleted
                            , ContentDocument.Title
                            , ContentDocument.FileExtension
                    FROM ContentDocumentLink
                    WHERE LinkedEntityId IN :surveysIds
            ];
        }
        private Id getPiProfileId() {
            return [SELECT Id FROM Profile WHERE Name = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME][0].Id;
        }
    }

    public without sharing class ScrMyPatientEDiarySelector extends ProfileMyPatientEDiarySelector {
        public override Id getProfileId() {
            return getScrProfileId();
        }
        public override SurveysWrapper getSurveys(Id caseId, String type, String sortOrder, String filterVal, String searchParam) {
            SurveysWrapper sw = new SurveysWrapper();
            Case caseRecord = new Case();
            List<Case> cases = getCaseByCaseId(caseId);
            if (!cases.isEmpty()) {
                caseRecord = cases[0];
                if (type == 'SourceForm' && caseRecord.VTD1_Study__r.VTR5_SourceFormToolSelection__c != 'eCOA ClinRO') {
                    return null;
                }
                sw.caseRecord = caseRecord;
            } else {
                return null;
            }
            List<VTD1_Survey__c> surveys = getSurveysByCaseId(caseId, type, sortOrder, filterVal, searchParam);
            sw.surveys = surveys;
            Long offset = (Datetime.newInstance(Datetime.now().date(), Datetime.now().time()).getTime() - Datetime.newInstance(Datetime.now().dateGmt(), Datetime.now().timeGmt()).getTime()) / (60 * 60 * 1000);
            sw.timeZoneOffset = offset;
            sw.surveyToFilesJson = surveys.isEmpty() ? '' : getFiles(surveys);
            return sw;
        }
        public String getFiles(List<VTD1_Survey__c> surveys) {
            Map<Id, List<ContentDocumentLink>> surveyToFiles = new Map<Id, List<ContentDocumentLink>>();
            Set<Id> surveysIds = new Set<Id>();
            for (VTD1_Survey__c survey : surveys) {
                surveysIds.add(survey.Id);
            }
            List<ContentDocumentLink> links = getContentDocumentLinks(surveysIds);
            for (ContentDocumentLink cdl : links) {
                if (!surveyToFiles.containsKey(cdl.LinkedEntityId)) {
                    surveyToFiles.put(cdl.LinkedEntityId, new List<ContentDocumentLink>());
                }
                surveyToFiles.get(cdl.LinkedEntityId).add(cdl);
            }
            return JSON.serialize(surveyToFiles);
        }
        private List<Case> getCaseByCaseId(Id caseId) {
            return [
                    SELECT Id
                            , VTD1_Study__r.Name
                            , Contact.Name
                            , VTD1_Study__r.VTR5_SourceFormToolSelection__c
                    FROM Case
                    WHERE Id = :caseId
                    LIMIT 1
            ];
        }
        private List<ContentDocumentLink> getContentDocumentLinks(Set<Id> surveysIds) {
            return [
                    SELECT Id
                            , LinkedEntityId
                            , ContentDocumentId
                            , IsDeleted
                            , ContentDocument.Title
                            , ContentDocument.FileExtension
                    FROM ContentDocumentLink
                    WHERE LinkedEntityId IN :surveysIds
            ];
        }
        private Id getScrProfileId() {
            return [SELECT Id FROM Profile WHERE Name = :VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME][0].Id;
        }
    }
    public class SurveysWrapper {
        @AuraEnabled public List<VTD1_Survey__c> surveys;
        @AuraEnabled public Long timeZoneOffset;
        @AuraEnabled public Case caseRecord;
        @AuraEnabled public String surveyToFilesJson;
    }

}