global class VT_D1_ScheduleLockoutProcessor implements Schedulable{
    
    private Id jobId;
    private  static Integer runNextAfter = 600;
    private static final string nameSchedule = 'Lockout Processing';
    
    global void execute(SchedulableContext sc)
	{
		this.jobId = sc.getTriggerId();
		try
		{
			database.executeBatch(new VT_D1_BatchLockoutUsersProcessor(this), 100);
			
		}
		catch (Exception ex){system.debug('Scheduler Error = ' + ex.getMessage());}
	}
	
	public void scheduleFirstRun()
	{			
		if (!Test.isRunningTest() && runNextAfter > 0)
			this.scheduleJob(datetime.now().addMinutes(1), false);
	}
	
	//returns cron expression for use with schedule()
	public static string getCronExpressionForDateTime(datetime theDateTime)
	{
		return '0 '	+ string.valueOf(theDateTime.minute()) + ' ' 
					+ string.valueOf(theDateTime.hour()) + ' '
					+ string.valueOf(theDateTime.day()) + ' '
					+ string.valueOf(theDateTime.month()) + ' ? '
					+ string.valueOf(theDateTime.year()) + ' ';
	}
	
	private void scheduleJob(datetime theDateTime, boolean throwOnError)
	{
		string cron = getCronExpressionForDateTime(theDateTime);
		system.debug('!!! cron='+cron);

		try
		{
			//id cronid = System.schedule(SCHEDULE_NAME, '0 15 0-23 * * ?', new scheduledMaintenance()); 
			//settings.JobId__c = 
			
			system.schedule(nameSchedule, cron, new VT_D1_ScheduleLockoutProcessor());
		}
		catch (Exception ex)
		{
			
		}
		finally
		{
			//update Settings;
		}
	}
	
	global void callBackAsync()
    {
    	try
    	{
    		if (this.jobId != null)
	    		system.abortJob(this.jobId);
	    	if (runNextAfter > 0)
	    		this.scheduleJob(datetime.now().addseconds(runNextAfter), false);
    	}
    	catch(Exception ex){}
    }
	
}