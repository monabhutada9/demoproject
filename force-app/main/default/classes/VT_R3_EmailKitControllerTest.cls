@IsTest
public class VT_R3_EmailKitControllerTest {

    @TestSetup
    private static void setupMethod() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();

        VTD2_Study_Geography__c studyGeography = new VTD2_Study_Geography__c(VTD2_Study__c = study.Id, Name = 'Geo');
        insert studyGeography;

        VTR2_StudyPhoneNumber__c phoneNumber = new VTR2_StudyPhoneNumber__c(VTR2_Study__c = study.Id, VTR2_Study_Geography__c = studyGeography.Id, Name = '8888888888');
        insert phoneNumber;

        Case carePlan = [
                SELECT Id, VTR2_StudyPhoneNumber__c, VTD1_Patient_User__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
                ORDER BY CreatedDate DESC
                LIMIT 1
        ];

        carePlan.VTR2_StudyPhoneNumber__c = phoneNumber.Id;
        update carePlan;
    }


    @IsTest
    private static void  testBehavior() {

        Test.startTest();

        Case carePlan = [
                SELECT Id, VTD2_Study_Phone_Number__c, VTD1_Patient_User__c,VTD1_Study__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
                ORDER BY CreatedDate DESC
                LIMIT 1
        ];

        Contact contact = new Contact(FirstName = 'FirstName', LastName = 'Nam');
        insert contact;
        //VTD1_Order__c pd = new VTD1_Order__c(VTD1_Case__c = carePlan.Id,VTD2_Study_Phone_Number__c = pd.VTD2_Study_Phone_Number__c);

        VTD1_Protocol_Delivery__c pd1 = new VTD1_Protocol_Delivery__c(Name = 'testpd' ,Care_Plan_Template__c=carePlan.VTD1_Study__c,VTD1_Delivery_Order__c='01',VTD1_Shipment_Type__c ='Initial');
        insert pd1;

        VTD1_Order__c pd = new VTD1_Order__c(VTD1_Case__c = carePlan.Id ,VTD1_Protocol_Delivery__c = pd1.id);
        insert pd;

        VT_R3_EmailKitController controller = new VT_R3_EmailKitController();
        controller.conId = contact.Id;
        controller.pDId = pd.Id;
        controller.template = 'shipped';
        controller.language = 'en_US';

        System.assertEquals(contact.FirstName, controller.getFirstName());

        System.debug('pd.VTD2_Study_Phone_Number__c '+pd.VTD2_Study_Phone_Number__c);
        System.debug('controller.getStudyPhoneNumber() '+controller.getStudyPhoneNumber());
        //System.assertEquals(pd.VTD2_Study_Phone_Number__c, controller.getStudyPhoneNumber());
        controller.getStudyPhoneNumber();
        controller.getMessageBody();


        //System.assertEquals(contact.FirstName, controller.getMessageBody());
        //System.assertEquals(contact.FirstName, controller.getvisitStudyHub());
        //System.assertEquals(contact.FirstName, controller.getsincerely());
        //System.assertEquals(contact.FirstName, controller.getClosingMail());
        System.assertNotEquals(null, controller.getSalutation());
        controller.template = 'delivered';
        System.assertNotEquals(null, controller.getMessageBody());
        controller.template = 'readyshipped';
        System.assertNotEquals(null, controller.getMessageBody());
        controller.template = 'readyreturned';
        System.assertNotEquals(null, controller.getMessageBody());
        controller.template = 'return';
        System.assertNotEquals(null, controller.getMessageBody());
        controller.template = 'delayed';
        System.assertNotEquals(null, controller.getMessageBody());

        Test.stopTest();
    }
}