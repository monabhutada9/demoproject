public with sharing class VT_R5_ChatButtonAndSkillHandler {
    /* 
     * method : insertServiceResourceSkills(Map<Id,Study_Team_Member__c> mapSTMnew)
     * params : mapSTMnew - map of StudyTeamMembers those are getting added as SC to a study.
     * Description : This method is used to creating ServiceResourceSkills for Study & Languages to a SC user.
     */
    public static void insertServiceResourceSkills(Map<Id,Study_Team_Member__c> mapSTMnew) {
        // Get SC recordType Id for StudyTeamMembers
        Id scRecordTypeId = VT_D1_ConstantsHelper.RECORD_TYPE_ID_STUDY_TEAM_MEMBER_SC;
        Map<String, Skill> mapStudySkills = new Map<String, Skill>();
        Set<Id> setStudyIds = new Set<Id>();
        Set<Id> setUserIds = new Set<Id>();   
        Map<Id,Study_Team_Member__c> mapSCstm = new Map<Id,Study_Team_Member__c>();
        try {
            for(Study_Team_Member__c stm : mapSTMnew.values()) {
            if (stm.RecordTypeId == scRecordTypeId && stm.VTD1_Type__c.equalsIgnoreCase('Study Concierge')) {
                setStudyIds.add(stm.Study__c); 
                setUserIds.add(stm.User__c); 
                mapSCstm.put(stm.Id, stm);
            }
        }
        } catch (Exception ex) {
            System.debug('VT_R5_ChatButtonAndSkillHandler : Exception Occured.');
            System.debug(ex.getMessage());
        }
        if (!mapSCstm.isEmpty()) {
            Map<Id, User> mapSCUser = new Map<Id, User>([
                            SELECT Id, FirstName, VTD1_Full_Name__c
                            FROM User 
                            WHERE Id IN : setUserIds]);   
            Map<Id, ServiceResource> mapSCUserServiceResource = VT_R5_SkillAndChatButtonUtils.createServiceResourcesForUsers(mapSCUser);
            List<Skill> lstStudySkills = [
                            SELECT Id, DeveloperName 
                            FROM Skill 
                            WHERE DeveloperName IN : setStudyIds];   
            for (Skill sk : lstStudySkills) {
                mapStudySkills.put(sk.DeveloperName, sk);
            }
            Set<ServiceResourceSkill> setServiceResourceSkillsToBeAdded = new Set<ServiceResourceSkill>();
            for (Study_Team_Member__c stm : mapSCstm.values()) {
                if (mapStudySkills.containsKey(stm.Study__c) && mapSCUserServiceResource.containsKey(stm.User__c)) {
                    //If ServiceResource exist for User then create study & language ServiceResourceSkills for user.
                    Map<String,ServiceResourceSkill> lstSkillIds = new Map<String,ServiceResourceSkill>(); //todo- map
                    for (ServiceResourceSkill skl : mapSCUserServiceResource.get(stm.User__c).ServiceResourceSkills) {
                        lstSkillIds.put(skl.SkillId, skl);
                    }
                    if (lstSkillIds.get(mapStudySkills.get(stm.Study__c).Id) == null) {
                        // add Study Skill if study skill not exist.
                        ServiceResourceSkill tempServiceResourceSkill = VT_R5_SkillAndChatButtonUtils.createServiceResourceSkill(
                                                                mapStudySkills.get(stm.Study__c).Id,
                                                                mapSCUserServiceResource.get(stm.User__c).Id);
                        setServiceResourceSkillsToBeAdded.add(tempServiceResourceSkill);                                                                             
                    }                
                }
                else if (!mapStudySkills.containsKey(stm.Study__c)){
                    //If Study Skill & ServiceResource not exist for User then show error that Study Skill for Study is not exist.
                    //todo - create custom label for error msg
                    System.debug('Unable to add Study Team Member. Study Skill for Study : ' + stm.Study__c + ' is not setup yet. Please create Study Skill.');
                    // stm.addError('Unable to add Study Team Member. Study Skill for Study : ' + stm.Study__c + ' is not setup yet. Please create Study Skill.');
                } 
            }   
            if (!setServiceResourceSkillsToBeAdded.isEmpty()) {
                try {
                    List<ServiceResourceSkill> lstServiceResourceSkillsToBeAdded = new List<ServiceResourceSkill>(setServiceResourceSkillsToBeAdded);
                    Database.insert(lstServiceResourceSkillsToBeAdded);                
                    System.debug('Skill after insert :' + setServiceResourceSkillsToBeAdded);
                } catch (DmlException ex) {
                    System.debug('Error Message :' + ex.getMessage());
                }
            }
        }              
    }    
    /* 
     * method : updateServiceResourceSkills(Map<Id,Study_Team_Member__c> mapSTMold, Map<Id,Study_Team_Member__c> mapSTMnew)
     * params : mapSTMold - map of StudyTeamMembers those are getting removed as SC from a study.
     * params : mapSTMnew - map of StudyTeamMembers those are getting added as SC to a study.
     * Description : This method is used to update ServiceResourceSkills for Study (only) for a SC user.
     */
    public static void updateServiceResourceSkills(Map<Id,Study_Team_Member__c> mapSTMold, Map<Id,Study_Team_Member__c> mapSTMnew) {
        Id scRecordTypeId = VT_D1_ConstantsHelper.RECORD_TYPE_ID_STUDY_TEAM_MEMBER_SC;
        Map<Id, Study_Team_Member__c> mapNewSC = new Map<Id, Study_Team_Member__c>(); 
        Map<Id, Study_Team_Member__c> mapOldSC = new Map<Id, Study_Team_Member__c>();        
        for(Study_Team_Member__c oldStm : mapSTMold.values()) {
            if (oldStm.RecordTypeId == scRecordTypeId && oldStm.VTD1_Type__c.equalsIgnoreCase('Study Concierge')) {
                Study_Team_Member__c newStm = mapSTMnew.get(oldStm.Id);
                if (newStm.User__c != oldStm.User__c) {
                    mapOldSC.put(oldStm.Id, oldStm);
                    mapNewSC.put(newStm.Id, newStm);
                }
            }
        }
        if (!mapOldSC.isEmpty()) {
            VT_R5_ChatButtonAndSkillHandler.deleteServiceResourceSkills(mapOldSC);
        }
        if (!mapNewSC.isEmpty()) {
            VT_R5_ChatButtonAndSkillHandler.insertServiceResourceSkills(mapNewSC);
        }
    }
    /* 
     * method : deleteServiceResourceSkills(Map<Id,Study_Team_Member__c> mapSTMold)
     * params : mapSTMold - map of StudyTeamMembers those are getting removed as SC from a study.
     * Description : This method is used to delete ServiceResourceSkills for Study (only) from a SC user.
     */
    public static void deleteServiceResourceSkills(Map<Id,Study_Team_Member__c> mapSTMold) {
        Id scRecordTypeId = VT_D1_ConstantsHelper.RECORD_TYPE_ID_STUDY_TEAM_MEMBER_SC;
        Set<Id> setStudyIds = new Set<Id>(); 
        Set<Id> setUserIds = new Set<Id>();         
        Map<Id,Study_Team_Member__c> mapSCstm = new Map<Id,Study_Team_Member__c>();
        for(Study_Team_Member__c stm : mapSTMold.values()) {
            if (stm.RecordTypeId == scRecordTypeId && stm.VTD1_Type__c.equalsIgnoreCase('Study Concierge')) {
                setStudyIds.add(stm.Study__c);
                setUserIds.add(stm.User__c);  
                mapSCstm.put(stm.Id, stm);
            }
        }
        if (!mapSCstm.isEmpty()) {
            Map<Id, Skill> mapSkills = new Map<Id,Skill>([
                        SELECT Id, DeveloperName 
                        FROM Skill 
                        WHERE DeveloperName IN : setStudyIds]);
            List<ServiceResource> lstServiceResources = [
                        SELECT Id,RelatedRecordId, (SELECT Id, SkillId FROM ServiceResourceSkills) 
                        FROM ServiceResource 
                        WHERE RelatedRecordId IN:setUserIds];
            Map<Id, ServiceResource> mapServiceResources = new Map<Id, ServiceResource>();
            for (ServiceResource sr : lstServiceResources) {
                mapServiceResources.put(sr.RelatedRecordId, sr);
            }
            Set<ServiceResourceSkill> setServiceResourceSkillsToBeDeleted = new Set<ServiceResourceSkill>();
            Set<String> setSerResourceSkillIds = new Set<String>();
            for(Study_Team_Member__c stm : mapSCstm.values()) {
                if (mapServiceResources.get(stm.User__c) != null) {
                    for (ServiceResourceSkill skl : mapServiceResources.get(stm.User__c).ServiceResourceSkills) {
                        if(mapSkills.get(skl.SkillId) != null) {
                            if (mapSkills.get(skl.SkillId).DeveloperName.equalsIgnoreCase(stm.Study__c)) {
                                setServiceResourceSkillsToBeDeleted.add(skl);
                            }                        
                        }
                    }    
                }                               
            }  
            System.debug('SRS to be deleted : '+setServiceResourceSkillsToBeDeleted);
            if (!setServiceResourceSkillsToBeDeleted.isEmpty()) {
                List<ServiceResourceSkill> lstServiceResourceSkillsToBeDeleted = new List<ServiceResourceSkill>(setServiceResourceSkillsToBeDeleted);
                Database.delete(lstServiceResourceSkillsToBeDeleted);
            }               
        }
    }
}