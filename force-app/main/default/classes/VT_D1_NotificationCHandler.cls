/**
 * Refactored by Yulia Yakushenkova on 15.06.2020.
 */

public with sharing class VT_D1_NotificationCHandler {

    public static Set<String> excludedToDuplicate;
    public static Map<Id, String> notificationsIdToVideoSession;
    public static Map<Id, Id> notificationToActualVisit = new Map<Id, Id>();
    private static final Set<String> VALID_LINKS_TO_OBJECTS_FOR_PATIENT = new Set<String>{
            'calendar',
            'my-diary',
            'full-profile',
            'my-study-supplies',
            'medical-records'
    };

    private static String iosAPIName;
    private static List<String> apiNames;

    public static void sendPushNotificationsToMobileDevices(List<VTD1_NotificationC__c> notifications) {
        if (notifications == null || notifications.isEmpty()) return;
        try {
            Boolean isVideoAvailable = isVideoConferenceAvailable(notifications);
            apiNames = getAppApiNames();
            for (VTD1_NotificationC__c notification : notifications) {
                Map<String, Object> pushData = getPushData(notification, isVideoAvailable);
                Set<String> users = new Set<String>{
                        notification.VTD1_Receivers__c
                };
                for (String apiName : apiNames) {
                    if (apiName.equals(iosAPIName)) pushData = getIOSPushData(pushData);
                    sendPushNotification(pushData, apiName, users);
                }
            }
        } catch (Exception e) {
            System.debug('Error: ' + e.getMessage() + ' ' + e.getStackTraceString());
        }
    }

    private static Map<String, Object> getPushData(VTD1_NotificationC__c notification, Boolean isVideoAvailable) {
        String sessionId = '', visitId = '', relatedObject = '';
        if (isVideoAvailable && notificationsIdToVideoSession.containsKey(notification.Id)) {
            sessionId = notificationsIdToVideoSession.get(notification.Id);
        }

        if (notificationToActualVisit.containsKey(notification.Id)) {
            visitId = notificationToActualVisit.get(notification.Id);
        }

        String pushMessage = notification.Message__c;

        if (String.isNotBlank(notification.Type__c)) {
            if (notification.Type__c.equals('Messages')) {
                if (notification.Number_of_unread_messages__c != null) {
                    pushMessage += ' ' + String.valueOf(Integer.valueOf(notification.Number_of_unread_messages__c));
                }
                relatedObject = 'messages';
            }
            if (notification.Type__c.equals('General')) {
                if (VALID_LINKS_TO_OBJECTS_FOR_PATIENT.contains(notification.Link_to_related_event_or_object__c)) {
                    relatedObject = notification.Link_to_related_event_or_object__c;
                }
            }
        }

        String badgeCount = String.valueOf(getCountUnreadNotifications(notification.VTD1_Receivers__c));

        Map<String, String> resultMap = new Map<String, String>{
                'notificationId' => notification.Id,
                'userId' => notification.VTD1_Receivers__c,
                'sessionId' => sessionId,
                'visitId' => visitId,
                'relatedObject' => relatedObject,
                'message' => pushMessage
//                ,                'badge' => badgeCount
        };
        if (notification.VTR3_Notification_Type__c != null && notification.VTR3_Notification_Type__c.equals('eCOA Notification')) {
            resultMap.put('successBannerNeed', 'true');
            resultMap.put('successBannerText', notification.Title__c);
            resultMap.put('relatedObject', 'my-diary');
        }

        return resultMap;
    }

    private static Map<String, Object> getIOSPushData(Map<String, Object> pushData) {
        return Messaging.PushNotificationPayload.apple(
                (String) pushData.get('message'),
                '',
                null,
                pushData
        );
    }

    private static List<String> getAppApiNames() {
        List<String> apiNames = new List<String>();
        List<VTD1_General_Settings__mdt> generalSettingsList = [
                SELECT
                        VTD1_Connected_App_API_Name_for_Android__c,
                        VTR3_Connected_App_API_Name_for_iOS__c
                FROM VTD1_General_Settings__mdt
        ];
        if (!generalSettingsList.isEmpty()) {
            String appAPINameIOS = generalSettingsList[0].VTR3_Connected_App_API_Name_for_iOS__c;
            if (String.isNotBlank(appAPINameIOS)) {
                apiNames.add(appAPINameIOS);
                iosAPIName = appAPINameIOS;
            }
//            String appAPINameAndroid = generalSettingsList[0].VTD1_Connected_App_API_Name_for_Android__c;
//            if (String.isNotBlank(appAPINameAndroid)) apiNames.add(appAPINameAndroid);
        }
        return apiNames;
    }

    private static void sendPushNotification(Map<String, Object> pushData, String appAPIName, Set<String> users) {
        Messaging.PushNotification pushNotification = new Messaging.PushNotification();
        pushNotification.setPayload(pushData);
        pushNotification.send(appAPIName, users);
        System.debug('pushNotifications for ' + appAPIName + ' : ' + JSON.serializePretty(pushNotification));
    }

    private static Integer getCountUnreadNotifications(Id userId) {
        Id caseId = [
                SELECT Contact.VTD1_Clinical_Study_Membership__c
                FROM User
                WHERE Id = :UserInfo.getUserId()
        ].Contact.VTD1_Clinical_Study_Membership__c;

        Integer counter = [
                SELECT COUNT()
                FROM VTD1_NotificationC__c
                WHERE VTR4_CurrentPatientCase__c = :caseId
                AND ((Type__c = 'Messages' AND Number_of_unread_messages__c > 0) OR (Type__c != 'Messages'))
                AND Type__c != 'Televisit'
                AND (OwnerId = :UserInfo.getUserId() OR VTD1_Receivers__c = :UserInfo.getUserId())
                AND VTD1_Read__c = FALSE
        ];
        return counter;
    }

    private static Boolean isVideoConferenceAvailable(List<VTD1_NotificationC__c> notifications) {
        Map<Id, Id> actualVisitToNotification = new Map<Id, Id>();
        for (VTD1_NotificationC__c notification : notifications) {
            if (String.isNotBlank(notification.VTD2_VisitID__c)) {
                actualVisitToNotification.put(notification.VTD2_VisitID__c, notification.Id);
            }
            notificationToActualVisit.put(notification.Id, notification.VTD2_VisitID__c);
        }
        List<Video_Conference__c> videoConferences = [
                SELECT Id, SessionId__c, VTD1_Actual_Visit__c
                FROM Video_Conference__c
                WHERE VTD1_Actual_Visit__c IN :actualVisitToNotification.keySet()
        ];
        Map<Id, String> notificationIdToVideoSessionId = new Map<Id, String>();
        for (Video_Conference__c vc : videoConferences) {
            if (String.isNotBlank(vc.SessionId__c)) {
                notificationIdToVideoSessionId.put(
                        actualVisitToNotification.get(vc.VTD1_Actual_Visit__c), vc.SessionId__c
                );
            }
        }
        if (notificationIdToVideoSessionId.isEmpty()) {
            return false;
        } else {
            notificationsIdToVideoSession = notificationIdToVideoSessionId;
            return true;
        }
    }


}