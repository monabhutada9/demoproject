public without sharing class VT_D1_PcfMembersFromMentionCreator {

    public static final List<String> EXTERNAL_PROFILES = new List<String>{
            'Primary Investigator',
            'Patient',
            'Caregiver',
            'Site Coordinator'
    };

    private List<VT_D1_Mention> caseMentions = new List<VT_D1_Mention>();
    private List<VT_D1_Mention> pcfMentions = new List<VT_D1_Mention>();
    private List<Id> userIds = new List<Id>();
    private List<Id> caseIds = new List<Id>();
    private Set<Id> validUserIds = new Set<Id>();
    private Map<Id, Set<Id>> pcfToExistingMemberIds = new Map<Id, Set<Id>>();

    public void addMembersFromMentions(List<VT_D1_Mention> mentions) {
        this.caseMentions = VT_D1_MentionHelper.getMentionsByParentType(mentions).get('Case');

        if (this.caseMentions != null && !this.caseMentions.isEmpty()) {
            getUserAndCaseIds();
        }

        if (!this.caseIds.isEmpty()) {
            getPcfToExistingMemberIds();
            getValidUserIds();
            getPcfMentions();

            createNewMembers();
        }
    }

    private void getUserAndCaseIds() {
        for (VT_D1_Mention item : this.caseMentions) {
            system.debug(item.getUserIds());
            System.debug('here1');
            if (!item.getUserIds().isEmpty()) {
                this.userIds.addAll(item.getUserIds());
                this.caseIds.add(item.ParentId);
            }
        }
    }

    private void getPcfToExistingMemberIds() {
        for (Case pcfCase : [
                SELECT Id, (SELECT VTD1_User_Id__c FROM PCF_Chat_Members__r WHERE VTD1_User_Id__c IN :this.userIds)
                FROM Case
                WHERE Id IN :this.caseIds
                AND RecordType.Id IN :VT_D1_HelperClass.getRecordTypeContactForm()
        ]) {
            Set<Id> existingMemberIds = new Set<Id>();
            for (VTD1_PCF_Chat_Member__c member : pcfCase.PCF_Chat_Members__r) {
                existingMemberIds.add(member.VTD1_User_Id__c);
            }
            this.pcfToExistingMemberIds.put(pcfCase.Id, existingMemberIds);
            System.debug('pcfToExistingMemberIds ' + this.pcfToExistingMemberIds);
        }
    }

    private void getValidUserIds() {
        System.debug('here2');
        this.validUserIds = new Map<Id, User>([
                SELECT Id
                FROM User
                WHERE Id IN :userIds AND Profile.Name IN :EXTERNAL_PROFILES
        ]).keySet();
    }

    private void getPcfMentions() {
        System.debug('here3');
        System.debug('caseMentions ' + this.caseMentions);
        System.debug('pcfToExistingMemberIds ' + this.pcfToExistingMemberIds);
        for (VT_D1_Mention item : this.caseMentions) {
            if (this.pcfToExistingMemberIds.containsKey(item.ParentId)) {
                this.pcfMentions.add(item);
            }
        }
        System.debug('pcfMentions ' + this.pcfMentions);
    }

    private void createNewMembers() {
        System.debug('finally here');
        List<VTD1_PCF_Chat_Member__c> newMembers = new List<VTD1_PCF_Chat_Member__c>();

        for (VT_D1_Mention mention : this.pcfMentions) {
            for (Id uId : mention.getUserIds()) {
                if (this.validUserIds.contains(uId) && !this.pcfToExistingMemberIds.get(mention.ParentId).contains(uId)) {
                    newMembers.add(new VTD1_PCF_Chat_Member__c(
                            VTD1_PCF__c = mention.ParentId,
                            VTD1_User_Id__c = uId
                    ));
                }
            }
        }

        if (!newMembers.isEmpty()) {
            insert newMembers;
        }
    }
}