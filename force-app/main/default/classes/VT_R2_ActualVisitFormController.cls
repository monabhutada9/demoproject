public without sharing class VT_R2_ActualVisitFormController {
    @AuraEnabled
    public static String getActualVisit(Id actualVisitId) {
        try {
            VTD1_Actual_Visit__c av;
            if (actualVisitId != null && Schema.VTD1_Actual_Visit__c.getSObjectType() == actualVisitId.getSobjectType()) {
                List<VTD1_Actual_Visit__c> avList = [
                        SELECT
                                Name,
                                VTD1_Protocol_Visit__c,
                                Unscheduled_Visits__c,
                                VTD1_Case__c,
                                VTD1_Status__c,
                                toLabel(VTD1_Status__c) statusLabel,
                                VTD1_Visit_Type__c,
                                VTD1_Unscheduled_Visit_Type__c,
                                VTD1_Unscheduled_Visit_Duration__c,
                                VTD1_Scheduled_Date_Time__c,
                                Sub_Type__c,
                                VTR2_Modality__c,
                                RecordTypeId,
                                RecordType.Name,
                                RecordType.DeveloperName,
                                VTR2_Visit_location__c,
                                VTD1_Case__r.Preferred_Lab_Visit__c,
                                Unscheduled_Visits__r.Preferred_Lab_Visit__c,
                                VTD1_Additional_Patient_Visit_Checklist__c,
                                VTD1_Additional_Visit_Checklist__c,
                                VTR2_Independent_Rater_Flag__c,
                                VTD1_Protocol_Visit__r.RecordType.DeveloperName,
                                VTD1_Reason_for_Cancellation__c,
                                Unscheduled_Visits__r.VTR3_VisitAvailableForCRA__c, //SH-9842
                                VTD1_Case__r.VTR3_VisitAvailableForCRA__c
                        FROM VTD1_Actual_Visit__c
                        WHERE Id = :actualVisitId
                ];
                if (!avList.isEmpty()) {
                    av = avList[0];
                }
            }
            return JSON.serialize(av);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled(Cacheable=true)
    public static String getRecordTypes() {
        try {
            return JSON.serialize(VTD1_Actual_Visit__c.SObjectType.getDescribe().getRecordTypeInfos());
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled(Cacheable=true)
    public static String getPreferredSubType(Id caseId) {
        String result;
        try {
            List<Case> cases = [SELECT Id, Preferred_Lab_Visit__c FROM Case WHERE Id = :caseId];
            if (!cases.isEmpty()) {
                result = cases[0].Preferred_Lab_Visit__c;
            }
            return result;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled(Cacheable=true)
    public static String getPatientPhone(Id caseId) {
        String phoneNumber;
        List<VT_D1_Phone__c> phones = [SELECT PhoneNumber__c FROM VT_D1_Phone__c WHERE VTD1_Contact__c IN (SELECT ContactId FROM Case WHERE Id = :caseId) ORDER BY IsPrimaryForPhone__c];
        if (!phones.isEmpty()) {
            phoneNumber = phones[0].PhoneNumber__c;
        }
        return phoneNumber;
    }

    @AuraEnabled(Cacheable=true)
    public static String getCurrentUserProfile() {
        try {
            return [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    public class InputSelectOption {
        @AuraEnabled public String value;
        @AuraEnabled public String label;
        public InputSelectOption(String value, String label) {
            this.value = value;
            this.label = label;
        }
    }

    @AuraEnabled(Cacheable=true)
    public static List<InputSelectOption> getInputSelectOptions(String sObjName, String fieldName) {
        List<InputSelectOption> options = new List<InputSelectOption>();
        if (!String.isEmpty(sObjName) && !String.isEmpty(fieldName)) {
            Schema.SObjectType objType = Schema.getGlobalDescribe().get(sObjName);
            SObject sObj = objType.newSObject();
            Map<String, String> optionsMap = VT_D1_CustomMultiPicklistController.getSelectOptionsWithLabel(sObj, fieldName);
            for (String value : optionsMap.keySet()) {
                InputSelectOption option = new InputSelectOption(value, optionsMap.get(value));
                options.add(option);
            }
        }
        return options;
    }

    @AuraEnabled
    public static String createOrUpdateEventRemote(String eventDateTime, Boolean isReschedule, Id actualVisitId,
            Decimal duration, String visitType, Boolean isUnscheduledVisit, Boolean deletePI) {
        try {
            Map<String, Integer> eventDateTimeMap = new Map<String, Integer>();
            eventDateTimeMap = (Map<String, Integer>) JSON.deserialize(eventDateTime, Map<String, Integer>.class);
            Datetime eventDateT = Datetime.newInstance(
                    eventDateTimeMap.get('Year'),
                    eventDateTimeMap.get('Month'),
                    eventDateTimeMap.get('Date'),
                    eventDateTimeMap.get('Hour'),
                    eventDateTimeMap.get('Minute'),
                    eventDateTimeMap.get('Second')
            );
//            Datetime eventDateT = Datetime.parse(eventDateTime);
            return VT_D1_ActualVisitsHelper.createUpdateEvent(eventDateT,
                    isReschedule, actualVisitId, duration, visitType, isUnscheduledVisit, deletePI);
        } catch (Exception e) {
            if (VT_D1_ActualVisitTriggerHandler.currentException != null) {
                e = VT_D1_ActualVisitTriggerHandler.currentException;
            }
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled(Cacheable = true)
    public static String getDatesTimesMap(String membersString) {
        if (membersString != null) {
            return VT_R2_ActualVisitParticipantsController.updateDatesTimesMap(membersString, null, null);
        } else {
            return Date.today().format();
        }
    }

    @AuraEnabled(Cacheable = true)
    public static String getTimesMap(String visitId, String membersString, String selectedDate) {
        return VT_R2_ActualVisitParticipantsController.updateDatesTimesMap(membersString, visitId, selectedDate);
    }
    // Optimized save all in one transaction
    /*@AuraEnabled
    public static void saveVisitAndParticipants(String visitJSON, String participantsJSON, List<Id> existingVisitParticipantIdList,String eventDateTime) {
        try {
            VTD1_Actual_Visit__c visit = (VTD1_Actual_Visit__c) JSON.deserialize(visitJSON, VTD1_Actual_Visit__c.class);
            upsert visit;
            VT_R2_ActualVisitParticipantsController.createVisitMembers(visit.Id, participantsJSON, existingVisitParticipantIdList);
            if (eventDateTime != null) {
                VT_D1_ChooseTimeOnTaskContollerRemote.createOrUpdateEventRemote(eventDateTime, visit.VTD1_Status__c == 'To Be Rescheduled',
                    visit.Id, visit.VTD1_Visit_Duration__c, visit.VTD1_Unscheduled_Visit_Type__c, true, false);
            }
        } catch (Exception e) {
            if (VT_D1_ActualVisitTriggerHandler.currentException != null) {
                e = VT_D1_ActualVisitTriggerHandler.currentException;
            }
            throw new AuraHandledException(e.getMessage());
        }
    }*/
}