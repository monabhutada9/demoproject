public without sharing class VT_R2_StudyTeamController {

    public static final String ALL_MY_STUDIES = 'All My Studies';
    public static final Map<String, Id> RECORD_TYPE_ID_BY_NAME = new Map<String, Id>{
            'PI' => Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByName().get('PI').getRecordTypeId()
            , 'PG' => Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByName().get('PG').getRecordTypeId()
            , 'SCR' => Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByName().get('SCR').getRecordTypeId()
            , 'SC' => Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByName().get('SC').getRecordTypeId()
            , 'CM' => Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByName().get('CM').getRecordTypeId()
            , 'CRA' => Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByName().get('CRA').getRecordTypeId()
    };

    @AuraEnabled(Cacheable = true)
    public static List<User> getStudyTeamMembers(String memberType, String studyName) {
        Set<Id> userIds = new Set<Id>();
        if (memberType == 'PL') {
            userIds = getUserIdsFromStms(studyName);
        } else {
            userIds = getUserIdsFromStmsByStudies(memberType, studyName);
        }
        return getUsersByIds(userIds);
    }

    @AuraEnabled(Cacheable = true)
    public static List<User> getStudyTeamMembersBySite(String memberType, String studyName) {
        Set<Id> userIds = new Set<Id>();
        Set<Id> virtualSiteIds = getVirtualSiteIds(studyName);
        if (memberType == 'PI') {
            for (Study_Team_Member__c stm : [
                    SELECT
                            User__c, (
                            SELECT
                                    User__c
                            FROM Study_Team_Members__r
                    ), (
                            SELECT
                                    VTR2_Associated_SubI__r.User__c
                            FROM Study_Site_Team_Members2__r
                    )
                    FROM Study_Team_Member__c
                    WHERE VTD1_VirtualSite__c IN :virtualSiteIds
            ]) {
                userIds.add(stm.User__c);
                for (Study_Team_Member__c backupPI : stm.Study_Team_Members__r) {
                    userIds.add(backupPI.User__c);
                }
                for (Study_Site_Team_Member__c subPI : stm.Study_Site_Team_Members2__r) {
                    userIds.add(subPI.VTR2_Associated_SubI__r.User__c);
                }
            }
        } else if (memberType == 'PG') {
            for (Study_Team_Member__c stm : [
                    SELECT
                            Id, (
                            SELECT
                                    VTD1_Associated_PG__r.User__c
                            FROM Study_Team_Member_Relationships1__r
                    )
                    FROM Study_Team_Member__c
                    WHERE VTD1_VirtualSite__c IN :virtualSiteIds
            ]) {
                for (Study_Site_Team_Member__c PG : stm.Study_Team_Member_Relationships1__r) {
                    userIds.add(PG.VTD1_Associated_PG__r.User__c);
                }
            }
        } else if (memberType == 'SCR') {
            for (Study_Team_Member__c stm : [
                    SELECT
                            Id, (
                            SELECT
                                    VTR2_Associated_SCr__r.User__c
                            FROM Study_Site_Team_Members1__r
                    )
                    FROM Study_Team_Member__c
                    WHERE VTD1_VirtualSite__c IN :virtualSiteIds
            ]) {
                for (Study_Site_Team_Member__c SCR : stm.Study_Site_Team_Members1__r) {
                    userIds.add(SCR.VTR2_Associated_SCr__r.User__c);
                }
            }
        }
        return getUsersByIds(userIds);
    }

    @TestVisible
    private static Set<Id> getVirtualSiteIds(String studyName) {
        Set<Id> virtualSiteIds = new Set<Id>();
        Set<Id> subISTMIds = new Set<Id>();
        Set<Id> scrSTMIds = new Set<Id>();

        for (Study_Team_Member__c stm : getSTMs(studyName)) {
            // Identify current user role/roles
            if (stm.VTD1_Type__c == 'Primary Investigator') {
                if (stm.VTD1_Backup_PI__c) {
                    // situation for Backup PI
                    virtualSiteIds.add(stm.Study_Team_Member__r.VTD1_VirtualSite__c);
                } else if (stm.VTD1_VirtualSite__c != null) {
                    // situation for Primary PI
                    virtualSiteIds.add(stm.VTD1_VirtualSite__c);
                } else {
                    // situation for Sub Investigator
                    subISTMIds.add(stm.Id);
                }
            } else if (stm.VTD1_Type__c == 'Site Coordinator') {
                // situation for SCR
                scrSTMIds.add(stm.Id);
            }
        }
        // found Virtual Site for SubI
        if (!subISTMIds.isEmpty()) {
            for (Study_Site_Team_Member__c sstm : [
                    SELECT VTR2_Associated_PI3__r.VTD1_VirtualSite__c
                    FROM Study_Site_Team_Member__c
                    WHERE VTR2_Associated_SubI__c IN (
                            SELECT Id
                            FROM Study_Team_Member__c
                            WHERE Id IN :subISTMIds)
            ]) {
                virtualSiteIds.add(sstm.VTR2_Associated_PI3__r.VTD1_VirtualSite__c);
            }
        }
        // found Virtual Site for SCR
        if (!scrSTMIds.isEmpty()) {
            for (Study_Site_Team_Member__c sstm : [
                    SELECT VTR2_Associated_PI__r.VTD1_VirtualSite__c
                    FROM Study_Site_Team_Member__c
                    WHERE VTR2_Associated_SCr__c IN (
                            SELECT Id
                            FROM Study_Team_Member__c
                            WHERE Id IN :scrSTMIds)
            ]) {
                virtualSiteIds.add(sstm.VTR2_Associated_PI__r.VTD1_VirtualSite__c);
            }
        }
        return virtualSiteIds;
    }

    @AuraEnabled
    public static Id getFeedItem(User user) {
        List<VT_D1_CommunityChat.PostObj> posts = VT_D1_CommunityChat.getPatientPosts();
        String mention = '@' + user.Name;
        for (VT_D1_CommunityChat.PostObj post : posts) {
            if (post.bodyText.contains(mention)) {
                return post.id;
            }
        }
        return Id.valueOf(VT_D1_CommunityChatHelper.createPost('', user.Id, null, ''));
    }

    private static Set<Id> getUserIdsFromStmsByStudies(String memberType, String studyName) {
        Set<Id> studyIds = getStmIds(studyName);
        return getUserIds(studyIds, memberType, studyName);
    }

    private static Set<Id> getUserIds(Set<Id> studyIds, String memberType, String studyName) {
        Set<Id> userIds = new Set<Id>();
        if (ALL_MY_STUDIES == studyName) {
            for (Study_Team_Member__c stm : getStmsByStudiesAndRecordTypeId(studyIds, memberType)) {
                userIds.add(stm.User__c);
            }
        } else {
            for (Study_Team_Member__c stm : getStmsByStudiesAndRecordTypeIdAndStudyName(studyIds, memberType, studyName)) {
                userIds.add(stm.User__c);
            }
        }
        return userIds;
    }

    private static Set<Id> getStmIds(String studyName) {
        Set<Id> studyIds = new Set<Id>();
        if (ALL_MY_STUDIES == studyName) {
            for (Study_Team_Member__c stm : getStmsByUserId()) {
                studyIds.add(stm.Study__c);
            }
        } else {
            for (Study_Team_Member__c stm : getStmsByUserIdAndStudyName(studyName)) {
                studyIds.add(stm.Study__c);
            }
        }
        return studyIds;
    }

    private static Set<Id> getUserIdsFromStms(String studyName) {
        Set<Id> userIds = new Set<Id>();
        if (ALL_MY_STUDIES == studyName) {
            for (Study_Team_Member__c stm : getStmsByUserId()) {
                userIds.add(stm.Study__r.VTD1_Project_Lead__c);
            }
        } else {
            for (Study_Team_Member__c stm : getStmsByUserIdAndStudyName(studyName)) {
                userIds.add(stm.Study__r.VTD1_Project_Lead__c);
            }
        }
        return userIds;
    }

    private static List<User> getUsersByIds(Set<Id> userIds) {
        // needed QueryBuilder because referencing FullPhotoUrl cause error on component
        return (List<User>) new QueryBuilder(User.class)
                .addFields('Id, Name, FullPhotoUrl, Phone, Email')
                .addConditions()
                .add(new QueryBuilder.InCondition(User.Id).inCollection(userIds))
                .endConditions()
                .toList();
    }

    private static List<Study_Team_Member__c> getStmsByStudiesAndRecordTypeId(Set<Id> stmIds, String memberType) {
        return [
                SELECT User__c
                FROM Study_Team_Member__c
                WHERE Study__c IN :stmIds
                AND RecordTypeId = :RECORD_TYPE_ID_BY_NAME.get(memberType)
        ];
    }

    private static List<Study_Team_Member__c> getStmsByStudiesAndRecordTypeIdAndStudyName(Set<Id> studyIds, String memberType, String studyName) {
        return [
                SELECT User__c
                FROM Study_Team_Member__c
                WHERE Study__c IN :studyIds
                AND RecordTypeId = :RECORD_TYPE_ID_BY_NAME.get(memberType)
                AND Study__r.Name = :studyName
        ];
    }

    private static List<Study_Team_Member__c> getStmsByUserIdAndStudyName(String studyName) {
        return [
                SELECT
                        Study__c
                        , Study__r.VTD1_Project_Lead__c
                        , VTD1_Type__c
                        , VTD1_VirtualSite__c
                        , VTD1_Backup_PI__c
                        , Study_Team_Member__r.VTD1_VirtualSite__c
                FROM Study_Team_Member__c
                WHERE User__c = :UserInfo.getUserId()
                AND Study__r.Name = :studyName
        ];
    }

    private static List<Study_Team_Member__c> getStmsByUserId() {
        return [
                SELECT
                        Study__c
                        , Study__r.VTD1_Project_Lead__c
                        , VTD1_Type__c
                        , VTD1_VirtualSite__c
                        , VTD1_Backup_PI__c
                        , Study_Team_Member__r.VTD1_VirtualSite__c
                FROM Study_Team_Member__c
                WHERE User__c = :UserInfo.getUserId()
        ];
    }

    private static List<Study_Team_Member__c> getSTMs(String studyName) {
        if (ALL_MY_STUDIES == studyName) {
            return getStmsByUserId();
        } else {
            return getStmsByUserIdAndStudyName(studyName);
        }
    }
}