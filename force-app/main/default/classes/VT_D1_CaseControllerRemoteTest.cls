/**
 * Created by User on 19/05/15.
 */
@isTest
public without sharing class VT_D1_CaseControllerRemoteTest {

    public static void subjectRemoteTest(){
        Case cas = [SELECT Id FROM Case WHERE Subject = 'CaseControllerRemoteTest' LIMIT 1];
        Test.startTest();
        System.assertNotEquals(null,VT_D1_CaseControllerRemote.subjectRemote(cas.Id));
        Test.stopTest();
    }

    public static void randomizeRemoteTest(){
        Case cas = [SELECT Id FROM Case WHERE Subject = 'CaseControllerRemoteTest' LIMIT 1];
        Test.startTest();
        System.assertNotEquals(null, VT_D1_CaseControllerRemote.randomizeRemote(cas.Id));
        Test.stopTest();
    }

    public static void getCaseInfoTest(){
        Case cas = [SELECT Id FROM Case WHERE Subject = 'CaseControllerRemoteTest' LIMIT 1];
        Test.startTest();
        System.assertNotEquals(null, VT_D1_CaseControllerRemote.getCaseInfo(cas.Id));
        Test.stopTest();
    }

    public static void getLogReportIdTest(){
        Test.startTest();
        VT_D1_CaseControllerRemote.getLogReportId();
        Test.stopTest();
    }

    public static void isUserSystemAdminTest(){
        List<User> users = [SELECT Id FROM User WHERE Id=:UserInfo.getUserId()];
        Test.startTest();
        System.runAs(users[0]) {
            VT_D1_CaseControllerRemote.isUserSystemAdmin();
        }
        Test.stopTest();
    }
}