/**
 * Created by shume on 6/16/2020.
 */

public with sharing class VT_R5_ContactCaregiverHandler extends Handler {
    public override void  onBeforeInsert(TriggerContext context) {
        List<Id> listIds = new List<Id>();
        for (Contact c : (List<Contact>)context.newList) {
            if (c.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_CAREGIVER) {
                if (c.AccountId != null) {
                    /* start : PB Populate Account field on Caregiver Contacts with the Patient Account */
                    c.VTD1_Account__c = c.AccountId;
                    /* end : PB Populate Account field on Caregiver Contacts with the Patient Account */
                    if (!c.VTD1_FullHomePage__c && c.VTD1_Primary_CG__c) {
                        listIds.add(c.AccountId);
                    }
                }
            }
        }
        if (!listIds.isEmpty()) {
            updateFHSonCG(context, listIds);
        }
    }

    public override void  onBeforeUpdate(TriggerContext context) {
        List<Id> listIds = new List<Id>();
        for (Contact c : (List<Contact>)context.newList) {
            if (c.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_CAREGIVER) {
                if (c.VTD1_Primary_CG__c && !((Contact) context.oldMap.get(c.Id)).VTD1_Primary_CG__c && c.AccountId != null && !c.VTD1_FullHomePage__c) {
                   listIds.add(c.AccountId);
                }
             }
        }
        if (!listIds.isEmpty()) {
            updateFHSonCG(context, listIds);
        }
    }

    /*
    PB Community Home UI switch for CG
    Flow Contact update FHS on CG
     */
    private void updateFHSonCG (TriggerContext context, List<Id> listIds) {
        Map<Id, Account> accountMap =
                new Map<Id, Account> ([SELECT Id,
                (SELECT VTD1_FullHomePage__c
                FROM Contacts
                WHERE RecordTypeId=:VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PATIENT
                AND VTD1_FullHomePage__c = TRUE
                )
                FROM Account
                WHERE Id IN :listIds]);
        for (Contact c : (List<Contact>)context.newList) {
            if (accountMap.get(c.AccountId) != null && !accountMap.get(c.AccountId).Contacts.isEmpty()) {
                c.VTD1_FullHomePage__c = true;
            }
        }
    }
}