/**
 * @author: Aliaksandr Vabishchevich
 * @date: 06-Aug-2020
 * @description: Scheduled jobs for processing Signant Files records and updating Case records
 *
 * For start processing of queue execute code:
 *
 * VT_R5_SignantFilesProcessingBatch.run();
 */
public class VT_R5_SignantFilesProcessingBatch implements Database.Batchable<SObject>, Database.AllowsCallouts {
    @TestVisible
    private static String cronId;
    public static final Integer REPEAT_INTERVAL_MINUTES = 2;
    public static final Integer SCOPE_SIZE = 2;
    private static final String TASK_OPEN_STATUS = 'Open';
    private static final String TASK_CATEGORY_FOLLOW_UP = 'Follow Up Required';

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
                SELECT Id,
                        Name,
                        VTR5_XMLContent__c,
                        VTR5_StudyHubSubjNum__c,
                        VTR5_IsError__c,
                        VTR5_ScrnNum__c,
                        VTR5_ConsDate__c,
                        VTR5_SubjImmuno__c,
                        VTR5_SubjSafety__c,
                        VTR5_ErrorType__c,
                        VTR5_Day1Dose__c,
                        VTR5_DateProcessed__c,
                        VTR5_IsProcessed__c,
                        VTR5_ErrorDetails__c,
                        VTR5_RandDate__c,
                        VTR5_IsThisARescreening__c,
                        VTR5_PrevScrnNumber__c,
                        VTR5_StudyHubSubjNum_Correction__c,
                        VTR5_ConsDate_Correction__c,
                        VTR5_Day1Dose_Correction__c,
                        VTR5_DateReceived__c,
                        VTR5_RelatedCase__c,
                        VTR5_Day57Dose__c,
                        VTR5_Day57Dose_Correction__c,
                        VTR5_IsRescreeningXML__c,
                        VTR5_IsCorrectionXML__c,
                        VTR5_XMLDate__c
                FROM VTR5_Signant_File__c
                WHERE VTR5_IsProcessed__c = FALSE
        ]);
    }

    public void execute(Database.BatchableContext bc, List<VTR5_Signant_File__c> scope) {
        List<Id> signantFileIds = new List<Id>();
        for (VTR5_Signant_File__c sf : scope) {
            signantFileIds.add(sf.Id);
        }
        System.debug('sf: SF ids to process: ' + String.join(signantFileIds, ', '));

        Map<String, Id> studyIdByIrtValue = new Map<String, Id>();
        List<VTR5_IRTConfiguration__c> irtConfigs = [SELECT VTR5_Study__c,VTR5_Value__c FROM VTR5_IRTConfiguration__c];

        for (VTR5_IRTConfiguration__c irtConf : irtConfigs) {
            studyIdByIrtValue.put(irtConf.VTR5_Value__c, irtConf.VTR5_Study__c);
        }

        parseSignantFiles(scope, studyIdByIrtValue);
        processCases(scope);
        createSignantFilesCopy(scope, studyIdByIrtValue);
    }

    private void parseSignantFiles(List<VTR5_Signant_File__c> sgFiles, Map<String, Id> studyIdByIrtValue) {
        Pattern pRegexp = Pattern.compile('Signant\\.[^.]+\\.(.+)\\.xml');

        for (VTR5_Signant_File__c sf : sgFiles) {
            try {

                DOM.Document doc = new DOM.Document();
                try {
                    doc.load(sf.VTR5_XMLContent__c);
                } catch (Exception e) {
                    throw new WrongFormatException('Wrong XML content');
                }

                DOM.XmlNode node = doc.getRootElement();
                if (node.getName().toLowerCase() != 'subject') {
                    throw new WrongFormatException('Wrong XML content');
                }

                sf.VTR5_StudyHubSubjNum__c = getSubNodeContentText(node, 'StudyHubSubjNum');

                Matcher regexMatcher = pRegexp.matcher(sf.Name);
                if (!regexMatcher.matches()) {
                    throw new WrongFormatException('Wrong File Name: ' + sf.Name);
                }
                String datetimeStr = regexMatcher.group(1);
                sf.VTR5_XMLDate__c = parseSignantDateTime(datetimeStr);
                if (sf.VTR5_XMLDate__c == null) {
                    throw new WrongFormatException('Wrong Date format in File Name: ' + datetimeStr);
                }

                if (sf.VTR5_XMLContent__c == null) {
                    throw new WrongFormatException('Wrong XML content');
                }

                Id studyIdByShSubjectNumber = getStudyIdByAbbrv(sf.VTR5_StudyHubSubjNum__c, studyIdByIrtValue);
                if (studyIdByShSubjectNumber == null) {
                    throw new WrongFormatException('IRT Configuration record cannot be found and Signant File Copy record cannot be created');
                }
                sf.VTR5_ConsDate__c = getSubNodeContentDate(node, 'ConsDate');
                sf.VTR5_Day1Dose__c = getSubNodeContentDate(node, 'Day1Dose');
                if (sf.VTR5_StudyHubSubjNum__c.substring(0,6) == 'Jan009') {
                    sf.VTR5_Day57Dose__c = getSubNodeContentDate(node, 'Day57Dose');
                    sf.VTR5_Day57Dose_Correction__c = getSubNodeAttr(node, 'Day57Dose', 'Correction') == 'yes';
                }

                sf.VTR5_StudyHubSubjNum_Correction__c = getSubNodeAttr(node, 'StudyHubSubjNum', 'Correction') == 'yes';
                sf.VTR5_ConsDate_Correction__c = getSubNodeAttr(node, 'ConsDate', 'Correction') == 'yes';
                sf.VTR5_Day1Dose_Correction__c = getSubNodeAttr(node, 'Day1Dose', 'Correction') == 'yes';
                sf.VTR5_IsThisARescreening__c = getSubNodeContentText(node, 'IsThisARescreening');

                sf.VTR5_ScrnNum__c = getSubNodeContentText(node, 'ScrnNum');
                sf.VTR5_SubjImmuno__c = getSubNodeContentText(node, 'SubjImmuno');
                sf.VTR5_SubjSafety__c = getSubNodeContentText(node, 'SubjSafety');
                sf.VTR5_PrevScrnNumber__c = getSubNodeContentText(node, 'PrevScrnNumber');
                sf.VTR5_RandDate__c = getSubNodeContentDate(node, 'RandDate');

                sf.VTR5_IsCorrectionXML__c = sf.VTR5_StudyHubSubjNum_Correction__c || sf.VTR5_ConsDate_Correction__c || sf.VTR5_Day1Dose_Correction__c || sf.VTR5_Day57Dose_Correction__c;
                sf.VTR5_IsRescreeningXML__c = sf.VTR5_IsThisARescreening__c != null && sf.VTR5_IsThisARescreening__c.toLowerCase() == 'yes';

                if (sf.VTR5_StudyHubSubjNum__c == null || sf.VTR5_ScrnNum__c == null || sf.VTR5_IsThisARescreening__c == null) {
                    throw new WrongFormatException('Missed required tag values (StudyHubSubjNum, ScrnNum or IsThisARescreening)');
                }

                /* commented as part of SH-20684
                // correction file validations
                if (sf.VTR5_IsCorrectionXML__c) {
                    if (sf.VTR5_StudyHubSubjNum_Correction__c) {
                        throw new ManualUpdateException('SH Internal Id can be corrected only manually');
                    }
                    if (sf.VTR5_Day1Dose_Correction__c) {
                        throw new ManualUpdateException('Day1Dose can be corrected only manually');
                    }
                    if (sf.VTR5_Day57Dose_Correction__c) {
                        throw new ManualUpdateException('Day57Dose can be corrected only manually');
                    }
                }
                // rescreening file validations
                if (sf.VTR5_IsRescreeningXML__c) {
                    if (sf.VTR5_PrevScrnNumber__c == null) {
                        throw new WrongFormatException('PrevScrnNumber on SignantFile is blank');
                    }
                }
                */

            } catch (Exception e) {
                processFileException(e, sf);
            }
        }
    }

    private void processCases(List<VTR5_Signant_File__c> signantFiles) {
        List<SignantFileWrapper> goodSignantFiles = new List<SignantFileWrapper>();
        for (VTR5_Signant_File__c sf : signantFiles) {
            if (!sf.VTR5_IsError__c) {
                goodSignantFiles.add(new SignantFileWrapper(sf));
            }
        }
        goodSignantFiles.sort();

        Map<String, Case> casesToProcessMap = getCasesToProcess(goodSignantFiles);

        Map<Id, Case> casesToUpdateMap = new Map<Id, Case>();
        List<CaseWithFieldsWrapper> caseWithFieldsWrappers = new List<CaseWithFieldsWrapper>();
        for (SignantFileWrapper sfw : goodSignantFiles) {
            VTR5_Signant_File__c sf = sfw.sObj;
            try {
                Case c = casesToProcessMap.get(sf.VTR5_StudyHubSubjNum__c);
                if (c == null) {
                    throw new MismatchException('Case with SH Internal Id "' + sf.VTR5_StudyHubSubjNum__c + '" cannot be found');
                }

                sf.VTR5_RelatedCase__c = c.Id;

                if (sf.VTR5_IsCorrectionXML__c) {           // Correction XML logic
                    caseWithFieldsWrappers.add(processCorrectionXML(c, sf));
                } else if (sf.VTR5_IsRescreeningXML__c) {   // Rescreening XML logic
                    caseWithFieldsWrappers.add(processRescreeningXML(c, sf));
                } else {                                    // Basic XML logic (not Correctio or Rescreening)
                    processBasicXML(c, sf);
                }

                if (c.VTR5_First_Signant_File_Date__c == null) {
                    c.VTR5_First_Signant_File_Date__c = Datetime.now();
                }
                c.VTR5_Last_Signant_File_Date__c = Datetime.now();
                casesToUpdateMap.put(c.Id, c);

            } catch (Exception e) {
                processFileException(e, sf);
            }
        }
        if(!caseWithFieldsWrappers.isEmpty()) {
            generateTasksAndNotifications(caseWithFieldsWrappers);
        }
        // try to update all cases
        List<Case> casesToUpdate = casesToUpdateMap.values();
        System.debug('sf: Updating ' + casesToUpdate.size() + ' cases');
        Map<Id, String> failedCases = new Map<Id, String>();
        List<Database.SaveResult> updateResults = Database.update(casesToUpdate, false);
        for (Integer i = 0; i < updateResults.size(); i++) {
            Database.SaveResult r = updateResults.get(i);
            System.debug('sf: isSuссess for Case #' + casesToUpdate.get(i).Id + ' - ' + r.isSuccess());
            if (!r.isSuccess()) {
                String errorMessage = r.getErrors().get(0).getMessage();
                System.debug('sf: errorMessage: ' + errorMessage);
                Id caseId = casesToUpdate.get(i).Id;
                failedCases.put(caseId, errorMessage);
            }
        }
        System.debug('sf: Number of Queries used in this apex code: ' + Limits.getQueries() + '/' + Limits.getLimitQueries());

        // for failed cases - set signant file to error
        for (VTR5_Signant_File__c sf : signantFiles) {
            if (sf.VTR5_RelatedCase__c != null && !sf.VTR5_IsError__c && failedCases.containsKey(sf.VTR5_RelatedCase__c)) {
                sf.VTR5_IsError__c = true;
                sf.VTR5_ErrorType__c = 'Exception';
                sf.VTR5_ErrorDetails__c = 'Failed to update Case: \n' + failedCases.get(sf.VTR5_RelatedCase__c);
            }
            sf.VTR5_IsProcessed__c = true;
            sf.VTR5_DateProcessed__c = Datetime.now();
            System.debug('sf: signantFile:' + sf);
        }

        System.debug('sf: Updating ' + signantFiles.size() + ' signant files');
        updateResults = Database.update(signantFiles, false);
        for (Integer i = 0; i < updateResults.size(); i++) {
            Database.SaveResult r = updateResults.get(i);
            System.debug('sf: isSuссess for File #' + signantFiles.get(i).Id + ' - ' + r.isSuccess());
            if (!r.isSuccess()) {
                System.debug('sf: errorMessage: ' + r.getErrors().get(0).getMessage());
            }
        }

    }

//    private static void updateRecords(List<SObject> records) {
//        if (records.isEmpty()) return;
//
//        System.debug('sf: Updating records: ' + records);
//        List<Database.SaveResult> results = Database.update(records, false);
//
//        for (Database.SaveResult sr : results) {
//            if (sr.isSuccess()) {
//                System.debug('sf: ' + 'Successfully updated. ID: ' + sr.getId());
//            } else {
//                for(Database.Error err : sr.getErrors()) {
//                    System.debug('sf: ' + 'Error updating. ID: ' + sr.getId() + '\n' + err.getStatusCode() + ': ' + err.getMessage());
//                }
//            }
//        }
//    }

    private CaseWithFieldsWrapper processCorrectionXML(Case c, VTR5_Signant_File__c sf) {
        /* commented as part of SH-20684
        if (sf.VTR5_ScrnNum__c != c.VTD1_Subject_ID__c && c.VTD1_Subject_ID__c != null) {  // if update ScrnNum
            throw new DuplicateException('ScrnNumber on SignantFile (' + sf.VTR5_ScrnNum__c + ') is different from ScrnNumber on Case (' + c.VTD1_Subject_ID__c + ')');
        }
        if (sf.VTR5_ConsDate_Correction__c && sf.VTR5_RandDate__c != null && sf.VTR5_RandDate__c != c.VTD1_Randomized_Date1__c)) {
            throw new ManualUpdateException('Randomized Date can be corrected only manually');
        }
        */
        List<FieldChange> fieldChanges = new List<FieldChange>();
        if (sf.VTR5_StudyHubSubjNum_Correction__c) {
            if(sf.VTR5_ScrnNum__c != null) {
                fieldChanges.add(new FieldChange('scrn num', c.VTD1_Subject_ID__c, sf.VTR5_ScrnNum__c));
                c.VTD1_Subject_ID__c = sf.VTR5_ScrnNum__c;
            }
            if(sf.VTR5_ConsDate__c != null) {
                fieldChanges.add(new FieldChange('consent date', c.VTR5_ConsentDate__c, sf.VTR5_ConsDate__c));
                c.VTR5_ConsentDate__c = sf.VTR5_ConsDate__c;
            }
            if(sf.VTR5_SubjImmuno__c != null) {
                fieldChanges.add(new FieldChange('subset immuno', c.VTR5_SubsetImmuno__c, sf.VTR5_SubjImmuno__c));
                c.VTR5_SubsetImmuno__c = sf.VTR5_SubjImmuno__c;
            }
            if(sf.VTR5_SubjSafety__c != null) {
                fieldChanges.add(new FieldChange('subset safety', c.VTR5_SubsetSafety__c, sf.VTR5_SubjSafety__c));
                c.VTR5_SubsetSafety__c = sf.VTR5_SubjSafety__c;
            }
            if(sf.VTR5_Day1Dose__c != null) {
                fieldChanges.add(new FieldChange('day 1 dose', c.VTR5_Day_1_Dose__c, sf.VTR5_Day1Dose__c));
                c.VTR5_Day_1_Dose__c = sf.VTR5_Day1Dose__c;
            }
            if(sf.VTR5_Day57Dose__c != null) {
                fieldChanges.add(new FieldChange('day 57 dose', c.VTR5_Day_57_Dose__c, sf.VTR5_Day57Dose__c));
                c.VTR5_Day_57_Dose__c = sf.VTR5_Day57Dose__c;
            }
            if(sf.VTR5_RandDate__c != null) {
                fieldChanges.add(new FieldChange('randomization date', c.VTD1_Randomized_Date1__c, sf.VTR5_RandDate__c));
                c.VTD1_Randomized_Date1__c = sf.VTR5_RandDate__c;
                c.Status = VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED;
            }
        }
        if (sf.VTR5_ConsDate_Correction__c) {
            fieldChanges.add(new FieldChange('consent date', c.VTR5_ConsentDate__c, sf.VTR5_ConsDate__c));
            c.VTR5_ConsentDate__c = sf.VTR5_ConsDate__c;
        }
        if (sf.VTR5_Day1Dose_Correction__c) {
            fieldChanges.add(new FieldChange('day 1 dose', c.VTR5_Day_1_Dose__c, sf.VTR5_Day1Dose__c));
            c.VTR5_Day_1_Dose__c = sf.VTR5_Day1Dose__c;
        }
        if (sf.VTR5_Day57Dose_Correction__c) {
            fieldChanges.add(new FieldChange('day 57 dose', c.VTR5_Day_57_Dose__c, sf.VTR5_Day57Dose__c));
            c.VTR5_Day_57_Dose__c = sf.VTR5_Day57Dose__c;
        }
        return new CaseWithFieldsWrapper(c, fieldChanges, 'correction');
    }

    private CaseWithFieldsWrapper processRescreeningXML(Case c, VTR5_Signant_File__c sf) {
        /* commented as part of SH-20684
        if (c.Status == VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED) {
            throw new CustomException('Rescreening XML is not allowed in Active/Randomized status');
        }
        if (sf.VTR5_PrevScrnNumber__c != c.VTD1_Subject_ID__c) {
            throw new DuplicateException('PrevScrnNumber on SignantFile (' + sf.VTR5_PrevScrnNumber__c + ') is different from SubjectId on Case (' + c.VTD1_Subject_ID__c + ')');
        }
        */
        List<FieldChange> fieldChanges = new List<FieldChange>();

        fieldChanges.add(new FieldChange('scrn num', c.VTD1_Subject_ID__c, sf.VTR5_ScrnNum__c));
        c.VTD1_Subject_ID__c = sf.VTR5_ScrnNum__c;

        if(sf.VTR5_ConsDate__c != null) {
            fieldChanges.add(new FieldChange('consent date', c.VTR5_ConsentDate__c, sf.VTR5_ConsDate__c));
            c.VTR5_ConsentDate__c = sf.VTR5_ConsDate__c;
        }
        return new CaseWithFieldsWrapper(c, fieldChanges, 'rescreening');
    }

    private void generateTasksAndNotifications(List<CaseWithFieldsWrapper> caseWithFieldsWrappers) {
        List<Task> tasks = new List<Task>();
        for(CaseWithFieldsWrapper caseWithFieldsWrapper : caseWithFieldsWrappers) {
            tasks.add(createTask(caseWithFieldsWrapper));
        }

        Database.DMLOptions dlo = new Database.DMLOptions();
        dlo.emailHeader.triggerUserEmail = true;
        Database.insert(tasks, dlo);

        Map<Id, Map<String, String>> notifMessageParams = new Map<Id, Map<String, String>>();
        for(Task task : tasks) {
            if (task.Id == null) continue;  // to avoid "Unable to serialize a map with a null key" error. Vlad Tyazhov
            notifMessageParams.put(task.Id, new Map<String, String>{task.VTD2_Task_Unique_Code__c => task.Description});
        }
        new Nots(notifMessageParams).publish();
    }

    private Task createTask(CaseWithFieldsWrapper caseWithFieldsWrapper) {
        String tnCode;
        String subjectLabel;
        if (caseWithFieldsWrapper.isCorrection) {
            tnCode = 'N601';
            subjectLabel = Label.VT_R5_SignantCorrectionTaskSubject;

        } else if (caseWithFieldsWrapper.isRescreening) {
            tnCode = 'N602';
            subjectLabel = Label.VT_R5_SignantFileRescreeningTaskMessage;
        }

        Task task = new Task();
        Case patientCase = caseWithFieldsWrapper.patient;
        task.OwnerId = patientCase.VTD2_Study_Geography__r.VTR5_Primary_VTSL__c;
        task.Status = TASK_OPEN_STATUS;
        task.Category__c = TASK_CATEGORY_FOLLOW_UP;
        task.VTD2_Task_Unique_Code__c = tnCode;

        List<String> subjectParams = new List<String>();
        if(caseWithFieldsWrapper.isCorrection) {
            subjectParams.add(caseWithFieldsWrapper.updatedFieldTitles);
        }
        subjectParams.add('"' + patientCase.VTD1_Subject_ID__c + '"');
        subjectParams.add(caseWithFieldsWrapper.updatedValues);
        task.Description = String.join(subjectParams, '; ');

        String taskSubject = String.format(subjectLabel, subjectParams);
        if(taskSubject.length() > 255) {
            taskSubject = taskSubject.substring(0,252) + '...';
        }
        task.Subject = taskSubject;
        return task;
    }

    public class CaseWithFieldsWrapper {
        public Case patient;
        public Boolean isCorrection = false;
        public Boolean isRescreening = false;
        public String updatedFieldTitles;
        public String updatedValues;

        public CaseWithFieldsWrapper(Case patientCase, List<FieldChange> fieldChanges, String xmlType) {
            this.patient = patientCase;
            if (xmlType == 'correction') {
                this.isCorrection = true;
            } else if (xmlType == 'rescreening') {
                this.isRescreening = true;
            } else {
                throw new CustomException('Wrong XML type: ' + xmlType);
            }

            List<String> fieldTitles = new List<String>();
            List<String> fieldChangeMessages = new List<String>();
            for(FieldChange fc : fieldChanges) {
                fieldTitles.add(fc.fieldTitle);
                fieldChangeMessages.add('from "' + fc.oldValue + '" to "' + fc.newValue + '"');
            }

            this.updatedFieldTitles = '"' + String.join(fieldTitles, ', ') + '"';
            this.updatedValues = String.join(fieldChangeMessages, ', ');
        }
    }

    private class FieldChange {
        public String fieldTitle;
        public String oldValue;
        public String newValue;

        public FieldChange(String fieldTitle, String oldValue, String newValue) {
            init(fieldTitle, oldValue, newValue);
        }

        public FieldChange(String fieldTitle, Date oldValue, Date newValue) {
            String oldValueStr = (oldValue != null) ? oldValue.format() : null;
            String newValueStr = (newValue != null) ? newValue.format() : null;
            init(fieldTitle, oldValueStr, newValueStr);
        }

        private void init(String fieldTitle, String oldValue, String newValue) {
            this.fieldTitle = fieldTitle;
            this.oldValue = (oldValue != null) ? oldValue : 'blank';
            this.newValue = newValue;
        }
    }

    public class Nots implements Callable {
        private Map<Id, Map<String, String>> notifMessageParams;

        public Nots(Map<Id, Map<String, String>> notifMessageParams) {
            this.notifMessageParams = notifMessageParams;
        }

        public Nots() {
        }

        public Type getType() {
            return Nots.class;
        }

        public Object call(String action, Map<String, Object> args){
            if(action == 'processEvents') {
                List<VTR4_New_Transaction__e> events = (List<VTR4_New_Transaction__e>) args.get('events');
                Map <Id, Map<String, String>> notificationParams = (Map <Id, Map<String, String>>) JSON.deserialize(
                        events[0].VTR4_Parameters__c, Map <Id, Map<String, String>>.class
                );
                List<String> tnCodes = new List<String>();
                List<String> tasksIds = new List<String>();
                for(Id taskId : notificationParams.keySet()) {
                    tasksIds.add(taskId);
                    for(String tnCode : notificationParams.get(taskId).keySet()){
                        tnCodes.add(tnCode);
                    }
                }
                VT_D2_TNCatalogNotifications.generateNotifications(
                        tnCodes,
                        tasksIds,
                        null,
                        null,
                        null,
                        notificationParams);
            }
            return null;
        }

        public void publish() {
            VTR4_New_Transaction__e eventToStartActionsExecution = new VTR4_New_Transaction__e(
                    VTR4_HandlerName__c = 'VT_R5_SignantFilesProcessingBatch.Nots',
                    VTR4_Parameters__c = JSON.serialize(notifMessageParams)
            );
            if(Test.isRunningTest()){
                List<VTR4_New_Transaction__e> eventsList = new List<VTR4_New_Transaction__e>{eventToStartActionsExecution};
                call('processEvents', new Map<String, Object>{'events' => eventsList});
            }
            else{
                EventBus.publish(eventToStartActionsExecution);
            }
        }
    }

    private void processBasicXML(Case c, VTR5_Signant_File__c sf) {
        if (sf.VTR5_ScrnNum__c != c.VTD1_Subject_ID__c && c.VTD1_Subject_ID__c != null) {  // if update ScrnNum
            throw new DuplicateException('ScrnNumber on SignantFile (' + sf.VTR5_ScrnNum__c + ') is different from ScrnNumber on Case (' + c.VTD1_Subject_ID__c + ')');
        }

        if (c.VTR5_Day_1_Dose__c == null && sf.VTR5_Day1Dose__c != null) {
            c.VTR5_Day_1_Dose__c = sf.VTR5_Day1Dose__c;
        }
        if (c.VTR5_Day_57_Dose__c == null && sf.VTR5_Day57Dose__c != null && sf.VTR5_StudyHubSubjNum__c.substring(0,6) == 'Jan009') {
            c.VTR5_Day_57_Dose__c = sf.VTR5_Day57Dose__c;
        }

        // if case is already randomized - break
        if (c.Status == VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED) return;

        // bellow - only for non-randomized Cases
        if (c.VTD1_Subject_ID__c == null && sf.VTR5_ScrnNum__c != null) {
            c.VTD1_Subject_ID__c = sf.VTR5_ScrnNum__c;
        }

        if (c.VTR5_ConsentDate__c == null && sf.VTR5_ConsDate__c != null) {
            c.VTR5_ConsentDate__c = sf.VTR5_ConsDate__c;
        }

        if (c.VTR5_SubsetImmuno__c == null && c.VTR5_SubsetSafety__c == null && (sf.VTR5_SubjImmuno__c != null || sf.VTR5_SubjSafety__c != null)) {
            c.VTR5_SubsetImmuno__c = sf.VTR5_SubjImmuno__c;
            c.VTR5_SubsetSafety__c = sf.VTR5_SubjSafety__c;
        }

        if (c.VTD1_Randomized_Date1__c == null && sf.VTR5_RandDate__c != null) {
            c.VTD1_Randomized_Date1__c = sf.VTR5_RandDate__c;
            c.Status = VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED;
        }
    }

    private Map<String, Case> getCasesToProcess(List<SignantFileWrapper> sfList) {
        Set<String> studyHubSubjNumSet = new Set<String>();
        for (SignantFileWrapper sf : sfList) {
            studyHubSubjNumSet.add(sf.sObj.VTR5_StudyHubSubjNum__c);
        }
        List<Case> caseList = [
                SELECT Id,
                        Status,
                        VTR5_Internal_Subject_Id__c,
                        VTD1_Subject_ID__c,
                        VTR5_ConsentDate__c,
                        VTD1_Randomized_Date1__c,
                        VTD2_Study_Geography__r.VTR5_Primary_VTSL__c,
                        VTR5_SubsetImmuno__c,
                        VTR5_SubsetSafety__c,
                        VTR5_Last_Signant_File_Date__c,
                        VTR5_Day_1_Dose__c,
                        VTR5_Day_57_Dose__c,
                        VTR5_First_Signant_File_Date__c,
                        VTD2_Study_Geography__c
                FROM Case
                WHERE VTR5_Internal_Subject_Id__c IN :studyHubSubjNumSet AND VTD1_Study__r.VTR5_IRTSystem__c = 'Signant'
                FOR UPDATE
        ];

        Map<String, Case> caseMap = new Map<String, Case>();
        for (Case c : caseList) {
            caseMap.put(c.VTR5_Internal_Subject_Id__c, c);
        }
        return caseMap;
    }

    private void createSignantFilesCopy(List<VTR5_Signant_File__c> sgFiles, Map<String, Id> studyIdByIrtValue) {
        List<VTR5_Signant_File_Copy__c> sgFilesCopiesToInsert = new List<VTR5_Signant_File_Copy__c>();
        for (VTR5_Signant_File__c sgFile : sgFiles) {
            VTR5_Signant_File_Copy__c sgFileCopy = createSignantFileCopyRecord(sgFile, getStudyIdByAbbrv(sgFile.VTR5_StudyHubSubjNum__c, studyIdByIrtValue));
            sgFilesCopiesToInsert.add(sgFileCopy);
        }
        if (sgFilesCopiesToInsert.size() > 0) {
            Database.insert(sgFilesCopiesToInsert, false);
        }
    }

    // HELPERS
    /**
     * @description parse DateTime in Signant format
     * @param s - String to parse, available formats:
     *   2020-08-21T14.30.10.234+07.00
     *   2020-08-21T14.30.10+07.00
     *   2020-08-21T14.30.10.234z
     *   2020-08-21T14.30.10z
     * @return - parsed Datetime
     */
    public static Datetime parseSignantDateTime(String s) {
        try {
            List<String> datetimeParts = s.split('T');                          // ["2020-08-21", "14.30.10.257+07.00"]
            String d = datetimeParts[0];
            String t = datetimeParts[1].toLowerCase().replace('.', ':');        // "14:30:10:257-07:00"

            String tzSign;
            for (String l : new Set<String>{'z', '-', '+'}) {
                if (t.contains(l)) {
                    tzSign = l;
                    break;
                }
            }
            List<String> timeParts = t.split(tzSign != '+' ? tzSign : '\\+');  // ["14:30:10:257", "07:00"]
            t = timeParts[0].substring(0, 8);                                   // "14:30:10"
            String tz = tzSign + (timeParts.size() > 1 ? timeParts[1] : '');     // "+07:00"

            String isoDatetime = d + 'T' + t + tz;                              // "2020-08-21T14:30:10+07:00"
            Datetime formattedDateTime = (Datetime) JSON.deserialize('"' + isoDatetime + '"', Datetime.class);

            return formattedDateTime;

        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @description parse Date in Signant format
     * @param s - String to parse, format: '01-Aug-2020'
     * @return - parsed Date
     */
    public static Date parseSignantDate(String s) {
        try {
            List<String> dateParts = s.split('-', 3);
            String day = dateParts[0];
            String monthName = dateParts[1];
            String year = dateParts[2];

            Map<String, String> monthNameMap = new Map<String, String>{
                    'Jan' => '01', 'Feb' => '02', 'Mar' => '03',
                    'Apr' => '04', 'May' => '05', 'Jun' => '06',
                    'Jul' => '07', 'Aug' => '08', 'Sep' => '09',
                    'Oct' => '10', 'Nov' => '11', 'Dec' => '12'
            };
            String month = monthNameMap.get(monthName);
            return Date.parse(month + '/' + day + '/' + year);

        } catch (Exception e) {
            return null;
        }
    }


    private static String getSubNodeContentText(Dom.XmlNode node, String subNodeName) {
        Dom.XmlNode subNode = node.getChildElement(subNodeName, null);
        if (subNode == null) {
            throw new WrongFormatException('XML schema violation. Tag "' + subNodeName + '" not recognized.');
        }
        String subNodeText = subNode.getText();
        if (subNodeText == '') return null;
        return subNodeText;
    }

    private static Date getSubNodeContentDate(Dom.XmlNode node, String subNodeName) {
        String subNodeText = getSubNodeContentText(node, subNodeName);
        if (subNodeText == null) return null;

        Date d = parseSignantDate(subNodeText);
        if (d == null) {
            throw new WrongFormatException('Bad date format in ' + subNodeName + ' tag: ' + subNodeText);
        }
        return d;
    }

    private static String getSubNodeAttr(Dom.XmlNode node, String subNodeName, String attrName) {
        Dom.XmlNode subNode = node.getChildElement(subNodeName, null);
        String result = null;
        if (subNode != null) {
            String attrValue = subNode.getAttributeValue(attrName, null);
            if (attrValue != null) {
                result = attrValue.toLowerCase();
            }
        }
        return result;
    }

    private static Id getStudyIdByAbbrv(String subjectNumber, Map<String, Id> studyIdByIrtValue) {
        Id studyId;
        if (subjectNumber != null) {
            String abbrv = subjectNumber.substring(0, 6);
            if (studyIdByIrtValue.containsKey(abbrv)) {
                studyId = studyIdByIrtValue.get(abbrv);
            }
        }
        System.debug('sg: studyId ' + studyId);
        return studyId;

    }

    private static VTR5_Signant_File_Copy__c createSignantFileCopyRecord(VTR5_Signant_File__c currentSignantFile, Id studyId) {
        VTR5_Signant_File_Copy__c sgFileCopy = new VTR5_Signant_File_Copy__c();
        sgFileCopy.Name = currentSignantFile.Name;
        sgFileCopy.VTR5_XMLDate_Copy__c = currentSignantFile.VTR5_XMLDate__c;
        sgFileCopy.VTR5_XMLContent_Copy__c = currentSignantFile.VTR5_XMLContent__c;
        sgFileCopy.VTR5_DateReceived_Copy__c = currentSignantFile.VTR5_DateReceived__c;
        sgFileCopy.VTR5_DateProcessed_Copy__c = currentSignantFile.VTR5_DateProcessed__c;
        sgFileCopy.VTR5_RelatedCase_Copy__c = currentSignantFile.VTR5_RelatedCase__c;
        sgFileCopy.VTR5_IsProcessed_Copy__c = currentSignantFile.VTR5_IsProcessed__c;
        sgFileCopy.VTR5_IsError_Copy__c = currentSignantFile.VTR5_IsError__c;
        sgFileCopy.VTR5_ErrorType_Copy__c = currentSignantFile.VTR5_ErrorType__c;
        sgFileCopy.VTR5_ErrorDetails_Copy__c = currentSignantFile.VTR5_ErrorDetails__c;
        sgFileCopy.VTR5_RelatedStudy__c = studyId;

        //XML Info
        sgFileCopy.VTR5_StudyHubSubjNum_Copy__c = currentSignantFile.VTR5_StudyHubSubjNum__c;
        sgFileCopy.VTR5_ScrnNum_Copy__c = currentSignantFile.VTR5_ScrnNum__c;
        sgFileCopy.VTR5_ConsDate_Copy__c = currentSignantFile.VTR5_ConsDate__c;
        sgFileCopy.VTR5_RandDate_Copy__c = currentSignantFile.VTR5_RandDate__c;
        sgFileCopy.VTR5_IsThisARescreening_Copy__c = currentSignantFile.VTR5_IsThisARescreening__c;
        sgFileCopy.VTR5_PrevScrnNumber_Copy__c = currentSignantFile.VTR5_PrevScrnNumber__c;
        sgFileCopy.VTR5_SubjImmuno_Copy__c = currentSignantFile.VTR5_SubjImmuno__c;
        sgFileCopy.VTR5_SubjSafety_Copy__c = currentSignantFile.VTR5_SubjSafety__c;
        sgFileCopy.VTR5_Day1Dose_Copy__c = currentSignantFile.VTR5_Day1Dose__c;
        sgFileCopy.VTR5_Day57Dose_Copy__c = currentSignantFile.VTR5_Day57Dose__c;
        sgFileCopy.VTR5_IsRescreeningXML_Copy__c = currentSignantFile.VTR5_IsRescreeningXML__c;

        //Correction
        sgFileCopy.VTR5_IsCorrectionXML_Copy__c = currentSignantFile.VTR5_IsCorrectionXML__c;
        sgFileCopy.VTR5_StudyHubSubjNum_Correction_Copy__c = currentSignantFile.VTR5_StudyHubSubjNum_Correction__c;
        sgFileCopy.VTR5_ConsDate_Correction_Copy__c = currentSignantFile.VTR5_ConsDate_Correction__c;
        sgFileCopy.VTR5_Day1Dose_Correction_Copy__c = currentSignantFile.VTR5_Day1Dose_Correction__c;
        sgFileCopy.VTR5_Day57Dose_Correction_Copy__c = currentSignantFile.VTR5_Day57Dose_Correction__c;

        return sgFileCopy;
    }

    /**
     * @description Custom Exceptions (SH-13089)
     */
    public class DuplicateException extends Exception {}
    public class MismatchException extends Exception {}
    public class WrongFormatException extends Exception {}
    // public class ManualUpdateException extends Exception {}
    public class CustomException extends Exception {}

    private void processFileException(Exception e, VTR5_Signant_File__c sf) {
        sf.VTR5_IsProcessed__c = true;
        sf.VTR5_IsError__c = true;
        sf.VTR5_DateProcessed__c = Datetime.now();

        String errorType = e.getTypeName();
        String errorMessage = e.getMessage();
        if (errorType == 'VT_R5_SignantFilesProcessingBatch.WrongFormatException') {
            sf.VTR5_ErrorType__c = 'Wrong Format';

        } else if (errorType == 'VT_R5_SignantFilesProcessingBatch.DuplicateException') {
            sf.VTR5_ErrorType__c = 'Duplicate';

        } else if (errorType == 'VT_R5_SignantFilesProcessingBatch.MismatchException') {
            sf.VTR5_ErrorType__c = 'Mismatch';

        } else if (errorType == 'VT_R5_SignantFilesProcessingBatch.ManualUpdateException') {
            sf.VTR5_ErrorType__c = 'Manual Update Required';

        } else if (errorType == 'VT_R5_SignantFilesProcessingBatch.CustomException') {
            sf.VTR5_ErrorType__c = 'Exception';

        } else {
            sf.VTR5_ErrorType__c = 'Exception';
            errorMessage = e.getStackTraceString();
        }

        sf.VTR5_ErrorDetails__c = errorMessage;
    }

    /**
     * @description Custom Wrapper for SignantFile to sort records by VTR5_XMLDate
     */
    public class SignantFileWrapper implements Comparable {
        public VTR5_Signant_File__c sObj;

        public SignantFileWrapper(VTR5_Signant_File__c sf) {
            sObj = sf;
        }

        // Compare opportunities based on the opportunity amount.
        public Integer compareTo(Object compareTo) {
            SignantFileWrapper compareToSF = (SignantFileWrapper) compareTo;
            Integer returnValue = 0;
            if (sObj.VTR5_XMLDate__c > compareToSF.sObj.VTR5_XMLDate__c) {
                returnValue = 1;
            }
            return returnValue;
        }
    }

    public void finish(Database.BatchableContext bc) {
        if(!Test.isRunningTest() && cronId == null){
            run();
        }
    }

    public static void run(){
        cronId = System.scheduleBatch(new VT_R5_SignantFilesProcessingBatch(), 'Signant Processing ' + System.now(), REPEAT_INTERVAL_MINUTES, SCOPE_SIZE);
    }
}