/**
 * Created by Denis Belkovskii on 4/20/2020.
 */

public class VT_R4_Scheduler_VCTrigger_Handler {

    public static void generateTelevisitEvents(List<VTR4_Scheduler_Video_Conference__c> schVideoConferences,
            Map<Id, VTR4_Scheduler_Video_Conference__c> schVideoConferencesOldMap) {

        if (VTR4_ExternalScheduler__c.getInstance().VTR4_Active__c){

            Integer openedConferences = 0;
            Integer closedConferences = 0;





            Map<Id, VTR4_Scheduler_Video_Conference__c> schedulersMap = new Map<Id, VTR4_Scheduler_Video_Conference__c>();
            List<Id> conferenceIdsToProcess = new List<Id>();
            for (VTR4_Scheduler_Video_Conference__c schVC : schVideoConferences) {
                conferenceIdsToProcess.add(schVC.VTR4_Video_Conference__c);
                schedulersMap.put(schVC.VTR4_Video_Conference__c, schVC);
            }

            if (!conferenceIdsToProcess.isEmpty()) {



                List<VTR4_CNotification__e> notificationsEvents = new List<VTR4_CNotification__e>();

                List<VTD1_Actual_Visit__c> visitsWithMembers = getRelatedVisitsWithMembers(conferenceIdsToProcess);
                for (VTD1_Actual_Visit__c visit : visitsWithMembers) {
                    Id conferenceId = visit.Video_Conferences__r[0].Id;
                    VTR4_Scheduler_Video_Conference__c sch = schedulersMap.get(conferenceId);
                    Set<Id> userIds = new Set<Id>();
                        for (Visit_Member__c member : visit.Visit_Members__r) {

                        userIds.add(member.VTD1_Participant_User__c);
                    }
                    if (sch.VTR4_ReadyToStart__c && !sch.VTR4_ReadyToEnd__c && (schVideoConferencesOldMap == null || !schVideoConferencesOldMap.get(sch.Id).VTR4_ReadyToStart__c)) {
                        openedConferences++;
                        notificationsEvents.add(new VTR4_CNotification__e(VTR4_UserId__c = JSON.serialize(userIds),
                                VTR4_LinkToRelatedEventOrObject__c = conferenceId,
                                VTR4_Type__c = 'Televisit',
                                VTR4_Message__c='Opened'));
                    } else if (sch.VTR4_ReadyToEnd__c && (schVideoConferencesOldMap == null || !schVideoConferencesOldMap.get(sch.Id).VTR4_ReadyToEnd__c)) {
                        closedConferences++;
                        notificationsEvents.add(new VTR4_CNotification__e(VTR4_UserId__c = JSON.serialize(userIds),
                                VTR4_LinkToRelatedEventOrObject__c = conferenceId,
                                VTR4_Type__c = 'Televisit',
                                VTR4_Message__c = 'Closed'));

                    }
                }
                if (!notificationsEvents.isEmpty()) {
                    EventBus.publish(notificationsEvents);


                    VTD1_Integration_Log__c triggerLog = new VTD1_Integration_Log__c(


                        VTD1_Body_Response__c = 'opened: ' + openedConferences + '; closed: ' + closedConferences,
                        VTD1_Action__c = 'Video conference'
                    );
                    insert triggerLog;
                } 
            }
        }
    }

    public static List<VTD1_Actual_Visit__c> getRelatedVisitsWithMembers(List<Id> conferenceIds) {
        List<VTD1_Actual_Visit__c> visitsWithMembers = new List<VTD1_Actual_Visit__c>([
                SELECT
                        Id, (
                        SELECT Id
                        FROM Video_Conferences__r
                        ORDER BY CreatedDate DESC
                        LIMIT 1
                ), (
                        SELECT VTD1_Participant_User__c
                        FROM Visit_Members__r
                )
                FROM VTD1_Actual_Visit__c
                WHERE Id IN (
                        SELECT VTD1_Actual_Visit__c
                        FROM Video_Conference__c
                        WHERE Id IN :conferenceIds
                )
                AND
                VTD1_Status__c IN (:VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
                        :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED)
        ]);
        return visitsWithMembers;
    }
}