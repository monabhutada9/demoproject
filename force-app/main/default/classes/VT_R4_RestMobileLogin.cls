/**
 * @description :
 *
 * @author : Dmitry Yartsev & Alex Komarov
date: 26.02.2020
 */
@RestResource(UrlMapping='/Patient/Auth/*')
global without sharing class VT_R4_RestMobileLogin {

    private static final Set<String> AVAILABLE_PROFILES = new Set<String>{
            'Patient',
            'Caregiver'
    };
    private static final String SF_OLD_PASSWORD_INVALID = 'INVALID_OLD_PASSWORD';
    private static final String SF_NEW_PASSWORD_INVALID = 'INVALID_NEW_PASSWORD';
    private static final String SF_INVALID_SESSION_ID = 'INVALID_SESSION_ID';
    private static final String SF_INVALID_CROSS_REFERENCE_KEY = 'INVALID_CROSS_REFERENCE_KEY';


    private static final String ERROR_MESSAGE = 'ERROR';

    @TestVisible static RestRequest request = RestContext.request;
    @TestVisible static RestResponse response = RestContext.response;

    private static VT_R4_RestMobileLoginFlow.PatientLoginFlowResponse patientLoginFlowResponse;

    global class AuthResponse {
        String sessionId;
        String userId;
        Integer attemptsRemaining;
        String errorCode;
        global String message { get; private set; }
        Boolean loginFlowFinished;
        Boolean success;
        String userFirstName;
        String userLastName;
        String userLanguage;
        Long passwordLastChangedDate;
        String userUsername;
        Boolean passwordExpired = false;
    }

    private class RequestParams {
        String username;
        String password;
        String deviceUniqueCode;
        String newPassword;
        String oldPassword;
        String userID;
        String stage;
        String verificationCode;
        Boolean isPrivacyNoticesAccepted;
        Boolean isAuthorizationAccepted;
    }
    private static void parseParams() {
        parsedParams = (RequestParams) JSON.deserialize(request.requestBody.toString(), RequestParams.class);
    }

    private static RequestParams parsedParams = new RequestParams();

    public class LoginFlowRequest_Remote extends VT_RemoteCall {

        private String requestBody;
        private Boolean successResponse;

        public LoginFlowRequest_Remote(String method, String requestBody) {
            this(method, '', requestBody);
        }

        public LoginFlowRequest_Remote(String method, String pathURL, String requestBody) {
            this.httpMethod = method;
            this.headersMap.putAll(new Map<String, String>{
                    'Authorization' => 'Bearer ' + VT_D1_HelperClass.getSessionIdForGuest(),
                    'Content-Type' => 'application/json'
            });
            this.endPointURL = getCommunityURL() + '/services/apexrest/Patient/Loginflow';
            this.endPointURL += pathURL;
            this.requestBody = requestBody;
        }

        public override Type getType() {
            return LoginFlowRequest_Remote.class;
        }

        public override String buildRequestBody() {
            return requestBody;
        }

        public override Object parseResponse(String responseBody) {
            successResponse = super.successResponse;
            if (successResponse) {
                return (VT_R4_RestMobileLoginFlow.PatientLoginFlowResponse) JSON.deserialize(responseBody, VT_R4_RestMobileLoginFlow.PatientLoginFlowResponse.class);

            } else {
                patientLoginFlowResponse.serverMessage += ERROR_MESSAGE;
                patientLoginFlowResponse.errors.add(errorMessage);
                return patientLoginFlowResponse;
            }
        }
    }


    @HttpGet
    global static void getMobileDictionaryPreLogin() {
        String language = 'en_US';
        if (request.params.containsKey('lang')) {
            language = request.params.get('lang');
        }
        Map<String, String> dictionaryData = new Map<String, String>();
        try {
            dictionaryData = VT_R3_CustomLabelsRenderPageController.getLabelsString(language, '%27%2525MobileLogin%2525%27');
            sendResponse(dictionaryData);
        } catch (Exception e) {
            sendResponse(500, dictionaryData);


        }
    }


    @HttpPut
    global static void activation() {
        try {
            parseParams();
            if (isParamsMissed()) return;
            System.debug('parsedParams' + parsedParams);

            LoginFlowRequest_Remote loginReq = new LoginFlowRequest_Remote(
                    VT_RemoteCall.METHOD_POST,
                    '?activation=true',
                    JSON.serialize(parsedParams)
            );
            patientLoginFlowResponse = (VT_R4_RestMobileLoginFlow.PatientLoginFlowResponse) loginReq.execute();

            if (patientLoginFlowResponse != null && patientLoginFlowResponse.newPassword != null
                    && patientLoginFlowResponse.username != null) {
                if (loginReq.successResponse) {
                    VT_R4_RestMobileLogin.AuthResponse authResponse = VT_R4_RestMobileLogin.login(patientLoginFlowResponse.username, patientLoginFlowResponse.newPassword, parsedParams.deviceUniqueCode);
                    System.debug('authResponse ' + authResponse);
                    sendResponse(authResponse);
                    return;
                }
            }
            sendResponse(patientLoginFlowResponse);
            return;
        } catch (Exception e) {
            sendResponse(e);
            return;
        }
    }

    private static Boolean isParamsMissed() {
        if (parsedParams.stage == null || parsedParams.deviceUniqueCode == null || parsedParams.userID == null) {
            sendResponse(400, 'stage or deviceUniqueCode or userID missing');
            return true;
        } else if (parsedParams.stage.equals(VT_R4_RestMobileLoginFlow.SETUP_PASSWORD_STAGE) && parsedParams.newPassword == null) {
            sendResponse(400, 'newPassword is missing');
            return true;
        } else {
            return false;
        }
    }

    @HttpPost
    global static void authAction() {
        AuthResponse authResp;
        RequestParams requestParams = (RequestParams) JSON.deserialize(request.requestBody.toString(), RequestParams.class);
        try {
            switch on request.requestURI {
                when '/Patient/Auth/Login' {
                    authResp = login(requestParams.username, requestParams.password, requestParams.deviceUniqueCode);
                    response.statusCode =
                            requestParams.username == null || requestParams.password == null || requestParams.deviceUniqueCode == null ? 400 : 200;
                }
                when '/Patient/Auth/ForgotPassword' {
                    authResp = forgotPassword(requestParams.username);
                    response.statusCode = 200;
                }
                when '/Patient/Auth/ChangePassword' {
                    authResp = changePassword(requestParams.oldPassword, requestParams.newPassword);
                    response.statusCode =
                            requestParams.oldPassword == null || requestParams.newPassword == null ? 400 : 200;
                }
                when '/Patient/Auth/SetPassword' {
                    authResp = login(requestParams.username, requestParams.oldPassword, requestParams.deviceUniqueCode);
                    if (authResp.errorCode == null && authResp.passwordExpired) {
                        authResp = setPassword(authResp.userId, authResp.sessionId, requestParams.newPassword);
                        if (authResp != null) {
                            if (authResp.message == null) {
                                response.statusCode = 200;
                                authResp = login(requestParams.username, requestParams.newPassword, requestParams.deviceUniqueCode);
                            } else {
                                response.statusCode = 500;
                            }
                        } else {
                            authResp = new AuthResponse();
                            authResp.message = 'UNKNOWN_EXCEPTION';
                            response.statusCode = 500;
                        }
                    } else {
                        authResp.message = 'OLD_PASSWORD_NOT_EXPIRED';
                        response.statusCode = 500;
                    }
                }
                when else {
                    authResp = notFound();
                    response.statusCode = 404;
                }
            }
        } catch (Exception e) {
            authResp = new AuthResponse();
            response.statusCode = 500;
            authResp.message = 'FAILURE. ' + e.getMessage() + ' ' + e.getStackTraceString();
        }
        response.responseBody = Blob.valueOf(JSON.serialize(authResp, true));
        response.addHeader('Content-Type', 'application/json');
        return;
    }
    public static AuthResponse login(String username, String password, String deviceUniqueCode) {
        AuthResponse response = new AuthResponse();
        if (username == null || password == null || deviceUniqueCode == null) {
            List<String> missingFields = new List<String>();
            if (username == null) missingFields.add('username');
            if (password == null) missingFields.add('password');
            if (deviceUniqueCode == null) missingFields.add('deviceUniqueCode');
            response.errorCode = 'BAD_REQUEST';
            response.message = 'Required field missing: ' + String.join(missingFields, ', ');
            return response;
        }
        List<User> userList = [
                SELECT Id,
                        HIPAA_Accepted__c,
                        VTD1_Privacy_Notice_Last_Accepted__c,
                        VTR3_Verified_Mobile_Devices__c,
                        FirstName,
                        LastName,
                        Username,
                        LanguageLocaleKey,
                        VTD1_Filled_Out_Patient_Profile__c,
                        LastPasswordChangeDate,
                        VTR4_Is_Activated__c,
                        First_Log_In__c,
                        Contact.VTR2_Primary_Language__c
                FROM User
                WHERE Username = :username
                AND Profile.Name IN :AVAILABLE_PROFILES
        ];
        if (userList.size() > 0) {
            HttpResponse resp = performSOAPAction(VT_R5_MobileAuth.getSoapLoginRequestBody(username, password));
            System.debug('1' + JSON.serializePretty(resp.getBody()));
            System.debug('1' + JSON.serializePretty(resp.getStatusCode()));
            response.sessionId = getValueFromSoapResponse(resp.getBody(), 'sessionId');
            if (response.sessionId == null) {
                response.errorCode = 'LOGIN_FAILED';
                response.message = System.Label.VTR3_UsernameOrPassIncorrect;
                response.attemptsRemaining = VT_D1_LockedUserHelper.checkForLockedUser(username);
            } else {
                System.debug(response);
                Boolean isPasswordExpired = Boolean.valueOf(getValueFromSoapResponse(resp.getBody(), 'passwordExpired'));
                if (isPasswordExpired) {
                    response.passwordExpired = true;
                    response.passwordLastChangedDate = userList[0].LastPasswordChangeDate.getTime();
                    response.userUsername = userList[0].Username;
                }
                response.loginFlowFinished = checkFinishedLoginFlow(userList[0], deviceUniqueCode);
                response.userId = getValueFromSoapResponse(resp.getBody(), 'userId');
                response.userFirstName = userList[0].FirstName;
                response.userLastName = userList[0].LastName;
                if (userList[0].Contact != null) {
                    response.userLanguage = userList[0].Contact.VTR2_Primary_Language__c;
                }
            }
        } else {
            response.errorCode = 'LOGIN_FAILED';
            response.message = System.Label.VTR3_UsernameOrPassIncorrect;
        }
        return response;
    }
    private static AuthResponse setPassword(String userId, String sessionId, String newPassword) {
        AuthResponse response = new AuthResponse();
        try {
            HttpResponse resp = performSOAPAction(VT_R5_MobileAuth.getSoapSetPasswordRequestBody(newPassword, sessionId, userId));
            Integer statusCode = resp.getStatusCode() == null ? 500 : resp.getStatusCode();
            if (statusCode == 200) {
                return response;
            } else {
                response.message = getValueFromSoapResponse(resp.getBody(), 'faultstring');
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }
    private static AuthResponse changePassword(String oldPassword, String newPassword) {
        AuthResponse response = new AuthResponse();
        if (newPassword == null || oldPassword == null) {
            List<String> missingFields = new List<String>();
            if (newPassword == null) missingFields.add('newPassword');
            if (oldPassword == null) missingFields.add('oldPassword');
            response.errorCode = 'BAD_REQUEST';
            response.message = 'Required field missing: ' + String.join(missingFields, ', ');
            return response;
        }
        try {
            HttpResponse resp = performSOAPAction(VT_R5_MobileAuth.getSoapChangePasswordRequestBody(oldPassword, newPassword));
            if (resp.getStatusCode() == 200) {

                response.message = VT_D1_TranslateHelper.getLabelValue(Label.VT_R4_ChangedPassword, UserInfo.getLanguage());

                response.success = true;
            } else if (resp.getStatusCode() == 500) {
                String sfExceptionCode = getValueFromSoapResponse(resp.getBody(), 'sf:exceptionCode');
                if (sfExceptionCode.equals(SF_OLD_PASSWORD_INVALID)
                        || sfExceptionCode.equals(SF_NEW_PASSWORD_INVALID)
                        || sfExceptionCode.equals(SF_INVALID_SESSION_ID)) {
                    response.errorCode = sfExceptionCode;
                    response.message = getValueFromSoapResponse(resp.getBody(), 'sf:exceptionMessage');
                } else {
                    response.errorCode = 'UNKNOWN_EXCEPTION';
                    response.message = 'panic';
                }
            } else {
                response.errorCode = 'UNKNOWN_EXCEPTION';
            }
        } catch (Exception e) {
            response.errorCode = 'UNKNOWN_EXCEPTION';
            response.message = e.getMessage();
        }
        return response;
    }
    private static AuthResponse forgotPassword(String username) {
        AuthResponse response = new AuthResponse();
        if (username == null) {
            response.errorCode = 'BAD_REQUEST';
            response.message = 'Required field missing: username';
            return response;
        }
        List<User> userList = [SELECT Id,Profile.Name FROM User WHERE Username = :username AND Profile.Name IN :AVAILABLE_PROFILES];
        if (userList.size() > 0) {
            String userId = userList[0].Id;
            try {
                HttpResponse resp = performSOAPAction(VT_R5_MobileAuth.getSoapResetPasswordRequestBody(userId));
                if (resp.getStatusCode() == 200) {
                    response.success = true;
                } else if (resp.getStatusCode() == 500) {
                    String sfExceptionCode = getValueFromSoapResponse(resp.getBody(), 'sf:exceptionCode');
                    if (sfExceptionCode.equals(SF_INVALID_CROSS_REFERENCE_KEY)) {
                        response.errorCode = sfExceptionCode;
                        response.message = getValueFromSoapResponse(resp.getBody(), 'sf:exceptionMessage');
                    } else {
                        response.errorCode = 'UNKNOWN_EXCEPTION';
                        response.message = 'panic';
                    }
                } else {
                    response.errorCode = 'UNKNOWN_EXCEPTION';
                }
            } catch (Exception e) {
                response.errorCode = 'UNKNOWN_EXCEPTION';
                response.message = e.getMessage();
            }
        } else {
            response.success = false;
            response.message = 'INVALID_USERNAME';
        }
        return response;
    }
    private static AuthResponse notFound() {
        AuthResponse response = new AuthResponse();
        response.errorCode = 'NOT_FOUND';
        response.message = 'Could not find a match for URL';
        return response;
    }
    private static HttpResponse performSOAPAction(String body) {
        HttpRequest request = new HttpRequest();
        request.setEndpoint(getCommunityURL() + '/services/Soap/u/47.0');
        request.setTimeout(60000);
        request.setMethod('POST');
        request.setHeader('SOAPAction', 'login');
        request.setHeader('Accept', 'text/xml');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setBody(body);
        return new Http().send(request);
    }

    private static String getValueFromSoapResponse(String responseBody, String keyField) {
        String fieldOpen = '<' + keyField + '>';
        String fieldClose = '</' + keyField + '>';
        if (responseBody.contains(fieldOpen)) {
            return responseBody.split(fieldOpen)[1].split(fieldClose)[0];
        }
        return null;
    }

    @TestVisible
    private static Boolean checkFinishedLoginFlow(User user, String deviceUniqueCode) {
        return user.HIPAA_Accepted__c && user.VTD1_Filled_Out_Patient_Profile__c && user.VTD1_Privacy_Notice_Last_Accepted__c != null && user.VTR3_Verified_Mobile_Devices__c != null && (user.VTR3_Verified_Mobile_Devices__c.contains(deviceUniqueCode) || user.VTR3_Verified_Mobile_Devices__c.contains('AUTOTEST'));
    }
    private static String getCommunityURL() {
        List<Organization> organizations = [SELECT Id, IsSandbox FROM Organization];
        List<Domain> domains = new List<Domain>();
        String patient = '';
        if (!organizations.isEmpty()) {
            if (organizations[0].IsSandbox) {
                domains = [SELECT Domain FROM Domain WHERE HttpsOption = 'Community'];
                patient += '/patient';
            } else {
                domains = [SELECT Domain FROM Domain WHERE Domain = 'virtualtrials-patient.solutions.iqvia.com'];
            }
        }
        if (!domains.isEmpty()) return 'https://' + domains[0].Domain + patient;
        System.debug('ATTENTION problem is here. Unexpected result.');
        return '';
    }

    private static void sendResponse(Exception e) {
        sendResponse(500, '{"error_message":"' + e.getMessage() + ' ' + e.getStackTraceString() + '"}');
    }

    private static void sendResponse(Integer code, Object data) {
        response.statusCode = code;
        sendResponse(data);
    }
    private static void sendResponse(Object data) {
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(data, true));
    }
}