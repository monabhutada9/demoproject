/**
 * @author:         Rishat Shamratov
 * @date:           01.09.2020
 * @description:    !!!THIS CLASS IS FOR SANDBOXES ONLY!!!
 *                  This schedulable batchable class is to:
 *                  + run test classes everyday,
 *                  + check the tests results,
 *                  + send the reports via email.
 *
 *                  To schedule the job just run following command at anonymous apex execution:
 *                  VT_R5_TestClassProcessor.scheduleJob();
 *
 *                  To add or remove test classes for the run - use 'includedTestNameList' or 'excludedTestNameList' accordingly.
 */

public class VT_R5_TestClassProcessor implements Schedulable, Database.Batchable<SObject>, Database.AllowsCallouts {
    private static List<String> includedTestNameList = new List<String>{
            'strike_tst_lookupController',
            'Test%',
            '%\\_Test%',
            '%AllTests%',
            '%Test',
            'VT_R3_GlobalSharing_TestExtras',
            'ZippexTests'
    };

    private static List<String> excludedTestNameList = new List<String>{
            'TBD_TestClass',
            'VT_D1_CaseRelatedSharingHandler_Test',
            'VT_D1_PatientConfirmDeliveryTest1',
            'VT_D1_ProtDeviationSharingHandler_Test',
            'VT_D1_TestData',
            'VT_D1_TestDataCleaner',
            'VT_D1_TestDataGeneratorController',
            'VT_D1_TestDataPostConversion',
            'VT_D1_TestUtils',
            'VT_D1_Test_DataFactory',
            'VT_D2_MedicalRecordsCertifyHelper_Test',
            'VT_R3_AbstractChainableQueueableTest',
            'VT_R3_GlobalSharingBatchTest',
            'VT_R5_GlobalSharing_ActionItem_UserTest',
            'VT_R5_GlobalSharing_Chat_UserTest',
            'VT_R5_GlobalSharing_Document_UserTest',
            'VT_R5_GlobalSharing_MonitoringV_UserTest',
            'VT_R5_GlobalSharing_PD_UserTest',
            'VT_R5_GlobalSharing_User_UserTest',
            'VT_R5_GlobalSharing_VidCon_UserTest',
            'VT_R5_TestClassProcessor',
            'VT_R5_TestCoverageProcessor'
    };

    public static void scheduleJob() {
        String cronJobDetailName = 'Test Class Processor - Run Tests';
        try {
            List<CronTrigger> cronTriggerList = [
                    SELECT Id
                    FROM CronTrigger
                    WHERE CronJobDetail.Name = :cronJobDetailName
            ];
            if (!cronTriggerList.isEmpty()) {
                System.debug('The job \'' + cronJobDetailName + '\' is already in progress.');
                return;
            }
        } catch (Exception e) {
            System.debug(e.getMessage());
        }

        try {
            System.schedule(cronJobDetailName, '0 0 3 * * ?', new VT_R5_TestClassProcessor());
            System.debug('The job \'' + cronJobDetailName + '\' has been scheduled.');
        } catch (Exception e) {
            System.debug('Schedule error: ' + e.getMessage());
        }
    }

    public void execute(SchedulableContext SC) {
        List<ApexTestQueueItem> pastApexTestQueueItemList = [SELECT Id FROM ApexTestQueueItem];
        for (ApexTestQueueItem apexTestQueueItem : pastApexTestQueueItemList) {
            apexTestQueueItem.Status = 'Aborted';
        }
        update pastApexTestQueueItemList;

        List<ApexClass> apexTestClassList = [
                SELECT Id
                FROM ApexClass
                WHERE NamespacePrefix = NULL
                AND Name != NULL
                AND (Name LIKE :includedTestNameList)
                AND (NOT Name LIKE :excludedTestNameList)
                ORDER BY Name ASC
        ];

        if (apexTestClassList.size() > 0) {
            List<ApexTestQueueItem> apexTestQueueItemList = new List<ApexTestQueueItem>();
            for (ApexClass testClass : apexTestClassList) {
                apexTestQueueItemList.add(new ApexTestQueueItem(ApexClassId = testClass.Id));
            }
            insert apexTestQueueItemList;

            String cronJobDetailName = 'Test Class Processor - Get Test Failed Results';
            try {
                List<CronTrigger> cronTriggerList = [
                        SELECT Id
                        FROM CronTrigger
                        WHERE CronJobDetail.Name = :cronJobDetailName
                ];
                if (!cronTriggerList.isEmpty()) {
                    System.debug('The job \'' + cronJobDetailName + '\' is already in progress.');
                    return;
                }
            } catch (Exception e) {
                System.debug(e.getMessage());
            }

            try {
                System.scheduleBatch(this, cronJobDetailName, 330, 2000);
                System.debug('The job \'' + cronJobDetailName + '\' has been scheduled.');
            } catch (Exception e) {
                System.debug('Schedule error: ' + e.getMessage());
            }
        } else {
            System.debug('No test classes found.');
        }
    }

    public static Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT ApexClass.Name,
                        Outcome,
                        MethodName,
                        StackTrace,
                        Message,
                        RunTime,
                        SystemModstamp
                FROM ApexTestResult
                WHERE SystemModstamp >= :System.now().addHours(-6)
                AND SystemModstamp <= :System.now()
                ORDER BY SystemModstamp ASC
                LIMIT 10000
        ]);
    }

    public void execute(Database.BatchableContext BC, List<ApexTestResult> apexTestResultList) {
        VT_R5_TestCoverageProcessor.sendEmailReports(null, null, null, null);
    }

    public void finish(Database.BatchableContext BC) {
        System.debug('!!! The work is done... for today. !!!');
    }
}