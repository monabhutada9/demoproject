public with sharing class VT_R5_DeviceArchivalController {

    @AuraEnabled
    public static String insertDeviceReadingRecords(VTR5_DeviceReadingAddInfo__c newRecord, Boolean isRetakeNeeded) {
        try {
            VTD1_Patient_Device__c patientDevice = new VTD1_Patient_Device__c(
                    VTR5_DeviceKeyId__c = newRecord.VTR5_DeviceSerialNumber__c
            );
            newRecord.VTR5_DeviceID__r = patientDevice;

            insert(newRecord);

            if (isRetakeNeeded && patientDevice.VTR5_SyncStatus__c != 'Retake Requested') {
                patientDevice.VTR5_SyncStatus__c = 'Retake Requested';
                VT_R4_ConstantsHelper_Misc.isRetakeRequest = true;
                upsert patientDevice VTR5_DeviceKeyId__c;
            }

         } catch (Exception e) {
             System.debug('error in record '+ e.getMessage());
             ErrorLogUtility.logException(e, ErrorLogUtility.ApplicationArea.APP_AREA_CHAT_AUTOMATION, VT_R5_DeviceArchivalController.class.getName());
             return 'failed' ;          
        }
         return 'SUCCESS';
    }


    @AuraEnabled
    public static String getContextUserUiThemeDisplayed() {
        return UserInfo.getUiThemeDisplayed();
    }

}