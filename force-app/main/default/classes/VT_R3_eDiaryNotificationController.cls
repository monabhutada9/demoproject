public with sharing class VT_R3_eDiaryNotificationController {
    public String eDiaryId {get;set;}
    public String recipientType {get;set;}
    public String userLanguage {get;set;}
    public Boolean eDiaryFlag {get;set;}


    public String getMainBody() {
        List<VTD1_Survey__c> eDiaryList = [
                SELECT  Id,
                        VTD1_Protocol_ePRO__r.Name,
                        VTD2_Study_Nickname__c,
                        VTD1_CSM__r.VTD1_Study__r.VTD1_Protocol_Nickname__c,
                        VTD2_Patient_s_Due_Time__c,
                        VTD2_Patient_s_Numeric_timezone__c,
                        VTD2_EmailDueDate__c,
                        VTD1_Reminder_Window__c
                FROM    VTD1_Survey__c
                WHERE   Id = :eDiaryId
        ];
        if(eDiaryList.isEmpty()) {
            return null;
        }
        VT_D1_TranslateHelper.translate(eDiaryList, userLanguage);
        System.debug('eDiaryList: ' + eDiaryList);
        String currentBody;

        if(eDiaryFlag) {
            currentBody = VT_D1_TranslateHelper.getLabelValue('VTR3_eDiary_Entry_Available', userLanguage);
        } else {
            currentBody = VT_D1_TranslateHelper.getLabelValue('VTR3_eDiary_Entry_Almost_Due', userLanguage);
        }
        return compileBody(currentBody, eDiaryList[0]);
    }

    public String getSalutation(){
        String salutation = VT_D1_TranslateHelper.getLabelValue('VT_R2_VisitEmailSalutaion', userLanguage);
        return salutation.replace('#Visit_participant', recipientType);
    }

    private String compileBody(String currentBody, VTD1_Survey__c eDiary) {
        String result = currentBody.replace('#VTD1_Survey__c.Name', eDiary.VTD1_Protocol_ePRO__r.Name);
        System.debug('result: ' + result);
        if(eDiary.VTD1_CSM__c != null && eDiary.VTD1_CSM__r.VTD1_Study__c != null) {
            result = eDiary.VTD1_CSM__r.VTD1_Study__r.VTD1_Protocol_Nickname__c == null ? result : result.replace('#VTD1_Survey__c.VTD2_Study_Nickname__c', eDiary.VTD1_CSM__r.VTD1_Study__r.VTD1_Protocol_Nickname__c);
        }
        result = eDiary.VTD2_Patient_s_Due_Time__c == null ? result : result.replace('#VTD1_Survey__c.VTD2_Patient_s_Due_Time__c', eDiary.VTD2_Patient_s_Due_Time__c);
        result = eDiary.VTD2_Patient_s_Numeric_timezone__c == null ? result : result.replace('#VTD1_Survey__c.VTD2_Patient_s_Numeric_timezone__c', VT_D1_TranslateHelper.translateTimeZone(eDiary.VTD2_Patient_s_Numeric_timezone__c, userLanguage));
        result = eDiary.VTD2_EmailDueDate__c == null ? result : result.replace('#VTD1_Survey__c.VTD2_EmailDueDate__c', translateDate(eDiary.VTD2_EmailDueDate__c));
        result = eDiary.VTD1_Reminder_Window__c == null ? result : result.replace('#VTD1_Survey__c.VTD1_Reminder_Window__c', eDiary.VTD1_Reminder_Window__c.toPlainString());
        return result;
    }

    private String translateDate(Date d) {
        String dateString = ((Datetime) d).formatGmt('EEEE, MMMM d yyyy');
        return VT_D1_TranslateHelper.translate(dateString, userLanguage);
    }
}