@IsTest
private class VT_D1_AsyncDMLTest {
    private static final Integer NUMBER_OF_ACCOUNTS = 10;
    private static final Integer NUMBER_OF_ACCOUNT_FROM_SETUP = 10;

    @TestSetup
    static void setup() {
        insert generateAccounts(NUMBER_OF_ACCOUNTS);
    }

    @IsTest
    static void asyncInsertTest() {
        Test.startTest();
        VT_D1_AsyncDML dmlManager = new VT_D1_AsyncDML(generateAccounts(NUMBER_OF_ACCOUNTS), DMLOperation.INS);
        dmlManager.enqueue();
        Test.stopTest();

        System.assertEquals(NUMBER_OF_ACCOUNTS + NUMBER_OF_ACCOUNT_FROM_SETUP, queryAccounts().size(), 'Account records must be inserted');
    }

    @IsTest
    static void asyncUpdateTest() {
        String NEW_ACCOUNT_NAME_PATTERN = 'New Name_';
        List<Account> accounts = queryAccounts();
        for (Integer i = 0; i < accounts.size(); i ++) {
            accounts.get(i).Name = NEW_ACCOUNT_NAME_PATTERN + i;
        }

        Test.startTest();
        VT_D1_AsyncDML dmlManager = new VT_D1_AsyncDML(accounts, DMLOperation.UPD).setAllOrNothing(true);
        dmlManager.enqueue();
        Test.stopTest();

        accounts = queryAccounts();
        System.assert(accounts.get(0).Name.contains(NEW_ACCOUNT_NAME_PATTERN), 'Account records must be updated');
    }

    @IsTest
    static void asyncUpsertTest() {
        Test.startTest();
        VT_D1_AsyncDML dmlManager = new VT_D1_AsyncDML(queryAccounts(), DMLOperation.UPS).setAllOrNothing(true);
        dmlManager.enqueue();
        Test.stopTest();

        System.assertEquals(NUMBER_OF_ACCOUNTS, queryAccounts().size(), 'Account records must be inserted or updated');
    }

    @IsTest
    static void asyncDeleteTest() {
        Test.startTest();
        VT_D1_AsyncDML dmlManager = new VT_D1_AsyncDML(queryAccounts(), DMLOperation.DEL).setAllOrNothing(true);
        dmlManager.enqueue();
        Test.stopTest();

        System.assertEquals(0, queryAccounts().size(), 'Account records must be deleted');
    }

    @IsTest
    static void asyncUndeleteTest() {
        List<Account> accounts = queryAccounts();
        delete accounts;

        Test.startTest();
        VT_D1_AsyncDML dmlManager = new VT_D1_AsyncDML(accounts, DMLOperation.UND).setAllOrNothing(true);
        dmlManager.enqueue();
        Test.stopTest();

        System.assertEquals(NUMBER_OF_ACCOUNTS, queryAccounts().size(), 'Account records must be undeleted');
    }

    @IsTest
    static void asyncInsertWithBatchSizeSetTest() {
        try {
            Test.startTest();
            VT_D1_AsyncDML dmlManager = new VT_D1_AsyncDML(generateAccounts(NUMBER_OF_ACCOUNTS), DMLOperation.INS);
            dmlManager.setAllOrNothing(true);
            dmlManager.setBatchSize(5);
            dmlManager.enqueue();
            Test.stopTest();
        } catch (Exception ex) {
            // Intends to avoid the 'Maximum stack depth has been reached' exception during the apex unit testing
            System.debug('VT_D1_AsyncDMLTest::asyncInsertWithBatchSizeSetTest::67 ' + ex.getMessage());
        } finally {
            System.assert(! queryAccounts().isEmpty(), 'Account records must be inserted');
        }
    }

    @IsTest
    static void asyncInsertWithErrorsTest(){
        Test.startTest();
        List<Account> accounts = generateAccounts(NUMBER_OF_ACCOUNTS);
        accounts[0].Name = null;
        VT_D1_AsyncDML dmlManager = new VT_D1_AsyncDML(accounts, DMLOperation.INS);
        dmlManager.addRetryError('Not a real error');
        dmlManager.setRetryLimit(1);
        dmlManager.setRetryErrors(new List<String>{'Not a real error'});
        dmlManager.setSemaphoreName('SemaphoreTest');
        dmlManager.setSemaphoreOnlyOnRetry(false);
        dmlManager.setAllOrNothing(false);
        dmlManager.setLoggingEnabled(true);
        dmlManager.setLogFullRecord(true);
        dmlManager.enqueue();
        Test.stopTest();

        System.assertEquals(NUMBER_OF_ACCOUNTS + NUMBER_OF_ACCOUNT_FROM_SETUP - 1, queryAccounts().size(), 'Account records must be inserted');
    }

    private static List<Account> generateAccounts(Integer numberOfAccount) {
        Long timeStamp = Datetime.now().getTime();
        List<Account> accounts = new List<Account>();
        for (Integer i = 0; i < numberOfAccount; i ++) {
            accounts.add(new Account(Name = 'Test Account_' + i + timeStamp));
        }
        return accounts;
    }

    private static List<Account> queryAccounts() {
        return [SELECT Id, Name FROM Account];
    }
}