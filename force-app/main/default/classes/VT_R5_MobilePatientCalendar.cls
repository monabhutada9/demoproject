/**
 * Created by Yuliya Yakushenkova on 9/23/2020.
 */

public class VT_R5_MobilePatientCalendar extends VT_R5_MobileRestResponse implements VT_R5_MobileLibrary.Routable {

    private static RequestParams requestParams;

    public List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/calendar'
        };
    }

    public VT_R5_MobileRestResponse versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        return get_v1();
                    }
                }
            }
        }
        return null;
    }

    public void initService() {
        requestParams = new RequestParams();
        parseBody();
        parseParams();
    }

    public Map<String, String> getMinimumVersions() {
        return null;
    }

    private VT_R5_MobileRestResponse get_v1() {
        DatetimeValues datetimeValues = new DatetimeValues(requestParams.startDate, requestParams.endDate);

        Object response;
        if (requestParams.visitIds != null) {
            response = VT_R5_PatientCalendarService.getInstance().getVisitsWithinByRangeAndByEventIds(
                    requestParams.visitIds, datetimeValues.startDate, datetimeValues.endDate
            );
        } else {
            response = VT_R5_PatientCalendarService.getInstance().getVisitsByRange(
                    datetimeValues.startDate, datetimeValues.endDate
            );
        }
        return this.buildResponse(response);
    }

    private void parseBody() {}

    private void parseParams() {
        if (request.params.containsKey('startDate')) requestParams.setStartDate(request.params.get('startDate'));
        if (request.params.containsKey('endDate')) requestParams.setEndDate(request.params.get('endDate'));
        if (request.params.containsKey('visitIds')) requestParams.setVisitIds(request.params.get('visitIds'));
    }

    private class RequestParams {
        private Long startDate;
        private Long endDate;
        private Set<Id> visitIds;

        private void setStartDate(String startDateStr) {
            this.startDate = Long.valueOf(startDateStr);
        }

        private void setEndDate(String endDateStr) {
            this.endDate = Long.valueOf(endDateStr);
        }

        private void setVisitIds(String eventsIds) {
            if (eventsIds != null) {
                this.visitIds = new Set<Id>();
                String visitIdPrefix = VTD1_Actual_Visit__c.getSObjectType().getDescribe().getKeyPrefix();
                for (String s : eventsIds.remove(' ').remove('"').remove('\'').split(',')) {
                    if (s.startsWith(visitIdPrefix)) {
                        this.visitIds.add(s);
                    }
                }
            }
        }
    }

    private class DatetimeValues {
        private Integer offset;
        private Datetime startDate;
        private Datetime endDate;

        private DatetimeValues(Long startDate, Long endDate) {
            offset = UserInfo.getTimeZone().getOffset(System.now());
            if (startDate != null && endDate != null) {
                this.startDate = Datetime.newInstance(startDate - offset);
                this.endDate = Datetime.newInstance(endDate - offset);
            } else {
                this.startDate = System.now().addHours(offset / 1000 / 60 / 60).date().toStartOfWeek();
                this.endDate = this.startDate.addDays(7);
            }
        }
    }
}