/**
 * Created by Alexander Komarov on 27.02.2019.
 */

public without sharing class VT_D2_EmailDriverTriggerHandler {

    public void onAfterInsert(List<VTD2_Email_Driver__c> recs) {
        System.debug('VERY START ' + recs + ' recs count: ' + recs.size());
        Set<Id> cancelledPatientsIds = queryCancelledPatients(recs);
        List<Id> cancelledDriverIds = new List<Id>();
        // remove emails for cancelled patients
        if (!cancelledPatientsIds.isEmpty()) {
            List<VTD2_Email_Driver__c> newEmailsList = new List<VTD2_Email_Driver__c>();
            for (VTD2_Email_Driver__c e : recs) {
                if (e == null) continue;
                if (e.VTD2_Email_Recepient__c != null && !cancelledPatientsIds.contains(e.VTD2_Email_Recepient__c)) {
                    newEmailsList.add(e);
                } else {
                    cancelledDriverIds.add(e.Id);
                }
            }
            recs = newEmailsList;
        }

        if (!cancelledDriverIds.isEmpty()) {
            System.enqueueJob(new CancelledPatientEmailsDeleter(cancelledDriverIds));
        }

        sendEmail(recs);
    }

    private Set<Id> queryCancelledPatients(List<VTD2_Email_Driver__c> emails) {
        System.debug('NOT VERY BUT START ' + emails + ' emails count: ' + emails.size());
        Set<Id> cancelledPatientsIDs = new Set<Id>();
        Set<Id> ownerIds = new Set<Id>();
        Set<String> patientStatuses = new Set<String>{
                'Dropped', 'Dropped Treatment,Compl F/Up',
                'Completed', 'Compl Treatment,Dropped F/Up', 'Closed'
        };
        String recipientSobjectType;

        for (VTD2_Email_Driver__c e : emails) {
            if (e == null) continue;
            if (e.VTD2_Email_Recepient__c != null) {
                recipientSobjectType = getSObjectType(e.VTD2_Email_Recepient__c);
                if (recipientSobjectType.equals('User')) {
                    ownerIds.add(e.VTD2_Email_Recepient__c);
                }
            }
        }

        if (!ownerIds.isEmpty()) {
            for (Case c : [SELECT VTD1_Patient_User__c FROM Case WHERE Status IN :patientStatuses AND VTD1_Patient_User__c IN :ownerIds]) {
                cancelledPatientsIDs.add(c.VTD1_Patient_User__c);
            }
        }

        return cancelledPatientsIDs;
    }

    public class CancelledPatientEmailsDeleter implements Queueable {
        private List<Id> emailIdsToDelete;

        public CancelledPatientEmailsDeleter(List<Id> emailIdsToDelete) {
            this.emailIdsToDelete = emailIdsToDelete;
        }

        public void execute(QueueableContext context) {
            delete [SELECT Id FROM VTD2_Email_Driver__c WHERE Id IN:emailIdsToDelete];
        }
    }

    private void sendEmail(List<VTD2_Email_Driver__c> recs) {
        System.debug('JUST IN CASE ' + recs + ' recs count: ' + recs.size());

        List<String> contactDrivers = new List<String>();
        List<String> userDrivers = new List<String>();
        List<String> emailTemplateDevNames = new List<String>();

        for (VTD2_Email_Driver__c driver : recs) {
            if (driver.VTD2_Email_Recepient__c != null) {
                String sobjectTypeRecipient = getSObjectType(driver.VTD2_Email_Recepient__c);
                if (sobjectTypeRecipient.equals('Contact')) {
                    contactDrivers.add(driver.VTD2_Email_Recepient__c);
                } else if (sobjectTypeRecipient.equals('User')) {
                    userDrivers.add(driver.VTD2_Email_Recepient__c);
                } else {
                    System.debug('VTD2_Email_Recepient__c have to be User or Contact Id, got ' + sobjectTypeRecipient + ' Id.');
                }
                emailTemplateDevNames.add(driver.VTD2_Email_template_API_Name__c);
            } else {
                System.debug('For VTD2_Email_Driver__c Id: ' + driver.Id + ', VTD2_Email_Recepient__c is null');
                throw new AuraHandledException('Failed to execute email driver, driver id - ' + driver.Id + ' recipient id - ' + driver.VTD2_Email_Recepient__c);
            }
        }

        Map<Id, String> contactIdsToEmails = new Map<Id, String>();
        if (!contactDrivers.isEmpty()) {
            for (Contact con : [SELECT Id,Email FROM Contact WHERE Id in:contactDrivers]) {
                if (con.Email != null) {
                    contactIdsToEmails.put(con.Id, con.Email);
                } else {
                    System.debug('MISSING EMAIL! Contact Id: ' + con.Id);
                }
            }
        }

        Map<Id, String> userIdsToEmails = new Map<Id, String>();
        if (!userDrivers.isEmpty()) {
            for (User u : [SELECT Id,Email FROM User WHERE Id in:userDrivers]) {
                if (u.Email != null) {
                    userIdsToEmails.put(u.Id, u.Email);
                } else {
                    System.debug('MISSING EMAIL! User Id: ' + u.Id);
                }
            }
        }

        Map<String, Id> idToEmailTemplateName = new Map<String, Id>();
        for (EmailTemplate template : [SELECT Id,DeveloperName FROM EmailTemplate WHERE DeveloperName in :emailTemplateDevNames]) {
            idToEmailTemplateName.put(template.DeveloperName, template.Id);
        }


        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();

        List<OrgWideEmailAddress> senderView = [select id from OrgWideEmailAddress]; //Address = 'iqviash@gmail.com'

        for (VTD2_Email_Driver__c email_driver : recs) {
            System.debug('IMPOSSIBLE ' + recs + ' recs count: ' + recs.size());
            if (email_driver == null) continue;
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();

            if (email_driver.VTD2_Email_Recepient__c == null) {
                System.debug('MISSING Recipient for driver ' + email_driver.Id);
                continue;
            }

            String recipientSobjectType = getSObjectType(email_driver.VTD2_Email_Recepient__c);
            String email;

            if (recipientSobjectType.equals('Contact')) {
                message.setTargetObjectId(email_driver.VTD2_Email_Recepient__c);
                email = contactIdsToEmails.get(email_driver.VTD2_Email_Recepient__c);
                if (email == null) continue;
                message.toAddresses = new String[]{
                        email
                };

                if (String.isNotBlank(email_driver.VTD2_Record_Id__c)) {
                    message.setWhatId(email_driver.VTD2_Record_Id__c);
                }
            } else if (recipientSobjectType.equals('User')) {
                message.setTargetObjectId(email_driver.VTD2_Email_Recepient__c);
                email = userIdsToEmails.get(email_driver.VTD2_Email_Recepient__c);
                if (email == null) continue;
                message.toAddresses = new String[]{
                        email
                };
            } else {
                System.debug('VTD2_Email_Recepient__c have to be User or Contact Id, got ' + recipientSobjectType);
            }

            if (String.isNotBlank(email_driver.VTD2_Email_Subject__c)) {
                message.setSubject(email_driver.VTD2_Email_Subject__c);
            }

            if (String.isNotBlank(email_driver.VTD2_Email_Body__c)) {
                message.setHtmlBody(email_driver.VTD2_Email_Body__c);
            }


            if (!senderView.isEmpty()) message.setOrgWideEmailAddressId(senderView.get(0).id);
            message.setUseSignature(false);
            message.setBccSender(false);
            message.setSaveAsActivity(false);
            message.setTemplateId(idToEmailTemplateName.get(email_driver.VTD2_Email_template_API_Name__c));

            mails.add(message);
        }

        if (!mails.isEmpty()) {
            Messaging.sendEmail(mails);
        }
    }

    private String getSObjectType(String str) {
        return Id.valueOf(str).getSobjectType().getDescribe().getName();
    }
}