public with sharing class VT_D2_CreateUpdateSubjectController
{

    public VT_D2_CreateUpdateSubjectController() {

    }

    Id caseId;
    private ApexPages.StandardController standardController;
 
    public VT_D2_CreateUpdateSubjectController(ApexPages.StandardController standardController)
    {
        this.standardController = standardController;
        caseId = ApexPages.CurrentPage().getParameters().get('id');

    }
 
    public PageReference createUpdateCase(){
                    
        caseId = standardController.getId();
        VT_D1_SubjectIntegration.upsertSubject(caseId);


        PageReference casePage = new PageReference('/' + caseId);            
        casePage.setRedirect(true);
        return casePage;

    }    
}