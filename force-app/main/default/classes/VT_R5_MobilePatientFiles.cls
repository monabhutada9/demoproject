/**
 * @author: Alexander Komarov
 * @date: 19.10.2020
 * @description:
 */

public with sharing class VT_R5_MobilePatientFiles extends VT_R5_MobileRestResponse implements VT_R5_MobileRestRouter.Routable{


    public List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/file/{id}'
        };
    }

    public void versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        String requestId = parameters.get('id');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        get_v1(requestId);
                        return;
                    }
                }
            }
        }
        VT_R5_MobileRestRouter.unsupportedVersionResponse();
        this.addError('unsupported version');
        this.sendResponse();
    }

    private void get_v1(String requestId) {
        try {
            ContentVersion contentVersion = [SELECT Id, FileType, VersionData, ContentSize, FileExtension FROM ContentVersion WHERE ContentDocumentId =:requestId ORDER BY CreatedDate DESC LIMIT 1];
            this.buildResponseWOAI(new FileObj(contentVersion));
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    public void initService() {
    }

    private class FileObj {
         String fileType;
         String fileExtension;
         Integer size;
         String content;
         Integer sizeBase64;

         FileObj(ContentVersion cv) {
            this.content = EncodingUtil.base64Encode(cv.VersionData);
            this.fileType = cv.FileType;
            this.fileExtension = cv.FileExtension;
            this.size = cv.ContentSize;
            this.sizeBase64 = content.length();
        }
    }
}