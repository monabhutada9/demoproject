/**
 * Created by user on 11-Nov-20.
 */

@IsTest
public with sharing class VT_R5_SetPrimaryPhoneOnContactBatchTest {

    @IsTest
    public static void SetPrimaryPhoneOnContactBatchTest() {
        Account acc = new Account();
        acc.Name = 'testAccount';
        insert acc;
        Contact con = new Contact();
        con.LastName = 'testBatch';
        con.AccountId = acc.Id;
        insert con;
        VT_D1_Phone__c phone = new VT_D1_Phone__c();
        phone.Account__c = acc.Id;
        phone.PhoneNumber__c = '+123321';
        phone.VTD1_Contact__c = con.Id;
        VT_D1_Phone__c primaryPhone = new VT_D1_Phone__c();
        primaryPhone.Account__c = acc.Id;
        primaryPhone.PhoneNumber__c = '+99999999';
        primaryPhone.IsPrimaryForPhone__c = true;
        primaryPhone.VTD1_Contact__c = con.Id;
        insert new List<VT_D1_Phone__c> {phone, primaryPhone};
        Test.startTest();
        Database.executeBatch(new VT_R5_SetPrimaryPhoneOnContactBatch());
        Test.stopTest();
        con = [SELECT VTR5_PatientPhone__c FROM Contact LIMIT 1];
        System.assertEquals(primaryPhone.PhoneNumber__c, con.VTR5_PatientPhone__c);
    }

}