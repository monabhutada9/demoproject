/**
* Created by Belkovskii Denis on 6/25/2020.
*/

@IsTest
private class VT_R4_CaregiverInitiationActionsTest {
    @TestSetup
    static void setup() {

        System.runAs(new User(Id = UserInfo.getUserId())){
            DomainObjects.Account_t patientAccount = new DomainObjects.Account_t()
                    .setName('testPatientAccount')
                    .setRecordTypeByName('Sponsor');

            DomainObjects.Study_t study = new DomainObjects.Study_t()
                    .addAccount(patientAccount);

            DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
                    .setFirstName('Patient')
                    .setLastName('Patient')
                    .addAccount(patientAccount);

            DomainObjects.User_t patientUser = new DomainObjects.User_t()
                    .addContact(patientContact)
                    .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);

            new DomainObjects.Task_t()
                    .addUser(patientUser)
                    .setStatus('Open');

            DomainObjects.Case_t domainCase =new DomainObjects.Case_t()
                    .addPIUser(new DomainObjects.User_t())
                    .setRecordTypeByName('CarePlan')
                    .addStudy(study)
                    .addVTD1_Patient_User(patientUser)
                    .addContact(patientContact)
                    .addAccount(patientAccount);
            domainCase.persist();

            Account acc = (Account) patientAccount.toObject();
            Case c = (Case) domainCase.toObject();
            acc.HealthCloudGA__CarePlan__c = c.Id;
            update acc;
            System.debug('Account Id is ' + acc.Id);

            Test.startTest();
            DomainObjects.User_t caregiverUser = new DomainObjects.User_t()


                    //.setUserRoleId()


                    .addContact(new DomainObjects.Contact_t()
                            .setFirstName('Caregiver')
                            .setLastName('Caregiver')
                            .setEmail(DomainObjects.RANDOM.getEmail())
                            .setRecordTypeName('Caregiver')
                            .setAccountId(acc.Id)
                    )
                    .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME);
            caregiverUser.persist();
            Test.stopTest();
        }
    }

    @IsTest
    static void insertTriggerTests() {
        Test.startTest();

        //assignToPatientMessageFeedOfStudy() test:
        List<User> caregivers = [
                SELECT Id,
                        Contact.AccountId,
                        Contact.VTD1_Clinical_Study_Membership__c,
                        Contact.VTD1_UserId__c,
                        ContactId
                FROM User
                WHERE Username LIKE '%@scott.com'
                AND Contact.FirstName = 'Caregiver'
                AND Contact.LastName = 'Caregiver'


        ];
        List<HealthCloudGA__CarePlanTemplate__c> cases = [
                SELECT VTD1_Patient_Group_Id__c
                FROM HealthCloudGA__CarePlanTemplate__c
                WHERE VTD1_Primary_Sponsor__c = :caregivers[0].Contact.AccountId
        ];
        List<VTD1_Chat_Member__c> chatMembers = [
                SELECT VTD1_User__c, VTD1_Chat__c
                FROM VTD1_Chat_Member__c
                WHERE VTD1_User__c = :caregivers[0].Id
        ];
//        System.assertEquals(chatMembers[0].VTD1_User__c, caregivers[0].Id);
//        System.assertEquals(chatMembers[0].VTD1_Chat__c, cases[0].VTD1_Patient_Group_Id__c);

        // updateUserIdCsmOnContact() test:
        List<Account> accounts = [
                SELECT Id,
                        HealthCloudGA__CarePlan__c,
                        HealthCloudGA__CarePlan__r.VTD1_Study__c,
                        HealthCloudGA__CarePlan__r.VTD1_Patient_User__c
                FROM Account
                WHERE Id = :caregivers[0].Contact.AccountId
        ];


        System.assertEquals(caregivers[0].Contact.VTD1_Clinical_Study_Membership__c, accounts[0].HealthCloudGA__CarePlan__c);

        // accessToPatientObjects() test:
        List<VTD1_Study_Sharing_Configuration__c> studyShares = [
                SELECT VTD1_Study__c
                FROM VTD1_Study_Sharing_Configuration__c
                WHERE VTD1_User__c = :caregivers[0].Id
                AND VTD1_Access_Level__c = 'Edit'
        ];
        System.assertEquals(studyShares[0].VTD1_Study__c, accounts[0].HealthCloudGA__CarePlan__r.VTD1_Study__c);
        List<UserShare> userShares = [SELECT UserId FROM UserShare WHERE RowCause = 'Manual' AND UserAccessLevel = 'Edit'];
        System.assertEquals(userShares[0].UserId, accounts[0].HealthCloudGA__CarePlan__r.VTD1_Patient_User__c);
        List<AccountShare> accountShares = [
                SELECT UserOrGroupId, AccountId
                FROM AccountShare
                WHERE UserOrGroupId = :caregivers[0].Id
        ];
        System.assertEquals(accountShares[0].AccountId, caregivers[0].Contact.AccountId);

        //createTaskRelations() test:
        List<TaskRelation> taskRelations = [SELECT TaskId FROM TaskRelation WHERE RelationId = :caregivers[0].ContactId];
        List<Task> tasks = [SELECT Id FROM Task WHERE OwnerId = :accounts[0].HealthCloudGA__CarePlan__r.VTD1_Patient_User__c];
        System.assertEquals(taskRelations[0].TaskId, tasks[0].Id);

        Test.stopTest();
    }

    @IsTest
    static void updateTriggerTests() {
        Test.startTest();
        System.runAs(new User(Id = UserInfo.getUserId())){
            User cgUser = [
                    SELECT VTD2_UserTimezone__c, FirstName, LastName, AccountId, TimeZoneSidKey
                    FROM User WHERE VTD1_Profile_Name__c = 'Caregiver'
                    AND Username LIKE '%@scott.com'
                    AND Contact.FirstName = 'Caregiver'
                    AND Contact.LastName = 'Caregiver'
            ];
            cgUser.VTD2_UserTimezone__c = 'America/New_York';
            cgUser.FirstName = 'Humpty';
            cgUser.LastName = 'Dumpty';
            update cgUser;

            User cgUserAfterUpdate = [
                    SELECT VTD2_UserTimezone__c,
                            FirstName,
                            LastName,
                            AccountId,
                            TimeZoneSidKey,
                            VTD1_Full_Name__c
                    FROM User WHERE VTD1_Profile_Name__c = 'Caregiver'
                    AND Username LIKE '%@scott.com'
                    AND Contact.FirstName = 'Caregiver'
                    AND Contact.LastName = 'Caregiver'
            ];

            //setCaregiverNameAndTimezoneToCase() test:
            List<Case> cases = [SELECT Caregiver_s_Name__c, Caregiver_s_TimeZone__c FROM Case LIMIT 1];
            System.assertEquals(cases[0].Caregiver_s_Name__c, cgUserAfterUpdate.VTD1_Full_Name__c);
        }
        Test.stopTest();
    }
}