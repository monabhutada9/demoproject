public with sharing class VT_D1_ApproveRejectDocumentController {
	/*public static final string S_ERROR = 'ERROR';
	public static final string S_SUCCESS = 'SUCCESS';
	public static final string S_USERNAME = 'eshane@vt.com.conf';
	
	public class MyException extends Exception {}
	
	public class JSONObject {

     //public List<CompanyContactsWrapper> CompanyContacts;
	     public String access_token;
	     public String instance_url;

	}
	
    @AuraEnabled
    public static String checkPassword(String password){
    	list<User> users = [SELECT ContactId, FirstName, Username FROM User WHERE Id=:UserInfo.getUserId()];
    	
    	if(users==null || users.isEmpty()) return null;
    	
    	String Username = S_USERNAME;//'eshane@vt.com.conf';//users[0].Username;
    	String clientId='3MVG9PE4xB9wtoY8JP3iPACiP_xMETP37Khxmh3jKl6G12.K8FwUKr4GflbZADZH2mve5K49v9LSW1lwDgW9m';
       	String clientSecret='4223779387703913275';
       	
       	try{
       		/*
       		JSONObject oauth = oauthLogin('https://conf-iqviavirtualtrials.cs70.force.com/pi', 
            clientId,
            clientSecret, 
            username, 
            password);
       		// comment end
       		
       		JSONObject oauth = oauthLogin('https://test.salesforce.com', 
            clientId,
            clientSecret, 
            username, 
            password);
         
        
	        String accessToken = oauth.access_token;
	        String instanceUrl = oauth.instance_url;
	        System.debug('!!! instanceUrl='+instanceUrl);   
	            
			
	        //GET Request to sandbox   
			HttpRequest req = new HttpRequest(); 
	 
	        req.setMethod('GET');
	        req.setEndpoint(instanceUrl+'/services/data/v28.0/query/?q=SELECT+name,+VTD1_LegalFirstName__c+from+Account+where+name=\'AC1\'');
	        //req.setHeader('Authorization', 'OAuth '+UserInfo.getSessionId());
	        
	        req.setHeader('Authorization', 'OAuth '+accessToken);
			//req.setHeader('Content-Type','application/json; charset=utf-8' );
	        Http http = new Http();
	  
	        HTTPResponse res = http.send(req);
	
	        System.debug('BODY: '+res.getBody());
	        System.debug('STATUS:'+res.getStatus());
	        System.debug('STATUS_CODE:'+res.getStatusCode());
	        
	        return S_SUCCESS;///res.getBody();
       	}
       	catch(AuraHandledException ex){
       		system.debug('!!! EXCEPTION ERROR');
       		throw new AuraHandledException('Authentification is failed!!!');//return S_ERROR;
       	}
    }
    
    private static JSONObject oauthLogin(String loginUri, String clientId,
        String clientSecret, String username, String password) {
        HttpRequest req = new HttpRequest(); 
 
        req.setMethod('POST');
        req.setEndpoint(loginUri+'/services/oauth2/token');
        req.setBody('grant_type=password' +
            '&client_id=' + clientId +
            '&client_secret=' + clientSecret +
            '&username=' + EncodingUtil.urlEncode(username, 'UTF-8') +
            '&password=' + EncodingUtil.urlEncode(password, 'UTF-8'));
    
        Http http = new Http();
  
        HTTPResponse res = http.send(req);

        System.debug('BODY: '+res.getBody());
        System.debug('STATUS:'+res.getStatus());
        System.debug('STATUS_CODE:'+res.getStatusCode());
        
        if(res.getStatusCode()==200){
        	return (JSONObject)JSON.deserialize(res.getBody(), JSONObject.class);//new JSONObject(res.getBody());	
        }
        else{
        	throw new AuraHandledException('ERROR');//MyException('test exception');
        	//return S_ERROR;
        }
    }
    
 
    public static final String LOGIN_DOMAIN = 'test'; //other options: test, prerellogin.pre
    //public String username {get{ return UserInfo.getUsername(); }}
    //public transient String password {get;set;}
 
 	@AuraEnabled
    public static String doVerify(String password){
      
        String username = UserInfo.getUsername();
        system.debug('In doVerify: username='+username+', password='+password);
        
    	HttpRequest request = new HttpRequest();
    	//request.setEndpoint('https://test.salesforce.com/services/Soap/c/42.0');///0DF3D0000008RiC
        request.setEndpoint('https://' + LOGIN_DOMAIN + '.salesforce.com/services/Soap/c/42.0');
        //request.setEndpoint('https://conf-iqviavirtualtrials.cs70.force.com/pi/Soap/u/37.0/');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('SOAPAction', '""');
        String buildSoap = buildSoapLogin(username,password);
        System.debug('!!! buildSoap='+buildSoap);
        request.setBody(buildSoap);
 
        //basically if there is a loginResponse element, then login succeeded; else there
        //  would be soap fault element after body
        final Boolean verified = (new Http()).send(request).getBodyDocument().getRootElement()
          .getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/')
          .getChildElement('loginResponse','urn:enterprise.soap.sforce.com') != null;
 
        if(verified){
        	system.debug('!!! SUCCESS');
        	return S_SUCCESS;
        }
        else{   
       		system.debug('!!! EXCEPTION ERROR');
       		throw new AuraHandledException('Authentification is failed!!!');//return S_ERROR;
       	}
        return '';
    }
    
    //0DB3D00000003mlWAA commId
    //00D3D000000DGN9 00D3D000000DGN9UAO
    /*
    public static String buildSoapLogin(String username, String password){
    	//String s='<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"urn:partner.soap.sforce.com\"><SOAP-ENV:Header><ns1:LoginScopeHeader><ns1:organizationId>00D3D000000DGN9UAO</ns1:organizationId></ns1:LoginScopeHeader></SOAP-ENV:Header><SOAP-ENV:Body><ns1:login><ns1:username>wilt.dustin@pi.conf</ns1:username><ns1:password>zaxscd33</ns1:password></ns1:login></SOAP-ENV:Body></SOAP-ENV:Envelope>';
    	//String s='<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"urn:partner.soap.sforce.com\"><Header><ns1:LoginScopeHeader><ns1:organizationId>00D3D000000DGN9UAO</ns1:organizationId></ns1:LoginScopeHeader></Header><Body><ns1:login><ns1:username>wilt.dustin@pi.conf</ns1:username><ns1:password>zaxscd33</ns1:password></ns1:login></Body></Envelope>';
    	String s='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:enterprise.soap.sforce.com"><soapenv:Body><urn:login><urn:username>wilt.dustin@pi.conf</urn:username><urn:password>zaxscd33</urn:password></urn:login></soapenv:Body></soapenv:Envelope>';
    	return s;
    }
    // comment end
    
    
    public static String buildSoapLogin(String username, String password){
        XmlStreamWriter w = new XmlStreamWriter();
        //LoginScopeHeader
        w.writeStartElement('', 'login', 'urn:enterprise.soap.sforce.com');//partner.soap
        w.writeNamespace('', 'urn:enterprise.soap.sforce.com');
        w.writeStartElement('', 'username', 'urn:enterprise.soap.sforce.com');
        w.writeCharacters(username);
        w.writeEndElement();
        w.writeStartElement('', 'password', 'urn:enterprise.soap.sforce.com');
        w.writeCharacters(password);
        w.writeEndElement();
        w.writeEndElement();
         
         /*
         String header='<Header><LoginScopeHeader><organizationId>00D3D000000DGN9UAO</organizationId><portalId>0DB3D00000003mlWAA</portalId></LoginScopeHeader></Header>';
         String xmlOutput =
              '<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">'+header
              +'<Body>'
            + w.getXmlString()
            + '</Body></Envelope>';
          //   comment end
         
        String xmlOutput =
              '<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Body>'
            + w.getXmlString()
            + '</Body></Envelope>';
            
        w.close();
        return xmlOutput;
    }
    
    
    
    @AuraEnabled
    public static void approveDocument(Id docId, String password){
    	system.debug('docId='+docId);
    	system.debug('password='+password);
    	if(String.isBlank(docId)) return;
    	
    	list<User> users = [SELECT ContactId, FirstName, Username FROM User WHERE Id=:UserInfo.getUserId()];
    	
    	if(users==null || users.isEmpty()) return;
    	
    	String username = S_USERNAME;//= users[0].Username;
    	
    	list<VTD1_Document__c> docList = [SELECT Id, VTD1_Eligibility_Assessment_Status__c FROM VTD1_Document__c WHERE Id=:docId];
    	
    	if(docList==null || docList.isEmpty()) return;
    	
    	VTD1_Document__c doc = docList[0];
    	doc.VTD1_Eligibility_Assessment_Status__c='Eligible';
    	update doc;
    	
    	String signature = generateSignature(username, password);
    	
    	VTD1_Approval_History_Doc__c ah = new VTD1_Approval_History_Doc__c();
    	ah.VTD1_DocumentId__c=doc.Id;
    	ah.VTD1_Status__c='Eligible';
    	ah.VTD1_Completed_Date__c=Datetime.now();
    	ah.ApproverId__c = UserInfo.getUserId();
    	ah.VTD1_Signature__c = signature;
    	
    	insert ah;
    	
    	return;
    }
    
    public static String generateSignature(String username, String password){
    	//using the AES algorithm
    	//use password as unique key to encrypt
    	String key=password;
    	
    	//The length of the private key must match to the specified algorithm.
    	if(key.length()<16){
    		Integer addLen = 16-key.length();
    		
    		for(Integer i=0; i<addLen; i++){
    			key = key+'1';
    		}
    	}
    	else{
    		key = key.substring(0, 16);
    	}
    	system.debug('key='+key);
    	
    	//Get an AES key
    	Blob cryptoKey = Blob.valueOf(key);
    	
    	// Generate the data to be encrypted
		Blob data = Blob.valueOf(username);
		
		// Encrypt the data and have Salesforce.com generate the initialization vector
        Blob encryptedData = Crypto.encryptWithManagedIV('AES128', cryptoKey , data );
        system.debug('encryptedData='+String.valueOf(encryptedData));
        String signature = EncodingUtil.base64Encode(encryptedData);//b64Data

        system.debug('!!! signature='+signature);
        return signature;
    } */
}