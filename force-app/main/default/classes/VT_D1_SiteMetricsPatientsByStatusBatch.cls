/**
* @author: Carl Judge
* @date: 22-Jun-18
* @description: Batch job to delete and recreate VTD1_SiteMetricsPatientsbyStatus__c on a daily basis
**/

global without sharing class VT_D1_SiteMetricsPatientsByStatusBatch implements Database.Batchable<sObject>, Database.Stateful, Schedulable {

    private final static Integer SCHEDULED_BATCH_SIZE = 10;
    private final static String RECORD_TYPE_ID_SITE_METRICS_PATIENT_BY_STATUS_PI = Schema.SObjectType.VTD1_SiteMetricsPatientsbyStatus__c.getRecordTypeInfosByDeveloperName().get('VTR2_PI').getRecordTypeId();
    private final static String RECORD_TYPE_ID_SITE_METRICS_PATIENT_BY_STATUS_VS = Schema.SObjectType.VTD1_SiteMetricsPatientsbyStatus__c.getRecordTypeInfosByDeveloperName().get('VTR2_VS').getRecordTypeId();

    private Map<Id, Map<String, Integer>> studyTotalsByStatus = new Map<Id, Map<String, Integer>>();

    // Daily
    // system.schedule('Site Metrics - Patients by Status', '0 0 1 * * ?',  new VT_D1_SiteMetricsPatientsByStatusBatch());
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new VT_D1_SiteMetricsPatientsByStatusBatch(), SCHEDULED_BATCH_SIZE);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        getStudyTotalsByStatus();

        String query = 'SELECT Id, isActive FROM User WHERE Profile.Name = \'Primary Investigator\'';
       // if (Test.isRunningTest()) {
      //      query += ' LIMIT :SCHEDULED_BATCH_SIZE';
       // }

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<User> scope){
        system.debug('scope2=' + scope);
        delete [SELECT Id FROM VTD1_SiteMetricsPatientsbyStatus__c WHERE OwnerId IN :scope];
        system.debug('scope=' + scope);
        Set<Id> uIds = new Set<Id>();
        for (User item : scope) {
            if (item.isActive) { uIds.add(item.Id); }
        }


        Map<Id, Map<Id, Map<String, Integer>>> studyTotalsByStatusAndPI = new Map<Id, Map<Id, Map<String, Integer>>>();
        Map<Id, Map<Id, Map<String, Integer>>> siteTotalsByStatusAndPI = new Map<Id, Map<Id, Map<String, Integer>>>();
        Map<Id, Id> siteToStudyMap = new Map<Id, Id>();


        for (Case item : [
                SELECT Id, Status, VTD1_Virtual_Site__c, VTD1_Study__c, VTD1_PI_user__c, VTD1_Backup_PI_User__c
            FROM Case
            WHERE RecordType.DeveloperName = 'CarePlan'
                AND VTD1_Virtual_Site__c != null
                AND VTD1_Study__c != null
                AND (VTD1_PI_user__c IN :uIds OR VTD1_Backup_PI_User__c IN :uIds)
        ]) {
            if (uIds.contains(item.VTD1_PI_user__c)) {
                incrementPiCountStudy(studyTotalsByStatusAndPI, item.VTD1_PI_user__c, item);
                incrementPiCountSite(siteTotalsByStatusAndPI, item.VTD1_PI_user__c, item);
            }

            if (item.VTD1_Backup_PI_User__c != item.VTD1_PI_user__c && uIds.contains(item.VTD1_Backup_PI_User__c)) {
                incrementPiCountStudy(studyTotalsByStatusAndPI, item.VTD1_Backup_PI_User__c, item);
            }
            if (! siteToStudyMap.containsKey(item.VTD1_Virtual_Site__c)){
                siteToStudyMap.put(item.VTD1_Virtual_Site__c, item.VTD1_Study__c);
            }
        }

        List<VTD1_SiteMetricsPatientsbyStatus__c> newMetrics = new List<VTD1_SiteMetricsPatientsbyStatus__c>();
        for (Id uId : uIds) {
            if (studyTotalsByStatusAndPI.containsKey(uId)) {
                for (Id studyId : studyTotalsByStatusAndPI.get(uId).keySet()) {
                    for (String status : studyTotalsByStatusAndPI.get(uId).get(studyId).keySet()) {
                        newMetrics.add(new VTD1_SiteMetricsPatientsbyStatus__c(
                                RecordTypeId = RECORD_TYPE_ID_SITE_METRICS_PATIENT_BY_STATUS_PI,
                            OwnerId = uId,
                            Name = status,
                            VTD1_StudyId__c = studyId,
                            VTD1_NumberofPatients__c = studyTotalsByStatusAndPI.get(uid).get(studyId).get(status),
                            VTD1_TotalNumberofPatients__c = this.studyTotalsByStatus.get(studyId).get(status)
                        ));
                    }
                }
            }
            if (siteTotalsByStatusAndPI.containsKey(uId)) {
                for (Id siteId : siteTotalsByStatusAndPI.get(uId).keySet()) {
                    for (String status : siteTotalsByStatusAndPI.get(uId).get(siteId).keySet()) {
                        newMetrics.add(new VTD1_SiteMetricsPatientsbyStatus__c(
                                RecordTypeId = RECORD_TYPE_ID_SITE_METRICS_PATIENT_BY_STATUS_VS,
                                OwnerId = uId,
                                Name = status,
                                VTR2_VirtualSite__c = siteId,
                                VTD1_NumberofPatients__c = siteTotalsByStatusAndPI.get(uid).get(siteId).get(status),
                                VTD1_TotalNumberofPatients__c = this.studyTotalsByStatus.get(siteToStudyMap.get(siteId)).get(status)
                        ));
                    }
                }
            }
        }

        if (!newMetrics.isEmpty()) { insert newMetrics; }
    }

    global void finish(Database.BatchableContext bc){

    }

    private void getStudyTotalsByStatus() {
        for (AggregateResult item : [
            SELECT Count(Id) cnt, VTD1_Study__c, Status
            FROM Case
            WHERE RecordType.DeveloperName = 'CarePlan'
                AND VTD1_Study__c != null
            GROUP BY VTD1_Study__c, Status
        ]) {
            Integer cnt = (Integer)item.get('cnt');
            Id studyId = (Id)item.get('VTD1_Study__c');
            String status = (String)item.get('Status');

            if (! this.studyTotalsByStatus.containsKey(studyId)) {
                this.studyTotalsByStatus.put(studyId, new Map<String, Integer>());
            }
            this.studyTotalsByStatus.get(studyId).put(status, cnt);
        }
    }

    private void incrementPiCountStudy (Map<Id, Map<Id, Map<String, Integer>>> studyTotalsByStatusAndPI, Id uId, Case cas) {
        if (! studyTotalsByStatusAndPI.containsKey(uId)) {
            studyTotalsByStatusAndPI.put(uid, new Map<Id, Map<String, Integer>>());
        }
        if (! studyTotalsByStatusAndPI.get(uid).containsKey(cas.VTD1_Study__c)) {
            studyTotalsByStatusAndPI.get(uId).put(cas.VTD1_Study__c, new Map<String, Integer>());
        }

        if (! studyTotalsByStatusAndPI.get(uId).get(cas.VTD1_Study__c).containsKey(cas.Status)) {
            studyTotalsByStatusAndPI.get(uId).get(cas.VTD1_Study__c).put(cas.Status, 1);
        } else {
            Integer currentCount = studyTotalsByStatusAndPI.get(uId).get(cas.VTD1_Study__c).get(cas.Status);
            studyTotalsByStatusAndPI.get(uId).get(cas.VTD1_Study__c).put(cas.Status, ++currentCount);
        }
    }

    private void incrementPiCountSite (Map<Id, Map<Id, Map<String, Integer>>> siteTotalsByStatusAndPI, Id uId, Case cas) {
        if (! siteTotalsByStatusAndPI.containsKey(uId)) {
            siteTotalsByStatusAndPI.put(uid, new Map<Id, Map<String, Integer>>());
        }
        if (! siteTotalsByStatusAndPI.get(uid).containsKey(cas.VTD1_Virtual_Site__c)) {
            siteTotalsByStatusAndPI.get(uId).put(cas.VTD1_Virtual_Site__c, new Map<String, Integer>());
        }

        if (! siteTotalsByStatusAndPI.get(uId).get(cas.VTD1_Virtual_Site__c).containsKey(cas.Status)) {
            siteTotalsByStatusAndPI.get(uId).get(cas.VTD1_Virtual_Site__c).put(cas.Status, 1);
        } else {
            Integer currentCount = siteTotalsByStatusAndPI.get(uId).get(cas.VTD1_Virtual_Site__c).get(cas.Status);
            siteTotalsByStatusAndPI.get(uId).get(cas.VTD1_Virtual_Site__c).put(cas.Status, ++currentCount);
        }
    }
}