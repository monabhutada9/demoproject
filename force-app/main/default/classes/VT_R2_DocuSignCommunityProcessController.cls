public with sharing class VT_R2_DocuSignCommunityProcessController {

    @AuraEnabled
    public static SigningEnvelope getSigningEnvelope(Id recordId) {
        if (String.isEmpty(recordId)) {
            return new SigningEnvelope();
        }
        return new SigningEnvelope(recordId)
                .addDocument(new SigningDocument(recordId));
    }

    @AuraEnabled
    public static SigningEnvelope saveEnvelope(String envelopeJson) {
        SigningEnvelope envelope = (SigningEnvelope) JSON.deserializeStrict(envelopeJson, SigningEnvelope.class);
        envelope.persist();
        return envelope;
    }

    @AuraEnabled
    public static ChunkedDocument createChunkedUpload(final String dataChunk) {
        DocuSignApi api = new DocuSignApi();
        return api.createNewChunkedDocument(dataChunk);
    }

    @AuraEnabled
    public static ChunkedDocument addDocumentChunk(final String chunkedDocumentJson, final String uploadPartSeq) {
        ChunkedDocument document = (ChunkedDocument) JSON.deserializeStrict(chunkedDocumentJson, ChunkedDocument.class);
        DocuSignApi api = new DocuSignApi();
        return api.updateChunkedDocument(document, uploadPartSeq);
    }

    @AuraEnabled
    public static String commitAndSubmitChunkedDocument(final String envelopeJson, final String chunkedDocumentJson) {
        SigningEnvelope envelope = (SigningEnvelope) JSON.deserializeStrict(envelopeJson, SigningEnvelope.class);
        ChunkedDocument document = (ChunkedDocument) JSON.deserializeStrict(chunkedDocumentJson, ChunkedDocument.class);

        DocuSignApi api = new DocuSignApi();
        ChunkedUploadResponse commitResponse = api.commitChunkedDocument(document);
        return api.submitEnvelope(envelope, commitResponse.chunkedUploadUri);
    }

    @AuraEnabled
    public static String submitEnvelope(String envelopeJson) {
        SigningEnvelope envelope = (SigningEnvelope) JSON.deserializeStrict(envelopeJson, SigningEnvelope.class);
        DocuSignApi api = new DocuSignApi();
        return api.submitEnvelope(envelope, null);
    }

    @AuraEnabled
    public static List<LookupSearchResult> find(String searchWord) {
        return new UserLookup().find(searchWord);
    }

    @AuraEnabled(Cacheable=true)
    public static DocuSignOptions getOptions() {
        return new DocuSignOptions();
    }

    @AuraEnabled(Cacheable=true)
    public static LookupSearchResult getCurrentUser() {
        return new UserLookup().getByUserId(UserInfo.getUserId());
    }


    /**
     * Set of classes to work with Envelopes
     */
    public without sharing class SigningEnvelope {

        @AuraEnabled public SigningRecord signingRecord;

        /**
         * SigningDocuments is a List for future support of multiple document signing.
         */
        @AuraEnabled public List<SigningDocument> signingDocuments;
        @AuraEnabled public List<EnvelopeRecipient> recipients;
        @AuraEnabled public EnvelopeMessage message;
        @AuraEnabled public dsfs__DocuSign_Envelope__c persistedEnvelope;

        public SigningEnvelope() {
            this.signingRecord = new SigningRecord();
            this.signingDocuments = new List<SigningDocument>();
            this.recipients = new List<EnvelopeRecipient>();
            this.message = new EnvelopeMessage();
        }

        public SigningEnvelope(Id recordId) {
            this.signingRecord = new SigningRecord(recordId);
            this.signingDocuments = new List<SigningDocument>();
            this.recipients = new List<EnvelopeRecipient>();
            this.message = new EnvelopeMessage();
        }

        public SigningEnvelope addDocument(SigningDocument signingDocument) {
            this.signingDocuments.add(signingDocument);
            return this;
        }

        public void persist() {
            try {
                dsfs__DocuSign_Envelope__c envelope = this.prepareEnvelope();
                insert envelope;

                this.persistedEnvelope = envelope;

                List<dsfs__DocuSign_Envelope_Document__c> documents = new List<dsfs__DocuSign_Envelope_Document__c>();
                for (SigningDocument document : this.signingDocuments) {
                    documents.add(this.prepareDocument(envelope.Id, document));
                }
                insert documents;

                List<dsfs__DocuSign_Envelope_Recipient__c> recipients = new List<dsfs__DocuSign_Envelope_Recipient__c>();
                for (EnvelopeRecipient recipient : this.recipients) {
                    recipients.add(this.prepareRecipient(envelope.Id, recipient));
                }
                insert recipients;
            } catch (Exception ex) {
                System.debug(ex);
                throw new AuraHandledException(ex.getMessage());
            }
        }

        public VT_D2_RequestBuilder_DocuSign.Envelope toDocusignEnvelope(final String remoteUrl) {
            VT_D2_RequestBuilder_DocuSign.Envelope envelope = new VT_D2_RequestBuilder_DocuSign.Envelope();
            envelope.emailSubject = this.message.subject;
            envelope.emailBlurb = this.message.message;
            envelope.addRecId(signingRecord.recordId);
            envelope.documents = new List<VT_D2_RequestBuilder_DocuSign.DSDocument>();
            Integer i = 1;
            for (SigningDocument document : this.signingDocuments) {
                VT_D2_RequestBuilder_DocuSign.DSDocument dsDocument = new VT_D2_RequestBuilder_DocuSign.DSDocument();
                dsDocument.documentId = String.valueOf(i++);
                ContentVersion version;
                try {
                    if (remoteUrl == null || String.isEmpty(remoteUrl)) {
                    version = [
                            SELECT Id, VersionData, FileExtension
                            FROM ContentVersion
                            WHERE ContentDocumentId = :document.contentDocumentId
                    ][0];
                        dsDocument.documentBase64 = EncodingUtil.base64Encode(version.VersionData);
                    } else {
                        version = [
                                SELECT Id, FileExtension
                                FROM ContentVersion
                                WHERE ContentDocumentId = :document.contentDocumentId
                        ][0];
                        dsDocument.remoteUrl = remoteUrl;
                    }
                    dsDocument.name = document.title + '.' + version.FileExtension;
                } catch (Exception exc) {
                    throw new AuraHandledException(exc.getMessage());
                }
                dsDocument.fileExtension = version.FileExtension;
                envelope.documents.add(dsDocument);
            }
            envelope.recipients = new VT_D2_RequestBuilder_DocuSign.Recipients();
            envelope.recipients.signers = new List<VT_D2_RequestBuilder_DocuSign.Signer>();
            i = 1;
            for (EnvelopeRecipient recipient : this.recipients) {
                VT_D2_RequestBuilder_DocuSign.Signer signer = new VT_D2_RequestBuilder_DocuSign.Signer();
                signer.name = recipient.name;
                signer.email = recipient.email;
                signer.recipientId = String.valueOf(i++);
                signer.routingOrder = String.valueOf(recipient.order);
                signer.tabs = new VT_D2_RequestBuilder_DocuSign.Tabs();
                envelope.recipients.signers.add(signer);
            }
            return envelope;
        }

        public String toDocusignEnvelopeJson(final String remoteUrl) {
            return JSON.serialize(this.toDocusignEnvelope(remoteUrl), true);
        }

        private dsfs__DocuSign_Envelope__c prepareEnvelope() {
            dsfs__DocuSign_Envelope__c envelope = new dsfs__DocuSign_Envelope__c();
            envelope.dsfs__DocuSign_Email_Subject__c = this.message.subject;
            envelope.dsfs__DocuSign_Email_Message__c = this.message.message;
            envelope.dsfs__Source_Object__c = this.signingRecord.recordId;

            return envelope;
        }

        private dsfs__DocuSign_Envelope_Document__c prepareDocument(Id envelopeId, SigningDocument signingDocument) {
            dsfs__DocuSign_Envelope_Document__c document = new dsfs__DocuSign_Envelope_Document__c();
            document.dsfs__DocuSign_EnvelopeID__c = envelopeId;
            document.dsfs__Document_Name__c = signingDocument.title;
            document.dsfs__Document_Order__c = signingDocument.order;
            document.dsfs__SFDocument_Type__c = 'Content';
            document.dsfs__External_Document_Id__c = signingDocument.contentVersionId;

            return document;
        }

        private dsfs__DocuSign_Envelope_Recipient__c prepareRecipient(Id envelopeId, EnvelopeRecipient envelopeRecipient) {
            dsfs__DocuSign_Envelope_Recipient__c recipient = new dsfs__DocuSign_Envelope_Recipient__c();
            recipient.dsfs__DocuSign_EnvelopeID__c = envelopeId;
            recipient.dsfs__Routing_Order__c = envelopeRecipient.order;
            recipient.dsfs__SignInPersonName__c = envelopeRecipient.name;
            recipient.dsfs__Recipient_Email__c = envelopeRecipient.email;
            recipient.dsfs__Recipient_Note__c = envelopeRecipient.noteForRecipient;
            recipient.dsfs__DocuSign_Recipient_Role__c = envelopeRecipient.recipientRole;
            recipient.dsfs__DocuSign_Signer_Type__c = envelopeRecipient.recipientType;
            if (envelopeRecipient.isUser()) {
                recipient.dsfs__Salesforce_Recipient_Type__c = 'USER';
                recipient.dsfs__DSER_UserID__c = envelopeRecipient.recordId;
            }
            if (envelopeRecipient.recipientType == 'SIGNER') {
                recipient.dsfs__SignInPersonEmail__c = envelopeRecipient.email;
            }
            return recipient;
        }
    }

    public without sharing class SigningRecord {

        @AuraEnabled public Id recordId;
        @AuraEnabled public String type;
        @AuraEnabled public SObject obj;

        public SigningRecord() {
        }

        public SigningRecord(Id recordId) {
            this.recordId = recordId;
            this.type = recordId.getSobjectType().getDescribe().getName();
            this.obj = this.getSigningSobject();
        }

        private SObject getSigningSobject() {
            return new QueryBuilder(this.type)
                    .addFields('Id, Name')
                    .addConditions()
                    .add(new QueryBuilder.CompareCondition('Id').eq(this.recordId))
                    .endConditions()
                    .toSObject();
        }
    }

    public without sharing class SigningDocument {

        @AuraEnabled public Integer order;
        @AuraEnabled public Id contentVersionId;
        @AuraEnabled public Id contentDocumentId;
        @AuraEnabled public String title;
        @AuraEnabled public Integer size;

        public SigningDocument() {
            this.title = '';
            this.size = 0;
        }

        public SigningDocument(String recordId) {
            ContentVersion latestContentVersion = this.getLatestContentVersion(recordId);
            this.contentVersionId = latestContentVersion.Id;
            this.contentDocumentId = latestContentVersion.ContentDocumentId;
            this.title = latestContentVersion.Title;
            this.size = latestContentVersion.ContentSize;
        }

        private ContentVersion getLatestContentVersion(Id recordId) {
            List<ContentDocumentLink> documentLinks = [
                    SELECT Id
                            , ContentDocumentId
                    FROM ContentDocumentLink
                    WHERE LinkedEntityId = :recordId
            ];
            if (documentLinks.isEmpty()) {
                return null;
            }
            Id contentDocumentId = documentLinks[0].ContentDocumentId;
            List<ContentVersion> contentVersions = [
                    SELECT Id
                            , ContentDocumentId
                            , Title
                            , ContentSize
                    FROM ContentVersion
                    WHERE IsLatest = TRUE
                    AND ContentDocumentId = :contentDocumentId
            ];
            if (contentVersions.isEmpty()) {
                return null;
            }
            return contentVersions[0];
        }
    }

    public class EnvelopeRecipient {

        @AuraEnabled public Integer order;
        @AuraEnabled public String recordId;
        @AuraEnabled public String recipientType;
        @AuraEnabled public String recipientRole;
        @AuraEnabled public String name;
        @AuraEnabled public String email;
        @AuraEnabled public String noteForRecipient;
        @AuraEnabled public String type;

        public EnvelopeRecipient() {

        }

        public Boolean isUser() {
            return String.isNotEmpty(this.type) && this.type == 'user';
        }
    }

    public class EnvelopeMessage {

        @AuraEnabled public String subject;
        @AuraEnabled public String message;

        public EnvelopeMessage() {
            this.subject = 'Documents for your DocuSign Signature';
            this.message = 'I am sending you this request for your electronic signature, please review and electronically sign by following the link below.';
        }
    }


    /**
     * Set of classes to load picklist values
     */
    public class DocuSignOptions {

        @AuraEnabled public List<DocuSignSelectOption> recipientRoleOptions;
        @AuraEnabled public List<DocuSignSelectOption> signerTypeOptions;

        private final Set<String> supportedSignerTypes;

        public DocuSignOptions() {
            //due to limitations of the implementation, not all the signer types are supported
            this.supportedSignerTypes = new Set<String>{
                    'Signer', 'Carbon Copy'
            };

            this.recipientRoleOptions = this.loadRecipientRoleOptions();
            this.signerTypeOptions = this.loadSignerTypeOptions();
        }

        private List<DocuSignSelectOption> loadRecipientRoleOptions() {
            List<DocuSignSelectOption> result = new List<DocuSignSelectOption>();
            for (PicklistEntry entry : this.loadOptions(dsfs__DocuSign_Envelope_Recipient__c.dsfs__DocuSign_Recipient_Role__c)) {
                result.add(new DocuSignSelectOption(entry));
            }
            return result;
        }

        private List<DocuSignSelectOption> loadSignerTypeOptions() {
            List<DocuSignSelectOption> result = new List<DocuSignSelectOption>();
            for (PicklistEntry entry : this.loadOptions(dsfs__DocuSign_Envelope_Recipient__c.dsfs__DocuSign_Signer_Type__c)) {
                if (this.supportedSignerTypes.contains(entry.getValue())) {
                    result.add(new DocuSignSelectOption(entry));
                }
            }
            return result;
        }

        private List<PicklistEntry> loadOptions(SObjectField field) {
            List<PicklistEntry> result = new List<PicklistEntry>();
            List<PicklistEntry> entries = field.getDescribe().getPicklistValues();
            for (PicklistEntry entry : entries) {
                if (entry.isActive()) {
                    result.add(entry);
                }
            }
            return result;
        }
    }

    public class DocuSignSelectOption {

        @AuraEnabled public String label;
        @AuraEnabled public String value;

        public DocuSignSelectOption(PicklistEntry entry) {
            this.label = entry.getLabel().toLowerCase().capitalize();
            this.value = entry.getValue();
        }
    }


    /**
     * Set of classes to work with DocuSign
     */
    public class DocuSignApi {

        private final String token;
        private final Http transport;
        private final DocuSignRequestBuilder requestBuilder;

        public DocuSignApi() {
            this.token = this.getToken();
            this.requestBuilder = new DocuSignRequestBuilder(this.token);
            this.transport = new Http();
        }

        public String getAuthToken() {
            return this.token;
        }

        public String getEndpoint() {
            return this.requestBuilder.getCreateEnvelopeUrl().toExternalForm();
        }

        public String getCreateChunkUploadEndpoint() {
            return this.requestBuilder.getCreateChunkedUploadUrl().toExternalForm();
        }

        /**
         * The method initiates a new chunked upload with the first part of the content.
         * @param dataChunk A Base64-encoded representation of the content hat is used to upload the file.
         * @return ChunkedDocument class
         */
        public ChunkedDocument createNewChunkedDocument(final String dataChunk) {
            ChunkedDocument document = new ChunkedDocument(null, dataChunk);
            Url createNewChunkedDocumentUrl = this.requestBuilder.getCreateChunkedUploadUrl();
            HttpResponse response = submitRequest(
                    this.requestBuilder
                            .withMethod('POST')
                            .withEndpoint(createNewChunkedDocumentUrl.toExternalForm())
                            .withDefaultHeaders()
                            .withBody(JSON.serialize(document, true))
                            .build()
            );
            return handleChunkedUploadResponse(response, 201);
        }

        public ChunkedDocument updateChunkedDocument(final ChunkedDocument document, final String uploadPartSeq) {
            Url updateChunkedDocumentUrl = this.requestBuilder.getUpdateChunkedUploadUrl(document.chunkedUploadId, uploadPartSeq);
            HttpResponse response = submitRequest(
                    this.requestBuilder
                            .withMethod('PUT')
                            .withEndpoint(updateChunkedDocumentUrl.toExternalForm())
                            .withDefaultHeaders()
                            .withBody(JSON.serialize(document, true))
                            .build()
            );
            return handleChunkedUploadResponse(response, 200);
        }

        public ChunkedUploadResponse commitChunkedDocument(final ChunkedDocument document) {
            Url commitChunkedDocumentUrl = this.requestBuilder.getCommitChunkedDocumentUrl(document.chunkedUploadId);
            HttpResponse response = submitRequest(
                    this.requestBuilder
                            .withMethod('PUT')
                            .withEndpoint(commitChunkedDocumentUrl.toExternalForm())
                            .withDefaultHeaders()
                            .withBody(JSON.serialize(document, true))
                            .build()
            );
            if (response.getStatusCode() == 200) {
                return (ChunkedUploadResponse) JSON.deserialize(
                        response.getBody(),
                        ChunkedUploadResponse.class
                );
            } else if (response.getStatusCode() == 400) {
                ErrorDetails errorDetails = (ErrorDetails) JSON.deserialize(
                        response.getBody(),
                        ErrorDetails.class
                );
                throw new AuraHandledException(errorDetails.errorCode + ' ' + errorDetails.message);
            } else {
                throw new AuraHandledException('Unexpected response status code');
            }
        }

        private ChunkedDocument handleChunkedUploadResponse(final HttpResponse response, final Integer successfulStatusCode) {
            if (response.getStatusCode() == successfulStatusCode) {
                ChunkedUploadResponse chunkedUploadResponse = (ChunkedUploadResponse) JSON.deserialize(
                        response.getBody(),
                        ChunkedUploadResponse.class
                );
                return new ChunkedDocument(chunkedUploadResponse.chunkedUploadId, null);
            } else if (response.getStatusCode() == 400) {
                ErrorDetails errorDetails = (ErrorDetails) JSON.deserialize(
                        response.getBody(),
                        ErrorDetails.class
                );
                throw new AuraHandledException(errorDetails.errorCode + ' ' + errorDetails.message);
            } else {
                throw new AuraHandledException('Unexpected response status code');
            }
        }

        public String submitEnvelope(final SigningEnvelope envelope, final String remoteUrl) {
            try {
                dsfs__DocuSign_Envelope__c persistedEnvelope = [
                        SELECT Id
                        FROM dsfs__DocuSign_Envelope__c
                        WHERE Id = :envelope.persistedEnvelope.Id
                        LIMIT 1
                ][0];
                String serializedDocusignEnvelope = envelope.toDocusignEnvelopeJson(remoteUrl);
                Url createEnvelopeUrl = this.requestBuilder.getCreateEnvelopeUrl();
                HttpResponse createEnvelopeResponse = submitRequest(
                        this.requestBuilder
                                .withMethod('POST')
                                .withEndpoint(createEnvelopeUrl.toExternalForm())
                                .withDefaultHeaders()
                                .withBody(serializedDocusignEnvelope)
                                .build()
                );
                if (createEnvelopeResponse.getStatusCode() == 201) {
                    EnvelopeCreate parsedEnvelope = (EnvelopeCreate) JSON.deserializeStrict(
                            createEnvelopeResponse.getBody(),
                            EnvelopeCreate.class
                    );
                    String iframeUrl = getGeneratedIframeUrl(parsedEnvelope.envelopeId);
                    persistedEnvelope.dsfs__DocuSign_Envelope_ID__c = parsedEnvelope.envelopeId;

                    update persistedEnvelope;
                    return iframeUrl;
                } else if (createEnvelopeResponse.getStatusCode() == 400) {
                    System.debug('errorDetails: ' + createEnvelopeResponse.getBody());
                   ErrorDetails errorDetails = (ErrorDetails) JSON.deserialize(
                            createEnvelopeResponse.getBody(),
                            ErrorDetails.class
                    );

                    delete persistedEnvelope;
                    throw new AuraHandledException(errorDetails.errorCode + ' ' + errorDetails.message);
                } else {
                    throw new AuraHandledException('Unexpected response status code');
                }

            } catch (Exception exc) {
                throw new AuraHandledException(exc.getMessage() + '\n' + exc.getStackTraceString());
            }
        }

        public String getGeneratedIframeUrl(String envelopeId) {
            try {
                Url createEditEnvelopeUrl = this.requestBuilder.getCreateEditEnvelopeUrl(envelopeId);
                string Returl=VTD1_RTId__c.getInstance().VT_R2_SCR_URL__c+'/s/blankpage';
                HttpResponse createEditViewResponse = submitRequest(
                        this.requestBuilder
                                .withMethod('POST')
                                .withEndpoint(createEditEnvelopeUrl.toExternalForm())
                                .withDefaultHeaders()
                                .withBody('{"returnUrl":"'+Returl+'"}')
                                //.withBody('{}')
                                .build()

                );
                if (createEditViewResponse.getStatusCode() == 201) {
                    EnvelopeViewEdit envelopeViewEdit = (EnvelopeViewEdit) JSON.deserializeStrict(
                            createEditViewResponse.getBody(),
                            EnvelopeViewEdit.class
                    );
                    return envelopeViewEdit.url;
                } else if (createEditViewResponse.getStatusCode() == 400) {
                    ErrorDetails errorDetails = (ErrorDetails) JSON.deserializeStrict(
                            createEditViewResponse.getBody(),
                            ErrorDetails.class
                    );
                    throw new AuraHandledException(errorDetails.errorCode + ' ' + errorDetails.message);
                } else {
                    throw new AuraHandledException('Unexpected response status code');
                }
            } catch (Exception exc) {
                throw new AuraHandledException(exc.getMessage() + '\n' + exc.getStackTraceString());
            }

        }

        private HttpResponse submitRequest(HttpRequest request) {
            request.setTimeout(120000);
            return this.transport.send(request);
        }

        public String getToken() {
            VT_D2_DocuSignAuthHelper.Result token = VT_D2_DocuSignAuthHelper.getAuthToken();
            System.debug('token value ' + token);
            if (String.isEmpty(token.value)) {
                throw new AuraHandledException('DocuSign is improperly configured. Please, contact your administrator.');
            }
            return token.value;
        }
    }

    public class EnvelopeCreate {
        public String envelopeId;
        public String uri;
        public String statusDateTime;
        public String status;
    }

    public class EnvelopeRecipients {
        public List<Signers> signers;
        public List<Signers> carbonCopies;
    }

    public class Signers {
        public String recipientId;
        public String recipientIdGuid;

    }

    public class ErrorDetails {
        public String errorCode;
        public String message;
    }

    public class EnvelopeViewEdit {
        public String url;
    }

    public class DocuSignRequestBuilder {
        // DocuSign ChunkedUploads Resource: https://developers.docusign.com/docs/esign-rest-api/reference/Envelopes/ChunkedUploads/

        private final String token;
        private final String protocol;
        private final String baseUrl;
        private final String apiVersion;
        private final VTD2_Docusign_API_Settings__c settings;
        private final String defaultUrl;
        private HttpRequest request;

        public DocuSignRequestBuilder(String token) {
            this.token = token;
            this.settings = VTD2_Docusign_API_Settings__c.getInstance();
            this.protocol = 'https://';
            this.baseUrl = this.getBaseUrl();
            this.apiVersion = '/v2';
            this.defaultUrl = this.baseUrl
                            + '/envelopes';
            this.request = new HttpRequest();
        }

        public Url getCreateEnvelopeUrl() {
            return new Url(this.defaultUrl);
        }

        public Url getCreateChunkedUploadUrl() {
            return new Url(this.baseUrl + '/chunked_uploads');
        }

        public Url getUpdateChunkedUploadUrl(final String chunkedUploadId, final String chunkedUploadPartSeq) {
            String urlPostfix = String.format('/chunked_uploads/{0}/{1}', new List<String>{chunkedUploadId, chunkedUploadPartSeq});
            return new Url(this.baseUrl + urlPostfix);
        }

        public Url getCommitChunkedDocumentUrl(final String chunkedUploadId) {
            String urlPostfix = String.format('/chunked_uploads/{0}', new List<String>{chunkedUploadId});
            return new Url(this.baseUrl + urlPostfix);
        }

        public Url getCreateEditEnvelopeUrl(String envelopeId) {
            return new Url(this.defaultUrl + '/' + envelopeId + '/views/edit');
        }

        public Url getEnvelopeRecipientsListUrl(String envelopeId) {
            return new Url(this.defaultUrl + '/' + envelopeId + '/recipients');
        }

        private String getBaseUrl() {
            return VT_D2_DocuSignAuthHelper.getBaseUri(token).value;
        }

        public DocuSignRequestBuilder withMethod(String method) {
            this.request.setMethod(method);
            return this;
        }

        public DocuSignRequestBuilder withEndpoint(String endpoint) {
            this.request.setEndpoint(endpoint);
            return this;
        }

        public DocuSignRequestBuilder withHeader(String key, String value) {
            this.request.setHeader(key, value);
            return this;
        }

        public DocuSignRequestBuilder withDefaultHeaders() {
            this.request.setHeader('Authorization', 'Bearer ' + this.token);
            this.request.setHeader('Content-Type', 'application/json');
            this.request.setHeader('Accept', 'application/json');
            return this;
        }

        public DocuSignRequestBuilder withBody(String body) {
            this.request.setBody(body);
            return this;
        }

        public HttpRequest build() {
            return this.request;
        }

    }

    /**
     * Set of classes to work with Lookup
     */
    public without sharing class UserLookup {

        public List<LookupSearchResult> find(String searchWord) {
            List<LookupSearchResult> result = new List<LookupSearchResult>();
            if (String.isEmpty(searchWord)) {
                return result;
            }
            String soslString = 'FIND \'*' + searchWord + '*\' RETURNING User(Id, Name, Email)';
            List<List<SObject>> searchResult = Search.query(soslString);
            if (searchResult.isEmpty() || searchResult[0].isEmpty()) {
                return result;
            }
            for (SObject searchItem : searchResult[0]) {
                result.add(new LookupSearchResult((User) searchItem));
            }
            return result;
        }

        public LookupSearchResult getByUserId(Id userId) {
            return new LookupSearchResult(
                    (User) new QueryBuilder(User.class)
                            .addFields('Id, Name, Email')
                            .addConditions()
                            .add(new QueryBuilder.CompareCondition(User.Id).eq(userId))
                            .endConditions()
                            .toSObject()
            );
        }
    }

    public without sharing class LookupSearchResult {

        @AuraEnabled public Id recordId;
        @AuraEnabled public String name;
        @AuraEnabled public String email;

        public LookupSearchResult(User user) {
            this.recordId = user.Id;
            this.name = user.Name;
            this.email = (String) user.get('Email');
        }
    }

    public class ChunkedDocument {
        @AuraEnabled public String chunkedUploadId;
        @AuraEnabled public String data;

        public ChunkedDocument(final String chunkedUploadId, final String data) {
            this.chunkedUploadId = chunkedUploadId;
            this.data = data;
        }
    }

    public class ChunkedUploadResponse {
        @AuraEnabled final public String chunkedUploadId;
        @AuraEnabled final public String chunkedUploadUri;
        @AuraEnabled final public String expirationDateTime;
        @AuraEnabled final public String checksum;
        @AuraEnabled final public String committed;
        @AuraEnabled final public String totalSize;
        @AuraEnabled final public String maxChunkedUploadParts;
        @AuraEnabled final public String maxTotalSize;
    }
}