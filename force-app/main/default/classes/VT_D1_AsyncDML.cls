/**
* @author: Carl Judge
* @date: 20/07/18
* @description: Performs a DML operation on the records passed in
* @usage: VT_D1_AsyncDML asyncDml = new VT_D1_AsyncDML(records, DMLOperation);
 *        asyncDml.setAllOrNothing(false);
 *        asyncDml.setBatchSize(50);
 *        asyncDml.addRetryError('LOCK_ROW');
 *        asyncDml.enqueue();
**/

public without sharing class VT_D1_AsyncDML extends VT_R3_AbstractChainableQueueable {
    private List<SObject> records;
    private DMLOperation operation;
    private Boolean allOrNothing = true; // if retry errors are provided, this will only fail on other errors
    private Integer batchSize = 200;
    private List<String> errorsToRetry = new List<String>();
    private Integer retryLimit = 10;
    private Integer retryNumber = 0;
    private List<SObject> recordsToRetry = new List<SObject>();
    private String semaphoreName;
    private Boolean semaphoreOnlyOnRetry = false;
    private Boolean loggingEnabled = false; // only works when allOrNothing == false
    private Boolean logFullRecord = false; // only works if logged is enabled
    private String classForLogging = this.getType().getName();
    private transient List<SObject> currentBatch;
    private transient List<VT_R5_DatabaseResultWrapper> wrappedSavedResults;
    private transient List<VT_R5_DatabaseResultWrapper> resultsToLog;
    private transient List<SObject> recordsToLog;

    public VT_D1_AsyncDML(List<SObject> records, DMLOperation operation) {
        this.records = records;
        this.operation = operation;
    }

    public static RetryResult isRetryDML(List<Database.Error> errors, List<String> retryErrors) {
        Boolean isRetry = false;
        String nonRetryError;
        for (Database.Error error : errors) {
            String errorMessage = error.getMessage();
            if (errorMessage != null) {
                isRetry = false;
                for (String retryErrorMsg : retryErrors) {
                    if (errorMessage.containsIgnoreCase(retryErrorMsg)) {
                        isRetry = true;
                        break;
                    }
                }
                if (!isRetry) {
                    nonRetryError = errorMessage;
                    break;
                }
            }
        }
        return new RetryResult(isRetry, nonRetryError);
    }

    public VT_D1_AsyncDML addRecord(SObject record) {
        this.records.add(record);
        return this;
    }

    public VT_D1_AsyncDML setAllOrNothing(Boolean allOrNothing) {
        this.allOrNothing = allOrNothing;
        return this;
    }

    public VT_D1_AsyncDML setBatchSize(Integer batchSize) {
        this.batchSize = batchSize;
        return this;
    }

    public VT_D1_AsyncDML addRetryError(String errorToRetry) {
        this.errorsToRetry.add(errorToRetry);
        return this;
    }

    public VT_D1_AsyncDML setRetryErrors(List<String> errorsToRetry) {
        this.errorsToRetry = errorsToRetry;
        return this;
    }

    public VT_D1_AsyncDML setRetryLimit(Integer retryLimit) {
        this.retryLimit = retryLimit;
        return this;
    }

    public VT_D1_AsyncDML setSemaphoreName(String semaphoreName) {
        this.semaphoreName = semaphoreName;
        return this;
    }

    public VT_D1_AsyncDML setSemaphoreOnlyOnRetry(Boolean semaphoreOnlyOnRetry) {
        this.semaphoreOnlyOnRetry = semaphoreOnlyOnRetry;
        return this;
    }

    public VT_D1_AsyncDML setLoggingEnabled(Boolean loggingEnabled) {
        this.loggingEnabled = loggingEnabled;
        return this;
    }

    public VT_D1_AsyncDML setLogFullRecord(Boolean logFullRecord) {
        this.logFullRecord = logFullRecord;
        return this;
    }

    public VT_D1_AsyncDML setClassForLogging(String classForLogging) {
        this.classForLogging = classForLogging;
        return this;
    }

    public override Type getType() {
        return VT_D1_AsyncDML.class;
    }

    public override void executeLogic() {
        if (!isSemaphoreInUse()) {
            setCurrentBatch();
            processCurrentBatch();
            if (!errorsToRetry.isEmpty() || loggingEnabled) {
                processSaveResults();
            }
        }
        setNextInstance();
    }

    private Boolean isSemaphoreInUse() {
        Boolean semaphoreInUse = false;
        if (semaphoreName != null && (!semaphoreOnlyOnRetry || retryNumber > 0)) {
            try {
                List<VTR4_Semaphore__c> semaphores = [SELECT Id FROM VTR4_Semaphore__c WHERE Name = :semaphoreName FOR UPDATE];
            } catch(Exception e) {
                semaphoreInUse = true;
            }
        }
        return semaphoreInUse;
    }

    private void setCurrentBatch() {
        currentBatch = new List<SObject>();
        if (records.size() <= batchSize) {
            currentBatch = records.clone();
            records.clear();
        } else {
            while (currentBatch.size() < batchSize && !records.isEmpty()) {
                currentBatch.add(records.remove(0));
            }
        }
    }

    private void processCurrentBatch() {
        List<Object> saveResults;
        switch on operation {
            when INS {
                saveResults = Database.insert(currentBatch, allOrNothing && errorsToRetry.isEmpty());
            }
            when UPD {
                saveResults = Database.update(currentBatch, allOrNothing && errorsToRetry.isEmpty());
            }
            when UPS {
                saveResults = Database.upsert(currentBatch, allOrNothing && errorsToRetry.isEmpty());
            }
            when DEL {
                saveResults = Database.delete(currentBatch, allOrNothing && errorsToRetry.isEmpty());
            }
            when UND {
                saveResults = Database.undelete(currentBatch, allOrNothing && errorsToRetry.isEmpty());
            }
        }
        wrappedSavedResults = VT_R5_DatabaseResultWrapper.wrapResults(saveResults);
    }

    private void processSaveResults() {
        resultsToLog = new List<VT_R5_DatabaseResultWrapper>();
        recordsToLog = new List<SObject>();
        for (Integer i = 0; i < wrappedSavedResults.size(); i++) {
            VT_R5_DatabaseResultWrapper saveResult = wrappedSavedResults[i];
            if (!saveResult.isSuccess()) {
                SObject record = currentBatch[i];
                RetryResult retryResult = getRetryResult(saveResult.getErrors(), record);
                if (retryResult.isRetry) {
                    recordsToRetry.add(record);
                } else {
                    handleNonRetryError(retryResult, saveResult, record);
                }
            }
        }
        if (!resultsToLog.isEmpty()) { logErrors(resultsToLog, recordsToLog); }
    }

    private RetryResult getRetryResult(List<Database.Error> errors, SObject record) {
        RetryResult retryResult = new RetryResult(false, null);
        if (retryNumber >= retryLimit) {
            System.debug('Max retries reached for ' + operation + ' operation on: ' + record);
            if (!errors.isEmpty()) {
                retryResult.nonRetryError = errors[0]?.getMessage();
            }
        } else {
            retryResult = isRetryDML(errors, errorsToRetry);
        }
        return retryResult;
    }

    private void handleNonRetryError(RetryResult retryResult, VT_R5_DatabaseResultWrapper saveResult, SObject record) {
        String errorMsg = retryResult.nonRetryError != null ? retryResult.nonRetryError : 'Unknown Error';
        if (allOrNothing) {
            throw new AsyncDMLException(errorMsg);
        } else {
            System.debug('Error: ' + errorMsg);
            if (loggingEnabled) {
                resultsToLog.add(saveResult);
                recordsToLog.add(record);
            }
        }
    }

    private void logErrors(List<VT_R5_DatabaseResultWrapper> resultsToLog, List<SObject> recordsToLog) {
        try {
            ErrorLogUtility.logErrorsInBulk(
                resultsToLog,
                recordsToLog,
                ErrorLogUtility.ApplicationArea.APP_AREA_ASYNC_DML,
                classForLogging,
                logFullRecord
            );
        } catch (Exception e) {
            try {
                PlatformEventErrorLogger logger = new PlatformEventErrorLogger(resultsToLog, recordsToLog, classForLogging, logFullRecord);
                logger.publish();
            } catch (Exception ex) {
                System.debug('Unable to log errors: ' + ex.getMessage());
            }
        }
    }

    private void setNextInstance() {
        List<SObject> nextInstanceRecords;
        if (!records.isEmpty()) {
            nextInstanceRecords = records;
        } else if (!recordsToRetry.isEmpty()) {
            nextInstanceRecords = recordsToRetry.clone();
            recordsToRetry.clear();
            retryNumber++;
        }
        if (nextInstanceRecords != null) {
            VT_D1_AsyncDML nextInstance = this.clone();
            nextInstance.records = nextInstanceRecords;
            addToChain(nextInstance);
        }
    }

    public class RetryResult {
        public Boolean isRetry;
        public String nonRetryError;
        public RetryResult(Boolean isRetry, String nonRetryError) {
            this.isRetry = isRetry;
            this.nonRetryError = nonRetryError;
        }
    }

    public class PlatformEventErrorLogger extends VT_R3_AbstractPublishedAction {
        private String serializedResultsToLog;
        private String listWrapperType;
        private List<SObject> recordsToLog;
        private String classForLogging;
        private Boolean logFullRecord;

        public PlatformEventErrorLogger(List<VT_R5_DatabaseResultWrapper> resultsToLog, List<SObject> recordsToLog, String classForLogging, Boolean logFullRecord) {
            this.serializedResultsToLog = JSON.serialize(resultsToLog);
            this.listWrapperType = 'List<' + resultsToLog[0].getType().getName() + '>';
            this.recordsToLog = recordsToLog;
            this.classForLogging = classForLogging;
            this.logFullRecord = logFullRecord;
        }

        public override void execute() {
            List<VT_R5_DatabaseResultWrapper> resultsToLog = (List<VT_R5_DatabaseResultWrapper>) JSON.deserialize(serializedResultsToLog, Type.forName(listWrapperType));
            ErrorLogUtility.logErrorsInBulk(
                resultsToLog,
                recordsToLog,
                ErrorLogUtility.ApplicationArea.APP_AREA_ASYNC_DML,
                classForLogging,
                logFullRecord
            );
        }

        public override Type getType() {
            return VT_D1_AsyncDML.PlatformEventErrorLogger.class;
        }
    }

    public class AsyncDMLException extends Exception{}
}