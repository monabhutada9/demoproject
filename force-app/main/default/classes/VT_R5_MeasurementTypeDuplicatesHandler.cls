/**
 * @author: Aliaksandr Vabishchevich
 * @date: 03-06-2020
 * @description: Handler class for VT_R5_MeasurementTypeDuplicatesTrigger
 **/
public with sharing class VT_R5_MeasurementTypeDuplicatesHandler {
    public static void validateDuplicates(List<VTR5_StudyMeasurementsConfiguration__c> newList){
        /**
        * Allow only one MeasurementConfig per type on Study
        http://jira.quintiles.net/browse/SH-11821
        * @description: before insert new Study Measurement Configuration, checks if in existing records Study Measurement Configuration have the same measurement type in the new record then set the error
        * @param: newList take list of Study Measurement Configuration (Trigger.new) from VT_R5_MeasurementTypeDuplicatesTrigger @trigger
        */
        List<VTR5_StudyMeasurementsConfiguration__c> studyConfigs = existingStudyMeasurementConfigs(newList);
        Map<String, VTR5_StudyMeasurementsConfiguration__c> existingSMCMap = new Map<String, VTR5_StudyMeasurementsConfiguration__c>();
        for (VTR5_StudyMeasurementsConfiguration__c sf : studyConfigs) {
            existingSMCMap.put(sf.VTR5_MeasurementType__c, sf);
        }
        for (VTR5_StudyMeasurementsConfiguration__c smc : newList) {
            if (existingSMCMap.containsKey(smc.VTR5_MeasurementType__c)) {
                smc.addError('You cannot create the same Measurement Type as you already have.');
            }
        }
    }
    public static void validateOnUpdateStudyMesConf(List<VTR5_StudyMeasurementsConfiguration__c> newList, Map<Id,VTR5_StudyMeasurementsConfiguration__c> oldMap) {
        /**
        * Allow only one MeasurementConfig per type on Study
        http://jira.quintiles.net/browse/SH-11821
        * @description: before update Study Measurement Configuration, checks the previous value measurement type and  the current value of measurement type,
        * if these values the same, we can update other fields of those record, else checks
        * if in existing records Study Measurement Configuration has the same measurement type then set the error
        * @param: newList take list of Study Measurement Configuration (Trigger.new) from VT_R5_MeasurementTypeDuplicatesTrigger @trigger
        * @param: oldMap allows to get previous map of Study Measurement Configuration records (Trigger.oldMap) from VT_R5_MeasurementTypeDuplicatesTrigger @trigger
        */
        List<VTR5_StudyMeasurementsConfiguration__c> studyConfigs = existingStudyMeasurementConfigs(newList);
        for (VTR5_StudyMeasurementsConfiguration__c studyConfig : newList) {
            if (oldMap.get(studyConfig.Id).VTR5_MeasurementType__c == studyConfig.VTR5_MeasurementType__c ) return;
            for (VTR5_StudyMeasurementsConfiguration__c sd : studyConfigs) {
                if (studyConfig.VTR5_MeasurementType__c == sd.VTR5_MeasurementType__c) {
                    studyConfig.VTR5_MeasurementType__c.addError('You cannot update the same Measurement Type as you already have.');
                }
            }
        }
    }
    private static List<VTR5_StudyMeasurementsConfiguration__c> existingStudyMeasurementConfigs(List<VTR5_StudyMeasurementsConfiguration__c> newList) {
        /**
        * @description compare all existing records Study Measurement Configuration Object by Study Id
        * @param: newList take list of Study Measurement Configuration (Trigger.new) from VT_R5_MeasurementTypeDuplicatesTrigger @trigger
        **/
        Set<Id> studyIds = new Set<Id>();
        for (VTR5_StudyMeasurementsConfiguration__c studyConfig : newList) {
            studyIds.add(studyConfig.VTR5_Study__c);
        }
        return [
                SELECT Id, VTR5_Study__c, VTR5_MeasurementType__c
                FROM VTR5_StudyMeasurementsConfiguration__c
                WHERE VTR5_MeasurementType__c != NULL
                AND VTR5_Study__c IN:studyIds
        ];
    }
}