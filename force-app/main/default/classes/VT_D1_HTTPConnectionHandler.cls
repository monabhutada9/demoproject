public class VT_D1_HTTPConnectionHandler {
    
    public class Result{
        public HttpResponse httpResponse;
        public VTD1_Integration_Log__c log;
        public Long timestamp;

        public Result(HttpResponse httpResponse, VTD1_Integration_Log__c log){
            this.httpResponse =httpResponse;
            this.log = log;
        }

        public Result(HttpResponse httpResponse, VTD1_Integration_Log__c log, Long timestamp){
            this.httpResponse =httpResponse;
            this.log = log;
            this.timestamp = timestamp;
        }
    }
        
    public static final String INTEGRATION_TIMESTAMP_FORMAT = 'yyyy-MM-dd HH:mm:ss.SSS';
    public static final Integer CALLOUT_TIMEOUT = 120000;
    
    // default send
    public static Result send(String method, String endpointURL, VT_D1_RequestBuilder requestBuilder, String action) {
        Map<String, String> headerMapDefault = new Map<String, String>{
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'TID' => generateUUID()
        };
        return send(method, endpointURL, requestBuilder, action, headerMapDefault);
    }

    // without skip log insert flag
    public static Result send(String method, String endpointURL, VT_D1_RequestBuilder requestBuilder, String action, Map<String, String> headerMap) {
        return send(method, endpointURL, requestBuilder, action, headerMap, false);
    }
    
    //common logic, return object with response and log.
    public static Result send(String method, String endpointURL, VT_D1_RequestBuilder requestBuilder, String action, Map<String, String> headerMap, Boolean skipLogInsert) {
        System.debug('METHOD: ' + method + ', ENDPOINT: ' + endpointURL);
        //init request:
        HttpRequest req = new HttpRequest();
        req.setMethod(method);
        req.setEndpoint(endpointURL);
        for (String key : headerMap.keySet()) {
            req.setHeader(key, headerMap.get(key));
        }
        req.setTimeout(CALLOUT_TIMEOUT);

        Datetime requestDT = Datetime.now();
        Datetime responseDT;
        //init log:
        VTD1_Integration_Log__c log = new VTD1_Integration_Log__c(
                VTD1_Action__c = action,
                VTD1_REST_Method__c = method,
                VTD1_EndPoint_URL__c = endpointURL,
                VTD1_TId__c = headerMap.get('TID'),
                VTD1_Timestamp_Request__c = requestDT.formatGmt(INTEGRATION_TIMESTAMP_FORMAT),
                VTD1_Request_Timeout_Encountered__c = '0'
        );
        // If URL is too big to fit in the field, truncate it and put the full version in long text field
        Integer maxURL = VTD1_Integration_Log__c.VTD1_EndPoint_URL__c.getDescribe().getLength();
        if (endpointURL.length() > maxURL) {
            log.VTD1_EndPoint_URL__c = endpointURL.left(maxURL - 4) + '....';
            log.VTR5_Long_Endpoint_URL__c = endpointURL;
        }
        //execute request:
        HttpResponse res;
        try {
            //build request body:
            if (requestBuilder != null) { // null for GET requests
                String requestBody = requestBuilder.buildRequestBody();
                req.setBody(requestBody);
                log.VTD1_Body_Request__c = requestBody.mid(0, 32768);
                log.VTD1_Body_Request_Length__c = requestBody.length();
            }

            Http http = new Http();
            res = http.send(req);
            responseDT = Datetime.now();
            //update log info:
            log.VTD1_Status_Code_Response__c = res.getStatusCode();
            log.VTD1_Status_Response__c = res.getStatus();
            if(!String.isEmpty(res.getBody())){
                log.VTD1_Body_Response__c = res.getBody().mid(0, 32768);
                log.VTD1_Body_Response_Length__c = res.getBody().length();
            }
            System.debug('RESPONSE STATUS: ' + res.getStatusCode() + ' ' + res.getStatus());
        } catch (Exception e) {
            responseDT = Datetime.now();
            String errorMessage = e.getMessage() + '\n' + e.getStackTraceString();
            System.debug('ERROR: ' + errorMessage);
            //update log info:
            log.VTD1_ErrorMessage__c = errorMessage.mid(0, 32768);
            log.VTD1_Request_Timeout_Encountered__c = e.getMessage().equalsIgnoreCase('Read timed out') ? '1' : '0';
        }finally {
            //update log info:
            log.VTD1_Timestamp_Response__c = responseDT.formatGmt(INTEGRATION_TIMESTAMP_FORMAT);
            log.VTD1_Call_Duration__c = (responseDT.getTime() - requestDT.getTime()) / 1000;
            //commit log:
            if (! skipLogInsert) { insert log; }
            System.debug('LOG: ' + log);
        }
        return new Result(res, log, responseDT.getTime());
    }
    
    
   
    public static VTD1_IntegrationInbound_Log__c createIntegrationInboundLogRecord(String bodyRequest, String bodyResponse, String operation, Datetime timestampRequest, String errorMessage) {

        System.debug('TIMESTAMP_REQUEST: ' + timestampRequest);

        VTD1_IntegrationInbound_Log__c log = new VTD1_IntegrationInbound_Log__c();

        log.VTD1_Body_Request__c = bodyRequest;
        log.VTD1_SOAP_Operation__c = operation;

        if (bodyResponse != null) {
            log.VTD1_Body_Response__c = bodyResponse;

            System.debug('BODY: ' + bodyResponse);
        }

        if (timestampRequest != null)
            log.VTD1_Timestamp_Request__c = timestampRequest;//.formatGMT(INTEGRATION_TIMESTAMP_FORMAT);
        log.VTD1_ErrorMessage__c = errorMessage;

        return log;
    }   
    
    public static VTD1_IntegrationInbound_Log__c createIntegrationInboundLogRecord(String bodyRequest, String bodyResponse, Datetime timestampRequest, String errorMessage) {
        return createIntegrationInboundLogRecord(bodyRequest, bodyResponse, null, timestampRequest, errorMessage);
    }
    
    /*public static String generateUniqueDigit(String timestampRequestStr) {
        String hashString = '1000' + timestampRequestStr;//String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString)); // Computes a secure, one-way hash digest based on the supplied input string and algorithm name
        String hexDigest = EncodingUtil.convertToHex(hash); // Returns a hexadecimal (base 16) representation of the inputBlob
        System.debug('hexDigest=' + hexDigest);
        
        return hexDigest;
    }*/
    
    public static String generateUUID() {
        Blob b = Crypto.generateAesKey(128);
        String h = EncodingUtil.convertToHex(b);
        String uuid = h.substring(0, 8) + '-' + h.substring(8, 12) + '-' + h.substring(12, 16) + '-' + h.substring(16, 20) + '-' + h.substring(20);
        return uuid; 
    }
}