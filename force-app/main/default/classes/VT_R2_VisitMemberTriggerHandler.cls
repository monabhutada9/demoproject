/**
* @author: Carl Judge
* @date: 20-Feb-19
**/
public without sharing class VT_R2_VisitMemberTriggerHandler {
    private static Set <Id> visitMemberHandledIds = new Set<Id>();
    private static Map<String, String> visitMemberAddedEmailMap = new Map<String, String>{
            'Patient' => 'VTR4_Visit_Member_Added_Patient',
            'Site Coordinator' => 'VTR4_Visit_Member_Added_SCR',
            'Primary Investigator' => 'VTR4_Visit_Member_Added_PI',
            'External' => 'VTR4_Visit_Member_Added_External',
            'External_TV' => 'VTR4_Visit_Member_Added_External_TV'};
    public void onBeforeDelete(List<Visit_Member__c> members) {
        VT_D1_LegalHoldDeleteValidator.validateDeletion(members);
    }
    public void onBeforeInsert(List<Visit_Member__c> members) {
        setEmailsAndParticipantTypes(members, null);
    }
    public void onBeforeUpdate(List<Visit_Member__c> members, Map<Id, Visit_Member__c> oldMap) {
        setEmailsAndParticipantTypes(members, oldMap);
    }
    public void onAfterInsert(List<Visit_Member__c> members) {
    }
    public void onAfterUpdate(List<Visit_Member__c> members, Map<Id, Visit_Member__c> oldMap) {
        updateConfMemberIds(members, oldMap);
    }
    public static void sendNotifications(Set <Id> visitMemberIds) {
        List <Visit_Member__c> members = [select Id, VTR2_DoNotSendNewMemberEmail__c, VTD1_Participant_User__c, VTD1_Actual_Visit__c  from Visit_Member__c where Id in : visitMemberIds];
        sendNotifications(members);
    }
    public static void addEventRelationsAndBuffers(List <Visit_Member__c> members) {
        Set <Id> EventActualVisitIds = new Set<Id>();
        Set <String> unique          = new Set<String>();
        for (Visit_Member__c member : members) {
            if(member.VTD2_Visit_Status__c != VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT
                    && member.VTD2_Visit_Status__c != VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED
                    && member.VTD2_Visit_Status__c != VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED
                    && member.VTD1_Participant_User__c != null){
                EventActualVisitIds.add(member.VTD1_Actual_Visit__c);
                unique.add(String.valueOf(member.VTD1_Participant_User__c) + String.valueOf(member.VTD1_Actual_Visit__c));
            }
        }
        Set <Id> profileIds = new Set<Id>();
        Map <String, Event> profileAndTypeToEventMap = new Map<String, Event>();
        if(!EventActualVisitIds.isEmpty()){
            members = [select Id, VTD1_Participant_User__c, VTD1_Participant_User__r.ProfileId, VTD1_Actual_Visit__r.VTD1_VisitType_SubType__c from Visit_Member__c where Id in : members];
            List<Event> eventList = [select Id, VTD1_Actual_Visit__c, WhatId, OwnerId, StartDateTime, EndDateTime, DurationInMinutes, Subject from Event
            where WhatId in:EventActualVisitIds and IsChild=false and VTD1_IsBufferEvent__c = false
            and (VTD1_Actual_Visit__r.VTD1_Status__c = :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED
            or VTD1_Actual_Visit__r.VTD1_Status__c = :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED)];
            List<EventRelation> eventRelationList = new List<EventRelation>();
            System.debug('eventList = ' + eventList);
            for(Event evnt: eventList){
                for (Visit_Member__c m : members) {
                    if (evnt.OwnerId != m.VTD1_Participant_User__c
                            && unique.contains(String.valueOf(m.VTD1_Participant_User__c) + String.valueOf(evnt.WhatId))) {
                        EventRelation evR = new EventRelation(EventId = evnt.Id, RelationId=m.VTD1_Participant_User__c,
                                IsInvitee = true );
                        eventRelationList.add(evR);
                        profileIds.add(m.VTD1_Participant_User__r.ProfileId);
                        profileAndTypeToEventMap.put(m.VTD1_Participant_User__r.ProfileId + '_' + m.VTD1_Actual_Visit__r.VTD1_VisitType_SubType__c, evnt);
                    }
                }
            }
            if (!eventRelationList.isEmpty()) {
                System.debug('eventRelationList = ' + eventRelationList);
                insert eventRelationList;
            }
            if (!profileIds.isEmpty()) {
                Map <String, VTD1_CalendarBufferForScheduledVisit__mdt> profileIdToBufferMap = new Map<String, VTD1_CalendarBufferForScheduledVisit__mdt>();
                List <VTD1_CalendarBufferForScheduledVisit__mdt> buffers = [SELECT VTD1_Buffer__c, VTD1_ProfileId__c, VTD1_VisitType__c
                    FROM VTD1_CalendarBufferForScheduledVisit__mdt
                    WHERE VTD1_ProfileId__c IN :profileIds];
                for (VTD1_CalendarBufferForScheduledVisit__mdt buffer : buffers) {
                    profileIdToBufferMap.put(buffer.VTD1_ProfileId__c + '_' + buffer.VTD1_VisitType__c, buffer);
                    System.debug('profileIdToBufferMap add for ' + (buffer.VTD1_ProfileId__c + '_' + buffer.VTD1_VisitType__c));
                }
                System.debug('profileAndTypeToEventMap = ' + profileAndTypeToEventMap);
                System.debug('profileIdToBufferMap = ' + profileIdToBufferMap);
                List <Event> bufferEvents = new List<Event>();
                for (Visit_Member__c member : members) {
                    System.debug('profile id = ' + member.VTD1_Participant_User__r.ProfileId);
                    System.debug('visit type = ' + member.VTD1_Actual_Visit__r.VTD1_VisitType_SubType__c);
                    Event evnt = profileAndTypeToEventMap.get(member.VTD1_Participant_User__r.ProfileId + '_' + member.VTD1_Actual_Visit__r.VTD1_VisitType_SubType__c);
                    VTD1_CalendarBufferForScheduledVisit__mdt buffer = profileIdToBufferMap.get(member.VTD1_Participant_User__r.ProfileId + '_' + member.VTD1_Actual_Visit__r.VTD1_VisitType_SubType__c);
                    System.debug('event = ' + evnt);
                    System.debug('buffer = ' + buffer);
                    if (evnt != null && buffer != null) {
                        bufferEvents.add(new Event(
                                OwnerId = member.VTD1_Participant_User__c,
                                VTD1_IsBufferEvent__c = true,
                                StartDateTime = evnt.EndDateTime,
                                EndDateTime = evnt.EndDateTime.addMinutes(buffer.VTD1_Buffer__c.intValue()),
                                DurationInMinutes = buffer.VTD1_Buffer__c.intValue(),
                                Subject = 'Post-Visit Buffer',
                                VTD1_Actual_Visit__c = evnt.VTD1_Actual_Visit__c,
                                WhatId = evnt.VTD1_Actual_Visit__c
                        ));
                    }
                }
                if (!bufferEvents.isEmpty()) {
                    insert bufferEvents;
                }
                System.debug('bufferEvents after insert = ' + bufferEvents);
            }
        }
    }
    public static void sendNotifications(List <Visit_Member__c> members) {
        System.debug('sendNotifications, members = ' + members);
        System.debug('visitMemberHandledIds = ' + visitMemberHandledIds);
        String emailTemplateName;
        List <Visit_Member__c> filtered = new List<Visit_Member__c>();
        Set <Id> userIds             = new Set<Id>();
        Set <Id> visitIds            = new Set<Id>();
        Set <Id> ids                 = new Set<Id>();
        for (Visit_Member__c member : members) {
            if (!member.VTR2_DoNotSendNewMemberEmail__c) {
                filtered.add(member);
                userIds.add(member.VTD1_Participant_User__c);
                visitIds.add(member.VTD1_Actual_Visit__c);
                ids.add(member.Id);
            }
        }
        if (filtered.isEmpty()) {
            return;
        }
        System.debug('filtered =  ' + filtered);
        Datetime currentTime = System.now();
        filtered = [select Id, RecordTypeId, VTD1_Actual_Visit__r.RecordTypeId, VTD1_Participant_User__c, VTD1_Is_internal__c, VTD1_Member_Type__c,
                VTD1_Actual_Visit__r.Unscheduled_Visits__c, VTD1_Actual_Visit__r.VTD1_Case__c, VTD1_Actual_Visit__r.VTD1_Contact__c, VTD1_Actual_Visit__c, VTD1_External_Participant_Email__c,VTD1_Participant_User__r.Email, VTD1_Actual_Visit__r.VTD2_Common_Visit_Type__c,
                VTD1_Actual_Visit__r.VTD1_Status__c, VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c, VTR2_ReasonForParticipation__c, VTD1_Actual_Visit__r.VTD1_Onboarding_Type__c,
                VTD1_Participant_User__r.TimeZoneSidKey, VTD1_Actual_Visit__r.VTD2_PatientName_PatientId__c, VTD1_Actual_Visit__r.VTR2_Televisit__c
        from Visit_Member__c where Id in : ids];
        List <VT_R3_EmailsSender.ParamsHolder> emaiParamsHolderList = new List <VT_R3_EmailsSender.ParamsHolder>();
        for (Visit_Member__c member : filtered) {
            if ((member.VTD1_Actual_Visit__r.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED ||
                    member.VTD1_Actual_Visit__r.VTD1_Status__c
                            == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED)
                        && member.VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c != null) {
                System.debug('member = ' + member);
                if (member.VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c.addHours(-1) <= currentTime
                        && !visitMemberHandledIds.contains(member.Id)) {
                    visitMemberHandledIds.add(member.Id);
                    System.debug('added to visitMemberHandledIds ' + visitMemberHandledIds);
                    continue;
                }
//                Email Added visitMember
                System.debug('&&&&&&&&&&&&&&&&& ' + visitMemberAddedEmailMap.keySet().contains(member.VTD1_Member_Type__c));
                if(member.VTD1_Participant_User__c == null){
                    emailTemplateName = (member.VTD1_Actual_Visit__r.VTR2_Televisit__c && member.VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c < System.now().addHours(1))
                                            ? visitMemberAddedEmailMap.get('External_TV') : visitMemberAddedEmailMap.get('External');
                }
                else if(visitMemberAddedEmailMap.keySet().contains(member.VTD1_Member_Type__c)) {
                    emailTemplateName = visitMemberAddedEmailMap.get(member.VTD1_Member_Type__c);
                }
                else{
                    emailTemplateName = 'VTR2_Visit_Member_Added';
                }
                emaiParamsHolderList.add(new VT_R3_EmailsSender.ParamsHolder(
                        member.VTD1_Participant_User__c,
                        member.Id,
                        emailTemplateName,
                        member.VTD1_External_Participant_Email__c,
                        true
                ));
            }
        }
        if (!emaiParamsHolderList.isEmpty() && !Test.isRunningTest()) {
            VT_R3_EmailsSender.sendEmails(emaiParamsHolderList); //Messaging.sendEmail(mails);
        }
    }
    public void setEmailsAndParticipantTypes(List<Visit_Member__c> members, Map<Id, Visit_Member__c> oldMap) {
        List<Id> userIds = new List<Id>();
        List<Visit_Member__c> membersToUpdate = new List<Visit_Member__c>();
        for (Visit_Member__c member : members) {
            if ((oldMap == null || member.VTD1_Participant_User__c != oldMap.get(member.Id).VTD1_Participant_User__c)
                    && member.VTD1_Participant_User__c != null) {
                userIds.add(member.VTD1_Participant_User__c);
                membersToUpdate.add(member);
            }
        }
        if (! membersToUpdate.isEmpty()) {
            Map<Id, User> userMap = new Map<Id, User>([SELECT Id, Email, Profile.Name FROM User WHERE Id IN :userIds]);
            for (Visit_Member__c member : membersToUpdate) {
                if (userMap.containsKey(member.VTD1_Participant_User__c)) {
                    member.VTD1_External_Participant_Email__c = userMap.get(member.VTD1_Participant_User__c).Email;
                    member.VTD1_External_Participant_Type__c = userMap.get(member.VTD1_Participant_User__c).Profile.Name;
                }
            }
        }
    }
    public void updateConfMemberIds(List<Visit_Member__c> members, Map<Id, Visit_Member__c> oldMap) {
        Map<Id, Visit_Member__c> vMembersWithChangedUserMap = new Map<Id, Visit_Member__c>();
        for (Visit_Member__c item : members) {
            if (item.VTD1_Participant_User__c != oldMap.get(item.Id).VTD1_Participant_User__c) {
                vMembersWithChangedUserMap.put(item.Id, item);
            }
        }
        if (! vMembersWithChangedUserMap.isEmpty()) {
            List<VTD1_Conference_Member__c> cMembers = new List<VTD1_Conference_Member__c>();
            for (VTD1_Conference_Member__c cMember : [
                SELECT Id, VTD1_Visit_Member__c, VTD1_User__c
                FROM VTD1_Conference_Member__c
                WHERE VTD1_Visit_Member__c IN :vMembersWithChangedUserMap.keySet()
                AND VTD1_Visit_Member__r.VTD1_Actual_Visit__r.VTD1_Status__c = :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED
            ]) {
                //
                cMember.VTD1_User__c = vMembersWithChangedUserMap.get(cMember.VTD1_Visit_Member__c).VTD1_Participant_User__c;
                cMembers.add(cMember);
            }
            if (! cMembers.isEmpty()) { update cMembers; }
        }
    }
}