/**
 * Created by Belkovskii Denis on 6/25/2020.
 */

public class VT_R4_CaregiverInitiationActions extends Handler implements Callable{
    @TestVisible
    static List<User> caregivers = new List<User>();
    @TestVisible
    static List<User> patients = new List<User>();

    @TestVisible
    static List<User> caregiversAndPatients = new List<User>();


    public override void onBeforeInsert(Handler.TriggerContext context){
        getPatientsAndCaregivers(context.newList, context.oldMap, context.operationType);
    }

    public override void onAfterInsert(Handler.TriggerContext context){
        executeCaregiverInitiationActions();
    }

    public override void onBeforeUpdate(Handler.TriggerContext context){
        getPatientsAndCaregivers(context.newList, context.oldMap, context.operationType);
    }

    public override void onAfterUpdate(Handler.TriggerContext context){
        executeCaregiverInitiationActions();
        setCaregiverNameAndTimezoneToCase(context.newMap, context.oldMap);
    }

    public static void executeCaregiverInitiationActions(){
        if(!caregiversAndPatients.isEmpty() && !VT_R4_PatientConversion.patientConversionInProgress){
            List<Id> caregiversAndPatientsIds = new List<Id>();
            for(User caregiverOrPatient: caregiversAndPatients){
                caregiversAndPatientsIds.add(caregiverOrPatient.Id);
            }
            VTR4_New_Transaction__e eventToStartActionsExecution = new VTR4_New_Transaction__e(
                    VTR4_HandlerName__c = 'VT_R4_CaregiverInitiationActions',
                    VTR4_Parameters__c = JSON.serialize(caregiversAndPatientsIds)
            );
            if(Test.isRunningTest()){
                List<VTR4_New_Transaction__e> eventsList = new List<VTR4_New_Transaction__e>{eventToStartActionsExecution};

                call('processEvents', new Map<String, Object>{'events' => eventsList});
            }
            else{
                EventBus.publish(eventToStartActionsExecution); 
            }
        }
    }

    public static Object call(String action, Map<String, Object> args){
        if(action == 'processEvents'){
            List<VTR4_New_Transaction__e> events = (List<VTR4_New_Transaction__e>) args.get('events');
            List<Id> caregiversAndPatientsIds = (List<Id>) JSON.deserialize(events[0].VTR4_Parameters__c, List<Id>.class);
            caregiversAndPatients = [
                    SELECT Id,
                           AccountId,
                           ContactId,
                           VTD1_Profile_Name__c,
                           VTD2_UserTimezone__c,
                           FirstName,
                           LastName
                    FROM User
                    WHERE Id IN :caregiversAndPatientsIds
                    AND AccountId != NULL
            ];
            if(caregiversAndPatients.isEmpty()){
                return null;
            }
            else{
                assignToPatientMessageFeedOfStudy(caregiversAndPatients);
            }

            List<Id> caregiverAccountIds = new List<Id>();
            for(User user: caregiversAndPatients){
                if(user.VTD1_Profile_Name__c == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME){
                    caregivers.add(user);
                    if(user.AccountId != null){
                        caregiverAccountIds.add(user.AccountId);
                    }
                }
            }

            if(caregiverAccountIds.isEmpty()){
                return null;
            }

            Map<Id, Account> accountMap = new Map<Id, Account>([
                    SELECT  Id,
                            HealthCloudGA__CarePlan__c,
                            HealthCloudGA__CarePlan__r.VTD1_Study__c,
                            HealthCloudGA__CarePlan__r.VTD1_Study__r.VTD1_Patient_Group_Id__c,
                            HealthCloudGA__CarePlan__r.VTD1_Patient_User__c,
                            HealthCloudGA__CarePlan__r.VTD1_Backup_PI_User__c,
                            HealthCloudGA__CarePlan__r.VTD1_PI_user__c,
                            HealthCloudGA__CarePlan__r.VTD1_Primary_PG__c,
                            HealthCloudGA__CarePlan__r.VTD1_Secondary_PG__c,
                            HealthCloudGA__CarePlan__r.VTR2_SiteCoordinator__c
                    FROM Account
                    WHERE Id IN :caregiverAccountIds
                    AND HealthCloudGA__CarePlan__c != NULL
            ]);


            if(!caregivers.isEmpty() && !accountMap.isEmpty()){
                updateUserIdCsmOnContact(caregivers, accountMap);
                accessToPatientObjects(caregivers, accountMap);
                createTaskRelations(caregivers, accountMap);
                grantAccessToStudyMembers(caregivers, accountMap);
            }
        }
        return null;
    }

    public static void assignToPatientMessageFeedOfStudy(List<User> triggerUsers) {
        Set<Id> account_Ids = new Set<Id>();
        for (User user: triggerUsers) {

            if(user.AccountId != null){
                account_Ids.add(user.AccountId);
            }
        }

        if(account_Ids.isEmpty()) return;
        Map<Id, Account> accountsMap = new Map<Id, Account> ([
                SELECT Id, Candidate_Patient__r.Study__r.VTD1_Patient_Group_Id__c
                FROM Account
                WHERE Id IN :account_Ids

                AND Candidate_Patient__r.Study__r.VTD1_Patient_Group_Id__c != NULL

        ]);
        if (!accountsMap.isEmpty()) {
            List<VTD1_Chat_Member__c> chatMembersToCreate = new List<VTD1_Chat_Member__c>();
            for (User user: triggerUsers) {
                if(user.AccountId != null){
                    Account accWithGroup = accountsMap.get(user.AccountId);
                    if (accWithGroup != null) {
                        chatMembersToCreate.add(new VTD1_Chat_Member__c(
                                VTD1_Chat__c = accWithGroup.Candidate_Patient__r.Study__r.VTD1_Patient_Group_Id__c,
                                VTD1_User__c = user.Id
                        ));
                    }
                }
            }
            if (!chatMembersToCreate.isEmpty()) {
                insert chatMembersToCreate;
            }
        }
    }

    public static void updateUserIdCsmOnContact(List<User> triggerUsers, Map<Id, Account> accountMap){


        Set<Contact> contacts = new Set<Contact>();


        for (User user: triggerUsers) {
            if(user.AccountId != null){
                Account acc = accountMap.get(user.AccountId);
                if (acc.HealthCloudGA__CarePlan__c != null) {
                    contacts.add(new Contact(Id = user.ContactId,
                            VTD1_Clinical_Study_Membership__c = acc.HealthCloudGA__CarePlan__c,
                            VTD1_UserId__c = user.Id));
                }
            }
        }
        if (!contacts.isEmpty()) {
            update new List<Contact>(contacts);
        }
    }

    public static void accessToPatientObjects(List<User> triggerUsers, Map<Id, Account> accountMap){
        List<AccountShare> accountShares = new List<AccountShare>();
        Map<Id, Id> userId_accountIds = new Map<Id, Id>();

        List<VTD1_Study_Sharing_Configuration__c> studySharingConfigurations = new List<VTD1_Study_Sharing_Configuration__c>();
        List<UserShare> userShares = new List<UserShare>();
        for (User user: triggerUsers) {
            if(user.AccountId == null){
                continue;
            }
            studySharingConfigurations.add(new VTD1_Study_Sharing_Configuration__c(
                    VTD1_Study__c = accountMap.get(user.AccountId).HealthCloudGA__CarePlan__r.VTD1_Study__c,
                    VTD1_User__c = user.Id,
                    VTD1_Access_Level__c = 'Edit'
            ));
            userShares.add(new UserShare(
                    RowCause = 'Manual',
                    UserAccessLevel = 'Edit',
                    UserId = accountMap.get(user.AccountId).HealthCloudGA__CarePlan__r.VTD1_Patient_User__c,
                    UserOrGroupId = user.Id
            ));

            userId_accountIds.put(user.Id, user.AccountId);
            AccountShare accountShare = new AccountShare(
                    AccountId = user.AccountId,
                    UserOrGroupId = user.Id,
                    AccountAccessLevel = 'Edit',
                    CaseAccessLevel = 'Edit',
                    ContactAccessLevel = 'Edit',
                    OpportunityAccessLevel = 'None',
                    RowCause = 'Manual'
            );
            accountShares.add(accountShare);
        }
        insert studySharingConfigurations;
        insert userShares;
        insert accountShares;
    }

    public static void grantAccessToStudyMembers(List<User> triggerUsers, Map<Id, Account> accountMap) {
        Set<String> fieldsToCheck = new Set<String>{'VTD1_Backup_PI_User__c',
                'VTD1_PI_user__c',
                'VTD1_Primary_PG__c',
                'VTD1_Secondary_PG__c',
                'VTR2_SiteCoordinator__c'};
        List<UserShare> userSharesToCreate = new List<UserShare>();
        for (User u : triggerUsers) {
            Account acc = accountMap.get(u.AccountId);
            if (acc.HealthCloudGA__CarePlan__c != null) {
                for (String userFieldName : fieldsToCheck) {
                    Id carePlanId = (Id) acc.HealthCloudGA__CarePlan__r.get(userFieldName);
                    if (carePlanId != null) {
                        System.debug('userFieldName is: ' + userFieldName);
                        userSharesToCreate.add(new UserShare(
                                RowCause = 'Manual',
                                UserAccessLevel = 'Edit',
                                UserId = u.Id,
                                UserOrGroupId = carePlanId)
                        );
                    }
                }
            }
        }
        if (!userSharesToCreate.isEmpty()) {
            Database.insert(userSharesToCreate, false);
        }
    }

    public static void setCaregiverNameAndTimezoneToCase(Map<Id, SObject> newMap, Map<Id, SObject>oldMap){
        Set<Id> userIdsWithChangedTimezone = new Set<Id>();
        Set<Id> userIdsWithChangedName = new Set<Id>();
        for(SObject sobjectVar: newMap.values()){
            User user = (User) sobjectVar;
            User oldUser = (User) oldMap.get(user.Id);
            if(user.VTD1_Profile_Name__c == 'Caregiver' && user.ContactId != null){
                if(user.VTD2_UserTimezone__c != oldUser.VTD2_UserTimezone__c){
                    userIdsWithChangedTimezone.add(user.Id);
                }
                if(user.FirstName != oldUser.FirstName || user.LastName != oldUser.LastName){
                    userIdsWithChangedName.add(user.Id);
                }
            }
        }

        if(!userIdsWithChangedName.isEmpty() || !userIdsWithChangedTimezone.isEmpty()){
            List<SObject> recordsToUpdate = new List<SObject>();
            for (Contact c : [
                    SELECT
                            VTD1_UserId__c,
                            Account.Id,
                            Account.HealthCloudGA__CarePlan__r.Id,
                            Account.HealthCloudGA__CarePlan__r.Caregiver_s_TimeZone__c,
                            Account.VTD2_Primary_Caregiver_s_TimeZone__c
                    FROM Contact
                    WHERE VTD1_UserId__c IN :userIdsWithChangedTimezone OR
                    VTD1_UserId__c IN :userIdsWithChangedName
                    //AND VTD1_Primary_CG__c = TRUE
            ])
            {
                User u = (User) newMap.get(c.VTD1_UserId__c);
                Boolean isCaseAdded = false;
                if (userIdsWithChangedTimezone.contains(u.Id)) {
                    c.Account.HealthCloudGA__CarePlan__r.Caregiver_s_TimeZone__c = u.TimeZoneSidKey;
                    c.Account.VTD2_Primary_Caregiver_s_TimeZone__c = u.VTD2_UserTimezone__c;
                    recordsToUpdate.add(c.Account.HealthCloudGA__CarePlan__r);
                    recordsToUpdate.add(c.Account);
                    isCaseAdded = true;
                }
                if (userIdsWithChangedName.contains(u.Id)) {
                    c.Account.HealthCloudGA__CarePlan__r.Caregiver_s_Name__c = u.VTD1_Full_Name__c;
                    if (!isCaseAdded) {
                        recordsToUpdate.add(c.Account.HealthCloudGA__CarePlan__r);
                    }
                }
            }
            update recordsToUpdate;
        }

    }

    public static void createTaskRelations(List<User> triggerUsers, Map<Id, Account> accountMap){
        Map<Id, List<Id>> accountIds_cgId = new Map<Id,  List<Id>>();
        for(User user: triggerUsers) {
            if (user.AccountId != null) {
                List<Id> cgList = accountIds_cgId.get(user.AccountId);
                if (cgList == null) {
                    cgList = new List<Id> ();
                    accountIds_cgId.put(user.AccountId, cgList);
                }
                cgList.add(user.ContactId);
            }
        }

        Map<Id, List<Id>> userIds_cgId = new Map<Id,  List<Id>>();
        for (Account acc : accountMap.values()) {
            if(acc.HealthCloudGA__CarePlan__r.VTD1_Study__c != null
                    && acc.HealthCloudGA__CarePlan__r.VTD1_Patient_User__c != null
                    && acc.HealthCloudGA__CarePlan__c != null){
                userIds_cgId.put(acc.HealthCloudGA__CarePlan__r.VTD1_Patient_User__c, accountIds_cgId.get(acc.Id));
            }
        }
        if(userIds_cgId.isEmpty()) return;
        List<Task> tasks = [SELECT Id, OwnerId FROM Task WHERE Status = 'Open' AND OwnerId IN :userIds_cgId.keySet()];
        if(!tasks.isEmpty()) {
            List<TaskRelation> taskRelations = new List<TaskRelation>();
            for(Task task: tasks){
                List<Id> cgList = userIds_cgId.get(task.OwnerId);
                for (Id cgContactId : cgList) {
                    taskRelations.add(new TaskRelation(TaskId = task.Id, RelationId = cgContactId));
                }
            }
            insert taskRelations;
        }
    }

    public static void getPatientsAndCaregivers(List<Object> newList, Map<Id, Object> oldMap, TriggerOperation operationType) {
        for(User user: (List<User>) newList) {
            User oldUser = new User();
            if(oldMap != null){
                oldUser = (User) oldMap.get(user.Id);
            }
            if(((user.IsActive && operationType == TriggerOperation.BEFORE_INSERT)
                    || (oldMap != null && oldUser != null && !oldUser.IsActive))
                    && !user.InitialActionsComplete__c 


                    && user.ContactId != null
                    && (user.VTD1_Profile_Name__c == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
                    || user.VTD1_Profile_Name__c == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME)
                    ) { 

                user.InitialActionsComplete__c = true;
                caregiversAndPatients.add(user);
            }
        }
    }
}