/**
 * Created by Leonid Bartenev
 */

public class VT_D1_CloneStudyController {
    
    private Id studyId;
    public HealthCloudGA__CarePlanTemplate__c inputStudy {get; set;}
    
    public VT_D1_CloneStudyController(ApexPages.StandardController sc){
        studyId = sc.getId();
        inputStudy = [SELECT Id, Name FROM HealthCloudGA__CarePlanTemplate__c WHERE Id =: studyId];
        inputStudy.Name += ' - Clone';
    }
    
    public PageReference cloneAction(){
        
        //prepare study:
        List<String> allStudyFields = new List<String>(HealthCloudGA__CarePlanTemplate__c.SObjectType.getDescribe().fields.getMap().keySet());
        String queryStr = 'SELECT ' + String.join(allStudyFields, ',') + ' FROM HealthCloudGA__CarePlanTemplate__c WHERE Id= \'' + studyId +  '\'';
        HealthCloudGA__CarePlanTemplate__c study = Database.query(queryStr);
        //clone study:
        HealthCloudGA__CarePlanTemplate__c cloneStudy = study.clone();
        cloneStudy.Name = inputStudy.Name;
        VTD1_Chat__c patientGroup = new VTD1_Chat__c(Name = cloneStudy.Name + ' Patient Group', VTD1_Chat_Type__c = 'Broadcast');
        VTD1_Chat__c deliveryTeamGroup = new VTD1_Chat__c(Name = cloneStudy.Name + ' DeliveryTeamGroup', VTD1_Chat_Type__c = 'Chat');
        VTD1_Chat__c piGroup = new VTD1_Chat__c(Name = cloneStudy.Name + ' PI Group', VTD1_Chat_Type__c = 'Chat');
        CollaborationGroup scGroup = new CollaborationGroup(Name = cloneStudy.Name + ' SC Chatter Group', CollaborationType = 'Private');
        CollaborationGroup pgStudyGroup = new CollaborationGroup(Name = cloneStudy.Name + ' PG Chatter Group', CollaborationType = 'Private');
        List<VTD1_Chat__c> chats = new List<VTD1_Chat__c>{patientGroup, deliveryTeamGroup, piGroup};
        List<CollaborationGroup> collaborationGroups = new List<CollaborationGroup>{scGroup, pgStudyGroup};
        insert chats;
        insert collaborationGroups;
        cloneStudy.VTD1_Patient_Group_Id__c = patientGroup.Id;
        cloneStudy.VTD1_DeliveryTeamGroupId__c = deliveryTeamGroup.Id;
        cloneStudy.VTD1_PIGroupId__c = piGroup.Id;
        cloneStudy.VTD1_SCGroupId__c = scGroup.Id;
        cloneStudy.VTD1_PG_Study_Group_Id__c = pgStudyGroup.Id;
        insert cloneStudy;
        for (VTD1_Chat__c chat : chats) {
            chat.VTR2_Study__c = cloneStudy.Id;
        }
        update chats;
        
        //prepare VS List:
        List<String> allVSFields = new List<String>(Virtual_Site__c.SObjectType.getDescribe().fields.getMap().keySet());
        queryStr = 'SELECT ' + String.join(allVSFields, ',') + ' FROM Virtual_Site__c WHERE VTD1_Study__c= \'' + studyId +  '\'';
        List<Virtual_Site__c> vsList = Database.query(queryStr);
        //clone VS
        Map<Id, Virtual_Site__c> cloneVSMap = new Map<Id, Virtual_Site__c>();
        for(Virtual_Site__c vs : vsList){
            Virtual_Site__c cloneVS = vs.clone();
            cloneVS.VTD1_Study__c = cloneStudy.Id;
            cloneVS.Name = cloneVS.Name.replace(study.Name, cloneStudy.Name);
            cloneVSMap.put(vs.Id, cloneVS);
        }
        Database.insert(cloneVSMap.values(), true);
        
        //prepare STM list
        List<String> allSTMFields = new List<String>(Study_Team_Member__c.SObjectType.getDescribe().fields.getMap().keySet());
        queryStr = 'SELECT ' + String.join(allSTMFields, ',') + ' FROM Study_Team_Member__c WHERE Study__c= \'' + studyId +  '\'';
        List<Study_Team_Member__c> stmList = Database.query(queryStr);
        Map<Id, Study_Team_Member__c> stmCloneMap = new Map<Id, Study_Team_Member__c>();
        //clone stm list
        for(Study_Team_Member__c stm : stmList) {
            Study_Team_Member__c cloneSTM = stm.clone();
            cloneSTM.Study__c = cloneStudy.Id;
            stmCloneMap.put(stm.Id, cloneSTM);
        }
        Database.insert(stmCloneMap.values(), true);
        //check lookups:
        for(Study_Team_Member__c cloneSTM : stmCloneMap.values()){
            if(stmCloneMap.containsKey(cloneSTM.Study_Team_Member__c)) cloneSTM.Study_Team_Member__c = stmCloneMap.get(cloneSTM.Study_Team_Member__c).Id;
            if(cloneVSMap.containsKey(cloneSTM.VTD1_VirtualSite__c)) cloneSTM.VTD1_VirtualSite__c = cloneVSMap.get(cloneSTM.VTD1_VirtualSite__c).Id;
        }
        Database.upsert(stmCloneMap.values(), true);
        
        //prepare SSTM:
        List<String> allSSTMFields = new List<String>(Study_Site_Team_Member__c.SObjectType.getDescribe().fields.getMap().keySet());
        queryStr = 'SELECT ' + String.join(allSSTMFields, ',') + ' FROM Study_Site_Team_Member__c ' +
                ' WHERE VTD1_Associated_PG__c IN (\'' + String.join(new List<Id>(stmCloneMap.keySet()), '\',\'') +  '\') ' +
                ' OR VTD1_Associated_PI__c IN (\'' + String.join(new List<Id>(stmCloneMap.keySet()), '\',\'') +  '\')';
        List<Study_Site_Team_Member__c> sstmList = Database.query(queryStr);
        Map<Id, List<Study_Site_Team_Member__c>> piAssociatedMap = new Map<Id, List<Study_Site_Team_Member__c>>();
        Map<Id, List<Study_Site_Team_Member__c>> pgAssociatedMap = new Map<Id, List<Study_Site_Team_Member__c>>();
        for(Study_Site_Team_Member__c sstm : sstmList){
            //pi prepare
            List<Study_Site_Team_Member__c> piList = piAssociatedMap.get(sstm.VTD1_Associated_PI__c);
            if(piList == null) piList = new List<Study_Site_Team_Member__c>();
            piList.add(sstm);
            piAssociatedMap.put(sstm.VTD1_Associated_PI__c, piList);
            //pg prepare
            List<Study_Site_Team_Member__c> pgList = piAssociatedMap.get(sstm.VTD1_Associated_PG__c);
            if(pgList == null) pgList = new List<Study_Site_Team_Member__c>();
            pgList.add(sstm);
            pgAssociatedMap.put(sstm.VTD1_Associated_PG__c, pgList);
        }
        
        //process SSTM
        Map<Id, Study_Site_Team_Member__c> sstmCloneMap = new Map<Id, Study_Site_Team_Member__c>();
        for(Study_Team_Member__c stm : stmList){
            List<Study_Site_Team_Member__c> piList = piAssociatedMap.get(stm.Id);
            if(piList != null){
                for(Study_Site_Team_Member__c sstm : piList){
                    Study_Site_Team_Member__c cloneSSTM = sstm.clone();
                    cloneSSTM.VTD1_Associated_PI__c = stmCloneMap.get(stm.Id).Id;
                    sstmCloneMap.put(sstm.Id, cloneSSTM);
                    if(stmCloneMap.containsKey(sstm.VTD1_Associated_PG__c)) cloneSSTM.VTD1_Associated_PG__c = stmCloneMap.get(cloneSSTM.VTD1_Associated_PG__c).Id;
                }
            }
            List<Study_Site_Team_Member__c> pgList = pgAssociatedMap.get(stm.Id);
            if(pgList != null){
                for(Study_Site_Team_Member__c sstm : pgList){
                    Study_Site_Team_Member__c cloneSSTM = sstm.clone();
                    cloneSSTM.VTD1_Associated_PG__c = stmCloneMap.get(stm.Id).Id;
                    sstmCloneMap.put(sstm.Id, cloneSSTM);
                    if(stmCloneMap.containsKey(sstm.VTD1_Associated_PI__c)) cloneSSTM.VTD1_Associated_PI__c = stmCloneMap.get(cloneSSTM.VTD1_Associated_PI__c).Id;
                }
            }
        }
        Database.insert(sstmCloneMap.values(), true);
        return new PageReference('/' + cloneStudy.Id);
    }

}