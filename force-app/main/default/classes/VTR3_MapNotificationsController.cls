/**
 * Created by user on 28.10.2019.
 */

public with sharing class VTR3_MapNotificationsController {

    public static void mapNotifications() {

        List<VTD2_TN_Catalog_Code__c> tasksList = [
                SELECT Id, VTR3_T_TN_Notification_code__c,VTR3_T_TN_Notification__c
                FROM VTD2_TN_Catalog_Code__c
                WHERE RecordType.Name = 'Task'
                AND VTR3_T_TN_Notification_code__c != null
        ];

        Map<String, Id> codeToNotificationMap = new Map<String, Id>();
        for (VTD2_TN_Catalog_Code__c task : tasksList) {
            codeToNotificationMap.put(task.VTR3_T_TN_Notification_code__c, null);
        }

        for (VTD2_TN_Catalog_Code__c notification : [
                SELECT Id, VTD2_T_Task_Unique_Code__c
                FROM VTD2_TN_Catalog_Code__c
                WHERE RecordType.Name = 'Notification'
                AND VTD2_T_Task_Unique_Code__c IN :codeToNotificationMap.keySet()
        ]) {
            codeToNotificationMap.put(notification.VTD2_T_Task_Unique_Code__c, notification.Id);
        }

        for (VTD2_TN_Catalog_Code__c task : tasksList) {
            task.VTR3_T_TN_Notification__c = codeToNotificationMap.get(task.VTR3_T_TN_Notification_code__c);
        }
        try {
            update tasksList;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Done'));
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
}