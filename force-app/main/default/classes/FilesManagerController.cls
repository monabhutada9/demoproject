public without sharing class FilesManagerController {
    public String getSrc() {
        Id studyId = Id.valueOf(ApexPages.currentPage().getParameters().get('studyId'));
        String sponsorLogoRTA = [SELECT VTR5_Sponsor_Logo__c FROM HealthCloudGA__CarePlanTemplate__c WHERE Id = :studyId].VTR5_Sponsor_Logo__c;
        System.debug(sponsorLogoRTA);
        String sponsorLogoUrl = 'https' + sponsorLogoRTA.unescapeHtml4().substringBetween('https', '"');
        PageReference logoReference = new PageReference(sponsorLogoUrl);
        String logoBase64 = EncodingUtil.base64Encode(logoReference.getContent());
        return 'data:image/jpeg;base64,' + logoBase64;
    }
}