/**
 * Created by Denis Belkovskii on 10/9/2020.
 * SH-17308 PB 'SC_3rd Party Daily Report Upload - Create SC Task'
 * To schedule the job execute this script: System.schedule('StudyBulkUploadBatch', '0 00 00 * * ?', new VT_R5_StudyBulkUploadBatch());
 */

public without sharing class VT_R5_StudyBulkUploadBatch implements Database.Batchable<sObject>, Schedulable {

    public Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id, VTD1_SC_Tasks_Queue_Id__c, VTD1_Next_Bulk_Upload_Date__c ' +
                'FROM HealthCloudGA__CarePlanTemplate__c ' +
                'WHERE VTD1_Next_Bulk_Upload_Date__c = TODAY ' +
                'AND VTD1_Bulk_Upload_Flag__c = true';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<HealthCloudGA__CarePlanTemplate__c> scope) {
        List<HealthCloudGA__CarePlanTemplate__c> scopeForUpdate = new List<HealthCloudGA__CarePlanTemplate__c>();
        List<String> tnTaskCodes = new List<String>();
        List<Id> taskSourceIds = new List<Id>();
        for (HealthCloudGA__CarePlanTemplate__c study : scope) {
            scopeForUpdate.add(new HealthCloudGA__CarePlanTemplate__c(
                    Id = study.Id,
                    VTD1_Next_Bulk_Upload_Date__c = study.VTD1_Next_Bulk_Upload_Date__c.addDays(1))
            );
            if(study.VTD1_SC_Tasks_Queue_Id__c != null) {
                tnTaskCodes.add('T632');
                taskSourceIds.add(study.Id);
            }
        }
        update scopeForUpdate;
        if (!taskSourceIds.isEmpty() && !tnTaskCodes.isEmpty()) {
            VT_D2_TNCatalogTasks.generateTasks(tnTaskCodes, taskSourceIds, null, null);
        }
    }

    public void finish(Database.BatchableContext BC) {}

    public void execute(SchedulableContext SC) {
        VT_R5_StudyBulkUploadBatch studyBulkUploadBatch = new VT_R5_StudyBulkUploadBatch();
        Database.executeBatch(studyBulkUploadBatch);
    }
}