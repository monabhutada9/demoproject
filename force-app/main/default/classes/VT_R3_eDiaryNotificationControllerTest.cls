@isTest
private class VT_R3_eDiaryNotificationControllerTest {

    testMethod static void testGenerateMessage() {

        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        HealthCloudGA__CandidatePatient__c candidatePatient = VT_D1_TestUtils.createPatientCandidate(
                'Ter', 'kin', VT_D1_TestUtils.generateUniqueUserName(), 'terkin@test.com', 'terkin', 'Converted', study.Id, 'PN', '11-11-11', 'US', 'NY');
        Test.stopTest();

        Case carePlan = [
                SELECT Id, VTD1_Primary_PG__c, VTD1_Secondary_PG__c, VTR2_SiteCoordinator__c, VTR2_Backup_Site_Coordinator__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
        ];

        VTD1_Survey__c survey = new VTD1_Survey__c(VTD1_CSM__c = carePlan.Id, VTD1_Due_Date__c = Date.today());
        insert survey;

        VT_R3_eDiaryNotificationController cntrl = new VT_R3_eDiaryNotificationController();
        cntrl.recipientType = 'test';
        System.assertEquals(null, cntrl.getMainBody());
        cntrl.eDiaryId = survey.Id;
        cntrl.userLanguage = UserInfo.getLanguage();
        cntrl.eDiaryFlag = false;
        System.assertNotEquals(null, cntrl.getMainBody());
        cntrl.eDiaryFlag = true;
        System.assertNotEquals(null, cntrl.getMainBody());
        System.assertNotEquals(null, cntrl.getSalutation());
    }

}