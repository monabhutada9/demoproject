/**
 * Created by Jane Ivanova
 *
 * Integration Action to send status to RH.
 *
 * Example of usage:
 *
 * VT_D2_QAction_SendSubjectStatus sendSubjectStatusAction = new VT_D2_QAction_SendSubjectStatus('someCaseIdHere');
 * sendSubjectStatusAction.execute();
 *
 * For async use:
 * sendSubjectStatusAction.executeFuture();
 *
 */

public class VT_D2_QAction_SendSubjectStatus extends VT_D1_AbstractAction {
    private Id caseId;
    
    public VT_D2_QAction_SendSubjectStatus(Id caseId) {
        this.caseId = caseId;
    }
    
    //action logic implementation:
    public override void execute() {
        if (caseId != null) {
            System.debug('Processing send subject status for + ' + caseId + ' started');
            //VT_D1_HTTPConnectionHandler.Result result = VT_D1_SubjectIntegration.upsertSubject(caseId);
            VT_D1_SubjectIntegration.SubjectIntegrationResponse result = VT_D1_SubjectIntegration.sendSubjectStatus(caseId);
            HttpResponse response = result.httpResponse;
            System.debug('response = ' + response);
            Case cs = new Case(Id = caseId);
            if(response == null) {
                //time out, action will not be added to queue:
                setStatus(STATUS_FAILED);
                setMessage(result.log.VTD1_ErrorMessage__c);
//            }else if(response.getStatusCode() >= 500 && response.getStatusCode() < 600) {
//                //internal server error, action will be added to queue:
//                setStatus(STATUS_FAILED);
//                setMessage('Server return status code: ' + response.getStatusCode() + ' ' + response.getStatus() + '; Body: ' + response.getBody());
//                addToQueue();
            }else if(response.getStatusCode() == 200){
                //success
                setStatus(STATUS_SUCCESS);
            }else{
                //other errors, action will not be added to queue
                setStatus(STATUS_FAILED);
                setMessage('Server return: ' + response.getStatusCode() + ' ' + response.getStatus() + ', Body: ' + response.getBody());
            }
            cs.VTD2_Send_Status_Log__c = result.log.Id;
    
//            //after all attempts, create tasks:
//            if(isLastAttempt() && getStatus() != STATUS_SUCCESS){
//                createTasksForCase(cs, 'Investigate Send Status Failure', 'Send status failure after all attempts: ' + getMessage());
//            }
    
            update cs;

        }
    }
    
    public override Type getType() {
        return VT_D2_QAction_SendSubjectStatus.class;
    }

}