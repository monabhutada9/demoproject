/**
* @author Ruslan Mullayanov
*/

public with sharing class VT_D2_PIEligibilityAssessmentFormReview {

    @AuraEnabled
    public static String getEligibilityData(Id taskId) {
        System.debug('taskId___'+taskId);
        try {
            EligibilityData elData = new EligibilityData();
            System.debug('EligibilityDat____'+elData);
            List<Task> tasks = getTasks(taskId);
            System.debug('Task____'+tasks);
            if (!tasks.isEmpty()) {
                System.debug('we inter not empty task');
                elData.task = tasks[0];
                System.debug('elData.task'+elData.task);
                elData.document = getDocument(tasks[0].Document__c);
                System.debug('elData.document'+elData.document);
                if (elData.document!=null) {
                    elData.pdfData = getContentAsPDF(tasks[0].Document__c);
                    System.debug('elData-pdfData' + elData.pdfData);
                    System.debug('inter in elData.document!=null block');
                }
                System.debug('after elData block');
            }
            System.debug(elData + 'elData****');
            return JSON.serialize(elData);
        } catch (Exception e){
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static void submitEligibility(String docString) {
        if (docString == null) return;
        try {
            VTD1_Document__c document = (VTD1_Document__c) JSON.deserialize(docString, VTD1_Document__c.class);
            String decision = document.VTD2_Final_Eligibility_Decision__c;
            String comments = document.VTD2_Final_Review_Comments__c;
            if (document.Id==null || decision==null || comments==null) {
                throw new AuraHandledException('Wrong request');
            }
            VTD1_Document__c docToUpdate = new VTD1_Document__c();
            docToUpdate.Id = document.Id;
            docToUpdate.VTD2_Final_Eligibility_Decision__c = decision;
            docToUpdate.VTD2_Final_Review_Comments__c = comments;
            update document;
        } catch (Exception e){
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    //-------------------------- HELPERS ---------------------------//
    @TestVisible
    private static String getContentAsPDF(Id docId) {
        System.debug('IdDOc**' + docId);
        if (docId == null)
            return null;

        List<ContentDocumentLink> cdl = getCDL(docId);
        System.debug('cdl____'+cdl[0].ContentDocumentId);
        ContentDocument contentDocumentl = [
                SELECT  Id,
                        FileType
                FROM    ContentDocument
                WHERE   Id =: cdl[0].ContentDocumentId
        ];
        System.debug('contentDocumentType ' + contentDocumentl.FileType);
        if(String.isNotEmpty(contentDocumentl.FileType) && contentDocumentl.FileType.toLowerCase().equals('pdf')) {
            if (!cdl.isEmpty()) {
                System.debug('inter cdl no empty ');
                PageReference pageRef = new PageReference('/sfc/servlet.shepherd/document/download/' + cdl[0].ContentDocumentId);
                String data = EncodingUtil.base64Encode(pageRef.getContentAsPDF());
                return data;
            } else {
                return null;
            }
        }
        //String notSupportedMsg = '<html><div style="font-family: Proxima Nova, semibold ;font-size: 20px;">This document format is not supported for preview. If needed, please download</div></html>';
        //        String notSupportedMsg = 'This document format is not supported for preview. If needed, please download';
        String notSupportedMsg = System.Label.VTR2_UnsupportedFormatWarning;
        String data = EncodingUtil.base64Encode(Blob.toPdf(notSupportedMsg));
        return data;
    }
    private static VTD1_Document__c getDocument(Id docId) {
        if (docId == null) return null;
        List<VTD1_Document__c> document = getDocs(docId);
        if (!document.isEmpty()) {
            return document[0];
        } else {
            return null;
        }
    }

    //--------------------------- SOQL --------------------------//

    private static List<Task> getTasks(Id taskId) {
        return [
                SELECT Id
                , Document__c
                , HealthCloudGA__CarePlanTemplate__c
                , HealthCloudGA__CarePlanTemplate__r.Name
                , VTD1_Case_lookup__r.VTD1_Patient_User__r.Name
                , Document__r.VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c
                , VTD2_Primary_Investigator__c
                FROM Task
                WHERE Id =: taskId
                AND VTD1_Type_for_backend_logic__c  = 'Final Eligibility Decision Task'];
    }
    private static List<VTD1_Document__c> getDocs(Id docId) {
        return [
                SELECT Id,
                       VTD1_Eligibility_Assessment_Status__c,
                       VTD2_TMA_Eligibility_Decision__c,
                       VTD2_TMA_Comments__c,
                       VTD2_Final_Eligibility_Decision__c,
                       VTD2_Final_Review_Comments__c
                FROM VTD1_Document__c
                WHERE Id = :docId
        ];
    }

    private static List<ContentDocumentLink> getCDL(Id docId) {
        System.debug('docIdInGetCDL '+docId);
        System.debug('getCDL' + [
        SELECT Id
        , ContentDocumentId
        , LinkedEntityId
        FROM ContentDocumentLink
        WHERE LinkedEntityId = :docId
                ]);
        return [
                SELECT Id
                , ContentDocumentId
                , LinkedEntityId
                FROM ContentDocumentLink
                WHERE LinkedEntityId = :docId
        ];
    }

    //------------------------- WRAPPERS ------------------------//

    public class EligibilityData {
        public Task task;
        public VTD1_Document__c document;
        public String pdfData;
    }

    //for Copado deploy
}