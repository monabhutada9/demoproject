public class VT_D1_MedicalRecordsList {
    @AuraEnabled
    public static Case getCase() {
        return VT_D1_PatientCaregiverBound.getCaseForUser();
    }

    @AuraEnabled
    public static List<VTD1_Document__c> getRecords(String caseId) {
        Map<String,Id> docRT = VT_D1_HelperClass.getRTMap().get(VT_R4_ConstantsHelper_Documents.SOBJECT_DOC_CONTAINER);
        List<Id> medicalRecordTypes = new List<Id>();
        medicalRecordTypes.add(docRT.get('Medical_Record_Archived'));
        medicalRecordTypes.add(docRT.get('Medical_Record_Release_Form'));
        medicalRecordTypes.add(docRT.get('Medical_Record_Rejected'));
        medicalRecordTypes.add(docRT.get('VTD1_Medical_Record'));

        List<VTD1_Document__c> documents;
        documents = [
            SELECT Id, Name,
                VTD1_FileNames__c,
                VTD1_Nickname__c,
                VTD1_Comment__c,
                VTD1_Version__c,
                VTD1_Status__c,
                toLabel(VTD1_Status__c) translatedStatus,
                VTD1_Does_file_needs_deletion__c,
                VTD1_Deletion_Date__c,
                VTD1_Deletion_User__c,
                VTD1_Deletion_Reason__c,
                VTD1_Why_File_is_Relevant__c,
                VTD1_Why_file_is_irrelevant__c,
                VTD1_PI_Comment__c
            FROM VTD1_Document__c
            WHERE VTD1_Clinical_Study_Membership__c = :caseId
            AND (RecordTypeId IN :medicalRecordTypes)
            AND (NOT (VTD1_Does_file_needs_deletion__c = 'Yes' AND VTD1_Deletion_Reason__c = 'Not Relevant'))
            ORDER BY Name
        ];
        VT_D1_TranslateHelper.translate(documents);
        return documents;
    }

    @AuraEnabled
    public static ContentDocumentLink getFileByRecord(Id recordId) {
        ContentDocumentLink link = [
            SELECT ContentDocumentId
            FROM ContentDocumentLink
            WHERE LinkedEntity.Id=:recordId
        ];

        return link;
    }

    @AuraEnabled
    public static ContentVersion getDocumentByRecord(Id recordId) {
        ContentDocumentLink contentDocLinkList = VT_D1_MedicalRecordsList.getFileByRecord(recordId);
        ContentVersion version = [
            SELECT ContentDocumentId, FileExtension, FileType, SharingPrivacy, Title
            FROM ContentVersion
            WHERE ContentDocumentId =: contentDocLinkList.ContentDocumentId
            AND IsLatest=TRUE
            /*AND VTD1_Current_Version__c = TRUE
            ORDER BY VTD1_CompoundVersionNumber__c DESC*/
            LIMIT 1
        ];
        return version;
    }

    @AuraEnabled
    public static VTD1_Document__c getRecordByFile(Id fileId) {
        List<ContentDocumentLink> contentDocLinkList = [
            SELECT LinkedEntity.Id, LinkedEntity.Type
            FROM ContentDocumentLink
            WHERE ContentDocumentId=:fileId
        ];

        Id contentDocId;
        for(ContentDocumentLink item: contentDocLinkList){
            if(item.LinkedEntity.Type == 'VTD1_Document__c') {
                contentDocId = item.LinkedEntity.Id;
                break;
            }
        }

        VTD1_Document__c document = [
            SELECT Id, Name, VTD1_FileNames__c, VTD1_Nickname__c, VTD1_Comment__c, VTD1_Version__c, VTD1_Status__c
            FROM VTD1_Document__c
            WHERE Id = :contentDocId
        ];
        VT_D1_TranslateHelper.translate(document);
        return document;
    }

//    @AuraEnabled
//    public static String getSiteURL(){
//        //String url = URL.getSalesforceBaseUrl().getRef();
//        String url;
//        List<VT_D1_MedicalRecordsConf__mdt> conf = [SELECT  BaseSiteURL__c FROM VT_D1_MedicalRecordsConf__mdt];
//        for (VT_D1_MedicalRecordsConf__mdt c: conf){
//            url = c.BaseSiteURL__c;
//        }
//        return url;
//    }

    @AuraEnabled
    public static String certifyDoc(Id caseId, Id docToCertifyId) {
        return VT_D2_MedicalRecordsCertifyDocsHelper.certifyDoc(caseId, docToCertifyId);
    }

    @AuraEnabled
    public static Boolean isSandbox(){
        return String.isNotEmpty(VT_D1_HelperClass.getSandboxPrefix());
    }

}