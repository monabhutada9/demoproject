/**
 * Created by User on 19/03/29.
 */
@IsTest
public with sharing class VTR2_SCRFileUploadControllerTest {

   @TestSetup
   static void dataSetup() {
       Test.startTest();
       Id docRecordTypeIdOfElib = VTD1_Document__c.getSObjectType()
               .getDescribe()
               .getRecordTypeInfosByName()
               .get('Patient Eligibility Assessment Form')
               .getRecordTypeId();
       VTD1_Document__c document = (VTD1_Document__c) new DomainObjects.VTD1_Document_t()
               .setRecordTypeId(docRecordTypeIdOfElib)
               .persist();
       ContentVersion contentVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
               .setPathOnClient('test.pdf')
               .setTitle('Test')
               .setVersionData(Blob.valueOf('TestData'))
               .persist();
       ContentVersion insertedContVersion = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
       new DomainObjects.ContentDocumentLink_t()
               .withLinkedEntityId(document.Id)
               .withContentDocumentId(insertedContVersion.ContentDocumentId)
               .persist();
       Test.stopTest();
   }

    @IsTest
    private static void saveTheChunk_WithContentDocumentLink_Test() {
        VTD1_Document__c doc = [SELECT Id FROM VTD1_Document__c LIMIT 1];
        String base64Data = 'RG9jdW1lbnQ=';
        Test.startTest();
            Id fileId = VTR2_SCRFileUploadController.saveTheChunk(doc.Id, null, 'test file', base64Data, '');
            System.assertNotEquals(null, fileId);
            Id fileId2 = VTR2_SCRFileUploadController.saveTheChunk(doc.Id, null, 'test file', base64Data, String.valueOf(fileId));
            System.assertNotEquals(null, fileId2);
        Test.stopTest();
    }

    @IsTest
    private static void saveTheChunk_WithoutContentDocumentLink_Test() {
        VTD1_Document__c doc = [SELECT Id FROM VTD1_Document__c LIMIT 1];
        List<ContentDocumentLink> cdlList = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: doc.Id];

        delete cdlList;

        String base64Data = 'RG9jdW1lbnQ=';

        Test.startTest();
            Id fileId = VTR2_SCRFileUploadController.saveTheChunk(doc.Id, null, 'test file', base64Data, '');
            System.assertNotEquals(null, fileId);
            Id fileId2 = VTR2_SCRFileUploadController.saveTheChunk(doc.Id, null, 'test file', base64Data, String.valueOf(fileId));
            System.assertNotEquals(null, fileId2);
        Test.stopTest();
    }

    @IsTest
    static void getDocumentTest() {
        VTD1_Document__c doc = [SELECT Id FROM VTD1_Document__c LIMIT 1];
        ContentDocumentLink link = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :doc.Id LIMIT 1];

        Test.startTest();
            VTR2_SCRFileUploadController.DocumentData documentData = new VTR2_SCRFileUploadController.DocumentData(doc.Id);
        Test.stopTest();

        System.assertEquals(doc.Id, documentData.document.Id, 'Incorrect document Id');
        System.assertEquals(link.ContentDocumentId, documentData.contentDocumentId, 'Incorrect content document Id');
    }

    @IsTest
    static void getDocumentByIdTest() {
        VTD1_Document__c doc = [SELECT Id FROM VTD1_Document__c LIMIT 1];
        ContentDocumentLink link = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :doc.Id LIMIT 1];

        Test.startTest();
            VTR2_SCRFileUploadController.DocumentData documentData = VTR2_SCRFileUploadController.getDocumentById(doc.Id);
        Test.stopTest();

        System.assertEquals(doc.Id, documentData.document.Id, 'Incorrect document Id');
        System.assertEquals(link.ContentDocumentId, documentData.contentDocumentId, 'Incorrect content document Id');
    }
    @IsTest
    private static void standardUploadertest() {
        VTD1_Document__c doc = [SELECT Id FROM VTD1_Document__c LIMIT 1];
        List<ContentDocumentLink> cdlList = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: doc.Id];



        delete cdlList;
        ContentVersion firstVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();
        ContentVersion nextVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();


        Id uploadedFirstDocId = [Select ContentDocumentId from ContentVersion where Id =: firstVersion.Id].ContentDocumentId;
        Id uploadedNextDocId = [Select ContentDocumentId from ContentVersion where Id =: nextVersion.Id].ContentDocumentId;
        Test.startTest();
            VTR2_SCRFileUploadController.convertToNewVersion(doc.Id, uploadedFirstDocId);
            VTR2_SCRFileUploadController.convertToNewVersion(doc.Id, uploadedNextDocId);

        Test.stopTest();
        ContentDocumentLink contentDocumentLink = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :doc.Id LIMIT 1];

        System.assertEquals(doc.Id, contentDocumentLink.LinkedEntityId, 'Incorrect LinkedEntityId');
    }
}