@IsTest
private with sharing class VT_R5_AllTests3_Part2 {

    @TestSetup
    static void setup() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();

        VT_D1_TestUtils.prepareStudy(1);

        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }
    @IsTest
    static void VT_D1_TestDocuSignStatus1() {
        VT_D1_TestDocuSignStatus.testStatusComplete();
    }
    @IsTest
    static void VT_D1_TestDocuSignStatus2() {
        VT_D1_TestDocuSignStatus.test();
    }
    @IsTest
    static void VT_D2_TranslateParametersTest() {
        VT_D2_TranslateParametersTest.testBehavior();
    }
    @IsTest
    static void VT_D2_CasePGReassignTest1() {
        VT_D2_CasePGReassignTest.getCaseSuccessTest();
    }
    @IsTest
    static void VT_D2_CasePGReassignTest2() {
        VT_D2_CasePGReassignTest.getCaseFailTest();
    }
    @IsTest
    static void VT_D2_CasePGReassignTest3() {
        VT_D2_CasePGReassignTest.updateCaseSuccessTest();
    }
    @IsTest
    static void VT_D2_CasePGReassignTest4() {
        VT_D2_CasePGReassignTest.updateCaseFailTest();
    }
    @IsTest
    static void VT_D1_BackupPgNotificationGenerator_Test() {
        VT_D1_BackupPgNotificationGenerator_Test.firstTest();
    }
    @IsTest
    static void VTD1_LiveChatTranscriptTriggerTest() {
        VTD1_LiveChatTranscriptTriggerTest.doTest();
    }
    @IsTest
    static void VT_R5_SiteMemberCalculatorTest() {
        VT_R5_SiteMemberCalculatorTest.doTest();
    }
    @IsTest
    static void VT_D1_PIElectronicDataCaptureTest() {
        VT_D1_PIElectronicDataCaptureTest.getEDCTest();
    }
    @IsTest
    static void LightningLoginFormControllerTest1() {
        LightningLoginFormControllerTest.testLoginWithInvalidUrl();
    }
    @IsTest
    static void LightningLoginFormControllerTest2() {
        LightningLoginFormControllerTest.testLoginWithInvalidCredential();
    }
    @IsTest
    static void LightningLoginFormControllerTest3() {
        LightningLoginFormControllerTest.testLoginWithValidCredential();
    }
    @IsTest
    static void LightningLoginFormControllerTest4() {
        LightningLoginFormControllerTest.LightningLoginFormControllerInstantiation();
    }
    @IsTest
    static void LightningLoginFormControllerTest5() {
        LightningLoginFormControllerTest.testIsUsernamePasswordEnabled();
    }
    @IsTest
    static void LightningLoginFormControllerTest6() {
        LightningLoginFormControllerTest.testAuthConfig();
    }
    @IsTest
    static void LightningLoginFormControllerTest7() {
        LightningLoginFormControllerTest.testSetExperienceId();
    }
    @IsTest
    static void LightningLoginFormControllerTest8() {
        LightningLoginFormControllerTest.testForgotPassword();
    }
    @IsTest
    static void VT_D1_ActionItemsControllerTest() {
        VT_D1_ActionItemsControllerTest.getActionItemsTest();
    }
    @IsTest
    static void VT_R2_Test_SCRPatientKitDetail1() {
        VT_R2_Test_SCRPatientKitDetail.test();
    }
    @IsTest
    static void VT_R2_Test_SCRPatientKitDetail2() {
        VT_R2_Test_SCRPatientKitDetail.exceptionTest();
    }
    @IsTest
    static void VT_D1_ContactTriggerHandlerTest1() {
        VT_D1_ContactTriggerHandlerTest.doTest();
    }
    @IsTest
    static void VT_D1_ContactTriggerHandlerTest2() {
        VT_D1_ContactTriggerHandlerTest.testLanguage();
    }
    @IsTest
    static void VT_D1_MonitoringVisitHandlerTest() {
        VT_D1_MonitoringVisitHandlerTest.testBehavior();
    }
    @IsTest
    static void VT_D1_PIMyPatientsControllerTest1() {
        VT_D1_PIMyPatientsControllerTest.getMyPatientsTest_PI();
    }
    @IsTest
    static void VT_D1_PIMyPatientsControllerTest2() {
        VT_D1_PIMyPatientsControllerTest.getMyPatientsTest_SCR();
    }
    @IsTest
    static void VT_D2_BatchForUpdating_STM_User_IdTest() {
        VT_D2_BatchForUpdating_STM_User_IdTest.testBatchBehavior();
    }
    @IsTest
    static void VT_D2_IUStudyTeamControllerTest() {
        VT_D2_IUStudyTeamControllerTest.getPatientStudyTeamTest();
    }
    @IsTest
    static void VT_D2_TaskHandlerTest() {
        VT_D2_TaskHandlerTest.cancelledPatientsTasksTest();
    }
    @IsTest
    static void VT_D2_TaskProcessesAndBatch() {
        VT_D2_TaskHandlerTest.testProcesses();
        VT_D2_TaskHandlerTest.testBatch();
    }
    @IsTest
    static void AccountPhonesControllerTest() {
        AccountPhonesControllerTest.getAccPhonesListTest();
    }
    @IsTest
    static void VT_D1_ActionItemServicesTest1() {
        VT_D1_ActionItemServicesTest.testBehavior();
    }
    @IsTest
    static void VT_D1_ActionItemServicesTest2() {
        VT_D1_ActionItemServicesTest.getDateFromStringTest();
    }
    @IsTest
    static void VT_D1_ActionItemServicesTest3() {
        VT_D1_ActionItemServicesTest.createActionItemTest();
    }
    @IsTest
    static void VT_D1_ActionItemServicesTest4() {
        VT_D1_ActionItemServicesTest.getVSiteByProtocolSiteIdTest();
    }
    @IsTest
    static void VT_D1_ActionItemServicesTest5() {
        VT_D1_ActionItemServicesTest.retrieveListOfActionItemTest();
    }
    @IsTest
    static void VT_D1_CaseVisitsListController_Test() {
        VT_D1_CaseVisitsListController_Test.testBehavior();
    }
    @IsTest
    static void VT_D1_DocumentCHandlerTest1() {
        VT_D1_DocumentCHandlerTest.TestDocs();
    }
    @IsTest
    static void VT_D1_DocumentCHandlerTest2() {
        VT_D1_DocumentCHandlerTest.doTestMedRecApproval();
    }
    @IsTest
    static void VT_D1_DocumentCHandlerTest3() {
        VT_D1_DocumentCHandlerTest.doCheckAppliedPatients();
    }
    @IsTest
    static void VT_D1_DocumentCHandlerTest4() {
        VT_D1_DocumentCHandlerTest.testDeleteContentDocument();
    }
    @IsTest
    static void VT_D1_DocumentCHandlerTest5() {
        VT_D1_DocumentCHandlerTest.testValidateCraReviewCompletedCheckbox();
    }
    @IsTest
    static void VT_D1_LoginFlowController_Test1() {
        VT_D1_LoginFlowController_Test.withVerificationTest();
    }
    @IsTest
    static void VT_D1_LoginFlowController_Test2() {
        VT_D1_LoginFlowController_Test.withoutVerificationTest();
    }
}