/**
* @author: Carl Judge
* @date: 15-Aug-19
* @description: Abstract for global sharing logic. Any class extending this must be able to take a list of record IDs
*               and return a list of VT_R3_GlobalSharing.ShareDetail, which should be templates of every share that should exist for these records
**/

public abstract without sharing class VT_R3_AbstractGlobalSharingLogic extends VT_R3_AbstractChainableQueueable {
    public enum SharingMode {RECORD, USER_ADD, USER_REMOVE}
    public static final VTR5_SharingSettings__mdt SHARING_SETTINGS;
    public static final List<String> RETRY_ERRORS = new List<String> {
        'duplicate value found: <unknown>'
    };
    public static final String RETRY_SEMAPHORE_NAME = 'ShareRetry';
    private static Map<String, VTR3_GlobalSharingConfig__mdt> sharingConfigurationMap;
    static {
        SHARING_SETTINGS = Database.query(
            'SELECT '
                + String.join(VT_D1_HelperClass.getAllFields(VTR5_SharingSettings__mdt.getSObjectType()), ',')
                + ' FROM VTR5_SharingSettings__mdt LIMIT 1'
        );
        sharingConfigurationMap = new Map<String, VTR3_GlobalSharingConfig__mdt>();
        for (VTR3_GlobalSharingConfig__mdt configMdt : [
            SELECT VTR3_CustomApexClassName__c, VTR3_ObjectType__r.QualifiedAPIName, VTR3_PathToCase__c,
                VTR3_PathToStudy__c, VTR3_SharingReason__c, VTR3_IsUserType__c
            FROM VTR3_GlobalSharingConfig__mdt
        ]) {
            String objectName = configMdt.VTR3_IsUserType__c ? 'User' : configMdt.VTR3_ObjectType__r.QualifiedAPIName;
            sharingConfigurationMap.put(objectName, configMdt);
        }
    }

    protected Set<Id> recordIds;
    protected Set<Id> includedUserIds;
    protected Boolean removeOnly;
    protected Set<Id> scopeRecordIds;
    protected SharingMode shareMode;
    protected String scopeType;
    protected Set<Id> scopedCaseIds;
    protected Set<Id> scopedSiteIds;
    protected Set<Id> scopedStudyIds;
    protected Map<String, Set<Id>> profileToUserIds = new Map<String, Set<Id>>();
    protected Map<Id, Set<Id>> siteToUserIds = new Map<Id, Set<Id>>();
    // Conquerors
    // Abhay
    // Jira Ref: SH-17438 
    protected Map<Id, Set<Id>> siteToCraUserIds = new Map<Id, Set<Id>>();
    // End of code: Conquerors 
    
    protected Set<Id> userSiteIds = new Set<Id>();
    protected Map<Id, Set<Id>> studyToStmUserIds = new Map<Id, Set<Id>>();
    protected Set<Id> studyFromStmIds = new Set<Id>();
    protected Set<Id> patientIds = new Set<Id>();
    protected Set<Id> pgIds = new Set<Id>();
    protected Set<Id> piIds = new Set<Id>();
    protected Set<Id> scrIds = new Set<Id>();
    protected Set<Id> vthoIds = new Set<Id>();
    protected Set<Id> craIds = new Set<Id>();
    protected Set<Id> projLeadIds = new Set<Id>();
    protected Set<Id> vtslIds = new Set<Id>();
    protected Set<Id> pmaIds = new Set<Id>();
    protected Set<Id> mrrIds = new Set<Id>();
    protected Map<String, ShareDetail> shareDetails = new Map<String, ShareDetail>();
    protected Boolean userPrepDone = false;
    protected Boolean forceBatch = false;

    public static VTR3_GlobalSharingConfig__mdt getConfigForObjectType(String objType) {
        return sharingConfigurationMap.get(objType);
    }

    public static VT_R3_AbstractGlobalSharingLogic getSharingLogicForObjectType(String objType) {
        VTR3_GlobalSharingConfig__mdt config = getConfigForObjectType(objType);
        VT_R3_AbstractGlobalSharingLogic sharingLogic;
        if (config != null && config.VTR3_CustomApexClassName__c != null) {
            Type classType = Type.forName(config.VTR3_CustomApexClassName__c);
            if (classType != null) {
                sharingLogic = (VT_R3_AbstractGlobalSharingLogic) classType.newInstance();
            }
        }
        return sharingLogic;
    }

    public override void enqueue() {
        if (VT_R3_GlobalSharing.doSynchronous) {
            this.execute(null);
        } else {
            super.enqueue();
        }
    }

    public void init(Set<Id> recordIds, Set<Id> includedUserIds, Set<Id> scopeRecordIds, Boolean removeOnly) {
        reset();
        this.recordIds = recordIds;
        this.includedUserIds = includedUserIds;
        this.scopeRecordIds = scopeRecordIds;
        this.removeOnly = removeOnly;
        setMode();
    }

    protected virtual void reset() {
        scopeType = null;
        scopedCaseIds = null;
        scopedSiteIds = null;
        scopedStudyIds = null;
        profileToUserIds.clear();
        siteToUserIds.clear();
        
        // Conquerors
        // Abhay
        // Jira Ref: SH-17438 
        // Description: clearing the collection as reset
        siteToCraUserIds.clear();
        // End of code: Conquerors 

        userSiteIds.clear();
        studyToStmUserIds.clear();
        studyFromStmIds.clear();
        patientIds.clear();
        pgIds.clear();
        piIds.clear();
        scrIds.clear();
        vthoIds.clear();
        craIds.clear();
        projLeadIds.clear();
        vtslIds.clear();
        pmaIds.clear();
        mrrIds.clear();
        shareDetails.clear();
        userPrepDone = false;
        forceBatch = false;
    }

    public virtual override void actionsBeforeSerialize() {
        super.actionsBeforeSerialize();
        reset();
    }

    private void setMode() {
        if (recordIds != null && !recordIds.isEmpty()) {
            shareMode = SharingMode.RECORD;
        } else if (includedUserIds != null && !includedUserIds.isEmpty()) {
            if (!removeOnly) {
                shareMode = SharingMode.USER_ADD;
            }
            else {
                if (scopeRecordIds == null || scopeRecordIds.size() != 1) {
                    throw new GlobalSharingException('Exactly one scope ID required for Remove mode.');
                }
                shareMode = SharingMode.USER_REMOVE;
            }
        }
    }

    public virtual override void executeLogic() {
        if (shareMode != null && (!Test.isRunningTest() || !VT_R3_GlobalSharing.disableForTest)) {
            System.debug('Executing sharing: ' + getType() + ' in ' + shareMode + ' mode');
            switch on shareMode {
                when RECORD {
                    executeRecordBased();
                }
                when USER_ADD {
                    executeUserBased();
                }
                when USER_REMOVE {
                    executeUserBasedRemove();
                }
            }
        }
    }

    private void executeRecordBased() {
        // switch to batch if we have too many record IDs to process. If there are no user IDs specified, this number is conservative because it could be any number of site staff
        // if we have user IDs, we can know the max number of shares we might need to create and can use that for max records instead
        Integer maxRecords = includedUserIds == null || includedUserIds.isEmpty()
            ? getMaxRecords()
            : getAdjustedQueryLimit();

        if (recordIds.size() > maxRecords && !System.isBatch()) {
            doBatch(SHARING_SETTINGS.VTR5_RecordBatchChunkSize__c.intValue());
        } else {
            addShareDetailsForRecords();
            if (forceBatch && !System.isBatch()) {
                doBatch(SHARING_SETTINGS.VTR5_RecordBatchChunkSize__c.intValue());
            } else {
                processRecordShareDetails();
            }
        }
    }

    protected abstract void addShareDetailsForRecords();

    private void processRecordShareDetails() {
        if (includedUserIds != null && !includedUserIds.isEmpty()) {
            filterByIncludedUsers();
        }
        if (!shareDetails.isEmpty()) {
            filterByStudySharing();
        }
        //  we might have no share details, but if there are recordIds + userIds, then we need to delete some shares so continue processing
        if (!shareDetails.isEmpty() || (!recordIds.isEmpty() && includedUserIds != null && !includedUserIds.isEmpty())) {
            // if we have too many shares to create, we can get DML error. In this case we can create the shares in batches
            if (shareDetails.values().size() > getMaxDetails() && !System.isBatch()) {
                doBatch(SHARING_SETTINGS.VTR5_RecordBatchChunkSize__c.intValue());
            } else {
                recalcRecordShares();
            }
        }
    }

    private void executeUserBased() {
        prepareUserBased();
        List<SObject> queryResult;
        try {
            queryResult = executeUserQuery();
        } catch (Exception e) {
            System.debug(getObjectName() + 'user sharing query failed: ' + e.getMessage());
        }
        if (queryResult != null) {
            System.debug('Processing ' + queryResult.size() + ' ' + getObjectName() + ' records for sharing');
            Integer adjustedQueryLimit = getAdjustedQueryLimit();
            if (queryResult.size() >= adjustedQueryLimit) {
                doBatch(adjustedQueryLimit);
            } else {
                processUserQueryResult(queryResult);
            }
        }
    }

    private void prepareUserBased() {
        setScopeType();
        processScopeIds();
        doUserQueryPrep();
        userPrepDone = true;
    }

    // optional method for pre-query steps
    protected virtual void doUserQueryPrep(){}

    // should return a list of records to be processed into shares. **Be sure to limit by QUERY_LIMIT**
    protected abstract List<SObject> executeUserQuery();

    protected virtual void processUserQueryResult(List<SObject> records) {
        createUserShareDetailsFromRecords(records);
        recordIds = new Set<Id>();
        for (ShareDetail detail : shareDetails.values()) {
            recordIds.add(detail.recordId);
        }
        filterByStudySharing();
        createUserShares();
    }

    protected abstract void createUserShareDetailsFromRecords(List<SObject> records);
    // given a list of records, this method should create any necessary shares for these records for userIds

    private void executeUserBasedRemove() {
        prepareUserBasedRemove();
        if ((scopedStudyIds != null && !scopedStudyIds.isEmpty()) || (scopedSiteIds != null && !scopedSiteIds.isEmpty())) {
            List<SObject> queryResult;
            try {
                queryResult = executeUserBasedRemoveQuery();
            } catch (Exception e) {
                System.debug(getObjectName() + ' removal query failed: ' + e.getMessage());
            }
            if (queryResult != null) {
                System.debug('Processing ' + queryResult.size() + ' ' + getObjectName() + ' share records for removal');
                Integer adjustedQueryLimit = getAdjustedQueryLimit();
                if (queryResult.size() >= adjustedQueryLimit) {
                    doBatch(adjustedQueryLimit);
                } else {
                    processRemoveQueryResult(queryResult);
                }
            }
        }
    }

    private void prepareUserBasedRemove() {
        setScopeType();
        if (scopeType == HealthCloudGA__CarePlanTemplate__c.getSObjectType().getDescribe().getName()) {
            scopedStudyIds = scopeRecordIds;
        } else if (scopeType == Virtual_Site__c.getSObjectType().getDescribe().getName()) {
            scopedSiteIds = scopeRecordIds;
        } else {
            System.debug('No valid scope Ids found!');
        }
        userPrepDone = true;
    }

    protected abstract String getRemoveQuery();

    protected virtual void processRemoveQueryResult(List<SObject> shares) {
        deleteShares(shares);
    }

    // should return a list of shares to be deleted. **Be sure to limit by QUERY_LIMIT**
    protected virtual List<SObject> executeUserBasedRemoveQuery() {
        String removeQuery = getRemoveQuery();
        List<SObject> result;
        if (removeQuery != null) {
            removeQuery += ' LIMIT ' + getMaxDetails();
            System.debug('removeQuery ---------> '+removeQuery);
            result = (List<SObject>) new VT_Stubber.ResultStub( // stubbing this because in tests, the remove queries arent working properly. Salesfore case #27759286 open about this issue
                'VT_R3_AbstractGlobalSharingLogic.executeUserBasedRemoveQuery',
                Database.query(removeQuery)
            ).getResult();
        }
        return result;
    }

    private void recalcRecordShares() {
        Map<Id, Map<Id, SObject>> existingShareMap = getExistingShareMap();
        List<SObject> sharesToDelete = new List<SObject>();
        List<SObject> sharesToInsert = new List<SObject>();
        SObjectType shareType = Schema.getGlobalDescribe().get(getShareObjectName());
        for (ShareDetail detail : shareDetails.values()) {
            Boolean doCreate = false;
            if (existingShareMap.containsKey(detail.recordId) && existingShareMap.get(detail.recordId).containsKey(detail.userOrGroupId)) {
                // if a share exists for this record/user, check for correct accessLevel(s), otherwise create a share
                SObject existingShare = existingShareMap.get(detail.recordId).get(detail.userOrGroupId);
                for (String accessLevelField : getAccessLevelFields()) {
                    String existingAccessLevel = (String) existingShare.get(accessLevelField);
                    if (existingAccessLevel != detail.accessLevel) {
                        doCreate = true;
                        break;
                    }
                }
                if (! doCreate) {
                    // remove an existing share with correct access level from the map so it wont be deleted
                    existingShareMap.get(detail.recordId).remove(detail.userOrGroupId);
                }
            } else {
                doCreate = true;
            }
            if (doCreate) {
                sharesToInsert.add(createNewShare(detail, shareType));
            }
        }
        // anything left in existing share map is either an unneeded share or has the wrong access level, so delete them
        for (Map<Id, SObject> val : existingShareMap.values()) {
            sharesToDelete.addAll(val.values());
        }
        deleteShares(sharesToDelete);
        insertShares(sharesToInsert);
    }

    private void deleteShares(List<SObject> sharesToDelete) {
        System.debug(sharesToDelete.size());
        if (!sharesToDelete.isEmpty()) {
            List<Database.DeleteResult> results = Database.delete(sharesToDelete, false);
            processSaveResults(VT_R5_DatabaseResultWrapper.wrapResults(results), sharesToDelete, DMLOperation.DEL);
        }
    }

    private void insertShares(List<SObject> sharesToInsert) {
        System.debug(sharesToInsert.size());
        if (!sharesToInsert.isEmpty()) {
            List<Database.SaveResult> results = Database.insert(sharesToInsert, false);
            processSaveResults(VT_R5_DatabaseResultWrapper.wrapResults(results), sharesToInsert, DMLOperation.INS);
        }
    }

    private void createUserShares() {
        SObjectType shareType = Schema.getGlobalDescribe().get(getShareObjectName());
        List<SObject> sharesToInsert = new List<SObject>();
        for (ShareDetail detail : shareDetails.values()) {
            sharesToInsert.add(createNewShare(detail, shareType));
        }
        insertShares(sharesToInsert);
    }

    private Map<Id, Map<Id, SObject>> getExistingShareMap() {
        Integer soqlLimit = Limits.getLimitQueryRows() - Limits.getQueryRows();
        String rowCause = getRowCause();
        String query = String.format(
            'SELECT Id,UserOrGroupId,{0},{1} FROM {2} WHERE {3} IN :recordIds AND RowCause =:rowCause{4} LIMIT :soqlLimit',
            new String[]{
                getShareObjectParentField(),
                String.join(getAccessLevelFields(),','),
                getShareObjectName(),
                getShareObjectParentField(),
                includedUserIds != null && !includedUserIds.isEmpty() ? ' AND UserOrGroupId IN :includedUserIds' : ''
            }
        );
        System.debug(query);
        List<SObject> queryResult = Database.query(query);
        if (queryResult.size() == soqlLimit) {
            System.debug('SOQL limit reached for existing shares!');
        }
        Map<Id, Map<Id, SObject>> existingShareMap = new Map<Id, Map<Id, SObject>>(); // ParentId => UserId => Share
        for (SObject shareRec : queryResult) {
            Id parentId = (Id)shareRec.get(getShareObjectParentField());
            Id userId = (Id)shareRec.get('UserOrGroupId');
            if (! existingShareMap.containsKey(parentId)) {
                existingShareMap.put(parentId, new Map<Id, SObject>());
            }
            existingShareMap.get(parentId).put(userId, shareRec);
        }
        return existingShareMap;
    }

    private void processSaveResults(List<VT_R5_DatabaseResultWrapper> results, List<SObject> shares, DMLOperation operation) {
        List<SObject> sharesToRetry = new List<SObject>();
        for (Integer i = 0; i < results.size(); i++) {
            if (!results[i].isSuccess()) {
                System.debug('Failed ' + operation + ' on share: ' + shares[i]);
                if (results[i].getErrors() != null && !results[i].getErrors().isEmpty()) {
                    System.debug(results[i].getErrors()[0].getMessage());
                }
                if (VT_D1_AsyncDML.isRetryDML(results[i].getErrors(), RETRY_ERRORS).isRetry) {
                    sharesToRetry.add(shares[i]);
                }
            }
        }
        if (!sharesToRetry.isEmpty()) {
            VT_D1_AsyncDML asyncDML = new VT_D1_AsyncDML(sharesToRetry, operation);
            asyncDML.setAllOrNothing(false);
            asyncDML.setBatchSize(getMaxDetails() <= 5000 ? getMaxDetails() : 5000);
            asyncDML.setRetryErrors(RETRY_ERRORS);
            asyncDML.setSemaphoreName(RETRY_SEMAPHORE_NAME);
            asyncDML.setLoggingEnabled(true);
            asyncDML.setLogFullRecord(true);
            addToChain(asyncDML);
        }
    }

    // should return an iterable to be processed by batch
    public Iterable<Object> getBatchIterable() {
        switch on shareMode {
            when RECORD {
                return (Iterable<Object>) recordIds;
            }
            when USER_ADD {
                if (!userPrepDone) { prepareUserBased(); }
                return getUserBatchIterable();
            }
            when USER_REMOVE {
                if (!userPrepDone) { prepareUserBasedRemove(); }
                return getRemoveBatchIterable();
            }
        }
        return null;
    }

    protected abstract Iterable<Object> getUserBatchIterable();

    protected virtual Iterable<Object> getRemoveBatchIterable() {
        return (Iterable<Object>) Database.getQueryLocator(getRemoveQuery());
    }

    public void processBatchScope(List<Object> scope) {
        System.debug('Processing batch scope for ' + getObjectName() + ' in ' + shareMode + ' mode');
        switch on shareMode {
            when RECORD {
                recordIds = new Set<Id>((List<Id>) scope);
                reset();
                executeRecordBased();
            }
            when USER_ADD {
                processUserQueryResult((List<SObject>) scope);
            }
            when USER_REMOVE {
                processRemoveQueryResult((List<SObject>) scope);
            }
        }
    }

    private void doBatch(Integer batchSize) {
        VT_R5_BatchQueueService.enqueueBatch(new VT_R3_GlobalSharingBatch(this),batchSize);
    }

    // returns appropriate config for subclass
    protected abstract VTR3_GlobalSharingConfig__mdt getConfig();

    // maximum number of sharing details we want to process in a transaction.
    protected Integer getMaxDetails() {
        return (Integer) new VT_Stubber.ResultStub(
            'VT_R3_AbstractGlobalSharingLogic.getMaxDetails',
            SHARING_SETTINGS.VTR5_MaxTransactionShares__c.intValue()
        ).getResult();
    }

    protected virtual Integer getAdjustedQueryLimit() {
        Decimal maxDetailsDecimal = 0.0 + getMaxDetails();
        Decimal adjustedLimit = Math.ceil(maxDetailsDecimal / includedUserIds.size());
        return adjustedLimit.intValue();
    }

    protected Integer getMaxRecords() {
        return (Integer) new VT_Stubber.ResultStub(
            'VT_R3_AbstractGlobalSharingLogic.getMaxRecords',
            SHARING_SETTINGS.VTR5_MaxRecords__c.intValue()
        ).getResult();

    }

    protected void getSiteUserIdsForSites(Set<Id> siteIds) {
        if (siteIds != null) {
            VT_R5_SiteMemberCalculator memberCalc = new VT_R5_SiteMemberCalculator();
            memberCalc.initForSites(siteIds);
            siteToUserIds = memberCalc.getSiteToUsersMap();

            // Conquerors
            // Abhay
            // Jira Ref: SH-17438 
            // Description: populating the map of Site and CRA users
            siteToCraUserIds = memberCalc.getSiteToCraUsersMap();
            // End of code: Conquerors 
        }
    }

    // check the scope records to find out what the scoped object type is, and fill the case/site/study ids if appropriate
    protected virtual void processScopeIds() {
        System.debug('Scope Type: ' + scopeType);
        System.debug('Scope Record Ids: ' + scopeRecordIds);
        if (scopeType != null) {
            if (scopeType == HealthCloudGA__CarePlanTemplate__c.getSObjectType().getDescribe().getName()) {
                processStudyScope();
            } else if (scopeType == Virtual_Site__c.getSObjectType().getDescribe().getName()) {
                processSiteScope();
            } else if (scopeType == Case.getSObjectType().getDescribe().getName()) {
                processCaseScope();
            }
            // remove nulls from all scopes
            removeNulls(scopeRecordIds);
            removeNulls(scopedCaseIds);
            removeNulls(scopedSiteIds);
            removeNulls(scopedStudyIds);
        }
    }

    private void setScopeType() {
        if (scopeRecordIds != null && !scopeRecordIds.isEmpty()) {
            scopeRecordIds.remove(null);
            List<Id> scopeIds = new List<Id>(scopeRecordIds);
            if (!scopeIds.isEmpty()) {
                scopeType = scopeIds[0].getSobjectType().getDescribe().getName();
            }
        }
    }

    private void processStudyScope() {
        scopedStudyIds = scopeRecordIds;
    }

    private void processSiteScope() {
        scopedSiteIds = scopeRecordIds;
        scopedStudyIds = new Set<Id>();
        for (Virtual_Site__c site : [SELECT VTD1_Study__c FROM Virtual_Site__c WHERE Id IN :scopeRecordIds]) {
            scopedStudyIds.add(site.VTD1_Study__c);
        }
    }

    private void processCaseScope() {
        scopedCaseIds = scopeRecordIds;
        scopedSiteIds = new Set<Id>();
        scopedStudyIds = new Set<Id>();
        for (Case site : [SELECT VTD1_Study__c, VTD1_Virtual_Site__c FROM Case WHERE Id IN :scopeRecordIds]) {
            scopedStudyIds.add(site.VTD1_Study__c);
            scopedSiteIds.add(site.VTD1_Virtual_Site__c);
        }
    }

    private void removeNulls(Set<Id> idSet) {
        if (idSet != null) {
            idSet.remove(null);
        }
    }

    protected void groupUserIdsByProfile(Set<String> profilesToShareWith) {
        System.debug('includedUserIds in GS-----> '+includedUserIds); 
        for (User usr : [SELECT Id, Profile.Name FROM User WHERE Id IN :includedUserIds AND IsActive = TRUE]) {
            if (profilesToShareWith == null || profilesToShareWith.contains(usr.Profile.Name)) {
                if (!profileToUserIds.containsKey(usr.Profile.Name)) {
                    profileToUserIds.put(usr.Profile.Name, new Set<Id>());
                }
                profileToUserIds.get(usr.Profile.Name).add(usr.Id);
            }
        }
        // split to separate sets for use as bind variables in dynamic queries
        splitProfilesToSets();
    }
    protected void groupUserIdsByProfile() {
        groupUserIdsByProfile(null);
    }

    private void splitProfilesToSets() {
        patientIds = splitProfileToSet(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        pgIds = splitProfileToSet(VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME);
        piIds = splitProfileToSet(VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME);
        scrIds = splitProfileToSet(VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME);
        vthoIds = splitProfileToSet(VT_R4_ConstantsHelper_ProfilesSTM.VTHO_PROFILE_NAME);
        craIds = splitProfileToSet(VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME);
        projLeadIds = splitProfileToSet(VT_R4_ConstantsHelper_ProfilesSTM.PL_PROFILE_NAME);
        vtslIds = splitProfileToSet(VT_R4_ConstantsHelper_ProfilesSTM.VTSL_PROFILE_NAME);
        pmaIds = splitProfileToSet(VT_R4_ConstantsHelper_ProfilesSTM.PMA_PROFILE_NAME);
        mrrIds = splitProfileToSet(VT_R4_ConstantsHelper_ProfilesSTM.MRR_PROFILE_NAME);
    }

    private Set<Id> splitProfileToSet(String profileName) {
        Set<Id> idSet = new Set<Id>();
        if (profileToUserIds.containsKey(profileName)) {
            idSet = profileToUserIds.get(profileName);
        }
        return idSet;
    }

    protected void getSiteUserIdsForUsers() {
        Boolean getSites = false;
        for (String siteProfile : VT_R5_SiteMemberCalculator.SITE_PROFILES) {
            if (profileToUserIds.containsKey(siteProfile)) {
                getSites = true;
                break;
            }
        }
        if (getSites) {
            VT_R5_SiteMemberCalculator memberCalc = new VT_R5_SiteMemberCalculator();
            memberCalc.initForUsers(includedUserIds, scopedStudyIds, scopedSiteIds);
            siteToUserIds = memberCalc.getSiteToUsersMap();
            userSiteIds = siteToUserIds.keySet();

            // Conquerors
            // Abhay
            // Jira Ref: SH-17438 
            // Description: populating the map of Site and CRA users
            siteToCraUserIds = memberCalc.getSiteToCraUsersMap();
            // End of code: Conquerors 
        }
    }

    protected void getStmUserIdByStudy(Set<Id> userIds) {
        String query = 'SELECT Study__c, User__c FROM Study_Team_Member__c WHERE User__c IN :userIds';
        if (scopedStudyIds != null) {
            query += ' AND Study__c IN :scopedStudyIds';
        }
        for (Study_Team_Member__c stm : Database.query(query)) {
            if (!studyToStmUserIds.containsKey(stm.Study__c)) {
                studyToStmUserIds.put(stm.Study__c, new Set<Id>());
            }
            studyToStmUserIds.get(stm.Study__c).add(stm.User__c);
        }
        studyFromStmIds = studyToStmUserIds.keySet();
    }

    protected void filterByIncludedUsers() {
        Map<String, ShareDetail> filteredDetails = new Map<String, ShareDetail>();
        for (String key : shareDetails.keySet()) {
            ShareDetail detail = shareDetails.get(key);
            if (includedUserIds.contains(detail.userOrGroupId)) {
                filteredDetails.put(key, detail);
            }
        }
        shareDetails = filteredDetails;
    }

    /**
     * check users have correct study access, remove/correct any shares for people who don't
     */
    protected void filterByStudySharing() {
        Map<Id, Map<Id, VTD1_Study_Sharing_Configuration__c>> studySharingMap = getStudySharingMap();
        Map<String, ShareDetail> filteredDetails = new Map<String, ShareDetail>();
        for (String key : shareDetails.keySet()) {
            ShareDetail detail = shareDetails.get(key);
            // group shares or anything without a study is exempt from this filtering.
            if (isGroupShare(detail) || detail.studyId == null) {
                filteredDetails.put(key, detail);
            } else if (studySharingMap.containsKey(detail.studyId) && studySharingMap.get(detail.studyId).containsKey(detail.userOrGroupId)) {
                VTD1_Study_Sharing_Configuration__c config = studySharingMap.get(detail.studyId).get(detail.userOrGroupId);
                if (config.VTD1_Access_Level__c == 'Read' || config.VTD1_Access_Level__c == 'Edit') {
                    if (config.VTD1_Access_Level__c == 'Read' && detail.accessLevel == 'Edit') {
                        detail.accessLevel = 'Read';
                    }
                    filteredDetails.put(key, detail);
                }
            }
        }
        shareDetails = filteredDetails;
    }

    // studyId => userId => sharing config
    private Map<Id, Map<Id, VTD1_Study_Sharing_Configuration__c>> getStudySharingMap() {
        Map<Id, Map<Id, VTD1_Study_Sharing_Configuration__c>> studySharingMap = new Map<Id, Map<Id, VTD1_Study_Sharing_Configuration__c>>();
        Set<Id> studyIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        for (ShareDetail detail : shareDetails.values()) {
            if (!isGroupShare(detail)) {
                studyIds.add(detail.studyId);
                userIds.add(detail.userOrGroupId);
            }
        }
        if (!studyIds.isEmpty()) {
            for (VTD1_Study_Sharing_Configuration__c config : [
                SELECT VTD1_Access_Level__c, VTD1_Study__c, VTD1_User__c
                FROM VTD1_Study_Sharing_Configuration__c
                WHERE VTD1_Study__c IN :studyIds
                AND VTD1_User__c IN :userIds
            ]) {
                if (! studySharingMap.containsKey(config.VTD1_Study__c)) {
                    studySharingMap.put(config.VTD1_Study__c, new Map<Id, VTD1_Study_Sharing_Configuration__c>());
                }
                studySharingMap.get(config.VTD1_Study__c).put(config.VTD1_User__c, config);
            }
        }
        return studySharingMap;
    }

    private SObject createNewShare(ShareDetail detail, SObjectType shareType) {
        SObject newShare = shareType.newSObject();
        newShare.put(getShareObjectParentField(), detail.recordId);
        newShare.put('UserOrGroupId', detail.userOrGroupId);
        for (String accessLevelField : getAccessLevelFields()) {
            newShare.put(accessLevelField, detail.accessLevel);
        }
        newShare.put('RowCause', getRowCause());
        return newShare;
    }

    protected void addShareDetail(Id recId, Id userOrGroupId, Id studyId, String accessLevel) {
        String key = '' + recId + userOrGroupId;
        if (recId != null && userOrGroupId != null && !shareDetails.containsKey(key)) {
            VT_R3_AbstractGlobalSharingLogic.ShareDetail detail = new VT_R3_AbstractGlobalSharingLogic.ShareDetail();
            detail.recordId = recId;
            detail.userOrGroupId = userOrGroupId;
            detail.studyId = studyId;
            detail.accessLevel = accessLevel;
            shareDetails.put(key, detail);
        }
    }

    private String getRowCause() {
        return getConfig().VTR3_SharingReason__c;
    }

    private String getObjectName() {
        VTR3_GlobalSharingConfig__mdt config = getConfig();
        return config.VTR3_IsUserType__c ? 'User' : config.VTR3_ObjectType__r.QualifiedAPIName;
    }

    private String getShareObjectName() {
        String objName = getObjectName();
        return isCustomObject(objName) ? objName.replace('__c', '__Share') : objName + 'Share';
    }

    private String getShareObjectParentField() {
        String objName = getObjectName();
        return (isCustomObject(objName) ? 'Parent' : objName) + 'Id';
    }

    private String getAccessLevelField(String objName) {
        return isCustomObject(objName) ? 'AccessLevel' : objName + 'AccessLevel';
    }
    private List<String> getAccessLevelFields() {
        String objName = getObjectName();
        List<String> fields;
        if (objName != 'Account') {
            fields = new List<String>{getAccessLevelField(objName)};
        } else {
            fields = new List<String> {
                'AccountAccessLevel',
                'OpportunityAccessLevel',
                'ContactAccessLevel'
            };
        }
        return fields;
    }

    private Boolean isGroupShare(ShareDetail detail) {
        return detail.userOrGroupId.getSobjectType() == Group.getSObjectType();
    }

    private Boolean isCustomObject(String objName) {
        return objName.contains('__c');
    }

    public class ShareDetail {
        public Id recordId;
        public Id userOrGroupId;
        public Id studyId;
        public String accessLevel;
    }

    public class GlobalSharingException extends Exception{}
}