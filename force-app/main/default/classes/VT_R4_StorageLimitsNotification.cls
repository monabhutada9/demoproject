/**
 * Created by Dmitry Gavrilov on 30.03.2020.
 */

global class VT_R4_StorageLimitsNotification implements Schedulable {
    global void execute(SchedulableContext SC) {
        Notifications();
    }

    @Future (Callout=true)
    static void Notifications(){

        List<String> recipientsMail = new List<String>();
        for (Storage_Limits_Notification_Email__mdt item : [SELECT Email__c FROM Storage_Limits_Notification_Email__mdt WHERE Email__c <> NULL] )
        {
            recipientsMail.add(item.Email__c);
        }
        //String[] recipientsMail = 	new String[] {'dmitry.gavrilov@customertimes.com','mark.kosobutskiy@customertimes.com','roman.artyushkov@customertimes.com'};
        //String[] recipientsMail = 	new String[] {'dmitry.gavrilov@customertimes.com'};
        String[] adminsMail 	=	recipientsMail; //new String[] {'dmitry.gavrilov@customertimes.com'}; //Sent when error occurs

        Integer limitDataStorageNotification = 90;
        Integer limitFileStorageNotification = 90;

        String requestUrl = '/setup/org/orgstorageusage.jsp?id=' + UserInfo.getOrganizationId() + '&setupid=CompanyResourceDisk';

        PageReference pg = new PageReference( requestUrl );
                String htmlCode;
        if(Test.isRunningTest()){
            htmlCode = '95%</td></tr>  0%</td></tr>';
        }
        else {
            htmlCode = pg.getContent().toString();
        }
        Pattern patternToSearch = Pattern.compile('\\d+%</td></tr>');
        Matcher matcherPattern = patternToSearch.matcher(htmlCode);

        String  dataStorageString, fileStorageString, dataStorageUsedPercentage, fileStorageUsedPercentage;

        if ( matcherPattern.find() ) {

            dataStorageString = htmlCode.substring(matcherPattern.start(), matcherPattern.end());
            System.debug('**************************** dataStorageString: ' + dataStorageString);

            Pattern subpatternToSearch = Pattern.compile('\\d+');
            Matcher matcherPatternPercentage = subpatternToSearch.matcher(dataStorageString);

            if ( matcherPatternPercentage.find() ) {

                dataStorageUsedPercentage = dataStorageString.substring(matcherPatternPercentage.start(), matcherPatternPercentage.end());

            }else{
                dataStorageString =  null;
            }
        }

        if ( matcherPattern.find() ) {

            fileStorageString = htmlCode.substring(matcherPattern.start(), matcherPattern.end());

            Pattern subpatternToSearch = Pattern.compile('\\d+');
            Matcher matcherPatternPercentage = subpatternToSearch.matcher(fileStorageString);

            if ( matcherPatternPercentage.find() ) {
                fileStorageUsedPercentage = fileStorageString.substring(matcherPatternPercentage.start(), matcherPatternPercentage.end());
            }else{
                fileStorageString = null;
            }
        }

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String bodyMessage = '';
        Boolean sendMessage = false;
        Boolean dataExceed = limitDataStorageNotification <= Integer.valueOf(dataStorageUsedPercentage);
        Boolean fileExceed = limitFileStorageNotification <= Integer.valueOf(fileStorageUsedPercentage);



        if ( (!String.isBlank(dataStorageUsedPercentage)) && (!String.isBlank(fileStorageUsedPercentage) ) ){

            if (  dataExceed  || fileExceed )
            {

                sendMessage = true;
                mail.setSubject('Storage limits notification for '
                        //+ UserInfo.getOrganizationName() + ', '
                        + system.Url.getSalesforceBaseUrl().getHost().remove('.my.salesforce.com') + ' sandbox'
                        //+ ', ID: '+ UserInfo.getOrganizationId()
                );

            }

            if ( sendMessage ){
                //Send Mail Notifications
                mail.setToAddresses(recipientsMail);
                mail.setOrgWideEmailAddressId([SELECT Id FROM OrgWideEmailAddress ORDER BY CreatedDate LIMIT 1][0].Id);
                if (dataExceed) {
                    bodyMessage += '<p></p><p><b>Please clean the Data storage.</b></p>';
                }
                if (fileExceed) {
                    bodyMessage += '<p></p><p><b>Please clean the File storage.</b></p>';
                }

                bodyMessage += 'Storage notification for '+ system.Url.getSalesforceBaseUrl().getHost().remove('.my.salesforce.com') +' sandbox has been generated because one of the following values has exceeded the limit:';
                bodyMessage += '<ul><li>Storage Data:'+ (dataExceed? ' <b>' : ' ') +  dataStorageUsedPercentage + (dataExceed ? '% - EXCEED!</b>' : '%');
                bodyMessage += '<li>Storage Files:'+ (fileExceed? ' <b>' : ' ') + fileStorageUsedPercentage + (fileExceed ? '% - EXCEED!</b>' : '%')+'</ul>';
                bodyMessage += 'Established limits are:';
                bodyMessage += '<ul><li>For notification Data Storage : ' + limitDataStorageNotification + '%';
                bodyMessage += '<li>For notification File Storage: '  + limitFileStorageNotification + '%';

                bodyMessage += '<p></p><p><a href="' + System.Url.getSalesforceBaseUrl().toExternalForm() + requestUrl + '" target="_blank">Storage Usage page</a></p>';
                mail.setHtmlBody(bodyMessage);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }

        }else{
            System.debug('Pattern finding failed miserayble - Esteve\'s Fault');

            mail.setToAddresses(adminsMail);
            mail.setSubject('Error Generation for Storage notification in: ' + UserInfo.getOrganizationName() + ' - ID: '+ UserInfo.getOrganizationId());

            bodyMessage  = 'Storage notification generation failed, values are dataStorageUsedPercentage:' +  dataStorageUsedPercentage + ' fileStorageUsedPercentage:' + fileStorageUsedPercentage;

            mail.setHtmlBody(bodyMessage);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }

}