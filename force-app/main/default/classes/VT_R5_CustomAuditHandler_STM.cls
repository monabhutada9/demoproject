/**


 * Created by Eugene Pavlovskiy on 11-Sep-20.
 */

public without sharing class VT_R5_CustomAuditHandler_STM {
    // TRIGGER HANDLERS

    public static void onAfterInsertSTM(Map<Id, Study_Team_Member__c> triggerNewValues) {

        System.debug('ua: onAfterInsertSTM start ' + Limits.getQueries());

        List<VTR5_SiteStudyAccessAudit__c> auditsToInsert = new List<VTR5_SiteStudyAccessAudit__c>();

        // get existing User Audits for users or create new
        Map<Id, VTR5_UserAudit__c> userAuditByUser = VT_R5_CustomAuditHandler_Misc.getOrCreateUserAudits(triggerNewValues.values());

        List<Study_Team_Member__c> stmsPiWithCra = new List<Study_Team_Member__c>();
        List<Study_Team_Member__c> stmsBackupPiWithoutSite = new List<Study_Team_Member__c>();


        for (Study_Team_Member__c stm : triggerNewValues.values()) {
            VTR5_UserAudit__c userAudit = userAuditByUser.get(stm.User__c);


            if (stm.VTD1_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) {    // PI
                if (stm.Study_Team_Member__c != null && stm.VTD1_VirtualSite__c == null) {  // backup PI without site
                    stmsBackupPiWithoutSite.add(stm);

                } else {    // all other PIs
                    auditsToInsert.add(createAccessAudit(stm, userAudit.Id, Datetime.now(), stm.VTD1_VirtualSite__c, Datetime.now()));

					//START RAJESH SH-17440 :CRA Fields are removed from the STM.
                    //if (stm.VTD1_VirtualSite__c != null && (stm.VTR2_Remote_CRA__c != null || stm.VTR2_Onsite_CRA__c != null)) {  // PI with site and CRA
					//
                    //    stmsPiWithCra.add(stm);
                    //}
					//END:SH-17440
                }
            } else {    // non-PI

                auditsToInsert.add(createAccessAudit(stm, userAudit.Id, Datetime.now(), null, null));

            }
        }

        if (!stmsBackupPiWithoutSite.isEmpty()) {
            auditsToInsert.addAll(processInsertStmBackupPi(stmsBackupPiWithoutSite));
        }
		
		//START RAJESH SH-17440
        // if (!stmsPiWithCra.isEmpty()) {

        //    auditsToInsert.addAll(processInsertStmRelatedCras(stmsPiWithCra));
        //}
		//END SH-17440
        upsert auditsToInsert;

        System.debug('ua: onAfterInsertSTM finish ' + Limits.getQueries());
    }

    public static void onAfterUpdateSTM(Map<Id, Study_Team_Member__c> triggerNewList, Map<Id, Study_Team_Member__c> triggerOldMap) {
        System.debug('ua: onAfterUpdateSTM start ' + Limits.getQueries());


        List<VTR5_SiteStudyAccessAudit__c> auditsForUpsert = new List<VTR5_SiteStudyAccessAudit__c>();

        Map<Id, Study_Team_Member__c> stmsSiteAdded = new Map<Id, Study_Team_Member__c>();
        Map<Id, Study_Team_Member__c> stmsSiteRemoved = new Map<Id, Study_Team_Member__c>();

        Map<Id, Study_Team_Member__c> stmsPrimaryPiRemoved = new Map<Id, Study_Team_Member__c>();
        Map<Id, Study_Team_Member__c> stmsPrimaryPiAdded = new Map<Id, Study_Team_Member__c>();

        Map<Id, Study_Team_Member__c> stmsCraRemoved = new Map<Id, Study_Team_Member__c>();
        Map<Id, Study_Team_Member__c> stmsCraAdded = new Map<Id, Study_Team_Member__c>();

        for (Study_Team_Member__c newStm : triggerNewList.values()) {
            // PROCESS AUDITS ONLY ON PI UPDATE
            if (newStm.VTD1_Type__c != VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) continue;
            Study_Team_Member__c oldStm = triggerOldMap.get(newStm.Id);

            // ON CHANGE OF VIRTUAL SITE FIELD
            if (newStm.VTD1_VirtualSite__c != oldStm.VTD1_VirtualSite__c) {
                if (oldStm.VTD1_VirtualSite__c != null) {
                    stmsSiteRemoved.put(newStm.Id, oldStm);
                }
                if (newStm.VTD1_VirtualSite__c != null) {
                    stmsSiteAdded.put(newStm.Id, newStm);
                }
            }

            // PROCESS RELATED STMS ONLY FOR PIs WITH SITES
            if (newStm.VTD1_VirtualSite__c == null) continue;

            // ON CHANGE OF PRIMARY PI FIELD
            if (newStm.Study_Team_Member__c != oldStm.Study_Team_Member__c) {
                if (oldStm.Study_Team_Member__c != null) {
                    stmsPrimaryPiRemoved.put(newStm.Id, oldStm);
                }
                if (newStm.Study_Team_Member__c != null) {
                    stmsPrimaryPiAdded.put(newStm.Id, newStm);
                }
            }

            // ON CHANGE OF CRA FIELDS
			//START RAJESH SH-17440: CRA fields are removed from STM.
			/*
            if (newStm.VTR2_Remote_CRA__c != oldStm.VTR2_Remote_CRA__c) {
                // remote CRA changed
                if (oldStm.VTR2_Remote_CRA__c != null && oldStm.VTR2_Remote_CRA__c != newStm.VTR2_Onsite_CRA__c) {
                    stmsCraRemoved.put(oldStm.VTR2_Remote_CRA__c, oldStm);
                }
                if (newStm.VTR2_Remote_CRA__c != null && newStm.VTR2_Remote_CRA__c != oldStm.VTR2_Onsite_CRA__c) {
                    stmsCraAdded.put(newStm.VTR2_Remote_CRA__c, newStm);
                }
            }
            if (newStm.VTR2_Onsite_CRA__c != oldStm.VTR2_Onsite_CRA__c) {
                // onsite CRA changed
                if (oldStm.VTR2_Onsite_CRA__c != null && oldStm.VTR2_Onsite_CRA__c != newStm.VTR2_Remote_CRA__c) {
                    stmsCraRemoved.put(oldStm.VTR2_Onsite_CRA__c, oldStm);
                }
                if (newStm.VTR2_Onsite_CRA__c != null && newStm.VTR2_Onsite_CRA__c != oldStm.VTR2_Remote_CRA__c) {
                    stmsCraAdded.put(newStm.VTR2_Onsite_CRA__c, newStm);
                }
            }
			*/
			//END:SH-17440
        }

        System.debug('ua: stmsSiteAdded: ' + stmsSiteAdded);
        System.debug('ua: stmsSiteRemoved: ' + stmsSiteRemoved);
        System.debug('ua: stmsPrimaryPiAdded: ' + stmsPrimaryPiAdded);
        System.debug('ua: stmsPrimaryPiRemoved: ' + stmsPrimaryPiRemoved);
        System.debug('ua: stmsCraAddedByCraStm: ' + stmsPrimaryPiRemoved);
        System.debug('ua: stmsCraRemovedByCraStm: ' + stmsPrimaryPiRemoved);

        Map<Id, StmWrapper> stmWrapperMapForDeactivate = new Map<Id, StmWrapper>();
        Map<Id, StmWrapper> stmWrapperMapForActivate = new Map<Id, StmWrapper>();

        // FIND RELATED STMS
        // process site changed
        if (!stmsSiteRemoved.isEmpty()) {
            stmWrapperMapForDeactivate.putAll(getWrapperMapForPrimaryPiAndRelatedStms(stmsSiteRemoved));
        }
        if (!stmsSiteAdded.isEmpty()) {
            stmWrapperMapForActivate.putAll(getWrapperMapForPrimaryPiAndRelatedStms(stmsSiteAdded));
        }

        // process Primary PI changed on Backup PI
        if (!stmsPrimaryPiRemoved.isEmpty()) {
            stmWrapperMapForDeactivate.putAll(getWrapperMapForBackupPi(stmsPrimaryPiRemoved));
        }
        if (!stmsPrimaryPiAdded.isEmpty()) {
            stmWrapperMapForActivate.putAll(getWrapperMapForBackupPi(stmsPrimaryPiAdded));
        }

        // process Remote/Onsite CRA changed on Primary PI
        if (!stmsCraRemoved.isEmpty()) {
            stmWrapperMapForDeactivate.putAll(getWrapperMapForCra(stmsCraRemoved));
        }
        if (!stmsCraAdded.isEmpty()) {
            stmWrapperMapForActivate.putAll(getWrapperMapForCra(stmsCraAdded));
        }

        System.debug('ua: stmWrapperMapForDeactivate: ' + stmWrapperMapForDeactivate);
        System.debug('ua: stmWrapperMapForActivate: ' + stmWrapperMapForActivate);

        // GET AUDITS TO UPSERT
        if (!stmWrapperMapForDeactivate.isEmpty()) {
            auditsForUpsert.addAll(deactivateAuditsFromSite(stmWrapperMapForDeactivate));
        }
        if (!stmWrapperMapForActivate.isEmpty()) {
            auditsForUpsert.addAll(activateAuditsOnSite(stmWrapperMapForActivate));
        }
        upsert auditsForUpsert;

        System.debug('ua: onAfterUpdateSTM finish ' + Limits.getQueries());

    }


    public static void onBeforeDeleteSTM(Map<Id, Study_Team_Member__c> triggerOldMap) {

        System.debug('ua: onBeforeDeleteSTM start ' + Limits.getQueries());

        Map<Id, Study_Team_Member__c> stmsPi = new Map<Id, Study_Team_Member__c>();
        Map<Id, Study_Team_Member__c> stms = new Map<Id, Study_Team_Member__c>();

        List<VTR5_SiteStudyAccessAudit__c> auditsToUpdate = new List<VTR5_SiteStudyAccessAudit__c>();
        for (Study_Team_Member__c stm : triggerOldMap.values()) {
            stms.put(stm.Id, stm);
            if (stm.VTD1_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) {    // PI
                stmsPi.put(stm.Id, stm);
            }
        }

        // deactivate study and site access for deleted STM
        if (!stms.isEmpty()) {
            auditsToUpdate.addAll(deactivateAuditsFromStudy(stms));
        }
        // deactivate site access for related CRAs, backup PIs and SSTMs
        if (!stmsPi.isEmpty()) {
            Map<Id, StmWrapper> stmWrapperMap = getWrapperMapForRelatedStms(stmsPi);
            auditsToUpdate.addAll(deactivateAuditsFromSite(stmWrapperMap));
        }

        update auditsToUpdate;

        System.debug('ua: onBeforeDeleteSTM finish ' + Limits.getQueries());

    }


    // HANDLER METHODS


    /**
     * @description on creation of Backup PI without Virtual Site - use Site of Primary PI for audit creation
     *
     * @param backupPiStms - list of created stms with Type = PI and StudyTeamMember != null and VirtualSite = null
     * @return list of Site Study Access Audits to create
     */
    private static List<VTR5_SiteStudyAccessAudit__c> processInsertStmBackupPi(List<Study_Team_Member__c> backupPiStms) {
        List<Study_Team_Member__c> stms = [
                SELECT Id, User__c, Study__c, Study_Team_Member__r.VTD1_VirtualSite__c
                FROM Study_Team_Member__c
                WHERE Id IN :backupPiStms
        ];
        Map<Id, VTR5_UserAudit__c> userAuditByUser = VT_R5_CustomAuditHandler_Misc.getOrCreateUserAudits(stms);

        List<VTR5_SiteStudyAccessAudit__c> auditsToCreate = new List<VTR5_SiteStudyAccessAudit__c>();
        for (Study_Team_Member__c backupPiStm : stms) {
            VTR5_UserAudit__c userAudit = userAuditByUser.get(backupPiStm.User__c);
            Id siteId = backupPiStm.Study_Team_Member__r.VTD1_VirtualSite__c;
            auditsToCreate.add(createAccessAudit(backupPiStm, userAudit.Id, Datetime.now(), siteId, Datetime.now()));
        }
        return auditsToCreate;
    }

    /**
     * @description on creation of PI with Remote/Onsite CRA - update or create CRA audits
     *
     * @param piStms - list of created stms with Type = PI and Remote/Onsite CRA != null
     * @return list of Site Study Access Audits to create
     */
	//START RAJESH SH-17440 :Onsite/Remote CRA are removed from the STM.
    /*public static List<VTR5_SiteStudyAccessAudit__c> processInsertStmRelatedCras(List<Study_Team_Member__c> piStms) {

        Map<Id, Study_Team_Member__c> piStmByCraStm = new Map<Id, Study_Team_Member__c>();

        for (Study_Team_Member__c piStm : piStms) {
            if (piStm.VTR2_Remote_CRA__c != null) {
                piStmByCraStm.put(piStm.VTR2_Remote_CRA__c, piStm);
            }
            if (piStm.VTR2_Onsite_CRA__c != null) {
                piStmByCraStm.put(piStm.VTR2_Onsite_CRA__c, piStm);
            }
        }
        Map<Id, Study_Team_Member__c> craStms = new Map<Id, Study_Team_Member__c>([
                SELECT Id, Study__c, User__c
                FROM Study_Team_Member__c
                WHERE Id IN :piStmByCraStm.keySet()
        ]);
        List<VTR5_SiteStudyAccessAudit__c> craAudits = [
                SELECT Id,
                        User_Audit__c,
                        VTR5_AssignedDateStudy__c,
                        VTR5_SiteAccessStatus__c,
                        VTR5_StudyTeamMember__c
                FROM VTR5_SiteStudyAccessAudit__c
                WHERE VTR5_StudyTeamMember__c IN :craStms.keySet()
        ];

        Map<Id, VTR5_SiteStudyAccessAudit__c> auditsToUpdate = new Map<Id, VTR5_SiteStudyAccessAudit__c>();
        for (VTR5_SiteStudyAccessAudit__c a : craAudits) {
            Study_Team_Member__c craStm = craStms.get(a.VTR5_StudyTeamMember__c);
            Study_Team_Member__c piStm = piStmByCraStm.get(a.VTR5_StudyTeamMember__c);

            if (a.VTR5_SiteAccessStatus__c) {

                a = updateAccessAudit(a, piStm.VTD1_VirtualSite__c, piStm.CreatedDate);


            } else {
                a = createAccessAudit(craStm, a.User_Audit__c, a.VTR5_AssignedDateStudy__c, piStm.VTD1_VirtualSite__c, piStm.CreatedDate);
            }
            auditsToUpdate.put(a.VTR5_StudyTeamMember__c, a);
        }
        return auditsToUpdate.values();
    }
 */
 //END:SH-17440

    /**
    * @description update or create audits for PIs when Site added
    *
    * @param piStms
    * @return list of Site Study Access Audits to update
    */
    private static List<VTR5_SiteStudyAccessAudit__c> activateAuditsOnSite(Map<Id, StmWrapper> stmWrapperMap) {
        System.debug('ua: in activateAudits. stmWrapperMap: ' + stmWrapperMap);
        List<VTR5_SiteStudyAccessAudit__c> auditsToUpsert = new List<VTR5_SiteStudyAccessAudit__c>();
        Map<Id, VTR5_UserAudit__c> userAuditByUser = VT_R5_CustomAuditHandler_Misc.getOrCreateUserAudits(stmWrapperMap.values());

        // process audits
        List <VTR5_SiteStudyAccessAudit__c> audits = [
                SELECT Id, VTR5_SiteName__c, VTR5_StudyTeamMember__c
                FROM VTR5_SiteStudyAccessAudit__c
                WHERE VTR5_StudyTeamMember__c IN :stmWrapperMap.keySet()
                AND VTR5_StudyAccessStatus__c = TRUE
                AND VTR5_SiteDeactivatedDate__c = NULL
        ];
        Map<Id, Map<Id, List<VTR5_SiteStudyAccessAudit__c>>> auditsByStmAndSite = generateMapAuditsByStmAndSite(audits);

        for (Id stmId : stmWrapperMap.keySet()) {
            StmWrapper stmWrapper = stmWrapperMap.get(stmId);
            Id siteId = stmWrapper.virtualSiteId;
            Study_Team_Member__c stm = stmWrapper.stm;

            List<VTR5_SiteStudyAccessAudit__c> stmAuditsBlankSite = getAuditsFromMap(auditsByStmAndSite, stm.Id, null);

            if (stmAuditsBlankSite != null && !stmAuditsBlankSite.isEmpty()) {

//                if (siteId != null) {
                    for (VTR5_SiteStudyAccessAudit__c a : stmAuditsBlankSite) {
                        auditsToUpsert.add(updateAccessAudit(a, siteId, Datetime.now()));
                    }
//                }


            } else {
                VTR5_UserAudit__c userAudit = userAuditByUser.get(stm.User__c);
                auditsToUpsert.add(createAccessAudit(stm, userAudit.Id, stm.CreatedDate, siteId, Datetime.now()));
            }
        }
        return auditsToUpsert;
    }

    private static List<VTR5_SiteStudyAccessAudit__c> deactivateAuditsFromSite(Map<Id, StmWrapper> stmWrapperMap) {
        System.debug('ua: in deactivateAuditsForPi. stmWrapperMap: ' + stmWrapperMap);
        List <VTR5_SiteStudyAccessAudit__c> audits = [
                SELECT Id, VTR5_SiteName__c, VTR5_StudyTeamMember__c
                FROM VTR5_SiteStudyAccessAudit__c
                WHERE VTR5_StudyTeamMember__c IN :stmWrapperMap.keySet()
                AND VTR5_StudyAccessStatus__c = TRUE
                AND VTR5_SiteName__c != NULL
                AND VTR5_SiteAccessStatus__c = TRUE
        ];
        Map<Id, Map<Id, List<VTR5_SiteStudyAccessAudit__c>>> auditsByStmAndSite = generateMapAuditsByStmAndSite(audits);

        Map<Id, VTR5_SiteStudyAccessAudit__c> auditsToUpdate = new Map<Id, VTR5_SiteStudyAccessAudit__c>();
        for (Id stmId : stmWrapperMap.keySet()) {
            StmWrapper stmWrapper = stmWrapperMap.get(stmId);

            List<VTR5_SiteStudyAccessAudit__c> stmAuditsForSite = getAuditsFromMap(auditsByStmAndSite, stmId, stmWrapper.virtualSiteId);
            if (stmAuditsForSite != null && !stmAuditsForSite.isEmpty()) {
                for (VTR5_SiteStudyAccessAudit__c a : stmAuditsForSite) {
                    a.VTR5_SiteAccessStatus__c = false;
                    a.VTR5_SiteDeactivatedDate__c = Datetime.now();
                    a.VTR5_SiteDeactivatedBy__c = UserInfo.getName();
                    auditsToUpdate.put(a.Id, a);
                }
            }
        }
        return auditsToUpdate.values();
    }

    /**
     * @description on any STM deletion - deactivate his own audit
     *
     * @param stms - list of deleted stms with any type
     * @return list of Site Study Access Audits to update
     */
    private static List<VTR5_SiteStudyAccessAudit__c> deactivateAuditsFromStudy(Map<Id, Study_Team_Member__c> stms) {
        List<VTR5_SiteStudyAccessAudit__c> audits = [
                SELECT Id, VTR5_StudyName__c, VTR5_StudyAccessStatus__c, VTR5_SiteAccessStatus__c, User_Audit__c
                FROM VTR5_SiteStudyAccessAudit__c
                WHERE VTR5_StudyTeamMember__c IN :stms.values()
                AND VTR5_StudyAccessStatus__c = TRUE
        ];

        for (VTR5_SiteStudyAccessAudit__c a : audits) {
            a.VTR5_StudyDeactivatedBy__c = UserInfo.getName();
            a.VTR5_StudyDeactivatedDate__c = Datetime.now();
            a.VTR5_StudyAccessStatus__c = false;
            if (a.VTR5_SiteAccessStatus__c) {
                a.VTR5_SiteDeactivatedBy__c = UserInfo.getName();
                a.VTR5_SiteDeactivatedDate__c = Datetime.now();
                a.VTR5_SiteAccessStatus__c = false;
            }
        }
        return audits;
    }

    private static Map<Id, StmWrapper> getWrapperMapPrimaryPi(Map<Id, Study_Team_Member__c> primaryPiStms) {
        Map<Id, StmWrapper> stmWrapperMap = new Map<Id, StmWrapper>();
        for (Study_Team_Member__c primaryPiStm : primaryPiStms.values()) {
            stmWrapperMap.put(primaryPiStm.Id, new StmWrapper(primaryPiStm, primaryPiStm.VTD1_VirtualSite__c));
        }
        return stmWrapperMap;
    }

    private static Map<Id, StmWrapper> getWrapperMapBackupPisForPi(Map<Id, Study_Team_Member__c> primaryPiStms) {
        Map<Id, StmWrapper> stmWrapperMap = new Map<Id, StmWrapper>();
        List<Study_Team_Member__c> backupPiStms = [
                SELECT Id, Study_Team_Member__c, Study__c, User__c, CreatedDate
                FROM Study_Team_Member__c
                WHERE Study_Team_Member__c IN :primaryPiStms.keySet()
        ];
        for (Study_Team_Member__c backupPiStm : backupPiStms) {
            Study_Team_Member__c primaryPiStm = primaryPiStms.get(backupPiStm.Study_Team_Member__c);
            stmWrapperMap.put(backupPiStm.Id, new StmWrapper(backupPiStm, primaryPiStm.VTD1_VirtualSite__c));
        }
        return stmWrapperMap;
    }
//START RAJESH SH-17440 :CRA fields are removed from STM.

 /*   private static Map<Id, StmWrapper> getWrapperMapCrasForPi(Map<Id, Study_Team_Member__c> primaryPiStms) {
        Map<Id, StmWrapper> stmWrapperMap = new Map<Id, StmWrapper>();
        Set<Id> additionalStmIds = new Set<Id>();
        for (Study_Team_Member__c piStm : primaryPiStms.values()) {
		
            if (piStm.VTR2_Remote_CRA__c != null) {
                additionalStmIds.add(piStm.VTR2_Remote_CRA__c);
                stmWrapperMap.put(piStm.VTR2_Remote_CRA__c, new StmWrapper(piStm.VTD1_VirtualSite__c));
            }
            if (piStm.VTR2_Onsite_CRA__c != null) {
                additionalStmIds.add(piStm.VTR2_Onsite_CRA__c);
                stmWrapperMap.put(piStm.VTR2_Onsite_CRA__c, new StmWrapper(piStm.VTD1_VirtualSite__c));
            }
		
        }

        List<Study_Team_Member__c> stms = [
                SELECT Id, Study_Team_Member__c, Study__c, User__c, CreatedDate
                FROM Study_Team_Member__c
                WHERE Id IN :additionalStmIds
        ];
        for (Study_Team_Member__c stm : stms) {
            stmWrapperMap.get(stm.Id).stm = stm;
        }
		//END:SH-17440	
        return stmWrapperMap;
    }
	*/
//END:SH-17440

    private static Map<Id, StmWrapper> getWrapperMapSstmsForPi(Map<Id, Study_Team_Member__c> primaryPiStms) {
        Map<Id, StmWrapper> stmWrapperMap = new Map<Id, StmWrapper>();
        Set<Id> additionalStmIds = new Set<Id>();

        List<Study_Site_Team_Member__c> sstms = [
                SELECT Id,
                        VTR2_Associated_SCr__c,
                        VTR2_Associated_PI__c,
                        VTD1_Associated_PG__c,
                        VTD1_Associated_PI__c,
                        VTR2_Associated_SubI__c,
                        VTR2_Associated_PI3__c
                FROM Study_Site_Team_Member__c
                WHERE VTR2_Associated_PI__c IN :primaryPiStms.keySet()
                OR VTD1_Associated_PI__c IN :primaryPiStms.keySet()
                OR VTR2_Associated_PI3__c IN :primaryPiStms.keySet()
        ];
        for (Study_Site_Team_Member__c sstm : sstms) {
            if (sstm.VTR2_Associated_SCr__c != null && sstm.VTR2_Associated_PI__c != null) {
                additionalStmIds.add(sstm.VTR2_Associated_SCr__c);

                Study_Team_Member__c piStm = primaryPiStms.get(sstm.VTR2_Associated_PI__c);
                stmWrapperMap.put(sstm.VTR2_Associated_SCr__c, new StmWrapper(piStm.VTD1_VirtualSite__c));

            } else if (sstm.VTD1_Associated_PG__c != null && sstm.VTD1_Associated_PI__c != null) {
                additionalStmIds.add(sstm.VTD1_Associated_PG__c);

                Study_Team_Member__c piStm = primaryPiStms.get(sstm.VTD1_Associated_PI__c);
                stmWrapperMap.put(sstm.VTD1_Associated_PG__c, new StmWrapper(piStm.VTD1_VirtualSite__c));

            } else if (sstm.VTR2_Associated_SubI__c != null && sstm.VTR2_Associated_PI3__c != null) {
                additionalStmIds.add(sstm.VTR2_Associated_SubI__c);

                Study_Team_Member__c piStm = primaryPiStms.get(sstm.VTR2_Associated_PI3__c);
                stmWrapperMap.put(sstm.VTR2_Associated_SubI__c, new StmWrapper(piStm.VTD1_VirtualSite__c));
            }
        }

        List<Study_Team_Member__c> stms = [
                SELECT Id, Study_Team_Member__c, Study__c, User__c, CreatedDate
                FROM Study_Team_Member__c
                WHERE Id IN :additionalStmIds
        ];
        for (Study_Team_Member__c stm : stms) {
            stmWrapperMap.get(stm.Id).stm = stm;
        }
        return stmWrapperMap;
    }

    private static Map<Id, StmWrapper> getWrapperMapForBackupPi(Map<Id, Study_Team_Member__c> backupPiStms) {
        Map<Id, StmWrapper> stmWrapperMap = new Map<Id, StmWrapper>();
        Set<Id> primaryPiIds = new Set<Id>();
        for (Study_Team_Member__c backupPiStm : backupPiStms.values()) {
            primaryPiIds.add(backupPiStm.Study_Team_Member__c);
        }

        Map<Id, Study_Team_Member__c> primaryPiStms = new Map<Id, Study_Team_Member__c>([
                SELECT Id, VTD1_VirtualSite__c
                FROM Study_Team_Member__c
                WHERE Id IN :primaryPiIds
        ]);

        for (Study_Team_Member__c backupPiStm : backupPiStms.values()) {
            Study_Team_Member__c primaryPiStm = primaryPiStms.get(backupPiStm.Study_Team_Member__c);
            stmWrapperMap.put(backupPiStm.Id, new StmWrapper(backupPiStm, primaryPiStm.VTD1_VirtualSite__c));
        }
        return stmWrapperMap;
    }

    private static Map<Id, StmWrapper> getWrapperMapForCra(Map<Id, Study_Team_Member__c> primaryPiStmByCra) {
        Map<Id, StmWrapper> stmWrapperMap = new Map<Id, StmWrapper>();

        Map<Id, Study_Team_Member__c> craStms = new Map<Id, Study_Team_Member__c>([
                SELECT Id, Study_Team_Member__c, Study__c, User__c, CreatedDate
                FROM Study_Team_Member__c
                WHERE Id IN :primaryPiStmByCra.keySet()
        ]);

        for (Id craStmId : primaryPiStmByCra.keySet()) {
            Study_Team_Member__c craStm = craStms.get(craStmId);
            Study_Team_Member__c primaryPiStm = primaryPiStmByCra.get(craStmId);
            stmWrapperMap.put(craStm.Id, new StmWrapper(craStm, primaryPiStm.VTD1_VirtualSite__c));
        }
        return stmWrapperMap;
    }

    private static Map<Id, StmWrapper> getWrapperMapForPrimaryPiAndRelatedStms(Map<Id, Study_Team_Member__c> primaryPiStms) {
        Map<Id, StmWrapper> stmWrapperMap = new Map<Id, StmWrapper>();
        stmWrapperMap.putAll(getWrapperMapPrimaryPi(primaryPiStms));
        stmWrapperMap.putAll(getWrapperMapBackupPisForPi(primaryPiStms));
		//START RAJESH SH-17440
        //stmWrapperMap.putAll(getWrapperMapCrasForPi(primaryPiStms));
		//END:SH-17440
        stmWrapperMap.putAll(getWrapperMapSstmsForPi(primaryPiStms));
        return stmWrapperMap;
    }
    private static Map<Id, StmWrapper> getWrapperMapForRelatedStms(Map<Id, Study_Team_Member__c> primaryPiStms) {
        Map<Id, StmWrapper> stmWrapperMap = new Map<Id, StmWrapper>();
        stmWrapperMap.putAll(getWrapperMapBackupPisForPi(primaryPiStms));
		//START RAJESH SH-17440
        //stmWrapperMap.putAll(getWrapperMapCrasForPi(primaryPiStms));
		//END:SH-17440
        stmWrapperMap.putAll(getWrapperMapSstmsForPi(primaryPiStms));
        return stmWrapperMap;
    }

    /**
     * @description transform plain Audits list to map grouped by StmId and VirtualSiteId. structure:
     * {stmId1: {vs1: [audit1, audit2], null: audit3}}
     *
     * @param audits - list of user audits
     * @return map of user audits (structure above)
     */
    private static Map<Id, Map<Id, List<VTR5_SiteStudyAccessAudit__c>>> generateMapAuditsByStmAndSite(List<VTR5_SiteStudyAccessAudit__c> audits) {
        Map<Id, Map<Id, List<VTR5_SiteStudyAccessAudit__c>>> auditsByStmAndSite = new Map<Id, Map<Id, List<VTR5_SiteStudyAccessAudit__c>>>();
        for (VTR5_SiteStudyAccessAudit__c a : audits) {
            if (auditsByStmAndSite.get(a.VTR5_StudyTeamMember__c) == null) {
                auditsByStmAndSite.put(a.VTR5_StudyTeamMember__c, new Map<Id, List<VTR5_SiteStudyAccessAudit__c>>());
            }
            if (auditsByStmAndSite.get(a.VTR5_StudyTeamMember__c).get(a.VTR5_SiteName__c) == null) {
                auditsByStmAndSite.get(a.VTR5_StudyTeamMember__c).put(a.VTR5_SiteName__c, new List<VTR5_SiteStudyAccessAudit__c>());
            }
            auditsByStmAndSite.get(a.VTR5_StudyTeamMember__c).get(a.VTR5_SiteName__c).add(a);
        }
        return auditsByStmAndSite;
    }

    /**
     * @param auditsByStmAndSite - map of audits grouped by StmId and VirtualSiteId (method generateMapAuditsByStmAndSite)
     * @param stmId - Id of STM to extract
     * @param siteId - Id of Site to extract
     *
     * @return list of audits for specific STM and site
     */
    private static List<VTR5_SiteStudyAccessAudit__c> getAuditsFromMap(Map<Id, Map<Id, List<VTR5_SiteStudyAccessAudit__c>>> auditsByStmAndSite, Id stmId, Id siteId) {
        List<VTR5_SiteStudyAccessAudit__c> audits = new List<VTR5_SiteStudyAccessAudit__c>();
        if (auditsByStmAndSite.get(stmId) != null && auditsByStmAndSite.get(stmId).get(siteId) != null) {
            audits = auditsByStmAndSite.get(stmId).get(siteId);
        }
        return audits;
    }


    private static VTR5_SiteStudyAccessAudit__c createAccessAudit(Study_Team_Member__c stm, Id userAuditId, Datetime studyDate, Id siteId, Datetime siteDate) {
        VTR5_SiteStudyAccessAudit__c audit = new VTR5_SiteStudyAccessAudit__c(

                VTR5_StudyTeamMember__c = stm.Id,
                VTR5_StudyName__c = stm.Study__c,
                VTR5_StudyAccessStatus__c = true,
                User_Audit__c = userAuditId,


                VTR5_AssignedDateStudy__c = studyDate
        );
        if (siteId != null) {
            audit.VTR5_SiteAccessStatus__c = true;
            audit.VTR5_SiteName__c = siteId;
            audit.VTR5_AssignedDateSite__c = siteDate;
        }
        return audit;
    }

    private static VTR5_SiteStudyAccessAudit__c updateAccessAudit(VTR5_SiteStudyAccessAudit__c audit, Id siteId, Datetime siteDate) {
        if (siteId != null) {
            audit.VTR5_SiteAccessStatus__c = true;
            audit.VTR5_SiteName__c = siteId;
            audit.VTR5_AssignedDateSite__c = siteDate;
        }
        return audit;


    }

    public class StmWrapper {
        public Id virtualSiteId {get;set;}
        public Study_Team_Member__c stm {get;set;}

        public StmWrapper(Id virtualSiteId){
            this.virtualSiteId = virtualSiteId;
        }
        public StmWrapper(Study_Team_Member__c stm, Id virtualSiteId) {
            this.stm = stm;
            this.virtualSiteId = virtualSiteId;
        }


    }
}