/**
 * Created by Yevhen Kharchuk on 16-Mar-20.
 */

/**
 * We used without sharing because for some reason conversion process recalculates oldCase
 * sharing rules and the user who initiated transfer process loose the access what breaks
 * transfer finalization logic
 */
public without sharing class VT_R4_PatientTransferService {
    public class PatientTransferServiceWrapper {
        public HealthCloudGA__CandidatePatient__c candidatePatientsTransfer;
        public Boolean isSuccess;
        //public Id newCaseId;
    }
    Map<Id, HealthCloudGA__CandidatePatient__c> oldMap;
    Map<Id, HealthCloudGA__CandidatePatient__c> newMap;

    public VT_R4_PatientTransferService(Map<Id, HealthCloudGA__CandidatePatient__c> oldMap, Map<Id, HealthCloudGA__CandidatePatient__c> newMap) {
        this.oldMap = oldMap;
        this.newMap = newMap;
    }

    public void finalizeTransfer() {
        try {
            finalizeTransferNoTryCatch();
        } catch (Exception e) {
            insert new VTR4_Conversion_Log__c(
                    VTR4_Candidate_Patient__c = newMap.values()[0].Id,
                    VTR4_Error_Message__c = 'Transfer process error. ' + e.getMessage() + ' ' + e.getStackTraceString()
            );
        }
    }

    private void finalizeTransferNoTryCatch() {
        System.debug('finalizeTransferNoTryCatch');
        //HealthCloudGA__CandidatePatient__c successfullyTransferredCandidate = getTransferCandidatePatient();
        List <HealthCloudGA__CandidatePatient__c> successfullyTransferredCandidates = getTransferCandidatePatients();
        if (successfullyTransferredCandidates != null) {
            System.debug('finalizeTransferNoTryCatch size = ' + successfullyTransferredCandidates.size());
        } else
                return;
        if (Test.isRunningTest()) {
            /*if (successfullyTransferredCandidate != null) {
                finalizeTransferSuccess(successfullyTransferredCandidate);
            } else {
                HealthCloudGA__CandidatePatient__c candidate = newMap.values()[0];
                List<String> failedStatuses = getFailedConversionStatuses(candidate);
                if (failedStatuses.size() == 1) {
                    finalizeTransferFail(candidate);
                    logError(failedStatuses[0]);
                }
            }*/
        } else {
            List<VTR4_New_Transaction__e> events = new List<VTR4_New_Transaction__e>();
            for (HealthCloudGA__CandidatePatient__c candidate : successfullyTransferredCandidates) {
                System.debug('candidate = ' + candidate);
                PatientTransferServiceWrapper wrapper = new PatientTransferServiceWrapper();
                wrapper.candidatePatientsTransfer = candidate;
                wrapper.isSuccess = true;
                String wrapperJSON = JSON.serialize(wrapper);
                events.add(new VTR4_New_Transaction__e(VTR4_HandlerName__c = 'VT_R4_PatientConversionTransferHandler', VTR4_Parameters__c = wrapperJSON));
            }
            /*if (successfullyTransferredCandidate != null) {
                wrapper.candidatePatientsTransfer = successfullyTransferredCandidate;
                wrapper.isSuccess = true;
            } else {
                HealthCloudGA__CandidatePatient__c candidate = newMap.values()[0];
                List<String> failedStatuses = getFailedConversionStatuses(candidate);
                if (failedStatuses.size() == 1) {
                    wrapper.candidatePatientsTransfer = candidate;
                    wrapper.isSuccess = false;
                    logError(failedStatuses[0]);
                }
            }
            if (wrapper.candidatePatientsTransfer == null)
                return;
            String wrapperJSON = JSON.serialize(wrapper);
            events.add(new VTR4_New_Transaction__e(VTR4_HandlerName__c = 'VT_R4_PatientConversionTransferHandler', VTR4_Parameters__c = wrapperJSON));
            */
            List<Database.SaveResult> results = EventBus.publish(events);
            for (Database.SaveResult result : results) {
                System.debug('persist save result = ' + result);
                if (!result.isSuccess()) {
                    throw new DMLException (result.getErrors()[0].getMessage());
                }
            }
        }
    }

    private List <HealthCloudGA__CandidatePatient__c> getTransferCandidatePatients() {
        Set<Id> candidatePatientsTransferId = new Set<Id>();
        for (Id id : newMap.keySet()) {
            HealthCloudGA__CandidatePatient__c candidatePatient = newMap.get(id);
        if (candidatePatient.VTR4_CaseForTransfer__c != null
                    && oldMap.get(id).Conversion__c != 'Converted'
                && candidatePatient.Conversion__c == 'Converted') {
            candidatePatientsTransferId.add(candidatePatient.Id);
        }
        }
        if (candidatePatientsTransferId.isEmpty())
            return null;
        List<HealthCloudGA__CandidatePatient__c> transferCandidatePatients = getCandidatePatientsWithData(candidatePatientsTransferId);
        if (transferCandidatePatients.isEmpty()) {
            return null;
        } else {
            return transferCandidatePatients;
        }
    }

    public static void finalizeTransferSuccess(HealthCloudGA__CandidatePatient__c candidatePatientsTransfer) {
        System.debug('finalizeTransferSuccess ' + candidatePatientsTransfer);
        Case newCase = getTransferredCase(candidatePatientsTransfer);
        System.debug('newCase = ' + newCase);
        updateContactRelatedData(candidatePatientsTransfer, newCase);
        shareCaseToCaregiver(newCase.Id);
        shareStudyToCaregiver(newCase.Id);
        if (candidatePatientsTransfer.VTR4_WillMedRecordsCarryOver__c) {
            transferMedRecords(candidatePatientsTransfer.VTR4_CaseForTransfer__c, newCase.Id);
        }
        sendPlatformEventToRedirectToHomePage(candidatePatientsTransfer.VTR4_CaseForTransfer__r);
        sendTNCatalogNotifications(candidatePatientsTransfer, true, newCase.Id);
        /*PatientTransferServiceWrapper wrapper = new PatientTransferServiceWrapper();
        wrapper.candidatePatientsTransfer = candidatePatientsTransfer;
        wrapper.isSuccess = true;
        wrapper.newCaseId = newCase.Id;
        List<VTR4_New_Transaction__e> events = new List<VTR4_New_Transaction__e>();
        String wrapperJSON = JSON.serialize(wrapper);
        events.add(new VTR4_New_Transaction__e(VTR4_HandlerName__c = 'VT_R4_PatientConversionTransferHandler', VTR4_Parameters__c = wrapperJSON));
        List<Database.SaveResult> results = EventBus.publish(events);
        for (Database.SaveResult result : results) {
            System.debug('persist save result = ' + result);
            if (!result.isSuccess()) {
                throw new DMLException (result.getErrors()[0].getMessage());
            }
        }*/
    }

    private static Case getTransferredCase(HealthCloudGA__CandidatePatient__c candidatePatientsTransfer) {
        Case oldCaseWithContId = [SELECT ContactId FROM Case WHERE Id = :candidatePatientsTransfer.VTR4_CaseForTransfer__c][0];
        return [SELECT Id, VTD1_Subject_ID__c, ContactId FROM Case WHERE ContactId = :oldCaseWithContId.ContactId ORDER BY CreatedDate DESC][0];
    }

    private static void updateContactRelatedData(HealthCloudGA__CandidatePatient__c candidatePatientsTransfer, Case newCase) {
        List<Contact> contactsToUpdate = getTransferProcessContacts(candidatePatientsTransfer);
        System.debug('contactsToUpdate = ' + contactsToUpdate);
        if (contactsToUpdate.isEmpty()) {
            return;
        }

        List<VT_D1_Phone__c> phonesToUpdate = new List<VT_D1_Phone__c>();
        List<Address__c> addressesToUpdate = new List<Address__c>();
        for (Contact contact : contactsToUpdate) {
            if (contact.VTD1_UserId__r.Profile.Name == VT_D1_ConstantsHelper.PATIENT_PROFILE_NAME) {
                contact.VTR4_IsLockedPatient__c = false;
                contact.FirstName = candidatePatientsTransfer.rr_firstName__c;
                contact.LastName = candidatePatientsTransfer.rr_lastName__c;
                contact.VTR2_Primary_Language__c = candidatePatientsTransfer.VTD2_Language__c;
                contact.Email = candidatePatientsTransfer.rr_Email__c;
                contact.VTD1_Clinical_Study_Membership__c = newCase.Id;
                contact.VTD1_FullHomePage__c = false;

                if (!contact.Phones__r.isEmpty()) {
                    contact.Phones__r[0].PhoneNumber__c = candidatePatientsTransfer.VTD1_Patient_Phone__c;
                    contact.Phones__r[0].Type__c = candidatePatientsTransfer.VTR2_Patient_Phone_Type__c;
                    phonesToUpdate.add(contact.Phones__r[0]);
                }

                if (!contact.Addresses__r.isEmpty()) {
                    contact.Addresses__r[0].Name = candidatePatientsTransfer.HealthCloudGA__Address1Line1__c;
                    contact.Addresses__r[0].VTD1_Address2__c = candidatePatientsTransfer.HealthCloudGA__Address1Line2__c;
                    contact.Addresses__r[0].VTR3_AddressLine3__c = candidatePatientsTransfer.VTR3_AddressLine3__c;
                    contact.Addresses__r[0].City__c = candidatePatientsTransfer.HealthCloudGA__Address1City__c;
                    contact.Addresses__r[0].State__c = candidatePatientsTransfer.HealthCloudGA__Address1State__c;
                    contact.Addresses__r[0].ZipCode__c = candidatePatientsTransfer.HealthCloudGA__Address1PostalCode__c;
                    addressesToUpdate.add(contact.Addresses__r[0]);
                }
            } else if (contact.VTD1_UserId__r.Profile.Name == VT_D1_ConstantsHelper.CAREGIVER_PROFILE_NAME) {
                contact.VTD1_Clinical_Study_Membership__c = newCase.Id;
            }
        }
        Account accountToUpdate = new Account(Id = contactsToUpdate[0].AccountId);
        accountToUpdate.HealthCloudGA__CarePlan__c = newCase.Id;
        update contactsToUpdate;
        update addressesToUpdate;
        update phonesToUpdate;
        update accountToUpdate;
    }

    private static void shareCaseToCaregiver(Id newCaseId) {
        List<User> caregivers = [
                SELECT Id
                FROM User
                WHERE Profile.Name = :VT_D1_ConstantsHelper.CAREGIVER_PROFILE_NAME
                AND Contact.VTD1_Clinical_Study_Membership__c = :newCaseId
        ];

        List<CaseShare> forInsert = new List<CaseShare>();
        for (User caregiver : caregivers) {
            CaseShare share = new CaseShare();
            share.CaseAccessLevel = 'Edit';
            share.UserOrGroupId = caregiver.Id;
            share.CaseId = newCaseId;
            share.RowCause = 'Manual';
            forInsert.add(share);
        }

        insert forInsert;
    }

    private static void shareStudyToCaregiver(Id newCaseId) {
        List<User> caregivers = [
                SELECT Id, Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c
                FROM User
                WHERE Profile.Name = :VT_D1_ConstantsHelper.CAREGIVER_PROFILE_NAME
                AND Contact.VTD1_Clinical_Study_Membership__c = :newCaseId
        ];

        List<VTD1_Study_Sharing_Configuration__c> sharings = new List<VTD1_Study_Sharing_Configuration__c>();
        for (User caregiver : caregivers) {
            sharings.add(new VTD1_Study_Sharing_Configuration__c(
                    VTD1_User__c = caregiver.Id,
                    VTD1_Study__c = caregiver.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c,
                    VTD1_Access_Level__c = 'Edit'
            ));
        }

        insert sharings;
    }

    private static void transferMedRecords(Id oldCaseId, Id newCaseId) {
        Database.executeBatch(new VT_R4_PatientTransferCloneDocuments(oldCaseId, newCaseId), 3);
    }

    private static void sendPlatformEventToRedirectToHomePage(Case oldCaseId) {
        List<VTR4_PatientTransferred__e> notifications = new List<VTR4_PatientTransferred__e>();
        notifications.add(new VTR4_PatientTransferred__e(
                VTR4_Transfer_Status__c = 'Success',
                VTR4_User_Id__c = oldCaseId.VTD1_Patient_User__c,
                VTR4_CaseId__c = oldCaseId.Id
        ));
        EventBus.publish(notifications);
    }

    @Future(Callout=true)
    private static void sendTNCatalogNotificationsFuture(Id candidatePatientId, Boolean isSuccess, Id newCaseId) {
        List<HealthCloudGA__CandidatePatient__c> candidatePatient = getCandidatePatientsWithData(
                new Set<Id>{
                        candidatePatientId
                }
        );
        if (!candidatePatient.isEmpty()) {
            sendTNCatalogNotifications(candidatePatient[0], isSuccess, newCaseId);
        }
    }

    public static void sendTNCatalogNotifications(HealthCloudGA__CandidatePatient__c candidatePatient, Boolean isSuccess, Id newCaseId) {
        if (isSuccess) {
         	
			Set<Id> externalUserIdsToNotify = new Set<Id>();

            List<Case> lstNewCases = [SELECT Id, VTD1_PI_user__c, VTD1_PI_user__r.Profile.Name FROM Case WHERE Id = :newCaseId];

             if(!lstNewCases.isEmpty()){
                 if(lstNewCases[0].VTD1_PI_user__c!=null && lstNewCases[0].VTD1_PI_user__r.Profile.Name==VT_D1_ConstantsHelper.PI_PROFILE_NAME){
                      externalUserIdsToNotify.add(lstNewCases[0].VTD1_PI_user__c);
                }

             } 

            Set<Id> patientUserIds = new Map<Id, User>([
                    SELECT Id
                    FROM User
                    WHERE Profile.Name = :VT_D1_ConstantsHelper.PATIENT_PROFILE_NAME
                    AND Contact.VTD1_Clinical_Study_Membership__c = :newCaseId
            ]).keySet();    
              



            if (Test.isRunningTest())
                return;

            VT_D2_TNCatalogNotifications.generateNotifications(
                    new List<String>{
                            'N571'
                    },
                    new List<Id>{
                            newCaseId
                    },
                    null,
                    null,
                    new Map<String, List<Id>>{
                            'N571' + newCaseId => new List<Id>(externalUserIdsToNotify)
                    }
            );
                     
            VT_D2_TNCatalogNotifications.generateNotifications(
                    new List<String>{
                            'N568'
                    },
                    new List<Id>{
                            newCaseId
                    },
                    null,
                    null,
                    new Map<String, List<Id>>{
                            'N568' + newCaseId => new List<Id>(patientUserIds)
                    }
            );
        } else {
            System.debug('Fail notifications block');
            User u = [SELECT Id, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
            if (u.Profile.Name == VT_D1_ConstantsHelper.PI_PROFILE_NAME || u.Profile.Name == VT_D1_ConstantsHelper.SCR_PROFILE_NAME) {
                System.debug('Community users');
                VT_D2_TNCatalogNotifications.generateNotifications(
                        new List<String>{
                                'N573'
                        },
                        new List<Id>{
                                newCaseId
                        },
                        null,
                        null,
                        new Map<String, List<Id>>{
                                'N573' + newCaseId => new List<Id>{
                                        UserInfo.getUserId()
                                }
                        }
                );
            } else {
                System.debug('Salesforce users');
                VT_D2_TNCatalogNotifications.generateNotifications(
                        new List<String>{
                                'N566'
                        },
                        new List<Id>{
                                newCaseId
                        },
                        null,
                        null,
                        new Map<String, List<Id>>{
                                'N566' + newCaseId => new List<Id>{
                                        UserInfo.getUserId()
                                }
                        }
                );
            }
        }
    }

    /*
    * Old case sharing rules are recalculated during the conversion process and for now
    * we didn't find the reason so we recalculate them async after successful transfer
     */
    private void recalculateSharingRulesForOldCase(Id oldCaseId) {
        VT_R3_GlobalSharing.doSharing(oldCaseId);
    }

    private static List<Study_Team_Member__c> getStudyTeamMembers(Set<String> typesForTransfer, Set<Id> oldAndNewStudyOnCandidatePatient) {
        return [
                SELECT
                        User__c
                        , VTD1_Type__c
                        , Study__r.VTD1_Virtual_Trial_Study_Lead__c
                FROM Study_Team_Member__c
                WHERE VTD1_Type__c IN :typesForTransfer
                AND VTD1_Active__c = TRUE
                AND Study__c IN :oldAndNewStudyOnCandidatePatient
        ];
    }

    private List<String> getFailedConversionStatuses(HealthCloudGA__CandidatePatient__c candidatePatientsTransfer) {
        List<String> failedStatuses = new List<String>();
        if (candidatePatientsTransfer.VTR4_CaseForTransfer__c == null) {
            return failedStatuses;
        }
        if (candidatePatientsTransfer.VTR4_StatusCreate__c == 'Failed') {
            failedStatuses.add('Status [Create]');
        }
        if (candidatePatientsTransfer.VTR4_StatusGPP__c == 'Failed') {
            failedStatuses.add('Status [Generation of Patient Payments]');
        }
        if (candidatePatientsTransfer.VTR4_StatusSharing__c == 'Failed') {
            failedStatuses.add('Status [Sharing]');
        }
        if (candidatePatientsTransfer.VTR4_StatusSTA__c == 'Failed') {
            failedStatuses.add('Status [Study team assignment]');
        }
        if (candidatePatientsTransfer.VTR4_StatusStudyPhoneNumber__c == 'Failed') {
            failedStatuses.add('Status [Study Phone Number]');
        }
        if (candidatePatientsTransfer.VTR4_StatusStudyStratification__c == 'Failed') {
            failedStatuses.add('Status [Study Stratification]');
        }
        if (candidatePatientsTransfer.VTR4_StatusVisits__c == 'Failed') {
            failedStatuses.add('Status [Visits]');
        }

        return failedStatuses;
    }

    public static void finalizeTransferFail(HealthCloudGA__CandidatePatient__c candidatePatientsTransfer) {
        System.debug('finalizeTransferFail ' + candidatePatientsTransfer);
        Case newCase = getTransferredCase(candidatePatientsTransfer);
        sendTNCatalogNotifications(candidatePatientsTransfer, false, newCase.Id);
    }

    private void logError(String message) {
        throw new TransferProcessException(message);
    }

    private static List<HealthCloudGA__CandidatePatient__c> getCandidatePatientsWithData(Set<Id> candidatePatientIds) {
        return [
                SELECT
                        Id
                        , VTR4_CaseForTransfer__c
                        , VTR4_CaseForTransfer__r.VTD1_Study__c
                        , VTR4_CaseForTransfer__r.VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c
                        , rr_firstName__c
                        , rr_lastName__c
                        , VTD2_Language__c
                        , rr_Email__c
                        , VTD1_Patient_Phone__c
                        , VTR2_Patient_Phone_Type__c
                        , HealthCloudGA__Address1Line1__c
                        , HealthCloudGA__Address1Line2__c
                        , VTR3_AddressLine3__c
                        , HealthCloudGA__Address1City__c
                        , HealthCloudGA__Address1State__c
                        , HealthCloudGA__Address1PostalCode__c
                        , VTR4_CaseForTransfer__r.VTD1_Subject_ID__c
                        , Study__c
                        , Study__r.VTD1_Virtual_Trial_Study_Lead__c
                        , VTR4_StatusCreate__c
                        , VTR4_WillMedRecordsCarryOver__c
                        , VTR4_CaseForTransfer__r.VTD1_Patient_User__c
                FROM HealthCloudGA__CandidatePatient__c
                WHERE Id IN :candidatePatientIds
        ];
    }

    private static List<Contact> getTransferProcessContacts(HealthCloudGA__CandidatePatient__c candidatePatientsTransfer) {
        System.debug('candidatePatientsTransfer = ' + candidatePatientsTransfer.VTR4_CaseForTransfer__c);
        System.debug('candidatePatientsTransfer = ' + VT_D1_ConstantsHelper.CAREGIVER_PROFILE_NAME);
        System.debug('candidatePatientsTransfer = ' + VT_D1_ConstantsHelper.PATIENT_PROFILE_NAME);
        return [
                SELECT Id, AccountId, VTD1_UserId__r.Profile.Name
                        , (SELECT Id FROM Phones__r WHERE IsPrimaryForPhone__c = TRUE)
                        , (SELECT Id FROM Addresses__r WHERE VTD1_Primary__c = TRUE)
                FROM Contact
                WHERE VTD1_Clinical_Study_Membership__c = :candidatePatientsTransfer.VTR4_CaseForTransfer__c
                AND (VTD1_UserId__r.Profile.Name = :VT_D1_ConstantsHelper.CAREGIVER_PROFILE_NAME
                OR VTD1_UserId__r.Profile.Name = :VT_D1_ConstantsHelper.PATIENT_PROFILE_NAME)
        ];
    }

    public class TransferProcessException extends Exception {

    }
}