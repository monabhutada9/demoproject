/**
 * Created by Yuliya Yakushenkova on 10/19/2020.
 */

public class VT_R5_PatientPaymentsService {

    private List<VTD1_Patient_Payment__c> patientPayments;
    private static final Integer TIME_OFFSET = UserInfo.getTimeZone().getOffset(System.now());

    public VT_R5_PatientPaymentsService(String paymentId) {
        String filter = String.isNotBlank(paymentId) ? 'id-' + paymentId : null;
        this.patientPayments = VT_D1_PatientPaymentsControllerRemote.getPatientPayments(filter);
    }

    public List<Payment> getPayments() {
        List<Payment> payments = new List<Payment>();
        for (VTD1_Patient_Payment__c p : patientPayments) {
            if(!p.RecordType.Name.equalsIgnoreCase('compensation')) continue;
            payments.add(new Payment(p));
        }
        return payments;
    }

    public Payment getPaymentById() {
        String paymentTypeTranslated =
                VT_D1_TranslateHelper.getLabelValue('VTD1_Compensation', UserInfo.getLanguage());
        Payment payment;
        if (!patientPayments.isEmpty()) {
            payment = new Payment(patientPayments[0]);
            payment.setPaymentType(paymentTypeTranslated);
        }
        return payment;
    }

    public String setPaymentCompleteAndGetPaymentName() {
        if (!patientPayments.isEmpty() || patientPayments[0].VTD1_Status__c.equals(System.Label.VTD1_NotStarted)) {
            VTD1_Patient_Payment__c patientPayment = patientPayments[0];
            patientPayment.VTD1_Activity_Complete__c = true;
            update patientPayment;
            return patientPayment.Name;
        }
        return '';
    }

    public Map<String, Decimal> getPaymentsSum() {
        Map<String, Decimal> currencyBySums = new Map<String, Decimal>();
        for (VTD1_Patient_Payment__c p : patientPayments) {
            if (p.RecordType.Name.equals('Reimbursement') || p.VTD1_Status__c != 'Payment Issued') continue;
            if (!currencyBySums.containsKey(p.VTD1_Currency__c)) {
                currencyBySums.put(p.VTD1_Currency__c, p.VTD1_Amount_Formula__c);
            } else {
                Decimal sums = currencyBySums.get(p.VTD1_Currency__c);
                sums += p.VTD1_Amount_Formula__c;
                currencyBySums.put(p.VTD1_Currency__c, sums);
            }
        }
        return currencyBySums;
    }

    public class Payment {
        public String id;
        public String paymentType;
        public String title;
        public String description;
        public String status;
        public Long dateLastChange;
        public Decimal amount;
        public String paymentCurrency;
        public Boolean paymentActivityComplete;


        public Payment(VTD1_Patient_Payment__c patientPayment) {
            this.id = patientPayment.Id;
            this.paymentType = String.valueOf(patientPayment.RecordType.get('Label'));
            this.title = patientPayment.Name;
            this.description = patientPayment.VTD1_Description__c;
            this.status = patientPayment.VTD1_Status__c;
            this.dateLastChange = patientPayment.LastModifiedDate.getTime() + TIME_OFFSET;
            this.amount = patientPayment.VTD1_Amount_Formula__c;
            this.paymentCurrency = patientPayment.VTD1_Currency__c;
            this.paymentActivityComplete = patientPayment.VTD1_Activity_Complete__c;
        }

        public void setPaymentType(String type) {
            this.paymentType = type;
        }
    }
}