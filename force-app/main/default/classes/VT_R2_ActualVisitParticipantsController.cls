public without sharing class VT_R2_ActualVisitParticipantsController {
    public class VisitParticipant {
        public String Id;
        public String Name;
        public String Role;
        public String type;
        public String nameHHNPhleb;
        public String externalParticipantType;
        public String reasonForParticipation;
        public String externalParticipantEmail;
        public String phone;
        public Datetime proposedVisitDateTime;
        public Id recordTypeId;
        public Integer externalIndex;
        public Id visitMemberId;
    }

    public class ActualVisitParticipants {
        public List<VisitParticipant> visitParticipantList;
        public List<Id> existingVisitParticipantIdList;
        public Id protocolVisitId;
        public Id externalParticipantRecordTypeId;
        public Integer externalParticipantsLastNumber;
    }

    @AuraEnabled
    public static String getVisitParticipants(Id caseId, Id actualVisitId) {
        try {
            ActualVisitParticipants actualVisitParticipants = new ActualVisitParticipants();
            List<VisitParticipant> visitParticipantList = new List<VisitParticipant>();
            Set<Id> userIdSet = new Set<Id>();
            Integer externalParticipantNumber = 1;
            List<Id> existingVisitParticipantIdList = new List<Id>();
            actualVisitParticipants.protocolVisitId = getProtocolVisitId(actualVisitId);
            if (actualVisitId == null && actualVisitParticipants.protocolVisitId == null) {
                Id recordTypeIdRegular = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_VISIT_MEMBER_REGULAR;
                Case cas = [
                        SELECT VTD1_Primary_PG__c, VTD1_PI_user__c, VTD1_Patient_User__c
                        FROM Case
                        WHERE Id = :caseId
                ];
                if (cas.VTD1_Primary_PG__c != null) {
                    VisitParticipant visitParticipant1 = new VisitParticipant();
                    visitParticipant1.Id = cas.VTD1_Primary_PG__c;
                    visitParticipant1.Role = Label.VTR2_PatientGuide;
                    visitParticipant1.type = VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME;
                    visitParticipant1.recordTypeId = recordTypeIdRegular;
                    visitParticipantList.add(visitParticipant1);
                    userIdSet.add(visitParticipant1.Id);
                }

                VisitParticipant visitParticipant2 = new VisitParticipant();
                visitParticipant2.Id = cas.VTD1_PI_user__c;
                visitParticipant2.Role = Label.VTR2_PrimaryInvestigator;
                visitParticipant2.type = VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME;
                visitParticipant2.recordTypeId = recordTypeIdRegular;
                visitParticipantList.add(visitParticipant2);
                userIdSet.add(visitParticipant2.Id);

                VisitParticipant visitParticipant3 = new VisitParticipant();
                visitParticipant3.Id = cas.VTD1_Patient_User__c;
                visitParticipant3.Role = Label.VTR2_Patient;
                visitParticipant3.type = VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME;
                visitParticipant3.recordTypeId = recordTypeIdRegular;
                visitParticipantList.add(visitParticipant3);
                userIdSet.add(visitParticipant3.Id);

                Id caregiverId = VT_D1_PatientCaregiverBound.findCaregiverOfPatient(cas.VTD1_Patient_User__c);
                if (caregiverId != null) {
                    VisitParticipant visitParticipant4 = new VisitParticipant();
                    visitParticipant4.Id = caregiverId;
                    visitParticipant4.Role = Label.VTD2_Caregiver;
                    visitParticipant4.type = VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME;
                    visitParticipant4.recordTypeId = recordTypeIdRegular;
                    visitParticipantList.add(visitParticipant4);
                    userIdSet.add(visitParticipant4.Id);
                }
            } else {
                List<Visit_Member__c> actualVisitMemberList = [
                        SELECT Id, VTD1_Participant_User__c, VTD1_Member_Type__c,
                                VTD1_Name_HHN_Phleb__c, VTD1_External_Participant_Type__c, VTR2_ReasonForParticipation__c,
                                VTD1_External_Participant_Email__c, VTD1_Phone__c, VTD1_Proposed_Visit_Date_Time__c, RecordTypeId
                        FROM Visit_Member__c
                        WHERE VTD1_Actual_Visit__c = :actualVisitId
                ];
                for (Visit_Member__c actualVisitMember : actualVisitMemberList) {
                    VisitParticipant visitParticipant = new VisitParticipant();
                    visitParticipant.Role = actualVisitMember.VTD1_Member_Type__c;
                    visitParticipant.recordTypeId = actualVisitMember.RecordTypeId;
                    if (actualVisitMember.VTD1_Participant_User__c == null) {
                        visitParticipant.Name = actualVisitMember.VTD1_Name_HHN_Phleb__c;
                        visitParticipant.nameHHNPhleb = actualVisitMember.VTD1_Name_HHN_Phleb__c;
                        visitParticipant.externalParticipantType = actualVisitMember.VTD1_External_Participant_Type__c;
                        visitParticipant.reasonForParticipation = actualVisitMember.VTR2_ReasonForParticipation__c;
                        visitParticipant.externalParticipantEmail = actualVisitMember.VTD1_External_Participant_Email__c;
                        visitParticipant.phone = actualVisitMember.VTD1_Phone__c;
                        visitParticipant.proposedVisitDateTime = actualVisitMember.VTD1_Proposed_Visit_Date_Time__c;
                        visitParticipant.externalIndex = externalParticipantNumber;
                        externalParticipantNumber++;
                    } else {
                        visitParticipant.Id = actualVisitMember.VTD1_Participant_User__c;
                        visitParticipant.type = actualVisitMember.VTD1_Member_Type__c;
                    }
                    visitParticipant.visitMemberId = actualVisitMember.Id;
                    existingVisitParticipantIdList.add(actualVisitMember.Id);

                    visitParticipantList.add(visitParticipant);
                    userIdSet.add(visitParticipant.Id);
                }
            }
            Map<Id, User> userIdToUserMap = new Map<Id, User>([
                    SELECT Name
                    FROM User
                    WHERE Id IN :userIdSet
            ]);
            if (userIdToUserMap != null && !userIdToUserMap.isEmpty()) {
                for (VisitParticipant visitParticipant : visitParticipantList) {
                    if (visitParticipant.nameHHNPhleb == null) {
                        User usr = userIdToUserMap.get(visitParticipant.Id);
                        if (usr != null) {
                            visitParticipant.Name = usr.Name;
                        }
                    }
                }
            }
            actualVisitParticipants.visitParticipantList = visitParticipantList;
            actualVisitParticipants.existingVisitParticipantIdList = existingVisitParticipantIdList;
            actualVisitParticipants.externalParticipantRecordTypeId = Schema.SObjectType.Visit_Member__c.getRecordTypeInfosByDeveloperName().get('VTR2_ExternalMember').getRecordTypeId();
            actualVisitParticipants.externalParticipantsLastNumber = externalParticipantNumber - 1;
            return JSON.serialize(actualVisitParticipants);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static String updateDatesTimesMap(String membersString, Id visitId, String selectedDate) {
        VT_D1_ActualVisitsHelper visitHelper = new VT_D1_ActualVisitsHelper();
        List<Visit_Member__c> visitMembersUpdatedList = new List<Visit_Member__c>();
        visitMembersUpdatedList = (List<Visit_Member__c>) JSON.deserialize(membersString, List<Visit_Member__c>.class);
        List<String> visitMembersIdsUpdatedList = new List<String>();
        for (Visit_Member__c visitMember : visitMembersUpdatedList) {
            visitMembersIdsUpdatedList.add(visitMember.Id);
        }
        visitHelper.visitMembersIdUpdatedList = visitMembersIdsUpdatedList;
        String dataMap = selectedDate == null ? visitHelper.getAvailableDateTimes(visitId) : visitHelper.getAvailableTimes(visitId, selectedDate);
        return dataMap;
    }

    public static String getProtocolVisitId(Id actualVisitId) {
        String protocolVisitId;
        if (actualVisitId != null) {
            List<VTD1_Actual_Visit__c> avList = [
                    SELECT VTD1_Protocol_Visit__c
                    FROM VTD1_Actual_Visit__c
                    WHERE Id = :actualVisitId
            ];
            if (!avList.isEmpty()) {
                protocolVisitId = avList[0].VTD1_Protocol_Visit__c;
            }
        }
        return protocolVisitId;
    }

    @AuraEnabled
    public static VTD1_Actual_Visit__c getActualVisit(Id actualVisitId) {
        List<VTD1_Actual_Visit__c> actualList = [
                SELECT Id, VTD1_Status__c, RecordType.DeveloperName, VTR2_Independent_Rater_Flag__c
                FROM VTD1_Actual_Visit__c
                WHERE Id = :actualVisitId
        ];
        if (!actualList.isEmpty()) {
            return actualList.get(0);
        } else {
            return null;
        }
    }

    @AuraEnabled(Cacheable=true)
    public static String getAllParticipants(Id caseId, Id actualVisitId) {
        try {
            ActualVisitParticipants actualVisitParticipants = new ActualVisitParticipants();
            List<VisitParticipant> visitParticipantList = new List<VisitParticipant>();
            actualVisitParticipants.protocolVisitId = getProtocolVisitId(actualVisitId);
            Id recordTypeIdRegular = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_VISIT_MEMBER_REGULAR;
//            List<String> excludeProfiles = new List<String>{
//                    VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME,
//                    VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME
//            };
            List<String> caseRoles = new List<String>{
                    'VTD1_Primary_PG__c',
                    'VTD1_Secondary_PG__c',
                    'VTD1_PI_user__c',
                    'VTD1_Backup_PI_User__c',
                    'VTR2_SiteCoordinator__c',
                    'VTD1_Patient_User__c'
            };
            String caseRolesFields = String.join(caseRoles, ', ');
//            List<Case> cases = Database.query('SELECT VTD1_Study__c, '+caseRolesFields+' FROM Case WHERE Id = :caseId');
            List<Case> cases = Database.query(
                    'SELECT VTD1_Study__c, VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c,'
                            + caseRolesFields
                            + ' FROM Case WHERE Id = :caseId'
            );
            if (!cases.isEmpty()) {
                Set<Id> userIdsOnCase = new Set<Id>();
                for (String role : caseRoles) {
                    userIdsOnCase.add(String.valueOf(cases[0].get(role)));
                }
                Id caregiverId = VT_D1_PatientCaregiverBound.findCaregiverOfPatient(cases[0].VTD1_Patient_User__c);
                if (caregiverId != null) {
                    userIdsOnCase.add(caregiverId);
                }
//                if (VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME == VT_D1_HelperClass.getUserProfileName(UserInfo.getUserId())) {
//                    userIdsOnCase.addAll(getSiteCoordinatorsUserIds());
//                }
//                userIdsOnCase.addAll(getSCRandSubIUserIds(String.valueOf(cases[0].get('VTD1_PI_user__c'))));
                userIdsOnCase.addAll(getSCRandSubIUserIds(String.valueOf(cases[0].VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c)));
                for (User u : [
                        SELECT Id, Name, Profile.Name
                        FROM User
                        WHERE Id IN :userIdsOnCase
                        ORDER BY Name
                ]) {
                    VisitParticipant vp = new VisitParticipant();
                    vp.Id = u.Id;
                    vp.Name = u.Name;
                    vp.Role = u.Profile.Name;
                    vp.type = u.Profile.Name;
                    vp.recordTypeId = recordTypeIdRegular;
                    visitParticipantList.add(vp);
                }
                List<Study_Team_Member__c> stmList = [
                        SELECT Id, User__c, User__r.Name, VTD1_Type__c
                        FROM Study_Team_Member__c
                        WHERE Study__c = :cases[0].VTD1_Study__c
                        AND VTD1_Type__c = 'Recruitment Expert'
                ];
                for (Study_Team_Member__c stm : stmList) {
                    VisitParticipant vp = new VisitParticipant();
                    vp.Id = stm.User__c;
                    vp.Name = stm.User__r.Name;
                    vp.Role = stm.VTD1_Type__c;
                    vp.type = stm.VTD1_Type__c;
                    vp.recordTypeId = recordTypeIdRegular;
                    visitParticipantList.add(vp);
                }
            }
            actualVisitParticipants.visitParticipantList = visitParticipantList;
            return JSON.serialize(actualVisitParticipants);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    public static Set<Id> getSCRandSubIUserIds(Id piId) {
        Set<Id> userIdsSet = new Set<Id>();
        /*List<Study_Site_Team_Member__c> sstmList = [
                SELECT VTR2_Associated_PI__r.User__c, VTR2_Associated_SCr__r.User__c, VTR2_Associated_PI3__r.User__c, VTR2_Associated_SubI__r.User__c
                FROM Study_Site_Team_Member__c
                WHERE ((VTR2_Associated_PI__r.User__c = :piId AND VTR2_Associated_SCr__c!=NULL)
                        OR (VTR2_Associated_PI3__r.User__c = :piId AND VTR2_Associated_SubI__c != NULL))
        ];
        
        for (Study_Site_Team_Member__c sstm : sstmList) {
            if (String.isEmpty(sstm.VTR2_Associated_PI__r.User__c)) {
                userIdsSet.add(sstm.VTR2_Associated_SubI__r.User__c);
            } else {
                userIdsSet.add(sstm.VTR2_Associated_SCr__r.User__c);
            }
        }*/
        // included SH-7148, SH-7065, SH-7063 requirements
        Id piSTMId = piId;
        List<Study_Team_Member__c> primaryPIWithMembersList = [
                SELECT User__c, (
                        SELECT User__c
                        FROM Study_Team_Members__r
                ), (
                        SELECT VTR2_Associated_SubI__r.User__c
                        FROM Study_Site_Team_Members2__r
                ), (
                        SELECT VTD1_Associated_PG__r.User__c
                        FROM Study_Team_Member_Relationships1__r
                ), (
                        SELECT VTR2_Associated_SCr__r.User__c
                        FROM Study_Site_Team_Members1__r
                )
                FROM Study_Team_Member__c
                WHERE Id = :piSTMId
        ];
        if (!primaryPIWithMembersList.isEmpty()) {
            Study_Team_Member__c primaryPIWithMembers = primaryPIWithMembersList[0];
            userIdsSet.add(primaryPIWithMembers.User__c);
            for (Study_Team_Member__c backupPIMember : primaryPIWithMembers.Study_Team_Members__r) {
                userIdsSet.add(backupPIMember.User__c);
            }
            for (Study_Site_Team_Member__c SubIMember : primaryPIWithMembers.Study_Site_Team_Members2__r) {
                userIdsSet.add(SubIMember.VTR2_Associated_SubI__r.User__c);
            }
            for (Study_Site_Team_Member__c PGMember : primaryPIWithMembers.Study_Team_Member_Relationships1__r) {
                userIdsSet.add(PGMember.VTD1_Associated_PG__r.User__c);
            }
            for (Study_Site_Team_Member__c SCRMember : primaryPIWithMembers.Study_Site_Team_Members1__r) {
                userIdsSet.add(SCRMember.VTR2_Associated_SCr__r.User__c);
            }
        }
        return userIdsSet;
    }

/*    public static List<User> getUsersOnStudyRoles(Id studyId) {
        List<User> users = new List<User>();

        List<String> studyRoles = new List<String>{
                'VTD1_Remote_CRA__c',
                'VTD1_Virtual_Trial_head_of_Operations__c',
                'VTD1_Virtual_Trial_Study_Lead__c',
                'VTD1_Project_Lead__c',
                'VTD1_PMA__c',
                'VTD1_Monitoring_Report_Reviewer__c',
                'VTD1_Regulatory_Specialist__c'
        };
        String fields = 'Id, ' + String.join(studyRoles, ', ');
        List<HealthCloudGA__CarePlanTemplate__c> study = Database.query('SELECT '+fields+' FROM HealthCloudGA__CarePlanTemplate__c WHERE Id=:studyId');
        if (!study.isEmpty()) {
            Set<Id> userIds = new Set<Id>();
            for (String role : studyRoles) {
                userIds.add(String.valueOf(study[0].get(role)));
            }
            users = [SELECT Id, Name, Profile.Name FROM User WHERE Id IN :userIds];
        }
        return users;
    }*/

    @AuraEnabled
    public static String getVisitMembers(Id actualVisitId) {
        try {
            List<Visit_Member__c> visitMembers = [
                    SELECT Id, VTD1_Member_Type__c, VTD1_External_Participant_Type__c
                    FROM Visit_Member__c
                    WHERE VTD1_Actual_Visit__c = :actualVisitId
            ];
            return JSON.serialize(visitMembers);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static void createVisitMembers(Id actualVisitId, String visitParticipantsString, List<Id> existingVisitParticipantIdList) {
        List<Visit_Member__c> visitMemberListToInsert = new List<Visit_Member__c>();
        List<Id> visitParticipantsIdListForDelete = new List<Id>();
        List<VisitParticipant> visitParticipantsList = (List<VisitParticipant>) JSON.deserialize(visitParticipantsString, List<VisitParticipant>.class);
        Id recordTypeIdRegular = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_VISIT_MEMBER_REGULAR;
        List<Visit_Member__c> visitMemberListToDelete = new List<Visit_Member__c>();
        try {
            List<Id> currentExistingVisitParticipantIdList = new List<Id>();
            for (VisitParticipant visitParticipant : visitParticipantsList) {
                if (visitParticipant.visitMemberId == null) {
                    Visit_Member__c visitMember = new Visit_Member__c();
                    visitMember.VTD1_Actual_Visit__c = actualVisitId;
                    visitMember.RecordTypeId = visitParticipant.recordTypeId;
                    if (visitParticipant.nameHHNPhleb != null) {
                        visitMember.VTD1_Name_HHN_Phleb__c = visitParticipant.nameHHNPhleb;
                        visitMember.VTD1_External_Participant_Type__c = visitParticipant.externalParticipantType;
                        visitMember.VTR2_ReasonForParticipation__c = visitParticipant.reasonForParticipation;
                        visitMember.VTD1_External_Participant_Email__c = visitParticipant.externalParticipantEmail;
                        visitMember.VTD1_Phone__c = visitParticipant.phone;
                        visitMember.VTD1_Proposed_Visit_Date_Time__c = visitParticipant.proposedVisitDateTime;
                    } else {
                        visitMember.VTD1_Participant_User__c = visitParticipant.Id;
                        if (visitParticipant.recordTypeId == null) {
                            visitMember.RecordTypeId = recordTypeIdRegular;
                        }
                    }
                    visitMemberListToInsert.add(visitMember);
                } else {
                    currentExistingVisitParticipantIdList.add(visitParticipant.visitMemberId);
                }
            }
            for (Id existingVisitParticipantId : existingVisitParticipantIdList) {
                if (!currentExistingVisitParticipantIdList.contains(existingVisitParticipantId)) {
                    visitParticipantsIdListForDelete.add(existingVisitParticipantId);
                }
            }
            visitMemberListToDelete = [SELECT Id FROM Visit_Member__c WHERE Id IN:visitParticipantsIdListForDelete];
            if (VT_Utilities.isCreateable('Visit_Member__c')) {
                insert visitMemberListToInsert;
            }
            if (VT_Utilities.isDeletable('Visit_Member__c')) {
                delete visitMemberListToDelete;
            }
        } catch (Exception e) {
            if (VT_D1_ActualVisitTriggerHandler.currentException != null) {
                e = VT_D1_ActualVisitTriggerHandler.currentException;
                throw new AuraHandledException(e.getMessage());
            }
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled(Cacheable=true)
    public static String getCurrentUserProfile() {
        return [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
    }

    @AuraEnabled
    public static String getPatientPhone(Id caseId) {
        try {
            return VT_R2_ActualVisitFormController.getPatientPhone(caseId);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled(Cacheable=true)
    public static Map<String, String> getPicklistOptions(String sObjName, String fieldName) {
        Map<String, String> options = new Map<String, String>();
        if (!String.isEmpty(sObjName) && !String.isEmpty(fieldName)) {
            Schema.SObjectType objType = Schema.getGlobalDescribe().get(sObjName);
            SObject sObj = objType.newSObject();
            options = VT_D1_CustomMultiPicklistController.getSelectOptionsWithLabel(sObj, fieldName);
        }
        return options;
    }
}