@IsTest
public with sharing class AccountPhonesControllerTest {

    public static void getAccPhonesListTest() {
        Test.startTest();
        List<Account> accountList = [SELECT Id FROM Account LIMIT 1];
        if(accountList.size() > 0){
            Account acc = accountList.get(0);

            AccountPhonesController.AccountPhoneWrapper accountPhoneWrapper = new AccountPhonesController.AccountPhoneWrapper();
            //VT_D1_Phone__c phoneRec = accountPhoneWrapper.phoneRec;
            //Boolean isEdited = accountPhoneWrapper.isEdited;

            List<VT_D1_Phone__c> accPhonesList = AccountPhonesController.getAccPhonesList(acc.Id);

            List<Case> casesList = [SELECT VTD1_Patient_User__c, ContactId, AccountId  FROM Case];
            if(casesList.size() > 0) {
                Case cas = casesList.get(0);
                VT_D1_Phone__c phone = new VT_D1_Phone__c();
                phone.PhoneNumber__c = '4444';
                //phone.VTD1_Contact__c = cas.ContactId;
                phone.Account__c = acc.Id;
                phone.IsPrimaryForPhone__c = true;
                insert phone;

                List<AccountPhonesController.AccountPhoneWrapper> accPhonesWrapList =  AccountPhonesController.getAccPhonesWrapList(acc.Id);

                String accPhoneListWrapStr = JSON.serialize(accPhonesWrapList);
                AccountPhonesController.saveAccPhones(accPhoneListWrapStr, accPhoneListWrapStr);
            }
        }
        Test.stopTest();
    }
}