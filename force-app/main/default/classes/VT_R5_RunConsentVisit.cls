/**
 * Created by Kirill Latysh on 05-May-20.
 */
public with sharing class VT_R5_RunConsentVisit {
    public static final String EVENT_TYPE = 'consent';
    @TestVisible
    private static List<Database.SaveResult> results = new List<Database.SaveResult>();



    /**
    * The method is applied to create Platform event
    * @author Kirill Latysh
    * @param participantsSetIds
    * @date 07/05/2020
    * @issue SH-10506
    */

    public class ParamsVisit {
        @InvocableVariable(Label = 'Visit Id' Required = true)
        public String visitId;

        @InvocableVariable(Label = 'scheduled Date' description='may contain both scheduledStartDate and scheduledEndDate' Required = true)
        public Datetime scheduledDate;

        @InvocableVariable(Label = 'Account Id' Required = true)
        public String accountId;
    }





    @InvocableMethod
    public static void sendSignal(List<ParamsVisit> participantsSetIds) {
        Map<Id, Id> accountByVisitMap = new Map<Id, Id>();
        List<Id> visitIds = new List<Id>();
        Map<Id, Datetime> mapScheduledDates = new Map<Id, Datetime>();
        List<VTR4_CNotification__e> eventList = new List<VTR4_CNotification__e>();
        Set<Id> currentAccountIds = new Set<Id>();






        for (ParamsVisit param : participantsSetIds) {
            visitIds.add(param.visitId);
            mapScheduledDates.put(param.visitId, param.scheduledDate);
            accountByVisitMap.put(param.visitId, param.accountId);
        }




        List<VTD1_Actual_Visit__c> listVisits = [SELECT VTD1_Scheduled_Date_Time__c, VTD1_Scheduled_Visit_End_Date_Time__c, VTD1_Status__c FROM VTD1_Actual_Visit__c WHERE Id IN :visitIds];
        for(VTD1_Actual_Visit__c visit : listVisits){
            if(String.valueOf(visit.VTD1_Scheduled_Date_Time__c) == String.valueOf(mapScheduledDates.get(visit.Id))  ||
                    (String.valueOf(visit.VTD1_Scheduled_Visit_End_Date_Time__c) == String.valueOf(mapScheduledDates.get(visit.Id)) && visit.VTD1_Status__c == 'Scheduled')) {




                currentAccountIds.add(accountByVisitMap.get(visit.Id));
            }
        }
        List<Id> userIds = getUserIds(currentAccountIds);
        for(Id userId : userIds) {
            if(String.isBlank(userId)) {
                continue;
            }
            eventList.add(new VTR4_CNotification__e(VTR4_UserId__c = userId, VTR4_Type__c = EVENT_TYPE));
        }
        results = EventBus.publish(eventList);
        for (Database.SaveResult result : results) {
            if (!result.isSuccess()) {
                for (Database.Error error : result.getErrors()) {
                    System.debug(LoggingLevel.ERROR, 'Error returned: ' + error.getMessage());
                }
            }
        }



    }



    public static List<Id> getUserIds(Set<Id> listAccIds) {
        List<Id> userIds = new List<Id>();
        List<Account> listAccounts = [SELECT VTD2_Caregiver_s_ID__c, HealthCloudGA__CarePlan__r.VTD1_Patient_User__c FROM Account WHERE Id = :listAccIds];
        for(Account acc : listAccounts) {
            if(acc.HealthCloudGA__CarePlan__r.VTD1_Patient_User__c != null){
                userIds.add(acc.HealthCloudGA__CarePlan__r.VTD1_Patient_User__c);
                if (acc.VTD2_Caregiver_s_ID__c != null){
                    userIds.add(acc.VTD2_Caregiver_s_ID__c);
                }
            }
        }
        return userIds;
    }
}