/**
 * Created by Dmitry Gavrilov on 2/28/2020.
 */

@IsTest
public with sharing class VT_R4_RegulatoryDocsCertifyHelperTest {
    public static void certifyDocumentTest(){
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setOriginalName('tN');

        VTD1_Document__c doc = (VTD1_Document__c) new DomainObjects.VTD1_Document_t().addStudy(study).persist();

        HealthCloudGA__CarePlanTemplate__c testStudy = (HealthCloudGA__CarePlanTemplate__c) study.toObject();

        ContentVersion contentVersionInsert = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = Blob.valueOf('Test Content Data')
        );
        insert contentVersionInsert;

        ContentDocument cd = [SELECT Id FROM ContentDocument LIMIT 1];

        VTR4_TemplateContainer__c testTemplateContainer = new VTR4_TemplateContainer__c(VTR4_Study__c = testStudy.Id,
                VTR4_Type__c = 'Regulatory Document');
        insert testTemplateContainer;
        ContentDocumentLink cdl = new ContentDocumentLink(LinkedEntityId = testTemplateContainer.Id,
                ContentDocumentId = cd.Id);
        ContentDocumentLink cdl2 = new ContentDocumentLink(LinkedEntityId = doc.Id,
                ContentDocumentId = cd.Id);
        insert new List<ContentDocumentLink>{cdl,cdl2};

        Test.startTest();
        VTD1_Document__c compareDoc = VT_R4_RegulatoryDocsCertifyHelper.getDocument(doc.Id);
        System.assertEquals(doc.Id, compareDoc.Id);
        VTD1_Document__c emptyDoc = VT_R4_RegulatoryDocsCertifyHelper.getDocument(null);
        System.assertEquals(null, emptyDoc.Id);

        ContentDocumentLink testCdl = VT_R4_RegulatoryDocsCertifyHelper.getCertifyContentDocumentLink(testStudy.Id);
        System.assertEquals(cdl.Id, testCdl.Id);

        Id id = VT_R4_RegulatoryDocsCertifyHelper.certifyDocument(doc.Id);
        Boolean isUploaded = !VT_R4_RegulatoryDocsCertifyHelper.hasFileUploaded(id);
        System.assertEquals(true, isUploaded);

        Id id2 = [SELECT Id FROM VTD1_Document__c WHERE VTD2_Associated_Medical_Record_id__c = :doc.Id][0].Id;
        System.assertEquals(id2, id);
        Test.stopTest();
    }
}