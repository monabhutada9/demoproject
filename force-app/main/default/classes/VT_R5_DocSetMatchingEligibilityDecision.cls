/**
* Created by Denis Belkovskii on 9/14/2020.
*/

public without sharing class VT_R5_DocSetMatchingEligibilityDecision extends Handler {

    public static List<Id> notificationSourceIds = new List<Id>(); 
    public static List<Id> taskSourceIds = new List<Id>();
    public static List<String> tnCatalogCodes = new List<String>();
    public static List<String> taskCodes = new List<String>();
    public static Map<Id, Case> casesToUpdateMap = new Map<Id, Case>();
    public static List<String> ELIGIBILITY_DECISIONS_FOR_NOTIFICATIONS = new List<String> {'Eligible', 'Ineligible'};
    public static List<VT_R3_EmailsSender.ParamsHolder> senderParams = new List<VT_R3_EmailsSender.ParamsHolder>();
    public static Set<Id> docsToCompleteTasksIds = new Set<Id>();

    protected override void onBeforeUpdate(Handler.TriggerContext context) {
        List<VTD1_Document__c> newDocuments = (List<VTD1_Document__c>) context.newList;
        Map<Id, VTD1_Document__c> oldMapDocs = (Map<Id, VTD1_Document__c>) context.oldMap;
        Set<Id> casesIds = new Set<Id>(); 
        for (VTD1_Document__c doc : newDocuments) {
            if (doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE
                    && doc.VTD1_Clinical_Study_Membership__c != null) {
                casesIds.add(doc.VTD1_Clinical_Study_Membership__c);
            }
        }
        Map<Id, Case> casesMap = getCasesMap(casesIds);
        for (VTD1_Document__c doc : newDocuments) {
            if (doc.RecordTypeId != VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE) {
                continue;
            }
            Case caseToUpdate;
            Case clinicalStudyMembership;
            if (doc.VTD1_Clinical_Study_Membership__c != null) {
                clinicalStudyMembership = casesMap.get(doc.VTD1_Clinical_Study_Membership__c);
                caseToUpdate = new Case(Id = doc.VTD1_Clinical_Study_Membership__c);
            }
            if (doc.VTD1_Eligibility_Assessment_Status__c != null) {
                if(oldMapDocs.get(doc.Id).VTD1_Eligibility_Assessment_Status__c == null) {
                    /*Initial PI determination*/
                    doc.VTD2_EAFStatus__c = 'Signed by PI';
                }
                if (doc.VTD2_TMA_Eligibility_Decision__c != null
                        && oldMapDocs.get(doc.Id).VTD2_TMA_Eligibility_Decision__c == null) {
                    /*Initial TMA determination: change doc status*/
                    doc.VTD2_EAFStatus__c = 'Signed by TMA';
                    docsToCompleteTasksIds.add(doc.Id);
                }
            }
            if (doc.VTD1_Eligibility_Assessment_Status__c != null
                    && oldMapDocs.get(doc.Id).VTD1_Eligibility_Assessment_Status__c == null
                    && clinicalStudyMembership != null
                    && !clinicalStudyMembership.VTD2_TMA_Review_Required__c) {
                /*TMA review is not required*/
                caseToUpdate.VTD1_Eligibility_Status__c = doc.VTD1_Eligibility_Assessment_Status__c;
                casesToUpdateMap.put(caseToUpdate.Id, caseToUpdate);
                doc.VTD2_EAFStatus__c = VT_R4_ConstantsHelper_Documents.DOCUMENT_EAF_STATUS_COMPLETED;
                doc.VTD2_Final_Eligibility_Decision__c = doc.VTD1_Eligibility_Assessment_Status__c;
            }
            if (doc.VTD2_Final_Eligibility_Decision__c != null
                    && oldMapDocs.get(doc.Id).VTD2_Final_Eligibility_Decision__c == null
                    && doc.VTD1_Eligibility_Assessment_Status__c != doc.VTD2_TMA_Eligibility_Decision__c) {
                /*PI Made a final decision*/
                if (clinicalStudyMembership != null) {
                    caseToUpdate.VTD1_Eligibility_Status__c = doc.VTD2_Final_Eligibility_Decision__c;
                    casesToUpdateMap.put(caseToUpdate.Id, caseToUpdate);
                }
                doc.VTD2_EAFStatus__c = VT_R4_ConstantsHelper_Documents.DOCUMENT_EAF_STATUS_COMPLETED;
            }
            if (clinicalStudyMembership != null 
                    && clinicalStudyMembership.VTD2_TMA_Review_Required__c
                    && doc.VTD2_TMA_Eligibility_Decision__c != null
                     && oldMapDocs.get(doc.Id).VTD2_TMA_Eligibility_Decision__c != doc.VTD2_TMA_Eligibility_Decision__c ) {
                if (doc.VTD1_Eligibility_Assessment_Status__c != doc.VTD2_TMA_Eligibility_Decision__c) {
                    if (doc.VTD2_Final_Eligibility_Decision__c == null
                            && ELIGIBILITY_DECISIONS_FOR_NOTIFICATIONS.contains(doc.VTD2_TMA_Eligibility_Decision__c)) {
                        /*TMA Eligibility decision does not match PI eligibility decision*/
                        fillNotificationCodesAndSources(clinicalStudyMembership, doc.Id);
                        VT_R3_EmailsSender.ParamsHolder emailSenderParamsHolder = new VT_R3_EmailsSender.ParamsHolder();
                        emailSenderParamsHolder.recipientId = doc.VTD1_PI_Approver__c;
                        emailSenderParamsHolder.relatedToId = doc.Id;
                        emailSenderParamsHolder.templateDevName = 'VTD2_Review_TMA_Eligibility_Decision';
                        senderParams.add(emailSenderParamsHolder);
                        taskCodes.add('T604');
                        taskSourceIds.add(doc.Id);
                    }
                } else if (doc.VTD1_Eligibility_Assessment_Status__c != null) {
                    doc.VTD2_EAFStatus__c = VT_R4_ConstantsHelper_Documents.DOCUMENT_EAF_STATUS_COMPLETED;
                    doc.VTD2_Final_Eligibility_Decision__c = doc.VTD1_Eligibility_Assessment_Status__c;
                    caseToUpdate.VTD1_Eligibility_Status__c = doc.VTD2_Final_Eligibility_Decision__c;
                    casesToUpdateMap.put(caseToUpdate.Id, caseToUpdate);
                } 
            }
        }
    }

    protected override void onAfterUpdate(Handler.TriggerContext context) {
        if (!casesToUpdateMap.isEmpty()) {
            update casesToUpdateMap.values();
        }
        /*TMA Eligibility decision does not match PI eligibility decision: email to PI*/
        if (!senderParams.isEmpty()) {
            VT_R3_EmailsSender.sendEmails(senderParams);
        }
        /*TMA Eligibility decision does not match PI eligibility decision: generate notifications/post to chatter*/
        if (!notificationSourceIds.isEmpty() && !tnCatalogCodes.isEmpty()) {
            VT_D2_TNCatalogNotifications.generateNotifications(tnCatalogCodes, notificationSourceIds, null);
        }
        /*TMA Eligibility decision does not match PI eligibility decision: generate tasks*/
        if (!taskSourceIds.isEmpty() && !taskCodes.isEmpty()) {
            VT_D2_TNCatalogTasks.generateTasks(taskCodes, taskSourceIds, null, null);
        }
        completeTasks(docsToCompleteTasksIds);
    } 

    public static void fillNotificationCodesAndSources(Case clStudyMembership, Id docId) {
        if (clStudyMembership.VTR2_SiteCoordinator__c != null
                && (clStudyMembership.VTD1_Study__r.VTR2_Site_Staff_Responsible_for_Elgbl__c
                    || (clStudyMembership.VTD1_Primary_PG__c != null
                        && !clStudyMembership.Actual_Visits__r.isEmpty())
                    || (clStudyMembership.VTD1_Primary_PG__c == null))) {
            tnCatalogCodes.add('N032');
            notificationSourceIds.add(docId);
        } else {
            tnCatalogCodes.add('P032');
            notificationSourceIds.add(docId);
        } 
    }

    public static void completeTasks(Set<Id> docsToCompleteTasksIds) {
        if (!docsToCompleteTasksIds.isEmpty()) {
            List<Task> tasksToUpdate = new List<Task>();
            for (Task task : [
                    SELECT Id FROM Task
                    WHERE Document__c IN :docsToCompleteTasksIds
                    AND VTD1_Type_for_backend_logic__c = 'TMA task'
                    AND Status != :VT_R4_ConstantsHelper_Tasks.TASK_STATUS_COMPLETED]) {
                task.Status = VT_R4_ConstantsHelper_Tasks.TASK_STATUS_COMPLETED;
                tasksToUpdate.add(task);
            }
            if (!tasksToUpdate.isEmpty()){
                update tasksToUpdate;
            }
        }
    }

    public static Map<Id, Case> getCasesMap(Set<Id> caseIds) {
        Map<Id, Case> casesMap = new Map<Id, Case>();
        if (!caseIds.isEmpty()) {
            casesMap = new Map<Id, Case>([
                    SELECT  Id,
                            VTD1_Study__c,
                            VTD1_Subject_ID__c,
                            VTD1_Primary_PG__c,
                            VTR2_SiteCoordinator__c,
                            VTD1_Eligibility_Status__c,
                            VTD2_TMA_Review_Required__c,
                            VTD1_Study__r.VTR2_Site_Staff_Responsible_for_Elgbl__c,
                            (SELECT VTR2_Modality__c, VTD1_Onboarding_Type__c
                            FROM Actual_Visits__r
                            WHERE VTD1_Onboarding_Type__c = 'Screening'
                            AND VTR2_Modality__c = 'At Location'
                            LIMIT 1)
                    FROM Case
                    WHERE Id IN :caseIds
            ]);
        }
        return casesMap;
    }
}