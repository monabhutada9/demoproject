/**
 * Created by Yulia Yakushenkova on 22.06.2020.
 */

@IsTest
public without sharing class VT_R4_DeleteEDiaryNotificationsBatchTest {

    public static void testBatch() {
        User caregiverUser = getUser(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME);
        User patientUser = getUser(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        Case aCase = [SELECT Id FROM Case WHERE RecordType.Name = 'CarePlan' LIMIT 1];

        VTD1_Survey__c survey = (VTD1_Survey__c)
                new DomainObjects.VTD1_Survey_t()
                        .setVTD1_CSM(aCase.Id)
                        .setPatientUser(patientUser)
                        .setCaregiverUser(caregiverUser)
                        .setRecordTypeByName('ePRO')
                        .persist();

        createNotifications(survey.Id, patientUser, caregiverUser);

        Test.startTest();
        Database.executeBatch(new VT_R4_DeleteEDiaryNotificationsBatch(survey.Id));
        Test.stopTest();

//        Integer amountOfNotifications = [SELECT COUNT() FROM VTD1_NotificationC__c];
//        System.assertEquals(2, amountOfNotifications); // PT's and CG's notifications should not be deleted, please see SH-14318
    }

    private static void createNotifications(final String surveyId, final User patient, final User caregiver) {
        String messageParameters = surveyId + 'Name;VTD1_Protocol_Nickname__c;' + surveyId + 'VTD1_Due_Date__c';
        system.debug('### patient '+patient);
        System.runAs(patient) {
            new DomainObjects.VTD1_NotificationC_t()
                    .setVTD1_Receivers(patient.Id)
                    .setMessageParameters(messageParameters)
                    .persist();
        }

        System.runAs(caregiver) {
            new DomainObjects.VTD1_NotificationC_t()
                    .setVTD1_Receivers(caregiver.Id)
                    .setMessageParameters(messageParameters)
                    .persist();
        }
    }

    private static User getUser(final String profileName) {

        list<User> ulst = [SELECT Id,IsActive,IsPortalEnabled  FROM User WHERE Profile.Name = :profileName and IsActive=true LIMIT 1];
        system.debug('#### '+ulst);
        return ulst[0];

    }
}