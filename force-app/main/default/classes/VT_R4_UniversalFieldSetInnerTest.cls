/**
 * Created by Alexey Mezentsev on 2/11/2020.
 */

@IsTest
private class VT_R4_UniversalFieldSetInnerTest {
    @TestSetup
    static void setupMethod() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c testStudy = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
        Case cs = [SELECT Id, VTD1_Primary_PG__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];
        Id caseId = cs.Id;
        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Range__c = 4;
        protocolVisit.VTD1_VisitOffset__c = 2;
        protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient; Site Coordinator';
        insert protocolVisit;
        VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
        actualVisit.VTD1_Protocol_Visit__c = protocolVisit.Id;
        actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
        actualVisit.VTD1_Case__c = caseId;
        actualVisit.VTR2_Televisit__c = true;
        actualVisit.VTD1_Scheduled_Date_Time__c = System.today().addDays(3);
        insert actualVisit;
    }

    @IsTest
    static void getActualVisitByIdTest() {
        VTD1_Actual_Visit__c visit = [SELECT RecordTypeId FROM VTD1_Actual_Visit__c];
        VTD1_Actual_Visit__c obj = (VTD1_Actual_Visit__c) VT_R4_UniversalFieldSetInner.getActualVisitById(visit.Id);
        System.assertEquals(visit.RecordTypeId, obj.RecordTypeId);
        Object objIncorrect = VT_R4_UniversalFieldSetInner.getActualVisitById('6002i000002C0sM');
        System.assertEquals(null, objIncorrect);
    }
}