/**
* @author: Olga Baranova
* @date: 23-01-2020
* @description:
**/
public abstract class VT_R4_AbstractEproCreator {
    protected Map<Id, User> caregiverMap = new Map<Id, User>();
    protected List<VTD1_Survey__c> ePros = new List<VTD1_Survey__c>();
    protected Map<String, Schema.RecordTypeInfo> surveyRecTypeMap = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName();
    protected VT_R4_ProtocolAmendmentVersionHelper paVersionHelper;
    protected List<VT_R5_eCoaAddEventDate> eventDates = new List<VT_R5_eCoaAddEventDate>();
    protected void addEvent(Id userId, String eventName) {
        if (userId != null && eventName != null) {
            VT_R5_eCoaAddEventDate event = new VT_R5_eCoaAddEventDate(userId, eventName);
            eventDates.add(event);
        }
    }
    protected void sendEvents() {

        VT_R5_eCoaQueueAction.processAction(eventDates);

    }
    protected Datetime getEventDateTime(VTD1_Protocol_ePRO__c template, String patientTimeZone) {
        Datetime eventDateTime = System.now();
        TimeZone tz = TimeZone.getTimeZone(patientTimeZone);
        Integer timeToAdd = template.VTR4_EventOffset__c != null ? template.VTR4_EventOffset__c.intValue() : 0;
        switch on template.VTR4_EventOffsetPeriod__c {
            when 'Hours' {
                eventDateTime = eventDateTime.addHours(timeToAdd);
            }
            when 'Days' {
                eventDateTime = eventDateTime.addDays(timeToAdd);
            }
            when 'Weeks' {
                eventDateTime = eventDateTime.addDays(timeToAdd * 7);
            }
            when 'Months' {
                eventDateTime = eventDateTime.addMonths(timeToAdd);
            }
        }
        //if the event time is set, then the patient's eDiary should be available at this time, taking into account time zones.
        if (template.VTR4_EventTime__c != null) {
            Integer offset = tz.getOffset(eventDateTime) / -1000;
            eventDateTime = Datetime.newInstanceGmt(eventDateTime.date(), template.VTR4_EventTime__c);
            eventDateTime = eventDateTime.addSeconds(offset);
            Datetime datetimeNow = Datetime.newInstanceGmt(System.now().date(), System.now().time());
            eventDateTime = Datetime.newInstanceGmt(eventDateTime.date(), eventDateTime.time()) < datetimeNow ? eventDateTime.addDays(1) : eventDateTime;
        }
        return eventDateTime;
    }
    protected List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrapListOfLists(List<List<VTD1_Protocol_ePRO__c>> listOfLists) {
        List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrappedRecords = new List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper>();
        for (List<VTD1_Protocol_ePRO__c> protocolList : listOfLists) {
            for (VTD1_Protocol_ePRO__c protocol : protocolList) {
                wrappedRecords.add(new VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper(
                        protocol, protocol.VTR4_RootProtocolId__c, protocol.VTD1_Protocol_Amendment__c, protocol.VTR4_RemoveFromProtocol__c
                ));
            }
        }
        return wrappedRecords;
    }
    protected Datetime getReminderDate(Datetime dueDate, Decimal reminderWindow) {
        Datetime reminderDate;
        if (dueDate != null && reminderWindow != null) {
            reminderDate = dueDate.addMinutes(-Math.round(reminderWindow));
            if (reminderDate < System.now()) {
                reminderDate = null;
            }
        }
        return reminderDate;
    }
    protected Boolean isSubjectCaregiver(VTD1_Protocol_ePRO__c template) {
        return template.VTD1_Subject__c != null && template.VTD1_Subject__c.equalsIgnoreCase('Caregiver');
    }

    protected Boolean isECoaEventNeeded(VTD1_Protocol_ePRO__c template, SObject sObj) {
        Case cas = getCase(sObj);
        Boolean templateSubsetSafetyIsNotEmpty = String.isNotEmpty(template.VTR5_SubsetSafetyID__c);
        Boolean templateSubsetImmunoIsNotEmpty = String.isNotEmpty(template.VTR5_SubsetImmunoID__c);
        if (!(templateSubsetSafetyIsNotEmpty || templateSubsetImmunoIsNotEmpty)) {
            return true;
        } else if (templateSubsetSafetyIsNotEmpty && templateSubsetImmunoIsNotEmpty) {
            return template.VTR5_SubsetSafetyID__c.equalsIgnoreCase(cas.VTR5_SubsetSafety__c) && template.VTR5_SubsetImmunoID__c.equalsIgnoreCase(cas.VTR5_SubsetImmuno__c);
        } else if (templateSubsetSafetyIsNotEmpty) {
            return template.VTR5_SubsetSafetyID__c.equalsIgnoreCase(cas.VTR5_SubsetSafety__c);
        } else if (templateSubsetImmunoIsNotEmpty) {
            return template.VTR5_SubsetImmunoID__c.equalsIgnoreCase(cas.VTR5_SubsetImmuno__c);
        } else {
            return false;
        }
    }

    private Case getCase(SObject sObj) {
        Case cas;
        if (sObj.getSObjectType() == Case.SObjectType) {
            cas = (Case) sObj;
        } else if (sObj.getSObjectType() == VTD1_Actual_Visit__c.SObjectType) {
            cas = (Case) sObj.getSObject('VTD1_Case__r');
        }
        return cas;
    }

    protected Id getCaregiverId(SObject sObj, VTD1_Protocol_ePRO__c template) {
        Id caregiverId;
        Case cas = getCase(sObj);
        if (isSubjectCaregiver(template) || isDelegatedToCaregiver(sObj, template)) {
            if (this.caregiverMap.containsKey(cas.VTD1_Patient_User__r.Contact.AccountId)) {
                caregiverId = this.caregiverMap.get(cas.VTD1_Patient_User__r.Contact.AccountId).Id;
            }
        }
        return caregiverId;
    }

    protected Boolean isDelegatedToCaregiver(SObject sObj, VTD1_Protocol_ePRO__c template) {
        Case cas = getCase(sObj);
        return template.VTD1_Caregiver_on_behalf_of_Patient__c
                        || (cas != null && cas.VTD1_ePro_can_be_completed_by_Caregiver__c);
    }

    public static String getReviewerForEDiary(String assignedPG, String assignedSCR, String protocolEDiaryReviewer) {
        if (protocolEDiaryReviewer == VT_D1_ConstantsHelper.PG_PROFILE_NAME && assignedPG == null) {
            protocolEDiaryReviewer = VT_D1_ConstantsHelper.SCR_PROFILE_NAME;
        } else if (protocolEDiaryReviewer == VT_D1_ConstantsHelper.SCR_PROFILE_NAME && assignedSCR == null) {
            protocolEDiaryReviewer = VT_D1_ConstantsHelper.PG_PROFILE_NAME;
        }
        return protocolEDiaryReviewer;
    }
}