@IsTest
public with sharing class VT_D2_TaskHandlerTest {
    public static void cancelledPatientsTasksTest() {
        insert new VTD1_RTId__c (VTR2_Backend_Logic_User_ID__c = UserInfo.getUserId().substring(0, 15));
        List<Case> casesList = [SELECT Id, VTD1_Patient_User__c, VTD1_Patient_User__r.ContactId, VTD1_Study__c, Status FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];
        casesList[0].Status = 'Closed';
        update casesList;
        Test.startTest();
        insert new List<Task>{
                new Task(
                        HealthCloudGA__CarePlanTemplate__c = casesList[0].VTD1_Study__c,
                        VTD1_Case_lookup__c = casesList[0].Id,
                        WhoId = casesList[0].VTD1_Patient_User__r.ContactId,
                        OwnerId = casesList[0].VTD1_Patient_User__c
                ),
                new Task(
                        HealthCloudGA__CarePlanTemplate__c = casesList[0].VTD1_Study__c,
                        VTD1_Case_lookup__c = casesList[0].Id,
                        WhoId = casesList[0].VTD1_Patient_User__r.ContactId,
                        OwnerId = UserInfo.getUserId()
                )
        };
        Test.stopTest();
    }

    public static void testProcesses () {
        VT_R5_ShareGroupMembershipService.disableMembership = true;
        Case patientCase = [SELECT Id, AccountId, VTD1_Patient_User__c FROM Case LIMIT 1];
        //VT_R4_PatientConversion.patientConversionInProgress = false;
        insert new Contact (AccountId = patientCase.AccountId, FirstName = 'Caregiver', email = 'testemail@fv1.com',
                LastName = 'Name' + System.now(),
                RecordTypeId = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_CAREGIVER) ;
        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTR2_Visit_Participants__c = 'PI';
        insert protocolVisit;

        List<VTD1_Actual_Visit__c> avLists = new List<VTD1_Actual_Visit__c> {
                new VTD1_Actual_Visit__c (VTD1_Protocol_Visit__c = protocolVisit.Id, VTD1_Case__c = patientCase.Id,
                        VTR5_PatientCaseId__c = patientCase.Id),
                new VTD1_Actual_Visit__c (VTD1_Protocol_Visit__c = protocolVisit.Id, VTD1_Case__c = patientCase.Id,
                        VTR5_PatientCaseId__c = patientCase.Id, VTR2_Task_Assignment__c = 'Site Coordinator')};
        insert avLists;
        VTD1_RTId__c rtIds = VTD1_RTId__c.getInstance();

        Id avId = avLists[0].Id;
        Id avId2 = avLists[1].Id;
        List<Task> tasks = new List<Task>();

        User scrId = [SELECT Id, IsActive FROM User WHERE Profile.Name = 'Site Coordinator'
                AND FirstName = 'SiteCoordinator1' AND isActive = true LIMIT 1];
        if (!scrId.IsActive) {
            scrId.IsActive = true;
            update scrId;
        }

        // Share Patient Task To Caregiver
        Task patientTask = new Task (Subject = 'test',  OwnerId = patientCase.VTD1_Patient_User__c, VTD1_Case_lookup__c = patientCase.Id);
        tasks.add(patientTask);
        // NotificationC for SCR when task is created
        tasks.add(new Task (VTD2_Task_Unique_Code__c = '003', Subject = 'test',  OwnerId = scrId.Id,
                VTD1_Actual_Visit__c = avId, VTD1_Case_lookup__c = patientCase.Id));
        tasks.add(new Task (VTD2_Task_Unique_Code__c = '301', Subject = 'test', OwnerId = scrId.Id,
                VTD1_Actual_Visit__c = avId, VTD1_Case_lookup__c = patientCase.Id));
        tasks.add(new Task (VTD2_Task_Unique_Code__c = '227', Subject = 'test', OwnerId =scrId.Id,
                VTD1_Actual_Visit__c = avId, VTD1_Case_lookup__c = patientCase.Id));
        tasks.add(new Task (VTD2_Task_Unique_Code__c = '226', Subject = 'test',  OwnerId =scrId.Id,
                VTD1_Actual_Visit__c = avId, VTD1_Case_lookup__c = patientCase.Id, WhatId = patientCase.Id));
        tasks.add(new Task (VTD2_Task_Unique_Code__c = '312', Subject = 'test',  OwnerId =scrId.Id,
                VTD1_Actual_Visit__c = avId, VTD1_Case_lookup__c = patientCase.Id, WhatId = patientCase.Id));
        tasks.add(new Task (VTD2_Task_Unique_Code__c = '313', Subject = 'test',  OwnerId =scrId.Id,
                VTD1_Actual_Visit__c = avId, VTD1_Case_lookup__c = patientCase.Id, WhatId = patientCase.Id));
        tasks.add(new Task (VTD2_Task_Unique_Code__c = '310', Subject = 'test',  OwnerId =scrId.Id,
                VTD1_Actual_Visit__c = avId, VTD1_Case_lookup__c = patientCase.Id, WhatId = patientCase.Id));
        tasks.add(new Task (VTD2_Task_Unique_Code__c = '311', Subject = 'test',  OwnerId =scrId.Id,
                VTD1_Actual_Visit__c = avId, VTD1_Case_lookup__c = patientCase.Id, WhatId = patientCase.Id));
        tasks.add(new Task (VTD2_Task_Unique_Code__c = '005', Subject = 'test',  OwnerId =scrId.Id,
                VTD1_Actual_Visit__c = avId, VTD1_Case_lookup__c = patientCase.Id, WhatId = patientCase.Id));
        //RAND2 Final Eligibility Review Notification for PI
        tasks.add(new Task (VTD1_Type_for_backend_logic__c = 'Final Eligibility Decision Task', Subject = 'test', OwnerId = scrId.Id));

        //PG receives 'Verify Consent Packet' Task PB
        Task consentPacketTask = new Task (RecordTypeId = rtIds.VTD1_Task_TaskForCountersignPacket__c, Subject = 'test', OwnerId = scrId.Id);
        tasks.add(consentPacketTask);

        // PI PAYMENTS Compensation Quality Check Task
        Task paymentTask = new Task (VTD1_Compensation_Quality_Check__c = true, OwnerId = scrId.Id);
        tasks.add(paymentTask);

        //KITS - Task Support Process PB // Review Lab Results task is Closed
        Task visitTask1 = new Task (Type = 'Review Lab Results', OwnerId = scrId.Id, VTD1_Actual_Visit__c = avId);
        tasks.add(visitTask1);
        Task visitTask2 = new Task (Type = 'Review Lab Results', OwnerId = scrId.Id, VTD1_Actual_Visit__c = avId2);
        tasks.add(visitTask2);

        //KITS - Task Support Process Pb // Ship Welcome Kit is complete
        Task kitTask = new Task (Subject = 'Ship Welcome Kit', OwnerId = scrId.Id, WhatId = avId);
        tasks.add(kitTask);
        insert tasks;

        // AutoPopulation (Closed by) PB
        System.runAs(new User (Id = patientCase.VTD1_Patient_User__c)) {
            patientTask.Status = 'Completed';
            update patientTask;
        }

        List<Task> tasksForUpdate = new List<Task>();

        //PG receives 'Verify Consent Packet' Task PB
        consentPacketTask.Status = 'Completed';
        tasksForUpdate.add(consentPacketTask);

        //PI PAYMENTS Compensation Quality Check Task
        paymentTask.Status = 'Completed';
        tasksForUpdate.add(paymentTask);
        //update tasksForUpdate;

        //KITS - Task Support Process PB // Review Lab Results task is Closed
        visitTask1.Status = 'Completed';
        tasksForUpdate.add(visitTask1);
        visitTask2.Status = 'Completed';
        tasksForUpdate.add(visitTask2);

        //KITS - Task Support Process Pb // Ship Welcome Kit is complete
        kitTask.Status = 'Completed';
        tasksForUpdate.add(kitTask);


        update tasksForUpdate;

        System.assert([SELECT Id FROM VTD1_NotificationC__c].size() != 0);
        System.assert([SELECT Id FROM Task].size() != 0);
    }

    public static void testBatch () {
        Case patientCase = [SELECT Id, AccountId, VTD1_Patient_User__c, VTD1_Study__c FROM Case LIMIT 1];
        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Study__c = patientCase.VTD1_Study__c;
        protocolVisit.RecordTypeId = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByName().get('Onboarding').getRecordTypeId();
        protocolVisit.VTR2_Visit_Participants__c = 'Patient; Site Coordinator';
        protocolVisit.VTD1_VisitNumber__c = 'O2';
        protocolVisit.VTD1_Onboarding_Type__c = 'Screening';
        protocolVisit.VTR2_Modality__c = 'At Location';
        insert protocolVisit;

        List<VTD1_Actual_Visit__c> avsts = new List<VTD1_Actual_Visit__c>();
        avsts.add(new VTD1_Actual_Visit__c (VTD1_Case__c = patientCase.Id,  VTR2_Modality__c = 'At Location', VTD1_Protocol_Visit__c = protocolVisit.Id));
        avsts.add(new VTD1_Actual_Visit__c (VTD1_Case__c = patientCase.Id,  VTR2_Modality__c = 'At Home', VTD1_Protocol_Visit__c = protocolVisit.Id));
        insert avsts;

        List<Task> allTasks = new List<Task> ();
        List<Task> tasksForUpdate24hours = new List<Task> ();
        tasksForUpdate24hours.add(new Task(Subject = 'Test', VTD2_Task_Unique_Code__c = 'T612'));
        tasksForUpdate24hours.add(new Task(Subject = 'Test', VTD1_Type_for_backend_logic__c = 'Final Eligibility Decision Task', VTD1_Case_lookup__c = patientCase.Id));
        tasksForUpdate24hours.add(new Task(Subject = 'Test', VTD1_Type_for_backend_logic__c = 'Sign Patient Eligibility Assessment Form task', VTD1_Case_lookup__c = patientCase.Id));
        tasksForUpdate24hours.add(new Task(Subject = 'Test', Type = 'Review Lab Results'));
        tasksForUpdate24hours.add(new Task(Subject = 'Test', VTD2_Task_Unique_Code__c = '203'));
        tasksForUpdate24hours.add(new Task(Subject = 'Test', VTD2_Task_Unique_Code__c = '212'));
        tasksForUpdate24hours.add(new Task(Subject = 'Test', VTD1_Type_for_backend_logic__c = 'Update Calendar'));

        allTasks.addAll(tasksForUpdate24hours);

        List<Task> tasksForUpdate2Days = new List<Task> ();
        tasksForUpdate2Days.add(new Task(Subject = 'Test', VTD2_Task_Unique_Code__c = '203'));

        allTasks.addAll(tasksForUpdate2Days);

        List<Task> tasksForUpdate3Days = new List<Task> ();
        tasksForUpdate3Days.add(new Task(Subject = 'Test', VTD2_Task_Unique_Code__c = '203'));

        allTasks.addAll(tasksForUpdate3Days);

        List<Task> tasksForUpdate4Days = new List<Task> ();
        tasksForUpdate4Days.add(new Task(Subject = 'Test', VTD2_Task_Unique_Code__c = '203'));
        tasksForUpdate4Days.add(new Task(Subject = 'Test', VTD1_Type_for_backend_logic__c = 'Update Calendar'));

        allTasks.addAll(tasksForUpdate4Days);

        insert allTasks;

        for (Task t : tasksForUpdate24hours) {
            Test.setCreatedDate(t.Id, System.now().addMinutes(-60*23 - 30));
        }
        for (Task t : tasksForUpdate2Days) {
            Test.setCreatedDate(t.Id, System.now().addHours(-24 * 2).addMinutes(30));
        }
        for (Task t : tasksForUpdate3Days) {
            Test.setCreatedDate(t.Id, System.now().addHours(-24 * 3).addMinutes(30));
        }
        for (Task t : tasksForUpdate4Days) {
            Test.setCreatedDate(t.Id, System.now().addHours(-24 * 4).addMinutes(30));
        }

        update allTasks;

        Database.executeBatch(new VT_R5_TasksBatch(50));
    }
}