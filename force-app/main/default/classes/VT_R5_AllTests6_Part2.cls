@IsTest
private with sharing class VT_R5_AllTests6_Part2 {
    //without TestSetup
    @IsTest
    static void VT_R4_PrivacyEulaViewControllerTest1() {
        VT_R4_PrivacyEulaViewControllerTest.testGetContent();
    }
    @IsTest
    static void VT_R4_PrivacyEulaViewControllerTest2() {
        VT_R4_PrivacyEulaViewControllerTest.testGetVFContent();
    }
    @IsTest
    static void VT_R4_PrivacyEulaViewControllerTest3() {
        VT_R4_PrivacyEulaViewControllerTest.testExceptions();
    }
    @IsTest
    static void VT_R4_PrivacyEulaViewControllerTest4() {
        VT_R4_PrivacyEulaViewControllerTest.testGetArticles();
    }
    @IsTest
    static void VT_R4_PatientFilesDownloadBlockerTest1() {
        VT_R4_PatientFilesDownloadBlockerTest.downloadFileSoql();
    }
    @IsTest
    static void VT_R4_PatientFilesDownloadBlockerTest2() {
        VT_R4_PatientFilesDownloadBlockerTest.patientFileDownloadingIsNotAllowedForStudy();
    }
    @IsTest
    static void VT_R4_PatientFilesDownloadBlockerTest3() {
        VT_R4_PatientFilesDownloadBlockerTest.patientFileDownloadingIsAllowedForStudy();
    }
    @IsTest
    static void VT_R4_DeletedFilesHistoryControllerTest() {
        VT_R4_DeletedFilesHistoryControllerTest.documentFieldHistoryTest();
    }
    @IsTest
    static void VT_R4_ContentVersionProcessTest1() {
        VT_R4_ContentVersionProcessTest.testVersionProcess();
    }
    @IsTest
    static void VT_R4_ContentVersionProcessTest2() {
        VT_R4_ContentVersionProcessTest.testStatusComplete();
    }
    @IsTest
    static void VT_R4_ContentVersionProcessTest3() {
        VT_R4_ContentVersionProcessTest.testAttachment();
    }
    @IsTest
    static void VT_R4_ContentVersionProcessHandlerTest1() {
        VT_R4_ContentVersionProcessHandlerTest.testVersionProcess();
    }
    @IsTest
    static void VT_R4_ContentVersionProcessHandlerTest2() {
        VT_R4_ContentVersionProcessHandlerTest.testStatusComplete();
    }
    @IsTest
    static void VT_R4_ContentVersionProcessHandlerTest3() {
        VT_R4_ContentVersionProcessHandlerTest.testAttachment();
    }
    @IsTest
    static void VT_R4_ContentVersionProcessHandlerTest4() {
        VT_R4_ContentVersionProcessHandlerTest.testAttachment2();
    }
    @IsTest
    static void VT_R4_ContentVersionProcessHandlerTest5() {
        VT_R4_ContentVersionProcessHandlerTest.test();
    }
    @IsTest
    static void VT_R4_ContentVersionProcessHandlerTest6() {
        VT_R4_ContentVersionProcessHandlerTest.test1();
    }
    @IsTest
    static void VT_R4_ContentVersionProcessHandlerTest7() {
        VT_R4_ContentVersionProcessHandlerTest.test2();
    }
    @IsTest
    static void VT_R4_ContentVersionProcessHandlerTest8() {
        VT_R4_ContentVersionProcessHandlerTest.sendNotificationsTest();
    }
    @IsTest
    static void VT_R3_RestTaskConfirmationTest() {
        VT_R3_RestTaskConfirmationTest.closeTaskTest();
    }
    @IsTest
    static void VT_R3_RestPatientTransCustomLabelsTest1() {
        VT_R3_RestPatientTransCustomLabelsTest.testGetTranslatedCustomLabels();
    }
    @IsTest
    static void VT_R3_RestPatientTransCustomLabelsTest2() {
        VT_R3_RestPatientTransCustomLabelsTest.testGetCustomLabelsByMasterLabel();
    }
    @IsTest
    static void VT_R3_RestPatientTrainingMaterialsTest1() {
        VT_R3_RestPatientTrainingMaterialsTest.getTrainingMaterialsTest();
    }
    @IsTest
    static void VT_R3_RestPatientTrainingMaterialsTest2() {
        VT_R3_RestPatientTrainingMaterialsTest.getTrainingMaterialsTest2();
    }
    @IsTest
    static void VT_R3_RestPatientBroadcastCommentsTest() {
        VT_R3_RestPatientBroadcastCommentsTest.getCommentsTest();
    }
    @IsTest
    static void VT_R3_RestNotificationsTest() {
        VT_R3_RestNotificationsTest.getNotificationsTest();
    }
    @IsTest
    static void VT_R3_RestLiveChatSettingsTest() {
        VT_R3_RestLiveChatSettingsTest.getSettingsTest();
    }
    @IsTest
    static void VT_R3_RestFileDownloadTest() {
        VT_R3_RestFileDownloadTest.getFile();
    }
    @IsTest
    static void VT_R3_EmailStudyComplControllerTest() {
        VT_R3_EmailStudyComplControllerTest.testGenerateMessage();
    }
    @IsTest
    static void VT_R3_EmailNewNotificationControllerTest() {
        VT_R3_EmailNewNotificationControllerTest.Test();
    }
    @IsTest
    static void VT_R3_EmailIneligiblePatControllerTest() {
        VT_R3_EmailIneligiblePatControllerTest.renderStoredEmailTemplateTest();
    }
    @IsTest
    static void VT_R3_CustomNotificationTest() {
        VT_R3_CustomNotificationTest.test();
    }
    @IsTest
    static void VT_R3_AsyncDMLTest1() {
        VT_R3_AsyncDMLTest.testInsert();
    }
    @IsTest
    static void VT_R3_AsyncDMLTest2() {
        VT_R3_AsyncDMLTest.testUpdate();
    }
    @IsTest
    static void VT_R3_AsyncDMLTest3() {
        VT_R3_AsyncDMLTest.testUpsert();
    }
    @IsTest
    static void VT_R3_AsyncDMLTest4() {
        VT_R3_AsyncDMLTest.testDelete();
    }
    @IsTest
    static void VT_R3_AsyncDMLTest5() {
        VT_R3_AsyncDMLTest.testUndelete();
    }
    @IsTest
    static void VT_R3_AsyncDMLTest6() {
        VT_R3_AsyncDMLTest.testBatch();
    }
    @IsTest
    static void VT_R2_Test_PIShares() {
        VT_R2_Test_PIShares.test();
    }
    @IsTest
    static void VT_R2_UploadFilesControllerTest() {
        VT_R2_UploadFilesControllerTest.testGetSessionId();
    }
    @IsTest
    static void VT_R5_DeletedRecordHandlerTest() {
        VT_R5_DeletedRecordHandlerTest.createDeletedRecordsDocumentTest();
    }
}