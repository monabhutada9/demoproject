@IsTest
private class VT_D1_PIMyPatientDocumentsControllerTest {
    @IsTest
    static void getPatientDocumentsTest_AsPI() {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
            .setMaximumDaysWithoutLoggingIn(1)
            .addAccount(new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor')
            );

        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t();
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String dynamicPart = orgId + dateString + randomInt;
        String staticPart = 'testusertma'; //example of static part
        String uniqueUsername = staticPart + dynamicPart + '@iqviavttest.com';
        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t().addAccount(patientAccount)
            .setFirstName('Patient')
            .setLastName('Patient'+uniqueUsername);

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
            .addContact(patientContact)
            .setProfile('Patient')
        ;
        patientUser.persist();

        DomainObjects.User_t userPI = new DomainObjects.User_t()
            .setEmail('test@test2019.test')
            .addContact(new DomainObjects.Contact_t())
            .setProfile('Primary Investigator');

        User userSCR = (User) new DomainObjects.User_t()
            .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME)
            .addContact(new DomainObjects.Contact_t().setLastName('TestSCR'))
            .persist();
        User userPG = (User) new DomainObjects.User_t()
            .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME)
            .persist();

        Case casNew = (Case) new DomainObjects.Case_t()
            .setRecordTypeByName('CarePlan')
            //.addStudy(study)
            .addVTD1_Primary_PG(new DomainObjects.User_t())
            .addPIUser(userPI)
            .addContact(patientContact)
            .addAccount(patientAccount)
            .addVTD1_Patient_User(patientUser)
            //.addVTD1_Patient(new DomainObjects.VTD1_Patient_t())
            .persist();

        User u;
        List<Case> casesList = [SELECT VTD1_PI_user__c FROM Case WHERE VTD1_PI_user__r.Profile.Name = 'Primary Investigator'];
        System.assert(casesList.size() > 0);
        Test.startTest();
        if(casesList.size() > 0){
            Case cas = casesList.get(0);

            VTD1_Document__c doc = new VTD1_Document__c();
            doc.VTD1_Clinical_Study_Membership__c = cas.Id;
            doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD;
            doc.VTD1_Archived_Date_Stamp__c = Date.today();
            insert doc;

            ContentVersion contentVersion = new ContentVersion();
            contentVersion.PathOnClient = 'test.txt';
            contentVersion.Title = 'Test file';
            contentVersion.VersionData = Blob.valueOf('Test Data');
            insert contentVersion;

            ContentVersion contentVersion1 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id=:contentVersion.Id];

            ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
            contentDocumentLink.LinkedEntityId = doc.Id;
            contentDocumentLink.ContentDocumentId = contentVersion1.ContentDocumentId;
            contentDocumentLink.ShareType = 'I';
            contentDocumentLink.Visibility = 'AllUsers';
            insert contentDocumentLink;

            String userId = cas.VTD1_PI_user__c;
            u = [SELECT Id FROM User WHERE Id =: userId];

            ContentVersion contentVersion2 = VT_D1_PIMyPatientDocumentsController.getDocumentByRecord(doc.Id);

            System.runAs(u){
                VT_D1_PIMyPatientDocumentsController.PatientDocumentsWithAdditionalInformation patientDocumentsWithAdditionalInformationString = VT_D1_PIMyPatientDocumentsController.getPatientDocuments(cas.Id);

                VT_D1_PIMyPatientDocumentsController.getSiteURL();
                Boolean pendingApprovalProcess = VT_D1_PIMyPatientDocumentsController.ifThereIsPendingApprovalProcess(cas.Id);
            }
            Test.stopTest();
        }

    }

    @IsTest
    static void getPatientDocumentsTest_AsSCR() {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
            .setMaximumDaysWithoutLoggingIn(1)
            .addAccount(new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor')
            );

        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t();
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String dynamicPart = orgId + dateString + randomInt;
        String staticPart = 'testusertma'; //example of static part
        String uniqueUsername = staticPart + dynamicPart + '@iqviavttest.com';
        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t().addAccount(patientAccount)
            .setFirstName('Patient')
            .setLastName('Patient'+uniqueUsername);

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
            .addContact(patientContact)
            .setProfile('Patient')
        ;
        patientUser.persist();

        DomainObjects.User_t userSCR = new DomainObjects.User_t()
            .setEmail('test@test2019.test')
            .addContact(new DomainObjects.Contact_t())
            .setProfile('Site Coordinator');

        /*User userSCR = (User) new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME)
                .addContact(new DomainObjects.Contact_t().setLastName('TestSCR'))
                .persist();*/
        User userPG = (User) new DomainObjects.User_t()
            .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME)
            .persist();

        Case casNew = (Case) new DomainObjects.Case_t()
            .setRecordTypeByName('CarePlan')
            //.addStudy(study)
            .addVTD1_Primary_PG(new DomainObjects.User_t())
            //.addUser(userPI)
            .addContact(patientContact)
            .addAccount(patientAccount)
            .addVTD1_Patient_User(patientUser)
            //.addVTD1_Patient(new DomainObjects.VTD1_Patient_t())
            .persist();

        User u;
        //List<Case> casesList = [SELECT VTR2_SiteCoordinator__c FROM Case WHERE VTR2_SiteCoordinator__r.Profile.Name = 'Site Coordinator'];
        List<Case> casesList = [SELECT VTR2_SiteCoordinator__c FROM Case];


        System.assert(casesList.size() > 0);
        Test.startTest();
        if(casesList.size() > 0){
            Case cas = casesList.get(0);
            cas.VTR2_SiteCoordinator__c = userSCR.Id;
            update cas;
            VTD1_Document__c doc = new VTD1_Document__c();
            doc.VTD1_Clinical_Study_Membership__c = cas.Id;
            doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD;
            doc.VTD1_Archived_Date_Stamp__c = Date.today();
            insert doc;

            ContentVersion contentVersion = new ContentVersion();
            contentVersion.PathOnClient = 'test.txt';
            contentVersion.Title = 'Test file';
            contentVersion.VersionData = Blob.valueOf('Test Data');
            insert contentVersion;

            ContentVersion contentVersion1 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id=:contentVersion.Id];

            ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
            contentDocumentLink.LinkedEntityId = doc.Id;
            contentDocumentLink.ContentDocumentId = contentVersion1.ContentDocumentId;
            contentDocumentLink.ShareType = 'I';
            contentDocumentLink.Visibility = 'AllUsers';
            insert contentDocumentLink;

            String userId = cas.VTR2_SiteCoordinator__c;
            u = [SELECT Id FROM User WHERE Id =: userId];

            ContentVersion contentVersion2 = VT_D1_PIMyPatientDocumentsController.getDocumentByRecord(doc.Id);

            System.runAs(u){
                VT_D1_PIMyPatientDocumentsController.PatientDocumentsWithAdditionalInformation patientDocumentsWithAdditionalInformationString = VT_D1_PIMyPatientDocumentsController.getPatientDocuments(cas.Id);

                VT_D1_PIMyPatientDocumentsController.getSiteURL();
                Boolean pendingApprovalProcess = VT_D1_PIMyPatientDocumentsController.ifThereIsPendingApprovalProcess(cas.Id);
            }
            Test.stopTest();
        }

    }

    @IsTest
    static void updateDocumentsStatuses_Positive() {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
            .setMaximumDaysWithoutLoggingIn(1)
            .addAccount(new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor')
            );

        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t();
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String dynamicPart = orgId + dateString + randomInt;
        String staticPart = 'testusertma'; //example of static part
        String uniqueUsername = staticPart + dynamicPart + '@iqviavttest.com';
        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t().addAccount(patientAccount)
            .setFirstName('Patient')
            .setLastName('Patient'+uniqueUsername);

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
            .addContact(patientContact)
            .setProfile('Patient')
        ;
        patientUser.persist();

        DomainObjects.User_t userPI = new DomainObjects.User_t()
            .setEmail('test@test2019.test')
            .addContact(new DomainObjects.Contact_t())
            .setProfile('Primary Investigator');

        User userSCR = (User) new DomainObjects.User_t()
            .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME)
            .addContact(new DomainObjects.Contact_t().setLastName('TestSCR'))
            .persist();
        User userPG = (User) new DomainObjects.User_t()
            .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME)
            .persist();

        Case casNew = (Case) new DomainObjects.Case_t()
            .setRecordTypeByName('CarePlan')
            //.addStudy(study)
            .addVTD1_Primary_PG(new DomainObjects.User_t())
            .addPIUser(userPI)
            .addContact(patientContact)
            .addAccount(patientAccount)
            .addVTD1_Patient_User(patientUser)
            //.addVTD1_Patient(new DomainObjects.VTD1_Patient_t())
            .persist();

        User u;
        List<Case> casesList = [SELECT VTD1_PI_user__c FROM Case WHERE VTD1_PI_user__r.Profile.Name = 'Primary Investigator'];
        Case cas = casesList.get(0);
        String userId = cas.VTD1_PI_user__c;
        u = [SELECT Id FROM User WHERE Id =: userId];
        Test.startTest();


        VTD1_Document__c doc = new VTD1_Document__c();
        doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD;

        System.runAs(u) {
            insert doc;

            List<VTD1_Document__c> documents = [SELECT Id, RecordTypeId FROM VTD1_Document__c];
            documents[0].VTD1_Status__c = '';
            String serializedDocuments = JSON.serializePretty(documents);
            System.debug('DOCUMENT_NO_CONTEXT - '+documents);

            System.debug('DOCUMENT_BEFORE ACTION_CONTEXT - '+documents);
            Boolean isSuccess = VT_D1_PIMyPatientDocumentsController.updateDocumentsStatuses(documents);
            System.debug('DOCUMENT_WITH_CONTEXT - '+documents);
            Test.stopTest();

            System.assertEquals(true, isSuccess);
        }
    }

    @IsTest
    static void updateDocumentsStatuses_Negative() {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
            .setMaximumDaysWithoutLoggingIn(1)
            .addAccount(new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor')
            );

        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t();
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String dynamicPart = orgId + dateString + randomInt;
        String staticPart = 'testusertma'; //example of static part
        String uniqueUsername = staticPart + dynamicPart + '@iqviavttest.com';
        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t().addAccount(patientAccount)
            .setFirstName('Patient')
            .setLastName('Patient'+uniqueUsername);

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
            .addContact(patientContact)
            .setProfile('Patient')
        ;
        patientUser.persist();

        DomainObjects.User_t userPI = new DomainObjects.User_t()
            .setEmail('test@test2019.test')
            .addContact(new DomainObjects.Contact_t())
            .setProfile('Primary Investigator');

        User userSCR = (User) new DomainObjects.User_t()
            .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME)
            .addContact(new DomainObjects.Contact_t().setLastName('TestSCR'))
            .persist();
        User userPG = (User) new DomainObjects.User_t()
            .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME)
            .persist();

        Case casNew = (Case) new DomainObjects.Case_t()
            .setRecordTypeByName('CarePlan')
            //.addStudy(study)
            .addVTD1_Primary_PG(new DomainObjects.User_t())
            .addPIUser(userPI)
            .addContact(patientContact)
            .addAccount(patientAccount)
            .addVTD1_Patient_User(patientUser)
            //.addVTD1_Patient(new DomainObjects.VTD1_Patient_t())
            .persist();

        Test.startTest();
        VTD1_Document__c doc = new VTD1_Document__c();
        doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD;
        insert doc;
        List<VTD1_Document__c> documents = [SELECT Id, RecordTypeId FROM VTD1_Document__c];
        String serializedDocuments = JSON.serializePretty(documents);
        serializedDocuments = serializedDocuments.substring(5, 20);

        try {
            Boolean isSuccess = VT_D1_PIMyPatientDocumentsController.updateDocumentsStatuses(documents);
        } catch (Exception exc) {
            System.assertEquals('Script-thrown exception', exc.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void testApprovalProcess_Approve() {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
            .setMaximumDaysWithoutLoggingIn(1)
            .addAccount(new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor')
            );

        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t();
        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t().addAccount(patientAccount)
            .setFirstName('Patient')
            .setLastName('Patient');
        DomainObjects.User_t patientUser = new DomainObjects.User_t()
            .addContact(patientContact)
            .setProfile('Patient')
        ;
        patientUser.persist();

        User userSCR = (User) new DomainObjects.User_t()
            .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME)
            .addContact(new DomainObjects.Contact_t().setLastName('TestSCR'))
            .persist();
        User userPG = (User) new DomainObjects.User_t()
            .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME)
            .persist();
        Test.startTest();
        Case cas = (Case) new DomainObjects.Case_t()
            .setRecordTypeByName('CarePlan')
            //.addStudy(study)
            .addVTD1_Primary_PG(new DomainObjects.User_t())
            .addPIUser(new DomainObjects.User_t())
            .addContact(patientContact)
            .addAccount(patientAccount)
            .addVTD1_Patient_User(patientUser)
            //.addVTD1_Patient(new DomainObjects.VTD1_Patient_t())
            .persist();

        VTD1_Document__c doc = new VTD1_Document__c();

        doc.VTD1_Clinical_Study_Membership__c = cas.Id;

        doc.VTD1_Status__c = 'Pending Approval';
        doc.VTD1_Files_have_not_been_edited__c = true;
        doc.VTR2_SCR_Approver__c = userSCR.Id;
        doc.VTD1_PG_Approver__c = userPG.Id;
        insert doc;

        List<VTD1_Document__c> docList = new List<VTD1_Document__c>();
        docList.add(doc);

        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting approval');
        req.setObjectId(doc.id);
        Approval.ProcessResult result = Approval.process(req);

        VT_D1_PIMyPatientDocumentsController.approveMedicalRecords(docList);

        Test.stopTest();
    }

}