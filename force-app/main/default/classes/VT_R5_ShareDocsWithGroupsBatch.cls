/**
* @author: Carl Judge
* @date: 12-Oct-20
* @description: One time batch to share Certification and Visit documents with groups
**/

public without sharing class VT_R5_ShareDocsWithGroupsBatch implements Database.Batchable<SObject> {
    public static final String READ_ACCESS = 'Read';
    public static final String EDIT_ACCESS = 'Edit';

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
            SELECT Id,
                VTD1_Site__c,
                VTD1_Site__r.VTR5_EditShareGroupId__c,
                VTD1_Site__r.VTR5_ReadShareGroupId__c,
                VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c,
                VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c
            FROM VTD1_Document__c
            WHERE RecordTypeId IN :VT_D2_DocumentSharing_Constants.GROUP_SHARE_RECORDTYPE_IDS
            AND (VTD1_Site__r.VTR5_EditShareGroupId__c != NULL OR VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c != NULL)
        ]);
    }

    public void execute(Database.BatchableContext bc, List<VTD1_Document__c> documents) {
        List<VTD1_Document__Share> docShares = new List<VTD1_Document__Share>();
        for (VTD1_Document__c doc : documents) {
            Virtual_Site__c site = getSite(doc);
            docShares.add(getDocShare(doc.Id, site.VTR5_ReadShareGroupId__c, READ_ACCESS));
            docShares.add(getDocShare(doc.Id, site.VTR5_EditShareGroupId__c, EDIT_ACCESS));
        }
        insert docShares;
    }

    public void finish(Database.BatchableContext bc) {

    }

    private Virtual_Site__c getSite(VTD1_Document__c doc) {
        return doc.VTD1_Site__c != null ? doc.VTD1_Site__r : doc.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r;
    }

    private VTD1_Document__Share getDocShare(Id docId, Id groupId, String accessLevel) {
        return new VTD1_Document__Share(
            ParentId = docId,
            AccessLevel = accessLevel,
            UserOrGroupId = groupId,
            RowCause = 'GlobalSharing__c'
        );
    }
}