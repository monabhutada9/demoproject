/**
* @author: Carl Judge
* @date: 08-Aug-19
**/

public without sharing class VT_R2_SstmSharingHandler {
    private static VT_R5_SiteMemberCalculator beforeDeleteCalc;

    // Conquerors
    // Abhay
    // Jira Ref: SH-17438
    // Description: Commented by Abhay as we do not require this update code as of now.
    //              After architect review and discussion with Carl, this needs to commented.

    // // Conquerors
    // // Abhay & Akansksha
    // // Jira Ref: SH-17438, SH-17437
    // public static Map<Id, Set<Id>> siteToUsersMap;
    // public static Map<Id, Study_Team_Member__c> userToStmMap;
    // // End of code: Conquerors

    public static void handleBeforeDelete(List<Study_Site_Team_Member__c> sstms) {
        beforeDeleteCalc = new VT_R5_SiteMemberCalculator();
        beforeDeleteCalc.initForSSTMs(new Map<Id, Study_Site_Team_Member__c>(sstms).keySet());
    }

    public static void handleAfterDelete(List<Study_Site_Team_Member__c> sstms) {
        if (beforeDeleteCalc != null) {
            VT_R5_SiteMemberCalculator afterDeleteCalc = new VT_R5_SiteMemberCalculator();
            afterDeleteCalc.initForSites(beforeDeleteCalc.getSiteIds());
            List<VT_R5_SiteMemberCalculator.SiteUser> removedUsers = afterDeleteCalc.getNonMembers(beforeDeleteCalc.getSiteUsers());
            if (!removedUsers.isEmpty()) {
                VT_R5_SharingChainFactory.createFromSiteUsersRemoval(removedUsers).enqueue();
                VT_R5_ShareGroupMembershipService.removeSiteMembers(removedUsers);
            }
            beforeDeleteCalc = null;
        }
    }

    // Conquerors
    // Abhay
    // Jira Ref: SH-17438
    // Description: Commented by Abhay as we do not require this update code as of now.
    //              After architect review and discussion with Carl, this needs to commented.

    // // Conquerors
    // // Abhay & Akansksha
    // // Jira Ref: SH-17438, SH-17437
    // // Description: Handled the before update event to faclitate the change in CRA user in SSTM
    // public static void handleBeforeUpdate(List<Study_Site_Team_Member__c> sstms) {
    //     beforeDeleteCalc = new VT_R5_SiteMemberCalculator();
    //     beforeDeleteCalc.initForSSTMs(new Map<Id, Study_Site_Team_Member__c>(sstms).keySet());
    // }

    // // Conquerors
    // // Abhay & Akansksha
    // // Jira Ref: SH-17438, SH-17437
    // // Description: Handled the after update event to faclitate the change in CRA user in SSTM
    // public static void handleAfterUpdate(List<Study_Site_Team_Member__c> sstms) {
    //     VT_R5_SiteMemberCalculator afterUpdateCalc = new VT_R5_SiteMemberCalculator();
    //     afterUpdateCalc.initForSites(beforeDeleteCalc.getSiteIds());
    //     List<VT_R5_SiteMemberCalculator.SiteUser> removedUsers = afterUpdateCalc.getNonMembers(beforeDeleteCalc.getSiteUsers());
    //     if (!removedUsers.isEmpty()) {
    //         VT_R5_SharingChainFactory.createFromSiteUsersRemoval(removedUsers).enqueue();
    //         VT_R5_ShareGroupMembershipService.removeSiteMembers(removedUsers);
    //     }
        
    //     VT_R5_SiteMemberCalculator memberCalc = new VT_R5_SiteMemberCalculator();
    //     memberCalc.initForSSTMs(new Map<Id, Study_Site_Team_Member__c>(sstms).keySet());
    //     List<VT_R5_SiteMemberCalculator.SiteUser> siteUsers = memberCalc.getSiteUsers();

    //     siteToUsersMap = memberCalc.getSiteToUsersMap();
    //     Set<Id> craUserIdSet = new Set<Id>();
    //     for(Set<Id> userIdSet: siteToUsersMap.values()) {
    //         craUserIdSet.addAll(userIdSet);
    //     }
    //     userToStmMap = new Map<Id, Study_Team_Member__c>();
    //     for(Study_Team_Member__c stmObj: [SELECT Id, 
    //                                              Study__c, 
    //                                              User__c
    //                                         FROM Study_Team_Member__c
    //                                         WHERE User__c IN: craUserIdSet]) {
    //         userToStmMap.put(stmObj.User__c, stmObj);
    //     } 
    //     if (!siteUsers.isEmpty()) {
    //         VT_R5_SharingChainFactory.createFromSiteUsers(siteUsers).enqueue();
    //         VT_R5_ShareGroupMembershipService.addSiteMembers(siteUsers);
    //     }
    // }
    // // End of code: Conquerors

    public static void handleInsert(List<Study_Site_Team_Member__c> sstms) {
        if (Test.isRunningTest() && VT_R3_GlobalSharing.disableForTest) { return; }
        VT_R5_SiteMemberCalculator memberCalc = new VT_R5_SiteMemberCalculator();
        memberCalc.initForSSTMs(new Map<Id, Study_Site_Team_Member__c>(sstms).keySet());
        List<VT_R5_SiteMemberCalculator.SiteUser> siteUsers = memberCalc.getSiteUsers();

        // Conquerors
        // Abhay
        // Jira Ref: SH-17438
        // Description: Commented by Abhay as we do not need the map as this is no longer required.

        // // Conquerors
        // // Abhay & Akansksha
        // // Jira Ref: SH-17438, SH-17437 
        // siteToUsersMap = memberCalc.getSiteToUsersMap();
        // Set<Id> craUserIdSet = new Set<Id>();
        // for(Set<Id> userIdSet: siteToUsersMap.values()) {
        //     craUserIdSet.addAll(userIdSet);
        // }
        // userToStmMap = new Map<Id, Study_Team_Member__c>();
        // for(Study_Team_Member__c stmObj: [SELECT Id, 
        //                                          Study__c, 
        //                                          User__c
        //                                     FROM Study_Team_Member__c
        //                                     WHERE User__c IN: craUserIdSet
        //                                     WITH SECURITY_ENFORCED]) {
        //     userToStmMap.put(stmObj.User__c, stmObj);
        // }
        // // End of code: Conquerors
        
        if (!siteUsers.isEmpty()) {
            VT_R5_SharingChainFactory.createFromSiteUsers(siteUsers).enqueue();
            VT_R5_ShareGroupMembershipService.addSiteMembers(siteUsers);
        }
    }

    // Conquerors
    // Abhay & Akansksha
    // Jira Ref: SH-17438, SH-17437
    // Description: Handling the updation of the Virtual Site lookup on the STM record.
    public static void updateVirtualSiteOnSTM(List<Study_Site_Team_Member__c> newList, Map<Id, Study_Site_Team_Member__c> oldMap) { 
        Map<Id, Id> stmIdAndVirtualSiteIdMap = new Map<Id, Id>();
        for(Study_Site_Team_Member__c sstmObj: newList) {
            if(oldMap == null 
                && sstmObj.VTR5_Associated_CRA__c != null
                && sstmObj.VTD1_SiteID__c != null) {

                stmIdAndVirtualSiteIdMap.put(sstmObj.VTR5_Associated_CRA__c, sstmObj.VTD1_SiteID__c);
            }
            else if(oldMap != null 
                    && sstmObj.VTR5_Associated_CRA__c != null
                    && sstmObj.VTR5_Associated_CRA__c != oldMap.get(sstmObj.Id).VTR5_Associated_CRA__c
                    && sstmObj.VTD1_SiteID__c != null) {

                stmIdAndVirtualSiteIdMap.put(sstmObj.VTR5_Associated_CRA__c, sstmObj.VTD1_SiteID__c);
            }
        }
        if(stmIdAndVirtualSiteIdMap.keySet().size() > 0) {
            List<Study_Team_Member__c> stmsToInsertList = new List<Study_Team_Member__c>();

            for(Study_Team_Member__c stmObj: [SELECT Id, 
                                                     VTD1_VirtualSite__c,
                                                     User__c
                                                FROM Study_Team_Member__c
                                                WHERE Id IN: stmIdAndVirtualSiteIdMap.keySet()]) {
        
                stmObj.VTD1_VirtualSite__c = stmIdAndVirtualSiteIdMap.containsKey(stmObj.Id) ? 
                                             stmIdAndVirtualSiteIdMap.get(stmObj.Id) : stmObj.VTD1_VirtualSite__c;
                stmsToInsertList.add(stmObj);
            }

            if(stmsToInsertList.size() > 0) {
                try {	
                    if (VT_Utilities.isUpdateable('Study_Team_Member__c')) {	
                        update stmsToInsertList;	
                    }	
                } 
                catch(Exception e) {	
                    ErrorLogUtility.logException(e, null, VT_D2_TNCatalogTasks.class.getName());	
                } 
            }
        }        
    }
    // End of code: Conquerors

}