@IsTest
public with sharing class VT_D1_CustomMultiPicklistControllerTest {
	public static void getSelectOptionsTest(){
    	VTD1_Order__c order = new VTD1_Order__c();
    	List<String> kitTypeList = VT_D1_CustomMultiPicklistController.getSelectOptions(order, 'VTD1_Kit_Type__c');
    	System.assert(kitTypeList.size() > 0);
    	if (kitTypeList.size() > 0){
    		System.assertEquals(true, kitTypeList.contains('IMP/Lab'));
    		//System.assertEquals(true, kitTypeList.contains('Lab'));
    		System.assertEquals(true, kitTypeList.contains('Connected Devices'));
    		System.assertEquals(true, kitTypeList.contains('Study Hub Tablet'));
    		System.assertEquals(true, kitTypeList.contains('Welcome to Study Package'));
    	}
    }

	public static void getSelectOptionsWithLabelTest(){
		VTD1_Order__c order = new VTD1_Order__c();
		Map<String, String> kitTypeList = VT_D1_CustomMultiPicklistController.getSelectOptionsWithLabel(order, 'VTD1_Kit_Type__c');
		System.assert(kitTypeList.size() > 0);
		if (kitTypeList.size() > 0){
			System.assertNotEquals(null, kitTypeList.get('IMP/Lab'));
			//System.assertNotEquals(null, kitTypeList.get('Lab'));
			System.assertNotEquals(null, kitTypeList.get('Connected Devices'));
			System.assertNotEquals(null, kitTypeList.get('Study Hub Tablet'));
			System.assertNotEquals(null, kitTypeList.get('Welcome to Study Package'));
		}
	}
}