/**
 * Created by Anatoliy Kozel on 1/14/2020.
 */

public with sharing class VT_R4_PatientFieldLockService extends Handler implements Database.Batchable<SObject> {

    /**
    * Method to update historical patients which are not in Pre-Consent status but sensitive fields are still updateable.
    */
    public void updateHistoricalData() {
        Database.executeBatch(this);
    }

    /*controller methods*/
    @AuraEnabled
    public static List<Id> verifyLockedFields(Id patientId, Id userId) {
        KeyFieldLockService locker = new KeyFieldLockService(patientId, userId);
        return locker.getLocked();
    }

    @AuraEnabled
    public static List<Id> verifyLockedFields(Id patientId) {
        KeyFieldLockService locker = new KeyFieldLockService(patientId);
        return locker.getLocked();
    }

    @AuraEnabled
    public static List<Id> verifyLockedFields(List<Id> patientIds, Id userId) {
        KeyFieldLockService locker = new KeyFieldLockService(patientIds, userId);
        return locker.getLocked();
    }

    @AuraEnabled
    public static List<Id> verifyLockedFields(List<Id> patientIds) {
        KeyFieldLockService locker = new KeyFieldLockService(patientIds);
        return locker.getLocked();
    }

    /*trigger methods*/
    protected override void onAfterInsert(Handler.TriggerContext context) {
        if (VT_R4_PatientConversion.patientConversionInProgress) {
            return;
        }
        if (this.typeName == Case.class) {
            final KeyFieldLockCaseTriggerHandler handler = new KeyFieldLockCaseTriggerHandler(context);
            handler.handleCaseInsert();
        }
        if (this.typeName == User.class) {
            new UserToContactUsernameSyncService(context).syncUsernamesOnInsert();
        }
    }

    protected override void onBeforeUpdate(Handler.TriggerContext context) {
        if (VT_R4_PatientConversion.patientConversionInProgress) {
            return;
        }
        if (this.typeName == Contact.class) {
            final KeyFieldLockContactTriggerHandler handler = new KeyFieldLockContactTriggerHandler(context);
            handler.handleContactUpdate();
        }
    }

    protected override void onAfterUpdate(Handler.TriggerContext context) {
        if (VT_R4_PatientConversion.patientConversionInProgress) {
            return;
        }
        if (this.typeName == Contact.class) {
            new ContactToUserUsernameSyncService(context).syncUsernames();
        }
        if (this.typeName == Case.class) {
            final KeyFieldLockCaseTriggerHandler handler = new KeyFieldLockCaseTriggerHandler(context);
            handler.handleCaseUpdate();
        }
        if (this.typeName == User.class) {
            new UserChangeEmailConfirmationTriggerHandler(context).sendConfirmationEmails();
            new UserToContactUsernameSyncService(context).syncUsernames();
        }
    }

    /*
        Service classes
     */
    public class KeyFieldLockService {

        private List<Id> patientIds;
        private Id userProfileId;

        public KeyFieldLockService(Id patientId) {
            this(new List<Id>{
                    patientId
            });
        }

        public KeyFieldLockService(List<Id> patientIds) {
            this.patientIds = patientIds;
            this.userProfileId = UserInfo.getProfileId();
        }

        public KeyFieldLockService(Id patientId, Id userId) {
            this(new List<Id>{
                    patientId
            }, userId);
        }

        public KeyFieldLockService(List<Id> patientIds, Id userId) {
            this.patientIds = patientIds;
            if (userId == null) {
                this.userProfileId = UserInfo.getProfileId();
            } else {
                this.userProfileId = [SELECT Profile.Id FROM User WHERE Id = :userId].Profile.Id;
            }
        }

        public List<Id> getLocked() {
            validateInputPatients();
            if (profileEntitled()) {
                return noLockedPatients();
            }

            return getLockedPatients();
        }

        private void validateInputPatients() {
            if (this.patientIds == null || this.patientIds.isEmpty()) {
                throw new BadInputException('Provide at least one Patient ID');
            }
        }

        private Boolean profileEntitled() {
            return new KeyFieldLockProfiles().getEntitledProfiles().contains(this.userProfileId);
        }

        private List<Id> noLockedPatients() {
            return new List<Id>();
        }

        private List<Id> getLockedPatients() {
            Set<Id> result = new QueryBuilder(Contact.getSObjectType())
                    .namedQueryRegister('VT_R4_PatientFieldLockService.getLockedPatients')
                    .addConditions()
                    .add(new QueryBuilder.CompareCondition('VTR4_IsLockedPatient__c').eq(true))
                    .add(new QueryBuilder.InCondition('Id').inCollection(this.patientIds))
                    .setConditionOrder('1 AND 2')
                    .endConditions()
                    .toIdSet();
            return new List<Id>(result);
        }
    }

    private class KeyFieldLockProfiles {

        private Set<Id> getEntitledProfiles() {
            Set<Id> result = new Set<Id>();
            // realize logic which collect all profileIds who can edit fields after Consented status
            result.add('00e1N000001qiJcQAI'); // System Administrator aka Study Admin
            return result;
        }
    }

    /**
     * Class to handle Case records insertion and update.
     * Locks Contact sensitive fields from update depending on related Case status
     * @author Oleksandr Bezzubenko
     * @date 27.01.2020
     */
    private class KeyFieldLockCaseTriggerHandler {

        private final Map<Id, SObject> oldCaseMap;
        private final List<Case> newCaseList;

        public KeyFieldLockCaseTriggerHandler(Handler.TriggerContext context) {
            if (context.oldMap != null) {
                this.oldCaseMap = context.oldMap;
            }
            this.newCaseList = (List<Case>) context.newList;
        }

        /**
        * Handle Case insertion
        */
        public void handleCaseInsert() {
            final List<Id> contactsToLock = new List<Id>();

            for (Case newCase : this.newCaseList) {
                if (newCase.Status != VT_R4_ConstantsHelper_AccountContactCase.CASE_PRE_CONSENT
                        && newCase.ContactId != null) {
                    contactsToLock.add(newCase.ContactId);
                }
            }

            if (!contactsToLock.isEmpty()) {
                this.lockContacts(contactsToLock);
            }
        }

        /**
         * Handle Case update
         */
        public void handleCaseUpdate() {
            List<Id> contactsToLock = new List<Id>();

            for (Case newCase : this.newCaseList) {
                final Case oldCase = (Case) oldCaseMap.get(newCase.Id);

                if (oldCase.Status == VT_R4_ConstantsHelper_AccountContactCase.CASE_PRE_CONSENT
                        && newCase.Status != VT_R4_ConstantsHelper_AccountContactCase.CASE_PRE_CONSENT
                        && newCase.ContactId != null) {
                    contactsToLock.add(newCase.ContactId);
                }
            }

            if (!contactsToLock.isEmpty()) {
                this.lockContacts(contactsToLock);
            }
        }

        /**

         * Lock contacts by setting VTR4_IsLockedPatient__c field value to TRUE
         *
         * @param contactsToLock - verification method for blocking for list to lock
          * lockContactsImmediatly method takes list of contact ids which should be locked
         */


        private void lockContacts(List<Id> contactsToLock) {
            if (System.isBatch() || System.isFuture() || Test.isRunningTest()) {
                lockContactsImmediatly(contactsToLock);
            }
            else {
                lockContactsFuture(contactsToLock);
            }
        }
    }

    @Future
    public static void lockContactsFuture (List<Id> contactsToLock) {
        lockContactsImmediatly(contactsToLock);
    }

    public static void lockContactsImmediatly(List<Id> contactsToLock) {
        final List<Contact> contactsToUpdate = [
                SELECT Id, VTR4_IsLockedPatient__c
                FROM Contact
                WHERE Id IN :contactsToLock
        ];
        for (Contact contact : contactsToUpdate) {
            contact.VTR4_IsLockedPatient__c = true;
        }
        update contactsToUpdate;
    }

    /**
     * Class to handle Contact records update.
     * Locks Contact sensitive fields from update depending on related Case status
     * @author Oleksandr Bezzubenko
     * @date 27.01.2020
     */
    private class KeyFieldLockContactTriggerHandler {

        private final List<SObjectField> restrictedFields;
        private final Map<Id, SObject> oldContactMap;
        private final List<Contact> newContactList;
        private final List<Id> lockedContacts;

        public KeyFieldLockContactTriggerHandler(Handler.TriggerContext context) {
            this.restrictedFields = new List<SObjectField>{
                    Contact.Salutation,
                    Contact.FirstName,
                    Contact.LastName,
                    Contact.HealthCloudGA__Gender__c,
                    Contact.Birthdate,
                    Contact.VTD1_MiddleName__c
            };
            this.oldContactMap = context.oldMap;
            this.newContactList = (List<Contact>) context.newList;
            this.lockedContacts = verifyLockedFields(new List<Id>(context.newMap.keySet()));
        }

        /**
         * Handle Contact update
         */
        public void handleContactUpdate() {
            for (Contact newContact : this.newContactList) {
                final Contact oldContact = (Contact) oldContactMap.get(newContact.Id);
                final List<SObjectField> restrictedFieldsToUpdate = this.getChangedRestrictedFields(oldContact, newContact);
                if (this.lockedContacts.contains(newContact.Id) && !restrictedFieldsToUpdate.isEmpty()) {
                    final String errorMessage = this.generateErrorMessage(restrictedFieldsToUpdate);
                    newContact.addError(errorMessage);
                }
            }
        }

        /**
         * Get list of sensitive fields which are attempted to be updated
         *
         * @param oldContact - old contact value receiver from trigger
         * @param newContact - new contact value receiver from trigger
         *
         * @return list SObjectField
         */
        private List<SObjectField> getChangedRestrictedFields(Contact oldContact, Contact newContact) {
            final List<SObjectField> restrictedFieldsToUpdate = new List<SObjectField>();

            for (SObjectField field : this.restrictedFields) {
                if (newContact.get(field) != oldContact.get(field)) {
                    restrictedFieldsToUpdate.add(field);
                }
            }

            return restrictedFieldsToUpdate;
        }

        /**
         * Generate error message based on sensitive fields list
         *
         * @param restrictedFields list of sensitive fields which should not be updated
         *
         * @return error message
         */
        private String generateErrorMessage(List<SObjectField> restrictedFields) {
            String baseErrorMessage;
            final List<String> arguments = new List<String>();

            if (restrictedFields.size() > 1) {
                baseErrorMessage = Label.VTR4_LockedForEditingVariant2;
                final Integer lastItemIndex = restrictedFields.size() - 1;
                String param1 = '';
                final String param2 = restrictedFields[lastItemIndex].getDescribe().getLabel();
                List<String> param1List = new List<String>();
                for (Integer i = 0; i < lastItemIndex; i++) {
                    param1List.add(restrictedFields[i].getDescribe().getLabel());
                }
                param1 = String.join(param1List, ', ');
                arguments.add(param1);
                arguments.add(param2);
            } else {
                baseErrorMessage = Label.VTR4_LockedForEditingVariant1;
                arguments.add(restrictedFields[0].getDescribe().getLabel());
            }

            return String.format(baseErrorMessage, arguments);
        }
    }

    /**
     * Class to send emails for notification Patients that them email was changed.
     * @author Igor Semenko
     * @date 27.01.2020
     */
    private class UserChangeEmailConfirmationTriggerHandler {

        private List<User> newUsers;
        private final Map<Id, SObject> oldUsers;
        private Map<Id, Account> caregivers = new Map<Id, Account>();
        private Map<Id, User> userProfiles = new Map<Id, User>();

        /**
         * Constructor of change email sender.
         *
         * @param context - User trigger context
         */
        public UserChangeEmailConfirmationTriggerHandler(Handler.TriggerContext context) {
            this.newUsers = context.newList;
            this.oldUsers = context.oldMap;
            List<Id> usersId = new List<Id>();
            for (User us :this.newUsers){
                usersId.add(us.Id);
            }
            List<User> userAccounts = [SELECT Id, Email, ContactId, Contact.VTD1_Account__c, AccountId, Profile.Name FROM User WHERE Id IN :usersId];

            List<Id> accountsId = new List<Id>();
            for (User u : userAccounts){
                this.userProfiles.put(u.Id, u);
                accountsId.add(u.AccountId);
            }
            List<Account> accounts = [SELECT Id, Name, (SELECT Id, Email, VTD1_Primary_CG__c, VTD1_UserId__c FROM Caregivers__r) FROM Account WHERE Id IN : accountsId];
            for (User u : userAccounts){
                for (Account a : accounts){
                    if (u.AccountId == a.Id) {
                        this.caregivers.put(u.Id, a);
                    }
                }
            }
        }

        /**
         * Check state of trigger from metadata and search users with new emails.
         */
        public void sendConfirmationEmails() {
            if (!this.canSendConfirmationEmail()) {
                System.debug('Sending confirmation email is disabled');
                return;
            }
            final Map<User, User> oldUserByNewUsers = this.getChangedUsers();
            if (oldUserByNewUsers.isEmpty()) {
                System.debug('No users\' emails changed');
                return;
            }
            this.sendConfirmationEmails(oldUserByNewUsers);
        }
        
        private Boolean canSendConfirmationEmail() {
            final VTD1_General_Settings__mdt triggerState = [SELECT VTR4_Trigger_Change_Email__c FROM VTD1_General_Settings__mdt];
            return triggerState.VTR4_Trigger_Change_Email__c;
        }

        private Map<User, User> getChangedUsers() {
            final Map<User, User> oldUserByNewUsers = new Map<User, User>();
            for (User newUser : this.newUsers) {
                final User oldUser = (User) this.oldUsers.get(newUser.Id);
                if (newUser.Email == oldUser.Email) {
                    continue;
                }
                oldUserByNewUsers.put(newUser, oldUser);
            }
            return oldUserByNewUsers;
        }

        /**
         * Generate emails for Patient and Patient by conditions.
         *
         * @param oldUserByNewUsers - Map of Users with old and new emails from trigger.
         */
        private void sendConfirmationEmails(Map<User, User> oldUserByNewUsers) {
            final List<VT_R3_EmailsSender.ParamsHolder> emails = new List<VT_R3_EmailsSender.ParamsHolder>();

            for (User newUser : oldUserByNewUsers.keySet()) {
                String userProfile = this.userProfiles.get(newUser.Id).Profile.Name;
                if ((userProfile != VT_D1_ConstantsHelper.PATIENT_PROFILE_NAME) &&
                        (userProfile != VT_D1_ConstantsHelper.CAREGIVER_PROFILE_NAME)) {
                                continue;
                }
                final User oldUser = oldUserByNewUsers.get(newUser);

                if ((caregivers.size() > 0) &&
                                !this.caregivers.get(newUser.Id).Caregivers__r.isEmpty() &&
                                (userProfile != 'Caregiver')) {
                    if (this.caregivers.get(newUser.Id).Caregivers__r.size() > 1 ){
                        for (Integer i = 0; i < this.caregivers.get(newUser.Id).Caregivers__r.size(); i++){
                            if (this.caregivers.get(newUser.Id).Caregivers__r[i].VTD1_Primary_CG__c){
                                emails.add(this.createHolder(this.caregivers.get(newUser.Id).Caregivers__r[i].VTD1_UserId__c,  this.caregivers.get(newUser.Id).Caregivers__r[i].Id, 'Caregiver_Update_Patient_s_Emails', this.caregivers.get(newUser.Id).Caregivers__r[i].Email));
                            }
                        }
                    } else {
                        emails.add(this.createHolder(this.caregivers.get(newUser.Id).Caregivers__r[0].VTD1_UserId__c,  this.caregivers.get(newUser.Id).Caregivers__r[0].Id, 'Caregiver_Update_Patient_s_Emails', this.caregivers.get(newUser.Id).Caregivers__r[0].Email));
                    }
                }
                emails.add(this.createHolder(newUser.Id, newUser.ContactId, 'Patient_Caregiver_Update_Own_Emails', newUser.Email));
                emails.add(this.createHolder(oldUser.Id, oldUser.ContactId, 'Patient_Caregiver_Update_Own_Emails', oldUser.Email));
            }
            VT_R3_EmailsSender.sendEmails(emails);
        }

        private VT_R3_EmailsSender.ParamsHolder createHolder(Id user, Id contactId, String templateName, String email) {
            return new VT_R3_EmailsSender.ParamsHolder(
                    user, contactId, templateName, email, true
            );
        }
    }

    public class ContactToUserUsernameSyncService implements Queueable {

        private final List<Contact> newContacts;
        private final Map<Id, SObject> oldContacts;
        private List<User> usersToUpdate;

        public ContactToUserUsernameSyncService(Handler.TriggerContext context) {
            this.newContacts = context.newList;
            this.oldContacts = context.oldMap;
        }

        public void syncUsernames() {
            final Map<Id, Contact> changedContacts = this.getChangedContacts();
            if (changedContacts.isEmpty()) {
                return;
            }

            final List<User> relatedUsers = this.getRelatedUsers(changedContacts);
            this.usersToUpdate = this.modifyUsersUsername(relatedUsers, changedContacts);

            //perform async update to eliminate mixed dml
            System.enqueueJob(this);
        }

        /*update*/
        private Map<Id, Contact> getChangedContacts() {
            final Map<Id, Contact> result = new Map<Id, Contact>();
            for (Contact newContact : this.newContacts) {
                final Contact oldContact = (Contact) this.oldContacts.get(newContact.Id);
                if (newContact.VTR4_Username__c != oldContact.VTR4_Username__c) {
                    result.put(newContact.Id, newContact);
                }
            }
            return result;
        }

        private List<User> getRelatedUsers(Map<Id, Contact> changedContacts) {
            return [
                    SELECT Id, Username, ContactId
                    FROM User
                    WHERE ContactId IN :changedContacts.keySet()
            ];
        }

        private List<User> modifyUsersUsername(List<User> users, Map<Id, Contact> changedContacts) {
            List<User> result = new List<User>();
            for (User user : users) {
                final Contact contact = changedContacts.get(user.ContactId);
                if (user.Username != contact.VTR4_Username__c) {
                    result.add(new User(Id = user.Id, Username = contact.VTR4_Username__c));
                }
            }
            return result;
        }

        public void execute(QueueableContext param1) {
            update this.usersToUpdate;
        }
    }

    public class UserToContactUsernameSyncService implements Queueable {

        private final List<User> newUsers;
        private final Map<Id, SObject> oldUsers;
        private List<Contact> changingContacts;

        public UserToContactUsernameSyncService(Handler.TriggerContext context) {
            this.newUsers = context.newList;
            this.oldUsers = context.oldMap;
        }

        public void syncUsernames() {
            final Map<Id, User> changedUsers = this.getChangedUsers();
            if (changedUsers.isEmpty()) {
                return;
            }

            this.changingContacts = this.getChangingContacts(changedUsers);

            System.enqueueJob(this);
        }

        public void syncUsernamesOnInsert() {
            if (!VT_R4_PatientConversion.patientConversionInProgress) {
                final List<User> usersWithContact = this.getUsersWithContact();
                if (usersWithContact.isEmpty()) {
                    return;
                }

                this.changingContacts = this.getChangingContacts(usersWithContact);

                System.enqueueJob(this);
            }
        }

        /*update*/
        private Map<Id, User> getChangedUsers() {
            final Map<Id, User> result = new Map<Id, User>();
            for (User newUser : this.newUsers) {
                final User oldUser = (User) this.oldUsers.get(newUser.Id);
                if (newUser.Username != oldUser.Username) {
                    result.put(newUser.Id, newUser);
                }
            }
            return result;
        }

        private List<Contact> getChangingContacts(Map<Id, User> changedUsers) {
            List<Contact> result = new List<Contact>();
            List<User> usersWithContact = [
                    SELECT Id, Username, ContactId, Contact.VTR4_Username__c
                    FROM User
                    WHERE Id IN :changedUsers.keySet()
                        AND ContactId != null
            ];
            for (User userWithContact : usersWithContact) {
                if (userWithContact.Username != userWithContact.Contact.VTR4_Username__c) {
                    result.add(new Contact(Id = userWithContact.ContactId, VTR4_Username__c = userWithContact.Username));
                }
            }
            return result;
        }

        public void execute(QueueableContext param1) {
            update this.changingContacts;
        }

        /*insert*/
        private List<User> getUsersWithContact() {
            final List<User> result = new List<User>();
            for (User newUser : this.newUsers) {
                if (String.isNotEmpty(newUser.ContactId)) {
                    result.add(newUser);
                }
            }
            return result;
        }

        private List<Contact> getChangingContacts(List<User> newUsers) {
            List<Contact> result = new List<Contact>();
            for (User newUser : newUsers) {
                result.add(new Contact(Id = newUser.ContactId, VTR4_Username__c = newUser.Username));
            }
            return result;
        }
    }

    private class BadInputException extends Exception {

    }

    /**
     * Batch public interface
     *
     * @param param1
     * @param param2
     */
    public Iterable<SObject> start(Database.BatchableContext param1) {
        QueryBuilder builder = new QueryBuilder(Contact.class)
                .addField('VTR4_IsLockedPatient__c ')
                .addConditionsWithOrder('1 AND 2 AND 3')
                .add(new QueryBuilder.CompareCondition('VTR4_IsLockedPatient__c').eq(false))
                .add(new QueryBuilder.NullCondition('VTD1_Clinical_Study_Membership__c').notNull())
                .add(new QueryBuilder.CompareCondition('VTD1_Clinical_Study_Membership__r.Status').ne(VT_R4_ConstantsHelper_AccountContactCase.CASE_PRE_CONSENT))
                .endConditions();
        return builder.toQueryLocator();
    }

    public void execute(Database.BatchableContext context, List<SObject> scope) {
        for (Contact contact : (List<Contact>) scope) {
            contact.VTR4_IsLockedPatient__c = true;
        }

        update scope;
    }

    public void finish(Database.BatchableContext param1) {
    }
}