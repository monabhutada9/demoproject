/**


 * Created by Bobich Viktar on 28-Oct-20.
 */
/**
	* @description Create time line visit (used QAction)
	* @author Viktar Bobich
	* @param Map<Id, List<VTD1_ProtocolVisit__c>> protocolVisitsByCaseIdMap
	* @date 04/11/2020
	* @issue SH-18596
	* @test VT_R5_AdditionalVisitsTest
	*/




public class VT_R5_QAction_AdditionalVisits extends VT_D1_AbstractAction{
	private Map<Id, List<VTD1_ProtocolVisit__c>> protocolVisitsByCaseIdMap;
	private Map<Id, Case> caseByIdMap;
	private static final String DEFAULT_MODALITY = 'At Home';
	private static final Map<String, String> PROTOCOL_TO_ACTUAL_VISIT_TYPES = new Map<String, String>{
			VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_LABS => VT_R4_ConstantsHelper_VisitsEvents.RT_ACTUAL_VISIT_LABS,
			VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_STUDY_TEAM => VT_R4_ConstantsHelper_VisitsEvents.RT_ACTUAL_VISIT_STUDY_TEAM,
			VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_HCP => VT_R4_ConstantsHelper_VisitsEvents.RT_ACTUAL_VISIT_HCP
	};

	public override void execute() {
		List<VTD1_Actual_Visit__c> actualVisitForCreate = new List<VTD1_Actual_Visit__c>();

		Set<String> offsetFromFieldApiNameSet = new Set<String>{'Id', 'Preferred_Lab_Visit__c', 'ContactId', 'VTR5_Day_1_Dose__c'};

		Set<Id> caseIdSet = protocolVisitsByCaseIdMap.keySet();
		for(List<VTD1_ProtocolVisit__c> protocolVisitList : protocolVisitsByCaseIdMap.values()){
			for(VTD1_ProtocolVisit__c protocolVisit : protocolVisitList){
				if(protocolVisit.VTR5_OffsetFrom__r.VTR5_FieldAPIName__c != null){
					offsetFromFieldApiNameSet.add(protocolVisit.VTR5_OffsetFrom__r.VTR5_FieldAPIName__c);
				}
			}
		}
		String queryString = 	'SELECT ' +
								+ offsetFromFieldApiNameSet.toString().remove('{').remove('}')+
								' FROM Case ' +
								'WHERE Id IN : caseIdSet';
		this.caseByIdMap = new Map<Id, Case>((List<Case>)Database.query(queryString));

		Map<String, Schema.RecordTypeInfo> recTypeMap = Schema.SObjectType.VTD1_Actual_Visit__c.getRecordTypeInfosByDeveloperName();

		Map<Id, VTD1_Actual_Visit__c> existingVisitMap = new Map<Id, VTD1_Actual_Visit__c>();
		existingVisitMap = getExistingVisitMap();

		for(Id patientCaseId : protocolVisitsByCaseIdMap.keySet()){
			for(VTD1_ProtocolVisit__c protocolVisit : protocolVisitsByCaseIdMap.get(patientCaseId)){
				Date visitOffsetDate;
				if(protocolVisit.VTR5_OffsetFrom__r.VTR5_FieldAPIName__c != null){
					visitOffsetDate = Date.valueOf(caseByIdMap.get(patientCaseId).get(protocolVisit.VTR5_OffsetFrom__r.VTR5_FieldAPIName__c));
				}else{
					visitOffsetDate = Date.valueOf(caseByIdMap.get(patientCaseId).VTR5_Day_1_Dose__c);
				}
				if (existingVisitMap.containsKey(patientCaseId) && existingVisitMap.get(patientCaseId).VTD1_Protocol_Visit__c == protocolVisit.Id) {
					existingVisitMap.get(patientCaseId).VTR5_OffsetDate__c = visitOffsetDate;
					existingVisitMap.get(patientCaseId).Name = protocolVisit.VTD1_EDC_Name__c;
					actualVisitForCreate.add(existingVisitMap.get(patientCaseId));
				} else {

				actualVisitForCreate.add(new VTD1_Actual_Visit__c(
						Name = protocolVisit.VTD1_EDC_Name__c,
						RecordTypeId = recTypeMap.get(PROTOCOL_TO_ACTUAL_VISIT_TYPES.get(protocolVisit.VTD1_VisitType__c)).getRecordTypeId(),
						Sub_Type__c = protocolVisit.VTD1_VisitType__c == 'Labs' ? caseByIdMap.get(patientCaseId).Preferred_Lab_Visit__c : null,
						VTD1_Case__c = caseByIdMap.get(patientCaseId).Id,
						VTD1_Contact__c = caseByIdMap.get(patientCaseId).ContactId,
						VTD1_Protocol_Visit__c = protocolVisit.Id,
						VTD1_Pre_Visit_Instructions__c = protocolVisit.VTD1_PreVisitInstructions__c,
						VTR2_Independent_Rater_Flag__c = protocolVisit.VTD1_Independent_Rater_Flag__c,
						VTD1_Additional_Patient_Visit_Checklist__c = protocolVisit.VTD1_Patient_Visit_Checklist__c,
						VTD1_Additional_Visit_Checklist__c = protocolVisit.VTD1_Visit_Checklist__c,
						VTR2_Televisit__c = protocolVisit.VTR2_SH_TelevisitNeeded__c,

							VTR2_Modality__c = protocolVisit.VTR2_Modality__c != null ? protocolVisit.VTR2_Modality__c : DEFAULT_MODALITY,
							VTR5_OffsetDate__c = visitOffsetDate
					));
				}
			}
		}
		upsert actualVisitForCreate;

		if(!actualVisitForCreate.isEmpty()) {
			Map<Id, VTD1_Actual_Visit__c> visitByIdMap = new Map<Id, VTD1_Actual_Visit__c>(actualVisitForCreate);
			actualVisitForCreate = [
					SELECT Id,
							VTD1_Visit_Number__c,
							VTD2_Baseline_Visit__c,
							VTD1_Schedule_Visit_Task_Date__c,
							VTD1_Visit_Type__c,
							VTD2_Schedule_Notification_Time_Formula__c,
							To_Be_Scheduled_Date__c,
							VTR2_ScheduleVisitTaskPatient__c,
							VTR2_f_ScheduleNotificationPT__c,
							Sub_Type__c,
							VTD1_Case__c
					FROM VTD1_Actual_Visit__c
					WHERE Id IN :visitByIdMap.keySet()
			];
			VTR5_ScheduleVisitsTaskAndNotification.scheduleActualVisits(actualVisitForCreate);
			update actualVisitForCreate;
		}
	}


	public Map<Id, VTD1_Actual_Visit__c> getExistingVisitMap(){
		Map<Id, VTD1_Actual_Visit__c>existingVisitBaCaseIdMap = new Map<Id, VTD1_Actual_Visit__c>();
		Map<Id, Id>caseIdByProtocolVisitId = new Map<Id, Id>();
		for(Id caseId : protocolVisitsByCaseIdMap.keySet()){
			for(VTD1_ProtocolVisit__c protocolVisit : protocolVisitsByCaseIdMap.get(caseId)){
				caseIdByProtocolVisitId.put(protocolVisit.Id, caseId);
			}
		}
		List<VTD1_Actual_Visit__c> existingVisitList = [
					SELECT 	Id,
							VTD1_Protocol_Visit__c,
							VTD1_Case__c
					FROM VTD1_Actual_Visit__c
					WHERE VTD1_Case__c IN :caseIdByProtocolVisitId.values()
						AND VTD1_Protocol_Visit__c IN :caseIdByProtocolVisitId.keySet()];

		for(VTD1_Actual_Visit__c actualVisit : existingVisitList){
			if(caseIdByProtocolVisitId.get(actualVisit.VTD1_Protocol_Visit__c) == actualVisit.VTD1_Case__c){
				existingVisitBaCaseIdMap.put(actualVisit.VTD1_Case__c, actualVisit);
			}
		}
		return existingVisitBaCaseIdMap;
	}


	public override Type getType() {
		return VT_R5_QAction_AdditionalVisits.class;
	}
}