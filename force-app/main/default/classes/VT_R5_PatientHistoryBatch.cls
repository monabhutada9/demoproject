/**
 * Created by Andrey Pivovarov on 7/9/2020.
 * Transfers Patient Field History records from VTR6_PatientFieldHistoryTemporary__c to VTR5_Patient_Field_History__b
 * @see VT_R2_PatientHistoryHandlerTest
 */

global without sharing class VT_R5_PatientHistoryBatch implements Database.Batchable<SObject> {

//    private List<VTR5_PatientFieldHistory__b> pfh = new List<VTR5_PatientFieldHistory__b>();
//    private Set<Id> processedIds = new Set<Id>();

    global Iterable<SObject> start(Database.BatchableContext BC) {
        return [
                SELECT Id, VTR6_ObjectType__c, VTR6_Object_Id__c, VTR6_Field__c, VTR6_OldValue__c, VTR6_NewValue__c,
                        VTR6_OperationType__c, VTR6_PatientId__c, VTR6_IsDeleted__c, VTR6_DateofChange__c, VTR6_Hash__c, VTR6_UserId__c, CreatedById
                FROM VTR6_PatientFieldHistoryTemporary__c
        ];
    }

    global void execute(Database.BatchableContext BC, List<VTR6_PatientFieldHistoryTemporary__c> scope) {
        if (scope == null || scope.isEmpty()) return;
        List<VTR5_PatientFieldHistory__b> pfh = new List<VTR5_PatientFieldHistory__b>();
        Set<Id> processedIds = new Set<Id>();
        for (VTR6_PatientFieldHistoryTemporary__c pfht : scope) {
            if (pfht.VTR6_PatientId__c != null && pfht.VTR6_DateofChange__c != null && pfht.VTR6_Hash__c != null) {
                pfh.add(new VTR5_PatientFieldHistory__b(
                        VTR5_Object_Type__c = pfht.VTR6_ObjectType__c,
                        VTR5_Object_Id__c = pfht.VTR6_Object_Id__c,
                        VTR5_Field__c = pfht.VTR6_Field__c,
                        VTR5_Old_Value__c = pfht.VTR6_OldValue__c,
                        VTR5_New_Value__c = pfht.VTR6_NewValue__c,
                        VTR5_Operation_Type__c = pfht.VTR6_OperationType__c,
                        VTR5_Patient_Id__c = pfht.VTR6_PatientId__c,
                        VTR5_Is_Deleted__c = String.valueOf(pfht.VTR6_IsDeleted__c),
                        VTR5_Date__c = pfht.VTR6_DateofChange__c,
                        VTR5_Hash__c = pfht.VTR6_Hash__c,
                        VTR5_User_Id__c = pfht.VTR6_UserId__c!=null? pfht.VTR6_UserId__c : pfht.CreatedById
                ));
            }
            processedIds.add(pfht.Id);
        }
        Boolean hasErrors = false;
        try {
            if (!Test.isRunningTest()) {
                Database.SaveResult[] srList = Database.insertImmediate(pfh);
                for (Database.SaveResult sr : srList) {
                    if (!sr.isSuccess()) {
                        hasErrors = true;
                        for (Database.Error err : sr.getErrors()) {
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Fields that affected this error: ' + err.getFields());
                        }
                    }
                }
            }
        } catch (Exception e) {
            hasErrors = true;
        }
        if (!hasErrors && !processedIds.isEmpty()) {
            Database.delete(new List<Id>(processedIds), false);
        }
    }

    global void finish(Database.BatchableContext BC) {
//        System.debug('+++processedIds '+processedIds.size());
//        Boolean hasErrors = false;
//        if (!Test.isRunningTest()) {
//            Database.SaveResult[] srList = Database.insertImmediate(pfh);
//            for (Database.SaveResult sr : srList) {
//                if (!sr.isSuccess()) {
//                    hasErrors = true;
//                    for (Database.Error err : sr.getErrors()) {
//                        System.debug('The following error has occurred:');
//                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
//                        System.debug('Fields that affected this error: ' + err.getFields());
//                    }
//                }
//            }
//        }
//        if (!hasErrors) {
//            System.debug('+++Deleting '+processedIds.size());
//            Database.delete(new List<Id>(processedIds), false);
//        }
    }
}