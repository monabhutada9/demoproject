/**
* @author: Carl Judge
* @date: 12-Sep-18
* @description: 
**/

@IsTest
private class VT_D1_SiteMetricsPatientsByStatus_Test {
/*
    @testSetup
    private static void setupMethod() {


        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.startTest();
        User priPG = [SELECT Id, ContactId, IsActive, Profile.Name FROM User WHERE FirstName = 'PrimaryPGUser1'];
        User secPG = [SELECT Id, ContactId, IsActive FROM User WHERE FirstName = 'SecondaryPGUser1'];
        User priPI = [SELECT Id, ContactId, IsActive, Profile.Name FROM User WHERE FirstName = 'PIUser1'];
        User secPI = [SELECT Id, ContactId FROM User WHERE FirstName = 'PIBackupUser1'];
        //priPI.IsActive = true;
        //update priPI;
        //Database.insert(priPI);
        System.debug('priPG]=' + priPG);
        VT_D1_TestUtils.createTestPatientCandidate(1);


        User patientUser = [
            SELECT Name, Profile.Name, ContactId, Contact.AccountId,  Contact.Account.Candidate_Patient__c,
                Contact.Account.Candidate_Patient__r.HealthCloudGA__Address1Country__c, IsActive
            FROM User
            ORDER BY CreatedDate DESC LIMIT 1
        ];
        User patient12 = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Primary Investigator' AND ContactId != null AND IsActive = true LIMIT 1];
        VTD1_Patient__c patient = new VTD1_Patient__c(VTD1_Account__c = patientUser.Contact.AccountId, VTD1_Contact__c = patientUser.ContactId);
        insert patient;
        system.debug('patient12' + patient12);
        system.debug('priPI' + priPI);

        Study_Team_Member__c pi = [
                SELECT Id,User__r.FirstName,Study_Team_Member__c
                FROM Study_Team_Member__c
                WHERE Study__c = :study.Id AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName = 'PIUser1'
        ];

        Study_Team_Member__c pi2 = [
                SELECT Id,User__r.FirstName,Study_Team_Member__c
                FROM Study_Team_Member__c
                WHERE Study__c = :study.Id AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName = 'PIBackupUser1'
        ];
        pi.VTR2_Remote_CRA__c = pi2.Id;
        pi.VTR2_Onsite_CRA__c = pi2.Id;
        update pi;

        Virtual_Site__c vSite = new Virtual_Site__c(
                VTD1_Study__c = study.Id,
                VTD1_Study_Team_Member__c = [SELECT Id FROM Study_Team_Member__c WHERE VTR2_Remote_CRA__c != null LIMIT 1].Id
        );
        insert vSite;
*/
        /*Case caseObj = new Case (
                Status = 'Open',
                VTD1_PCF_Safety_Concern_Indicator__c = 'No',
                ContactId = patient12.ContactId,
                VTD1_Subject_ID__c = '1111'
                //VTD1_Study__c = study.Id
                //RecordTypeId=VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN
        );
        insert caseObj;*/

        //Test.stopTest();

       /* Case cases = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        system.debug('1111=' + cases);
        cases.VTD1_Study__c = study.Id;
        cases.ContactId = patientUser.ContactId;
        cases.VTD1_Virtual_Site__c = vSite.Id;
        cases.VTD1_PI_user__c = priPI.Id;
        update cases;
        system.debug('2222=' + cases);*/
/*    }


    @isTest
    private static void firstTest() {
        String CRON_EXP = '0 0 0 15 3 ? *';
        Test.startTest();
        //System.schedule('SiteMetrics',  CRON_EXP, new VT_D1_SiteMetricsPatientsByStatusBatch());
        VT_D1_SiteMetricsPatientsByStatusBatch b = new VT_D1_SiteMetricsPatientsByStatusBatch();
        Id batchId = Database.executeBatch(b);
        Test.stopTest();
    }
*/
}