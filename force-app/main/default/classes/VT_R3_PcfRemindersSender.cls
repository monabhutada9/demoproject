/**
 * Created by Dmitry Yartsev on 28.08.2019.
 */
@RestResource(UrlMapping='/PcfRemindersSender/DeleteInterviews')
global without sharing class VT_R3_PcfRemindersSender {
    private static final String FLOW_CURRENT_ELEMENT_NAME = 'PCF_SC_Reminder_Sender_Wait';
    private static final String REMINDER_TYPE = 'PCF Safety Concern';
    private static final Set<String> PCF_OWNER_PROFILES = new Set<String>{
            VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME,
            VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME,
            VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME
    };
    private static final Map<Boolean, Map<String, String>> processTypesMap = new Map<Boolean, Map<String, String>>{
            true => new Map<String, String>{
                    VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME => 'Escalated_PG',
                    VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME => 'Escalated_PI',
                    VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME => 'Escalated_SCR',
                    'main' => 'Escalated_Main'
            },
            false => new Map<String, String>{
                    VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME => 'Non_Escalated_PG',
                    VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME => 'Non_Escalated_PI',
                    VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME => 'Non_Escalated_SCR',
                    'main' => 'Non_Escalated_Main'
            }
    };
    private static Set<String> PG_ROLES = new Set<String> {
            'PG',
            'Backup_PG'
    };
    private static Set<String> SCR_ROLES = new Set<String> {
            'SCR',
            'Backup_SCR'
    };
    private static Map<String, Map<String, Map<Integer, Set<VTR2_Reminders__mdt>>>> allRemindersMap = new Map<String, Map<String, Map<Integer, Set<VTR2_Reminders__mdt>>>>();
    // notifications/posts to chatter
    private static List<String> tnCatalogNotificationCodes;
    private static List<Id> sourceNotificationIds;
    private static Map<String, String> subjectParams;
    private static Map<String, String> linkToRelatedEventMap;
    private static Map<Id, Map<String, String>> messageParams;
    private static Map<String, List<Id>> receiversMap;
    // emails
    private static Map<Id, Map<String, List<Id>>> emailsMap;
    private static Set<String> allTemlatesNames;
    public class ParamsHolder {
        @InvocableVariable(Label = 'Contact Form Id' Required = true)
        public Id pcfId;
        @InvocableVariable(Label = 'Date From Flow' Required = false)
        public Datetime dateFromFlow;
        public ParamsHolder(Id pcfId, Datetime dateFromFlow) {
            this.pcfId = pcfId;
            this.dateFromFlow = dateFromFlow;
        }
        public ParamsHolder(Id pcfId) {
            this.pcfId = pcfId;
        }
        public ParamsHolder() {}
    }
    /**
     * Main method for send PCF Safety Concern reminders
     *
     * @param params A list of ParamsHolder
     *
     * @see ParamsHolder
     */
    @InvocableMethod
    public static void sendReminders (List <ParamsHolder> params) {
        Datetime dt = Datetime.now();
        tnCatalogNotificationCodes = new List<String>();
        sourceNotificationIds = new List<Id>();
        subjectParams = new Map<String, String>();
        linkToRelatedEventMap = new Map<String, String>();
        messageParams = new Map<Id, Map<String, String>>();
        receiversMap = new Map<String, List<Id>>();
        emailsMap = new Map<Id, Map<String, List<Id>>>();
        allTemlatesNames = new Set<String>();
        Map <Id,Datetime> pcfIdsDateTime = new Map <Id,Datetime>();
        for (ParamsHolder holder : params) {
            pcfIdsDateTime.put(holder.pcfId, holder.dateFromFlow != null ? holder.dateFromFlow : dt);
        }
        getAllRemindersMap();
        // flow interviews
        List<FlowInterview> interviewsToDelete = [SELECT Id, InterviewLabel FROM FlowInterview WHERE InterviewLabel IN :pcfIdsDateTime.keySet() AND CurrentElement = :FLOW_CURRENT_ELEMENT_NAME];
        List <Case> pcfList = [
                SELECT Id, CaseNumber,
                        IsEscalated, Status,
                        OwnerId, Owner.Name, Owner.Profile.Name,
                        VTD1_PCF_Safety_Concern_Indicator__c,
                        VTD2_PCF_Safety_Concern_Indication_Date__c,
                        VTR3_PCF_Assignment_Date__c,
                        VTD1_IsEscalatedDateTime__c,
                        VTD1_Study__r.VTD1_Protocol_Nickname__c,
                        VTD2_Study_Geography__r.VTR5_Primary_VTSL__c,
                        VTD1_Clinical_Study_Membership__c,
                        VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR3_Study_Geography__r.VTR5_Primary_VTSL__c,
                        VTD1_Clinical_Study_Membership__r.VTR2_PTNameSubId__c,
                        VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c,
                        VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c,
                        VTD1_Clinical_Study_Membership__r.VTD1_Secondary_PG__c,
                        VTD1_Clinical_Study_Membership__r.VTD1_PI_user__c,
                        VTD1_Clinical_Study_Membership__r.VTD1_Backup_PI_User__c,
                        VTD1_Clinical_Study_Membership__r.VTR2_SiteCoordinator__c,
                        VTD1_Clinical_Study_Membership__r.VTR2_Backup_Site_Coordinator__c,
                        VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c,
                        VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTD2_TMA_Queue_ID__c
                FROM Case
                WHERE Id IN :pcfIdsDateTime.keySet() AND Status = 'Open' AND OwnerId != NULL
                AND VTD1_PCF_Safety_Concern_Indicator__c = 'Possible' AND VTD1_Clinical_Study_Membership__c != NULL
                AND RecordTypeId = :VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF AND VTD2_PCF_Safety_Concern_Indication_Date__c != NULL
                AND Owner.Profile.Name IN :PCF_OWNER_PROFILES
        ];
        for (Case pcf : pcfList) {
            if (pcf.VTR3_PCF_Assignment_Date__c == null) {
                pcf.VTR3_PCF_Assignment_Date__c = pcf.VTD2_PCF_Safety_Concern_Indication_Date__c;
            }
            String ownerBasedProcessType = processTypesMap.get(pcf.IsEscalated).get(pcf.Owner.Profile.Name);
            String mainProcessType = processTypesMap.get(pcf.IsEscalated).get('main');
            Datetime ownerBasedRemindersBaseTime;
            if (pcf.Owner.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) {
                ownerBasedRemindersBaseTime = pcf.VTR3_PCF_Assignment_Date__c;
            } else {
                if (pcf.isEscalated) {
                    ownerBasedRemindersBaseTime = pcf.VTD1_IsEscalatedDateTime__c;
                } else {
                    ownerBasedRemindersBaseTime = pcf.VTD2_PCF_Safety_Concern_Indication_Date__c;
                }
            }
            Integer mainRemindersHour = (Integer)((pcfIdsDateTime.get(pcf.Id).getTime() - pcf.VTD2_PCF_Safety_Concern_Indication_Date__c.getTime()) /  (1000 * 60 * 60));
            Boolean justScheduleMainReminders = Math.mod(((Integer)((pcfIdsDateTime.get(pcf.Id).getTime() - pcf.VTD2_PCF_Safety_Concern_Indication_Date__c.getTime()) / 1000)), (60 * 60)) > 0;
            Integer ownerBasedRemindersHour = (Integer)((pcfIdsDateTime.get(pcf.Id).getTime() - ownerBasedRemindersBaseTime.getTime()) / (1000 * 60 * 60));
            Boolean justScheduleOwnerBasedReminders = Math.mod(((Integer)((pcfIdsDateTime.get(pcf.Id).getTime() - ownerBasedRemindersBaseTime.getTime()) / 1000)), (60 * 60)) > 0;
            messageParams.put(pcf.Id, new Map<String, String>());
            emailsMap.put(pcf.Id, new Map<String, List<Id>>());
            if (!justScheduleMainReminders) {
                processReminders(pcf, mainRemindersHour, mainProcessType, 'main');
            }
            if (!justScheduleOwnerBasedReminders) {
                processReminders(pcf, ownerBasedRemindersHour, ownerBasedProcessType, 'ownerBased');
            }
            Datetime nextMainRemindersDate;
            Datetime nextOwnerBasedRemindersDate;
            Integer nextMainRemindersHours = getNextRemindersHours(mainRemindersHour, mainProcessType, 'main');
            Integer nextOwnerBasedRemindersHours = getNextRemindersHours(ownerBasedRemindersHour, ownerBasedProcessType, 'ownerBased');
            if (nextMainRemindersHours != null) {
                nextMainRemindersDate = pcf.VTD2_PCF_Safety_Concern_Indication_Date__c.addHours(nextMainRemindersHours);
            }
            if (nextOwnerBasedRemindersHours != null) {
                nextOwnerBasedRemindersDate = ownerBasedRemindersBaseTime.addHours(nextOwnerBasedRemindersHours);
            }
            if (nextMainRemindersDate != null && nextOwnerBasedRemindersDate == null) {
                scheduleNextReminders (nextMainRemindersDate, pcf.Id);
            } else if (nextMainRemindersDate == null && nextOwnerBasedRemindersDate != null) {
                scheduleNextReminders (nextOwnerBasedRemindersDate, pcf.Id);
            } else if (nextMainRemindersDate != null && nextOwnerBasedRemindersDate != null) {
                Datetime nextRemindersDate;
                if (nextMainRemindersDate.getTime() <= nextOwnerBasedRemindersDate.getTime()) {
                    nextRemindersDate = nextMainRemindersDate;
                } else {
                    nextRemindersDate = nextOwnerBasedRemindersDate;
                }
                scheduleNextReminders (nextRemindersDate, pcf.Id);
            }
        }
        if (!tnCatalogNotificationCodes.isEmpty()) {
            VT_D2_TNCatalogNotifications.generateNotifications(new List<String>(tnCatalogNotificationCodes), sourceNotificationIds, subjectParams, linkToRelatedEventMap, receiversMap, messageParams);
        }
        if (!allTemlatesNames.isEmpty()) {
            sendEmails();
        }
        if (interviewsToDelete.size() > 0 && !Test.isRunningTest()) {
            try {
                deleteFlowInterviews(JSON.serialize(interviewsToDelete));
            } catch (Exception e) {
                System.debug('Exception deleteFlowInterviews: ' + e.getMessage());
            }
        }
    }
    @Future(Callout = true)
    private static void deleteFlowInterviews(String interviewsToDeleteJSON) {
        List<FlowInterview> interviewsToDelete = (List<FlowInterview>)JSON.deserialize(interviewsToDeleteJSON, List<FlowInterview>.class);
        HttpRequest req = new HttpRequest();
        req.setMethod('DELETE');
        req.setHeader('Authorization', 'Bearer ' + VT_D1_HelperClass.getSessionIdForGuest());
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(System.Url.getOrgDomainUrl().toExternalForm() + '/services/apexrest/PcfRemindersSender/DeleteInterviews');
        req.setBody(JSON.serialize(interviewsToDelete));
        req.setTimeout(120000);
        System.debug(req);
        Http http = new Http();
        HttpResponse res = http.send(req);
        System.debug(res.getStatus() +' '+res.getBody());
        if (res.getStatusCode() != 202) {
            throw new DmlException('Failed to delete FlowInterview');
        }
    }
    @HttpDelete
    global static void doDelete() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        String requestBody = request.requestBody.toString();
        try {
            List<FlowInterview> interviews = (List<FlowInterview>)JSON.deserialize(requestBody, List<FlowInterview>.class);
            response.responseBody = Blob.valueOf(deleteFlowInterviews(interviews));
            response.statusCode = 202;
        } catch (Exception e) {
            response.responseBody = Blob.valueOf(e.getMessage());
            response.statusCode = 500;
        }
    }
    private static String deleteFlowInterviews(List<FlowInterview> interviewsToDelete) {
        String result;
        try {
            Database.DeleteResult[] DR_Dels = Database.delete(interviewsToDelete, false);
            result = JSON.serialize(DR_Dels);
        } catch (DmlException ex) {
            result = ex.getMessage();
        }
        return result;
    }
    /**
     * Get PCF Safety Concern reminders from VTR2_Reminders__mdt
     *   and put them in Map<String, Map<String, Map<Integer, Set<VTR2_Reminders__mdt>>>> allRemindersMap
     *
     *   @see VTR2_Reminders__mdt
     */
    private static void getAllRemindersMap () {
        if (!allRemindersMap.isEmpty()) {
            return;
        }
        List<VTR2_Reminders__mdt> reminders = [
                SELECT Id,
                        Email_Template__c,
                        Hours__c,
                        Profile_Name__c,
                        TN_Cataloge_Notification__c,
                        VTR3_PCF_Process_Type__c,
                        VTR3_Infinity_Loop__c,
                        VTR3_Loop_Interval__c,
                        VTR3_Loop_Upper_Bound__c,
                        VTR3_Role__c,
                        VTR3_Main_Reminders__c
                FROM VTR2_Reminders__mdt
                WHERE VTR3_ReminderType__c = :REMINDER_TYPE AND Hours__c != NULL
                ORDER BY Hours__c ASC
        ];
        for (VTR2_Reminders__mdt reminder : reminders) {
            String reminderType = reminder.VTR3_Main_Reminders__c ? 'main' : 'ownerBased';
            if (!allRemindersMap.containsKey(reminderType)) {
                allRemindersMap.put(reminderType, new Map<String, Map<Integer, Set<VTR2_Reminders__mdt>>>());
            }
            if (!allRemindersMap.get(reminderType).containsKey(reminder.VTR3_PCF_Process_Type__c)) {
                allRemindersMap.get(reminderType).put(reminder.VTR3_PCF_Process_Type__c, new Map<Integer, Set<VTR2_Reminders__mdt>>());
            }
            if (!allRemindersMap.get(reminderType).get(reminder.VTR3_PCF_Process_Type__c).containsKey(reminder.Hours__c.intValue())) {
                if (reminder.VTR3_Loop_Interval__c != null && reminder.VTR3_Loop_Upper_Bound__c != null) {
                    for (Integer offsetHours = 0; offsetHours < reminder.VTR3_Loop_Upper_Bound__c.intValue(); offsetHours += reminder.VTR3_Loop_Interval__c.intValue()) {
                        allRemindersMap.get(reminderType).get(reminder.VTR3_PCF_Process_Type__c).put(reminder.Hours__c.intValue() + offsetHours, new Set<VTR2_Reminders__mdt>());
                    }
                } else {
                    allRemindersMap.get(reminderType).get(reminder.VTR3_PCF_Process_Type__c).put(reminder.Hours__c.intValue(), new Set<VTR2_Reminders__mdt>());
                }
            }
            if (reminder.VTR3_Loop_Interval__c != null && reminder.VTR3_Loop_Upper_Bound__c != null) {
                for (Integer offsetHours = 0; offsetHours < reminder.VTR3_Loop_Upper_Bound__c.intValue(); offsetHours += reminder.VTR3_Loop_Interval__c.intValue()) {
                    allRemindersMap.get(reminderType).get(reminder.VTR3_PCF_Process_Type__c).get(reminder.Hours__c.intValue() + offsetHours).add(reminder);
                }
            } else {
                allRemindersMap.get(reminderType).get(reminder.VTR3_PCF_Process_Type__c).get(reminder.Hours__c.intValue()).add(reminder);
            }
        }
    }
    /**
     * Find needed reminders for current PCF and put them in the appropriate collections
     *
     * @param pcf A current PCF
     * @param currentRemindersHour A current reminders hour (hour since VTR3_PCF_Assignment_Date__c, VTD2_PCF_Safety_Concern_Indication_Date__c, etc.)
     * @param processType Process type name (Escalated_PG, Escalated_PI, etc.)
     * @param reminderType Reminder type 'main' (reminders based on VTD2_PCF_Safety_Concern_Indication_Date__c or VTD1_IsEscalatedDateTime__c) or 'ownerBased' (reminders based on VTR3_PCF_Assignment_Date__c)
     */
    private static void processReminders (Case pcf, Integer currentRemindersHour, String processType, String reminderType) {
        Map<String, Id> rolesToIdMap = new Map<String, Id>{
                'PI' => pcf.VTD1_Clinical_Study_Membership__r.VTD1_PI_user__c,
                'Backup_PI' => pcf.VTD1_Clinical_Study_Membership__r.VTD1_Backup_PI_User__c,
                'PG' => pcf.VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c,
                'Backup_PG' => pcf.VTD1_Clinical_Study_Membership__r.VTD1_Secondary_PG__c,
                'SCR' => pcf.VTD1_Clinical_Study_Membership__r.VTR2_SiteCoordinator__c,
                'Backup_SCR' => pcf.VTD1_Clinical_Study_Membership__r.VTR2_Backup_Site_Coordinator__c,
                'VTSL' => pcf.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR3_Study_Geography__r.VTR5_Primary_VTSL__c,
                'TMA_Queue_Id' => pcf.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTD2_TMA_Queue_ID__c
        };
        Map<String, String> messageParamsMap;
        if (reminderType == 'main') {
            messageParamsMap =  new Map<String, String>{
                    'N' => pcf.CaseNumber + ';' + (pcf.isEscalated ? System.Label.VTR3_SAE : System.Label.VTR3_AE) + ';' + pcf.VTD1_Clinical_Study_Membership__r.VTR2_PTNameSubId__c + ';' + pcf.VTD1_Study__r.VTD1_Protocol_Nickname__c + ';' + currentRemindersHour + ';' + (pcf.isEscalated ? System.Label.VTR3_SAE : System.Label.VTR3_AE),
                    'P' => pcf.CaseNumber + ';' + (pcf.isEscalated ? System.Label.VTR3_SAE : System.Label.VTR3_AE) + ';' + pcf.VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c + ';' + pcf.VTD1_Study__r.VTD1_Protocol_Nickname__c + ';' + currentRemindersHour + ';' + (pcf.isEscalated ? System.Label.VTR3_SAE : System.Label.VTR3_AE)
            };
        } else {
            messageParamsMap =  new Map<String, String>{
                    'N' => pcf.CaseNumber + ';' + (pcf.isEscalated ? System.Label.VTR3_SAE : System.Label.VTR3_AE) + ';' + pcf.VTD1_Clinical_Study_Membership__r.VTR2_PTNameSubId__c + ';' + pcf.VTD1_Study__r.VTD1_Protocol_Nickname__c + ';' + pcf.Owner.Name + ';' + currentRemindersHour + ';' + (pcf.isEscalated ? System.Label.VTR3_SAE : System.Label.VTR3_AE),
                    'P' => pcf.CaseNumber + ';' + (pcf.isEscalated ? System.Label.VTR3_SAE : System.Label.VTR3_AE) + ';' + pcf.VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c + ';' + pcf.VTD1_Study__r.VTD1_Protocol_Nickname__c + ';' + pcf.Owner.Name + ';' + currentRemindersHour + ';' + (pcf.isEscalated ? System.Label.VTR3_SAE : System.Label.VTR3_AE)
            };
        }
        Set<VTR2_Reminders__mdt> currentReminders = getCurrentReminders(processType, currentRemindersHour, reminderType);
        if (currentReminders != null) {
            for (VTR2_Reminders__mdt reminder : currentReminders) {
                if (rolesToIdMap.get(reminder.VTR3_Role__c) != null && !checkPgScrOwnerConflict(pcf.Owner.Profile.Name, reminder.VTR3_Role__c)) {
                    if (reminder.TN_Cataloge_Notification__c != null) {
                        String receiversMapKey = reminder.TN_Cataloge_Notification__c + pcf.Id;
                        if (!messageParams.get(pcf.Id).containsKey(reminder.TN_Cataloge_Notification__c)) {
                            messageParams.get(pcf.Id).put(
                                    reminder.TN_Cataloge_Notification__c,
                                    messageParamsMap.get(reminder.TN_Cataloge_Notification__c.substring(0, 1))
                            );
                            tnCatalogNotificationCodes.add(reminder.TN_Cataloge_Notification__c);
                            sourceNotificationIds.add(pcf.Id);
                        }
                        if (receiversMap.containsKey(receiversMapKey)) {
                            receiversMap.get(receiversMapKey).add(rolesToIdMap.get(reminder.VTR3_Role__c));
                        } else {
                            receiversMap.put(receiversMapKey, new List<Id>{
                                    rolesToIdMap.get(reminder.VTR3_Role__c)
                            });
                        }
                    }
                    if (reminder.Email_Template__c != null) {
                        if (!emailsMap.get(pcf.Id).containsKey(reminder.Email_Template__c)) {
                            emailsMap.get(pcf.Id).put(reminder.Email_Template__c, new List<Id>{
                                    rolesToIdMap.get(reminder.VTR3_Role__c)
                            });
                        } else {
                            emailsMap.get(pcf.Id).get(reminder.Email_Template__c).add(rolesToIdMap.get(reminder.VTR3_Role__c));
                        }
                        allTemlatesNames.add(reminder.Email_Template__c);
                    }
                }
            }
        }
    }
    /**
     * Get set of reminders for current process type, hour and reminders type
     *
     * @param processType Process type name (Escalated_PG, Escalated_PI, etc.)
     * @param currentRemindersHour A current reminders hour (hour since VTR3_PCF_Assignment_Date__c, VTD2_PCF_Safety_Concern_Indication_Date__c, etc.)
     * @param reminderType Reminder type 'main' (reminders based on VTD2_PCF_Safety_Concern_Indication_Date__c or VTD1_IsEscalatedDateTime__c) or 'ownerBased' (reminders based on VTR3_PCF_Assignment_Date__c)
     *
     * @return Set<VTR2_Reminders__mdt>
     *
     * @see VTR2_Reminders__mdt
     */
    private static Set<VTR2_Reminders__mdt> getCurrentReminders (String processType, Integer currentRemindersHour, String reminderType) {
        if (allRemindersMap.get(reminderType) == null || allRemindersMap.get(reminderType).get(processType) == null) {
            return null;
        }
        Set<VTR2_Reminders__mdt> currentReminders;
        if (allRemindersMap.get(reminderType).get(processType).get(currentRemindersHour) != null) {
            currentReminders = allRemindersMap.get(reminderType).get(processType).get(currentRemindersHour);
        } else {
            Set<Integer> remindersHours = allRemindersMap.get(reminderType).get(processType).keySet();
            List<Integer> sortedRemindersHours = new List<Integer>(remindersHours);
            sortedRemindersHours.sort();
            Integer targetReminderHours = sortedRemindersHours[sortedRemindersHours.size() - 1];
            Set<VTR2_Reminders__mdt> remindersList = allRemindersMap.get(reminderType).get(processType).get(targetReminderHours);
            for (VTR2_Reminders__mdt reminder : remindersList) {
                if (reminder.VTR3_Infinity_Loop__c && reminder.VTR3_Loop_Interval__c != null && currentRemindersHour >= reminder.Hours__c.intValue()) {
                    Integer newHour;
                    Integer mathModHours = Math.Mod((currentRemindersHour - reminder.Hours__c.intValue()), reminder.VTR3_Loop_Interval__c.intValue());
                    if (mathModHours == 0) {
                        newHour = currentRemindersHour;
                    } else {
                        newHour = currentRemindersHour - mathModHours + reminder.VTR3_Loop_Interval__c.intValue();
                    }
                    if (!allRemindersMap.get(reminderType).get(processType).containsKey(newHour)) {
                        allRemindersMap.get(reminderType).get(processType).put(newHour, new Set<VTR2_Reminders__mdt>());
                    }
                    allRemindersMap.get(reminderType).get(processType).get(newHour).add(reminder);
                    if (newHour == currentRemindersHour) {
                        newHour += reminder.VTR3_Loop_Interval__c.intValue();
                        if (!allRemindersMap.get(reminderType).get(processType).containsKey(newHour)) {
                            allRemindersMap.get(reminderType).get(processType).put(newHour, new Set<VTR2_Reminders__mdt>());
                        }
                        allRemindersMap.get(reminderType).get(processType).get(newHour).add(reminder);
                    }
                }
            }
            currentReminders = allRemindersMap.get(reminderType).get(processType).get(currentRemindersHour);
        }
        return currentReminders;
    }
    /**
     * Send emails
     */
    private static void sendEmails () {
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        List <OrgWideEmailAddress> owEmailAddresses = [select Id from OrgWideEmailAddress order by CreatedDate limit 1];
        Map <String, Id> templates = new Map <String, Id> ();
        for (EmailTemplate et : [SELECT Id, DeveloperName FROM EmailTemplate WHERE DeveloperName IN :allTemlatesNames]) {
            templates.put(et.DeveloperName, et.Id);
        }
        if (!templates.isEmpty()) {
            for (Id pcfId : emailsMap.keySet()) {
                for (String templateName : emailsMap.get(pcfId).keySet()) {
                    String tId = templates.get(templateName);
                    if (tId != null) {
                        for (Id recipientId : emailsMap.get(pcfId).get(templateName)) {
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            mail.setTargetObjectId(recipientId);
                            mail.setTreatTargetObjectAsRecipient(false);
                            mail.setUseSignature(false);
                            mail.setBccSender(false);
                            mail.setSaveAsActivity(false);
                            mail.setTemplateId(tId);
                            mail.setWhatId(pcfId);
                            if (!owEmailAddresses.isEmpty()) {
                                mail.setOrgWideEmailAddressId(owEmailAddresses[0].Id);
                            }
                            mails.add(mail);
                        }
                    }
                }
            }
            if (!mails.isEmpty() && !Test.isRunningTest()) {
                List<Messaging.SendEmailResult> results = Messaging.sendEmail(mails, false);
                for (Messaging.SendEmailResult result : results) {
                    System.debug('result = ' + result.isSuccess());
                }
            }
        }
    }
    /**
     * Get hour offset for next scheduled run for current process and reminder types
     *
     * @param currentRemindersHour A current reminders hour (hour since VTR3_PCF_Assignment_Date__c, VTD2_PCF_Safety_Concern_Indication_Date__c, etc.)
     * @param processType Process type name (Escalated_PG, Escalated_PI, etc.)
     * @param reminderType Reminder type 'main' (reminders based on VTD2_PCF_Safety_Concern_Indication_Date__c or VTD1_IsEscalatedDateTime__c) or 'ownerBased' (reminders based on VTR3_PCF_Assignment_Date__c)
     *
     * @return Integer
     */
    private static Integer getNextRemindersHours (Integer currentRemindersHour, String processType, String reminderType) {
        if (allRemindersMap.get(reminderType) == null || allRemindersMap.get(reminderType).get(processType) == null || allRemindersMap.get(reminderType).get(processType).keySet().isEmpty()) {
            return null;
        }
        Set<Integer> remindersHours = new Set<Integer>(allRemindersMap.get(reminderType).get(processType).keySet());
        remindersHours.add(currentRemindersHour);
        List<Integer> sortedRemindersHours = new List<Integer>();
        sortedRemindersHours.addAll(remindersHours);
        sortedRemindersHours.sort();
        Integer nextRemindersHour;
        if (sortedRemindersHours.indexOf(currentRemindersHour) < (sortedRemindersHours.size() - 1)) {
            nextRemindersHour = sortedRemindersHours[sortedRemindersHours.indexOf(currentRemindersHour) + 1];
        } else {
            remindersHours = new Set<Integer>(allRemindersMap.get(reminderType).get(processType).keySet());
            sortedRemindersHours.clear();
            sortedRemindersHours.addAll(remindersHours);
            sortedRemindersHours.sort();
            Integer targetReminderHours = sortedRemindersHours[sortedRemindersHours.size() - 1];
            Set<VTR2_Reminders__mdt> remindersSet = allRemindersMap.get(reminderType).get(processType).get(targetReminderHours);
            if (remindersSet != null) {
                for (VTR2_Reminders__mdt reminder : remindersSet) {
                    if (reminder.VTR3_Infinity_Loop__c && reminder.VTR3_Loop_Interval__c != null && reminder.VTR3_Loop_Upper_Bound__c == null && currentRemindersHour > 0) {
                        nextRemindersHour = currentRemindersHour - (Integer) Math.Mod((currentRemindersHour - reminder.Hours__c.intValue()), reminder.VTR3_Loop_Interval__c.intValue()) + reminder.VTR3_Loop_Interval__c.intValue();
                        break;
                    }
                }
            }
        }
        return nextRemindersHour;
    }
    /**
     * Schedule next reminders for current PCF
     *
     * @param baseTime A datetime of next reminders run
     * @param pcfId Id of current PCF
     */
    private static void scheduleNextReminders (Datetime baseTime, Id pcfId) {
        Map<String, Object> params = new Map<String, Object>{
                'baseTime' => baseTime,
                'pcfId' => pcfId
        };
        Flow.Interview.VTR3_PCF_SC_Reminders_Sender_Helper waitFlow = new Flow.Interview.VTR3_PCF_SC_Reminders_Sender_Helper(params);
        waitFlow.start();
    }

    private static Boolean checkPgScrOwnerConflict(String ownerProfileName, String role) {
        return ownerProfileName == VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME && SCR_ROLES.contains(role)
                || ownerProfileName == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME && PG_ROLES.contains(role);
    }
}