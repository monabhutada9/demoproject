/**
 * Created by user on 12.12.2018.
 */

public with sharing class VT_D1_TestDataGeneratorController {
    public String level { get; set; }
    public String message { get; set; }
    public String SCQueueId { get; set; }
    public String TMAQueueId { get; set; }
    public String counter { get; set; }
    public String SCGroupId { get; set; }
    public String PGGroupId { get; set; }
    public String CMGroupId { get; set; }
    public List <VTD1_Chat__c> chats { get; set; }
    public HealthCloudGA__CandidatePatient__c multipleCandidatePatients { get; set;}
    public Integer numberOfCandidatePatients { get; set;}
    // Added by Scrum-4(Harshita/Abhay) 06-June-2020
    // Part of the TDG Fix
    public static Boolean ifPiPayment = false;
    //=======================================
    
    // Added by Scrum-4(Harshita/Abhay) 06-June-2020
    // Part of the TDG Fix
    public VT_D1_TestDataGeneratorController() {
       multipleCandidatePatients = new HealthCloudGA__CandidatePatient__c();
    }
    
  
    public PageReference save(){
      try {
                if(numberOfCandidatePatients == null) {
                    numberOfCandidatePatients = 1;
                }
      
                List<HealthCloudGA__CandidatePatient__c>  lstCP = new List<HealthCloudGA__CandidatePatient__c>  ();
                HealthCloudGA__CandidatePatient__c multipleCandidatePatientsClone;
            
            for(Integer i=1; i<=numberOfCandidatePatients; i++) {
                    multipleCandidatePatientsClone = new HealthCloudGA__CandidatePatient__c();
                    //multipleCandidatePatientsClone = multipleCandidatePatients;
                    multipleCandidatePatientsClone.rr_firstName__c = multipleCandidatePatients.rr_firstName__c +String.ValueOf(i);
                    multipleCandidatePatientsClone.rr_lastName__c = multipleCandidatePatients.rr_lastName__c +String.ValueOf(i);
                    multipleCandidatePatientsClone.rr_Email__c = String.ValueOf(i)+multipleCandidatePatients.rr_Email__c;
                    multipleCandidatePatientsClone.HealthCloudGA__Address1Country__c = multipleCandidatePatients.HealthCloudGA__Address1Country__c;
                    multipleCandidatePatientsClone.HealthCloudGA__Address1State__c = multipleCandidatePatients.HealthCloudGA__Address1State__c;
                    multipleCandidatePatientsClone.HealthCloudGA__Address1City__c = multipleCandidatePatients.HealthCloudGA__Address1City__c;
                    multipleCandidatePatientsClone.HealthCloudGA__Address1PostalCode__c = multipleCandidatePatients.HealthCloudGA__Address1PostalCode__c;
                    multipleCandidatePatientsClone.VTD2_Language__c = multipleCandidatePatients.VTD2_Language__c;
                    multipleCandidatePatientsClone.Study__c = multipleCandidatePatients.Study__c;
                    multipleCandidatePatientsClone.VTD1_ProtocolNumber__c = multipleCandidatePatients.VTD1_ProtocolNumber__c;
                    multipleCandidatePatientsClone.VTR2_Caregiver_Preferred_Language__c = multipleCandidatePatients.VTR2_Caregiver_Preferred_Language__c;
                
              lstCP.add(multipleCandidatePatientsClone); 
            }
                // Added by Scrum-4(Harshita/Abhay) 06-June-2020
                // Part of the TDG Fix
                if(lstCP.Size()>0 && !lstCP.isEmpty()) {
                      insert lstCP;
                      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Successfully inserted to list'));
                }  
          }
           catch(System.DmlException e){
            ApexPages.addMessages(e);
            return null;
        }
        PageReference patientSuccess =new ApexPages.StandardController(multipleCandidatePatients).view();
           return patientSuccess;
    } 
    
    
    class GenerateDataSecondPhase implements Queueable {
        final Id studyId;
        final Integer index;
        final String name;
        public GenerateDataSecondPhase(Id studyId, Integer index, String name) {
            this.studyId = studyId;
            this.index = index;
            this.name = name;
        }
        public void execute(QueueableContext queueableContext) {
            VT_R2_TestDataGeneratorSettings__mdt settings = VT_D1_TestData.getSettings();
            List <Study_Team_Member__c> studyTeamMembers = [select Id, Name, User__c from Study_Team_Member__c where Study__c = :studyId and RecordType.Name = 'PI' and VTD1_PIsAssignedPG__c != null];
            Virtual_Site__c site = new Virtual_Site__c(
                    VTD1_Study__c = studyId,
                    Name = 'REG_TEST-' + index,
                    VTD1_Study_Team_Member__c = studyTeamMembers[0].Id,
                    VTD1_PI_User__c = studyTeamMembers[0].User__c
            );
            insert site;
            List <HealthCloudGA__CandidatePatient__c> candidatePatients = new List<HealthCloudGA__CandidatePatient__c>();
            candidatePatients.add(VT_D1_TestData.prepareTestPatientCandidate(index, studyId, settings.StateWithLicence__c, true, name));
            candidatePatients.add(VT_D1_TestData.prepareTestPatientCandidate(index + 1, studyId, settings.StateWithLicence__c, false, name));
            candidatePatients.add(VT_D1_TestData.prepareTestPatientCandidate(index + 2, studyId, settings.StateWithoutLicence__c, false, name));
            VT_D1_TestData.persistCandidatePatients(candidatePatients);
        }
    }

    public static Map <String, Long> jobDurationMap = new Map<String, Long>();
    private static void addCreateJobDuration(Integer index) {
        jobDurationMap.put('CG_F' + index, System.now().getTime());
        System.debug('addCreateJobDuration ' + jobDurationMap);
    }
    private static boolean checkCreateJobDurationExceed(Integer index) {
        long startTime = jobDurationMap.get('CG_F' + index);
        System.debug('st = ' + startTime + ' ' + System.now().getTime() + ' ' + ('CG_F' + index) + ' ' + jobDurationMap.size());
        if (startTime == null || System.now().getTime() - startTime < 30000)
            return false; else
                return true;
    }
    @RemoteAction
    public static void generateData(String name, String strIndex) {
        VT_R2_TestDataGeneratorSettings__mdt settings = VT_D1_TestData.getSettings();

        Integer index = Integer.valueOf(strIndex);
        System.debug('generateData ' + name + ' ' + index);
        List <HealthCloudGA__CarePlanTemplate__c> existsStudies = [select Id from HealthCloudGA__CarePlanTemplate__c where Name = :settings.GenericStudyName__c];
        if (name == 'EPro' || name == 'PreScreening') {
            System.debug('creating test Study for EPro');
            HealthCloudGA__CarePlanTemplate__c study = null;
            if (name == 'Create Study') {
                study = VT_D1_TestData.createTestStudy(index, false);
            } else if (name == 'EPro') {
                study = VT_D1_TestData.createTestStudy(index, true);
            } else {
                study = VT_D1_TestData.createTestStudy(index, false);
            }// create Study
            System.debug('creating sites for test study');


            Map <HealthCloudGA__CarePlanTemplate__c, List <Virtual_Site__c>> studyToSitesMap = VT_D1_TestData.createVirtualSites(new List<HealthCloudGA__CarePlanTemplate__c>{
                    study
            }, index, 1);
            VTD1_Regulatory_Document__c regulatoryDocument = new VTD1_Regulatory_Document__c(Name = 'RD-' + index, Level__c = 'Site', VTD1_TMF__c = true, VTD1_ISF__c = true, VTD1_Document_Type__c = 'Site Activation Approval Form', VTR2_Way_to_Send_with_DocuSign__c = 'Manual');
            insert regulatoryDocument;
            VTD1_Regulatory_Binder__c regulatoryBinder = new VTD1_Regulatory_Binder__c(VTD1_Care_Plan_Template__c = study.Id, VTD1_Regulatory_Document__c = regulatoryDocument.Id);
            insert regulatoryBinder;
            List <Study_Team_Member__c> studyTeamMembers = [select Id, Name, User__c from Study_Team_Member__c where Study__c = :study.Id and RecordType.Name = 'PI' and VTD1_PIsAssignedPG__c != null];
            Virtual_Site__c site = new Virtual_Site__c(
                    VTD1_Study__c = study.Id,
                    Name = 'REG_TEST-' + index,
                    VTD1_Study_Team_Member__c = studyTeamMembers[0].Id,
                    VTD1_PI_User__c = studyTeamMembers[0].User__c
            );
            insert site;
            List <HealthCloudGA__CandidatePatient__c> candidatePatients = new List<HealthCloudGA__CandidatePatient__c>();
            candidatePatients.add(VT_D1_TestData.prepareTestPatientCandidate(index, study.Id, settings.StateWithLicence__c, true, name));
            if (!Test.isRunningTest()) {
                candidatePatients.add(VT_D1_TestData.prepareTestPatientCandidate(index + 1, study.Id, settings.StateWithLicence__c, false, name));
                candidatePatients.add(VT_D1_TestData.prepareTestPatientCandidate(index + 2, study.Id, settings.StateWithoutLicence__c, false, name));
            }
            VT_D1_TestData.persistCandidatePatients(candidatePatients);
            //System.enqueueJob(new GenerateDataSecondPhase(study.Id, index, name));
        } else if (name == 'Visits Monitoring' || name == 'Regulatory Documents' || name == 'PI Payments') {
            // Added by Scrum-4(Harshita/Abhay) 06-June-2020
            // Part of the TDG Fix
            ifPiPayment = name == 'PI Payments'?true:false;
            HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestData.createTestStudy(index, true);
            Map <HealthCloudGA__CarePlanTemplate__c, List <Virtual_Site__c>> studyToSitesMap = VT_D1_TestData.createVirtualSites(new List<HealthCloudGA__CarePlanTemplate__c>{
                    study
            }, index, 10);
            if (name == 'Visits Monitoring') {
                VT_D1_TestData.createMonitoringVisits(new List<HealthCloudGA__CarePlanTemplate__c>{
                        study
                }, settings.VM_CRA__c);
            }
        } 
        //======================================================
        
        else if (name == 'Create Study') {
            HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestData.createTestStudy(index, true);
            Map <HealthCloudGA__CarePlanTemplate__c, List <Virtual_Site__c>> studyToSitesMap = VT_D1_TestData.createVirtualSites(new List<HealthCloudGA__CarePlanTemplate__c>{
                    study
            }, index, 10);
            
        }
        
        //======================================================
        else if (name == 'Virtual Study Startup') {
            VT_D1_TestData.createSCQueue(settings.VirtualStudyStartupSC__c);
            //VT_D1_TestData.createTestStudy(index);
            createGroupsAndChats(index);
            //createStudy(index);
        } else if (name == 'Virtual Study Closeout') {
            HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestData.createTestStudy(index, true); // create Study
            ContentVersion contentVersion = new ContentVersion(Title = 'test.txt', PathOnClient = 'test.txt', VersionData = Blob.valueOf('test content'));
            insert contentVersion;
            contentVersion = [select Id, ContentDocumentId, OwnerId from ContentVersion where Id = :contentVersion.Id];
            ContentDocumentLink cdl = new ContentDocumentLink(ShareType = 'V', LinkedEntityId = study.Id, ContentDocumentId = contentVersion.ContentDocumentId);
            insert cdl;
            VT_D1_TestData.createVirtualSite(study, index);
            List <HealthCloudGA__CandidatePatient__c> candidatePatients = new List<HealthCloudGA__CandidatePatient__c>();
            candidatePatients.add(VT_D1_TestData.prepareTestPatientCandidate(index, study.Id, settings.StateWithLicence__c, true, name));
            VT_D1_TestData.persistCandidatePatients(candidatePatients);
        } else if (name == 'Medical Records' || name == 'Communications') {
            HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestData.createTestStudy(index, true); // create Study
            List <HealthCloudGA__CandidatePatient__c> candidatePatients = new List<HealthCloudGA__CandidatePatient__c>();
            candidatePatients.add(VT_D1_TestData.prepareTestPatientCandidate(index, study.Id, settings.StateWithLicence__c, true, name));
            VT_D1_TestData.persistCandidatePatients(candidatePatients);
            if (name == 'Medical Records') {
                VT_D1_TestData.createUsers(index);
            }
        } else if (name == 'Scheduling Conduction Visits') {
            HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestData.createTestStudy2(index); // create Study
            List <HealthCloudGA__CandidatePatient__c> candidatePatients = new List<HealthCloudGA__CandidatePatient__c>();
            candidatePatients.add(VT_D1_TestData.prepareTestPatientCandidate(index, study.Id, settings.StateWithLicence__c, true, name));
            VT_D1_TestData.persistCandidatePatients(candidatePatients);
        }
    }
    @future
    public static void createGroupsAndChats(Integer index) {
        String name = 'TestProt-00' + String.valueOf(index);
        List <String> cgnames = new List<String>{
                name + '-SC', name + '-PG', name + '-CM'
        };
        List <Id> chatterGroupsIds = VT_D1_TestData.createChatterGroups(cgnames, null);
    }
    @future
    public static void createStudy(Integer index) {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestData.createTestStudy(index, true); // create Study
    }

    @RemoteAction
    public static void deleteData(String name, String strIndex) {
        Integer index = Integer.valueOf(strIndex);
        if (name == 'EPro' || name == 'PreScreening') {
            VT_D1_TestDataCleaner dataCleaner = new VT_D1_TestDataCleaner(new List<Integer>{
                    index, index + 1, index + 2
            }, 0);
            dataCleaner.run();
        } else if (name == 'Visits Monitoring' || name == 'Regulatory Documents' || name == 'PI Payments') {
            VT_D1_TestData.deleteTestStudy(index);
        } else if (name == 'Virtual Study Startup') {
            System.debug('delete ' + name);
            ConnectApi.ChatterGroupPage p = ConnectApi.ChatterGroups.searchGroups(null, 'TestProt-00' + index + '-SC');
            if (!p.groups.isEmpty()) {
                ConnectApi.ChatterGroups.deleteGroup(null, p.groups[0].Id);
            }
            p = ConnectApi.ChatterGroups.searchGroups(null, 'TestProt-00' + index + '-PG');
            if (!p.groups.isEmpty()) {
                ConnectApi.ChatterGroups.deleteGroup(null, p.groups[0].Id);
            }
            p = ConnectApi.ChatterGroups.searchGroups(null, 'TestProt-00' + index + '-CM');
            if (!p.groups.isEmpty()) {
                ConnectApi.ChatterGroups.deleteGroup(null, p.groups[0].Id);
            }
            String studyName = 'TestProt-00' + index;
            List <HealthCloudGA__CarePlanTemplate__c> studies = [select Id, VTD1_Patient_Group_Id__c, VTD1_PIGroupId__c, VTD1_DeliveryTeamGroupId__c from HealthCloudGA__CarePlanTemplate__c where Name = :studyName];
            if (!studies.isEmpty()) {
                Set <Id> chatIds = new Set<Id>();
                chatIds.add(studies[0].VTD1_Patient_Group_Id__c);
                chatIds.add(studies[0].VTD1_PIGroupId__c);
                chatIds.add(studies[0].VTD1_DeliveryTeamGroupId__c);
                List <VTD1_Chat__c> chats = [select Id from VTD1_Chat__c where Id in :chatIds];
                if (!chats.isEmpty())
                    delete chats;
            }
            deleteQueue('SCTestQueue');
            deleteQueue('TMATestPool');
        } else if (name == 'Virtual Study Closeout') {
            System.debug('delete Virtual Study Closeout');
            VT_D1_TestDataCleaner dataCleaner = new VT_D1_TestDataCleaner(new List<Integer>{
                    index
            }, 0);
            dataCleaner.run();
        } else if (name == 'Medical Records' || name == 'Communications' || name == 'Scheduling Conduction Visits') {
            VT_D1_TestDataCleaner dataCleaner = new VT_D1_TestDataCleaner(new List<Integer>{
                    index
            }, 0);
            dataCleaner.run();
            if (name == 'Medical Records') {
                VT_D1_TestData.deactivateUsers(index);
            }
        }
    }
    @future
    public static void deleteQueue(String name) {
        VT_D1_TestData.deleteSCQueue(name);
    }
    private static String createStatusEPro(String name, Integer index, Integer size) {
        String jobName = 'CG_F' + index;
        List<CronTrigger> jobsToAbort = [select Id, NextFireTime from CronTrigger where CronJobDetail.Name = :jobName];
        if (!jobsToAbort.isEmpty() && jobsToAbort[0].NextFireTime.getTime() - System.now().getTime() > 600000) {
            System.abortJob(jobsToAbort[0].id);
            jobsToAbort = [select Id, NextFireTime from CronTrigger where CronJobDetail.Name = :jobName];
        }

        if (!jobsToAbort.isEmpty())
            return 'progress'; else {
            String CGUserName = 'CG_F' + index + '@test.com_user';
            List <User> CGusers = [select Id from User where Username = :CGUserName];
            Set <String> accountNames = new Set<String>();
            for (Integer i = index; i <= index + size; i++) {
                accountNames.add('F' + i + ' ' + 'L' + i);
                accountNames.add('AF' + i + ' ' + 'AL' + i);
            }
            List <Account> accounts = [select Id from Account where Name in :accountNames];
            System.debug('CGusers = ' + CGusers);
            System.debug('accounts = ' + accounts);
            if (CGusers.isEmpty() && accounts.isEmpty())
                return 'create'; else
                    return 'delete';
        }
    }
    private static String createStatusMV(String name, Integer index) {
        String studyName = 'TestProt-00' + index;
        List <HealthCloudGA__CarePlanTemplate__c> studies = [select Id, VTD1_Patient_Group_Id__c, VTD1_PIGroupId__c, VTD1_DeliveryTeamGroupId__c from HealthCloudGA__CarePlanTemplate__c where Name = :studyName];
        if (studies.isEmpty())
            return 'create'; else
                return 'delete';
    }
    private static String createStatusVSS(String name, Integer index) {
        ConnectApi.ChatterGroupPage p1 = ConnectApi.ChatterGroups.searchGroups(null, 'TestProt-00' + index + '-SC');
        ConnectApi.ChatterGroupPage p2 = ConnectApi.ChatterGroups.searchGroups(null, 'TestProt-00' + index + '-PG');
        if (p1.groups.isEmpty() && p2.groups.isEmpty())
            return 'create'; else
                return 'delete';
    }
    @RemoteAction
    public static String getCreateResult(String name, String strIndex) {
        Integer index = Integer.valueOf(strIndex);
        Integer size = 0;
        if (name == 'EPro' || name == 'PreScreening')
            size = 2;
        if (name == 'EPro' || name == 'Virtual Study Closeout' || name == 'Medical Records' || name == 'Communications' || name == 'PreScreening' || name == 'Scheduling Conduction Visits') {
            String jobName = 'CG_F' + strIndex;
            List<CronTrigger> jobsToAbort = [select Id from CronTrigger where CronJobDetail.Name = :jobName];

            String CGUserName = 'CG_F' + strIndex + '@test.com_user';
            List <User> CGusers = [select Id from User where Username = :CGUserName];

            Set <String> accountNames = new Set<String>();


            for (Integer i = index; i <= index + size; i++) {
                accountNames.add('F' + i + ' ' + 'L' + i);
            }

            System.debug('accountNames = ' + accountNames);

            List <Account> accounts = [select Id from Account where Name in :accountNames];

            System.debug('accounts = ' + accounts);
            System.debug('CGusers = ' + CGusers + ' ' + size);

            if ((CGusers.isEmpty() || accounts.size() != size + 1) && jobsToAbort.isEmpty())
                return 'error'; else if (!CGusers.isEmpty() && accounts.size() == size + 1)
                return 'ok'; else
                    return '';
        } else if (name == 'Visits Monitoring' || name == 'Regulatory Documents' || name == 'PI Payments') {
            return 'ok';
        } else if (name == 'Create Study') {
            return 'ok';
        } else if (name == 'Virtual Study Startup') {
            ConnectApi.ChatterGroupPage p1 = ConnectApi.ChatterGroups.searchGroups(null, 'TestProt-00' + index + '-SC');
            ConnectApi.ChatterGroupPage p2 = ConnectApi.ChatterGroups.searchGroups(null, 'TestProt-00' + index + '-PG');
            try {
                ID id = p1.groups[0].Id;
                id = p2.groups[0].Id;
            } catch (Exception e) {
                return '';
            }
            return 'ok';
        } else
                return 'error';
    }
    @RemoteAction
    public static String getDeleteResult(String name, String strIndex) {
        Integer index = Integer.valueOf(strIndex);
        System.debug('getDeleteResult ' + name + ' ' + index);
        if (name == 'EPro' || name == 'Virtual Study Closeout' || name == 'Medical Records' || name == 'PreScreening' || name == 'Communications' || name == 'Scheduling Conduction Visits') {
            /*List <String> accountNames = new List<String>();
            accountNames.add('F' + index + ' L' + index);
            accountNames.add('F' + (index + 1) + ' L' + (index + 1));
            System.debug('names = ' + accountNames);
            List <Account> accounts = [select Id from Account where Name in :accountNames];
            if (accounts.isEmpty())
                return 'ok';
            else
                return '';*/
            String studyName = 'TestProt-00' + index;
            List <HealthCloudGA__CarePlanTemplate__c> studies = [select Id, VTD1_Patient_Group_Id__c, VTD1_PIGroupId__c, VTD1_DeliveryTeamGroupId__c from HealthCloudGA__CarePlanTemplate__c where Name = :studyName];
            if (studies.isEmpty())
                return 'ok'; else
                    return '';
        } else if (name == 'Visits Monitoring' || name == 'Regulatory Documents' || name == 'PI Payments') {
            return 'ok';
        } else if (name == 'Create Study') {
            return 'ok';
        } else if (name == 'Virtual Study Startup') {
            return 'ok';
        } else return
                'error';
    }
    public void showPageMessage() {
        if (level == 'WARNING') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, message));
        } else if (level == 'INFO') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, message));
        }
    }
    public void VSSDataCreate() {
        /*String name = 'TestProt-00'+String.valueOf(counter);
        List <String> cgnames = new List<String>{name + '-SC', name + '-PG'};
        List <Id> chatterGroupsIds = VT_D1_TestData.createChatterGroups(cgnames);
        SCGroupId = chatterGroupsIds[0];
        PGGroupId = chatterGroupsIds[1];
        System.debug('VSSDataCreate');
        SCQueueId = VT_D1_TestData.createSCQueue('Alvin Brooks');*/
        System.debug('counter = ' + counter);
        ConnectApi.ChatterGroupPage p = ConnectApi.ChatterGroups.searchGroups(null, 'TestProt-00' + counter + '-SC');
        SCGroupId = p.groups[0].Id;
        p = ConnectApi.ChatterGroups.searchGroups(null, 'TestProt-00' + counter + '-PG');
        PGGroupId = p.groups[0].Id;
        p = ConnectApi.ChatterGroups.searchGroups(null, 'TestProt-00' + counter + '-CM');
        CMGroupId = p.groups[0].Id;
        List <Group> groups = [select Id, Name from Group where Name in ('SCTestQueue', 'TMATestPool') and Type = 'Queue'] ;
        if (groups.isEmpty())
            return;
        for (Group g : groups) {
            if (g.Name == 'SCTestQueue') {
                SCQueueId = g.Id;
            } else if (g.Name == 'TMATestPool') {
                TMAQueueId = g.Id;
            }
        }

        chats = new List<VTD1_Chat__c>();
        for (Integer i = 0; i < 3; i++) {
            VTD1_Chat__c chat = new VTD1_Chat__c(VTD1_Chat_Type__c = 'Broadcast');
            chats.add(chat);
        }

        insert chats;
    }

    @RemoteAction
    public static String checkButtonStatuses(String name, String strIndex, Integer queryNumber) {
        Integer index = Integer.valueOf(strIndex);
        String status = '';
        if (name == 'EPro' || name == 'PreScreening') {
            status = createStatusEPro(name, index, 2);
        } else if (name == 'Virtual Study Closeout' || name == 'Medical Records' || name == 'Communications' || name == 'Scheduling Conduction Visits') {
            status = createStatusEPro(name, index, 0);
        } else if (name == 'Visits Monitoring' || name == 'Regulatory Documents' || name == 'PI Payments') {
            status = createStatusMV(name, index);
        } else if (name == 'Virtual Study Startup') {
            status = createStatusVSS(name, index);
        }
        return name + '_' + status + '_' + queryNumber;
    }
}