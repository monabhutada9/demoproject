/**
* @author: Carl Judge
* @date: 21-Sep-18
* @description: Handler for VT_D2_TimeSlotTrigger
**/

public without sharing class VT_D2_TimeSlotTriggerHandler {

    public void onAfterUpdate(List<VTD2_Time_Slot__c> recs, Map<Id, VTD2_Time_Slot__c> oldMap) {
        sendRequestEmails(recs, oldMap);
    }

    private void sendRequestEmails(List<VTD2_Time_Slot__c> recs, Map<Id, VTD2_Time_Slot__c> oldMap) {
        List<VTD2_Time_Slot__c> toSendEmail = new List<VTD2_Time_Slot__c>();
        List<Id> visitIds = new List<Id>();

        for (VTD2_Time_Slot__c item : recs) {
            if (item.VTD2_Status__c == VT_R4_ConstantsHelper_VisitsEvents.TIMESLOT_CONFIRMED && oldMap.get(item.Id).VTD2_Status__c != VT_R4_ConstantsHelper_VisitsEvents.TIMESLOT_CONFIRMED) {
                toSendEmail.add(item);
                if (item.VTD2_Actual_Visit__c != null) { visitIds.add(item.VTD2_Actual_Visit__c	); }
            }
        }

        if (! visitIds.isEmpty()) {
            EmailTemplate template;
            for (EmailTemplate item : [SELECT Id, Subject, Body FROM EmailTemplate WHERE DeveloperName = 'VT_D2_Visit_Confirmed_Template']) {
                template = item;
            }

            if (template != null) {
                Map<Id, VTD1_Actual_Visit__c> visitMap = new Map<Id, VTD1_Actual_Visit__c>([
                    SELECT
                        Id,
                        RecordType.DeveloperName,
                        VTD1_Case__r.VTD1_PI_user__c,
                                VTD1_Case__r.VTD1_PI_user__r.Email,
                        VTD1_Case__r.VTD1_PI_user__r.Contact.VTD2_PI_Office_Emails__c,
                        Unscheduled_Visits__r.VTD1_PI_user__c,
                                Unscheduled_Visits__r.VTD1_PI_user__r.Email,
                        Unscheduled_Visits__r.VTD1_PI_user__r.Contact.VTD2_PI_Office_Emails__c
                    FROM VTD1_Actual_Visit__c
                    WHERE Id IN :visitIds
                ]);

//                List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                List <VT_R3_EmailsSender.ParamsHolder> emList = new List <VT_R3_EmailsSender.ParamsHolder>();

//                List <OrgWideEmailAddress> owEmailAddresses = [select Id from OrgWideEmailAddress order by CreatedDate limit 1];
                for (VTD2_Time_Slot__c item : toSendEmail) {
                    if (visitMap.containsKey(item.VTD2_Actual_Visit__c)) {
                        VTD1_Actual_Visit__c visit = visitMap.get(item.VTD2_Actual_Visit__c);
                        Case cas = visit.RecordType.DeveloperName == 'VTD1_Unscheduled' ?
                            visit.Unscheduled_Visits__r : visit.VTD1_Case__r;

//                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
//                        mail.setTargetObjectId(cas.VTD1_PI_user__c);
//                        mail.setUseSignature(false);
//                        mail.setBccSender(false);
//                        mail.setSaveAsActivity(false);
//                        mail.setTemplateId(template.Id);
//                        mail.setWhatId(item.Id);
//                        if (!owEmailAddresses.isEmpty())
//                            mail.setOrgWideEmailAddressId(owEmailAddresses[0].Id);
//                        //mail.setPlainTextBody('Test Email');
//                        system.debug(cas.VTD1_PI_user__r.Contact.VTD2_PI_Office_Emails__c);
//                        if (cas.VTD1_PI_user__r.Contact.VTD2_PI_Office_Emails__c != null) {
//                            mail.setCcAddresses(cas.VTD1_PI_user__r.Contact.VTD2_PI_Office_Emails__c.split(';'));
//                        }
//
//
                        emList.add(new VT_R3_EmailsSender.ParamsHolder(null, item.Id, 'VT_D2_Visit_Confirmed_Template', cas.VTD1_PI_user__r.Email, true));
//                        mails.add(mail);
                        }
                    }
                system.debug('!!! mails='+emList);
                if (! emList.isEmpty()) {
                    VT_R3_EmailsSender.sendEmails(emList);
//                    Messaging.sendEmail(mails);
                }
            } else {
                System.debug('Email template VT_D2_Visit_Confirmed_Template not found!');
            }
        }
    }
}