/**
 * Created by user on 21.02.2019.
 */

public without sharing class VT_R2_VisitMemberNotificationController {
    public Id visitMemberId { get; set; }
    public Id visitId { get; set; }
    public Boolean isVisitMemberRemoved { get; set; } {
        isVisitMemberRemoved = false;
    }
    public Boolean isVisitMemberAdded { get; set; } {
        isVisitMemberAdded = false;
    }
    public Boolean isVisitCancelled { get; set; } {
        isVisitCancelled = false;
    }
    public Boolean showNonStudyHubText { get; set; } {
        showNonStudyHubText = false;
    }
    public Boolean isReadyToBeScheduled { get; set; } {
        isReadyToBeScheduled = false;
    }
    public Boolean isCompletedVisit { get; set; } {
        isCompletedVisit = false;
    }
    public Boolean isScheduledVisit { get; set; } {
        isScheduledVisit = false;
    }
    public Boolean isRescheduledVisit { get; set; } {
        isRescheduledVisit = false;
    }
    public Boolean isTelevisit { get; set; }
    public String NonStudyHubText { get; set; }
    public String joinToTelevisitLink { get; set; }
    public String televisitInvitation { get; set; }
    public String linkToStudyHub { get; set; }
    //public  String closingMail            {get; set;}
    public String visitIqvia { get; set; }
    public String sincerely { get; set; }
    public String reasonForParticipationLabel { get; set; }
    public String reasonForRemovalLabel { get; set; }
    public String reasonForParticipation { get; set; }
    public String reasonForRemoval { get; set; }
    public String announcement { get; set; }
    public String mode { get; set; }
    public String timeOffset { get; set; }
    public String joinTelevisitText { get; set; }
    public Boolean isVisitSoftCanceled { get; set; } {
        isVisitSoftCanceled = false;
    }
    public Boolean isVisitReqReschedule { get; set; } {
        isVisitReqReschedule = false;
    }
    public String reachOutPatient { get; set; }
    public String reqReachOutPatient { get; set; }
    public String debug { get; set; }
    public VTD1_Conference_Member__c conferenceMember { get; set; }
    private String language = 'en_US';
    public Visit_Member__c visitMember;
    private TimeZone tz = UserInfo.getTimeZone();//TimeZone.getTimeZone([SELECT TimeZoneSidKey FROM Organization][0].TimeZoneSidKey);
    public Datetime boundTime;
    public String emailText = '';
    public String Study_Nickname = '';
    public User recipient { get; set; }

    public Visit_Member__c getVisitMember() {
        if (visitMember != null) {
            return visitMember;
        }
        Datetime currentTime = System.now();
        boundTime = currentTime.addHours(1);

        if (visitId == null) {
            visitMember = [
                    SELECT Id, Name, RecordTypeId, VTD1_Is_internal__c, VTR2_ReasonForParticipation__c, VTD1_Actual_Visit__r.VTR2_ReasonToRemove__c, VTD1_Member_Type__c,
                            VTD1_Participant_User__r.TimeZoneSidKey, VTD1_Participant_User__r.LanguageLocaleKey, VTD1_Participant_User__r.ProfileId,
                            VTD1_Participant_User__r.FirstName, VTD1_Participant_User__r.LastName,
                            Visit_Member_Name__c, VTD1_Actual_Visit__r.VTD2_Common_Visit_Type__c, VTD1_Actual_Visit__r.VTD2_Study__c,
                            VTD1_Participant_User__r.Profile.Name,
                            VTD1_Actual_Visit__r.VTD1_Status__c, VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c,
                            VTD1_Actual_Visit__r.VTD1_Case__r.VTR2_StudyPhoneNumber__r.Name,
                            VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTR2_StudyPhoneNumber__r.Name,
                            VTD1_Actual_Visit__r.VTD1_Protocol_Visit__r.VTD1_EDC_Name__c, VTD1_Actual_Visit__r.VTD2_Patient_ID__c, VTD1_Actual_Visit__r.Name,
                            VTD1_Actual_Visit__r.VTD1_Visit_Name__c, VTD1_Actual_Visit__r.VTD1_Study_name_formula__c,
                            VTD1_Actual_Visit__r.VTD2_PatientName_PatientId__c, VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Study__c,
                            VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Study__c, VTD1_Actual_Visit__r.VTD1_Onboarding_Type__c,
                            VTD1_Actual_Visit__r.VTD1_Contact__r.Name,
                            VTD1_Actual_Visit__r.VTD1_VisitType_SubType__c,
                            VTD1_Actual_Visit__r.Sub_Type__c,
                            VTD1_Actual_Visit__r.VTD1_Protocol_Nickname__c,
                            VTD1_Actual_Visit__r.VTD2_Email_Study_Nickname__c,
                            VTD1_Actual_Visit__r.Patient_Name_Formula__c,
                            VTD1_Actual_Visit__r.VTR2_Televisit__c,
                            VTD1_Actual_Visit__r.VTD2_Study_Phone_Number__c,
                            VTD1_Participant_User__r.Id,
                            VTD1_Actual_Visit__r.RecordType.DeveloperName,
                            VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Study__r.VTD1_Protocol_Nickname__c,
                            VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Study__r.VTD1_Protocol_Nickname__c,
                            VTD1_Actual_Visit__r.VTR3_Previous_Schedule_Date__c,
                            VTD1_Actual_Visit__r.VTD1_Unscheduled_Visit_Type__c
                    FROM Visit_Member__c
                    WHERE Id = :visitMemberId
            ];
        } else {
            VTD1_Actual_Visit__c visit = [
                    SELECT Id, VTR2_ReasonToRemove__c,
                            VTD2_Common_Visit_Type__c, VTD2_Study__c,
                            VTD1_Status__c, VTD1_Scheduled_Date_Time__c,
                            VTD1_Case__r.VTR2_StudyPhoneNumber__r.Name,
                            Unscheduled_Visits__r.VTR2_StudyPhoneNumber__r.Name,
                            VTD1_Protocol_Visit__r.VTD1_EDC_Name__c, VTD2_Patient_ID__c, Name,
                            VTD1_Visit_Name__c, VTD1_Study_name_formula__c,
                            VTD2_PatientName_PatientId__c, VTD1_Case__r.VTD1_Study__c,
                            Unscheduled_Visits__r.VTD1_Study__c, VTD1_Onboarding_Type__c,
                            VTD1_Contact__r.Name,
                            VTD1_VisitType_SubType__c,
                            Sub_Type__c,
                            VTD1_Protocol_Nickname__c,
                            VTD2_Email_Study_Nickname__c,
                            Patient_Name_Formula__c,
                            VTR2_Televisit__c,
                            VTD1_Case__r.VTR2_SiteCoordinator__c,
                            VTD1_Case__r.VTR2_Backup_Site_Coordinator__c,
                            Unscheduled_Visits__r.VTR2_SiteCoordinator__c,
                            Unscheduled_Visits__r.VTR2_Backup_Site_Coordinator__c,
                            VTD1_Case__r.VTR2_SiteCoordinator__r.Email,
                            VTD1_Case__r.VTR2_Backup_Site_Coordinator__r.Email,
                            Unscheduled_Visits__r.VTR2_SiteCoordinator__r.Email,
                            Unscheduled_Visits__r.VTR2_Backup_Site_Coordinator__r.Email,
                            VTD1_Case__r.VTR2_SiteCoordinator__r.LanguageLocaleKey,
                            VTD1_Case__r.VTR2_Backup_Site_Coordinator__r.LanguageLocaleKey,
                            Unscheduled_Visits__r.VTR2_SiteCoordinator__r.LanguageLocaleKey,
                            Unscheduled_Visits__r.VTR2_Backup_Site_Coordinator__r.LanguageLocaleKey,
                            VTD1_Case__r.VTR2_SiteCoordinator__r.TimeZoneSidKey,
                            VTD1_Case__r.VTR2_Backup_Site_Coordinator__r.TimeZoneSidKey,
                            Unscheduled_Visits__r.VTR2_SiteCoordinator__r.TimeZoneSidKey,
                            Unscheduled_Visits__r.VTR2_Backup_Site_Coordinator__r.TimeZoneSidKey,
                            VTD1_Case__r.VTR2_SiteCoordinator__r.Name,
                            VTD1_Case__r.VTR2_Backup_Site_Coordinator__r.Name,
                            Unscheduled_Visits__r.VTR2_SiteCoordinator__r.Name,
                            Unscheduled_Visits__r.VTR2_Backup_Site_Coordinator__r.Name,
                            VTD1_Case__r.VTR2_SiteCoordinator__r.FirstName,
                            VTD1_Case__r.VTR2_Backup_Site_Coordinator__r.FirstName,
                            Unscheduled_Visits__r.VTR2_SiteCoordinator__r.FirstName,
                            Unscheduled_Visits__r.VTR2_Backup_Site_Coordinator__r.FirstName,
                            VTD1_Case__r.VTR2_SiteCoordinator__r.LastName,
                            VTD1_Case__r.VTR2_Backup_Site_Coordinator__r.LastName,
                            Unscheduled_Visits__r.VTR2_SiteCoordinator__r.LastName,
                            Unscheduled_Visits__r.VTR2_Backup_Site_Coordinator__r.LastName,
                            VTD1_Case__r.VTR2_SiteCoordinator__r.Profile.Name,
                            VTD1_Case__r.VTR2_Backup_Site_Coordinator__r.Profile.Name,
                            Unscheduled_Visits__r.VTR2_SiteCoordinator__r.Profile.Name,
                            Unscheduled_Visits__r.VTR2_Backup_Site_Coordinator__r.Profile.Name,
                            RecordType.DeveloperName,
                            VTD1_Case__r.VTD1_Study__r.VTD1_Protocol_Nickname__c,
                            Unscheduled_Visits__r.VTD1_Study__r.VTD1_Protocol_Nickname__c,
                            VTR3_Previous_Schedule_Date__c,
                            VTD1_Unscheduled_Visit_Type__c
                    FROM VTD1_Actual_Visit__c
                    WHERE Id = :visitId
            ];
            visitMember = new Visit_Member__c(VTD1_Actual_Visit__r = visit, VTD1_Actual_Visit__c = visitId);
            if (isVisitMemberRemoved) {
                visitMember.VTD1_Participant_User__r = recipient;
            } else {
                if (visit.VTD1_Case__c != null) {
                    visitMember.VTD1_Participant_User__r = visit.VTD1_Case__r.VTR2_SiteCoordinator__r;
                    visitMember.VTD1_Participant_User__c = visit.VTD1_Case__r.VTR2_SiteCoordinator__c;
                    if (visitMember.VTD1_Participant_User__r == null) {
                        visitMember.VTD1_Participant_User__r = visit.VTD1_Case__r.VTR2_Backup_Site_Coordinator__r;
                        visitMember.VTD1_Participant_User__c = visit.VTD1_Case__r.VTR2_Backup_Site_Coordinator__c;
                    }
                } else if (visit.Unscheduled_Visits__c != null) {
                    visitMember.VTD1_Participant_User__r = visit.Unscheduled_Visits__r.VTR2_SiteCoordinator__r;
                    visitMember.VTD1_Participant_User__c = visit.Unscheduled_Visits__r.VTR2_SiteCoordinator__c;
                    if (visitMember.VTD1_Participant_User__r == null) {
                        visitMember.VTD1_Participant_User__r = visit.Unscheduled_Visits__r.VTR2_Backup_Site_Coordinator__r;
                        visitMember.VTD1_Participant_User__c = visit.Unscheduled_Visits__r.VTR2_Backup_Site_Coordinator__c;
                    }
                }
            }
        }
        if (visitMember.VTD1_Participant_User__r != null) {
            language = visitMember.VTD1_Participant_User__r.LanguageLocaleKey;
            tz = TimeZone.getTimeZone(visitMember.VTD1_Participant_User__r.TimeZoneSidKey);
        }
        VT_D1_TranslateHelper.translate(new List <Visit_Member__c>{
                visitMember
        }, language);
        if (visitMember.VTD1_Actual_Visit__r.VTR2_Televisit__c && !visitMember.VTD1_Is_internal__c
                && (currentTime >= visitMember.VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c.addHours(-1) && isVisitMemberAdded
                || timeOffset == '1HourBefore')) {
            List <VTD1_Conference_Member__c> conferenceMembers = [
                    SELECT Id, VTD1_Video_Conference__c, VTD1_Televisit_Start__c,
                            VTD1_Video_Conference__r.SessionId__c
                    FROM VTD1_Conference_Member__c
                    WHERE VTD1_Visit_Member__c = :visitMemberId
            ];

            if (!conferenceMembers.isEmpty()) {
                conferenceMember = conferenceMembers[0];
            }
        }
        return visitMember;
    }
    public String getVisitDescription() {
        return getVisitMember().VTD1_Actual_Visit__r.VTD2_Common_Visit_Type__c;
    }

    public String getVisitTimeScheduled() {
        Visit_Member__c visitMember = getVisitMember();
        if (visitMember.VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c != null && visitMember.VTD1_Participant_User__r.TimeZoneSidKey != null) {
            return visitMember.VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c.format('EEEE, d-MMM-yyyy', visitMember.VTD1_Participant_User__r.TimeZoneSidKey);
        } else {
            return '';
        }
    }

    public String getReasonForParticipation() {
        Visit_Member__c visitMember = getVisitMember();
        if (visitMember.RecordTypeId != VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_VISIT_MEMBER_REGULAR) {
            return visitMember.VTR2_ReasonForParticipation__c;
        } else {
            return '';
        }
    }

    public String getSalutaion() {
        Visit_Member__c visitMember = getVisitMember();
        String salutationString = VT_D1_TranslateHelper.getLabelValue('VT_R2_VisitEmailSalutaion', language);
        if (!isVisitMemberRemoved) {
            salutationString = salutationString.replace('#Visit_participant', getRecipientName(visitMember));
        }
        return salutationString;
    }

    public String getRecipientName(Visit_Member__c visitMember) {
        String recipientName = ' ';
        String memberType = visitMember.VTD1_Member_Type__c;
        if (memberType == null) {
            memberType = visitMember.VTD1_Participant_User__r.Profile.Name;
        }
        String memberName = visitMember.Visit_Member_Name__c;
        if (memberName == null) {
            memberName = visitMember.VTD1_Participant_User__r.FirstName + ' ' + visitMember.VTD1_Participant_User__r.LastName;
        }
        if (memberType == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME || memberType == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME
                || memberType == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
            recipientName = visitMember.VTD1_Participant_User__r.FirstName;
        } else if (memberType == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) {
            recipientName = 'Dr. ' + visitMember.VTD1_Participant_User__r.LastName;
        } else {
            recipientName = visitMember.Visit_Member_Name__c;
        }
        return recipientName;
    }

    public String getMessageBody() {
        String messagePattern;
        try {
            Visit_Member__c visitMember = getVisitMember();
            isTelevisit = visitMember.VTD1_Actual_Visit__r.VTR2_Televisit__c;
            messagePattern = getLabel();

            sincerely = VT_D1_TranslateHelper.getLabelValue('VTD2_Sincerely', language);

            messagePattern = replacePatientName(messagePattern, '#Patient_Name');
            messagePattern = replaceScheduledDate(messagePattern);
            messagePattern = replaceVisitName(messagePattern);
            messagePattern = replaceStudyNicknameAndType(messagePattern);
            getJoinTelevisitText();
            getNonStudyHubText();
            getLinkToStudyHub();
            getClosingMail();
            if (isVisitMemberAdded) {

                messagePattern = messagePattern.replace('#NonStudyHub', VT_D1_TranslateHelper.getLabelValue('VTR2_NonStudyHubGreetings', language));
                messagePattern = messagePattern.replace('#ReasonForParticipation', visitMember.VTR2_ReasonForParticipation__c);
                reasonForParticipationLabel = VT_D1_TranslateHelper.getLabelValue('VTR2_ReasonForParticipation', language);
                reasonForParticipation = visitMember.VTR2_ReasonForParticipation__c;
                if (conferenceMember == null) {
                    //messagePattern += VT_D1_TranslateHelper.getLabelValue('VTR2_NonStudyHubAnnouncement', language);
                    if (isTelevisit) {
                        announcement = VT_D1_TranslateHelper.getLabelValue('VTR2_NonStudyHubAnnouncement', language);
                    } else {
                        announcement = visitMember.VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c > boundTime ? VT_D1_TranslateHelper.getLabelValue('VTR2_YouWillReceiveAnEmail1HourPrior', language) : null;
                    }
                } else {
                    televisitInvitation = VT_D1_TranslateHelper.getLabelValue('VTR2_NonStudyHubTelevisit', language);
                    if (conferenceMember.VTD1_Televisit_Start__c != null) {
//						televisitInvitation = televisitInvitation.replace('#scheduled_date', conferenceMember.VTD1_Televisit_Start__c.format('EEEE, MMMM d yyyy', tz.getID()))
//								.replace('#scheduled_time', conferenceMember.VTD1_Televisit_Start__c.format('hh:mm a', tz.getID()))
//								.replace(' #timezone', ' ' + tz.getDisplayName());
                        televisitInvitation = replaceScheduledDate(televisitInvitation);
                        joinToTelevisitLink = getJoinToTelevisitUrl();
                    }
                }
            }
            if (messagePattern.contains('#NonStudyHub')) {
                messagePattern = messagePattern.replace('#NonStudyHub', '');
            }
        } catch (Exception e) {
            System.debug('Exception VT_R2_VisitMemberNotificationController : ' + e);
        }
        emailText += messagePattern;
        return messagePattern;
    }

    public String getMessageBody(Visit_Member__c visitMember) {
        this.visitMember = visitMember;
        return getMessageBody();
    }

    public void getNonStudyHubText() {
        if (!isVisitSoftCanceled && !isVisitReqReschedule) {
            if (visitMember.VTD1_Participant_User__c != null) {
                if (mode == null) {
                    if ((visitMember.VTD1_Participant_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
                            || visitMember.VTD1_Participant_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME)
                            && (isVisitMemberAdded || isVisitCancelled || showNonStudyHubText)) {
                        NonStudyHubText = VT_D1_TranslateHelper.getLabelValue('VTR2_VisitMemberIfYouHaveAnyQuestions', language);
                    }
                } else if (mode == 'External' && timeOffset == '2DaysBefore') {
                    NonStudyHubText = VT_D1_TranslateHelper.getLabelValue('VTR2_NonStudyHubIfYouHaveQuestions', language);
                } else if (mode != 'External' /*&& timeOffset != '1HourBefore'*/ && mode != 'Primary Investigator' && mode != 'SCR') {
                    NonStudyHubText = VT_D1_TranslateHelper.getLabelValue('VTR2_NeedReschedule', language);
                }
            } else {
                NonStudyHubText = VT_D1_TranslateHelper.getLabelValue('VTR2_NonStudyHubIfYouHaveQuestions', language);
            }
        } else {
            if ((visitMember.VTD1_Participant_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
                    || visitMember.VTD1_Participant_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME)) {
                NonStudyHubText = VT_D1_TranslateHelper.getLabelValue('VTR2_VisitMemberIfYouHaveAnyQuestions', language);
            } else if (mode == 'External') {
                NonStudyHubText = VT_D1_TranslateHelper.getLabelValue('VTR2_NonStudyHubIfYouHaveQuestions', language);
            }
        }
        if (NonStudyHubText != null) {
            replacePhoneNumber(NonStudyHubText);
            if (emailText.contains('IQVIA™')) {
                NonStudyHubText = NonStudyHubText.remove('IQVIA™ ');
            }
            emailText += NonStudyHubText;
        }
    }

    public String getJoinToTelevisit() {
        String messagePattern = '';
        try {
            if (visitMember.VTD1_Actual_Visit__r.VTR2_Televisit__c && timeOffset != '2DaysBefore') {
                messagePattern = VT_D1_TranslateHelper.getLabelValue('VTR2_JoinTelevisit', language);
            }
        } catch (Exception e) {
            System.debug('VT_R2_VisitMemberNotificationController.getJoinToTelevisit error ' + e.getMessage());
        }
        return messagePattern;
    }

    public String getJoinToTelevisitUrl() {
        if (mode == 'SCR') {
            return VTD1_RTId__c.getInstance().VT_R2_SCR_URL__c + '/s';
        } else if (mode == 'Primary Investigator') {
            return VTD1_RTId__c.getInstance().PI_URL__c + '/s';
        } else if (conferenceMember == null) {
            return VTD1_RTId__c.getInstance().VTD2_Patient_Community_URL__c;
        } else {
            return ExternalVideoController.getVideoDataForExternal(conferenceMember.VTD1_Video_Conference__c, conferenceMember.VTD1_Video_Conference__r.SessionId__c, conferenceMember.Id);
        }
    }

    public void getLinkToStudyHub() {
        if (visitMember.VTD1_Participant_User__c != null) {
            visitIqvia = VT_D1_TranslateHelper.getLabelValue('VTR2_linkVisitIqvia', language);
            if (visitMember.VTD1_Participant_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) {
                linkToStudyHub = VTD1_RTId__c.getInstance().PI_URL__c + '/s';
            } else if (visitMember.VTD1_Participant_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME
                    || visitMember.VTD1_Participant_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
                linkToStudyHub = VTD1_RTId__c.getInstance().VTD2_Patient_Community_URL__c;
            } else if (visitMember.VTD1_Participant_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME) {
                linkToStudyHub = VTD1_RTId__c.getInstance().VT_R2_SCR_URL__c + '/s';
            } else {
                linkToStudyHub = VTD1_RTId__c.getInstance().VTD1_Org_Current_Instance__c;
            }
            if (!(isVisitMemberRemoved || isVisitCancelled || isVisitMemberAdded)) {
                if (mode == 'PatientCaregiver' && timeOffset == '1HourBefore' || isScheduledVisit || isRescheduledVisit) {
                    visitIqvia = VT_D1_TranslateHelper.getLabelValue('VTD2_ViewCalendarInStudyHub', language);
                    linkToStudyHub += '/calendar';
                } else if (visitMember.VTD1_Participant_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME) {
                    visitIqvia = VT_D1_TranslateHelper.getLabelValue('VTR2_linkVisitIqvia', language);
                    linkToStudyHub += '/visit?visitId=' + visitMember.VTD1_Actual_Visit__c + '&caseId=' + visitMember.VTD1_Actual_Visit__r.VTD1_Case__c;
                } else if (isVisitSoftCanceled
                        && visitMember.VTD1_Participant_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) {
                    visitIqvia = VT_D1_TranslateHelper.getLabelValue('VTR2_linkViewVisitIqviaPI', language);
                    linkToStudyHub += '/patient-menu?caseId=' + visitMember.VTD1_Actual_Visit__r.VTD1_Case__c;
                }
            }
            if (emailText.contains('IQVIA™')) {
                visitIqvia = visitIqvia.remove('IQVIA™ ');
            }
            emailText += visitIqvia;
        }
    }

//	#scheduled_date, #scheduled_time, #timezone
    public String replaceScheduledDate(String messagePattern) {
        Visit_Member__c visitMember = getVisitMember();
        if (visitMember.VTD1_Actual_Visit__c != null) {
            Datetime dd;
            dd = visitMember.VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c != null ?
                    visitMember.VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c
                    :visitMember.VTD1_Actual_Visit__r.VTR3_Previous_Schedule_Date__c;
            if (dd != null) {
                messagePattern = messagePattern.replace('#scheduled_date',
                        VT_D1_TranslateHelper.translate(dd.format('EEEE, MMMM d yyyy', tz.getID()), language))
                        .replace('#scheduled_time', VT_D1_TranslateHelper.translate(dd.format('hh:mm a', tz.getID()), language))
                        .replace(' #timezone', ' ' + VT_D1_TranslateHelper.translateTimeZone(tz.getID(), language));
            }
        }
        return messagePattern;
    }

//	#Visit_name
    public String replaceVisitName(String messagePattern) {
        Visit_Member__c visitMember = getVisitMember();
        messagePattern = messagePattern.replace('#Visit_name',
                VT_D1_HelperClass.getActualVisitName(visitMember.VTD1_Actual_Visit__r, language));
        return messagePattern;
    }

//	#Study_nickname, #Visit_type
    public String replaceStudyNicknameAndType(String messagePattern) {
        if (visitMember.VTD1_Actual_Visit__r.RecordType.DeveloperName == 'VTD1_Unscheduled') {
            Study_Nickname = visitMember.VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Study__r.VTD1_Protocol_Nickname__c;
        } else {
            Study_Nickname = visitMember.VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Study__r.VTD1_Protocol_Nickname__c;
        }
        if (Study_Nickname != null) {
            messagePattern = messagePattern.replace('#Study_nickname', Study_Nickname);
        }
        if (visitMember.VTD1_Actual_Visit__r.VTD2_Common_Visit_Type__c != null) {
            messagePattern = messagePattern.replace('#Visit_type', visitMember.VTD1_Actual_Visit__r.VTD2_Common_Visit_Type__c);
        }
        return messagePattern;
    }

//	#Patient_Name
    public String replacePatientName(String messagePattern, String patientNameTag) {
        String memberType = visitMember.VTD1_Member_Type__c;
        if (memberType == null) {
            memberType = visitMember.VTD1_Participant_User__r.Profile.Name;
        }
        if (visitMember.VTD1_Actual_Visit__c != null) {
            String patientName;
            if (visitMember.VTD1_Member_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME) {
                patientName = visitMember.VTD1_Actual_Visit__r.VTD1_Contact__r.Name;
            } else if (visitMember.VTD1_Member_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME ||
                    visitMember.VTD1_Member_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME ||
                    visitMember.VTD1_Participant_User__r == null) {
                patientName = visitMember.VTD1_Actual_Visit__r.VTD2_Patient_ID__c;
            } else if (memberType == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME) {
                patientName = visitMember.VTD1_Actual_Visit__r.VTD2_Patient_ID__c;
            } else {
                patientName = visitMember.VTD1_Actual_Visit__r.VTD2_PatientName_PatientId__c;
            }
            if(patientName == null){
                patientName = '';
            }
            messagePattern = messagePattern.replace(patientNameTag, patientName);
        }
        return messagePattern;
    }

//	#Phone_number
    public String replacePhoneNumber(String messagePattern) {
        if (visitMember.VTD1_Participant_User__c != null) {
            if (visitMember.VTD1_Actual_Visit__r.VTD1_Case__r.VTR2_StudyPhoneNumber__c != null) {
                NonStudyHubText = NonStudyHubText.replace('#Phone_number', visitMember.VTD1_Actual_Visit__r.VTD1_Case__r.VTR2_StudyPhoneNumber__r.Name);
            } else if (visitMember.VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTR2_StudyPhoneNumber__c != null) {
                NonStudyHubText = NonStudyHubText.replace('#Phone_number', visitMember.VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTR2_StudyPhoneNumber__r.Name);
            } else if (visitMember.VTD1_Actual_Visit__r.VTD2_Study_Phone_Number__c != null) {
                NonStudyHubText = NonStudyHubText.replace('#Phone_number', visitMember.VTD1_Actual_Visit__r.VTD2_Study_Phone_Number__c);
            }
        } else {
            Id studyId;
            if (visitMember.VTD1_Actual_Visit__r.VTD1_Case__c != null) {
                studyId = visitMember.VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Study__c;
            } else {
                studyId = visitMember.VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Study__c;
            }
            List<VTR2_StudyPhoneNumber__c> phones = [
                    SELECT Name, VTR2_Study__c, VTR2_Language__c
                    FROM VTR2_StudyPhoneNumber__c
                    WHERE VTR2_Study__c = :studyId
                    AND VTR2_Language__c = 'en_US'
                    ORDER BY CreatedDate DESC
                    LIMIT 1
            ];
            if (phones.size() > 0) {
                NonStudyHubText = NonStudyHubText.replace('#Phone_number', phones[0].Name);
            }
        }
        NonStudyHubText = NonStudyHubText.replace('#Phone_number', '');
        return messagePattern;
    }

//	Get label
    public String getLabel() {
        String labelText = '';
        if (mode != null || timeOffset != null) {
            if (mode == 'PatientCaregiver' && timeOffset == '1HourBefore') {
                labelText = VT_D1_TranslateHelper.getLabelValue('VTR2_ReminderPatient1Hour', language);
            } else if (mode == 'PatientCaregiverPSC' && timeOffset == '1HourBefore') {
                labelText = VT_D1_TranslateHelper.getLabelValue('VTR2_ReminderPatient1PSCHour', language);
            } else if (mode == 'PatientCaregiver' && timeOffset == '2DaysBefore') {
                labelText = VT_D1_TranslateHelper.getLabelValue('VTR2_ReminderPatient2Days', language);
                isTelevisit = false;
            } else if (mode == 'External' && timeOffset == '2DaysBefore') {
                labelText = VT_D1_TranslateHelper.getLabelValue('VTR2_ReminderExternal2Days', language);
            } else if (mode == 'External' && timeOffset == '1HourBefore') {
                labelText = isTelevisit ? VT_D1_TranslateHelper.getLabelValue('VTR2_ReminderExternal1Hour', language) :
                        VT_D1_TranslateHelper.getLabelValue('VTR2_ReminderExternal1HourNoTelevisit', language);
                televisitInvitation = VT_D1_TranslateHelper.getLabelValue('VTR2_NonStudyHubTelevisit', language);
            } else if (mode == 'Primary Investigator' && timeOffset == '1HourBefore') {
                labelText = isTelevisit ? VT_D1_TranslateHelper.getLabelValue('VTR2_ReminderExternal1Hour', language) :
                        VT_D1_TranslateHelper.getLabelValue('VTR2_ReminderExternal1Hour', language);
            } else if (mode == 'SCR' && timeOffset == '2HourBefore') {
                labelText = VT_D1_TranslateHelper.getLabelValue('VTR2_VisitSatrts2Hours', language);
            }
            showNonStudyHubText = true;
        } else {
            if (visitMember.VTD1_Actual_Visit__r.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED) {
                labelText = visitMember.VTD1_Member_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
                        ? VT_D1_TranslateHelper.getLabelValue('VTR2_VisitCancelledBodyPatient', language)
                        : VT_D1_TranslateHelper.getLabelValue('VTR2_VisitCancelledBody', language);
                isVisitCancelled = true;
            } else if (visitMember.VTD1_Actual_Visit__r.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED) {
                showNonStudyHubText = true;
                labelText = visitMember.VTD1_Actual_Visit__r.Sub_Type__c != 'PSC'
                        ? VT_D1_TranslateHelper.getLabelValue('VT_R2_StudyVisitCompletedBodyPatient', language)
                        : VT_D1_TranslateHelper.getLabelValue('VT_R2_StudyVisitCompletedBodyPatientPSC', language); //TODO Other profile body label
                if (visitMember.VTD1_Actual_Visit__r.VTD1_Protocol_Nickname__c != null) {
                    labelText = labelText.replace('#ProtocolNickname', visitMember.VTD1_Actual_Visit__r.VTD1_Protocol_Nickname__c);
                }
            } else if (visitMember.VTD1_Actual_Visit__r.VTD1_Status__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED) {
                labelText = VT_D1_TranslateHelper.getLabelValue('VT_R2_StudyVisitToBeScheduledBody', language);
                NonStudyHubText = VT_D1_TranslateHelper.getLabelValue('VTR2_NeedReschedule', language);
                if (visitMember.VTD1_Actual_Visit__r.VTD1_Case__r.VTR2_StudyPhoneNumber__r != null) {
                    NonStudyHubText = NonStudyHubText.replace('#Phone_number', visitMember.VTD1_Actual_Visit__r.VTD1_Case__r.VTR2_StudyPhoneNumber__r.Name);
                }
            } else if (isVisitMemberRemoved) {
                labelText = VT_D1_TranslateHelper.getLabelValue('VTR2_VisitMemberRemoved', language);
            } else if (isVisitMemberAdded) {
                labelText = VT_D1_TranslateHelper.getLabelValue('VTR2_NewVisitMember', language);
            } else if (isScheduledVisit || isRescheduledVisit) {
                labelText = isScheduledVisit ?
                            VT_D1_TranslateHelper.getLabelValue('VTR3_VisitHasBeenScheduledPatient', language)
                        :VT_D1_TranslateHelper.getLabelValue('VTR3_Visit_has_been_rescheduled_email', language);
                NonStudyHubText = VT_D1_TranslateHelper.getLabelValue('VTR2_NeedReschedule', language);
            }
        }
        if (isVisitSoftCanceled || isVisitReqReschedule) {
            isTelevisit = false;
            if (mode == 'Site Coordinator') {
                labelText = isVisitSoftCanceled ? VT_D1_TranslateHelper.getLabelValue('VTR2_RescheduleVisitPI', language) :
                        VT_D1_TranslateHelper.getLabelValue('VTR2_ReqRescheduleVisitSCR', language);
                if (!isVisitSoftCanceled) {
                    reqReachOutPatient = VT_D1_TranslateHelper.getLabelValue('VTR2_ReqReachOutPatient', language);
                }
            } else if (mode == 'Main Site Coordinator') {
                labelText = VT_D1_TranslateHelper.getLabelValue('VTR2_RescheduleVisitMainSCR', language);
                reasonForRemovalLabel = VT_D1_TranslateHelper.getLabelValue('VTR2_ReasonForRemoval', language);
                reasonForRemoval = visitMember.VTD1_Actual_Visit__r.VTR2_ReasonToRemove__c;
                reachOutPatient = VT_D1_TranslateHelper.getLabelValue('VTR2_ReachOutPatient', language);
            } else if (mode == 'Primary Investigator') {
                labelText = VT_D1_TranslateHelper.getLabelValue('VTR2_RescheduleVisitPI', language);
            } else if (mode == 'PatientCaregiver') {
                labelText = VT_D1_TranslateHelper.getLabelValue('VTR2_RescheduleVisitPTCG', language);
            } else if (mode == 'External') {
                labelText = VT_D1_TranslateHelper.getLabelValue('VTR2_RescheduleVisitExternal', language);
            }
        }
        emailText += labelText;
        return labelText;
    }

    public String getClosingMail() {
        String closingMail;
        if (mode == 'External' || !emailText.contains('IQVIA™')) {
            closingMail = VT_D1_TranslateHelper.getLabelValue('VTR2_ComplimentaryClosingEmailVisitExternal', language);
        } else {
            closingMail = VT_D1_TranslateHelper.getLabelValue('VTR2_ComplimentaryClosingEmailVisit', language);
        }
        return closingMail;
    }

    public void getJoinTelevisitText() {
        if (mode == 'External' && timeOffset == '2DaysBefore') {
            if (isTelevisit) {
                joinTelevisitText = VT_D1_TranslateHelper.getLabelValue('VTR2_NonStudyHubAnnouncement', language);
            } else {
                joinTelevisitText = VT_D1_TranslateHelper.getLabelValue('VTR2_YouWillReceiveAnEmail1HourPrior', language);
            }
        } else if ((!isVisitMemberRemoved && !isVisitCancelled && isTelevisit && !isVisitMemberAdded
                && !isReadyToBeScheduled && !isCompletedVisit && !isScheduledVisit && !isRescheduledVisit) || conferenceMember!=null) {
            joinTelevisitText = VT_D1_TranslateHelper.getLabelValue('VTR2_ToJoinTheTelevisit', language);
            joinTelevisitText = replaceScheduledDate(joinTelevisitText);
        }
    }
}