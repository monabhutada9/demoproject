public without sharing class VT_D1_MilestoneHelper {

    public static final Set<String> SUBJECT_ENROLLED_STATUS = new Set<String> {
        VT_R4_ConstantsHelper_AccountContactCase.CASE_WASHOUT_RUN_IN,
        VT_R4_ConstantsHelper_AccountContactCase.CASE_WASHOUT_RUN_IN_FAILURE,
        VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED
    };

    public static final Set<String> SITE_ACTIVATED_STATUS = new Set<String> {
        VT_R4_ConstantsHelper_AccountContactCase.SITE_ENROLLMENT_OPEN,
        VT_R4_ConstantsHelper_AccountContactCase.SITE_ENROLLMENT_CLOSED
    };

    public static final Set<String> SITE_CLOSED_STATUS = new Set<String> {
        VT_R4_ConstantsHelper_AccountContactCase.SITE_CLOSED,
        VT_R4_ConstantsHelper_AccountContactCase.SITE_PREMATURELY_CLOSED
    };

    public static final Set<String> SITE_CLOSED_OR_CLOSED_ENROLLED_STATUS = new Set<String> {
        VT_R4_ConstantsHelper_AccountContactCase.SITE_CLOSED,
        VT_R4_ConstantsHelper_AccountContactCase.SITE_PREMATURELY_CLOSED,
        VT_R4_ConstantsHelper_AccountContactCase.SITE_ENROLLMENT_CLOSED
    };

    public static final List<String> SUBJECT_LAST_STATUS = new List<String> { // List of statuses of which we want to record the last occurrence when sites are all closed
        VT_R4_ConstantsHelper_AccountContactCase.CASE_WASHOUT_RUN_IN,
        VT_R4_ConstantsHelper_AccountContactCase.CASE_WASHOUT_RUN_IN_FAILURE,
        VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED,
        VT_R4_ConstantsHelper_AccountContactCase.CASE_SCREENED
    };

    /*** Milestone Conditions ***/
    /* Patients */
    public static Boolean isSubjectEnrolled(Case patient) {
        return SUBJECT_ENROLLED_STATUS.contains(patient.Status);
    }
    public static Boolean isSubjectRandomized(Case patient) {
        return patient.Status != null && patient.Status.equalsIgnoreCase(VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED);
    }
    public static Boolean isSubjectScreened(Case patient) {
        return patient.Status != null && patient.Status.equalsIgnoreCase(VT_R4_ConstantsHelper_AccountContactCase.CASE_SCREENED);
    }
    /* Sites */
    public static Boolean isSiteActivated(Virtual_Site__c site) {
        return SITE_ACTIVATED_STATUS.contains(site.VTD1_Site_Status__c);
    }
    public static Boolean isSiteClosed(Virtual_Site__c site) {
        return SITE_CLOSED_STATUS.contains(site.VTD1_Site_Status__c);
    }
    public static Boolean isSiteClosedOrClosedEnrollment(Virtual_Site__c site) {
        return SITE_CLOSED_OR_CLOSED_ENROLLED_STATUS.contains(site.VTD1_Site_Status__c);
    }
    /* Monitoring Visits */
    public static Boolean isVisitCloseout(VTD1_Monitoring_Visit__c visit) {
        return
            visit.VTD1_Site_Visit_Type__c != null &&
            visit.VTD1_Site_Visit_Type__c.equalsIgnoreCase(VT_R4_ConstantsHelper_VisitsEvents.MONITORING_VISIT_TYPE_CLOSEOUT) &&
            visit.VTD1_Study_Site_Visit_Status__c != null &&
            visit.VTD1_Study_Site_Visit_Status__c.equalsIgnoreCase(VT_R4_ConstantsHelper_VisitsEvents.MONITORING_VISIT_COMPLETE)
        ;
    }
    public static Boolean isVisitInitiated(VTD1_Monitoring_Visit__c visit) {
        return
            visit.VTD1_Site_Visit_Type__c != null &&
            visit.VTD1_Site_Visit_Type__c.equalsIgnoreCase(VT_R4_ConstantsHelper_VisitsEvents.MONITORING_VISIT_TYPE_INITIATION) &&
            visit.VTD1_Study_Site_Visit_Status__c != null &&
            visit.VTD1_Study_Site_Visit_Status__c.equalsIgnoreCase(VT_R4_ConstantsHelper_VisitsEvents.MONITORING_VISIT_COMPLETE)
        ;
    }
    public static Boolean isVisitSelected(VTD1_Monitoring_Visit__c visit) {
        return
            visit.VTD1_Site_Visit_Type__c != null &&
            visit.VTD1_Site_Visit_Type__c.equalsIgnoreCase(VT_R4_ConstantsHelper_VisitsEvents.MONITORING_VISIT_TYPE_SELECTION) &&
            visit.VTD1_Study_Site_Visit_Status__c != null &&
            visit.VTD1_Study_Site_Visit_Status__c.equalsIgnoreCase(VT_R4_ConstantsHelper_VisitsEvents.MONITORING_VISIT_COMPLETE)
        ;
    }
    /* Documents */
    public static Boolean isApprovedEssentialDoc(VTD1_Document__c doc) {
        return
            doc.VTD1_Regulatory_Document_Type__c != null &&
            doc.VTD1_Regulatory_Document_Type__c.equalsIgnoreCase(VT_R4_ConstantsHelper_Documents.DOCUMENT_EDPRC_NAME) &&
            doc.VTD1_Status__c != null &&
            doc.VTD1_Status__c.equalsIgnoreCase(VT_R4_ConstantsHelper_Documents.DOCUMENT_SITE_RSU_COMPLETE)
        ;
    }
}