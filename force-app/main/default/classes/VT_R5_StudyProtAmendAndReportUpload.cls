/**
 * Created by Denis Belkovskii on 10/5/2020.
 * SH-17308
 * PB 'Amendments | Protocol Amendment Uploaded'
 */

public without sharing class VT_R5_StudyProtAmendAndReportUpload extends Handler {

    private static List<String> tnTaskCodes = new List<String>();
    private static List<Id> taskSourceIds = new List<Id>();

//    private static Set<VT_D2_TNCatalogNotifications.ParamsHolder> paramsHolders =
//            new Set<VT_D2_TNCatalogNotifications.ParamsHolder>();

    private static Map<Id, List<Id>> piNotificationsMap = new Map<Id, List<Id>>();
    private static Map<Id, List<Id>> pgScVtslNotificationsMap = new Map<Id, List<Id>>();

    private static final List<Id> SC_PI_PG_RECORD_TYPE_IDS = new List<Id>{
            Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('SC').getRecordTypeId(),
            Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('PI').getRecordTypeId(),
            Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('PG').getRecordTypeId()
    };

    protected override void onAfterUpdate(Handler.TriggerContext context){
       // paramsHolders.clear();
        List<HealthCloudGA__CarePlanTemplate__c> newStudies =
                (List<HealthCloudGA__CarePlanTemplate__c>) context.newList;
        Map<Id, HealthCloudGA__CarePlanTemplate__c> oldStudiesMap =
                (Map<Id, HealthCloudGA__CarePlanTemplate__c>) context.oldMap;
        List<HealthCloudGA__CarePlanTemplate__c> studiesInfo = new List<HealthCloudGA__CarePlanTemplate__c>();
        for (HealthCloudGA__CarePlanTemplate__c study : newStudies) {
            if (study.VTD1_ProtocolAmendment_DocumentsUploaded__c
                    && !oldStudiesMap.get(study.Id).VTD1_ProtocolAmendment_DocumentsUploaded__c) {
                if (study.VTD1_Virtual_Trial_Study_Lead__c != null) {
//                    VT_D2_TNCatalogNotifications.ParamsHolder paramsHolder = new VT_D2_TNCatalogNotifications.ParamsHolder();
//                    paramsHolder.tnCode = 'N555';
//                    paramsHolder.sourceId = study.Id;
//                    paramsHolder.Receiver = study.VTD1_Virtual_Trial_Study_Lead__c;
//                    paramsHolders.add(paramsHolder);

                    List<Id> receiversIds = pgScVtslNotificationsMap.get(study.Id);
                    if (receiversIds == null) {
                        receiversIds = new List<Id>();
                        pgScVtslNotificationsMap.put(study.Id, receiversIds);
                    }
                    receiversIds.add(study.VTD1_Virtual_Trial_Study_Lead__c);
                }
                studiesInfo.add(study);
            }
            if (study.VTD1_StudyStatus__c == VT_R4_ConstantsHelper_AccountContactCase.CASE_COMPLETED
                    && study.VTR5_Patient_Data_Deletion_Allowed__c
                    && (oldStudiesMap.get(study.Id).VTD1_StudyStatus__c != study.VTD1_StudyStatus__c
                    || !oldStudiesMap.get(study.Id).VTR5_Patient_Data_Deletion_Allowed__c)){
                tnTaskCodes.add('T638');
                taskSourceIds.add(study.Id);
            }
        }
        if (!studiesInfo.isEmpty()) {
            studyUploadedDocumentsNotifications(new Map<Id, HealthCloudGA__CarePlanTemplate__c>(studiesInfo));
        }
//        if (!paramsHolders.isEmpty()) {
//            VT_D2_TNCatalogNotifications.generateNotification(new List<VT_D2_TNCatalogNotifications.ParamsHolder>(paramsHolders));
//        }
        if (!piNotificationsMap.isEmpty() || !pgScVtslNotificationsMap.isEmpty()) {
            List<String> tnCatalogCodes = new List<String>();
            List<Id> sourceIds = new List<Id>();
            Map<String, List<Id>> receiversMap = new Map<String, List<Id>>();
            for (Id studyId : piNotificationsMap.keySet()) {
                tnCatalogCodes.add('N564');
                sourceIds.add(studyId);
                receiversMap.put('N564'+studyId, piNotificationsMap.get(studyId));
            }
            for (Id studyId : pgScVtslNotificationsMap.keySet()) {
                tnCatalogCodes.add('N555');
                sourceIds.add(studyId);
                receiversMap.put('N555'+studyId, pgScVtslNotificationsMap.get(studyId));
            }
            System.debug('!!!### tnCatalogCodes: ' + tnCatalogCodes);
            System.debug('!!!### sourceIds: ' + sourceIds);
            System.debug('!!!### receiversMap: ' + receiversMap);

            VT_D2_TNCatalogNotifications.generateNotifications(tnCatalogCodes, sourceIds, null, null, receiversMap);
        }
        if (!tnTaskCodes.isEmpty() && !taskSourceIds.isEmpty()){
            VT_D2_TNCatalogTasks.generateTasks(tnTaskCodes, taskSourceIds, null, null);
        }
    }

    public static void studyUploadedDocumentsNotifications(Map<Id, HealthCloudGA__CarePlanTemplate__c> studiesInfoMap) {
        List<Study_Team_Member__c> studyTeamMembers = [
                SELECT User__c, Study__c, User__r.IsActive, RecordTypeId
                FROM Study_Team_Member__c
                WHERE Study__c IN :studiesInfoMap.keySet()
                AND RecordTypeId IN :SC_PI_PG_RECORD_TYPE_IDS
        ];
        if (studyTeamMembers.isEmpty()) {
            return;
        }
        for (Study_Team_Member__c stm : studyTeamMembers) {
            if (stm.User__c == null) {
                continue;
            }
            if (stm.RecordTypeId != Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('PI').getRecordTypeId()){
                if (stm.User__c != null && stm.User__c != studiesInfoMap.get(stm.Study__c).VTD1_Virtual_Trial_Study_Lead__c){
//                    VT_D2_TNCatalogNotifications.ParamsHolder paramsHolder = new VT_D2_TNCatalogNotifications.ParamsHolder();
//                    paramsHolder.tnCode = 'N555';
//                    paramsHolder.sourceId = stm.Study__c;
//                    paramsHolder.Receiver = stm.User__c;
//                    paramsHolders.add(paramsHolder);

                    List<Id> receiversIds = pgScVtslNotificationsMap.get(stm.Study__c);
                    if (receiversIds == null) {
                        receiversIds = new List<Id>();
                        pgScVtslNotificationsMap.put(stm.Study__c, receiversIds);
                    }
                    receiversIds.add(stm.User__c);



                }
            } else {
                if (stm.User__r.IsActive) {
//                    VT_D2_TNCatalogNotifications.ParamsHolder paramsHolder = new VT_D2_TNCatalogNotifications.ParamsHolder();
//                    paramsHolder.tnCode = 'N564';
//                    paramsHolder.sourceId = stm.Study__c;
//                    paramsHolder.Receiver = stm.User__c;
//                    paramsHolders.add(paramsHolder);

                    List<Id> receiversIds = piNotificationsMap.get(stm.Study__c);
                    if (receiversIds == null) {
                        receiversIds = new List<Id>();
                        piNotificationsMap.put(stm.Study__c, receiversIds);
                    }
                    receiversIds.add(stm.User__c);

                }
            }
        }
    }
}