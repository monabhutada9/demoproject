public with sharing class VT_R5_SkillAndChatButtonUtils {
    /* 
     * method : restApiCallout(String endpoint, String body)
     * param : String endpoint - Specifies the Url endpoint.
     * param : String body - Specifies the body for the Callout.
     * Description : This method is used to make the callout uses POST method for creation of records.
     * return : HttpResponse object - Http response of the callout
     */
    public static HttpResponse restApiCallout(String endpoint, String body, String method){
        try {
            endpoint = URL.getSalesforceBaseUrl().toExternalForm() + endpoint;
            HttpRequest request = new HttpRequest();
            request.setEndpoint(endPoint);
            request.setMethod(method);
            request.setHeader('Content-Type', 'application/json;charset=UTF-8');
            request.setHeader('Accept', 'application/json');
            request.setHeader('Authorization', 'Bearer ' + VT_D1_HelperClass.getSessionIdForGuest());
            if (method.equalsIgnoreCase('POST')) {
                request.setBody(body);
            }            
            Http http = new Http();
            //Send the request (callout) & get the response.
            HttpResponse response = http.send(request);
            return response;
        } catch (CalloutException ex) {
            System.debug('CalloutException occured' + ex.getMessage());

            ErrorLogUtility.logException(ex, ErrorLogUtility.ApplicationArea.APP_AREA_CHAT_AUTOMATION, VT_R5_SkillAndChatButtonUtils.class.getName());

            return null;
        }   
    }
    /* 
     * method : createServiceResourceSkill(Id skillId, ServiceResource servResource)
     * params : skillId - for which ServiceResourceSkill object to be prepared
     * params : servResourceId - Id of the ServiceResource which is associated with ServiceResourceSkill object
     * Description : This method is used to prepare ServiceResourceSkill.
     */
    public static ServiceResourceSkill createServiceResourceSkill(Id skillId, Id servResourceId){
        ServiceResourceSkill serResSkill = new ServiceResourceSkill();
        serResSkill.SkillId = skillId;
        if (servResourceId != null) {
            serResSkill.ServiceResourceId = servResourceId;
        }
        serResSkill.SkillLevel = 10;
        serResSkill.EffectiveStartDate = System.now();
        return serResSkill;
    }
    /* 
     * method : createServiceResource(Study_Team_Member__c stm)
     * params : stm - Study team Mmber for which Service resource to be created.
     * Description : This method is used to prepare ServiceResource.
     */
    public static ServiceResource createServiceResource(User usr){
        ServiceResource newServiceResource = new ServiceResource();
        newServiceResource.Name = 'SR-SC_' + usr.VTD1_Full_Name__c;
        newServiceResource.IsActive = true;
        newServiceResource.ResourceType = 'A';
        newServiceResource.RelatedRecordId = usr.Id;
        newServiceResource.Description = 'Service Resource for User Id : ' + usr.Id;
        return newServiceResource;
    } 
    /* 
     * method : createServiceResourcesForUsers(Map<Id, User> mapSCUser)
     * params : mapSCUser - map of Users 
     * Description : This method is used to create the ServiceResources for all the users.
     */  
    public static Map<Id, ServiceResource> createServiceResourcesForUsers(Map<Id, User> mapSCUser){

              

        List<ServiceResource> lstServiceResource = [
                            SELECT Id, RelatedRecordId, (SELECT Id, SkillId FROM ServiceResourceSkills) 
                            FROM ServiceResource 
                            WHERE ResourceType = 'A' AND RelatedRecordId IN : mapSCUser.keySet()];
        System.debug('SRS : '+ lstServiceResource);
        Set<Id> setSRExist = new Set<Id>();
        for (ServiceResource sr : lstServiceResource) {
            setSRExist.add(sr.RelatedRecordId);
        }
        List<ServiceResource> lstServResToBeInserted = new List<ServiceResource>();
        for (User usr : mapSCUser.values()) {
            System.debug('User ID : ' + usr.Id);
            System.debug('Is SR Exist : ' + setSRExist.contains(usr.Id));
            if (!setSRExist.contains(usr.Id)) {
                    ServiceResource newServiceResource = VT_R5_SkillAndChatButtonUtils.createServiceResource(usr);
                    lstServResToBeInserted.add(newServiceResource); 
                }
            }
            if (!lstServResToBeInserted.isEmpty()) {
            System.debug('SR to be inserted '+ lstServResToBeInserted);
                insert lstServResToBeInserted;
                lstServiceResource.clear();
                lstServiceResource = [
                            SELECT Id, RelatedRecordId, (SELECT Id, SkillId FROM ServiceResourceSkills) 
                            FROM ServiceResource 
                        WHERE ResourceType = 'A' AND RelatedRecordId IN : mapSCUser.keySet()];
            }
        Map<Id, ServiceResource> mapSCUserServiceResource = new Map<Id, ServiceResource>();
        for (ServiceResource sr : lstServiceResource) {
            mapSCUserServiceResource.put(sr.RelatedRecordId, sr);
        }
        return mapSCUserServiceResource;
    }   
}