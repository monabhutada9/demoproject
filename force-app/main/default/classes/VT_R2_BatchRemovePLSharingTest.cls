/**
* @author: Carl Judge
* @date: 25-Sep-19
* @description: 
**/

@isTest
private class VT_R2_BatchRemovePLSharingTest {

    @testSetup
    static void setup() {
        insert new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Project Lead'].Id,
            FirstName = 'ProjectLeadTestName',
            LastName = 'L' ,
            Email = VT_D1_TestUtils.generateUniqueUserName(),
            Username = VT_D1_TestUtils.generateUniqueUserName(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            IsActive=true
        );
    }

    @isTest
    private static void doTest(){
        Case pcf = new Case(RecordTypeId = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF);
        insert pcf;
        insert new CaseShare(
            CaseId = pcf.Id,
            CaseAccessLevel = 'Edit',
            UserOrGroupId = [SELECT Id FROM User WHERE Profile.Name = 'Project Lead' ORDER BY CreatedDate DESC LIMIT 1].Id
        );
        Test.startTest();
        Database.executeBatch(new VT_R2_BatchRemovePLSharing());
        Test.stopTest();
    }
}