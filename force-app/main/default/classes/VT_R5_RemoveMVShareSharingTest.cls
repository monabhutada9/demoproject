/********************************************************
* @Class Name   : VT_R5_RemoveMVShareSharingTest
* @Created Date : 15/10/2020 
* @Version      : R5.5
* @group-content: http://jira.quintiles.net/browse/SH-17314
* @Purpose      : This is the test class for the class VT_R5_RemoveMVShareSharing
* *****************************************************/
@IsTest
public class VT_R5_RemoveMVShareSharingTest { 
    @testSetup
    private static void testData(){
        

        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1); 
        

    }
    
    @isTest
    private static void testShareRecord(){
        

        Virtual_Site__c vs = [SELECT Id, 
                             VTD1_Study__c
                             FROM Virtual_Site__c
                             LIMIT 1];
        
       
        List<Study_Team_Member__c> studyTeamMembers = [select Id, 
                                                       			RecordType.Name, 
                                                       			Study__c, User__c, 
                                                       			User__r.Profile.Name 
                                                       from Study_Team_Member__c 
                                                       where Study__c = :vs.VTD1_Study__c AND
                                                        User__r.Profile.Name = 'CRA' LIMIT 1];  

          Study_Site_Team_Member__c objSSTM = new Study_Site_Team_Member__c(
          VTR5_Associated_CRA__c = studyTeamMembers[0].Id,
              VTD1_SiteID__c = vs.ID
          );
        Insert objSSTM; 
        HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
        objVirtualShare.ParentId = vs.VTD1_Study__c;
        objVirtualShare.UserOrGroupId = studyTeamMembers[0].User__c;
        objVirtualShare.AccessLevel = 'Edit';
        insert objVirtualShare;
          VTD1_Monitoring_Visit__c mv  = new VTD1_Monitoring_Visit__c();
            mv.VTD1_Site_Visit_Type__c ='Interim Visit';
            mv.VTD1_Virtual_Site__c = vs.Id;
            mv.VTD1_Study__c = vs.VTD1_Study__c;
        	mv.VTR5_VisitCRAUser__c = studyTeamMembers[0].User__c;
            mv.VTD1_Visit_Start_Date__c = System.now().addHours(2);
            
            System.runAs(new User(Id = studyTeamMembers[0].User__c)){
                insert mv;
                Test.StartTest();
                Database.executeBatch(new VT_R5_RemoveMVShareSharing());
                Test.StopTest();
            }
           
        
        List<VTD1_Monitoring_Visit__share>lstToCheck = new List<VTD1_Monitoring_Visit__share>();
        FOr(VTD1_Monitoring_Visit__share objShare:[SELECT ID,
                          rowcause,
                          UserOrGroupId,
                          Parent.VTD1_Virtual_Site__r.VTR2_Onsite_CRA__c,
                          Parent.VTD1_Virtual_Site__r.VTD1_Remote_CRA__c
                          from VTD1_Monitoring_Visit__share
                          where UserOrGroupId IN (Select User__c
                                                  from Study_Team_Member__c
                                                  where VTD1_Type__c='CRA')
                          AND rowCause ='GlobalSharing__c' 
                          AND Parent.VTD1_Virtual_Site__r.VTR2_Onsite_CRA__c != Null
                           AND Parent.VTD1_Virtual_Site__r.VTD1_Remote_CRA__c != Null]){

                            if(objShare != Null && objShare.UserOrGroupId != objShare.Parent.VTD1_Virtual_Site__r.VTR2_Onsite_CRA__c && 
                            objShare.UserOrGroupId != objShare.Parent.VTD1_Virtual_Site__r.VTD1_Remote_CRA__c){
                                lstToCheck.add(objShare);
                            }
                           }
        System.assertEquals(0,lstToCheck.size());
        
      }


}