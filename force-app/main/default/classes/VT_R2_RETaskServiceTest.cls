@IsTest
private class VT_R2_RETaskServiceTest {

    @testSetup
    static void setup() {

        DomainObjects.VTD2_TN_Catalog_Code_t tnCatalogCode_t = new DomainObjects.VTD2_TN_Catalog_Code_t()
                .setVTD2_T_Task_Unique_Code('T550')
                .setRecordTypeId(Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Task').getRecordTypeId())
                .setVTD2_T_SObject_Name('Case')
                .setVTD2_T_Patient('Case.Id')
                .setVTD2_T_Care_Plan_Template('Case.VTD1_Study__c')
                .setVTD2_T_Related_To_Id('Case.Id')
                .setVTD2_T_Task_Record_Type_ID(Task.getSObjectType().getDescribe().getRecordTypeInfosByName().get('Task For RE').getRecordTypeId())
                .setVTR2_Name_ID('Case.ContactId')
                .setVTD2_T_Subject('test');
        tnCatalogCode_t.persist();


        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );


        DomainObjects.User_t user = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5))
                .setEmail('test@test2019.test');

        DomainObjects.User_t user1 = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5))
                .setEmail('test@test2020.test');

        new DomainObjects.Case_t()
                .addStudy(study)
                .addUser(user)
                .persist();

        new DomainObjects.Case_t()
                .addStudy(study)
                .addUser(user1)
                .persist();

    }

    @IsTest
    static void testPositiveBatchExecution() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];
        User user = [SELECT Id FROM User WHERE Email = 'test@test2019.test' LIMIT 1];
        new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('RE')
                .setUser(user)
                .setStudy(study.Id)
                .persist();

        Test.startTest();
        Database.executeBatch(new VT_R2_RETaskService());
        Test.stopTest();

        System.assertEquals(2, [SELECT Id FROM Task].size());
    }

    @IsTest
    static void testNegativeBatchExecutionWithoutStm() {
        Test.startTest();
        Database.executeBatch(new VT_R2_RETaskService());
        Test.stopTest();

        System.assertEquals(0, [SELECT Id FROM Task].size());
    }

    @IsTest
    static void testPositiveBatchWithBigData() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];
        List<User> users = [SELECT Id FROM User];
        List<Case> cases = new List<Case>();
        List<Study_Team_Member__c> stms = new List<Study_Team_Member__c>();
        for (Integer i = 0; i < 100; i++) {
            stms.add((Study_Team_Member__c )new DomainObjects.StudyTeamMember_t()
                    .setRecordTypeByName('RE')
                    .setUserId(users[i < 50 ? 0 : 1].Id)
                    .setStudyId(study.Id)
                    .toObject()
            );
            cases.add((Case) new DomainObjects.Case_t()
                    .setStudy(study.Id)
                    .setUser(users[i < 50 ? 0 : 1].Id)
                    .toObject()
            );
        }
        insert stms;
        insert cases;
        Long startTime = Datetime.now().getTime();
        Test.startTest();
        Database.executeBatch(new VT_R2_RETaskService());
        Test.stopTest();
        System.debug('after batch completed* ' + (Datetime.now().getTime() - startTime));
    }

    @IsTest
    static void testPositiveBatchWithOpenTask() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];
        User user = [SELECT Id FROM User WHERE Email = 'test@test2019.test' LIMIT 1];
        Case cas = [SELECT Id FROM Case LIMIT 1];
        new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('RE')
                .setUser(user)
                .setStudy(study.Id);

        DomainObjects.Task_t task = new DomainObjects.Task_t()
                .setUserId(user.Id)
                .setCaseId(cas.Id)
                .setStudyId(study.Id)
                .setRecordTypeByName('VTR2_RE_Task')
                .setStatus('Open');
        task.persist();

        Test.startTest();
        Database.executeBatch(new VT_R2_RETaskService());
        Test.stopTest();

        System.assertEquals(2, [SELECT Id FROM Task].size());
    }

    @IsTest
    static void testPositiveTaskClose() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];
        User user = [SELECT Id,First_Log_In__c FROM User WHERE Email = 'test@test2019.test' LIMIT 1];
        Case cas = [SELECT Id FROM Case LIMIT 1];

        DomainObjects.Task_t task = new DomainObjects.Task_t()
                .setUserId(user.Id)
                .setCaseId(cas.Id)
                .setStudyId(study.Id)
                .setRecordTypeByName('VTR2_RE_Task')
                .setStatus('Open');
        task.persist();

        user.First_Log_In__c = Date.today();
        Test.startTest();
        update user;
        Test.stopTest();

        System.assertEquals(1, [SELECT Id FROM Task WHERE Status = 'Completed'].size());
    }

}