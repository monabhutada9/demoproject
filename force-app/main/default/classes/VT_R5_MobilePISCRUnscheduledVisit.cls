/**
 * Created by Yuliya Yakushenkova on 11/20/2020.
 */

public class VT_R5_MobilePISCRUnscheduledVisit extends VT_R5_MobileRestResponse implements VT_R5_MobileLibrary.Routable {

    private transient RequestParams requestParams = new RequestParams();

    public List<String> getMapping() {
        return new List<String>{
                '/mobile/{version}/unscheduled-visit/{action}'
        };
    }

    public VT_R5_MobileRestResponse versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        String requestAction = parameters.get('action');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        if (requestAction == 'participants') {
                            return get_v1_participants();
                        } if (requestAction == 'timeslots') {
                            return get_v1_time_slots();
                        } if (requestAction == 'types') {
                            // TODO:
                        }
                    }
                }
            }
            when 'POST' {
                switch on requestVersion {
                    when 'v1' {
                        switch on requestAction {
                            when 'schedule' {
                                return post_v1_schedule();
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    public void initService() {
        parseBody();
        parseParams();
    }

    public Map<String, String> getMinimumVersions() {
        return null;
    }

    public VT_R5_MobileRestResponse get_v1_participants() {
        return this.buildResponse(
                VT_R5_PISCRCalendarService.getInstance().getAllPossibleParticipantsByCaseId(requestParams.caseId)
        );
    }

    public VT_R5_MobileRestResponse get_v1_time_slots() {
        return this.buildResponse(
                new VT_R5_VisitTimeSlotsService.DateTimeSlotsHelper(
                        null, requestParams.members, requestParams.preferredDate
                )
                        .getDateTimeSlots()
        );
    }

    public VT_R5_MobileRestResponse post_v1_schedule() {
        try {
            VT_R5_PISCRCalendarService.Visit visit =
                    VT_R5_PISCRCalendarService.getConvertedVisit(JSON.serialize(requestParams.visit));
            List<VT_R5_PISCRCalendarService.Participant> externalParticipants =
                    VT_R5_PISCRCalendarService.getConvertedParticipants(JSON.serialize(requestParams.visit.externalParticipants));

            VT_R5_PISCRCalendarService.getInstance()
                    .scheduleVisit(requestParams.caseId, visit, externalParticipants, requestParams.visit.members);

            return this.buildResponse('GOOD'); // TODO: add label
        } catch (DmlException e) {
            e.setMessage('BAD');
            return this.buildResponse(e); // TODO: add label
        } catch (VT_R5_PISCRCalendarService.CalendarServiceException e) {
            e.setMessage('Datetime not available');
            return this.buildResponse(e); // TODO: add label
        }
    }

    private void parseBody() {
        if (request.requestBody.size() > 0) {
            this.requestParams = (RequestParams) JSON.deserialize(request.requestBody.toString(), RequestParams.class);
        }
    }

    private void parseParams() {
        if (request.params.containsKey('members')) this.requestParams.setMembers(request.params.get('members'));
        if (request.params.containsKey('caseId')) this.requestParams.setCaseId(request.params.get('caseId'));
        if (request.params.containsKey('preferredDate')) this.requestParams.setPreferredDate(request.params.get('preferredDate'));
    }

    private class RequestParams {
        private String caseId;
        private Date preferredDate;
        private Set<Id> members = new Set<Id>();
        private Visit visit;


        private void setMembers(String members) {
            this.members = new Set<Id>();
            for (String member : members.split(';')) {
                this.members.add(member);
            }
        }

        private void setCaseId(String caseId) {
            this.caseId = caseId;
        }

        private void setPreferredDate(String preferredDate) {
            this.preferredDate = Date.valueOf(preferredDate);
        }
    }

    private class Visit {
        private String name;
        private String reason;
        private String duration;
        private String instructions;
        private String scheduledDateTime;
        private String unscheduledVisitType;
        private String modality;
        private Boolean isTelevisit;
        private Set<Id> members = new Set<Id>();
        private List<ExternalParticipant> externalParticipants;
    }

    private class ExternalParticipant {
        private String name;
        private String email;
    }
}