public with sharing class VTR2_SCR_ProfileController {

    @AuraEnabled
    public static Object getCurrentUserScrProfile() {
        try {
            return new ScrProfile();
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static void updateCurrentUserScrProfile(String profileToUpdate) {
        System.debug('profileToUpdate: ' + profileToUpdate);
        try {
            VTR2_SCR_ProfileController.ProfileToUpdate profile = (VTR2_SCR_ProfileController.ProfileToUpdate) JSON.deserialize(profileToUpdate, VTR2_SCR_ProfileController.ProfileToUpdate.class);
            profile.updateProfile();
        } catch (Exception e) {
            System.debug('Exception: ' + e);
            AuraHandledException ex = new AuraHandledException(e.getMessage());
            throw ex;
        }
    }

    private class ScrProfile {

        @AuraEnabled
        public User user;

        @AuraEnabled
        public Contact contact;

        @AuraEnabled
        public List<VT_D1_Phone__c> contactPhoneList;

        @AuraEnabled
        public List<DropdownOption> phoneTypeOptions;

        @AuraEnabled
        public List<PrimaryInvestigator> primaryInvestigatorList;

        @AuraEnabled
        public List<DropdownOption> userTimeZoneOptions;

        @AuraEnabled
        public Boolean smsVisitRemindersAvailable = false;

        @AuraEnabled
        public Boolean smsNewMessagesAvailable = false;

        public ScrProfile() {
            this.setCurrentUser();
            this.populateContact();
            this.populateContactPhoneList();
            this.populatePhoneTypeOptionList();
            this.populateUserTimeZoneOptionList();
            this.populatePrimaryInvestigatorList();
            this.populateSMSCategoriesAvailability();
        }


        private void setCurrentUser() {
            this.user = [
                    SELECT
                        Id
                        , Title
                        , ContactId
                        , Email
                        , TimeZoneSidKey
                        , VTD1_AccountId__c
                    FROM User
                    WHERE Id = :UserInfo.getUserId()
            ];
        }

        private void populateContact() {
            List<Contact> contacts = [
                    SELECT
                            Id
                            , FirstName
                            , VTD1_MiddleName__c
                            , LastName
                            , Title
                            , VTD1_Suffix__c
                            , Email
                            , VTR2_Primary_Language__c
                            , VTR2_Receive_SMS_Visit_Reminders__c
                            , VTR2_Receive_SMS_New_Message__c
                    FROM Contact
                    WHERE Id = :this.user.ContactId
            ];
            if (!contacts.isEmpty()) {
                this.contact = contacts[0];
            }
        }

        private void populateContactPhoneList() {
            this.contactPhoneList = [
                    SELECT
                        Id
                        , PhoneNumber__c
                        , Type__c
                        , IsPrimaryForPhone__c
                        , VTR2_OptInFlag__c
                        , VTR2_SMS_Opt_In_Status__c
                    FROM VT_D1_Phone__c
                    WHERE VTD1_Contact__c = :this.user.ContactId
            ];
        }

        private void populatePhoneTypeOptionList() {
            DropdownOption dOption = new DropdownOption();
            this.phoneTypeOptions = dOption.getPhoneTypePicklistValues();
        }

        private void populateUserTimeZoneOptionList() {
            DropdownOption dOption = new DropdownOption();
            this.userTimeZoneOptions = dOption.getUserTimeZonePicklistValues(user);
        }

        private void populatePrimaryInvestigatorList() {
            PrimaryInvestigator pi = new PrimaryInvestigator();
            this.primaryInvestigatorList = pi.getPrimaryInvestigatorsForScr(this.user.Id);
        }

        private void populateSMSCategoriesAvailability() {
            Boolean stmSmsNewMessagesAvailable = false;
            Boolean stmSmsVisitRemindersAvailable = false;
            Boolean phoneSmsNewMessagesAvailable = false;
            Boolean phoneSmsVisitRemindersAvailable = false;
            for (Study_Team_Member__c stm : [
                    SELECT Study__r.VTR2_Send_New_Messages__c, Study__r.VDT2_Send_Visit_Reminders__c
                    FROM Study_Team_Member__c
                    WHERE User__c =: UserInfo.getUserId()
                    AND (Study__r.VDT2_Send_Visit_Reminders__c = true OR Study__r.VTR2_Send_New_Messages__c = true)
                    AND (VTD1_Type__c =: VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME) ]) {
                    if (stm.Study__r.VTR2_Send_New_Messages__c) {
                        stmSmsNewMessagesAvailable = true;
                    }
                    if (stm.Study__r.VDT2_Send_Visit_Reminders__c) {
                        stmSmsVisitRemindersAvailable = true;
                    }
            }
            for (VT_D1_Phone__c p : this.contactPhoneList) {
                if (p.VTR2_SMS_Opt_In_Status__c == 'Active') {
                    phoneSmsNewMessagesAvailable = true;
                    phoneSmsVisitRemindersAvailable = true;
                    break;
                }
            }

            this.smsNewMessagesAvailable = (stmSmsNewMessagesAvailable && phoneSmsNewMessagesAvailable);
            this.smsVisitRemindersAvailable = (stmSmsVisitRemindersAvailable && phoneSmsVisitRemindersAvailable);
        }
    }

    private without sharing class PrimaryInvestigator {

        @AuraEnabled
        public String name;

        @AuraEnabled
        public String researchFacility;

        @AuraEnabled
        public String department;

        @AuraEnabled
        public Virtual_Site__c address;


        private List<PrimaryInvestigator> getPrimaryInvestigatorsForScr(Id scrUserId) {
            List<PrimaryInvestigator> piList = new List<VTR2_SCR_ProfileController.PrimaryInvestigator>();
            Set<String> foundAddresses  = new Set<String>();
            for (Study_Site_Team_Member__c sstm : [
                    SELECT
                            VTR2_Associated_PI__c
                            , VTR2_Associated_PI__r.User__r.Name
                            , VTR2_Associated_PI__r.User__r.Department
                            , VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTD1_Research_Facility_Id__r.Name
                            , VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTD1_Address_Line_1__c
                            , VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTD1_City__c
                            , VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTD1_ZIP__c
                            , toLabel(VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTR3_Country__c)
                            , toLabel(VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTD1_State__c)
                            , VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTR3_AddressLine_2__c
                            , VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTR3_AddressLine_3__c
                            , VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTR3_Department__c
                            , VTR2_Associated_PI__r.VTD1_UserContact__r.Account.Name
                            , VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTD1_Study_Site_Number__c
                    FROM Study_Site_Team_Member__c
                    WHERE VTR2_Associated_SCr__r.User__c = :scrUserId
                    AND VTR2_Associated_PI__c != NULL
            ]) {

                List<String> addrressList = new List<String>{
                        sstm.VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTD1_Address_Line_1__c,
                        sstm.VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTR3_AddressLine_2__c,
                        sstm.VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTR3_AddressLine_3__c,
                        sstm.VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTD1_Study_Site_Number__c,
                        sstm.VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTR3_Department__c,
                        sstm.VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTD1_City__c,
                        sstm.VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTR3_Country__c,
                        sstm.VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTD1_State__c,
                        sstm.VTR2_Associated_PI__r.VTD1_VirtualSite__r.VTD1_ZIP__c
                };
                String foundAddress = String.join(addrressList,'-');
                if(foundAddresses.contains(foundAddress)) {
                    continue;
                }
                foundAddresses.add(foundAddress);
                PrimaryInvestigator pi = new PrimaryInvestigator();
                pi.name = sstm.VTR2_Associated_PI__r.User__r.Name;
                pi.researchFacility = sstm.VTR2_Associated_PI__r.VTD1_UserContact__r.Account.Name;
                pi.department = sstm.VTR2_Associated_PI__r.User__r.Department;
                pi.address = sstm.VTR2_Associated_PI__r.VTD1_VirtualSite__r;
                piList.add(pi);
            }
            System.debug('addressResult ' + foundAddresses);
            return piList;
        }
    }

    private class DropdownOption {

        @AuraEnabled
        public String label;

        @AuraEnabled
        public String key;


        private List<DropdownOption> getUserTimeZonePicklistValues(User user) {
            return modifyTimeZoneLabels(getDropdownOptionList(User.getSObjectType(), 'TimeZoneSidKey', false), user);
        }

        private List<DropdownOption> modifyTimeZoneLabels(List<DropdownOption> languageList, User user){
            String regex='^(?:[(]){1}[^(]*';
            Map<String, DropdownOption> newLanguageMap = new Map<String, DropdownOption>();
            for(DropdownOption lang: languageList){
                Pattern pattern = Pattern.compile(regex);
                if(pattern != null && lang.label != null) {
                    Matcher matcher = pattern.matcher(lang.label);
                    Boolean result = matcher.find();
                    if (result) {
                        lang.label = lang.label.trim().substring(matcher.start(),matcher.end());
                    }
                }
                if (newLanguageMap.containsKey(lang.label)/* || lang.key == user.TimeZoneSidKey*/) {
                    //newLanguageSet.add(lang);
                    newLanguageMap.get(lang.label).key += ';' + lang.key;
                } else {
                    newLanguageMap.put(lang.label, lang);
                }
            }
            return new List<DropdownOption>(newLanguageMap.values());
        }

        private List<DropdownOption> getPhoneTypePicklistValues() {
            return getDropdownOptionList(VT_D1_Phone__c.getSObjectType(), 'Type__c', true);
        }

        private List<DropdownOption> getDropdownOptionList(Schema.SObjectType objType, String fld, Boolean addEmptyOption) {
            List<DropdownOption> optionList = new List<VTR2_SCR_ProfileController.DropdownOption>();
            Map<String, String> optionMap = getSelectOptionsWithLabel(objType, fld);
            for (String option : optionMap.keySet()) {
                DropdownOption dOption = new DropdownOption();
                dOption.key = option;
                dOption.label = optionMap.get(option);
                optionList.add(dOption);
            }
            if (addEmptyOption) {
                DropdownOption dOption = new DropdownOption();
                dOption.key = null;
                dOption.label = null;
                optionList.add(dOption);
            }

            return optionList;
        }

        private Map<String, String> getSelectOptionsWithLabel(Schema.SObjectType objType, String fld) {
            Map<String, String> allOptsMap = new Map<String, String>();
            Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
            Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
            List<Schema.PicklistEntry> values = fieldMap.get(fld).getDescribe().getPickListValues();
            for (Schema.PicklistEntry a: values) {
                allOptsMap.put(a.getValue(),a.getLabel());
            }

            Map<String, String> sortedMap = new Map<String, String>();
            Set<String> keySet = allOptsMap.keySet();
            List<String> keyList = new List<String>();
            keyList.addAll(keySet);
            keyList.sort();
            for (Integer i = 0; i < keyList.size(); i++) {
                sortedMap.put(keyList[i], allOptsMap.get(keyList[i]));
            }
            return sortedMap;
        }

        public Boolean equals(Object o) {
            if (this.label == ((DropdownOption)o).label) {
                return true;
            }
            return false;
        }
        public override Integer hashCode() {
            return this.label.hashCode();
        }
    }

    @AuraEnabled
    public static void optInRemote (String phoneId) {
        VT_R2_NotificationCSMSProcessor.optInRemote(phoneId);
    }

    @AuraEnabled
    public static void resendOptInRemote (String phoneId) {
        VT_R2_NotificationCSMSProcessor.resendOptInRemote(phoneId);
    }

    @AuraEnabled
    public static void optOutRemote (String phoneId) {
        VT_R2_NotificationCSMSProcessor.optOutRemote(phoneId);
    }

    @TestVisible
    private without sharing class ProfileToUpdate {

        @TestVisible
        private User user;

        private Contact contact;

        @TestVisible
        private List<VT_D1_Phone__c> phonesToUpsert;

        @TestVisible
        private List<VT_D1_Phone__c> phonesToDelete;

        public void updateProfile() {

            System.debug(user);
            for (VT_D1_Phone__c phone : phonesToDelete) {
                System.debug(phone);
            }
            for (VT_D1_Phone__c phone : phonesToUpsert) {
                System.debug(phone);
            }

            delete phonesToDelete;
            try{
                upsert phonesToUpsert;
            }
            catch (Exception e){
                String errorMsg = e.getMessage();
                if (errorMsg.contains(Label.VTR2_ValidationSameMobileNumber)) {
                    errorMsg = Label.VTR2_ValidationSameMobileNumber;
                }
                AuraHandledException ex = new AuraHandledException(errorMsg);
                ex.setMessage(errorMsg);
                throw ex;
            }
            //upsert phonesToUpsert;
            List <String> timeZones = new List<String>();
            if (user != null && String.isNotEmpty(user.TimeZoneSidKey)) {
                timeZones = user.TimeZoneSidKey.split(';');
            }
            if (!timeZones.isEmpty()) {
                user.TimeZoneSidKey = timeZones[0];
            }
            if (user != null) {
                update user;
            }
            if (contact != null) {
                update contact;
            }
        }
    }
}