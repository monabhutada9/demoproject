/**
 * Created by Andrey Pivovarov on 4/17/2020.
 */

global with sharing class VT_R4_VideoConferenceScheduler implements Schedulable {
    private static List <Integer> minuteBounds = new List<Integer>{0, 10, 25, 40, 55, 60};
    private static List <Integer> minuteBoundsOffset = new List<Integer>{10, 25, 40, 55, 10};
    private static List <Integer> hourBoundsOffset = new List<Integer>{0, 0, 0, 0, 1};

    Datetime dt;
    private Id jobId;
    private static String nameSchedule = 'VideoConferenceScheduler To Heroku';

    global void execute(SchedulableContext sc) {
        this.jobId = sc.getTriggerId();
        Database.executeBatch(new VT_R4_VideoConferenceBatch(this, dt),200);
    }

    public Datetime getAlignedTimeJob(Datetime dt) {
        if (dt == null) {
            dt = System.now();
        }
        Integer minutes = dt.minute();
        Integer hours = dt.hour();
        List <Integer> offsets = new List<Integer>();
        for (Integer i = 0; i < minuteBoundsOffset.size(); i ++) {
            if (minutes >= minuteBounds[i] && minutes < minuteBounds[i + 1]) {
                offsets.add(minuteBoundsOffset[i]);
                offsets.add(hourBoundsOffset[i]);
                break;
            }
        }
        minutes = offsets[0];
        hours += offsets[1];
        if (hours >= 24) {
            hours = 0;
        }
        Time t = Time.newInstance(hours, minutes, 0, 0);
        dt = Datetime.newInstance(dt.date(), t);
        return dt;
    }

    public void scheduleJob(Datetime dt) {
        if (this.jobId != null) {
            System.abortJob(this.jobId);
        }
        if (Test.isRunningTest()) nameSchedule = 'TestVideoConferenceSchedulerHeroku';
        System.debug('scheduleJob ' + dt + ' ' + this.jobId);
        Integer minutes = dt.minute();
        Integer hours = dt.hour();
        String cron = '0 ' + minutes + ' ' + hours + ' * * ?';
        System.debug('cron = ' + cron);
        VT_R4_VideoConferenceScheduler videoConferenceScheduler = new VT_R4_VideoConferenceScheduler();
        videoConferenceScheduler.dt = dt;
        System.schedule(nameSchedule, cron, videoConferenceScheduler);
    }
}