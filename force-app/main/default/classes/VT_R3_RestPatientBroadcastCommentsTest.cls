@isTest
public with sharing class VT_R3_RestPatientBroadcastCommentsTest {

//    @isTest(SeeAllData=true)
//    public static void getCommentsTest() {
//        Test.startTest();
//        String responseString;
//        FeedItem feedItem = new FeedItem(
//                ParentId = UserInfo.getUserId(),
//                Body = 'Test post'
//        );
//        insert feedItem;
//        RestRequest request = new RestRequest();
//        RestResponse response = new RestResponse();
//        request.requestURI = '/Patient/Broadcasts/';
//        request.httpMethod = 'POST';
//        RestContext.request = request;
//        RestContext.response = response;
//
//        responseString = VT_R3_RestPatientBroadcastComments.sendComment(null, null, null);
//        Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
//        System.assertEquals('No more comments in broadcasts for patient, sorry ¯\\_(ツ)_/¯', responseMap.get('serviceMessage'));
////		System.assertEquals(400, response.statusCode);
//
////		RestContext.request.requestURI = '/Patient/Broadcasts/' + feedItem.Id;
////		VT_R3_RestPatientBroadcastComments.sendComment('Test message', null, null);
////		String bigString = 'bigstring';
////		while (bigString.length() < 10001){
////			bigString = bigString + 'bigString';
////		}
////		responseString = VT_R3_RestPatientBroadcastComments.sendComment(bigString, null, null);
////		bigString = '';
////		responseMap = (Map<String, Object>)JSON.deserializeUntyped(responseString);
////		System.assertEquals('FAILURE.  Your comment can not have more than 10000 characters.', responseMap.get('serviceResponse'));
////		System.assertEquals(400, response.statusCode);
//
////		while (bigString.length()< 1000001){
////			bigString = bigString + '11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111';
////		}
////		bigString = bigString + bigString + bigString + bigString + bigString;
////		responseString = VT_R3_RestPatientBroadcastComments.sendComment('Test message', 'fileName', bigString);
////		bigString = '';
////		responseMap = (Map<String, Object>)JSON.deserializeUntyped(responseString);
////		System.assertEquals('FAILURE.  Attachment file can not have more than 5000000 characters.', responseMap.get('serviceResponse'));
////		System.assertEquals(400, response.statusCode);
//
////		responseString = VT_R3_RestPatientBroadcastComments.sendComment('Test message', 'fileName', 'test blob string');
////		responseMap = (Map<String, Object>)JSON.deserializeUntyped(responseString);
////		Map<String, Object> resp = (Map<String, Object>)JSON.deserializeUntyped(responseString);
////		resp = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(resp.get('serviceResponse')));
////		System.assertEquals('Test message', resp.get('text'));
//
//        request.requestURI = '/Patient/Broadcasts/0016F00002R604KQAR';
//
//
//        request.httpMethod = 'GET';
//        RestContext.request = request;
//        RestContext.response = response;
//        responseString = VT_R3_RestPatientBroadcastComments.getComments();
//
//        VT_D1_CommunityChat.sendCommentWithFile(feedItem.Id, 'Test message', 'somefile.txt', 'somedata');
//
//        request.requestURI = '/Patient/Broadcasts/' + feedItem.Id;
//        responseString = VT_R3_RestPatientBroadcastComments.getComments();
//        Map<String, Object> resp = new Map<String, Object>();
//        resp = (Map<String, Object>) JSON.deserializeUntyped(responseString);
//        List<Object> objList = (List<Object>) resp.get('serviceResponse');
//        resp = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(objList[1]));
//        System.assertEquals('Test message', resp.get('text'));
//
////		delete feedItem;
////		responseString = VT_R3_RestPatientBroadcastComments.getComments();
////		System.assertEquals(500, response.statusCode);
//
//        Test.stopTest();
//    }
//
//    @IsTest(SeeAllData=true)
//    public static void exceptionTest() {
//        User adminUser = [SELECT Id FROM User WHERE IsActive = TRUE AND Profile.Name = 'System Administrator' LIMIT 1];
//
//        System.runAs(adminUser) {
//            String responseString;
//            FeedItem feedItem = new FeedItem(
//                    ParentId = UserInfo.getUserId(),
//                    Body = 'Test post'
//            );
//            insert feedItem;
//            RestRequest request = new RestRequest();
//            RestResponse response = new RestResponse();
//            request.requestURI = '/Patient/Broadcasts/' + feedItem.Id;
//            request.httpMethod = 'GET';
//            RestContext.request = request;
//            RestContext.response = response;
//            VT_D1_CommunityChat.sendComment(feedItem.Id, 'Test message');
//
//            delete feedItem;
//
//            responseString = VT_R3_RestPatientBroadcastComments.getComments();
//            System.assertEquals(500, response.statusCode);
//        }
//    }

    public static void getCommentsTest() {

        Id feedItemId = fflib_IDGenerator.generate(FeedItem.getSObjectType());
        Id broadcastCreatorUserId = fflib_IDGenerator.generate(User.getSObjectType());

        VT_Stubber.applyStub(
                'FirstComment',
                new List<FeedItem>{
                        new FeedItem(
                                Body = 'some text',
                                CreatedDate = System.now().addDays(-5),
                                CreatedById = broadcastCreatorUserId,
                                Id = feedItemId
                        )
                });

        VT_Stubber.applyStub(
                'FirstCommentAuthor',
                (List<User>) JSON.deserialize(
                        '[{"Name":"Sample Name"}]',
                        List<User>.class));

        Id contentVersionId = fflib_IDGenerator.generate(ContentVersion.getSObjectType());
        Id contentDocumentId = fflib_IDGenerator.generate(ContentDocument.getSObjectType());

        VT_Stubber.applyStub(
                'ContentVersions',
                new List<ContentVersion>{
                        new ContentVersion(
                                Id = contentVersionId,
                                VersionData = EncodingUtil.base64Decode('aa'),
                                ContentDocumentId = contentDocumentId
                        )
                });

        VT_Stubber.applyStub(
                'User',
                new List<User>{
                        new User(
                                VTR3_UnreadFeeds__c = 'blalba;' + feedItemId + ';'
                        )
                });

        RestContext.request = new RestRequest();
        RestContext.response = new RestResponse();
        RestContext.request.requestURI = '/Patient/Broadcasts/';
        RestContext.request.httpMethod = 'GET';

        VT_R3_RestPatientBroadcastComments.getComments();
        RestContext.request.requestURI = '/Patient/Broadcasts/' + feedItemId;
        VT_R3_RestPatientBroadcastComments.getComments();


        VT_R3_RestPatientBroadcastComments.sendComment('gg',null,null);
    }


}