/**
 * THIS CLASS IS DEPRECATED! DON'T USE IT!
 *
 * Use the following instead:
 * @see VT_R4_ConstantsHelper_AccountContactCase
 * @see VT_R4_ConstantsHelper_Documents
 * @see VT_R4_ConstantsHelper_Integrations
 * @see VT_R4_ConstantsHelper_KitsOrders
 * @see VT_R4_ConstantsHelper_Misc
 * @see VT_R4_ConstantsHelper_ProfilesSTM
 * @see VT_R4_ConstantsHelper_Protocol_ePRO
 * @see VT_R4_ConstantsHelper_Tasks
 * @see VT_R4_ConstantsHelper_VisitsEvents
 */
public with sharing class VT_D1_ConstantsHelper {
    
    /* Constants */
    public static final string SOBJECT_CASE = 'Case';
    public static final string SOBJECT_ACTUAL_VISIT = 'VTD1_Actual_Visit__c';
    public static final string SOBJECT_SURVEY = 'VTD1_Survey__c';
    public static final string SOBJECT_MONITORING_VISIT = 'VTD1_Monitoring_Visit__c';
    public static final string SOBJECT_DOC_CONTAINER = 'VTD1_Document__c';

    /* RecordType Constants  */
    public static final String RT_CAREPLAN = 'CarePlan';
    public static final String RT_PCF = 'VTD1_PCF';
    public static final String RT_GCF = 'VT_R2_General_Contact_Form';
    public static final String RT_PCF_READONLY = 'VTD1_PCF_Read_Only';
    public static final String RT_SURVEY_LOCKED = 'Locked';
    public static final String RT_SURVEY_UNLOCKED = 'Unlocked';

    public static final String RT_ACTUAL_VISIT_LABS = 'Labs';
    public static final String RT_ACTUAL_VISIT_STUDY_TEAM = 'Study_Team';
    public static final String RT_ACTUAL_VISIT_HCP = 'Health_Care_Professional_HCP';

    public static final String RT_PROTOCOL_KIT_AD_HOC_LAB = 'VTD1_Ad_hoc_Lab';
    public static final String RT_PROTOCOL_KIT_CONNECTED_DEVICES = 'VTD1_Connected_Devices';
    public static final String RT_PROTOCOL_KIT_IMP = 'VTD1_IMP';
    public static final String RT_PROTOCOL_KIT_LAB = 'VTD1_Lab';
    public static final String RT_PROTOCOL_KIT_STUDY_HUB_TABLET = 'VTD1_Study_Hub_Tablet';
    public static final String RT_PROTOCOL_KIT_WELCOME_TO_STUDY_PACKAGE = 'VTD1_Welcome_to_Study_Package';

    public static final String RT_PATIENT_KIT_AD_HOC_LAB = 'VTD1_Adhoc_Replacement_Lab';
    public static final String RT_PATIENT_KIT_CONNECTED_DEVICES = 'VTD1_Connected_Devices';
    public static final String RT_PATIENT_KIT_IMP = 'VTD1_IMP';
    public static final String RT_PATIENT_KIT_LAB = 'VTD1_Lab';
    public static final String RT_PATIENT_KIT_STUDY_HUB_TABLET = 'VTD1_Study_Hub_Tablet';
    public static final String RT_PATIENT_KIT_WELCOME_TO_STUDY_PACKAGE = 'VTD1_Welcome_to_Study_Package';

    /*Statuses Constants*/
    public static final String ACTUAL_VISIT_STATUS_SCHEDULED = 'Scheduled';
    public static final String ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED = 'To Be Scheduled';
    public static final String ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED = 'To Be Rescheduled';
    public static final String ACTUAL_VISIT_STATUS_REQUESTED = 'Requested';
    public static final String ACTUAL_VISIT_STATUS_COMPLETED = 'Completed';
    public static final String ACTUAL_VISIT_STATUS_CANCELLED = 'Cancelled';
    public static final String ACTUAL_VISIT_STATUS_MISSED = 'Missed';
    public static final String ACTUAL_VISIT_STATUS_FUTURE_VISIT = 'Future Visit';
    public static final String SURVEY_NOT_STARTED = 'Not Started';
    public static final String SURVEY_IN_PROGRESS = 'In Progress';
    public static final String SURVEY_REVIEW_REQUIRED = 'Review Required';
    public static final String SURVEY_REVIEWED = 'Reviewed';
    public static final String SURVEY_REVIEWED_PARTIALLY_COMPLETED = 'Reviewed (Partially Completed)';
    public static final String TASK_CLOSED = 'Closed';
    public static final String CASE_PRE_CONSENT = 'Pre-Consent';
    public static final String CASE_WASHOUT_RUN_IN = 'Washout/Run-In';
    public static final String CASE_WASHOUT_RUN_IN_FAILURE = 'Washout/Run-In Failure';
    public static final String CASE_ACTIVE_RANDOMIZED = 'Active/Randomized';
    public static final String CASE_RANDOMIZED = 'Randomized';
    public static final String CASE_SCREENED = 'Screened';
    public static final String CASE_CONSENTED = 'Consented';
    public static final String CASE_SCREEN_FAILURE = 'Screen Failure';
    public static final String CASE_LOST_TO_FOLLOW_UP = 'Lost to Follow-Up';    // Obsolete in R1.1?
    public static final String CASE_DROPPED = 'Dropped';
    public static final String CASE_COMPLETED = 'Completed';
    public static final String CASE_CLOSED = 'Closed';
    public static final String CASE_DOES_NOT_QUALIFY = 'Does Not Qualify';
    public static final String CASE_DROPPED_IN_FOLLOW_UP = 'Dropped Treatment,In F/Up';
    public static final String CASE_DROPPED_COMPL_FOLLOW_UP = 'Dropped Treatment,Compl F/Up';
    public static final String CASE_COMPL_IN_FOLLOW_UP = 'Compl Treatment,In F/Up';
    public static final String CASE_COMPL_DROPPED_FOLLOW_UP = 'Compl Treatment,Dropped F/Up';
    public static final String PROTOCOL_DELIVERY_NOT_STARTED = 'Not Started';
    public static final String TIMESLOT_PROPOSED = 'Proposed';
    public static final String TIMESLOT_CONFIRMED = 'Confirmed';
    public static final String TIMESLOT_REJECTED = 'Rejected';
    public static final String DOCUMENT_APPROVED = 'Approved';
    public static final String DOCUMENT_REJECTED = 'Rejected';
    public static final String ASSIGNMENT_HISTORY_CURRENT = 'Current';
    public static final String ASSIGNMENT_HISTORY_TRANSFERRED = 'Transferred';
    public static final String CANDIDATE_PATIENT_CONVERSION_CONVERTED = 'Converted';
    public static final String CASE_WASHOUT_RUN_IN_STATUS = 'Successful';
    public static final String CASE_ELIGIBLITY_STATUS_ELIGIBLE = 'Eligible';
    public static final String CASE_ELIGIBLITY_STATUS_INELIGIBLE = 'Inligible';

    public static final Set<String> SEND_SUBJECT_CASE_STATUSES = new Set<String>{   // For Cenduit / CSM only
        CASE_CONSENTED,
        //CASE_DOES_NOT_QUALIFY,
        CASE_SCREENED,
        CASE_SCREEN_FAILURE,
        CASE_WASHOUT_RUN_IN,
        CASE_WASHOUT_RUN_IN_FAILURE,
        CASE_ACTIVE_RANDOMIZED,
        CASE_DROPPED,
        CASE_DROPPED_IN_FOLLOW_UP,
        CASE_DROPPED_COMPL_FOLLOW_UP,
        CASE_COMPLETED,
        CASE_COMPL_IN_FOLLOW_UP,
        CASE_COMPL_DROPPED_FOLLOW_UP
    };

    public static final Set<String> SEND_SUBJECT_CASE_STATUSES_RH = new Set<String>{    // For RR only
        CASE_PRE_CONSENT,
        CASE_CONSENTED,
        CASE_DOES_NOT_QUALIFY,
        CASE_SCREENED,
        CASE_SCREEN_FAILURE,
        CASE_WASHOUT_RUN_IN,
        CASE_WASHOUT_RUN_IN_FAILURE,
        CASE_ACTIVE_RANDOMIZED,
        CASE_DROPPED//,
        //CASE_DROPPED_IN_FOLLOW_UP,
        //CASE_DROPPED_COMPL_FOLLOW_UP,
        //CASE_COMPLETED,
        //CASE_COMPL_IN_FOLLOW_UP,
        //CASE_COMPL_DROPPED_FOLLOW_UP
    };

    /* Record types */
    public static final String RECORD_TYPE_ID_ACCOUNT_PATIENT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Patient').getRecordTypeId();
    public static final String RECORD_TYPE_ID_ACCOUNT_SPONSOR = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Sponsor').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CONTACT_PATIENT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Patient').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CONTACT_CAREGIVER = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Caregiver').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_SIMPLETASK = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('SimpleTask').getRecordTypeId();
    public static final String RECORD_TYPE_ID_EVENT_OUTOFOFFICE = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('OutOfOffice').getRecordTypeId();

    public static final String RECORD_TYPE_ID_PROTOCOL_VISIT_PROTOCOL = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByDeveloperName().get('Protocol').getRecordTypeId();
    public static final String RECORD_TYPE_ID_PROTOCOL_VISIT_ONBOARDING = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByDeveloperName().get('VTD1_Onboarding').getRecordTypeId();
    public static final String RECORD_TYPE_ID_ACTUAL_VISIT_LABS = Schema.SObjectType.VTD1_Actual_Visit__c.getRecordTypeInfosByDeveloperName().get('Labs').getRecordTypeId();
    public static final String RECORD_TYPE_ID_ACTUAL_VISIT_UNSCHEDULED = Schema.SObjectType.VTD1_Actual_Visit__c.getRecordTypeInfosByDeveloperName().get('VTD1_Unscheduled').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CASE_CAREPLAN = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CarePlan').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CASE_PCF_READONLY = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VTD1_PCF_Read_Only').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CASE_PCF = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VTD1_PCF').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CONTACT_PCP = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('PCP').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CONTACT_PI = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('PI').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CONTACT_SPONSOR = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Sponsor_Contact').getRecordTypeId();
    public static final String RECORD_TYPE_ID_PATIENT_BLIND = Schema.SObjectType.VTD1_Patient__c.getRecordTypeInfosByDeveloperName().get('Blind').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_ARCHIVED = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('Medical_Record_Archived').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_REJECTED = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('Medical_Record_Rejected').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_RELEASE_FORM = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('Medical_Record_Release_Form').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD1_Medical_Record').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_OTHER = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD1_Other').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_CERTIFICATON = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD2_Certification').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD1_Patient_eligibility_assessment_form').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSR = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('Patient_Eligibility_Assessment_Form_Rejected').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD1_Regulatory_Document').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT_LOCKED = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD1_Regulatory_Document_Locked').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT_REJECTED = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('Regulatory_Document_Rejected').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_PA_SIGNATURE_PAGE = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD2_PA_Signature_Page').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_NOTE_OF_TRANSFER = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTR2_Note_of_Transfer').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_INFORMED_CONSENT_GUIDE = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTR2_Informed_Consent_Guide').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTR3_VisitDocument').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_PAYMENT = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('Payment').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_CANCELLATION = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('Cancellation').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_CONSENT = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('VTD1_Consent').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_DAILY_REPORT_UPLOAD = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('VTD1_Daily_Report_Upload').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_DELIVERY = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('Delivery').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_NOTIFICATION = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('Notification').getRecordTypeId();
    //public static final String RECORD_TYPE_ID_SC_TASK_QUEUE = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('Payment').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_RESCHEDULE = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('Reschedule').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_SCHEDULE_HCP_VISIT = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('VTD1_HCP_Visit').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_INDEPENDENT_RATER = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('VTD1_Independent_Rater_schedule_visit_task').getRecordTypeId();

    public static final String RECORD_TYPE_ID_TASK_PCF_TASK = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('VTD1_PCF_Task').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_RESCHEDULE = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Reschedule').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_FOR_COUNTERSIGN_PACKET = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('TaskForCountersignPack').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_FOR_PAYMENT = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('TaskForPayment').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_FOR_SCHEDULING_VISIT = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('VTD1_TaskForSchedulingVisits').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_FOR_VISIT = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('TaskForVisit').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_FOR_VISIT_AD_HOC = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('VTD2_TaskForVisitAdHoc').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_FOR_REPORT_UPLOAD = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Task_for_Report_Upload').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_EAF_TASK = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('EAF_Task').getRecordTypeId();
 
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_PI = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('PI').getRecordTypeId();
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_PG = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('PG').getRecordTypeId();
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_SC = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('SC').getRecordTypeId();
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_CM = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('CM').getRecordTypeId();
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_TMA = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('TMA').getRecordTypeId();
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_SCR = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('VTR2_Site_Coordinator').getRecordTypeId();
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_CRA = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('VTR2_CRA').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SITE_METRICS_PATIENT_BY_STATUS_PI = Schema.SObjectType.VTD1_SiteMetricsPatientsbyStatus__c.getRecordTypeInfosByDeveloperName().get('VTR2_PI').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SITE_METRICS_PATIENT_BY_STATUS_VS = Schema.SObjectType.VTD1_SiteMetricsPatientsbyStatus__c.getRecordTypeInfosByDeveloperName().get('VTR2_VS').getRecordTypeId();

    public static final String RECORD_TYPE_ID_VISIT_MEMBER_REGULAR = Schema.SObjectType.Visit_Member__c.getRecordTypeInfosByDeveloperName().get('VTD1_RegularMember').getRecordTypeId();

    public static final String RECORD_TYPE_ID_PROTOCOL_KIT_WELCOME_TO_STUDY_PACKAGE = Schema.SObjectType.VTD1_Protocol_Kit__c.getRecordTypeInfosByDeveloperName().get('VTD1_Welcome_to_Study_Package').getRecordTypeId();
    public static final String RECORD_TYPE_ID_PROTOCOL_KIT_LAB = Schema.SObjectType.VTD1_Protocol_Kit__c.getRecordTypeInfosByDeveloperName().get('VTD1_Lab').getRecordTypeId();
    public static final String RECORD_TYPE_ID_PROTOCOL_KIT_IMP = Schema.SObjectType.VTD1_Protocol_Kit__c.getRecordTypeInfosByDeveloperName().get('VTD1_IMP').getRecordTypeId();

    public static final String RECORD_TYPE_ID_ORDER_AD_HOC_DELIVERY = Schema.SObjectType.VTD1_Order__c.getRecordTypeInfosByDeveloperName().get('VTD1_Ad_hoc_Delivery').getRecordTypeId();
    public static final String RECORD_TYPE_ID_ORDER_PACKING_MATERIALS = Schema.SObjectType.VTD1_Order__c.getRecordTypeInfosByDeveloperName().get('VTD1_Regular_Kit_Delivery').getRecordTypeId();
    public static final String RECORD_TYPE_ID_ORDER_AD_HOC_RETURN = Schema.SObjectType.VTD1_Order__c.getRecordTypeInfosByDeveloperName().get('VTD1_Ad_hoc_Return').getRecordTypeId();
    public static final String RECORD_TYPE_ID_ORDER_AD_HOC_REPLACEMENT_DELIVERY = Schema.SObjectType.VTD1_Order__c.getRecordTypeInfosByDeveloperName().get('VTD1_Ad_hoc_Replacement_Delivery').getRecordTypeId();
    public static final String RECORD_TYPE_ID_PATIENT_KIT_IMP = Schema.SObjectType.VTD1_Patient_Kit__c.getRecordTypeInfosByDeveloperName().get('VTD1_IMP').getRecordTypeId();
    public static final String RECORD_TYPE_ID_PATIENT_KIT_PACKING_MATERIALS = Schema.SObjectType.VTD1_Patient_Kit__c.getRecordTypeInfosByDeveloperName().get('VTR3_Packaging_Materials').getRecordTypeId();

    public static final String RECORD_TYPE_ID_REPLACEMENT_ITEM_KIT = Schema.SObjectType.VTD1_Replacement_Item__c.getRecordTypeInfosByDeveloperName().get('VTD1_Kit').getRecordTypeId();

    public static final String SITE_ENROLLMENT_OPEN = 'Enrollment Open';
    public static final String SITE_CLOSED = 'Closed';
    public static final String SITE_ENROLLMENT_CLOSED = 'Enrollment Closed';
    public static final String SITE_PREMATURELY_CLOSED = 'Prematurely Closed';
    public static final String MONITORING_VISIT_COMPLETE = 'Visit Completed';
    public static final String DOCUMENT_SITE_RSU_COMPLETE = 'Site RSU Complete';
    public static final String ORDER_STATUS_RECEIVED = 'Received';
    public static final String ORDER_STATUS_REPLACEMENT_ORDERED = 'Replacement Ordered';

    /* Misc */
    public static final String ACTUAL_VISIT_SUBTYPE_PSC = 'PSC';
    public static final String SURVEY_SCORING_TASK_SUBJECT = 'Score ePRO Entry';
    public static final String SURVEY_PATIENT_TASK_SUBJECT = 'ePRO Entry Available';
    public static final String MONITORING_VISIT_TYPE_CLOSEOUT = 'Closeout Visit';
    public static final String MONITORING_VISIT_TYPE_SELECTION = 'Selection Visit';
    public static final String MONITORING_VISIT_TYPE_INITIATION = 'Initiation Visit';
    public static final String DOCUMENT_EDPRC_NAME = 'EDPRC';
    public static final String STUDY_MILESTONE_PARENT_FIELD = 'VTD1_Study__c';
    public static final String SITE_MILESTONE_PARENT_FIELD = 'VTD1_Site__c';
    public static final String MILESTONE_DESCRIPTION_FIELD_NAME = 'VTD1_Milestone_Description__c';
    public static final String CASE_STUDY_FIELD_NAME = 'VTD1_Study__c';
    public static final String CASE_PG_FIELD_NAME = 'VTD1_Primary_PG__c';
    public static final String CASE_PI_FIELD_NAME = 'VTD1_PI_user__c';
    public static final String CASE_BACKUP_PG_FIELD_NAME = 'VTD1_Secondary_PG__c';
    public static final String CASE_BACKUP_PI_FIELD_NAME = 'VTD1_Backup_PI_User__c';
    public static final String LOGIN_ERROR_STANDARD = 'Your login attempt has failed. Make sure the username and password are correct.';
    public static final String LOGIN_ERROR_CUSTOM = 'You have entered an invalid Username and/or Password combination.<br/>Please verify that you entered this information correctly.';
    public static final String PSC_EPRO_DEFAULT_QUESTION = 'Did the PSC visit occur and successfully complete?';
    public static final String VISIT_REQUEST_TASK_SUBJECT = 'Manually Schedule Visit';
    public static final String PROTOCOL_VISIT_TYPE_LABS = 'Labs';
    public static final String PROTOCOL_VISIT_TYPE_STUDY_TEAM = 'Study Team';
    public static final String PROTOCOL_VISIT_TYPE_HCP = 'Health Care Professional (HCP)';
    public static final String TASK_CATEGORIES_OTHER_TASKS = 'Other Tasks';
    public static final String REGULATORY_DOCUMENT_TYPE_SOURCE_NOTE_OF_TRANSFER = 'Source Note of Transfer';
    public static final String REGULATORY_DOCUMENT_TYPE_TARGET_NOTE_OF_TRANSFER = 'Target Note of Transfer';
    public static final String REGULATORY_DOCUMENT_TYPE_DOA_LOG = 'DOA Log';
    public static final String REGULATORY_DOCUMENT_TYPE_NOTE_TO_FILE_STEP_DOWN = 'Note to File Step Down';
    public static final String REGULATORY_DOCUMENT_TYPE_SUBJECT_SCREENING_LOG = 'Subject Screening Log';
    public static final String REGULATORY_DOCUMENT_TYPE_SUBJECT_IDENTIFICATION_LOG = 'Subject Identification Log';
    public static final String REGULATORY_DOCUMENT_TYPE_1572 = '1572 IRB Acknowledgement Letter';
    public static final String REGULATORY_DOCUMENT_TYPE_ACKNOWLEDGEMENT_LETTER = 'Acknowledgement Letter';
    public static final String REGULATORY_DOCUMENT_TYPE_FOLLOW_UP_LETTER = 'Follow-Up Letter';
    public static final String REGULATORY_DOCUMENT_LEVEL_SITE = 'Site';
    public static final String REGULATORY_DOCUMENT_LEVEL_SUBJECT = 'Subject';
    public static final String SSTM_SCR_TYPE_PRIMARY = 'Primary';
    public static final String SSTM_SCR_TYPE_BACKUP = 'Backup';
    public static final String CANDIDATE_PATIENT_CAREGIVER_STATUS_SUCCESS = 'Success';
    public static final String CANDIDATE_PATIENT_CAREGIVER_STATUS_FAILURE = 'Failure';
    public static final String DOCUMENT_CATEGORY_UNCATEGORIZED = 'Uncategorized';

    /* Milestone Descriptions */
    public static final String STUDY_FIRST_SUBJECT_ENROLLED = 'First Subject Enrolled';
    public static final String STUDY_FIRST_SUBJECT_RANDOMIZED = 'First Subject Randomized';
    public static final String STUDY_FIRST_SUBJECT_SCREENED = 'First Subject Screened';
    public static final String STUDY_FIRST_VIRTUAL_SITE_ACTIVATED = 'First Virtual Site Activated';
    public static final String STUDY_FIRST_VIRTUAL_SITE_CLOSED = 'First Virtual Site Closed';
    public static final String STUDY_FIRST_VIRTUAL_SITE_INITIATED = 'First Virtual Site Initiated';
    public static final String STUDY_FIRST_VIRTUAL_SITE_SELECTED = 'First Virtual Site Selected';
    public static final String STUDY_RANDOMIZED_YES = 'Yes';
    public static final String STUDY_RANDOMIZED_NO = 'No';
    public static final String LAST_SUBJECT_ENROLLED = 'Last Subject Enrolled';
    public static final String LAST_SUBJECT_RANDOMIZED = 'Last Subject Randomized';
    public static final String LAST_SUBJECT_SCREENED = 'Last Subject Screened';
    public static final String LAST_SUBJECT_OUT = 'Last Subject Out LPLV';
    public static final String SITE_VIRTUAL_SITE_INITIATED = 'Virtual Site Initiated';
    public static final String SITE_VIRTUAL_SITE_SELECTED = 'Virtual Site Selected';
    public static final String SITE_VIRTUAL_SITE_STARTUP_COMPLETION = 'Virtual Site Start-Up Completion';

    /* Protocol ePRO */
    public static final String PROTOCOL_EPRO_TRIGGER_EVENT = 'Event';
    public static final String PROTOCOL_EPRO_TRIGGER_PERIODIC = 'Periodic';
    public static final String PROTOCOL_EPRO_TRIGGER_EVENT_TASK_SUBJECT = 'eDiary Response Completion';
    public static final String PROTOCOL_EPRO_TRIGGER_PERIODIC_TASK_SUBJECT = 'Recurring eDiary Response Completion';
    public static final String PROTOCOL_EPRO_PERIOD_DAILY = 'Daily';
    public static final String PROTOCOL_EPRO_PERIOD_WEEKLY = 'Weekly';
    public static final String PROTOCOL_EPRO_PERIOD_MONTHLY = 'Monthly';
    public static final String PROTOCOL_EPRO_PERIOD_X_DAYS = 'Every X Days';
    public static final String PROTOCOL_EPRO_PERIOD_SPECIFIC_DAYS = 'Specific Days Repeated';

    /* Profiles */
    public static final String PG_PROFILE_NAME = 'Patient Guide';
    public static final String PI_PROFILE_NAME = 'Primary Investigator';
    public static final String PATIENT_PROFILE_NAME = 'Patient';
    public static final String CAREGIVER_PROFILE_NAME = 'Caregiver';
    public static final Set<String> HIPAA_PROFILES = new Set<String>{PATIENT_PROFILE_NAME, CAREGIVER_PROFILE_NAME};
    public static final String TMA_PROFILE_NAME = 'Therapeutic Medical Advisor';
    public static final String SC_PROFILE_NAME = 'Study Concierge';
    public static final String CM_PROFILE_NAME = 'Monitor';
    public static final String CRA_PROFILE_NAME = 'CRA';
    public static final String SCR_PROFILE_NAME = 'Site Coordinator';
    public static final String MRR_PROFILE_NAME = 'Monitoring Report Reviewer';
    public static final String RS_PROFILE_NAME = 'Regulatory Specialist';

    /* Integration */
    public static final String MULESOFT_SERVICE_URL = 'callout:Mulesoft_Subject';
    public static final String MULESOFT_SERVICE_DELIVERY_URL ='callout:Mulesoft_Delivery';
    public static final String MULESOFT_SERVICE_DELIVERY_URL_1_1 ='callout:Mulesoft_Delivery_1_1';
    public static final String MULESOFT_SERVICE_REPLACEMENT_URL ='callout:Mulesoft_Replacement';

    public static final String INTEGRATION_TIMESTAMP_FORMAT = 'yyyy-MM-dd HH:mm:ss.SSS';
    public static final String INTEGRATION_ACTION_CREATE_SUBJECT = 'Create Subject';
    public static final String INTEGRATION_ACTION_UPDATE_SUBJECT = 'Update Subject';
    public static final String INTEGRATION_ACTION_RANDOMIZE_SUBJECT = 'Randomize Subject';
    public static final String INTEGRATION_ACTION_RECEIVED_ORDER = 'Received Order';
    public static final String INTEGRATION_ACTION_ADHOC_REPLACEMENT = 'Ad-hoc Replacement';
    public static final String ENDPOINT_URL_CREATE_UPDATE_SUBJECT = MULESOFT_SERVICE_URL+'/subject';//'http://neoexpapis.cloudhub.io/api/subject';
    public static final String ENDPOINT_URL_RANDOMIZE_SUBJECT = MULESOFT_SERVICE_URL+'/subject/randomize';//'https://rds-neo-subject-exp-1-0-dev.cloudhub.io/api/randomize';//'http://neoexpapis.cloudhub.io/api/randomize';
    public static final String ENDPOINT_URL_RECEIVED_ORDER = MULESOFT_SERVICE_DELIVERY_URL+'/shipment/received-state';
    public static final String ENDPOINT_URL_RECEIVED_ORDER_1_1 = MULESOFT_SERVICE_DELIVERY_URL_1_1+'/shipment/received-state';
    public static final String ENDPOINT_URL_REPLACEMENT = MULESOFT_SERVICE_REPLACEMENT_URL+'/shipment/replacement';
    public static final String POST_METHOD = 'POST';
    public static final String PUT_METHOD = 'PUT';
    public static final String test1 = '1';

    //Document
    public static final String DOCUMENT_STATUS_APPROVED = 'Approved';
    public static final String DOCUMENT_STATUS_CERTIFIED = 'Certified';
    public static final String DOCUMENT_STATUS_NO_CERTIFICATION_REQUIRED = 'No Certification Required';
    //test
    /* Chat Routing */
    public static final String ROUTING_CONFIGURATION = 'VTD1_Live_Agent';

}