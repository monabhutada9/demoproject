@isTest
public with sharing class StudyTeamMembersUploadController_Test {

    private static final String EMAIL_ONE = 'email@one.test';
    private static final String EMAIL_TWO = 'email@two.test';
    private static final String EMAIL_THREE = 'email@three.test';
    private static final String EMAIL_MASTER = 'email@master.test';

    @isTest
    public static void doTest() {
        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c();
        insert study;

        Id pId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        insert new List<User>{
            getUser(EMAIL_ONE, pId),
            getUser(EMAIL_TWO, pId),
            getUser(EMAIL_THREE, pId),
            getUser(EMAIL_MASTER, pId)
        };

        String csvString = EMAIL_ONE + ',\n'
                         + EMAIL_TWO + ',' + EMAIL_MASTER + '\n'
                         + EMAIL_THREE + ',' + EMAIL_MASTER + '\n'
        ;

        ContentVersion conVer = new ContentVersion(
            Title = 'CSV',
            PathOnClient = 'test.csv',
            VersionData = Blob.valueOf(csvString),
            IsMajorVersion = true
        );
        insert conVer;

        Id conDocId = [SELECT Id FROM ContentDocument LIMIT 1].Id;
        StudyTeamMembersUploadController.processUpload(conDocId, study.id);
        System.AssertEquals(4, [SELECT Count() FROM Study_Team_Member__c]);
    }

    private static User getUser(String email, Id profileId) {
        return new User(
            Alias = 'standt',
            Email = email,
            EmailEncodingKey = 'UTF-8',
            LastName = 'Testing',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ProfileId = profileId,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = email
        );
    }

}