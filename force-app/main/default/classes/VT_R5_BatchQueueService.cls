/**
* @author: Carl Judge
* @date: 28-Sep-20
* @description: enqueue batches
**/

public class VT_R5_BatchQueueService {
    public static void enqueueBatch(VT_R5_QueueableBatch batchInstance, Integer scopeSize) {

        if (scopeSize > 2000) scopeSize = 2000;

        List<VTR4_Semaphore__c> semaphores;
        Boolean failedToGetSemaphore = false;
        try {
            semaphores = [SELECT Id FROM VTR4_Semaphore__c WHERE Name = 'BatchQueueSemaphore' FOR UPDATE];
        } catch (Exception e) {
            failedToGetSemaphore = true;
            System.debug('Failed to get semaphore: ' + e.getMessage());
        }

        VTR5_QueueBatch__c batchRecord = new VTR5_QueueBatch__c(
            VTR5_BatchClass__c = batchInstance.getType().getName(),
            VTR5_QueueName__c = batchInstance.getQueueName(),
            VTR5_ScopeSize__c = scopeSize
        );

        Integer existingBatchCount = [

            SELECT COUNT() FROM VTR5_QueueBatch__c
            WHERE VTR5_QueueName__c = :batchInstance.getQueueName()
            LIMIT 1
        ];


        Boolean serialized = false;
        if (System.isBatch() || failedToGetSemaphore || existingBatchCount > 0) {
            try {
                batchInstance.actionsBeforeSerialize();
                String jsonStr = JSON.serialize(batchInstance);
                if (jsonStr.length() > VTR5_QueueBatch__c.VTR5_BatchJSON__c.getDescribe().getLength()) {
                    throw new BatchQueueServiceException('JSON string too long');
                }
                batchRecord.VTR5_BatchJSON__c = jsonStr;
                serialized = true;
            } catch (Exception e) {
                System.debug('Failed to serialize batch: ' + e.getMessage());
                batchInstance.actionsAfterDeserialize();
            }
        }

        try {
            insert batchRecord;
            batchInstance.setId(batchRecord.Id);
        } catch (Exception e) {
            System.debug('Failed to create batch record: ' + e.getMessage());
        }

        if (!serialized || batchRecord == null) {

            Database.executeBatch(batchInstance, scopeSize);
        }
    }


    public class BatchQueueServiceException extends Exception {}

}