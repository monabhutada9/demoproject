/**
 * Created by Alexander Komarov on 13.05.2019.
 */

@RestResource(UrlMapping='/Patient/Payments/Sum/*')
global with sharing class VT_R3_RestPatientPaymentsSum {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();

    @HttpGet
    global static String getPayments() {
        RestResponse response = RestContext.response;


        List<VTD1_Patient_Payment__c> paymentsList = new List<VTD1_Patient_Payment__c>();
        Map<String, Decimal> result = new Map<String, Decimal>();
        try {
            paymentsList = VT_D1_PatientPaymentsControllerRemote.getPatientPayments(null);

            result = formResult(paymentsList);
        } catch (Exception e) {
            response.statusCode = 500;
            cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName());
            return JSON.serialize(cr,true);
        }
        cr.buildResponse(result);
        return JSON.serialize(cr,true);
    }

    private static Map<String, Decimal> formResult(List<VTD1_Patient_Payment__c> paymentsList) {
        Map<String, Decimal> result = new Map<String, Decimal>();
        for (VTD1_Patient_Payment__c p : paymentsList) {
            if (p.RecordType.Name.equals('Reimbursement') || p.VTD1_Status__c != 'Payment Issued') continue;
            if (!result.containsKey(p.VTD1_Currency__c)) {
                result.put(p.VTD1_Currency__c, p.VTD1_Amount_Formula__c);
            } else {
                Decimal temp = result.get(p.VTD1_Currency__c);
                temp += p.VTD1_Amount_Formula__c;
                result.put(p.VTD1_Currency__c, temp);
            }
        }
        return result;
    }

}