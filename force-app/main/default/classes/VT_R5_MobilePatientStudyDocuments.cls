/**
 * @author: Alexander Komarov
 * @date: 13.10.2020
 * @description:
 */

public with sharing class VT_R5_MobilePatientStudyDocuments extends VT_R5_MobileRestResponse implements VT_R5_MobileRestRouter.Routable {


    public List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/study-documents',
                '/patient/{version}/study-documents/{id}'
        };
    }

    public void versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        String docId = parameters.get('id');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        if (docId != null && docId.equals('visits')) {
                            get_visits_v1();
                            return;
                        } else {
                            get_v1(docId);
                            return;
                        }
                    }
                }
            }
            when 'POST' {
                switch on requestVersion {
                    when 'v1' {
                        post_v1();
                        return;
                    }
                }
            }
            when 'PATCH' {
                switch on requestVersion {
                    when 'v1' {
                        patch_v1(docId);
                        return;
                    }
                }
            }
        }
        VT_R5_MobileRestRouter.unsupportedVersionResponse();
        this.addError('unsupported version');
        this.sendResponse();
    }

    private class RequestParams {
        String comment;
        String nickname;
        String contentVersionId;
        String visitId;
    }

    private void post_v1() {
        try {
            RequestParams params = (RequestParams) JSON.deserialize(this.request.requestBody.toString(), RequestParams.class);
            if (params.contentVersionId != null) {
                Id contentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :params.contentVersionId].ContentDocumentId;
                String preview = getPreview(contentDocumentId);
                VTD1_Document__c document = documentRegistration(params, contentDocumentId);
                documentsList = new List<MobileDocumentWrapper>{
                        new MobileDocumentWrapper(document)
                                .addDownloadLink()
                                .addPreview(preview)
                };
            }
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private VTD1_Document__c documentRegistration(RequestParams params, Id contentDocumentId) {
        Case patientCase = VT_D1_PatientCaregiverBound.getCaseForUser();
        Id documentId = null;
        List <ContentDocumentLink> cdls = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId = :contentDocumentId];
        String documentPrefix = VTD1_Document__c.getSObjectType().getDescribe().getKeyPrefix();
        for (ContentDocumentLink cdl : cdls) {
            if (String.valueOf(cdl.LinkedEntityId).startsWith(documentPrefix)) {
                documentId = cdl.LinkedEntityId;
                break;
            }
        }
        VTD1_Document__c document = getDocumentById(documentId);
        document.VTR3_Upload_File_Completed__c = true;
        document.VTR3_CreatedProfile__c = true;
        document.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT;
        document.VTR3_Category__c = 'Uncategorized';
        document.VTD1_Nickname__c = params.nickname;
        document.VTD1_Comment__c = params.comment;
        if (String.isNotEmpty(params.visitId)) {
            document.VTR3_Associated_Visit__c = params.visitId;
        }
        document.VTD1_Flagged_for_Deletion__c = false;
        update document;
        VT_D2_MedicalRecordsCertifyDocsHelper.certifyDoc(patientCase.Id, document.Id);
        return document;
    }

    private void patch_v1(String docId) {
        try {
            VTD1_Document__c docToUpdate = getDocumentById(docId);
            Id contentDocumentId = docToUpdate.AttachedContentDocuments[0].ContentDocumentId;
            String preview = getPreview(contentDocumentId);
            RequestParams params = (RequestParams) JSON.deserialize(this.request.requestBody.toString(), RequestParams.class);
            if (params.comment != null) docToUpdate.VTD1_Comment__c = params.comment;
            if (params.nickname != null) docToUpdate.VTD1_Nickname__c = params.nickname;
            update docToUpdate;
            documentsList = new List<MobileDocumentWrapper>{
                    new MobileDocumentWrapper(docToUpdate)
                            .addDownloadLink()
                            .addPreview(preview)
            };
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private List<VisitObj> visits;
    private String networkId;
    private String caseId;
    private void get_visits_v1() {
        try {
            Case currentCase = VT_D1_PatientCaregiverBound.getCaseForUser();
            List<VTD1_Actual_Visit__c> visits = VT_R3_AddStudyDocumentsController.getVisits(currentCase.Id);
            this.visits = convertVisits(visits);
            this.caseId = currentCase.Id;
            this.networkId = Network.getNetworkId();
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();

    }
    private static List<VisitObj> convertVisits(List<VTD1_Actual_Visit__c> visits) {
        List<VisitObj> result = new List<VisitObj>();
        for (VTD1_Actual_Visit__c v : visits) {
            result.add(new VisitObj(v));
        }
        return result;
    }

    private class VisitObj {
        private String visitName;
        private Long visitDate;
        private String id;

        public VisitObj(VTD1_Actual_Visit__c actualVisit) {
            this.id = actualVisit.Id;
            this.visitName = actualVisit.Name;
            this.visitDate = actualVisit.VTD1_Date_for_Timeline__c == null ? null : actualVisit.VTD1_Date_for_Timeline__c.getTime();
        }
    }

    private List<MobileDocumentWrapper> documentsList;
    private MobileDocumentWrapperInfo metaInfo;
    private void get_v1(String docId) {
        try {
            documentsList = new List<MobileDocumentWrapper>();
            if (docId != null) {
                documentsList.add(
                        new MobileDocumentWrapper(getDocumentById(docId))
                                .addDownloadLink()
                );
                addPreviews(documentsList);
            } else {
                parseParams(this.request);
                Case currentCase = VT_D1_PatientCaregiverBound.getCaseForUser();
                metaInfo = new MobileDocumentWrapperInfo(countDocuments(currentCase.Id), studyDocumentsOffset, studyDocumentsLimit);
                List<VTD1_Document__c> docs = getDocuments(currentCase.Id, studyDocumentsOffset, studyDocumentsLimit);
                for (VTD1_Document__c doc : docs) {
                    MobileDocumentWrapper mobileDocumentWrapper = new MobileDocumentWrapper(doc);
                    documentsList.add(mobileDocumentWrapper);
                }
                addPreviews(documentsList);
            }

        } catch (Exception e) {
            System.debug('exception?'+e.getStackTraceString() + e.getMessage() + e.getLineNumber());
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private static List<VTD1_Document__c> getDocuments(Id caseId, Integer qOffset, Integer qLimit) {
        String queryStr = 'SELECT Id, ' +
                'Name, ' +
                'VTD1_FileNames__c, ' +
                'VTD1_Nickname__c, ' +
                'VTD1_Comment__c, ' +
                'VTD1_Version__c, ' +
                'VTD1_Status__c, ' +
                'toLabel(VTD1_Status__c) translatedStatus, ' +
                'VTD1_Does_file_needs_deletion__c, ' +
                'VTD1_Deletion_Date__c, ' +
                'VTD1_PI_Comment__c, ' +
                'CreatedBy.FirstName, ' +
                'CreatedBy.LastName, ' +
                'CreatedDate, ' +
                'VTR3_Associated_Visit__c, ' +
                'VTR3_Associated_Visit__r.Name, ' +
                'VTR3_Category__c, ' +
                'toLabel(VTR3_Category__c) translatedCategory, ' +
                '(SELECT ContentSize, FileExtension, ContentDocumentId FROM AttachedContentDocuments ORDER BY CreatedDate DESC LIMIT 1) ' +
                'FROM VTD1_Document__c ' +
                'WHERE VTD1_Clinical_Study_Membership__c = :caseId AND ' +
                '(((RecordTypeId IN :medicalRecordTypes) AND (NOT (VTD1_Does_file_needs_deletion__c = \'Yes\' AND VTD1_Deletion_Reason__c = \'Not Relevant\'))) ' +
                'OR (RecordTypeId IN :visitDocumentRecordTypes)) ' +
                'ORDER BY CreatedDate DESC';

        if (qLimit != null) {
            queryStr += ' LIMIT ' + qLimit;
        }

        if (qOffset != null) {
            queryStr += ' OFFSET ' + qOffset;
        }

        List<VTD1_Document__c> result = Database.query(queryStr);

        return result;
    }

    private static VTD1_Document__c getDocumentById(String docId) {
        return [
                SELECT
                        Id,
                        Name,
                        VTD1_FileNames__c,
                        VTD1_Nickname__c,
                        VTD1_Comment__c,
                        VTD1_Version__c,
                        VTD1_Status__c,
                        toLabel(VTD1_Status__c) translatedStatus,
                        CreatedBy.FirstName,
                        CreatedBy.LastName,
                        CreatedDate,
                        VTR3_Associated_Visit__c,
                        VTR3_Associated_Visit__r.Name,
                        toLabel(VTR3_Category__c) translatedCategory,
                        VTR3_Upload_File_Completed__c,
                        VTR3_CreatedProfile__c,
                        RecordTypeId,
                        VTD1_Flagged_for_Deletion__c,
                        VTR3_Category__c, (SELECT ContentSize, FileExtension, ContentDocumentId FROM AttachedContentDocuments ORDER BY CreatedDate DESC LIMIT 1)
                FROM VTD1_Document__c
                WHERE Id = :docId
        ];
    }

    private class MobileDocumentWrapperInfo {
        private Integer totalDocs;
        private Integer queryOffset;
        private Integer queryLimit;

        MobileDocumentWrapperInfo(Integer totalDocs, Integer queryOffset, Integer queryLimit) {
            this.totalDocs = totalDocs;
            this.queryLimit = queryOffset;
            this.queryOffset = queryLimit;
        }
    }

    private class MobileDocumentWrapper {
        String documentId;
        String fileName;
        String fileExtension;
        String fileDownloadLink;
        String fileCategory;
        String fileCategoryTranslated;
        String fileNickname;
        String fileComment;
        String fileStatus;
        String fileUserFirstName;
        String fileUserLastName;
        String fileAssociatedVisitId;
        String fileAssociatedVisitName;
        Long fileUploadDate;
        String filePreviewBase64;
        String filePreviewStatus;
        String contentDocumentId;
        Boolean isImage;
        Integer fileSize;
        String fileType;

        private MobileDocumentWrapper(VTD1_Document__c document) {
            this.documentId = document.Id;
            this.fileName = document.VTD1_FileNames__c;
            System.debug('HERE' + document);
            this.fileCategory = document.VTR3_Category__c;
            this.fileCategoryTranslated = String.valueOf(document.get('translatedCategory'));
            this.fileNickname = document.VTD1_Nickname__c;
            this.fileComment = document.VTD1_Comment__c;
            this.fileStatus = String.valueOf(document.get('translatedStatus'));
            this.fileUserFirstName = document.CreatedBy.FirstName;
            this.fileUserLastName = document.CreatedBy.LastName;
            this.fileAssociatedVisitId = document.VTR3_Associated_Visit__c;
            this.fileAssociatedVisitName = document.VTR3_Associated_Visit__r.Name;
            this.fileUploadDate = document.CreatedDate.getTime();

            if (!document.AttachedContentDocuments.isEmpty()) {
                this.fileExtension = document.AttachedContentDocuments[0].FileExtension;
                this.contentDocumentId = document.AttachedContentDocuments[0].ContentDocumentId;
                this.fileSize = document.AttachedContentDocuments[0].ContentSize;
                this.isImage = IMAGE_FILE_EXTENSION.contains(fileExtension);
                this.fileType = VT_R3_RestHelper.FILE_CATEGORIES_MAPPING.get(fileExtension);
            }
        }

        private MobileDocumentWrapper addDownloadLink() {
            List<ContentVersion> versions = [SELECT Id FROM ContentVersion WHERE ContentDocumentId = :this.contentDocumentId ORDER BY CreatedDate DESC];
            String lastContentVersionId;
            if (!versions.isEmpty()) {
                lastContentVersionId = versions.get(0).Id;
                this.fileDownloadLink = System.Url.getSalesforceBaseUrl().toExternalForm() + VT_D1_HelperClass.getSandboxPrefix() + '/services/data/v46.0/sobjects/ContentVersion/' + lastContentVersionId + '/VersionData';
            }
            return this;
        }

        private MobileDocumentWrapper addPreview(String preview) {
            this.filePreviewBase64 = preview;
            return this;
        }
    }

    private static String getPreview(String contentDocumentId) {
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
        req.setEndpoint(Url.getOrgDomainUrl().toExternalForm() + '/services/data/v45.0/connect/communities/' + Network.getNetworkId() + '/files/' + contentDocumentId + '/rendition?type=THUMB720BY480');
        Http http = new Http();
        try {
            HttpResponse res = http.send(req);
            if (res.getStatusCode() == 200) {
                return EncodingUtil.base64Encode(res.getBodyAsBlob());
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }

    }

    private static void addPreviews(List<MobileDocumentWrapper> income) {
        for (MobileDocumentWrapper mdw : income) {
            mdw.filePreviewStatus = getPreview(mdw.contentDocumentId);
            if (mdw.filePreviewStatus != null) {
                mdw.filePreviewStatus = 'Available';
            } else {
                mdw.filePreviewStatus = 'NotAvailable';
            }
        }
    }

    private static Integer countDocuments(Id caseId) {
        return [
                SELECT count()
                FROM VTD1_Document__c
                WHERE VTD1_Clinical_Study_Membership__c = :caseId AND
                (((RecordTypeId IN :medicalRecordTypes) AND (NOT (VTD1_Does_file_needs_deletion__c = 'Yes' AND VTD1_Deletion_Reason__c = 'Not Relevant'))) OR (RecordTypeId IN :visitDocumentRecordTypes))
        ];
    }
    private static void parseParams(RestRequest request) {
        if (request.params.containsKey('offset')) {
            studyDocumentsOffset = Integer.valueOf(request.params.get('offset'));
        }
        if (request.params.containsKey('limit')) {
            studyDocumentsLimit = Integer.valueOf(request.params.get('limit'));
        }
    }

    static Integer studyDocumentsOffset = 0;//DEFAULT VALUE
    static Integer studyDocumentsLimit = 40;//DEFAULT VALUE
    static List<String> medicalRecordTypes;
    static List<String> visitDocumentRecordTypes;
    private static String BIG_PREVIEW;
    private static Set<String> IMAGE_FILE_EXTENSION;
    public void initService() {
        IMAGE_FILE_EXTENSION = new Set<String>{
                'jpg', 'gif', 'png', 'jpeg', 'bmp'
        };
        BIG_PREVIEW = 'big-thumbnail';
        medicalRecordTypes = new List<String>{
                VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_ARCHIVED,
                VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_REJECTED,
                VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_RELEASE_FORM,
                VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD
        };
        visitDocumentRecordTypes = new List<String>{
                VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT
        };
    }
}