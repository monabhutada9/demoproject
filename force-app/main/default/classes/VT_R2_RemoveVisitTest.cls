/**
 * Created by Michael Salamakha on 20.02.2019.
 */

@IsTest
private class VT_R2_RemoveVisitTest {
    @testSetup
    static void setup() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        study.VTR2_FirstVisitStatus__c = 'Scheduled';
        study.VTD1_Protocol_Nickname__c = 'SomeProtocolName';
        update study;
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();

        VTD1_ProtocolVisit__c pVisit = new VTD1_ProtocolVisit__c(
                VTR2_Visit_Participants__c = 'Patient; Patient Guide; PI',
                VTD1_Study__c = study.Id
        );
        insert pVisit;

        Case cas = [select Id, ContactId from Case WHERE RecordType.DeveloperName = 'CarePlan'];
        cas.VTD1_Study__c = study.Id;
        update cas;

        VTD1_Actual_Visit__c visit = new VTD1_Actual_Visit__c();
        visit.VTD1_Protocol_Visit__c = pVisit.Id;
        visit.VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today()+1;
        visit.VTD1_Scheduled_Date_Time__c = Date.today()+1;
        visit.VTR2_Televisit__c = false;
        visit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED;
        visit.VTD1_Case__c = cas.Id;
        visit.VTD1_Contact__c = cas.ContactId;
        insert visit;

        insert new Visit_Member__c(
                VTD1_Participant_User__c = null,
                VTD1_Actual_Visit__c = visit.id,
                VTD1_External_Participant_Type__c = 'HHN',
                RecordTypeId=Schema.SObjectType.Visit_Member__c.getRecordTypeInfosByDeveloperName().get('VTR2_ExternalMember').getRecordTypeId(),
                VTD1_Name_HHN_Phleb__c = 'Vist Member Name');
        List <Visit_Member__c> visitMembers = [SELECT Id, VTD1_Participant_User__c FROM Visit_Member__c WHERE VTD1_Participant_User__c != null AND VTD1_External_Participant_Type__c = 'Patient Guide'];
        List <Visit_Member__c> visitMembers2 = [SELECT Id, VTD1_Participant_User__c FROM Visit_Member__c WHERE VTD1_Participant_User__c != null AND VTD1_External_Participant_Type__c != 'Patient Guide'];

        Event event  = new Event(VTD1_Actual_Visit__c = visit.id, OwnerId=visitMembers[0].VTD1_Participant_User__c,
                DurationInMinutes=30, WhatId = visit.Id, ActivityDateTime=System.now());
        insert event;

        List<EventRelation> eventRelations = new List<EventRelation>();
        eventRelations.add(new EventRelation(RelationId = visitMembers2[0].VTD1_Participant_User__c,
                EventId=event.id));
        eventRelations.add(new EventRelation(RelationId = visitMembers2[1].VTD1_Participant_User__c,
                EventId=event.id));
        insert eventRelations;
    }

    @isTest
    static void testDeletion() {
        /**
         * RECOMMITTING
         */
        HttpCalloutMock cMock = new CalloutMock();
        Test.setMock(HttpCalloutMock.class, cMock);
        Test.startTest();
        delete [SELECT Id FROM Visit_Member__c WHERE VTD1_External_Participant_Type__c = 'Patient Guide' OR VTD1_External_Participant_Type__c = 'HHN'];
        Test.stopTest();
    }

    public with sharing class CalloutMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/xml');
            List<Video_Conference__c> vConf = [SELECT Id,SessionId__c FROM Video_Conference__c];
            System.assertNotEquals(0,vConf.size());
            res.setBody(JSON.serialize(new List<String>{vConf[0].SessionId__c}));
            res.setStatusCode(200);
            return res;
        }
    }
}