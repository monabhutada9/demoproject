/**
 * Created by Vlad Tyazhov on 12.09.2019.
 */
@IsTest
public with sharing class VT_D1_DocumentCHandlerTest {

    public static void TestDocs() {
        Case c = [SELECT Id, VTD1_PI_contact__c, VTD1_Study__c FROM Case LIMIT 1];
        // setup email for PI contact manually
        Contact contact = [SELECT Id, Email FROM Contact WHERE Id = :c.VTD1_PI_contact__c];
        contact.Email = 'test@test.test';
        update contact;
        VTD1_Document__c doc = new VTD1_Document__c(
                VTD1_Study__c=c.VTD1_Study__c,
                VTD1_Clinical_Study_Membership__c=c.Id,
                RecordTypeId=VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT,
                VTR4_Signature_Type__c = 'Wet',
                VTD2_PI_Signature_required__c = true
        );
        insert doc;
        VTD1_Document__c docCerf = new VTD1_Document__c(
                VTD1_Study__c=c.VTD1_Study__c,
                VTD1_Clinical_Study_Membership__c=c.Id,
                VTD2_Associated_Medical_Record_id__c = doc.Id,
                RecordTypeId=VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON,
                VTR4_Signature_Type__c = 'Wet',
                VTD2_PI_Signature_required__c = true
               // VTD1_Status__c='Certified', 
              //  VTD1_Signature_Date__c = Date.today()
        );
        insert docCerf;
        test.startTest();
        doc.VTD1_Status__c = 'Pending approval';
        update doc;
        docCerf.VTD1_Status__c = 'TMF Started';
        update docCerf;
        docCerf.VTD1_Status__c='Certified';
        docCerf.VTD1_Signature_Date__c=Date.today().addDays(1);
        update docCerf;
              //  Test.startTest();
                delete doc;
                Test.stopTest();
    }

    public static void doTestMedRecApproval() {
        Case c = [SELECT Id, VTD1_PI_contact__c, VTD1_Study__c FROM Case LIMIT 1];
        // setup email for PI contact manually
        Contact contact = [SELECT Id, Email FROM Contact WHERE Id = :c.VTD1_PI_contact__c];
        contact.Email = 'test@test.test';
        update contact;
        // create doc
        VTD1_Document__c doc = new VTD1_Document__c(
                VTD1_Study__c=c.VTD1_Study__c,
                VTD1_Clinical_Study_Membership__c=c.Id,
                RecordTypeId=VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD
        );
        insert doc;
        doc.VTD1_Status__c = 'Pending approval';
        update doc;
        doc.VTD1_Status__c = 'Approved';
        update doc;
        doc.VTD1_Status__c = 'Certified';
        update doc;
        // delete doc;
    }

    public static void doCheckAppliedPatients() {
        HealthCloudGA__CarePlanTemplate__c stdy = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        Study_Team_Member__c studyTeamMember = [SELECT Id, Name FROM Study_Team_Member__c LIMIT 1];
        VTD1_Regulatory_Document__c regulatoryDocumentPAsignature = new VTD1_Regulatory_Document__c(
                Name = 'TestRegularDocument',
                VTD1_Document_Type__c = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_SIGNATURE_PAGE,
                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI'
        );
        insert regulatoryDocumentPAsignature;
        VTD1_Regulatory_Document__c regulatoryDocumentPA = new VTD1_Regulatory_Document__c(
                Name = 'TestRegularDocument',
                VTD1_Document_Type__c = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_PROTOCOL_AMENDMENT,
                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI'
        );
        insert regulatoryDocumentPA;
        VTD1_Regulatory_Binder__c regulatoryBinderPAsignature = new VTD1_Regulatory_Binder__c(
                VTD1_Regulatory_Document__c = regulatoryDocumentPAsignature.Id,
                VTD1_Care_Plan_Template__c = stdy.Id
        );
        insert regulatoryBinderPAsignature;
        VTD1_Regulatory_Binder__c regulatoryBinderPA = new VTD1_Regulatory_Binder__c(
                VTD1_Regulatory_Document__c = regulatoryDocumentPA.Id,
                VTD1_Care_Plan_Template__c = stdy.Id
        );
        insert regulatoryBinderPA;
        VTD1_Protocol_Amendment__c protocolAmendment = new VTD1_Protocol_Amendment__c(
                Name = 'TestPA',
                VTD1_Study_ID__c = stdy.Id
        );
        insert protocolAmendment;
        Virtual_Site__c virtualSite = new Virtual_Site__c(
                VTD1_Study__c = stdy.Id,
                VTD1_Study_Team_Member__c = studyTeamMember.Id,
                VTD1_Study_Site_Number__c = '129839'
        );
        insert virtualSite;
        VTD1_Document__c docum = new VTD1_Document__c(
                VTD1_Name__c = 'TestDoc',
                VTD1_Study__c = stdy.Id,
                VTD1_Protocol_Amendment__c = protocolAmendment.Id,
                VTD1_Regulatory_Binder__c = regulatoryBinderPAsignature.Id,
                VTD1_Site__c = virtualSite.Id,
                RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT
        );
        insert docum;
       // docum.VTD1_Regulatory_Document_Type__c = 'EDPRC';
        docum.VTD1_Lifecycle_State__c='Approved';
        docum.VTD1_Status__c='Site RSU Complete';
        update docum;
        docum.VTD1_Status__c='Certified';
        docum.VTD1_Signature_Date__c=Date.today();
        update docum;
        VTD1_Protocol_Amendment__c pa = [SELECT VTD1_Changes_Applied_to_All_Patients__c, Name FROM VTD1_Protocol_Amendment__c LIMIT 1];
        System.assertEquals(false, pa.VTD1_Changes_Applied_to_All_Patients__c);
        List<VTD1_Document__c> docs = [
                SELECT VTR4_ChangesAppliedToAllPatients__c, VTD1_Name__c, VTD1_Protocol_Amendment__r.Name, VTD1_Protocol_Amendment__c
                FROM VTD1_Document__c
                WHERE VTD1_Protocol_Amendment__c =: pa.Id
        ];
        System.assert(docs.size() > 1);
        for (VTD1_Document__c doc : docs) {
            doc.VTR4_ChangesAppliedToAllPatients__c = true;
        }
        update docs;
        VTD1_Protocol_Amendment__c paChangedtoTrue = [SELECT VTD1_Changes_Applied_to_All_Patients__c, Name FROM VTD1_Protocol_Amendment__c LIMIT 1];
        System.assertEquals(true, paChangedtoTrue.VTD1_Changes_Applied_to_All_Patients__c);
        docs[0].VTR4_ChangesAppliedToAllPatients__c = false;
        update docs[0];
        VTD1_Protocol_Amendment__c paChangedtoFalse = [SELECT VTD1_Changes_Applied_to_All_Patients__c, Name FROM VTD1_Protocol_Amendment__c LIMIT 1];
        System.assertEquals(false, paChangedtoFalse.VTD1_Changes_Applied_to_All_Patients__c);
        delete docs[0];
        VTD1_Protocol_Amendment__c paChangedtoTrueDel = [SELECT VTD1_Changes_Applied_to_All_Patients__c, Name FROM VTD1_Protocol_Amendment__c LIMIT 1];
        System.assertEquals(true, paChangedtoTrueDel.VTD1_Changes_Applied_to_All_Patients__c);
        docs = [
                SELECT VTR4_ChangesAppliedToAllPatients__c, VTD1_Name__c, VTD1_Protocol_Amendment__r.Name, VTD1_Protocol_Amendment__c
                FROM VTD1_Document__c
                WHERE VTD1_Protocol_Amendment__c =: pa.Id
        ];
        System.assert(docs.size() == 1);
        delete docs[0];
        paChangedtoTrueDel = [SELECT VTD1_Changes_Applied_to_All_Patients__c, Name FROM VTD1_Protocol_Amendment__c LIMIT 1];
        System.assertEquals(false, paChangedtoTrueDel.VTD1_Changes_Applied_to_All_Patients__c);
    }

    public static void testDeleteContentDocument() {
        VT_R3_GlobalSharing.disableForTest = true;  // fix for SH Perf 01 by Vlad Tyazhov

        HealthCloudGA__CarePlanTemplate__c stdy = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        Study_Team_Member__c studyTeamMember = [SELECT Id, Name FROM Study_Team_Member__c LIMIT 1];
        VTD1_Regulatory_Document__c regulatoryDocumentPAsignature = new VTD1_Regulatory_Document__c(
                Name = 'TestRegularDocument',
                VTD1_Document_Type__c = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_SIGNATURE_PAGE,
                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI'
        );
        insert regulatoryDocumentPAsignature;

        VTD1_Regulatory_Document__c regulatoryDocumentPA = new VTD1_Regulatory_Document__c(
                Name = 'TestRegularDocument',
                VTD1_Document_Type__c = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_PROTOCOL_AMENDMENT,
                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI'
        );
        insert regulatoryDocumentPA;

        VTD1_Regulatory_Binder__c regulatoryBinderPAsignature = new VTD1_Regulatory_Binder__c(
                VTD1_Regulatory_Document__c = regulatoryDocumentPAsignature.Id,
                VTD1_Care_Plan_Template__c = stdy.Id
        );
        insert regulatoryBinderPAsignature;

        VTD1_Regulatory_Binder__c regulatoryBinderPA = new VTD1_Regulatory_Binder__c(
                VTD1_Regulatory_Document__c = regulatoryDocumentPA.Id,
                VTD1_Care_Plan_Template__c = stdy.Id
        );
        insert regulatoryBinderPA;

        VTD1_Protocol_Amendment__c protocolAmendment = new VTD1_Protocol_Amendment__c(
                Name = 'TestPA',
                VTD1_Study_ID__c = stdy.Id
        );
        insert protocolAmendment;

        Virtual_Site__c virtualSite = new Virtual_Site__c(
                VTD1_Study__c = stdy.Id,
                VTD1_Study_Team_Member__c = studyTeamMember.Id,
                VTD1_Study_Site_Number__c = '129839'
        );
        insert virtualSite;

        VTD1_Document__c docum = new VTD1_Document__c(
                VTD1_Name__c = 'TestDoc',
                VTD1_Study__c = stdy.Id,
                VTD1_Protocol_Amendment__c = protocolAmendment.Id,
                VTD1_Regulatory_Binder__c = regulatoryBinderPAsignature.Id,
                VTD1_Site__c = virtualSite.Id,
                RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT
        );
        insert docum;

        ContentVersion cv = new ContentVersion(
                PathOnClient = 'test.txt',
                Title = 'Test file',
                VersionData = Blob.valueOf('Test Data')
        );
        insert cv;
        System.assertEquals(1, [SELECT COUNT() FROM ContentDocument]);

        Id cdId = [SELECT Id FROM ContentDocument ORDER BY CreatedDate DESC LIMIT 1].Id;

        ContentVersion cv2 = new ContentVersion(
                PathOnClient = 'test.txt',
                Title = 'Test file',
                VersionData = Blob.valueOf('Test Data'),
                ContentDocumentId = cdId
        );
        insert cv2;

        ContentDocumentLink cdl = new ContentDocumentLink(ShareType = 'V', LinkedEntityId = docum.Id, ContentDocumentId = cdId);
        insert cdl;

        System.assertEquals(2, [SELECT Id FROM ContentVersion WHERE ContentDocumentId = :cdId].size());
        User piUSER = [SELECT Id FROM User WHERE Profile.Name = 'Primary Investigator' LIMIT 1];
   //     system.runAs(piUSER) {
            Test.startTest();
            VT_D1_DocumentCHandlerWithoutSharing.deleteContentDocument(cdId, docum.Id, 'Redundant document.');
            Test.stopTest();
   //     }
        VTD1_Document__c vtDocument = [
                SELECT VTR4_DeletionReason__c, VTD1_FileNames__c, VTD1_Version__c, VTD1_Lifecycle_State__c, VTD1_Status__c, VTD1_Deletion_User__c, VTD1_Deletion_Date__c
                FROM VTD1_Document__c
                WHERE Id = :docum.Id
        ];
        System.assertEquals(Datetime.now().format('h:mm a'), vtDocument.VTD1_Deletion_Date__c.format('h:mm a'));
        System.assertEquals('Redundant document.', vtDocument.VTR4_DeletionReason__c);
        System.assertEquals(null, vtDocument.VTD1_FileNames__c);
        System.assertEquals(null, vtDocument.VTD1_Version__c);
        System.assertEquals(null, vtDocument.VTD1_Lifecycle_State__c);
        System.assertEquals(null, vtDocument.VTD1_Status__c);

        //   system.assertEquals(piUSER.Id, vtDocument.VTD1_Deletion_User__c);
        //   system.assertEquals(Date.today(), vtDocument.VTD1_Deletion_Date__c);
        System.assertEquals(UserInfo.getUserId(), vtDocument.VTD1_Deletion_User__c);
        System.assertEquals(0, [SELECT Id FROM ContentVersion WHERE ContentDocumentId = :cdId].size());
        System.assertEquals(0, [SELECT Id FROM ContentDocument WHERE Id = :cdId].size());
    }

    public static void testValidateCraReviewCompletedCheckbox() {
        Case c = [SELECT Id, VTD1_PI_contact__c, VTD1_Study__c FROM Case LIMIT 1];
        VTD1_Document__c doc = new VTD1_Document__c(
                VTR4_ReviewCompleted__c = false,
                VTD1_Study__c=c.VTD1_Study__c,
                VTD1_Status__c = null,
                VTD1_Clinical_Study_Membership__c=c.Id,
                RecordTypeId=VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT,
                VTR4_Signature_Type__c = 'Wet',
                VTD2_PI_Signature_required__c = true
        );
        insert doc;
        Set<String> profilesToSelect = new Set<String> {
                VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME
        };
        System.debug('CRA profile: '+profilesToSelect);
        Set<Id> forbiddenProfileIds = new Map<Id, Profile>([SELECT Id FROM Profile WHERE Name IN :profilesToSelect]).keySet();
        System.debug('set id profile :' + forbiddenProfileIds);
        Id idForbiddenProfile ;
        for(Id idexample : forbiddenProfileIds){
            idForbiddenProfile = idexample;
            break;
        }
        User userT = (User) new DomainObjects.User_t()
            .setProfile(idForbiddenProfile)
            .persist();
       // User CRAUsr = [SELECT Id FROM User WHERE ProfileId =: forbiddenProfileIds LIMIT 1];
        boolean isErrorCatched = false;
        List<VTD1_Document__c> getDocument = [
                SELECT  Id, Name, VTR4_ReviewCompleted__c, VTD1_Status__c
                FROM    VTD1_Document__c
                WHERE   VTR4_ReviewCompleted__c = FALSE
                LIMIT   1

        ];
        VTD1_Document__Share documentObjectSharing = new VTD1_Document__Share();
        documentObjectSharing.ParentId = getDocument[0].Id;
        documentObjectSharing.UserOrGroupId = userT.Id;
        documentObjectSharing.AccessLevel = 'Edit';
        documentObjectSharing.RowCause = Schema.VTD1_Document__Share.RowCause.Manual;
        insert documentObjectSharing;

        System.debug('USer! ' + userT);
        System.runAs(userT) {

            System.debug('docum: ' + getDocument);
            getDocument[0].VTR4_ReviewCompleted__c = true;
//            getDocument[0].VTD1_Status__c = 'testStatus';
            System.debug('getDocument' + getDocument);
            System.debug(getDocument[0].VTD1_Status__c);
            try {
                update getDocument;
            } catch (DMLException e) {
                System.debug('DML exc');
                System.debug('except : ' + e.getMessage());
                if (e.getMessage().contains(Label.VTR4_ErrorDocumentReviewCheckboxChecked)) {
                    isErrorCatched = true;
                }
            }
        }
        System.assertEquals(true, isErrorCatched);
    }
}