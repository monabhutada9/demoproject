/***********************************************************************
* @author              Aarohi Sanjekar <aarohi.sanjekar@quintiles.com>
* @date                30-Oct-2020
* @group               Cookies Opt-in/Out
* @group-content       http://jira.quintiles.net/browse/SH-19347
* @description         Batch to update 'Cookies Accepted Details'field of Contact object for existing records
*/
global class VT_R5_UpdateCookieAcceptFieldBatch implements Database.Batchable<sObject> {
    /*******************************************************************************************************
* @description Return the contact records that have data in VTR3_CookiesAcceptedDetails__c
* @param Contact
* @param VTR3_CookiesAcceptedDetails__c
* @return the batches of records to be passed to execute
*/
    global Database.QueryLocator start(Database.BatchableContext BC) {

        Set<RecordType> rt = new Set<RecordType>([SELECT id from RecordType where DeveloperName IN('Patient','PI','VTR2_SCR','Caregiver')]);

        String query = 'SELECT id,VTR3_CookiesAcceptedDetails__c from Contact where VTR3_CookiesAcceptedDetails__c!=NULL and RecordTypeId IN :rt';
        
        return Database.getQueryLocator(query);
        
        
    }
    /*******************************************************************************************************
* @description process each batch of records
* @param Contact
* @param VTR3_CookiesAcceptedDetails__c
* @return Update the field VTR3_CookiesAcceptedDetails__c on contact
*/
    global void execute(Database.BatchableContext BC, List<Contact> scope) {
        try{
            for(Contact con : scope)
            {
                
                if(!String.isBlank(con.VTR3_CookiesAcceptedDetails__c)){
                    if( Test.isRunningTest() && con.VTR3_CookiesAcceptedDetails__c.equalsIgnoreCase('Test') )
                    { 
                        throw new VT_D1_Test_DataFactory.MyException('testing'); 
                    } 
                    con.VTR3_CookiesAcceptedDetails__c = NULL ;
                }
                
                
            }
            VT_D1_ContactTriggerHandler.byPassContactTrigger();
            HandlerExecutionPool.getInstance().addHandlerToDisabledList('VT_R5_ContactCaregiverHandler');
            HandlerExecutionPool.getInstance().addHandlerToDisabledList('VTR4_PatientFieldLockService_Contact');
            
            
            update scope;
            
        }Catch (Exception ex){
            //System.debug('Failed to update contact'+ ex.getMessage());
            ErrorLogUtility.logException(ex, ErrorLogUtility.ApplicationArea.APP_AREA_DELETE_COOKIES, VT_R5_UpdateCookieAcceptFieldBatch.class.getName());
            
        }
    }
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
    }
    public static Id runBatch(Integer batchSize){
        return database.executeBatch(new VT_R5_UpdateCookieAcceptFieldBatch(),batchSize);
    }
}