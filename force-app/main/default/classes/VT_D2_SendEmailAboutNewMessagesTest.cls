/**
 * Created by shume on 29.01.2019.
 */

@IsTest
private class VT_D2_SendEmailAboutNewMessagesTest {
    @TestSetup
    static void testSetup() {

        User patientUser =(User) new DomainObjects.User_t()
                .setProfile('Patient')
                .addContact(new DomainObjects.Contact_t()
                    .setVTD2_Send_notifications_as_emails(true))

                .persist();
        VTD1_NotificationC__c patientNotification = new VTD1_NotificationC__c(
                Type__c = 'Messages',
                Message__c = 'Message text',
                VTD1_Receivers__c = patientUser.Id,
                Number_of_unread_messages__c = 2
        );
        insert patientNotification;
    }

    @IsTest
    static void testBehavior() {
        Test.startTest();
        VT_D2_SendEmailAboutNewMessages sendEmail = new VT_D2_SendEmailAboutNewMessages();
        Database.executeBatch(sendEmail);
        Test.stopTest();
    }
}