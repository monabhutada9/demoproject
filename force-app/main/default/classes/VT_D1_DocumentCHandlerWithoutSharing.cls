public without sharing class VT_D1_DocumentCHandlerWithoutSharing {

    private static String DEBUG_HEADER = 'VT_D1_DocumentCHandlerWithoutSharing: ';

    public static void postToCMPool(List<VTD1_Document__c> documents, Map<Id, VTD1_Document__c> oldMap) {
        List<VTD1_Document__c> docsToProcess = new List<VTD1_Document__c>();
        Set<Id> caseIds = new Set<Id>();
        for(VTD1_Document__c doc: documents) {

            if((doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD
                    || doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON)
                    && doc.VTD1_Status__c == 'Approved'
                    && doc.VTD1_Clinical_Study_Membership__c != null
                    && (oldMap == null || doc.VTD1_Status__c != oldMap.get(doc.Id).VTD1_Status__c)) {
                docsToProcess.add(doc);
                caseIds.add(doc.VTD1_Clinical_Study_Membership__c);
            }
        }
        if (caseIds.isEmpty()) {
            return;
        }
        VTD1_RTId__c rtIds = VTD1_RTId__c.getInstance();
        Map<Id, Case> casesMap = new Map<Id, Case>([SELECT Id, VTD1_Study__r.VTD2_CM_Group_Id__c FROM Case WHERE Id IN :caseIds]);
        List<FeedItem> postList = new List<FeedItem>();
        for (VTD1_Document__c doc: docsToProcess) {
            Id cmGroupId = casesMap.get(doc.VTD1_Clinical_Study_Membership__c).VTD1_Study__r.VTD2_CM_Group_Id__c;
            if (cmGroupId != null) {
                FeedItem post = new FeedItem();
                post.ParentId = cmGroupId;
                post.Body = System.Label.VTD2_MedicalRecordsApproved + ' - ' + doc.Name + ' ' + rtIds.VTD1_Org_Current_Instance__c + '/' + doc.Id;
                postList.add(post);
            }
        }
        if (!postList.isEmpty()) {

            insert postList;
        }
    }

    @AuraEnabled
    public static void deleteContentDocument(Id contentDocumentId, Id vtDocumentId, String deletionReason) {
        if (contentDocumentId == null || vtDocumentId == null || String.isBlank(deletionReason)) {
            return;
        }

        List<VTD1_Document__c> documents = [SELECT VTD1_Study__r.VTD1_StudyStatus__c, VTD1_FileNames__c FROM VTD1_Document__c WHERE Id = :vtDocumentId];

        if (documents.isEmpty() || documents[0].VTD1_Study__r.VTD1_StudyStatus__c == Label.VTR4_StudyStatusCompleted) {
            throw new AuraHandledException('Document with Id ' + vtDocumentId + ' not found or it\'s Study is Completed');
        }

        try {
            update new VTD1_Document__c(
                    Id = vtDocumentId,
                    VTR4_DeletionReason__c = deletionReason,

                    VTR4_DeletionFileName__c = documents[0].VTD1_FileNames__c,

                    VTD1_FileNames__c = null,
                    VTD1_Version__c = null,
                    VTD1_Lifecycle_State__c = null,
                    VTD1_Signature_Date__c = null,
                    VTD1_Status__c = null
            );

            delete new ContentDocument(Id = contentDocumentId);
        } catch (DmlException e) {
            System.debug(
                    LoggingLevel.ERROR, DEBUG_HEADER + 'deleteContentDocument. Document with Id ' + contentDocumentId
                            + ' can\'t be deleted or ' + vtDocumentId + ' updated. ' + e.getMessage()
            );
            throw new AuraHandledException(
                    'deleteContentDocument. Document with Id ' + contentDocumentId + ' can\'t be deleted or ' + vtDocumentId + ' updated. ' + e.getStackTraceString()
            );
        }
    }


    public static void setFileNamesOnDocument(Map<Id, ContentDocumentLink> newContentDocMap, List<ContentDocumentLink> deletedList) {
        //find VTD1_Document recored content document is connected with
        //map<Id, Id> contentDocId_vtd1DocId = new map<Id, Id>();
        List<VTD1_Document__c> docContainersList = new List<VTD1_Document__c>();
        if (UserInfo.getUserType() != 'Guest') {
            if (newContentDocMap == null) {
                docContainersList = getDocContainersList(deletedList);
            } else if (deletedList == null) {
                docContainersList = getDocContainersList(newContentDocMap.keySet());
            }
        }
        if (docContainersList == null || docContainersList.isEmpty()) return;
        /*
        for(VTD1_Document__c docItem:docContainersList){
            docItem.VTD1_ContainsFiles__c=true;
        }
        update docContainersList;
        */


        Map<Id, VTD1_Document__c> docId_doc = new Map<Id, VTD1_Document__c>(docContainersList);
        //go through VTD1_Document__c to see if it has the files and make a list of file's names
        setSharedFileNamesByVTD1Documents(docContainersList, docId_doc);
    }


    private static List<VTD1_Document__c> getDocContainersList(Set<Id> newContentDocIdsSet) {
        Set<Id> vtd1DocIds = new Set<Id>();
        List<ContentDocumentLink> cdlList = [SELECT Id, LinkedEntityId, ContentDocument.LatestPublishedVersion.VTD1_Uploaded_by_Guest__c FROM ContentDocumentLink WHERE Id IN: newContentDocIdsSet];
        for (ContentDocumentLink ctlItem : cdlList) {
            if (!ctlItem.ContentDocument.LatestPublishedVersion.VTD1_Uploaded_by_Guest__c && String.valueOf((ctlItem.LinkedEntityId).getSobjectType()) == VT_R4_ConstantsHelper_Documents.SOBJECT_DOC_CONTAINER) {
                vtd1DocIds.add(ctlItem.LinkedEntityId);
                System.debug('!!! LinkedEntityId=' + ctlItem.LinkedEntityId);
            }
        }
        if (vtd1DocIds.isEmpty()) return null;
        List<VTD1_Document__c> docContainersList = [
                SELECT  Id,
                        VTD1_FileNames__c,
                        VTD1_Version__c,
                        VTD1_Lifecycle_State__c,
                        VTD1_Signature_Date__c,
                        VTD1_Status__c,
                        VTD1_Deletion_Date__c,
                        VTD1_Deletion_User__c,
                        VTR4_DeletionFileName__c,
                        VTR4_DeletionUserName__c
                FROM    VTD1_Document__c
                WHERE   Id IN:vtd1DocIds
        ];
        return docContainersList;
    }


    private static List<VTD1_Document__c> getDocContainersList(List<ContentDocumentLink> deletedList) {
        Set<Id> vtd1DocIds = new Set<Id>();
        for (ContentDocumentLink ctlItem : deletedList) {
            if (String.valueOf((ctlItem.LinkedEntityId).getSobjectType()) == VT_R4_ConstantsHelper_Documents.SOBJECT_DOC_CONTAINER) {
                vtd1DocIds.add(ctlItem.LinkedEntityId);
                System.debug('!!! LinkedEntityId=' + ctlItem.LinkedEntityId);
            }
        }
        if (vtd1DocIds.isEmpty()) return null;
        List<VTD1_Document__c> docContainersList = [
                SELECT  Id,
                        VTD1_FileNames__c,
                        VTD1_Version__c,
                        VTD1_Lifecycle_State__c,
                        VTD1_Signature_Date__c,
                        VTD1_Status__c,
                        VTD1_Deletion_Date__c,
                        VTD1_Deletion_User__c,
                        VTR4_DeletionFileName__c,
                        VTR4_DeletionUserName__c
                FROM    VTD1_Document__c
                WHERE   Id IN:vtd1DocIds
        ];
        return docContainersList;
    }


    public static void setSharedFileNamesByVTD1Documents(List<VTD1_Document__c> docContainersList, map<Id, VTD1_Document__c> docId_doc) {
        //set file Names for VTD1_Document records
        Set<Id> docIds = docId_doc.keySet();
        if (docIds == null || docIds.isEmpty()) return;


        List<ContentDocumentLink> contentDocLinkList = [
                SELECT ContentDocumentId, ContentDocument.Title, Id, Visibility, ShareType, LinkedEntityId,
                        ContentDocument.FileExtension
                FROM ContentDocumentLink
                WHERE LinkedEntityId in:docIds
        ];
        //if(contentDocLinkList.isEmpty()) return;
        Map<Id, String> docId_fileNames = new Map<Id, String>();
        if (! contentDocLinkList.isEmpty()) {
            for (ContentDocumentLink linkItem : contentDocLinkList) {
                String title = linkItem.ContentDocument.Title;


                String fileExtension = linkItem.ContentDocument.FileExtension != null ? '.' + linkItem.ContentDocument.FileExtension : '';
                String fileName = title + (title.endsWithIgnoreCase(fileExtension) ? '' : fileExtension);
                if (! docId_fileNames.containsKey(linkItem.LinkedEntityId)) {
                    docId_fileNames.put(linkItem.LinkedEntityId, fileName);
                } else {
                    String names = docId_fileNames.get(linkItem.LinkedEntityId);
                    docId_fileNames.put(linkItem.LinkedEntityId, names + ', ' + fileName);
                }
                System.debug('docId=' + linkItem.LinkedEntityId + ', file names=' + docId_fileNames.get(linkItem.LinkedEntityId));
            }
        }


        for (Id docId : docId_doc.keySet()) {
            if (docId_fileNames.containsKey(docId)) {
                docId_doc.get(docId).VTD1_FileNames__c = docId_fileNames.get(docId);
            } else {
                if (docId_doc.get(docId).VTD1_FileNames__c != null) {
                    docId_doc.get(docId).VTR4_DeletionFileName__c = docId_doc.get(docId).VTD1_FileNames__c;
                }
                docId_doc.get(docId).VTD1_FileNames__c = null;
                docId_doc.get(docId).VTD1_Version__c = null;
                docId_doc.get(docId).VTD1_Lifecycle_State__c = null;
                docId_doc.get(docId).VTD1_Signature_Date__c = null;
                docId_doc.get(docId).VTD1_Status__c = null;
                docId_doc.get(docId).VTD1_Deletion_Date__c = DateTime.now();
                docId_doc.get(docId).VTD1_Deletion_User__c = UserInfo.getUserId();
                docId_doc.get(docId).VTR4_DeletionUserName__c = UserInfo.getName();


            }
        }
        if (docId_doc.values() != null && ! docId_doc.values().isEmpty()) update docId_doc.values();
    }


    public static void clearDocumentDeletionReasonWhenFileInserted(Handler.TriggerContext context) {
        Set<Id> documentIds = new Set<Id>();
        String documentSobjectName = VTD1_Document__c.getSObjectType().getDescribe().getName();
        for (ContentDocumentLink contentDocumentLink : (List<ContentDocumentLink>) context.newList) {
            String LinkedEntitySobjectName = contentDocumentLink.LinkedEntityId.getSobjectType().getDescribe().getName();
            if (documentSobjectName == LinkedEntitySobjectName) {
                documentIds.add(contentDocumentLink.LinkedEntityId);
            }
        }
        if (!documentIds.isEmpty()) {
            List<VTD1_Document__c> documentsToUpdate = [


                    SELECT Id, VTR4_DeletionReason__c, VTD1_Deletion_Date__c, VTD1_Deletion_User__c, VTR4_DeletionUserName__c, VTR4_DeletionFileName__c


                    FROM VTD1_Document__c
                    WHERE Id IN :documentIds
            ];
            for (VTD1_Document__c document : documentsToUpdate) {
                document.VTR4_DeletionReason__c = null;
                document.VTD1_Deletion_Date__c = null;
                document.VTD1_Deletion_User__c = null;
				document.VTR4_DeletionFileName__c = null;
                document.VTR4_DeletionUserName__c = null;
            }
            update documentsToUpdate;
        }
    }
}