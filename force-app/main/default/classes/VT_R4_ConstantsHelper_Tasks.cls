/**
 * Modified by Harshita Khandelwal on 19.10.2020 for SH-17444 (Multiple CRA)
 */
public with sharing class VT_R4_ConstantsHelper_Tasks {
    public static final String VISIT_REQUEST_TASK_SUBJECT = 'Manually Schedule Visit';
    public static final String TASK_CATEGORIES_OTHER_TASKS = 'Other Tasks';

    public static final String SURVEY_SCORING_TASK_SUBJECT = 'Score ePRO Entry';
    public static final String SURVEY_PATIENT_TASK_SUBJECT = 'ePRO Entry Available';

    public static final String TASK_CLOSED = 'Closed'; 
    public static final String TASK_STATUS_COMPLETED = 'Completed';

    public static final String RECORD_TYPE_ID_SC_TASK_PAYMENT = VT_D1_HelperClass.getRTId('VTD1_SC_Task__c','Payment');
    public static final String RECORD_TYPE_ID_SC_TASK_CANCELLATION = VT_D1_HelperClass.getRTId('VTD1_SC_Task__c','Cancellation');
    public static final String RECORD_TYPE_ID_SC_TASK_CONSENT = VT_D1_HelperClass.getRTId('VTD1_SC_Task__c','VTD1_Consent');
    public static final String RECORD_TYPE_ID_SC_TASK_DAILY_REPORT_UPLOAD = VT_D1_HelperClass.getRTId('VTD1_SC_Task__c','VTD1_Daily_Report_Upload');
    public static final String RECORD_TYPE_ID_SC_TASK_DELIVERY = VT_D1_HelperClass.getRTId('VTD1_SC_Task__c','Delivery');
    public static final String RECORD_TYPE_ID_SC_TASK_NOTIFICATION = VT_D1_HelperClass.getRTId('VTD1_SC_Task__c','Notification');
    //public static final String RECORD_TYPE_ID_SC_TASK_QUEUE = VT_D1_HelperClass.getRTId('VTD1_SC_Task__c','Payment');
    public static final String RECORD_TYPE_ID_SC_TASK_RESCHEDULE = VT_D1_HelperClass.getRTId('VTD1_SC_Task__c','Reschedule');
    public static final String RECORD_TYPE_ID_SC_TASK_SCHEDULE_HCP_VISIT = VT_D1_HelperClass.getRTId('VTD1_SC_Task__c','VTD1_HCP_Visit');
    public static final String RECORD_TYPE_ID_SC_TASK_INDEPENDENT_RATER = VT_D1_HelperClass.getRTId('VTD1_SC_Task__c','VTD1_Independent_Rater_schedule_visit_task');

    public static final String RECORD_TYPE_ID_TASK_PCF_TASK = VT_D1_HelperClass.getRTId('Task','VTD1_PCF_Task');
    public static final String RECORD_TYPE_ID_TASK_RESCHEDULE = VT_D1_HelperClass.getRTId('Task','Reschedule');
    public static final String RECORD_TYPE_ID_TASK_FOR_COUNTERSIGN_PACKET = VT_D1_HelperClass.getRTId('Task','TaskForCountersignPack');
    public static final String RECORD_TYPE_ID_TASK_FOR_PAYMENT = VT_D1_HelperClass.getRTId('Task','TaskForPayment');
    public static final String RECORD_TYPE_ID_TASK_FOR_SCHEDULING_VISIT = VT_D1_HelperClass.getRTId('Task','VTD1_TaskForSchedulingVisits');
    public static final String RECORD_TYPE_ID_TASK_FOR_VISIT = VT_D1_HelperClass.getRTId('Task','TaskForVisit');
    public static final String RECORD_TYPE_ID_TASK_FOR_VISIT_AD_HOC = VT_D1_HelperClass.getRTId('Task','VTD2_TaskForVisitAdHoc');
    public static final String RECORD_TYPE_ID_TASK_FOR_REPORT_UPLOAD = VT_D1_HelperClass.getRTId('Task','Task_for_Report_Upload');
    public static final String RECORD_TYPE_ID_TASK_EAF_TASK = VT_D1_HelperClass.getRTId('Task','EAF_Task');
    public static final String RECORD_TYPE_ID_TASK_SIMPLETASK = VT_D1_HelperClass.getRTId('Task','SimpleTask');
    public static final String RECORD_TYPE_ID_TASK_RE = VT_D1_HelperClass.getRTId('Task','VTR2_RE_Task');

    public static final String RECORD_TYPE_NAME_TNCATALOG_CRATASK = 'CRATask'; // SH-17444 Multiple CRA
}