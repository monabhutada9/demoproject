/**
 * Created by shume on 6/29/2020. not longer needed - switched to batch for conversion instead of platform events
 */

public with sharing class VT_R4_CreateUserHandler {
    /* public static void processEvents(List <VTR4_New_Transaction__e> events) {
        System.debug('VT_R4_CreateUserHandler ' + events.size());



        VT_R4_PatientConversion.patientConversionInProgress = true;



        List <VT_R4_PatientConversionHelper.ContactCandidateWrapper> cpWraps = new List<VT_R4_PatientConversionHelper.ContactCandidateWrapper>();
        List <User> users = new List<User>();
        for (VTR4_New_Transaction__e event : events) {
            VT_R4_PatientConversionHelper.ContactCandidateWrapper wrapper = (VT_R4_PatientConversionHelper.ContactCandidateWrapper)JSON.deserialize(event.VTR4_Parameters__c, VT_R4_PatientConversionHelper.ContactCandidateWrapper.class);
            System.debug('wrapper = ' + wrapper);
            users.add(wrapper.user);
            VT_R4_PatientConversionHelper.contactsByIdMap.put(wrapper.contact.Id, wrapper.contact);
            if (wrapper.cgUser != null) {
                users.add(wrapper.cgUser);
                VT_R4_PatientConversionHelper.contactsByIdMap.put(wrapper.cgContact.Id, wrapper.cgContact);
            }
            cpWraps.add(wrapper);
        }


        try {
            new VT_R4_PatientConversionHelper().persistUsers(users, cpWraps, false, true);
        } catch (Exception e) {
            for (VT_R4_PatientConversionHelper.ContactCandidateWrapper wrapper : cpWraps) {
                VT_R4_PatientConversionHelper.logError(null, wrapper.candidate, 'VTR4_StatusCreate__c', e);
            }
            VT_R4_PatientConversion.saveErrors();
        }


    }
    public Object call(String action, Map<String, Object> args) {
        processEvents((List <VTR4_New_Transaction__e>)args.get('events'));
        return null;
    }
*/
}