/**
 * @author:         Rishat Shamratov
 * @date:           22.07.2020
 * @description:    Test class for VT_R5_PIandSCRusersActivEmailSentBatch
 * 
 *                  The creation of users via DomainObjects.User_t
 *                  fills fields 'Country' and 'VTR5_ActivationEmailSent__c'
 */

@IsTest
public with sharing class VT_R5_PIandSCRusActivEmailSentBatchTest {

    public static void testBatch() {
        VT_Stubber.applyStub(
                'VT_R5_PIandSCRusersActivEmailSentBatch.getQueryLocator',
                new List<User>{ new User(Id = UserInfo.getUserId(), Country = '', VTR5_ActivationEmailSent__c = false)}
        );
        Test.startTest();
        Database.executeBatch(new VT_R5_PIandSCRusersActivEmailSentBatch());
        Test.stopTest();

    }
}