/**
 * Created by Alexey Mezentsev on 11/23/2020.
 */

@IsTest
private class VT_R5_DocumentsCategorizeProcessTest {
    @IsTest
    static void documentsCategorizeProcessHandlingTest() {
        List<VTD1_Document__c> docList = new List<VTD1_Document__c>();
        VTD1_Document__c doc1 = new VTD1_Document__c(
                RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT,
                VTR3_Category__c = VT_R4_ConstantsHelper_Documents.DOCUMENT_CATEGORY_UNCATEGORIZED,
                VTR3_CreatedProfile__c = true,
                VTR3_Upload_File_Completed__c = true
        );
        docList.add(doc1);
        VTD1_Document__c doc2 = new VTD1_Document__c(
                RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON,
                VTD1_Files_have_not_been_edited__c = true
        );
        docList.add(doc2);
        VTD1_Document__c doc3 = new VTD1_Document__c(
                VTR3_CreatedProfile__c = false,
                VTR3_Category__c = 'Lab Requisition Form'
        );
        docList.add(doc3);
        insert docList;
        Map<Id, VTD1_Document__c> docMap = new Map<Id, VTD1_Document__c>([
                SELECT VTD1_Status__c
                FROM VTD1_Document__c
                WHERE Id IN :docList
        ]);
        System.assertEquals('Pending Certification', docMap.get(doc1.Id).VTD1_Status__c);
        System.assertEquals('Certified', docMap.get(doc2.Id).VTD1_Status__c);
        System.assertEquals('Pending Certification', docMap.get(doc3.Id).VTD1_Status__c);
        Datetime fakeTime = System.now();
        List<VT_R5_GenericScheduledActionsService.ScheduledActionData> scheduledActions = new List<VT_R5_GenericScheduledActionsService.ScheduledActionData>();
        scheduledActions.add(new VT_R5_GenericScheduledActionsService.ScheduledActionData(
                fakeTime, doc1.Id, 'Document_FollowUpTask')
        );
        new VT_R5_GenericScheduledActionsService().herokuExecute(JSON.serialize(scheduledActions));
        doc1.VTR3_Category_add__c = 'Source Document';
        update docList;
        docMap = new Map<Id, VTD1_Document__c>([
                SELECT VTR3_Category__c
                FROM VTD1_Document__c
                WHERE Id IN :docList
        ]);
        System.assertEquals('Source Document', docMap.get(doc1.Id).VTR3_Category__c);
    }
}