/**
 * Created by User on 19/05/20.
 */
@isTest
public with sharing class VT_R2_RequestBuilder_AdHocReplacemTest {

    public static void buildRequestBodyTest(){
        Case cas = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];

        VTD1_Order__c order = (VTD1_Order__c) new DomainObjects.VTD1_Order_t()
                .setVTD1_Status('Shipped')
                .setVTD1_Case(cas.Id)
                .setVTD1_Expected_Shipment_Date(Date.today())
                .setVTD1_Delivery_Order(1)
                .setVTD1_ShipmentId('shipmentId')
                .persist();

        VTD1_Patient_Kit__c kit = new VTD1_Patient_Kit__c();
        kit.VTD1_Patient_Delivery__c = order.Id;
        kit.VTD1_Case__c = cas.Id;
        insert kit;

        Test.startTest();
        new VT_R2_RequestBuilder_AdHocReplacement(order.Id).buildRequestBody();
        Test.stopTest();
    }
}