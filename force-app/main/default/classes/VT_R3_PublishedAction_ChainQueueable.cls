/**
* @author: Carl Judge
* @date: 02-Sep-19
* @description:
**/

public without sharing class VT_R3_PublishedAction_ChainQueueable extends VT_R3_AbstractPublishedAction {
    private List<VT_R5_TypeSerializable.SerializedItem> separatedSerializedQueueables;

    public VT_R3_PublishedAction_ChainQueueable(VT_R3_AbstractChainableQueueable chainQueueable) {
        this.separatedSerializedQueueables = chainQueueable.getSeparatedSerializedItems();
    }

    public override Type getType() {
        return VT_R3_PublishedAction_ChainQueueable.class;
    }

    public override void execute() {
        VT_R3_AbstractChainableQueueable chainQueueable = VT_R3_AbstractChainableQueueable.reassembleSeparatedChain(separatedSerializedQueueables);
        if (chainQueueable != null) {
            chainQueueable.enqueue();
        }
    }
}