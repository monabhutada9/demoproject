public with sharing class VT_R4_ConstantsHelper_Protocol_ePRO {
    public static final VTR5_DiarySettings__mdt DIARY_SETTINGS = Database.query(
        'SELECT '
            + String.join(VT_D1_HelperClass.getAllFields(VTR5_DiarySettings__mdt.getSObjectType()), ',')
            + ' FROM VTR5_DiarySettings__mdt WHERE Label = \'default\''
    );

    public static final String PROTOCOL_EPRO_TRIGGER_EVENT = 'Event';
    public static final String PROTOCOL_EPRO_TRIGGER_PERIODIC = 'Periodic';
    public static final String PROTOCOL_EPRO_TRIGGER_EVENT_TASK_SUBJECT = 'eDiary Response Completion';
    public static final String PROTOCOL_EPRO_TRIGGER_PERIODIC_TASK_SUBJECT = 'Recurring eDiary Response Completion';
    public static final String PROTOCOL_EPRO_PERIOD_DAILY = 'Daily';
    public static final String PROTOCOL_EPRO_PERIOD_WEEKLY = 'Weekly';
    public static final String PROTOCOL_EPRO_PERIOD_MONTHLY = 'Monthly';
    public static final String PROTOCOL_EPRO_PERIOD_X_DAYS = 'Every X Days';
    public static final String PROTOCOL_EPRO_PERIOD_SPECIFIC_DAYS = 'Specific Days Repeated';

    public static final String SURVEY_NOT_STARTED = 'Not Started';
    public static final String SURVEY_READY_TO_START = 'Ready to Start';
    public static final String SURVEY_IN_PROGRESS = 'In Progress';
    public static final String SURVEY_REVIEW_REQUIRED = 'Review Required';
    public static final String SURVEY_REVIEWED = 'Reviewed';
    public static final String SURVEY_EDIARY_MISSED = 'eDiary Missed';
    public static final String SURVEY_DUE_SOON = 'Due Soon';
    public static final String SURVEY_REVIEWED_PARTIALLY_COMPLETED = 'Reviewed (Partially Completed)';
    public static final String SURVEY_COMPLETED = 'Completed';
    public static final String SURVEY_MISSED = 'Missed';
    public static final String PROTOCOL_DELIVERY_NOT_STARTED = 'Not Started';

    public static final String PSC_EPRO_DEFAULT_QUESTION = 'Did the PSC visit occur and successfully complete?';

}