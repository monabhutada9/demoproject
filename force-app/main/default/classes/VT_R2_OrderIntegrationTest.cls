/**
 * @author Ruslan Mullayanov
 * @description Tests for covering VT_D1_OrderTrigger, VT_D1_OrderProcessHandler, VT_D1_OrderIntegration, VT_D1_RequestBuilder_Order, VT_R2_RequestBuilder_AdHocReplacement
 */
@IsTest
private class VT_R2_OrderIntegrationTest {
	private static Case cas;
	private static User u;

	@TestSetup
	private static void setupMethod() {
		HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
		VT_D1_TestUtils.createProtocolDelivery(study.Id);
		Test.startTest();

		//VT_D1_TestUtils.createTestPatientCandidate(1, false);
		VT_D1_TestUtils.createTestPatientCandidate(1);

		Test.stopTest();
		VT_R4_GenerateActualVisitsAndKits kitsGenerator = new VT_R4_GenerateActualVisitsAndKits();
		List<Case> casesList = [SELECT VTD1_Patient_User__c FROM Case];
		kitsGenerator.executeDeliveriesAndKits(new List<Id>{casesList[0].Id});
	}

	static {
		List<Case> casesList = [SELECT VTD1_Patient_User__c FROM Case];
		if(casesList.size() > 0){
			cas = casesList.get(0);
			String userId = cas.VTD1_Patient_User__c;
			u = [SELECT Id, ContactId FROM User WHERE Id =: userId];
		}
	}

	@IsTest
	static void sendPatientDeliveryToCSMTest() {
		List<VTD1_Order__c> orders = [
				SELECT Id
				FROM VTD1_Order__c
				WHERE VTD1_Case__c = :cas.Id
		];

		System.debug('orders: ' + orders);
		VTD1_Order__c order0 = orders.get(0);

		VT_R2_HttpCalloutMockImpl httpMock = new VT_R2_HttpCalloutMockImpl();
		Test.setMock(HttpCalloutMock.class, httpMock);
		httpMock.response = new HttpResponse();
		httpMock.response.setStatus('OK');
		httpMock.response.setStatusCode(200);

		List<VTD1_Order__c> ordersToUpdate = new List<VTD1_Order__c>();
		order0.VTD1_ExpectedDeliveryDateTime__c = Datetime.now();
		order0.VTD1_Status__c = VT_R4_ConstantsHelper_KitsOrders.ORDER_STATUS_RECEIVED;
		ordersToUpdate.add(order0);
		Test.startTest();
		update ordersToUpdate;
		Test.stopTest();

		String requestBody = new VT_D1_RequestBuilder_Order(order0.Id, '2.0').buildRequestBody();
		VT_D1_RequestBuilder_Order.Delivery orderDelivery = (VT_D1_RequestBuilder_Order.Delivery) JSON.deserialize(requestBody, VT_D1_RequestBuilder_Order.Delivery.class);

		System.assertNotEquals(null, orderDelivery);
	}

	@IsTest
	static void sendAdHocReplacementToCSMTest() {
		List<VTD1_Order__c> orders = [
				SELECT Id, VTD1_ShipmentId__c, VTD1_IntegrationId__c,
						VTD1_Case__r.VTD1_Subject_ID__c,
						VTD1_Case__r.VTD1_Study__r.Name,
						VTD1_Status__c
				FROM VTD1_Order__c
				WHERE VTD1_Case__c = :cas.Id
		];

		System.debug('orders: ' + orders);
        VTD1_Order__c order0 = orders.get(0);

		VT_R2_RequestBuilder_AdHocReplacement.Delivery responseObjMock = new VT_R2_RequestBuilder_AdHocReplacement.Delivery();
		responseObjMock.protocolId = order0.VTD1_Case__r.VTD1_Study__r.Name;
		responseObjMock.subjectId = order0.VTD1_Case__r.VTD1_Subject_ID__c;
		responseObjMock.shipmentId = order0.VTD1_ShipmentId__c;
		responseObjMock.auditLog = new VT_R2_RequestBuilder_AdHocReplacement.AuditLog();

		List<RecordType> kitRecordTypes = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType='VTD1_Patient_Kit__c'];
		List<VTD1_Patient_Kit__c> patientKitsToInsert = new List<VTD1_Patient_Kit__c>();
		for (Integer i=0; i<kitRecordTypes.size(); i++) {
			VTD1_Patient_Kit__c patientKit = new VTD1_Patient_Kit__c(
					VTD2_MaterialId__c = 'ID'+i,
					VTR2_Quantity__c = i+1,
					VTD1_Patient_Delivery__c = order0.Id,
					VTD1_Case__c = cas.Id,
					RecordTypeId = kitRecordTypes[i].Id
			);
			patientKitsToInsert.add(patientKit);
		}
		insert patientKitsToInsert;

		List<VTD1_Patient_Kit__c> patientKitsToProcess = [
				SELECT VTD1_Kit_Name__c,
						VTD2_MaterialId__c,
						VTR2_Quantity__c,
						VTD1_Kit_Type__c,
						VTD1_Delivery_Status__c
				FROM VTD1_Patient_Kit__c
				WHERE VTD1_Patient_Delivery__c =: order0.Id
				AND Id IN :patientKitsToInsert
		];

		if (!patientKitsToProcess.isEmpty()) {
			for (VTD1_Patient_Kit__c patientKit : patientKitsToProcess) {
				VT_R2_RequestBuilder_AdHocReplacement.Kit kit = new VT_R2_RequestBuilder_AdHocReplacement.Kit();
				kit.materialId = patientKit.VTD2_MaterialId__c;
				kit.replacementMaterialId = patientKit.VTD2_MaterialId__c+'RE';
				kit.quantity = (Integer)patientKit.VTR2_Quantity__c;
				kit.kitType = patientKit.VTD1_Kit_Type__c;
				responseObjMock.kits.add(kit);
			}
		}

		VT_R2_HttpCalloutMockImpl httpMock = new VT_R2_HttpCalloutMockImpl();
		Test.setMock(HttpCalloutMock.class, httpMock);
		httpMock.response = new HttpResponse();
		httpMock.response.setBody(System.JSON.serialize(responseObjMock));
		httpMock.response.setStatus('OK');
		httpMock.response.setStatusCode(200);
 
        List<VTD1_Order__c> ordersToUpdate = new List<VTD1_Order__c>();
        order0.VTD1_Status__c = VT_R4_ConstantsHelper_KitsOrders.ORDER_STATUS_REPLACEMENT_ORDERED;
        ordersToUpdate.add(order0);
		Test.startTest();
        update ordersToUpdate;
		//VT_D1_OrderIntegration.sendAdHocReplacementToCSM(order0.Id);
		Test.stopTest();

		List<VTD1_Patient_Kit__c> patientKitsReplaced = [
				SELECT VTD2_MaterialId__c, VTR2_ReplacementMaterialId__c
				FROM VTD1_Patient_Kit__c
				WHERE VTD1_Patient_Delivery__c =: order0.Id
				AND Id IN :patientKitsToProcess AND VTR2_ReplacementMaterialId__c!=NULL
		];
//		System.assertEquals(patientKitsToProcess.size(), patientKitsReplaced.size());
	}
}