public without sharing class VT_D1_StudySharingConfigurationHandler {
    public static Boolean isConversion = false;

    public void onBeforeInsert(List<VTD1_Study_Sharing_Configuration__c> recs) {
        if (!isConversion) {
            lockConfigs(recs);
        }
    }

    public void onBeforeUpdate(List<VTD1_Study_Sharing_Configuration__c> recs, Map<Id, VTD1_Study_Sharing_Configuration__c> oldMap) {
        List<VTD1_Study_Sharing_Configuration__c> accessChanged = getChangedAccess(recs, oldMap);
        if (!accessChanged.isEmpty()) {
            lockConfigs(accessChanged);
        }
    }

    public void onAfterInsert(List<VTD1_Study_Sharing_Configuration__c> recs) {
        calcSharing(recs);
        if (!isConversion) {
            VT_R5_ShareGroupMembershipService.processStudyConfigs(recs, null);
        }
    }

    public void onAfterUpdate(List<VTD1_Study_Sharing_Configuration__c> recs, Map<Id, VTD1_Study_Sharing_Configuration__c> oldMap) {
        List<VTD1_Study_Sharing_Configuration__c> accessChanged = getChangedAccess(recs, oldMap);
        if (!accessChanged.isEmpty()) {
            calcSharing(accessChanged);
            VT_R5_ShareGroupMembershipService.processStudyConfigs(accessChanged, oldMap);
        }
    }

    // This only locks the records in the lightning component on study
    private void lockConfigs(List<VTD1_Study_Sharing_Configuration__c> configs) {
        for (VTD1_Study_Sharing_Configuration__c config : configs) {
            config.VTD1_Locked__c = true;
        }
    }

    private void calcSharing(List<VTD1_Study_Sharing_Configuration__c> configs) {
        if (Test.isRunningTest() && VT_R3_GlobalSharing.disableForTest) { return; }
        VT_R3_AbstractChainableQueueable sharingChain = VT_R5_SharingChainFactory.createFromStudyConfigs(configs);
        if (sharingChain != null) {
            sharingChain.enqueue();
        }
    }

    private List<VTD1_Study_Sharing_Configuration__c> getChangedAccess(List<VTD1_Study_Sharing_Configuration__c> recs, Map<Id, VTD1_Study_Sharing_Configuration__c> oldMap) {
        List<VTD1_Study_Sharing_Configuration__c> accessChanged = new List<VTD1_Study_Sharing_Configuration__c>();
        for (VTD1_Study_Sharing_Configuration__c item : recs) {
            if (item.VTD1_Access_Level__c != oldMap.get(item.Id).VTD1_Access_Level__c) {
                accessChanged.add(item);
            }
        }
        return accessChanged;
    }
}