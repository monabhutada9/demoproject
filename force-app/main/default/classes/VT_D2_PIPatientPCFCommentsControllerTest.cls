@IsTest
private class VT_D2_PIPatientPCFCommentsControllerTest {
    @testSetup
    static void setup() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.startTest();
        //HealthCloudGA__CandidatePatient__c candidatePatient = VT_D1_TestUtils.createPatientCandidate('Mike', 'Birn', 'mikebirn@test.com', 'mikebirn@test.com', 'mikebirn', 'Converted', study.Id, 'PN', '11-11-11', 'US', 'NY');
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }

    @IsTest
    static void getPcfCommentsTest() {
        User piUser;
        List<Case> casesList = [SELECT VTD1_PI_user__c FROM Case];
        System.assert(casesList.size() > 0);
        if(casesList.size() > 0){
            Case cas = casesList.get(0);
            String userId = cas.VTD1_PI_user__c;
            piUser = [SELECT Id, ContactId FROM User WHERE Id =: userId];
            Test.startTest();
            String pcfId = VT_D1_CommunityChat.createPcf(cas.Id, 'subject', 'description', null, 'Possible');


            VT_D2_PIPatientPCFCommentsController.addPCFComment(pcfId, 'newComment', null);
            List<VTD1_PCF_Patient_Contact_Form_Comment__c> comments = [SELECT Id FROM VTD1_PCF_Patient_Contact_Form_Comment__c WHERE Patient_Contact_Form__c = :pcfId];
            System.assertEquals(1, comments.size());
            VT_D2_PIPatientPCFCommentsController.addPCFComment(pcfId, 'newComment1', comments.get(0).Id);
            VT_D2_PIPatientPCFCommentsController.addPCFComment(pcfId, 'newComment2', comments.get(0).Id);
            VT_D2_PIPatientPCFCommentsController.addPCFComment(pcfId, 'newComment3', null);
            String pcfCommentsString = VT_D2_PIPatientPCFCommentsController.getPcfComments(pcfId);
            List<VT_D2_PIPatientPCFCommentsController.PCFComment> pcfComments = (List<VT_D2_PIPatientPCFCommentsController.PCFComment>) JSON.deserialize(pcfCommentsString, List<VT_D2_PIPatientPCFCommentsController.PCFComment>.class);
            System.assertEquals(2, pcfComments.size());
            Test.stopTest();
        }
    }
}