/**
 * Created by User on 19/05/17.
 */
@isTest
public with sharing class VT_R2_SCPatientEDiaryTest {
    public static void getSurveysTest(){
        VT_R2_SCPatientEDiary.getSurveys();
    }
    public static void getQuestionsAnswersTest(){
        VTD1_Protocol_ePRO__c protocolEPROT = [SELECT Id FROM VTD1_Protocol_ePRO__c LIMIT 1];
        VTD1_Survey__c survey = [SELECT Id FROM VTD1_Survey__c LIMIT 1];
        VTD1_Survey_Answer__c answer = [SELECT Id FROM VTD1_Survey_Answer__c LIMIT 1];
        VTR2_SCRPatientEDiary.AnswerWrapper answ = new VTR2_SCRPatientEDiary.AnswerWrapper();
        answ.answer = 'Test';
        answ.answerId = answer.Id;
        VTR2_SCRPatientEDiary.AnswerWrapper answ2 = new VTR2_SCRPatientEDiary.AnswerWrapper();
        answ2.answer = 'Test2';

        Test.startTest();
        VT_R2_SCPatientEDiary.getQuestionsAnswers(survey.Id,protocolEPROT.Id);
        Test.stopTest();
    }
    public static void saveAnswersTest(){
        VTD1_Survey__c survey = [SELECT Id FROM VTD1_Survey__c LIMIT 1];
        VTD1_Survey_Answer__c answer = [SELECT Id FROM VTD1_Survey_Answer__c LIMIT 1];
        VTR2_SCRPatientEDiary.AnswerWrapper answ = new VTR2_SCRPatientEDiary.AnswerWrapper();
        answ.answer = 'Test';
        answ.answerId = answer.Id;
        VTR2_SCRPatientEDiary.AnswerWrapper answ2 = new VTR2_SCRPatientEDiary.AnswerWrapper();
        answ2.answer = 'Test2';
        Test.startTest();

        VT_R2_SCPatientEDiary.saveAnswers(JSON.serialize(new List<VTR2_SCRPatientEDiary.AnswerWrapper>{answ,answ2}),survey.Id,true,VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED); //String answers, Id surveyId, Boolean submit, String surveyStatus
        try {
            VT_R2_SCPatientEDiary.saveAnswers('wsws',survey.Id,true,VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED);
        }
        catch (Exception e){

        }
        Test.stopTest();
    }
    public static void updateDateTimeTest(){
        VTD1_Survey__c survey = [SELECT Id FROM VTD1_Survey__c LIMIT 1];
        Test.startTest();
        VT_R2_SCPatientEDiary.updateDateTime(survey.Id,VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED);
        Test.stopTest();
    }
}