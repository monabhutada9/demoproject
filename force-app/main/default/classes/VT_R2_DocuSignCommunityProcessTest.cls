@IsTest
private class VT_R2_DocuSignCommunityProcessTest {

    public with sharing class CalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            if (request.getEndpoint().endsWith('/oauth/token')) {
                HttpResponse res = new HttpResponse();
                res.setHeader('Content-Type', 'application/json');
                res.setBody('{"access_token":"test-token","token_type":"Bearer","expires_in":"3600"}');
                res.setStatusCode(200);
                return res;
            } else if (request.getEndpoint().endsWith('/oauth/userinfo')) {
                HttpResponse res = new HttpResponse();
                res.setHeader('Content-Type', 'application/json');
                res.setBody('{"accounts":[{"account_id":"test-id","account_name":"test-account-id","base_uri":"http://test.com"}]}');
                res.setStatusCode(200);
                return res;
            } else if (request.getEndpoint().endsWith('/envelopes')) {
                HttpResponse res = new HttpResponse();
                res.setHeader('Content-Type', 'application/json');
                res.setBody('{"envelopeId":"envelope-id","uri":"url/envelope-id","status":"create"}');
                res.setStatusCode(201);
                return res;
            } else if (request.getEndpoint().endsWith('/views/edit')) {
                HttpResponse res = new HttpResponse();
                res.setHeader('Content-Type', 'application/json');
                res.setBody('{"url":"iframe-url"}');
                res.setStatusCode(201);
                return res;
            } else if (request.getEndpoint().endsWith('/chunked_uploads')) {
                HttpResponse res = new HttpResponse();
                res.setHeader('Content-Type', 'application/json');
                res.setBody('{"chunkedUploadId":"123","data":"data"}');
                res.setStatusCode(201);
                return res;
            } else {
                HttpResponse res = new HttpResponse();
                res.setHeader('Content-Type', 'application/json');
                res.setBody('{}');
                res.setStatusCode(200);
                return res;
            }
        }
    }

    @TestSetup
    static void setupData() {
        VTD2_Docusign_API_Settings__c settings = new VTD2_Docusign_API_Settings__c(
                Name = 'Test Settings',
                VTD2_Auth_Domain__c = 'test-domain',
                VTD2_Account_Name__c = 'test-account-id'
        );
        insert settings;

        VTD1_Document__c document = (VTD1_Document__c) new DomainObjects.VTD1_Document_t()
                .addCase(new DomainObjects.Case_t())
                .addStudy(new DomainObjects.Study_t())
                .persist();
        ContentVersion contentVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();
        ContentVersion insertedContVersion = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        new DomainObjects.ContentDocumentLink_t()
                .withLinkedEntityId(document.Id)
                .withContentDocumentId(insertedContVersion.ContentDocumentId)
                .persist();
        new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t().addAccount(new DomainObjects.Account_t()))
                .setUsername('docusignuser@test.com')
                .setProfile('Site Coordinator')
                .persist();
    }

    @IsTest
    static void getSigningEnvelopeTest_EmptyRecordId() {
        Test.startTest();
        VT_R2_DocuSignCommunityProcessController.SigningEnvelope signingEnvelope = VT_R2_DocuSignCommunityProcessController.getSigningEnvelope(null);
        Test.stopTest();

        System.assertEquals(null, signingEnvelope.signingRecord.recordId);
    }

    @IsTest
    static void getSigningEnvelopeTest_NonEmptyRecordId() {
        VTD1_Document__c document = [SELECT Id FROM VTD1_Document__c LIMIT 1][0];
        ContentVersion contentVersion = [SELECT ContentDocumentId FROM ContentVersion LIMIT 1][0];
        ContentDocumentLink link = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId = :contentVersion.ContentDocumentId LIMIT 1][0];
        Id recordId = link.LinkedEntityId;

        Test.startTest();
        VT_R2_DocuSignCommunityProcessController.SigningEnvelope signingEnvelope = VT_R2_DocuSignCommunityProcessController.getSigningEnvelope(recordId);
        Test.stopTest();

        System.assertEquals(recordId, signingEnvelope.signingRecord.recordId);
    }

    @IsTest
    static void saveEnvelopeTest() {
        String serializedEnvelope = getTestSerializedEnvelope();

        Test.startTest();
        VT_R2_DocuSignCommunityProcessController.SigningEnvelope deserializedEnvelope = (VT_R2_DocuSignCommunityProcessController.SigningEnvelope)
                JSON.deserialize(serializedEnvelope, VT_R2_DocuSignCommunityProcessController.SigningEnvelope.class);
        VT_R2_DocuSignCommunityProcessController.SigningEnvelope signingEnvelope = VT_R2_DocuSignCommunityProcessController.saveEnvelope(serializedEnvelope);
        Test.stopTest();

        System.assertEquals(deserializedEnvelope.message.message, signingEnvelope.message.message, 'Incorrect field matching after serialization/deserialization');
        System.assertEquals(deserializedEnvelope.signingRecord.recordId, signingEnvelope.signingRecord.recordId, 'Incorrect field matching after serialization/deserialization');
        System.assertEquals(deserializedEnvelope.recipients[0].recordId, signingEnvelope.recipients[0].recordId, 'Incorrect field matching after serialization/deserialization');
    }

    @IsTest
    static void submitEnvelopeTest() {
        ContentVersion contentVersion = [SELECT ContentDocumentId FROM ContentVersion LIMIT 1][0];
        String serializedEnvelope = getTestSerializedEnvelope();
        VT_R2_DocuSignCommunityProcessController.SigningEnvelope signingEnvelope = VT_R2_DocuSignCommunityProcessController.saveEnvelope(serializedEnvelope);
        signingEnvelope.signingDocuments[0].contentDocumentId = contentVersion.ContentDocumentId;
        signingEnvelope.signingDocuments[0].contentVersionId = contentVersion.Id;

        Test.setMock(HttpCalloutMock.class, new CalloutMock());
        Test.startTest();
        String iframeUrl = VT_R2_DocuSignCommunityProcessController.submitEnvelope(JSON.serialize(signingEnvelope));
        Integer numOfCallouts = Limits.getCallouts();
        Test.stopTest();

        System.assertEquals('iframe-url', iframeUrl, 'Incorrect iframe url');
        //System.assertEquals(4, numOfCallouts, 'Incorrect number of performed callouts');
    }

    @IsTest
    static void createChunkedUpload() {
        String serializedEnvelope = getTestSerializedEnvelope();

        Test.setMock(HttpCalloutMock.class, new CalloutMock());
        Test.startTest();
        VT_R2_DocuSignCommunityProcessController.ChunkedDocument chunkedDocument =
                VT_R2_DocuSignCommunityProcessController.createChunkedUpload('test-data-chunk');
        String chunkedDocumentJson = JSON.serialize(chunkedDocument);
        VT_R2_DocuSignCommunityProcessController.ChunkedDocument addedDocument =
                VT_R2_DocuSignCommunityProcessController.addDocumentChunk(chunkedDocumentJson, 'test-data-chunk');
        try {
            VT_R2_DocuSignCommunityProcessController.commitAndSubmitChunkedDocument(serializedEnvelope, chunkedDocumentJson);
        } catch (AuraHandledException ex) {}
        Test.stopTest();

        System.assert(chunkedDocument != null);
        System.assert(addedDocument != null);
    }

    @IsTest
    static void findTest() {
        User us = [SELECT Id, Name, Email FROM User WHERE Profile.Name = 'Site Coordinator' AND Username = 'docusignuser@test.com' AND IsActive = TRUE LIMIT 1];
        Test.setFixedSearchResults(new Id[]{
                us.Id
        });
        Test.startTest();
        List<VT_R2_DocuSignCommunityProcessController.LookupSearchResult> lookupSearchResults =
                (List<VT_R2_DocuSignCommunityProcessController.LookupSearchResult>) VT_R2_DocuSignCommunityProcessController.find(us.Name);
        Test.stopTest();

        System.assertEquals(1, lookupSearchResults.size(), 'Incorrect returned list size');
        System.assertEquals(us.Id, lookupSearchResults[0].recordId, 'Incorrect record Id');
        System.assertEquals(us.Name, lookupSearchResults[0].name, 'Incorrect record name');
        System.assertEquals(us.Email, lookupSearchResults[0].email, 'Incorrect record email');
    }

    @IsTest
    static void getCurrentUserTest() {
        User us = [SELECT Id, Name, Email FROM User WHERE Profile.Name = 'Site Coordinator' AND Username = 'docusignuser@test.com' AND IsActive = TRUE LIMIT 1];
        System.runAs(us) {
            Test.startTest();
            VT_R2_DocuSignCommunityProcessController.LookupSearchResult lookupSearchResult = VT_R2_DocuSignCommunityProcessController.getCurrentUser();
            Test.stopTest();

            System.assertEquals(us.Id, lookupSearchResult.recordId, 'Incorrect record Id');
            System.assertEquals(us.Name, lookupSearchResult.name, 'Incorrect record name');
            System.assertEquals(us.Email, lookupSearchResult.email, 'Incorrect record email');
        }
    }

    @IsTest
    static void getOptionsTest() {
        Test.startTest();
        VT_R2_DocuSignCommunityProcessController.DocuSignOptions docuSignOptions = VT_R2_DocuSignCommunityProcessController.getOptions();
        System.debug(docuSignOptions);
        Test.stopTest();

        System.assertEquals(4, docuSignOptions.recipientRoleOptions.size());
    }

    private static String getTestSerializedEnvelope() {
        User us = [SELECT Id FROM User WHERE Username = 'docusignuser@test.com' AND IsActive = TRUE LIMIT 1][0];
        VTD1_Document__c document = [SELECT Id FROM VTD1_Document__c LIMIT 1][0];
        ContentVersion contentVersion = [SELECT ContentDocumentId FROM ContentVersion LIMIT 1][0];
        return '{"message":{"message":"test","subject":"Signature"},"recipients":[{"email":"test@gmail.com","name":"Test Test","recordId":"'
                + us.Id + '","recipientType":"Signer","type":"user","recipientRole":"Customer 1","order":1}],"signingDocuments":[{"contentDocumentId":"'
                + contentVersion.ContentDocumentId + '","contentVersionId":"' + contentVersion.Id + '","size":140560,"title":"test.pdf","order":1}],"signingRecord":{"obj":{"Id":"'
                + document.Id + '","Name":"TS-257","attributes":{"type":"VTD1_Document__c"}},"recordId":"' + document.Id + '","type":"VTD1_Document__c"}}';
    }
}