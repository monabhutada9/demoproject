public with sharing class VT_D1_OrderProcessHandler {
    
    public static void sendPatientDeliveryToCSM(Map<Id, VTD1_Order__c> oldOrderMap, Map<Id, VTD1_Order__c> newOrderMap) {
        for (VTD1_Order__c newOrder : newOrderMap.values()) {
            VTD1_Order__c oldOrder = oldOrderMap.get(newOrder.Id);
            //if status changed to Received and isIMP or isLab then send to CSM:
            if (oldOrder.VTD1_Status__c != newOrder.VTD1_Status__c) {
                if (newOrder.VTD1_Status__c == VT_R4_ConstantsHelper_KitsOrders.ORDER_STATUS_RECEIVED
                        //&& (newOrder.VTD1_isIMP__c || newOrder.VTD1_isLab__c)
                        ) {
                    String apiVersion = [SELECT VTD1_Study__r.VTR2_API_version__c FROM Case WHERE Id =: newOrder.VTD1_Case__c LIMIT 1].VTD1_Study__r.VTR2_API_version__c;
                    VT_D1_OrderIntegration.sendPatientDeliveryToCSMFuture(newOrder.Id, apiVersion);
//                } else if (newOrder.VTD1_Status__c == VT_D1_ConstantsHelper.ORDER_STATUS_REPLACEMENT_ORDERED
//                        ) {
//                    VT_D1_OrderIntegration.sendAdHocReplacementToCSMFuture(newOrder.Id);
                }
            } else if (oldOrder.VTR2_IsDeliveryCompleted__c != newOrder.VTR2_IsDeliveryCompleted__c
                       && newOrder.VTR2_IsDeliveryCompleted__c == true) {
                VT_D1_OrderIntegration.sendAdHocReplacementToCSMFuture(newOrder.Id);
            }
        }
    }
    

    public static void processOrdersByCaseStatus(List<Case> newList, Map<Id, Case> oldMap) {
        Set<Id> caseIdSet = new Set<Id>();
        for (Case cas : newList) {
            if (cas.VTD1_Study__c != null && cas.Status != oldMap.get(cas.Id).Status) {
                caseIdSet.add(cas.Id);
            }
        }
        if (!caseIdSet.isEmpty()) {
            String impLabType = 'IMP/Lab';
            String activeRandomizedStatus = 'Active/Randomized';
            String washoutRunInStatus = 'Washout/Run-In';
            String consentedStatus = 'Consented';
            Set<String> caseStatusesForDevises = new Set<String>{
                    activeRandomizedStatus,
                    washoutRunInStatus
            };
            Set<String> kitTypesDevices = new Set<String>{
                    'Connected Devices',
                    'Study Hub Tablet'
            };
            Map<String, String> whenRecieveKitMap = new Map<String, String>{
                    consentedStatus => 'When the patient is determined eligible',
                    washoutRunInStatus => 'Prior to day 0'
            };
            List<VTD1_Order__c> ordersToUpdate = new List<VTD1_Order__c>();
            for (VTD1_Order__c ptDel : [
                    SELECT Id,
                            VTD1_Kit_Type__c,
                            VTD1_IMP_Baseline_Status__c,
                            VTD1_Lab_Baseline_Status__c,
                            Delivery_Offset__c,
                            VTD1_Case__c,
                            VTD1_Case__r.Status,
                            VTD1_Case__r.VTD1_Study__r.When_Does_Patient_Receive_Kit__c, (SELECT VTD1_Kit_Type__c, VTD1_Required_for_Screening__c FROM Patient_Kits__r LIMIT 1)
                    FROM VTD1_Order__c
                    WHERE VTD1_Case__c IN :caseIdSet
                    AND (
                            (VTD1_Kit_Type__c = :impLabType
                            AND (VTD1_IMP_Baseline_Status__c != null OR VTD1_Lab_Baseline_Status__c != null))
                            OR VTD1_Kit_Type__c IN :kitTypesDevices
                    )
            ]) {
                if (ptDel.VTD1_Kit_Type__c == impLabType
                        && (ptDel.VTD1_IMP_Baseline_Status__c == ptDel.VTD1_Case__r.Status
                            || ptDel.VTD1_Lab_Baseline_Status__c == ptDel.VTD1_Case__r.Status)
                        && (ptDel.VTD1_Case__r.Status != consentedStatus
                            || (ptDel.VTD1_Case__r.Status == consentedStatus
                                && ptDel.VTD1_Case__r.VTD1_Study__r.When_Does_Patient_Receive_Kit__c == whenRecieveKitMap.get(ptDel.VTD1_Case__r.Status))
                            )
                        ) {
                    ptDel.VTD1_Expected_Shipment_Date__c = Date.today() + (Integer) ptDel.Delivery_Offset__c;
                    ptDel.VTD1_Shipping_Details_Form_Trigger__c = true;
                    ordersToUpdate.add(ptDel);
                } else if (caseStatusesForDevises.contains(ptDel.VTD1_Case__r.Status)
                        && kitTypesDevices.contains(ptDel.VTD1_Kit_Type__c)
                        && !ptDel.Patient_Kits__r.isEmpty()
                        && (
                            (ptDel.VTD1_Case__r.Status == activeRandomizedStatus
                                && !ptDel.Patient_Kits__r[0].VTD1_Required_for_Screening__c)
                            || (ptDel.VTD1_Case__r.Status == washoutRunInStatus
                                && ptDel.VTD1_Case__r.VTD1_Study__r.When_Does_Patient_Receive_Kit__c == whenRecieveKitMap.get(ptDel.VTD1_Case__r.Status)
                                && ptDel.Patient_Kits__r[0].VTD1_Required_for_Screening__c)
                            )
                        ) {
                    ptDel.VTD1_Status__c = 'Pending Shipment';
                    ordersToUpdate.add(ptDel);
                }
            }
            if (!ordersToUpdate.isEmpty()) {
                update ordersToUpdate;
            }
        }
    }
}