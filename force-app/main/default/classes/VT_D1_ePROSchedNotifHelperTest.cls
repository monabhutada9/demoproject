/**
 * Created by user on 5/14/2019.
 * Test class for VT_D1_ePROSchedulingNotificationHelper
 */
@IsTest
private class VT_D1_ePROSchedNotifHelperTest {
    @testSetup
    private static void setupMethod() {
        //HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        HealthCloudGA__CarePlanTemplate__c study = (HealthCloudGA__CarePlanTemplate__c) new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'))
                .persist();
        study.VTD1_End_Date__c = Date.Today().addMonths(3);
        update study;


        VTD1_ProtocolVisit__c pVisit = new VTD1_ProtocolVisit__c(
            VTD1_Study__c = study.Id,
            VTD1_VisitDuration__c = '1 hour',
            VTR2_Visit_Participants__c = VTD1_ProtocolVisit__c.VTR2_Visit_Participants__c.getDescribe().getPicklistValues()[0].getValue()
        );
        insert pVisit;

        Id satisfactionSurveyRecTypeId = Schema.SObjectType.VTD1_Protocol_ePRO__c.getRecordTypeInfosByDeveloperName().get('SatisfactionSurvey').getRecordTypeId();
        Id eProRecTypeId = Schema.SObjectType.VTD1_Protocol_ePRO__c.getRecordTypeInfosByDeveloperName().get('ePRO').getRecordTypeId();

        List<VTD1_Protocol_ePRO__c> pEpros = new List<VTD1_Protocol_ePRO__c>();

        // satisfaction ePRO
        pEpros.add(new VTD1_Protocol_ePRO__c(
            VTD1_Study__c = study.Id,
            RecordTypeId = satisfactionSurveyRecTypeId,
            VDD1_Scoring_Window__c = 10,
            VTD1_Type__c = 'PSC',
            VTR2_Protocol_Reviewer__c = 'Patient Guide',
            VTD1_Subject__c = 'Patient',
            VTD1_Response_Window__c = 10,
            VTR4_EventType__c = 'Visit Completion'
        ));

        // event epro
        pEpros.add(new VTD1_Protocol_ePRO__c(
            VTD1_Study__c = study.Id,
            RecordTypeId = eProRecTypeId,
            VTD1_Event__c = pVisit.Id,
            VTD1_Trigger__c = 'Event',
            VTD1_Safety_Keywords__c = 'Test',
            VDD1_Scoring_Window__c = 10,
            VTR2_Protocol_Reviewer__c = 'Patient Guide',
            VTD1_Subject__c = 'Patient',
            VTD1_Response_Window__c = 10,
            VTR4_EventType__c = 'Visit Completion'
        ));

        // daily epro
        pEpros.add(new VTD1_Protocol_ePRO__c(
            VTD1_Study__c = study.Id,
            RecordTypeId = eProRecTypeId,
            VTD1_First_Available__c = pVisit.Id,
            VTD1_Period__c = 'Daily',
            VTD1_Trigger__c = 'Periodic',
            VDD1_Scoring_Window__c = 10,
            VTR2_Protocol_Reviewer__c = 'Patient Guide',
            VTD1_Subject__c = 'Patient',
            VTD1_Response_Window__c = 10
        ));

        // weekly epro
        pEpros.add(new VTD1_Protocol_ePRO__c(
            VTD1_Study__c = study.Id,
            RecordTypeId = eProRecTypeId,
            VTD1_First_Available__c = pVisit.Id,
            VTD1_Period__c = 'Weekly',
            VTD1_Trigger__c = 'Periodic',
            VDD1_Scoring_Window__c = 10,
            VTR2_Protocol_Reviewer__c = 'Patient Guide',
            VTD1_Subject__c = 'Patient',
            VTD1_Response_Window__c = 10
        ));

        // monthly epro
        pEpros.add(new VTD1_Protocol_ePRO__c(
            VTD1_Study__c = study.Id,
            RecordTypeId = eProRecTypeId,
            VTD1_First_Available__c = pVisit.Id,
            VTD1_Period__c = 'Monthly',
            VTD1_Trigger__c = 'Periodic',
            VDD1_Scoring_Window__c = 10,
            VTR2_Protocol_Reviewer__c = 'Patient Guide',
            VTD1_Subject__c = 'Patient',
            VTD1_Response_Window__c = 10
        ));

        // every x days epro
        pEpros.add(new VTD1_Protocol_ePRO__c(
            VTD1_Study__c = study.Id,
            RecordTypeId = eProRecTypeId,
            VTD1_First_Available__c = pVisit.Id,
            VTD1_Period__c = 'Every X Days',
            VTD1_Day_Frequency__c = 3,
            VTD1_Trigger__c = 'Periodic',
            VDD1_Scoring_Window__c = 10,
            VTR2_Protocol_Reviewer__c = 'Patient Guide',
            VTD1_Subject__c = 'Patient',
            VTD1_Response_Window__c = 10
        ));

        // specific days repeated epro
        pEpros.add(new VTD1_Protocol_ePRO__c(
            VTD1_Study__c = study.Id,
            RecordTypeId = eProRecTypeId,
            VTD1_First_Available__c = pVisit.Id,
            VTD1_Period__c = 'Specific Days Repeated',
            Days__c = 'Monday;Tuesday',
            VTD1_Trigger__c = 'Periodic',
            VDD1_Scoring_Window__c = 10,
            VTR2_Protocol_Reviewer__c = 'Patient Guide',
            VTD1_Subject__c = 'Patient',
            VTD1_Response_Window__c = 10
        ));
        insert pEpros;

        List<VTD1_Protocol_ePro_Question__c> questions = new List<VTD1_Protocol_ePro_Question__c>();
        for (VTD1_Protocol_ePRO__c item : pEpros) {
            questions.add(new VTD1_Protocol_ePro_Question__c(
                VTD1_Protocol_ePRO__c = item.Id,
                VTD1_Safety_Threshold_Lower__c = 2
            ));
        }
        insert questions;

/*
        User patient = [
            SELECT Id, ContactId, Contact.AccountId
            FROM User
            WHERE Profile.Name = 'Patient'
                AND Contact.AccountId != null
            ORDER BY CreatedDate DESC
            LIMIT 1
        ];

        Contact con = new Contact(
            FirstName = 'CG',
            LastName = 'Caregiver',
            Email = 'test@gmail.com',
            AccountId = patient.Contact.AccountId,
            RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Caregiver').getRecordTypeId()
        );
        System.debug('here point 149');
        insert con;


        User caregiver = new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'Caregiver'].Id,
                FirstName = 'Caregiver',
                LastName = 'Caregiver' ,
                Email = VT_D1_TestUtils.generateUniqueUserName(),
                Username = VT_D1_TestUtils.generateUniqueUserName(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                IsActive = true,
                ContactId = con.Id
        );
        System.debug('here point 169');
        insert caregiver;
*/


        /*Account patientAcc = new Account(RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId(), Name = 'Patient Account for Patient User');
        insert patientAcc;
        User patientUser = VT_D1_TestUtils.createUserByProfile('Patient', 'PatientUserWithPatientRecordType');
        Contact patientCont = new Contact(FirstName = patientUser.FirstName, LastName = patientUser.LastName, AccountId = patientAcc.Id);
        insert patientCont;
        patientUser.ContactId = patientCont.Id;
        patientUser.VTD2_UserTimezone__c = 'America/Los_Angeles';
        insert patientUser;

        Contact con = new Contact(
                FirstName = 'CG',
                LastName = 'Caregiver',
                Email = 'test@gmail.com',
                AccountId = patientAcc.Id,
                RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Caregiver').getRecordTypeId(),
                VTR2_Doc_Language__c =  'es'
        );
        insert con;
        User caregiver = VT_D1_TestUtils.createUserByProfile('Caregiver', 'CaregiverTestUser');
        caregiver.ContactId = con.Id;
        caregiver.VTD2_UserTimezone__c = 'America/Los_Angeles';
        insert caregiver;*/

        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t().setRecordTypeByName('Sponsor');
        Account account = (Account) patientAccount.persist();

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
                .setFirstName('Patient')
                .setLastName('Patient')
                .setAccountId(account.Id);

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        patientUser.persist();

        DomainObjects.User_t caregiver = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t()
                        .setFirstName('Caregiver')
                        .setLastName('Caregiver')
                        .setEmail(DomainObjects.RANDOM.getEmail())
                        .setRecordTypeName('Caregiver')
                        .setAccountId(account.Id)
                )
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME);
        caregiver.persist();

        DomainObjects.User_t userPI = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Primary Investigator');
        userPI.persist();

        Test.startTest();
        User userPG = (User) new DomainObjects.User_t()
                .setProfile('Patient Guide').persist();


        Case cas = new Case(
            VTD1_Study__c = study.Id,
            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VTD1_PCF').getRecordTypeId(),
            VTD1_Patient_User__c = patientUser.Id,
            VTD1_Primary_PG__c = userPG.Id,
            VTD1_PI_contact__c = [SELECT ContactId FROM User WHERE Profile.Name = 'Primary Investigator' AND IsActive = true AND Username LIKE '%@scott.com' LIMIT 1].ContactId,
            VTD1_ePro_can_be_completed_by_Caregiver__c = true
        );

        insert cas;
        Test.stopTest();

        insert new VTD1_Actual_Visit__c(
            VTD1_Protocol_Visit__c = pVisit.Id,
            Sub_Type__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_SUBTYPE_PSC,
            VTD1_Case__c = cas.Id,
            VTD1_Status__c = 'Requested',
            VTD1_Compensation_Approved__c = true,
            VTD1_Visit_Completion_Date__c = System.now().addDays(1)
        );

    }
    @IsTest
    static void testBehavior_toAddPatient() {
        Test.startTest();
            VTD1_Actual_Visit__c visit = [SELECT Id FROM VTD1_Actual_Visit__c];
            visit.VTD1_Scheduled_Date_Time__c = Datetime.now();
            visit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED;
            update visit;

            VTD1_Survey__c eventSurvey = [SELECT Id, VTD1_Protocol_ePRO__c, VTD1_CSM__c
                                          FROM VTD1_Survey__c
                                          WHERE VTD1_Protocol_ePRO__r.VTD1_Safety_Keywords__c = 'Test'];

            VT_D1_ePROSchedulingNotificationHelper.NotificationTemplate nTemplate = new VT_D1_ePROSchedulingNotificationHelper.NotificationTemplate();
            nTemplate.surveyId = eventSurvey.Id;
            nTemplate.title = 'Title';
            nTemplate.messageLabel = 'VTR3_eDiary_Entry_Available_Notification';
            nTemplate.receiverId = UserInfo.getUserId();
            nTemplate.hasDirectLink = true;
            nTemplate.linkToRelatedEventOrObject = 'my-diary';

            VT_D1_ePROSchedulingNotificationHelper.sendPatientCaregiverNotifications(new VT_D1_ePROSchedulingNotificationHelper.NotificationTemplate[]{nTemplate});
            Test.stopTest();
            VTD1_NotificationC__c notification = [SELECT Id, Message__c, Title__c, Link_to_related_event_or_object__c, HasDirectLink__c
                                                  FROM VTD1_NotificationC__c
                                                  LIMIT 1];

            System.assertNotEquals(null, notification);
            System.assertEquals('my-diary', notification.Link_to_related_event_or_object__c);
            System.assertEquals(true, notification.HasDirectLink__c);
    }

    @IsTest
    static void testBehavior_toAddCaregiver() {
        Test.startTest();
        VTD1_Actual_Visit__c visit = [SELECT Id FROM VTD1_Actual_Visit__c];
        visit.VTD1_Scheduled_Date_Time__c = Datetime.now();
        visit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED;
        update visit;

        VTD1_Survey__c eventSurvey = [SELECT Id, VTD1_Protocol_ePRO__c,VTD1_Protocol_ePRO__r.VTD1_Subject__c, VTD1_CSM__c
        FROM VTD1_Survey__c
        WHERE VTD1_Protocol_ePRO__r.VTD1_Safety_Keywords__c = 'Test'];
        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c();
        insert study;
        VTD1_Protocol_ePRO__c protocolEPRO = new VTD1_Protocol_ePRO__c(
                VTD1_Subject__c = 'Caregiver',
                VTD1_Study__c = study.Id,
                VTR2_Protocol_Reviewer__c = 'Patient Guide',
                VTD1_Response_Window__c = 10
        );
        insert protocolEPRO;
        eventSurvey.VTD1_Protocol_ePRO__c = protocolEPRO.Id;
        User us = [SELECT Id FROM User WHERE Profile.Name = 'Caregiver' AND IsActive = TRUE LIMIT 1];
        eventSurvey.VTD1_Caregiver_User_Id__c = us.Id;
        update eventSurvey;

        VT_D1_ePROSchedulingNotificationHelper.NotificationTemplate nTemplate = new VT_D1_ePROSchedulingNotificationHelper.NotificationTemplate();
        nTemplate.surveyId = eventSurvey.Id;
        nTemplate.title = 'Title';
        nTemplate.messageLabel = 'VTR3_eDiary_Entry_Available_Notification';
        nTemplate.receiverId = UserInfo.getUserId();
        nTemplate.hasDirectLink = true;
        nTemplate.linkToRelatedEventOrObject = 'my-diary';

        VT_D1_ePROSchedulingNotificationHelper.sendPatientCaregiverNotifications(new VT_D1_ePROSchedulingNotificationHelper.NotificationTemplate[]{nTemplate});
        Test.stopTest();
            VTD1_NotificationC__c notification = [
                    SELECT Id, Message__c, Title__c, Link_to_related_event_or_object__c, HasDirectLink__c
                    FROM VTD1_NotificationC__c
                    LIMIT 1
            ];

            System.assertNotEquals(null, notification);
            System.assertEquals('my-diary', notification.Link_to_related_event_or_object__c);
            System.assertEquals(true, notification.HasDirectLink__c);
    }
}