/**
 * Created by Leonid Bartenev
 */


public with sharing class VT_R4_TranslationValidator extends Handler {

    private static final List<DisplayType> SUPPORTED_TYPES = new List<DisplayType>{
            DisplayType.STRING,
            DisplayType.TEXTAREA,
            DisplayType.COMBOBOX
    };
    private List<String> supportedLanguages = new List<String>();
    private List<PicklistEntry> picklistEntries = User.LanguageLocaleKey.getDescribe().getPicklistValues();

    public VT_R4_TranslationValidator() {
        for (Schema.PicklistEntry picklistEntry : this.picklistEntries) {
            this.supportedLanguages.add(picklistEntry.getValue());
        }
    }

    public override void onBeforeInsert(Handler.TriggerContext context) {
        validate(context.newList);
    }

    public override void onBeforeUpdate(Handler.TriggerContext context) {
        validate(context.newList);
    }

    private void validate(List<VTD1_Translation__c> translations) {
        for (VTD1_Translation__c translation : translations) {
            try {
                Id recordId = Id.valueOf(translation.VTD1_Record_Id__c);
                translation.VTD1_Record_Id__c = recordId + '';
                SObjectField sObjectField = recordId.getSobjectType().getDescribe().fields.getMap().get(translation.VTD1_Field_Name__c);
                if (sObjectField == null) {
                    translation.VTD1_Field_Name__c.addError('Field: "' + translation.VTD1_Field_Name__c + '" not found for object: ' + recordId.getSobjectType());
                } else if(String.valueOf(recordId.getSobjectType()) != translation.VTD1_Object_Name__c) {
                    translation.VTD1_Object_Name__c.addError('Id is never corresponded to ' + translation.VTD1_Object_Name__c + ' but is to ' + recordId.getSobjectType());
                } else if (!SUPPORTED_TYPES.contains(sObjectField.getDescribe().getType())) {
                    translation.VTD1_Field_Name__c.addError('Unsupported field type. Only string fields are supported. Field type: ' + sObjectField.getDescribe().getType());
                } else if (sObjectField.getDescribe().isCalculated()) {
                    translation.VTD1_Field_Name__c.addError('Formula field not supported for translation');
                }
//                System.debug('supportedLanguages: ' + supportedLanguages);
//                System.debug('now: ' + Datetime.now());
                if (!supportedLanguages.contains(translation.VTD1_Language__c)) {
                    translation.VTD1_Language__c.addError('Unsupported language, code: ' + translation.VTD1_Language__c);
                }
            } catch (Exception e) {
                translation.VTD1_Record_Id__c.addError(e.getMessage());
            }
        }
    }

}