/**
 * Update by Galiya Khalikova on 31.12.2019.
 * As part of refactoring in R4 SH-7022
 */
@isTest
private with sharing class VT_D1_OutOfOfficeTaskReassignReminTest {
/*
    @testSetup
    private static void setup() {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );

        List <VTD2_TN_Catalog_Code__c> tnCatalogCodes = new List<VTD2_TN_Catalog_Code__c>();
        Test.startTest();
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'T572',
                VTD2_T_Subject__c = 'test',
                VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD_RTId__c.VTD1_Task_SimpleTask__c.Id',
                VTD2_T_SObject_Name__c = 'User',
                VTD2_T_Assigned_To_ID__c = 'User.Id',
                VTD2_T_Category__c = 'Follow Up Required',
                VTD2_T_Due_Date__c = 'TODAY()',
                VTD2_T_Status__c = 'Open',
                VTD2_T_Priority__c = 'Normal',
                VTD2_T_Reminder_Set__c = true,
                VTD2_T_Reminder_Date_Time__c = 'NOW()',
                VTD2_T_Type_for_Backend_Logic__c = 'Update Calendar',
                VTR4_T_isBatchReminder__c = true
        ));
        Test.stopTest();
        insert tnCatalogCodes;
    }

    @isTest
    private static void doTest() {
        Test.startTest();
        Database.executeBatch(new VT_D1_OutOfOfficeTaskReassignReminder());
        Test.stopTest();
        //System.assertEquals(1, [SELECT Id FROM Task].size());
    }*/
}