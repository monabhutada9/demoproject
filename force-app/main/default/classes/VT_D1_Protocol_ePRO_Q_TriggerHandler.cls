/**
* @author: Carl Judge
* @date: 27-Aug-18
* @description: 
**/

public without sharing class VT_D1_Protocol_ePRO_Q_TriggerHandler {

    public void onBeforeInsert(List<VTD1_Protocol_ePro_Question__c> recs) {
        validateUpdates(recs);
    }

    public void onBeforeUpdate(List<VTD1_Protocol_ePro_Question__c> recs, Map<Id, VTD1_Protocol_ePro_Question__c> oldMap) {
        validateUpdates(recs);
    }

    public void onAfterDelete(List<VTD1_Protocol_ePro_Question__c> recs) {
        deleteQuestion(recs);
    }

    public void onAfterInsert(List<VTD1_Protocol_ePro_Question__c> recs) {
        updateContainsBranchingLogicForParentQuestion(recs);
    }

    public void onAfterUpdate(List<VTD1_Protocol_ePro_Question__c> recs, Map<Id, VTD1_Protocol_ePro_Question__c> oldMap) {
        List<VTD1_Protocol_ePro_Question__c> childQuestions = new List<VTD1_Protocol_ePro_Question__c>();
        List<VTD1_Protocol_ePro_Question__c> childQuestionsOldMap = new List<VTD1_Protocol_ePro_Question__c>();
        for (VTD1_Protocol_ePro_Question__c item : recs) {
            VTD1_Protocol_ePro_Question__c old = oldMap.get(item.Id);
            if (item.VTR4_Branch_Parent__c != old.VTR4_Branch_Parent__c) {
                childQuestions.add(item);
                childQuestionsOldMap.add(old);
            }
        }
        if (!childQuestions.isEmpty()) {
            updateContainsBranchingLogicForParentQuestion(childQuestions);
            deleteQuestion(childQuestionsOldMap);
        }
    }

    private void validateUpdates(List<VTD1_Protocol_ePro_Question__c> recs) {
        List<Id> protocolIds = new List<Id>();
        for (VTD1_Protocol_ePro_Question__c item : recs) {
            protocolIds.add(item.VTD1_Protocol_ePRO__c);
        }

        Set<Id> inprogressIds = VT_D1_Protocol_ePRO_Update_Verifier.getInprogressIds(
        [SELECT Id, VTR4_Ignore_Edit_Validation__c FROM VTD1_Protocol_ePRO__c WHERE Id IN :protocolIds]
        );

        for (VTD1_Protocol_ePro_Question__c item : recs) {
            if (inprogressIds.contains(item.VTD1_Protocol_ePRO__c)) {
                item.addError(VT_D1_Protocol_ePRO_Update_Verifier.ERROR_MESSAGE);
            }
        }


    }

    /**
    * Added by Galiya Khalikova on 23.01.2020.
    * Edited as SH-7381
    * @param recs List<VTD1_Protocol_ePro_Question__c> contains list Protocol Question
    * update checkbox field Contains Branching Logic on true for Parent Question
    */
    private void updateContainsBranchingLogicForParentQuestion(List<VTD1_Protocol_ePro_Question__c> recs) {
        Set<Id> parentIds = new Set<Id>();
        List <VTD1_Protocol_ePro_Question__c> parentQuestions = new List<VTD1_Protocol_ePro_Question__c>();

        for (VTD1_Protocol_ePro_Question__c item : recs) {
            if (item.VTR4_Branch_Parent__c != null) {
                parentIds.add(item.VTR4_Branch_Parent__c);
            }
        }

        if (!parentIds.isEmpty()) {
            for (Id parentId : parentIds) {
                parentQuestions.add(new VTD1_Protocol_ePro_Question__c(Id = parentId, VTR4_Contains_Branching_Logic__c = false));
            }

            for (VTD1_Protocol_ePro_Question__c item : parentQuestions) {
                item.VTR4_Contains_Branching_Logic__c = true;
            }

            update parentQuestions;
        }
    }

    /**
    * Added by Galiya Khalikova on 24.01.2020.
    * Edited as SH-7381
    * @param recs List<VTD1_Protocol_ePro_Question__c> contains list Protocol Question
    * update checkbox field Contains Branching Logic on false for Parent Question, if it doesn't contain childs question(records)
    */
    private void deleteQuestion(List<VTD1_Protocol_ePro_Question__c> recs) {

        Set<Id> parentIds = new Set<Id>();
        Map<Id, List<Id>> ParentIdToChildIds = new Map<Id, List<Id>>();
        List <VTD1_Protocol_ePro_Question__c> parentQuestions = new List<VTD1_Protocol_ePro_Question__c>();

        for (VTD1_Protocol_ePro_Question__c item : recs) {
            if (item.VTR4_Branch_Parent__c != null) {
                parentIds.add(item.VTR4_Branch_Parent__c);
            }
        }
        if (!parentIds.isEmpty()) {
            for (Id parentId : parentIds) {
                parentQuestions.add(new VTD1_Protocol_ePro_Question__c(Id = parentId, VTR4_Contains_Branching_Logic__c = true));
            }

            List <VTD1_Protocol_ePro_Question__c> childQuestions = [
                SELECT Id, VTR4_Branch_Parent__c
                from
                    VTD1_Protocol_ePro_Question__c
                where VTR4_Branch_Parent__c IN:parentIds
            ];

            for (VTD1_Protocol_ePro_Question__c childQuestion : childQuestions) {
                if (!ParentIdToChildIds.containsKey(childQuestion.VTR4_Branch_Parent__c)) {
                    ParentIdToChildIds.put(childQuestion.VTR4_Branch_Parent__c, new List<Id>());
                }
                ParentIdToChildIds.get(childQuestion.VTR4_Branch_Parent__c).add(childQuestion.Id);
            }

            for (VTD1_Protocol_ePro_Question__c item : parentQuestions) {
                if (!ParentIdToChildIds.containsKey(item.Id)) {
                    item.VTR4_Contains_Branching_Logic__c = false;
                }
            }

            update parentQuestions;
        }
    }
}