/**
* @author: Carl Judge
* @date: 19-Sep-20
* @description: Class to build sharing queueable chains
**/

public class VT_R5_SharingChainFactory {
    public static final List<SObjectType> STAFF_CONFIG_OBJECT_TYPES = new List<SObjectType> {
        HealthCloudGA__CarePlanTemplate__c.getSObjectType(),
        VTD1_Monitoring_Visit__c.getSObjectType(),
        VTD1_Protocol_Deviation__c.getSObjectType(), // PDs are shared with groups, but are also viewable by some profiles that don't see patients. So it needs to be here too
        VTD1_Document__c.getSObjectType() 
    };

    // Documents are shared conditionally to pt/cg. Add any objects for ALL pt.cg here
    public static final List<SObjectType> PATIENT_OBJECT_TYPES = new List<SObjectType> {
        HealthCloudGA__CarePlanTemplate__c.getSObjectType()
    };

    public static final List<SObjectType> CAREGIVER_OBJECT_TYPES = new List<SObjectType> {
        HealthCloudGA__CarePlanTemplate__c.getSObjectType(),
        Case.getSObjectType(),
        User.getSObjectType(),
        Account.getSObjectType()
    };

    public static final List<SObjectType> CAREPLAN_OBJECT_TYPES = new List<SObjectType> {
        Case.getSObjectType(),
        User.getSObjectType(),
        Account.getSObjectType()
    };

    public static final List<SObjectType> CHANGED_SITE_OBJECT_TYPES = new List<SObjectType> {
        VTD1_Monitoring_Visit__c.getSObjectType(),
        VTD1_Document__c.getSObjectType()
    };

    public static VT_R3_AbstractChainableQueueable createSharingQueueable(Set<Id> recordIds, Set<Id> userIds,
        Set<Id> scopeIds, SObjectType sObjectType, Boolean removeOnly)
    {
        VT_R3_AbstractGlobalSharingLogic sharingLogic = VT_R3_AbstractGlobalSharingLogic.getSharingLogicForObjectType(sObjectType.getDescribe().getName());
        if (sharingLogic != null) {
            sharingLogic.init(recordIds, userIds, scopeIds, removeOnly);
        }
        return sharingLogic;
    }
    public static VT_R3_AbstractChainableQueueable createSharingQueueable(Set<Id> recordIds, Set<Id> userIds,
        Set<Id> scopeIds, SObjectType sObjectType)
    {
        return createSharingQueueable(recordIds, userIds, scopeIds, sObjectType, false);
    }

    public static VT_R3_AbstractChainableQueueable createFromCarePlanIds(Set<Id> carePlanIds) {
        VT_R3_AbstractChainableQueueable chainQueueable;
        Set<Id> recordIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        Set<Id> scopeIds = new Set<Id>();
        for(SObjectType objectType : CAREPLAN_OBJECT_TYPES){
            VT_R3_GlobalSharingLogic_PatientRelated sharingLogic = (VT_R3_GlobalSharingLogic_PatientRelated)VT_R3_AbstractGlobalSharingLogic.getSharingLogicForObjectType(objectType.getDescribe().getName());
            sharingLogic.init(recordIds, userIds, scopeIds, false);
            sharingLogic.setCarePlanIds(carePlanIds);
            if(chainQueueable == null) {
                chainQueueable = sharingLogic;
            } else {
                chainQueueable.addToChain(sharingLogic);
            }
        }
        return chainQueueable;
    }

    public static VT_R3_AbstractChainableQueueable createFromChangedSiteUsers(List<VT_R5_SiteMemberCalculator.SiteUser> addedUsers,
        List<VT_R5_SiteMemberCalculator.SiteUser> removedUsers)
    {
        List<VT_R3_AbstractChainableQueueable> chains = new List<VT_R3_AbstractChainableQueueable>();
        if (addedUsers != null && !addedUsers.isEmpty()) {
            chains.add(createFromSiteUsers(addedUsers));
        }
        if (removedUsers != null && !removedUsers.isEmpty()) {
            chains.add(createFromSiteUsersRemoval(removedUsers));
        }
        return combineChainQueueables(chains);
    }

    public static VT_R3_AbstractChainableQueueable createFromSiteUsers(List<VT_R5_SiteMemberCalculator.SiteUser> siteUsers) {
        VT_R3_AbstractChainableQueueable chainQueueable;
        Set<Id> recordIds = new Set<Id>();
        Set<Id> userIds = getUserIds(siteUsers);
        Set<Id> siteIds = getSiteIds(siteUsers);
        for (SObjectType objectType : CHANGED_SITE_OBJECT_TYPES) {
            VT_R3_AbstractGlobalSharingLogic sharingLogic = VT_R3_AbstractGlobalSharingLogic.getSharingLogicForObjectType(objectType.getDescribe().getName());
            sharingLogic.init(recordIds, userIds, siteIds, false);
            if (chainQueueable == null) {
                chainQueueable = sharingLogic;
            } else {
                chainQueueable.addToChain(sharingLogic);
            }
        }
        return chainQueueable;
    }

    public static VT_R3_AbstractChainableQueueable createFromSiteUsersRemoval(List<VT_R5_SiteMemberCalculator.SiteUser> siteUsers) {
        RemovalChainCreator removalCreator = new RemovalChainCreator();
        for (VT_R5_SiteMemberCalculator.SiteUser siteUser : siteUsers) {
            removalCreator.addParamSet(siteUser.siteId, CHANGED_SITE_OBJECT_TYPES, siteUser.userId);
        }
        return removalCreator.getChain();
    }

    public static VT_R3_AbstractChainableQueueable createFromStudyConfigs(List<VTD1_Study_Sharing_Configuration__c> configs){
        ConfigGrouper groupedConfigs = new ConfigGrouper(configs);
        List<VT_R3_AbstractChainableQueueable> queueables = new List<VT_R3_AbstractChainableQueueable>();
        queueables.addAll(getConfigCreateChains(groupedConfigs));
        queueables.add(getConfigRemovalChain(groupedConfigs));
        if (!VT_D1_StudySharingConfigurationHandler.isConversion) {
            queueables.add(new VT_R3_ChainQueueable_PostStudySharing(configs));
        }
        VT_R3_AbstractChainableQueueable combinedChain = combineChainQueueables(queueables);
        return combinedChain;
    }

    public static VT_R3_AbstractChainableQueueable createFromInvocableMethod(List<VT_R3_GlobalSharing.ParamsHolder> params) {
        Map<SObjectType, Set<Id>> recordIdsByObjType = new Map<SObjectType, Set<Id>>();
        for (VT_R3_GlobalSharing.ParamsHolder param : params) {
            SObjectType objType = param.recordId.getSobjectType();
            if (!recordIdsByObjType.containsKey(objType)) {
                recordIdsByObjType.put(objType, new Set<Id>());
            }
            recordIdsByObjType.get(objType).add(param.recordId);
        }
        VT_R3_AbstractChainableQueueable chainQueueable;
        for(SObjectType objectType : recordIdsByObjType.keySet()){
            System.debug(objectType);
            VT_R3_AbstractGlobalSharingLogic sharingLogic = VT_R3_AbstractGlobalSharingLogic.getSharingLogicForObjectType(objectType.getDescribe().getName());
            if (sharingLogic != null) {
                sharingLogic.init(recordIdsByObjType.get(objectType), null, null, false);
                if(chainQueueable == null) {
                    chainQueueable = sharingLogic;
                } else {
                    chainQueueable.addToChain(sharingLogic);
                }
            }
        }
        return chainQueueable;
    }

    public static VT_R3_AbstractChainableQueueable combineChainQueueables(List<VT_R3_AbstractChainableQueueable> chainQueueables) {
        VT_R3_AbstractChainableQueueable combinedChain;
        for (VT_R3_AbstractChainableQueueable chainItem : chainQueueables) {
            if (chainItem != null) {
                if (combinedChain == null) {
                    combinedChain = chainItem;
                } else {
                    combinedChain.addToChain(chainItem);
                }
            }
        }
        return combinedChain;
    }

    private static VT_R3_AbstractChainableQueueable createPTWithDocumentsChain(List<VTD1_Study_Sharing_Configuration__c> configs){
        Set<Id> scopeIds = getScopeIdsPatient(configs);
        List<SObjectType> objectTypes = new List<SObjectType>{VTD1_Document__c.getSObjectType()};
        if (!VT_D1_StudySharingConfigurationHandler.isConversion) {
            objectTypes.addAll(PATIENT_OBJECT_TYPES);
        }
        return buildConfigChainQueueable(configs, scopeIds, objectTypes);
    }

    private static VT_R3_AbstractChainableQueueable createPTWithoutDocumentsChain(List<VTD1_Study_Sharing_Configuration__c> configs){
        Set<Id> scopeIds = getScopeIdsPatient(configs);
        return buildConfigChainQueueable(configs, scopeIds, PATIENT_OBJECT_TYPES);
    }

    private static VT_R3_AbstractChainableQueueable createCGWithDocumentsChain(List<VTD1_Study_Sharing_Configuration__c> configs){
        Set<Id> scopeIds = getScopeIdsCG(configs);
        List<SObjectType> objectTypes = CAREGIVER_OBJECT_TYPES;
        objectTypes.add(VTD1_Document__c.getSObjectType());
        return buildConfigChainQueueable(configs, scopeIds, objectTypes);
    }

    private static VT_R3_AbstractChainableQueueable createCGWithoutDocumentsChain(List<VTD1_Study_Sharing_Configuration__c> configs){
        Set<Id> scopeIds = getScopeIdsCG(configs);
        return buildConfigChainQueueable(configs, scopeIds, CAREGIVER_OBJECT_TYPES);
    }

    private static VT_R3_AbstractChainableQueueable createSiteStaffChain(List<VTD1_Study_Sharing_Configuration__c> configs){
        Set<Id> scopeIds = getScopeIdsSiteStaff(configs);
        List<SObjectType> objectTypes = STAFF_CONFIG_OBJECT_TYPES;
        // if site level user has no site scopes,  calc shares just for study level objects
        if (scopeIds.isEmpty()) {
            scopeIds = getScopeIdsStudyStaff(configs);
            objectTypes = new List<SObjectType>{
                HealthCloudGA__CarePlanTemplate__c.getSObjectType(),
                VTD1_Monitoring_Visit__c.getSObjectType(),
                VTD1_Action_Item__c.getSObjectType()
            };
        }
        return buildConfigChainQueueable(configs, scopeIds, objectTypes);
    }

    private static VT_R3_AbstractChainableQueueable createStudyStaffChain(List<VTD1_Study_Sharing_Configuration__c> configs){
        Set<Id> scopeIds = getScopeIdsStudyStaff(configs);
        return buildConfigChainQueueable(configs, scopeIds, STAFF_CONFIG_OBJECT_TYPES);
    }

    private static List<VT_R3_AbstractChainableQueueable> getConfigCreateChains(ConfigGrouper grouper) {
        List<VT_R3_AbstractChainableQueueable> queueables = new List<VT_R3_AbstractChainableQueueable>();
        if (!VT_D1_StudySharingConfigurationHandler.isConversion) { // non-document shares created in conversion process
            if (!grouper.patientNoDocsConfigs.isEmpty()) {
                queueables.add(createPTWithoutDocumentsChain(grouper.patientNoDocsConfigs));
            }
        }
        if (!grouper.cgNoDocsConfigs.isEmpty()) {
            queueables.add(createCGWithoutDocumentsChain(grouper.cgNoDocsConfigs));
        }
        if (!grouper.patientConfigs.isEmpty()) {
            queueables.add(createPTWithDocumentsChain(grouper.patientConfigs));
        }
        if (!grouper.cgConfigs.isEmpty()) {
            queueables.add(createCGWithDocumentsChain(grouper.cgConfigs));
        }

        if (!grouper.siteConfigs.isEmpty()) {
            queueables.add(createSiteStaffChain(grouper.siteConfigs));
        }
        if (!grouper.studyConfigs.isEmpty()) {
            queueables.add(createStudyStaffChain(grouper.studyConfigs));
        }
        return queueables;
    }

    private static VT_R3_AbstractChainableQueueable getConfigRemovalChain(ConfigGrouper grouper) {
        RemovalChainCreator removalCreator = new RemovalChainCreator();
        if (!grouper.patientConfigsNoAccess.isEmpty()) {
            for (VTD1_Study_Sharing_Configuration__c config : grouper.patientConfigsNoAccess) {
                removalCreator.addParamSet(config.VTD1_Study__c, PATIENT_OBJECT_TYPES, config.VTD1_User__c);
            }
        }
        if (!grouper.patientNoDocsConfigsNoAccess.isEmpty()) {
            List<SObjectType> objectTypes = PATIENT_OBJECT_TYPES;
            objectTypes.add(VTD1_Document__c.getSObjectType());
            for (VTD1_Study_Sharing_Configuration__c config : grouper.patientConfigsNoAccess) {
                removalCreator.addParamSet(config.VTD1_Study__c, objectTypes, config.VTD1_User__c);
            }
        }
        if (!grouper.cgConfigsNoAccess.isEmpty()) {
            for (VTD1_Study_Sharing_Configuration__c config : grouper.cgConfigsNoAccess) {
                removalCreator.addParamSet(config.VTD1_Study__c, CAREGIVER_OBJECT_TYPES, config.VTD1_User__c);
            }
        }
        if (!grouper.cgNoDocsConfigsNoAccess.isEmpty()) {
            List<SObjectType> objectTypes = CAREGIVER_OBJECT_TYPES;
            objectTypes.add(VTD1_Document__c.getSObjectType());
            for (VTD1_Study_Sharing_Configuration__c config : grouper.cgNoDocsConfigsNoAccess) {
                removalCreator.addParamSet(config.VTD1_Study__c, objectTypes, config.VTD1_User__c);
            }
        }
        if (!grouper.siteConfigsNoAccess.isEmpty()) {
            for (VTD1_Study_Sharing_Configuration__c config : grouper.siteConfigsNoAccess) {
                removalCreator.addParamSet(config.VTD1_Study__c, STAFF_CONFIG_OBJECT_TYPES, config.VTD1_User__c);
            }
        }
        if (!grouper.studyConfigsNoAccess.isEmpty()) {
            for (VTD1_Study_Sharing_Configuration__c config : grouper.studyConfigsNoAccess) {
                removalCreator.addParamSet(config.VTD1_Study__c, STAFF_CONFIG_OBJECT_TYPES, config.VTD1_User__c);
            }
        }
        return removalCreator.getChain();
    }

    private static VT_R3_AbstractChainableQueueable buildConfigChainQueueable(List<VTD1_Study_Sharing_Configuration__c> configs,
        Set<Id> scopeIds, List<SObjectType> objectTypes)
    {
        VT_R3_AbstractChainableQueueable chainQueueable;
        if (!scopeIds.isEmpty()) {
            Set<Id> userIds = getUserIds(configs);
            Set<Id> recordIds = new Set<Id>();
            for(SObjectType objectType : objectTypes){
                VT_R3_AbstractGlobalSharingLogic sharingLogic = VT_R3_AbstractGlobalSharingLogic.getSharingLogicForObjectType(objectType.getDescribe().getName());
                sharingLogic.init(recordIds, userIds, scopeIds, false);
                if(chainQueueable == null) {
                    chainQueueable = sharingLogic;
                } else {
                    chainQueueable.addToChain(sharingLogic);
                }
            }
        }
        return chainQueueable;
    }

    private static Set<Id> getScopeIdsPatient(List<VTD1_Study_Sharing_Configuration__c> configs) {
        Set<Id> userIds = getUserIds(configs);
        return new Map<Id, Case>([SELECT Id FROM Case WHERE VTD1_Patient_User__c IN :userIds]).keySet();
    }

    private static Set<Id> getScopeIdsCG(List<VTD1_Study_Sharing_Configuration__c> configs) {
        Set<Id> studyIds = new Set<Id>();
        Set<Id> accIds = new Set<Id>();
        for (VTD1_Study_Sharing_Configuration__c config : [
            SELECT VTD1_Study__c, VTD1_User__r.Contact.AccountId
            FROM VTD1_Study_Sharing_Configuration__c
            WHERE Id IN :configs
        ]) {
            studyIds.add(config.VTD1_Study__c);
            accIds.add(config.VTD1_User__r.Contact.AccountId);
        }
        return new Map<Id, Case>([
            SELECT Id FROM Case
            WHERE Contact.AccountId IN :accIds
            AND VTD1_Study__c IN :studyIds
            AND RecordType.DeveloperName = 'CarePlan'
        ]).keySet();
    }

    private static Set<Id> getScopeIdsSiteStaff(List<VTD1_Study_Sharing_Configuration__c> configs) {
        Set<Id> studyIds = getStudyIds(configs);
        Set<Id> userIds = getUserIds(configs);
        Set<Id> scopeIds = new Set<Id>();
        VT_R5_SiteMemberCalculator memberCalc = new VT_R5_SiteMemberCalculator();
        memberCalc.initForUsers(userIds, studyIds);
        scopeIds.addAll(memberCalc.getSiteToUsersMap().keySet());
        return scopeIds;
    }

    private static Set<Id> getScopeIdsStudyStaff(List<VTD1_Study_Sharing_Configuration__c> configs) {
        Set<Id> scopeIds = new Set<Id>();
        for (VTD1_Study_Sharing_Configuration__c config : configs) {
            scopeIds.add(config.VTD1_Study__c);
        }
        return scopeIds;
    }

    private static Set<Id> getUserIds(List<VTD1_Study_Sharing_Configuration__c> configs) {
        Set<Id> userIds = new Set<Id>();
        for (VTD1_Study_Sharing_Configuration__c config : configs) {
            userIds.add(config.VTD1_User__c);
        }
        return userIds;
    }

    private static Set<Id> getStudyIds(List<VTD1_Study_Sharing_Configuration__c> configs) {
        Set<Id> studyIds = new Set<Id>();
        for (VTD1_Study_Sharing_Configuration__c config : configs) {
            studyIds.add(config.VTD1_Study__c);
        }
        return studyIds;
    }

    private static Set<Id> getUserIds(List<VT_R5_SiteMemberCalculator.SiteUser> siteUsers) {
        Set<Id> userIds = new Set<Id>();
        for (VT_R5_SiteMemberCalculator.SiteUser siteUser : siteUsers) {
            userIds.add(siteUser.userId);
        }
        return userIds;
    }

    private static Set<Id> getSiteIds(List<VT_R5_SiteMemberCalculator.SiteUser> siteUsers) {
        Set<Id> siteIds = new Set<Id>();
        for (VT_R5_SiteMemberCalculator.SiteUser siteUser : siteUsers) {
            siteIds.add(siteUser.siteId);
        }
        return siteIds;
    }

    private class RemovalChainCreator {
        // scope Id => sobject list => userIds
        private Map<Id, Map<List<SObjectType>, Set<Id>>> logicParamsMap = new Map<Id, Map<List<SObjectType>, Set<Id>>>();

        public void addParamSet(Id scopeId, List<SObjectType> objectTypes, Id userId) {
            if (!logicParamsMap.containsKey(scopeId)) {
                logicParamsMap.put(scopeId, new Map<List<SObjectType>, Set<Id>>());
            }
            if (!logicParamsMap.get(scopeId).containsKey(objectTypes)) {
                logicParamsMap.get(scopeId).put(objectTypes, new Set<Id>());
            }
            logicParamsMap.get(scopeId).get(objectTypes).add(userId);
        }

        public VT_R3_AbstractChainableQueueable getChain() {
            VT_R3_AbstractChainableQueueable chainQueueable;
            Set<Id> recordIds = new Set<Id>();
            for (Id scopeId : logicParamsMap.keySet()) {
                Set<Id> scopeIds = new Set<Id>{scopeId};
                for (List<SObjectType> objectTypes : logicParamsMap.get(scopeId).keySet()) {
                    Set<Id> userIds = logicParamsMap.get(scopeId).get(objectTypes);
                    for (SObjectType objectType : objectTypes) {
                        VT_R3_AbstractGlobalSharingLogic sharingLogic = VT_R3_AbstractGlobalSharingLogic.getSharingLogicForObjectType(objectType.getDescribe().getName());
                        sharingLogic.init(recordIds, userIds, scopeIds, true);
                        if (chainQueueable == null) {
                            chainQueueable = sharingLogic;
                        } else {
                            chainQueueable.addToChain(sharingLogic);
                        }
                    }
                }
            }
            return chainQueueable;
        }
    }

    private class ConfigGrouper {
        public List<VTD1_Study_Sharing_Configuration__c> patientConfigs = new List<VTD1_Study_Sharing_Configuration__c>();
        public List<VTD1_Study_Sharing_Configuration__c> patientNoDocsConfigs = new List<VTD1_Study_Sharing_Configuration__c>();
        public List<VTD1_Study_Sharing_Configuration__c> cgConfigs = new List<VTD1_Study_Sharing_Configuration__c>();
        public List<VTD1_Study_Sharing_Configuration__c> cgNoDocsConfigs = new List<VTD1_Study_Sharing_Configuration__c>();
        public List<VTD1_Study_Sharing_Configuration__c> siteConfigs = new List<VTD1_Study_Sharing_Configuration__c>();
        public List<VTD1_Study_Sharing_Configuration__c> studyConfigs = new List<VTD1_Study_Sharing_Configuration__c>();
        public List<VTD1_Study_Sharing_Configuration__c> patientConfigsNoAccess = new List<VTD1_Study_Sharing_Configuration__c>();
        public List<VTD1_Study_Sharing_Configuration__c> patientNoDocsConfigsNoAccess = new List<VTD1_Study_Sharing_Configuration__c>();
        public List<VTD1_Study_Sharing_Configuration__c> cgConfigsNoAccess = new List<VTD1_Study_Sharing_Configuration__c>();
        public List<VTD1_Study_Sharing_Configuration__c> cgNoDocsConfigsNoAccess = new List<VTD1_Study_Sharing_Configuration__c>();
        public List<VTD1_Study_Sharing_Configuration__c> siteConfigsNoAccess = new List<VTD1_Study_Sharing_Configuration__c>();
        public List<VTD1_Study_Sharing_Configuration__c> studyConfigsNoAccess = new List<VTD1_Study_Sharing_Configuration__c>();
        private List<VTD1_Study_Sharing_Configuration__c> allConfigs;

        public ConfigGrouper(List<VTD1_Study_Sharing_Configuration__c> configs) {
            allConfigs = configs;
            groupConfigs();
        }

        public void groupConfigs() {
            for (VTD1_Study_Sharing_Configuration__c config : [
                SELECT Id, VTD1_Study__c, VTD1_User__c, VTD1_User__r.Profile.Name, VTD1_Access_Level__c,
                    VTD1_Study__r.VTR5_Hide_Study_Documents_For_Patients__c
                FROM VTD1_Study_Sharing_Configuration__c
                WHERE Id IN :allConfigs
            ]) {
                if (config.VTD1_Access_Level__c != null && config.VTD1_Access_Level__c != 'None') {
                    // with access
                    if (config.VTD1_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
                        if (config.VTD1_Study__r.VTR5_Hide_Study_Documents_For_Patients__c) {
                            patientNoDocsConfigs.add(config);
                        } else {
                            patientConfigs.add(config);
                        }
                    } else if (config.VTD1_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME) {
                        if (config.VTD1_Study__r.VTR5_Hide_Study_Documents_For_Patients__c) {
                            cgNoDocsConfigs.add(config);
                        } else {
                            cgConfigs.add(config);
                        }
                    } else if (VT_R5_SiteMemberCalculator.SITE_PROFILES.contains(config.VTD1_User__r.Profile.Name)) {
                        siteConfigs.add(config);
                    } else {
                        studyConfigs.add(config);
                    }
                } else {
                    // no access
                    if (config.VTD1_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
                        if (config.VTD1_Study__r.VTR5_Hide_Study_Documents_For_Patients__c) {
                            patientNoDocsConfigsNoAccess.add(config);
                        } else {
                            patientConfigsNoAccess.add(config);
                        }
                    } else if (config.VTD1_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME) {
                        if (config.VTD1_Study__r.VTR5_Hide_Study_Documents_For_Patients__c) {
                            cgNoDocsConfigsNoAccess.add(config);
                        } else {
                            cgConfigsNoAccess.add(config);
                        }
                    } else if (VT_R5_SiteMemberCalculator.SITE_PROFILES.contains(config.VTD1_User__r.Profile.Name)) {
                        siteConfigsNoAccess.add(config);
                    } else {
                        studyConfigsNoAccess.add(config);
                    }
                }
            }
        }
    }

    public class VT_R3_ChainQueueable_PostStudySharing extends VT_R3_AbstractChainableQueueable {
        public List<Id> configIds;

        public VT_R3_ChainQueueable_PostStudySharing(List<VTD1_Study_Sharing_Configuration__c> configs) {
            this.configIds = new List<Id>(new Map<Id, VTD1_Study_Sharing_Configuration__c>(configs).keySet());
        }

        public override Type getType() {
            return VT_R3_ChainQueueable_PostStudySharing.class;
        }

        public override void executeLogic() {
            List<VTD1_Study_Sharing_Configuration__c> configs = new List<VTD1_Study_Sharing_Configuration__c>();
            for (Id configId : configIds) {
                configs.add(new VTD1_Study_Sharing_Configuration__c(Id = configId, VTD1_Locked__c = false));
            }
            if (!Test.isRunningTest()) { update configs; } // prevent recursive update loop in tests as this doesn't run async
        }
    }
}