@IsTest
private with sharing class DoNotDeployAllTest {
    @TestSetup
    static void setup() {
        Test.startTest();

        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );
        DomainObjects.User_t user = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5));
        DomainObjects.User_t piuser = new DomainObjects.User_t()
                .setProfile('Primary Investigator')
                .setEmail('test@email.com')
                .setUsername('test15052020_1@email.com')
                .addContact(new DomainObjects.Contact_t());
            piuser.persist();
        DomainObjects.StudyTeamMember_t stm = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('PI')
                .addUser(piuser)
                .addStudy(study);
       		 stm.persist();
        //Added by :Conquerors, Priyanka Ambre SH-21178	
        //START
        DomainObjects.User_t scruser = new DomainObjects.User_t()
                .setProfile('Site Coordinator')
                .setEmail('testscr@email.com')
                .setUsername('test15052020_2@email.com')
                .addContact(new DomainObjects.Contact_t());
            piuser.persist();        
        DomainObjects.StudyTeamMember_t stm1 = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('SCR')
                .addUser(scruser)
                .addStudy(study);
                stm1.persist();
        //END
        DomainObjects.VirtualSite_t virtSite = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .setStudySiteNumber('12345')
                .addStudyTeamMember(stm);
            virtSite.persist();
		   DomainObjects.VirtualSite_t virtSite2 = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .setStudySiteNumber('12345')
                .addStudyTeamMember(stm);
            virtSite.persist();
         Test.stoptest();
        Case cas = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .setSubject('EDiaryAllTests')
                .addStudy(study)
                .addUser(user)
                .addVirtualSite(virtSite)
                .persist();
           Case cas2 = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .setSubject('EDiaryAllTests')
                .addStudy(study)
                .addUser(user)
                .addVirtualSite(virtSite2)
                .persist();
        DomainObjects.VTD1_Protocol_ePRO_t protocolEPROT = new DomainObjects.VTD1_Protocol_ePRO_t()
                .setRecordTypeByName('SatisfactionSurvey')
                .addVTD1_Study(study)
                .setVTD1_Type('Non-PSC')
                .setVTR4_Is_Branching_eDiary(true)
                .setVTR2_Protocol_Reviewer('Patient Guide')
                .setVTD1_Subject('Patient')
                .setVTD1_Response_Window(10);
        DomainObjects.VTD1_Protocol_ePRO_t protocolEPROT2 = new DomainObjects.VTD1_Protocol_ePRO_t()
                .setRecordTypeByName('SatisfactionSurvey')
                .addVTD1_Study(study)
                .setVTR2_Protocol_Reviewer('Patient Guide')
                .setVTD1_Subject('Patient')
                .setVTD1_Response_Window(10);
        DomainObjects.VTD1_Survey_t survey = new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(cas.Id)
                .setName('testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttest')
                .addVTD1_Protocol_ePRO(protocolEPROT);
        survey.persist();
        DomainObjects.VTD1_Survey_t survey2 = new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(cas.Id)
                .addVTD1_Protocol_ePRO(protocolEPROT2)
                .setName('test2test2')
                .setRecordTypeByName('VTR5_External');
        survey2.persist();
          DomainObjects.VTD1_Survey_t survey3 = new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(cas2.Id)
                .addVTD1_Protocol_ePRO(protocolEPROT2)
                .setName('test4test4')
                .setRecordTypeByName('VTR5_External');
        survey3.persist();
          DomainObjects.VTD1_Survey_t survey4 = new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(cas2.Id)
                .addVTD1_Protocol_ePRO(protocolEPROT2)
                .setName('test4test4')
                .setRecordTypeByName('VTR5_External');
        survey4.persist();
        DomainObjects.VTD1_Survey_Answer_t answer = new DomainObjects.VTD1_Survey_Answer_t()
                .addVTD1_Question(new DomainObjects.VTD1_Protocol_ePro_Question_t()
                        .addVTD1_Protocol_ePRO(new DomainObjects.VTD1_Protocol_ePRO_t()
                                .setRecordTypeByName('SatisfactionSurvey')
                                .addVTD1_Study(study)
                                .setVTD1_Type('PSC')
                                .setVTR2_Protocol_Reviewer('Patient Guide')
                                .setVTD1_Subject('Patient')
                                .setVTD1_Response_Window(10)))
                .addVTD1_Survey(survey);
        answer.persist();

        DomainObjects.VTD1_Survey_Answer_t answer1 = new DomainObjects.VTD1_Survey_Answer_t()
                .addVTD1_Question(new DomainObjects.VTD1_Protocol_ePro_Question_t()
                        .addVTD1_Protocol_ePRO(new DomainObjects.VTD1_Protocol_ePRO_t()
                                .addVTD1_Study(study)
                                .setVTR2_Protocol_Reviewer('Patient Guide')
                                .setVTD1_Subject('Patient')
                                .setVTD1_Response_Window(10)))
                                .setVT_R5_External_Widget_Data_Key('Unwell?')
                                .setVTD1_Answer('YES')
                .addVTD1_Survey(survey2);
        answer.persist();
        
        //Added by :Conquerors, Vijendra Hire SH-20622	
        //START
        DomainObjects.Study_t study1 = new DomainObjects.Study_t()
            	.setOriginalName('Test Study')
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));
        DomainObjects.User_t scruser2 = new DomainObjects.User_t()
                .setProfile('Site Coordinator')
                .setEmail('testscr@email.com')
                .setUsername('test1052020_2@email.com')
                .addContact(new DomainObjects.Contact_t());
            piuser.persist();        
        DomainObjects.StudyTeamMember_t stm2 = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('SCR')
                .addUser(scruser2)
                .addStudy(study1);
                stm1.persist();
        DomainObjects.VirtualSite_t virtSite3 = new DomainObjects.VirtualSite_t()
                .addStudy(study1)
                .setStudySiteNumber('12345')
                .addStudyTeamMember(stm2);
            virtSite3.persist();
        Case cas3 = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .setSubject('EDiaryAllTest')
                .addStudy(study1)
                .addUser(user)
                .addVirtualSite(virtSite3)
                .persist();
        DomainObjects.VTD1_Protocol_ePRO_t protocolED = new DomainObjects.VTD1_Protocol_ePRO_t()
            	.setName('Test PD1')
                .setRecordTypeByName('SatisfactionSurvey')
                .addVTD1_Study(study)
                .setVTR2_Protocol_Reviewer('Site Coordinator')
                .setVTD1_Subject('Patient')
                .setVTD1_Response_Window(1);
        system.debug('protocolED ---> '+ protocolED);
        //END

        
    }
	//Added by :Conquerors, Vijendra Hire SH-20622	
    //START
	@IsTest
    static void VT_R5_EDiariesTableControllerEDiariesTotalScoreDataTest() {
         DoNotDeployTestClass.getEDiariesTotalScoreDataTest(); 
    } 
    //END
    
    
    
    
    
    
    
    
    
    
    
    /*
    @IsTest
    static void VT_R5_eDiarySiteStaffAlertsTest() {
        VT_R5_eCoaAlertsTest.doSiteStaffAlertsTest();
    }

    @IsTest
    static void VT_R5_eDiarySiteStaffAlertsMissedTest() {
        VT_R5_eCoaAlertsTest.doSiteStaffAlertsMissedTest();
    }

    @IsTest
    static void VT_R5_eDiaryPtCgAlertsTest() {
        VT_R5_eCoaAlertsTest.doPtCgAlertsTest();
    }

    @IsTest
    static void VT_R5_DiaryReviewingModalControllerTest1() {
        VT_R5_DiaryReviewingModalControllerTest.getDataTest();
    }

    @IsTest
    static void VT_R5_DiaryReviewingModalControllerTest2() {
        VT_R5_DiaryReviewingModalControllerTest.updateScoringTest();
    }
    @IsTest
    static void VT_R5_EDiariesTableControllerTest() {
        VT_R5_EDiariesTableControllerTest.getDataTest();
    }
    @IsTest
    static void VT_R5_EDiariesVirtualSiteDataTestMethod() {
        VT_R5_ShareGroupMembershipService.disableMembership = true;
        VT_R5_EDiariesTableControllerTest.getEDiariesVirtualSiteDataTest();
    }
    @IsTest
    static void VT_R5_EDiariesEdiariesNameDataTestMethod() {
        VT_R5_ShareGroupMembershipService.disableMembership = true;
        VT_R5_EDiariesTableControllerTest.getEDiariesNameDataTest();
    }
    @IsTest

    static void VT_R5_EDiariesTableControllerUpdateEdiaryTest() {
        VT_R5_EDiariesTableControllerTest.updateEdiaryTest();
    }
    @IsTest
    static void VT_R5_EDiariesTableControllerUpdateEdiaryNegativeTest() {
        VT_R5_EDiariesTableControllerTest.updateEdiaryNegativeTest();
    }
    @IsTest
    static void VT_R5_EDiariesTableControllerPrefiltersTest() {
        VT_R5_EDiariesTableControllerTest.prefiltersTest();
    }
	

    //Added by :Conquerors, Priyanka Ambre SH-21178	
    //START
    @IsTest
    static void VT_R5_EDiariesTableControllerEdiaryFilterPITest() {
       // VT_R5_EDiariesTableControllerTest.ediaryFilterForPITest();
    }
    /*
    @IsTest
    static void VT_R5_EDiariesTableControllerEdiaryFilterSCRTest() {
        VT_R5_EDiariesTableControllerTest.ediaryFilterForSCRTest();
    }

     @IsTest
    static void ediaryFilterForSCRNoAnswersTest() {
        VT_R5_EDiariesTableControllerTest.ediaryFilterForPINoAnswersTest();
    }
    //END
    @IsTest
	*/
    /*
    static void VT_R2_PIMyPatientEDiaryItemContrlTest1() {
        VT_R2_PIMyPatientEDiaryItemContrlTest.getCurrentUserTest();
    }
    @IsTest
    static void VT_R2_PIMyPatientEDiaryItemContrlTest2() {
        VT_R2_PIMyPatientEDiaryItemContrlTest.getQuestionsAnswersTest();
    }
    @IsTest
    static void VT_R2_PIMyPatientEDiaryItemContrlTest3() {
        VT_R2_PIMyPatientEDiaryItemContrlTest.flagSurveyAsPossibleConcernTest();
    }
    @IsTest
    static void VT_R2_PIMyPatientEDiaryItemContrlTest4() {
        VT_R2_PIMyPatientEDiaryItemContrlTest.updateSurveyStatusTest();
    }
    @IsTest
    static void VT_R2_PIMyPatientEDiaryItemContrlTest5() {
        VT_R2_PIMyPatientEDiaryItemContrlTest.testMissedReason();
    }
    @IsTest
    static void VT_R2_EDiaryStudyHubControllerTest1() {
        VT_R2_EDiaryStudyHubControllerTest.getDataTest();
    }
    @IsTest
    static void VT_R2_EDiaryStudyHubControllerTest2() {
        VT_R2_EDiaryStudyHubControllerTest.updateScoreTest();
    }
    @IsTest
    static void VT_R2_EDiaryStudyHubControllerTest3() {
        VT_R2_EDiaryStudyHubControllerTest.markReviewedTest();
    }
    @IsTest
    static void VT_R2_SCPatientEDiaryTest1() {
        VT_R2_SCPatientEDiaryTest.getSurveysTest();
    }
    @IsTest
    static void VT_R2_SCPatientEDiaryTest2() {
        VT_R2_SCPatientEDiaryTest.getQuestionsAnswersTest();
    }
    @IsTest
    static void VT_R2_SCPatientEDiaryTest3() {
        VT_R2_SCPatientEDiaryTest.saveAnswersTest();
    }
    @IsTest
    static void VT_R2_SCPatientEDiaryTest4() {
        VT_R2_SCPatientEDiaryTest.updateDateTimeTest();
    }
    @IsTest
    static void VT_R2_PIMyPatientEDiaryControllerGetSurveysTest() {
        VT_R2_PIMyPatientEDiaryControllerTest.getSurveysTest();
    }
    @IsTest
    static void VT_R2_PIMyPatientEDiaryControllerUpdateSurveyTest() {
        VT_R2_PIMyPatientEDiaryControllerTest.updateSurveyTest();
    }
    @IsTest
    static void VTR2_SCRPatientEDiaryTest1() {
        VTR2_SCRPatientEDiaryTest.getSurveysTest();
    }
    @IsTest
    static void VTR2_SCRPatientEDiaryTest2() {
        VTR2_SCRPatientEDiaryTest.getQuestionsAnswersTest();
    }
    @IsTest
    static void VTR2_SCRPatientEDiaryTest3() {
        VTR2_SCRPatientEDiaryTest.saveAnswersTest();
    }
    @IsTest
    static void VTR2_SCRPatientEDiaryTest4() {
        VTR2_SCRPatientEDiaryTest.updateDateTimeTest();
    }
    @IsTest
    static void VTR2_SCRPatientEDiaryTest5() {
        VTR2_SCRPatientEDiaryTest.updateSurveyStatusTest();
    }
    @IsTest
    static void VT_R5_DeletedRecordHandlerTest() {
        VT_R5_DeletedRecordHandlerTest.createDeletedRecordsSurveyTest();
    }
*/
}