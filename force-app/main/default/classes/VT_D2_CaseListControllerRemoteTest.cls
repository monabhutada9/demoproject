/**
 * Created by shume on 21.01.2019.
 */

@IsTest
private class VT_D2_CaseListControllerRemoteTest {
    @testSetup
    private static void setupMethod() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(11);
        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(11);
        Test.stopTest();
    }

    @IsTest
    static void testBehavior() {
        User patient = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Patient' AND ContactId != null AND IsActive = true LIMIT 1];
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c ORDER BY CreatedDate DESC LIMIT 1];
        Test.startTest();
        List<Case> caseList = new List<Case>();

        List<HealthCloudGA__CandidatePatient__c> HCP = [SELECT Id, HealthCloudGA__AccountId__c, HealthCloudGA__Address1Country__c FROM HealthCloudGA__CandidatePatient__c];

        for(Integer i = 0; i < 11; i++) {
            Case caseObj = new Case (
                AccountId = HCP[0].HealthCloudGA__AccountId__c,
                Status = 'Open',
                VTD1_PCF_Safety_Concern_Indicator__c = 'No',
                ContactId = patient.ContactId,
                VTD1_Subject_ID__c = '1111',
                //VTD1_Study__c = study.Id,
                RecordTypeId = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN
            );

            caseList.add(caseObj);
        }
        insert caseList;
        Test.stopTest();

        List<Case> cases = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];
        system.debug('1111=' + cases.size());
        for (Case item : cases) {
            item.VTD1_Subject_ID__c = '1111';
        }
        update cases;

        VT_D2_CaseListControllerRemote.getCases();
    }
}