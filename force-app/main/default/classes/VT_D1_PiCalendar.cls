public with sharing class VT_D1_PiCalendar {
	public class DateTimeWrapper {
		@AuraEnabled public String dt { get; set; }
		@AuraEnabled public VTD1_Actual_Visit__c visit { get; set; }
		@AuraEnabled public List <Visit_Member__c> visitMembers { get; set; }
	}
	public Id currentUserId;
	public List<Id> myStudyIds;
	public Map<Id, VTD1_Actual_Visit__c> actualVisitMap;
	public Map<Id, List<Visit_Member__c>> visitToMembersMap;
	public Map<Id, VTD1_Monitoring_Visit__c> monitoringVisitMap;
	public List<Event> eventList;
	public VT_D1_PiCalendar() {
		this(UserInfo.getUserId(), getMyStudyIds());
	}
	public VT_D1_PiCalendar(Id userId, List<Id> studyIds) {
		currentUserId = userId;
		myStudyIds = studyIds;
	}
	@AuraEnabled(Cacheable=true)
	public static List<Id> getMyStudyIds() {
		return VT_D1_PIStudySwitcherRemote.getMyStudyIds();
	}
	@AuraEnabled(Cacheable=true)
	public static String getUserInfo() {
		try {
			List<User> user = [SELECT VTD1_StartDay__c, VTD1_EndOfDay__c, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
			return JSON.serialize(user[0]);
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
		}
	}


	@AuraEnabled(Cacheable=true)

	public static List<CalendarObj> getEvents() {
		List<CalendarObj> calendarRecords = new List<CalendarObj>();
		VT_D1_PiCalendar c = new VT_D1_PiCalendar();
		c.actualVisitMap = c.getActualVisitMap();
		if (c.actualVisitMap != null) {
			c.visitToMembersMap = c.getVisitToMembersMap();
			c.monitoringVisitMap = c.getMonitoringVisitMap();
			c.eventList = c.getEventList();
			if (c.eventList != null) {
				calendarRecords = c.processEventList();
			}
		}
		return calendarRecords;
	}
	public Map<Id, VTD1_Actual_Visit__c> getActualVisitMap() {
		Id userId = currentUserId!=null? currentUserId : UserInfo.getUserId();
		List<Id> userIds = new List<Id>{userId};
		String currentUserProfile = VT_D1_HelperClass.getUserProfileName(userId);
		if (currentUserProfile == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) {
			userIds = getPrimaryInvestigatorIds();
		}
		if (userIds.isEmpty() || myStudyIds==null) {
			return null;
		}
		Map<Id, VTD1_Actual_Visit__c> actualVisitMap = new Map<Id, VTD1_Actual_Visit__c>([
				SELECT
						Id,
						Name,
						VTD1_Scheduled_Date_Time__c,
						VTD1_Scheduled_Visit_End_Date_Time__c,
						VTD1_Visit_Description__c,
						VTD1_Pre_Visit_Instructions__c,
						VTD1_Visit_Label__c,
						VTD1_Onboarding_Type__c,
						Sub_Type__c,
						VTD1_Status__c,
						toLabel(VTD1_Status__c) statusLabel,
						VTD1_Reason_for_Cancellation__c,
						VTD1_Date_Time_Stamp__c,
						//VTD1_Protocol_Visit__r.VTD1_Procedures__c,
						VTD1_Protocol_Visit__r.VTD1_VisitNumber__c,
						VTD1_Protocol_Visit__r.VTD1_Required_EDC_CRF_Form_ID__c,
						VTD1_Case__r.ContactId,
						VTD1_Case__r.Contact.Name,
						VTD1_Case__r.VTD1_Subject_ID__c,
						VTD1_Case__r.VTD1_Study__r.Name,
						VTD1_Case__r.VTD1_Study__c,
						Unscheduled_Visits__r.VTD1_Subject_ID__c,
						Unscheduled_Visits__r.ContactId,
						Unscheduled_Visits__r.Contact.Name,
						Unscheduled_Visits__r.VTD1_Study__r.Name,
						Unscheduled_Visits__r.VTD1_Study__c,
						RecordType.DeveloperName, Type__c,
						VTD1_Additional_Visit_Checklist__c,
						VTR2_Modality__c,
						VTR2_Loc_Name__c,
						VTR2_Loc_Address__c,
						VTR2_Loc_Phone__c
				FROM VTD1_Actual_Visit__c
				WHERE Id IN (
						SELECT VTD1_Actual_Visit__c
						FROM Visit_Member__c
						WHERE VTD1_Participant_User__c IN :userIds
				)
				AND VTD1_Scheduled_Date_Time__c != NULL
				AND (NOT (VTD1_Protocol_Visit__r.VTD1_isArchivedVersion__c = TRUE AND VTD1_Status__c != :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED))
				AND VTD1_Status__c != :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED
				AND ((VTD1_Case__r.VTD1_Study__c IN :myStudyIds AND VTD1_Case__r.Status != 'Dropped')
				OR (Unscheduled_Visits__r.VTD1_Study__c IN :myStudyIds AND Unscheduled_Visits__r.Status != 'Dropped'))
		]);
		System.debug('actualVisitMap (' + actualVisitMap.size() + ')');
		return actualVisitMap;
	}
	public List<Id> getPrimaryInvestigatorIds() {
		Id userId = currentUserId!=null? currentUserId : UserInfo.getUserId();
		List<Id> primaryInvestigatorIds = new List<Id>{userId};
		if (myStudyIds!=null) {
			List<AggregateResult> arList = [
					SELECT Study_Team_Member__r.User__c userId
					FROM Study_Team_Member__c
					WHERE User__c = :userId
					AND VTD1_Type__c = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME
					AND Study_Team_Member__c != NULL
					AND Study__c IN :myStudyIds
					GROUP BY Study_Team_Member__r.User__c
			];
			for (AggregateResult ar : arList) {
				primaryInvestigatorIds.add(String.valueOf(ar.get('userId')));
			}
		}
		System.debug('primaryInvestigatorIds: '+primaryInvestigatorIds);
		return primaryInvestigatorIds;
	}
	public Map<Id, VTD1_Monitoring_Visit__c> getMonitoringVisitMap() {
		if (myStudyIds==null) return null;
		Map<Id, VTD1_Monitoring_Visit__c> monitoringVisitMap = new Map<Id, VTD1_Monitoring_Visit__c>([
				SELECT Id, Name, VTD1_Visit_Start_Date__c, VTD1_Visit_End_Date__c, VTD1_Study__c, VTD1_Study__r.Name, VTD1_Site_Visit_Type__c, VTD1_Study_Site_Visit_Status__c
				FROM VTD1_Monitoring_Visit__c
				WHERE VTD1_Study__c IN :myStudyIds
		]);
		return monitoringVisitMap;
	}
	public Map<Id, List<Visit_Member__c>> getVisitToMembersMap() {
		if (actualVisitMap==null) return null;
		//Set<Id> actualVisitIds = actualVisitMap.keySet();
		List<Visit_Member__c> allMembersList= [
				SELECT VTD1_Actual_Visit__c, Visit_Member_Name__c
				FROM Visit_Member__c
				WHERE VTD1_Actual_Visit__c IN :actualVisitMap.keySet()
				ORDER BY VTD1_Actual_Visit__c ASC
		];
		Map<Id, List<Visit_Member__c>> visitToMembersMap = new Map<Id, List<Visit_Member__c>>();
		for (Visit_Member__c member : allMembersList) {
			if (visitToMembersMap.containsKey(member.VTD1_Actual_Visit__c)) {
				List<Visit_Member__c> visitMembersList = visitToMembersMap.get(member.VTD1_Actual_Visit__c);
				visitMembersList.add(member);
				visitToMembersMap.put(member.VTD1_Actual_Visit__c, visitMembersList);
			} else {
				visitToMembersMap.put(member.VTD1_Actual_Visit__c, new List<Visit_Member__c>{member});
			}
		}
		return visitToMembersMap;
	}
	public List<Event> getEventList() {
		if (currentUserId==null || actualVisitMap==null) return null;
		Set<Id> actualVisitIds = actualVisitMap.keySet();
		List<Event> eventList = [
				SELECT Id, Subject, Description, StartDateTime, EndDateTime, Record_Type__c, WhatId, VTD1_Actual_Visit__c, Visit_Type__c, VTD1_IsBufferEvent__c, VTD2_Sub_PI_Event__c, VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Subject_ID__c,VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Subject_ID__c
				FROM Event
				WHERE OwnerId = :currentUserId
				AND (VTD1_Actual_Visit__c = NULL OR VTD1_Actual_Visit__c IN :actualVisitIds)
				ORDER BY StartDateTime
		];
		System.debug('eventList ('+eventList.size() + ')');
		return eventList;
	}

	public static Boolean isGeneralVisit(Id sObjectId) {
		return sObjectId != null && sObjectId.getSobjectType().getDescribe().getName() == 'VTR4_General_Visit__c';
	}

	public List<CalendarObj> processEventList() {
		List<CalendarObj> calendarRecords = new List<CalendarObj>();
		if (eventList==null) return calendarRecords;

		Set <Id> generalVisitIds = new Set<Id>();

		for (Event ev : eventList) {
			if (isGeneralVisit(ev.WhatId)) {
				generalVisitIds.add(ev.WhatId);
			}
		}

		Map<Id, List<VTR4_General_Visit_Member__c>> generalVisitToMembersMap = new Map<Id, List<VTR4_General_Visit_Member__c>>();
		List <VTR4_General_Visit_Member__c> generalVisitMembers =
			[select Id, VTR4_General_Visit__c, VTR4_User_Name__c from VTR4_General_Visit_Member__c where VTR4_General_Visit__c in : generalVisitIds];
		for (VTR4_General_Visit_Member__c member : generalVisitMembers) {
			List <VTR4_General_Visit_Member__c> membersByVisit = generalVisitToMembersMap.get(member.VTR4_General_Visit__c);
			if (membersByVisit == null) {
				membersByVisit = new List<VTR4_General_Visit_Member__c>();
				generalVisitToMembersMap.put(member.VTR4_General_Visit__c, membersByVisit);
			}
			membersByVisit.add(member);
		}


		for (Event ev : eventList) {
			VTD1_Actual_Visit__c av = (
					actualVisitMap!=null
							&& ev.VTD1_Actual_Visit__c!=null
							&& actualVisitMap.containsKey(ev.VTD1_Actual_Visit__c)
			)? actualVisitMap.get(ev.VTD1_Actual_Visit__c) : null;
			VTD1_Monitoring_Visit__c monitoringVisit = (
					monitoringVisitMap!=null
							&& monitoringVisitMap.containsKey(ev.WhatId)
			)? monitoringVisitMap.get(ev.WhatId) : null;

			//List<Visit_Member__c> participants = new List<Visit_Member__c>();
			List<SObject> participants = new List<SObject>();
			if (visitToMembersMap!=null && ev.VTD1_Actual_Visit__c!=null && visitToMembersMap.containsKey(ev.VTD1_Actual_Visit__c)) {
				participants = visitToMembersMap.get(ev.VTD1_Actual_Visit__c);
			}
			if (participants == null || participants.isEmpty()) {
				List <VTR4_General_Visit_Member__c> members = generalVisitToMembersMap.get(ev.WhatId);
				if (members != null) {
					participants = members;
				}
			}
			Case avCase = new Case();
			if (av!=null) {
				avCase = (av.Unscheduled_Visits__r!=null? av.Unscheduled_Visits__r : av.VTD1_Case__r);
			}
			CalendarObj newEv = new CalendarObj(
					(av==null? ev.Id : ev.VTD1_Actual_Visit__c),
					String.valueOf(av==null? ev.Subject : av.Name),
					Datetime.valueOf((ev.VTD1_IsBufferEvent__c || av==null)? ev.StartDateTime : av.VTD1_Scheduled_Date_Time__c),
					Datetime.valueOf((ev.VTD1_IsBufferEvent__c || av==null)? ev.EndDateTime : av.VTD1_Scheduled_Visit_End_Date_Time__c),
					String.valueOf(av==null?  ev.Description : av.VTD1_Visit_Description__c),
					String.valueOf(av==null? '' : av.VTD1_Pre_Visit_Instructions__c),
					String.valueOf(av==null? ev.Subject : av.VTD1_Visit_Label__c),
					participants,
					Boolean.valueOf(ev.VTD1_IsBufferEvent__c),
					String.valueOf(monitoringVisit==null? ev.Visit_Type__c : monitoringVisit.VTD1_Site_Visit_Type__c),
					String.valueOf(av==null? (monitoringVisit==null? '' : monitoringVisit.VTD1_Study_Site_Visit_Status__c) : av.VTD1_Status__c),
					String.valueOf(av==null? '' : av.VTR2_Modality__c),
					String.valueOf(av==null? '' : av.VTD1_Onboarding_Type__c=='N/A'? av.Sub_Type__c : av.VTD1_Onboarding_Type__c),
					'', //String.valueOf(av==null? '' : av.VTD1_Protocol_Visit__r.VTD1_Procedures__c),
					String.valueOf(av==null? '' : av.VTD1_Protocol_Visit__r.VTD1_VisitNumber__c),
					String.valueOf(av==null? '' : av.VTD1_Protocol_Visit__r.VTD1_Required_EDC_CRF_Form_ID__c),
					String.valueOf(av==null? (monitoringVisit==null? '' : monitoringVisit.VTD1_Study__c ) : (avCase==null? '' : avCase.VTD1_Study__c)),
					String.valueOf(av==null? (monitoringVisit==null? '' : monitoringVisit.VTD1_Study__r.Name ) : (avCase==null? '' : avCase.VTD1_Study__r.Name)),
					String.valueOf(av==null? ev.Record_Type__c : av.RecordType.DeveloperName),
					Boolean.valueOf(monitoringVisitMap.containsKey(ev.WhatId)),
					String.valueOf(av==null? '' : av.Type__c),
					String.valueOf(av==null? '' : (avCase==null? '' : avCase.Contact.Name)),
					String.valueOf(av==null? '' : (avCase==null? '' : avCase.VTD1_Subject_ID__c)),
					String.valueOf(av==null? '' : av.VTD1_Additional_Visit_Checklist__c),
					String.valueOf(av==null? '' : av.VTR2_Loc_Name__c),
					String.valueOf(av==null? '' : av.VTR2_Loc_Address__c),
					String.valueOf(av==null? '' : av.VTR2_Loc_Phone__c),
					Boolean.valueOf(generalVisitIds.contains(ev.WhatId))
			);
			calendarRecords.add(newEv);
		}
		return calendarRecords;
	}
	public class CalendarObj {
		@AuraEnabled public String Id {get;set;}
		@AuraEnabled public String name {get;set;}
		@AuraEnabled public Datetime startDateTime {get;set;}
		@AuraEnabled public Datetime endDateTime {get;set;}
		@AuraEnabled public String description {get;set;}
		@AuraEnabled public String instructions {get;set;}
		@AuraEnabled public String label {get;set;}
		@AuraEnabled public List<Visit_Member__c> participants {get;set;}
		@AuraEnabled public Boolean isBuffer {get;set;}
		@AuraEnabled public String visitType {get;set;}
		@AuraEnabled public String status {get;set;}
		@AuraEnabled public String modality {get;set;}
		@AuraEnabled public String category {get;set;}
		@AuraEnabled public String requiredProcedures {get;set;}
		@AuraEnabled public String visitNumber {get;set;}
		@AuraEnabled public String crf {get;set;}
		@AuraEnabled public String study {get;set;}
		@AuraEnabled public String studyName {get;set;}
		@AuraEnabled public String recordType {get;set;}
		@AuraEnabled public Boolean isMonitoring {get;set;}
		@AuraEnabled public String type {get;set;}
		@AuraEnabled public String contactName {get;set;}
		@AuraEnabled public String contactId {get;set;}
		@AuraEnabled public String visitChecklist {get;set;}
		@AuraEnabled public String location {get;set;}
		@AuraEnabled public String address {get;set;}
		@AuraEnabled public String phone {get;set;}
		@AuraEnabled public Boolean isGeneralVisit {get;set;}

		public CalendarObj(String i, String t, Datetime s, Datetime e, String d, String o, String l, List<Visit_Member__c> p,
				Boolean b, String vt, String sta, String m, String c, String rp, String n, String crf, String stu,
				String stn, String rt, Boolean im, String tp, String cn, String cnid, String vcl, String loc, String addr, String phone, Boolean ig) {
			this.Id = i;
			this.name = t;
			this.startDateTime = s;
			this.endDateTime = e;
			this.description = d;
			this.instructions = o;
			this.label = l;
			this.participants = p;
			this.isBuffer = b;
			this.visitType = vt;
			this.status = sta;
			this.modality = m;
			this.category = c;
			this.requiredProcedures = rp;
			this.visitNumber = n;
			this.crf = crf;
			this.study = stu;
			this.studyName = stn;
			this.recordType = rt;
			this.isMonitoring = im;
			this.type = tp;
			this.contactName = cn;
			this.contactId = cnid;
			this.visitChecklist = vcl;
			this.location = loc;
			this.address = addr;
			this.phone = phone;
			this.isGeneralVisit = ig;
		}
	}
	@AuraEnabled
	public static void cancelVisitRemote(Id visitID, String reason) {
		VTD1_Actual_Visit__c vt = new VTD1_Actual_Visit__c();
		vt.Id = visitID;
		vt.VTD1_Reason_for_Cancellation__c = reason;
		vt.VTD1_Date_Time_Stamp__c = Datetime.now();
		try {
			update vt;
		} catch (Exception e) {
			System.debug('Error while cancelling visit');
			throw new DmlException();
		}
	}


	@AuraEnabled(Cacheable=true)

	public static String getDatesTimes(Id visitId) {
		DateTimeWrapper wrapper = new DateTimeWrapper();
		VT_D1_ActualVisitsHelper helper = new VT_D1_ActualVisitsHelper();
		wrapper.dt = helper.getAvailableDateTimes(visitId);
		wrapper.visit = VT_D1_ActualVisitsHelper.getActualVisitInfo(visitId);
		wrapper.visitMembers = VT_D1_ActualVisitsHelper.getVisitMembersList(visitId);
		return JSON.serialize(wrapper);
		//return new VT_D1_ActualVisitsHelper().getAvailableDateTimes(visitId);
	}
	@AuraEnabled
	public static void updateActualVisit(Id actualVisitId, String dateTimeFrom,
			String dateTimeTo, String reasonForReschedule) {
		VT_D1_ActualVisitsHelper.updateActualVisit(actualVisitId, dateTimeFrom, dateTimeTo, reasonForReschedule);
	}
	@AuraEnabled
	public static EventObj upsertEvents(String sEventObj, String sObjectName, String titleField, String startDateTimeField, String endDateTimeField, String descriptionField, String userField){
		EventObj upsertingEvent = (EventObj)JSON.deserialize(sEventObj, EventObj.class);
		SObject newRecord = Schema.getGlobalDescribe().get(sObjectName).newSObject();
		if (upsertingEvent.Id != null && VT_Utilities.isAccessible(sObjectName,'Id')){
			newRecord.put('Id', upsertingEvent.Id);
		}
		if (VT_Utilities.isAccessible(sObjectName,titleField)) {
			newRecord.put(titleField, upsertingEvent.title);
		}
		if (VT_Utilities.isAccessible(sObjectName,startDateTimeField)) {
			newRecord.put(startDateTimeField, upsertingEvent.startDateTime);
		}
		if (VT_Utilities.isAccessible(sObjectName,endDateTimeField)) {
			newRecord.put(endDateTimeField, upsertingEvent.endDateTime);
		}
		if (VT_Utilities.isAccessible(sObjectName,descriptionField)) {
			newRecord.put(descriptionField, upsertingEvent.description);
		}
		if (VT_Utilities.isAccessible(sObjectName,'RecordTypeId')) {
			Id eventRecTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('OutOfOffice').getRecordTypeId();
			if (eventRecTypeId != null) newRecord.put('RecordTypeId', eventRecTypeId);
		}
		try {
			upsert newRecord;
			EventObj newEv = new EventObj(newRecord.Id,
					String.valueOf(newRecord.get(titleField)),
					Datetime.valueOf(newRecord.get(startDateTimeField)),
					Datetime.valueOf(newRecord.get(endDateTimeField)),
					String.valueOf(newRecord.get(descriptionField)),
					String.valueOf(newRecord.get(userField))
			);
			return newEv;
		} catch (Exception e) {
			System.debug(e.getMessage());
			return null;
		}
	}
	@AuraEnabled
	public static String deleteEvent(String eventId){
		Id eventRecTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('OutOfOffice').getRecordTypeId();
		Event eventObj = [SELECT Id, Subject, Description, StartDateTime, EndDateTime, OwnerId
		FROM Event	WHERE RecordTypeId = :eventRecTypeId AND OwnerId = :UserInfo.getUserId() AND Id = :eventId	];
		try {
			//if (isDeletable('Event')) {
			delete eventObj;
			//}
			return eventId;
		} catch (Exception e) {
			System.debug(e.getMessage());
			return null;
		}
	}
	public class EventObj {
		@AuraEnabled
		public String Id {get;set;}
		@AuraEnabled
		public String title {get;set;}
		@AuraEnabled
		public Datetime startDateTime {get;set;}
		@AuraEnabled
		public Datetime endDateTime {get;set;}
		@AuraEnabled
		public String description {get;set;}
		@AuraEnabled
		public String owner {get;set;}
		public EventObj(String i,String t, Datetime s, Datetime e, String d, String o){
			this.Id = i;
			this.title = t;
			this.startDateTime = s;
			this.endDateTime = e;
			this.description = d;
			this.owner = o;
		}
	}
}