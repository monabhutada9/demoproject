/**
* @author: Carl Judge, Ruslan Mullayanov
* @date: 11-Sep-18
* @description:
**/

@IsTest
private class VT_D1_CommunityTimelineController_Test {
    private static User piUser;
    private static User ptUser;
    private static User scrUser;
    private static Case cas;

    @TestSetup private static void setupMethod() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();

        VTD1_ProtocolVisit__c pVisit = new VTD1_ProtocolVisit__c(VTD1_Study__c = study.Id,VTR2_Visit_Participants__c = 'Patient; PI');
        insert pVisit;

        Case cas = [SELECT Id, VTD1_Patient_User__c, VTD1_PI_user__c, VTR2_SiteCoordinator__c, VTR5_Day_1_Dose__c, VTR5_Day_57_Dose__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        cas.VTR5_Day_1_Dose__c = Date.today();
        cas.VTR5_Day_57_Dose__c = Date.today() + 1;
        update cas;
        insert new VTD1_Order__c(VTD1_Case__c = cas.Id, VTD1_Status__c = 'Pending Shipment', VTD1_Expected_Shipment_Date__c = Date.today(), VTD1_Delivery_Order__c = 1);
        insert new VTD1_Case_Status_History__c(VTD1_Case__c = cas.Id, VTD1_Is_Active__c = true);

        List<VTD1_Actual_Visit__c> visits = new List<VTD1_Actual_Visit__c>{
                new VTD1_Actual_Visit__c(VTD1_Case__c = cas.Id, VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED, VTD1_Protocol_Visit__c = pVisit.Id),
                new VTD1_Actual_Visit__c(VTD1_Case__c = cas.Id, VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_REQUESTED, VTD1_Protocol_Visit__c = pVisit.Id),
                new VTD1_Actual_Visit__c(VTD1_Case__c = cas.Id, VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED, VTD1_Protocol_Visit__c = pVisit.Id),
                new VTD1_Actual_Visit__c(VTD1_Case__c = cas.Id, VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED, VTD1_Protocol_Visit__c = pVisit.Id)
        };
        System.runAs(new User(Id = cas.VTD1_Patient_User__c)) {
            insert visits;
        }

        List<Id> userIds = new List<Id>{
                cas.VTD1_PI_user__c,
                cas.VTD1_Patient_User__c,
                cas.VTR2_SiteCoordinator__c
        };
        List<Visit_Member__c> members = new List<Visit_Member__c>();
        for (VTD1_Actual_Visit__c visit : visits) {
            for (Id usrId : userIds) {
                members.add(new Visit_Member__c(
                        VTD1_Actual_Visit__c = visit.Id,
                        VTD1_Participant_User__c = usrId
                ));
            }
        }
        insert members;
    }

    static {
        List<Case> casesList = [
                SELECT Id, VTD1_PI_user__c, VTD1_Patient_User__c, VTR2_SiteCoordinator__c, VTD1_Study__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
                LIMIT 1
        ];
        if (casesList.size() > 0) {
            cas = casesList.get(0);
            List<Id> userIds = new List<Id>{
                    cas.VTD1_PI_user__c,
                    cas.VTD1_Patient_User__c,
                    cas.VTR2_SiteCoordinator__c
            };
            Map<Id, User> usersMap = new Map<Id, User>(
            [SELECT Id, Name FROM User WHERE Id IN :userIds]
            );
            piUser = usersMap.get(cas.VTD1_PI_user__c);
            ptUser = usersMap.get(cas.VTD1_Patient_User__c);
            scrUser = usersMap.get(cas.VTR2_SiteCoordinator__c);
        }
    }

    @IsTest
    private static void patientTest() {
        Test.startTest();
        System.runAs(ptUser) {
            System.assertNotEquals(null, VT_D1_CommunityTimelineController.getCase(cas.Id));
            System.assertNotEquals(0, VT_D1_CommunityTimelineController.getTimeLineItems(cas.Id).size());
            System.assertNotEquals(0, VT_D1_CommunityTimelineController.getTimeLineItems(null).size());
        }
        Test.stopTest();
    }

    @IsTest
    private static void piTest(){
        Test.startTest();
        System.runAs(piUser) {
            System.assertNotEquals(null, VT_D1_CommunityTimelineController.getCase(cas.Id));
            System.assertNotEquals(0, VT_D1_CommunityTimelineController.getTimeLineItems(cas.Id).size());
            System.assertEquals(0, VT_D1_CommunityTimelineController.getTimeLineItems(null).size());
        }
        Test.stopTest();
    }

    @IsTest
    private static void scrTest(){
        Test.startTest();
        System.runAs(scrUser) {
        VT_D1_CommunityTimelineController.ScrPatientDocumentsSelector selector = new VT_D1_CommunityTimelineController.ScrPatientDocumentsSelector();
        System.assertNotEquals(null, selector.getCase(cas.Id));
        System.assertNotEquals(0, selector.getTimeLineItems(cas.Id).size());
        System.assertEquals(0, selector.getTimeLineItems(null).size());
        }
        Test.stopTest();
    }

}