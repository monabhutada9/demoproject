/**
 * Created by user on 6/18/2020.
 * Modified by Priyanka Ambre, SH-17441
 */

public with sharing class VT_R5_ChangeCRAOnMonitoringVisit_Batch implements Database.Batchable<sObject>, Database.Stateful  {
    public Database.QueryLocator start(Database.BatchableContext bc) {
        //Priyanka Ambre SH-17441:Removed VTR2_Remote_Onsite__c, VTR5_VisitCRAUser__c fields
        return Database.getQueryLocator(
                'SELECT VTR5_PrimarySCRUser__c ' +
                'FROM VTD1_Monitoring_Visit__c ' +
                'WHERE VTR5_PrimarySCRUser__c = NULL'
        );
    }
        public void execute(Database.BatchableContext bc, List<VTD1_Monitoring_Visit__c> scope){
            //START:Priyanka Ambre SH-17441:Removed the reference of VTR2_Remote_Onsite__c field

            Map<Id, VTD1_Monitoring_Visit__c> craAndSrcUsers = new Map<Id, VTD1_Monitoring_Visit__c>([
                    SELECT VTD1_Virtual_Site__r.VTR2_Primary_SCR__c
                    FROM VTD1_Monitoring_Visit__c
                    WHERE Id IN:scope
            ]);

            List<VTD1_Monitoring_Visit__c> monitoringVisitsToUpdate = new List<VTD1_Monitoring_Visit__c>();
            for (VTD1_Monitoring_Visit__c mv : scope) {
                mv.VTR5_PrimarySCRUser__c = craAndSrcUsers.get(mv.id).VTD1_Virtual_Site__r.VTR2_Primary_SCR__c;
                monitoringVisitsToUpdate.add(mv);
            }
            //END

            update monitoringVisitsToUpdate;
        }
    public void finish(Database.BatchableContext bc) {

    }

}