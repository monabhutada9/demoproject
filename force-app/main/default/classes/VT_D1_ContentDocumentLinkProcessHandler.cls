public with sharing class VT_D1_ContentDocumentLinkProcessHandler extends Handler {
    public static final VTD1_RTId__c RT_IDS = VTD1_RTId__c.getInstance();
    public static final Set<Id> PCF_RT_IDS = new Set<Id>{
            VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF_READONLY,
            VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF
    };

    protected override void onBeforeInsert(Handler.TriggerContext context) {
        shareVirtualSiteFilesToCustomers(context);
        validateOnlyOneContentDocumentOnDocument(context.newList); //VTD1_Document
    }

    protected override void onAfterInsert(Handler.TriggerContext context) {
        VT_D1_DocumentCHandlerWithoutSharing.clearDocumentDeletionReasonWhenFileInserted(context);
    }

//     public static void createMVRContainers(List<ContentDocumentLink> newContentDocList, Map<Id, ContentDocumentLink> newContentDocMap) {
//         Set<Id> statusSet = new Set<Id>();
//         List<Id> linkIdsToDelete = new List<Id>();
//         Map<Id, Id> mvrMap = new Map<Id, Id>();
//         List <ContentDocumentLink> cdltoCopy = new List<ContentDocumentLink>();
//         List<ContentDocumentLink> cdltoCreate = new List<ContentDocumentLink>();
//         for (ContentDocumentLink cdl : newContentDocList) {
//             if (cdl.LinkedEntityId.getSobjectType() == dsfs__DocuSign_Recipient_Status__c.SObjectType) {
//                 statusSet.add(cdl.LinkedEntityId);
//                 cdltoCopy.add(cdl);
//             }
//         }
//         if (statusSet.size() == 0) {
//             return;
//         }
//         List<dsfs__DocuSign_Recipient_Status__c> recps = [
//                 SELECT Id,
//                         dsfs__Recipient_Status__c,
//                         dsfs__Parent_Status_Record__r.VTD1_Monitoring_Visit__c
//                 FROM dsfs__DocuSign_Recipient_Status__c
//                 WHERE Id in:statusSet
//                 AND dsfs__Parent_Status_Record__r.dsfs__Envelope_Status__c = 'Completed'
//         ];
//         for (dsfs__DocuSign_Recipient_Status__c rep : recps) {
//             mvrMap.put(rep.Id, rep.dsfs__Parent_Status_Record__r.VTD1_Monitoring_Visit__c);
//         }
//         for (ContentDocumentLink cdl : cdltoCopy) {
//             if (mvrMap.get(cdl.LinkedEntityId) == null) {
//                 continue;
//             }
//             ContentDocumentLink cdlCopy = cdl.clone(false, true, false, false);
//             cdlCopy.LinkedEntityId = mvrMap.get(cdl.LinkedEntityId);
//             cdltoCreate.add(cdlCopy);
//             linkIdsToDelete.add(cdl.Id);
//         }
//         if (!cdltoCreate.isEmpty()) {
//             insert cdltoCreate;
//             deleteLinksFuture(linkIdsToDelete);
//         }
//     }

    public static void createContainers(Map<Id, ContentDocumentLink> newContentDocMap) {
        if (!checkIfNeedCreateContainers()) {
            return;
        }
        Map<Id, Id> contentDocId_caseId = new Map<Id, Id>();
        Map<Id, ContentDocumentLink> contentDocId_contentDoc = new Map<Id, ContentDocumentLink>();
        Set<Id> caseIds = new Set<Id>();

        List<ContentDocumentLink> newContentDocList = new List<ContentDocumentLink>();
        for (ContentDocumentLink cdl : newContentDocMap.values()) {
            if (cdl.LinkedEntityId.getSobjectType() == Case.SObjectType) {
                caseIds.add(cdl.LinkedEntityId);
                newContentDocList.add(cdl);
            }
        }
        if (caseIds.isEmpty()) {
            return;
        }
        Map<Id, Case> carePlanCaseMap = new Map<Id, Case>([
                SELECT Id, VTD1_Primary_PG__c, VTD1_PI_user__c
                FROM Case
                WHERE Id IN :caseIds
                AND RecordTypeId = :VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN
        ]);
        for (ContentDocumentLink cdl : newContentDocList) {
            if (carePlanCaseMap.keySet().contains(cdl.LinkedEntityId)) {
                contentDocId_caseId.put(cdl.ContentDocumentId, cdl.LinkedEntityId);
                contentDocId_contentDoc.put(cdl.ContentDocumentId, cdl);
            }
        }
        if (contentDocId_caseId.keySet().isEmpty()) {
            return;
        }
        Map<Id, VTD1_Document__c> contentDocId_doc = new Map<Id, VTD1_Document__c>();
        Id recTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT;
        Map<Id, Boolean> contentDocId_isGuestUpload = new Map<Id, Boolean>();

        for (ContentDocument item : [
                SELECT Id, LatestPublishedVersion.VTD1_Uploaded_by_Guest__c
                FROM ContentDocument
                WHERE Id IN :contentDocId_caseId.keySet()
        ]) {
            contentDocId_isGuestUpload.put(item.Id, item.LatestPublishedVersion.VTD1_Uploaded_by_Guest__c);
        }

        Map<Id, String> docIdUploadedFromCmpMap = getDocIdUploadedFromCmpMap(newContentDocList);
        //List<VTD1_Document__c> docList = new List<VTD1_Document__c>();
        List<ContentDocumentLink> cdlForVTD1DocumentList = new List<ContentDocumentLink>();
        List<Id> linkIdsToDelete = new List<Id>();

        for (Id contentDocIdItem : contentDocId_caseId.keySet()) {
            if (docIdUploadedFromCmpMap.get(contentDocIdItem) != null
                    && docIdUploadedFromCmpMap.get(contentDocIdItem) == 'VT_R3_AddStudyDocuments') {
                continue;
            }
            VTD1_Document__c vtd1_doc = new VTD1_Document__c();
            Id caseId = contentDocId_caseId.get(contentDocIdItem);
            vtd1_doc.VTD1_Clinical_Study_Membership__c = caseId;
            if (carePlanCaseMap.containsKey(caseId)) {
                vtd1_doc.VTD1_PG_Approver__c = carePlanCaseMap.get(caseId).VTD1_Primary_PG__c;
                vtd1_doc.VTD1_PI_Approver__c = carePlanCaseMap.get(caseId).VTD1_PI_user__c;
            }
            vtd1_doc.VTD1_Uploading_Not_Finished__c = contentDocId_isGuestUpload.get(contentDocIdItem);
            if (recTypeId != null) {
                vtd1_doc.RecordTypeId = recTypeId;
            }
            //docList.add(vtd1_doc);
            contentDocId_doc.put(contentDocIdItem, vtd1_doc);
        }
        if (!contentDocId_doc.isEmpty()) {
            insert contentDocId_doc.values();
        }
        for (Id contentDocIdItem : contentDocId_doc.keySet()) {
            ContentDocumentLink cdl = contentDocId_contentDoc.get(contentDocIdItem);
            if(cdl != null){
                ContentDocumentLink cdlCopy = cdl.clone(false, true, false, false);
                cdlCopy.LinkedEntityId = contentDocId_doc.get(contentDocIdItem).Id;
                cdlForVTD1DocumentList.add(cdlCopy);
                linkIdsToDelete.add(cdl.Id);
            }
        }
        if (!cdlForVTD1DocumentList.isEmpty()) {
            insert cdlForVTD1DocumentList;
            deleteLinksFuture(linkIdsToDelete);
        }
    }

    @Future
    public static void deleteLinksFuture(List<Id> linkIds) {
        delete [SELECT Id FROM ContentDocumentLink WHERE Id IN :linkIds];
    }

    /**
     * @param documentLinks List of ContentDocumentLink records from the trigger context
     * @return Map of Document Id and ContentVersion.VTR5_UploadedFrom_fileupload__c values
     * VTR5_UploadedFrom_fileupload__c indicates the source component (aura/lwc), using which the document was uploaded
     */
    public static Map<Id, String> getDocIdUploadedFromCmpMap(List<ContentDocumentLink> documentLinks) {
        Map<Id, String> result = new Map<Id, String>();
        for (ContentDocument document : [
                SELECT Id, (SELECT VTR5_UploadedFrom_fileupload__c FROM ContentVersions ORDER BY CreatedDate DESC LIMIT 1)
                FROM ContentDocument
                WHERE Id IN :getContentDocumentIdSet(documentLinks)
        ]) {
            if (!document.ContentVersions.isEmpty() && document.ContentVersions.get(0).VTR5_UploadedFrom_fileupload__c != null) {
                result.put(document.Id, document.ContentVersions.get(0).VTR5_UploadedFrom_fileupload__c);
            }
        }
        return result;
    }

    public static Set<Id> getContentDocumentIdSet(final List<ContentDocumentLink> documentLinks) {
        Set<Id> result = new Set<Id>();
        for (ContentDocumentLink link : documentLinks) {
            result.add(link.ContentDocumentId);
        }
        return result;
    }

    public static List<VTD1_Document__c> getDocContainersList(List<ContentDocumentLink> deletedList) {
        Set<Id> vtd1DocIds = new Set<Id>();
        for (ContentDocumentLink cdl : deletedList) {
            if (cdl.LinkedEntityId.getSobjectType() == VTD1_Document__c.SObjectType) {
                vtd1DocIds.add(cdl.LinkedEntityId);
            }
        }
        if (vtd1DocIds.isEmpty()) return null;
        List<VTD1_Document__c> docContainersList = [
                SELECT Id,
                        VTD1_FileNames__c,
                        VTD1_Version__c,
                        VTD1_Lifecycle_State__c,
                        VTD1_Signature_Date__c,
                        VTD1_Status__c,
                        VTD1_Deletion_Date__c,
                        VTD1_Deletion_User__c,
                        VTR4_DeletionFileName__c,
                        VTR4_DeletionUserName__c
                FROM VTD1_Document__c
                WHERE Id IN:vtd1DocIds
        ];
        return docContainersList;
    }

    public static void setPCFFilesSharing(List<ContentDocumentLink> contentDocumentLinks) {
        Set<Id> caseIds = new Set<Id>();
        Set<ContentDocumentLink> pcfCdls = new Set<ContentDocumentLink>();
        for (ContentDocumentLink cdl : contentDocumentLinks) {
            if (cdl.LinkedEntityId.getSobjectType() == Case.SObjectType) {
                caseIds.add(cdl.LinkedEntityId);
                pcfCdls.add(cdl);
            }
        }
        if (caseIds.isEmpty()) {
            return;
        }
        Map<Id, Case> patientContactFormMap = new Map<Id, Case>([
                SELECT Id
                FROM Case
                WHERE Id IN :caseIds
                AND RecordTypeId IN :PCF_RT_IDS
        ]);
        if (patientContactFormMap.isEmpty()) {
            return;
        }
        Set<Id> patientContactFormIds = patientContactFormMap.keySet();
        for (ContentDocumentLink cdl : pcfCdls) {
            if (patientContactFormIds.contains(cdl.LinkedEntityId)) {
                cdl.ShareType = 'V';
                cdl.Visibility = 'AllUsers';
            }
        }
    }

    public static Boolean checkIfNeedCreateContainers() {
        //user with profile Patient or Caregiver or user which can upload med records via public site
        //only for them system should create containers when file is uploaded to case record
        if (new Set<Id>{
                RT_IDS.VTD2_Profile_Patient__c,
                RT_IDS.VTD2_Profile_Caregiver__c,
                RT_IDS.VTD2_Profile_SC__c,
                RT_IDS.VTD2_Profile_PG__c,
                RT_IDS.VTD2_Profile_PI__c,
                RT_IDS.VTD2_Profile_SCR__c
        }.contains(UserInfo.getProfileId())) {
            return true;
        }
//        Id ProfileId = UserInfo.getProfileId();
//        Profile prof = [SELECT Name FROM Profile WHERE Id = :ProfileId LIMIT 1];
//        if (prof.Name == 'Study Concierge' || prof.Name == 'Patient Guide' || prof.Name == 'Patient' || prof.Name == 'Caregiver'
//                || prof.Name == 'Site Coordinator'
//                || prof.Name == 'Primary Investigator'
//                ) return true;
        String Username = UserInfo.getUserName();
        VTD1_FileUploadUserCredentials__c creds = VTD1_FileUploadUserCredentials__c.getInstance();
        if (creds.username__c == Username) return true;
        if (Test.isRunningTest()) return true;
        return false;
    }

    public static void setCertificationDocVisibility(List<ContentDocumentLink> docLinks) {
        List<ContentDocumentLink> studyLinks = new List<ContentDocumentLink>();
        List<Id> studyIds = new List<Id>();
        List<Id> docIds = new List<Id>();
        for (ContentDocumentLink cdl : docLinks) {
            if (cdl.LinkedEntityId.getSobjectType() == HealthCloudGA__CarePlanTemplate__c.SObjectType) {
                studyLinks.add(cdl);
                studyIds.add(cdl.LinkedEntityId);
                docIds.add(cdl.ContentDocumentId);
            }
        }
        if (!studyLinks.isEmpty()) {
            Map<Id, HealthCloudGA__CarePlanTemplate__c> studyMap = new Map<Id, HealthCloudGA__CarePlanTemplate__c>([
                    SELECT Id, VTD2_Certification_form_Medical_Records__c
                    FROM HealthCloudGA__CarePlanTemplate__c
                    WHERE Id IN :studyIds
                    AND VTD2_Certification_form_Medical_Records__c != NULL
            ]);
            if (!studyMap.isEmpty() && studyMap != null) {
                Map<Id, ContentDocument> docMap = new Map<Id, ContentDocument>([
                        SELECT Id, Title
                        FROM ContentDocument
                        WHERE Id IN :docIds
                ]);
                for (ContentDocumentLink docLink : studyLinks) {
                    if (studyMap.containsKey(docLink.LinkedEntityId)) {
                        String docTitle = docMap.get(docLink.ContentDocumentId).Title;
                        String studyCertName = studyMap.get(docLink.LinkedEntityId).VTD2_Certification_form_Medical_Records__c;
                        if (String.isNotBlank(studyCertName)) {
                            Pattern pat = Pattern.compile('^' + studyCertName + '(_[a-z]{2}(_[A-Z]{2})?)?$');
                            Matcher mat = pat.matcher(docTitle);
                            if (mat.matches()) {
                                docLink.Visibility = 'AllUsers';
                            }
                        }
                    }
                }
            }
        }
    }

    public static void setUploadDateToDocument(List<ContentDocumentLink> docLinks) {
        Set<VTD1_Document__c> documentToUpdateList = new Set<VTD1_Document__c>();
        Datetime currentDatetime = Datetime.now();
        for (ContentDocumentLink docLink : docLinks) {
            if (docLink.LinkedEntityId.getSobjectType() == VTD1_Document__c.SObjectType) {
                documentToUpdateList.add(new VTD1_Document__c(Id = docLink.LinkedEntityId, VTR5_LastFileUploadedDate__c = currentDatetime));
            }
        }
        update new List<VTD1_Document__c>(documentToUpdateList);
    }

    public static void recalculateLastFileUploadedDate(List<ContentDocumentLink> contentDocumentLinkList) {
        Set<Id> documentsIds = new Set<Id>();
        for (ContentDocumentLink currentContentDocumentLink : contentDocumentLinkList) {
            if (currentContentDocumentLink.LinkedEntityId.getSobjectType() == VTD1_Document__c.SObjectType) {
                documentsIds.add(currentContentDocumentLink.LinkedEntityId);
            }
        }
        if (documentsIds.isEmpty()) {
            return;
        }
        List<VTD1_Document__c> listDeletedFileInDocument = [
                SELECT Id,
                        VTR5_LastFileUploadedDate__c, (
                        SELECT LinkedEntityId,
                                ContentDocumentId,
                                ContentDocument.LastModifiedDate
                        FROM ContentDocumentLinks
                        ORDER BY ContentDocument.LastModifiedDate DESC
                        LIMIT 1
                )
                FROM VTD1_Document__c
                WHERE Id IN:documentsIds AND RecordTypeId = :VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_INFORMED_CONSENT_GUIDE
        ];
        for (VTD1_Document__c documentForResetLastFileUploadedDate : listDeletedFileInDocument) {
            if (documentForResetLastFileUploadedDate.ContentDocumentLinks != null && !documentForResetLastFileUploadedDate.ContentDocumentLinks.isEmpty()) {
                documentForResetLastFileUploadedDate.VTR5_LastFileUploadedDate__c = documentForResetLastFileUploadedDate.ContentDocumentLinks[0].ContentDocument.LastModifiedDate;
            } else {
                documentForResetLastFileUploadedDate.VTR5_LastFileUploadedDate__c = null;
            }
        }
        update listDeletedFileInDocument;
    }

    //Uncommented for SH-18740
    public static void blockUploadForSignMonVisitTask(List<ContentDocumentLink> newCDLList) {
        Map<Id, List<ContentDocumentLink>> mvToCDLMap = new Map<Id, List<ContentDocumentLink>>();
        for (ContentDocumentLink newCDL : newCDLList) {
            if (newCDL.LinkedEntityId.getSobjectType() != VTD1_Monitoring_Visit__c.SObjectType) {
                return;
            }
            List<ContentDocumentLink> exactCDLList = mvToCDLMap.get(newCDL.LinkedEntityId);
            if (exactCDLList == null) {
                exactCDLList = new List<ContentDocumentLink>();
            }
            exactCDLList.add(newCDL);
            mvToCDLMap.put(newCDL.LinkedEntityId, exactCDLList);
        }
        if (mvToCDLMap.isEmpty()) {
            return;
        }
        List<VTD1_Monitoring_Visit__c> blockedMVList = [
                SELECT Id,
                        Follow_Up_Letter_Status__c,
                        VTR5_SignMVReportTaskSent__c,
                        VTR5_FollowUpLetterSubmitted__c
                FROM VTD1_Monitoring_Visit__c
                WHERE Id IN :mvToCDLMap.keySet()
        ];
        List<ContentDocumentLink> wrongCDLList = new List<ContentDocumentLink>();
        for (VTD1_Monitoring_Visit__c blockedMV : blockedMVList) {
            if (blockedMV.VTR5_SignMVReportTaskSent__c || blockedMV.VTR5_FollowUpLetterSubmitted__c &&
                    blockedMV.Follow_Up_Letter_Status__c != VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED) {
                wrongCDLList.addAll(mvToCDLMap.get(blockedMV.Id));
            }
        }
        for (ContentDocumentLink wrongCDL : wrongCDLList) {
            wrongCDL.addError('A file already exists for this task.');
        }
    }
    //Uncommented for SH-18740

//     public static void sendPlatformEventToRefreshPage(List<ContentDocumentLink> documentLinks) {
//         List<String> allDocIds = new List<String>();
//         for (ContentDocumentLink contentDocumentLink : documentLinks) {
//             allDocIds.add(contentDocumentLink.LinkedEntityId);
//         }
//         List<ContentDocumentLink> mvDocs = [
//                 SELECT ContentDocumentId, ContentDocument.Title,
//                         LinkedEntityId, ContentDocument.CreatedDate,
//                         ContentDocument.FileExtension, ContentDocument.ContentSize,
//                         ContentDocument.LatestPublishedVersionId
//                 FROM ContentDocumentLink
//                 WHERE LinkedEntityId IN :allDocIds AND LinkedEntity.Type = 'VTD1_Monitoring_Visit__c' AND Id IN :documentLinks
//         ];
//         if (!mvDocs.isEmpty()) {
//             List<VTR5_FilesChange__e> fileUploadEvents = new List<VTR5_FilesChange__e>();
//             for (ContentDocumentLink contentDocumentLink : mvDocs) {
//                 fileUploadEvents.add(
//                         new VTR5_FilesChange__e(
//                                 Id__c = contentDocumentLink.ContentDocumentId,
//                                 LinkedEntityId__c = contentDocumentLink.LinkedEntityId,
//                                 Title__c = contentDocumentLink.ContentDocument.Title,
//                                 LatestPublishedVersionId__c = contentDocumentLink.ContentDocument.LatestPublishedVersionId,
//                                 FileExtension__c = contentDocumentLink.ContentDocument.FileExtension,
//                                 ContentSize__c = contentDocumentLink.ContentDocument.ContentSize,
//                                 CreatedDate__c = contentDocumentLink.ContentDocument.CreatedDate,
//                                 isDeleted__c = false
//                         )
//                 );
//             }
//             System.debug(fileUploadEvents.size());
//             EventBus.publish(fileUploadEvents);
//         }
//     }

    //upload no more than one file related to VTD1_Document
    public static void validateOnlyOneContentDocumentOnDocument(List<ContentDocumentLink> contentDocumentLinks) {
        Set<Id> documentIds = new Set<Id>();
        for (ContentDocumentLink cdl : contentDocumentLinks) {
            if (cdl.LinkedEntityId.getSobjectType() == VTD1_Document__c.SObjectType) {
                documentIds.add(cdl.LinkedEntityId);
            }
        }
        if (!documentIds.isEmpty()) {
            Set<Id> documentIdsWithLinkedContentDocuments = new Set<Id>();
            for (VTD1_Document__c document : [
                    SELECT Id, (SELECT Id FROM ContentDocumentLinks LIMIT 1)
                    FROM VTD1_Document__c
                    WHERE Id IN :documentIds
                    AND VTD1_Regulatory_Binder__c != NULL
            ]) {
                if (!document.ContentDocumentLinks.isEmpty()) {
                    documentIdsWithLinkedContentDocuments.add(document.Id);
                }
            }
            for (ContentDocumentLink contentDocumentLink : contentDocumentLinks) {
                if (documentIdsWithLinkedContentDocuments.contains(contentDocumentLink.LinkedEntityId)) {
                    contentDocumentLink.addError(System.Label.VTR4_ErrorUploadMoreThanOneFile);
                }
            }
        }
    }


    private void shareVirtualSiteFilesToCustomers(Handler.TriggerContext context) {
        for (ContentDocumentLink cdl : (List<ContentDocumentLink>) context.newList) {
            if (cdl.LinkedEntityId.getSobjectType() == Virtual_Site__c.SObjectType) {
                cdl.Visibility = 'AllUsers';
            }
        }
    }

    public static void changeRecordType(List<ContentDocumentLink> newContentDocList) {
        Set<Id> docIds = new Set<Id>();
        for (ContentDocumentLink contentDocLink : newContentDocList) {
            if (contentDocLink.LinkedEntityId.getSobjectType() == VTD1_Document__c.SObjectType) {
                docIds.add(contentDocLink.ContentDocumentId);
            }
        }
        Set<Id> contentVersionIds = new Set<Id>();
        if (!docIds.isEmpty()) {
            for (ContentDocument contentDoc : [SELECT Id, LatestPublishedVersionId FROM ContentDocument WHERE Id IN :docIds]) {
                contentVersionIds.add(contentDoc.LatestPublishedVersionId);
            }
        }
        if (!contentVersionIds.isEmpty()) {
            List<ContentVersion> contentVersions = new List<ContentVersion>();
            for (Id contentVersionId : contentVersionIds) {
                ContentVersion contentVersionToUpdate = new ContentVersion();
                contentVersionToUpdate.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_CONTENT_VERSION_CV;
                contentVersionToUpdate.Id = contentVersionId;
                contentVersions.add(contentVersionToUpdate);
            }
            update contentVersions;
        }
    }

    // Added in R5.5 for epic# 8126 to restrict file upload for Auditor
    public static void validateFileOperationPermission(List<ContentDocumentLink> contentDocList) {
        String profileName = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        if(profileName == VT_R4_ConstantsHelper_Profiles.AUDITOR_PROFILE_NAME){
            for(ContentDocumentLink cd : contentDocList){
                cd.addError(System.Label.VTR2_NoAccess);
            }
        }
    }
}