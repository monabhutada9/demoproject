/**
 * Created by Andrey Pivovarov on 6/19/2020.
 */
@IsTest
private class VT_R5_CaseProcessHandlerTest {

    private static final String ID_CASE_CAREPLAN = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN;
    private static List<String> pbNames = new List<String>{
            'Create Case Status History', 'Task for the Patient Guide to re-submit the randomization', 'RAND2 Randomization Results',
            'Case - Reschedule Transferred Patient\'s Visits', 'Case -Patient Transfer Date Update', 'When PI contact is changed update PI User',
            'Case - update Termination Reason Description and Termination Reason Description', 'RAND2 Increment Number of Patients',
            'PI Notification when Status = Pre Consent', 'Notify PG when Clinical Study Membership Status Changes',
            'Study Completion Email 3', 'NotificationC when Patient Status is Screen Failure | Washout/Run-In Failure',
            'Community Home UI switch for Patient', 'Notify PI about successful randomization', 'Count Number of Reconciled Patients',
            'Cases - Initiate Tasks', 'Schedule Visit Task & generate Baseline Visit', 'Schedule Visits Task & Notification 2',
            'Start Up - Case 1', 'Update Preferred Lab Visit'
    };

    @TestSetup
    static void setup() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = (HealthCloudGA__CarePlanTemplate__c) new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .setName('testStudy')
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')).persist();
        User universalUser = (User) new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5))
                .setEmail('universalUser@test.com').persist();
        Study_Team_Member__c piMemberT = (Study_Team_Member__c) new DomainObjects.StudyTeamMember_t()
                .setStudy(study.Id)
                .setUserId(universalUser.Id).persist();
        Contact piContact = (Contact) new DomainObjects.Contact_t()
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'))
                .setLastName('piContact')
                .persist();
        study.VTD1_Study_Admin__c = universalUser.Id;
        study.VTD2_Maximum_Rescreening_Attempts__c = 5;
        study.VTD2_Rescreening_Wait_Period__c = 1;
        study.VTD2_Washout_Run_In__c = true;
        study.VTD2_Rescreening_Allowed__c = true;
        study.VTD2_TMA_Eligibility_Review_Required__c = true;
        study.VTD2_TMA_Eligibility_Review_Options__c = 'Countries';
        study.VTD1_Randomized__c = 'No';
        study.VTR5_WhenToCreate_eCoaSubject__c = 'Randomization';
        study.VTR5_eDiaryTool__c = 'eCOA';
        update study;
        Virtual_Site__c vs = new Virtual_Site__c();
        vs.Name = 'vsTest';
        vs.VTD1_Study_Site_Number__c = '12345';
        vs.VTD2_TMA_Review_Required__c = true;
        vs.VTD1_Study__c = study.Id;
        insert vs;
        VTD2_Study_Geography__c sg = new VTD2_Study_Geography__c(
                VTD2_TMA_Review_Required__c = true,
                VTD2_Study__c = study.Id,
                VTD2_Geographical_Region__c = 'Country',
                VTR3_Country__c = 'US'
        );
        insert sg;
        List<Case> cases = new List<Case>();
        for (Integer i = 0; i < pbNames.size(); i++) {
            cases.add(new Case(
                    RecordTypeId = ID_CASE_CAREPLAN,
                    VTD1_Study__c = study.Id,
                    VTD1_PI_user__c = universalUser.Id,
                    VTD2_Study_Geography__c = sg.Id,
                    VTD1_Patient_User__c = universalUser.Id,
                    VTD1_Primary_PG__c = universalUser.Id,
                    Subject = pbNames[i]
            ));
        }
        insert cases;
        Test.stopTest();
    }

    @IsTest
    static void firstTest() {
        VT_R3_GlobalSharing.disableForTest = true;

        Contact ct = [SELECT Id FROM Contact WHERE LastName = 'piContact' LIMIT 1];

        List<Case> updateCases = [

                SELECT Status, RecordType.Name, RecordTypeId, VTD1_PI_user__c, VTD1_Patient_User__r.Id, VTD2_Do_Not_Contact_for_Rescreening__c,
                        VTD2_Rescreening_Attempts__c, VTD2_Geography_Flow_is_Complete__c, VTD1_Virtual_Site__c, VTD2_Baseline_Visit_Complete__c,
                        VTR4_StandaloneBaselineVisitComplete__c, VTD1_Study__c, Subject, VTD2_TMA_Review_Required__c, (
                        SELECT VTD1_Case__c, VTD1_Is_Active__c, VTD1_Status__c
                        FROM Case_Status_Histories__r
                )
                FROM Case
                WHERE RecordType.Name = 'CarePlan'
        ];
        VTD2_Study_Geography__c studyGeography = [SELECT VTD2_TMA_Review_Required__c FROM VTD2_Study_Geography__c LIMIT 1];
        Virtual_Site__c vs = [SELECT Id FROM Virtual_Site__c WHERE VTD1_Study_Site_Number__c = '12345' LIMIT 1];
        for (Case cs : updateCases) {
                cs.VTD1_Enrollment_Date__c = System.now().date();
                cs.VTR3_Dropped_OtherReason_Description__c = 'test is the reason';
                cs.VTD1_Eligibility_Status__c = 'Eligible';
                cs.VTD1_Virtual_Site__c = vs.Id;
                cs.VTD2_Study_Geography__c = studyGeography.Id;
                cs.Status = VT_R4_ConstantsHelper_AccountContactCase.CASE_CONSENTED;
                cs.VTD1_Reconsent_Needed__c = true;
                cs.VTD1_PI_contact__c = ct.Id;
                cs.VTD1_Drug_Reconciliation_Process__c = 'Incomplete';
                cs.VTD1_Reconsent_Refused__c = false;
        }
        Test.startTest();
        update updateCases;
        for (Case cs : updateCases) {
            cs.Status = VT_R4_ConstantsHelper_AccountContactCase.CASE_WASHOUT_RUN_IN;
            cs.VTD2_Baseline_Visit_Complete__c = true;
            if (cs.Subject == 'RAND2 Randomization Results' || cs.Subject == 'Schedule Visit Task & generate Baseline Visit'
                    || cs.Subject == 'Study Completion Email 3' || cs.Subject == 'Notify PI about successful randomization'
                    || cs.Subject == 'Community Home UI switch for Patient') {
                cs.VTR2_WashoutRunInOutcome__c = 'Successful';
            } else {
                cs.VTR2_WashoutRunInOutcome__c = 'Failure';
                cs.VTD1_Enrollment_Date__c = System.now().date();
                cs.VTD1_Reconsent_Needed__c = false;
                cs.VTD1_Drug_Reconciliation_Process__c = 'Complete';
                cs.VTD2_Geography_Flow_is_Complete__c = true;
            }
        }
        update updateCases;
        for (Case cs : updateCases) {
            if (cs.Subject == 'RAND2 Randomization Results' || cs.Subject == 'Schedule Visit Task & generate Baseline Visit'
                    || cs.Subject == 'Study Completion Email 3' || cs.Subject == 'Notify PI about successful randomization'
                    || cs.Subject == 'Community Home UI switch for Patient') {
                cs.Status = VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED;
            } else {
                cs.VTR2_WashoutRunInOutcome__c = 'Failure';
                cs.Status = VT_R4_ConstantsHelper_AccountContactCase.CASE_WASHOUT_RUN_IN_FAILURE;
                // cs.VTD1_Eligibility_Status__c = 'Ineligible';
                cs.VTD2_Failure_Date__c = System.now().date();
            }
        }
        update updateCases;
        Test.stopTest();
        for (Case cs : updateCases) {
            if (cs.Status == VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED) {
                cs.Status = VT_R4_ConstantsHelper_AccountContactCase.CASE_COMPLETED;
            }
        }
       // update updateCases;
        List<VTD1_Actual_Visit__c> actualVisits = [SELECT Name, VTD1_Reason_for_Request__c, VTD1_Unscheduled_Visit_Type__c
                                                    FROM VTD1_Actual_Visit__c
        WHERE VTD1_Unscheduled_Visit_Type__c = 'Screening Results Visit'];
        System.assert(actualVisits.size() > 0);
        List<Case> cases = new List<Case>([
                SELECT Subject, VTD2_TMA_Review_Required__c, Patient_Transfer_Date__c, VTD1_Termination_OtherReason_Description__c,
                        VTR3_Dropped_OtherReason_Description__c, VTD2_Failure_Date__c
                FROM Case
                WHERE Subject IN : pbNames
        ]);
        for (Case cs : cases) {
            if (cs.Subject == 'Case - Reschedule Transferred Patient\'s Visits') {
                System.assertEquals(System.now().date(), cs.Patient_Transfer_Date__c);
            } else if (cs.Subject == 'Case - update Termination Reason Description and Termination Reason Description') {
                System.assertEquals(cs.VTD1_Termination_OtherReason_Description__c, cs.VTR3_Dropped_OtherReason_Description__c);
            }
        }
    }

    @IsTest
    static void testPBStartUpCase() {
        Test.startTest();
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .setName('forStartUpCase')
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));
        VTD1_Document__c documentVTSIF = (VTD1_Document__c) new DomainObjects.VTD1_Document_t()
                .addCase(new DomainObjects.Case_t()
                        .setRecordTypeByName('Sponsor_Contact_Form')
                        .addStudy(study)
                        .setSubject('forVTSIF'))
                .addRegBinder(new DomainObjects.VTD1_Regulatory_Binder_t()
                            .addStudy(study)
                            .addRegDocument(
                                new DomainObjects.VTD1_Regulatory_Document_t()
                                        .setLevel('Study')
                                        .setWayToSendToDocuSign('Manual')
                                        .setDocumentType('VTSIF')
                                        .setSiteRSU(true)))
                .persist();
        VTD1_Document__c documentSAAF = (VTD1_Document__c) new DomainObjects.VTD1_Document_t()
                .addCase(new DomainObjects.Case_t()
                        .setRecordTypeByName('Sponsor_Contact_Form')
                        .addStudy(study)
                        .setSubject('forSAAF'))
                .addRegBinder(new DomainObjects.VTD1_Regulatory_Binder_t()
                            .addStudy(study)
                            .addRegDocument(
                                new DomainObjects.VTD1_Regulatory_Document_t()
                                        .setLevel('Study')
                                        .setWayToSendToDocuSign('Manual')
                                        .setDocumentType('Site Activation Approval Form')
                                        .setSiteRSU(true)))
                .persist();
        List<Case> caseToUpdate = [
                SELECT VTD1_Sponsor_Response_Received__c, Subject
                FROM Case
                WHERE Subject IN ('forVTSIF', 'forSAAF')];
        for (Case cs : caseToUpdate) {
            cs.VTD1_Sponsor_Response_Received__c = true;
            if (cs.Subject == 'forVTSIF') {
                cs.VTD1_Document__c = documentVTSIF.Id;
            }
            else if (cs.Subject == 'forSAAF') {
                cs.VTD1_Document__c = documentSAAF.Id;
            }
        }
        update caseToUpdate;
        Test.stopTest();
    }

    @IsTest
    static void preferredLabVisitTest() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        User universalUser = [SELECT Id FROM User WHERE Email = 'universalUser@test.com' LIMIT 1];
        Case labVisitCase = [SELECT Id FROM Case WHERE Subject = 'Update Preferred Lab Visit' LIMIT 1];

        DomainObjects.VTD1_Actual_Visit_t actualVisit = new DomainObjects.VTD1_Actual_Visit_t()
                .setRecordTypeByName('VTD1_Unscheduled')
                .setVTD1_UnscheduledVisitType('Labs')
                .setVTD1_UnscheduledVisit(labVisitCase.Id)
                .setVTD1_Case(labVisitCase.Id);

        new DomainObjects.Visit_Member_t()
                .addVTD1_Actual_Visit(actualVisit)
                .setVTD1_Participant_UserId(universalUser.Id)
                .persist();
        new DomainObjects.Visit_Member_t()
                .addVTD1_Actual_Visit(actualVisit)
                .setVTD1_External_Participant_Type('HHN')
                .persist();

        labVisitCase.Preferred_Lab_Visit__c = 'Phlebotomist';
        update labVisitCase;
        labVisitCase.Preferred_Lab_Visit__c = 'Home Health Nurse';
        update labVisitCase;
        List<VTD1_Actual_Visit__c> actualVisits = [
                SELECT Sub_Type__c, VTR2_Modality__c, VTD1_Unscheduled_Visit_Type__c, VTR2_SubtypeUpdate__c, RecordTypeId,
                        Unscheduled_Visits__c, VTD1_Case__c
                FROM VTD1_Actual_Visit__c
                WHERE VTD1_Case__c =: labVisitCase.Id
        ];
        for (VTD1_Actual_Visit__c av : actualVisits) {
            System.assertEquals('At Home', av.VTR2_Modality__c);
        }

        Test.stopTest();
    }

    /************************************************************************************
    * @description  This test method cover the scenario of set the status of Actual Visit 
    *               to 'To Be Scheduled' for V1 type visit.
    *               Created for for SH-13732
    * @param        NA
    * @return       void 
    */
    @IsTest
    private static void testV1VisitToBeScheduled() {
        // Get the study and set it to Baseline type
        HealthCloudGA__CarePlanTemplate__c study = [
                SELECT Id,
                        name,
                        VTR5_Baseline_Visit_After_A_R_Flag__c
                FROM HealthCloudGA__CarePlanTemplate__c
                LIMIT 1
        ];

        // for baseline type study
        study.VTR5_Baseline_Visit_After_A_R_Flag__c = true;
        study.VTD1_Randomized__c = 'No';
        study.VTR3_LTFU__c = false;
        study.VTD2_Washout_Run_In__c = false;
        update study;

        // Getting all the cases with the Care Plan record type
        List<Case> cases = [
                SELECT Id
                FROM Case
                WHERE RecordTypeId = :VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN
        ];

        // Create list of Protocol visits with onboarding and protocol record types
        List<VTD1_ProtocolVisit__c> protocolvistList = new List<VTD1_ProtocolVisit__c>();

        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Range__c = 4;
        protocolVisit.VTD1_VisitOffset__c = 2;
        protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisit.RecordTypeId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_PROTOCOL_VISIT_ONBOARDING;
        protocolVisit.VTD1_Onboarding_Type__c = 'Consent';
        protocolVisit.VTD1_VisitType__c = 'Study Team';
        protocolVisit.VTD1_VisitNumber__c = 'o1';
        protocolVisit.VTD1_Study__c = study.Id;
        protocolvistList.add(protocolVisit);

        VTD1_ProtocolVisit__c protocolVisit1 = new VTD1_ProtocolVisit__c();
        protocolVisit1.VTD1_Range__c = 4;
        protocolVisit1.VTD1_VisitOffset__c = 2;
        protocolVisit1.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit1.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit1.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisit1.VTD1_VisitNumber__c = 'v1';
        protocolVisit1.VTD1_VisitType__c = 'Labs';
        protocolVisit1.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit1.RecordTypeId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_PROTOCOL_VISIT_PROTOCOL;
        protocolVisit1.VTD1_Study__c = study.Id;
        protocolvistList.add(protocolVisit1);


        VTD1_ProtocolVisit__c screeningPVisit = new VTD1_ProtocolVisit__c();
        screeningPVisit.VTD1_Range__c = 4;
        screeningPVisit.VTD1_VisitOffset__c = 2;
        screeningPVisit.VTD1_VisitOffsetUnit__c = 'days';
        screeningPVisit.VTD1_VisitDuration__c = '30 minutes';
        screeningPVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        screeningPVisit.RecordTypeId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_PROTOCOL_VISIT_ONBOARDING;
        screeningPVisit.VTD1_Onboarding_Type__c = 'Screening';
        screeningPVisit.VTD1_VisitNumber__c = 'o2';
        screeningPVisit.VTD1_Study__c = study.Id;
        protocolvistList.add(screeningPVisit);
        insert protocolvistList;

        // Create a list Of Actual Visit types with Scheduled and unscheduled type
        List<VTD1_Actual_Visit__c> visitList = new List<VTD1_Actual_Visit__c>();

        VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
        actualVisit.VTD1_Protocol_Visit__c = protocolvistList[0].Id;
        actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
        actualVisit.VTD1_Case__c = cases[0].Id;
        visitList.add(actualVisit);

        VTD1_Actual_Visit__c actualVisitUns = new VTD1_Actual_Visit__c();
        actualVisitUns.VTD1_Protocol_Visit__c = protocolvistList[1].Id;
        actualVisitUns.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT;
        actualVisitUns.RecordTypeId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_ACTUAL_VISIT_LABS;
        actualVisitUns.Sub_Type__c = 'Phlebotomist';
        actualVisitUns.VTD1_Case__c = cases[0].Id;
        visitList.add(actualVisitUns);

        VTD1_Actual_Visit__c actualVisit1 = new VTD1_Actual_Visit__c();
        actualVisit1.VTD1_Protocol_Visit__c = protocolvistList[2].Id;
        actualVisit1.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT;
        actualVisit1.VTD1_Case__c = cases[0].Id;
        visitList.add(actualVisit1);

        Test.startTest();
        insert visitList;

        // Now completed the first onboarding visit to Completed
        VTD1_Actual_Visit__c acv = visitList[0];
        acv.id = visitList[0].Id;
        acv.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED;
        acv.VTD1_Visit_Completion_Date__c = System.today();
        update acv;

        VTD1_Actual_Visit__c updatedActualVisit = [
                SELECT Id,
                        Name,
                        VTD1_Visit_Completion_Date__c,
                        VTD1_Status__c,
                        VTD1_Case__c
                FROM VTD1_Actual_Visit__c
                WHERE Id = :acv.Id
        ];

        Case updatedCase = [
                SELECT Id,
                        VTR5_FirstOnboardingVisitCompleteDate__c,
                        VTR5_LastOnboardingVisitCompleteDate__c,
                        Status
                FROM Case
                WHERE Id = :acv.VTD1_Case__c
        ];

        System.assertEquals(updatedActualVisit.VTD1_Visit_Completion_Date__c,
                updatedCase.VTR5_FirstOnboardingVisitCompleteDate__c,
                'First onboarding Completion field is not null');
        System.assertEquals(null,
                updatedCase.VTR5_LastOnboardingVisitCompleteDate__c,
                'Last onboarding Completion field is null');


        //now update the case status as well from preconsent to Active randomized
        // here need to set the case status
        updatedCase.VTD1_Eligibility_Status__c = 'Eligible';
        updatedCase.Status = 'Consented';
        update updatedCase;

        // This is the last actual visit after completion this last Completion date field will update
        VTD1_Actual_Visit__c acv1 = visitList[2];
        acv1.Id = visitList[2].Id;
        acv.VTD1_Visit_Completion_Date__c = System.today().addYears(-1);
        acv1.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED;
        update acv1;

        VTD1_Actual_Visit__c updatedLastActualVisit = [
                SELECT Id,
                        Name,
                        VTD1_Visit_Completion_Date__c,
                        VTD1_Status__c,
                        VTD1_Case__c
                FROM VTD1_Actual_Visit__c
                WHERE Id = :acv1.Id
        ];

        Case updatedLastCase = [
                SELECT Id,
                        VTR5_FirstOnboardingVisitCompleteDate__c,
                        VTR5_LastOnboardingVisitCompleteDate__c,
                        VTD1_Eligibility_Status__c,
                        VTD1_Screening_Complete__c,
                        status
                FROM Case
                WHERE Id = :acv1.VTD1_Case__c
        ];

        System.assertEquals('Consented',
                updatedLastCase.Status,
                'Case status is consented');
        System.assertEquals('Eligible',
                updatedLastCase.VTD1_Eligibility_Status__c,
                'Case status is eligible');
        System.assertEquals(VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED,
                updatedLastActualVisit.VTD1_Status__c,
                'screened Actual Visit is completed');
        System.assert(updatedLastCase.VTR5_LastOnboardingVisitCompleteDate__c != null,
                'Last onboarding Completion field is not null');

        // now change the case staus to screened so it will automatically changed to 'Active/Randomized'
        updatedLastCase.Status = 'Screened';
        updatedLastCase.VTD1_Screening_Complete__c = true;
        Test.stopTest();
        update updatedLastCase;

        // now get the case with active randomized and eligible true

        Case updatedScreenCase = [
                SELECT Id,
                        VTR5_FirstOnboardingVisitCompleteDate__c,
                        VTR5_LastOnboardingVisitCompleteDate__c,
                        VTD1_Eligibility_Status__c,
                        Status
                FROM Case
                WHERE Id = :acv1.VTD1_Case__c
        ];
        System.assertEquals(VT_R4_ConstantsHelper_Statuses.CASE_ACTIVE_RANDOMIZED,
                updatedScreenCase.Status,
                'Case status is not active/rendomized');

        VTD1_Actual_Visit__c v1ActualVisit = [
                SELECT Id,
                        Name,
                        VTD1_Visit_Completion_Date__c,
                        VTD1_Status__c,
                        VTD1_Case__c,
                        VTD1_Schedule_Visit_Task_Date__c,
                        VTD1_Visit_Type__c,
                        VTD1_Onboarding_Type__c,
                        VTD1_Scheduled_Date_Time__c,
                        VTR3_LTFUStudyVisit__c
                FROM VTD1_Actual_Visit__c
                WHERE Id = :actualVisitUns.Id
        ];

        System.assertEquals(Date.today(),
                v1ActualVisit.VTD1_Schedule_Visit_Task_Date__c,
                'Actual Visit of type v1 task date is not today');
        System.assertEquals(VT_R4_ConstantsHelper_Statuses.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED,
                v1ActualVisit.VTD1_Status__c,
                'Actual Visit status is future visit');
    }

    /************************************************************************************
    * @description  This test method cover the scenario of after set V1 as to be scheduled 
    *               rest visit v2-vn set to be scheduled
    *               Created for for SH-13732
    * @param        NA
    * @return       void 
    */
    @IsTest
    private static void testVisitV2ToVnScheduled() {

        // Get the study and set it to Baseline type
        HealthCloudGA__CarePlanTemplate__c study = [
                SELECT Id,
                        Name,
                        VTR5_Baseline_Visit_After_A_R_Flag__c
                FROM HealthCloudGA__CarePlanTemplate__c
                LIMIT 1
        ];

        // for baseline type study
        study.VTR5_Baseline_Visit_After_A_R_Flag__c = true;
        study.VTD1_Randomized__c = 'No';
        study.VTR3_LTFU__c = false;
        study.VTD2_Washout_Run_In__c = false;
        update study;

        // Getting all the cases with the Care Plan record type
        List<Case> cases = [
                SELECT Id
                FROM Case
                WHERE RecordTypeId = :VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN
        ];

        // Create list of Protocol visits till v2 with onboarding and protocol record types
        List<VTD1_ProtocolVisit__c> protocolvistList = new List<VTD1_ProtocolVisit__c>();

        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Range__c = 4;
        protocolVisit.VTD1_VisitOffset__c = 2;
        protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisit.RecordTypeId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_PROTOCOL_VISIT_ONBOARDING;
        protocolVisit.VTD1_Onboarding_Type__c = 'Consent';
        protocolVisit.VTD1_VisitType__c = 'Study Team';
        protocolVisit.VTD1_VisitNumber__c = 'o1';
        protocolVisit.VTD1_Study__c = study.Id;
        protocolvistList.add(protocolVisit);

        VTD1_ProtocolVisit__c protocolVisit1 = new VTD1_ProtocolVisit__c();
        protocolVisit1.VTD1_Range__c = 4;
        protocolVisit1.VTD1_VisitOffset__c = 2;
        protocolVisit1.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit1.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit1.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisit1.VTD1_VisitNumber__c = 'v1';
        protocolVisit1.VTD1_VisitType__c = 'Labs';
        protocolVisit1.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit1.RecordTypeId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_PROTOCOL_VISIT_PROTOCOL;
        protocolVisit1.VTD1_Study__c = study.Id;
        protocolvistList.add(protocolVisit1);


        VTD1_ProtocolVisit__c screeningPVisit = new VTD1_ProtocolVisit__c();
        screeningPVisit.VTD1_Range__c = 4;
        screeningPVisit.VTD1_VisitOffset__c = 2;
        screeningPVisit.VTD1_VisitOffsetUnit__c = 'days';
        screeningPVisit.VTD1_VisitDuration__c = '30 minutes';
        screeningPVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        screeningPVisit.RecordTypeId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_PROTOCOL_VISIT_ONBOARDING;
        screeningPVisit.VTD1_Onboarding_Type__c = 'Screening';
        screeningPVisit.VTD1_VisitNumber__c = 'o2';
        screeningPVisit.VTD1_Study__c = study.Id;
        protocolvistList.add(screeningPVisit);

        VTD1_ProtocolVisit__c protocolVisit2 = new VTD1_ProtocolVisit__c();
        protocolVisit2.VTD1_Range__c = 4;
        protocolVisit2.VTD1_VisitOffset__c = 2;
        protocolVisit2.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit2.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit2.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisit2.VTD1_VisitNumber__c = 'v2';
        protocolVisit2.VTD1_VisitType__c = 'Labs';
        protocolVisit2.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit2.RecordTypeId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_PROTOCOL_VISIT_PROTOCOL;
        protocolvistList.add(protocolVisit2);
        insert protocolvistList;

        // Create a list Of Actual Visit types with Scheduled and unscheduled type
        List<VTD1_Actual_Visit__c> visitList = new List<VTD1_Actual_Visit__c>();

        VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
        actualVisit.VTD1_Protocol_Visit__c = protocolvistList[0].Id;
        actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
        actualVisit.VTD1_Case__c = cases[0].Id;
        visitList.add(actualVisit);

        VTD1_Actual_Visit__c actualVisitUns = new VTD1_Actual_Visit__c();
        actualVisitUns.VTD1_Protocol_Visit__c = protocolvistList[1].Id;
        actualVisitUns.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT;
        actualVisitUns.RecordTypeId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_ACTUAL_VISIT_LABS;
        actualVisitUns.Sub_Type__c = 'Phlebotomist';
        actualVisitUns.VTD1_Case__c = cases[0].Id;
        visitList.add(actualVisitUns);

        VTD1_Actual_Visit__c actualVisit1 = new VTD1_Actual_Visit__c();
        actualVisit1.VTD1_Protocol_Visit__c = protocolvistList[2].Id;
        actualVisit1.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT;
        actualVisit1.VTD1_Case__c = cases[0].Id;
        visitList.add(actualVisit1);

        VTD1_Actual_Visit__c actualVisitV2 = new VTD1_Actual_Visit__c();
        actualVisitV2.VTD1_Protocol_Visit__c = protocolvistList[3].Id;
        actualVisitV2.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT;
        actualVisitV2.RecordTypeId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_ACTUAL_VISIT_LABS;
        actualVisitV2.Sub_Type__c = 'Home Health Nurse';
        actualVisitV2.VTD1_Case__c = cases[0].Id;
        visitList.add(actualVisitV2);

        insert visitList;


        // Now completed the first onboarding visit to Completed and First Onboarding field will populate
        VTD1_Actual_Visit__c acv = visitList[0];
        acv.Id = visitList[0].Id;
        acv.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED;
        acv.VTD1_Visit_Completion_Date__c = System.today();
        update acv;

        //now update the case status as well from preconsent to Active randomized
        // here need to set the case status
        cases[0].VTD1_Eligibility_Status__c = 'Eligible';
        cases[0].Status = 'Consented';
        update cases[0];
        Test.startTest();
        // This is the last actual visit after completion this last Completion date field will update
        VTD1_Actual_Visit__c acv1 = visitList[2];
        acv1.Id = visitList[2].Id;
        acv.VTD1_Visit_Completion_Date__c = System.today().addYears(-1);
        acv1.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED;
        update acv1;

        // now change the case staus to screened so it will automatically changed to 'Active/Randomized'
        cases[0].Status = 'Screened';
        cases[0].VTD1_Screening_Complete__c = true;
        update cases[0];

        //now update V1 actual visit to complete
        VTD1_Actual_Visit__c acv2 = visitList[1];
        acv2.Id = visitList[1].Id;
        acv2.VTD1_Visit_Completion_Date__c = System.today().addYears(-1);
        acv2.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED;
        acv2.VTR5_Reason_Of_Backdating__c = 'test';
        update acv2;

        Test.stopTest();

        // Enrollment date would get populated. Assert on this.
        Case updatedCase = [
                SELECT Id,
                        VTD1_Enrollment_Date__c
                FROM Case
                WHERE Id = :acv2.VTD1_Case__c
        ];

        System.assert(updatedCase.VTD1_Enrollment_Date__c != null,
                'Enrollment date is null');

        // assert/check the visit target date for v2 is there or not
        VTD1_Actual_Visit__c v2ActualVisitUpdated = [
                SELECT Id,
                        Name,
                        To_Be_Scheduled_Date__c,
                        VTD1_Status__c
                FROM VTD1_Actual_Visit__c
                WHERE Id = :actualVisitV2.Id
        ];

        System.assert(v2ActualVisitUpdated.To_Be_Scheduled_Date__c != null,
                'To be scheduled on actual visit is null');

    }

    @IsTest
    static void executeHerokuPayloadForScheduledActionsTest() {
        Datetime fakeTime = System.now();
        List<VT_R5_GenericScheduledActionsService.ScheduledActionData> scheduledActions = new List<VT_R5_GenericScheduledActionsService.ScheduledActionData>();
        Case caseRecord = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        scheduledActions.add(new VT_R5_GenericScheduledActionsService.ScheduledActionData(
                fakeTime, caseRecord.Id, 'ContactPatientForRescreening')
        );
        new VT_R5_GenericScheduledActionsService().herokuExecute(JSON.serialize(scheduledActions));
    }
}