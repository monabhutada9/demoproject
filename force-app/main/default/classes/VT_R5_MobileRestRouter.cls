/**
 * @author: Alexander Komarov
 * @date: 15.04.2020
 * @description: Technical class-router for mobile HTTP requests
 */
@RestResource(UrlMapping='/patient/*')
global with sharing class VT_R5_MobileRestRouter {
    /**
     * @description the interface allowing a class to be part of the routing
     */
    global interface Routable {
        /**
         * @description returns list of class routes with masks.
         *
         * @return example: '/patient/{version}/ediaries/{id}'
         */
        List<String> getMapping();
        /**
         * @description the implementing class must provide version routing
         *
         * @param parameters extracted from request URI parameters, based on route masks
         * example param map: version => v1, id => SOME_ID
         */
        void versionRoute(Map<String, String> parameters);
        /**
         * @description for better performance, the implementing class must not have static variables pre-initialized with values and initialize it only then needed
         */
        void initService();
    }
    static Map<RequestType, List<Routable>> routes;
    enum RequestType {
        HTTP_GET, HTTP_POST, HTTP_PUT, HTTP_DELETE, HTTP_PATCH
    }
    /**
     * @description the routing registration
     */
    static {
        System.debug('initialization start' + System.Limits.getCpuTime());
        VT_R5_MobileLiveChat liveChatSettings = new VT_R5_MobileLiveChat();
        VT_R5_RestPatientDevices patientDevices = new VT_R5_RestPatientDevices();
        VT_R5_MobileInitial mobileInitial = new VT_R5_MobileInitial();
        VT_R5_MobileResources mobileResources = new VT_R5_MobileResources();
        VT_R5_MobileECoaSSO mobileECoaSSO = new VT_R5_MobileECoaSSO();
        VT_R5_MobileTimezoneUpdater mobileTimezoneUpdater = new VT_R5_MobileTimezoneUpdater();
        VT_R5_MobilePushRegService mobilePushRegService = new VT_R5_MobilePushRegService();
        VT_R5_MobilePatientVideoConference videoConference = new VT_R5_MobilePatientVideoConference();
        VT_R3_RestVisitTimeslots timeslots = new VT_R3_RestVisitTimeslots();
        VT_R5_MobilePatientStudyDocuments studyDocuments = new VT_R5_MobilePatientStudyDocuments();
        VT_R5_MobilePatientStudySupplies studySupplies = new VT_R5_MobilePatientStudySupplies();
        VT_R5_MobilePatientMessaging patientMessaging = new VT_R5_MobilePatientMessaging();
        VT_R5_MobilePatientFiles patientFiles = new VT_R5_MobilePatientFiles();
        VT_R5_MobilePatientPhoneSMS phoneSMS = new VT_R5_MobilePatientPhoneSMS();

        routes = new Map <RequestType, List<Routable>>{
                RequestType.HTTP_GET => new List<Routable>{
                        mobileInitial,
                        patientDevices,
                        mobileResources,
                        liveChatSettings,
                        mobileECoaSSO,
                        mobilePushRegService,
                        timeslots,
                        videoConference,
                        studyDocuments,
                        studySupplies,
                        patientMessaging,
                        patientFiles
                },
                RequestType.HTTP_POST => new List<Routable>{
                        mobileTimezoneUpdater,
                        mobilePushRegService,
                        studyDocuments,
                        studySupplies,
                        patientMessaging,
                        phoneSMS
                },
                RequestType.HTTP_PUT => new List<Routable>(),
                RequestType.HTTP_DELETE => new List<Routable>{
                        mobilePushRegService
                },
                RequestType.HTTP_PATCH => new List<Routable>{
                        patientDevices,
                        studyDocuments
                }
        };
    }
    @HttpGet
    global static void doGET() {
        execute(RequestType.HTTP_GET);
    }
    @HttpPost
    global static void doPOST() {
        execute(RequestType.HTTP_POST);
    }
    @HttpPatch
    global static void doPATCH() {
        execute(RequestType.HTTP_PATCH);
    }
    @HttpPut
    global static void doPUT() {
        execute(RequestType.HTTP_PUT);
    }
    @HttpDelete
    global static void doDELETE() {
        execute(RequestType.HTTP_DELETE);
    }
    //TODO: add param to handle different places of unsupporting
    public static void unsupportedVersionResponse() {
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf('{"todo":"error_unsupported_version"}');
    }
    private static Boolean match(String requestURI, String routableURI) {
        List<String> routableURIList = routableURI.split('/');
        List<String> requestURIList = requestURI.split('/');
        if (routableURIList.size() != requestURIList.size()) {
            return false;
        } else {
            for (Integer i = 0; i < routableURIList.size(); i++) {
                if (!routableURIList.get(i).contains('{') && routableURIList.get(i) != requestURIList.get(i)) {
                    return false;
                }
            }
        }
        System.debug('matched '+requestURI + ' and ' +routableURI);
        return true;
    }
    private static Map<String, String> getParameters(String requestURI, String routableURI) {
        List<String> requestURIList = requestURI.split('/');
        List<String> routableURIList = routableURI.split('/');
        Map<String, String> result = new Map<String, String>();
        for (Integer i = 0; i < routableURIList.size(); i++) {
            if (routableURIList.get(i).contains('{')) {
                result.put(routableURIList.get(i).substring(1, routableURIList.get(i).length()-1), requestURIList.get(i));
            }
        }
        return result;
    }
    private static void execute(RequestType requestType) {
        Boolean routed = false;
        System.debug('before routing' + System.Limits.getCpuTime());
        String requestURI = RestContext.request.requestURI;
        for (Routable r : routes.get(requestType)) {
            for (String mapping : r.getMapping()) {
                if (match(requestURI, mapping)) {
                    System.debug('after routing' + System.Limits.getCpuTime());
                    routed = true;
                    r.versionRoute(getParameters(requestURI, mapping));
                }
            }
        }

        if (!routed) unsupportedVersionResponse();
    }
}