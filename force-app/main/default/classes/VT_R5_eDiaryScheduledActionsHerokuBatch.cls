/**
 * Created by Aleksandr Mazan on 18-Nov-20.
 */

public with sharing class VT_R5_eDiaryScheduledActionsHerokuBatch implements Database.Batchable<VT_R5_eDiaryScheduledActionsHeroku.FlowInput>, Database.AllowsCallouts {

    private String strParameter;

    public VT_R5_eDiaryScheduledActionsHerokuBatch(String strParam) {
        strParameter = strParam;
    }

    public Iterable<VT_R5_eDiaryScheduledActionsHeroku.FlowInput> start(Database.BatchableContext BC) {
        return (List<VT_R5_eDiaryScheduledActionsHeroku.FlowInput>) JSON.deserialize(strParameter, List<VT_R5_eDiaryScheduledActionsHeroku.FlowInput>.class);
    }

    public void execute(Database.BatchableContext BC, List<VT_R5_eDiaryScheduledActionsHeroku.FlowInput> scope) {
        Map<Id, VT_R5_eDiaryScheduledActionsHeroku.FlowInput> alertsById = new Map<Id, VT_R5_eDiaryScheduledActionsHeroku.FlowInput>();
        for (VT_R5_eDiaryScheduledActionsHeroku.FlowInput oldSurvey : new List<VT_R5_eDiaryScheduledActionsHeroku.FlowInput>(scope)) {
            alertsById.put(oldSurvey.surveyId, oldSurvey);
        }
        new VT_R5_eDiaryScheduledActionsHeroku().processAlerts(alertsById);
    }

    public void finish(Database.BatchableContext BC) {
    }
}