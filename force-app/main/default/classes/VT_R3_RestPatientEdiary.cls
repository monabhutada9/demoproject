/**
 * @author: Alexander Komarov
 * @date: 15.05.2019
 * @description: REST Resource for mobile app to get ediary data.
 */

@RestResource(UrlMapping='/Patient/Ediaries/*')
global with sharing class VT_R3_RestPatientEdiary {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();


    private static Map<String, String> STATUS_LABELS_TRANSLATED = new Map<String, String>{
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_NOT_STARTED => Label.VTR4_ReadyToStart,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED => Label.VTR2_Completed,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED => Label.VTR2_Completed,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED_PARTIALLY_COMPLETED => Label.VTR2_Completed
    };

    private static Map<String, String> STATUS_LABELS_TO_API_NAMES = new Map<String, String>{
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_NOT_STARTED => 'VTR4_ReadyToStart',
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED => 'VTR2_Completed',
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED => 'VTR2_Completed',
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED_PARTIALLY_COMPLETED => 'VTR2_Completed'
    };

    @HttpPut
    global static String resetEdiary() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        try {
            String temp = request.requestURI.substringAfterLast('/Ediaries/Reset/');
            Id surveyId = temp;

            VTD1_Survey__c currentSurvey = [SELECT Id, VTD1_Status__c, Name, (SELECT Id, VTD1_Answer__c, VTR4_AnswerTimestamp__c FROM Survey_Answers__r) FROM VTD1_Survey__c WHERE Id = :surveyId LIMIT 1];

            processResetEdiary(currentSurvey);
            SurveyWrapper result = getSingleSurvey(surveyId);
            cr.buildResponse(result);
            return JSON.serialize(cr, true);
        } catch (Exception e) {
            response.statusCode = 500;
            cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName() + '.');
            System.debug('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName() + '.');
            return JSON.serialize(cr, true);
        }
    }

    private static void processResetEdiary(VTD1_Survey__c survey) {
        Boolean gotAtLeastOne = false;
        for (VTD1_Survey_Answer__c sa : survey.Survey_Answers__r) {
            if (String.isNotBlank(sa.VTD1_Answer__c)) {
                sa.VTD1_Answer__c = null;
                sa.VTR2_Is_Answered__c = false;
                gotAtLeastOne = true;
            }
        }
        if (gotAtLeastOne) update survey.Survey_Answers__r;
        survey.VTD1_Status__c = 'Ready to Start';
        update survey;

    }


    @HttpGet
    global static String getEdiaries() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        String temp = request.requestURI.substringAfterLast('/Ediaries/');


        if (String.isBlank(temp)) {
            VT_D1_eProHomeController.SurveysWrapperForPatient surveys = VT_D1_eProHomeController.getSurveysForPatient();
            if (!surveys.surveys.isEmpty()) {
                List<SurveyObj> result = formSurveysList(surveys);
                cr.buildResponse(result);
                return JSON.serialize(cr, true);
            } else {
                response.statusCode = 200;
                cr.buildResponse('{}');
                return JSON.serialize(cr, true);
            }
        } else {
            Id surveyId = temp;
            if (!helper.isIdCorrectType(temp, 'VTD1_Survey__c')) {
                response.statusCode = 400;
                cr.buildResponse(helper.forAnswerForIncorrectInput());
                return JSON.serialize(cr, true);
            }
            try {
//                VTD1_Survey__c currentSurvey = [SELECT Id, VTD1_Protocol_ePRO__r.VTR4_Is_Branching_eDiary__c FROM VTD1_Survey__c WHERE Id = :surveyId LIMIT 1];
                cr.buildResponse(getSingleSurvey(surveyId));
                return JSON.serialize(cr, true);
            } catch (QueryException qe) {
                response.statusCode = 400;
                cr.buildResponse('No surveys for this patient with such ID - ' + surveyId);
                return JSON.serialize(cr, true);
            } catch (Exception e) {
                response.statusCode = 500;
                cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName() + '.');
                return JSON.serialize(cr, true);
            }
        }
    }

    @HttpPost
    global static String postAnswers() {
        RestResponse response = RestContext.response;
        RestRequest request = RestContext.request;

        String temp = request.requestURI.substringAfterLast('/Ediaries/');

        if (!helper.isIdCorrectType(temp, 'VTD1_Survey__c')) {
            response.statusCode = 400;
            cr.buildResponse(helper.forAnswerForIncorrectInput());
            return JSON.serialize(cr, true);
        }
        Id surId = temp;
        String requestBodyString = request.requestBody.toString();

        Set<String> notCompletedStatuses = new Set<String>{
                VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_NOT_STARTED, VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_IN_PROGRESS, VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_READY_TO_START
        };
        VTD1_Survey__c currentSurvey = [SELECT Id, VTD1_Status__c, Name, (SELECT Id, VTD1_Answer__c, VTR4_AnswerTimestamp__c FROM Survey_Answers__r) FROM VTD1_Survey__c WHERE Id = :surId LIMIT 1];
        if (!notCompletedStatuses.contains(currentSurvey.VTD1_Status__c)) {
            response.statusCode = 417;
            cr.buildResponse('FAILURE', 'FAILURE. Survey already completed or missed');
            return JSON.serialize(cr, true);
        }

        List<CustomAnswerWrapper> answersList = (List<CustomAnswerWrapper>) JSON.deserialize(requestBodyString, List<CustomAnswerWrapper>.class);

        Set<String> answersIds = new Set<String>();
        for (CustomAnswerWrapper caw : answersList) {
            answersIds.add(caw.answerId);
        }
        System.debug('answersIds *** ' + answersIds);
        for (VTD1_Survey_Answer__c answer : [SELECT Id, VTD1_Survey__c FROM VTD1_Survey_Answer__c WHERE Id IN :answersIds]) {
            if (!answer.VTD1_Survey__c.equals(temp)) {
                response.statusCode = 409;
                cr.buildResponse('FAILURE', 'FAILURE. Some answers not belong to this survey.');
                return JSON.serialize(cr, true);
            }
        }
        answersList = processTimestampsSecondVersionEspeciallyForCopado(currentSurvey, answersList);

        String answersForController = JSON.serialize(answersList);

        Map<String, String> paramsMap = request.params;
        if (!paramsMap.containsKey('complete')) {
            response.statusCode = 452;
            cr.buildResponse('FAILURE. "complete" parameter is missing.');
            return JSON.serialize(cr, true);
        }
        Boolean complete = Boolean.valueOf(paramsMap.get('complete'));

        VT_D1_eProHomeController.SavedResultsWrapper resultsWrapper = VT_D1_eProHomeController.saveAnswers(answersForController, surId, complete);

        SurveyWrapper result = getSingleSurvey(surId);

        if (complete) {
            cr.serviceMessage = System.Label.VTR3_SomethingSubmitted.replace('#1', currentSurvey.Name);
        }
//        else {
//            cr.serviceMessage = System.Label.VT_D2_SavedSuccess;
//        }
        response.statusCode = 200;
        cr.buildResponse(result);
        return JSON.serialize(cr, true);
    }

    private static List<CustomAnswerWrapper> processTimestampsSecondVersionEspeciallyForCopado(VTD1_Survey__c survey, List<CustomAnswerWrapper> answersList) {
        List<CustomAnswerWrapper> result = new List<VT_R3_RestPatientEdiary.CustomAnswerWrapper>();
        Map<Id, Long> answerIdsToTimestamp = new Map<Id, Long>();
        for (VTD1_Survey_Answer__c answer : survey.Survey_Answers__r) {
            answerIdsToTimestamp.put(answer.Id, answer.VTR4_AnswerTimestamp__c == null ? 0L : answer.VTR4_AnswerTimestamp__c.getTime());
        }
        for (CustomAnswerWrapper caw : answersList) {
            if (answerIdsToTimestamp.containsKey(caw.answerId)) {
                Long savedTimestamp = answerIdsToTimestamp.get(caw.answerId);
                System.debug('saved - ' + savedTimestamp + ' caw.ts ' + caw);
                if (savedTimestamp < caw.timestamp) {
                    System.debug('approved - ' + caw);
                    result.add(caw);
                    continue;
                }
            }
            System.debug('rejected - ' + caw);
        }
        return result;
    }


    private static SurveyWrapper getSingleSurvey(Id surveyId) {
        SurveyWrapper result = new SurveyWrapper();
        VTD1_Survey__c survey = [
                SELECT VTR2_DocLanguage__c, VTD1_Status__c, VTD1_Protocol_ePRO__c, VTD1_Protocol_ePRO__r.VTD1_Subject__c, VTD1_Start_Time__c
                FROM VTD1_Survey__c
                WHERE Id = :surveyId
                LIMIT 1
        ];
        String protocolId = survey.VTD1_Protocol_ePRO__c;
        String status = survey.VTD1_Status__c;
        String ediaryLanguage = survey.VTR2_DocLanguage__c;
        if (survey.VTD1_Start_Time__c == null) {
            VT_D1_eProHomeController.updateDateTime(surveyId, status);
        }
        VT_D1_eProHomeController.QAWrapper qawrapper = VT_D1_eProHomeController.getQuestionsAnswersForPTCG(surveyId, protocolId, survey.VTD1_Status__c);
        System.debug('HERE' + qawrapper);
        result.surveyId = surveyId;
        if (STATUS_LABELS_TO_API_NAMES.containsKey(status)) {
            result.surveyStatus = VT_D1_TranslateHelper.getLabelValue(STATUS_LABELS_TO_API_NAMES.get(status), 'en_US');
        } else {
            result.surveyStatus = status;
        }
        result.sQA = formQuestions(qawrapper);
        result.targetSubject = survey.VTD1_Protocol_ePRO__r.VTD1_Subject__c;
        result.instructions = getInstructions(protocolId, ediaryLanguage);
        result.lblForSurvey = qawrapper.lblsForSurvey;
        return result;
    }

    private static List<Instruction> getInstructions(Id protocolId, String ediaryLanguage) {
        List<Instruction> instructions = new List<Instruction>();

        List<VTD1_Protocol_ePRO__c> protocols = [SELECT Id, (SELECT VTR2_Content__c, VTR2_Group_number__c FROM Protocol_eDiary_Instructions__r) FROM VTD1_Protocol_ePRO__c WHERE Id = :protocolId];
        if (protocols.isEmpty()) {
            return instructions;
        }

        List<VTR2_eDiaryInstructions__c> listToProcess = new List<VTR2_eDiaryInstructions__c>();
        listToProcess = protocols[0].Protocol_eDiary_Instructions__r;

        User currentUser = [
                SELECT Contact.VTR2_Doc_Language__c
                FROM User
                WHERE Id = :UserInfo.getUserId()
        ];
        String language = currentUser.Contact.VTR2_Doc_Language__c;

        if (ediaryLanguage != null) {
            language = ediaryLanguage;
        }
        VT_D1_TranslateHelper.translate(listToProcess, language);

        for (VTR2_eDiaryInstructions__c instruction : listToProcess) {
            Instruction i = new Instruction();
            i.content = instruction.VTR2_Content__c;
            i.groupNumber = instruction.VTR2_Group_number__c;
            instructions.add(i);
        }
        System.debug('into getting instuctions' + ' ' + ediaryLanguage + ' ' + protocolId + ' ' + listToProcess + ' ' + instructions);
        return instructions;

    }

    private static List<SurveyQuestionAnswers> formQuestions(VT_D1_eProHomeController.QAWrapper qawrapper) {
        List<SurveyQuestionAnswers> result = new List<VT_R3_RestPatientEdiary.SurveyQuestionAnswers>();
        for (VTD1_Protocol_ePro_Question__c q : qawrapper.qas) {
            System.debug('HEREEE' + q);
            SurveyQuestionAnswers sqa = new SurveyQuestionAnswers();
            sqa.questionId = q.Id;
            sqa.type = q.VTD1_Type__c;
            sqa.content = q.VTR2_QuestionContentLong__c;
            sqa.options = String.isBlank(q.VTD1_Options__c) ? null : q.VTD1_Options__c;
            sqa.answerId = q.Survey_Answers__r[0].Id;
            sqa.answer = String.isBlank(q.Survey_Answers__r[0].VTD1_Answer__c) ? null : q.Survey_Answers__r[0].VTD1_Answer__c;
            sqa.groupNumber = Integer.valueOf(q.VTD1_Group_number__c);
            sqa.ordinalNumber = q.VTR4_Number__c != null ? Integer.valueOf(q.VTR4_Number__c.split('\\.')[0]) : null;
            result.add(sqa);
        }

        return result;
    }

    private static List<SurveyObj> formSurveysList(VT_D1_eProHomeController.SurveysWrapperForPatient surveysWrapperForPatient) {
        List<SurveyObj> result = new List<SurveyObj>();
        for (VT_D1_eProHomeController.SurveyWrapper sw : surveysWrapperForPatient.surveys) {
            Integer answeredCounter = 0;
            for (VTD1_Survey_Answer__c sa : sw.sv.Survey_Answers__r) {
                if (String.isNotBlank(sa.VTD1_Answer__c)) {
                    answeredCounter++;
                }
            }
            SYstem.debug('INCOME SV ' + JSON.serializePretty(sw.sv));
            result.add(
                    new SurveyObj(
                            sw.sv.Id,
                            sw.sv.VTD1_Due_Date__c,
                            sw.sv.Name,
                            sw.sv.VTD1_Status__c,
                            sw.sv.VTD1_Due_Date__c.format('dd-MMM-yyyy'),
                            String.valueOf(sw.sv.get('lblStatus')),
                            sw.sv.Survey_Answers__r.size(),
                            answeredCounter,
                            sw.sv.VTD1_Protocol_ePRO__c,
                            sw.sv.VTD1_Protocol_ePRO__r.VTR4_Is_Branching_eDiary__c
                    )
            );
        }

        return result;
    }

    private class SurveyWrapper {
        public String surveyId;
        public String surveyStatus;
        public String targetSubject;
        public List<Instruction> instructions;
        public List<SurveyQuestionAnswers> sQA;
        public VT_D1_eProHomeController.LabelsForSurvey lblForSurvey;

        public SurveyWrapper() {
        }
    }

    private class SurveyQuestionAnswers {
        public String questionId;
        public String type;
        public String content;
        public String options;
        public String answerId;
        public String answer;
        public Integer ordinalNumber;
        public Integer groupNumber;

        public SurveyQuestionAnswers() {
        }
    }

    private class SurveyObj {
        public String id;
        public String title;
        public String status;
        public String dueDateLabel;
        public Long dueDate;
        public String translatedStatus;
        public Integer countQuestions;
        public Integer countAnsweredQuestions;
        public String protocolId;
        public Boolean containsBranching;

        public SurveyObj(String i, Datetime dueDate, String title, String status, String dueDateLabel, String translatedStatus, Integer countQuestions, Integer countAnsweredQuestions, String protocolId, Boolean containsBranching) {
            System.debug('income survey ' + i + ': ' + status + ' ' + translatedStatus);

            this.id = i;
            this.dueDate = dueDate.getTime() + timeOffset;
            this.title = title;
            if (STATUS_LABELS_TO_API_NAMES.containsKey(status)) {
                this.status = VT_D1_TranslateHelper.getLabelValue(STATUS_LABELS_TO_API_NAMES.get(status), 'en_US');
            } else {
                this.status = status;
            }
            this.dueDateLabel = dueDateLabel;
            if (STATUS_LABELS_TRANSLATED.containsKey(status)) {
                this.translatedStatus = STATUS_LABELS_TRANSLATED.get(status);
            } else {
                this.translatedStatus = translatedStatus;
            }
            this.countQuestions = countQuestions;
            this.countAnsweredQuestions = countAnsweredQuestions;
            this.protocolId = protocolId;
            this.containsBranching = containsBranching;
        }

    }

    public class Instruction {
        public String content;
        public Decimal groupNumber;
    }

    public class CustomAnswerWrapper {
        public String questionId;
        public String answerId;
        public String answer;
        public Long timestamp;
    }

}