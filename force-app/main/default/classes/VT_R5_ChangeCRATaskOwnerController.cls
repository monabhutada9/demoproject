/*************************************************************************************
    @author: Vijendra Hire
    @Story: SH-14821
    @Description: This is the Change CRA Task Owner Controller Class. 
    @Date: 22.10.2020
****************************************************************************************/
public with sharing class VT_R5_ChangeCRATaskOwnerController {
    
/*********************************************************************************************
 	 * Story  : SH-14821
     * Method : changeOwnerMethod
     * param  : Id taskId - Specifies the CRA Tasks Id.
     * description : This mothod is used to Change the CRA Task Owner based passed taskId.
************************************************************************************************/
    @AuraEnabled
    public static String changeOwnerMethod(Id taskId) {
        String result;
        if(taskId != null) {
            if (UserInfo.getProfileId() == VT_D1_HelperClass.getProfileIdByName(VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME)) {
                
                VTR5_CRA_Task__c task = [SELECT Id, OwnerId FROM VTR5_CRA_Task__c WHERE Id = :taskId];
                
                if (task.OwnerId.getSobjectType().getDescribe().getName() == 'User') {
                    result = Label.VTR5_CRATaskIsAlreadyTaken;
                } else{                    
                    task.OwnerId = UserInfo.getUserId();
                    try {
                        if (VT_Utilities.isUpdateable('VTR5_CRA_Task__c')) {
                            update task;
                            result = Label.VTR5_The_TaskIsSuccessfullyAssignedToYou;
                        }
                    } catch (Exception e) {
                        ErrorLogUtility.logException(e, null, VT_R5_ChangeCRATaskOwnerController.class.getName());
                    }
                }
            } else {
                result = Label.VTR5_SorryOnlyCRACanTakeThisTask;
            }
        }
        return result;
    }
}