/**
 * Created by Slav on 07.02.2019.
 */

global class VT_R2_McloudBroadcaster {
	public static String STATUS_FAILURE_1 = 'Failure 1';
	public static String STATUS_FAILURE_2 = 'Failure 2';
	public static String STATUS_SUCCESS = 'Success';
	public static List<VTR2_McloudSettings__mdt> mcloudSettings;

	/**
	 * requestBean class contains request data
	 */
	public class RequestBean {
		private String			clientId;
		private String			clientSecret;
		
		private List<String>	mobileNumbers;
		private String			keyword;
		private Boolean			subscribe;
		private Boolean			resubscribe;
		private String			shortCode;
		private Boolean			override_x;
		private String			messageText;
		private String			sendTime;
	}
	
	/**
	 * ResponseBean class contains response data
	 */
	public class ResponseBean {
		@TestVisible private String					accessToken;
		@TestVisible private Integer				expiresIn;
		@TestVisible private String					message;
		@TestVisible private Integer				errorcode;
		@TestVisible private String					documentation;
		@TestVisible private List<String>			errors;
		@TestVisible private List<InvalidSubscriber> invalidSubscribers;
		@TestVisible private String					tokenId;
	}
	
	/**
	 * InvalidSubscriber class contains information on invalid subscriber
	 */
	public class InvalidSubscriber {
		private String			mobileNumber;
		private String			validationErrorMessage;
	}
	
	/**
	 * ConfigurationException
	 */
	public class ConfigurationException extends Exception {
	}
	
	/**
	 * UnauthorizedException
	 */
	public class AuthorizationException extends Exception {
	}
	
	/**
	 * SendException
	 */
	public class SendException extends Exception {
	}
	
	/******************************************************************************************/
	/*** класс LogHandler предназначен для предварительной загрузки записи логов,			***/
	/*** которые соответствуют номерам телефонов текущего запроса							***/
	/******************************************************************************************/
	public class LogHandler {
		private Map<String, Id> phoneNumbers;
		private String notificationId;
		private Id realNotificationId;
		private List<VTR2_SendSMSLog__c> logList;
		private Map<String, VTR2_SendSMSLog__c> logMap;
		
		/******************************************/
		/*** constructor                        ***/
		/******************************************/
		private LogHandler(Map<String, Id> phoneNumbers, String notificationId, Id realNotificationId) {
			this.phoneNumbers = phoneNumbers;
			this.notificationId = notificationId;
			this.realNotificationId = realNotificationId;
			System.debug('***() + '+notificationId);
			// ==================================================================================
			// выбираем записи VTR2_SendSMSLog__c в соответствии со списком phoneNumbers
			// ==================================================================================
			logList = [SELECT VTR2_PhoneNumber__c, VTR2_Status__c, VTR2_Notification__c FROM VTR2_SendSMSLog__c WHERE VTR2_NotificationId__c = :notificationId AND VTR2_PhoneNumber__c IN :phoneNumbers.keySet()];
			
			// ==================================================================================
			// организуем записи VTR2_SendSMSLog__c в справочник
			// ==================================================================================
			logMap = new Map<String, VTR2_SendSMSLog__c>();
			System.debug('*** ' + logList);
			for (VTR2_SendSMSLog__c log : logList) {
				logMap.put(log.VTR2_PhoneNumber__c, log);
			}
		}
		
		/******************************************************************/
		/*** getLog() returns Log object with specified phone number	***/
		/******************************************************************/
		private VTR2_SendSMSLog__c getLog(String phoneNumber) {
			return logMap.get(phoneNumber);
		}
		
		/******************************************************************/
		/*** setFatalError() sets specified error message for all phone	***/
		/*** numbers included in current request						***/
		/******************************************************************/
		private void setFatalError(String errorMessage, String outboundMessage) {
			for (String phoneNumber : phoneNumbers.keySet()) {
				VTR2_SendSMSLog__c log = logMap.get(phoneNumber);
				if (log == null) {
					log = new VTR2_SendSMSLog__c();
					log.VTR2_PhoneNumber__c = phoneNumber;
					log.VTR2_Contact__c = phoneNumbers.get(phoneNumber);
					log.VTR2_NotificationId__c = notificationId;
					log.VTR2_Notification__c = realNotificationId;
					log.VTR2_Error_Message1__c = errorMessage;
					log.VTR2_Status__c = STATUS_FAILURE_1;
					logMap.put(phoneNumber, log);
				} else {
					log.VTR2_Error_Message2__c = errorMessage;
					log.VTR2_Status__c = STATUS_FAILURE_2;
				}
				
				log.VTR2_MessageText__c = outboundMessage;
			}
		}
		
		/******************************************************************/
		/*** setSendResults() sets send result on each individual phone	***/
		/*** number included in current request. Parameter failedNumbers***/
		/*** includes a list of phone numbers on which delivery failed	***/
		/******************************************************************/
		private void setSendResults(Map<String, String> failedNumbers, String outboundMessage) {
			for (String phoneNumber : phoneNumbers.keySet()) {
				VTR2_SendSMSLog__c log = logMap.get(phoneNumber);
				if (log == null) {
					log = new VTR2_SendSMSLog__c();
					log.VTR2_PhoneNumber__c = phoneNumber;
					log.VTR2_Contact__c = phoneNumbers.get(phoneNumber);
					log.VTR2_NotificationId__c = notificationId;
					log.VTR2_Notification__c = realNotificationId;
					logMap.put(phoneNumber, log);
				}
				
				if (failedNumbers.containsKey(log.VTR2_PhoneNumber__c)) {
					if (String.isBlank(log.VTR2_Status__c)) {
						log.VTR2_Status__c = STATUS_FAILURE_1;
						log.VTR2_Error_Message1__c = failedNumbers.get(log.VTR2_PhoneNumber__c);
					} else if (log.VTR2_Status__c == STATUS_FAILURE_1){
						log.VTR2_Status__c = STATUS_FAILURE_2;
						log.VTR2_Error_Message2__c = failedNumbers.get(log.VTR2_PhoneNumber__c);
					} else {
						log.VTR2_Manual_Sending_Error__c = failedNumbers.get(log.VTR2_PhoneNumber__c);
					}
				} else {
					log.VTR2_Status__c = STATUS_SUCCESS;
					log.VTR2_Error_Message1__c = null;
					log.VTR2_Error_Message2__c = null;
					log.VTR2_Manual_Sending_Error__c = null;
				}
				
				log.VTR2_MessageText__c = outboundMessage;
			}
		}
		
		/******************************************************************/
		/*** updateLog() updates database of VTR2_SendSMSLog__c objects	***/
		/******************************************************************/
		private void updateLog() {
			upsert logMap.values();
		}
	}
	
	/******************************************************************************************************************/
	private final String restURI;
	private final String shortCode;
	private final String msgApiKey;
	private final String sendSMS;
	private final String sendOptIn;
	private final String deliveryCheck;
	
	private String accessToken;				// authorization Access Token
	private String tokenId;					// Token Id is returned if Send was successful
	private String errorMessage;			// MC API request error message
	/******************************************************************************************************************/
	
	/******************************************/
	/*** constructor                        ***/
	/******************************************/
	public VT_R2_McloudBroadcaster () {
		if (mcloudSettings==null) {
			mcloudSettings = [
					SELECT	VTR2_OutboundMessageKey_API_Triggered__c,
							VTR2_RestURI__c,
							VTR2_ShortCode__c
					FROM	VTR2_McloudSettings__mdt];
		}
		
		if (mcloudSettings.isEmpty()) {
			ConfigurationException e = new ConfigurationException();
			e.setMessage('Marketing Cloud settings not found for Org \'' + UserInfo.getOrganizationId() + '\'');
			throw e;
		} else {
			VTR2_McloudSettings__mdt settings = mcloudSettings.get(0);
			restURI			= settings.VTR2_RestURI__c;
			shortCode		= settings.VTR2_ShortCode__c;
			msgApiKey		= settings.VTR2_OutboundMessageKey_API_Triggered__c;
			sendSMS			= 'sms/v1/messageContact/' + msgApiKey + '/send';
			sendOptIn		= 'sms/v1/queueMO';
			deliveryCheck	= 'sms/v1/messageContact/' + msgApiKey + '/deliveries';
		}
	}

	public static void setMCsettings(List<VTR2_McloudSettings__mdt> mcSettings) {
		mcloudSettings = mcSettings;
	}
	
	/**
	 * authorize() logs on Marketing Cloud and obtains an Access Token
	 * returns TRUE if successful, or FALSE otherwise
	 */
	public Boolean authorize () {
		try {
			errorMessage = null;
			
			RequestBean requestBean = new RequestBean();
			requestBean.clientId = '{!$Credential.Username}';		//clientId;
			requestBean.clientSecret = '{!$Credential.Password}';	//clientSecret;

			String body = JSON.serialize(requestBean);
			
			HttpRequest request = new HttpRequest();
			request.setTimeout(60000);
			request.setMethod('POST');
			request.setEndpoint('callout:VTR2_MarketingCloud');
			request.setHeader('Content-Type', 'application/json');
			request.setHeader('Content-Length', '' + body.length());
			request.setBody(body);
			HttpResponse response = new Http().send(request);
			
			if (response.getStatusCode() == 200) {
				ResponseBean responseBean = (ResponseBean) JSON.deserialize(response.getBody(), ResponseBean.class);
				accessToken = responseBean.accessToken;
				return true;
			} else {
				AuthorizationException e = new AuthorizationException();
				e.setMessage('Failed to authorize on Marketing Cloud');
				throw e;
			}
		} catch (Exception e) {
			errorMessage = e.getMessage();
		}
		
		return false;
	}
	

	/**
	 * sendMessage() sends message witch is pre-configured in Marketing Cloud
	 * returns TRUE if successful, or FALSE otherwise
	 */
	public Boolean sendMessage (Map<String, Id> mobileNumbers, String messageText, String notificationId, Id realNotificationId) {

		errorMessage = null;
		mobileNumbers = validateMobileNumbers(mobileNumbers);
		validateMessageText(messageText);
		validateNotificationId(notificationId);
		LogHandler logHandler = new LogHandler(mobileNumbers, notificationId, realNotificationId);

		/*if (true) {
			Map <String, String> failedNumbers = new Map<String, String>();
			for (String phoneNumber : mobileNumbers.keySet()) {
				failedNumbers.put(phoneNumber, 'some error');
			}
			logHandler.setSendResults(failedNumbers, messageText);
			logHandler.updateLog();
			return true;
		}*/
		
		try {
			RequestBean requestBean = new RequestBean();
			requestBean.mobileNumbers = new List<String>();
			requestBean.mobileNumbers.addAll(mobileNumbers.keySet());
			requestBean.subscribe = true;
			requestBean.resubscribe = true;
			requestBean.override_x = true;
			requestBean.messageText = messageText;
			requestBean.keyword = 'JOIN';
			String body = JSON.serialize(requestBean).replaceAll('_x', '');
			
			HttpRequest request = new HttpRequest();
			request.setTimeout(60000);
			request.setMethod('POST');
			request.setEndpoint(restURI + sendSMS);
			request.setHeader('Content-Type', 'application/json');
			request.setHeader('Authorization', 'Bearer ' + accessToken);
			request.setHeader('Content-Length', '' + body.length());
			request.setBody(body);
			HttpResponse response = new Http().send(request);

			if (response.getStatusCode() == 401) {		//
				if (authorize()) {
					//------------------------------------------------------------------//
					// if not authorized then try to authorize and re-send the request	//
					//------------------------------------------------------------------//
					request.setHeader('Authorization', 'Bearer ' + accessToken);
					System.debug('request ' + request.getBody());
					response = new Http().send(request);
					System.debug('response: ' + response);
				} else {
					SendException e = new SendException();
					e.setMessage(response.getStatusCode() + ' ' + response.getStatus());
					throw e;
				}
			}
			
			ResponseBean responseBean = (ResponseBean) JSON.deserialize(response.getBody(), ResponseBean.class);
			System.debug('**ResponseBean '+responseBean);
			if (response.getStatusCode() == 202) {
				//--------------------------------------------------------------------------------------//
				// Send request was accepted, but we have to check if any phone numbers were invalid	//
				//--------------------------------------------------------------------------------------//
				tokenId = responseBean.tokenId;
//				checkDelivery();
				Map<String, String> failedNumbers = new Map<String, String>();
				if (responseBean.invalidSubscribers != null) {
					for (InvalidSubscriber invalidSubscriber : responseBean.invalidSubscribers) {
						failedNumbers.put(invalidSubscriber.mobileNumber, invalidSubscriber.validationErrorMessage);
					}
				}
				
				logHandler.setSendResults(failedNumbers, messageText);
				logHandler.updateLog();
				return true;
			} else {
				SendException e = new SendException();
				String errMsg = 'Errors: ';
				for (String error : responseBean.errors) {
					errMsg += error + '\n';
				}
				e.setMessage(errMsg);
				throw e;
			}
		} catch (Exception e) {
			errorMessage = e.getMessage() + ' ' + e.getStackTraceString();
			logHandler.setFatalError(errorMessage, messageText);
			logHandler.updateLog();
		}
		
		return false;
	}
	
	/**
	 * getErrorMessage() returns error message for the last invoked request
	 * the method return null if last request was successful
	 */
	public String getErrorMessage () {
		return errorMessage;
	}

	
	/**
	 * validateMobileNumbers() validates the list of phone numbers
	 * if it's invalid (null, empty or exceeds 250) the method throws an Exception
	 */
	private Map<String, Id> validateMobileNumbers (Map<String, Id> mobileNumbers) {
		if (mobileNumbers == null || mobileNumbers.isEmpty()) {
			SendException e = new SendException();
			e.setMessage('Mobile numbers not specified');
			throw e;
		}
		
		if (mobileNumbers.size() > 250) {
			SendException e = new SendException();
			e.setMessage('Mobile number list size exceeds 250');
			throw e;
		}
		Map<String, Id> mobileNumbersOnlyDigits = new Map<String, Id>();
		for (String key : mobileNumbers.keySet()) {
			String newKey = key.replaceAll('\\D','');
			if (newKey.length()==10) {
				newKey = '1' + newKey;
			}
			mobileNumbersOnlyDigits.put(newKey, mobileNumbers.get(key));
		}
		return mobileNumbersOnlyDigits;
	}
	
	/**
	 * validateMessageText() validates the message text
	 * if it's null or blank the method throws an Exception
	 */
	private void validateMessageText (String messageText) {
		if (String.isBlank(messageText)) {
			SendException e = new SendException();
			e.setMessage('Message cannot be empty');
			throw e;
		}
	}
	
	/**
	 * validateNotificationId() validates the Notification Id
	 * if it's null or blank the method throws an Exception
	 */
	private void validateNotificationId (String notificationId) {
		if (String.isBlank(notificationId)) {
			SendException e = new SendException();
			e.setMessage('Notification Id cannot be null or blank');
			throw e;
		}
	}

	webservice static String send(Id logSMSId) {
		System.debug('logSMSId = ' + logSMSId);
		VTR2_SendSMSLog__c log = [select Id, VTR2_Status__c, VTR2_PhoneNumber__c, VTR2_Contact__c, VTR2_MessageText__c, VTR2_NotificationId__c, VTR2_Notification__c from VTR2_SendSMSLog__c where Id = : logSMSId];
		if (log.VTR2_Status__c == 'Success') {
			return 'Sending is forbidden for item with success status';
		}

		VT_R2_McloudBroadcaster broadcaster = new VT_R2_McloudBroadcaster();
		try {
			broadcaster.sendMessage(new Map<String, Id>{log.VTR2_PhoneNumber__c => log.VTR2_Contact__c}, log.VTR2_MessageText__c, log.VTR2_NotificationId__c, log.VTR2_Notification__c);
			return 'Message sent';
		} catch (Exception e) {
			return e.getMessage();
		}
	}
}