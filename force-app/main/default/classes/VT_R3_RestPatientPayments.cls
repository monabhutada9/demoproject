/**
 * Created by Alexander Komarov on 06.05.2019.
 */

@RestResource(UrlMapping='/Patient/Payments/*')
global with sharing class VT_R3_RestPatientPayments {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();
    static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());

    @HttpGet
    global static String getPayments() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        String temp = request.requestURI.substringAfterLast('/Payments/');
        if (String.isBlank(temp)) {
            List<PatientPayment> result = new List<PatientPayment>();
            List<VTD1_Patient_Payment__c> paymentsList = new List<VTD1_Patient_Payment__c>();
            try {
                paymentsList = VT_D1_PatientPaymentsControllerRemote.getPatientPayments(null);

                result = formResult(paymentsList);
            } catch (Exception e) {
                response.statusCode = 500;
                cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName());
                return JSON.serialize(cr, true);
            }
            cr.buildResponse(result);
            return JSON.serialize(cr, true);
        } else {
            Id paymentId = temp;

            if (!helper.isIdCorrectType(paymentId, 'VTD1_Patient_Payment__c')) {
                response.statusCode = 400;
                cr.buildResponse(helper.forAnswerForIncorrectInput());
                return JSON.serialize(cr, true);
            }
            PatientPayment result;
            try {
                result = getPaymentDetails(paymentId);
            } catch (QueryException qe) {
                response.statusCode = 404;
                cr.buildResponse('No payments for this patient with such ID - ' + paymentId);
                return JSON.serialize(cr, true);
            } catch (Exception e) {
                response.statusCode = 500;
                cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName());
                return JSON.serialize(cr, true);
            }
            cr.buildResponse(result);
            return JSON.serialize(cr, true);

        }
    }

    @HttpPatch
    global static String completePayment() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        String paymentIdString = request.requestURI.substringAfterLast('/Payments/');

        if (String.isBlank(paymentIdString) || !helper.isIdCorrectType(paymentIdString, 'VTD1_Patient_Payment__c')) {
            response.statusCode = 400;
            cr.buildResponse(helper.forAnswerForIncorrectInput());
            return JSON.serialize(cr, true);
        }
        VTD1_Patient_Payment__c paymentToWork;
        try {
            paymentToWork = [
                    SELECT
                            Id,
                            VTD1_Status__c,
                            VTD1_Amount__c,
                            VTD1_Activity_Complete__c,
                            RecordType.Name,
                            Name,
                            VTD1_Description__c,
                            VTD1_Amount_Formula__c,
                            LastModifiedDate,
                            VTD1_Currency__c,
                            VTD1_Payment_Received__c
                    FROM VTD1_Patient_Payment__c
                    WHERE Id = :paymentIdString
                    LIMIT 1
            ];
            if (paymentToWork == null || !paymentToWork.VTD1_Status__c.equals(System.Label.VTD1_NotStarted)) {
                response.statusCode = 417;
                cr.buildResponse('FAILURE. Payment not in "' + System.Label.VTD1_NotStarted + '" status.');
                return JSON.serialize(cr, true);
            }

            paymentToWork.VTD1_Activity_Complete__c = true;

            update paymentToWork;

        } catch (Exception e) {
            response.statusCode = 500;
            System.debug('here! '+e.getMessage() + e.getStackTraceString());
            cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName());
            return JSON.serialize(cr, true);
        }

        cr.buildResponse(getPaymentDetails(paymentToWork.Id),System.Label.VTR3_CompletePaymentActivity.replace('#1', paymentToWork.Name));
        return JSON.serialize(cr, true);


    }

    private static PatientPayment getPaymentDetails(Id paymentId) {
        VTD1_Patient_Payment__c pp = [
                SELECT
                        Id,
                        Name,
                        VTD1_Description__c,
                        VTD1_Status__c,
                        RecordType.Name,
                        VTD1_Amount_Formula__c,
                        LastModifiedDate,
                        VTD1_Currency__c,
                        VTD1_Payment_Received__c,
                        VTD1_Activity_Complete__c
                FROM VTD1_Patient_Payment__c
                WHERE Id = :paymentId
                LIMIT 1
        ];
        String paymentTypeTranslated = VT_D1_TranslateHelper.getLabelValue('VTD1_Compensation', UserInfo.getLanguage());
        PatientPayment result = new PatientPayment(
                pp.Id,
                paymentTypeTranslated,
                pp.Name,
                pp.VTD1_Description__c,
                pp.VTD1_Status__c,
                pp.LastModifiedDate,
                pp.VTD1_Amount_Formula__c,
                pp.VTD1_Currency__c,
                pp.VTD1_Activity_Complete__c
        );
        return result;
    }

    private static List<PatientPayment> formResult(List<VTD1_Patient_Payment__c> patientPayments) {
        List<PatientPayment> result = new List<PatientPayment>();
        for (VTD1_Patient_Payment__c p : patientPayments) {
            if(!p.RecordType.Name.equalsIgnoreCase('compensation')) continue;
            result.add(new PatientPayment(
                    p.Id,
                    String.valueOf(p.RecordType.get('Label')),
                    p.Name,
                    p.VTD1_Description__c,
                    p.VTD1_Status__c,
                    p.LastModifiedDate,
                    p.VTD1_Amount_Formula__c,
                    p.VTD1_Currency__c,
                    p.VTD1_Activity_Complete__c));
        }

        return result;
    }

    private class PatientPayment {
        public String id;
        public String paymentType;
        public String title;
        public String description;
        public String status;
        public Long dateLastChange;
        public Decimal amount;
        public String paymentCurrency;
        public Boolean paymentActivityComplete;


        public PatientPayment(String i, String type, String t, String d, String s, Datetime dt, Decimal amount, String c, Boolean b) {
            this.id = i;
            this.paymentType = type;
            this.title = t;
            this.description = d;
            this.status = s;
            this.dateLastChange = dt.getTime() + timeOffset;
            this.amount = amount;
            this.paymentCurrency = c;
            this.paymentActivityComplete = b;
        }
    }
}