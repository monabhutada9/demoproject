@IsTest
private class VT_D1_PiFullProfileControllerTest {
    private static Case cas;
    private static User user;

    @TestSetup
    static void setup() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        study.VDT2_Send_Visit_Reminders__c = true;
        study.VTR2_Send_New_Messages__c = true;
        upsert study;

        List<Study_Team_Member__c> stm = [SELECT Id, Study__c, VTD1_PIsAssignedPG__c
        FROM Study_Team_Member__c
        WHERE VTD1_PIsAssignedPG__c != null AND Study__c != null];

        user = [SELECT Id, ContactId, AccountId FROM User WHERE Id = :stm[0].VTD1_PIsAssignedPG__c];
        System.debug('user: ' + user);

        VT_D1_Phone__c phone = new VT_D1_Phone__c();
        phone.VTD1_Contact__c = user.ContactId;
        phone.VTR2_SMS_Opt_In_Status__c = 'Active';
        phone.PhoneNumber__c = '375333366666';
        phone.Account__c = [SELECT 	Id FROM Account LIMIT 1].Id;
        upsert phone;

        Test.startTest();
        //HealthCloudGA__CandidatePatient__c candidatePatient = VT_D1_TestUtils.createPatientCandidate('Mike', 'Birn', 'mikebirn@test.com', 'mikebirn@test.com', 'mikebirn', 'Converted', study.Id, 'Prot-001', '11-11-11', 'US', 'NY');
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }

    static {
        List<Case> casesList = [SELECT VTD1_PI_user__c, VTD1_Study__c FROM Case];
        if (casesList.size() > 0) {
            cas = casesList.get(0);
            String userId = cas.VTD1_PI_user__c;
            user = [SELECT Id, ContactId, TimeZoneSidKey FROM User WHERE Id = :userId];
        }
    }

    @IsTest
    static void updateProfileTest() {
        Test.startTest();
        System.assertNotEquals(null, cas);
        System.assertNotEquals(null, user);

        /*
        User userSysAdmin =  [SELECT ContactId FROM User WHERE Profile.Name = 'System Administrator' AND Id !=: UserInfo.getUserId() AND IsActive = true LIMIT 1];
        System.runAs(userSysAdmin) {
            List<Licenses__c> licenses = new List<Licenses__c>();
            for (Integer i=0; i<3; i++){
                licenses.add(new Licenses__c(Name='TestLicense_'+i, VTD1_Country__c='United States', State__c='CA', Contact__c = u.ContactId, OwnerId = u.Id));
            }
            if (!licenses.isEmpty()) {
                insert licenses;
            }
            System.debug('Licenses created: '+licenses);
        }
        */
        try {
            System.runAs(user) {
                String piFullProfileString = VT_D1_PiFullProfileController.getFullProfile();
                VT_D1_PiFullProfileController.PiFullProfile piFullProfile = (VT_D1_PiFullProfileController.PiFullProfile) JSON.deserialize(piFullProfileString, VT_D1_PiFullProfileController.PiFullProfile.class);

                System.assert(piFullProfile.userTimeZoneSelectRatioElementList.size() > 0);
                //System.assert(piFullProfile.licensesList.size() > 0);

                piFullProfile.contact.VTR2_Primary_Language__c = 'es';
                piFullProfile.user.TimeZoneSidKey = 'America/Chicago';

                VT_D1_PiFullProfileController.ContactPhone contactPhone = new VT_D1_PiFullProfileController.ContactPhone();
                VT_D1_PiFullProfileController.ContactPhone contactPhone2 = new VT_D1_PiFullProfileController.ContactPhone();
//                VT_D1_PiFullProfileController.InputSelectRatioElement contactPhone = new VT_D1_PiFullProfileController.ContactPhone();
                VT_D1_Phone__c phone = [SELECT Id FROM VT_D1_Phone__c LIMIT 1];
                contactPhone.phone = phone;
                contactPhone2.phone = phone;
                contactPhone2.deletePhone = true;
                piFullProfile.contactPhoneList.add(contactPhone);
                piFullProfile.contactPhoneList.add(contactPhone2);

                VT_D1_PiFullProfileController.NewPhone newPhone = new VT_D1_PiFullProfileController.NewPhone();
                newPhone.phoneNumber = '44444';
                newPhone.phoneType = 'Mobile';
                newPhone.primary = true;
                List<VT_D1_PiFullProfileController.NewPhone> newPhoneList = new List<VT_D1_PiFullProfileController.NewPhone>();
                newPhoneList.add(newPhone);
                piFullProfile.newContactPhoneList = newPhoneList;

                String piFullProfileStringUpd = JSON.serialize(piFullProfile);
                VT_D1_PiFullProfileController.updateProfile(piFullProfileStringUpd);

                User user = [SELECT Id, ContactId, TimeZoneSidKey FROM User WHERE Id = :user.Id];
                System.assertEquals('America/Chicago', user.TimeZoneSidKey);

                String piFullProfileString3 = VT_D1_PiFullProfileController.getFullProfile();
                VT_D1_PiFullProfileController.PiFullProfile piFullProfile2 = (VT_D1_PiFullProfileController.PiFullProfile) JSON.deserialize(piFullProfileString3, VT_D1_PiFullProfileController.PiFullProfile.class);
                piFullProfile.contact.VTR2_Primary_Language__c = 'es';
                piFullProfile.user.TimeZoneSidKey = 'America/Chicago';
                VT_D1_PiFullProfileController.ContactPhone contactPhone3 = new VT_D1_PiFullProfileController.ContactPhone();
                VT_D1_PiFullProfileController.ContactPhone contactPhone4 = new VT_D1_PiFullProfileController.ContactPhone();

                VT_D1_Phone__c vtd1PhoneTest = new VT_D1_Phone__c();
                vtd1PhoneTest.PhoneNumber__c = '3333';
                vtd1PhoneTest.Type__c = 'Mobile';
                vtd1PhoneTest.VTD1_Contact__c = user.ContactId ;
                vtd1PhoneTest.Account__c = [
                                            SELECT  Id
                                            FROM Account
                                            LIMIT 1
                                            ].Id;
                insert vtd1PhoneTest;
                VT_D1_Phone__c vtd1PhoneTest2 = new VT_D1_Phone__c();
                vtd1PhoneTest2.PhoneNumber__c = '2222';
                vtd1PhoneTest2.Type__c = 'Mobile';
                vtd1PhoneTest2.VTD1_Contact__c = user.ContactId ;
                vtd1PhoneTest2.Account__c = [
                        SELECT  Id
                        FROM Account
                        LIMIT 1
                ].Id;
                insert vtd1PhoneTest2;
                VT_D1_Phone__c testPhone = [
                                            SELECT  PhoneNumber__c,
                                                    Type__c
                                            FROM	VT_D1_Phone__c
                                            WHERE	PhoneNumber__c = '3333' AND Type__c = 'Mobile'
                                            LIMIT 1
                                            ];
                VT_D1_Phone__c testPhone2 = [
                                            SELECT	PhoneNumber__c,
                                                    Type__c
                                            FROM	VT_D1_Phone__c
                                            WHERE	PhoneNumber__c = '2222' AND Type__c = 'Mobile'
                                            LIMIT 1
                                            ];
                testPhone.PhoneNumber__c = '2222';

                contactPhone3.phone = testPhone;
                contactPhone4.phone = testPhone2;
                piFullProfile2.contactPhoneList.add(contactPhone3);
                piFullProfile2.contactPhoneList.add(contactPhone4);
                System.debug('contact phone list '+ piFullProfile2.contactPhoneList);
                String piFullProfileStringUpd2 = JSON.serialize(piFullProfile2);
                try{
                    System.debug('Inter Vt_d1_PiFulProfileController');
                    VT_D1_PiFullProfileController.updateProfile(piFullProfileStringUpd2);
                }
                catch (Exception e){
                    System.debug('Epx** '+e.getMessage());
                }
                //System.assertEquals('Spanish', contact.HealthCloudGA__PrimaryLanguage__c);
            }
        } catch(Exception ex) {
            System.debug('exception: ' + ex);
        }
        Test.stopTest();
    }
    @isTest
    public static void getProfileInfoTest() {
        Test.startTest();

        System.runAs(user){
            VT_D1_Phone__c phone = new VT_D1_Phone__c();
            phone.VTD1_Contact__c = user.ContactId;
            phone.VTR2_SMS_Opt_In_Status__c = 'Active';
            phone.PhoneNumber__c = '375333366666';
            phone.Account__c = [SELECT 	Id FROM Account LIMIT 1].Id;
            upsert phone;

            String piFullProfileString = VT_D1_PiFullProfileController.getFullProfile();
            VT_D1_PiFullProfileController.PiFullProfile piFullProfile = new VT_D1_PiFullProfileController.PiFullProfile();
            piFullProfile = (VT_D1_PiFullProfileController.PiFullProfile)JSON.deserialize(piFullProfileString, VT_D1_PiFullProfileController.PiFullProfile.class);
            System.assertEquals(true, piFullProfile.smsVisitRemindersAvailable);
            System.assertEquals(true, piFullProfile.smsNewMessagesAvailable);

        }

        User user2 = [SELECT Id FROM User WHERE ContactId = null AND IsActive = true LIMIT 1];
        System.runAs(user2) {
            String piFullProfileString = VT_D1_PiFullProfileController.getFullProfile();
            System.assert(String.isEmpty(piFullProfileString));
        }
        Test.stopTest();
    }

    @isTest
    public static void optTest(){
        setUpMCSettings();
        List<VT_D1_Phone__c> contactPhonesList = [SELECT Id
                    FROM VT_D1_Phone__c
                    WHERE PhoneNumber__c = '375333366666'];
        Test.startTest();
        VT_D1_PiFullProfileController.optInRemote(contactPhonesList[0].Id);
        System.assertEquals(true, [SELECT VTR2_OptInFlag__c FROM VT_D1_Phone__c WHERE Id = :contactPhonesList[0].Id].VTR2_OptInFlag__c);
        VT_D1_PiFullProfileController.optOutRemote(contactPhonesList[0].Id);
        System.assertEquals(false, [SELECT VTR2_OptInFlag__c FROM VT_D1_Phone__c WHERE Id = :contactPhonesList[0].Id].VTR2_OptInFlag__c);
        try{
            VT_D1_PiFullProfileController.sendContactsRemote(contactPhonesList[0].Id);
        }
        catch(Exception e){
            System.assert(e.getMessage().contains('Upsert failed'));
        }
        Boolean exc = false;
        try{
            VT_D1_PiFullProfileController.resendOptInRemote(contactPhonesList[0].Id);
        }
        catch (Exception e){
            exc = true;
        }
        System.assert(exc);
        Test.stopTest();
    }

    @isTest
    public static void ExceptionTest(){
        List<Exception> exceptionList = new List<Exception>();

        try {
            Test.startTest();
            VT_D1_PiFullProfileController.updateContact('');
            Test.stopTest();
        }
        catch (Exception e){
            exceptionList.add(e);
        }
        try {
            VT_D1_PiFullProfileController.updateProfile('');
        }
        catch (Exception e){
            exceptionList.add(e);
        }
        System.assertEquals(2, exceptionList.size());
    }

    static void setUpMCSettings() {
        List<VTR2_McloudSettings__mdt> mcloudSettings = new List<VTR2_McloudSettings__mdt>();
        mcloudSettings.add(new VTR2_McloudSettings__mdt(VTR2_OutboundMessageKey_API_Triggered__c = 'NjU6Nzg6MA',
                VTR2_RestURI__c = 'https://mc2gwv28hdsr86gd-sldnjzvzzgm.rest.marketingcloudapis.com/',
                VTR2_ShortCode__c = '99808'));
        VT_R2_McloudBroadcaster.setMCsettings(mcloudSettings);
    }
}