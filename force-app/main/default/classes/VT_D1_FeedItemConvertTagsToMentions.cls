public without sharing class VT_D1_FeedItemConvertTagsToMentions {

    /* Converts a tag in the body text such as @0053D000000qrsrQAA (probably from process builder) to a real @mention*/

    public static final String USER_PREFIX = SObjectType.User.getKeyPrefix();
    public static final String EXPRESSION = '@(' + USER_PREFIX +'[0-9a-zA-Z]{15})';
    public static Pattern COMPILED_EXPRESSION = Pattern.compile(EXPRESSION);

    private static boolean conversionDone = false;

    public static Set<Id> convert(List<FeedItem> recs) {
        List<FeedItem> toConvert = new List<FeedItem>();
        String s = '@' + USER_PREFIX;
        for (FeedItem item : recs) {
            System.debug('item before if ' + item);
            if (item.Body != null && item.Body.contains(s)) {
                System.debug('condition meet ' + item);
                toConvert.add(item);
            }
        }
        System.debug('toConvert ' + toConvert);
        System.debug('@USER_PREFIX ' + s);
        System.debug(toConvert.isEmpty());
        return !toConvert.isEmpty() ? doConversions(toConvert) : new Set<Id>();
    }


    private static Set<Id> doConversions(List<FeedItem> recs) {
        System.debug('here2');
        List<ConnectApi.BatchResult> batchResponse =
            ConnectApi.ChatterFeeds.getFeedElementBatch(Network.getNetworkId(), new List<Id>(new Map<Id, FeedItem>(recs).keySet()));
        System.debug('batchResponse ' + batchResponse);

        Set<Id> convertedIds = new Set<Id>();
        System.debug('here3');
        for (Object result : VT_D1_MentionHelper.getBatchResults(batchResponse)) {
            conversionDone = false;
            ConnectApi.FeedElement existingRec = (ConnectApi.FeedElement)result;
            System.debug('existingRec ' + existingRec);
            ConnectApi.FeedItemInput updatedRec = new ConnectApi.FeedItemInput();
            updatedRec.body = getBody(existingRec.body.messageSegments);
            System.debug('updatedRec ' + updatedRec);
            if (conversionDone) {
                try {
                 //   ConnectApi.ChatterFeeds.updateFeedElement(Network.getNetworkId(), existingRec.Id, updatedRec);
                    System.debug('here6');
                    convertedIds.add(existingRec.Id);
                } catch (exception e) {
                    system.debug(e.getMessage());
                }
            }
        }

        if (!convertedIds.isEmpty()) { doConversionsFuture(new List<Id>(convertedIds)); }
        System.debug('here7');
        return convertedIds;
    }

    @Future
    private static void doConversionsFuture(List<Id> feedItemIds) {
        List<ConnectApi.BatchResult> batchResponse =
            ConnectApi.ChatterFeeds.getFeedElementBatch(Network.getNetworkId(), feedItemIds);

        Set<Id> convertedIds = new Set<Id>();
        for (Object result : VT_D1_MentionHelper.getBatchResults(batchResponse)) {
            conversionDone = false;
            ConnectApi.FeedElement existingRec = (ConnectApi.FeedElement)result;
            ConnectApi.FeedItemInput updatedRec = new ConnectApi.FeedItemInput();
            updatedRec.body = getBody(existingRec.body.messageSegments);
            if (conversionDone) {
                ConnectApi.ChatterFeeds.updateFeedElement(Network.getNetworkId(), existingRec.Id, updatedRec);
            }
        }
    }

    private static ConnectApi.MessageBodyInput getBody(List<ConnectApi.MessageSegment> existingSegments) {
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

        // add existing message segments first
        for (ConnectApi.MessageSegment msgSeg : existingSegments) {
            String segType = String.ValueOf(msgSeg.type);

            if (segType == 'MarkupBegin') {
                messageBodyInput.messageSegments.add(VT_D1_MentionHelper.getMarkupBeginSeg(msgSeg));
            } else if (segType == 'Mention') {
                messageBodyInput.messageSegments.add(VT_D1_MentionHelper.getMentionSeg(msgSeg));
            } else if (segType == 'Text') {
                messageBodyInput.messageSegments.addAll(getSegmentsFromText(msgSeg.text));
            } else if (segType == 'Link') {
                messageBodyInput.messageSegments.add(VT_D1_MentionHelper.getLinkSeg(msgSeg));
            } else if (segType == 'MarkupEnd') {
                messageBodyInput.messageSegments.add(VT_D1_MentionHelper.getMarkupEndSeg(msgSeg));
            } else if (segType == 'Hashtag') {
                messageBodyInput.messageSegments.add(VT_D1_MentionHelper.getHashtagSeg(msgSeg));
            }
        }

        return messageBodyInput;
    }

    public static List<ConnectApi.MessageSegmentInput> getSegmentsFromText(String text) {
        Matcher m = COMPILED_EXPRESSION.matcher(text);
        integer idx = 0;
        List<ConnectApi.MessageSegmentInput> segs = new List<ConnectApi.MessageSegmentInput>();

        while (m.find()) {
            if (idx < m.start()) {
                segs.add(VT_D1_MentionHelper.getTextSeg(text.subString(idx, m.start())));
            }
            segs.add(VT_D1_MentionHelper.getMentionSeg(text.subString(m.Start() + 1, m.end())));
            idx = m.end();
            conversionDone = true;
        }

        if (idx < text.length()) {
            segs.add(VT_D1_MentionHelper.getTextSeg(text.subString(idx, text.length())));
        }

        return segs;
    }

    public static List<FeedItem> getFeedItemsMentionsToProcess(List<FeedItem> feedItems) {
        Set<Id> convertedIds = VT_D1_FeedItemConvertTagsToMentions.convert(feedItems);

        List<FeedItem> toProcessMentions = new List<FeedItem>(); // no need to do this for any records which have had @ tags converted to mentions - it will be done in update trigger instead
        for (FeedItem item : feedItems) {
            if (!convertedIds.contains(item.Id)) {
                toProcessMentions.add(item);
            }
        }
        System.debug('toProcessMentions ' + toProcessMentions);
        return toProcessMentions;
    }


}