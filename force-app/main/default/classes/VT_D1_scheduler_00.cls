global class VT_D1_scheduler_00 implements Schedulable {
    global void execute(SchedulableContext ctx) {
        VTD1_General_Settings__mdt reTaskBatchSize = [
                SELECT VTR5_ReTaskServiceBatchSize__c
                FROM VTD1_General_Settings__mdt LIMIT 1
        ];
        Integer batchSize = 200;
        if (reTaskBatchSize.VTR5_ReTaskServiceBatchSize__c != null) {
            batchSize = (Integer) reTaskBatchSize.VTR5_ReTaskServiceBatchSize__c;
        }
        Database.executeBatch(new VT_R2_RETaskService(), batchSize);
    }
}