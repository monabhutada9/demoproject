/**
* @author: N.Arbatskiy
* @date: 05-May-20
* @description: Class to create JSON body for HTTP request (eCOA API Update Subject).
**/
public class VT_R5_RequestBuilder_eCoaUpdateSubject implements VT_D1_RequestBuilder {
    public String timezone;
    public String subjectLocale;
    public String eproRuleSetGuid;
    public String clinroRuleSetGuid;
    public String studyGuid;
    public String orgGuid = eCOA_IntegrationDetails__c.getInstance().eCOA_OrgGuid__c;

    public VT_R5_RequestBuilder_eCoaUpdateSubject(User u) {
        timezone = u.TimeZoneSidKey;
        subjectLocale = VT_R5_eCoaLocaleAdaptor.adaptLocale(u.Id);
        studyGuid = u.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eCOA_Guid__c;

        eproRuleSetGuid = u.VTR5_ePRO_GUID__c;
        clinroRuleSetGuid = u.VTR5_ClinRO_GUID__c;
    }

    public String buildRequestBody() {
        return JSON.serialize(this);
    }
}