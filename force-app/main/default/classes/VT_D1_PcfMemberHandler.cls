public without sharing class VT_D1_PcfMemberHandler {
	public void onAfterInsert(List<VTD1_PCF_Chat_Member__c> recs) {
		doAdd(getMap(recs));
	}

	private void doAdd(Map<Id, Set<Id>> pcfToUserIdsMap) {
		addSharing(pcfToUserIdsMap);
		//        addFollows(pcfToUserIdsMap);
	}

	private void addSharing(Map<Id, Set<Id>> pcfToUserIdsMap) {
		List<CaseShare> newShares = new List<CaseShare>();

		for (Id chatId : pcfToUserIdsMap.keySet()) {
			for (Id uId : pcfToUserIdsMap.get(chatId)) {
				newShares.add(new CaseShare(
						UserOrGroupId = uId,
						CaseId = chatId,
						RowCause = 'Manual',
						CaseAccessLevel = 'Read'
				));
			}
		}
		Database.Insert(newShares, false);
	}

	//    private void removeSharing(Map<Id, Set<Id>> pcfToUserIdsMap) {
	//        List<CaseShare> toDelete = new List<CaseShare>();
	//        for (CaseShare item : [
	//            SELECT Id, ParentId, UserOrGroupId
	//            FROM CaseShare
	//            WHERE ParentId IN :pcfToUserIdsMap.keySet()
	//            AND UserOrGroupId IN :getUserIdsFromMap(pcfToUserIdsMap)
	//            AND RowCause = 'Manual'
	//        ]) {
	//            if (pcfToUserIdsMap.get(item.ParentId).contains(item.UserOrGroupId)) {
	//                toDelete.add(item);
	//            }
	//        }
	//
	//        if (!toDelete.isEmpty()) { delete toDelete; }
	//    }
	//
	private Map<Id, Set<Id>> getMap(List<VTD1_PCF_Chat_Member__c> recs) {
		Map<Id, Set<Id>> pcfToUserIdsMap = new Map<Id, Set<Id>>();
		for (VTD1_PCF_Chat_Member__c item : recs) {
			if (!pcfToUserIdsMap.containsKey(item.VTD1_PCF__c)) {
				pcfToUserIdsMap.put(item.VTD1_PCF__c, new Set<Id>());
			}
			pcfToUserIdsMap.get(item.VTD1_PCF__c).add(item.VTD1_User_Id__c);
		}
		return pcfToUserIdsMap;
	}
	//
	//    private List<Id> getUserIdsFromMap(Map<Id, Set<Id>> pcfToUserIdsMap) {
	//        List<Id> userIds = new List<Id>();
	//        for (Set<Id> uIds : pcfToUserIdsMap.Values()) {
	//            userIds.addAll(uIds);
	//        }
	//        return userIds;
	//    }
	//
	//    private void addFollows(Map<Id, Set<Id>> pcfToUserIdsMap) {
	//        Map<Id, User> userMap = new Map<Id, User>([
	//            SELECT Id, UserType FROM User WHERE Id IN :getUserIdsFromMap(pcfToUserIdsMap)
	//        ]);
	//
	//        List<EntitySubscription> newSubs = new List<EntitySubscription>();
	//        for (Id chatId : pcfToUserIdsMap.keySet()) {
	//            for (Id uId : pcfToUserIdsMap.get(chatId)) {
	//                if (userMap.containsKey(uId) && userMap.get(uId).UserType == 'Standard'){
	//                    newSubs.add(new EntitySubscription(
	//                        ParentId = chatId,
	//                        SubscriberId = uId
	//                    ));
	//                }
	//            }
	//        }
	//
	//        Database.insert(newSubs, false);
	//    }
	//
	//    private void removeFollows(Map<Id, Set<Id>> pcfToUserIdsMap) {
	//        List<EntitySubscription> toDelete = new List<EntitySubscription>();
	//        for (EntitySubscription item : [
	//            SELECT Id, ParentId, SubscriberId
	//            FROM EntitySubscription
	//            WHERE ParentId IN :pcfToUserIdsMap.keySet()
	//            AND SubscriberId IN :getUserIdsFromMap(pcfToUserIdsMap)
	//        ]) {
	//            if (pcfToUserIdsMap.get(item.ParentId).contains(item.SubscriberId)) {
	//                toDelete.add(item);
	//            }
	//        }
	//
	//        if (!toDelete.isEmpty()) { delete toDelete; }
	//    }
}