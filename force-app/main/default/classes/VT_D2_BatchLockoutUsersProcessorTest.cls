@IsTest
public with sharing class VT_D2_BatchLockoutUsersProcessorTest {

    public static void lockoutUserTest() {
        List<Case> casesList = [SELECT Id, VTD1_Primary_PG__c FROM Case];
        System.assert(casesList.size() > 0);
        if (casesList.size() > 0) {
            Case cas = casesList.get(0);

            UserLogin userLogin = [SELECT UserId, IsFrozen FROM UserLogin WHERE UserId=: cas.VTD1_Primary_PG__c];
            System.assertEquals(false, userLogin.IsFrozen);

            Test.startTest();
            VT_D1_BatchLockoutUsersProcessor batch = new VT_D1_BatchLockoutUsersProcessor(new VT_D1_ScheduleLockoutProcessor());
            batch.query = 'SELECT UserId, IsFrozen FROM UserLogin WHERE UserId=\'' + cas.VTD1_Primary_PG__c + '\'';
            Id batchId = Database.executeBatch(batch);
            Test.stopTest();

            UserLogin userLogin1 = [SELECT UserId, IsFrozen FROM UserLogin WHERE UserId=: cas.VTD1_Primary_PG__c];
            System.assertEquals(true, userLogin1.IsFrozen);
        }
    }

}