/**
 * Created by shume on 23/10/2018.
 */

@IsTest
private class VT_D1_Test_CustomNotificationsController {
    @IsTest
    static void testGetNotifications() {
        DomainObjects.Case_t cas = new DomainObjects.Case_t()
            .setRecordTypeByName('VTD1_PCF');
        cas.persist();

        DomainObjects.VTD1_Actual_Visit_t actualVisit = new DomainObjects.VTD1_Actual_Visit_t()
                .setVTD1_Status(VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED)
                .addVTD1_Case(cas);
        actualVisit.persist();

        List<VTD1_NotificationC__c> newNots = new List<VTD1_NotificationC__c>();
        VTD1_NotificationC__c n = new VTD1_NotificationC__c();
        n.Type__c = 'Study Team Visit';
        n.VTD2_VisitID__c = actualVisit.id;
        n.VTD1_Receivers__c = UserInfo.getUserId();
        newNots.add(n);
        VTD1_NotificationC__c n1 = new VTD1_NotificationC__c();
        n1.Type__c = 'General';
        n1.HasDirectLink__c = true;
        n1.VTD1_Receivers__c = UserInfo.getUserId();
        newNots.add(n1);
        VTD1_NotificationC__c n2 = new VTD1_NotificationC__c();
        n2.Type__c = 'Messages';
        n2.Number_of_unread_messages__c = 3;
        n2.VTD2_New_Message_Added_DateTme__c = Datetime.now()+1;
        n2.VTD1_Receivers__c = UserInfo.getUserId();
        newNots.add(n2);
        insert newNots;

        List<VTD1_NotificationC__c> nots = VT_D1_CustomNotificationsController.getNotificationsForCurrentUser();
        System.assertEquals(3, nots.size());
    }

    @IsTest
    static void testUpdateAndDeleteNotifications() {
        VTD1_NotificationC__c n = new VTD1_NotificationC__c();
        n.Type__c = 'General';
        n.VTD1_Receivers__c = UserInfo.getUserId();
        insert n;
        List<Id> notsIds = new List<Id>();
        notsIds.add(n.Id);
        VT_D1_CustomNotificationsController.updateNotifications(notsIds);
        List<VTD1_NotificationC__c> nots = [SELECT Id, VTD1_Read__c FROM VTD1_NotificationC__c];
        System.assertEquals(true, nots[0].VTD1_Read__c);

        // test delete

        VT_D1_CustomNotificationsController.hideNotifications(nots);
        nots = [SELECT Id, VTD1_Read__c FROM VTD1_NotificationC__c WHERE VTR5_HideNotification__c = FALSE];
        System.assertEquals(0, nots.size());
    }

    @IsTest
    static void getSessionIdUserIdTest() {
        System.assertNotEquals(null, VT_D1_CustomNotificationsController.getSessionIdUserId());
    }
//    @IsTest
//    static void notificationDateTimeTest() {
//        VTD1_NotificationC__c n = new VTD1_NotificationC__c();
//        n.Type__c = 'General';
//        n.VTD1_Receivers__c = UserInfo.getUserId();
//        insert n;
//        VT_D1_CustomNotificationsController.notificationDateTime(n.Id);
//    }
}