/**
 * Created by shumeyko on 09/11/2018.
 */

public with sharing class VT_D2_StudyUpdateHelper {
    public static void convertCandidatePatientsOnHold (List<Id> studyIds) {
        List<HealthCloudGA__CandidatePatient__c> candidatePatients = [SELECT Id,
                                                                        VTD2_Study_Recruitment_Is_On_Hold__c,
                                                                        Conversion__c,
                                                                        VTR4_New_Conversion_Process__c
                                                                        FROM HealthCloudGA__CandidatePatient__c
                                                                        WHERE Study__c IN :studyIds
                                                                        AND Conversion__c = 'Draft'
                                                                        AND VTD1_DuplicatedRecord__c = false];
        VT_D1_CandidatePatientProcessHandler.processCandidateConversion(candidatePatients);
        /*for (HealthCloudGA__CandidatePatient__c cp : candidatePatients) {
            cp.Conversion__c = 'Converted';
        }
        update candidatePatients;*/
    }
}