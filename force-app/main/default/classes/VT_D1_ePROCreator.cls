public without sharing class VT_D1_ePROCreator extends VT_R4_AbstractEproCreator{
    private List<VTD1_Actual_Visit__c> completedVisits = new List<VTD1_Actual_Visit__c>();
    private List<VTD1_Actual_Visit__c> endDateSetVisits = new List<VTD1_Actual_Visit__c>();
    private Map<Id, List<VTD1_Protocol_ePRO__c>> templatesByProtocolVisitId = new Map<Id, List<VTD1_Protocol_ePRO__c>>();
    private Map<Id, Map<String, List<VTD1_Protocol_ePRO__c>>> satisfactionTemplatesByStudyId = new Map<Id, Map<String, List<VTD1_Protocol_ePRO__c>>>(); // Study ID => Visit sub type => list of protocol epros (list because there can be multiple versions)
    public void createEpros(List<VTD1_Actual_Visit__c> recs, Map<Id, VTD1_Actual_Visit__c> oldMap) {
        queryVisits(recs, oldMap);
        if (! this.completedVisits.isEmpty() || ! this.endDateSetVisits.isEmpty()) {
            getCaregiverMap();
            getTemplatesByProtocolVisitId();
            initialiseProtocolAmendmentVersioning();
            generateEpros();
        }
    }

    private void queryVisits(List<VTD1_Actual_Visit__c> recs, Map<Id, VTD1_Actual_Visit__c> oldMap) {
        Set<Id> completedVisitIds = new Set<Id>();
        Set<Id> endDateSetVisitIds = new Set<Id>();
        for (VTD1_Actual_Visit__c item : recs) {
            VTD1_Actual_Visit__c old = oldMap.get(item.Id);
            if (isVisitCompleted(item) && ! isVisitCompleted(old)) {
                completedVisitIds.add(item.Id);
            }
            if (item.VTD1_Scheduled_Visit_End_Date_Time__c != null &&
                old.VTD1_Scheduled_Visit_End_Date_Time__c == null &&
                item.Sub_Type__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_SUBTYPE_PSC
            ) {
                endDateSetVisitIds.add(item.Id);
            }
        }
        if (! completedVisitIds.isEmpty() || ! endDateSetVisitIds.isEmpty()) {
            for (VTD1_Actual_Visit__c item : [
                SELECT  Id,
                        VTD1_Visit_Satisfaction_Email__c,
                        Sub_Type__c,
                        VTD1_Case__c,
                        VTD1_Status__c,
                        VTD1_Scheduled_Visit_End_Date_Time__c,
                        VTD1_Case__r.VTD1_ePro_can_be_completed_by_Caregiver__c,
                        VTD1_Case__r.VTD1_Patient_User__r.Contact.AccountId,
                        VTD1_Case__r.VTD1_Patient_User__c,
                        VTD1_Case__r.VTD1_Patient_User__r.VTD2_UserTimezone__c,
                        VTD1_Case__r.VTD1_Study__c,
                        VTD1_Case__r.VTD1_Virtual_Site__c,
                        VTD1_Case__r.VTD1_Primary_PG__c,
                        VTD1_Case__r.VTR2_SiteCoordinator__c,
                        VTD1_Case__r.VTR5_SubsetSafety__c,
                        VTD1_Case__r.VTR5_SubsetImmuno__c,
                        VTD1_Protocol_Visit__c,
                        VTD1_Protocol_Visit__r.VTD1_Visit_Satisfaction_Email__c
                FROM VTD1_Actual_Visit__c
                WHERE (Id IN :completedVisitIds OR Id IN :endDateSetVisitIds)
                AND VTD1_Case__r.VTD1_Patient_User__c != NULL
            ]) {
                if (completedVisitIds.contains(item.Id)) { this.completedVisits.add(item); }
                if (endDateSetVisitIds.contains(item.Id)) { this.endDateSetVisits.add(item); }
            }
        }
    }
    private void getCaregiverMap() {
        List<Id> patientAccIds = new List<Id>();
        for (VTD1_Actual_Visit__c item : this.completedVisits) {
            if (item.VTD1_Case__r.VTD1_Patient_User__r.Contact.AccountId != null) {
                patientAccIds.add(item.VTD1_Case__r.VTD1_Patient_User__r.Contact.AccountId);
            }
        }
        if (! patientAccIds.isEmpty()) {
            for (User item : [
                SELECT Id, Contact.AccountId
                FROM User
                WHERE Contact.AccountId IN :patientAccIds
                AND Contact.RecordType.DeveloperName = 'Caregiver'
                ORDER BY Contact.VTD1_Primary_CG__c ASC
            ]) {
                this.caregiverMap.put(item.Contact.AccountId, item);
            }
        }
    }
    private void getTemplatesByProtocolVisitId() {
        List<Id> pVisitIds = new List<Id>();
        List<Id> studyIds = new List<Id>();
        for (VTD1_Actual_Visit__c item : this.completedVisits) {
            if (item.VTD1_Protocol_Visit__c != null) {
                pVisitIds.add(item.VTD1_Protocol_Visit__c);
            }
            if (satisfactionEmail(item) && item.Sub_Type__c != VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_SUBTYPE_PSC) {
                studyIds.add(item.VTD1_Case__r.VTD1_Study__c);
            }
        }
        for (VTD1_Actual_Visit__c item : this.endDateSetVisits) {
            studyIds.add(item.VTD1_Case__r.VTD1_Study__c);
        }
        for (VTD1_Protocol_ePRO__c item : [
            SELECT  Id,
                    Name,
                    VTD1_Previous_Version__c,
                    VTD1_Trigger__c,
                    VTD1_Event__c,
                    VTD1_Event__r.Name,
                    VTD1_First_Available__c,
                    VTD1_Type__c,
                    VTD1_Response_Window__c,
                    VTD1_Reminder_Window__c,
                    VTD1_Period__c,
                    VTD1_Day_Frequency__c,
                    Days__c,
                    VTD1_Subject__c,
                    VTD1_Caregiver_on_behalf_of_Patient__c,
                    RecordType.DeveloperName,
                    VTD1_Study__c,
                    VTD1_Study__r.VTD1_End_Date__c,
                    VTR2_Protocol_Reviewer__c,
                    VTR2_eDiary_delay__c,
                    VTD1_Protocol_Amendment__c,
                    VTR4_RootProtocolId__c,
                    VTR4_RemoveFromProtocol__c,
                    VTR4_EventOffset__c, VTR4_EventOffsetPeriod__c, VTR4_EventTime__c, VTR4_EventType__c, VTR4_PatientMilestone__c,
                    VTR5_eCOAEventName__c,
                    VTR5_SubsetSafetyID__c,
                    VTR5_SubsetImmunoID__c
            FROM VTD1_Protocol_ePRO__c
            WHERE (
                (VTD1_Trigger__c = :VT_R4_ConstantsHelper_Protocol_ePRO.PROTOCOL_EPRO_TRIGGER_EVENT AND VTD1_Event__c IN :pVisitIds) OR
                (VTD1_Trigger__c = :VT_R4_ConstantsHelper_Protocol_ePRO.PROTOCOL_EPRO_TRIGGER_PERIODIC AND VTD1_First_Available__c IN :pVisitIds) OR
                (RecordType.DeveloperName = 'SatisfactionSurvey' AND VTD1_Study__c IN :studyIds)
            ) AND VTR4_Dont_Apply_PA_Versioning__c = FALSE AND VTD1_isArchivedVersion__c = FALSE
        ]) {
            if (item.RecordType.DeveloperName == 'ePRO' || item.RecordType.DeveloperName == 'VTR5_External') {
                Id pVisitId = item.VTD1_Trigger__c.equalsIgnoreCase(VT_R4_ConstantsHelper_Protocol_ePRO.PROTOCOL_EPRO_TRIGGER_EVENT) ?
                    item.VTD1_Event__c : item.VTD1_First_Available__c;
                if (! this.templatesByProtocolVisitId.containsKey(pVisitId)) {
                    this.templatesByProtocolVisitId.put(pVisitId, new List<VTD1_Protocol_ePRO__c>());
                }
                this.templatesByProtocolVisitId.get(pVisitId).add(item);
            } else {
                if (! this.satisfactionTemplatesByStudyId.containsKey(item.VTD1_Study__c)) {
                    this.satisfactionTemplatesByStudyId.put(item.VTD1_Study__c, new Map<String, List<VTD1_Protocol_ePRO__c>>());
                    this.satisfactionTemplatesByStudyId.get(item.VTD1_Study__c).put(item.VTD1_Type__c, new List<VTD1_Protocol_ePRO__c>());
                }
                this.satisfactionTemplatesByStudyId.get(item.VTD1_Study__c).get(item.VTD1_Type__c).add(item);
            }
        }
    }
    private void initialiseProtocolAmendmentVersioning() {
        List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrappedRecords = new List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper>();
        wrappedRecords.addAll(wrapListOfLists(templatesByProtocolVisitId.values()));
        for (Map<String, List<VTD1_Protocol_ePRO__c>> satisfactionMap : satisfactionTemplatesByStudyId.values()) {
            wrappedRecords.addAll(wrapListOfLists(satisfactionMap.values()));
        }
        paVersionHelper = new VT_R4_ProtocolAmendmentVersionHelper(wrappedRecords);
    }
    private void generateEpros() {
        for (VTD1_Actual_Visit__c visit : this.completedVisits) {
            if (!this.templatesByProtocolVisitId.containsKey(visit.VTD1_Protocol_Visit__c)) {
                continue;
            }
            List<VTD1_Protocol_ePRO__c> unVersionedTemplates = templatesByProtocolVisitId.get(visit.VTD1_Protocol_Visit__c);
            List<VTD1_Protocol_ePRO__c> correctVersionProtocols = paVersionHelper.getCorrectVersions(visit.VTD1_Case__r.VTD1_Virtual_Site__c, unVersionedTemplates);
            for (VTD1_Protocol_ePRO__c template : correctVersionProtocols) {
                if (isCaregiverSubjectWithNoCaregiver(visit, template)) {
                    continue;
                }
                if (template.VTD1_Trigger__c.equalsIgnoreCase(VT_R4_ConstantsHelper_Protocol_ePRO.PROTOCOL_EPRO_TRIGGER_EVENT)) {
                    if (template.VTD1_Previous_Version__c == null && template.VTD1_Protocol_Amendment__c != null && !paVersionHelper.isPAApproved(template.VTD1_Protocol_Amendment__c)) {
                        continue;
                    }
                    if (template.RecordType.DeveloperName != 'VTR5_External') {
                        String patientTimeZone = visit.VTD1_Case__r.VTD1_Patient_User__r.VTD2_UserTimezone__c;
                        this.ePros.add(getNewEpro(visit, template, getEventDateTime(template, patientTimeZone))); //create SH Diaries
                    } else if (isECoaEventNeeded(template, visit)) {
                        Id subjectId = isSubjectCaregiver(template) ? getCaregiverId(visit, template) : visit.VTD1_Case__r.VTD1_Patient_User__c;
                        addEvent(subjectId, template.VTR5_eCOAEventName__c); //send date to eCoa event to trigger the diary
                    }
                } else if (isPeriodTemplateValid(template)) {
                    addPeriodEpros(visit, template);
                }

            }

            addSatisfactionSurvey(visit, false);
        }
        for (VTD1_Actual_Visit__c visit : this.endDateSetVisits) {
            addSatisfactionSurvey(visit, true);
        }
        if (!this.ePros.isEmpty()) {
            insert this.ePros;
        }
        sendEvents();
    }
    private void addSatisfactionSurvey(VTD1_Actual_Visit__c visit, Boolean isPSC) {
        if (
            satisfactionEmail(visit) &&
            (visit.Sub_Type__c == VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_SUBTYPE_PSC) == isPSC &&
            this.satisfactionTemplatesByStudyId.containsKey(visit.VTD1_Case__r.VTD1_Study__c) &&
            this.satisfactionTemplatesByStudyId.get(visit.VTD1_Case__r.VTD1_Study__c).containsKey(visit.Sub_Type__c)
        ) {
            List<VTD1_Protocol_ePRO__c> allVersionTemplates = satisfactionTemplatesByStudyId.get(visit.VTD1_Case__r.VTD1_Study__c).get(visit.Sub_Type__c);
            List<VTD1_Protocol_ePRO__c> correctVersionList = paVersionHelper.getCorrectVersions(visit.VTD1_Case__r.VTD1_Virtual_Site__c, allVersionTemplates);
            if (!correctVersionList.isEmpty()) {
                Datetime availableDate = isPSC ? Datetime.now() : visit.VTD1_Scheduled_Visit_End_Date_Time__c.addHours(-1);
                this.ePros.add(getNewEpro(visit, correctVersionList[0], availableDate));
            }
        }
    }
    private VTD1_Survey__c getNewEpro(VTD1_Actual_Visit__c visit, VTD1_Protocol_ePRO__c template, Datetime availableDate) {
        if (template.VTR2_eDiary_delay__c != null && template.VTR2_eDiary_delay__c > 0) {
            availableDate = availableDate.addDays(template.VTR2_eDiary_delay__c.intValue());
        }
        VTD1_Survey__c newSurvey = new VTD1_Survey__c(
            Name = template.Name,
            VTD1_CSM__c = visit.VTD1_Case__c,
            //VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_NOT_STARTED, // use the default picklist value for the specific RT
            VTD1_Date_Available__c = availableDate,
            VTD1_Protocol_ePRO__c = template.Id,
            VTD1_Patient_User_Id__c = visit.VTD1_Case__r.VTD1_Patient_User__c,
            VTD1_Caregiver_User_Id__c = getCaregiverId(visit, template),
            RecordTypeId = this.surveyRecTypeMap.get(template.RecordType.DeveloperName).getRecordTypeId(),
            VTD1_Actual_Visit__c = visit.Id,
            VTD1_Due_Date__c = template.VTD1_Response_Window__c != null ?
                availableDate.addMinutes(Math.round(template.VTD1_Response_Window__c * 60)) : availableDate,
            VTR2_Reviewer_User__c = getReviewerForEDiary(visit.VTD1_Case__r.VTD1_Primary_PG__c, visit.VTD1_Case__r.VTR2_SiteCoordinator__c, template.VTR2_Protocol_Reviewer__c)
        );
        newSurvey.VTD1_Reminder_Due_Date__c = getReminderDate(newSurvey.VTD1_Due_Date__c, template.VTD1_Reminder_Window__c);
        return newSurvey;
    }

    private void addPeriodEpros(VTD1_Actual_Visit__c visit, VTD1_Protocol_ePRO__c template) {
        // get available date for first ePro
        Datetime availDate = System.now();
        Set<String> validDays;
        if (template.VTD1_Period__c.equalsIgnoreCase(VT_R4_ConstantsHelper_Protocol_ePRO.PROTOCOL_EPRO_PERIOD_SPECIFIC_DAYS)) {
            validDays = new Set<String>(template.Days__c.split(';'));
            availDate = getNextValidSpecificDay(validDays, availDate);
        }
        while (availDate.dateGmt() <= template.VTD1_Study__r.VTD1_End_Date__c) {
            this.ePros.add(getNewEpro(visit, template, availDate));
            // increment available date
            switch on template.VTD1_Period__c {
                when 'Daily' {
                    availDate = availDate.addDays(1);
                }
                when 'Weekly' {
                    availDate = availDate.addDays(7);
                }
                when 'Monthly' {
                    availDate = availDate.addMonths(1);
                }
                when 'Every X Days' {
                    availDate = availDate.addDays(template.VTD1_Day_Frequency__c.intValue());
                }
                when 'Specific Days Repeated' {
                    availDate = availDate.addDays(1);
                    availDate = getNextValidSpecificDay(validDays, availDate);
                }
            }
        }
    }
    private Datetime getNextValidSpecificDay(Set<String> days, Datetime d) {
        if (days == null || days.isEmpty()) {
            return null;
        }
        while(! days.contains(d.format('EEEE'))) {
            d = d.addDays(1);
        }
        return d;
    }
    private Boolean isVisitCompleted(VTD1_Actual_Visit__c visit) {
        return visit.VTD1_Status__c != null && visit.VTD1_Status__c.equalsIgnoreCase(VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED);
    }
    // check study has an end date, and also that a template doesn't have Specific Days Repeated but no days selected - this would create an infinite loop later on
    private Boolean isPeriodTemplateValid(VTD1_Protocol_ePRO__c template) {
        return
            template.VTD1_Study__r.VTD1_End_Date__c != null &&
            template.VTD1_Period__c != null &&
            ! (template.VTD1_Period__c.equalsIgnoreCase(VT_R4_ConstantsHelper_Protocol_ePRO.PROTOCOL_EPRO_PERIOD_SPECIFIC_DAYS) && template.Days__c.length() == 0)
        ;
    }
    private Boolean isCaregiverSubjectWithNoCaregiver(VTD1_Actual_Visit__c visit, VTD1_Protocol_ePRO__c template) {
        return
            isSubjectCaregiver(template) &&
            ! this.caregiverMap.containsKey(visit.VTD1_Case__r.VTD1_Patient_User__r.Contact.AccountId)
        ;
    }

    private Boolean satisfactionEmail(VTD1_Actual_Visit__c visit) {
        return visit.VTD1_Visit_Satisfaction_Email__c || visit.VTD1_Protocol_Visit__r.VTD1_Visit_Satisfaction_Email__c;
    }
}