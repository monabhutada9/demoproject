@isTest
public with sharing class VT_R3_RestPeriodicTest {

	public static void firstTest() {
		User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
		Test.startTest();
		System.runAs(toRun) {
			Test.setMock(HttpCalloutMock.class, new MockSendVideoConfSignalHttpResponseGenerator());
			VT_R3_RestPeriodic.getPeriodicData();
		}
		Test.stopTest();
	}
	public static void secondTest() {
		User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
		Test.startTest();
		System.runAs(toRun) {
			RestContext.response = new RestResponse();
			VT_R3_RestPeriodic.checkToken();
		}
		VT_R3_RestPeriodic.checkToken();
		Test.stopTest();
	}

	private class MockSendVideoConfSignalHttpResponseGenerator implements HttpCalloutMock {
		public HTTPResponse respond(HTTPRequest req) {
			HttpResponse res = new HttpResponse();
			res.setHeader('Content-Type', 'application/json');
			res.setBody('["test"]');
			res.setStatusCode(200);
			return res;
		}
	}
}