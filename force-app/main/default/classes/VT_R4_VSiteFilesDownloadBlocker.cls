/**
 * Created by Yevhen Kharchuk on 10-Apr-20.
 *
 * @description This class is run before any attempt to select ContentVersion.versionData or when this attempt is being
 * made from UI. It blocks attempts of downloading files related to Virtual Site where on the Study level field
 * VTR4_Download_Site_Files__c equals FALSE. This behavior is not applied to community users and admins.
 */
public without sharing class VT_R4_VSiteFilesDownloadBlocker implements Sfc.ContentDownloadHandlerFactory {
    private static String VIRTUAL_SITE_FILES_DOWNLOAD_ERROR = VT_D1_TranslateHelper.getLabelValue('VTR4_File_Download_Forbidden');
    public Sfc.ContentDownloadHandler getContentDownloadHandler(List<Id> ids, Sfc.ContentDownloadContext context) {
        if (isNotSoqlContext(context) &&
                isLightningInternalUser() &&
                isNotAdminUser() &&
                isFileForBlocking(ids[0])) {
            return forbidDownloading();
        }
        return allowDownloading();
    }
    private Boolean isNotSoqlContext(Sfc.ContentDownloadContext context) {
        return context != Sfc.ContentDownloadContext.SOQL;
    }
    private Boolean isLightningInternalUser() {
        return Network.getNetworkId() == null;
    }
    private Boolean isNotAdminUser() {
        return [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND Id = :UserInfo.getUserId()].isEmpty();
    }
    private Boolean isFileForBlocking(Id contentVersionId) {
        List<ContentDocumentLink> docLinks = getDocumentLinks(getDocumentIdFromCVId(contentVersionId));
        for (ContentDocumentLink link : docLinks) {
            if (isVirtualSite(link.LinkedEntityId)) {
                return isBlockingEnabledForStudy(link.LinkedEntityId);
            }
        }
        return false;
    }
    private Id getDocumentIdFromCVId(Id contentVersionId) {
        return ((ContentVersion) new QueryBuilder(ContentVersion.class)
                .addField(ContentVersion.ContentDocumentId)
                .addConditions()
                .add(new QueryBuilder.CompareCondition('Id').eq(contentVersionId))
                .endConditions()
                .namedQueryRegister('VT_R4_VSiteFilesDownloadBlocker.getDocumentIdFromCVId')
                .toSObject()).ContentDocumentId;
    }
    private List<ContentDocumentLink> getDocumentLinks(Id documentId) {
        return new QueryBuilder(ContentDocumentLink.class)
                .addField(ContentDocumentLink.LinkedEntityId)
                .addConditions()
                .add(new QueryBuilder.CompareCondition('ContentDocumentId').eq(documentId))
                .endConditions()
                .namedQueryRegister('VT_R4_VSiteFilesDownloadBlocker.getDocumentLinks')
                .toList();
    }
    private Boolean isVirtualSite(Id sObjectId) {
        return sObjectId.getSobjectType().getDescribe().getName() == 'Virtual_Site__c';
    }
    private Boolean isBlockingEnabledForStudy(Id virtualSiteId) {
        Virtual_Site__c site = (Virtual_Site__c)
                new QueryBuilder(Virtual_Site__c.class)
                        .addField('VTD1_Study__r.VTR4_Download_Site_Files__c')
                        .addConditions()
                        .add(new QueryBuilder.CompareCondition('Id').eq(virtualSiteId))
                        .endConditions()
                        .namedQueryRegister('VT_R4_VSiteFilesDownloadBlocker.isBlockingEnabledForStudy')
                        .toSObject();
        return site != null && !site.VTD1_Study__r.VTR4_Download_Site_Files__c;
    }
    private Sfc.ContentDownloadHandler forbidDownloading() {
        Sfc.ContentDownloadHandler contentDownloadHandler = new Sfc.ContentDownloadHandler();
        contentDownloadHandler.isDownloadAllowed = false;
        contentDownloadHandler.downloadErrorMessage = VIRTUAL_SITE_FILES_DOWNLOAD_ERROR;
        return contentDownloadHandler;
    }
    private Sfc.ContentDownloadHandler allowDownloading() {
        Sfc.ContentDownloadHandler contentDownloadHandler = new Sfc.ContentDownloadHandler();
        contentDownloadHandler.isDownloadAllowed = true;
        return contentDownloadHandler;
    }
}