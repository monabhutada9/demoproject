/**
 * Created by shume on 05.04.2019.
 */

@IsTest
private class VT_R2_AddProtocolKitsToPDeliveryTest {
    @testSetup
    private static void setupMethod() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        List<VTD1_Protocol_Kit__c> pks = [SELECT Id, VTD1_Protocol_Delivery__c
                                        FROM VTD1_Protocol_Kit__c];
        if (pks.size() != 0) {
            for (VTD1_Protocol_Kit__c pk : pks) {
                pk.VTD1_Protocol_Delivery__c = null;
            }
            update pks;
        }
    }

    @isTest
    private static void firstTest() {
        List<VTD1_Protocol_Delivery__c> pds = [SELECT Id, Care_Plan_Template__c FROM VTD1_Protocol_Delivery__c];
        if (pds.size() != 0) {
            List<VTD1_Protocol_Kit__c> pks = VT_R2_AddProtocolKitsToPDeliveryRemote.getProtocolKits(pds[0].Id);
            if (pks.size() != 0) {
                for (VTD1_Protocol_Kit__c pk : pks) {
                    pk.RecordTypeId = Schema.SObjectType.VTD1_Protocol_Kit__c
                            .getRecordTypeInfosByDeveloperName().get(VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_CONNECTED_DEVICES)
                            .getRecordTypeId();
                }
                List<Id> kitsToAdd = new List<Id> ();
                VT_R2_AddProtocolKitsToPDeliveryRemote.addKitsRemote(kitsToAdd, pds[0].Care_Plan_Template__c, pds[0].Id);
                kitsToAdd.add(pks[0].Id);
                VT_R2_AddProtocolKitsToPDeliveryRemote.addKitsRemote(null, pds[0].Care_Plan_Template__c, pds[0].Id);
            }
        }
    }
}