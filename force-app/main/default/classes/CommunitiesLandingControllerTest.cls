/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
@IsTest public with sharing class CommunitiesLandingControllerTest {
    @IsTest(SeeAllData=true) public static void testCommunitiesLandingController() {
        // Instantiate a new controller with all parameters in the page
        CommunitiesLandingController controller = new CommunitiesLandingController();
        PageReference pageRef = controller.forwardToStartPage();
        //PageRef is either null or an empty object in test context
        if(pageRef != null){
            String url = pageRef.getUrl();
            if(url != null){
                System.assertEquals(true, String.isEmpty(url));
                //show up in perforce
            }
        }
    }
    @IsTest public static void testSaveCookieAcceptedAudit() {
        Id accountId = new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor')
                .persist()
                .Id;
        User testUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t()
                        .setFirstName('Patient')
                        .setLastName('Patient')
                        .setAccountId(accountId))
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME)
                .persist();
        insert new AccountShare(
                AccountId = accountId,
                UserOrGroupId = testUser.Id,
                AccountAccessLevel = 'edit',
                OpportunityAccessLevel = 'edit',
                ContactAccessLevel = 'edit',
                RowCause = 'Manual'
        );
        System.debug([SELECT Id, ContactId, Contact.VTD1_UserId__c FROM USer WHERE Id=: testUser.Id]);
        Test.startTest();
        System.runAs(testUser) {
            CommunitiesLandingController.saveCookieAcceptedAudit('cookiesAccepted');
        }
        CommunitiesLandingController.saveCookieAcceptedAudit('cookiesAccepted');
        Test.stopTest();

    }

}