@IsTest
public without sharing class VT_D1_PIMyPatientPcfDetailTest {
    
    public static void getPcfData() {
        Case patientCase = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        Case pcf = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'VTD1_PCF' LIMIT 1];

        Test.startTest();
        Map<String, SObject> pcfData = VT_D1_PIMyPatientPcfDetail.getPcfData(patientCase.Id, pcf.Id);
        System.assertEquals(pcfData.get('case').Id, patientCase.Id);
        System.assertEquals(pcfData.get('pcf').Id, pcf.Id);
        Test.stopTest();
    }

    public static void getPicklistOptions() {
        Case patientCase = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];

        Test.startTest();
        VT_D1_PIMyPatientPcfDetail.getVisitPicklistOptions();
        VT_D1_PIMyPatientPcfDetail.getOwnerOptions(patientCase.Id);
        Test.stopTest();

        // todo: add assertEquals for getOwnerOptions (it should contain right PI or SCR)
    }

    public static void changePcfFields() {
        Case pcf = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'VTD1_PCF' LIMIT 1];

        Test.startTest();
        VT_D1_PIMyPatientPcfDetail.changeSafetyStatus(pcf.Id, 'Possible', 'safetyComment');
        VT_D1_PIMyPatientPcfDetail.changeSafetyReviewedByPI(pcf.Id, false);
        VT_D1_PIMyPatientPcfDetail.changeStatus(pcf.Id, 'Pre-Consent');
        VT_D1_PIMyPatientPcfDetail.changeNarrative(pcf.Id, 'narrative');
        VT_D1_PIMyPatientPcfDetail.changeEscalated(pcf.Id, false);
        Test.stopTest();

        // todo: add assertEquals for object fields after all changes
    }

    public static void changePcfOwner() {
        Case pcf = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'VTD1_PCF' LIMIT 1];

        Test.startTest();
        VT_D1_PIMyPatientPcfDetail.changeOwner(pcf.Id, UserInfo.getUserId());
        Test.stopTest();

        // todo: change owner not to UserInfo.getUserId(), but to another user (i.e., SCR)
        // todo: add assertEquals for pcf.OwnerId (it should be new owner)
    }

    public static void createQueueTest() {
        Case pcf = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'VTD1_PCF' LIMIT 1];
        User piUser = [
                SELECT Id, ContactId, IsActive
                FROM User
                WHERE Profile.Name = 'Primary Investigator'
                AND IsActive = true
                LIMIT 1
        ];

        Test.startTest();
        System.runAs(piUser) {
            VT_D1_PIMyPatientPcfDetail.createQueue(pcf.Id);
        }
        Test.stopTest();

        // todo: add assertEquals for result queue (it should contain right Study Team Members)
    }
//Added for SH-19644


    public static void handleSaveTest() {
        Case patientCase = [
                SELECT Id, VTD1_Study__c, VTD1_Patient_User__c, VTD1_Patient__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1
        ];
        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c(
                Id = patientCase.VTD1_Study__c,
                eCOA_Stop_Schedule_Event__c = 'stop_sic'
        );
        update study;

        Case pcf = new Case(
                RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'VTD1_PCF'].Id,
                Priority = 'Medium',
                Status = 'Open',
                VTD1_PCF_Reason_for_Contact__c = 'test',
                VTD2_Reason_Safety_Concern_Indicator_Cha__c = 'No',
                VTR5_Stop_eDiary_Schedule__c = true,
                VTD1_Clinical_Study_Membership__c = patientCase.Id
        );
        insert pcf;

        Test.startTest();
        VT_D1_PIMyPatientPcfDetail.handleSave(pcf.Id, true, 'test', String.valueOf(System.today()));
        Test.stopTest();

        pcf = [SELECT Id, VTR5_Stop_eDiary_Schedule__c FROM Case WHERE Id = :pcf.Id];
        System.assertEquals(true, pcf.VTR5_Stop_eDiary_Schedule__c);


    }
}