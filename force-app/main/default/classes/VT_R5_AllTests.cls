/**
 * Created by Igor Shamenok on 22-Sep-20.
 */

@IsTest 
private with sharing class VT_R5_AllTests {
    @TestSetup
    static void setup() {
        VT_R3_GlobalSharing.disableForTest = true;
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'))
                .setOriginalName('tN')
                .setStudyAdminId(UserInfo.getUserId());

        DomainObjects.User_t piUser = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Primary Investigator')
                .setEmail(DomainObjects.RANDOM.getEmail());

        DomainObjects.Contact_t scrCon = new DomainObjects.Contact_t()
                .setLastName('Con1');
        DomainObjects.User_t scrUser = new DomainObjects.User_t()
                .addContact(scrCon)
                .setProfile('Site Coordinator');

        DomainObjects.Phone_t phone = new DomainObjects.Phone_t(new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor'), '345-6789')
                .setType('Home')
                .setPrimaryForPhone(false)
                .addContact(scrCon);
        phone.persist();
        DomainObjects.VTR2_Study_Geography_t studyGeo = new DomainObjects.VTR2_Study_Geography_t()
                .addVTD2_Study(study)
                .setGeographicalRegion('Country')
                .setCountry('US')
                .setDateOfBirthRestriction('Year Only');
        DomainObjects.VTR2_StudyPhoneNumber_t studyPhone = new DomainObjects.VTR2_StudyPhoneNumber_t()
                .addVTR2_Study_Geography(studyGeo);

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t();
        DomainObjects.User_t ptUser = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile('Patient');
        DomainObjects.Case_t patientCase = new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .setSubject('VT_R5_allTest')
                .addPIUser(piUser)
                .addVTD1_Patient_User(ptUser)
                .addStudy(study)
                .addUser(ptUser)
                .addContact(patientContact)
                .addStudyGeography(studyGeo);
        patientCase.persist();
        Test.startTest();
        DomainObjects.VirtualSite_t virtualSite = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .setStudySiteNumber('12345');

        DomainObjects.StudyTeamMember_t stmCra = new DomainObjects.StudyTeamMember_t()
                .addUser(new DomainObjects.User_t().setProfile('CRA'))
                .addStudy(study)
                .setRecordTypeByName('CRA');

        DomainObjects.StudyTeamMember_t stmPI = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(piUser)
                .addVirtualSite(virtualSite)
                .setRecordTypeByName('PI');
            //START RAJESH SH-17440
                //.setOnsiteCraId(stmCra.id)
                //.setRemoteCraId(stmCra.id);
			//END:SH-17440
        DomainObjects.StudyTeamMember_t stmSCR = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addVirtualSite(virtualSite)
                .setUserId(UserInfo.getUserId())
                .setRecordTypeByName('SCR');
        new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addVirtualSite(virtualSite)
                .setUserId(UserInfo.getUserId())
                .setRecordTypeByName('SCR')
                .persist();

        new DomainObjects.Study_Site_Team_Member_t()
                .addVTR2_Associated_SCr(stmSCR)
                .addVTR2_Associated_PI(stmPI)
                .addVTD1_Associated_PI_t(stmPI)
                .persist();
        Test.stopTest();
        DomainObjects.StudySiteTeamMember_t sstm = new DomainObjects.StudySiteTeamMember_t()
                .setAssociatedPI2(stmPI)
                .setAssociatedScr(stmSCR);
        sstm.persist();

        DomainObjects.Case_t cas = new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .setSubject('CaseControllerRemoteTest')
                .setVTD1_Clinical_Study_Membership(patientCase.id)
                .addStudy(study)
                .addPIUser(piUser)
                .addVirtualSite(virtualSite)
                .addContact(scrCon);

        DomainObjects.VTR2_Site_Address_t address = new DomainObjects.VTR2_Site_Address_t()
                .setVirtualSiteId(virtualSite.toObject().Id)
                .setPrimary(true);
        address.persist();

        new DomainObjects.VTD1_Patient_Stratification_t()
                .addCase(cas)
                .addStratification(new DomainObjects.VTD1_Study_Stratification_t()
                .addStudy(study))
                .addValue('TestValue')
                .persist();
        DomainObjects.VTD1_Actual_Visit_t visit = new DomainObjects.VTD1_Actual_Visit_t()
                .setVTD1_Case(patientCase.id)
                .setVTD1_Status('To Be Scheduled')
                .setScheduledDateTime(Datetime.newInstance(2020, 2, 17))
                .setRecordTypeByName('VTD1_Unscheduled');

        new DomainObjects.Visit_Member_t()
                .setVTD1_Participant_UserId(ptUser.Id)
                .setVTD1_External_Participant_Type('Patient')
                .addVTD1_Actual_Visit(visit)
                .persist();
        new DomainObjects.Visit_Member_t()
                .setVTD1_External_Participant_Type('HHN')
                .addVTD1_Actual_Visit(visit)
                .persist();

        new DomainObjects.VTD1_Regulatory_Binder_t()
                .addStudy(study)
                .addRegDocument(new DomainObjects.VTD1_Regulatory_Document_t()
                        .setDocumentType(VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_SOURCE_NOTE_OF_TRANSFER))
                .persist();
        new DomainObjects.VTD1_Regulatory_Binder_t()
                .addStudy(study)
                .addRegDocument(new DomainObjects.VTD1_Regulatory_Document_t()
                        .setDocumentType(VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_TARGET_NOTE_OF_TRANSFER))
                .persist();
    }

    @IsTest
    static void VT_D1_PIMyPatientPcfDetailTest1() {
        VT_D1_PIMyPatientPcfDetailTest.getPcfData();
    }
    @IsTest
    static void VT_D1_PIMyPatientPcfDetailTest2() {
        VT_D1_PIMyPatientPcfDetailTest.getPicklistOptions();
    }
    @IsTest
    static void VT_D1_PIMyPatientPcfDetailTest3() {
        VT_D1_PIMyPatientPcfDetailTest.changePcfFields();
    }
    @IsTest
    static void VT_D1_PIMyPatientPcfDetailTest4() {
        VT_D1_PIMyPatientPcfDetailTest.changePcfOwner();
    }
    @IsTest
    static void VT_D1_PIMyPatientPcfDetailTest5() {
        VT_D1_PIMyPatientPcfDetailTest.createQueueTest();
    }
    
    //Added for SH-19644
    @IsTest
    static void VT_D1_PIMyPatientPcfDetailTest6() {
        VT_D1_PIMyPatientPcfDetailTest.handleSaveTest();
    }
    
    @IsTest
    static void VT_D1_PatientCommunityControllerTest1() {
        VT_D1_PatientCommunityControllerTest.getPatientUserPositiveTest();
    }
    @IsTest
    static void VT_D1_PatientCommunityControllerTest2() {
        VT_D1_PatientCommunityControllerTest.getPatientUserNegativeTest();
    }
    @IsTest
    static void VT_D1_PatientCommunityControllerTest3() {
        VT_D1_PatientCommunityControllerTest.testNavigationTranslation();
    }
    @IsTest
    static void VT_D1_PatientCommunityControllerTest4() {
        VT_D1_PatientCommunityControllerTest.testCommunityName();
    }
    @IsTest
    static void VT_D1_CaseControllerRemoteTest1() {
        VT_D1_CaseControllerRemoteTest.subjectRemoteTest();
    }
    @IsTest
    static void VT_D1_CaseControllerRemoteTest2() {
        VT_D1_CaseControllerRemoteTest.randomizeRemoteTest();
    }
    @IsTest
    static void VT_D1_CaseControllerRemoteTest3() {
        VT_D1_CaseControllerRemoteTest.getCaseInfoTest();
    }
    @IsTest
    static void VT_D1_CaseControllerRemoteTest4() {
        VT_D1_CaseControllerRemoteTest.getLogReportIdTest();
    }
    @IsTest
    static void VT_D1_CaseControllerRemoteTest5() {
        VT_D1_CaseControllerRemoteTest.isUserSystemAdminTest();
    }
    @IsTest
    static void VT_D1_CloneStudyController_Test() {
        VT_D1_CloneStudyController_Test.cloneActionTest();
    }
    @IsTest
    static void VT_D1_SendSubject_PatientStratTest() {
        VT_D1_SendSubject_PatientStratTest.doTest();
    }
    @IsTest
    static void VT_D2_ProtocolAmendmentTriggerHandlTest1() {
        VT_D2_ProtocolAmendmentTriggerHandlTest.doTest();
    }
    @IsTest
    static void VT_D2_ProtocolAmendmentTriggerHandlTest2() {
        VT_D2_ProtocolAmendmentTriggerHandlTest.testSignaturePagePH();
    }
    @IsTest
    static void VT_D2_EmailControllerTest() {
        VT_D2_EmailControllerTest.testBehavior();
    }
    @IsTest
    static void VT_R2_NotificationCSMSProcessorTest1() {
        VT_R2_NotificationCSMSProcessorTest.optInRemoteTest();
    }
//    @IsTest
//    static void VT_R2_NotificationCSMSProcessorTest2() {
//        VT_R2_NotificationCSMSProcessorTest.resendOptInRemoteTest();
//    }
    @IsTest
    static void VT_R2_NotificationCSMSProcessorTest3() {
        VT_R2_NotificationCSMSProcessorTest.optOutRemoteTest();
    }
    @IsTest
    static void VT_R2_NotificationCSMSProcessorTest4() {
        VT_R2_NotificationCSMSProcessorTest.sendContactsRemoteTest();
    }
    @IsTest
    static void VT_R3_ScTaskForActualVisitHandlerTest1() {
        VT_R3_ScTaskForActualVisitHandlerTest.testScCreationForStudyTeamOrHomeHealthNurseVisitType();
    }
    @IsTest
    static void VT_R3_ScTaskForActualVisitHandlerTest2() {
        VT_R3_ScTaskForActualVisitHandlerTest.testScCreationForHomeHealthNurseVisitType();
    }
    @IsTest
    static void VT_R3_ScTaskForActualVisitHandlerTest3() {
        VT_R3_ScTaskForActualVisitHandlerTest.testScCreationForPhlebotomistVisitType();
    }
    @IsTest
    static void VT_R3_ScTaskForActualVisitHandlerTest4() {
        VT_R3_ScTaskForActualVisitHandlerTest.testScCreationForHealthCareProfessionalVisitType();
    }
    @IsTest
    static void VT_R4_EmailChangeControllerTest() {
        VT_R4_EmailChangeControllerTest.testBehavior();
    }
    @IsTest
    static void VTR4_BatchFlagReconsentPatientsTest() {
        VTR4_BatchFlagReconsentPatientsTest.testBehavior();
    }
    @IsTest
    static void VT_R4_DocumentServiceTest() {
        VT_R4_DocumentServiceTest.populateStudyAndVirtualSiteTest();
    }
    @IsTest
    static void VTR2_SCRLookupControllerTest1() {
        VTR2_SCRLookupControllerTest.testFetchAccount();
    }
    @IsTest
    static void VTR2_SCRLookupControllerTest2() {
        VTR2_SCRLookupControllerTest.testGetNameById();
    }
    @IsTest
    static void VT_R2_MonitoringVisitFormControllerTest1() {
        VT_R2_MonitoringVisitFormControllerTest.getNewMonitoringVisitTest();
    }
    //@IsTest
    //static void VT_R2_MonitoringVisitFormControllerTest2() {
        //VT_R2_MonitoringVisitFormControllerTest.getVisitCRATest();
    //}
    @IsTest
    static void VT_R2_MonitoringVisitFormControllerTest3() {
        VT_R2_MonitoringVisitFormControllerTest.getVisitDatesTest();
    }
}