/**
 * Created by user on 11-Mar-20.
 */
@IsTest
private class VT_R4_StudyTeamAssignmentTest {
    @TestSetup
    static void setupMethod() {
        Test.startTest();
        VT_D1_TestUtils.prepareStudy(1);
        Test.stopTest();
    }

    @IsTest
    static void defaultConversionTest() {
        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
        System.assertEquals('Finished', [SELECT VTR4_StatusSTA__c FROM HealthCloudGA__CandidatePatient__c].VTR4_StatusSTA__c);
    }

    @IsTest
    static void oneKeyFieldFlowTest() {
        Study_Team_Member__c piTeamMember = [SELECT User__c FROM Study_Team_Member__c WHERE VTD1_Type__c = 'Primary Investigator' AND VTD1_Backup_PI__c = FALSE LIMIT 1];
        User piUser = [SELECT People_Soft_ID__c FROM User WHERE Id = :piTeamMember.User__c];
        piUser.People_Soft_ID__c = '123';
        update piUser;
        System.debug('--- piUser ' +piUser);
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];
        HealthCloudGA__CandidatePatient__c candidatePatient = new HealthCloudGA__CandidatePatient__c();
        candidatePatient.rr_firstName__c = 'firstName';
        candidatePatient.rr_lastName__c = 'lastName';
        candidatePatient.rr_Username__c = VT_D1_TestUtils.generateUniqueUserName();
        candidatePatient.rr_Email__c = candidatePatient.rr_Username__c;
        candidatePatient.rr_Alias__c = 'alias';
        candidatePatient.Conversion__c = 'Draft';
        candidatePatient.Study__c = study.Id;
        candidatePatient.VTD1_ProtocolNumber__c = 'Prot-001';
        candidatePatient.VTD1_Patient_Phone__c = String.valueOf(VT_D1_TestUtils.getRandomInteger());
        candidatePatient.HealthCloudGA__Address1Country__c = 'US';
        candidatePatient.HealthCloudGA__Address1PostalCode__c = 'state';
        candidatePatient.HealthCloudGA__Address1State__c = 'state';
        candidatePatient.HealthCloudGA__SourceSystemId__c = candidatePatient.rr_Username__c;
        candidatePatient.VTD2_Language__c = 'en_US';
        candidatePatient.VTD1_One_Key_ID__c = '123';
        Test.startTest();
        System.enqueueJob(new VT_D1_TestUtils.CPCreator(candidatePatient));
        Test.stopTest();
        //System.assertEquals('Finished', [SELECT VTR4_StatusSTA__c FROM HealthCloudGA__CandidatePatient__c].VTR4_StatusSTA__c);
        //System.assertEquals(candidatePatient.VTD1_One_Key_ID__c,[SELECT VTD1_PI_user__r.People_Soft_ID__c FROM Case].VTD1_PI_user__r.People_Soft_ID__c);
    }

    @IsTest
    static void PIFlowTest() {
        Study_Team_Member__c piTeamMember = [SELECT Id, User__c FROM Study_Team_Member__c WHERE VTD1_Type__c = 'Primary Investigator' AND VTD1_Backup_PI__c = false LIMIT 1];
        User piUser = [SELECT Id, People_Soft_ID__c FROM User WHERE Id = :piTeamMember.User__c];
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];
        HealthCloudGA__CandidatePatient__c candidatePatient = new HealthCloudGA__CandidatePatient__c();
        candidatePatient.rr_firstName__c = 'firstName';
        candidatePatient.rr_lastName__c = 'lastName';
        candidatePatient.rr_Username__c = VT_D1_TestUtils.generateUniqueUserName();
        candidatePatient.rr_Email__c = candidatePatient.rr_Username__c;
        candidatePatient.rr_Alias__c = 'alias';
        candidatePatient.Conversion__c = 'Draft';
        candidatePatient.Study__c = study.Id;
        candidatePatient.VTD1_ProtocolNumber__c = 'Prot-001';
        candidatePatient.VTD1_Patient_Phone__c = String.valueOf(VT_D1_TestUtils.getRandomInteger());
        candidatePatient.HealthCloudGA__Address1Country__c = 'US';
        candidatePatient.HealthCloudGA__Address1PostalCode__c = 'state';
        candidatePatient.HealthCloudGA__Address1State__c = 'state';
        candidatePatient.HealthCloudGA__SourceSystemId__c = candidatePatient.rr_Username__c;
        candidatePatient.VTD2_Language__c = 'en_US';
        candidatePatient.VTR2_Pi_Id__c = piUser.Id;
        Test.startTest();
        System.enqueueJob(new VT_D1_TestUtils.CPCreator(candidatePatient));
        Test.stopTest();
        System.assertEquals('Finished', [SELECT VTR4_StatusSTA__c FROM HealthCloudGA__CandidatePatient__c].VTR4_StatusSTA__c);
    }

}