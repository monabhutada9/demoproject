public with sharing class VTR2_SCR_AddressHandler extends Handler {

    public override void onBeforeInsert(Handler.TriggerContext context) {
        AddressInsertValidator validator = new AddressInsertValidator(context);
        validator.validate();
    }

    public override void onAfterUpdate(Handler.TriggerContext context) {
        PrimaryAddressManager manager = new PrimaryAddressManager(context);
        manager.updatePrimaryAddress();

        AddressAfterUpdateValidator validator = new AddressAfterUpdateValidator(context);
        validator.validate();
    }

    public override void onBeforeDelete(Handler.TriggerContext context) {
        AddressDeleteValidator validator = new AddressDeleteValidator(context);
        validator.validate();
    }


    private abstract class AddressValidator {

        private Map<Id, PatientAddresses> patientAddresses;
        private List<ValidationRule> validatorRules;

        public AddressValidator(Handler.TriggerContext context) {
            this.patientAddresses = this.getPatientAddresses(context);
            this.validatorRules = new List<ValidationRule>();
        }

        public void validate() {
            for (ValidationRule validator : this.validatorRules) {
                for (PatientAddresses addresses : this.patientAddresses.values()) {
                    validator.validate(addresses);
                }
            }
        }


        protected abstract Set<Id> getContactAddressIds(Handler.TriggerContext context);

        protected abstract Map<Id, PatientAddresses> addTriggerContextAddresses(Map<Id, PatientAddresses> patientAddresses, Handler.TriggerContext context);


        private Map<Id, PatientAddresses> getPatientAddresses(TriggerContext context) {

            Map<Id, PatientAddresses> result = new Map<Id, PatientAddresses>();
            Set<Id> changedAddressesContactIds = this.getContactAddressIds(context);

            if (!changedAddressesContactIds.isEmpty()) {
                List<Address__c> dbAddresses = this.getDatabaseAddresses(changedAddressesContactIds);
                result = this.convertAddressesToPatientAddresses(dbAddresses);
                result = this.addTriggerContextAddresses(result, context);
            }

            return result;
        }

        private List<Address__c> getDatabaseAddresses(Set<Id> changedAddressesContactIds) {
            return [
                    SELECT Id
                            , VTD1_Primary__c
                            , VTD1_Contact__c
                    FROM Address__c
                    WHERE VTD1_Contact__c IN :changedAddressesContactIds

            ];
        }

        private Map<Id, PatientAddresses> convertAddressesToPatientAddresses(List<Address__c> dbAddresses) {

            Map<Id, PatientAddresses> patientAddresses = new Map<Id, PatientAddresses>();

            for (Address__c address : dbAddresses) {
                Id contactId = address.VTD1_Contact__c;
                if (!patientAddresses.containsKey(contactId)) {
                    patientAddresses.put(contactId, new PatientAddresses(contactId));
                }
                patientAddresses.get(contactId).addDbAddress(address);
            }

            return patientAddresses;
        }

    }

    private class AddressInsertValidator extends AddressValidator {

        public AddressInsertValidator(Handler.TriggerContext context) {
            super(context);
            this.validatorRules.add(new InsertPrimaryAddressValidationRule());
        }

        protected override Set<Id> getContactAddressIds(TriggerContext context) {

            Set<Id> changedAddressesContactIds = new Set<Id>();

            for (Address__c address : (List<Address__c>) context.newList) {
                changedAddressesContactIds.add(address.VTD1_Contact__c);
            }

            return changedAddressesContactIds;
        }

        protected override Map<Id, PatientAddresses> addTriggerContextAddresses(Map<Id, PatientAddresses> patientAddresses, Handler.TriggerContext context) {

            for (Address__c address : (List<Address__c>) context.newList) {
                Id contactId = address.VTD1_Contact__c;
                if (!patientAddresses.containsKey(contactId)) {
                    patientAddresses.put(contactId, new PatientAddresses(contactId));
                }
                patientAddresses.get(contactId).addTriggerAddress(address);
            }

            return patientAddresses;
        }

    }

    private class AddressAfterUpdateValidator extends AddressValidator {

        public AddressAfterUpdateValidator(Handler.TriggerContext context) {
            super(context);
            this.validatorRules.add(new AfterUpdatePrimaryAddressValidationRule());

        }

        protected override Set<Id> getContactAddressIds(TriggerContext context) {

            Set<Id> changedAddressesContactIds = new Set<Id>();
            List<Address__c> oldList = (List<Address__c>) context.oldList;
            Map<Id, Address__c> newMap = (Map<Id, Address__c>) context.newMap;

            for (Address__c oldAddress : oldList) {
                Address__c newAddress = newMap.get(oldAddress.Id);
                changedAddressesContactIds.add(newAddress.VTD1_Contact__c);
                changedAddressesContactIds.add(oldAddress.VTD1_Contact__c);
            }

            return changedAddressesContactIds;
        }

        protected override Map<Id, PatientAddresses> addTriggerContextAddresses(Map<Id, PatientAddresses> patientAddresses, Handler.TriggerContext context) {

            for (Address__c address : (List<Address__c>) context.newList) {
                Id contactId = address.VTD1_Contact__c;
                if (!patientAddresses.containsKey(contactId)) {
                    patientAddresses.put(contactId, new PatientAddresses(contactId));
                }
                patientAddresses.get(contactId).addTriggerAddress(address);
            }

            for (Address__c address : (List<Address__c>) context.oldList) {
                Address__c newAddress = (Address__c) context.newMap.get(address.Id);
                Id contactId = address.VTD1_Contact__c;
                if (!patientAddresses.containsKey(contactId)) {
                    patientAddresses.put(contactId, new PatientAddresses(contactId));
                }
                patientAddresses.get(contactId).addTriggerAddress(newAddress);
            }

            return patientAddresses;
        }

    }

    private class AddressDeleteValidator extends AddressValidator {

        public AddressDeleteValidator(Handler.TriggerContext context) {
            super(context);
            this.validatorRules.add(new DeletePrimaryAddressValidationRule());
        }

        protected override Set<Id> getContactAddressIds(TriggerContext context) {

            Set<Id> changedAddressesContactIds = new Set<Id>();
            for (Address__c address : (List<Address__c>) context.oldList) {
                if (address.VTD1_Primary__c) {
                    changedAddressesContactIds.add(address.VTD1_Contact__c);
                }
            }

            return changedAddressesContactIds;
        }

        protected override Map<Id, PatientAddresses> addTriggerContextAddresses(Map<Id, PatientAddresses> patientAddresses, Handler.TriggerContext context) {

            for (Address__c address : (List<Address__c>) context.oldList) {
                Id contactId = address.VTD1_Contact__c;
                if (!patientAddresses.containsKey(contactId)) {
                    patientAddresses.put(contactId, new PatientAddresses(contactId));
                }
                patientAddresses.get(contactId).addTriggerAddress(address);
            }

            return patientAddresses;
        }

    }


    public interface ValidationRule {
        Boolean validate(PatientAddresses addresses);
    }

    public class InsertPrimaryAddressValidationRule implements ValidationRule {

        public Boolean validate(PatientAddresses addresses) {

            Boolean isValid = false;
            String error;

            Map<Id, Address__c> nonTriggerDbAddresses = addresses.dbAddresses.clone();
            for (Address__c address : addresses.triggerAddresses) {
                if (nonTriggerDbAddresses.containsKey(address.Id)) {
                    nonTriggerDbAddresses.remove(address.Id);
                }
            }

            Integer triggerPrimaryQuantity = addresses.getTriggerPrimaryQuantity();
            Integer quantityNonTriggerPrimaryAddresses = addresses.getPrimaryQuantity(nonTriggerDbAddresses.values());
            Integer finalPrimaryQuantity = quantityNonTriggerPrimaryAddresses + triggerPrimaryQuantity;

            if (finalPrimaryQuantity > 1) {
                error = Label.VTR2_SCR_One_Primary_Address_Allowed;
            } else if (finalPrimaryQuantity == 0) {
                error = Label.VTR2_SCR_Patient_Should_Have_Primary_Address;
            }

            if (error != null) {
                addresses.setErrorToRecords(error);
            } else {
                isValid = true;
            }

            return isValid;
        }
    }

    public class AfterUpdatePrimaryAddressValidationRule implements ValidationRule {

        public Boolean validate(PatientAddresses addresses) {

            Boolean isValid = false;
            String error;

            Integer dbPrimaryQuantity = addresses.getDbPrimaryQuantity();

            if (dbPrimaryQuantity > 1) {
                error = Label.VTR2_SCR_One_Primary_Address_Allowed;
            } else if (dbPrimaryQuantity == 0) {
                error = Label.VTR2_SCR_Patient_Should_Have_Primary_Address;
            }

            if (error != null) {
                addresses.setErrorToRecords(error);
            } else {
                isValid = true;
            }

            return isValid;
        }
    }

    public class DeletePrimaryAddressValidationRule implements ValidationRule {

        public Boolean validate(PatientAddresses addresses) {

            Boolean isValid = false;
            String error;

            Integer triggerPrimaryQuantity = addresses.getTriggerPrimaryQuantity();

            if (triggerPrimaryQuantity != 0) {
                error = Label.VTR2_SCR_Patient_Should_Have_Primary_Address;
                addresses.setErrorToRecords(error);
            } else {
                isValid = true;
            }

            return isValid;
        }
    }


    private class PrimaryAddressManager {

        Handler.TriggerContext context;

        public PrimaryAddressManager(Handler.TriggerContext context) {
            this.context = context;
        }

        private void updatePrimaryAddress() {

            Set<Id> newPrimaries = new Set<Id>();
            Set<Id> contactIds = new Set<Id>();
            List<Address__c> oldList = this.context.oldList;
            Map<Id, Address__c> newMap = (Map<Id, Address__c>) this.context.newMap;

            for (Address__c oldAddress : oldList) {
                Address__c newAddress = newMap.get(oldAddress.Id);
                if (!oldAddress.VTD1_Primary__c && newAddress.VTD1_Primary__c
                        && (oldAddress.VTD1_Contact__c == newAddress.VTD1_Contact__c)) {
                    newPrimaries.add(newAddress.Id);
                    contactIds.add(newAddress.VTD1_Contact__c);
                }
            }

            List<Address__c> addressesToUpdate = [
                    SELECT Id
                            ,Name
                            , VTD1_Primary__c
                    FROM Address__c
                    WHERE Id NOT IN :newPrimaries
                    AND VTD1_Contact__c IN :contactIds
            ];

            for (Address__c address : addressesToUpdate) {
                address.VTD1_Primary__c = false;
            }

            update addressesToUpdate;
        }
    }


    private class PatientAddresses {

        private final Id contactId;
        private final Map<Id, Address__c> dbAddresses;
        private final List<Address__c> triggerAddresses;

        public PatientAddresses(Id contactId) {
            this.contactId = contactId;
            this.dbAddresses = new Map<Id, Address__c>();
            this.triggerAddresses = new List<Address__c>();
        }

        private void addDbAddress(Address__c address) {
            dbAddresses.put(address.Id, address);
        }

        private void addTriggerAddress(Address__c address) {
            triggerAddresses.add(address);
        }

        private Integer getDbPrimaryQuantity() {
            return getPrimaryQuantity(this.dbAddresses.values());
        }

        private Integer getTriggerPrimaryQuantity() {
            return getPrimaryQuantity(this.triggerAddresses);
        }

        private Integer getPrimaryQuantity(List<Address__c> addresses) {
            Integer quantity = 0;
            for (Address__c address : addresses) {
                if (address.VTD1_Primary__c) {
                    quantity++;
                }
            }

            return quantity;
        }

        private void setErrorToRecords(String error) {
            for (Address__c address : this.triggerAddresses) {
                address.addError(error);
            }
        }
    }

}