/**
 * Created by Alexey Mezentsev on 9/15/2020.
 */

public without sharing class VT_R5_GrantAccessForApexClassesBatch implements Database.Batchable<SObject> {
    private Id apexAccessPermissionSetId;

    public VT_R5_GrantAccessForApexClassesBatch() {
        apexAccessPermissionSetId = [SELECT Id FROM PermissionSet WHERE Name = 'UserApexAccess'].Id;
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        if (!Test.isRunningTest()) {
            return Database.getQueryLocator([SELECT Id FROM User WHERE Id NOT IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId = :apexAccessPermissionSetId) AND IsActive = TRUE]);
        } else {
            return Database.getQueryLocator([SELECT Id FROM User WHERE Id NOT IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId = :apexAccessPermissionSetId) AND IsActive = TRUE ORDER BY CreatedDate DESC LIMIT 1]);
        } 
    }

    public void execute(Database.BatchableContext bc, List<User> users) {
        List<PermissionSetAssignment> permissionSetAssignments = new List<PermissionSetAssignment>();
        try {
            for (User user : users) {
                permissionSetAssignments.add(new PermissionSetAssignment(PermissionSetId = apexAccessPermissionSetId, AssigneeId = user.Id));
            }
            Database.insert(permissionSetAssignments, false);
        } catch (Exception e) {
            System.debug(e.getMessage());
        }
    }

    public void finish(Database.BatchableContext bc) {
    }
}