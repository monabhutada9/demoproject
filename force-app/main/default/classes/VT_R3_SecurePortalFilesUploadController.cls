/**
 * @author: Dmitry Yartsev
 * @date: 01.11.2019
 * @description:
 */

@RestResource(UrlMapping='/SecureFileUpload')
global without sharing class VT_R3_SecurePortalFilesUploadController {

    private static final Set<String> CATEGORIES_WITHOUT_CERTIFIED_STATUS = new Set<String>{
            'Order Form',
            'Other'
    };

    private class DocWrapper {
        private Id fileId;
        private String fileName;
        private String category;
        private String nickname;
        private String comments;
    }

    public class RecordObjWrapper {
        private Id userId;
        private String patient;
        private String phone;
        private String visit;
        private String study;
        private Boolean linkIsValid;
    }


    public class CalloutWrapper {
        public String dwListJSON;
        public Id recId;
        public String token;
    }

    @AuraEnabled
    public static VTR3_Temporary_SObject__c getLinkStatus(String recId, String token) {
        List<VTR3_Temporary_SObject__c> linkObjectsList = [SELECT Id, VTR3_LinkStatus__c FROM VTR3_Temporary_SObject__c WHERE VTR3_SObjectID__c =: recId AND VTR3_LinkToken__c =: token];
        if (linkObjectsList.isEmpty()) {
            return null;
        } else {
            return linkObjectsList[0];
        }
    }

    @AuraEnabled
    public static String getVisitObject(String recId, String token) {
        RecordObjWrapper recObj = new RecordObjWrapper();
        List<VTD1_Actual_Visit__c> avList = [
                SELECT Id,
                        Name,
                        VTD1_Case__c,
                        VTD1_Case__r.VTD1_Patient_User__r.Name,
                        VTD1_Case__r.VTD1_Study__r.VTD1_Protocol_Nickname__c,
                        VTD1_Case__r.VTD1_Virtual_Site__r.VTR3_Study_Geography__c,
                        VTD1_Case__r.VTD1_Study__c,
                        Unscheduled_Visits__r.VTD1_Patient_User__r.Name,
                        Unscheduled_Visits__r.VTD1_Study__r.VTD1_Protocol_Nickname__c,
                        Unscheduled_Visits__r.VTD1_Virtual_Site__r.VTR3_Study_Geography__c,
                        Unscheduled_Visits__r.VTD1_Study__c,
                        VTD1_Case__r.VTD2_Study_Geography__c
                FROM VTD1_Actual_Visit__c
                WHERE Id = :recId
        ];
        if (!avList.isEmpty()) {
            VTD1_Actual_Visit__c av = avList[0];
            Id studyGeography;
            recObj.study = av.VTD1_Case__c == null ? av.Unscheduled_Visits__r.VTD1_Study__r.VTD1_Protocol_Nickname__c : av.VTD1_Case__r.VTD1_Study__r.VTD1_Protocol_Nickname__c;
            recObj.patient = av.VTD1_Case__c == null ? av.Unscheduled_Visits__r.VTD1_Patient_User__r.Name : av.VTD1_Case__r.VTD1_Patient_User__r.Name;
            studyGeography = av.VTD1_Case__c == null ? av.Unscheduled_Visits__r.VTD1_Virtual_Site__r.VTR3_Study_Geography__c : av.VTD1_Case__r.VTD1_Virtual_Site__r.VTR3_Study_Geography__c;
            recObj.visit = av.Name;
            recObj.userId = UserInfo.getUserId();
            recObj.linkIsValid = linkIsValid(recId, token);
            recObj.phone = getStudyPhone(studyGeography);
        } else {
            recObj.linkIsValid = null;
        }
        return JSON.serialize(recObj);
    }

    @AuraEnabled
    public static String getCaseObject(String recId, String token) {
        RecordObjWrapper recObj = new RecordObjWrapper();
        List<Case> casList = [
                SELECT Id,
                        VTD1_Patient_User__r.Name,
                        VTD1_Study__r.VTD1_Protocol_Nickname__c,
                        VTD1_Virtual_Site__r.VTR3_Study_Geography__c
                FROM Case
                WHERE Id = :recId
        ];
        if (!casList.isEmpty()) {
            Case cas = casList[0];
            Id studyGeography;
            recObj.study = cas.VTD1_Study__r.VTD1_Protocol_Nickname__c;
            recObj.patient = cas.VTD1_Patient_User__r.Name;
            studyGeography = cas.VTD1_Virtual_Site__r.VTR3_Study_Geography__c;
            recObj.userId = UserInfo.getUserId();
            recObj.linkIsValid = linkIsValid(recId, token);
            recObj.phone = getStudyPhone(studyGeography);
        } else {
            recObj.linkIsValid = null;
        }
        return JSON.serialize(recObj);
    }

    @AuraEnabled
    public static String uploadDocs(String docWrapperListJSON, Id recId, String token) {
        String responseBody;
        try {
            CalloutWrapper cw = new CalloutWrapper();
            cw.token = token;
            cw.recId = recId;
            cw.dwListJSON = docWrapperListJSON;
            HttpRequest req = new HttpRequest();
            req.setTimeout(120000);
            req.setMethod('POST');
            req.setHeader('Authorization', 'Bearer ' + VT_D1_HelperClass.getSessionIdForGuest());
            req.setHeader('Content-Type', 'application/json');
            req.setEndpoint(System.Url.getOrgDomainUrl().toExternalForm() + '/services/apexrest/SecureFileUpload');
            req.setBody(JSON.serialize(cw));
            Http http = new Http();
            HttpResponse res = http.send(req);
            System.debug(res.getStatus() + ' ' + res.getBody());
            responseBody = res.getBody();
            List<VTR3_Temporary_SObject__c> tempSObj = [SELECT Id, VTR3_LinkStatus__c FROM VTR3_Temporary_SObject__c WHERE VTR3_SObjectID__c =: cw.recId AND VTR3_LinkToken__c =: cw.token];
            tempSObj[0].VTR3_LinkStatus__c = responseBody;
            update tempSObj;
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage() + '\n' + ex.getStackTraceString());
        }
        return responseBody;
    }

    @HttpPost
    global static void doPost() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        String requestBody = request.requestBody.toString();
        try {
            insertDocs(requestBody);
            response.responseBody = Blob.valueOf('In Progress');
            response.statusCode = 201;
        } catch (Exception e) {
            response.responseBody = Blob.valueOf('Error: ' + e.getMessage());
            response.statusCode = 500;
        }
    }

    private static Boolean linkIsValid(String recId, String token) {
        List<VTR3_Temporary_SObject__c> tempSObj = [SELECT Id FROM VTR3_Temporary_SObject__c WHERE VTR3_SObjectID__c =: recId AND VTR3_LinkToken__c =: token];
        return !tempSObj.isEmpty();
    }

    private static String getStudyPhone(Id studyGeography) {
        String phone;
        List<VTR2_StudyPhoneNumber__c> phoneNumbersList = [
                SELECT Name
                FROM VTR2_StudyPhoneNumber__c
                WHERE VTR2_Study_Geography__c = :studyGeography
                AND VTR2_Language__c = 'en_US'
                LIMIT 1
        ];
        if (!phoneNumbersList.isEmpty()) {
            phone = phoneNumbersList[0].Name;
        } else {
            List<VTR2_StudyPhoneNumber__c> phoneNumbersList2 = [
                    SELECT Name
                    FROM VTR2_StudyPhoneNumber__c
                    WHERE VTR2_Study_Geography__c = :studyGeography
                    ORDER BY LastModifiedDate DESC
                    LIMIT 1
            ];
            if (!phoneNumbersList2.isEmpty()) {
                phone = phoneNumbersList2[0].Name;
            }
        }
        return phone;
    }

    @Future(Callout = true)
    private static void insertDocs(String calloutWrapperJSON) {
        insertDocs((CalloutWrapper)JSON.deserialize(calloutWrapperJSON, CalloutWrapper.class));
    }

    @TestVisible
    private static List<VTD1_Document__c> insertDocs(CalloutWrapper cw) {
        List<DocWrapper> docWrapperList = (List<DocWrapper>) JSON.deserialize(cw.dwListJSON, List<DocWrapper>.class);
        Boolean isCase = cw.recId.getSobjectType() == Case.getSObjectType();
        List<VTD1_Actual_Visit__c> avList = new List<VTD1_Actual_Visit__c>();
        Id casId;
        if (!isCase) {
            avList = [SELECT VTD1_Case__c, Unscheduled_Visits__c FROM VTD1_Actual_Visit__c WHERE Id = :cw.recId];
            if (!avList.isEmpty()) {
                casId = avList[0].VTD1_Case__c == null ? avList[0].Unscheduled_Visits__c : avList[0].VTD1_Case__c;
            }
        }
        List<VTR3_Temporary_SObject__c> tempSObj = [SELECT Id, VTR3_LinkStatus__c FROM VTR3_Temporary_SObject__c WHERE VTR3_SObjectID__c =: cw.recId AND VTR3_LinkToken__c =: cw.token];
        if (tempSObj.isEmpty()) { return null; }

        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        List<VTD1_Document__c> docList = new List<VTD1_Document__c>();
        for (DocWrapper dw : docWrapperList) {
            if (isCase) {
                docList.add(new VTD1_Document__c(
                        VTD1_Clinical_Study_Membership__c = cw.recId,
                        VTD1_Flagged_for_Deletion__c = false,
                        VTD1_Nickname__c = dw.nickname,
                        VTD1_FileNames__c = dw.fileName,
                        VTD1_Comment__c = dw.comments,
                        RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD,
                        VTD1_Files_have_not_been_edited__c = true,
                        VTR3_Upload_File_Completed__c = true,
                        VTD1_Status__c = 'Pending Approval'
                ));
            } else {
                docList.add(new VTD1_Document__c(
                        VTD1_Clinical_Study_Membership__c = casId,
                        VTD1_Flagged_for_Deletion__c = false,
                        VTR3_Category__c = dw.category,
                        VTR3_Associated_Visit__c = cw.recId,
                        VTD1_Nickname__c = dw.nickname,
                        VTD1_Comment__c = dw.comments,
                        VTD1_Status__c = CATEGORIES_WITHOUT_CERTIFIED_STATUS.contains(dw.category) ? 'No Certification Required' : 'Certified',
                        VTD1_FileNames__c = dw.fileName,
                        RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT
                ));
            }
            cdlList.add(new ContentDocumentLink(
                    ContentDocumentId = dw.fileId,
                    ShareType = 'V'
            ));
        }
        try {
            delete tempSObj;
            insert docList;
            for (Integer i = 0; i < cdlList.size(); i++) {
                cdlList[i].LinkedEntityId = docList[i].Id;
            }
            if (!cdlList.isEmpty()) {
                insert cdlList;
            }
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            tempSObj[0].VTR3_LinkStatus__c = 'Error: ' + ex.getMessage();
            update tempSObj;
        }
        return docList;
    }

    private static String generateToken(Integer len) {
        final String symbols = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        String randomString = '';
        while (randomString.length() < len) {
            Integer index = Math.mod(Math.abs(Crypto.getRandomInteger()), symbols.length());
            randomString += symbols.substring(index, index + 1);
        }

        return randomString;
    }

    @AuraEnabled
    public static String generateLink(Id recId, Integer len) {
        Boolean isCase = recId.getSobjectType() == Case.getSObjectType();
        String linkPath;
        String token = generateToken(len);
        if (isCase) {
            recId = [SELECT Id FROM Case WHERE Id =: recId].Id;
            linkPath = '/medical-records-upload-external?language=en_US';
        } else {
            recId = [SELECT Id FROM VTD1_Actual_Visit__c WHERE Id =: recId].Id;
            linkPath = '/visit-documents-upload-external?language=en_US';
        }
        VTR3_Temporary_SObject__c tempSObj = new VTR3_Temporary_SObject__c(
                VTR3_SObjectID__c = recId,
                VTR3_LinkToken__c = token
        );
        try {
            insert tempSObj;
            String link = VTD1_RTId__c.getInstance().VTD2_Patient_Community_URL__c + linkPath + '&recId=' + recId + '&token=' + token;
            return link;
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }

    }
}