@IsTest
public with sharing class VT_D1_PIStudyDocumentsControllerTest {

    public static void getPatientDocumentsTest() {
        User u;

        Test.startTest();
        List<Case> casesList = [SELECT VTD1_PI_user__c FROM Case];
        System.debug(casesList);
        System.assert(casesList.size() > 0);
        if (casesList.size() > 0) {
            Case cas = casesList.get(0);

            VTD1_Document__c doc = new VTD1_Document__c();
            doc.VTD1_Clinical_Study_Membership__c = cas.Id;
            doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSR;
            insert doc;
            System.debug([
                    SELECT
                            Id,
                            Name,
                            VTD1_FileNames__c,
                            RecordType.Name,
                            CreatedDate,
                            VTD1_Version__c,
                            VTD2_TMA_Eligibility_Decision__c,
                            VTD2_TMA_Comments__c,
                            VTD2_Template__c
                    FROM VTD1_Document__c
                    WHERE VTD1_Clinical_Study_Membership__c = :cas.Id
                    AND RecordTypeId IN ( :VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE
                            , :VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSR)
                    ORDER BY Name ASC
            ]);
            ContentVersion contentVersion = new ContentVersion();
            contentVersion.PathOnClient = 'test.txt';
            contentVersion.Title = 'Test file';
            contentVersion.VersionData = Blob.valueOf('Test Data');
            insert contentVersion;

            ContentVersion contentVersion1 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];

            ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
            contentDocumentLink.LinkedEntityId = doc.Id;
            contentDocumentLink.ContentDocumentId = contentVersion1.ContentDocumentId;
            contentDocumentLink.ShareType = 'I';
            contentDocumentLink.Visibility = 'AllUsers';
            insert contentDocumentLink;

            String userId = cas.VTD1_PI_user__c;
            u = [SELECT Id FROM User WHERE Id = :userId];

            System.debug('runAsUnder' + [
                    SELECT
                            Id,
                            Name,
                            VTD1_FileNames__c,
                            RecordType.Name,
                            CreatedDate,
                            VTD1_Version__c,
                            VTD2_TMA_Eligibility_Decision__c,
                            VTD2_TMA_Comments__c,
                            VTD2_Template__c
                    FROM VTD1_Document__c
                    WHERE VTD1_Clinical_Study_Membership__c = :cas.Id
                    AND RecordTypeId IN ( :VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE
                            , :VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSR)
                    ORDER BY Name ASC
            ]);
            String patientDocumentsWithAdditionalInformationString = VT_D1_PIStudyDocumentsController.getPatientDocuments(cas.Id);
            VT_D1_PIStudyDocumentsController.PatientDocumentsWithAdditionalInformation patientDocumentsWithAdditionalInformation = (VT_D1_PIStudyDocumentsController.PatientDocumentsWithAdditionalInformation) JSON.deserialize(patientDocumentsWithAdditionalInformationString, VT_D1_PIStudyDocumentsController.PatientDocumentsWithAdditionalInformation.class);


        }
        Test.stopTest();
    }

    public static void testIsSandbox() {
        System.assertEquals(false, VT_D1_PIStudyDocumentsController.isSandbox());
    }
}