/**
 * Created by user on 23.10.2019.
 */

@isTest
public with sharing class VT_R2_SCRTaskListControllerTest {
    
    public static void test() {
        Case caseObj = [select Id from Case];
        List <IMP_Replacement_Form__c> replacementForms = new List<IMP_Replacement_Form__c>();
        replacementForms.add(new IMP_Replacement_Form__c(VTD1_Replacement_Decision__c = 'decision', VTD1_Case__c = caseObj.Id));
        replacementForms.add(new IMP_Replacement_Form__c(VTD1_Case__c = caseObj.Id));
        insert replacementForms;
        VT_R2_SCRTaskListController.canCloseTask(UserInfo.getUserId());
        VT_R2_SCRTaskListController.canCloseTask(replacementForms[0].Id);
        VT_R2_SCRTaskListController.canCloseTask(replacementForms[1].Id);
        Task task = new Task(WhatId = replacementForms[0].Id);
        insert task;
        VT_R2_SCRTaskListController.closeTask(task.Id);
    }
}