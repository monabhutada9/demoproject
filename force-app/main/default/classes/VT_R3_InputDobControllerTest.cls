@IsTest
public without sharing class VT_R3_InputDobControllerTest {

    private static final String YEAR_ONLY = 'Year Only';

    public static void testGetContactSuccess() {
        Case patient = [
                SELECT Id, ContactId, VTD2_Study_Geography__c, Status
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
                AND Subject = 'VT_R5_allTest'
                LIMIT 1
        ];
        Contact cont = [SELECT Id, VTD1_Clinical_Study_Membership__c FROM Contact WHERE id = :patient.ContactId];
        cont.VTD1_Clinical_Study_Membership__c = patient.Id;
        update cont;

        Test.startTest();
        Contact contact = VT_R3_InputDobController.getContact(cont.Id);
        Test.stopTest();

        System.assertNotEquals(null, contact);
        System.assertEquals(
                YEAR_ONLY,
                contact.VTD1_Clinical_Study_Membership__r.VTD2_Study_Geography__r.VT_R3_Date_Of_Birth_Restriction__c,
                'Incorrect VT_R3_Date_Of_Birth_Restriction__c value'
        );
    }

    public static void testGetStudyGeographySuccess() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];

        Test.startTest();
        VTD2_Study_Geography__c studyGeography = VT_R3_InputDobController.getStudyGeography(study.Id, 'US');
        Test.stopTest();

        System.assertNotEquals(null, studyGeography);
        System.assertEquals(
                YEAR_ONLY,
                studyGeography.VT_R3_Date_Of_Birth_Restriction__c
        );
    }
}