/**
 * @author: Alexander Komarov
 * @date: 21.05.2020
 * @description:
 */

public with sharing class VT_R5_MobileResources extends VT_R5_MobileRestResponse implements VT_R5_MobileRestRouter.Routable {

    static Integer consentGuidesOffset = 0;//DEFAULT VALUE
    static Integer consentGuidesLimit = 200;//DEFAULT VALUE
    static Contact currentContact;
    static Case currentCase;
    static List<String> approvedLanguages;

    private static final String DOCUMENT_ACCESS_LEVEL = 'Read';
    private static final String DOCUMENT_ROW_CAUSE = 'Manual';

    private static final Map<String, String> ARTICLE_TYPES_BY_REQUEST = new Map<String, String>{
            'study-details' => VT_R5_KnowledgeArticleService.STUDY_DETAILS_ARTICLE_TYPE,
            'privacy-notices' => VT_R5_KnowledgeArticleService.PRIVACY_NOTICE_ARTICLE_TYPE,
            'training-materials' => VT_R5_KnowledgeArticleService.TRAINING_MATERIALS_ARTICLE_TYPE,
            'terms-and-conditions' => ''
    };

    public List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/resources/{type}/{id}',
                '/patient/{version}/resources/{type}'
        };
    }

    public void versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        String requestType = parameters.get('type');
        String requestId = parameters.get('id');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestType {
                    when 'privacy-notices', 'training-materials', 'study-details' {
                        switch on requestVersion {
                            when 'v1' {
                                getStudyArticle_v1(ARTICLE_TYPES_BY_REQUEST.get(requestType), requestId);
                                return;
                            }
                        }
                    }
                    when 'terms-and-conditions' {
                        switch on requestVersion {
                            when 'v1' {
                                if (String.isNotBlank(requestId)) {
                                    getStudyArticle_v1(ARTICLE_TYPES_BY_REQUEST.get(requestType), requestId);
                                } else {
                                    getTermsAndConditions_v1();
                                }
                                return;
                            }
                        }
                    }
                    when 'informed-consent-guides' {
                        switch on requestVersion {
                            when 'v1' {
                                getInformedConsentGuides_v1();
                                return;
                            }
                        }
                    }
                }
            }
        }
        VT_R5_MobileRestRouter.unsupportedVersionResponse();
        this.addError('unsupported version');
        this.sendResponse();
    }

    private void getInformedConsentGuides_v1() {
        try {
            parseParams(this.request);
            MobileListDocumentWrapper mobileListDocumentWrapper = new MobileListDocumentWrapper();
            this.buildResponse(mobileListDocumentWrapper);
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private void getStudyArticle_v1(String articleType, String articleId) {
        try {
            VT_R5_KnowledgeArticleService.KnowledgeArticles article =
                    new VT_R5_KnowledgeArticleService(
                            currentContact.VTR2_Primary_Language__c,
                            currentCase.VTD1_Study__c
                    )
                            .getArticle(articleId, articleType);
            this.buildResponse(article);
            System.debug('>>> ' + JSON.serializePretty(article));
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private void getTermsAndConditions_v1() {
        try {
            List<VT_R5_KnowledgeArticleService.KnowledgeArticles> articles =
                    new VT_R5_KnowledgeArticleService(
                            currentContact.VTR2_Primary_Language__c,
                            currentCase.VTD1_Study__c
                    )
                            .getArticlesPreview(VT_R5_KnowledgeArticleService.TERM_AND_CONDITIONS_ARTICLE_TYPES);
            this.buildResponse(articles);
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private static void parseParams(RestRequest request) {
        if (request.params.containsKey('offset')) {
            consentGuidesOffset = Integer.valueOf(request.params.get('offset'));
        }
        if (request.params.containsKey('limit')) {
            consentGuidesLimit = Integer.valueOf(request.params.get('limit'));
        }
    }

    private static void getCaseAndContact() {
//        currentContact = [SELECT VTD1_Clinical_Study_Membership__c, VTR2_Primary_Language__c FROM Contact WHERE Id IN (SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()) LIMIT 1];
//        currentCase = [SELECT Id, VTD2_Study_Geography__c, VTD1_Study__c, VTD1_Virtual_Site__r.VTR3_Study_Geography__c FROM Case WHERE Id = :currentContact.VTD1_Clinical_Study_Membership__c LIMIT 1];

        List<Contact> contacts = (List<Contact>) new VT_Stubber.ResultStub('CurrentContact',
        [
                SELECT VTD1_Clinical_Study_Membership__c, VTR2_Primary_Language__c
                FROM Contact
                WHERE Id IN (
                        SELECT ContactId
                        FROM User
                        WHERE Id = :UserInfo.getUserId()
                )
                LIMIT 1
        ]
        ).getResult();
        currentContact = contacts[0];
        List<Case> cases = (List<Case>) new VT_Stubber.ResultStub('CurrentCase',
        [
                SELECT Id, VTD2_Study_Geography__c, VTD1_Study__c,
                        VTD1_Virtual_Site__r.VTR3_Study_Geography__c,
                        VTD1_Virtual_Site__r.VTR5_DefaultIRBLanguage__c,
                        VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c
                FROM Case
                WHERE Id = :currentContact.VTD1_Clinical_Study_Membership__c
                LIMIT 1
        ]
        ).getResult();
        currentCase = cases[0];
        if (currentCase.VTD1_Virtual_Site__r.VTR5_DefaultIRBLanguage__c != null
                && currentCase.VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c != null) {
            approvedLanguages = new List<String>{
                    currentCase.VTD1_Virtual_Site__r.VTR5_DefaultIRBLanguage__c
            };
            approvedLanguages.addAll(currentCase.VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c.split(';'));
        }
    }

    public void initService() {
        getCaseAndContact();
    }

    private without sharing class MobileListDocumentWrapper {
        private List<MobileEConsentGuide> guidesList;
        private MobileEConsentWrapperInfo metaInfo;

        MobileListDocumentWrapper() {
            this.metaInfo = new MobileEConsentWrapperInfo();

            String currentLanguage = currentContact.VTR2_Primary_Language__c;

            List<VTD1_Document__c> documentsWithDefaults = getInfGuidesDocumentsWithDefaults(currentLanguage);

            List<VTD1_Document__c> documentsIdsSameLanguage = new List<VTD1_Document__c>();
            List<VTD1_Document__c> documentsIdsDefaults = new List<VTD1_Document__c>();
            for (VTD1_Document__c document : documentsWithDefaults) {
                if (document.VTR2_Language__c != null && document.VTR2_Language__c.equals(currentLanguage)) {
                    documentsIdsSameLanguage.add(document);
                } else if (document.VTR5_DefaultDocument__c) {
                    documentsIdsDefaults.add(document);
                }
            }

            List<VTD1_Document__c> documentsToReturn = documentsIdsSameLanguage.isEmpty() ? documentsIdsDefaults : documentsIdsSameLanguage;
            this.metaInfo.totalGuides = documentsToReturn.size();

            List<MobileEConsentGuide> guides = new List<MobileEConsentGuide>();
            if (!documentsToReturn.isEmpty()) {
                shareDocuments(documentsToReturn);

                Set<Id> docToReturnIds = new Set<Id>();
                for (Integer i = consentGuidesOffset; i < documentsToReturn.size() && i < consentGuidesOffset + consentGuidesLimit; i++) {
                    docToReturnIds.add(documentsToReturn[i].Id);
                }

                List<ContentDocumentLink> infGuidesLinks = getInfGuidesLinks(docToReturnIds);
                Map<Id, ContentDocumentLink> documentLinkMap = new Map<Id, ContentDocumentLink>();
                Map<Id, Id> contentDocumentToLatestContentVersion = new Map<Id, Id>();

                for (ContentDocumentLink link : infGuidesLinks) {
                    contentDocumentToLatestContentVersion.put(link.ContentDocumentId, null);
                    documentLinkMap.put(link.LinkedEntityId, link);
                }

                List<ContentVersion> contentVersions = getInfGuidesContentVersions(contentDocumentToLatestContentVersion);
                for (ContentVersion cv : contentVersions) {
                    contentDocumentToLatestContentVersion.put(cv.ContentDocumentId, cv.Id);
                }

                Map<Id, VTD1_Document__c> documentMap = getInfGuidesDocumentMap(documentLinkMap);

                for (ContentDocumentLink currentLink : documentLinkMap.values()) {
                    guides.add(new MobileEConsentGuide(
                            documentMap.get(currentLink.LinkedEntityId),
                            currentLink,
                            contentDocumentToLatestContentVersion.get(currentLink.ContentDocumentId))
                    );
                }
            }
            this.guidesList = guides;
        }

        private Map<Id, VTD1_Document__c> getInfGuidesDocumentMap(Map<Id, ContentDocumentLink> documentLinkMap) {
            Map<Id, VTD1_Document__c> documentMap = new Map<Id, VTD1_Document__c>();
            List<VTD1_Document__c> documentList = (List<VTD1_Document__c>) new VT_Stubber.ResultStub('infGuidesDocuments', [
                    SELECT Id, VTR5_DocumentTitle__c, VTR5_DocumentVersion__c, VTR2_Language__c
                    FROM VTD1_Document__c
                    WHERE Id IN :documentLinkMap.keySet()
            ]).getResult();
            for (VTD1_Document__c d : documentList) {
                documentMap.put(d.Id, d);
            }
            return documentMap;
        }

        private List<ContentVersion> getInfGuidesContentVersions(Map<Id, Id> contentDocumentToLatestContentVersion) {
            List<ContentVersion> contentVersions = (List<ContentVersion>) new VT_Stubber.ResultStub(
                    'infGuidesContentVersions',
            [
                    SELECT Id,ContentDocumentId
                    FROM ContentVersion
                    WHERE ContentDocumentId = :contentDocumentToLatestContentVersion.keySet()
                    AND IsLatest = TRUE
            ]
            ).getResult();
            return contentVersions;
        }

        private List<ContentDocumentLink> getInfGuidesLinks(Set<Id> docToReturnIds) {
            List<ContentDocumentLink> infGuidesLinks = (List<ContentDocumentLink>) new VT_Stubber.ResultStub(
                    'infGuidesLinks',
            [
                    SELECT
                            Id, LinkedEntityId, ContentDocumentId, ContentDocument.LastModifiedDate
                    FROM ContentDocumentLink
                    WHERE LinkedEntityId IN :docToReturnIds
                    ORDER BY ContentDocument.LastModifiedDate ASC
            ]
            ).getResult();
            return infGuidesLinks;
        }

        private List<VTD1_Document__c> getInfGuidesDocumentsWithDefaults(String currentLanguage) {
            List<VTD1_Document__c> documentsWithDefaults = (List<VTD1_Document__c>) new VT_Stubber.ResultStub(
                    'infGuidesDocumentsWithDefaults',
            [
                    SELECT Id,VTR2_Language__c,VTR5_DefaultDocument__c
                    FROM VTD1_Document__c
                    WHERE VTR2_Geography__c = :currentCase.VTD1_Virtual_Site__r.VTR3_Study_Geography__c
                    AND RecordTypeId = :VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_INFORMED_CONSENT_GUIDE
                    AND (VTR2_Language__c = :currentLanguage
                    OR VTR5_DefaultDocument__c = TRUE)
                    AND VTD1_Study__c = :currentCase.VTD1_Study__c
                    ORDER BY CreatedDate DESC
            ]
            ).getResult();
            return documentsWithDefaults;
        }
        private void shareDocuments(List<VTD1_Document__c> docToShare) {
            List<VTD1_Document__Share> shareList = new List<VTD1_Document__Share>();
            for (VTD1_Document__c currentDoc : docToShare) {
                shareList.add(new VTD1_Document__Share(ParentId = currentDoc.Id, UserOrGroupId = UserInfo.getUserId(), AccessLevel = DOCUMENT_ACCESS_LEVEL, RowCause = DOCUMENT_ROW_CAUSE));
            }
            if (!shareList.isEmpty() && !Test.isRunningTest()) insert shareList;
        }
    }

    private without sharing class MobileEConsentWrapperInfo {
        private Integer totalGuides;
        private Integer queryOffset;
        private Integer queryLimit;

        MobileEConsentWrapperInfo() {
            queryOffset = consentGuidesOffset;
            queryLimit = consentGuidesLimit;
        }
    }

    public class MobileEConsentGuide {
        String title;
        String version;
        String id;
        Long lastModifiedDate;
        String url;

        MobileEConsentGuide(VTD1_Document__c document, ContentDocumentLink link, Id latestContentVersion) {
            this.id = link.ContentDocumentId;
            this.version = document.VTR5_DocumentVersion__c;
            this.title = document.VTR5_DocumentTitle__c;
            this.lastModifiedDate = link.ContentDocument.LastModifiedDate.getTime();
            this.url = System.Url.getSalesforceBaseUrl().toExternalForm() + VT_D1_HelperClass.getSandboxPrefix() + '/services/data/v46.0/sobjects/ContentVersion/' + latestContentVersion + '/VersionData';
        }
    }

}