/**
 * Created by N.Arbatskiy on 02.11.2020.
 */
@IsTest
public class VT_R5_DiaryReviewingModalControllerTest {
    private static final String UPDATED_SCORE = '10';
    private static List<String>fieldsToCheck = new List<String>{
            '"isBranching":true'
    };

    public static void getDataTest() {
        VTD1_Survey__c survey = getSurveyId(true);
        fieldsToCheck.addAll(new List<String>{
                '"name":"' + survey.Name + '"',
                '"id":"' + getAnswerId(survey.Id) + '"',
                '"reviewed":false',
                '"score":null'
        });
        assertFields(JSON.serialize(VT_R5_DiaryReviewingModalController.getData(survey.Id)), fieldsToCheck);
    }

    public static void updateScoringTest() {
        VTD1_Survey__c survey = getSurveyId(true);
        VT_R5_DiaryReviewingModalController.reviewSurveyWithScore(
                survey.Id,
                '[{"id":"' + getAnswerId(survey.Id) + '","score":' + UPDATED_SCORE + '}]'
        );
        fieldsToCheck.addAll(new List<String>{
                '"reviewed":true',
                '"score":' + UPDATED_SCORE
        });
        assertFields(JSON.serialize(VT_R5_DiaryReviewingModalController.getData(survey.Id)), fieldsToCheck);
    }

    private static VTD1_Survey__c getSurveyId(Boolean isBranching) {
        return [
                SELECT Id,Name
                FROM VTD1_Survey__c
                WHERE VTD1_Protocol_ePRO__r.VTR4_Is_Branching_eDiary__c = :isBranching
                LIMIT 1
        ];
    }

    private static Id getAnswerId(Id surveyId) {
        return [
                SELECT Id
                FROM VTD1_Survey_Answer__c
                WHERE VTD1_Survey__c = :surveyId
                LIMIT 1
        ].Id;
    }

    private static void assertFields(String serializedJson, List<String>fields) {
        for (String field : fields) {
            System.assert(serializedJson.contains(field), 'INCORRECT FIELD: ' + field);
        }
    }
}