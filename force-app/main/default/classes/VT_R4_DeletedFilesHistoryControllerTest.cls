/**
 * Created by user on 20-May-20.
 * Aleksandr Mazan
 */
@IsTest
public with sharing class VT_R4_DeletedFilesHistoryControllerTest {


    public static void documentFieldHistoryTest() {
        try {
            VT_R4_DocumentHistoryTestDataCreate testDataCreate = new VT_R4_DocumentHistoryTestDataCreate();
            testDataCreate.CreateTestData();
            Test.startTest();
            List<VT_R4_DeletedFilesHistoryController.FileInformation> testData = VT_R4_DeletedFilesHistoryController.getDocumentHistory('{"sortingParams": null,"filterParams": [],"searchParams": null,"entriesOnPage": 10,"currentPage": 1}');
            Test.stopTest();
            System.assertEquals(testData.size(), 2);
        } catch (Exception e) {
            System.debug(e.getMessage());
        }
    }

    private without sharing class VT_R4_DocumentHistoryTestDataCreate {

        private List<HealthCloudGA__CarePlanTemplate__c>  CreateTestStudyData() {
            HealthCloudGA__CarePlanTemplate__c newStudy = new HealthCloudGA__CarePlanTemplate__c(Name = 'A Test study');
            List<HealthCloudGA__CarePlanTemplate__c> testStudies = new List<HealthCloudGA__CarePlanTemplate__c>{
                    newStudy
            };
            insert testStudies;
            return testStudies;
        }


        private List<Virtual_Site__c>  CreateTestSiteData(List<HealthCloudGA__CarePlanTemplate__c> studies) {
            Virtual_Site__c newSite = new Virtual_Site__c(Name = '1 Test site', VTD1_Study__c = studies[0].Id, VTD1_Study_Site_Number__c = 'test');
            Virtual_Site__c newSite1 = new Virtual_Site__c(Name = 'D Test site', VTD1_Study__c = studies[0].Id, VTD1_Study_Site_Number__c = 'test2');
            Virtual_Site__c newSite2 = new Virtual_Site__c(Name = 'M Test site', VTD1_Study__c = studies[0].Id, VTD1_Study_Site_Number__c = 'test3');
            Virtual_Site__c newSite3 = new Virtual_Site__c(Name = 'D Test site', VTD1_Study__c = studies[0].Id, VTD1_Study_Site_Number__c = 'test4');
            List<Virtual_Site__c> testSites = new List<Virtual_Site__c>{
                    newSite, newSite1, newSite2, newSite3
            };
            insert testSites;
            return testSites;
        }


        private List<VTD1_Document__c>  CreateTestDocData(List<HealthCloudGA__CarePlanTemplate__c> studies, List<Virtual_Site__c> sites) {
            VTD1_Document__c newDoc1 = new VTD1_Document__c(VTD1_FileNames__c = 'A 21', Document_Name__c = '1', VTD1_Site__c = sites[0].Id);
            VTD1_Document__c newDoc2 = new VTD1_Document__c(VTD1_FileNames__c = 'F 21', Document_Name__c = '2', VTD1_Site__c = sites[0].Id);
            VTD1_Document__c newDoc3 = new VTD1_Document__c(VTD1_FileNames__c = 'B 21', Document_Name__c = '3', VTD1_Site__c = sites[1].Id);
            VTD1_Document__c newDoc4 = new VTD1_Document__c(VTD1_FileNames__c = '11', Document_Name__c = '4', VTD1_Site__c = sites[2].Id);
            VTD1_Document__c newDoc5 = new VTD1_Document__c(VTD1_FileNames__c = 'DR15', Document_Name__c = '5',VTD1_Site__c = sites[3].Id);
            VTD1_Document__c newDoc6 = new VTD1_Document__c(VTD1_FileNames__c = 'B 21', Document_Name__c = '6', VTD1_Study__c = studies[0].Id);
            VTD1_Document__c newDoc7 = new VTD1_Document__c(VTD1_FileNames__c = 'Test', Document_Name__c = '7', VTD1_Study__c = studies[0].Id);
            List<VTD1_Document__c> newDocsList = new List<VTD1_Document__c>{
                    newDoc1, newDoc2, newDoc3, newDoc4, newDoc5, newDoc6, newDoc7
            };
            insert newDocsList;
            System.debug(newDocsList);
            return newDocsList;
        }

        private void  CreateTestDocHistoryData(List<VTD1_Document__c> docs) {
            List<VTD1_Document__History> docsHistory = new List<VTD1_Document__History>();
            docsHistory.add(new VTD1_Document__History(ParentId = docs[0].Id, Field = VT_R4_DeletedFilesHistoryController.VT_R4_DELETION_FILE_NAME));
            docsHistory.add(new VTD1_Document__History(ParentId = docs[0].Id, Field = VT_R4_DeletedFilesHistoryController.VTD1_DOCUMENT_DATE_DELETED));
            insert docsHistory;
        }

        private void  CreateTestData() {
            List<HealthCloudGA__CarePlanTemplate__c> testStudies = CreateTestStudyData();
            List<Virtual_Site__c> testSites = CreateTestSiteData(testStudies);
            List<VTD1_Document__c> testDocs = CreateTestDocData(testStudies, testSites);
            CreateTestDocHistoryData(testDocs);
        }
    }
}