/**
 * Created by Leonid Bartenev
 */

public class VT_D1_RequestBuilder_Order implements VT_D1_RequestBuilder {
    
    // Data classes: ---------------------------------------------------------------------------------------------------
    public class Delivery {
        public String protocolId;
        public String subjectId;
        public String shipmentId;
        public Boolean receivedStateOk;
        public Date receivedDate;
        public Boolean receivedAll;
        public List<Kit> kits;
        public AuditLog auditLog;
    }

    public class Kit {
        public String kitName;
        public String materialId;
        public Integer quantity;
        public String kitType;
        public String deliveryStatus;
    }
    
    public class AuditLog {
        public String UserId;
        public Datetime Timestamp;
        
        public AuditLog(){
            UserId = UserInfo.getUserId();
            Timestamp = Datetime.now();
        }
    }
    
    private Id orderId;
    private String apiVersion;
    
    public VT_D1_RequestBuilder_Order(Id orderId, String apiVersion) {
        this.orderId = orderId;
        this.apiVersion = apiVersion;
    }

    // Build logic: ----------------------------------------------------------------------------------------------------
    public String buildRequestBody() {
        VTD1_Order__c orderDelivery = [
                SELECT Id, VTD1_ShipmentId__c,
                        VTD1_ExpectedDeliveryDateTime__c,
                        VTR2_Received_All__c,
                        //VTD1_Patient_Kit__r.VTD1_Kit_Not_Received__c,
                        //VTD1_Patient_Kit__r.VTD1_Kit_Contents_Damaged__c,
                        VTD1_Case__c,
                        VTD1_Case__r.VTD1_Subject_ID__c,
                        VTD1_Case__r.VTD1_Study__r.Name,
                        VTD1_Status__c
                FROM VTD1_Order__c
                WHERE Id = :orderId
        ];
        Delivery deliveryObj = new Delivery();
        deliveryObj.protocolId = orderDelivery.VTD1_Case__r.VTD1_Study__r.Name;
        deliveryObj.subjectId = orderDelivery.VTD1_Case__r.VTD1_Subject_ID__c;
        deliveryObj.shipmentId = orderDelivery.VTD1_ShipmentId__c;
        if (orderDelivery.VTD1_ExpectedDeliveryDateTime__c != null) {
            deliveryObj.receivedDate = orderDelivery.VTD1_ExpectedDeliveryDateTime__c.date();
        }
        if (apiVersion == '1.1') {
            deliveryObj.receivedAll = false;
            deliveryObj.receivedStateOk = false;
        } else {
            deliveryObj.receivedAll = orderDelivery.VTR2_Received_All__c;
            deliveryObj.kits = new List<Kit>();
        }

        Boolean receivedAll = true;
        Boolean receivedStateOk = true;
        List<VTD1_Patient_Kit__c> patientKits = [
                SELECT VTD1_Kit_Name__c,
                        VTD2_MaterialId__c,
                        VTR2_Quantity__c,
                        VTD1_Kit_Type__c,
                        VTD1_Delivery_Status__c,
                        VTD1_Kit_Contents_Damaged__c,
                        VTD1_Kit_Not_Received__c,
                        VTD1_Kit_Contains_All_Contents__c
                FROM VTD1_Patient_Kit__c
                WHERE VTD1_Patient_Delivery__c =: orderId
        ];
        if (!patientKits.isEmpty()) {
            for (VTD1_Patient_Kit__c patientKit : patientKits) {
                if (apiVersion == '1.1') {
                    if (receivedAll) {
                        receivedAll = !patientKit.VTD1_Kit_Not_Received__c;
                    }
                    if (receivedStateOk) {
                        receivedStateOk = !patientKit.VTD1_Kit_Contents_Damaged__c;
                    }
                } else {
                    Kit kit = new Kit();
                    kit.kitName = patientKit.VTD1_Kit_Name__c;
                    kit.materialId = patientKit.VTD2_MaterialId__c;
                    kit.quantity = (Integer) patientKit.VTR2_Quantity__c;
                    kit.kitType = patientKit.VTD1_Kit_Type__c;
                    if (patientKit.VTD1_Kit_Contents_Damaged__c) {
                        kit.deliveryStatus = 'Received Damaged';
                    } else if (patientKit.VTD1_Kit_Not_Received__c) {
                        kit.deliveryStatus = 'Not Received';
                    } else if (patientKit.VTD1_Kit_Contains_All_Contents__c) {
                        kit.deliveryStatus = 'Received in good state';
                    }
                    deliveryObj.kits.add(kit);
                }
                //deliveryObj.receivedAll = !patientKits[0].VTD1_Kit_Not_Received__c;
                //deliveryObj.receivedStateOk = !patientKits[0].VTD1_Kit_Contents_Damaged__c;
            }
            if (apiVersion == '1.1') {
                deliveryObj.receivedAll = receivedAll;
                deliveryObj.receivedStateOk = receivedStateOk;
            }
        }
        deliveryObj.auditLog = new AuditLog();
        
        String body = JSON.serializePretty(deliveryObj, true);
        return body;
    }
    
}