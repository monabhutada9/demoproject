/**
* @author: Michael Salamakha
* @date: 30-Jan-19
* @description:
**/

//Batch for updating STM_User_ID__c field on Study_Team_Member__c with values from User__c field
global class VT_D2_BatchForUpdating_STM_User_Id implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'select User__c, STM_User_ID__c from Study_Team_Member__c where STM_User_ID__c = null and User__c != null';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Study_Team_Member__c> scope) {
        if(scope.isEmpty()) return;
        for (Study_Team_Member__c stm : scope) {
            stm.STM_User_ID__c = stm.User__c;
        }
        update scope;
    }
    global void finish(Database.BatchableContext BC){}
}