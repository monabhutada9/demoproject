public with sharing class VT_D1_EventListController {
    public class EventObj {
        public Datetime StartDateTime;
        public Datetime EndDateTime;
        public String VisitName;
        public String PatientName;
        public String PatientId;
        public String StudyName;
        public String Subject;
        public String EventRecordType;
    }
    @AuraEnabled
    public static String getEvents() {
        List<Event> events = [
                SELECT Subject, OwnerId, StartDateTime, EndDateTime,
                        WhatId, Record_Type__c,
                        VTD1_Actual_Visit__r.VTD1_Case__c,
                        VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Study__r.Name,
                        VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Study__r.Name,
                        VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Subject_ID__c,
                        VTD1_Actual_Visit__r.VTD1_Case__r.Contact.FirstName, VTD1_Actual_Visit__r.VTD1_Case__r.Contact.LastName,
                        VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Subject_ID__c,
                        VTD1_Actual_Visit__r.Unscheduled_Visits__r.Contact.FirstName, VTD1_Actual_Visit__r.Unscheduled_Visits__r.Contact.LastName,
                        VTD1_Actual_Visit__r.Type__c, VTD1_Actual_Visit__r.Name, VTD1_IsBufferEvent__c, VTD1_Actual_Visit__c
                FROM Event
                WHERE OwnerId = :UserInfo.getUserID()
                ORDER BY StartDateTime
        ];
        Map<String, List<Event>> actualVisitIdToBufferEventListMap = new Map<String, List<Event>>();
        List<Event> eventListWithoutBufferEvent = new List<Event>();
        List<Id> monitoringVisits = new List<Id>();
        for (Event ev : events) {
            if (ev.VTD1_IsBufferEvent__c) {
                if (!actualVisitIdToBufferEventListMap.containsKey(ev.VTD1_Actual_Visit__c)) {
                    actualVisitIdToBufferEventListMap.put(ev.VTD1_Actual_Visit__c, new List<Event>());
                }
                actualVisitIdToBufferEventListMap.get(ev.VTD1_Actual_Visit__c).add(ev);
            } else {
                eventListWithoutBufferEvent.add(ev);
            }
            if (ev.Record_Type__c == 'VTR2_Monitoring_Visit') {
                monitoringVisits.add(ev.WhatId);
            }
        }
        Map<Id, String> visitStudyName = new Map<Id, String>();
        Map<Id, String> visitNames = new Map<Id, String>();
        if (monitoringVisits.size() > 0) {
            for (VTD1_Monitoring_Visit__c mv : [SELECT Id, VTD1_Study__r.Name, Name FROM VTD1_Monitoring_Visit__c WHERE Id IN :monitoringVisits]) {
                visitStudyName.put(mv.Id, mv.VTD1_Study__r.Name);
                visitNames.put(mv.Id, mv.Name);
            }
        }
        List<EventObj> eventObjList = new List<EventObj>();
		Map <Id, String> generalVisitNamesMap = new Map<Id, String>();
		Map <Id, EventObj> eventObjMap = new Map<Id, EventObj>();
        for (Event ev : eventListWithoutBufferEvent) {
            Datetime eventStartDateTime = ev.StartDateTime;
            Datetime eventEndDateTime = ev.EndDateTime;
            if (ev.VTD1_Actual_Visit__c != null && actualVisitIdToBufferEventListMap.get(ev.VTD1_Actual_Visit__c) != null) {
                List<Event> bufferEventList = actualVisitIdToBufferEventListMap.get(ev.VTD1_Actual_Visit__c);
                for (Event bufferEvent : bufferEventList) {
                    if (bufferEvent.StartDateTime < eventStartDateTime) {
                        eventStartDateTime = bufferEvent.StartDateTime;
                    }
                    if (bufferEvent.EndDateTime > eventEndDateTime) {
                        eventEndDateTime = bufferEvent.EndDateTime;
                    }
                }
            }
            EventObj eventObj = new EventObj();
            eventObj.StartDateTime = eventStartDateTime;
            eventObj.EndDateTime = eventEndDateTime;
            eventObj.VisitName = ev.VTD1_Actual_Visit__r.Name;
            eventObj.EventRecordType = ev.Record_Type__c;
            //if it's a monitoring visit
            if (visitStudyName.containsKey(ev.WhatId) && visitNames.containsKey(ev.WhatId)) {
                    eventObj.Subject = visitNames.get(ev.WhatId);
                    eventObj.StudyName = visitStudyName.get(ev.WhatId);
            } else {
                eventObj.Subject = ev.Subject;
                //if it's actual unscheduled visit
                if (ev.VTD1_Actual_Visit__r.VTD1_Case__c == null) {
                    eventObj.StudyName = ev.VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Study__r.Name;
                    eventObj.PatientName = ev.VTD1_Actual_Visit__r.Unscheduled_Visits__r.Contact.FirstName + ' ' + ev.VTD1_Actual_Visit__r.Unscheduled_Visits__r.Contact.LastName;
                    eventObj.PatientId = ev.VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Subject_ID__c;
                } else {
                    //if it's actual scheduled visit
                    eventObj.StudyName = ev.VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Study__r.Name;
                    eventObj.PatientName = ev.VTD1_Actual_Visit__r.VTD1_Case__r.Contact.FirstName + ' ' + ev.VTD1_Actual_Visit__r.VTD1_Case__r.Contact.LastName;
                    eventObj.PatientId = ev.VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Subject_ID__c;
                }

            }

            if (ev.WhatId != null && ev.WhatId.getSobjectType().getDescribe().getName() == 'VTR4_General_Visit__c') {

                eventObj.EventRecordType = 'GeneralVisit';
				generalVisitNamesMap.put(ev.WhatId, null);
				eventObjMap.put(ev.WhatId, eventObj);

            }
            eventObjList.add(eventObj);
        }
		if (!generalVisitNamesMap.isEmpty()) {
			List <VTR4_General_Visit__c> visits = [select Id, Name from VTR4_General_Visit__c where Id in : generalVisitNamesMap.keySet()];
			for (VTR4_General_Visit__c visit : visits) {
				generalVisitNamesMap.put(visit.Id, visit.Name);
			}
		}
		for (Event ev : eventListWithoutBufferEvent) {
			String visitName = generalVisitNamesMap.get(ev.WhatId);
			if (visitName != null) {
				EventObj eventObj = eventObjMap.get(ev.WhatId);
				eventObj.VisitName = visitName;
			}
		}
        return JSON.serialize(eventObjList);
    }
    @AuraEnabled
    public static String getTimeZone() {
        User user = [SELECT TimeZoneSidKey FROM User WHERE Id = :UserInfo.getUserId()];
        return user.TimeZoneSidKey;
    }
    /*
     @AuraEnabled
    public static List<EventObj> getEvents() {
        List<EventObj> events = new List<EventObj>();
        List<Event> eventList = [
                SELECT Subject, OwnerId, StartDateTime, EndDateTime, Patient_Name__c, VTD1_Actual_Visit__r.Type__c, VTD1_Actual_Visit__r.Name
                FROM Event
                WHERE OwnerId=:UserInfo.getUserID()
                ORDER BY StartDateTime
        ];
        for (Event ev : eventList) {
            events.add(new EventObj(
                    ev.Id,
                    ev.StartDateTime,
                    ev.EndDateTime,
                    ev.VTD1_Actual_Visit__r.Type__c,
                    ev.VTD1_Actual_Visit__r.Name,
                    ev.Patient_Name__c
            ));
        }
        return events;
    }
    public class EventObj {
        @AuraEnabled
        public String Id {get;set;}
        @AuraEnabled
        public DateTime StartDateTime {get;set;}
        @AuraEnabled
        public DateTime EndDateTime {get;set;}
        @AuraEnabled
        public String VisitType {get;set;}
        @AuraEnabled
        public String VisitName {get;set;}
        @AuraEnabled
        public String PatientName {get;set;}
        public EventObj(String i, DateTime s, DateTime e, String t, String n, String p){
            this.Id = i;
            this.StartDateTime = s;
            this.EndDateTime = e;
            this.VisitType = t;
            this.VisitName = n;
            this.PatientName = p;
        }
    }*/
}