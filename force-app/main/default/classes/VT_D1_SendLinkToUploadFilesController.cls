public with sharing class VT_D1_SendLinkToUploadFilesController {
	
	@AuraEnabled
    public static String getLink(){
    	List<VTD1_General_Settings__mdt> generalSettingsList = [select VTD1_Url_link_for_uploading_files__c from 
    															VTD1_General_Settings__mdt];
    	if(generalSettingsList.isEmpty()) return null;
    	
    	return generalSettingsList[0].VTD1_Url_link_for_uploading_files__c;
    }
    
    @AuraEnabled
    public static void sendLinkByEmail(String email, String link) {
    	system.debug('!!! In sendLinkByEmail ');
    	system.debug('email='+email+', link='+link);
    	
    	EmailTemplate et=[Select id, Body, Subject from EmailTemplate where DeveloperName = 'External_link_to_upload_files_VF' limit 1];
    	
    	Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
		message.toAddresses = new String[] { email };
		message.setTemplateId(et.id);
		message.setSubject(et.Subject);
		message.setPlainTextBody(et.Body+'\n\n'+link);
		message.setTargetObjectId(UserInfo.getUserId());
		//message.setWhatId('5003D000001ycOlQAI');
		message.saveAsActivity = false;
		
		//message.subject = 'Link to upload files';
		//message.plainTextBody = 'Please go to the link bellow and upload files:\n\n' + link;

		Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
		Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

		if (results[0].success) {
    		System.debug('The email was sent successfully.');
		} else {
		    System.debug('The email failed to send: ' + results[0].errors[0].message);
		}

    	return;
    }
}