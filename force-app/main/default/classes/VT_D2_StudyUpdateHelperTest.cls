/**
 * Created by user on 16-May-19.
 */
@isTest
public with sharing class VT_D2_StudyUpdateHelperTest {
	
	public static void convertCandidatePatientsOnHoldTest (){
		Test.startTest();
		List<Id> studyIdList = new List<Id>();
		for(HealthCloudGA__CarePlanTemplate__c carePlan : [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c]){
			studyIdList.add(carePlan.Id);
		}
		VT_D2_StudyUpdateHelper.convertCandidatePatientsOnHold(studyIdList);
		Test.stopTest();
	}
}