/**
 * Created by User on 19/05/17.
 */
@IsTest
private with sharing class VT_D1_DocumentDeletionBatchTest {

    @TestSetup
    static void setup() {
        VTD1_Document__c doc = (VTD1_Document__c) new DomainObjects.VTD1_Document_t()
            .setVTD1_Flagged_for_Deletion(true)
            .persist();
        ContentVersion contentVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
            .setPathOnClient('test.pdf')
            .setTitle('Test')
            .setVersionData(Blob.valueOf('TestData'))
            .persist();
        ContentVersion insertedContVersion = [SELECT ContentDocumentId, Origin FROM ContentVersion WHERE Id = :contentVersion.Id];
        new DomainObjects.ContentDocumentLink_t()
            .withShareType('V')
            .withLinkedEntityId(doc.Id)
            .withContentDocumentId(insertedContVersion.ContentDocumentId)
            .persist();
    }

    @IsTest
    private static void doTest(){
        List<VTD1_Document__c> docList = [SELECT Id FROM VTD1_Document__c WHERE VTD1_Flagged_for_Deletion__c = TRUE LIMIT 1];
        Test.setCreatedDate(docList[0].Id, Datetime.now().addHours(-12));
        Test.startTest();
        //just a comment
        System.schedule('Delete flagged Documents Test', '0 0 * * * ?',  new VT_D1_DocumentDeletionBatch()); //one more comment
        Test.stopTest();
        //hello copado
    }
}