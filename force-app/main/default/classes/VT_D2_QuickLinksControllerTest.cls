@isTest
private class VT_D2_QuickLinksControllerTest {
    public static final String PROFILE_NAME = 'Standard User';

    @testSetup
    static void setup() {
        Profile p = [SELECT Id FROM Profile WHERE Name = :PROFILE_NAME];
        User u = new User(
                Alias = 'stdu79',
                Email = VT_D1_TestUtils.generateUniqueUserName(),
                EmailEncodingKey = 'UTF-8',
                FirstName = 'User1',
                LastName = 'Test1',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles',
                Username = VT_D1_TestUtils.generateUniqueUserName()
        );
        insert u;
    }

    @isTest static void getProfileNameTest() {
        User u = [SELECT Name FROM User WHERE Username LIKE '%@iqviavttest.com' LIMIT 1];
        String profileToAssert;
        System.runAs(u) {
            Test.startTest();
            profileToAssert = VT_D2_QuickLinksController.getProfileName();
            Test.stopTest();
        }
        System.assertEquals(PROFILE_NAME, profileToAssert);
    }
/*
    @isTest static void getProfileNameExceptionTest() {
        User u = [SELECT Name FROM User WHERE Username = 'userstandard123123@test.com' LIMIT 1];

        Id profileId = [SELECT Id FROM Profile WHERE Name = :PROFILE_NAME].Id;
        List<PermissionSet> psList = [SELECT Id FROM PermissionSet WHERE ProfileId = :profileId]; //0PS1N000001Xu47WAC

        PermissionSet ps = new PermissionSet();
        ps.Label = 'ProfileNameBlocked';
        ps.Name = 'ProfileNameBlocked';
        insert ps;

        FieldPermissions fp = new FieldPermissions();
        fp.ParentId = ps.Id;
        fp.Field = 'Profile.Name';
        fp.PermissionsRead = false;
        fp.PermissionsEdit = false;
        insert fp;

        System.runAs(u) {
            Test.startTest();
            System.debug(VT_D2_QuickLinksController.getProfileName());
            Test.stopTest();
        }
    }
*/
    @isTest static void getOpMetricsDashboardIdTest() {
        VTD1_RTId__c cs = new VTD1_RTId__c();
        cs.VTD2_Operational_Metrics_Dashboard__c = '01Z0v0000001qtm';
        insert cs;
        String dashboardId = VT_D2_QuickLinksController.getOpMetricsDashboardId();
        System.assertEquals('01Z0v0000001qtm', dashboardId);
    }

    @isTest static void getOpMetricsDashboardIdExceptionTest() {

    }

    @isTest static void getAllListViewsTest() {
        List<String> l = new List<String>();
        l.add('Quote');
        l.add('Address__c');
        System.debug(VT_D2_QuickLinksController.getAllListViews(l));
    }

    @isTest static void getAllListViewsExceptionTest() {
        try {
            List<String> l;
            System.debug(VT_D2_QuickLinksController.getAllListViews(l));
        } catch (Exception e) {

        }
    }


}