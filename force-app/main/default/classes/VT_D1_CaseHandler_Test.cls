/**
* @author: Carl Judge
* @date: 04-Nov-18
* @description:
**/

@IsTest
private class VT_D1_CaseHandler_Test {

    @TestSetup
    private static void setupMethod() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        //VT_D1_TestUtils.createPatientCandidate('Mike', 'Birn', 'mikebirn@test.com', 'mikebirn@test.com', 'mikebirn', 'Converted', study.Id, 'PN', '11-11-11', 'US', 'NY');
        //SH-13732 : Populating eCOA_Stop_Schedule_Event__c field on study to by pass newly added validation rule VTR5_eCOA_Stop_Schedule_cannot_be_null on case. | Added by Manu on 11-Aug-2020
        study.VTR5_eDiaryTool__c = 'eCOA';
        update study;

        List<VTR4_TemplateContainer__c> templateContainerList = new List<VTR4_TemplateContainer__c>();
        VTR4_TemplateContainer__c templateContainer1 = new VTR4_TemplateContainer__c();
        templateContainer1.VTR4_Type__c = 'EAF With TMA';
        templateContainer1.VTR4_Study__c = study.Id;
        templateContainerList.add(templateContainer1);

        VTR4_TemplateContainer__c templateContainer2 = new VTR4_TemplateContainer__c();
        templateContainer2.VTR4_Type__c = 'EAF Without TMA';
        templateContainer2.VTR4_Study__c = study.Id;
        templateContainerList.add(templateContainer2);
        insert templateContainerList;

        List<VTD1_Regulatory_Document__c> regDocList = new List<VTD1_Regulatory_Document__c>();
        VTD1_Regulatory_Document__c regDoc1 = new VTD1_Regulatory_Document__c();
        regDoc1.Name = 'Test1';
        regDoc1.VTR2_Way_to_Send_with_DocuSign__c = 'Manual';
        regDoc1.VTD1_Document_Type__c = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_SOURCE_NOTE_OF_TRANSFER;
        regDocList.add(regDoc1);

        VTD1_Regulatory_Document__c regDoc2 = new VTD1_Regulatory_Document__c();
        regDoc2.Name = 'Test2';
        regDoc2.VTR2_Way_to_Send_with_DocuSign__c = 'Manual';
        regDoc2.VTD1_Document_Type__c = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_TARGET_NOTE_OF_TRANSFER;
        regDocList.add(regDoc2);
        insert regDocList;

        List<VTD1_Regulatory_Binder__c> regBinderList = new List<VTD1_Regulatory_Binder__c>();
        VTD1_Regulatory_Binder__c binder1 = new VTD1_Regulatory_Binder__c();
        binder1.VTD1_Care_Plan_Template__c = study.Id;
        binder1.VTD1_Regulatory_Document__c = regDoc1.Id;
        regBinderList.add(binder1);

        VTD1_Regulatory_Binder__c binder2 = new VTD1_Regulatory_Binder__c();
        binder2.VTD1_Care_Plan_Template__c = study.Id;
        binder2.VTD1_Regulatory_Document__c = regDoc2.Id;
        regBinderList.add(binder2);
        insert regBinderList;

        List<VTD2_Study_Geography__c> studyGeographyList = new List<VTD2_Study_Geography__c>();
        VTD2_Study_Geography__c geo1 = new VTD2_Study_Geography__c();
        geo1.VTD2_Study__c = study.Id;
        geo1.VTD2_Geographical_Region__c = 'StateProvince';
        geo1.VTR2_StateProvince__c = 'NY';
        geo1.VTR3_Country__c = 'US';
        studyGeographyList.add(geo1);

        VTD2_Study_Geography__c geo2 = new VTD2_Study_Geography__c();
        geo2.VTD2_Study__c = study.Id;
        geo2.VTD2_Geographical_Region__c = 'Coast';
        geo2.VTR2_StateProvince__c = 'NY';
        geo2.VTR3_Country__c = 'US';
        studyGeographyList.add(geo2);

        VTD2_Study_Geography__c geo3 = new VTD2_Study_Geography__c();
        geo3.VTD2_Study__c = study.Id;
        geo3.VTD2_Geographical_Region__c = 'Country';
        geo3.VTR2_StateProvince__c = 'NY';
        geo3.VTR3_Country__c = 'US';
        studyGeographyList.add(geo3);

        VTD2_Study_Geography__c geo4 = new VTD2_Study_Geography__c();
        geo4.VTD2_Study__c = study.Id;
        geo4.VTD2_Geographical_Region__c = 'World';
        geo4.VTR2_StateProvince__c = 'NY';
        geo4.VTR3_Country__c = 'US';
        studyGeographyList.add(geo4);

        insert studyGeographyList;

        List<VTR2_StudyPhoneNumber__c> studyPhoneNumberList = new List<VTR2_StudyPhoneNumber__c>();
        VTR2_StudyPhoneNumber__c phn1 = new VTR2_StudyPhoneNumber__c();
        phn1.VTR2_Study__c = study.Id;
        phn1.VTR2_Language__c = 'en_US';
        phn1.VTR2_Study_Geography__c = geo1.Id;
        studyPhoneNumberList.add(phn1);

        VTR2_StudyPhoneNumber__c phn2 = new VTR2_StudyPhoneNumber__c();
        phn2.VTR2_Study__c = study.Id;
        phn2.VTR2_Language__c = 'en_US';
        phn2.VTR2_Study_Geography__c = geo1.Id;
        studyPhoneNumberList.add(phn2);

        VTR2_StudyPhoneNumber__c phn3 = new VTR2_StudyPhoneNumber__c();
        phn3.VTR2_Study__c = study.Id;
        phn3.VTR2_Language__c = 'en_US';
        phn3.VTR2_Study_Geography__c = geo1.Id;
        studyPhoneNumberList.add(phn3);

        VTR2_StudyPhoneNumber__c phn4 = new VTR2_StudyPhoneNumber__c();
        phn4.VTR2_Study__c = study.Id;
        phn4.VTR2_Language__c = 'en_US';
        phn4.VTR2_Study_Geography__c = geo1.Id;
        studyPhoneNumberList.add(phn4);

        insert studyPhoneNumberList;

        insert new VTD2_TN_Catalog_Code__c(
                VTR2_Name_ID__c = 'VTD1_Document__c.VTD1_Clinical_Study_Membership__r.ContactId',
                VTD2_T_Task_Unique_Code__c = 'T585',
                VTD2_T_Category__c = 'Enrollment Related',
                VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD1_RTId__c.VTD2_Task_EAF_task__c',
                VTD2_T_Due_Date__c = 'TODAY()',
                VTD2_T_SObject_Name__c = 'VTD1_Document__c',
                RecordTypeId = VTD2_TN_Catalog_Code__c.getSObjectType().getDescribe().getRecordTypeInfosByName().get('Task').getRecordTypeId(),
                VTD2_T_Patient__c = 'VTD1_Document__c.VTD1_Clinical_Study_Membership__c',
                VTD2_T_Assigned_To_ID__c = 'VTD1_Document__c.VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c',
                VTR3_T_Name__c = 'VTD1_Document__c',
                VTD2_T_Subject__c = '"Prepare Eligibility Assessment Form for " & VTD1_Document__c.VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__r.Name & " (" & VTD1_Document__c.VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c & ")"'
        );

        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }

    @IsTest
    private static void creationPCFTest() {
        Study_Team_Member__c stm = [
                SELECT Id,
                        User__c,
                        Study__c
                FROM Study_Team_Member__c
                WHERE User__r.VTD1_Profile_Name__c = 'Site Coordinator'
                LIMIT 1
        ];

        Test.startTest();
        Case cas = VT_D1_TestUtils.createCase('VTD1_PCF', null, null, null, null, null, null);
        cas.VTD1_Study__c = stm.Study__c;
        cas.VTR2_SiteCoordinator__c = stm.User__c;
        insert cas;

        User patient = [SELECT Id FROM User WHERE Profile.Name = 'Patient' AND IsActive = TRUE LIMIT 1];
        insert new CaseShare(UserOrGroupId = patient.Id, CaseId = cas.Id, CaseAccessLevel = 'Edit', RowCause = 'Manual');

        Case childCase = VT_D1_TestUtils.createCase('VTD1_PCF', null, null, null, null, null, null);
        childCase.ParentId = cas.Id;
        insert childCase;

        Case csmChildCase = VT_D1_TestUtils.createCase('VTD1_PCF', null, null, null, null, null, null);
        csmChildCase.VTD1_Clinical_Study_Membership__c = cas.Id;
        insert csmChildCase;
        Test.stopTest();
    }

    @IsTest
    private static void reassignPCFTest01() {
        // MODIFIED VERSION OF CODE TAKEN FROM VT_D1_Test_CaseTriggerHandler

        Study_Team_Member__c stm = [
                SELECT Id,
                        User__c,
                        Study__c
                FROM Study_Team_Member__c
                WHERE User__r.VTD1_Profile_Name__c = 'Site Coordinator'
                LIMIT 1
        ];

        User patient = [SELECT Id, ContactId, Contact.AccountId FROM User WHERE Profile.Name = 'Patient' AND IsActive = TRUE AND Contact.AccountId != NULL LIMIT 1];

        Case cas = VT_D1_TestUtils.createCase('VTD1_PCF', null, null, null, null, null, null);
        cas.VTD1_Study__c = stm.Study__c;
        cas.VTR2_SiteCoordinator__c = stm.User__c;
        insert cas;

        List<Case> updateList = new List<Case>();

        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();
        User uSC = VT_D1_TestUtils.createUserByProfile('Study Concierge', 'PrimSC');
        User uQSC = VT_D1_TestUtils.createUserByProfile('Study Concierge', 'QueueSC');
        User uPG = VT_D1_TestUtils.createUserByProfile('Patient Guide', 'PrimPG');
        User uBPG = VT_D1_TestUtils.createUserByProfile('Patient Guide', 'BackPG');
        VT_D1_TestUtils.persistUsers();

        Test.startTest();
        //Study Concierge can route to:

        Case c1_1 = VT_D1_Test_CaseTriggerHandler.createCase(patient.ContactId, cas.Id, uSC.Id);
        //pool of Study Concierges
        List<Group> pool = [SELECT Id, DeveloperName, Name, Type FROM Group WHERE Type = 'Queue' AND DeveloperName = :'SC_Queue'];
        c1_1.OwnerId = pool[0].Id; //Queue SC Queue
        updateList.add(c1_1);

        Case c1_2 = VT_D1_Test_CaseTriggerHandler.createCase(patient.ContactId, cas.Id, uSC.Id);
        //Study Concierge in the study’s pool of SCs
        c1_2.OwnerId = uQSC.Id;
        updateList.add(c1_2);

        Case c1_3 = VT_D1_Test_CaseTriggerHandler.createCase(patient.ContactId, cas.Id, uSC.Id);
        //patient’s Patient Guide
        c1_3.OwnerId = uPG.Id;
        updateList.add(c1_3);

        Case c1_4 = VT_D1_Test_CaseTriggerHandler.createCase(patient.ContactId, cas.Id, uSC.Id);
        //patient’s Backup Patient Guide
        c1_4.OwnerId = uBPG.Id;
        updateList.add(c1_4);

        Test.stopTest();

        update updateList;
    }

    @IsTest
    private static void reassignPCFTest02() {
        // MODIFIED VERSION OF CODE TAKEN FROM VT_D1_Test_CaseTriggerHandler

        Study_Team_Member__c stm = [
                SELECT Id,
                        User__c,
                        Study__c
                FROM Study_Team_Member__c
                WHERE User__r.VTD1_Profile_Name__c = 'Site Coordinator'
                LIMIT 1
        ];

        User patient = [SELECT Id, ContactId, Contact.AccountId FROM User WHERE Profile.Name = 'Patient' AND IsActive = TRUE AND Contact.AccountId != NULL LIMIT 1];

        Case cas = VT_D1_TestUtils.createCase('VTD1_PCF', null, null, null, null, null, null);
        cas.VTD1_Study__c = stm.Study__c;
        cas.VTR2_SiteCoordinator__c = stm.User__c;
        insert cas;

        List<Case> updateList = new List<Case>();

        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();
        User uQSC = VT_D1_TestUtils.createUserByProfile('Study Concierge', 'QueueSC');
        User uPG = VT_D1_TestUtils.createUserByProfile('Patient Guide', 'PrimPG');
        User uPI = VT_D1_TestUtils.createUserByProfile('Primary Investigator', 'PrimPI');
        VT_D1_TestUtils.persistUsers();

        Test.startTest();
        //Patient Guide can route to:

        Case c2_1 = VT_D1_Test_CaseTriggerHandler.createCase(patient.ContactId, cas.Id, uPG.Id);
        //pool of Study Concierges
        List<Group> pool = [SELECT Id, DeveloperName, Name, Type FROM Group WHERE Type = 'Queue' AND DeveloperName = :'SC_Queue'];
        c2_1.OwnerId = pool[0].Id; //Queue SC Queue
        updateList.add(c2_1);

        Case c2_2 = VT_D1_Test_CaseTriggerHandler.createCase(patient.ContactId, cas.Id, uPG.Id);
        //specific Study Concierge in the study’s pool of SCs
        c2_2.OwnerId = uQSC.Id;
        updateList.add(c2_2);

        Case c2_3 = VT_D1_Test_CaseTriggerHandler.createCase(patient.ContactId, cas.Id, uPG.Id);
        //patient’s Patient Guide
        c2_3.OwnerId = uPG.Id;
        updateList.add(c2_3);

        Case c2_4 = VT_D1_Test_CaseTriggerHandler.createCase(patient.ContactId, cas.Id, uPG.Id);
        //patient’s Primary Investigator
        c2_4.OwnerId = uPI.Id;
        updateList.add(c2_4);

        Test.stopTest();

        update updateList;
    }

    @IsTest
    private static void reassignPCFTest03() {
        // MODIFIED VERSION OF CODE TAKEN FROM VT_D1_Test_CaseTriggerHandler

        Study_Team_Member__c stm = [
                SELECT Id,
                        User__c,
                        Study__c
                FROM Study_Team_Member__c
                WHERE User__r.VTD1_Profile_Name__c = 'Site Coordinator'
                LIMIT 1
        ];

        User patient = [SELECT Id, ContactId, Contact.AccountId FROM User WHERE Profile.Name = 'Patient' AND IsActive = TRUE AND Contact.AccountId != NULL LIMIT 1];

        Case cas = VT_D1_TestUtils.createCase('VTD1_PCF', null, null, null, null, null, null);
        cas.VTD1_Study__c = stm.Study__c;
        cas.VTR2_SiteCoordinator__c = stm.User__c;
        insert cas;

        List<Case> updateList = new List<Case>();

        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();
        User uQSC = VT_D1_TestUtils.createUserByProfile('Study Concierge', 'QueueSC');
        User uPG = VT_D1_TestUtils.createUserByProfile('Patient Guide', 'PrimPG');
        User uBPG = VT_D1_TestUtils.createUserByProfile('Patient Guide', 'BackPG');
        User uPI = VT_D1_TestUtils.createUserByProfile('Primary Investigator', 'FirsPI');
        VT_D1_TestUtils.persistUsers();

        Test.startTest();
        // Primary Investigator can route to:

        Case c3_1 = VT_D1_Test_CaseTriggerHandler.createCase(patient.ContactId, cas.Id, uPI.Id);
        //pool of Study Concierges
        List<Group> pool = [SELECT Id, DeveloperName, Name, Type FROM Group WHERE Type = 'Queue' AND DeveloperName = :'SC_Queue'];
        c3_1.OwnerId = pool[0].Id; //Queue SC Queue
        updateList.add(c3_1);

        Case c3_2 = VT_D1_Test_CaseTriggerHandler.createCase(patient.ContactId, cas.Id, uPI.Id);
        //specific Study Concierge in the study’s pool of SCs
        c3_2.OwnerId = uQSC.Id;
        updateList.add(c3_2);

        Case c3_3 = VT_D1_Test_CaseTriggerHandler.createCase(patient.ContactId, cas.Id, uPI.Id);
        //patient’s Patient Guide
        c3_3.OwnerId = uPG.Id;
        updateList.add(c3_3);

        Case c3_4 = VT_D1_Test_CaseTriggerHandler.createCase(patient.ContactId, cas.Id, uPI.Id);
        //patient’s Backup Patient Guide
        c3_4.OwnerId = uBPG.Id;
        updateList.add(c3_4);

        Test.stopTest();

        update updateList;
    }

    @IsTest
    static void linkPCFWithParentCaseTest() {
        Account acc = (Account) new DomainObjects.Account_t()
                .persist();
        Contact con = (Contact) new DomainObjects.Contact_t()
                .setAccountId(acc.Id)
                .persist();
        Case cas = (Case) new DomainObjects.Case_t()
                .setContactId(con.Id)
                .setRecordTypeByName('CarePlan')
                .persist();
        List<LiveChatTranscript> chatList = new List<LiveChatTranscript>{
                new LiveChatTranscript(ContactId = con.Id, CaseId = cas.Id)
        };

        VT_D1_CaseHandler.linkPCFWithParentCase(chatList,new map<Id,LiveChatTranscript>());
    }

    @IsTest
    static void updateIntegrationIdTest() {
        Case cas = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        VTD1_Patient_Kit__c kit = new VTD1_Patient_Kit__c(VTD1_Case__c = cas.Id);
        insert kit;
        insert new VTD1_Patient_Device__c(
                VTD1_Patient_Kit__c = kit.Id,
                VTD1_Case__c = cas.Id
        );
        cas.VTD1_Subject_ID__c = 'test subject id';
        update cas;
    }

    @IsTest
    static void adjustVisitScheduleLTFUTest() {
        //Setup LTFU Study
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        study.VTR3_LTFU__c = true;
        study.VTR3_LTFU_Years__c = 10;
        study.VTR3_LTFU_Months__c = 0;
        study.VTR3_LTFU_Days__c = 0;
        update study;

        String userName = VT_D1_TestUtils.generateUniqueUserName();
        Test.startTest();
        VT_D1_TestUtils.createPatientCandidate('Mike', 'Birn', userName, userName, 'mikebirn', 'Converted', study.Id, 'PN', '11-11-11', 'US', 'NY');
        //VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
        Case cas = [SELECT Id FROM Case WHERE VTD1_Patient_User__r.Username = :userName];

        VTD1_ProtocolVisit__c protocolVisitSkipped = new VTD1_ProtocolVisit__c();
        protocolVisitSkipped.VTD1_Range__c = 4;
        protocolVisitSkipped.VTD1_VisitOffset__c = 2;
        protocolVisitSkipped.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisitSkipped.VTD1_VisitDuration__c = '30 minutes';
        protocolVisitSkipped.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisitSkipped.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient; Site Coordinator';

        VTD1_ProtocolVisit__c protocolVisitNearest = new VTD1_ProtocolVisit__c();
        protocolVisitNearest.VTD1_Range__c = 4;
        protocolVisitNearest.VTD1_VisitOffset__c = 10;
        protocolVisitNearest.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisitNearest.VTD1_VisitDuration__c = '30 minutes';
        protocolVisitNearest.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisitNearest.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient; Site Coordinator';
        protocolVisitNearest.VTD1_VisitNumber__c = 'v15';

        insert new List<VTD1_ProtocolVisit__c>{protocolVisitSkipped, protocolVisitNearest} ;

        VTD1_Actual_Visit__c actualVisitSkipped = new VTD1_Actual_Visit__c();
        actualVisitSkipped.VTD1_Protocol_Visit__c = protocolVisitSkipped.Id;
        actualVisitSkipped.VTD1_Status__c = VT_D1_ConstantsHelper.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
        actualVisitSkipped.VTD1_Case__c = cas.Id;

        VTD1_Actual_Visit__c actualVisitNearest = new VTD1_Actual_Visit__c();
        actualVisitNearest.VTD1_Protocol_Visit__c = protocolVisitNearest.Id;
        actualVisitNearest.VTD1_Status__c = VT_D1_ConstantsHelper.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
        actualVisitNearest.VTD1_Case__c = cas.Id;

        insert new List<VTD1_Actual_Visit__c>{actualVisitSkipped, actualVisitNearest};
        cas.VTR3_LastDoseDate__c = Date.today().addDays(-3);
        update cas;
        cas.VTD1_Randomized_Date1__c = Date.today();
        update cas;
    }

    @IsTest
    static void taskAndPlaceholderForEAFTest() {
        Test.startTest();
        //Select Patient
        VTR4_TemplateContainer__c templateContainer = [SELECT Id FROM VTR4_TemplateContainer__c WHERE VTR4_Type__c = 'EAF Without TMA' LIMIT 1];

        ContentVersion contentVersion = new ContentVersion();
        contentVersion.PathOnClient = 'test.txt';
        contentVersion.Title = 'Test file';
        contentVersion.VersionData = Blob.valueOf('Test Data');
        insert contentVersion;

        ContentVersion contentVersion1 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = templateContainer.Id ;
        contentDocumentLink.ContentDocumentId = contentVersion1.ContentDocumentId;
        contentDocumentLink.ShareType = 'I';
        contentDocumentLink.Visibility = 'AllUsers';
        insert contentDocumentLink;

        Case cas = [SELECT Id, Status FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        cas.Status = 'Consented';
        update cas;
        Test.stopTest();
    }

    @IsTest
    static void automaticUpdateStatusToActiveRandomizedTest() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        study.VTD1_Randomized__c = 'No';
        study.VTD2_Washout_Run_In__c = false;
        update study;

        Case cas = [
                SELECT Id,
                        Status,
                        VTD1_Send_to_DF__c,
                        VTD1_Screening_Complete__c,
                        VTD1_Eligibility_Status__c,
                        VTD2_Baseline_Visit_Complete__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
                AND VTD1_Study__c = :study.Id
                AND VTD1_Study__r.VTD1_Randomized__c = 'No'
                AND VTD1_Study__r.VTD2_Washout_Run_In__c = FALSE
                LIMIT 1
        ];

        Test.startTest();

        cas.Status = 'Consented';
        cas.VTD1_Screening_Complete__c = true;
        cas.VTD1_Eligibility_Status__c = 'Eligible';
        update cas;

        cas.Status = 'Screened';
        update cas;

        Test.stopTest();

        cas.VTD2_Baseline_Visit_Complete__c = true;
        update cas;


        Case caseForAssert = [
                SELECT Id,
                        Status,
                        VTD1_Send_to_DF__c
                FROM Case
                WHERE Id = :cas.Id
        ];

        //System.assertEquals('Active/Randomized', caseForAssert.Status);
        System.assertEquals(true, caseForAssert.VTD1_Send_to_DF__c);

        User user = [SELECT ContactId FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = TRUE LIMIT 1];

        System.runAs(user) {
            cas.Status = 'Dropped';
            cas.VTD1_Reason__c = 'Death';
            cas.VTD1_End_of_study_visit_completed__c = true;
            update cas;
        }
    }
    /**********
     * @description SH - 13732 : To cover Baseline Visit After A/R Scenario | Created by Manu
     * 
     */
    @IsTest
    static void automaticUpdateStatusToActiveRandomizedTestWithBaselineVisitAfterARTrue() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id, Name FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        study.VTD1_Randomized__c = VT_R4_ConstantsHelper_Misc.STUDY_RANDOMIZED_NO;
        study.VTD2_Washout_Run_In__c = false;
        study.VTR5_Baseline_Visit_After_A_R_Flag__c = true;
        study.VTD2_Washout_Run_In__c = false;
        //study.VTD2_CM_Group_Id__c=VT_D1_TestUtils.createChatterGroup(study.Name,'-CM');
        study.eCOA_Stop_Schedule_Event__c = 'test';
        update study;

        Case cas = [
                SELECT Id,
                        Status,
                        VTD1_Screening_Complete__c,
                        VTD1_Eligibility_Status__c,
                        VTD1_Send_to_DF__c,
                        VTD1_Study__c,
                        VTD1_Study__r.VTR5_Baseline_Visit_After_A_R_Flag__c
                FROM Case
                WHERE RecordType.DeveloperName = :VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN
                AND VTD1_Study__c = :study.Id
                AND VTD1_Study__r.VTD1_Randomized__c = :VT_R4_ConstantsHelper_Misc.STUDY_RANDOMIZED_NO
                AND VTD1_Study__r.VTD2_Washout_Run_In__c = FALSE
                LIMIT 1
        ];

        Test.startTest();

        cas.Status = VT_R4_ConstantsHelper_AccountContactCase.CASE_CONSENTED;
        cas.VTD1_Screening_Complete__c = true;
        cas.VTD1_Eligibility_Status__c = VT_R4_ConstantsHelper_Misc.CASE_ELIGIBLITY_STATUS_ELIGIBLE;
        update cas;

        cas.Status = VT_R4_ConstantsHelper_AccountContactCase.CASE_SCREENED;
        //cas.VTD1_Eligibility_Status__c = 'Eligible';
        update cas;
        
        Test.stopTest();

        Case caseForAssert = [
                SELECT Id,
                        Status,
                        VTD1_Send_to_DF__c
                FROM Case
                WHERE Id = :cas.Id
        ];

        //System.assertEquals('Active/Randomized', caseForAssert.Status);
        System.assertEquals(true, caseForAssert.VTD1_Send_to_DF__c,'Send to DF Should be true');
    }
    @IsTest
    public static void validateEproByCaregiverTest() {
        Case cas = [
                SELECT Id,
                        VTD1_Patient_User__r.Contact.AccountId,
                        VTD1_ePro_can_be_completed_by_Caregiver__c,
                        Status
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
                LIMIT 1
        ];

        String email = VT_D1_TestUtils.generateUniqueUserName();

        Contact cgContact = new Contact(
                FirstName = 'Test',
                LastName = 'Caregiver',
                VTD1_EmailNotificationsFromCourier__c = true,
                Email = email,
                RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId(),
                AccountId = cas.VTD1_Patient_User__r.Contact.AccountId,
                VTD1_Clinical_Study_Membership__c = cas.Id,
                VTD1_Primary_CG__c = true
        );
        insert cgContact;

        User cgUser = new User (
                FirstName = 'Test',
                LastName = 'Caregiver',
                Username = email,
                Email = email,
                Alias = 'alias',
                TimeZoneSidKey = 'America/New_York',
                LocaleSidKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                ContactId = cgContact.Id,
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'Caregiver'].Id,
                LanguageLocaleKey = 'en_US'
        );
        insert cgUser;

        cas.VTD1_ePro_can_be_completed_by_Caregiver__c = true;
        update cas;
    }

    @IsTest
    public static void handlePCFTasksTest() {
        Case cas = [
                SELECT Id,
                        VTD1_Patient__c,
                        VTD1_Patient_User__r.Contact.AccountId,
                        Status
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
                LIMIT 1
        ];

        Test.startTest();

        Case pcf = VT_D1_TestUtils.createCase('VTD1_PCF', cas.VTD1_Patient_User__r.ContactId, null, cas.VTD1_Patient__c, null, cas.Id, cas.VTD1_Patient_User__c);
        pcf.VTD1_PCF_Safety_Concern_Indicator__c = 'Possible';
        insert pcf;

        Test.stopTest();
    }

    private class StudyTeamMemberCreator implements Queueable {
        Study_Team_Member__c stmToInsert;

        public StudyTeamMemberCreator(Study_Team_Member__c stmToInsert) {
            this.stmToInsert = stmToInsert;
        }

        public void execute(QueueableContext context) {
            insert stmToInsert;
        }
    }
    @IsTest
    static void phxVirtualSiteTest() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        study.VTD1_Randomized__c = 'No';
        study.VTD2_Washout_Run_In__c = false;
        update study;
        Study_Team_Member__c studyTeamMember = [SELECT Id, Name,VTD1_UserContact__c FROM Study_Team_Member__c WHERE User__r.VTD1_Profile_Name__c = 'Primary Investigator'
                LIMIT 1];
        Case cas = [
                SELECT Id,
                        Status,VTD1_Patient_User__c,VTD1_Virtual_Site__c,Previous_PI_Lookup__c,Patient_Transfer_Date__c,
                        VTD1_Send_to_DF__c,VTD1_Screening_Complete__c, VTD1_Eligibility_Status__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
                AND VTD1_Study__c = :study.Id
                AND VTD1_Study__r.VTD1_Randomized__c = 'No'
                AND VTD1_Study__r.VTD2_Washout_Run_In__c = FALSE
                LIMIT 1
        ];

        Test.startTest();

        cas.Status = 'Consented';
        cas.VTD1_Screening_Complete__c = true;
        cas.VTD1_Eligibility_Status__c = 'Eligible';
        update cas;
        
        cas.Status = 'Screened';
        update cas;
        
        Test.stopTest();
        /*Virtual_Site__c virtualSite = new Virtual_Site__c(
                VTD1_Study__c = study.Id,
                VTD1_Study_Team_Member__c = studyTeamMember.Id,
                VTD1_Study_Site_Number__c = '007'
        );
        insert virtualSite;*/
        
        //cas.VTD1_Virtual_Site__c = virtualSite.Id;
        cas.Previous_PI_Lookup__c = studyTeamMember.VTD1_UserContact__c;
        update cas;
    }


    @IsTest
    static void handleChangeFieldDayOfDoseTest() {
        Date dayOneDose;
        Date dayFiftySevenDose;
        Test.startTest();
        insert new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'T642',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByDeveloperName().get('SCTask').getRecordTypeId(),
                VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD1_RTId__c.VTD1_SC_Task_Schedule_HCP_Visit_Task__c',
                VTD2_T_Category__c = 'Other Tasks',
                VTD2_T_Due_Date__c = 'TODAY()',
                VTD2_T_SObject_Name__c = 'Case',
                VTR2_Pass_if_empty_recipient__c = true,
                VTD2_T_Patient__c = 'Case.Id',
                VTD2_T_Care_Plan_Template__c = 'Case.VTD1_Study__c',
                VTD2_T_Assigned_To_ID__c = 'Case.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c',
                VTR3_T_Name__c = 'Review visits',
                VTD2_T_Subject__c = '"Review visits for patient " & Case.VTD1_Subject_ID__c & " after Dose Date Correction"'
        );
        List<Case> patients = [SELECT Id, VTR5_Day_1_Dose__c, VTR5_Day_57_Dose__c, VTD1_Study__c FROM Case];
        if (patients.size() > 0) {
            Case patient = patients[0];
            patient.VTR5_Day_1_Dose__c = Date.today();
            patient.VTR5_Day_57_Dose__c = Date.today().addDays(2);
            update patient;

            Case patientToUpdate = [SELECT Id, VTR5_Day_1_Dose__c, VTR5_Day_57_Dose__c FROM Case WHERE id = :patient.Id];
            patientToUpdate.VTR5_Day_1_Dose__c = Date.today().addDays(3);
            patientToUpdate.VTR5_Day_57_Dose__c = Date.today().addDays(4);
            dayOneDose = patientToUpdate.VTR5_Day_1_Dose__c;
            dayFiftySevenDose = patientToUpdate.VTR5_Day_57_Dose__c;
            update patientToUpdate;
        }
        Test.stopTest();
        Integer countOfStudyConcierge = [SELECT count() FROM Study_Team_Member__c WHERE Study__c = :patients[0].VTD1_Study__c
        AND VTD1_Type__c = 'Study Concierge'];
        Integer countOfSCTasks = [SELECT count() FROM Task WHERE VTD2_Task_Unique_Code__c = 'T641'];
        System.assertEquals(countOfStudyConcierge * 2, countOfSCTasks, 'For every Study Concierge should be created two tasks.');

    }

    @IsTest
    public static void calleCOAToIntitiateSecondVaccinationDiaries() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id, VTR5_eCOASecondVaccinationEvent__c, VTR5_IRTSystem__c FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        study.VTR5_eCOASecondVaccinationEvent__c = 'vaccination_2';
        study.VTR5_IRTSystem__c = 'Signant';
        update study;

        Case cas = [
                SELECT Id,
                        VTR5_Day_57_Dose__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan' AND VTR5_Day_57_Dose__c = NULL AND VTD1_Study__c = :study.Id
                LIMIT 1
        ];

        Test.startTest();
        cas.VTR5_Day_57_Dose__c = Date.today();
        update cas;
        Test.stopTest();
    }

    @IsTest
    public static void createeCOASubjectOnActiveRandomized(){
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        study.eCOA_Stop_Schedule_Event__c = 'test';
        study.VTR5_WhenToCreate_eCoaSubject__c = 'Randomization';
        update study;

       // Case thisCase = [select Id, Status from Case where VTD1_Study__c = :study.Id];
       // thisCase.Status = 'Active/Randomized';
        Test.startTest();
        Case thisCase = VT_D1_TestUtils.createCase('VTD1_PCF', null, null, null, null, null, null);
        thisCase.VTD1_Study__c = study.Id;
        thisCase.VTD1_Screening_Complete__c = true;
        thisCase.VTD1_Eligibility_Status__c = 'Eligible';
        thisCase.Status = 'Screened';
        thisCase.VTD1_Randomized_Date1__c = Date.today();
        thisCase.VTD1_Randomized_ID__c = 'someRandomId';
        insert thisCase;
        thisCase.Status = 'Active/Randomized';
        update thisCase;
        Test.stopTest();

        List<VTD1_Queue_Action__c> actionsCreated = [select Id, Name from VTD1_Queue_Action__c];
        system.assertEquals(1,actionsCreated.size());

    }
}