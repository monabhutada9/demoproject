public class VT_D1_CustomMultiPicklistController {
    @AuraEnabled (Cacheable = true)
    public static List <String> getSelectOptions(SObject objObject, String fld) {
        System.debug('objObject --->' + objObject);
        System.debug('fld --->' + fld);
        List <String> allOpts = new List<String>();
        // Get the object type of the SObject.
        Schema.SObjectType objType = objObject.getSObjectType();

        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();

        // Get a map of fields for the SObject
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();

        // Get the list of picklist values for this field.
        List<Schema.PicklistEntry> values = fieldMap.get(fld).getDescribe().getPicklistValues();

        // Add these values to the selectOption list.
        for (Schema.PicklistEntry a : values) {
            allOpts.add(a.getValue());
        } 
        System.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
    }

    @AuraEnabled (Cacheable = true)
    public static Map<String, String> getSelectOptionsWithLabel(SObject objObject, String fld) {
        Map<String, String> allOptsMap = new Map<String, String>();
        Schema.SObjectType objType = objObject.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        List<Schema.PicklistEntry> values = fieldMap.get(fld).getDescribe().getPicklistValues();
        for (Schema.PicklistEntry a : values) {
            allOptsMap.put(a.getValue(),a.getLabel());
        }

        Map<String, String> sortedMap = new Map<String, String>();
        Set<String> keySet = allOptsMap.keySet();
        List<String> keyList = new List<String>();
        keyList.addAll(keySet);

        for (Integer i = 0; i < keyList.size(); i++) {
			sortedMap.put(keyList[i], allOptsMap.get(keyList[i]));
        }

        return sortedMap;
    }

    @AuraEnabled (Cacheable = true)
    public static List<List<String>> getSortedSelectOptionsWithLabel(SObject objObject, String fld) {
        List<List<String>> sortedOptions = new List<List<String>>();

        // get map o fields for object
        Map<String, Schema.SObjectField> fieldMap = objObject.getSObjectType().getDescribe().fields.getMap();

        // get list of picklist values
        List<Schema.PicklistEntry> values = fieldMap.get(fld).getDescribe().getPicklistValues();

        // create list of option pairs (api, label)
        for (Schema.PicklistEntry a : values) {
            sortedOptions.add(new List<String>{a.getValue(),a.getLabel()});
        }
        return sortedOptions;
    }
}