/**
 * Created by Dmitry Yartsev on 11.06.2019.
 */
public without sharing class VT_R2_StudySiteTeamMemberHandler {

    //START RAJESH SH-17314 :Added boolean variable to skip the SSTM trigger execution from SSTM creation batch class .
    public static boolean isRun = false;
    //END:SH-17314
	
    public void onBeforeDelete(List<Study_Site_Team_Member__c> recs) {
        VT_D1_LegalHoldDeleteValidator.validateDeletion(recs);
        VT_R2_SstmSharingHandler.handleBeforeDelete(recs);


        //VT_R5_SSTMauditHandler.deactivateSSAA(recs);
        VT_R5_CustomAuditHandler_Misc.onBeforeDeleteSstm(recs);

    } 

    public void onBeforeInsert(List<Study_Site_Team_Member__c> recs) {
        VT_R2_SstmScrCountValidator.validateSCRsCount(recs, null);
        // Jira Ref: SH-17543 Akanksha Singh
        // Existing CRA should not be added as SSTM on virtual site again
        List<Study_Site_Team_Member__c> craSSTM = new List<Study_Site_Team_Member__c>();
        for(Study_Site_Team_Member__c sstm : recs) {
            if (sstm.VTR5_Associated_CRA__c != null) {
                craSSTM.add(sstm);
            }
        }
        if(craSSTM.size() > 0) {
            checkDuplicateCRASSTM(craSSTM);
        }
        //End of Jira Ref: SH-17543 Akanksha Singh
    }

    public void onBeforeUpdate(List<Study_Site_Team_Member__c> recs, Map<Id, Study_Site_Team_Member__c> oldMap) {
        
        VT_R2_SstmScrCountValidator.validateSCRsCount(recs, oldMap);

        // Conquerors
        // Abhay
        // Jira Ref: SH-17438
        // Description: Commented by Abhay as we do not require this update code as of now.
        //              After architect review and discussion with Carl, this needs to commented.

        // // Conquerors
        // // Abhay & Akansksha
        // // Jira Ref: SH-17438, SH-17437 
        // VT_R2_SstmSharingHandler.handleBeforeUpdate(recs);
    }

    // Conquerors
    // Abhay
    // Jira Ref: SH-17438
    // Description: Commented by Abhay as we do not require this update code as of now.
    //              After architect review and discussion with Carl, this needs to commented.
    // public void onAfterUpdate(List<Study_Site_Team_Member__c> recs, Map<Id, Study_Site_Team_Member__c> oldMap) {
    //     VT_R2_SstmSharingHandler.handleAfterUpdate(recs);
    //     // Jira Ref: SH-17543 Akanksha Singh
    //     // Update SSTM user in related Virtual Site queue
    //     // Future method cannot be called from Batch or other future method 
    //     if(!System.isBatch() && !System.isFuture()) {
    //         List<Id> siteIds = new List<Id>();
    //         for(Study_Site_Team_Member__c sstm :recs) {
    //             if(sstm.VTR5_Associated_CRA__c != null) {
    //                 siteIds.add(sstm.VTD1_SiteID__c);
    //             }
    //         } 
    //         VT_R5_QueueBasedonSSTM.assignMemberToGroup(siteIds); 
    //     }
    //    //Jira Ref: SH-17543 Akanksha Singh
    // }
    // // End of code: Conquerors

    public void onAfterDelete(List<Study_Site_Team_Member__c> recs) {
        
        VT_R2_SstmSharingHandler.handleAfterDelete(recs);
        // Jira Ref: SH-17543 Akanksha Singh
        // Delete SSTM user from queue related to Virtual site
        // Future method cannot be called from Batch or other future method 
        if(!System.isBatch() && !System.isFuture()) {
            List<Id> siteIds = new List<Id>();
            for(Study_Site_Team_Member__c sstm :recs) {
				if(sstm.VTR5_Associated_CRA__c != null) {
					siteIds.add(sstm.VTD1_SiteID__c);
                }
            } 
            VT_R5_QueueBasedonSSTM.assignMemberToGroup(siteIds); 
        }
        // Jira Ref: SH-17543 Akanksha Singh
    }

    public void onAfterInsert(List<Study_Site_Team_Member__c> recs) {
        VT_R2_SstmSharingHandler.handleInsert(recs);

        //VT_R5_SSTMauditHandler.createSSAA(recs);
        VT_R5_CustomAuditHandler_Misc.onAfterInsertSstm(recs);

        // Jira Ref: SH-17543 Akanksha Singh
        // Add SSTM as Group Member in queue related to Virtual site
        // Future method cannot be called from Batch or other future method 
            if(!System.isBatch() && !System.isFuture()) {
            List<Id> siteIds = new List<Id>();
            for(Study_Site_Team_Member__c sstm :recs) {
                if(sstm.VTR5_Associated_CRA__c != null) {
                    siteIds.add(sstm.VTD1_SiteID__c);
                }


            } 
            VT_R5_QueueBasedonSSTM.assignMemberToGroup(siteIds); 
        }
        // Jira Ref: SH-17543 Akanksha Singh
    }

    // Jira Ref: SH-17543 Akanksha Singh
    // Method to check duplicate CRA SSTM
    public void checkDuplicateCRASSTM (List<Study_Site_Team_Member__c> recs) {
        
        List<Id> virSiteIdList = new List<Id>();
        List<Id> craList = new List<Id>();
        List<String> virSitewithCRAList = new List<String>();
        
        //Add Site Id and Associated CRA of new CRA SSTM record in list
        for(Study_Site_Team_Member__c sstm : recs) {
            virSiteIdList.add(sstm.VTD1_SiteID__c);
            craList.add(sstm.VTR5_Associated_CRA__c);
        }

        //Retrieve all CRA SSTM having same VTR5_Associated_CRA__c value from site 
        String  virSitewithCRAkey;
        for(Study_Site_Team_Member__c sstmVal : [Select Id,
                                                    VTD1_SiteID__c,
                                                    VTR5_Associated_CRA__c
                                                    FROM Study_Site_Team_Member__c
                                                    WHERE VTD1_SiteID__c IN:virSiteIdList
                                                    AND VTR5_Associated_CRA__c IN: craList
                                                    WITH SECURITY_ENFORCED ]) {
            virSitewithCRAkey = String.valueOf(sstmVal.VTD1_SiteID__c) + String.valueOf(sstmVal.VTR5_Associated_CRA__c);                                          
            virSitewithCRAList.add(virSitewithCRAkey);  
        }

        //If new CRA SSTM present in map, then it represents existing CRA, So Notify User 
        String virSiteWithCRA;
        for(Study_Site_Team_Member__c newCRASSTM : recs) {
            virSiteWithCRA = String.valueOf(newCRASSTM.VTD1_SiteID__c) + String.valueOf(newCRASSTM.VTR5_Associated_CRA__c);
            if(virSitewithCRAList.contains(virSiteWithCRA)) {
                newCRASSTM.addError(System.Label.VTR5_Duplicate_CRA_Error);  
            }

        }   

        }   
    //End of Jira Ref: SH-17543 Akanksha Singh
    }