/**
 * Created by Andrey Pivovarov on 4/28/2020.
 */

global with sharing class VT_R4_PushNotificationsBatch implements Database.Batchable<SObject> {
    private Datetime curDate;
    private VT_R4_PushNotificationsScheduler context;

    public VT_R4_PushNotificationsBatch(VT_R4_PushNotificationsScheduler context, Datetime dt) {
        curDate = dt.addSeconds(-dt.second());
        this.context = context;
    }
    /*for single run*/
    public VT_R4_PushNotificationsBatch() {
        curDate = System.now();
        curDate = curDate.addSeconds(-curDate.second());
    }

    global Iterable<SObject> start(Database.BatchableContext BC) {
        List<Video_Conference__c> videoConferences = new List<Video_Conference__c>();
        if (Test.isRunningTest() || (VTR4_ExternalScheduler__c.getInstance().VTR4_PushNotificationIsActive__c)) {
            videoConferences = [
                    SELECT Scheduled_For__c, VTD1_Scheduled_Visit_End__c,
                        (SELECT VTR4_Scheduled_for__c
                        FROM Schedulers_Push_Notifications__r)
                    FROM Video_Conference__c
                    WHERE (Scheduled_For__c >: curDate.addMinutes(15)
                    AND Scheduled_For__c <= :curDate.addHours(1))
                    AND VTD1_Actual_Visit__c != NULL
                    ORDER BY Scheduled_For__c
            ];
        }
        return videoConferences;
    }

    global void execute(Database.BatchableContext BC, List<Video_Conference__c> scope) {
        if (scope == null || scope.isEmpty()) return;
        Integer countSchedulerPushNs = [SELECT COUNT() FROM VTR4_Scheduler_PushNotifications__c];
        List<VTR4_Scheduler_PushNotifications__c> pushNotifications = new List<VTR4_Scheduler_PushNotifications__c>();
        for (Video_Conference__c videoConference : scope) {
            if (videoConference.Schedulers_Push_Notifications__r.size() == 0 && countSchedulerPushNs < 9500) {
                VTR4_Scheduler_PushNotifications__c pn = new VTR4_Scheduler_PushNotifications__c();
                pn.VTR4_Video_Conference__c = videoConference.Id;
                pn.VTR4_Scheduled_for__c = videoConference.Scheduled_For__c.addMinutes(-15);
                pushNotifications.add(pn);
            }
            else if (videoConference.Schedulers_Push_Notifications__r.size() != 0 && (Integer) (videoConference.Scheduled_For__c.getTime() -
                    videoConference.Schedulers_Push_Notifications__r[0].VTR4_Scheduled_for__c.getTime())/1000/60 != 15) {
                videoConference.Schedulers_Push_Notifications__r[0].VTR4_Scheduled_for__c =
                        videoConference.Scheduled_For__c.addMinutes(-15);
                videoConference.Schedulers_Push_Notifications__r[0].VTR4_ReadyToStart__c = false;
                pushNotifications.add(videoConference.Schedulers_Push_Notifications__r[0]);
            }
        }
        if (!pushNotifications.isEmpty()) upsert pushNotifications;
    }
    global void finish(Database.BatchableContext BC) {
        try {
            List<VTR4_Scheduler_PushNotifications__c> pushNotificationsToDelete = [
                    SELECT Id
                    FROM VTR4_Scheduler_PushNotifications__c
                    WHERE VTR4_Video_Conference__r.Scheduled_For__c <: curDate
                    OR VTR4_Video_Conference__r.Scheduled_For__c >: curDate.addHours(1)
            ];
            delete pushNotificationsToDelete;
        }
        catch (Exception e) {
            System.debug('VTR4_Scheduler_PushNotifications__c delete exception ' + e);
        }
        finally {
            if (this.context != null) this.context.scheduleJob(context.getAlignedTimeJob(System.now()));
        }
    }
}