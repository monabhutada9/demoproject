public without sharing class VT_R2_MonitoringVisitFormController {

    @AuraEnabled
    public static String getNewMonitoringVisit(Id virtualSiteId) {
        try {
        	MonitoringVisit newMonitoring = null;

            if (String.isBlank(virtualSiteId)) {
                return '{}';
            }

            Virtual_Site__c virtualSite = [SELECT Id,
                                                  Name,
                                                  VTD1_Study_Team_Member__c,
                                                  VTD1_Study_Team_Member__r.Id,
                                                  VTD1_Study_Team_Member__r.Name,
                                                  VTD1_Study_Team_Member__r.User__c,
                                                  VTD1_Study__c,
                                                  VTD1_Study__r.Id,
                                                  VTD1_Study__r.Name
                                           FROM Virtual_Site__c
                                           WHERE Id = :virtualSiteId];

            if (virtualSite != null) {
                newMonitoring = new MonitoringVisit();
                newMonitoring.studyId = virtualSite.VTD1_Study__c;
                newMonitoring.studyName = virtualSite.VTD1_Study__r.Name;
                newMonitoring.virtualSiteId = virtualSite.Id;
                newMonitoring.virtualSiteName = virtualSite.Name;
                newMonitoring.duration = 1;
                newMonitoring.assignedPI = virtualSite.VTD1_Study_Team_Member__r.User__c;
            }

        	return JSON.serialize(newMonitoring);
		} catch (Exception e){
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    //Removed getVisitCRA method by Priyanka Ambre. SH-17441

    @AuraEnabled
    public static String getVisitDates(DateTime plannedDate, DateTime schDate, Integer duration) {
        VisitDate visitDate = new VisitDate();

        if (plannedDate != null) {
            visitDate.visitPlannedEndDate = plannedDate.addHours(duration);
        }
        if (schDate != null) {
            visitDate.visitEndDate = schDate.addHours(duration);
        }

        return JSON.serialize(visitDate);
    }

    //Removed visitCRAId, visitCRAName by Priyanka Ambre. SH-17441
    public class MonitoringVisit {
        public Id studyId {get; set;}
        public String studyName {get; set;}
        public Id virtualSiteId {get; set;}
        public String virtualSiteName {get; set;}
        public Id siteAddressId {get; set;}
        public Integer duration {get; set;}
        public String siteVisitType {get; set;}
        public String studySiteVisitStatus {get; set;}
        public DateTime visitPlannedStartDate {get; set;}
        public DateTime visitPlannedEndDate {get; set;}
        public DateTime visitStartDate {get; set;}
        public DateTime visitEndDate {get; set;}
        public Id assignedPI {get; set;}
    }

    public class VisitDate {
        public DateTime visitPlannedEndDate {get; set;}
        public DateTime visitEndDate {get; set;}
    }

}