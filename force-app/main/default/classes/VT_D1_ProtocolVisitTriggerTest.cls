/**
 * Created by User on 19/05/14.
 */
@isTest
public with sharing class VT_D1_ProtocolVisitTriggerTest {

    public static void deleteTest(){
        Test.startTest();
        VTD1_ProtocolVisit__c protocol = (VTD1_ProtocolVisit__c) new DomainObjects.VTD1_ProtocolVisit_t().persist();
        delete protocol;
        Test.stopTest();
    }
}