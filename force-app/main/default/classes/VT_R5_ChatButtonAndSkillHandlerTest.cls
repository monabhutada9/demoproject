@isTest
public class VT_R5_ChatButtonAndSkillHandlerTest {
    

    @TestSetup
    public static void testSetup(){
        Test.startTest();
        new DomainObjects.Study_t()
                .setName('TestStudy1')
                .setVT_R5_Is_study_skill_created(true)
                .persist();
        new DomainObjects.Study_t()
                .setName('TestStudy2')
                .setVT_R5_Is_study_skill_created(true)
                .persist();
        new DomainObjects.Study_t()
                .setName('TestStudy3')
                .setVT_R5_Is_study_skill_created(true)
                .persist();
        Test.stopTest();
    }
    
    @IsTest() 
    static void testBehaviour(){
        

        List<HealthCloudGA__CarePlanTemplate__c> studies = [
                        SELECT Id, Name, VT_R5_Is_study_skill_created__c 
                        FROM HealthCloudGA__CarePlanTemplate__c 
                        WHERE VT_R5_Is_study_skill_created__c = true LIMIT 3];
        if (studies.size() == 3) { 
            List<User> lstUsers = new List<User>();
            VT_D1_TestUtils.loadProfiles();
            VT_D1_TestUtils.initUserRole();
            lstUsers.add(VT_D1_TestUtils.createUserByProfile('Study Concierge', 'TestUser1@iqvia.com.masked'));
            lstUsers.add(VT_D1_TestUtils.createUserByProfile('Study Concierge', 'TestUser2@iqvia.com.masked'));
            lstUsers.add(VT_D1_TestUtils.createUserByProfile('Study Concierge', 'TestUser3@iqvia.com.masked'));

            VT_R5_SerResSkillAssignForSC.blockSrCreation = true; //servResources will be created on STM insert, otherwise "dublicate" error occurs

            insert lstUsers;
            ServiceResource newServiceResource = VT_R5_SkillAndChatButtonUtils.createServiceResource(lstUsers[0]);  
            List<Study_Team_Member__c> lstSTMsToBeAdded = new List<Study_Team_Member__c>();
            Id recordTypeId = VT_D1_ConstantsHelper.RECORD_TYPE_ID_STUDY_TEAM_MEMBER_SC;
            Integer i = 0;
            for (User usr : lstUsers) {
                lstSTMsToBeAdded.add(new Study_Team_Member__c(User__c=usr.Id, Study__c=studies[i++].Id,RecordTypeId = recordTypeId));
            }          

            Test.startTest();       
            insert lstSTMsToBeAdded;
            Test.stopTest();

            List<ServiceResource> servResources = [SELECT Id, (SELECT Id, SkillId FROM ServiceResourceSkills) 
                                                FROM ServiceResource 
                                                WHERE RelatedRecordId IN:lstUsers];
            System.assertEquals(servResources.size(), lstUsers.size(),'ServiceResource is created for each user.');

            //for(ServiceResource sr : servResources) {
            //    System.assertEquals(1, sr.ServiceResourceSkills.size(),'One ServiceResourceSkill is created');
            //}

            List<Study_Team_Member__c> lstStms = [SELECT Id, Study__c, User__c 
                                                FROM Study_Team_Member__c 
                                                WHERE User__c IN:lstUsers ];
            lstStms[0].User__c = lstUsers[2].Id;
            lstStms[1].User__c = lstUsers[1].Id;
            lstStms[2].User__c = lstUsers[0].Id;
            update lstStms;
            servResources.clear();
            servResources = [SELECT Id, (SELECT Id, SkillId FROM ServiceResourceSkills) 
                                                FROM ServiceResource 
                                                WHERE RelatedRecordId IN:lstUsers];
            System.assertEquals(servResources.size(), lstUsers.size(),'ServiceResource is created for each user.');

            //for(ServiceResource sr : servResources) {
            //    System.assertEquals(1, sr.ServiceResourceSkills.size(),'One ServiceResourceSkill is created');
            //}

            delete lstStms;
            servResources.clear();
            servResources = [SELECT Id, RelatedRecordId, (SELECT Id, SkillId FROM ServiceResourceSkills) 
                            FROM ServiceResource 
                            WHERE RelatedRecordId IN : lstUsers];            
            System.assertEquals(servResources.size(), lstUsers.size(),'ServiceResource is created for each user.');
            for(ServiceResource sr : servResources) {
                System.assertEquals(0, sr.ServiceResourceSkills.size(),'ServiceResourceSkill is deleted');
            }
        } else {
            System.debug('Test Failed');
        }

        

    }
}