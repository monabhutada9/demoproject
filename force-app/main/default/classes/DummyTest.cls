@isTest(SeeAllData=true)
public class DummyTest {
	
    @isTest(SeeAllData=true)
    public static void methodName(){
        
        String vtslUserProfileId = [SELECT Id FROM Profile WHERE Profile.Name = 'Virtual Trial Study Lead'].Id;
        User vtslUser = new User(ProfileId = vtslUserProfileId,
                                 LastName = 'last',
                                 Email = 'puser000@amamama.com',
                                 Username = 'puser000@amamama.com' + System.currentTimeMillis(),
                                 CompanyName = 'TEST',
                                 Title = 'title',
                                 Alias = 'alias',
                                 TimeZoneSidKey = 'America/Los_Angeles',
                                 EmailEncodingKey = 'UTF-8',
                                 LanguageLocaleKey = 'en_US',
                                 LocaleSidKey = 'en_US');
        insert vtslUser;
        set<Id> caseIdSet = new set<Id>{'5001w000006RrsJ','5001w000006RruK','5001w000006RtaP',
                                        '5001w000006RtaP','5001w000006Rutn','5001w000006Ruuv',
                                        '5001w000006RyhX','5001w000006RzFr','5001w000006RzHE',
                                        '5001w000006RzXp','5001w000006RzpM','5001w000006RzrI',
                                        '5001w000006S12E','5001w000006S15k','5001w000006S3lH',
                                        '5001w000006S4Qy','5001w000006S7kP','5001w000006S8RP'};
                                        
        list<Case> Caselist=[select id,VTD1_Patient_User__c,VTD1_Reconsent_Needed__c from Case where id in:caseIdSet];
        list<VTD1_NotificationC__c> NotificationcList = new list<VTD1_NotificationC__c>();
        list<CaseShare> userSharelist = new list<CaseShare>();

        for(Case cs: Caselist){
            CaseShare u = new CaseShare();
            u.CaseId = cs.Id;
            u.UserOrGroupId = vtslUser.Id;
            u.RowCause = 'Manual'; 
            u.CaseAccessLevel = 'Edit';
            userSharelist.add(u);
        }
        if(!userSharelist.isempty()){
            insert userSharelist;
        }
        
        Test.startTest();
        System.runAs(vtslUser){
            for(Case cs: Caselist){
                cs.VTD1_Reconsent_Needed__c=true;
            }
            if(!Caselist.isempty())
                update Caselist;
        }
        Test.stopTest();
    }
}