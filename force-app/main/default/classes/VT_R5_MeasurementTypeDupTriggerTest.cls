/**
 * Created by user on 30-Jun-20.
 */

@IsTest
private class VT_R5_MeasurementTypeDupTriggerTest {
    @TestSetup
    private static void setup() {
        DomainObjects.Account_t account_t = new DomainObjects.Account_t().setRecordTypeByName('Sponsor');
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(account_t);
        study.persist();

        Account account = (Account) account_t.toObject();

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
                .setFirstName('Patient')
                .setLastName('Patient')
                .setAccountId(account.Id);

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile('Patient');
        patientUser.persist();
        Case caseX = (Case) new DomainObjects.Case_t()
                .addPIUser(new DomainObjects.User_t())
                .addVTD1_Patient_User(patientUser)
                .setStudy(((HealthCloudGA__CarePlanTemplate__c) study.toObject()).Id)
                .setContactId(((User) patientUser.toObject()).ContactId)
                .setRecordTypeByName('CarePlan')
                .setAccountId(account.Id)
                .persist();

        DomainObjects.VTD1_Protocol_Delivery_t pDelivery = new DomainObjects.VTD1_Protocol_Delivery_t()
                .addCare_Plan_Template(study);

        DomainObjects.VTD1_Protocol_Kit_t pKit = new DomainObjects.VTD1_Protocol_Kit_t()
                .setRecordTypeByName('VTD1_Connected_Devices')
                .addVTD1_Care_Plan_Template(study)
                .addVTD1_Protocol_Delivery(pDelivery);
        pKit.persist();

        DomainObjects.Product2_t product = new DomainObjects.Product2_t()
                .setProductName('NewProduct')
                .setRecordTypeByName('VTD1_Connected_Devices');
        product.persist();
        DomainObjects.HealthCloudGA_EhrDevice_t ehrDevice = new DomainObjects.HealthCloudGA_EhrDevice_t()
                .addProduct(product)
                .addVTD1_Care_Plan_Template(study)
                .addVTD1_Protocol_Kit(pKit)
                .setVTD1_Kit_Type('Connected Devices')
                .addMeasurementType('heart_rate;blood_pressure');
        ehrDevice.persist();
        DomainObjects.Patient_Device_t patientDevice = new DomainObjects.Patient_Device_t()
                .addEhrDevice(ehrDevice)
                .setModelName('TestPatientDevice')
//                .setDeviceKeyId('test1234')
                .setCaseId(caseX.Id)
                .setRecordTypeByName('VTD1_Connected_Device');

        patientDevice.persist();
        patientDevice.setDeviceKeyId('test123');
        update (VTD1_Patient_Device__c) patientDevice.toObject();
        DomainObjects.VTR5_StudyMeasurementsConfiguration_t studyConfig = new DomainObjects.VTR5_StudyMeasurementsConfiguration_t()
                .addStudy(study)
                .addEhrDevice(ehrDevice)
                .addHighBoundary(10)
                .addLowBoundary(5)
                .addLowerBoundary(7)
                .addUpperBoundary(9)
                .addMeasurementType('heart_rate');

        studyConfig.persist();

        DomainObjects.VTR5_StudyMeasurementsConfiguration_t studyConfig1 = new DomainObjects.VTR5_StudyMeasurementsConfiguration_t() //TODO should we remove studyConfig1, studyConfig2 ? Contact with Vlad
                .addStudy(study)
                .addEhrDevice(ehrDevice)
                .addHighBoundary(15)
                .addLowBoundary(10)
                .addLowerBoundary(8)
                .addUpperBoundary(12)
                .addMeasurementType('pressure_sys');

        studyConfig1.persist();

//        DomainObjects.VTR5_StudyMeasurementsConfiguration_t studyConfig2 = new DomainObjects.VTR5_StudyMeasurementsConfiguration_t()
//                .addStudy(study)
//                .addEhrDevice(ehrDevice)
//                .addHighBoundary(5)
//                .addLowBoundary(2)
//                .addLowerBoundary(1)
//                .addUpperBoundary(3)
//                .addMeasurementType('pressure_dia');
//
//        studyConfig2.persist();

    }

    @IsTest
    private static void testInsertFailed() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM  HealthCloudGA__CarePlanTemplate__c];
        HealthCloudGA__EhrDevice__c ehrDevice = [SELECT Id FROM HealthCloudGA__EhrDevice__c];
        VTR5_StudyMeasurementsConfiguration__c studyConfig = new VTR5_StudyMeasurementsConfiguration__c(
                VTR5_Study__c = study.Id,
                VTR5_ProtocolDevice__c = ehrDevice.Id,
                VTR5_HighBoundary__c = 5,
                VTR5_LowBoundary__c = 2,
                VTR5_MeasurementType__c = 'pressure_sys'
        );
        String dmlMessage;
        try {
            insert studyConfig;
        }
        catch(DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        System.assertEquals('You cannot create the same Measurement Type as you already have.',dmlMessage);
    }

    @IsTest
    private static void testInsertValid() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM  HealthCloudGA__CarePlanTemplate__c];
        HealthCloudGA__EhrDevice__c ehrDevice = [SELECT Id FROM HealthCloudGA__EhrDevice__c];
        VTR5_StudyMeasurementsConfiguration__c studyConfig = new VTR5_StudyMeasurementsConfiguration__c(
                VTR5_Study__c = study.Id,
                VTR5_ProtocolDevice__c = ehrDevice.Id,
                VTR5_HighBoundary__c = 5,
                VTR5_LowBoundary__c = 2,
                VTR5_MeasurementType__c = 'pressure_dia'
        );
        String dmlMessage;
        try {
            insert studyConfig;
        }
        catch(DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        System.debug('dmlMessage ' + dmlMessage);
        System.assertEquals(5,studyConfig.VTR5_HighBoundary__c);
    }

    @IsTest
    private static void testValidUpdateMeasurements() {
        VTR5_StudyMeasurementsConfiguration__c studyConfig = [SELECT Id,Name, VTR5_MeasurementType__c FROM VTR5_StudyMeasurementsConfiguration__c LIMIT 1];
        studyConfig.VTR5_MeasurementType__c = 'weight';
        update studyConfig;
    }

    @IsTest
    private static void testFailedUpdateMeasurements() {
        VTR5_StudyMeasurementsConfiguration__c studyConfig = [SELECT Id,Name, VTR5_MeasurementType__c FROM VTR5_StudyMeasurementsConfiguration__c LIMIT 1];
        studyConfig.VTR5_MeasurementType__c = 'pressure_sys';
        String dmlMessage;
        try {
            update studyConfig;
        }
        catch(DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        System.assertEquals('You cannot update the same Measurement Type as you already have.',dmlMessage);
    }
}