public without sharing class VT_D1_SurveyHandlerHelper {
    private static final String TN_CATALOG_CODE_FOR_PI_SCR = 'N051';
    private static final String TN_CATALOG_CODE_FOR_PG = 'N574';
    private static final Id EXTERNAL_RT_ID = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('VTR5_External').getRecordTypeId();
    private static final Id SOURCE_FORM_RT_ID = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('VTR5_SourceForm').getRecordTypeId();

    public static Boolean isScoringDone(VTD1_Survey__c rec) {
        return rec.VTD1_Scoring_Due_Date__c != null && rec.VTD1_Status__c != null &&
                (rec.VTD1_Status__c.equalsIgnoreCase(VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED) || rec.VTD1_Status__c.equalsIgnoreCase(VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED_PARTIALLY_COMPLETED));
    }
    public static Boolean isSubmitted(VTD1_Survey__c rec) {
        return rec.VTD1_Status__c != null && rec.VTD1_Status__c.equalsIgnoreCase(VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED);
    }
    public static Boolean isEcoaEproDiary(VTD1_Survey__c rec) {
        return rec.RecordTypeId == EXTERNAL_RT_ID;
    }
    public static Boolean isEcoaClinRoDiary(VTD1_Survey__c rec) {
        return rec.RecordTypeId == SOURCE_FORM_RT_ID;
    }
    public static Boolean isCompleted(VTD1_Survey__c rec) {
        return rec.VTD1_Status__c != null && rec.VTD1_Status__c.equalsIgnoreCase(VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_COMPLETED);
    }
    public static Boolean isMissed(VTD1_Survey__c rec) {
        return rec.VTD1_Status__c != null && rec.VTD1_Status__c.equalsIgnoreCase(VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_MISSED);
    }
    public static Boolean isReviewed(VTD1_Survey__c rec) {
        return rec.VTD1_Status__c == VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED || rec.VTD1_Status__c == VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED_PARTIALLY_COMPLETED;
    }
    public static void handleStarted(List<VTD1_Survey__c> startedSurveys) {
        for (VTD1_Survey__c item : startedSurveys) {
            item.VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_IN_PROGRESS;
        }
    }
    public static void handleReset(List<VTD1_Survey__c> resetSurveys) {
        for (VTD1_Survey__c item : resetSurveys) {
            item.VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_READY_TO_START;
        }
    }
    public static void autoScoreAnswers(List<VTD1_Survey__c> submittedSurveys) {
        List<VTD1_Survey_Answer__c> answers = [
            SELECT Id, VTD1_Question__r.VTR2_Protocol_eDiary_Scale__c, VTR2_English_Answer__c,
                        VTD1_Question__r.VTR2_Auto_Score_Default__c, VTD1_Question__r.VTD1_Type__c,
                        VTD1_Question__r.VTR4_AutoScoreDefaultForSuppressedBranch__c,
                        VTD1_Question__r.VTR4_Branch_Parent__c,
                        VTD1_Question__r.VTR4_Response_Trigger__c,
                        VTD1_Question__r.VTR2_Scoring_Type__c,
                        VTD1_Question__c
            FROM VTD1_Survey_Answer__c
            WHERE VTD1_Survey__c IN :submittedSurveys
        ];
        List<VTD1_Survey_Answer__c> answersToScore = new List<VTD1_Survey_Answer__c>();
        Map<Id, String> answersByParentQuestionMap = new Map<Id, String>();
        for(Integer i = 0; i < answers.size(); i++) {
            if(answers[i].VTD1_Question__r.VTR2_Scoring_Type__c == 'Auto') {
                answersToScore.add(answers[i]);
            }
            answersByParentQuestionMap.put(answers[i].VTD1_Question__c, answers[i].VTR2_English_Answer__c);
        }
        if (! answersToScore.isEmpty()) {
            List<Id> scaleIds = new List<Id>();
            for (VTD1_Survey_Answer__c answer : answersToScore) {
                if (answer.VTD1_Question__r.VTR2_Protocol_eDiary_Scale__c != null) {
                    scaleIds.add(answer.VTD1_Question__r.VTR2_Protocol_eDiary_Scale__c);
                }
            }
            Map<Id, Map<String, Decimal>> scaleResponseScoreMap = new Map<Id, Map<String, Decimal>>(); // scaleId => response value => response score
            for (VTR2_Protocol_eDiary_Scale__c scale : [
                SELECT Id, (
                    SELECT VTR2_Response__c, VTR2_Score__c
                    FROM Protocol_eDiary_Scale_Responses__r
                    WHERE VTR2_Response__c != null
                    AND VTR2_Score__c != null
                )
                FROM VTR2_Protocol_eDiary_Scale__c
                WHERE Id IN :scaleIds
            ]) {
                scaleResponseScoreMap.put(scale.Id, new Map<String, Decimal>());
                for (VTR2_ProtocoleDiaryScaleResponse__c scaleResponse : scale.Protocol_eDiary_Scale_Responses__r) {
                    scaleResponseScoreMap.get(scale.Id).put(scaleResponse.VTR2_Response__c, scaleResponse.VTR2_Score__c);
                }
            }
            for (VTD1_Survey_Answer__c answer : answersToScore) {
                Boolean responseFound = false;
                Decimal scoreTotal = 0;
                if (answer.VTR2_English_Answer__c != null && scaleResponseScoreMap.containsKey(answer.VTD1_Question__r.VTR2_Protocol_eDiary_Scale__c)) {
                    Map<String, Decimal> scoreMap = scaleResponseScoreMap.get(answer.VTD1_Question__r.VTR2_Protocol_eDiary_Scale__c);
                    List<String> answerStrings = answer.VTD1_Question__r.VTD1_Type__c == 'Multi Select Drop Down' ?
                        answer.VTR2_English_Answer__c.split(';') : new List<String> { answer.VTR2_English_Answer__c };
                    for (String answerString : answerStrings) {
                        if (scoreMap.containsKey(answerString)) {
                            scoreTotal += scoreMap.get(answerString);
                            responseFound = true;
                        }
                    }
                }
                if(responseFound) {
                    answer.VTD1_Score__c = scoreTotal;
                } else {
                    answer.VTD1_Score__c = answer.VTD1_Question__r.VTR2_Auto_Score_Default__c;
                    if(answersByParentQuestionMap.containsKey(answer.VTD1_Question__r.VTR4_Branch_Parent__c)
                            && answersByParentQuestionMap.get(answer.VTD1_Question__r.VTR4_Branch_Parent__c) != answer.VTD1_Question__r.VTR4_Response_Trigger__c) {
                        answer.VTD1_Score__c = answer.VTD1_Question__r.VTR4_AutoScoreDefaultForSuppressedBranch__c;
                    }
                }
            }
            update answersToScore;
        }
    }
    public static void setFieldsWhenScoringDone(List<VTD1_Survey__c> recs) {
        for (VTD1_Survey__c item : recs) {
            item.VTD1_ePRO_Scored__c = true;
        }
    }
    private static void createPCFSafetyConcernNotification(List<Case> recs, Map<String, List<Id>> receiverMap, String tnCatalog) {
        List<String> tnCatalogCodes = new List<String>();
        List<Id> sourceIds = new List<Id>();
        for (Case item : recs) {
            tnCatalogCodes.add(tnCatalog);
            sourceIds.add(item.Id);
        }
        VT_D2_TNCatalogNotifications.generateNotifications(tnCatalogCodes, sourceIds, new Map<String, String>(), null, receiverMap);
    }
    public static void createPcfsForSafetyConcerns(List<VTD1_Survey__c> recs) {
        Map<Id, Case> patientMap = getPatientMap(recs);
        Map<String, List<Id>> receiverMap = new Map<String, List<Id>>();
        List<Id> pgUserIdsList = new List<Id>();
        List<Id> receiverList;
        List<Case> newPcfs = new List<Case>();
        List<Case> newPcfsWithPG = new List<Case>();
        List<List<Id>> receiverListList = new List<List<Id>>();
        Id pcfRecTypeId = VT_D1_HelperClass.getRTMap().get(VT_R4_ConstantsHelper_AccountContactCase.SOBJECT_CASE).get(VT_R4_ConstantsHelper_AccountContactCase.RT_PCF);
        for (VTD1_Survey__c item : recs) {
            Case patientCase = patientMap.get(item.VTD1_CSM__c);
            Id reviewerId;
            if (item.VTR2_Reviewer_User__c == 'Site Coordinator') {
                reviewerId = patientCase.VTD1_Virtual_Site__r.VTR2_Primary_SCR__c;
            }
            else {
                reviewerId = patientCase.VTD1_Primary_PG__c;
            }
            if (patientCase != null && reviewerId != null) {
                Case pcfCase = new Case(
                    RecordTypeId = pcfRecTypeId,
                    OwnerId = reviewerId,
                    VTD1_Clinical_Study_Membership__c = item.VTD1_CSM__c,
                    ContactId = patientCase.ContactId,
                    VTD1_PCF_Safety_Concern_Indicator__c = 'Possible',
                        VTD1_PCF_Safety_Concern_Status_Comment__c = item.Name + ' - Possible eDiary Safety Concern');
                newPcfs.add(pcfCase);
                receiverList = new List<Id>();
                if (patientCase.VTD1_Virtual_Site__r.VTR2_Primary_SCR__c != null) {
                    receiverList.add(patientCase.VTD1_Virtual_Site__r.VTR2_Primary_SCR__c);
                }
                if (patientCase.VTD1_PI_user__c != null) {
                    receiverList.add(patientCase.VTD1_PI_user__c);
                }
                if (patientCase.VTD1_Primary_PG__c != null) {
                    newPcfsWithPG.add(pcfCase);
                }
                receiverListList.add(receiverList);
            }
        }
        system.debug(newPcfs);
        if (! newPcfs.isEmpty()) {
            insert newPcfs;
            for(Integer i = 0; i < newPcfs.size(); i++) {
                if (!receiverListList[i].isEmpty()) {
                    receiverMap.put(TN_CATALOG_CODE_FOR_PI_SCR + newPcfs[i].Id, receiverListList[i]);
                }
            }
            if(!receiverMap.isEmpty()) {
                createPCFSafetyConcernNotification(newPcfs, receiverMap, TN_CATALOG_CODE_FOR_PI_SCR); //NotificationC for PI and SCR
            }
            if (!newPcfsWithPG.isEmpty()) {
                createPCFSafetyConcernNotification(newPcfsWithPG, null, TN_CATALOG_CODE_FOR_PG); //Custom Notification for PG
            }
        }
    }
    private static Map<Id, Case> getPatientMap(List<VTD1_Survey__c> recs) {
        List<Id> caseIds = new List<Id>();
        for (VTD1_Survey__c item : recs) {
            caseIds.add(item.VTD1_CSM__c);
        }
        return new Map<Id, Case>([
            SELECT Id, VTD1_Primary_PG__c, ContactId, VTD1_Subject_ID__c, VTD1_Patient__r.VTD1_First_Name__c,
                VTD1_Patient__r.VTD1_Last_Name__c, VTD1_Study__c, VTD1_Virtual_Site__r.VTR2_Primary_SCR__c,
                VTD1_PI_user__c
            FROM Case
            WHERE Id IN :caseIds
        ]);
    }
    private static Boolean isSurveyValidForEventCreation(VTD1_Survey__c rec) {
        return
            rec.VTD1_Due_Date__c != null &&
            rec.VTD1_Trigger__c != null &&
            (rec.VTD1_Patient_User_Id__c != null || rec.VTD1_Caregiver_User_Id__c != null)
        ;
    }

    /**
    * Hide eCoa notifications for patient and caregiver if related surveys are completed or missed
    * @author Olya Baranova
    * @param completedOrMissedSurveys list of eCoa diaries that are not in the 'Due Soon' status
    * @date 11-Aug-20
    * @issue SH-14468
    */
    public static void hideNotifications(List<VTD1_Survey__c> completedOrMissedSurveys) {
        List<VTD1_NotificationC__c> notificationsToHide = new List<VTD1_NotificationC__c>();
        Set<String> ruleGUIDs = new Set<String>();
        Set<Id> patientIds = new Set<Id>();
        List<String> uniqueKeys = new List<String>(); //ruleGUID + PatientId

        for (VTD1_Survey__c diary : completedOrMissedSurveys) {
            if (diary.VTR5_eCoa_ruleGuid__c != null) {
                ruleGUIDs.add(diary.VTR5_eCoa_ruleGuid__c);
                patientIds.add(diary.VTD1_Patient_User_Id__c);
                uniqueKeys.add(diary.VTR5_eCoa_ruleGuid__c + diary.VTD1_Patient_User_Id__c);
            }
        }

        if (!ruleGUIDs.isEmpty() && !patientIds.isEmpty()) {
            List <VTD1_NotificationC__c> notifications = new List<VTD1_NotificationC__c>();
            for (VTD1_NotificationC__c notification : [
                    SELECT Id, VTR5_HideNotification__c,
                            VTR5_eCoa_ruleGuid__c,
                            VTD1_Receivers__c
                    FROM VTD1_NotificationC__c
                    WHERE OwnerId IN :patientIds
                    AND VTR5_eCoa_ruleGuid__c IN :ruleGUIDs
                    AND VTR5_HideNotification__c = FALSE
            ]) {
                if (uniqueKeys.contains(notification.VTR5_eCoa_ruleGuid__c + notification.VTD1_Receivers__c)) {
                    notifications.add(notification);
                }
            }
            if (!notifications.isEmpty()) {
                List<Id> patientNotificationIds = new List<Id>();
                for (VTD1_NotificationC__c notification : notifications) {
                    notification.VTR5_HideNotification__c = true;
                    notificationsToHide.add(notification);
                    patientNotificationIds.add(notification.Id);
                }
                for (VTD1_NotificationC__c cgNotification : [
                        SELECT Id, VTR5_HideNotification__c
                        FROM VTD1_NotificationC__c
                        WHERE VTR2_PatientNotificationId__c IN :patientNotificationIds
                        AND VTR5_eCoa_ruleGuid__c IN :ruleGUIDs
                        AND VTR5_HideNotification__c = FALSE
                ]) {
                    cgNotification.VTR5_HideNotification__c = true;
                    notificationsToHide.add(cgNotification);
                }
                update notificationsToHide;
            }
        }

    }

    /**
    * Create tasks for the patient and the caregiver if new surveys from eCoa have any status other than 'Completed'
    * @author Olya Baranova
    * @param notCompletedSurveys list of eCoa diaries that are not in the 'Completed' status
    * @date 11-Aug-20
    * @issue SH-14337
    */
    public static void createTasks (List<VTD1_Survey__c> notCompletedSurveys) {
        if (!System.isBatch() && !System.isFuture() && !Test.isRunningTest() && Limits.getFutureCalls() < Limits.getLimitFutureCalls()) {
            createTasksFuture(VT_D1_HelperClass.getIdsFromObjs(notCompletedSurveys));
        } else {
            createTasksNow(notCompletedSurveys);
        }
    }

    @Future
    public static void createTasksFuture (List<Id> surveyIds) {
        createTasksNow([
            SELECT Id, Name, VTD1_Patient_User_Id__c, VTD1_Date_Available__c, VTD1_Due_Date__c, VTD1_CSM__c, VTR5_TranslatedName__c
            FROM VTD1_Survey__c
            WHERE Id IN :surveyIds
        ]);
    }

    public static void createTasksNow (List<VTD1_Survey__c> notCompletedSurveys) {
        List<Task> tasks = new List<Task>();
        Id recTypeId = Task.getSObjectType().getDescribe().getRecordTypeInfosByName().get('SimpleTask').getRecordTypeId();
        for (VTD1_Survey__c diary : notCompletedSurveys) {
            String traslatedDiaryName = String.isEmpty(diary.VTR5_TranslatedName__c) ? diary.Name : diary.VTR5_TranslatedName__c;
            if (diary.VTD1_Patient_User_Id__c != null) {
                Task subjectTask = new Task(
                        Subject = traslatedDiaryName,
                        OwnerId = diary.VTD1_Patient_User_Id__c,
                        VTR5_DiaryId__c = diary.Id,
                        VTR5_StartDate__c = diary.VTD1_Date_Available__c,
                        VTR5_EndDate__c = diary.VTD1_Due_Date__c,
                        Category__c = 'Other Tasks',
                        VTD1_Case_lookup__c = diary.VTD1_CSM__c,
                        VTD1_Caregiver_Clickable__c = true,
                        VTD2_My_Task_List_Redirect__c = 'my-diary?banner=' + EncodingUtil.urlEncode(traslatedDiaryName, 'UTF-8'),
                        RecordTypeId = recTypeId,
                        VTR5_PreventPBInvocation__c = true
                );
                tasks.add(subjectTask);
            }
        }

        insert tasks;
    }

    /**
    * Close tasks for the patient and the caregiver if related surveys from eCoa are completed or missed
    * @author Olya Baranova
    * @param completedOrMissedSurveys list of eCoa diaries that are not in the 'Due Soon' status
    * @date 12-Aug-20
    * @issue SH-14344
    */
    public static void closeTasks (List<VTD1_Survey__c> completedOrMissedSurveys) {
        List<Task> tasksToClose = [SELECT Id FROM Task WHERE VTR5_DiaryId__c IN :completedOrMissedSurveys AND VTR5_DiaryId__c != null];
        for (Task task : tasksToClose) {
            task.Status = 'Completed';
        }
        update tasksToClose;
    }

    /**
    * Update review checkbox if diary was reviewed
    * @param reviewedDiaries list of diaries that have a 'Review Required' status
    * @date 30-Oct-20
    */
    public static void updateReviewedCheckbox(List<VTD1_Survey__c> reviewedDiaries) {
        for (VTD1_Survey__c diary : reviewedDiaries) {
            if (!diary.VTR5_Reviewed__c) {
                diary.VTR5_Reviewed__c = true;
            }
        }
    }
    /**
    * Update caregiver checkbox if diary was completed by caregiver
    * @param submitted list of diaries that have been submitted by patient or caregiver
    * @date 30-Oct-20
    */
    public static void updateCaregiverCheckbox(List<VTD1_Survey__c> submitted) {
        for (VTD1_Survey__c diary : submitted) {
            diary.VTR5_CompletedBy__c = diary.LastModifiedById;
        }
    }

    public static void populateAlertKey(List<VTD1_Survey__c> recs, Map<Id, VTD1_Survey__c> oldRecs) {

        Set<Id> caseIds = new Set<Id>();
        for (VTD1_Survey__c diary : recs) {
            if (isEcoaClinRoDiary(diary) || diary.VTD1_Due_Date__c == null) {
                continue;
            }
            if (oldRecs == null) {
                caseIds.add(diary.VTD1_CSM__c);
                continue;
            }

            Boolean dueDateIsChanged = diary.VTD1_Due_Date__c != oldRecs.get(diary.Id).VTD1_Due_Date__c && diary.VTR5_DiaryStudyKey__c != null;
            if (dueDateIsChanged || diary.VTR5_eCoaAlertsKey__c == null) {
                String dueDateGmt = diary.VTD1_Due_Date__c.formatGmt('yyyy-MM-dd HH:mm:ss');
                diary.VTR5_eCoaAlertsKey__c = diary.VTR5_DiaryStudyKey__c + dueDateGmt;
            }
        }

        if (!caseIds.isEmpty()) {
            Map<Id, Case> caseMap = new Map<Id, Case>([SELECT Id, VTD1_Study__c FROM Case WHERE Id IN :caseIds]);
            for (VTD1_Survey__c item : recs) {
                if (caseMap.containsKey(item.VTD1_CSM__c)) {
                    String dueDateGmt = item.VTD1_Due_Date__c.formatGmt('yyyy-MM-dd HH:mm:ss');
                    item.VTR5_eCoaAlertsKey__c = item.Name + caseMap.get(item.VTD1_CSM__c).VTD1_Study__c + dueDateGmt;
                }
            }
        }
    }
}