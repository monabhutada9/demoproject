public with sharing class VT_D1_Patient_Upload_PhotoController {
	@AuraEnabled
    public static void setProfilePhoto(String userId, String photoId){
    	 ConnectApi.UserProfiles.setPhoto(Network.getNetworkId(), userId, photoId, null);	
    }
}