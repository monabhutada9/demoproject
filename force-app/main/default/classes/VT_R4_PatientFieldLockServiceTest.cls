/**
 * Created by Anatoliy Kozel on 1/16/2020.
 */

@IsTest
public with sharing class VT_R4_PatientFieldLockServiceTest {

   public static void testVerifyLockedFieldsSoloPatientSet() {
        List<Id> testPatientIds = generateRegPatientIds(2);
        stubQueries(testPatientIds);

        Test.startTest();
        System.debug('QueryLims: ' + Limits.getQueries());
        List<Id> actualResult = VT_R4_PatientFieldLockService.verifyLockedFields(testPatientIds);
        Test.stopTest();

        System.assertEquals(new List<Id>(), actualResult);
    }

    public static void testVerifyLockedFieldsPatientSetWithUser() {
        Test.startTest();
        List<Id> testPatientIds = generateRegPatientIds(2);
        stubQueries(testPatientIds);
        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();
        User user = VT_D1_TestUtils.createUserByProfile('Patient Guide', 'PrimaryPGUser');


        insert user;
        List<Id> actualResult = VT_R4_PatientFieldLockService.verifyLockedFields(testPatientIds, user.Id);
        Test.stopTest();

        System.assertEquals(testPatientIds, actualResult);
    }

    public static void testVerifyLockedFieldsSoloPatient() {
        Test.startTest();
        Id testPatientId = generateRegPatientId();
        stubQueries(new List<Id>{
                testPatientId
        });
        List<Id> actualResult = VT_R4_PatientFieldLockService.verifyLockedFields(testPatientId);
        Test.stopTest();

        System.assertEquals(new List<Id>(), actualResult);
    }

    public static void testVerifyLockedFieldsSoloPatientWithUser() {
        Test.startTest();
        Id testPatientId = generateRegPatientId();
        Id userId = UserInfo.getUserId();
        stubQueries(new List<Id>{
                testPatientId
        });
        List<Id> actualResult = VT_R4_PatientFieldLockService.verifyLockedFields(testPatientId, userId);
        Test.stopTest();

        System.assertEquals(new List<Id>(), actualResult);
    }

    public static void testVerifyLockedFieldsEmptyValues() {
        List<Id> testPatientIds = new List<Id>();
        Id userId = null;

        try {
            Test.startTest();
            VT_R4_PatientFieldLockService.verifyLockedFields(testPatientIds, userId);
            Test.stopTest();
        } catch (Exception e) {
            System.assert(e.getMessage().contains('Provide at least one Patient ID'));
        }
    }

    public static void testUpdateRestrictedFieldByPgSuccess() {
        Test.startTest();
        final Case caseRecord = getCase();

        final Contact patientContact = (Contact) new DomainObjects.Contact_t()
                .setFirstName('Patient')
                .setLastName('Patient')
                .setVTD1_Clinical_Study_Membership(caseRecord.Id)
                .persist();

        final User patientGuideUser;
        System.runAs(new User(Id = UserInfo.getUserId())) {
            patientGuideUser = (User) new DomainObjects.User_t()
            .setProfile('Patient Guide')
            .persist();
        }

        final ContactShare contactShare = new ContactShare();
        contactShare.UserOrGroupId = patientGuideUser.Id;
        contactShare.ContactId = patientContact.Id;
        contactShare.ContactAccessLevel = 'Edit';
        insert contactShare;

        final CaseShare caseShare = new CaseShare();
        caseShare.UserOrGroupId = patientGuideUser.Id;
        caseShare.CaseId = caseRecord.Id;
        caseShare.CaseAccessLevel = 'Edit';
        insert caseShare;

        final String newGender = 'Male';
        patientContact.HealthCloudGA__Gender__c = newGender;

        String exceptionMessage;

        try {
            System.runAs(patientGuideUser) {
                patientContact.HealthCloudGA__Gender__c = 'Male';
                update patientContact;
            }
        } catch (Exception e) {
            exceptionMessage = e.getMessage();
            System.debug(e);
        }
        Test.stopTest();

        final Contact updatedContact = [SELECT HealthCloudGA__Gender__c FROM Contact WHERE Id = :patientContact.Id];
        System.assertEquals(null, exceptionMessage);
        System.assertEquals(newGender, updatedContact.HealthCloudGA__Gender__c);
    }

    public static void testUpdateRestrictedFieldByPgFail() {
        Test.startTest();
        final Case caseRecord = getCase();
        final Contact patientContact = getPatientContact();

        final User patientGuideUser;
        System.runAs(new User(Id = UserInfo.getUserId())) {
            patientGuideUser = (User) new DomainObjects.User_t()
            .setProfile('Patient Guide')
            .persist();
        }

        final ContactShare contactShare = new ContactShare();
        contactShare.UserOrGroupId = patientGuideUser.Id;
        contactShare.ContactId = patientContact.Id;
        contactShare.ContactAccessLevel = 'Edit';
        insert contactShare;

        final CaseShare caseShare = new CaseShare();
        caseShare.UserOrGroupId = patientGuideUser.Id;
        caseShare.CaseId = caseRecord.Id;
        caseShare.CaseAccessLevel = 'Edit';
        insert caseShare;


        caseRecord.Status = 'Consent';
        caseRecord.ContactId = patientContact.Id;
        update caseRecord;

        patientContact.LastName = 'Last';
        patientContact.HealthCloudGA__Gender__c = 'Male';

        String exceptionMessage;
        String expectedMessage = String.format(Label.VTR4_LockedForEditingVariant2, new List<String>{Contact.LastName.getDescribe().getLabel(), Contact.HealthCloudGA__Gender__c.getDescribe().getLabel()});

        try {
            System.runAs(patientGuideUser) {
                patientContact.HealthCloudGA__Gender__c = 'Male';
                update patientContact;
            }
        } catch (Exception e) {
            exceptionMessage = e.getMessage();
            System.debug(e);
        }


        final Contact updatedContact = [SELECT HealthCloudGA__Gender__c FROM Contact WHERE Id = :patientContact.Id];
        Test.stopTest();
        System.assert(exceptionMessage.contains(expectedMessage));
        System.assertEquals(null, updatedContact.HealthCloudGA__Gender__c);
    }

    public static void testUpdateRestrictedFieldByAdminSuccess() {
        final Contact patientContact = getPatientContact();

        final String newGender = 'Male';
        patientContact.HealthCloudGA__Gender__c = newGender;

        final String exceptionMessage;
        Test.startTest();
        try {
            patientContact.HealthCloudGA__Gender__c = 'Male';
            update patientContact;
        } catch (Exception e) {
            exceptionMessage = e.getMessage();
            System.debug(e);
        }
        Test.stopTest();

        final Contact updatedContact = [SELECT HealthCloudGA__Gender__c FROM Contact WHERE Id = :patientContact.Id];
        System.assertEquals(null, exceptionMessage);
        System.assertEquals(newGender, updatedContact.HealthCloudGA__Gender__c);
    }

    public static void testUpdateHistoricalDataSuccess() {
        Case caseRecord = [SELECT Id FROM Case WHERE RecordType.Name = 'CarePlan' LIMIT 1];
        caseRecord.Status = 'Consent';
        update caseRecord;

        VT_R4_PatientFieldLockService service = new VT_R4_PatientFieldLockService();
        Test.startTest();
        service.updateHistoricalData();
        Test.stopTest();

        Contact updatedContact = [SELECT VTR4_IsLockedPatient__c FROM Contact WHERE RecordType.Name = 'Patient' LIMIT 1];
        System.assertEquals(true, updatedContact.VTR4_IsLockedPatient__c);
    }

    public static void UserChangeEmailConfirmationTriggerHandlerWithOutCaregiversTest() {
        User patient = [SELECT Id FROM User WHERE Profile.Name = 'Patient' AND Email LIKE '%@example.com' ORDER BY CreatedDate DESC LIMIT 1];
        patient.Email = 'newemail@dot.com';

        System.runAs(new User(Id = UserInfo.getUserId())) {
            Test.startTest();
            update patient;
            Test.stopTest();
        }
    }

    public static void UserChangeEmailConfirmationTriggerHandlerCaregiverTest() {
        Test.startTest();
        Contact patient = getPatientContact();
        User u = [SELECT Id, Email FROM User WHERE ContactId = :patient.Id];
        u.Email = 'testfac@google.com';
        System.runAs(new User(Id = UserInfo.getUserId())) {
            update u;
        }
        Test.stopTest();
    }

    private static void stubQueries(List<Id> testPatientIds) {
        new QueryBuilder()
                .buildStub()
                .addStubToIdSet(new Set<Id>(testPatientIds))
                .namedQueryStub('VT_R4_PatientFieldLockService.getLockedPatients')
                .applyStub();
    }

    private static Id generateRegPatientId() {
        return fflib_IDGenerator.generate(Contact.getSObjectType());
    }

    private static List<Id> generateRegPatientIds(Integer amount) {
        List<Id> ids = new List<Id>();
        for (Integer i = 0; i < amount; i++) {
            ids.add(generateRegPatientId());
        }
        return ids;
    }

    //-----------------SOQL-------------------//
    private static Case getCase() {
        return [SELECT Id FROM Case WHERE RecordType.Name = 'CarePlan' AND Subject != 'VT_R5_allTest' LIMIT 1];
    }

    private static Contact getPatientContact() {
        return [
                SELECT Id, VTD1_Clinical_Study_Membership__c, AccountId, RecordType.Name
                FROM Contact
                WHERE AccountId != NULL AND RecordType.Name = 'Patient'
        ];
    }
}