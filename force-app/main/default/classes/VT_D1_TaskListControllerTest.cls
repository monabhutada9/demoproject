/**
 * @author Ruslan Mullayanov
 * @created 11.06.2020
 * @see VT_D1_TaskListController
 */
@IsTest
private class VT_D1_TaskListControllerTest {
    @TestSetup
    static void setup(){
        Test.startTest();

        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t().setRecordTypeByName('Sponsor');
        Account account = (Account) patientAccount.persist();

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
                .setFirstName('Patient')
                .setLastName('Patient')
                .setAccountId(account.Id);

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        patientUser.persist();

        DomainObjects.User_t caregiverUser = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t()
                        .setFirstName('Caregiver')
                        .setLastName('VT_D1_TaskListControllerTest')
                        .setEmail(DomainObjects.RANDOM.getEmail())
                        .setRecordTypeName('Caregiver')
                        .setAccountId(account.Id)
                )
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME);
        caregiverUser.persist();

        DomainObjects.Case_t patientCase = new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .addVTD1_Patient_User(patientUser)
                .addContact(patientContact)
                .setAccountId(account.Id);
        patientCase.persist();

        List<String> taskCategories = new List<String>{'Follow Up Required','Signature Required'};
        insert new VTD2_TaskCategoriesByProfile__c(VTD2_Categories__c = String.join(taskCategories, ';'));

        VTD1_Protocol_Deviation__c pd = (VTD1_Protocol_Deviation__c) new DomainObjects.VTD1_Protocol_Deviation_t().persist();

        DomainObjects.Task_t task = new DomainObjects.Task_t()
                .addUser(caregiverUser)
                .setWhatId(pd.Id)
                .setStatus('Open')
                .setSubject('Test')
                .setCategory(taskCategories[0]);
        task.persist();

        Test.stopTest();
    }

    @IsTest
    static void taskListTest() {
        User caregiver = [SELECT Id FROM User WHERE Contact.LastName = 'VT_D1_TaskListControllerTest' LIMIT 1];
        System.assertNotEquals(null, caregiver);

        Task tsk = [SELECT Id, Status, Category__c FROM Task WHERE Subject = 'Test' LIMIT 1];
        System.assertNotEquals(null, tsk);

        Test.startTest();
        System.runAs(caregiver){
            VT_D1_TaskListController.TaskInfo taskInfo = VT_D1_TaskListController.getTasksInfo(tsk.Status, tsk.Category__c);
            System.assertNotEquals(null, taskInfo);
            System.assertEquals(false, taskInfo.tasks.isEmpty());
            System.assertEquals(false, String.isEmpty(taskInfo.userProfile));
            System.assertEquals(false, String.isEmpty(taskInfo.userId));

            VT_D1_TaskListController.closeTask(tsk.Id);
            System.assertEquals('Completed', [SELECT Status FROM Task WHERE Id = :tsk.Id].Status);

            List<String> categoryList = VT_D1_TaskListController.getCategories();
            System.assertEquals(false, categoryList.isEmpty());

            VT_D1_TaskListController.getNetworkId();
        }
        Test.stopTest();
    }

    @IsTest
    static void getTasksFromPoolTest() {
        User caregiver = [SELECT Id FROM User WHERE Contact.LastName = 'VT_D1_TaskListControllerTest' LIMIT 1];
        String category = 'Follow Up Required';
        String status = 'Open';
        String jsonParams = '{"offset":"0", "limit":"100"}';
        Test.startTest();
        System.runAs(caregiver) {
            String tasksFromPool = VT_D1_TaskListController.getTasksFromPool(category, status, jsonParams);
            Map<String, Object> wrapper = (Map<String, Object>) JSON.deserializeUntyped(tasksFromPool);
            Map<String, Object> result = (Map<String, Object>) wrapper.get('result');
            List<Object> tasks = (List<Object>) result.get('records');
            if (tasks.size() > 0) {
                Map<String, Object> task = (Map<String, Object>) tasks[0];
                System.assertEquals(caregiver.Id, task.get('OwnerId'), 'Task ownerID should be equal to current user ID.');
            }
        }
        Test.stopTest();
    }
}