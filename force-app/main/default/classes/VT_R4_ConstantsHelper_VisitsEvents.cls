/**
 * Created by user on 2019.12.24.
 */

public with sharing class VT_R4_ConstantsHelper_VisitsEvents {
    public static final String SOBJECT_ACTUAL_VISIT = 'VTD1_Actual_Visit__c';
    public static final String SOBJECT_MONITORING_VISIT = 'VTD1_Monitoring_Visit__c';

    public static final String ACTUAL_VISIT_SUBTYPE_PSC = 'PSC';

    public static final String MONITORING_VISIT_TYPE_CLOSEOUT = 'Closeout Visit';
    public static final String MONITORING_VISIT_TYPE_SELECTION = 'Selection Visit';
    public static final String MONITORING_VISIT_TYPE_INITIATION = 'Initiation Visit';

    public static final String PROTOCOL_VISIT_TYPE_LABS = 'Labs';
    public static final String PROTOCOL_VISIT_TYPE_STUDY_TEAM = 'Study Team';
    public static final String PROTOCOL_VISIT_TYPE_HCP = 'Health Care Professional (HCP)';

    public static final String ACTUAL_VISIT_STATUS_SCHEDULED = 'Scheduled';
    public static final String ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED = 'To Be Scheduled';
    public static final String ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED = 'To Be Rescheduled';
    public static final String ACTUAL_VISIT_STATUS_REQUESTED = 'Requested';
    public static final String ACTUAL_VISIT_STATUS_COMPLETED = 'Completed';
    public static final String ACTUAL_VISIT_STATUS_CANCELLED = 'Cancelled';
    public static final String ACTUAL_VISIT_STATUS_MISSED = 'Missed';
    public static final String ACTUAL_VISIT_STATUS_FUTURE_VISIT = 'Future Visit';

    public static final List<String> ACTUAL_VISIT_PATIENT_STATUSES = new List<String>{
            ACTUAL_VISIT_STATUS_SCHEDULED,
            ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED,
            ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED,
            ACTUAL_VISIT_STATUS_FUTURE_VISIT,
            ACTUAL_VISIT_STATUS_REQUESTED
    };

    public static final String VISIT_REQUEST_TASK_SUBJECT = 'Manually Schedule Visit';

    public static final String RT_ACTUAL_VISIT_LABS = 'Labs'; //VT_R4_ConstantsHelper_VisitsEvents
    public static final String RT_ACTUAL_VISIT_STUDY_TEAM = 'Study_Team'; //VT_R4_ConstantsHelper_VisitsEvents
    public static final String RT_ACTUAL_VISIT_HCP = 'Health_Care_Professional_HCP'; //VT_R4_ConstantsHelper_VisitsEvents

    public static final String MONITORING_VISIT_COMPLETE = 'Visit Completed'; //VT_R4_ConstantsHelper_VisitsEvents

    public static final String RECORD_TYPE_ID_VISIT_MEMBER_REGULAR = VT_D1_HelperClass.getRTId('Visit_Member__c', 'VTD1_RegularMember');  //VT_R4_ConstantsHelper_VisitsEvents

    public static final String RECORD_TYPE_ID_EVENT_OUTOFOFFICE = VT_D1_HelperClass.getRTId('Event', 'OutOfOffice');

    public static final String RECORD_TYPE_ID_PROTOCOL_VISIT_PROTOCOL = VT_D1_HelperClass.getRTId('VTD1_ProtocolVisit__c', 'Protocol'); //VT_R4_ConstantsHelper_VisitsEvents
    public static final String RECORD_TYPE_ID_PROTOCOL_VISIT_ONBOARDING = VT_D1_HelperClass.getRTId('VTD1_ProtocolVisit__c', 'VTD1_Onboarding');//VT_R4_ConstantsHelper_VisitsEvents
    public static final String RECORD_TYPE_ID_PROTOCOL_VISIT_SUBGROUP_TIMELINE_VISIT = VT_D1_HelperClass.getRTId('VTD1_ProtocolVisit__c', 'VTR5_Subgrouptimelinevisit');//VT_R4_ConstantsHelper_VisitsEvents
    public static final String RECORD_TYPE_ID_ACTUAL_VISIT_LABS = VT_D1_HelperClass.getRTId('VTD1_Actual_Visit__c', 'Labs');//VT_R4_ConstantsHelper_VisitsEvents
    public static final String RECORD_TYPE_ID_ACTUAL_VISIT_UNSCHEDULED = VT_D1_HelperClass.getRTId('VTD1_Actual_Visit__c', 'VTD1_Unscheduled');//VT_R4_ConstantsHelper_VisitsEvents

    public static final String TIMESLOT_PROPOSED = 'Proposed';
    public static final String TIMESLOT_CONFIRMED = 'Confirmed';
    public static final String TIMESLOT_REJECTED = 'Rejected';
}