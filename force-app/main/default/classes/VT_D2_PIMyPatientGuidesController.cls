/**
* @author Ruslan Mullayanov
* @description Primary and Additional Patient Guides for current PI
*/

public with sharing class VT_D2_PIMyPatientGuidesController {

    @AuraEnabled(Cacheable=true)
    public static String getMyPatientGuides(Id studyId) {
        try {
            if (studyId == null) throw new NullPointerException();

            List<Id> myStudyIds = VT_D1_PIStudySwitcherRemote.getMyStudyIds(studyId);
            List<Study_Team_Member__c> primaryGuides = getSTMs(myStudyIds);
            List<Study_Site_Team_Member__c> additionalGuides = getSSTMs(myStudyIds);
            Set<Id> userIds = getUserIds(primaryGuides,additionalGuides);

            return getRoleToUsers(userIds,primaryGuides,additionalGuides);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    //------------------------- HELPERS ---------------------------//

    private static  Set<Id> getUserIds(List<Study_Team_Member__c> primaryGuides,List<Study_Site_Team_Member__c> additionalGuides){
        Set<Id> userIds = new Set<Id>();

        Set<Id> userIdsFromSTMs = getUserIdsFromSTMs(primaryGuides);
        if (!userIdsFromSTMs.isEmpty()) {
            userIds.addAll(userIdsFromSTMs);
        }

        Set<Id> userIdsFromSSTMs = getUserIdsFromSSTMs(additionalGuides);
        if (!additionalGuides.isEmpty()) {
            userIds.addAll(userIdsFromSSTMs);
        }
        return userIds;
    }
    private static String getRoleToUsers( Set<Id> userIds,List<Study_Team_Member__c> primaryGuides, List<Study_Site_Team_Member__c> additionalGuides ){
        Map<Id,User> userMap = getUserMap(userIds);
        Map<String,List<User>> roleToUsers = new Map<String,List<User>>();
        if (!primaryGuides.isEmpty()) {
            setRoleToPGUsers(primaryGuides,roleToUsers,userMap);
        }
        if (!additionalGuides.isEmpty()) {
            setRoleToAdditionalGuideUsers(additionalGuides,roleToUsers,userMap);
        }
        return JSON.serialize(roleToUsers);
    }
    private static void setRoleToAdditionalGuideUsers(List<Study_Site_Team_Member__c> additionalGuides,Map<String,List<User>> roleToUsers, Map<Id,User> userMap){
        List<User> AGUsers = new List<User>();
        for (Study_Site_Team_Member__c sstm : additionalGuides) {
            if (userMap.containsKey(sstm.VTD1_Associated_PG__r.User__c)) {
                AGUsers.add(userMap.get(sstm.VTD1_Associated_PG__r.User__c));
            }
        }
        roleToUsers.put('additionalGuides', AGUsers);
    }
    private static void setRoleToPGUsers(List<Study_Team_Member__c> primaryGuides,Map<String,List<User>> roleToUsers, Map<Id,User> userMap){
        List<User> PGUsers = new List<User>();
        for (Study_Team_Member__c stm : primaryGuides) {
            if (userMap.containsKey(stm.VTD1_PIsAssignedPG__c)) {
                PGUsers.add(userMap.get(stm.VTD1_PIsAssignedPG__c));
            }
        }
        roleToUsers.put('primaryGuides', PGUsers);
    }
    private static Set<Id> getUserIdsFromSSTMs(List<Study_Site_Team_Member__c> additionalGuides){
        Set<Id> userIds = new Set<Id>();
        if (!additionalGuides.isEmpty()) {
            for (Study_Site_Team_Member__c sstm : additionalGuides) {
                userIds.add(sstm.VTD1_Associated_PG__r.User__c);
            }
        }
        return userIds;
    }
    private static Set<Id> getUserIdsFromSTMs(List<Study_Team_Member__c> primaryGuides){
        Set<Id> userIds = new Set<Id>();
        if (!primaryGuides.isEmpty()) {
            for (Study_Team_Member__c stm : primaryGuides) {
                userIds.add(stm.VTD1_PIsAssignedPG__c);
            }
        }
        return userIds;
    }

    //--------------------------- SOQL ---------------------------//

    private static Map<Id,User> getUserMap(Set<Id> userIds){
         return new Map<Id,User>([
                 SELECT Id
                         , Name
                         , Email
                         , Phone
                         , FullPhotoUrl
                 FROM User
                 WHERE Id IN :userIds
         ]);
    }
    private static List<Study_Site_Team_Member__c> getSSTMs(List<Id> myStudyIds){
        return [
                SELECT Id
                        , VTD1_Associated_PG__r.User__c
                FROM Study_Site_Team_Member__c
                WHERE VTD1_Associated_PI__r.User__c =: UserInfo.getUserId()
                        AND VTD1_Associated_PG__r.Study__c IN :myStudyIds
                ORDER BY VTD1_Associated_PG__r.User__r.Name ASC
        ];
    }
    private static List<Study_Team_Member__c> getSTMs(List<Id> myStudyIds){
        return [
                SELECT Id
                        , Study__c
                        , VTD1_PIsAssignedPG__c
                FROM Study_Team_Member__c
                WHERE VTD1_Type__c =:VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME
                AND User__c =: UserInfo.getUserId()
                AND Study__c IN :myStudyIds
        ];
    }

    //------------------------- WRAPPERS --------------------------//

    public class Study {
        public String id;
        public String name;
    }

}