@isTest
public with sharing class VT_R3_RestPatientPhoneSMSTest {

    public static void updatePhoneOptTest() {
        setUpMCSettings();
        User user = [SELECT Id,ContactId FROM User WHERE Profile.Name = 'Patient' AND IsActive = true AND Name = 'CP1 CP1L' LIMIT 1];
        VT_D1_Phone__c phone = new VT_D1_Phone__c();
        phone.VTD1_Contact__c = user.ContactId;
        phone.VTR2_SMS_Opt_In_Status__c = 'Active';
        phone.PhoneNumber__c = '375333366666';
        phone.Account__c = [SELECT Id FROM Account LIMIT 1].Id;
        insert phone;

        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/Patient/Profile/Phones/SMS';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('00E2a000000SlsSEAS');
        request.params.put('action', 'optIn');
        RestContext.request = request;
        RestContext.response = response;

        String responseString = VT_R3_RestPatientPhoneSMS.updatePhoneOpt();

        request.requestBody = Blob.valueof(phone.Id);
        responseString = VT_R3_RestPatientPhoneSMS.updatePhoneOpt();
        System.assertEquals('Phone should be in optIn-status "Inactive" or with null status', responseString);
        System.assertEquals(417, response.statusCode);

        phone.VTR2_SMS_Opt_In_Status__c = 'Inactive';
        update phone;
        responseString = VT_R3_RestPatientPhoneSMS.updatePhoneOpt();
        Map<String, Object>responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
        System.assertEquals('Pending', responseMap.get('optInStatus'));
    }
    
    public static void updatePhoneOptTest2() {
        setUpMCSettings();
        User user = [SELECT Id,ContactId FROM User WHERE Profile.Name = 'Patient' AND IsActive = true AND Name = 'CP1 CP1L' LIMIT 1];
        VT_D1_Phone__c phone = new VT_D1_Phone__c();
        phone.VTD1_Contact__c = user.ContactId;
        phone.VTR2_SMS_Opt_In_Status__c = 'Inactive';
        phone.PhoneNumber__c = '375333366666';
        phone.Account__c = [SELECT Id FROM Account LIMIT 1].Id;
        insert phone;

        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/Patient/Profile/Phones/SMS';
        request.httpMethod = 'POST';
        request.params.put('action', 'optOut');
        request.requestBody = Blob.valueof(phone.Id);

        RestContext.request = request;
        RestContext.response = response;


        String responseString = VT_R3_RestPatientPhoneSMS.updatePhoneOpt();
        System.assertEquals('Phone should be in optIn-status "Active"', responseString);

        phone.VTR2_SMS_Opt_In_Status__c = 'Active';
        update phone;
        responseString = VT_R3_RestPatientPhoneSMS.updatePhoneOpt();
        Map<String, Object>responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
        System.assert(!(Boolean) responseMap.get('optInFlag'));

        RestContext.request.params.put('action', 'resendOptIn');
        responseString = VT_R3_RestPatientPhoneSMS.updatePhoneOpt();
//		responseMap = (Map<String, Object>)JSON.deserializeUntyped(responseString);
        System.assertEquals('Phone should be in optIn-status "Pending"', responseString);

        phone.VTR2_SMS_Opt_In_Status__c = 'Pending';
        update phone;
        responseString = VT_R3_RestPatientPhoneSMS.updatePhoneOpt();
        System.assert(!String.isEmpty(responseString));

        RestContext.request.params.put('action', 'sendContacts');
        responseString = VT_R3_RestPatientPhoneSMS.updatePhoneOpt();
        System.assertEquals('Phone should be in optIn-status "Active"', responseString);

//		RestContext.request.params.put('action', 'reset');
//		responseString = VT_R3_RestPatientPhoneSMS.updatePhoneOpt();
//		System.assert(!String.isEmpty(responseString));
//
//		RestContext.request.params.put('action', 'resetreset');
//		responseString = VT_R3_RestPatientPhoneSMS.updatePhoneOpt();
//		System.assert(!String.isEmpty(responseString));

//		RestContext.request.params.put('action1', '');
//		responseString = VT_R3_RestPatientPhoneSMS.updatePhoneOpt();
//		System.assert(!String.isEmpty(responseString));


    }
    static void setUpMCSettings() {
        List<VTR2_McloudSettings__mdt> mcloudSettings = new List<VTR2_McloudSettings__mdt>();
        mcloudSettings.add(new VTR2_McloudSettings__mdt(VTR2_OutboundMessageKey_API_Triggered__c = 'NjU6Nzg6MA',
                VTR2_RestURI__c = 'https://mc2gwv28hdsr86gd-sldnjzvzzgm.rest.marketingcloudapis.com/',
                VTR2_ShortCode__c = '99808'));
        VT_R2_McloudBroadcaster.setMCsettings(mcloudSettings);
    }
}