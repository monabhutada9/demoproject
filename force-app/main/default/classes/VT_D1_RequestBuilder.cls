/**
 * Created by Leonid Bartenev
 */

public interface VT_D1_RequestBuilder {
    
    //this method must generate request body
    String buildRequestBody();

}