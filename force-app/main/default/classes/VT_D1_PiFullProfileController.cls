public without sharing class VT_D1_PiFullProfileController {
    public class InputSelectRatioElement{
        public String key;
        public String label;
        public Boolean value;

        public Boolean equals(Object o) {
            if (this.label == ((InputSelectRatioElement)o).label) {
                return true;
            }
            return false;
        }
        public override Integer hashCode() {
            return label.hashCode();
        }
    }

    public class PicklistOption {

        @AuraEnabled public String label { get; set; }
        @AuraEnabled public String value { get; set; }
        @AuraEnabled public Boolean selected { get; set; }

    }

    public class NewPhone{
        public String phoneNumber;
        public String phoneType;
        public Boolean primary;
    }

    public class ContactPhone {
        public VT_D1_Phone__c phone;
        public List<InputSelectRatioElement> phoneTypeInputSelectRatioElementList;
        public Boolean deletePhone = false;
    }

  	public class PiFullProfile{
        public User user;
    	public Contact contact;
        public Boolean smsVisitRemindersAvailable = false;
        public Boolean smsNewMessagesAvailable = false;
        //public List<InputSelectRatioElement> contactLanguageSelectRatioElementList;
        public List<InputSelectRatioElement> userTimeZoneSelectRatioElementList;
        public List<InputSelectRatioElement> userLocaleSelectRatioElementList;
        public List<PicklistOption> userTimeZoneOptions;
        public List<PicklistOption> userLocaleOptions;
        //public Address__c address;
        public Virtual_Site__c[] virtualSiteData;
        public Licenses__c[] licensesList;
        public List<ContactPhone> contactPhoneList;
        public List<NewPhone> newContactPhoneList;
        public List<InputSelectRatioElement> newPhoneTypeInputSelectRatioElementList;
    }

    @AuraEnabled(Cacheable = false)
  	public static String getFullProfile() {
    	try{
            PiFullProfile piFullProfile = new PiFullProfile();
            User u = [SELECT Id, Title, ContactId, TimeZoneSidKey, Email, Department, VTD2_Languages__c, LocaleSidKey
                      FROM User WHERE Id =: UserInfo.getUserId()];
            piFullProfile.user = u;

            if (u.ContactId == null) {
                return null;
            }

            //, HealthCloudGA__PrimaryLanguage__c
            Contact contact = [SELECT Id, FirstName, LastName, VTD1_Middle_Initial__c, VTD1_Suffix__c,
                                VTD1_Board_Certified__c, VTD1_Degree__c, VTD1_University__c, Primary_Speciality__c,
                                Phone, Account.Name, Account.BillingAddress, VTD2_PI_Office_Emails__c, VTD2_Send_notifications_as_emails__c,
                                VTR2_Receive_SMS_Visit_Reminders__c, VTR2_Receive_SMS_New_Message__c, VTR2_SMSAgreement__c
                                FROM Contact WHERE Id =: u.ContactId];

            piFullProfile.contact = contact;
            //Address__c address = [SELECT Id, Name, VTD1_Address2__c, City__c, State__c, ZipCode__c
            //                       FROM Address__c WHERE VTD1_Contact__c =: u.ContactId AND VTD1_Primary__c = TRUE];
            //Account account = [SELECT Name, BillingAddress FROM Account];
            //piFullProfile.address = address;
            System.debug('userId ' + u.Id);
            List<Id> virtualSiteIds = new List<Id>();
            Set<String> foundAddresses = new Set<String>();
            List<Study_Site_Team_Member__c> query1 = [SELECT VTR2_Associated_PI3__r.VTD1_VirtualSite__c FROM Study_Site_Team_Member__c WHERE VTR2_Associated_SubI__c =:u.Id];
            List<Study_Team_Member__c> query2 = [SELECT Study_Team_Member__r.VTD1_VirtualSite__c FROM Study_Team_Member__c WHERE VTD1_Backup_PI__c = true AND User__c =:u.Id];
            List<Study_Team_Member__c> query3 = [SELECT VTD1_VirtualSite__c FROM Study_Team_Member__c WHERE User__c =:u.Id AND VTD1_VirtualSite__c != NULL];
            for (Study_Site_Team_Member__c stm : query1) {
                virtualSiteIds.add(stm.VTR2_Associated_PI3__r.VTD1_VirtualSite__c);
            }
            for(Study_Team_Member__c stm : query2){
                virtualSiteIds.add(stm.Study_Team_Member__r.VTD1_VirtualSite__c);
            }
            for(Study_Team_Member__c stm : query3){
                virtualSiteIds.add(stm.VTD1_VirtualSite__c);
            }
            System.debug('allIds ' + virtualSiteIds);
            List<Virtual_Site__c> filteredVirtualSites = new List<Virtual_Site__c>();
            for (Virtual_Site__c virt_site : [
                    SELECT toLabel(VTR2_PrimarySiteAddress__r.VTR3_Country__c)
                            , VTR2_PrimarySiteAddress__r.VTR3_AddressLine_2__c
                            , VTR2_PrimarySiteAddress__r.VTR3_AddressLine_3__c
                            , VTR2_PrimarySiteAddress__r.VTR2_AddressLine1__c
                            , VTR2_PrimarySiteAddress__r.VTR3_Department__c
                            , VTR2_PrimarySiteAddress__r.VTR2_ZIP__c
                            , toLabel(VTR2_PrimarySiteAddress__r.VTR2_State__c)
                            , VTR2_PrimarySiteAddress__r.VTR2_City__c
                            , VTD1_Study_Site_Number__c
                    FROM Virtual_Site__c
                    WHERE Id IN:virtualSiteIds
                    WITH SECURITY_ENFORCED
            ]) {
                List<String> addressList = new List<String>{
                        virt_site.VTR2_PrimarySiteAddress__r.VTR2_AddressLine1__c,
                        virt_site.VTR2_PrimarySiteAddress__r.VTR3_AddressLine_2__c,
                        virt_site.VTR2_PrimarySiteAddress__r.VTR3_AddressLine_3__c,
                        virt_site.VTR2_PrimarySiteAddress__r.VTR3_Department__c,
                        virt_site.VTR2_PrimarySiteAddress__r.VTR2_City__c,
                        virt_site.VTR2_PrimarySiteAddress__r.VTR2_ZIP__c,
                        virt_site.VTR2_PrimarySiteAddress__r.VTR3_Country__c,
                        virt_site.VTR2_PrimarySiteAddress__r.VTR2_State__c,
                        virt_site.VTD1_Study_Site_Number__c
                };
                String foundAddress = String.join(addressList,'-');
                if (foundAddresses.contains(foundAddress)) {
                    continue;
                }
                foundAddresses.add(foundAddress);
                filteredVirtualSites.add(virt_site);
            }
            piFullProfile.virtualSiteData = filteredVirtualSites;
            System.debug('piFullProfile.virtualSiteData ' + piFullProfile.virtualSiteData);
            System.debug('foundAddresses ' + foundAddresses);

            Licenses__c[] licensesList = [
                    SELECT
                            Id,
                            Name,
                            toLabel(VTD1_Country__c),
                            toLabel(State__c)
                    FROM Licenses__c
                    WHERE Contact__c =: u.ContactId
//                    WITH SECURITY_ENFORCED
            ];// AND VTD1_Primary_Investigator_License__c = TRUE];
            piFullProfile.licensesList = licensesList;

            addPiInformation(piFullProfile, contact, u);
            addPIInfo(piFullProfile, u);
            addContactInformation(piFullProfile, contact, u);
            addSmsSettingAvailable(piFullProfile, contact);

            System.debug('piFullProfile' + piFullProfile);
            return JSON.serialize(piFullProfile, true);
	    } catch (Exception e){
	      	throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString() + ' ' + e.getTypeName());
	    }
	}
    public static void addPiInformation(PiFullProfile piFullProfile, Contact contact, User u){
        piFullProfile.userTimeZoneSelectRatioElementList = modifyTimeZoneLabels(getInputSelectRatioList(u, 'TimeZoneSidKey', u.TimeZoneSidKey, true));
        piFullProfile.userLocaleSelectRatioElementList = getInputSelectRatioList(u, 'LocaleSidKey', u.LocaleSidKey, true);
    }

    private static void addPIInfo(final PiFullProfile piFullProfile, final User us) {
        piFullProfile.userTimeZoneOptions = getPicklistOptions(us, 'TimeZoneSidKey', us.TimeZoneSidKey);
        piFullProfile.userLocaleOptions = getPicklistOptions(us, 'LocaleSidKey', us.LocaleSidKey);
    }

    private static List<PicklistOption> getPicklistOptions(final SObject sObj, final String fieldName, final String fieldValue) {
        final List<PicklistOption> picklistOptions = new List<PicklistOption>();
        final Map<String, String> optionsMap = VT_D1_CustomMultiPicklistController.getSelectOptionsWithLabel(sObj, fieldName);
        for (String option : optionsMap.keySet()) {
            final PicklistOption picklistOption = new PicklistOption();
            picklistOption.value = option;
            final String label = optionsMap.get(option);
            picklistOption.label = label.countMatches('(') > 1 ? label.substringBeforeLast(' ') : label;
            picklistOption.selected = fieldValue == option;
            picklistOptions.add(picklistOption);
        }
        return picklistOptions;
    }

    public static void addContactInformation(PiFullProfile piFullProfile, Contact contact, User u){
        List<ContactPhone> contactPhoneList = new List<ContactPhone>();
        List<VT_D1_Phone__c> contactPhonesList = [SELECT Id, PhoneNumber__c, Type__c, IsPrimaryForPhone__c,
                                                    VTR2_OptInFlag__c, VTR2_SMS_Opt_In_Status__c
                                                    FROM VT_D1_Phone__c WHERE VTD1_Contact__c =: u.ContactId];
        for(VT_D1_Phone__c phone : contactPhonesList){
            ContactPhone contactPhone = new ContactPhone();
            contactPhone.phone = phone;
            contactPhone.phoneTypeInputSelectRatioElementList = getInputSelectRatioList(phone, 'Type__c', phone.Type__c, true);
            contactPhoneList.add(contactPhone);
        }
        piFullProfile.contactPhoneList = contactPhoneList;

        piFullProfile.newPhoneTypeInputSelectRatioElementList = getInputSelectRatioListForNewElement(new VT_D1_Phone__c(), 'Type__c');
    }

    public static List<InputSelectRatioElement> getInputSelectRatioList(SObject sObj, String fieldName, String fieldValue, Boolean addNullKeyIfValueEmpty){
        Boolean inputSelectRatioElementEmpty = true;
        List<InputSelectRatioElement> inputSelectRatioList = new List<InputSelectRatioElement>();
        Map<String, String> optionsMap = VT_D1_CustomMultiPicklistController.getSelectOptionsWithLabel(sObj, fieldName);
        for (String option : optionsMap.keySet()) {
            InputSelectRatioElement inputSelectRatioElement = new InputSelectRatioElement();
            inputSelectRatioElement.key = option;
            inputSelectRatioElement.label = optionsMap.get(option);
            if (fieldValue == option){
                inputSelectRatioElement.value = true;
                inputSelectRatioElementEmpty = false;
            } else {
                inputSelectRatioElement.value = false;
            }
            inputSelectRatioList.add(inputSelectRatioElement);
        }
        if (addNullKeyIfValueEmpty && inputSelectRatioElementEmpty){
            InputSelectRatioElement inputSelectRatioElement = new InputSelectRatioElement();
            inputSelectRatioElement.key = null;
            inputSelectRatioElement.value = true;
            inputSelectRatioList.add(inputSelectRatioElement);
        }
        return inputSelectRatioList;
    }

    public static List<InputSelectRatioElement> getInputSelectRatioListForNewElement(SObject sObj, String fieldName){
        List<String> optionsList = VT_D1_CustomMultiPicklistController.getSelectOptions(sObj, fieldName);
        List<InputSelectRatioElement> newSelectRatioElementList = new List<InputSelectRatioElement>();
        for (String option : optionsList) {
            InputSelectRatioElement inputSelectRatioElement = new InputSelectRatioElement();
            inputSelectRatioElement.key = option;
            inputSelectRatioElement.value = false;
            newSelectRatioElementList.add(inputSelectRatioElement);
        }
//        InputSelectRatioElement inputSelectRatioElement = new InputSelectRatioElement();
//        inputSelectRatioElement.key = null;
//        inputSelectRatioElement.value = true;
//        newSelectRatioElementList.add(inputSelectRatioElement);

        return newSelectRatioElementList;
    }

    @AuraEnabled
    public static void updateProfile(String piFullProfileString) {
        try{
            PiFullProfile piFullProfile = (PiFullProfile) JSON.deserialize(piFullProfileString, PiFullProfile.class);
            User user = piFullProfile.user;
            update user;

            updateContact(piFullProfileString);

            List<VT_D1_Phone__c> phonesToUpdate = new List<VT_D1_Phone__c>();
            List<VT_D1_Phone__c> phonesToDelete = new List<VT_D1_Phone__c>();
            List<VT_D1_Phone__c> phonesToInsert = new List<VT_D1_Phone__c>();
            System.debug(LoggingLevel.INFO, ' piFullProfile' + JSON.serializePretty(piFullProfile));

            List<ContactPhone> contactPhoneList = piFullProfile.contactPhoneList;
            for (ContactPhone contactPhone : contactPhoneList) {
                if (contactPhone.deletePhone){
                    phonesToDelete.add(contactPhone.phone);
                } else if (String.isNotEmpty(contactPhone.phone.Id)) {
                    phonesToUpdate.add(contactPhone.phone);
                } else {
                    phonesToInsert.add(contactPhone.phone);
                }
            }
            try{
                delete phonesToDelete;
            } catch (Exception e) {
                AuraHandledException ex = new AuraHandledException(e.getMessage());
                ex.setMessage(e.getMessage());
                throw ex;
            }
            try {
                update phonesToUpdate;
            } catch (Exception e){
                String errorMsg = e.getMessage();
                if (errorMsg.contains(Label.VTR2_ValidationSameMobileNumber)) {
                    errorMsg = Label.VTR2_ValidationSameMobileNumber;
                }
                AuraHandledException ex = new AuraHandledException(errorMsg);
                ex.setMessage(errorMsg);
                throw ex;
            }
            try{
                insert phonesToInsert;
            } catch (Exception e) {
                String errorMsg = e.getMessage();
                if (errorMsg.contains(Label.VTR2_ValidationSameMobileNumber)) {
                    errorMsg = Label.VTR2_ValidationSameMobileNumber;
                }
                AuraHandledException ex = new AuraHandledException(errorMsg);
                ex.setMessage(errorMsg);
                throw ex;
            }
        } catch (Exception e){
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    public static void updateContact(String piFullProfileString) {
        try{
            PiFullProfile piFullProfile = (PiFullProfile) JSON.deserialize(piFullProfileString, PiFullProfile.class);
            Contact contact = piFullProfile.contact;
            update contact;
        } catch (Exception e){
            String errorMsg = Label.VTR2_RecordUpdatedFail;
            AuraHandledException ex = new AuraHandledException(errorMsg);
            ex.setMessage(errorMsg);
            throw ex;
        }
    }

    @AuraEnabled
    public static void optInRemote (String phoneId) {
        VT_R2_NotificationCSMSProcessor.optInRemote(phoneId);
    }

    @AuraEnabled
    public static void resendOptInRemote (String phoneId) {
        VT_R2_NotificationCSMSProcessor.resendOptInRemote(phoneId);
    }

    @AuraEnabled
    public static void optOutRemote (String phoneId) {
        VT_R2_NotificationCSMSProcessor.optOutRemote(phoneId);
    }

    @AuraEnabled
    public static void sendContactsRemote (String phoneId) {
        VT_R2_NotificationCSMSProcessor.sendContactsRemote(phoneId);
    }

    public static void addSmsSettingAvailable(PiFullProfile piFullProfile, Contact contact){
        Boolean stmSmsNewMessagesAvailable = false;
        Boolean stmSmsVisitRemindersAvailable = false;
        Boolean phoneSmsNewMessagesAvailable = false;
        Boolean phoneSmsVisitRemindersAvailable = false;
        List<Study_Team_Member__c> studyTeamMemberList = [SELECT Id, Study__r.VTR2_Send_New_Messages__c, Study__r.VDT2_Send_Visit_Reminders__c
        FROM Study_Team_Member__c
        WHERE VTD1_UserContact__c  = :contact.Id];
        System.debug('contact contr: '+ contact.Id);
        for(Study_Team_Member__c studyTeamMember : studyTeamMemberList ){
            if (studyTeamMember.Study__r.VTR2_Send_New_Messages__c) {
                stmSmsNewMessagesAvailable = true;
            }
            if (studyTeamMember.Study__r.VDT2_Send_Visit_Reminders__c) {
                stmSmsVisitRemindersAvailable = true;
            }
        }
        for (ContactPhone contactPhone : piFullProfile.contactPhoneList) {
            if (contactPhone.phone.VTR2_SMS_Opt_In_Status__c == 'Active') {
                phoneSmsNewMessagesAvailable = true;
                phoneSmsVisitRemindersAvailable = true;
                break;
            }
        }
        piFullProfile.smsNewMessagesAvailable = (stmSmsNewMessagesAvailable && phoneSmsNewMessagesAvailable);
        piFullProfile.smsVisitRemindersAvailable = (stmSmsVisitRemindersAvailable && phoneSmsVisitRemindersAvailable);

    }

    private static List<InputSelectRatioElement> modifyTimeZoneLabels(List<InputSelectRatioElement> languageList){
        String regex='^(?:[(]){1}[^(]*';
        Set<InputSelectRatioElement> newLanguageList = new Set<InputSelectRatioElement>();
        for(InputSelectRatioElement lang: languageList){
            Pattern pattern = Pattern.compile(regex);
            if(pattern != null && lang.label != null) {
                Matcher matcher = pattern.matcher(lang.label);
                Boolean result = matcher.find();
                if (result) {
                    lang.label = lang.label.trim().substring(matcher.start(),matcher.end());
                }
            }
            newLanguageList.add(lang);
        }
        return new List<InputSelectRatioElement>(newLanguageList);
    }
}