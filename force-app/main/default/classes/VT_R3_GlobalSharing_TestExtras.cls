/**
* @author: Carl Judge
* @date: 07-Oct-19
* @description: Due to the queue depth limit in tests, some methods in various sharing classes are never reached in normal operation. This class covers these extra methods
**/

// TODO Remove this

@IsTest
private class VT_R3_GlobalSharing_TestExtras {
    @TestSetup
    static void setup() {
        /*
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest(); */
    }

    @IsTest
    private static void doTest() {
        /*
        List<Id> studyIds = VT_D1_HelperClass.getIdsFromObjs([SELECT Id FROM HealthCloudGA__CarePlanTemplate__c]);
        List<Id> caseIds = VT_D1_HelperClass.getIdsFromObjs([SELECT Id FROM Case]);

        // study sharing
        VTR3_GlobalSharingConfig__mdt studyConfig = VT_R3_AbstractGlobalSharingLogic.getConfigForObjectType(HealthCloudGA__CarePlanTemplate__c.getSObjectType().getDescribe);
        VT_R3_GlobalSharingLogic_Study studyLogic = new VT_R3_GlobalSharingLogic_Study();
        studyLogic.getRecordIdsFromStudyIds(studyIds, studyConfig);
        studyLogic.getSharingDetails(new Set<Id>(studyIds), null, null, studyConfig);

        // account sharing
        VTR3_GlobalSharingConfig__mdt accountConfig = .getConfigForObjectType(Account.getSObjectType());
        VT_R3_GlobalSharingLogic_Account accountLogic = new VT_R3_GlobalSharingLogic_Account();
        accountLogic.getRecordIdsFromCarePlanIds(caseIds, accountConfig);


        // case sharing
        VTR3_GlobalSharingConfig__mdt caseConfig = .getConfigForObjectType(Case.getSObjectType());
        VT_R3_GlobalSharingLogic_Case caseLogic = new VT_R3_GlobalSharingLogic_Case();
        caseLogic.getRecordIdsFromStudyIds(studyIds, caseConfig);
        */
        
    }
}