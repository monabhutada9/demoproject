/**
 * Created by MPlatonov on 25.01.2019.
 * Modified by Harshita Khandelwal on 19.10.2020 for SH-17444 (Multiple CRA)
 */
public without sharing class VT_D2_TNCatalogTasks {
    public class ParamsHolder {
        @InvocableVariable(Label = 'TN Code' Required = false)
        public String tnCode;
        @InvocableVariable(Label = 'TN Codes' Required = false)
        public List <String> tnCodes;
        @InvocableVariable(Label = 'Record ID' Required = false)
        public String sourceId;
        @InvocableVariable(Label = 'Record IDs' Required = false)
        public List <String> sourceIds;
        @InvocableVariable(Label = 'Subject parameter ID 1' Required = false)
        public String subjectParamId_1;
        @InvocableVariable(Label = 'Subject parameter ID 2' Required = false)
        public String subjectParamId_2;
        @InvocableVariable(Label = 'Subject parameter ID 3' Required = false)
        public String subjectParamId_3;
        @InvocableVariable(Label = 'Subject parameter ID 4' Required = false)
        public String subjectParamId_4;
        @InvocableVariable(Label = 'Subject parameter ID 5' Required = false)
        public String subjectParamId_5;
        @InvocableVariable(Label = 'Assigned To ID' Required = false)
        public String assignedToID;
        @InvocableVariable(Label = 'Assigned To IDs' Required = false)
        public List <String> assignedToIDs;

        public ParamsHolder() {
        }

        public ParamsHolder(String sourceId, String tnCode) {
            this.sourceId = sourceId;
            this.tnCode = tnCode;
        }
    }

    public interface Listener {
        void tasksCreated(List <SObject> tasks);
    }

    public static List <Listener> listeners = new List<Listener>();

    private static final Set <String> PROFILES_AUTO_NOTIFY = new Set<String>{
            'Primary Investigator',
            'Site Coordinator',
            'Patient',
            'Caregiver'
    };

    private static final Map <String, Map <String, String>> tnFieldNamesMapping = new Map<String, Map <String, String>>{
            'Task' => new Map<String, String>{
                    'VTD2_T_Task_Unique_Code__c' => 'VTD2_Task_Unique_Code__c',
                    'VTD2_T_Assigned_To_ID__c' => 'OwnerId',
                    'VTD2_T_Related_To_Id__c' => 'WhatId',
                    'VTD2_T_Task_Record_Type_ID__c' => 'RecordTypeId',
                    'VTD2_T_Description__c' => 'Description',
                    'VTD2_T_Subject__c' => 'Subject',
                    'VTD2_T_Care_Plan_Template__c' => 'HealthCloudGA__CarePlanTemplate__c',
                    'VTD2_T_Due_Date__c' => 'ActivityDate',
                    'VTD2_T_Reminder_Date_Time__c' => 'ReminderDateTime',
                    'VTD2_T_Category__c' => 'Category__c',
                    'VTD2_T_Priority__c' => 'Priority',
                    'VTD2_T_Status__c' => 'Status',
                    'VTD2_T_Reminder_Set__c' => 'IsReminderSet',
                    'VTD2_T_Document__c' => 'Document__c',
                    'VTD2_T_Patient__c' => 'VTD1_Case_lookup__c',
                    'VTD2_T_Pre_Visit_Instructions__c' => 'VTD2_Pre_Visit_Instructions__c',
                    'VTD2_T_IsRequestVisitRedirect__c' => 'VTD2_isRequestVisitRedirect__c',
                    'VTD2_T_My_Task_List_Redirect__c' => 'VTD2_My_Task_List_Redirect__c',
                    'VTD2_T_Actual_Visit__c' => 'VTD1_Actual_Visit__c',
                    'VTD2_T_Description_Parameters__c' => 'VTD2_Description_Parameters__c',
                    'VTD2_T_Type_for_Backend_Logic__c' => 'VTD1_Type_for_backend_logic__c',
                    'VTR2_Name_ID__c' => 'WhoId',
                    'VTR2_T_Patient_Guide__c' => 'VTD1_PatientGuide1__c',
                    'VTD2_T_Type__c' => 'Type',
                    'VTD2_T_Subject_Parameter_Field__c' => null,
                    'VTD2_T_Subject_Parameter_Field_2__c' => null,
                    'VTD2_T_Subject_Parameter_Field_3__c' => null,
                    'VTD2_T_Subject_Parameter_Field_4__c' => null,
                    'VTD2_T_Subject_Parameter_Field_5__c' => null,
                    'VTR4_T_Compensation_Quality_Check__c' => 'VTD1_Compensation_Quality_Check__c',

                    'VTR5_T_MonitoringVisit__c' => 'VTR2_Monitoring_Visit_del__c',

                    /*
                    ** Mapping of new fields for email notification
                    ** - Eashan P (Strikers)
                    */
                    'VTR5_T_Email_Subject__c' => 'VTR5_Email_Subject__c',
                    'VTR5_T_Email_Subject_Parameters__c' => 'VTR5_Email_Subject_Parameters__c',
                    'VTR5_T_Email_Body_Parameters__c' => 'VTR5_Email_Body_Parameters__c',
                    'VTR5_T_Email_Body__c' => 'VTR5_Email_Body__c'
            },
            'SCTask' => new Map<String, String>{
                    'VTD2_T_Task_Unique_Code__c' => 'VTD2_Task_Unique_Code__c',
                    'VTD2_T_Assigned_To_ID__c' => 'OwnerId',
                    'VTD2_T_Task_Record_Type_ID__c' => 'RecordTypeId',
                    'VTD2_T_Description__c' => 'VTD1_Comments__c',
                    'VTD2_T_Subject__c' => 'VTD1_Subject__c',
                    'VTD2_T_Care_Plan_Template__c' => 'VTD1_CarePlanTemplate__c',
                    'VTD2_T_Due_Date__c' => 'VTD1_Due_Date__c',
                    'VTD2_T_Category__c' => 'VTD2_Category__c',
                    'VTD2_T_Document__c' => 'VTD1_Document__c',
                    'VTD2_T_Patient__c' => 'VTD1_Patient__c',
                    'VTD2_T_Type__c' => 'VTD1_Type__c',
                    'VTD2_T_Actual_Visit__c' => 'VTD1_Actual_Visit__c',
                    'VTR4_T_Patient_Delivery__c' => 'VTD1_PatientDelivery__c',
                    'VTR4_T_Patient_Kit__c' => 'VTD1_Patient_Kit__c',
                    'VTR4_T_Patient_Payment__c' => 'VTD1_Patient_Payment__c',
                    'VTR4_T_PI_ID__c' => 'VTD1_PI_ID__c',
                    'VTR4_T_Protocol_Deviation__c' => 'VTD1_Protocol_Deviation_ID__c',
                    'VTR4_T_SMS_Log_Name__c' => 'VTR2_SMS_Log_Name__c',
                    'VTR3_T_Name__c' => 'Name'
            },
            'TMATask' => new Map<String, String>{
                    'VTD2_T_Task_Unique_Code__c' => 'VTD2_Task_Unique_Code__c',
                    'VTD2_T_Assigned_To_ID__c' => 'OwnerId',
                    'VTR2_T_CSM__c' => 'VTD2_PCF__c',
                    'VTD2_T_Category__c' => 'VTD2_Category__c',
                    'VTD2_T_Document__c' => 'VTD2_Document__c',
                    'VTD2_T_Status__c' => 'VTD2_Status__c',
                    'VTD2_T_Care_Plan_Template__c' => 'VTD2_Study__c',
                    'VTD2_T_Subject__c' => 'VTD2_Subject__c',
                    'VTR4_T_Protocol_Deviation__c' => 'VTD2_Protocol_Deviation__c'
            },
            'PoolTask' => new Map<String, String>{
                    'VTD2_T_Assigned_To_ID__c' => 'OwnerId',
                    'VTD2_T_Care_Plan_Template__c' => 'VTR5_Study__c',
                    'VTD2_T_Category__c' => 'VTR5_Category__c',
                    'VTD2_T_Document__c' => 'VTR5_Document__c',
                    'VTR5_T_DocuSignStatus__c' => 'VTR5_DocuSignStatus__c',
                    'VTD2_T_Due_Date__c' => 'VTR5_DueDate__c',
                    'VTR5_T_MonitoringVisit__c' => 'VTR5_MonitoringVisit__c',
                    'VTR3_T_Name__c' => 'Name',
                    'VTD2_T_Subject__c' => 'VTR5_Subject__c',
                    'VTD2_T_Task_Record_Type_ID__c' => 'RecordTypeId',
                    'VTD2_T_Task_Unique_Code__c' => 'VTR5_TaskUniqueCode__c',
                    'VTD2_T_Type__c' => 'VTR5_Type__c',
                    'VTD2_T_Type_for_Backend_Logic__c' => 'VTR5_TypeForBackendLogic__c',
                    'VTR5_T_VirtualSite__c' => 'VTR5_VirtualSite__c'
            },
            // SH-17444 Multiple CRA (Mapping TN Catalog with CRA Task fields) - Start
            'CRATask' => new Map<String, String>{
                    'VTD2_T_Task_Unique_Code__c' => 'VTR5_Task_Unique_Code__c',
                    'VTD2_T_Assigned_To_ID__c' => 'OwnerId',
                    'VTD2_T_Task_Record_Type_ID__c' => 'RecordTypeId',
                    'VTD2_T_Description__c' => null,
                    'VTD2_T_Subject__c' => 'VTR5_Subject__c',

                    'VTD2_T_Care_Plan_Template__c' => 'VTR5_Study__c',

                    'VTD2_T_Due_Date__c' => 'VTR5_Due_Date__c',
                    'VTD2_T_Category__c' => 'VTR5_Category__c',
                    'VTD2_T_Document__c' => 'VTR5_Document__c',
                    'VTD2_T_Type__c' => 'VTR5_Type__c',
                    'VTD2_T_Actual_Visit__c' => null,
                    'VTR4_T_Patient_Delivery__c' => null,
                    'VTR4_T_Patient_Kit__c' => null,
                    'VTR4_T_Patient_Payment__c' => null,
                    'VTR4_T_PI_ID__c' => null,
                    'VTR4_T_Protocol_Deviation__c' => null,
                    'VTR4_T_SMS_Log_Name__c' => null,
                    'VTR3_T_Name__c' => 'Name',
                    'VTR5_T_VirtualSite__c' => 'VTR5_Virtual_Site__c'
            } // SH-17444 Multiple CRA - End
    };
//    private static final List <String> tnCatalogFieldNames = new List<String>();
    /*
    static {
        tnFieldNamesMapping.put('VTD2_T_Task_Unique_Code__c', 'VTD2_Task_Unique_Code__c');
        tnFieldNamesMapping.put('VTD2_T_Assigned_To_ID__c', 'OwnerId');
        tnFieldNamesMapping.put('VTD2_T_Related_To_Id__c', 'WhatId');
        tnFieldNamesMapping.put('VTD2_T_Task_Record_Type_ID__c', 'RecordTypeId');
        tnFieldNamesMapping.put('VTD2_T_Description__c', 'Description');
        tnFieldNamesMapping.put('VTD2_T_Subject__c', 'Subject');
        tnFieldNamesMapping.put('VTD2_T_Care_Plan_Template__c', 'HealthCloudGA__CarePlanTemplate__c');
        tnFieldNamesMapping.put('VTD2_T_Due_Date__c', 'ActivityDate');
        tnFieldNamesMapping.put('VTD2_T_Reminder_Date_Time__c', 'ReminderDateTime');
        tnFieldNamesMapping.put('VTD2_T_Category__c', 'Category__c');
        tnFieldNamesMapping.put('VTD2_T_Priority__c', 'Priority');
        tnFieldNamesMapping.put('VTD2_T_Status__c', 'Status');
        tnFieldNamesMapping.put('VTD2_T_Reminder_Set__c', 'IsReminderSet');
        tnFieldNamesMapping.put('VTD2_T_Document__c', 'Document__c');
        tnFieldNamesMapping.put('VTD2_T_Patient__c', 'VTD1_Case_lookup__c');
        tnFieldNamesMapping.put('VTD2_T_Pre_Visit_Instructions__c', 'VTD2_Pre_Visit_Instructions__c');
        tnFieldNamesMapping.put('VTD2_T_IsRequestVisitRedirect__c', 'VTD2_isRequestVisitRedirect__c');
        tnFieldNamesMapping.put('VTD2_T_My_Task_List_Redirect__c', 'VTD2_My_Task_List_Redirect__c');
        tnFieldNamesMapping.put('VTD2_T_Actual_Visit__c', 'VTD1_Actual_Visit__c');
        tnFieldNamesMapping.put('VTD2_T_Description_Parameters__c', 'VTD2_Description_Parameters__c');
        tnFieldNamesMapping.put('VTD2_T_Type_for_Backend_Logic__c', 'VTD1_Type_for_backend_logic__c');
        tnFieldNamesMapping.put('VTR2_Name_ID__c', 'WhoId');
        tnFieldNamesMapping.put('VTR2_T_Patient_Guide__c', 'VTD1_PatientGuide1__c');
        tnFieldNamesMapping.put('VTD2_T_Type__c', 'Type');
        //tnFieldNamesMapping.put('T_Case__c', 'VTD1_Case__c ');
        tnFieldNamesMapping.put('VTD2_T_Subject_Parameter_Field__c', null);
        tnFieldNamesMapping.put('VTD2_T_Subject_Parameter_Field_2__c', null);
        tnFieldNamesMapping.put('VTD2_T_Subject_Parameter_Field_3__c', null);
        tnFieldNamesMapping.put('VTD2_T_Subject_Parameter_Field_4__c', null);
        tnFieldNamesMapping.put('VTD2_T_Subject_Parameter_Field_5__c', null);
        for (String name : tnFieldNamesMapping.keySet()) {
            //tnCatalogFieldNames.add(name);
        }
    }
    */
    public class TNCatalogDescriptorElement {
        public final String fieldName;
        public final Object value;
    }

    public class TNCatalogDescriptor {
        public final String SObjectName;
        public final String recordTypeName;
        public final String tnNotificationCode;
        //public final List <TNCatalogDescriptorElement> elements = new List<TNCatalogDescriptorElement>();
        public final Map <String, Object> params = new Map<String, Object>();
        public final Boolean passIfEmptyRecipient;
        public String query;

        public TNCatalogDescriptor(String SObjectName, Boolean passIfEmptyRecipient, String recordTypeName, String tnNotificationCode) {
            this.SObjectName = SObjectName;
            this.passIfEmptyRecipient = passIfEmptyRecipient;
            this.recordTypeName = recordTypeName;
            this.tnNotificationCode = tnNotificationCode;
        }
    }

    @InvocableMethod
    public static List <Task> generateTask(List <ParamsHolder> params) {
        List <String> tnCatalogCodes = new List<String>();
        List <Id> sourceIds = new List<Id>();
//        List <String> subjectParamsIds = new List<String>();
//        List <String> sObjectNames = new List<String>();
        Map <String, String> subjectParams = new Map<String, String>();
        Map <String, String> assignedToMap = new Map<String, String>();
        for (ParamsHolder paramsHolder : params) {
            if (paramsHolder.tnCode != null && paramsHolder.assignedToIDs == null) {
                tnCatalogCodes.add(paramsHolder.tnCode);
            }
            if (paramsHolder.sourceId != null) {
                sourceIds.add(paramsHolder.sourceId);
            }
            if (paramsHolder.sourceIds != null) {
                for (String sourceId : paramsHolder.sourceIds) {
                    sourceIds.add(sourceId);
                    if (paramsHolder.tnCode != null) {
                        tnCatalogCodes.add(paramsHolder.tnCode);
                    }
                }
            }
            if (paramsHolder.tnCodes != null) {
                for (String tnCode : paramsHolder.tnCodes) {
                    tnCatalogCodes.add(tnCode);
                }
            }
            subjectParams.put('VTD2_T_Subject_Parameter_Field__c' + paramsHolder.sourceId, paramsHolder.subjectParamId_1);
            subjectParams.put('VTD2_T_Subject_Parameter_Field_2__c' + paramsHolder.sourceId, paramsHolder.subjectParamId_2);
            subjectParams.put('VTD2_T_Subject_Parameter_Field_3__c' + paramsHolder.sourceId, paramsHolder.subjectParamId_3);
            subjectParams.put('VTD2_T_Subject_Parameter_Field_4__c' + paramsHolder.sourceId, paramsHolder.subjectParamId_4);
            subjectParams.put('VTD2_T_Subject_Parameter_Field_5__c' + paramsHolder.sourceId, paramsHolder.subjectParamId_5);
            if (paramsHolder.assignedToID != null) {
                assignedToMap.put(paramsHolder.tnCode + (Id) paramsHolder.sourceId, (Id) paramsHolder.assignedToID);
            } else if (paramsHolder.assignedToIDs != null) {
                Integer index = 0;
                Map <String, List <Id>> assignedToIDs = new Map<String, List<Id>>();
                for (String assignedToID : paramsHolder.assignedToIDs) {
                    List <Id> receivers = assignedToIDs.get(paramsHolder.tnCode + (Id) paramsHolder.sourceIds[index]);
                    if (receivers == null) {
                        receivers = new List<Id>();
                        assignedToIDs.put(paramsHolder.tnCode + (Id) paramsHolder.sourceIds[index], receivers);
                    }
                    receivers.add((Id) assignedToID);
                    index++;
                }
                for (String key : assignedToIDs.keySet()) {
                    assignedToMap.put(key, String.join(assignedToIDs.get(key), ','));
                }
            }
        }
        return generateTasks(tnCatalogCodes, sourceIds, subjectParams, assignedToMap);
    }

    public static List<VTD1_SC_Task__c> generateSCTasks(List <String> tnCatalogCodes, List <Id> sourceIds, Map <String, String> subjectParams, Map <String, String> assignedToMap, Boolean autoInsert) {
        return generateTasks(tnCatalogCodes, sourceIds, subjectParams, assignedToMap, autoInsert);
    }

    public static void generateTasksAsync(List <String> tnCatalogCodes, List <Id> sourceIds
            , Map <String, String> subjectParams, Map <String, String> assignedToMap, Boolean autoInsert) {

        if (!System.isFuture() && !System.isBatch() && !System.isQueueable()) {

        String tnCatalogCodesJSON = JSON.serialize(tnCatalogCodes);
        String sourceIdsJSON = JSON.serialize(sourceIds);
        String subjectParamsJSON = subjectParams != null ? JSON.serialize(subjectParams) : null;
        String assignedToJSON = assignedToMap != null ? JSON.serialize(assignedToMap) : null;
        generateTasksFuture(tnCatalogCodesJSON, sourceIdsJSON, subjectParamsJSON, assignedToJSON, autoInsert);
    }

    }


    @Future
    public static void generateTasksFuture(String tnCatalogCodesJSON, String sourceIdsJSON
            , String subjectParamsJSON, String assignedToJSON, Boolean autoInsert) {
        List <String> tnCatalogCodes = (List <String>)JSON.deserialize(tnCatalogCodesJSON, List <String>.class);
        List <Id> sourceIds = (List <String>)JSON.deserialize(sourceIdsJSON, List <Id>.class);
        Map <String, String> subjectParams = subjectParamsJSON != null ? (Map <String, String>)JSON.deserialize(subjectParamsJSON, Map <String, String>.class) : null;
        Map <String, String> assignedToMap = assignedToJSON != null ? (Map <String, String>)JSON.deserialize(assignedToJSON, Map <String, String>.class) : null;
        List<SObject> tasks = generateTasks(tnCatalogCodes, sourceIds, subjectParams, assignedToMap, autoInsert);
        for (Listener listener : listeners) {
            listener.tasksCreated(tasks);
        }
    }

    public static List<Task> generateTasks(List <String> tnCatalogCodes, List <Id> sourceIds, Map <String, String> subjectParams, Map <String, String> assignedToMap) {
        return generateTasks(tnCatalogCodes, sourceIds, subjectParams, assignedToMap, true);
    }

    public static List<SObject> generateTasks(List <String> tnCatalogCodes, List <Id> sourceIds, Map <String, String> subjectParams, Map <String, String> assignedToMap, Boolean autoInsert) {
        Map <String, String> tnNotificationCodeMap = new Map<String, String>();
        Map <String, List <Id>> sourceIdsByCodeMap = new Map<String, List<Id>>();
        Map <String, TNCatalogDescriptor> codeToTnCatalogDescriptorMap = new Map<String, TNCatalogDescriptor>();
        String ownerFieldName = null;
        for (Integer i = 0; i < tnCatalogCodes.size(); i++) {
            String code = tnCatalogCodes[i];
            codeToTnCatalogDescriptorMap.put(code, null);
            List <Id> ids = sourceIdsByCodeMap.get(code);
            if (ids == null) {
                ids = new List<Id>();
                sourceIdsByCodeMap.put(code, ids);
            }
            ids.add(sourceIds[i]);
        }
        List <String> codes = new List<String>();
        for (String code : sourceIdsByCodeMap.keySet()) {
            codes.add('\'' + code + '\'');
        }
        List <SObject> catalogs = loadTNCatalogs(codes);
        if(catalogs != null && !catalogs.isEmpty()){
            for (SObject sobj : catalogs) {
                VTD2_TN_Catalog_Code__c tnCatalog = (VTD2_TN_Catalog_Code__c) sobj;
                String sObjectName = (String) tnCatalog.get('VTD2_T_SObject_Name__c');
                Boolean passIfEmptyRecipient = (Boolean) tnCatalog.get('VTR2_Pass_if_empty_recipient__c');
                String recordTypeName = (String) tnCatalog.getSObject('RecordType').get('Name');
                String tnNotificationCode = tnCatalog.getSObject('VTR3_T_TN_Notification__r') != null ? (String) tnCatalog.getSObject('VTR3_T_TN_Notification__r').get('VTD2_T_Task_Unique_Code__c') : null;
                VT_D2_TNCatalogTasks.TNCatalogDescriptor catalogDescriptor = new VT_D2_TNCatalogTasks.TNCatalogDescriptor(sObjectName, passIfEmptyRecipient, recordTypeName, tnNotificationCode);
                List <String> queryParts = new List<String>();
                Set <String> fields = new Set<String>();
                queryParts.add('SELECT ');
                for (String catalogFieldName : tnFieldNamesMapping.get(recordTypeName).keySet()) {
                    Object value = tnCatalog.get(catalogFieldName);
                    String taskFieldName = tnFieldNamesMapping.get(recordTypeName).get(catalogFieldName);
                    if (taskFieldName == 'OwnerId') {
                        ownerFieldName = catalogFieldName;
                    }
                    if (catalogFieldName == 'VTD2_T_Task_Unique_Code__c') {
                        catalogDescriptor.params.put(catalogFieldName, value);
                    } else if (value instanceof Boolean) {
                        catalogDescriptor.params.put(catalogFieldName, value);
                    } else {
                        List <VT_D2_TNCatalogUtils.ExpressionComponent> components = VT_D2_TNCatalogUtils.parseExpression((String) value, sObjectName, fields, queryParts, taskFieldName == 'OwnerId' && recordTypeName == 'Task');
                        catalogDescriptor.params.put(catalogFieldName, components);
                    }
                }
                queryParts.add(' FROM ' + sObjectName);
                catalogDescriptor.query = String.join(queryParts, '');
                codeToTnCatalogDescriptorMap.put((String) catalogDescriptor.params.get('VTD2_T_Task_Unique_Code__c'), catalogDescriptor);
            } 
        }
        
        List <Task> Tasks = new List<Task>();
        List <VTD1_SC_Task__c> SCTasks = new List<VTD1_SC_Task__c>();
        List <VTD2_TMA_Task__c> TMATasks = new List<VTD2_TMA_Task__c>();
        List <VTR5_Pool_Task__c> PoolTasks = new List<VTR5_Pool_Task__c>();
        List <VTR5_CRA_Task__c> CRATasks = new List<VTR5_CRA_Task__c>(); // SH-17444 Multiple CRA 

        List<VTR5_ConnectedDeviceNotifications__c> customSetting = [
                SELECT Id, Name,

                    VTR5_OutOfRangeBatchLastExecutionTime__c, 
                    VTR5_OutOfRangeBatchPublicReadingQuery__c, 
                    VTR5_OutOfRangeBatchPatientDeviceField__c, 
                    VTR5_OutOfRangeBatchPublicReadingField__c,
                    VTR5_PG_Notification_Catalog_Code__c,
                    VTR5_PG_Task_Catalog_Code__c,
                    VTR5_PI_Notification_Catalog_Code__c,
                    VTR5_SCR_Notification_Catalog_Code__c,
                    VTR5_SCR_Task_Catalog_Code__c

                FROM VTR5_ConnectedDeviceNotifications__c
                LIMIT 1
        ];

        Map <Id, User> userMap = null;
        if (assignedToMap != null) {
            Set <String> receiverIds = new Set<String>();
            for (String text : assignedToMap.values()) {
                receiverIds.addAll(text.split(','));
            }
            userMap = new Map<Id, User>([SELECT Id, IsActive, Profile.Name FROM User WHERE Id IN :receiverIds]);
        }
        for (String tnCode : sourceIdsByCodeMap.keySet()) {
            TNCatalogDescriptor tnCatalogDescriptor = codeToTnCatalogDescriptorMap.get(tnCode);
            if (tnCatalogDescriptor == null) {
                continue;
            }
            String query = tnCatalogDescriptor.query + ' WHERE Id IN (\'' + String.join(sourceIdsByCodeMap.get(tnCode), '\',\'') + '\')';
            List <SObject> result = Database.query(query);
            for (SObject sobj : result) {
                SObject task = null;
                if (tnCatalogDescriptor.recordTypeName == 'Task') {
                    task = new Task();
                } else if (tnCatalogDescriptor.recordTypeName == 'SCTask') {
                    task = new VTD1_SC_Task__c();
                } else if (tnCatalogDescriptor.recordTypeName == 'TMATask') {
                    task = new VTD2_TMA_Task__c();
                }
                // SH-17444 Multiple CRA (Creating CRA Task Instance) - Start
                else if (tnCatalogDescriptor.recordTypeName == VT_R4_ConstantsHelper_Tasks.RECORD_TYPE_NAME_TNCATALOG_CRATASK) {
                    task = new VTR5_CRA_Task__c();
                } 
                // SH-17444 Multiple CRA - End
                else {
                    task = new VTR5_Pool_Task__c();
                }
                Boolean isOwnerActive = false;
                String ownerProfileName = null;
                List <String> subjectParameters = new List<String>();
                for (String tnFieldName : tnCatalogDescriptor.params.keySet()) { 
                    Object value = null;
                    Object params = tnCatalogDescriptor.params.get(tnFieldName);
                    if (params != null && tnFieldName.indexOf('VTD2_T_Subject_Parameter_Field') >= 0) {
                        value = VT_D2_TNCatalogUtils.evalExpression((List <VT_D2_TNCatalogUtils.ExpressionComponent>) params, sobj, false, false);
                        if (value != null) {
                            subjectParameters.add(subjectParams.get(tnFieldName + sobj.Id) != null ? subjectParams.get(tnFieldName + sobj.Id) + value : '' + value);
                        }
                    } else if (params instanceof List <VT_D2_TNCatalogUtils.ExpressionComponent>) {
                        value = VT_D2_TNCatalogUtils.evalExpression((List <VT_D2_TNCatalogUtils.ExpressionComponent>) params, sobj, false, false);
                        if (tnFieldName == ownerFieldName && tnCatalogDescriptor.recordTypeName == 'Task') {
                            Object objOwnerActive = VT_D2_TNCatalogUtils.evalExpression((List <VT_D2_TNCatalogUtils.ExpressionComponent>) params, sobj, true, false);
                            if (objOwnerActive instanceof Boolean) {
                                isOwnerActive = (Boolean) objOwnerActive;
                            }
                            Object objOwnerProfileName = VT_D2_TNCatalogUtils.evalExpression((List <VT_D2_TNCatalogUtils.ExpressionComponent>) params, sobj, false, true);
                            if (objOwnerProfileName instanceof String) {
                                ownerProfileName = (String) objOwnerProfileName;
                                if (tnCatalogDescriptor.tnNotificationCode != null/* && PROFILES_AUTO_NOTIFY.contains(ownerProfileName)*/) {
                                    tnNotificationCodeMap.put(tnCatalogDescriptor.tnNotificationCode + '_' + sobj.get('Id'), ownerProfileName);
                                }
                            }
                        }
                    } else {
                        value = tnCatalogDescriptor.params.get(tnFieldName);
                    }
                    if (value != null) {
                        String taskFieldName = tnFieldNamesMapping.get(tnCatalogDescriptor.recordTypeName).get(tnFieldName);
                        if (taskFieldName != null) {
                            try {
                                task.put(taskFieldName, value);
                            } catch (SObjectException e) {
                                if (e.getMessage() == 'Illegal assignment from Datetime to Date') {
                                    task.put(taskFieldName, ((Datetime) value).date());
                                } else if (e.getMessage() == 'Illegal assignment from Date to Datetime') {
                                    task.put(taskFieldName, Datetime.valueOf(value));
                                }
                            }
                        }
                    }
                }
              
                if (subjectParameters.size() > 0 && task instanceof Task) {
                    ((Task) task).VTD2_Subject_Parameters__c = String.join(subjectParameters, ';');
                }
                

                if (task instanceof Task && !customSetting.isEmpty() &&
                        (((Task) task).VTD2_Task_Unique_Code__c == customSetting[0].VTR5_PG_Task_Catalog_Code__c
                                || ((Task) task).VTD2_Task_Unique_Code__c == customSetting[0].VTR5_SCR_Task_Catalog_Code__c)) {
                    VT_D1_TranslateHelper.translateAllfields(new List<Task>{
                            (Task) task
                    });

                }

                String key = tnCode + sobj.Id;
                List <Task> newTasks = new List<Task>();
                if (assignedToMap != null && assignedToMap.containsKey(key) && task instanceof Task) {
                    List <Id> receivers = assignedToMap.get(key).split(',');
                    for (Id receiver : receivers) {
                        Task taskClone = (Task) task.clone();
                        taskClone.put('OwnerId', receiver);
                        newTasks.add(taskClone);
                    }
                } else if (task instanceof Task) {
                    newTasks.add((Task) task);
                }
                for (Task newTask : newTasks) {
                    if (tnCatalogDescriptor.passIfEmptyRecipient && newTask.get('OwnerId') == null) {
                        continue;
                    }
                    if (userMap != null && newTask.get('OwnerId') != null) {
                        User user = userMap.get((Id) newTask.get('OwnerId'));
                        if (user != null) {
                            isOwnerActive = user.IsActive;
                            if (tnCatalogDescriptor.tnNotificationCode != null) {
                                tnNotificationCodeMap.put(tnCatalogDescriptor.tnNotificationCode + '_' + sobj.get('Id'), user.Profile.Name);
                            }
                        }
                    }
                    if (isOwnerActive != null && isOwnerActive) {
                        Tasks.add((Task) newTask);
                    }
                }
                if (task instanceof VTD1_SC_Task__c) {
                    SCTasks.add((VTD1_SC_Task__c) task);
                } else if (task instanceof VTD2_TMA_Task__c) {
                    TMATasks.add((VTD2_TMA_Task__c) task);
                } else if (task instanceof VTR5_Pool_Task__c) {
                    PoolTasks.add((VTR5_Pool_Task__c) task);
                }
                // SH-17444 Multiple CRA - Start
                else if (task instanceof VTR5_CRA_Task__c) {
                    CRATasks.add((VTR5_CRA_Task__c) task);
                } // SH-17444 Multiple CRA - End
            }
        }
        if (!Tasks.isEmpty() && autoInsert) {
            insert Tasks;
        }
        if (!SCTasks.isEmpty() && autoInsert) {
            insert SCTasks;
        }
        if (!TMATasks.isEmpty() && autoInsert) {
            insert TMATasks;
        }
        if (!PoolTasks.isEmpty() && autoInsert) {
            insert PoolTasks;
        }
        // SH-17444 Multiple CRA - Start
        if (!CRATasks.isEmpty() && autoInsert) {
            try {
                if (VT_Utilities.isCreateable('VTR5_CRA_Task__c')) {
                    insert CRATasks;
                }
            } catch(Exception e) {
                ErrorLogUtility.logException(e, null, VT_D2_TNCatalogTasks.class.getName());
            }
            
        } // SH-17444 Multiple CRA - End

        List <String> tnNotificationCodes = new List<String>();
        List <Id> sourceNotificationIds = new List<Id>();
        for (String tnCode : tnNotificationCodeMap.keySet()) {
            String ownerProfileName = tnNotificationCodeMap.get(tnCode);
            if (PROFILES_AUTO_NOTIFY.contains(ownerProfileName)) {
                List <String> parts = tnCode.split('_');
                tnNotificationCodes.add(parts[0]);
                sourceNotificationIds.add((Id) parts[1]);
            }
        }
        if (!tnNotificationCodes.isEmpty()) {
            VT_D2_TNCatalogNotifications.generateNotifications(tnNotificationCodes, sourceNotificationIds, new Map <String, String>());
        }
        if (!Tasks.isEmpty()) {
            return Tasks;
        } else if (!SCTasks.isEmpty()) {
            return SCTasks;
        } else if (!TMATasks.isEmpty()) {
            return TMATasks;
        } 
        // SH-17444 Multiple CRA - Start
        else if (!CRATasks.isEmpty()) {
            return CRATasks;
        } // SH-17444 Multiple CRA - End
        else {
            return PoolTasks;
        }
    }

    private static List <SObject> loadTNCatalogs(List <String> codes) {
        Set <String> fieldNamesSet = new Set <String>();
        for (String recordTypeName : tnFieldNamesMapping.keySet()) {
            Map <String, String> mapping = tnFieldNamesMapping.get(recordTypeName);
            for (String fieldName : mapping.keySet()) {
                fieldNamesSet.add(fieldName);
            }
        }
        List <String> tnCatalogFieldNames = new List<String>();
        for (String fieldName : fieldNamesSet) {
            tnCatalogFieldNames.add(fieldName);
        }
        if(!codes.isEmpty()){
            String query = 'SELECT RecordType.Name, VTR3_T_TN_Notification__r.VTD2_T_Task_Unique_Code__c, ' + String.join(tnCatalogFieldNames, ',') +
                +',VTD2_T_SObject_Name__c, VTR2_Pass_if_empty_recipient__c FROM VTD2_TN_Catalog_Code__c WHERE VTD2_T_Task_Unique_Code__c IN (' + String.join(codes, ',') + ')';
            return Database.query(query);  
        }else
        {
            return null;
        }
       
    }
}