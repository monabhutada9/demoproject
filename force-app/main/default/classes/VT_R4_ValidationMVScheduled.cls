/**
* @author: Olga Baranova
* @date: 03-Mar-20
**/

public with sharing class VT_R4_ValidationMVScheduled {

    public static Map<Id, Integer> buffersMap = new Map<Id, Integer>();
    public static Map<Id, List<User>> visitUsersMap = new Map<Id, List<User>>();
    public Map<Event, VTD1_Monitoring_Visit__c> fakeEventVisitMap = new Map<Event, VTD1_Monitoring_Visit__c>();
    public Map<Id, List<Event>> visitEventsMap = new Map<Id, List<Event>>();
    public Map<Id, User> userMap = new Map<Id, User>();

    public void isItAvailableTimeForVisit(List<VTD1_Monitoring_Visit__c> visits) {
        populateVisitUsersMap(visits);
        List<Event> fakeEvents = createFakeEvent(visits);
        if (fakeEvents.size() > 0) {
            checkDateTimeIsAvailable(fakeEvents);
        }
    }

    private List<Event> createFakeEvent(List<VTD1_Monitoring_Visit__c> visits) {
        List<User> users = new List<User>();
        for (VTD1_Monitoring_Visit__c visit : visits) {
            users = visitUsersMap.get(visit.Id);
        }
        List<Id> profileIds = new List<Id>();
        for (User usr : users) {
            if (usr != null) {
                profileIds.add(usr.ProfileId);
            }
        }

        setBuffers(profileIds);
        List<Event> events = new List<Event>();
        for (VTD1_Monitoring_Visit__c visit : visits) {
            List<User> participantUsers = new List<User>();
            participantUsers = visitUsersMap.get(visit.Id);
            Datetime endDateTime = visit.VTD1_Visit_Start_Date__c.addHours(Integer.valueOf(visit.VTD1_MV_Duration__c));
            for (User usr : participantUsers) {
                if (usr != null) {
                    Event nEvent = new Event();
                    nEvent.OwnerId = usr.Id;
                    nEvent.StartDateTime = visit.VTD1_Visit_Start_Date__c;
                    nEvent.WhatId = visit.Id;
                    Integer buffer = buffersMap.get(usr.ProfileId);
                    if (buffer != null) {
                        endDateTime = endDateTime.addMinutes(buffer);
                    }
                    nEvent.EndDateTime = endDateTime;
                    fakeEventVisitMap.put(nEvent, visit);
                    events.add(nEvent);
                }
            }
            visitEventsMap.put(visit.Id, events);
        }
        return events;
    }

    private static void setBuffers(List<Id> profileIds) {
        List<VTD1_CalendarBufferForScheduledVisit__mdt> buffers = [
                SELECT VTD1_Buffer__c, VTD1_ProfileId__c
                FROM VTD1_CalendarBufferForScheduledVisit__mdt
                WHERE VTD1_ProfileId__c IN :profileIds
        ];
        for (VTD1_CalendarBufferForScheduledVisit__mdt b : buffers) {
            buffersMap.put(b.VTD1_ProfileId__c, b.VTD1_Buffer__c.intValue());
        }
    }

    private void populateVisitUsersMap(List<VTD1_Monitoring_Visit__c> monitoringVisit) {
        List<Id> siteIds = new List<Id>();

        for (VTD1_Monitoring_Visit__c visits : monitoringVisit) {
            siteIds.add(visits.VTD1_Virtual_Site__c);
        }
        Map<Id, Virtual_Site__c> siteMap = new Map<Id, Virtual_Site__c>([
                SELECT
                        Id,
                        VTR2_Primary_SCR__c,
                        VTD1_Study_Team_Member__r.User__c,
                        VTD1_Study_Team_Member__r.VTD1_PIsAssignedPG__c
                FROM Virtual_Site__c
                WHERE Id IN :siteIds
        ]);

        Map<Id, Virtual_Site__c> visitSite = new Map<Id, Virtual_Site__c>();
        for (VTD1_Monitoring_Visit__c visit : monitoringVisit) {
            visitSite.put(visit.Id, siteMap.get(visit.VTD1_Virtual_Site__c));
        }
        
        List<Id> participantIds = new List<Id>();
        for (VTD1_Monitoring_Visit__c visit : monitoringVisit) {
            participantIds.add(visitSite.get(visit.Id).VTR2_Primary_SCR__c);
            participantIds.add(visitSite.get(visit.Id).VTD1_Study_Team_Member__r.User__c);
            participantIds.add(visitSite.get(visit.Id).VTD1_Study_Team_Member__r.VTD1_PIsAssignedPG__c);
            participantIds.add(visit.VTR5_VisitCRAUser__c);//Priyanka Ambre SH-17441:Removed the reference of VTR2_Remote_Onsite__c field
        }

        for (User usr : [SELECT Id, ProfileId, Profile.Name FROM User WHERE Id IN :participantIds]) {
            userMap.put(usr.Id, usr);
        }

        for (VTD1_Monitoring_Visit__c visit : monitoringVisit) {
            List<User> users = new List<User>();
            users.add(userMap.get(visitSite.get(visit.Id).VTR2_Primary_SCR__c));
            users.add(userMap.get(visitSite.get(visit.Id).VTD1_Study_Team_Member__r.User__c));
            users.add(userMap.get(visitSite.get(visit.Id).VTD1_Study_Team_Member__r.VTD1_PIsAssignedPG__c));          
            users.add(userMap.get(visit.VTR5_VisitCRAUser__c));//Priyanka Ambre SH-17441:Removed the reference of VTR2_Remote_Onsite__c field
            visitUsersMap.put(visit.Id, users);
        }
    }

    private Boolean compareDateTime(Event eventToCheckAgainst, Event eventToCheck) {
        if (((eventToCheckAgainst.StartDateTime <= eventToCheck.StartDateTime
                && eventToCheckAgainst.EndDateTime > eventToCheck.StartDateTime)
                || (eventToCheckAgainst.EndDateTime > eventToCheck.StartDateTime
                && eventToCheckAgainst.EndDateTime <= eventToCheck.EndDateTime))
                && eventToCheckAgainst.OwnerId == eventToCheck.OwnerId && eventToCheckAgainst.WhatId != eventToCheck.WhatId) {
            return true;
        } else {
            return false;
        }
    }

    private void checkDateTimeIsAvailable(List<Event> fakeEvents) {
        Datetime minDT = fakeEvents[0].StartDateTime;
        Datetime maxDT = fakeEvents[0].EndDateTime;
        List<Id> owners = new List<Id>();
        for (Event evToCheck : fakeEvents) {
            if (evToCheck.StartDateTime < minDT) {
                minDT = evToCheck.StartDateTime;
            }
            if (evToCheck.EndDateTime > maxDT) {
                maxDT = evToCheck.EndDateTime;
            }
            owners.add(evToCheck.OwnerId);
        }

        String errorSame = 'Sorry, but you have the same time for different visits!';
        for (Id currentMvId : visitEventsMap.keySet()) {
            List<Event> currentFakeEvents = visitEventsMap.get(currentMvId);
            for (Id otherMvId : visitEventsMap.keySet()) {
                if (otherMvId != currentMvId) {
                    List<Event> fakeEventsToCheckAgainst = visitEventsMap.get(currentMvId);
                    for (Event fakeEventToCheckAgainst : fakeEventsToCheckAgainst) {
                        for (Event currentFakeEvent : currentFakeEvents) {
                            if (compareDateTime(fakeEventToCheckAgainst, currentFakeEvent)) {
                                VTD1_Monitoring_Visit__c visit = fakeEventVisitMap.get(currentFakeEvent);
                                visit.VTD1_Visit_Start_Date__c.addError(errorSame);
                                break;
                            }
                        }
                    }
                }
            }
        }

        List<Event> eventsInSystem = [
                SELECT Id, StartDateTime, EndDateTime, OwnerId, WhatId
                FROM Event
                WHERE ((StartDateTime >= :minDT AND StartDateTime < :maxDT)
                OR (EndDateTime > :minDT AND EndDateTime <= :maxDT)
                OR (StartDateTime <= :minDT AND EndDateTime >= :maxDT))
                AND IsChild = FALSE
                AND OwnerId IN :owners
        ];
        if (eventsInSystem.size() != 0) {
            String error = 'Sorry, but this time is no longer available!';
            for (Event evInSystem : eventsInSystem) {
                for (Event evToCheck : fakeEvents) {
                    if (compareDateTime(evInSystem, evToCheck)) {
                        VTD1_Monitoring_Visit__c visit = fakeEventVisitMap.get(evToCheck);
                        visit.VTD1_Visit_Start_Date__c.addError(error);
                        break;
                    }
                }
            }
        }
    }
}