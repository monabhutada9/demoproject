// SH-17976
global without sharing class VT_R6_CaseLastVisitDateUpdatedBatch implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
                SELECT VTR5_LastVisitCompleteDate__c,
                    (SELECT VTD1_Visit_Completion_Date__c FROM Actual_Visits__r WHERE VTD1_Status__c = :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED ORDER BY VTD1_Case__c, VTD1_Visit_Completion_Date__c)
                FROM Case
                WHERE VTR5_LastVisitCompleteDate__c = null
        ]);
    }

    global void execute(Database.BatchableContext bc, List<Case> scope) {
        for (Case c : scope) {
            if (!c.Actual_Visits__r.isEmpty()) {
                c.VTR5_LastVisitCompleteDate__c = Date.valueOf(c.Actual_Visits__r[c.Actual_Visits__r.size() - 1].VTD1_Visit_Completion_Date__c);
            }
        }
        update scope;
    }

    global void finish(Database.BatchableContext bc) {
        System.debug('VT_R6_CaseLastVisitDateUpdatedBatch finished.');
    }
}