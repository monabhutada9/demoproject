@IsTest
public with sharing class VT_R2_DocuSignCommunityProcessCtrlTest {
    
    public static void test1() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );
        new DomainObjects.VTD2_Study_Geography_t(study).persist();

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t();
        DomainObjects.User_t userPatient = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        userPatient.persist();

        DomainObjects.Contact_t piContact = new DomainObjects.Contact_t();
        piContact.persist();

        DomainObjects.User_t userPI = new DomainObjects.User_t()
                .addContact(piContact)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME);
        userPI.persist();

        DomainObjects.User_t userCRA = new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME);
        userCRA.persist();

        DomainObjects.User_t userPG = new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME);
        userPG.persist();

        DomainObjects.StudyTeamMember_t stmCra = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(userCRA)
                .setVTD1_PIsAssignedPG(userPG)
                .setRecordTypeByName('CRA');
        stmCra.persist();

        DomainObjects.StudyTeamMember_t stmPI = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(userPI)
                .setRecordTypeByName('PI');
        stmPI.persist();
        Test.stopTest();

        DomainObjects.StudyTeamMember_t stmPG = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(userPG)
                .setIsActive(true)
            //START RAJESH SH-17440
               // .setOnsiteCraId(stmCra.id)
               // .setRemoteCraId(stmCra.id)
            //END:SH-17440
                .setVTD1_Ready_For_Assignment(true)
                .setRecordTypeByName('PG');
        stmPG.persist();


//   hello copado     DomainObjects.StudySiteTeamMember_t sstm = new DomainObjects.StudySiteTeamMember_t()
//   hello copado              .setAssociatedPI2(stmPI)
//   hello copado              .setAssociatedPg(stmPG);
//   hello copado      sstm.persist();


//        DomainObjects.HealthCloudGA_CandidatePatient_t candidatePatient = new DomainObjects.HealthCloudGA_CandidatePatient_t();
//        candidatePatient.persist();

        DomainObjects.Case_t caseRecord = new DomainObjects.Case_t()
//                .setRecordTypeByName('CarePlan')
//                .addStudy(study)
//                .addAccount(new DomainObjects.Account_t().setHealthCloudGA_CandidatePatient(candidatePatient.id))
//                .addPIUser(userPI)
//                .addVTD1_PI_contact(piContact)
//                .addContact(patientContact)
//                .addVTD1_Patient_User(userPatient)
//                .setVTD1_PCF_Safety_Concern_Indicator('No')
                .addUser(userPatient);
        /*Status = 'Open',
        VTD1_PCF_Safety_Concern_Indicator__c = 'No',
        ContactId = contactId,
        VTD1_Clinical_Study_Membership__c = parentId,
        VTD1_PI_contact__c = PIcontactId,
        VTD1_Patient__c = patientId,
        VTD1_PI_user__c = PIUserID
        */
        caseRecord.persist();
        System.assert(String.isNotEmpty(caseRecord.id));
    }
}