/**
 * @description Created by Yuliya Yakushenkova on 10/16/2020.
 */
@IsTest
public class VT_R5_MobilePatientHomePageTest {

    public static void firstTest() {
        User usr = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        System.runAs(usr) {
            VT_R5_AllTestsMobile.setRestContext('/mobile/v0/homepage', 'GET');
            Test.startTest();
            VT_R5_MobileRouter.doGET();
            Test.stopTest();
        }
    }
    public static void secondTest() {
        User usr = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        System.runAs(usr) {
            VT_R5_AllTestsMobile.setRestContext('/mobile/v1/homepage', 'GET');
            Test.startTest();
            VT_R5_MobileRouter.doGET();
            Test.stopTest();
        }
    }
    public static void thirdTest() {
        User usr = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        Case patientCase = VT_R5_PatientQueryService.getCaseByUserId(usr.Id);
        patientCase.Contact.VTD1_FullHomePage__c = true;
        Test.startTest();
        update patientCase.Contact;
        insert new Task(VTD2_My_Task_List_Redirect__c = '&banner=some-value', VTD1_Case_lookup__c = patientCase.Id, ActivityDate = System.now().addDays(-5).date(), OwnerId = usr.Id, Subject = 'some subject', VTD1_Caregiver_Clickable__c = false, VTD2_isRequestVisitRedirect__c = false, Status = 'Open', WhatId = patientCase.Id, Category__c = 'Signature Required');
        insert new Task(VTD2_My_Task_List_Redirect__c = 'full-profile', VTD1_Case_lookup__c = patientCase.Id, ActivityDate = System.now().addDays(-5).date(), OwnerId = usr.Id, Subject = 'some subject', VTD1_Caregiver_Clickable__c = false, VTD2_isRequestVisitRedirect__c = false, Status = 'Completed', WhatId = patientCase.Id, Category__c = 'Signature Required');
        insert new Task(VTR5_EndDate__c =System.now().addDays(5).date(), VTD1_Case_lookup__c = patientCase.Id, OwnerId = usr.Id, Subject = 'some subject', VTD1_Caregiver_Clickable__c = false, VTD2_isRequestVisitRedirect__c = false, Status = 'Completed', WhatId = patientCase.Id, Category__c = 'Signature Required');

        System.runAs(usr) {
            VT_R5_AllTestsMobile.setRestContext('/mobile/v1/homepage', 'GET');
            VT_R5_MobileRouter.doGET();
        }
        Test.stopTest();
    }
}