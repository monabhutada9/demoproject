/**
 * Created by user on 9/16/2020.
 */

public without sharing class VT_R5_AssignApexClassesAccessPermission extends VT_R3_AbstractPublishedAction {
    private Set<Id> userIds;

    public VT_R5_AssignApexClassesAccessPermission(Set<Id> userIds) {
        this.userIds = userIds;
    }

    public override Type getType() {
        return VT_R5_AssignApexClassesAccessPermission.class;
    }

    public override void execute() {
        List<PermissionSetAssignment> permissionSetAssignments = new List<PermissionSetAssignment>();
        try {
            PermissionSet userApexAccess = [SELECT Id FROM PermissionSet WHERE Name = 'UserApexAccess'];
            for (Id userId : userIds) {
                permissionSetAssignments.add(new PermissionSetAssignment(PermissionSetId = userApexAccess.Id, AssigneeId = userId));
            }
            Database.insert(permissionSetAssignments, false);
        } catch (Exception e) {
            System.debug(e.getMessage());
        }
    }
}