/**
* @author: Carl Judge
* @date: 02-Sep-19
* @description: Creates caregivers for candidate patients after main patient conversion
**/

public without sharing class VT_R3_PublishedAction_CreateCaregivers extends VT_R3_AbstractPublishedAction {
    private List<HealthCloudGA__CandidatePatient__c> candidatePatients;

    public VT_R3_PublishedAction_CreateCaregivers(List<HealthCloudGA__CandidatePatient__c> candidatePatients) {
        this.candidatePatients = candidatePatients;
    }

    public override Type getType(){
        return VT_R3_PublishedAction_CreateCaregivers.class;
    }

    public override void execute() {
        List<Contact> caregivers = new List<Contact>();
        for (HealthCloudGA__CandidatePatient__c cand : candidatePatients) {
            caregivers.add(new Contact(
                AccountId = cand.HealthCloudGA__AccountId__c,
                FirstName = cand.VTR2_Caregiver_First_Name__c,
                LastName = cand.VTR2_Caregiver_Last_Name__c,
                Email = cand.VTR2_Caregiver_Email__c,
                Phone = cand.VTR2_Caregiver_Phone__c,
                RecordTypeId = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_CAREGIVER,
                VTR2_Primary_Language__c = cand.VTR2_Caregiver_Preferred_Language__c,
                VTR2_Doc_Language__c = cand.VTR2_Caregiver_Preferred_Language__c,
                VTD1_Primary_CG__c = true,
                VTR3_CreateUser__c = true
            ));
        }

        Database.SaveResult[] results = Database.insert(caregivers, false);
        Map<Id, String> candIdToErrorMap = new Map<Id, String>();
        for (Integer i = 0; i < results.size(); i++) {
            if (!results[i].isSuccess()) {
                String error = '';
                for (Database.Error err : results[i].getErrors()) {
                    error += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                }
                candIdToErrorMap.put(candidatePatients[i].Id, error);
            }
        }

        if (! candIdToErrorMap.isEmpty()) {
            List<Task> failureTasks = new List<Task>();
            for (HealthCloudGA__CandidatePatient__c failedCand : [
                SELECT Id, Study__c, Study__r.VTD1_Study_Admin__c, HealthCloudGA__AccountId__r.HealthCloudGA__CarePlan__c
                FROM HealthCloudGA__CandidatePatient__c
                WHERE Id IN :candIdToErrorMap.keySet()
            ]) {
                failureTasks.add(new Task(
                    WhatId = failedCand.Id,
                    OwnerId = failedCand.Study__r.VTD1_Study_Admin__c,
                    Subject = 'Investigate Caregiver Account Creation Failure',
                    RecordTypeId = VT_R4_ConstantsHelper_Tasks.RECORD_TYPE_ID_TASK_SIMPLETASK,
                    Status = 'Not Started',
                    Description = candIdToErrorMap.get(failedCand.Id),
                    HealthCloudGA__CarePlanTemplate__c = failedCand.Study__c,
                    VTD1_Case_lookup__c = failedCand.HealthCloudGA__AccountId__r.HealthCloudGA__CarePlan__c,
                    ActivityDate = Date.today()
                ));
            }
            insert failureTasks;
        }

        for (HealthCloudGA__CandidatePatient__c item : candidatePatients) {
            item.VTR2_Caregiver_Creation_Status__c = candIdToErrorMap.containsKey(item.Id) ?
                VT_R4_ConstantsHelper_ProfilesSTM.CANDIDATE_PATIENT_CAREGIVER_STATUS_FAILURE : VT_R4_ConstantsHelper_ProfilesSTM.CANDIDATE_PATIENT_CAREGIVER_STATUS_SUCCESS;
        }
        update candidatePatients;
    }
}