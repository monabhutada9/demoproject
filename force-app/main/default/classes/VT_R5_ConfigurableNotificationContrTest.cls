/**
 * Created by A.Lutsevich on 23.10.2020.
 */
@IsTest
private with sharing class VT_R5_ConfigurableNotificationContrTest {
    @IsTest
    static void testVT_R5_ConfigurableNotificationController() {
        System.assertEquals(0, VT_D1_UtilityBarVideoController.getModalData().size());
        System.assertNotEquals(null, VT_R5_ConfigurableNotificationController.getBaseUrl());

        SObject study = (SObject) new DomainObjects.Study_t()
                .persist();

        new DomainObjects.StudyTeamMember_t()
                .setStudyId(study.Id)
                .setUserId(UserInfo.getUserId())
                .persist();

        VTR5_StudyTeamNotification__c notification = new VTR5_StudyTeamNotification__c(
                VTR5_Study__c = study.Id,
                VTR5_NotificationActive__c = true,
                VTR5_ActiveOfSubmitBanner__c = true,
                VTR5_StartDate__c = Datetime.now().addMinutes(-1),

                VTR5_Duration__c = 4,

                VTR5_DurationSubmitMessage__c = 1

        );
        insert notification;

        new VT_R5_ConfigurableNotificationService().herokuExecute(JSON.serialize(new List<Id>{notification.Id}));

        Boolean customValidationRule = false;
        try {
            insert new VTR5_StudyTeamNotification__c(
                    VTR5_Study__c = study.Id,
                    VTR5_NotificationActive__c = true,
                    VTR5_ActiveOfSubmitBanner__c = true,
                    VTR5_StartDate__c = Datetime.now().addMinutes(-1),

                    VTR5_Duration__c = 4,

                    VTR5_DurationSubmitMessage__c = 1

            );
        } catch (Exception ex) {
            customValidationRule = true;
        }

        System.assertEquals(true, customValidationRule);

        System.assertNotEquals(0, VT_R5_ConfigurableNotificationController.getInitData().size());
        VT_D1_UtilityBarVideoController.createNotificationAction(notification.Id);
        System.assertNotEquals(0, VT_R5_ConfigurableNotificationController.getInitData().size());

        notification.VTR5_NotificationActive__c = false;
        update notification;
        System.assertNotEquals(0, VT_R5_ConfigurableNotificationController.getInitData().size());
        

        notification.VTR5_StartDate__c = Datetime.now().addDays(-5);
        update notification;

        new VT_R5_ConfigurableNotificationService().herokuExecute(JSON.serialize(new List<Id>{notification.Id}));
    }
}