@IsTest
private class VT_R5_eCoaMultidayDiaryProcessTest {
    private static final String RULE_NAME = 'testRule';

    @IsTest
    static void doTest() {
        VT_Stubber.applyStub('VT_R5_eCoaMultidayDiaryProcess.getRules', new List<eCOA_Multi_Day_Rule__mdt>{
            new eCOA_Multi_Day_Rule__mdt(Label = RULE_NAME)
        });
        Case patientCase = (Case) new DomainObjects.Case_t()
            .setRecordTypeByName('VTD1_PCF')
            .addAccount(new DomainObjects.Account_t())
            .persist();

        Datetime now = Datetime.now();

        // Test 2 diaries in trigger together
        Datetime firstStartTime = now;
        Datetime firstEndTime = now.addHours(1);
        VTD1_Survey__c firstDiary = new VTD1_Survey__c(
            VTD1_CSM__c = patientCase.Id,
            RecordTypeId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('VTR5_External').getRecordTypeId(),
            VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_DUE_SOON,
            VTD1_Date_Available__c = firstStartTime,
            VTD1_Due_Date__c = firstEndTime,
            VTR5_eCoa_RuleName__c = RULE_NAME
        );
        Datetime secondStartTime = now.addHours(1);
        Datetime secondEndTime = now.addHours(2);
        VTD1_Survey__c secondDiary = new VTD1_Survey__c(
            VTD1_CSM__c = patientCase.Id,
            RecordTypeId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('VTR5_External').getRecordTypeId(),
            VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_DUE_SOON,
            VTD1_Date_Available__c = secondStartTime,
            VTD1_Due_Date__c = secondEndTime,
            VTR5_eCoa_RuleName__c = RULE_NAME
        );
        insert new List<VTD1_Survey__c>{firstDiary, secondDiary};
        List<VTD1_Survey__c> surveys = [SELECT VTD1_Date_Available__c, VTD1_Due_Date__c FROM VTD1_Survey__c];
        System.assertEquals(1, surveys.size());
        System.assertEquals(firstStartTime, surveys[0].VTD1_Date_Available__c);
        System.assertEquals(secondEndTime, surveys[0].VTD1_Due_Date__c);

        // Test another diary to expand the window
        Datetime thirdStartTime = now.addHours(4);
        Datetime thirdEndTime = now.addHours(5);
        VTD1_Survey__c thirdDiary = new VTD1_Survey__c(
            VTD1_CSM__c = patientCase.Id,
            RecordTypeId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('VTR5_External').getRecordTypeId(),
            VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_DUE_SOON,
            VTD1_Date_Available__c = thirdStartTime,
            VTD1_Due_Date__c = thirdEndTime,
            VTR5_eCoa_RuleName__c = RULE_NAME
        );
        insert thirdDiary;
        surveys = [SELECT VTD1_Date_Available__c, VTD1_Due_Date__c FROM VTD1_Survey__c];
        System.assertEquals(1, surveys.size());
        System.assertEquals(firstStartTime, surveys[0].VTD1_Date_Available__c);
        System.assertEquals(thirdEndTime, surveys[0].VTD1_Due_Date__c);
    }
}