public without sharing class VT_D1_SomeFilesListByRecord {

    private static String sessionId {
        get {
            if (sessionId == null) {
                sessionId = VT_D1_HelperClass.getSessionIdForGuest();
            }
            return sessionId;
        } set;
    }

    private static String org_url = VTD1_FileUploadUserCredentials__c.getInstance().org_url__c;

	@AuraEnabled
	public static List<VTD1_Document__c> getDocs(String recordId) {
		return [
			SELECT Id, VTD1_FileNames__c
			FROM VTD1_Document__c
			WHERE VTD1_Clinical_Study_Membership__c = :recordId
			AND VTD1_Uploading_Not_Finished__c = TRUE
		];
	}

    @AuraEnabled
    public static Case getCase(String recordId) {
        return [
            SELECT VTD1_Patient_User__r.Name,
                VTR2_StudyPhoneNumber__r.Name,
                VTD1_Study__r.Name
            FROM Case
            WHERE Id = :recordId
        ];
    }

    @AuraEnabled
    public static List<VTD1_Document__c> doDelete(Id docId, Id recordId) {
        String baseUrl = org_url + '/services/data/v43.0';

        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer ' + sessionId);
        HttpResponse res;

        // query ContentDocument IDs
        String queryStr = 'SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId=\'' + docId + '\'';
        queryStr = EncodingUtil.urlEncode(queryStr, 'UTF-8');
        queryStr = '/query/?q=' + queryStr;

        req.setMethod('GET');
        req.setEndpoint(baseUrl + queryStr);
        res = http.send(req);

        List<Id> contentDocumentIds = new List<Id>();

        Map<String, Object> deserializedResults = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        List<Object> results = (List<Object>)deserializedResults.get('records');
        for (Object result : results) {
            Map<String, Object> resultMap = (Map<String, Object>)result;
            contentDocumentIds.add((Id)resultMap.get('ContentDocumentId'));
        }

        // delete content documents
        if (! contentDocumentIds.isEmpty()) {
            String deleteStr = '/composite/sobjects?ids=';

            for (Id contentDocId : contentDocumentIds) {
                deleteStr += contentDocId + ',';
            }

            deleteStr = deleteStr.substringBeforeLast(',');

            req.setMethod('DELETE');
            req.setEndpoint(baseUrl + deleteStr);
            res = http.send(req);
        }
        if (docId != null) {
        delete [SELECT Id FROM VTD1_Document__c WHERE Id = :docId];
        }
        return getDocs(recordId);
    }

	@AuraEnabled
	public static void finishSession(List<VTD1_Document__c> docs) {
		for (VTD1_Document__c item : docs) {
			item.VTD1_Uploading_Not_Finished__c = false;
			item.VTD1_Files_have_not_been_edited__c = true;
		}
		update docs;
	}
}