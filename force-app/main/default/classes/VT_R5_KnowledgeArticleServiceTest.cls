/**
 * Created by Yuliya Yakushenkova on 7/16/2020.
 */

@IsTest
private class VT_R5_KnowledgeArticleServiceTest {

    static List<String> approvedLanguages = new List<String>{'en_US', 'de', 'fr', 'es'};

    @IsTest
    static void getArticle() {
        String studyId = getStudyIdAndCreatArticles();

        VT_R5_KnowledgeArticleService.KnowledgeArticles article;
        Test.startTest();
        article = new VT_R5_KnowledgeArticleService('es', studyId)
                .getArticle(null, VT_R5_KnowledgeArticleService.PRIVACY_NOTICE_ARTICLE_TYPE);
        Test.stopTest();
        System.assert(article != null);
    }

    @IsTest
    static void getArticleById() {
        String studyId = getStudyIdAndCreatArticles();

        VT_R5_KnowledgeArticleService.KnowledgeArticles article;
        Test.startTest();
        article = new VT_R5_KnowledgeArticleService('es', studyId)
                .getArticle(getArticleId(studyId), VT_R5_KnowledgeArticleService.PRIVACY_NOTICE_ARTICLE_TYPE);
        Test.stopTest();
        System.assert(article != null);
    }

    @IsTest
    static void getArticlesPreview() {
        String studyId = getStudyIdAndCreatArticles();

        List<VT_R5_KnowledgeArticleService.KnowledgeArticles> articles;
        Test.startTest();
        articles = new VT_R5_KnowledgeArticleService('es', studyId)
                .getArticlesPreview(
                        new List<String>{
                                VT_R5_KnowledgeArticleService.PRIVACY_POLICY_ARTICLE_TYPE,
                                VT_R5_KnowledgeArticleService.EULA_ARTICLE_TYPE
                        }
                );
        Test.stopTest();
        System.debug(JSON.serializePretty(articles));
        System.assert(articles.size() > 0);
    }

    private static String getStudyIdAndCreatArticles() {
        HealthCloudGA__CarePlanTemplate__c study = (HealthCloudGA__CarePlanTemplate__c) new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'))
                .persist();
        String studyId = study.Id;

        List<Knowledge__kav> articles =  new List<Knowledge__kav>{
                new Knowledge__kav(
                        Title = 'Knowledge Title Spain',
                        UrlName = 'UrlName',
                        VTD2_Type__c = 'Privacy_Policy',
                        Summary = 'Some summary',
                        VTD1_Content__c = 'ARTICLE CONTENT',
                        VTD2_Study__c = studyId,
                        Language = 'es'
                ),
                new Knowledge__kav(
                        Title = 'Knowledge Title English',
                        UrlName = 'UrlName',
                        VTD2_Type__c = 'Privacy_Policy',
                        Summary = 'Some summary',
                        VTD1_Content__c = 'ARTICLE CONTENT',
                        VTD2_Study__c = studyId,
                        Language = 'en_US'
                ),
                new Knowledge__kav(
                        Title = 'EULA Title English',
                        UrlName = 'UrlName',
                        VTD2_Type__c = 'EULA',
                        Summary = 'Some summary',
                        VTD1_Content__c = 'ARTICLE CONTENT',
                        VTD2_Study__c = studyId,
                        Language = 'en_US'
                ),
                new Knowledge__kav(
                        Title = 'EULA Title Spain',
                        UrlName = 'UrlName',
                        VTD2_Type__c = 'EULA',
                        Summary = 'Some summary',
                        VTD1_Content__c = 'ARTICLE CONTENT',
                        VTD2_Study__c = studyId,
                        Language = 'es'
                )};
        new QueryBuilder()
                .buildStub()
                .addStubToList(articles)
                .namedQueryStub('VT_R5_KnowledgeArticleService.getKnowledgeData')
                .applyStub();
        return studyId;
    }

    private static String getArticleId(String studyId) {
        List<Knowledge__kav> articles = new List<Knowledge__kav>{
                new Knowledge__kav(
                        Title = 'Knowledge Title English',
                        UrlName = 'UrlName',
                        VTD2_Type__c = 'Privacy_Notice',
                        Summary = 'Some summary',
                        VTD1_Content__c = 'ARTICLE CONTENT',
                        VTD2_Study__c = studyId,
                        Language = 'en_US'
                )
        };
        insert articles;
        articles = [SELECT KnowledgeArticleId FROM Knowledge__kav WHERE Id IN :articles];
        List<User> toPublish = [
                SELECT Id FROM User
                WHERE UserPermissionsKnowledgeUser = TRUE
                AND IsActive = TRUE AND Profile.Name = 'System Administrator'
        ];
        System.runAs(toPublish[0]) {
            for (Knowledge__kav item : articles) {
                KbManagement.PublishingService.publishArticle(item.KnowledgeArticleId, true);
            }
        }
        return articles.get(0).Id;
    }
}