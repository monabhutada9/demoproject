/**
 * Created by Aliaksei Yamayeu on 03.11.2020.
 */
// SH-19506
@IsTest
private class VT_R5_eDiaryCreatorBatchTest {

    @IsTest
    private static void testEDiadrySenderBatch() {
        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor');
        Account account = (Account) patientAccount.persist();

        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(patientAccount)
                .setVTD1_SC_Tasks_Queue_Id(UserInfo.getUserId());

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
                .setFirstName('Patient')
                .setLastName('Patient')
                .setAccountId(account.Id);

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        patientUser.persist();

        Case patientCase = (Case) new DomainObjects.Case_t()
                .addPIUser(new DomainObjects.User_t())
                .addVTD1_Patient_User(patientUser)
                .addContact(patientContact)
                .setRecordTypeByName('CarePlan')
                .setAccountId(account.Id)
                .addStudy(study)
                .setStatus('Pre-Consent')
                .persist();

        Case patientCase2 = (Case) new DomainObjects.Case_t()
                .addPIUser(new DomainObjects.User_t())
                .addVTD1_Patient_User(patientUser)
                .addContact(patientContact)
                .setRecordTypeByName('CarePlan')
                .setAccountId(account.Id)
                .addStudy(study)
                .setStatus(VT_R5_eDiaryCreatorBatch.ACTIVE_RANDOMIZED_STATUS)
                .persist();

        Case patientCase3 = (Case) new DomainObjects.Case_t()
                .addPIUser(new DomainObjects.User_t())
                .addVTD1_Patient_User(patientUser)
                .addContact(patientContact)
                .setRecordTypeByName('CarePlan')
                .setAccountId(account.Id)
                .setStatus(VT_R5_eDiaryCreatorBatch.ACTIVE_RANDOMIZED_STATUS)
                .persist();

        DomainObjects.VTD1_Protocol_ePRO_t protocolEPROT = new DomainObjects.VTD1_Protocol_ePRO_t()
                .setRecordTypeByName('SatisfactionSurvey')
                .addVTD1_Study(study)
                .setVTD1_Type('Non-PSC')
                .setVTR2_Protocol_Reviewer('Patient Guide')
                .setVTD1_Subject('Patient')
                .setVTD1_Response_Window(10);
        DomainObjects.VTD1_Survey_t survey = new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(patientCase.Id)
                .addVTD1_Protocol_ePRO(protocolEPROT);
        DomainObjects.VTD1_Survey_Answer_t answer = new DomainObjects.VTD1_Survey_Answer_t()
                .addVTD1_Question(new DomainObjects.VTD1_Protocol_ePro_Question_t()
                        .addVTD1_Protocol_ePRO(new DomainObjects.VTD1_Protocol_ePRO_t()
                                .setRecordTypeByName('SatisfactionSurvey')
                                .addVTD1_Study(study)
                                .setVTD1_Type('PSC')
                                .setVTR2_Protocol_Reviewer('Patient Guide')
                                .setVTD1_Subject('Patient')
                                .setVTD1_Response_Window(10)))
                .addVTD1_Survey(survey);
        answer.persist();

        System.assertEquals(1, [SELECT Id FROM VTD1_Survey__c].size());

        VTD1_Protocol_ePRO__c pEDiary = [SELECT VTR4_EventType__c, VTR4_PatientMilestone__c FROM VTD1_Protocol_ePRO__c LIMIT 1];

        Test.startTest();
        VT_R5_eDiaryCreatorBatch eDiaryCreatorBatch = new VT_R5_eDiaryCreatorBatch(new List<Id>{pEDiary.Id});
        String cronStr = '0 0 * * * ?';
        System.schedule('Protocol eDiary Creator Batch Test', cronStr, eDiaryCreatorBatch);
        eDiaryCreatorBatch.execute(null);
        Test.stopTest();

        List<VTD1_Survey__c> eDiaries = [SELECT VTD1_CSM__c FROM VTD1_Survey__c ORDER BY CreatedDate DESC];
        System.assertEquals(patientCase2.Id, eDiaries.get(0).VTD1_CSM__c);
        System.assertEquals(2, eDiaries.size());
    }
}