/**
 * Created by user on 21-Jun-19.
 */

@IsTest
private class VT_D1_CandidatePatientProcessHandlerTest {
    @testSetup
    static void setup() {
        VT_R3_GlobalSharing.disableForTest = true;
        VT_R5_ShareGroupMembershipService.disableMembership = true;
        /*VTD1_RTId__c settings = new VTD1_RTId__c();
        settings.VTD1_Protocol_Payment_Compensation__c = VTD1_Protocol_Payment__c.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('VTD1_Compensation').getRecordTypeId();
        settings.VTD1_Protocol_Payment_Reimbursement__c = VTD1_Protocol_Payment__c.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('VTD1_Reimbursement').getRecordTypeId();
        insert settings;*/
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VTD1_Chat__c chat = new VTD1_Chat__c(VTD1_Chat_Type__c = 'Broadcast');
        insert chat;
        study.VTD1_Patient_Group_Id__c = chat.Id;
        update study;
        VTD2_Study_Geography__c geography = new VTD2_Study_Geography__c(Name = 'Test', VTD2_Study__c = study.Id, VTR3_Country__c = 'US', VTD2_Geographical_Region__c = 'Country', VT_R3_Date_Of_Birth_Restriction__c = 'Year Only');
        insert geography;
        List<VTD1_Protocol_Payment__c> protocolPayments = new List<VTD1_Protocol_Payment__c>();
        protocolPayments.add(new VTD1_Protocol_Payment__c(VTD1_Study__c = study.Id, VTD2_Study_Geography__c = geography.Id, VTD1_Currency__c = 'USD', VTD1_Amount__c = 100, VTD1_Completion_Type__c = 'Manual', RecordTypeId = VTD1_Protocol_Payment__c.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('VTD1_Compensation').getRecordTypeId()));
        protocolPayments.add(new VTD1_Protocol_Payment__c(VTD1_Study__c = study.Id, VTD2_Study_Geography__c = geography.Id, VTD1_Currency__c = 'USD', VTD1_Amount__c = 100, VTD1_Completion_Type__c = 'Manual', RecordTypeId = VTD1_Protocol_Payment__c.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('VTD1_Reimbursement').getRecordTypeId()));
        insert protocolPayments;
        List <VTD1_Study_Stratification__c> stratifications = new List<VTD1_Study_Stratification__c>();
        stratifications.add(new VTD1_Study_Stratification__c(Name = 'test', VTD1_Study__c = study.Id));
        insert stratifications;
        Test.startTest();
        //VT_D1_TestUtils.createTestPatientCandidate(1);
        HealthCloudGA__CandidatePatient__c candidatePatient = new HealthCloudGA__CandidatePatient__c();
        candidatePatient.rr_firstName__c = 'First';
        candidatePatient.rr_lastName__c = 'Last';
        candidatePatient.rr_Username__c = 'test@test.com';
        candidatePatient.rr_Email__c = 'test@test.com';
        candidatePatient.rr_Alias__c = 'al_test';
        candidatePatient.Conversion__c = 'Converted';
        candidatePatient.Study__c = study.Id;
        candidatePatient.VTD1_ProtocolNumber__c = 'Prot-001';
        candidatePatient.VTD1_Patient_Phone__c = '01234567';
        candidatePatient.HealthCloudGA__Address1Country__c = 'US';
        candidatePatient.HealthCloudGA__Address1PostalCode__c = 'NY';
        candidatePatient.HealthCloudGA__Address1State__c = 'NY';
        candidatePatient.HealthCloudGA__SourceSystemId__c = candidatePatient.rr_Username__c;
        candidatePatient.VTD2_Language__c = 'en_US';
        candidatePatient.VTR2_Caregiver_Email__c = 'test_cg@test.com';
        candidatePatient.VTR2_Caregiver_First_Name__c = 'First CG';
        candidatePatient.VTR2_Caregiver_Last_Name__c = 'Last CG';
        candidatePatient.VTR2_Caregiver_Phone__c = '234567';
        candidatePatient.VTR2_Caregiver_Preferred_Language__c = 'en_US';
        candidatePatient.healthcloudga__BirthDate__c = System.today().addYears(-30);
        System.enqueueJob(new VT_D1_TestUtils.CPCreator(candidatePatient));
        Test.stopTest();
    }

    /*

    @IsTest
    static void testRetry() {
        HealthCloudGA__CandidatePatient__c candidatePatient = [select Id, Conversion__c, VTR4_ConversionRetryTriggered__c, VTR4_TriggerVisits__c from HealthCloudGA__CandidatePatient__c candidatePatient where Conversion__c = 'Converted'];
        candidatePatient.Conversion__c = 'Draft';
        candidatePatient.VTR4_StatusCreate__c = 'Failed';
        candidatePatient.VTR4_TriggerCreate__c = true;
        update candidatePatient;
        VT_R4_PatientConversionHelper.retryConversionBlocks(new List<HealthCloudGA__CandidatePatient__c>{candidatePatient}, null);
    }
    */
    @IsTest
    static void forceErrors() {



        //Case cas = [select Id from Case limit 1];



        List <VTD2_Study_Geography__c> geographies = [select Id from VTD2_Study_Geography__c];
        delete geographies;
        HealthCloudGA__CarePlanTemplate__c study = [select Id from HealthCloudGA__CarePlanTemplate__c];
        Test.startTest();
        HealthCloudGA__CandidatePatient__c candidatePatient = new HealthCloudGA__CandidatePatient__c();
        candidatePatient.rr_firstName__c = 'First1';
        candidatePatient.rr_lastName__c = 'Last1';
        candidatePatient.rr_Username__c = 'test1@test.com';
        candidatePatient.rr_Email__c = 'test1@test.com';
        candidatePatient.rr_Alias__c = 'al_test';
        candidatePatient.Conversion__c = 'Converted';
        candidatePatient.Study__c = study.Id;
        candidatePatient.VTD1_ProtocolNumber__c = 'Prot-001';
        candidatePatient.VTD1_Patient_Phone__c = '01234567';
        candidatePatient.HealthCloudGA__Address1Country__c = 'US';
        candidatePatient.HealthCloudGA__Address1PostalCode__c = 'NY';
        candidatePatient.HealthCloudGA__Address1State__c = 'NY';
        candidatePatient.HealthCloudGA__SourceSystemId__c = candidatePatient.rr_Username__c;
        candidatePatient.VTD2_Language__c = 'en_US';
        System.enqueueJob(new VT_D1_TestUtils.CPCreator(candidatePatient));
        Test.stopTest();
        Database.SaveResult result = Database.insert(study, false);
        VT_R4_PatientConversionHelper.logError(result, candidatePatient, 'VTR4_StatusCreate__c', null);
    }
    @IsTest
    static void testUserNameDuplicate() {
        HealthCloudGA__CarePlanTemplate__c study = [select Id from HealthCloudGA__CarePlanTemplate__c];
        Test.startTest();
        HealthCloudGA__CandidatePatient__c candidatePatient = new HealthCloudGA__CandidatePatient__c();
        candidatePatient.rr_firstName__c = 'First';
        candidatePatient.rr_lastName__c = 'Last';
        candidatePatient.rr_Username__c = 'test@test.com';
        candidatePatient.rr_Email__c = 'test1@test.com';
        candidatePatient.rr_Alias__c = 'al_test';
        candidatePatient.Conversion__c = 'Converted';
        candidatePatient.Study__c = study.Id;
        candidatePatient.VTD1_ProtocolNumber__c = 'Prot-001';
        candidatePatient.VTD1_Patient_Phone__c = '01234567';
        candidatePatient.HealthCloudGA__Address1Country__c = 'US';
        candidatePatient.HealthCloudGA__Address1PostalCode__c = 'NY';
        candidatePatient.HealthCloudGA__Address1State__c = 'NY';
        candidatePatient.HealthCloudGA__SourceSystemId__c = candidatePatient.rr_Username__c;
        candidatePatient.VTD2_Language__c = 'en_US';
        candidatePatient.VTR2_Caregiver_Email__c = 'test_cg@test.com';
        candidatePatient.VTR2_Caregiver_First_Name__c = 'First CG';
        candidatePatient.VTR2_Caregiver_Last_Name__c = 'Last CG';
        candidatePatient.VTR2_Caregiver_Phone__c = '234567';
        candidatePatient.VTR2_Caregiver_Preferred_Language__c = 'en_US';
        System.enqueueJob(new VT_D1_TestUtils.CPCreator(candidatePatient));
        Test.stopTest();
    }
    @IsTest
    static void testEmailDuplicate() {
        HealthCloudGA__CarePlanTemplate__c study = [select Id from HealthCloudGA__CarePlanTemplate__c];
        Test.startTest();
        HealthCloudGA__CandidatePatient__c candidatePatient = new HealthCloudGA__CandidatePatient__c();
        candidatePatient.rr_firstName__c = 'First';
        candidatePatient.rr_lastName__c = 'Last';
        candidatePatient.rr_Username__c = 'test@test.com';
        candidatePatient.rr_Email__c = 'test@test.com';
        candidatePatient.rr_Alias__c = 'al_test';
        candidatePatient.Conversion__c = 'Converted';
        candidatePatient.Study__c = study.Id;
        candidatePatient.VTD1_ProtocolNumber__c = 'Prot-001';
        candidatePatient.VTD1_Patient_Phone__c = '01234567';
        candidatePatient.HealthCloudGA__Address1Country__c = 'US';
        candidatePatient.HealthCloudGA__Address1PostalCode__c = 'NY';
        candidatePatient.HealthCloudGA__Address1State__c = 'NY';
        candidatePatient.HealthCloudGA__SourceSystemId__c = candidatePatient.rr_Username__c;
        candidatePatient.VTD2_Language__c = 'en_US';
        candidatePatient.VTR2_Caregiver_Email__c = 'test_cg@test.com';
        candidatePatient.VTR2_Caregiver_First_Name__c = 'First CG';
        candidatePatient.VTR2_Caregiver_Last_Name__c = 'Last CG';
        candidatePatient.VTR2_Caregiver_Phone__c = '234567';
        candidatePatient.VTR2_Caregiver_Preferred_Language__c = 'en_US';
        System.enqueueJob(new VT_D1_TestUtils.CPCreator(candidatePatient));
        Test.stopTest();
    }
    @IsTest
    static void setStudyByProtocolNameTest() {
        List<HealthCloudGA__CandidatePatient__c> candidatePatientList =  [
                SELECT  Id,
                        Name,
                        Study__c,
                        VTD1_ProtocolNumber__c
                FROM    HealthCloudGA__CandidatePatient__c
        ];
        candidatePatientList[0].Study__c = null;
        VT_D1_CandidatePatientProcessHandler.setStudyByProtocolName(candidatePatientList);

        System.debug([
                SELECT Id, Name
                FROM HealthCloudGA__CarePlanTemplate__c ]);
    }
    @IsTest
    static void setStudyByProtocolNameWithVirtualSiteTest() {
        HealthCloudGA__CarePlanTemplate__c study = [
            SELECT Name
            FROM HealthCloudGA__CarePlanTemplate__c
            WHERE Name = 'TestProt-001'
            LIMIT 1
        ];

        Study_Team_Member__c PI = new Study_Team_Member__c(
            Study__c = study.Id,
            User__c = UserInfo.getUserId(),
            VTD1_PIsAssignedPG__c = UserInfo.getUserId(),
            VTD1_Active__c = true,
            VTD1_Ready_For_Assignment__c = true,
            RecordTypeId = VT_R4_ConstantsHelper_ProfilesSTM.RECORD_TYPE_ID_STUDY_TEAM_MEMBER_PI
        );
        insert PI;

        Virtual_Site__c virtualSiteUSA = new Virtual_Site__c(
            VTD1_Study__c = study.Id,
            Name = 'REG_TEST',
            VTD1_Study_Site_Number__c = '2',
            VTD1_Study_Team_Member__c = PI.Id,
            VTD1_PI_User__c = UserInfo.getUserId(),
            VTD1_Country_Code__c = 'US'
        );
        Virtual_Site__c virtualSiteCanada = new Virtual_Site__c(
            VTD1_Study__c = study.Id,
            Name = 'REG_TEST',
            VTD1_Study_Site_Number__c = '2',
            VTD1_Study_Team_Member__c = PI.Id,
            VTD1_PI_User__c = UserInfo.getUserId(),
            VTD1_Country_Code__c = 'CA'
        );
        insert new List<Virtual_Site__c>{virtualSiteUSA, virtualSiteCanada};

        HealthCloudGA__CandidatePatient__c candidatePatient =  [
            SELECT VTD1_ProtocolNumber__c, VT_R4_VirtualSite__c, VTR2_Pi_Id__c
            FROM HealthCloudGA__CandidatePatient__c
            LIMIT 1
        ];
        candidatePatient.VTD1_ProtocolNumber__c = study.Name;
        candidatePatient.VTR5_Study_Site_Number__c = '2';

        HealthCloudGA__CandidatePatient__c candidateUSA = candidatePatient.clone();
        candidateUSA.VTR5_Country_Code__c = 'US';

        System.Test.startTest();

        VT_D1_CandidatePatientProcessHandler.setStudyByProtocolName(new List<HealthCloudGA__CandidatePatient__c>{candidateUSA, candidatePatient});

        System.Test.stopTest();

        System.assertEquals(virtualSiteUSA.Id, candidateUSA.VT_R4_VirtualSite__c);
        System.assertEquals(UserInfo.getUserId(), candidateUSA.VTR2_Pi_Id__c);
        System.assertNotEquals(null, candidatePatient.VT_R4_VirtualSite__c);
        System.assertNotEquals(null, candidatePatient.VTR2_Pi_Id__c);
    }

    @IsTest
    static  void getAliasTest(){
        String firstName = 'Test firstName';
        String lastName = 'Test lastName';
        VT_D1_CandidatePatientProcessHandler.getAlias(firstName,lastName);
    }
    @IsTest
    static  void testCreateCG(){
        Test.startTest();
        List<HealthCloudGA__CandidatePatient__c> candidatePatientList =  [
                SELECT  Id,
                        Name,
                        Study__c,
                        VTD1_ProtocolNumber__c
                FROM    HealthCloudGA__CandidatePatient__c
        ];
        HealthCloudGA__CandidatePatient__c cand = candidatePatientList[0];
        cand.VTR2_CreateCaregiver__c = true;
        cand.VTR2_Caregiver_First_Name__c = 'OT';
        cand.VTR2_Caregiver_Last_Name__c = '1';
        cand.VTR2_Caregiver_Email__c = 'ot1@test.com';
        cand.VTR2_Caregiver_Phone__c = '2020327';
        cand.VTR2_Caregiver_Preferred_Language__c = 'en_US';
        update cand;
        Test.stopTest();
    }
    @IsTest
    static  void testCreate2CG(){
        Test.startTest();
        List<HealthCloudGA__CandidatePatient__c> candidatePatientList =  [
                SELECT  Id,
                        Name,
                        HealthCloudGA__AccountId__c,
                        Study__c,
                        VTD1_ProtocolNumber__c
                FROM    HealthCloudGA__CandidatePatient__c
        ];
        HealthCloudGA__CandidatePatient__c cand = candidatePatientList[0];
        cand.VTR2_CreateCaregiver__c = true;
        cand.VTR2_Caregiver_Last_Name__c = '1';
        cand.VTR2_Caregiver_Email__c = 'ot1@test.com';
        cand.VTR2_Caregiver_Phone__c = '2020327';
        cand.VTR2_Caregiver_Preferred_Language__c = 'en_US';
        update cand;
        Test.stopTest();
    }
    @IsTest
    static void validateRescueStudySTMTest() {

        DomainObjects.Study_t study = new DomainObjects.Study_t().setName('Study Name').setRescueStudy(TRUE).addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor')); study.persist();
        DomainObjects.User_t PIUSER = new DomainObjects.User_t().setProfile('Primary Investigator').setEmail('test@email.com').setUsername('test15052020_1@email.com').addContact(new DomainObjects.Contact_t()); PIUSER.persist();
        DomainObjects.StudyTeamMember_t stm1 = new DomainObjects.StudyTeamMember_t().setRecordTypeByName('PI').addUser(PIUSER).addStudy(study);  stm1.persist();
        DomainObjects.StudyTeamMember_t stm2 = new DomainObjects.StudyTeamMember_t().setRecordTypeByName('PI').addUser(PIUSER).addStudy(study);  stm2.persist();


        List<HealthCloudGA__CandidatePatient__c> cplist = new List<HealthCloudGA__CandidatePatient__c>();
        HealthCloudGA__CandidatePatient__c candidatePatient = new HealthCloudGA__CandidatePatient__c();
        candidatePatient.rr_firstName__c = 'firstName';
        candidatePatient.rr_lastName__c = 'lastName';
        candidatePatient.rr_Username__c =VT_D1_TestUtils.generateUniqueUserName();
        candidatePatient.rr_Email__c = candidatePatient.rr_Username__c;
        candidatePatient.rr_Alias__c = 'alias';
        candidatePatient.Conversion__c = 'Draft';
        candidatePatient.Study__c = study.id;
        candidatePatient.VTD1_ProtocolNumber__c = 'Study Name';
        candidatePatient.HealthCloudGA__Address1Country__c = 'US';
        candidatePatient.HealthCloudGA__Address1PostalCode__c = 'state';
        candidatePatient.HealthCloudGA__Address1State__c = 'state';
        candidatePatient.VTD2_Language__c = 'en_US';
        candidatePatient.VTR2_Pi_Id__c = PIUSER.id;
        cplist.add(candidatePatient);
        Test.startTest();

        VT_R3_GlobalSharing.disableForTest = true;
        System.enqueueJob(new VT_D1_TestUtils.CPCreator(candidatePatient));
        /*Database.SaveResult[] result = Database.insert(cplist, false);
        for(database.SaveResult err : result) {
            for(database.Error msg : err.getErrors()){
                System.debug('#####123'+msg.getMessage());
                System.assertEquals('This PI is associated with more than one virtual site please add the virtual site',msg.getMessage());

            }
        }*/


        Test.stopTest();

    }


    @IsTest
    static void oneKeyFieldFlowTest() {
        Test.startTest();
        Study_Team_Member__c piTeamMember = [SELECT User__c FROM Study_Team_Member__c WHERE VTD1_Type__c = 'Primary Investigator' AND VTD1_Backup_PI__c = FALSE LIMIT 1];
        User piUser = [SELECT Id, People_Soft_ID__c FROM User WHERE Id = :piTeamMember.User__c];
        piUser.People_Soft_ID__c = '123';
        VT_R5_eCoaUserUpdateHelper.processedUserIds.add(piUser.Id);
        update piUser;
        System.debug('--- piUser ' +piUser);
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];
        HealthCloudGA__CandidatePatient__c candidatePatient = new HealthCloudGA__CandidatePatient__c();
        candidatePatient.rr_firstName__c = 'firstName';
        candidatePatient.rr_lastName__c = 'lastName';
        candidatePatient.rr_Username__c = VT_D1_TestUtils.generateUniqueUserName();
        candidatePatient.rr_Email__c = candidatePatient.rr_Username__c;
        candidatePatient.rr_Alias__c = 'alias';
        candidatePatient.Conversion__c = 'Draft';
        candidatePatient.Study__c = study.Id;
        candidatePatient.VTD1_ProtocolNumber__c = 'Prot-001';
        candidatePatient.VTD1_Patient_Phone__c = String.valueOf(VT_D1_TestUtils.getRandomInteger());
        candidatePatient.HealthCloudGA__Address1Country__c = 'US';
        candidatePatient.HealthCloudGA__Address1PostalCode__c = 'state';
        candidatePatient.HealthCloudGA__Address1State__c = 'state';
        candidatePatient.HealthCloudGA__SourceSystemId__c = candidatePatient.rr_Username__c;
        candidatePatient.VTD2_Language__c = 'en_US';
        candidatePatient.VTD1_One_Key_ID__c = '123';
        System.enqueueJob(new VT_D1_TestUtils.CPCreator(candidatePatient));
        Test.stopTest();
    }

    @IsTest
    static void PIFlowTest() {
        Study_Team_Member__c piTeamMember = [SELECT Id, User__c FROM Study_Team_Member__c WHERE VTD1_Type__c = 'Primary Investigator' AND VTD1_Backup_PI__c = false LIMIT 1];
        User piUser = [SELECT Id, People_Soft_ID__c FROM User WHERE Id = :piTeamMember.User__c];
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];
        HealthCloudGA__CandidatePatient__c candidatePatient = new HealthCloudGA__CandidatePatient__c();
        candidatePatient.rr_firstName__c = 'secondName';
        candidatePatient.rr_lastName__c = 'lastName';
        candidatePatient.rr_Username__c = VT_D1_TestUtils.generateUniqueUserName();
        candidatePatient.rr_Email__c = candidatePatient.rr_Username__c;
        candidatePatient.rr_Alias__c = 'alias';
        candidatePatient.Conversion__c = 'Draft';
        candidatePatient.Study__c = study.Id;
        candidatePatient.VTD1_ProtocolNumber__c = 'Prot-001';
        candidatePatient.VTD1_Patient_Phone__c = String.valueOf(VT_D1_TestUtils.getRandomInteger());
        candidatePatient.HealthCloudGA__Address1Country__c = 'US';
        candidatePatient.HealthCloudGA__Address1PostalCode__c = 'state';
        candidatePatient.HealthCloudGA__Address1State__c = 'state';
        candidatePatient.HealthCloudGA__SourceSystemId__c = candidatePatient.rr_Username__c;
        candidatePatient.VTD2_Language__c = 'en_US';
        candidatePatient.VTR2_Pi_Id__c = piUser.Id;
        Test.startTest();
        System.enqueueJob(new VT_D1_TestUtils.CPCreator(candidatePatient));
        Test.stopTest();
    }

    @IsTest

    static void testCPbatch() {
        HealthCloudGA__CandidatePatient__c cp = [SELECT Id, Conversion__c, VTR4_StatusCreate__c FROM HealthCloudGA__CandidatePatient__c LIMIT 1];
        cp.Conversion__c = 'Draft';
        cp.VTR4_StatusCreate__c = '';
        cp.VTR4_Convertation_Process_Status__c = 'Initial';
        update cp;
        VT_R5_ConversionBatch.launchBatch(true);

    }
}