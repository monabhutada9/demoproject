/**
 * Created by Maksim Fedarenka on 21-May-20.
 */

@IsTest
public with sharing class VT_R4_DocsAuditDataPopulationBatchTest {

	public static void testBehavior() {
		Test.startTest();
		Case aCase = [
				SELECT Id, VTD1_Virtual_Site__c, VTD1_Virtual_Site__r.VTD1_Study__c
				FROM Case
				WHERE Subject = 'VT_R5_allTest'
		];

		Map<String, VTD1_Document__c> documentNameDocumentMap = VT_R4_DocumentServiceTest.generateDocuments(aCase);
		for (VTD1_Document__c document : documentNameDocumentMap.values()) {
			document.VTR4_AuditStudy__c = null;
			document.VTR4_AuditSite__c = null;
		}
		update documentNameDocumentMap.values();


		VT_R4_DocsAuditDataPopulationBatch.run(200);
		Test.stopTest();

		VT_R4_DocumentServiceTest.assertResults(aCase, documentNameDocumentMap);
	}
}