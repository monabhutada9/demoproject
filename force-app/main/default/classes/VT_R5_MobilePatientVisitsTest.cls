/**
 * Created by Yuliya Yakushenkova on 10/6/2020.
 */

@IsTest
public class VT_R5_MobilePatientVisitsTest {

    private static final String URI = '/patient/v1/visit/';
    private static Id visitId;
    static {
        new VT_R5_MobilePatientVisits().getMinimumVersions();
    }

    public static void firstTest() {
        User usr = fillRestContexts();

        VT_R5_TestRestContext.doTest(
                usr, new VT_R5_TestRestContext.MockRestContext( URI + visitId, null)
        );
    }

    public static void secondTest() {
        User usr = fillRestContexts();

        VT_R5_TestRestContext.doTest(
                usr, new VT_R5_TestRestContext.MockRestContext( URI + visitId + '/timeslots', null)
        );
    }

    public static void thirdTest() {
        User usr = fillRestContexts();

        VT_R5_TestRestContext.doTest(
                usr, new VT_R5_TestRestContext.MockRestContext( URI + visitId + '/schedule', null)
        );
    }

    public static void forthTest() {
        User usr = fillRestContexts();

        RequestParams requestParams = new RequestParams();
        requestParams.slots = '';

        VT_R5_TestRestContext.doTest(
                usr, new VT_R5_TestRestContext.MockRestContext(
                        'POST', URI + visitId + '/schedule', requestParams
                )
        );
    }

    public static void sixthTest() {
        User usr = fillRestContexts();

        RequestParams requestParams = new RequestParams();
        requestParams.reason = 'reason for cancel';

        VT_R5_TestRestContext.doTest(
                usr, new VT_R5_TestRestContext.MockRestContext(
                        'POST', URI + visitId + '/cancel', requestParams
                )
        );
    }

    public static void fifthTest() {
        User usr = fillRestContexts();

        RequestParams requestParams = new RequestParams();
        requestParams.fromDateTime = System.now().addDays(-5);
        requestParams.toDateTime = System.now().addDays(5);
        requestParams.reason = 'reason for reschedule';

        VT_R5_TestRestContext.doTest(
                usr, new VT_R5_TestRestContext.MockRestContext(
                        'POST', URI + visitId + '/reschedule', requestParams
                )
        );
    }

    public static User fillRestContexts() {
        User usr = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.PT_USERNAME LIMIT 1];
        Id caseId = VT_R5_PatientQueryService.getCaseByUserId(usr.Id).Id;
        visitId = VT_R5_PatientQueryService.getVisitsAndMembersByCaseId(caseId).get(0).Id;
        return usr;
    }

    private class RequestParams {
        private Datetime fromDateTime;
        private Datetime toDateTime;
        private String reason;
        private String slots;
    }
}