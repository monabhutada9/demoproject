/**
 * Created by user on 27.12.2019.
 */

global with sharing class VT_R4_GenerateActualVisitsAndKits {

    private static final Map<String, String> PROTOCOL_TO_ACTUAL_VISIT_TYPES = new Map<String, String>{
            VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_LABS => VT_R4_ConstantsHelper_VisitsEvents.RT_ACTUAL_VISIT_LABS,
            VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_STUDY_TEAM => VT_R4_ConstantsHelper_VisitsEvents.RT_ACTUAL_VISIT_STUDY_TEAM,
            VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_HCP => VT_R4_ConstantsHelper_VisitsEvents.RT_ACTUAL_VISIT_HCP
    };

    private static final Map<String, String> PROTOCOL_TO_PATIENT_KIT_TYPES = new Map<String, String>{
            VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_AD_HOC_LAB => VT_R4_ConstantsHelper_KitsOrders.RT_PATIENT_KIT_AD_HOC_LAB,
            VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_CONNECTED_DEVICES => VT_R4_ConstantsHelper_KitsOrders.RT_PATIENT_KIT_CONNECTED_DEVICES,
            VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_IMP => VT_R4_ConstantsHelper_KitsOrders.RT_PATIENT_KIT_IMP,
            VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_LAB => VT_R4_ConstantsHelper_KitsOrders.RT_PATIENT_KIT_LAB,
            VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_STUDY_HUB_TABLET => VT_R4_ConstantsHelper_KitsOrders.RT_PATIENT_KIT_STUDY_HUB_TABLET,
            VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_WELCOME_TO_STUDY_PACKAGE => VT_R4_ConstantsHelper_KitsOrders.RT_PATIENT_KIT_WELCOME_TO_STUDY_PACKAGE
    };
    String RECORD_TYPE_ID_ORDER_PACKING_MATERIALS = Schema.SObjectType.VTD1_Order__c.getRecordTypeInfosByDeveloperName().get('VTD1_Regular_Kit_Delivery').getRecordTypeId();
    private static final String DEFAULT_MODALITY = 'At Home';
    public List <Case> patientCases;
    private Map <Id, HealthCloudGA__CarePlanTemplate__c> studyMapInfo;
    private Map <Id, List <String>> returnNumbersIMPMap = new Map<Id, List <String>>();
    private Map <Id, List <String>> returnNumbersLabMap = new Map<Id, List <String>>();
    private Map <Id, List <Case>> studyToCasesMap = new Map<Id, List <Case>>();
    private Map <Id, Case> casesMap = new Map<Id, Case>();
    private Map<Id, VTD1_Patient_Device__c> newDevicesByProtocolId = new Map<Id, VTD1_Patient_Device__c>();

    List<VTD1_Order__c> newDeliveries = new List<VTD1_Order__c>();
    private Map<Id, VTD1_Patient_Kit__c> newKitsByProtocolId = new Map<Id, VTD1_Patient_Kit__c>();
    private List<Patient_LKC__c> newLKCs = new List<Patient_LKC__c>();
    private List<VTD1_Patient_Accessories__c> newAccessories = new List<VTD1_Patient_Accessories__c>();
    private List<VTD1_Patient_Lab_Sample__c> newSamples = new List<VTD1_Patient_Lab_Sample__c>();

    private List<VTD1_Order__c> childDeliveries = new List<VTD1_Order__c>();
    private List<VTD1_Order__c> childImpLabDeliveries = new List<VTD1_Order__c>();
    private List<VTD1_Patient_Kit__c> packMatKits = new List<VTD1_Patient_Kit__c>();

    private String statusVisits = VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_FINISHED;


    public void executeDeliveriesAndKits(List<Id> caseIds) {
        prepareData_DeliveriesAndKits(caseIds);
        createDeliveries();
    }

    public void execute(List<Id> caseIds) {
        prepareData (caseIds);
        createCaseRelatedObjects(caseIds);
    }

    public void prepareData (List<Id> caseIds) {
        this.patientCases = [
                SELECT Id, ContactId, Preferred_Lab_Visit__c, VTD1_Study__c, VTD1_Study__r.VTD1_IMP_Return_Numbers__c,
                        VTD1_Study__r.VTD1_Lab_Return_Numbers__c,
                        VTD1_Study__r.VTD1_Return_Process__c, VTD1_Subject_ID__c,
                        VTD1_Study__r.VTD1_Lab_Return_Process__c,
                        VTD1_PI_user__c,
                        VTD1_Primary_PG__c,
                        VTD1_Patient_User__c,
                        VTR2_SiteCoordinator__c,
                        VTD1_Virtual_Site__c,
                        VTR4_CandidatePatient__c
                FROM Case
                WHERE Id IN :caseIds
        ];
        for (Case patientCase : patientCases) {
            casesMap.put(patientCase.Id, patientCase);
            List <Case> cases = studyToCasesMap.get(patientCase.VTD1_Study__c);
            if (cases == null) {
                cases = new List<Case>();
                studyToCasesMap.put(patientCase.VTD1_Study__c, cases);
            }
            cases.add(patientCase);
        }

        studyMapInfo = new Map<Id, HealthCloudGA__CarePlanTemplate__c>(
        [SELECT Id,
                // visits
            (SELECT Id, Name, VTD1_VisitType__c, VTD1_EDC_Name__c, VTD1_PreVisitInstructions__c, VTR2_SH_TelevisitNeeded__c,
                    VTD1_Independent_Rater_Flag__c, VTD1_Patient_Visit_Checklist__c, VTD1_Visit_Checklist__c, VTR2_Modality__c,
                    RecordTypeId, VTD1_VisitNumber__c, VTD1_Protocol_Amendment__c, VTR4_Original_Version__c, VTR4_Remove_from_Protocol__c
            FROM Visits2__r
            WHERE  VTD1_VisitType__c IN ('Labs', 'Health Care Professional (HCP)', 'Study Team')
            AND RecordType.DeveloperName != 'VTR5_Subgrouptimelinevisit'),
                    // Protocol Amendments
            (SELECT Id
            FROM Protocol_Amendments__r)
        FROM HealthCloudGA__CarePlanTemplate__c
        WHERE Id in : studyToCasesMap.keySet()]);
    }

    public void prepareData_DeliveriesAndKits (List<Id> caseIds) {
        this.patientCases = [
                SELECT Id, ContactId, Preferred_Lab_Visit__c, VTD1_Study__c, VTD1_Study__r.VTD1_IMP_Return_Numbers__c,
                        VTD1_Study__r.VTD1_Lab_Return_Numbers__c,
                        VTD1_Study__r.VTD1_Return_Process__c, VTD1_Subject_ID__c,
                        VTD1_Study__r.VTD1_Lab_Return_Process__c,
                        VTD1_PI_user__c,
                        VTD1_Primary_PG__c,
                        VTD1_Patient_User__c,
                        VTR2_SiteCoordinator__c,
                        VTD1_Virtual_Site__c,
                        VTR4_CandidatePatient__c
                FROM Case
                WHERE Id IN :caseIds
        ];
        for (Case patientCase : patientCases) {
            if (patientCase.VTD1_Study__r.VTD1_IMP_Return_Numbers__c != null) {
                returnNumbersIMPMap.put(patientCase.Id, patientCase.VTD1_Study__r.VTD1_IMP_Return_Numbers__c.replace(' ', '').split(','));
            }
            if (patientCase.VTD1_Study__r.VTD1_Lab_Return_Numbers__c != null) {
                returnNumbersLabMap.put(patientCase.Id, patientCase.VTD1_Study__r.VTD1_Lab_Return_Numbers__c.replace(' ', '').split(','));
            }
            casesMap.put(patientCase.Id, patientCase);
            List <Case> cases = studyToCasesMap.get(patientCase.VTD1_Study__c);
            if (cases == null) {
                cases = new List<Case>();
                studyToCasesMap.put(patientCase.VTD1_Study__c, cases);
            }
            cases.add(patientCase);
        }
        studyMapInfo = new Map<Id, HealthCloudGA__CarePlanTemplate__c>(
        [SELECT Id,
                // deliveries
            (SELECT Id,
                    Name,
                    VTR4_Original_Version__c,
                    VTR4_Protocol_Amendment__c,
                    VTR4_Remove_from_Protocol__c
            FROM Protocol_Deliveries__r),
                    // kits
            (SELECT Id, Name, VTD1_Protocol_Delivery__c, RecordType.DeveloperName, VTD1_HHN_Phleb_Visit_Activity_Checklist__c,
                    VTD1_PSC_Visit_Activity_Checklist__c, VTR2_Quantity__c,VTR2_Description__c, VTR2_TemperatureControlled__c
            FROM Protocol_Kits__r),
                    // devices
            (SELECT Id, VTD1_Protocol_Kit__c, VTD1_Manufacturer__c, VTD1_ModelName__c, VTD1_Device_Type__c,
                    VTD1_Device_Name__c, VTD1_Kit_Type__c
            FROM Protocol_Devices__r),
                    // LKC
            (SELECT Id, VTD1_Lab_Kit__c, VTD1_Content_Item_Description__c, VTD1_Content_Item_Type__c
            FROM Protocol_LKCs__r),
                    // Accessories
            (SELECT Id, VTD1_Protocol_Device__c, VTD1_Accessory_Qty__c, VTD1_Accessory_Description__c
            FROM Protocol_Accessories__r),
                    // LabSamples
            (SELECT Id, VTD1_Lab_Kit__c, VTD1_Care_Plan_Template__c
            FROM Protocol_Lab_Samples__r),
                    // LabSamples
            (SELECT Id
            FROM Protocol_Amendments__r)
        FROM HealthCloudGA__CarePlanTemplate__c
        WHERE Id in : studyToCasesMap.keySet()]);
    }

    private void createCaseRelatedObjects(List<Id> caseIds) {
        createActualVisits();
        try {
            // only by 1 case to prevent CPU limit exeed
            for (Id caseId : caseIds) {
                new VT_R5_CreateDeliveriesKits(new List <Id> {caseId}).enqueue();
            }
        } catch (Exception e) {

        }
    }

    private void createActualVisits() {
        Map<String, Schema.RecordTypeInfo> recTypeMap = Schema.SObjectType.VTD1_Actual_Visit__c.getRecordTypeInfosByDeveloperName();
        List<VTD1_Actual_Visit__c> aVisits = new List<VTD1_Actual_Visit__c>();
        List<VTD1_ProtocolVisit__c> allProtocolVisits = new List<VTD1_ProtocolVisit__c> ();
        Map<Id, List<VTD1_ProtocolVisit__c>> correctVersionsMap_siteId_pVisits = new Map<Id, List<VTD1_ProtocolVisit__c>>();
        for (HealthCloudGA__CarePlanTemplate__c study : studyMapInfo.values()) {
            if (!study.Protocol_Amendments__r.isEmpty()) {
                allProtocolVisits.addAll(study.Visits2__r);
            }
        }
        List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrappedRecords;
        VT_R4_ProtocolAmendmentVersionHelper paVersionHelper;
        if (!allProtocolVisits.isEmpty()) {
            wrappedRecords = wrapProtocolVisits(allProtocolVisits);
            paVersionHelper = new VT_R4_ProtocolAmendmentVersionHelper(wrappedRecords);
        }
        for (Case patientCase : patientCases) {
            if (patientCase.VTD1_Virtual_Site__c != null) {
                List<VTD1_ProtocolVisit__c> correctVersions = studyMapInfo.get(patientCase.VTD1_Study__c).Visits2__r;
                if (correctVersions.isEmpty()) continue;
                if (!studyMapInfo.get(patientCase.VTD1_Study__c).Protocol_Amendments__r.isEmpty()) {
                    correctVersions = correctVersionsMap_siteId_pVisits.get(patientCase.VTD1_Virtual_Site__c);
                    if (correctVersions == null) {
                        correctVersions = paVersionHelper.getCorrectVersions(patientCase.VTD1_Virtual_Site__c);
                        correctVersionsMap_siteId_pVisits.put(patientCase.VTD1_Virtual_Site__c, correctVersions);
                    }
                }
                if (!correctVersions.isEmpty()) {
                    for (VTD1_ProtocolVisit__c pVisit : correctVersions) {
                        VTD1_Actual_Visit__c newVisit = new VTD1_Actual_Visit__c(
                                VTR5_PatientCaseId__c = patientCase.Id,
                                Name = pVisit.VTD1_EDC_Name__c,
                                RecordTypeId = recTypeMap.get(PROTOCOL_TO_ACTUAL_VISIT_TYPES.get(pVisit.VTD1_VisitType__c)).getRecordTypeId(),
                                Sub_Type__c = pVisit.VTD1_VisitType__c == 'Labs' ? patientCase.Preferred_Lab_Visit__c : null,
                                VTD1_Case__c = patientCase.Id,
                                VTD1_Contact__c = patientCase.ContactId,
                                VTD1_Protocol_Visit__c = pVisit.Id,
                                VTD1_Pre_Visit_Instructions__c = pVisit.VTD1_PreVisitInstructions__c,
                                VTR2_Independent_Rater_Flag__c = pVisit.VTD1_Independent_Rater_Flag__c,
                                VTD1_Additional_Patient_Visit_Checklist__c = pVisit.VTD1_Patient_Visit_Checklist__c,
                                VTD1_Additional_Visit_Checklist__c = pVisit.VTD1_Visit_Checklist__c,
                                VTR2_Televisit__c = pVisit.VTR2_SH_TelevisitNeeded__c,
                                VTR2_Modality__c = pVisit.VTR2_Modality__c != null ? pVisit.VTR2_Modality__c : DEFAULT_MODALITY
                        );
                        aVisits.add(newVisit);
                    }
                }
            }
        }
        if (!aVisits.isEmpty()) {
            if (!aVisits.isEmpty()) {
                try {
                    insert aVisits;
                    VT_R2_MembersForVisitCreator.createMembers(aVisits);
                    List<HealthCloudGA__CandidatePatient__c> cps = new List<HealthCloudGA__CandidatePatient__c>();
                    for (Case patientCase : patientCases) {
                        cps.add(new HealthCloudGA__CandidatePatient__c(
                                Id = patientCase.VTR4_CandidatePatient__c, VTR4_StatusVisits__c = statusVisits
                        ));
                    }
                    update cps;
                } catch (Exception dbE) {
                    insert ((SObject) new VTR4_Conversion_Log__c(
                            VTR4_Error_Message__c = dbE.getMessage() + ' ' + dbE.getStackTraceString()));
                }
            }
        }
    }

    private List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrapProtocolDeliveries(List<VTD1_Protocol_Delivery__c> protocolDeliveries) {
        List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrappedRecords = new List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper>();
        for (VTD1_Protocol_Delivery__c delivery : protocolDeliveries) {
            wrappedRecords.add(new VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper(
                    delivery, delivery.VTR4_Original_Version__c, delivery.VTR4_Protocol_Amendment__c, delivery.VTR4_Remove_from_Protocol__c
            ));
        }
        return wrappedRecords;
    }

    private void createDeliveries() {
        List<VTD1_Protocol_Delivery__c> allProtocolDeliveries = new List<VTD1_Protocol_Delivery__c>();
        for (HealthCloudGA__CarePlanTemplate__c study : studyMapInfo.values()) {
            if (!study.Protocol_Deliveries__r.isEmpty()) {
                allProtocolDeliveries.addAll(study.Protocol_Deliveries__r);
            }
        }
        List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrappedRecords;
        VT_R4_ProtocolAmendmentVersionHelper paVersionHelper;
        if (!allProtocolDeliveries.isEmpty()) {
            wrappedRecords = wrapProtocolDeliveries(allProtocolDeliveries);
            paVersionHelper = new VT_R4_ProtocolAmendmentVersionHelper(wrappedRecords);
        }
        Map<Id, List<VTD1_Protocol_Delivery__c>> correctVersionsMap_siteId_pDelivs = new Map<Id, List<VTD1_Protocol_Delivery__c>>();
        for (Case patientCase : patientCases) {
            if (patientCase.VTD1_Virtual_Site__c != null) {
                List<VTD1_Protocol_Delivery__c> correctVersions = studyMapInfo.get(patientCase.VTD1_Study__c).Protocol_Deliveries__r;
                if (correctVersions.isEmpty()) continue;
                if (!studyMapInfo.get(patientCase.VTD1_Study__c).Protocol_Amendments__r.isEmpty()) {
                    correctVersions = correctVersionsMap_siteId_pDelivs.get(patientCase.VTD1_Virtual_Site__c);
                    if (correctVersions == null) {
                        correctVersions = paVersionHelper.getCorrectVersions(patientCase.VTD1_Virtual_Site__c);
                        correctVersionsMap_siteId_pDelivs.put(patientCase.VTD1_Virtual_Site__c, correctVersions);
                    }
                }
                for (VTD1_Protocol_Delivery__c pDel : correctVersions) {
                    this.newDeliveries.add(new VTD1_Order__c(
                            VTD1_Case__r = patientCase,
                            VTD1_Case__c = patientCase.Id,
                            VTD1_Protocol_Delivery__c = pDel.Id,
                            Name = pDel.Name,
                            VTD1_Status__c = 'Not Started',
                            RecordTypeId = RECORD_TYPE_ID_ORDER_PACKING_MATERIALS
                    ));
                }
            }
        }

        if (!newDeliveries.isEmpty()) {
            try {
                insert newDeliveries;
                createKits();
            } catch (Exception dbE) {
                insert ((SObject) new VTR4_Conversion_Log__c(
                        VTR4_Error_Message__c = dbE.getMessage() + ' ' + dbE.getStackTraceString()));
            }
        }
    }

    private void createKits() {
        Map<Id, VTD1_Protocol_Delivery__c> deliveryId_kits_map = new Map<Id, VTD1_Protocol_Delivery__c> ([
                SELECT
                        Id,
                (SELECT Id, Name, VTD1_Protocol_Delivery__c, RecordType.DeveloperName, VTD1_HHN_Phleb_Visit_Activity_Checklist__c,
                        VTD1_PSC_Visit_Activity_Checklist__c, VTR2_Quantity__c,VTR2_Description__c, VTR2_TemperatureControlled__c FROM Protocol_Kits__r)
                FROM VTD1_Protocol_Delivery__c
                WHERE Care_Plan_Template__c = :studyMapInfo.keySet()
        ]);

        Map<String, Schema.RecordTypeInfo> recTypeMap = Schema.SObjectType.VTD1_Patient_Kit__c.getRecordTypeInfosByDeveloperName();
        for (VTD1_Order__c patientDelivery : this.newDeliveries) {
            List<VTD1_Protocol_Kit__c> protocolKits = deliveryId_kits_map.get(patientDelivery.VTD1_Protocol_Delivery__c).Protocol_Kits__r;
            Case patientCase = patientDelivery.VTD1_Case__r;
            for (VTD1_Protocol_Kit__c pKit : protocolKits) {
                this.newKitsByProtocolId.put(pKit.Id, new VTD1_Patient_Kit__c(
                        Name = pKit.Name,
                        VTD1_Case__c = patientCase.Id,
                        VTD1_Patient_Delivery__c = patientDelivery.Id,
                        VTD1_Protocol_Kit__c = pKit.Id,
                        RecordTypeId = recTypeMap.get(PROTOCOL_TO_PATIENT_KIT_TYPES.get(pKit.RecordType.DeveloperName)).getRecordTypeId(),
                        VTD1_Status__c = 'Pending',
                        VTD1_HHN_Phleb_Visit_Activity_Checklist__c = pKit.VTD1_HHN_Phleb_Visit_Activity_Checklist__c,
                        VTD1_PSC_Visit_Activity_Checklist__c = pKit.VTD1_PSC_Visit_Activity_Checklist__c,
                        VTD1_IntegrationId__c = patientCase.VTD1_Subject_ID__c + '-' + pKit.Name,
                        VTR2_Quantity__c = pKit.VTR2_Quantity__c,
                        VTR2_Description__c = pKit.VTR2_Description__c,
                        VTR2_IfTemperatureControlled__c = pKit.VTR2_TemperatureControlled__c
                ));
            }

        }
        if (!this.newKitsByProtocolId.isEmpty()) {
            insert this.newKitsByProtocolId.values();
        }
        createDevices();
        createLKCs();
        createLabSamples();
        updateDeliveries();
    }

    private void createDevices() {
        Map<String, Schema.RecordTypeInfo> recTypeMap = Schema.SObjectType.VTD1_Patient_Device__c.getRecordTypeInfosByDeveloperName();

        for (HealthCloudGA__EhrDevice__c pDevice : [
                SELECT Id, VTD1_Care_Plan_Template__c, VTD1_Protocol_Kit__c, VTD1_Manufacturer__c, VTD1_ModelName__c, VTD1_Device_Type__c,
                        VTD1_Device_Name__c, VTD1_Kit_Type__c
                FROM HealthCloudGA__EhrDevice__c
                WHERE VTD1_Care_Plan_Template__c in : studyToCasesMap.keySet()
                AND VTD1_isArchivedVersion__c = false
                AND VTD1_Protocol_Kit__c IN :this.newKitsByProtocolId.keySet() order by VTD1_Care_Plan_Template__c
        ]) {
            List <Case> cases = studyToCasesMap.get(pDevice.VTD1_Care_Plan_Template__c);
            for (Case patientCase : cases) {
                this.newDevicesByProtocolId.put(pDevice.Id, new VTD1_Patient_Device__c(
                        VTD1_Device_Status__c = 'Pending',
                        VTD1_Protocol_Device__c = pDevice.Id,
                        VTD1_Patient_Kit__c = this.newKitsByProtocolId.get(pDevice.VTD1_Protocol_Kit__c).Id,
                        VTD1_Patient_Delivery__c = this.newKitsByProtocolId.get(pDevice.VTD1_Protocol_Kit__c).VTD1_Patient_Delivery__c,
                        VTD1_Case__c = patientCase.Id,
                        VTD1_Manufacturer__c = pDevice.VTD1_Manufacturer__c,
                        VTD1_Model_Name__c = pDevice.VTD1_ModelName__c,
                        RecordTypeId = recTypeMap.get(pDevice.VTD1_Kit_Type__c == 'Connected Devices' ? 'VTD1_Connected_Device' : 'VTD1_Tablet_Device').getRecordTypeId()
                ));
            }
        }

        if (!this.newDevicesByProtocolId.isEmpty()) {
            insert this.newDevicesByProtocolId.values();
        }
        createAccessories();
    }

    private void createLKCs() {
        for (VTD1_Protocol_LKC__c pLKC : [
                SELECT Id, VTD1_Care_Plan_Template__c, VTD1_Lab_Kit__c, VTD1_Content_Item_Description__c, VTD1_Content_Item_Type__c
                FROM VTD1_Protocol_LKC__c
                WHERE VTD1_Lab_Kit__c IN :this.newKitsByProtocolId.keySet()
                AND VTD1_Care_Plan_Template__c in : studyToCasesMap.keySet()
                AND VTD1_isArchivedVersion__c = false
        ]) {
            List <Case> cases = studyToCasesMap.get(pLKC.VTD1_Care_Plan_Template__c);
            for (Case patientCase : cases) {
                this.newLKCs.add(new Patient_LKC__c(
                        VTD1_CSM__c = patientCase.Id,
                        VTD1_Lab_Kit__c = this.newKitsByProtocolId.get(pLKC.VTD1_Lab_Kit__c).Id,
                        VTD1_Patient_Delivery__c = this.newKitsByProtocolId.get(pLKC.VTD1_Lab_Kit__c).VTD1_Patient_Delivery__c,
                        VTD1_Protocol_LKC__c = pLKC.Id,
                        VTD1_Description__c = pLKC.VTD1_Content_Item_Description__c,
                        VTD1_Type__c = pLKC.VTD1_Content_Item_Type__c
                ));
            }
        }

        if (!this.newLKCs.isEmpty()) {
            insert this.newLKCs;
        }
    }

    private void createAccessories() {
        Id recTypeId = Schema.SObjectType.VTD1_Patient_Accessories__c.getRecordTypeInfosByDeveloperName().get('Scheduled_Accessory').getRecordTypeId();

        for (VTD1_Protocol_Accessories__c pAccessories : [
                SELECT Id, VTD1_Care_Plan_Template__c, VTD1_Protocol_Device__c, VTD1_Accessory_Qty__c, VTD1_Accessory_Description__c
                FROM VTD1_Protocol_Accessories__c
                WHERE VTD1_Care_Plan_Template__c in : studyToCasesMap.keySet()
                AND VTD1_isArchivedVersion__c = false
                AND VTD1_Protocol_Device__c IN :this.newDevicesByProtocolId.keySet()
        ]) {
            List <Case> cases = studyToCasesMap.get(pAccessories.VTD1_Care_Plan_Template__c);
            for (Case patientCase : cases) {
                this.newAccessories.add(new VTD1_Patient_Accessories__c(
                        VTD1_Accessory_Status__c = 'Pending',
                        VTD1_Protocol_Accessories__c = pAccessories.Id,
                        VTD1_Patient_Kit__c = this.newDevicesByProtocolId.get(pAccessories.VTD1_Protocol_Device__c).VTD1_Patient_Kit__c,
                        VTD1_Patient_Device__c = this.newDevicesByProtocolId.get(pAccessories.VTD1_Protocol_Device__c).Id,
                        VTD1_Patient_Delivery__c = this.newDevicesByProtocolId.get(pAccessories.VTD1_Protocol_Device__c).VTD1_Patient_Delivery__c,
                        VTD1_Case__c = patientCase.Id,
                        VTD1_Accessory_Qty__c = pAccessories.VTD1_Accessory_Qty__c,
                        VTD1_Accessory_Description__c = pAccessories.VTD1_Accessory_Description__c,
                        RecordTypeId = recTypeId
                ));
            }
        }

        if (!this.newAccessories.isEmpty()) {
            insert this.newAccessories;
        }
    }

    private void createLabSamples() {
        for (VTD1_Protocol_Lab_Sample__c pSample : [
                SELECT Id, VTD1_Lab_Kit__c, VTD1_Care_Plan_Template__c
                FROM VTD1_Protocol_Lab_Sample__c
                WHERE VTD1_Lab_Kit__c IN :this.newKitsByProtocolId.keySet()
                AND VTD1_Care_Plan_Template__c in : studyToCasesMap.keySet()
                AND VTD1_isArchivedVersion__c = false
        ]) {
            List <Case> cases = studyToCasesMap.get(pSample.VTD1_Care_Plan_Template__c);
            for (Case patientCase : cases) {
                this.newSamples.add(new VTD1_Patient_Lab_Sample__c(
                        VTD1_CSM__c = patientCase.Id,
                        VTD1_Lab_Kit__c = this.newKitsByProtocolId.get(pSample.VTD1_Lab_Kit__c).Id,
                        VTD1_Patient_Delivery__c = this.newKitsByProtocolId.get(pSample.VTD1_Lab_Kit__c).VTD1_Patient_Delivery__c,
                        VTD1_Protocol_Lab_Sample__c = pSample.Id
                ));
            }
        }

        if (!this.newSamples.isEmpty()) {
            insert this.newSamples;
        }
    }

    private void updateDeliveries() {
        this.newDeliveries = [SELECT Id, VTD1_Case__c FROM VTD1_Order__c WHERE Id IN :this.newDeliveries ORDER BY VTD1_Protocol_Delivery_Order__c];

        updateDeliveriesAndCreateChildren();
        sortDeliveries();

        List<VTD1_Order__c> allDeliveries = new List<VTD1_Order__c>();
        allDeliveries.addAll(this.newDeliveries);
        allDeliveries.addAll(this.childDeliveries);
        update allDeliveries;
    }

    private void updateDeliveriesAndCreateChildren() {
        Map<Id, VTD1_Order__c> deliveryMap = new Map<Id, VTD1_Order__c>(this.newDeliveries);

        Set<Id> impDeliveryIds = new Set<Id>();
        Set<Id> labDeliveryIds = new Set<Id>();
        Integer impDeliveryCounter = 0;
        Integer labDeliveryCounter = 0;
        Integer impReturnCounter = 0;
        Integer labReturnCounter = 0;

        for (VTD1_Patient_Kit__c kit : [
                SELECT Id, VTD1_Patient_Delivery__c, VTD1_Kit_Type__c, Name
                FROM VTD1_Patient_Kit__c
                WHERE Id IN :this.newKitsByProtocolId.values()
                ORDER BY VTD1_Delivery_Order__c
        ]) {
            VTD1_Order__c delivery = deliveryMap.get(kit.VTD1_Patient_Delivery__c);

            delivery.VTD1_isWelcomeToStudyPackage__c = kit.VTD1_Kit_Type__c == 'Welcome to Study Package';
            delivery.VTD1_containsDevice__c = kit.VTD1_Kit_Type__c == 'Connected Devices';
            delivery.VTD1_containsTablet__c = kit.VTD1_Kit_Type__c == 'Study Hub Tablet';
            if (delivery.VTD1_isWelcomeToStudyPackage__c) {
                delivery.VTD1_Kit_Type__c = 'Welcome to Study Package';
            } else if (delivery.VTD1_containsDevice__c) {
                delivery.VTD1_Kit_Type__c = 'Connected Devices';
            } else if (delivery.VTD1_containsTablet__c) {
                delivery.VTD1_Kit_Type__c = 'Study Hub Tablet';
            }
            if (kit.VTD1_Kit_Type__c == 'IMP') {
                Boolean isImpDeliveryProcessed = impDeliveryIds.contains(kit.VTD1_Patient_Delivery__c);
                if (!isImpDeliveryProcessed) {
                    impDeliveryIds.add(kit.VTD1_Patient_Delivery__c);
                }
                impDeliveryCounter = impDeliveryIds.size();
                delivery.VTD1_IMP_Delivery_Order__c = impDeliveryCounter;
                delivery.VTD1_CountIMPPatientDeliveryComma__c = impDeliveryCounter + ',';
                delivery.VTD1_isIMP__c = true;
                delivery.VTD1_Kit_Type__c = 'IMP/Lab';

                if (!isImpDeliveryProcessed && createReturnItemIMP(impDeliveryCounter, delivery.VTD1_Case__c)) {
                    impReturnCounter++;
                    this.childImpLabDeliveries.add(getReturnDelivery(delivery.Id, kit.VTD1_Kit_Type__c, delivery.VTD1_Case__c, impReturnCounter));
                    delivery.VTD1_ContainsReturn__c = true;
                }
            } else if (kit.VTD1_Kit_Type__c == 'Lab') {
                Boolean isLabDeliveryProcessed = labDeliveryIds.contains(kit.VTD1_Patient_Delivery__c);
                if (!isLabDeliveryProcessed) {
                    labDeliveryIds.add(kit.VTD1_Patient_Delivery__c);
                }
                labDeliveryCounter = labDeliveryIds.size();
                delivery.VTD1_Lab_Delivery_Order__c = labDeliveryCounter;
                delivery.VTD1_CountLabPatientDeliveryComma__c = labDeliveryCounter + ',';
                delivery.VTD1_isLab__c = true;
                delivery.VTD1_Kit_Type__c = 'IMP/Lab';

                System.debug('createReturnItemLab ' + delivery);
                if (!isLabDeliveryProcessed && createReturnItemLab(labDeliveryCounter, delivery.VTD1_Case__c)) {
                    labReturnCounter++;
                    this.childImpLabDeliveries.add(getReturnDelivery(delivery.Id, kit.VTD1_Kit_Type__c, delivery.VTD1_Case__c, labReturnCounter));
                    delivery.VTD1_ContainsReturn__c = true;
                }
            }

            if (kit.VTD1_Kit_Type__c == 'Connected Devices' || kit.VTD1_Kit_Type__c == 'Study Hub Tablet') {
                delivery.VTD1_Kit_Type__c = kit.VTD1_Kit_Type__c;
                this.childDeliveries.add(getReturnDelivery(delivery.Id, kit.VTD1_Kit_Type__c, delivery.VTD1_Case__c, null));
                delivery.VTD1_ContainsReturn__c = true;
            }
        }
        if (!this.childImpLabDeliveries.isEmpty()) {
            insert this.childImpLabDeliveries;

            Map<Id, VTD1_Order__c> parentDeliveryMap = new Map<Id, VTD1_Order__c>(this.newDeliveries);
            for (VTD1_Order__c returnDelivery : this.childImpLabDeliveries) {
                if (createPackageIMP(returnDelivery.VTR2_Return_Kit_Type__c, returnDelivery.VTD1_Case__c) || createPackageLab(returnDelivery.VTR2_Return_Kit_Type__c, returnDelivery.VTD1_Case__c)) {
                    this.childDeliveries.add(getPackageDelivery(returnDelivery.Id, returnDelivery.VTD1_Case__c));
                    parentDeliveryMap.get(returnDelivery.VTD1_Patient_Delivery__c).VTD1_ContainsPackage__c = true;
                }
            }
        }

        if (!this.childDeliveries.isEmpty()) {
            insert this.childDeliveries;
            for (VTD1_Order__c packageDelivery : this.childDeliveries) {
                if (packageDelivery.RecordTypeId == VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_ORDER_PACKING_MATERIALS) {
                    this.packMatKits.add(getPatientKit(packageDelivery.Id, packageDelivery.VTD1_Case__c));
                }
            }
            if (!this.packMatKits.isEmpty()) {
                insert this.packMatKits;
                List<VTD1_Order__c> packDelToUpdateList = new List <VTD1_Order__c>();
                for (VTD1_Order__c packageDelivery : this.childDeliveries) {
                    for (VTD1_Patient_Kit__c pMKit : this.packMatKits) {
                        if (packageDelivery.Id == pMKit.VTD1_Patient_Delivery__c) {
                            packageDelivery.VTD1_Patient_Kit__c = pMKit.Id;
                            packDelToUpdateList.add(packageDelivery);
                        }
                    }
                }
                if (!packDelToUpdateList.isEmpty()) {
                    update packDelToUpdateList;
                }
            }
        }
        if (!this.childImpLabDeliveries.isEmpty()) {
            this.childDeliveries.addAll(this.childImpLabDeliveries);
        }
    }

    private Boolean createPackageIMP(String kitType, Id caseId) {
        return casesMap.get(caseId).VTD1_Study__r.VTD1_Return_Process__c == 'Packaging Materials Needed' &&
                kitType == 'IMP';
    }

    private Boolean createReturnItemIMP(Integer counter, Id caseId) {
        List <String> result = returnNumbersIMPMap.get(caseId);
        if (result == null)
            return false;
        return result.contains('' + counter);
    }

    private Boolean createPackageLab(String kitType, Id caseId) {
        return casesMap.get(caseId).VTD1_Study__r.VTD1_Lab_Return_Process__c == 'Packaging Materials Needed' &&
                kitType == 'Lab';
    }

    private Boolean createReturnItemLab(Integer counter, Id caseId) {
        List <String> result = returnNumbersLabMap.get(caseId);
        if (result == null)
            return false;
        return result.contains('' + counter);
    }

    private VTD1_Order__c getReturnDelivery(Id deliveryId, String kitType, Id caseId, Integer counter) {
        VTD1_Order__c parentDelivery = new Map<Id, VTD1_Order__c>(this.newDeliveries).get(deliveryId);
        return new VTD1_Order__c(
                Name = 'Return of ' + kitType + (counter != null ? ' #' + counter : ''),
                VTD1_Case__c = caseId,
                VTD1_Patient_Delivery__c = deliveryId,
                VTD1_Status__c = 'Not Started',
                VTD1_isReturnDelivery__c = true,
                RecordTypeId = VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_ORDER_AD_HOC_RETURN,
                VTD1_Kit_Type__c = parentDelivery.VTD1_Kit_Type__c,
                //VTR2_Destination_Type__c = 'Patient',
                VTR2_Return_Kit_Type__c = kitType,
                VT_R3_ReturnNumber__c = counter != null ? ' #' + counter : ''
        );
    }

    private VTD1_Order__c getPackageDelivery(Id deliveryId, Id caseId) {
        //Map<Id, VTD1_Order__c> returnDeliveryMap = new Map<Id, VTD1_Order__c>(this.childImpLabDeliveries);
        VTD1_Order__c returnDelivery = new Map<Id, VTD1_Order__c>(this.childImpLabDeliveries).get(deliveryId);
        return new VTD1_Order__c(
                Name = 'Package Delivery',
                VTD1_Case__c = caseId,
                VTD1_Patient_Delivery__c = returnDelivery.VTD1_Patient_Delivery__c,
                VTD1_Status__c = 'Not Started',
                VTD1_isPackageDelivery__c = true,
                RecordTypeId = VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_ORDER_PACKING_MATERIALS,
                VTD1_Kit_Type__c = returnDelivery.VTD1_Kit_Type__c,
                //VTR2_Destination_Type__c = 'Patient',
                VTR2_For_Return_Kit__c = deliveryId
        );
    }

    VTD1_Patient_Kit__c getPatientKit(Id packageDeliveryId, Id caseId) {
        return new VTD1_Patient_Kit__c(
                Name = 'Packaging Materials',
                VTD1_Patient_Delivery__c = packageDeliveryId,
                VTD1_Patient_Kits__c = packageDeliveryId,
                RecordTypeId = VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_PATIENT_KIT_PACKING_MATERIALS,
                VTD1_Status__c = 'Pending',
                VTD1_Case__c = caseId,
                VTD1_Kit_Contains_All_Contents__c = true
        );
    }

    private void sortDeliveries() {
        Integer idx = 1;

        Map<Id, List<VTD1_Order__c>> packageMap = new Map<Id, List<VTD1_Order__c>>();
        Map<Id, List<VTD1_Order__c>> returnMap = new Map<Id, List<VTD1_Order__c>>();
        for (VTD1_Order__c item : this.childDeliveries) {
            addToChildMap(item.VTD1_isPackageDelivery__c ? packageMap : returnMap, item);
        }

        for (VTD1_Order__c delivery : this.newDeliveries) {
            delivery.VTD1_Delivery_Order__c = idx++;

            if (packageMap.containsKey(delivery.Id)) {
                for (VTD1_Order__c child : packageMap.get(delivery.Id)) {
                    child.VTD1_Delivery_Order__c = idx++;
                }
            }

            if (returnMap.containsKey(delivery.Id)) {
                for (VTD1_Order__c child : returnMap.get(delivery.Id)) {
                    child.VTD1_Delivery_Order__c = idx++;
                }
            }
        }
    }

    private void addToChildMap(Map<Id, List<VTD1_Order__c>> childMap, VTD1_Order__c item) {
        if (!childMap.containsKey(item.VTD1_Patient_Delivery__c)) {
            childMap.put(item.VTD1_Patient_Delivery__c, new List<VTD1_Order__c>());
        }
        childMap.get(item.VTD1_Patient_Delivery__c).add(item);
    }

    global with sharing class VT_R5_CreateDeliveriesKits extends VT_R3_AbstractChainableQueueable implements Database.AllowsCallouts{
        private List<Id> caseIds;
        public VT_R5_CreateDeliveriesKits (List<Id> caseIds) {
            this.caseIds = caseIds;
        }

        public override Type getType() {
            return VT_R5_CreateDeliveriesKits.class;
        }

        public override void executeLogic() {
            new VT_R4_GenerateActualVisitsAndKits().executeDeliveriesAndKits(caseIds);
        }

        public override void enqueue() {
            if (VT_R4_PatientConversion.patientConversionInProgress) {
                enqueueViaPlatformEvent();
            } else {
                System.enqueueJob(this);
            }
        }
    }

    private List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrapProtocolVisits(List<VTD1_ProtocolVisit__c> protocolVisits) {
        List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrappedRecords = new List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper>();
        for (VTD1_ProtocolVisit__c protocol : protocolVisits) {
            wrappedRecords.add(new VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper(
                    protocol, protocol.VTR4_Original_Version__c, protocol.VTD1_Protocol_Amendment__c, protocol.VTR4_Remove_from_Protocol__c
            ));
        }
        for (VTD1_ProtocolVisit__c protocol : protocolVisits) {
            System.debug('protocol = ' + protocol);
        }
        return wrappedRecords;
    }
}