public with sharing class VT_D1_TrainingMaterialsControllerRemote {

    @AuraEnabled
    public static List<Knowledge__kav> getTrainingMaterials(Boolean piView, Id studyId) {
        if (!piView) {
            System.debug('piView ' + piView);
            Case c = VT_D1_PatientCaregiverBound.getPatientCase(UserInfo.getUserId());
            System.debug('Case VTD1_Study__c ' + c.VTD1_Study__c);
            studyId = c.VTD1_Study__c;
        }
        String lng = UserInfo.getLanguage();
        User u = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
        String queryStr =
                'SELECT Id, Language, VTR2_Use_Doc_Language_for_Translation__c, ArticleNumber, Summary, Title, VTD1_Content__c, KnowledgeArticleId ' +
                        'FROM Knowledge__kav ' +
                        'WHERE IsLatestVersion = TRUE ' +
                        ' AND PublishStatus=\'Online\' ' +
                        ' AND VTD2_Type__c INCLUDES(\'Training_Materials\') ';

        if (String.isNotEmpty(studyId)) {
            queryStr += 'AND VTD2_Study__c =\'' + studyId + '\' ';
        }

        if (piView) {
            queryStr += ' AND VTD2_Study__c IN (SELECT Study__c FROM Study_Team_Member__c WHERE User__c = \'' + UserInfo.getUserId() + '\')';
            queryStr += ' AND Language = \'' + lng + '\'';
            System.debug('Training materials queries for PI:\n' + queryStr);
            List<Knowledge__kav> lst = Database.query(queryStr);
            System.debug('Training materials objects for PI:');
            System.debug(lst);
            return lst;
        }
        Contact con = [SELECT VTR2_Doc_Language__c FROM Contact WHERE Id = :u.ContactId];
        String docLanguage = String.isBlank(con.VTR2_Doc_Language__c) ? 'en_US' : con.VTR2_Doc_Language__c;
        String queryStr1 = queryStr + 'AND (VTR2_Use_Doc_Language_for_Translation__c = FALSE AND Language = \'' + lng + '\'' + ')';
        String queryStr2 = queryStr + 'AND (VTR2_Use_Doc_Language_for_Translation__c = TRUE AND Language = \'' + docLanguage + '\'' + ')';
        System.debug('Training materials queries for patient:\n' + queryStr1 + '\n\n' + queryStr2);

        List<Knowledge__kav> lst1 = Database.query(queryStr1);
        List<Knowledge__kav> lst2 = Database.query(queryStr2);
        List<Knowledge__kav> result = new List<Knowledge__kav>();
        for (Knowledge__kav k : lst1) {
            result.add(k);
        }
        for (Knowledge__kav k : lst2) {
            result.add(k);
        }
        System.debug(result);
        return result;
    }

    @AuraEnabled
    public static Knowledge__kav getSingleArticle(Boolean piView, Id studyId, String articleType) {
        if (!piView) {
            Case c = VT_D1_PatientCaregiverBound.getPatientCase(UserInfo.getUserId());
            studyId = c.VTD1_Study__c;
        }

        Knowledge__kav article;
        String lng = UserInfo.getLanguage();
        String queryStr = 'SELECT Id, Title, VTD1_Content__c, KnowledgeArticleId ' +
                'FROM Knowledge__kav ' +
                'WHERE IsLatestVersion = TRUE' +
                ' AND PublishStatus=\'Online\'' +
                ' AND Language = \'' + lng + '\'' +
                ' AND VTD2_Study__c =\'' + studyId + '\'' +
                ' AND VTD2_Type__c INCLUDES( \'' + articleType + '\')' +
                ' ORDER BY LastModifiedDate DESC';
        System.debug('queryStr getSingleArticle ' + queryStr);

        try {
            List<Knowledge__kav> lstArticle = Database.query(queryStr);
            System.debug('Articles:' + lstArticle);
            article = lstArticle.get(0);
        } catch (Exception e) {
            System.debug('Error' + e);
        }
        System.debug('article ' + article + 'studyId ' + studyId + ' ' + 'articleType ' + articleType);
        return article;
    }

    @AuraEnabled
    public static List<ContentDocumentLink> getContentDocs(Id articleId) {
        List<ContentDocumentLink> docs = [
                SELECT ContentDocumentId, LinkedEntityId, ContentDocument.Title
                FROM ContentDocumentLink
                WHERE LinkedEntityId IN (
                        SELECT Id
                        FROM Knowledge__kav
                        WHERE IsLatestVersion = TRUE AND PublishStatus = 'Online'
                        AND Id = :articleId
                )
        ];
        return docs;
    }
}