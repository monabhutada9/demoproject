/*
 * Created by shumeyko on 28/08/2018.
 */
public class VT_D2_ContactTriggerHelper {
    private static Map<String, VT_D2_UserLanguageLocaleAssignment__mdt> uLocals = new Map<String, VT_D2_UserLanguageLocaleAssignment__mdt>();
    public static void createPortalUsersForPatients(List<Contact> contacts) {
        setLocales();
        Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Contact.getRecordTypeInfosById();
        List<User> users = new List<User>();
        Map<Id,List<Contact>> acContactMap = new Map<Id,List<Contact>>();
        System.debug('!! VT_D2_ContactTriggerHelper ');
        for (Contact newContact : contacts) {
            String recordTypeName = rtMap.get(newContact.RecordTypeID).getName();
//            Set<String> profiles = new Set<String> {
//                VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME,
//                VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME
//            };
//            if (profiles.contains(recordTypeName) && newContact.AccountId != null) {
//                if (! acContactMap.containsKey(newContact.AccountId)) {
//                    acContactMap.put(newContact.AccountId, new List<Contact>());
//                }
//                acContactMap.get(newContact.AccountId).add(newContact);
//            }
            if ((recordTypeName == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
                    || (recordTypeName == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME && newContact.VTR3_CreateUser__c))
                    && newContact.AccountId != null) {
                if (! acContactMap.containsKey(newContact.AccountId)) {
                    acContactMap.put(newContact.AccountId, new List<Contact>());
                }
                acContactMap.get(newContact.AccountId).add(newContact);
            }
        }

        System.debug('!! acContactMap ' + acContactMap);
        if (!acContactMap.isEmpty()) {
            Map<Id, Account> accMap = new Map<Id, Account>([
                    Select Id, Candidate_Patient__r.rr_lastName__c, Candidate_Patient__r.rr_firstName__c,
                Candidate_Patient__r.rr_Alias__c, Candidate_Patient__r.rr_Username__c,
                Candidate_Patient__r.rr_Email__c, Candidate_Patient__r.HealthCloudGA__Address1Country__c,
                Candidate_Patient__r.VTD2_Language__c,
                Candidate_Patient__r.VTR2_Caregiver_Email__c, Candidate_Patient__r.VTR2_Caregiver_First_Name__c,
                Candidate_Patient__r.VTR2_Caregiver_Last_Name__c, Candidate_Patient__r.VTR2_Caregiver_Preferred_Language__c
        FROM Account
                    WHERE Id IN :acContactMap.keySet()
            ]);
        System.debug('!! lstAcc ' + accMap);
        VTD1_RTId__c idSettings =  VTD1_RTId__c.getOrgDefaults();
        for (Id accId : acContactMap.keySet()) {
            HealthCloudGA__CandidatePatient__c cand =  accMap.get(accId).Candidate_Patient__r;
            if (cand != null) {
                for (Contact con : acContactMap.get(accId)) {
                        if (rtMap.get(con.RecordTypeID).getName() == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME
                                && !con.VTR3_CreateUser__c) {
                            continue;
                        }
                    User newUser = new User();
                    Boolean isCG = rtMap.get(con.RecordTypeID).getName() == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME;
                    newUser.ContactId = con.Id;
                    newUser.FirstName = isCG ? cand.VTR2_Caregiver_First_Name__c : cand.rr_firstName__c;
                    newUser.LastName =  isCG ? cand.VTR2_Caregiver_Last_Name__c : cand.rr_lastName__c;
                    newUser.Alias =     isCG ? VT_D1_CandidatePatientProcessHandler.getAlias(newUser.FirstName, newUser.LastName) : cand.rr_Alias__c;
                    newUser.Username =  isCG ? cand.VTR2_Caregiver_Email__c : cand.rr_Username__c;
                    newUser.Email =     isCG ? cand.VTR2_Caregiver_Email__c : cand.rr_Email__c;

                    if (Test.isRunningTest()) {
                        newUser.ProfileId = VT_D1_HelperClass.getProfileIdByName(
                                isCG ? VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME : VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
                        );
                    } else {
                        newUser.ProfileId = isCG ? idSettings.VTD2_Profile_Caregiver__c : idSettings.VTD2_Profile_Patient__c;
                    }

                    newUser.EmailEncodingKey = 'ISO-8859-1';
                    newUser.CountryCode = cand.HealthCloudGA__Address1Country__c;
                    newUser.LanguageLocaleKey = isCG ? cand.VTR2_Caregiver_Preferred_Language__c : cand.VTD2_Language__c;

                    VT_D2_UserLanguageLocaleAssignment__mdt langCountryLcl = uLocals.get(
                        newUser.CountryCode + newUser.LanguageLocaleKey);
                    //VT_D2_UserLanguageLocaleAssignment__mdt countryLcl = uLocals.get(acc.Candidate_Patient__r.HealthCloudGA__Address1Country__c);
                    system.debug('!!! uLocals='+uLocals);
                    system.debug('!!! langCountryLcl='+langCountryLcl);
                    if (langCountryLcl == null) {
                        system.debug('!!! try by country only: '+newUser.CountryCode);
                        langCountryLcl = uLocals.get(newUser.CountryCode);
                    }
                    if (langCountryLcl != null) {
                        system.debug('!!! found');
                        //newUser.LanguageLocaleKey = langCountryLcl.VTD2_LanguageLocaleKey__c;
                        newUser.TimezoneSidKey = langCountryLcl.VTD2_TimeZoneSidKey__c;
                        newUser.LocaleSidKey = langCountryLcl.VTD2_LocaleSidKey__c;
                        newUser.VTD1_Country_Code__c = langCountryLcl.VTD2_CountryCode__c;
                        } else system.debug('!!! not found');
                    //if (countryLcl != null) {
                    //newUser.VTD1_Country_Code__c = countryLcl.VTD2_CountryCode__c;
                    //}
                    system.debug('!!! newUser' + newUser);
                    users.add(newUser);
                }
            }
        }

        //insert users;
        if (!users.isEmpty()) {
            Database.SaveResult[] srList = Database.insert(users, true);

            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted user. User ID: ' + sr.getId());
                } else {
                    // Operation failed, so get all errors
                    for (Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('User fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
    }
    }

    private static void setLocales() {
        VT_D2_UserLanguageLocaleAssignment__mdt [] uLocals_mdt = [SELECT VTD2_LanguageLocaleKey__c,
                                                                VTD2_TimeZoneSidKey__c,
                                                                VTD2_CountryCode__c,
                                                                VTD2_LocaleSidKey__c,
                                                                VTD2_StdCountryCode__c                                              
                                                                FROM VT_D2_UserLanguageLocaleAssignment__mdt];
        if (uLocals_mdt.size() == 0) {
            throw new DmlException('Please provide values to the User Language Locale Assignment (custom metadata)!');
        }
        String lang;
        for (VT_D2_UserLanguageLocaleAssignment__mdt lcl : uLocals_mdt) {
            if(String.isBlank(lcl.VTD2_LanguageLocaleKey__c))
                lang='';
            else
                lang=lcl.VTD2_LanguageLocaleKey__c;
            uLocals.put(lcl.VTD2_StdCountryCode__c + lang, lcl);
            //uLocals.put(lcl.VDT2_Country__c, lcl);
        }
    }
}