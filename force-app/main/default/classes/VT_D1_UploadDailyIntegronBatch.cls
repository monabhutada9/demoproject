global class VT_D1_UploadDailyIntegronBatch implements Database.Batchable<sObject>, Schedulable {

    // To schedule via apex:
    // system.schedule('Upload Daily Integron Tasks', '0 0 12 * * ?',  new VT_D1_UploadDailyIntegronBatch());

    global void execute(SchedulableContext sc) {
        Database.executeBatch(new VT_D1_UploadDailyIntegronBatch());
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id, VTD1_Virtual_Trial_Study_Lead__c
            FROM HealthCloudGA__CarePlanTemplate__c
            WHERE VTD1_Integron_Bulk_Upload_Flag__c = TRUE
        ]);
    }

    /**
     * Edited by Galiya Khalikova on 26.12.2019.
     * Edited as part of SH-7022
     * @param BC
     * @param recs List<HealthCloudGA__CarePlanTemplate__c> contains list of Studies with VTD1_Integron_Bulk_Upload_Flag__c = TRUE
     * Move unconverted Task to TN Catalog
     */
    global void execute(Database.BatchableContext BC, List<HealthCloudGA__CarePlanTemplate__c> recs) {
        List<Id> sourceIds = new List<Id>();
        List <String> tnCatalogCodes = new List<String>();
        for (HealthCloudGA__CarePlanTemplate__c item : recs) {
            sourceIds.add(item.Id);
            tnCatalogCodes.add('T565');
        }

        List <Task> newTasks = VT_D2_TNCatalogTasks.generateTasks(tnCatalogCodes, sourceIds, null, null, false);
        insert newTasks;
    }

    global void finish(Database.BatchableContext BC) {

    }

}