/**
 * Created by User on 19/05/15.
 */
@isTest
private with sharing class VerifyAddressCustomComponentCntrlTest {
    @isTest
    private static void getcallVerifyAddressTest(){
        System.assertNotEquals(null,VerifyAddressCustomComponentController.getcallVerifyAddress(null,null));
    }
    @isTest
    private static void getOfficeTest(){
        DomainObjects.Contact_t firstContact = new DomainObjects.Contact_t();
        DomainObjects.Address_t firstAddress = new DomainObjects.Address_t()
                .addContact(firstContact)
                .setPrimary(true);
        firstAddress.persist();
        System.assertNotEquals(null,VerifyAddressCustomComponentController.getOffice(firstAddress.Id));
    }
}