/**
 * Created by shume on 13/12/2018.
 */

public with sharing class VT_D2_CaseListControllerRemote {
    @AuraEnabled
    public static CaseListInfo getCases () {
        CaseListInfo caseListInfo = new CaseListInfo();
        caseListInfo.cases = [SELECT Id, VTD1_Patient_User__r.Name,
                                VTD1_Subject_ID__c,
                                VTD1_Study__c, VTD1_Study__r.Name
                            FROM Case
                            WHERE RecordTypeId=:VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN
                            AND VTD1_Study__c != null AND VTD1_Subject_ID__c!=null AND ContactId  != null
                            ORDER BY VTD1_Study__r.Name LIMIT 11];
        if (caseListInfo.cases.size() > 10) {
            List<Case> casesCopy = new List<Case>();
            Integer i =0;
            while (casesCopy.size() < 10) {
                casesCopy.add(caseListInfo.cases[i]);
                i++;
            }
            caseListInfo.cases = casesCopy;
            caseListInfo.casesCount = '(10+)';
        } else {
            caseListInfo.casesCount = '(' + caseListInfo.cases.size() + ')';
        }
        return caseListInfo;
    }

    public class CaseListInfo {
        @AuraEnabled public List<Case> cases;
        @AuraEnabled public String casesCount;
    }
}