/**
 * Created by K.Latysh on 05-Oct-20.
 */

public with sharing class VT_R5_StudyGenCreateUsersHelper {

    public static String createUsers(Set<String> userNames, String whatIsUpdate){
        String sandboxName = URL.getSalesforceBaseUrl().getHost().substringBetween('--','--c.') + '@mailinator.com';
        Set<String> UserNamesForUpdate= new Set<String>();
        try {
            if(whatIsUpdate=='main'){
                for(String userName : userNames) {
                    if (userName.contains('PI') || userName.contains('SCR') || userName.contains('Investigator')) {
                        UserNamesForUpdate.add(userName.toLowerCase() + '_' + sandboxName);
                    }
                }
                System.debug('main= ' + UserNamesForUpdate);
                List<User> userList = [SELECT Id, Username
                                        FROM User
                                        WHERE Username IN : UserNamesForUpdate
                ];

                if(userList.isEmpty()) {
                    createMainUsers(UserNamesForUpdate);
                } else {
                    for(User u : userList){
                        if(UserNamesForUpdate.contains(u.Username)){
                            UserNamesForUpdate.remove(u.Username);
                        }
                    }
                    if(!UserNamesForUpdate.isEmpty()){
                        createMainUsers(UserNamesForUpdate);
                    }

                }

            } else if(whatIsUpdate=='additional'){
                for(String userName : userNames) {
                    if (!userName.contains('PI') && !userName.contains('SCR') && !userName.contains('Investigator')) {
                        UserNamesForUpdate.add(userName.toLowerCase() + '_' + sandboxName);
                        System.debug('PIuserName= ' + userName);
                        System.debug('!userName.contains(\'PI\')= ' + !userName.contains('PI'));
                        System.debug('!userName.contains(\'PI\')= ' + !userName.contains('SCR'));
                        System.debug('!userName.contains(\'PI\')= ' + !userName.contains('Investigator'));
                    }

                }
                System.debug('additional= ' + UserNamesForUpdate);
                List<User> userList = [SELECT Id, Username
                                        FROM User
                                        WHERE Username IN : UserNamesForUpdate
                ];

                if(userList.isEmpty()) {
                    createAdditionalUsers(UserNamesForUpdate);
                } else {
                    for(User u : userList){

                        if(UserNamesForUpdate.contains(u.Username)){
                            UserNamesForUpdate.remove(u.Username);
                        }
                    }
                    if(!UserNamesForUpdate.isEmpty()){
                        for(String name : UserNamesForUpdate){
                            System.debug('name==' + name);
                        }
                        createAdditionalUsers(UserNamesForUpdate);
                    }

                }

            } else if(whatIsUpdate=='CMT'){
                for(String userName : userNames) {
                    UserNamesForUpdate.add(userName.toLowerCase() + '_' + sandboxName);
                }
                System.debug('CMT= ' + UserNamesForUpdate);
                upsertCMT(UserNamesForUpdate);
            }


            System.debug('mainUserNames= ' + UserNamesForUpdate);
        } catch (Exception e) {
            System.debug('exception handling');
            System.debug(e.getStackTraceString());
            System.debug(e.getMessage());
            return e.getMessage();
        }
        return 'ok';
    }

    public static void createMainUsers(Set<String> mainUserNames){
        System.debug('createMainUsers');
        RecordType researchFacilityRecordType = [
                SELECT
                        Name,
                        DeveloperName
                FROM RecordType
                WHERE DeveloperName = 'Research_Facility'
        ];

        RecordType sponsorRecordType = [
                SELECT
                        Name,
                        DeveloperName
                FROM RecordType
                WHERE DeveloperName = 'Sponsor'
        ];

        List<String> profileNames = new List<String>();
        profileNames.add('Primary Investigator');
        profileNames.add('Site Coordinator');

        List<Profile> profiles = [
                SELECT
                        CreatedById,
                        CreatedDate,
                        Description,
                        Id,
                        LastModifiedById,
                        LastModifiedDate,
                        LastReferencedDate,
                        LastViewedDate,
                        Name,
                        UserLicenseId,
                        UserType
                FROM Profile
                WHERE Name IN :profileNames
        ];

        Map<String, Profile> profileByName = new Map<String, Profile>();
        for (Profile profile : profiles) {
            profileByName.put(profile.Name, profile);
        }

        List<Account> accounts = new List<Account>();

        for (String n : mainUserNames) {
            String name = n.remove('@mailinator.com');
            System.debug('name' + name);
            if (name.contains('pi') || name.contains('investigator')) {
                accounts.add(new Account(Name = name, RecordTypeId = researchFacilityRecordType.Id));
            } else if (name.contains('scr')) {
                accounts.add(new Account(Name = name, RecordTypeId = sponsorRecordType.Id));
            }
        }
        upsert accounts;
        System.debug('123123');

        List<Account> accountsFromDatabase = [
                SELECT Id,
                        Name
                FROM Account
                WHERE Name IN :mainUserNames
        ];
        List<Contact> contacts = new List<Contact>();
        for (Account account : accounts) {
            System.debug('name' + account.Name);
            contacts.add(new Contact(
                    AccountId = account.Id,
                    Email = account.Name + '@mailinator.com',
                    FirstName = account.Name,
                    LastName = account.Name));
        }
        System.debug('123123');

        upsert contacts;
        System.debug('123123');

        List<Contact> contactsFromDatabase = [
                SELECT Id,
                        Email,
                        FirstName
                FROM Contact
                WHERE FirstName IN :mainUserNames];

        List<User> users = new List<User>();

        List<Profile> piProfile = [
                SELECT Id,
                        Name
                FROM Profile
                WHERE Name = 'Primary Investigator'
        ];
        List<Profile> scrProfile = [
                SELECT Id,
                        Name
                FROM Profile
                WHERE Name = 'Site Coordinator'
        ];
        for (Contact contact : contacts) {
            System.debug('contact.Email= ' + contact.Email);
            users.add(new User(
                    Alias = contact.FirstName.toLowerCase().substring(1, 7),
                    ContactId = contact.Id,
                    Email = contact.Email,
                    FirstName = contact.FirstName,
                    IsActive = true,
                    LastName = contact.FirstName,
                    CommunityNickname = contact.FirstName,
                    Username = contact.Email,
                    EmailEncodingKey = 'ISO-8859-1',
                    LocaleSidKey = 'en_US',
                    LanguageLocaleKey = 'en_US',
                    TimeZoneSidKey = 'America/Los_Angeles',

                    ProfileId = contact.FirstName.containsIgnoreCase('PI') || contact.FirstName.containsIgnoreCase('Investigator') ? piProfile.get(0).Id : scrProfile.get(0).Id

            ));
        }

        insert users;

        List<Licenses__c> licenses = new List<Licenses__c>();


        for (Contact contact : contacts) {
            if (contact.FirstName.containsIgnoreCase('PI') || contact.FirstName.containsIgnoreCase('Investigator')) {

                licenses.add(new Licenses__c(
                        Contact__c = contact.Id,
                        Name = '1' + contact.Id,
                        State__c = 'NY',
                        VTD1_Country__c = 'US',
                        VTD1_Expiry_Date__c = Date.newInstance(System.now().year(), System.now().month() + 1, System.now().day()),
                        VTD1_License_Type__c = 'Type Placeholder 1',
                        VTD1_Primary_Investigator_License__c = true));
                licenses.add(new Licenses__c(
                        Contact__c = contact.Id,
                        Name = '2' + contact.Id,
                        State__c = 'CA',
                        VTD1_Country__c = 'US',
                        VTD1_Expiry_Date__c = Date.newInstance(System.now().year(), System.now().month() + 1, System.now().day()),
                        VTD1_License_Type__c = 'Type Placeholder 1',
                        VTD1_Primary_Investigator_License__c = true));
            }
        }

        insert licenses;

    }

    public static void createAdditionalUsers(Set<String> additionalUserNames){
        List<String> profileNames = new List<String>();
        profileNames.add('Monitor');
        profileNames.add('Monitoring Report Reviewer');
        profileNames.add('Project Lead');
        profileNames.add('Project Management Analyst');
        profileNames.add('Recruitment Expert');
        profileNames.add('Regulatory Specialist');
        profileNames.add('Study Concierge');
        profileNames.add('Study Admin');
        profileNames.add('CRA');
        profileNames.add('Patient Guide');
        profileNames.add('Therapeutic Medical Advisor');
        profileNames.add('Virtual Trial Head of Operations');
        profileNames.add('Virtual Trial Study Lead');

        List<Profile> profiles = [
                SELECT
                        CreatedById,
                        CreatedDate,
                        Description,
                        Id,
                        LastModifiedById,
                        LastModifiedDate,
                        LastReferencedDate,
                        LastViewedDate,
                        Name,
                        UserLicenseId,
                        UserType
                FROM Profile
                WHERE Name IN :profileNames
        ];

        Map<String, Profile> profileByName = new Map<String, Profile>();
        for (Profile profile : profiles) {
            profileByName.put(profile.Name, profile);
        }


        List<User> restUsers = new List<User>();
        for (String name : additionalUserNames) {
            String abbrName = name.remove('@mailinator.com');
            String profileName;
            if(abbrName.containsIgnoreCase('Monitor_')){
                profileName ='Monitor';
            } else if(abbrName.containsIgnoreCase('Monitoring')){
                profileName ='Monitoring Report Reviewer';
            } else if(abbrName.containsIgnoreCase('Project_Lead')){
                profileName ='Project Lead';
            } else if(abbrName.containsIgnoreCase('Project_Management_Analyst')){
                profileName ='Project Management Analyst';
            } else if(abbrName.containsIgnoreCase('Recruitment_Expert')){
                profileName ='Recruitment Expert';
            } else if(abbrName.containsIgnoreCase('Regulatory_Specialist')){
                profileName ='Regulatory Specialist';
            } else if(abbrName.containsIgnoreCase('SC1') || abbrName.containsIgnoreCase('SC2')){
                profileName ='Study Concierge';
            } else if(abbrName.containsIgnoreCase('Study_Admin')){
                profileName ='Study Admin';
            } else if(abbrName.containsIgnoreCase('CRA')){
                profileName ='CRA';
            } else if(abbrName.containsIgnoreCase('PG')){
                profileName ='Patient Guide';
            } else if(abbrName.containsIgnoreCase('TMA')){
                profileName ='Therapeutic Medical Advisor';
            } else if(abbrName.containsIgnoreCase('VTHO')){
                profileName ='Virtual Trial Head of Operations';
            } else if(abbrName.containsIgnoreCase('VTSL')){
                profileName ='Virtual Trial Study Lead';
            }
            System.debug('abbrName= ' + abbrName);
            System.debug('profileName= ' + profileName);
            restUsers.add(new User(
                    Alias = abbrName.toLowerCase().substring(1, 7),
                    Email = abbrName + '@mailinator.com',
                    FirstName = abbrName,
                    IsActive = true,
                    LastName = abbrName,
                    CommunityNickname = abbrName,
                    Username = abbrName + '@mailinator.com',
                    EmailEncodingKey = 'ISO-8859-1',
                    LocaleSidKey = 'en_US',
                    LanguageLocaleKey = 'en_US',
                    TimeZoneSidKey = 'America/Los_Angeles',
                    ProfileId = profileByName.get(profileName).Id));
        }


        insert restUsers;
    }

    public static void upsertCMT(Set<String> userNames){
        String sandboxName = URL.getSalesforceBaseUrl().getHost().substringBetween('--','--c.') + '@mailinator.com';
        Map<String, String> userNameToUserIdMap = new Map<String, String>();
        List<User> userList = [SELECT Id, Username
        FROM User
        WHERE Username IN : userNames];
        System.debug('userList' + userList);
        for(User u : userList) {
            userNameToUserIdMap.put(u.Username, u.Id);
        }

        List<VTR3_Study_Setup_Prerequisites_Roles__mdt> metaDataUsers = [
                SELECT Id,
                        DeveloperName,
                        VTR3_UserId__c
                FROM VTR3_Study_Setup_Prerequisites_Roles__mdt
        ];

        List<String> metadataUsersNames = new List<String>();
        for (VTR3_Study_Setup_Prerequisites_Roles__mdt user : metaDataUsers) {
            metadataUsersNames.add('VTR3_Study_Setup_Prerequisites_Roles__mdt.' + user.DeveloperName);
        }
        List<Metadata.Metadata> records =
                Metadata.Operations.retrieve(Metadata.MetadataType.CustomMetadata,
                        metadataUsersNames);
        Metadata.DeployContainer container = new Metadata.DeployContainer();
        System.debug('records=' + records);

        for (Metadata.Metadata record : records) {
            Metadata.CustomMetadata vatRecord = (Metadata.CustomMetadata) record;
            String abbreviatedName = vatRecord.fullName.remove('VTR3_Study_Setup_Prerequisites_Roles__mdt.');
            System.debug('abbreviatedName=' + abbreviatedName);
            for (Metadata.CustomMetadataValue vatRecValue : vatRecord.values) {
                if (vatRecValue.field.equals('VTR3_UserId__c')) {
                    if (abbreviatedName.equals('Monitor')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('monitor_' + sandboxName));
                        System.debug('value' + String.valueOf(userNameToUserIdMap.get('monitor_' + sandboxName)));
                    } else if (abbreviatedName.equals('Monitoring_Report_Reviewer')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('monitoring_report_reviewer_' + sandboxName));

                    } else if (abbreviatedName.equals('Project_Lead')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('project_lead_' + sandboxName));
                    } else if (abbreviatedName.equals('Project_Management_Analyst')) {

                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('project_management_analyst_' + sandboxName));
                    } else if (abbreviatedName.equals('Recruitment_Expert')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('recruitment_expert_' + sandboxName));
                    } else if (abbreviatedName.equals('Study_Admin')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('study_admin_' + sandboxName));
                    } else if (abbreviatedName.equals('TMA')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('tma_' + sandboxName));
                    } else if (abbreviatedName.equals('VTHO')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('vtho_' + sandboxName));
                    } else if (abbreviatedName.equals('VTSL')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('vtsl_' + sandboxName));
                    } else if (abbreviatedName.equals('Regulatory_Specialist')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('regulatory_specialist_' + sandboxName));
                    }
                    else if (abbreviatedName.equals('SC1')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('sc1_' + sandboxName));
                    } else if (abbreviatedName.equals('SC2')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('sc2_' + sandboxName));
                    }
                    else if (abbreviatedName.equals('Site_1_Backup_PI')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_1_backup_pi_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_1_PI1')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_1_pi1_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_1_Sub_Investigator1')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_1_sub_investigator1_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_1_Sub_Investigator2')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_1_sub_investigator2_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_2_Backup_PI')) {

                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_2_backup_pi_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_2_PI2')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_2_pi2_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_2_Sub_Investigator1')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_2_sub_investigator1_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_2_Sub_Investigator2')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_2_sub_investigator2_' + sandboxName));
                    }
                    else if (abbreviatedName.equals('Site_1_Backup_SCR')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_1_backup_scr_' + sandboxName));

                    } else if (abbreviatedName.equals('Site_1_Normal_SCR1')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_1_normal_scr1_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_1_Normal_SCR2')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_1_normal_scr2_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_1_Primary_SCR')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_1_primary_scr_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_2_Backup_SCR')) {

                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_2_backup_scr_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_2_Normal_SCR1')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_2_normal_scr1_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_2_Normal_SCR2')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_2_normal_scr2_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_2_Primary_SCR')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_2_primary_scr_' + sandboxName));

                    }
                    else if (abbreviatedName.equals('Site_1_PG1')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_1_pg1_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_1_PG2')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_1_pg2_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_1_PG3')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_1_pg3_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_1_PG4')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_1_pg4_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_2_PG1')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_2_pg1_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_2_PG2')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_2_pg2_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_2_PG3')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_2_pg3_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_2_PG4')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_2_pg4_' + sandboxName));
                    }
                    else if (abbreviatedName.equals('Site_1_Onsite_CRA')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_1_onsite_cra_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_2_Onsite_CRA')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_2_onsite_cra_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_1_Remote_CRA')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_1_remote_cra_' + sandboxName));
                    } else if (abbreviatedName.equals('Site_2_Remote_CRA')) {
                        vatRecValue.value = String.valueOf(userNameToUserIdMap.get('site_2_remote_cra_' + sandboxName));
                    }

                }
            }
            container.addMetadata(vatRecord);
        }
        Id asyncResultId = Metadata.Operations.enqueueDeployment(container, null);
        System.debug(asyncResultId);
    }
    //comment

}