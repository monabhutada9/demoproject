/**
* Update By SOnam on 06.11.2020 for User Story SH - 17965
*/

public without sharing class VT_R5_CustomAuditHandler_Misc {
    public static List<VTR5_UserAudit__c> userAuditsToInsert = new List<VTR5_UserAudit__c>();


    public static final List<String> STUDY_PARTICIPANT_FIELDS = new List<String>{
            'VTD1_Project_Lead__c',
            'VTD1_Virtual_Trial_Study_Lead__c',
            'VTD1_Regulatory_Specialist__c',
            'VTD1_Remote_CRA__c',
            'VTD1_Virtual_Trial_head_of_Operations__c'
    };

    public static void onAfterUpdateStudy(List<HealthCloudGA__CarePlanTemplate__c> studies, Map<Id, HealthCloudGA__CarePlanTemplate__c> oldMap) {

        Map<String, List<String>> newFieldValuesMap = new Map<String, List<String>>();
        Map<String, List<String>> previousFieldValuesMap = new Map<String, List<String>>();
        List<String> newFieldValues = new List<String>();
        List<String> previousFieldValues = new List<String>();

        List<Id> studyIds = new List<Id>();

        for (HealthCloudGA__CarePlanTemplate__c study : studies) {
            studyIds.add(study.Id);

            previousFieldValuesMap.put(study.Id, new List<String>());
            newFieldValuesMap.put(study.Id, new List<String>());

            for (String field : STUDY_PARTICIPANT_FIELDS) {
                String newVal = (String) study.get(field);
                String oldVal = (String) oldMap.get(study.Id).get(field);

                if (oldVal != newVal) {
                    previousFieldValuesMap.get(study.Id).add(oldVal);
                    newFieldValuesMap.get(study.Id).add(newVal);
                    if (oldVal != null) {
                        previousFieldValues.add(oldVal);
                    }
                    if (newVal != null) {
                        newFieldValues.add(newVal);
                    }
                }
            }
        }
        if (!previousFieldValues.isEmpty()) {
            List<HealthCloudGA__CarePlanTemplate__c> studyWithStm = [
                    SELECT Id, (
                            SELECT Id,User__c
                            FROM Study_Team_Members__r
                            WHERE User__c IN :previousFieldValues
                    )
                    FROM HealthCloudGA__CarePlanTemplate__c
                    WHERE Id IN :studyIds
            ];
            Map<Id, Id> stmByUserId = new Map<Id, Id>();
            for (HealthCloudGA__CarePlanTemplate__c study : studyWithStm) {
                for (Study_Team_Member__c stm : study.Study_Team_Members__r) {
                    stmByUserId.put(stm.User__c, stm.Id);
                }
            }

            List<VTR5_UserAudit__c> oldSsaAudits = [
                    SELECT Id, User__c, (
                            SELECT Id,
                                    VTR5_StudyAccessStatus__c,
                                    VTR5_StudyDeactivatedBy__c,
                                    VTR5_StudyDeactivatedDate__c,
                                    VTR5_StudyName__c
                            FROM Site_Study_Access_Audits__r
                            WHERE VTR5_StudyName__c IN :studyIds
                    )
                    FROM VTR5_UserAudit__c
                    WHERE User__c IN :previousFieldValues
            ];

            List<VTR5_SiteStudyAccessAudit__c> oldSsaAuditsForUpdate = new List<VTR5_SiteStudyAccessAudit__c>();
            for (VTR5_UserAudit__c ua : oldSsaAudits) {
                for (VTR5_SiteStudyAccessAudit__c ssaAudit : ua.Site_Study_Access_Audits__r) {
                    for (HealthCloudGA__CarePlanTemplate__c study : studyWithStm) {
                        if (stmByUserId.containsKey(ua.User__c)) {
                            continue;

                        } else {
                            if (ssaAudit.VTR5_StudyAccessStatus__c && ssaAudit.VTR5_StudyName__c == study.Id) {
                                ssaAudit.VTR5_StudyAccessStatus__c = false;
                                ssaAudit.VTR5_StudyDeactivatedBy__c = UserInfo.getName();
                                ssaAudit.VTR5_StudyDeactivatedDate__c = Datetime.now();
                                oldSsaAuditsForUpdate.add(ssaAudit);
                            }
                        }
                    }
                }
            }

            if (oldSsaAuditsForUpdate.size() > 0) {
                update oldSsaAuditsForUpdate;
            }
        }

        if (!newFieldValues.isEmpty()) {
            List<VTR5_UserAudit__c> ssaAudits = [
                    SELECT Id, (
                            SELECT Id,VTR5_StudyAccessStatus__c
                            FROM Site_Study_Access_Audits__r
                            WHERE VTR5_StudyName__c IN :studyIds
                    )
                    FROM VTR5_UserAudit__c
                    WHERE User__c IN:newFieldValues
            ];
            List<VTR5_SiteStudyAccessAudit__c> ssaAuditToCreate = new List<VTR5_SiteStudyAccessAudit__c>();

            for (HealthCloudGA__CarePlanTemplate__c study : studies) {
                for (VTR5_UserAudit__c ua : ssaAudits) {
                    if (ua.Site_Study_Access_Audits__r.isEmpty()) {
                        //create new site study audit
                        VTR5_SiteStudyAccessAudit__c newSsaAudit1 = new VTR5_SiteStudyAccessAudit__c();
                        newSsaAudit1.VTR5_StudyAccessStatus__c = true;
                        newSsaAudit1.VTR5_StudyName__c = study.Id;
                        newSsaAudit1.VTR5_AssignedDateStudy__c = Datetime.now();
                        newSsaAudit1.User_Audit__c = ua.Id;
                        ssaAuditToCreate.add(newSsaAudit1);
                    } else {
                        Boolean hasStudyAccessStatus = false;
                        for (VTR5_SiteStudyAccessAudit__c ssaAudit : ua.Site_Study_Access_Audits__r) {
                            if (ssaAudit.VTR5_StudyAccessStatus__c) {
                                hasStudyAccessStatus = true;
                            }
                        }
                        if (hasStudyAccessStatus) {
                            continue;
                        }

                        VTR5_SiteStudyAccessAudit__c newSsaAudit = new VTR5_SiteStudyAccessAudit__c();
                        newSsaAudit.VTR5_StudyAccessStatus__c = true;
                        newSsaAudit.VTR5_StudyName__c = study.Id;
                        newSsaAudit.VTR5_AssignedDateStudy__c = Datetime.now();
                        newSsaAudit.User_Audit__c = ua.Id;
                        ssaAuditToCreate.add(newSsaAudit);
                    }
                }
            }
            if (ssaAuditToCreate.size() > 0) {
                insert ssaAuditToCreate;
            }
        }


    }

    public static void onAfterInsertSstm(List<Study_Site_Team_Member__c> members) {

        Map<Id, Study_Team_Member__c> associatedSTM = new Map<Id, Study_Team_Member__c>();
        for (Study_Site_Team_Member__c member : members) {
            associatedSTM.put(member.VTR2_Associated_PI3__c, null);
            associatedSTM.put(member.VTD1_Associated_PI__c, null);
            associatedSTM.put(member.VTR2_Associated_PI__c, null);
            associatedSTM.put(member.VTR2_Associated_SCr__c, null);
            associatedSTM.put(member.VTD1_Associated_PG__c, null);
            associatedSTM.put(member.VTR2_Associated_SubI__c, null);
            associatedSTM.put(member.VTR5_Associated_CRA__c, null); //Start SH-17965 add the CRA as well for audit Report

        }
        associatedSTM.remove(null);
        associatedSTM.putAll([SELECT Id, Study__c, VTD1_VirtualSite__c, User__r.Id  FROM Study_Team_Member__c WHERE Id IN :associatedSTM.keySet()]);

        // get existing User Audits for users or create new
        Set<Id> userIds = new Set<Id>();
        Set<Id> membersIds = new Set<Id>();
        for (Study_Site_Team_Member__c member : members) {
            Id userId, memberId;
            if(member.VTR2_Associated_SCr__c != null){
                userId = associatedSTM.get(member.VTR2_Associated_SCr__c).User__r.Id;
                memberId = associatedSTM.get(member.VTR2_Associated_SCr__c).Id;
            }else if(member.VTD1_Associated_PG__c != null){
                userId = associatedSTM.get(member.VTD1_Associated_PG__c).User__r.Id;
                memberId = associatedSTM.get(member.VTD1_Associated_PG__c).Id;
            }else if(member.VTR2_Associated_SubI__c != null){
                userId = associatedSTM.get(member.VTR2_Associated_SubI__c).User__r.Id;
                memberId = associatedSTM.get(member.VTR2_Associated_SubI__c).Id;
            }
            //Start SH-17965 add the CRA as well for audit Report
            else if(member.VTR5_Associated_CRA__c != null){
                userId = associatedSTM.get(member.VTR5_Associated_CRA__c).User__r.Id;
                memberId = associatedSTM.get(member.VTR5_Associated_CRA__c).Id;
            } // End SH-17965

            userIds.add(userId);
            membersIds.add(memberId);
        }
        Map<Id, VTR5_UserAudit__c> userAuditByUser = VT_R5_CustomAuditHandler_Misc.getOrCreateUserAudits(userIds);

        // Get SSTM's with existed SSAA
        List<VTR5_SiteStudyAccessAudit__c> existedSSAA = [
                SELECT Id, User_Audit__c,  VTR5_StudyName__c, VTR5_SiteName__c, VTR5_SiteAccessStatus__c,
                        VTR5_AssignedDateSite__c, VTR5_SiteDeactivatedDate__c, VTR5_StudyTeamMember__c
                FROM VTR5_SiteStudyAccessAudit__c
                WHERE User_Audit__c IN :userAuditByUser.values()
                AND VTR5_SiteDeactivatedDate__c = NULL
        ];


        Map<Id, VTR5_SiteStudyAccessAudit__c> studyToSSAAmap = new Map<Id, VTR5_SiteStudyAccessAudit__c>();
        for(VTR5_SiteStudyAccessAudit__c ssaa : existedSSAA){
            studyToSSAAmap.put(ssaa.VTR5_StudyName__c , ssaa);
        }


        //to get old date of assign on study
        List<VTR5_SiteStudyAccessAudit__c> allSSAA = [
                SELECT Id, User_Audit__c,  VTR5_StudyName__c, VTR5_SiteName__c, VTR5_SiteAccessStatus__c,
                        VTR5_AssignedDateSite__c, VTR5_SiteDeactivatedDate__c, VTR5_AssignedDateStudy__c, VTR5_StudyTeamMember__c
                FROM VTR5_SiteStudyAccessAudit__c
                WHERE User_Audit__c IN :userAuditByUser.values()
        ];

        Map<Id, VTR5_SiteStudyAccessAudit__c> stmToSSAAmap = new Map<Id, VTR5_SiteStudyAccessAudit__c>();
        for(VTR5_SiteStudyAccessAudit__c ssaa : allSSAA) {
            stmToSSAAmap.put(ssaa.VTR5_StudyTeamMember__c, ssaa);
        }
        List<VTR5_SiteStudyAccessAudit__c> ssaaToUpsert = new List<VTR5_SiteStudyAccessAudit__c>();

        // Create, Update SSAA
        for(Study_Site_Team_Member__c member : members) {

            Id userId, memberStudy, memberStm, memberSiteId;
            if(member.VTR2_Associated_SCr__c != null){
                userId = associatedSTM.get(member.VTR2_Associated_SCr__c).User__r.Id;
                memberStudy = associatedSTM.get(member.VTR2_Associated_PI__c).Study__c;
                memberSiteId = associatedSTM.get(member.VTR2_Associated_PI__c).VTD1_VirtualSite__c;

                memberStm = member.VTR2_Associated_SCr__c;
            }else if(member.VTD1_Associated_PG__c != null){
                userId = associatedSTM.get(member.VTD1_Associated_PG__c).User__r.Id;
                memberStudy = associatedSTM.get(member.VTD1_Associated_PI__c).Study__c;

                memberSiteId = associatedSTM.get(member.VTD1_Associated_PI__c).VTD1_VirtualSite__c;

                memberStm = member.VTD1_Associated_PG__c;
            }else if(member.VTR2_Associated_SubI__c != null){
                userId = associatedSTM.get(member.VTR2_Associated_SubI__c).User__r.Id;
                memberStudy = associatedSTM.get(member.VTR2_Associated_PI3__c).Study__c;

                memberSiteId = associatedSTM.get(member.VTR2_Associated_PI3__c).VTD1_VirtualSite__c;
                memberStm = member.VTR2_Associated_SubI__c;
            }
            //Start SH-17965 add the CRA as well for audit Report
            else if(member.VTR5_Associated_CRA__c != null){
                userId = associatedSTM.get(member.VTR5_Associated_CRA__c).User__r.Id;
                memberStudy = associatedSTM.get(member.VTR5_Associated_CRA__c).Study__c;
                memberSiteId = member.VTD1_SiteID__c;
                memberStm = member.VTR5_Associated_CRA__c;
            } // END SH-17965

            VTR5_SiteStudyAccessAudit__c a = studyToSSAAmap.get(memberStudy);
            Id userAuditId = userAuditByUser.get(userId).Id;

            VTR5_SiteStudyAccessAudit__c auditToUpdate;
            if (a == null){
                auditToUpdate = new VTR5_SiteStudyAccessAudit__c(
                        VTR5_StudyTeamMember__c = memberStm,
                        VTR5_StudyName__c = memberStudy,
                        VTR5_StudyAccessStatus__c = true,
                        VTR5_AssignedDateStudy__c = stmToSSAAmap.get(memberStm) != null ? stmToSSAAmap.get(memberStm).VTR5_AssignedDateStudy__c : Datetime.now(),
                        User_Audit__c = userAuditId
                );
            } else if (a.VTR5_StudyName__c != null && a.User_Audit__c == userAuditId && a.VTR5_SiteDeactivatedDate__c == null
                    && (a.VTR5_SiteName__c == memberSiteId || a.VTR5_SiteName__c == null)) {
                //Check That SSAA exist on Virtual Site
                auditToUpdate = a;

            } else {
                auditToUpdate = new VTR5_SiteStudyAccessAudit__c(
                        VTR5_StudyTeamMember__c = memberStm,
                        VTR5_StudyName__c = memberStudy,
                        VTR5_StudyAccessStatus__c = true,
                        VTR5_AssignedDateStudy__c = stmToSSAAmap.get(memberStm) != null ? stmToSSAAmap.get(memberStm).VTR5_AssignedDateStudy__c : Datetime.now(),
                        User_Audit__c = userAuditByUser.get(userId).Id
                );
            }

            // fill site fields
            if (memberSiteId != null) {
                auditToUpdate.VTR5_SiteAccessStatus__c = true;
                auditToUpdate.VTR5_SiteName__c = memberSiteId;
                auditToUpdate.VTR5_AssignedDateSite__c = Datetime.now();
            }
            ssaaToUpsert.add(auditToUpdate);
        }
        // SH - 17965 Audit Report issue
       try{
           upsert ssaaToUpsert;
       } catch (Exception ex) {
           ErrorLogUtility.logException(ex, null, VT_R5_SSTMauditHandler.class.getName());
       } // End Sh - 17965

    }

    public static void onBeforeDeleteSstm(List<Study_Site_Team_Member__c> members) {

        Map<Id, Study_Team_Member__c> associatedSTM = new Map<Id, Study_Team_Member__c>();
        for (Study_Site_Team_Member__c member : members) {
            associatedSTM.put(member.VTR2_Associated_PI3__c, null);
            associatedSTM.put(member.VTD1_Associated_PI__c, null);
            associatedSTM.put(member.VTR2_Associated_PI__c, null);
            associatedSTM.put(member.VTR2_Associated_SCr__c, null);
            associatedSTM.put(member.VTD1_Associated_PG__c, null);
            associatedSTM.put(member.VTR2_Associated_SubI__c, null);
            associatedSTM.put(member.VTR5_Associated_CRA__c, null); // Start SH - 17965 Add the CRA user as well for the Audit Object

        }
        associatedSTM.remove(null);
        associatedSTM.putAll([SELECT Id, Study__c, VTD1_VirtualSite__c, User__r.Id  FROM Study_Team_Member__c WHERE Id IN :associatedSTM.keySet()]);

        // get existing User Audits for users or create new
        Set<Id> userIds = new Set<Id>();
        Set<Id> membersIds = new Set<Id>();
        Map<Id, Study_Team_Member__c> userByStmMap = new Map<Id, Study_Team_Member__c>();
        for (Study_Site_Team_Member__c member : members) {
            if(member.VTR2_Associated_SCr__c != null){
                userByStmMap.put(associatedSTM.get(member.VTR2_Associated_SCr__c).User__r.Id,associatedSTM.get(member.VTR2_Associated_PI__c));
            }else if(member.VTD1_Associated_PG__c != null){
                userByStmMap.put(associatedSTM.get(member.VTD1_Associated_PG__c).User__r.Id, associatedSTM.get(member.VTD1_Associated_PI__c));
            }else if(member.VTR2_Associated_SubI__c != null){
                userByStmMap.put(associatedSTM.get(member.VTR2_Associated_SubI__c).User__r.Id, associatedSTM.get(member.VTR2_Associated_PI3__c));
            }
            // Start SH - 17965 Add the CRA user as well for the Audit Object
            else if(member.VTR5_Associated_CRA__c != null){
                userByStmMap.put(associatedSTM.get(member.VTR5_Associated_CRA__c).User__r.Id, associatedSTM.get(member.VTR5_Associated_CRA__c));
            } // End SH - 17965
        }

        List<VTR5_SiteStudyAccessAudit__c> ssaaToUpdate = [
                SELECT Id, User_Audit__c,  VTR5_StudyName__c, VTR5_SiteName__c, VTR5_SiteAccessStatus__c,
                        VTR5_AssignedDateSite__c, VTR5_SiteDeactivatedDate__c, VTR5_StudyTeamMember__c, User_Audit__r.User__c
                FROM VTR5_SiteStudyAccessAudit__c
                WHERE User_Audit__r.User__c IN :userByStmMap.keySet()
                AND VTR5_SiteDeactivatedDate__c = NULL
        ];

        for(VTR5_SiteStudyAccessAudit__c ssaa : ssaaToUpdate){
            Study_Team_Member__c stm = userByStmMap.get(ssaa.User_Audit__r.User__c);
            if (stm.VTD1_VirtualSite__c == ssaa.VTR5_SiteName__c && stm.Study__c == ssaa.VTR5_StudyName__c && ssaa.VTR5_SiteAccessStatus__c) {
                ssaa.VTR5_SiteDeactivatedBy__c = UserInfo.getName();
                ssaa.VTR5_SiteDeactivatedDate__c = Datetime.now();
                ssaa.VTR5_SiteAccessStatus__c = false;
            }
        }

        // SH - 17965 Audit Report issue
        try {
            update ssaaToUpdate;
        } catch (Exception ex) {
            ErrorLogUtility.logException(ex, null, VT_R5_SSTMauditHandler.class.getName());
        } // End SH - 17965

    }


    /**
     * Get or create user audits for existing users
     * @param userIds - set of userIds to process
     * @return - map of each userId and its UserAudit record
     */
    public static Map<Id, VTR5_UserAudit__c> getOrCreateUserAudits(Set<Id> userIds) {
        Map<Id, VTR5_UserAudit__c> userAuditByUser = new Map<Id, VTR5_UserAudit__c>();

        // generate a map of existing User Audits
        List<VTR5_UserAudit__c> userAudits = [
                SELECT Id, User__c
                FROM VTR5_UserAudit__c
                WHERE User__c IN :userIds
        ];
        for (VTR5_UserAudit__c userAudit : userAudits) {
            userAuditByUser.put(userAudit.User__c, userAudit);
        }

        // generate and insert a list of new User Audits
        List<VTR5_UserAudit__c> userAuditsToInsert = new List<VTR5_UserAudit__c>();
        for (Id userId : userIds) {
            if (userAuditByUser.get(userId) == null) {
                VTR5_UserAudit__c userAudit = new VTR5_UserAudit__c(User__c = userId);
                userAuditByUser.put(userAudit.User__c, userAudit);
                userAuditsToInsert.add(userAudit);
            }
        }
        insert userAuditsToInsert;

        return userAuditByUser;
    }



    public static Map<Id, VTR5_UserAudit__c> getUserAudits(Set<Id> userIds) {
        Map<Id, VTR5_UserAudit__c> userAuditByUser = new Map<Id, VTR5_UserAudit__c>();

        // generate a map of existing User Audits
        List<VTR5_UserAudit__c> userAudits = [
                SELECT Id, User__c
                FROM VTR5_UserAudit__c
                WHERE User__c IN :userIds
        ];
        for (VTR5_UserAudit__c userAudit : userAudits) {
            userAuditByUser.put(userAudit.User__c, userAudit);
        }
        return userAuditByUser;
    }

    public void createUserAuditForEveryNewUser(List<User> newUsers) {
        List<VTR5_UserAudit__c> uaToCreate = new List<VTR5_UserAudit__c>();
        for (User u : newUsers) {
            if (u.IsActive) {
                VTR5_UserAudit__c ua = new VTR5_UserAudit__c();
                ua.User__c = u.Id;
                uaToCreate.add(ua);
            }
        }
        if (!uaToCreate.isEmpty()) {
            if (System.isBatch() && VT_R4_PatientConversion.patientConversionInProgress) {
                userAuditsToInsert.addAll(uaToCreate);
            } else {
                insert uaToCreate;
            }
        }

    }



    public static Map<Id, VTR5_UserAudit__c> getOrCreateUserAudits(List<Study_Team_Member__c> stms) {
        Set<Id> userIds = new Set<Id>();
        for (Study_Team_Member__c stm : stms) {
            userIds.add(stm.User__c);
        }
        return VT_R5_CustomAuditHandler_Misc.getOrCreateUserAudits(userIds);
    }

    public static Map<Id, VTR5_UserAudit__c> getOrCreateUserAudits(List<VT_R5_CustomAuditHandler_STM.StmWrapper> stmWrappers) {
        Set<Id> userIds = new Set<Id>();
        for (VT_R5_CustomAuditHandler_STM.StmWrapper stmWrapper : stmWrappers) {
            if (stmWrapper.stm != null && stmWrapper.stm.User__c != null) {
                userIds.add(stmWrapper.stm.User__c);
            }
        }
        return VT_R5_CustomAuditHandler_Misc.getOrCreateUserAudits(userIds);
    }

    public static Map<Id, VTR5_UserAudit__c> getOrCreateUserAuditsByStm(Set<Id> stmIds) {
        List<Study_Team_Member__c> stms = [
                SELECT Id, User__c
                FROM Study_Team_Member__c
                WHERE Id IN :stmIds
        ];
        return VT_R5_CustomAuditHandler_Misc.getOrCreateUserAudits(stms);
    }


}