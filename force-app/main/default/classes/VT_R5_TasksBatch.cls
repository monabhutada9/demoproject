/**
 * Created by shume on 11/1/2020.
 */

public with sharing class VT_R5_TasksBatch implements Database.Batchable<SObject>, Database.Stateful {

    public static Datetime currentTime;
    public static Datetime startWindow;
    public static Datetime endWindow;
    public Integer batchSize = 50;

    public VT_R5_TasksBatch (Integer batchSize) {
        this.batchSize = batchSize;
    }

    public Iterable<SObject> start(Database.BatchableContext context) {
        currentTime = System.now();
        startWindow = currentTime.addHours(-24 * 4);
        endWindow = currentTime.addHours(-23);
        return [SELECT Id,
                Type,
                VTD2_Task_Unique_Code__c,
                Status,
                CreatedDate,
                OwnerId,
                PG_reminder_date__c,
                VTD1_Case_lookup__c,
                VTD1_Case_lookup__r.VTD1_Study__r.VTR2_Site_Staff_Responsible_for_Elgbl__c,
                VTD1_Case_lookup__r.VTD1_Primary_PG__c,
                VTD1_Case_lookup__r.VTR2_SiteCoordinator__c,
                VTD1_Type_for_backend_logic__c
                FROM Task
        WHERE Status = 'Open' AND CreatedDate >= :startWindow AND CreatedDate < :endWindow];
    }

    public void execute(List<SObject> tasks) {
        List<String> tnCodesNotifications = new List<String>();
        List<String> tnCodesTasks = new List<String>();
        List<Id> sourceIdsNotifications = new List<Id>();
        List<Id> sourceIdsTasks = new List<Id>();
        Map<String, String> receiverMap = new Map<String, String> ();
        List<Id> caseIdsToQueryScreeningVisits = new List<Id>();
        Map<Id, WrapperCase> taskCodeCaseMap = new Map<Id, WrapperCase>();
        for (Task t : (List<Task>) tasks) {
            System.debug('task code ' + t.VTD2_Task_Unique_Code__c);
            if (t.CreatedDate >= currentTime.addHours(-24) && t.CreatedDate < currentTime.addHours(-23)) { // 24 hours after creation
                if (t.VTD2_Task_Unique_Code__c == 'T612') {
                    //  Task for Certification Form PB
                    tnCodesTasks.add('T611');
                    sourceIdsTasks.add(t.Id);
                } else if (t.VTD1_Type_for_backend_logic__c == 'Final Eligibility Decision Task'
                        || t.VTD1_Type_for_backend_logic__c == 'Sign Patient Eligibility Assessment Form task') {
                    // RAND2 Follow Up tasks for PG for TMA and PI review PB
                    Id scrId = t.VTD1_Case_lookup__r.VTR2_SiteCoordinator__c;
                    Id pgId = t.VTD1_Case_lookup__r.VTD1_Primary_PG__c;
                    String taskCode = t.VTD1_Type_for_backend_logic__c == 'Final Eligibility Decision Task' ? '508' : 'T478';
                    if (t.VTD1_Case_lookup__r.VTD1_Study__r.VTR2_Site_Staff_Responsible_for_Elgbl__c) {
                        if (scrId != null) {
                            tnCodesTasks.add(taskCode);
                            sourceIdsTasks.add(t.Id);
                            receiverMap.put(taskCode + t.Id, scrId);

                            tnCodesNotifications.add('N478');
                            sourceIdsNotifications.add(t.Id);
                        } else if (pgId != null) {
                            tnCodesTasks.add(taskCode);
                            sourceIdsTasks.add(t.Id);
                        }
                    } else {
                        if (scrId != null && pgId != null) {
                            caseIdsToQueryScreeningVisits.add(t.VTD1_Case_lookup__c);
                            WrapperCase taskCodes = taskCodeCaseMap.get(t.VTD1_Case_lookup__c);
                            if (taskCodes == null) {
                                taskCodes = new WrapperCase ();
                                taskCodes.taskCode1 = taskCode;
                                taskCodes.taskId = t.Id;
                                taskCodeCaseMap.put(t.VTD1_Case_lookup__c, taskCodes);
                            } else {
                                taskCodes.taskCode2 = taskCode;
                            }
                        } else if (pgId != null) {
                            tnCodesTasks.add(taskCode);
                            sourceIdsTasks.add(t.Id);
                        } else if (scrId != null) {
                            tnCodesTasks.add(taskCode);
                            sourceIdsTasks.add(t.Id);
                            receiverMap.put(taskCode + t.Id, scrId);

                            tnCodesNotifications.add('N478');
                            sourceIdsNotifications.add(t.Id);
                        }
                    }
                } else if (t.Type == 'Review Lab Results') {
                    //KITS - Task Support Process //  Review Lab Results task is Open
                    if (t.VTD1_Case_lookup__r.VTD1_Primary_PG__c != null) {
                        tnCodesTasks.add('223');
                        sourceIdsTasks.add(t.Id);
                    } else if (t.VTD1_Case_lookup__r.VTR2_SiteCoordinator__c != null) {
                        tnCodesTasks.add('222');
                        sourceIdsTasks.add(t.Id);

                        tnCodesNotifications.add('N028');
                        sourceIdsNotifications.add(t.Id);
                    }
                } else if (t.VTD2_Task_Unique_Code__c == '203') {
                    //KITS - Task Support Process // no Lab/IMP address confirmation from patient
                    tnCodesTasks.add('T542');
                    sourceIdsTasks.add(t.Id);
                } else if (t.VTD2_Task_Unique_Code__c == '212' || t.VTD2_Task_Unique_Code__c == 'T536') {
                    //KITS - Task Support Process // no Lab/IMP PM address confirmation from patient
                    tnCodesTasks.add('T561');
                    sourceIdsTasks.add(t.Id);
                }
            }  else if (t.CreatedDate >= currentTime.addHours(-24 * 4) && t.CreatedDate < currentTime.addHours(-24 * 4 + 1)) { // 4 days after status changed
                // Task for VTSL if the PG/PI didn't update his calendar PB
                if (t.VTD1_Type_for_backend_logic__c == 'Update Calendar') {
                    tnCodesTasks.add('T610');
                    sourceIdsTasks.add(t.OwnerId);
                } else if (t.VTD2_Task_Unique_Code__c == '203') {
                    //KITS - Task Support Process // no Lab/IMP address confirmation from patient
                    tnCodesTasks.add('311');
                    sourceIdsTasks.add(t.OwnerId);
                }
            } else if (t.CreatedDate >= currentTime.addHours(-24 * 3) && t.CreatedDate < currentTime.addHours(-24 * 3 + 1)) { // 3 days after status changed
                if (t.VTD2_Task_Unique_Code__c == '203') {
                    //KITS - Task Support Process // no Lab/IMP address confirmation from patient
                    tnCodesTasks.add('T560');
                    sourceIdsTasks.add(t.OwnerId);
                }
            } else if (t.CreatedDate >= currentTime.addHours(-24 * 2) && t.CreatedDate < currentTime.addHours(-24 * 2 + 1)) { // 2 days after status changed
                if (t.VTD2_Task_Unique_Code__c == '203') {
                    //KITS - Task Support Process // no Lab/IMP address confirmation from patient
                    tnCodesTasks.add('T558');
                    sourceIdsTasks.add(t.OwnerId);
                }
            }
        }
        if (!caseIdsToQueryScreeningVisits.isEmpty()) {
            List<VTD1_Actual_Visit__c> screeningVisits = [
                    SELECT  Id, VTR2_Modality__c, VTD1_Case__c,
                            VTD1_Case__r.VTR2_SiteCoordinator__c, VTD1_Case__r.VTD1_Primary_PG__c
                    FROM VTD1_Actual_Visit__c
                    WHERE VTD1_Case__c IN :caseIdsToQueryScreeningVisits AND VTD1_Onboarding_Type__c = 'Screening'];
            for (VTD1_Actual_Visit__c av : screeningVisits) {
                WrapperCase wrapperCase = taskCodeCaseMap.get(av.VTD1_Case__c);
                if (av.VTR2_Modality__c == 'At Location') {
                    tnCodesTasks.add(wrapperCase.taskCode1);
                    sourceIdsTasks.add(wrapperCase.taskId);
                    receiverMap.put(wrapperCase.taskCode1 + wrapperCase.taskId, av.VTD1_Case__r.VTR2_SiteCoordinator__c);

                    tnCodesNotifications.add('N478');
                    sourceIdsNotifications.add(wrapperCase.taskId);
                    if (wrapperCase.taskCode2 != null) {
                        tnCodesTasks.add(wrapperCase.taskCode2);
                        sourceIdsTasks.add(wrapperCase.taskId);
                        receiverMap.put(wrapperCase.taskCode2 + wrapperCase.taskId, av.VTD1_Case__r.VTR2_SiteCoordinator__c);
                    }
                } else {
                    tnCodesTasks.add(wrapperCase.taskCode1);
                    sourceIdsTasks.add(wrapperCase.taskId);

                    tnCodesTasks.add(wrapperCase.taskCode2);
                    sourceIdsTasks.add(wrapperCase.taskId);
                }
            }
        }
        if (!tnCodesTasks.isEmpty()) {
            System.debug('tnCodesTasks ' + tnCodesTasks);
            System.debug(sourceIdsTasks);
            System.debug(receiverMap);
            VT_D2_TNCatalogTasks.generateTasks(tnCodesTasks, sourceIdsTasks, null, receiverMap);
        }
        if (!tnCodesNotifications.isEmpty()) {
            System.debug('tnCodesNotifications' + tnCodesNotifications);
            System.debug(sourceIdsNotifications);
            VT_D2_TNCatalogNotifications.generateNotifications(tnCodesNotifications, sourceIdsNotifications, new Map<String, String>());
        }
    }

    public void execute(Database.BatchableContext param1, List<SObject> tasks) {
        execute(tasks);
    }

    public void finish(Database.BatchableContext param1) {
        Integer nextRunIntervalMinutes = Math.round((System.now().getTime() - currentTime.getTime()) / 1000 / 60);
        launch(this.batchSize, nextRunIntervalMinutes);
    }

    public void launch (Integer batchSize, Integer nextRunIntervalMinutes) {
        System.scheduleBatch(new VT_R5_TasksBatch (batchSize), 'VT_R5_TasksBatch ' + System.now(), nextRunIntervalMinutes, batchSize);
    }

    private class WrapperCase {
        Id taskId;
        String taskCode1;
        String taskCode2;
    }
}