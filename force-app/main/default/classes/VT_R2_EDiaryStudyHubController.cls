public with sharing class VT_R2_EDiaryStudyHubController {
	private static final String STATUS_REVIEW_REQUIRED ='Review Required';
	private static final Boolean IS_SCORE_UPDATEABLE = isUpdateable('VTD1_Survey_Answer__c', 'VTD1_Score__c');
	private static final Boolean IS_STATUS_UPDATEABLE = isUpdateable('VTD1_Survey__c', 'VTD1_Status__c');
	private static final Boolean IS_REVIEWED_UPDATEABLE = isUpdateable('VTD1_Survey__c', 'VTR5_Reviewed__c');
	public class EDiaryData {
		public List<VTD1_Survey_Answer__c> answers;
		public VTD1_Survey__c survey;
		public List<GroupedAnswerWrapper> answersWithGroups;
		public Boolean isScoreUpdateable = IS_SCORE_UPDATEABLE;
		public Boolean isStatusUpdateable = IS_STATUS_UPDATEABLE;
		public Boolean isReviewUpdateable = IS_REVIEWED_UPDATEABLE;
		public Boolean userIsReviewer;
	}
	public class QGroupWrapper {
		Id id;
		String name;
		Decimal score;
		Decimal page;
		Decimal numberOnPage;
		public QGroupWrapper (Id id, String name, Decimal score, Decimal page, Decimal numberOnPage) {
			this.id = id;
			this.name = name;
			this.score = score;
			this.page = page;
			this.numberOnPage = numberOnPage;
		}
	}
	public class GroupedAnswerWrapper {
		VTD1_Survey_Answer__c answer;
		List<QGroupWrapper> groups;
	}
	@AuraEnabled
	public static String getData(Id surveyId) {
		EDiaryData eDiaryData = new EDiaryData();
		VTD1_Survey__c survey = getSurvey(surveyId);
		eDiaryData.survey = survey;
		Map<Id, VTD1_Survey_Answer__c> answersMap = getAnswers(surveyId);
		List<VTD1_Survey_Answer__c> answers = answersMap.values();
		VT_D1_TranslateHelper.translate(answers, survey.VTR2_DocLanguage__c);
		eDiaryData.answers = answers;
		List<GroupedAnswerWrapper> groupedAnswers = getGroupedAnswers(answersMap);
		eDiaryData.answersWithGroups = groupedAnswers;
		eDiaryData.userIsReviewer = isReviewer();
		return JSON.serialize(eDiaryData);
	}

	public static Boolean isReviewer () {
		String userProfileName = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
		return VT_R4_ConstantsHelper_ProfilesSTM.SITE_STAFF.contains(userProfileName);
	}

	public static Map<Id, VTD1_Survey_Answer__c> getAnswers(Id surveyId) {
		return new Map<Id, VTD1_Survey_Answer__c>([
				SELECT VTD1_Question__r.VTR2_QuestionContentLong__c,
						VTD1_Question__r.VTR4_QuestionContentRichText__c,
						VTD1_Question__r.VTR2_Scoring_Type__c,
						VTD1_Question__r.VTR2_AllowOverridingAutoscore__c,
						VTD1_Question__r.VTR4_Branch_Parent__c,
						VTD1_Question__r.VTR4_Response_Trigger__c,
						VTD1_Question__r.VTR4_Number__c,
						VTD1_Question__r.VTD1_Options__c,
						VTD1_Question__r.VTD1_Type__c,
						VTD1_Answer__c, VTD1_Score__c, VTD1_Question__c,
						VTR5_External_Question__c,
						VTR5_External_Question_Number__c,
						RecordType.Name,
						RecordType.DeveloperName,
						RecordTypeId
				FROM VTD1_Survey_Answer__c
				WHERE VTD1_Survey__c =:surveyId
				ORDER BY VTR5_External_Question_Number__c
		]);
	}
	public static VTD1_Survey__c getSurvey(Id surveyId) {
		VTD1_Survey__c survey;
		try {
			List<VTD1_Survey__c> surveys = [
				SELECT Name, VTD1_Status__c, VTD1_Number_of_Answers__c, VTR2_Number_of_Answered_Answers__c,
					VTR2_Reviewer_User__c, VTD1_CSM__r.VTD1_Primary_PG__c, VTD1_CSM__r.VTR2_SiteCoordinator__c,
						VTR2_DocLanguage__c, VTD1_Protocol_ePRO__r.VTR4_Is_Branching_eDiary__c, RecordType.DeveloperName,
						VTR5_CompletedbyCaregiver__c, VTR5_Clinician__c, LastModifiedDate, VTR5_Reviewed__c
				FROM VTD1_Survey__c
				WHERE Id = :surveyId
				LIMIT 1
			];
			if (!surveys.isEmpty()) {
				survey = surveys[0];
			}
		} catch (Exception e) {
			System.debug(e.getMessage()+' '+e.getStackTraceString());
		}
		return survey;
	}
	public static List<GroupedAnswerWrapper> getGroupedAnswers (Map<Id, VTD1_Survey_Answer__c> answersMap) {
		List<GroupedAnswerWrapper> groupedAnswers = new List<GroupedAnswerWrapper>();
		Map<Id, List<QGroupWrapper>> eDiaryGroupAnswerMap = new Map<Id, List<QGroupWrapper>>();
		for (VTR2_Patient_eDiary_Group_Answer__c eDiaryGroupAnswer : [
				SELECT Id,
						VTR2_eDiary_Answer__c,
						VTR2_Patient_eDiary_Group__c,
						VTR2_Patient_eDiary_Group__r.Name,
						VTR2_Patient_eDiary_Group__r.VTR2_Score__c,
						VTR2_Patient_eDiary_Group__r.VTR2_Protocol_eDiary_Group__r.VTR2_Page_Number__c,
						VTR2_Patient_eDiary_Group__r.VTR2_Protocol_eDiary_Group__r.VTR2_Number_Within_Page__c
				FROM VTR2_Patient_eDiary_Group_Answer__c
		]) {
			if (!eDiaryGroupAnswerMap.containsKey(eDiaryGroupAnswer.VTR2_eDiary_Answer__c)) {
				eDiaryGroupAnswerMap.put(eDiaryGroupAnswer.VTR2_eDiary_Answer__c, new List<QGroupWrapper>());
			}
			eDiaryGroupAnswerMap.get(eDiaryGroupAnswer.VTR2_eDiary_Answer__c).add(
					new QGroupWrapper(
							eDiaryGroupAnswer.VTR2_Patient_eDiary_Group__c,
							eDiaryGroupAnswer.VTR2_Patient_eDiary_Group__r.Name,
							eDiaryGroupAnswer.VTR2_Patient_eDiary_Group__r.VTR2_Score__c,
							eDiaryGroupAnswer.VTR2_Patient_eDiary_Group__r.VTR2_Protocol_eDiary_Group__r.VTR2_Page_Number__c,
							eDiaryGroupAnswer.VTR2_Patient_eDiary_Group__r.VTR2_Protocol_eDiary_Group__r.VTR2_Number_Within_Page__c
					)
			);
		}
		for (VTD1_Survey_Answer__c answer : answersMap.values()) {
			GroupedAnswerWrapper a = new GroupedAnswerWrapper();
			a.answer = answer;
			a.groups = eDiaryGroupAnswerMap.get(answer.Id);
			groupedAnswers.add(a);
		}
		return groupedAnswers;
	}

	@AuraEnabled
	public static void updateScore(Id answerId, String score) {
		try {
			List<VTD1_Survey_Answer__c> answersToUpdate = new List<VTD1_Survey_Answer__c>();
			if (answerId!=null && IS_SCORE_UPDATEABLE) {
				answersToUpdate = [
					SELECT VTD1_Score__c
					FROM VTD1_Survey_Answer__c
					WHERE Id = :answerId
					AND (VTD1_Question__r.VTR2_Scoring_Type__c = 'Manual' OR (VTD1_Question__r.VTR2_Scoring_Type__c = 'Auto' AND VTD1_Question__r.VTR2_AllowOverridingAutoscore__c = TRUE))
					AND VTD1_Survey__r.VTD1_Status__c = :STATUS_REVIEW_REQUIRED
						LIMIT 1
				];
			}
			if (!answersToUpdate.isEmpty()) {
				Decimal scoreDecimal = (score == null || score == '') ? null : Decimal.valueOf(score);
				answersToUpdate[0].VTD1_Score__c = scoreDecimal;
				update answersToUpdate;
			}
		} catch (Exception e) {
			System.debug(e.getMessage()+' '+e.getStackTraceString());
		}
	}
	@AuraEnabled
	public static void markReviewed(Id surveyId) {
		try {
			List<VTD1_Survey__c> surveys = new List<VTD1_Survey__c>();
			if (surveyId!=null && (IS_STATUS_UPDATEABLE || IS_REVIEWED_UPDATEABLE)) {
				surveys = [
						SELECT 	Id, VTD1_Number_of_Answers__c, VTR2_Number_of_Answered_Answers__c,
								VTD1_Protocol_ePRO__r.VTR4_Is_Branching_eDiary__c, VTR4_Percentage_Completed__c,
								RecordType.DeveloperName, VTR5_Reviewed__c
						FROM VTD1_Survey__c
						WHERE Id = :surveyId
				];
			}
			if (!surveys.isEmpty()) {
				VTD1_Survey__c surveyToUpdate = new VTD1_Survey__c(
						Id = surveyId
				);
				if (IS_STATUS_UPDATEABLE && surveys[0].RecordType.DeveloperName != 'VTR5_External') {
					surveyToUpdate.VTD1_Status__c = 'Reviewed';
					if (surveys[0].RecordType.DeveloperName == 'ePRO'
							&& surveys[0].VTR4_Percentage_Completed__c != 100
							&& surveys[0].VTD1_Number_of_Answers__c > surveys[0].VTR2_Number_of_Answered_Answers__c) {
						surveyToUpdate.VTD1_Status__c = 'Reviewed (Partially Completed)';
					}
				}
				if (surveys[0].RecordType.DeveloperName == 'VTR5_External'
						&& IS_REVIEWED_UPDATEABLE) {
					surveyToUpdate.VTR5_Reviewed__c = true;
				}
				update surveyToUpdate;
			}
		} catch (Exception e) {
			System.debug(e.getMessage()+' '+e.getStackTraceString());
		}
	}
	public static Boolean isUpdateable(String sObjectType, String fieldName){
		SObjectType schemaType = Schema.getGlobalDescribe().get(sObjectType);
		Map<String, SObjectField> fields = schemaType.getDescribe().fields.getMap();
		DescribeFieldResult fieldDescribe = fields.get(fieldName).getDescribe();
		return fieldDescribe.isUpdateable();
	}
}