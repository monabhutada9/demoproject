/**
 * Created by dmitry on 30.10.2020.
 */

public with sharing class VT_R5_eDiaryAlertsService implements VT_R5_HerokuScheduler.HerokuExecutable {

    public enum RecipientType { SITE_STAFF, PT_CG }

    public class AlertRequestInfo {
        public Id alertBuilderId { public get; private set; }
        public Datetime dueDate { public get; private set; }
        public RecipientType recipient { public get; private set; }
        public Integer timezoneOffset { public get; private set; }
        public Boolean diaryBased { public get; private set; }

        public AlertRequestInfo(Id alertBuilderId, RecipientType recipient, Integer timezoneOffset, Boolean diaryBased) {
            this.alertBuilderId = alertBuilderId;
            this.recipient = recipient;
            this.timezoneOffset = timezoneOffset;
            this.diaryBased = diaryBased;
        }

        public AlertRequestInfo(Id alertBuilderId, RecipientType recipient, Datetime dueDate) {
            this.alertBuilderId = alertBuilderId;
            this.recipient = recipient;
            this.dueDate = dueDate;
            this.diaryBased = true;
        }
    }

    public VT_R5_eDiaryAlertsService() {}

    public void herokuExecute(String payload) {
        System.debug('test class herokuexecute---');
        List<AlertRequestInfo> alerts = (List<AlertRequestInfo>) JSON.deserialize(payload, List<AlertRequestInfo>.class);
        if (!alerts.isEmpty()) {
            System.debug('test class alerts not empty---');
            processAlerts(alerts);
        }
    }

    private static void processAlerts(List<AlertRequestInfo> alerts) {
        System.debug('test class processalerts---');
        Map<Id, AlertRequestInfo> siteStaffAlerts = new Map<Id, AlertRequestInfo>();
        Map<Id, AlertRequestInfo> ptCgAlertsMap = new Map<Id, AlertRequestInfo>();
        for (AlertRequestInfo alert : alerts) {
            if (alert.recipient == RecipientType.SITE_STAFF) {
                 System.debug('test class alert rec sitestaff---');
                siteStaffAlerts.put(alert.alertBuilderId, alert);
            } else {
                ptCgAlertsMap.put(alert.alertBuilderId, alert);
            }
        }
        if (!siteStaffAlerts.isEmpty()) {
            System.debug('test class alert sitestaff not empty---');
          VT_R5_AbstractEDiaryAlertsBatch batch = new VT_R5_eDiaryAlertsSiteStaffBatch(siteStaffAlerts,'Site_Staff'); //5.7 Epic: SH-20625
           Database.executeBatch(batch, batch.getBatchScopeSize());
        }
        if (!ptCgAlertsMap.isEmpty()) {
           VT_R5_AbstractEDiaryAlertsBatch batch = new VT_R5_eDiaryAlertsPtCgBatch(ptCgAlertsMap, 'Pt_Cg'); //5.7 Epic: SH-20625
           Database.executeBatch(batch, batch.getBatchScopeSize());
        }
    }

}