/**
 * Created by Aleksandr Mazan on 11-Nov-20.
 * Only for once update contact.
 */

public with sharing class VT_R5_SetPrimaryPhoneOnContactBatch implements Database.Batchable<SObject> {
    public List<VT_D1_Phone__c> start(Database.BatchableContext BC) {
        return [
                SELECT Id, PhoneNumber__c, VTD1_Contact__c
                FROM VT_D1_Phone__c
                WHERE IsPrimaryForPhone__c = true
        ];
    }

    public void execute(Database.BatchableContext BC, List<VT_D1_Phone__c> phones) {
        List<Contact> contactsForUpdate = new List<Contact>();
        for (VT_D1_Phone__c phone : phones) {
            contactsForUpdate.add(new Contact(Id = phone.VTD1_Contact__c, VTR5_PatientPhone__c = phone.PhoneNumber__c));
        }
        Database.update(contactsForUpdate, false);
    }

    public void finish(Database.BatchableContext BC) {
        System.debug('VT_R5_SetPrimaryPhoneOnContactBatch::finish');
    }
}