global class VTD1_TokboxHelper {
    @Future(Callout=true)
    public static void session(Set<Id> vcs) {
        String herokuAppURL = ExternalVideoController.getHerokuAppURL();
        if (String.isBlank(herokuAppURL)) return;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(herokuAppURL + '/api/session');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');

        req.setBody('{"count" :' + vcs.size() + '}');

        Http http = new Http();
        HttpResponse res = http.send(req);

        Map<String, Object> response = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        List<Object> sessions = (List<Object>) response.get('message');

        Map<Id, Video_Conference__c> vcsMap = new Map<Id, Video_Conference__c>([SELECT Id FROM Video_Conference__c WHERE Id IN :vcs]);
        Integer c = 0;
        for (Id k : vcsMap.keySet()) {
            vcsMap.get(k).SessionId__c = (String) sessions[c];
            c++;
        }
        update vcsMap.values();
    }
}