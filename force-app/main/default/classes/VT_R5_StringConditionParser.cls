/**
 * Created by user on 24-Jul-20.
 */

public with sharing class VT_R5_StringConditionParser {

	final static String TYPE_OPERATOR_AND = 'AND';
	final static String TYPE_OPERATOR_OR = 'OR';
	final static String TYPE_OPERATOR_BRACES = '(';
	final static String TYPE_OPERATOR_BACK_BRACES = ')';
	final static String ONE_SPACE = ' ';
	final static String DOUBLE_SPACE = '  ';
	final static Map<String, String> LOGIC_TYPES = new Map<String, String>{
			TYPE_OPERATOR_AND => 'AND',
			TYPE_OPERATOR_OR => 'OR',
			TYPE_OPERATOR_BRACES => 'BRACES',
			TYPE_OPERATOR_BACK_BRACES => 'BACK_BRACES'
	};
	static Map<String, Map<String, String>> expressionLogic = new Map<String, Map<String, String>>();

	/**
	* @description The method parse string expression  Parse logic AND, OR, (). (ex. true or false)
	* @author Viktar Bobich
	* @param expression string expression
	* @return Boolean
	* @date 03/08/2020
	* @issue
	* @test VT_R5_StringConditionParserTest
	*/
	public static Boolean evaluate(String expression) {
		expression = expression.toUpperCase();
		if(expression == 'FALSE') {
			return false;
		}
		if(expression == 'TRUE') {
			return true;
		}
		expression = formatExpression(expression);
		fillLogic();
		return Boolean.valueOf(evaluateExpression(expression));
	}

	/**
	* @description The method parse number expression  Parse logic AND, OR, (). (ex. 1 or 2)
	* @author Viktar Bobich
	* @param numbersExpression number expression
	* @param booleanByExpressionNumberMap Map with answer (ex 1 => true)
	* @return Boolean
	* @date 03/08/2020
	* @issue
	* @test VT_R5_StringConditionParserTest
	*/
	public static Boolean evaluate(String numbersExpression, Map<Integer, Boolean> booleanByExpressionNumberMap){
		List<String> expressionList = new List<String>();
		numbersExpression = formatExpression(numbersExpression);
		expressionList = numbersExpression.split(ONE_SPACE);
		String formattedExpression = '';
		for(Integer i = 0; i < expressionList.size(); i++){
			String expressionElement = '';
			if(!LOGIC_TYPES.keySet().contains(expressionList[i]) && canParseToInteger(expressionList[i])) {
				if (booleanByExpressionNumberMap.keySet().contains(Integer.valueOf(expressionList[i]))){
					expressionElement = String.valueOf(booleanByExpressionNumberMap.get(Integer.valueOf(expressionList[i])));
				}else{
					expressionElement = 'FALSE';
				}
			}else if(LOGIC_TYPES.keySet().contains(expressionList[i])){
				expressionElement = expressionList[i];
			}
			formattedExpression += expressionElement + ' ';
		}
		return evaluate(formattedExpression);
	}

	//	Evaluate the expression
	private static String evaluateExpression(String expression) {
		for(String logicType : LOGIC_TYPES.keySet()) {
			if(expression.contains(logicType)) {
				expression = simplifyExpression(expression, LOGIC_TYPES.get(logicType));
			}
		}
		if(expression.contains(TYPE_OPERATOR_AND) || expression.contains(TYPE_OPERATOR_OR) || expression.contains(TYPE_OPERATOR_BRACES)) {
			expression = evaluateExpression(expression);
		}
		return expression;
	}

	//	Simplify the expression

	private static String simplifyExpression(String expression, String LogicType){
		Map<String, String> logic = new Map<String, String>(expressionLogic.get(LogicType));
		for(String key : logic.keySet()) {
			expression = expression.replace(key, logic.get(key));
		}
		return expression;
	}

	private static String formatExpression (String expression){
		expression = expression.replaceAll('\\(', ' ( ' ).replaceAll('\\)', ' ) ').toUpperCase();
		for (String logicType :  LOGIC_TYPES.values()){
			expression = expression.replaceAll(logicType, ONE_SPACE + logicType + ONE_SPACE);
		}
		while(expression.contains(DOUBLE_SPACE)){
			expression = expression.replaceAll(DOUBLE_SPACE, ONE_SPACE);
		}
		return expression.trim();
	}

	//	Fill AND and OR Logic

	private static void fillLogic() {
		Map<String, String> ANDLogic = new Map<String, String>();
		Map<String, String> ORLogic = new Map<String, String>();
		Map<String, String> BRACELogic = new Map<String, String>();

		// AND Logic
		ANDLogic.put('TRUE AND TRUE', 'TRUE');
		ANDLogic.put('TRUE AND FALSE', 'FALSE');
		ANDLogic.put('FALSE AND TRUE', 'FALSE');
		ANDLogic.put('FALSE AND FALSE', 'FALSE');
		expressionLogic.put(LOGIC_TYPES.get(TYPE_OPERATOR_AND), ANDLogic);

		// OR Logic
		ORLogic.put('TRUE OR TRUE', 'TRUE');
		ORLogic.put('TRUE OR FALSE', 'TRUE');
		ORLogic.put('FALSE OR TRUE', 'TRUE');
		ORLogic.put('FALSE OR FALSE', 'FALSE');
		expressionLogic.put(LOGIC_TYPES.get(TYPE_OPERATOR_OR), ORLogic);

		// Braces Logic
		BRACELogic.put('(TRUE)', 'TRUE');
		BRACELogic.put('( TRUE)', 'TRUE');
		BRACELogic.put('(TRUE )', 'TRUE');
		BRACELogic.put('( TRUE )', 'TRUE');
		BRACELogic.put('(FALSE)', 'FALSE');
		BRACELogic.put('( FALSE)', 'FALSE');
		BRACELogic.put('(FALSE )', 'FALSE');
		BRACELogic.put('( FALSE )', 'FALSE');
		expressionLogic.put(LOGIC_TYPES.get(TYPE_OPERATOR_BRACES), BRACELogic);
		expressionLogic.put(LOGIC_TYPES.get(TYPE_OPERATOR_BACK_BRACES), BRACELogic);
	}

	public static Boolean canParseToInteger(String value){
		Boolean result;
		try{
			Integer i = Integer.valueOf(value);
			result = true;
		}
		catch(Exception e){
			result = false;
		}
		return result;
	}
}