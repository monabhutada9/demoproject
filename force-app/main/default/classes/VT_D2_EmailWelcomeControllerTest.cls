/**
 * Created by user on 17-May-19.
 */
@isTest
public with sharing class VT_D2_EmailWelcomeControllerTest {
	@TestSetup
	static void setupMethod() {
//		HealthCloudGA__CarePlanTemplate__c testStudy = VT_D1_TestUtils.prepareStudy(1);
//		Test.startTest();
//		VT_D1_TestUtils.createTestPatientCandidate(1);
//		Test.stopTest();


	}

	@isTest
	public static void EmailWelcomeTest(){
		Test.startTest();
		UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
		insert r;
		User u = new User(
				ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
				FirstName = 'TestFirstName',
				LastName = 'last',
				Email = VT_D1_TestUtils.generateUniqueUserName(),
				Username = VT_D1_TestUtils.generateUniqueUserName(),
				CompanyName = 'TEST',
				Title = 'title',
				Alias = 'alias',
				TimeZoneSidKey = 'America/Los_Angeles',
				EmailEncodingKey = 'UTF-8',
				LanguageLocaleKey = 'en_US',
				LocaleSidKey = 'en_US'
		);
		insert u;



		VT_D2_EmailWelcomeController emailWelcomeController = new VT_D2_EmailWelcomeController();
		emailWelcomeController.getStudyNickname();
		emailWelcomeController.getStudyPhoneNumber();
		System.assertEquals('TestFirstName',emailWelcomeController.getFirstName());
		Test.stopTest();
	}


}