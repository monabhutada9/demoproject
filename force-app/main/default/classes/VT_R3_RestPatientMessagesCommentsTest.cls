@isTest
public with sharing class VT_R3_RestPatientMessagesCommentsTest {
	static VT_R3_RestHelper helper = new VT_R3_RestHelper();

	@IsTest(SeeAllData=true)
	public static void getCommentsTest() {
		FeedItem feedItem = new FeedItem(
				parentId = UserInfo.getUserId(),
				body = 'Test post'
		);
		insert feedItem;
		FeedComment comment = new FeedComment(
				FeedItemId = feedItem.Id,
				CommentBody = 'commet message',
				CommentType = 'ContentComment'
		);
		insert comment;
		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();
		request.requestURI = '/Patient/Messages/0016F00002R604KQAR';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response  = response;
		String responseString = VT_R3_RestPatientMessagesComments.sendComment('Test message', null, null);
		System.assertEquals(helper.forAnswerForIncorrectInput(), responseString);
		System.assertEquals(400, response.statusCode);

		RestContext.request.requestURI = '/Patient/Messages/' + feedItem.Id;
		responseString = VT_R3_RestPatientMessagesComments.sendComment('Test message', null, null);

		String bigString = 'bigstring';
		while (bigString.length() < 10001){
			bigString = bigString + 'bigString';
		}
		responseString = VT_R3_RestPatientMessagesComments.sendComment(bigString, null, null);
		bigString = '';
		Map<String, Object>responseMap = (Map<String, Object>)JSON.deserializeUntyped(responseString);
		System.assertEquals('FAILURE.  Your comment can not have more than 10000 characters.', responseMap.get('serviceResponse'));
		System.assertEquals(400, response.statusCode);

		while (bigString.length()< 1000001){
			bigString = bigString + '11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111';
		}
		bigString = bigString + bigString + bigString + bigString + bigString;
		responseString = VT_R3_RestPatientMessagesComments.sendComment('Test message', 'fileName', bigString);
		bigString = '';
		responseMap = (Map<String, Object>)JSON.deserializeUntyped(responseString);
		System.assertEquals('FAILURE.  Attachment file can not have more than 5000000 characters.', responseMap.get('serviceResponse'));
		System.assertEquals(452, response.statusCode);

		responseString = VT_R3_RestPatientMessagesComments.getComments();

		RestContext.request.requestURI = '/Patient/Messages/0016F00002R604KQAR';
		responseString = VT_R3_RestPatientMessagesComments.getComments();
		System.assertEquals(400, response.statusCode = 400);
		Map<String, Object> resp = (Map<String, Object>)JSON.deserializeUntyped(responseString);
		System.assertEquals('INCORRECT INPUT', resp.get('serviceResponse'));

	}
}