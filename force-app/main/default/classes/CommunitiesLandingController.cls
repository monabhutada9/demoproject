/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
public with sharing class CommunitiesLandingController {
    
    // Code we will invoke on page load.
    public PageReference forwardToStartPage() {
    	User u = [SELECT Id, Username, VTD1_Filled_Out_Patient_Profile__c, HIPAA_Accepted__c FROM User WHERE Id = :UserInfo.getUserId()];
        String cookiesAccepted = System.currentPageReference().getParameters().get('cookiesAccepted');

        if (u.VTD1_Filled_Out_Patient_Profile__c == false && u.HIPAA_Accepted__c == true) {
            return new PageReference('/s/initial-profile-set-up');
        } else {
            if (!String.isBlank(cookiesAccepted)) {
                saveCookieAcceptedAudit(cookiesAccepted);
            }
        	return Network.communitiesLanding();
        }
    }

    public static void saveCookieAcceptedAudit(String cookiesAccepted) {
        User u = [SELECT Id, ContactId FROM User WHERE Id=:UserInfo.getUserId()];
        List<Contact> c = [SELECT Id, VTR3_CookiesAcceptedDetails__c FROM Contact WHERE Id=:u.ContactId];
        if (c.isEmpty()) {
            return;
        }
        Contact contact = c[0];
        contact.VTR3_CookiesAcceptedDetails__c = cookiesAccepted;
        update contact;
    }

    public CommunitiesLandingController() {}
}