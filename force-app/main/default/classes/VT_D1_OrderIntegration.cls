/*****************************************************************
 * @author Elena Shane
 * 
 * Class for Patient Delivery integration with Cenduit / CSM.
 *****************************************************************/

public with sharing class VT_D1_OrderIntegration {
    
    public static void sendPatientDeliveryToCSM(Id orderId, String apiVersion) {
        String endpointURL;
        if (apiVersion == '1.1') {
            endpointURL = VT_R4_ConstantsHelper_Integrations.ENDPOINT_URL_RECEIVED_ORDER_1_1;
        } else {
            endpointURL = VT_R4_ConstantsHelper_Integrations.ENDPOINT_URL_RECEIVED_ORDER;
        }
        VT_D1_HTTPConnectionHandler.Result result = VT_D1_HTTPConnectionHandler.send(
                VT_R4_ConstantsHelper_Integrations.POST_METHOD,
                endpointURL,
                new VT_D1_RequestBuilder_Order(orderId, apiVersion),
                VT_R4_ConstantsHelper_Integrations.INTEGRATION_ACTION_RECEIVED_ORDER
        );
        update new VTD1_Order__c(
                Id = orderId,
                VTD1_Integration_Log__c = result.log.Id
        );
    }

    @Future(Callout=true)
    public static void sendPatientDeliveryToCSMFuture(Id orderId, String apiVersion) {
        sendPatientDeliveryToCSM(orderId, apiVersion);
    }

    public static void sendAdHocReplacementToCSM(Id orderId) {
        VT_D1_HTTPConnectionHandler.Result result = VT_D1_HTTPConnectionHandler.send(
                VT_R4_ConstantsHelper_Integrations.POST_METHOD,
                VT_R4_ConstantsHelper_Integrations.ENDPOINT_URL_REPLACEMENT,
                new VT_R2_RequestBuilder_AdHocReplacement(orderId),
                VT_R4_ConstantsHelper_Integrations.INTEGRATION_ACTION_ADHOC_REPLACEMENT
        );
        update new VTD1_Order__c(
                Id = orderId,
                VTD1_Integration_Log__c = result.log.Id
        );
        if (result.httpResponse.getStatusCode() == 200){
            ReplacementResponse replacementResponse = (ReplacementResponse) JSON.deserialize(result.log.VTD1_Body_Response__c, ReplacementResponse.class);
            processAdHocReplacementResponse(orderId, replacementResponse);
            //System.debug('AdHocReplacement response: ' + result.log.VTD1_Body_Response__c);
        }
    }

    @Future(Callout=true)
    public static void sendAdHocReplacementToCSMFuture(Id orderId) {
        sendAdHocReplacementToCSM(orderId);
    }

    public static void processAdHocReplacementResponse(Id orderId, ReplacementResponse replacementResponse) {
        Map<String, Kit> responseKitsMap = new Map<String, Kit>();
        for (Kit kit : replacementResponse.kits) {
            responseKitsMap.put(kit.materialId, kit);
        }

        List<VTD1_Patient_Kit__c> patientKitsToUpdate = [
                SELECT Id, VTD2_MaterialId__c
                FROM VTD1_Patient_Kit__c
                WHERE VTD1_Patient_Delivery__c = :orderId
                AND VTD2_MaterialId__c IN :responseKitsMap.keySet()
        ];
        for (VTD1_Patient_Kit__c kit : patientKitsToUpdate) {
            kit.VTR2_ReplacementMaterialId__c = responseKitsMap.get(kit.VTD2_MaterialId__c).replacementMaterialId;
        }

        update patientKitsToUpdate;
    }

    public class ReplacementResponse {
        public String protocolId;
        public String subjectId;
        public String shipmentId;
        public String shipmentName;
        public List<Kit> kits = new List<Kit>();
        public AuditLog auditLog;
    }

    public class Kit {
        public String kitName;
        public String materialId;
        public String replacementMaterialId;
        public Integer quantity;
        public String kitType;
    }

    public class AuditLog {
        public String UserId;
        public Datetime Timestamp;
    }
}