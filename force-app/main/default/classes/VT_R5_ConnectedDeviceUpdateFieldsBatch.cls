/**
 * @author: Dmitry Yartsev
 * @date: 02.06.2020
 * @description: Scheduled job for updating Status, Next Reading, Last Reading on Patient Device
 */

global without sharing class VT_R5_ConnectedDeviceUpdateFieldsBatch implements Database.Batchable<SObject>, Schedulable {

    private static final String SYNC_STATUS_UP_TO_DATE = 'Up to Date';
    private static final String SYNC_STATUS_READING_DUE = 'Reading Due';
    private static final Set<String> SUPPORTED_MEASUREMENT_TYPES = new Set<String>{
            'heart_rate',
            'weight',
            'daily_steps',
            'pressure_sys',
            'pressure_dia',
            'blood_pressure'
    };
    private Datetime DATE_TIME;

    /**
    * To schedule hourly:
    * System.schedule('Update fields on Patient Devices', '0 0 * * * ?',  new VT_R5_ConnectedDeviceUpdateFieldsBatch());
    */
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new VT_R5_ConnectedDeviceUpdateFieldsBatch(), 500);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
                'SELECT Id ' +
                'FROM VTD1_Patient_Device__c ' +
                'WHERE VTR5_DeviceKeyId__c != NULL ' +
                        'AND VTD1_Protocol_Device__r.VTD1_Device__c != NULL ' +
                        'AND VTD1_Protocol_Device__r.VTD1_Device__r.VTR5_IsContinous__c = FALSE ' +
                        'AND VTD1_Protocol_Device__r.VTR5_Measurement_Type__c INCLUDES (\'' + String.join(new List<String>(SUPPORTED_MEASUREMENT_TYPES), '\',\'') + '\')'
        );
    }

    global void execute(Database.BatchableContext bc, List<VTD1_Patient_Device__c> scope) {
        DATE_TIME = [SELECT CreatedDate FROM AsyncApexJob WHERE Id = :bc.getJobId()].CreatedDate;
        List<VTD1_Patient_Device__c> devicesToUpdate = new List<VTD1_Patient_Device__c>();
        for (VTD1_Patient_Device__c device : getDevicesWithReadings(scope)) {
            if (!device.public_readings1__r.isEmpty() && (device.VTR5_LastReadingDate__c == null || device.VTR5_LastReadingDate__c < device.public_readings1__r.get(0).VTR5_date__c)) {
                device.VTR5_SyncStatus__c = SYNC_STATUS_UP_TO_DATE;
                device.VTR5_LastReadingDate__c = device.public_readings1__r.get(0).VTR5_date__c;
                devicesToUpdate.add(device);
            } else if (device.VTR5_SyncStatus__c != SYNC_STATUS_READING_DUE && device.VTR5_Next_Reading_Date__c != null && device.VTR5_Next_Reading_Date__c <= DATE_TIME) {
                device.VTR5_SyncStatus__c = SYNC_STATUS_READING_DUE;
                devicesToUpdate.add(device);
            }
        }
        if (!devicesToUpdate.isEmpty()) {
            Database.update(devicesToUpdate, false);
        }
    }

    global void finish(Database.BatchableContext bc) {}

    private Set<VTD1_Patient_Device__c> getDevicesWithReadings(List<VTD1_Patient_Device__c> scope) {
        Set<VTD1_Patient_Device__c> newScope = new Set<VTD1_Patient_Device__c>();
        newScope.addAll((List<VTD1_Patient_Device__c>) new QueryBuilder('VTD1_Patient_Device__c')
                .namedQueryRegister('VT_R5_ConnectedDeviceUpdateFieldsBatch_QueryLocator')
                .addFields('VTR5_LastReadingDate__c, VTR5_Next_Reading_Date__c, VTR5_SyncStatus__c, VTR5_SyncStatus__c')
                .addSubQuery(new QueryBuilder('public_readings1__r')
                        .addField('VTR5_publicreadings__x.VTR5_date__c')
                        .addOrderDesc('VTR5_publicreadings__x.VTR5_date__c')
                        .addConditions()
                        .add(new QueryBuilder.SimpleCondition('VTR5_publicreadings__x.VTR5_date__c >= ' + DATE_TIME.addHours(-1).formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'')))
                        .endConditions()
                        .setLimit(20000)
                )
                .addConditions()
                .add(new QueryBuilder.InCondition('Id').inCollection(scope))
                .endConditions()
                .toList());
        return newScope;
    }
}