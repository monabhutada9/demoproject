/************************************************************************
* Name  : VT_R5_EmailGeneratorForTaskNotificationC
* Test Class : VT_R5_EmailForTaskNotificationCTest
* Date  : 09/06/2020
* Author : Yogesh More
* Desc  : This class created to send email notifictaion 
* It invokes from multiple location.
*
*
* Modification Log:
* ----------------------------------------------------------------------
* Developer                Date                description
* ----------------------------------------------------------------------
* ---ABC-----             09/06/2020           Original 
* 
*************************************************************************/
public class VT_R5_EmailGeneratorForTaskNotificationC {
    
    public static void sendEmail(List<sObject> sObjectList){
        Set<Id> useIds = new Set<Id>();
        List<OrgWideEmailAddress> owEmailAddresses = new List<OrgWideEmailAddress>();
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        for(SObject sObj :  sObjectList){
            useIds.add((Id)sObj.get('OwnerId'));
        }
        
        Map<Id, User> userMap = new Map<Id, User>([SELECT Id, Email, Firstname, Lastname, VTD1_Profile_Name__c, LanguageLocaleKey FROM User WHERE Id IN : useIds WITH SECURITY_ENFORCED]);
        if(VTD1_RTId__c.getInstance().VTR5_SHOrgWideEmailAddress__c != null){
            owEmailAddresses = [SELECT Id FROM OrgWideEmailAddress WHERE Address =: VTD1_RTId__c.getInstance().VTR5_SHOrgWideEmailAddress__c LIMIT 1];
        }
        
        if(String.valueOf(sObjectList.getSObjectType()) == 'VTD1_NotificationC__c'){            
            System.debug('Trans==>'+sObjectList.size());
            VT_D1_TranslateHelper.translateAllfields(sObjectList,true);            
        }
        
        for(SObject sObj :  sObjectList){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            If(!String.isBlank((String)sObj.get('VTR5_Email_Subject__c'))){
                mail.setSubject((String)sObj.get('VTR5_Email_Subject__c'));
            }
            else{
                mail.setSubject(VT_D1_TranslateHelper.getLabelValue('VTD2_NewNotification', userMap.get((Id)sObj.get('OwnerId')).LanguageLocaleKey));
            }
            mail.setTargetObjectId((Id)sObj.get('OwnerId'));
            mail.setHtmlBody(CreateHTMLBody(sObj,userMap.get((Id)sObj.get('OwnerId'))));
            if(owEmailAddresses.size()>0)
            mail.setOrgWideEmailAddressId(owEmailAddresses[0].Id);
            mail.setSaveAsActivity(false);
            emailList.add(mail);
        }
        
        if(!emailList.isEmpty()){
            System.debug('EmailList ==>'+EmailList.size());
            List<Messaging.SendEmailResult> results = Messaging.sendEmail(EmailList, false);
            for (Messaging.SendEmailResult result : results) {
                System.debug('result = ' + result.isSuccess());
                for (Messaging.SendEmailError error : result.errors) {
                    System.debug(error.getMessage());
                }
            }
        }
    }
    
    public static string CreateHTMLBody(SObject sObj, User use){
        VT_R3_EmailNewNotificationController emailCont = new VT_R3_EmailNewNotificationController();
        emailCont.userLanguage = use.LanguageLocaleKey;
        emailCont.profileName = use.VTD1_Profile_Name__c;
        emailCont.lastName = use.LastName;        
        emailCont.firstName = use.FirstName;
        String Body;
        String getBody;
        
        If(!String.isBlank((String)sObj.get('VTR5_Email_Body__c'))){
            getBody = (String)sObj.get('VTR5_Email_Body__c');
        }
        else{
            getBody = emailCont.getBodyText();
        }
        
        Body='<html><body><p>'+emailCont.getSalutation()+'</p>'+
            '<p>'+getBody+'</p>'+
            '<p><a href='+emailCont.getLinkToStudyHub()+'>'+VT_D1_TranslateHelper.getLabelValue('VTD2_ViewNotificationInStudyHub', use.LanguageLocaleKey)+'</a></p>'+
            '<p>'+emailCont.sincerely+'</p>'+
            '<p>'+emailCont.closingMail+'</p></body></html>';
        
        return Body;
    }
    
    @future(callout=true)
    Public static void sendEmailNotification(String createNotificationJson){
        List<VTD1_NotificationC__c> createNotificationList = new List<VTD1_NotificationC__c>(); 
        if(String.isNotBlank(createNotificationJson)){
            createNotificationList = (List<VTD1_NotificationC__c>)
                JSON.deserialize(createNotificationJson, List<VTD1_NotificationC__c>.class); 
            sendEmail(createNotificationList);
        }
    }
}