/**
 * @author: Alexander Komarov
 * @date: 29.04.2019
 * @description: REST Resource for mobile app to get live-chat histories data, without messages.
 */

@RestResource(UrlMapping='/Patient/ChatHistories')
global with sharing class VT_R3_RestPatientChatHistory {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();
    static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());

    @HttpGet
    global static String getChatHistories() {

        List<VT_D1_CommunityChat.PostObj> transcriptsList = VT_D1_CommunityChat.getPatientTranscripts();

        ChatHistoriesResponse chatHistoriesResponse = new ChatHistoriesResponse();
        chatHistoriesResponse.chatsList = getLastCommentsAndFormResult(transcriptsList);

        cr.buildResponse(chatHistoriesResponse);
        return JSON.serialize(cr, true);

    }

    private static List<PatientChatHistory> getLastCommentsAndFormResult(List<VT_D1_CommunityChat.PostObj> posts) {
        Set<Id> ltcIds = new Set<Id>();
        Set<Id> usersIds = new Set<Id>();
        for (VT_D1_CommunityChat.PostObj post : posts) ltcIds.add(post.id);
        Map <Id, LiveChatTranscript> idToChats = new Map<Id, LiveChatTranscript>();
        for (LiveChatTranscript lct : [SELECT Id, Body, CreatedDate, OwnerId, Case.Subject, EndTime, Contact.PhotoUrl, Owner.Name, Status, Contact.VTD1_UserId__c FROM LiveChatTranscript WHERE Id IN :ltcIds]) {
            idToChats.put(lct.Id, lct);
            usersIds.add(lct.OwnerId);
            usersIds.add(lct.Contact.VTD1_UserId__c);
        }

        Map<Id, String> userIdToPhoto = new Map<Id, String>();
        for (Id i : usersIds) {
            ConnectApi.Photo p;
            try {
                p = ConnectApi.UserProfiles.getPhoto(null, i);
                userIdToPhoto.put(i, p.standardEmailPhotoUrl);

            } catch (Exception e) {
                userIdToPhoto.put(i, null);
            }
        }

        List<PatientChatHistory> result = new List<PatientChatHistory>();
        for (Id i : idToChats.keySet()) {
            LiveChatTranscript current = idToChats.get(i);
            String currentLCTTitle = current.Case != null ? current.Case.Subject : 'default subject';
            if (current.Status == 'InProgress' && current.Case != null) {
                PatientChatHistory pch = new PatientChatHistory();
                pch.id = current.Id;
                pch.title = currentLCTTitle;
                pch.textLastMessage = 'This transcript is not ready yet. Check back later.';
                result.add(pch);
            } else if (String.isNotBlank(current.Body)) {
                String text = current.Body.substringBeforeLast('<br>').substringAfterLast('<br>').substringAfter(': ');
                String author = current.Body.substringBeforeLast('<br>').substringAfterLast('<br>').substringAfter(') ').substringBefore(':');
                if (String.isEmpty(text) || String.isEmpty(author)) {
                    text = current.Body.substringAfterLast('</p>').substringAfter(': ').substringBefore('<br>');
                    author = current.Body.substringAfterLast('</p>').substringAfter(') ').substringBefore(':');
                }
                String photoString = '';
                if (current.Owner != null && String.isNotBlank(current.Owner.Name)) {
                    photoString = current.Owner.Name.contains(author) ? userIdToPhoto.get(current.Owner.Id) : userIdToPhoto.get(current.Contact.VTD1_UserId__c);
                } else {
                    photoString = null;
                }
                result.add(new PatientChatHistory(
                        current.Id,
                        currentLCTTitle,
                        author,
                        current.EndTime,
                        photoString,
                        text
                ));
            } else {
                PatientChatHistory pch = new PatientChatHistory();
                pch.id = current.Id;
                pch.title = currentLCTTitle;
                pch.textLastMessage = 'This chat history is empty.';
                result.add(pch);
            }
        }

        return result;
    }

    private class PatientChatHistory {
        public String id;
        public String title;
        public String lastMessageAuthorName;
        public Long lastMessageData;
        public String photo;
        public String textLastMessage;

        public PatientChatHistory(String i, String title, String authorName, Datetime lastMessageDate, String photo, String text) {
            this.id = i;
            this.title = title;
            this.lastMessageAuthorName = authorName;
            this.lastMessageData = lastMessageDate.getTime();// + timeOffset;
            this.photo = photo == null ? '' : photo.contains('default_profile') ? '' : photo;
            this.textLastMessage = text;
        }

        public PatientChatHistory() {
        }
    }

    private class ChatHistoriesResponse {
        private List<PatientChatHistory> chatsList; //broadcasts //liveChatHistories
        private VT_R3_RestHelper.Unread unreadInfo = new VT_R3_RestHelper.Unread();
    }

}