global with sharing class VT_D1_BatchLockoutUsersProcessor implements Database.Batchable<sObject>,Database.Stateful{
    public String query = 'SELECT UserId, IsFrozen FROM UserLogin WHERE IsPasswordLocked=true and IsFrozen!=true';// WHERE IsPasswordLocked=true

    private VT_D1_ScheduleLockoutProcessor context;
    public Set<Id> userIds;

    global VT_D1_BatchLockoutUsersProcessor(VT_D1_ScheduleLockoutProcessor context){
        this.context = context;
        userIds = new Set<Id>();
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<UserLogin> scope){

        //set<Id> userIds = new set<Id>();
        List<UserLogin> userLoginsToUpdate = new List<UserLogin>();

        for(UserLogin us:scope){
            if(us.IsFrozen!=true){
                us.IsFrozen=true;
                userLoginsToUpdate.add(us);
                userIds.add(us.UserId);
            }
        }

        if(!userLoginsToUpdate.isEmpty()) update userLoginsToUpdate;
    }

    global void finish(Database.BatchableContext BC){
        system.debug('!!! userIds.size()='+userIds.size());
        system.debug('!!! userIds='+userIds);
        //send email

        Set<String> addresses = new Set<String>();
        for (VTD1_General_Settings__mdt item : [SELECT VTD1_Emails_info_freezed_users__c FROM VTD1_General_Settings__mdt]) {
            if (item.VTD1_Emails_info_freezed_users__c != null) {
                addresses.addAll(item.VTD1_Emails_info_freezed_users__c.split(';'));
            }
        }

        List<String> toAddresses = new List<String>(addresses);

        system.debug('!!! toAddresses.size()='+toAddresses.size());
        system.debug('!!! toAddresses='+toAddresses);

        String linksToUsers='';
        String externalURL=system.URL.getSalesforceBaseUrl().toExternalForm();
        String server = externalURL.substring(0,externalURL.indexOf('.'));
        String lightningURL=server+'.lightning.force.com/lightning/setup/ManageUsers/page?address=%2F';
        //String lightningURL=server+'.lightning.force.com/lightning/r/User/';

        if(!userIds.isEmpty()){
            for(Id uId:userIds){
                String link=lightningURL+uId;//+'/view';
                linksToUsers+=link+' \n ';
            }
            List <OrgWideEmailAddress> owEmailAddresses = [select Id from OrgWideEmailAddress order by CreatedDate limit 1];
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            //a.CreatedBy.Email
            mail.setToAddresses(toAddresses);
//            List<EmailTemplate> templateList = [Select id, Body, Subject from EmailTemplate where DeveloperName = 'Notification_about_freezed_users_text'];
            List<EmailTemplate> templateList = [Select id, Body, Subject from EmailTemplate where DeveloperName = 'VTD2_Notification_about_freezed_users'];
            if(!templateList.isEmpty()){
                EmailTemplate template = templateList[0];
                mail.setTemplateID(templateList[0].Id);
                mail.setSubject(template.Subject);
                mail.setPlainTextBody((template.Body != null ? template.Body : Label.VTD2_UsersHaveBeenFreezed)+'\n\n'+linksToUsers); //template.Body+'\n\n'+linksToUsers  System.Label.UsersHaveBeenFreezed
                mail.setTargetObjectId(UserInfo.getUserId());
                mail.saveAsActivity = false;
            }
            else{
                mail.setSubject(Label.VTD2_UsersHaveBeenFreezedSubject);
                mail.setPlainTextBody(Label.VTD2_UsersHaveBeenFreezed+' \n '+linksToUsers);
            }
            if (!owEmailAddresses.isEmpty())
                mail.setOrgWideEmailAddressId(owEmailAddresses[0].Id);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        if(this.context == null) return;

        this.context.callBackAsync();
    }
}