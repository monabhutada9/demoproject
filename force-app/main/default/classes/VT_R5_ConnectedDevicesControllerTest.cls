/**
 * Created by Alexander on 16.06.2020.
 */

@IsTest
private class VT_R5_ConnectedDevicesControllerTest {
    private static List<VTR5_publicreadings__x> readings;
    private static final String timezone = 'America/New_York';
    private static Case cas;
    private static Datetime minDate = Datetime.valueOf('2020-06-20 14:00:00');
    private static Datetime maxDate = Datetime.now();

    @TestSetup
    static void setup() {
        DomainObjects.Account_t account_t = new DomainObjects.Account_t().setRecordTypeByName('Sponsor');
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(account_t);
        study.persist();

        Account account = (Account) account_t.toObject();

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
                .setFirstName('Patient')
                .setLastName('Patient')
                .setAccountId(account.Id);

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile('Patient');
        patientUser.persist();

        Case caseX = (Case) new DomainObjects.Case_t()
                .addPIUser(new DomainObjects.User_t())
                .addVTD1_Patient_User(patientUser)
                .setStudy(((HealthCloudGA__CarePlanTemplate__c) study.toObject()).Id)
                .setContactId(((User) patientUser.toObject()).ContactId)
                .setRecordTypeByName('CarePlan')
                .setAccountId(account.Id)
                .persist();

        patientContact.setVTD1_Clinical_Study_Membership(caseX.Id);
        update (Contact) patientContact.toObject();
        DomainObjects.VTD1_Protocol_Delivery_t pDelivery = new DomainObjects.VTD1_Protocol_Delivery_t()
                .addCare_Plan_Template(study);
        DomainObjects.VTD1_Protocol_Kit_t pKit = new DomainObjects.VTD1_Protocol_Kit_t()
                .setRecordTypeByName('VTD1_Connected_Devices')
                .addVTD1_Care_Plan_Template(study)
                .addVTD1_Protocol_Delivery(pDelivery);
        pKit.persist();

        DomainObjects.Product2_t product = new DomainObjects.Product2_t()
                .setProductName('NewProduct')
                .setRecordTypeByName('VTD1_Connected_Devices');
        product.persist();
        DomainObjects.HealthCloudGA_EhrDevice_t ehrDevice = new DomainObjects.HealthCloudGA_EhrDevice_t()
                .addProduct(product)
                .addVTD1_Care_Plan_Template(study)
                .addVTD1_Protocol_Kit(pKit)
                .setVTD1_Kit_Type('Connected Devices')
                .addMeasurementType('heart_rate;blood_pressure');
        ehrDevice.persist();
        DomainObjects.Patient_Device_t patientDevice = new DomainObjects.Patient_Device_t()
                .addEhrDevice(ehrDevice)
                .setModelName('TestPatientDevice')
                .setCaseId(caseX.Id)
                .setSerialNumber('test123')
                .setRecordTypeByName('VTD1_Connected_Device');

        patientDevice.persist();
        DomainObjects.VTR5_StudyMeasurementsConfiguration_t studyConfig = new DomainObjects.VTR5_StudyMeasurementsConfiguration_t()
                .addStudy(study)
                .addEhrDevice(ehrDevice)
                .addHighBoundary(10)
                .addLowBoundary(5)
                .addLowerBoundary(7)
                .addUpperBoundary(9)
                .addMeasurementType('heart_rate');

        studyConfig.persist();

        DomainObjects.VTR5_StudyMeasurementsConfiguration_t studyConfig1 = new DomainObjects.VTR5_StudyMeasurementsConfiguration_t() //TODO should we remove studyConfig1, studyConfig2 ? Contact with Vlad
                .addStudy(study)
                .addEhrDevice(ehrDevice)
                .addHighBoundary(15)
                .addLowBoundary(10)
                .addLowerBoundary(8)
                .addUpperBoundary(12)
                .addMeasurementType('pressure_sys');

        studyConfig1.persist();

        DomainObjects.VTR5_StudyMeasurementsConfiguration_t studyConfig2 = new DomainObjects.VTR5_StudyMeasurementsConfiguration_t()
                .addStudy(study)
                .addEhrDevice(ehrDevice)
                .addHighBoundary(5)
                .addLowBoundary(2)
                .addLowerBoundary(1)
                .addUpperBoundary(3)
                .addMeasurementType('pressure_dia');

        studyConfig2.persist();

        DomainObjects.VTR5_DeviceReadingAddInfo_t deviceReadingAddInfo = new DomainObjects.VTR5_DeviceReadingAddInfo_t()
                .addDeviceId(patientDevice);
        deviceReadingAddInfo.persist();
    }

    @IsTest
    static void getDevicesTest() {
        VTD1_Patient_Device__c patientDevice = [SELECT Id, VTD1_Model_Name__c FROM VTD1_Patient_Device__c];
        List<VTD1_Patient_Device__c> devices = VT_R5_ConnectedDevicesController.getDevicesList();
        System.assertEquals(1, devices.size());
        VTD1_Patient_Device__c device = VT_R5_ConnectedDevicesController.getDeviceInfo(patientDevice.Id);
        System.assertEquals('test123', device.VTR5_DeviceKeyId__c);
        VT_R5_ConnectedDevicesController.permittedProfiles();
    }

    @IsTest
    static void testReadingsForHeartRate() {
        getReadings('heart_rate');
        List<VT_R5_ConnectedDevicesController.Measurement> measurements = VT_R5_ConnectedDevicesController.getReadingsPatient('test123', 'heart_rate', minDate, maxDate, timezone);
        System.assertEquals(1,measurements.size());
        List<VTR5_StudyMeasurementsConfiguration__c> studyConfig = VT_R5_ConnectedDevicesController.getBoundaries(cas.Id,'heart_rate');
        System.assertEquals(1, studyConfig.size());
        List<VT_R5_ConnectedDevicesController.Measurement> singleDevice = VT_R5_ConnectedDevicesController.getReadingsSingleDevice(cas.Id, 'test123', 'heart_rate', minDate, maxDate, timezone);
        System.assertEquals(60, singleDevice[0].values[0]);
        List<VT_R5_ConnectedDevicesController.Device> multipleDevices = VT_R5_ConnectedDevicesController.getReadingsMultipleDevices(cas.Id, 'heart_rate', minDate, maxDate, timezone);
        System.assertEquals('NewProduct',multipleDevices[0].info.VTD1_Name__c);
        VT_R5_ConnectedDevicesController.createCommentRecord('testComment', 'test123', new List<String>{null});
    }
    @IsTest
    private static void testReadingsForBloodPressure() {
        getReadings('blood_pressure');
        List<VT_R5_ConnectedDevicesController.Measurement> measurements = VT_R5_ConnectedDevicesController.getReadingsPatient('test123','blood_pressure', minDate, maxDate,timezone);
        System.assertEquals(140,measurements[0].values[0]);
        List<VTR5_StudyMeasurementsConfiguration__c> studyConfig = VT_R5_ConnectedDevicesController.getBoundaries(cas.Id,'blood_pressure');
        System.assertEquals(2, studyConfig.size());
        List<VT_R5_ConnectedDevicesController.Measurement> singleDevice = VT_R5_ConnectedDevicesController.getReadingsSingleDevice(cas.Id, 'test123', 'blood_pressure', minDate, maxDate, timezone);
        System.assertEquals(true, singleDevice[0].isOutOfRange);
        List<VT_R5_ConnectedDevicesController.Device> multipleDevices = VT_R5_ConnectedDevicesController.getReadingsMultipleDevices(cas.Id, 'blood_pressure', minDate, maxDate, timezone);
        System.assertEquals('test123',multipleDevices[0].readings[0].deviceKey);
        VT_R5_ConnectedDevicesController.createCommentRecord('testComment', 'test123', new List<String>{null});
    }

    private static void getReadings(String measurementType) {
        cas = new Case();
        cas = [SELECT Id FROM Case];
        VT_R5_ConnectedDevicesController.readingsForTest.clear();
        readings = new List<VTR5_publicreadings__x>();
            if (measurementType == 'blood_pressure') {
                readings.add(new VTR5_publicreadings__x(
                        VTR5_serial_number__c = 'test123',
                        VTR5_device_key_id__c = 'test123',
                        VTR5_type__c = 'pressure_sys',
                        VTR5_date__c = Datetime.now(),
                        VTR5_value__c = 140,
                        ExternalId = '1234',
                        VTR5_unit__c = 'mm Hg'
                ));
                readings.add(new VTR5_publicreadings__x(
                        VTR5_serial_number__c = 'test123',
                        VTR5_device_key_id__c = 'test123',
                        VTR5_type__c = 'pressure_dia',
                        VTR5_date__c = Datetime.now(),
                        VTR5_value__c = 70,
                        ExternalId = '12345',
                        VTR5_unit__c = 'mm Hg'
                ));
            } else {
                readings.add(new VTR5_publicreadings__x(
                        VTR5_serial_number__c = 'test123',
                        VTR5_device_key_id__c = 'test123',
                        VTR5_type__c = measurementType,
                        VTR5_date__c = Datetime.now(),
                        VTR5_value__c = 60,
                        ExternalId = '1234',
                        VTR5_unit__c = 'bpm'
                ));
            }
        VT_R5_ConnectedDevicesController.readingsForTest.addAll(readings);
    }

}