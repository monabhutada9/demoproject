/**
* @author: N.Arbatskiy
* @date: 30-May-20
* @description: Helper class that processes eCOA subject creation for ClinRo only.
**/

public with sharing class VT_R5_eCoaPatientSubjectCreator {
    /**
    * @description: Creates eCOA subject if all conditions are met.
    * @param caseList takes a list of cases from VT_D1_CaseTrigger @trigger.
    *
    */
    public static void processClinroSubjectCreation(List<Case>caseList) {
        List<VT_R5_eCoaAbstractAction> actions = new List<VT_R5_eCoaAbstractAction>();
        Set<Id>caseIds = new Set<Id>();
        for (Case cs : caseList) {
            caseIds.add(cs.Id);
        }

        List<Case> cases = [
                SELECT Id,
                        VTD1_Study__c,VTD1_Study__r.VTR5_eDiaryTool__c,
                        VTD1_Study__r.VTR5_SourceFormToolSelection__c,
                        VTD1_Patient_User__c,
                        VTD1_Patient_User__r.VTD1_Profile_Name__c,
                        VTD1_Patient_User__r.VTR5_eCOA_Guid__c
                FROM Case
                WHERE Id IN :caseIds
        ];

        for (Case c : cases) {
            //if conditions are met the action is added to List<VT_D1_AbstractAction> actions
            if ((c.VTD1_Patient_User__r.VTD1_Profile_Name__c == 'Patient' || c.VTD1_Patient_User__r.VTD1_Profile_Name__c == 'Caregiver')
                    && (c.VTD1_Study__r.VTR5_eDiaryTool__c != 'eCOA' && c.VTD1_Study__r.VTR5_SourceFormToolSelection__c == 'eCOA ClinRO' && c.VTD1_Patient_User__r.VTR5_eCOA_Guid__c == null)) {
                VT_R5_eCoaCreateSubject eCoaCreate = new VT_R5_eCoaCreateSubject(c.VTD1_Patient_User__c);
                actions.add(eCoaCreate);
            }
        }

        VT_R5_eCoaQueueAction.processAction(actions);
    }
}