/**
* @author: Carl Judge
* @date: 05-Nov-18
* @description: 
**/

@IsTest
public with sharing class VT_D1_LoginFlowController_Test {

    public static void withVerificationTest() {
        Case carePlan = [
                SELECT VTD1_Patient_User__r.Id, ContactId, VTD1_Virtual_Site__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
                LIMIT 1
        ];

        insert new VTR3_Temporary_SObject__c(VTR3_SObjectID__c = carePlan.ContactId, VTR4_CookieAcceptedText__c = 'test');
        update new Virtual_Site__c(Id = carePlan.VTD1_Virtual_Site__c, VTR5_IRBApprovedLanguages__c = 'en_US', VTR5_DefaultIRBLanguage__c = 'en_US');

        System.runAs(new User(Id = carePlan.VTD1_Patient_User__r.Id)) {
            VT_D1_LoginFlowController controller = new VT_D1_LoginFlowController();
            controller.getPageLanguage();
            controller.getArticle('');
            controller.findWhatsNext();
            controller.accept();
            controller.checkVerificationCodeExpired();
            controller.cancel();
        }

    }

    public static void withoutVerificationTest() {
       Case carePlan = [
                SELECT VTD1_Patient_User__r.Id
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
                LIMIT 1
        ];
        update new User(Id = carePlan.VTD1_Patient_User__r.Id, First_Log_In__c = System.now().date());

        System.runAs(new User(Id = carePlan.VTD1_Patient_User__r.Id)) {
            VT_D1_LoginFlowController controller = new VT_D1_LoginFlowController();
            controller.getArticle('');
            controller.gethideCancelButton();
            controller.sendVerificationCode();
            controller.findWhatsNext();
            controller.getStudyPhoneNumber();
            controller.accept();
            controller.accept();
            controller.cancel();
        }

    }
}