public with sharing class VT_D1_GridPhonesListController {
	public class VT_D1_GridPhonesWrapper {
        @AuraEnabled VT_D1_Phone__c phoneRec{ get; set; }
        @AuraEnabled Boolean isEdited { get; set; }
        
        public VT_D1_GridPhonesWrapper() {

        }
    }
    
    @AuraEnabled
    public static List<VT_D1_Phone__c> getPhonesList(Id objId, Id conId) {
        Schema.sObjectType objType = objId.getsobjecttype();
        if(objType == Case.sObjectType) {
        	List<Account> ac = [SELECT Id FROM Account WHERE Id IN (SELECT Accountid FROM Case WHERE Id =:objId)];
            return [Select Id, Name, PhoneNumber__c, IsPrimaryForPhone__c, IsPrimaryForText__c, Type__c, Account__c	 
                FROM VT_D1_Phone__c WHERE Account__c IN:ac   ORDER BY CreatedDate];          	
        }  
        if(objType == Contact.sObjectType && conId!=null){
            return [SELECT Id, Name, PhoneNumber__c, IsPrimaryForPhone__c, IsPrimaryForText__c, Type__c, Account__c	 
                FROM VT_D1_Phone__c WHERE  VTD1_Contact__c =:conId ORDER BY CreatedDate];
        } 
        if(objType == Account.sObjectType){
        	return [SELECT Id, Name, PhoneNumber__c, IsPrimaryForPhone__c, IsPrimaryForText__c, Type__c, Account__c	 
                FROM VT_D1_Phone__c WHERE Account__c=:objId ORDER BY CreatedDate];
        }  
        return 	new List<VT_D1_Phone__c>();
    }
    @AuraEnabled
    public static List<VT_D1_GridPhonesWrapper> getPhonesWrapList(Id objId, Id conId) {
    	if(objId==null){
    		return new List<VT_D1_GridPhonesWrapper>();
    	}
        List<VT_D1_Phone__c> phoneList = VT_D1_GridPhonesListController.getPhonesList(objId, conId);     
        if(phoneList==null || phoneList.isEmpty()){ 
        	return new List<VT_D1_GridPhonesWrapper>();
        }
        List<VT_D1_GridPhonesWrapper> phoneWrapList = new List<VT_D1_GridPhonesWrapper>();
        for(VT_D1_Phone__c ap:phoneList){
        	VT_D1_GridPhonesWrapper apw = new VT_D1_GridPhonesWrapper();
        	apw.phoneRec = ap;
        	apw.isEdited = false;
        	phoneWrapList.add(apw);
        }   
        return phoneWrapList;

    }
 	@AuraEnabled
 	public static Id getAccountId(Id objId){
 		Id ret;
 		// There are three type of object that use Grid Phone List
 		Schema.sObjectType objType = objId.getsobjecttype();
        if(objType == Case.sObjectType) {
        	List<Case> cs = [SELECT AccountId FROM Case WHERE Id =:objId];
        	for(Case c:cs){
        		ret = c.AccountId;
        	}
        }
        if(objType == Contact.sObjectType) {
        	List<Contact> cc = [SELECT AccountId FROM Contact WHERE Id =:objId];
        	for(Contact c:cc){
        		ret = c.AccountId;
        	}
        }
        if(objType == Account.sObjectType){
        	ret = objId;	
        }
 		return ret;
 	}
    @AuraEnabled
 	public static Id getContactId(Id objId){
 		Id ret;
 		// There are three type of object that use Grid Phone List
 		Schema.sObjectType objType = objId.getsobjecttype();
        if(objType == Case.sObjectType) {
        	List<Case> cs = [SELECT ContactId FROM Case WHERE Id =:objId];
        	for(Case c:cs){
        		ret = c.ContactId;
        	}
        }
        if(objType == Contact.sObjectType) {
        		ret = objId;
        }
 		return ret;
 	}
 	
 
    @AuraEnabled
    public static void savePhones(String phoneListWrapStr, String phoneDeleteListWrapStr){

		if(String.isBlank(phoneListWrapStr)) return;
    	
    	List<VT_D1_GridPhonesWrapper> phoneListWrap = (List<VT_D1_GridPhonesWrapper>)JSON.deserialize(phoneListWrapStr, List<VT_D1_GridPhonesWrapper>.class);
    	
    	List<VT_D1_GridPhonesWrapper> phoneDeleteListWrap = (List<VT_D1_GridPhonesWrapper>)JSON.deserialize(phoneDeleteListWrapStr, List<VT_D1_GridPhonesWrapper>.class);

		//delete rows
		if(phoneDeleteListWrap!=null && !phoneDeleteListWrap.isEmpty()){
			List<VT_D1_Phone__c> deleteList = new List<VT_D1_Phone__c>();
			for(VT_D1_GridPhonesWrapper apwItem:phoneDeleteListWrap){
				if(apwItem==null || apwItem.phoneRec==null){
					continue;
				}
				if(apwItem.phoneRec.Id!=null){
					deleteList.add(apwItem.phoneRec);
				}
			}
			if(deleteList!=null && !deleteList.isEmpty())
				delete deleteList;
		}
		//upsert rows
    	if(phoneListWrap!=null && !phoneListWrap.isEmpty()){
	    	List<VT_D1_Phone__c> updateList = new List<VT_D1_Phone__c>();
	    	List<VT_D1_Phone__c> insertList = new List<VT_D1_Phone__c>(); 
	    	
	    	for(VT_D1_GridPhonesWrapper apwItem:phoneListWrap){
	    		if(apwItem==null || apwItem.phoneRec==null){
	    			continue;
	    		}
	    		if(apwItem.phoneRec.Id!=null){
	        		updateList.add(apwItem.phoneRec);
	    		}
	            else{
	        		insertList.add(apwItem.phoneRec);  	
	        	}	
	    	}
			try {
				if(insertList!=null && !insertList.isEmpty())
					insert insertList;

				if(updateList!=null && !updateList.isEmpty())
					update updateList;

				} catch (Exception e) {
					String errorMsg = e.getMessage();
					if (errorMsg.contains(Label.VTR2_UpToTwoMobilePhones)) {
						errorMsg = Label.VTR2_UpToTwoMobilePhones;
					}
					AuraHandledException ex = new AuraHandledException(errorMsg);
					ex.setMessage(errorMsg);
					throw ex;
				}
    	}
    }
}