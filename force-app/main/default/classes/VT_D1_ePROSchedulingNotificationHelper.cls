public without sharing class VT_D1_ePROSchedulingNotificationHelper {

    public class NotificationTemplate {

        @InvocableVariable(Label = 'Receiver')
        public String receiverId;

        @InvocableVariable(Label = 'Has Direct Link')
        public Boolean hasDirectLink;

        @InvocableVariable(Label = 'Link To Related Event Or Object')
        public String linkToRelatedEventOrObject;

        @InvocableVariable(Label = 'OwnerId')
        public String ownerId;

        @InvocableVariable(Label = 'Title')
        public String title;

        @InvocableVariable(Label = 'Study')
        public String studyId;

        @InvocableVariable(Label = 'Message')
        public String messageLabel;

        @InvocableVariable(Label = 'Survey ID')
        public Id surveyId;


    }

    @InvocableMethod(Label = 'Send Patient Caregiver eDiary Notifications')
    public static void sendPatientCaregiverNotifications(List<NotificationTemplate> templates) {
        List<VTD1_NotificationC__c> notificationList = createNotifications(templates);
        insert notificationList;
    }

    private static List<VTD1_NotificationC__c> createNotifications(List<NotificationTemplate> templates) {
        Map<Id, VTD1_Survey__c> surveyMap = getSurveys(templates);
        List<VTD1_NotificationC__c> notificationList = new List<VTD1_NotificationC__c>();
        NotificationTemplate currentParams;
        for(Integer i = 0; i < templates.size(); i++) {
            currentParams = templates[i];
            VTD1_Survey__c survey = surveyMap.get(currentParams.surveyId);
            notificationList.add(
                    new VTD1_NotificationC__c(
                            VTD1_Receivers__c = currentParams.receiverId,
                            Message__c = currentParams.messageLabel,
                            VTD1_Parameters__c = buildParams(currentParams, survey),
                            OwnerId = currentParams.receiverId,
                            Title__c = 'VDT2_ePROReminder',
                            Link_to_related_event_or_object__c = currentParams.linkToRelatedEventOrObject,
                            HasDirectLink__c = currentParams.hasDirectLink,
                            VTD2_DoNotDuplicate__c = !survey.VTD1_Protocol_ePRO__r.VTD1_Caregiver_on_behalf_of_Patient__c
                    )
            );
        }
        return notificationList;
    }

    private static Map<Id, VTD1_Survey__c> getSurveys(List<NotificationTemplate> templates) {
        Set<String> eDiaryIds = new Set<String>();
        for(Integer i = 0; i < templates.size(); i++) {
            eDiaryIds.add(templates[i].surveyId);
        }
        return new Map<Id, VTD1_Survey__c>([
                SELECT  Id,
                        Name,
                        VTD2_Study_Nickname__c,
                        VTD1_CSM__r.VTD1_Study__r.VTD1_Protocol_Nickname__c,
            			VTD1_Protocol_ePRO__r.VTD1_Caregiver_on_behalf_of_Patient__c,
                        VTD1_Due_Date__c
                FROM    VTD1_Survey__c
                WHERE   Id  IN :eDiaryIds
        ]);
    }

    private static String buildParams(NotificationTemplate currentParams, VTD1_Survey__c eDiary) {
        List<String> paramsList = new List<String>();
        paramsList.add(eDiary.Id + 'Name');
        if(eDiary.VTD1_CSM__c != null && eDiary.VTD1_CSM__r.VTD1_Study__c != null && eDiary.VTD1_CSM__r.VTD1_Study__r.VTD1_Protocol_Nickname__c != null) {
            paramsList.add(eDiary.VTD1_CSM__r.VTD1_Study__c + 'VTD1_Protocol_Nickname__c');
        } else {
            paramsList.add('');
        }
        if(eDiary.VTD1_Due_Date__c == null ) {
            paramsList.add('');
        } else {
            paramsList.add(eDiary.Id + 'VTD1_Due_Date__c');
        }
        return String.join(paramsList, ';');
    }
}