/**
 * Created by Rishat Shamratov on 30.10.2019.
 */

public class VT_R3_EmailIneligiblePatientController {
    public Id patientCaseId { get; set; }

    public class MessageBodyWrapper {
        public String patientName { get; set; }
        public String notifyPatientIneligibleStatus { get; set; }
        public String contactTheStudyTeamWithStudyPhoneNumber { get; set; }
    }

    public MessageBodyWrapper getMessageBody() {
        MessageBodyWrapper messBody = new MessageBodyWrapper();
        List<Case> patientCases = [
                SELECT Id, CaseNumber,
                        Email_PFN__c,
                        VTD1_Patient_User__r.LanguageLocaleKey,
                        VTD1_Study__r.VTD1_Protocol_Nickname__c,
                        VTR2_StudyPhoneNumber__r.Name
                FROM Case
                WHERE Id = :patientCaseId
        ];
        String language = patientCases[0].VTD1_Patient_User__r.LanguageLocaleKey;
        VT_D1_TranslateHelper.translate(patientCases, language);
        Case patientCase = patientCases[0];

        messBody.patientName = patientCase.Email_PFN__c;
        messBody.notifyPatientIneligibleStatus = VT_D1_TranslateHelper.getLabelValue('VTR2_NotifyPatientIneligibleStatus', language).replaceAll('#1', patientCase.VTD1_Study__r.VTD1_Protocol_Nickname__c);
        messBody.contactTheStudyTeamWithStudyPhoneNumber = VT_D1_TranslateHelper.getLabelValue('VTR3_ContactTheStudyTeam', language).replaceAll('#PhoneNumber', patientCase.VTR2_StudyPhoneNumber__r.Name);

        return messBody; 
    }
}