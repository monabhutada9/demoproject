/**
* @author: Carl Judge
* @date: 17-Jan-20
* @description: A class to get the correct version of PA versioned records for a given site or sites
**/

/*
    Usage:
    Wrap PA versioned records into a list of RecordWrappers - this should be all versions of every record. If results
    for multiple sites are required, include records for all sites in this list.

    Instantiate this class, passing the wrapped records as a constructor param.
    Then for each site, call the getCorrectVersions method.

    Example:
    VT_R4_ProtocolAmendmentVersionHelper helper = new VT_R4_ProtocolAmendmentVersionHelper(wrappedRecords);
    for (Id siteId : siteIds) {
        List<SObject> correctVersions = helper.getCorrectVersions(siteId);
        // do some stuff
    }
 */

public without sharing class VT_R4_ProtocolAmendmentVersionHelper {
    private Map<Id, RecordWrapper> wrappedRecordsById = new Map<Id, RecordWrapper>();
    private Map<Id, Set<Id>> siteToValidPAIds = new Map<Id, Set<Id>>();
    private Map<Id, VTD1_Protocol_Amendment__c> paMap = new Map<Id, VTD1_Protocol_Amendment__c>();
    private Map<Id, List<RecordWrapper>> wrappedRecordsByRootId = new Map<Id, List<RecordWrapper>>();

    public VT_R4_ProtocolAmendmentVersionHelper(List<RecordWrapper> wrappedRecords) {
        List<Id> paIds = new List<Id>();
        for (RecordWrapper wrappedRec : wrappedRecords) {
            paIds.add(wrappedRec.protocolAmendmentId);
            wrappedRecordsById.put(wrappedRec.record.Id, wrappedRec);
        }
        getPaDetails(paIds);
        organizeWrappedRecords();
        System.debug(wrappedRecordsByRootId);
    }

    public List<SObject> getCorrectVersions(Id siteId) {
        return calculateCorrectVersionsForSite(siteId, wrappedRecordsByRootId.keySet());
    }

    public List<SObject> getCorrectVersions(Set<Id> siteIds) {
        return calculateCorrectVersionsForSites(siteIds, wrappedRecordsByRootId.keySet());
    }

    /*
        If recordIds or records param is passed, results will only include these records. Note that if a record was not
        wrapped and passed to the constructor, it will not be in the results
     */
    public List<SObject> getCorrectVersions(Id siteId, Set<Id> recordIds) {
        return calculateCorrectVersionsForSite(siteId, getRootIds(recordIds));
    }

    public List<SObject> getCorrectVersions(Id siteId, List<SObject> records) {
        return calculateCorrectVersionsForSite(siteId, getRootIds(new Set<Id>(VT_D1_HelperClass.getIdsFromObjs(records))));
    }

    public Boolean isPAApproved(Id paId) {
        return paId != null && paMap.containsKey(paId) && paMap.get(paId).Documents__r != null && !paMap.get(paId).Documents__r.isEmpty();
    }

    private List<SObject> calculateCorrectVersionsForSite(Id siteId, Set<Id> rootIds) {
        List<SObject> correctVersions = new List<SObject>();
        Set<Id> validPAIds = siteToValidPAIds.get(siteId);
        for (Id rootId : rootIds) {
            SObject correctRecord = wrappedRecordsById.containsKey(rootId) ? wrappedRecordsById.get(rootId).record : null;
            System.debug('correctRecord ' + correctRecord);
            System.debug('wrappedRecordsById ' + wrappedRecordsById);
            System.debug(validPAIds);
            if (correctRecord != null && wrappedRecordsById.get(rootId).protocolAmendmentId != null
                    && validPAIds == null) {
                correctRecord = null;
            }
            if (validPAIds != null && wrappedRecordsByRootId.containsKey(rootId)) {
                for (RecordWrapper wrappedRec : wrappedRecordsByRootId.get(rootId)) {
                    System.debug(validPAIds);
                    if (validPAIds.contains(wrappedRec.protocolAmendmentId)) {
                        correctRecord = wrappedRec.removeFromProtocol ? null : wrappedRec.record; // set record to null if it has been removed from protocol
                        break;
                    }
                }
            }
            if (correctRecord != null) {
                correctVersions.add(correctRecord);
            }
        }

        return correctVersions;
    }

    private List<SObject> calculateCorrectVersionsForSites(Set<Id> siteIds, Set<Id> rootIds) {
        Map<Id, SObject> correctVersionsMap = new Map<Id, SObject>();
        for (Id siteId : siteIds) {
            correctVersionsMap.putAll(calculateCorrectVersionsForSite(siteId, rootIds));
        }
        return correctVersionsMap.values();
    }

    private void getPaDetails(List<Id> paIds) {
        for (VTD1_Protocol_Amendment__c pa : [
                SELECT Id, CreatedDate, (
                        SELECT VTD1_Site__c
                        FROM Documents__r
                        WHERE RecordTypeId = :VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT
                        AND VTD1_Regulatory_Document_Type__c = :VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_SIGNATURE_PAGE
                        AND VTD2_EAFStatus__c = :VT_R4_ConstantsHelper_Documents.DOCUMENT_EAF_STATUS_COMPLETED
                        AND VTR4_IRB_Approved__c = TRUE
                )
                FROM VTD1_Protocol_Amendment__c
                WHERE Id IN :paIds
        ]) {
            if (!pa.Documents__r.isEmpty()) {
                paMap.put(pa.Id, pa);
                for (VTD1_Document__c doc : pa.Documents__r) {
                    if (!siteToValidPAIds.containsKey(doc.VTD1_Site__c)) {
                        siteToValidPAIds.put(doc.VTD1_Site__c, new Set<Id>());
                    }
                    siteToValidPAIds.get(doc.VTD1_Site__c).add(pa.Id);
                }
            }
        }
    }

    private void organizeWrappedRecords() {
        for (RecordWrapper rec : wrappedRecordsById.values()) {
            if (!wrappedRecordsByRootId.containsKey(rec.rootId)) {
                wrappedRecordsByRootId.put(rec.rootId, new List<RecordWrapper>());
            }
            if (paMap.containsKey(rec.protocolAmendmentId)) {
                rec.paCreatedDate = paMap.get(rec.protocolAmendmentId).CreatedDate;
                wrappedRecordsByRootId.get(rec.rootId).add(rec);
            }
        }
        for (List<RecordWrapper> recList : wrappedRecordsByRootId.values()) {
            recList.sort();
        }
    }

    private Set<Id> getRootIds(Set<Id> recordIds) {
        Set<Id> rootIds = new Set<Id>();
        for (Id recordId : recordIds) {
            if (wrappedRecordsById.containsKey(recordId)) {
                rootIds.add(wrappedRecordsById.get(recordId).rootId);
            }
        }
        return rootIds;
    }

    public class RecordWrapper implements Comparable {
        public SObject record;
        public Id rootId;
        public Id protocolAmendmentId;
        public Boolean removeFromProtocol;
        public Datetime paCreatedDate;

        public RecordWrapper(SObject record, Id rootId, Id protocolAmendmentId, Boolean removeFromProtocol) {
            this.record = record;
            this.rootId = rootId != null ? rootId : record.Id;
            this.protocolAmendmentId = protocolAmendmentId;
            this.removeFromProtocol = removeFromProtocol;
        }

        public Integer compareTo(Object compareTo) {
            RecordWrapper compareToItem = (RecordWrapper) compareTo;
            if (paCreatedDate != compareToItem.paCreatedDate) {
                return paCreatedDate > compareToItem.paCreatedDate ? -1 : 1;
            }
            return 0;
        }
    }
}