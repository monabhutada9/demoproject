/**
 * Created by Andrey Pivovarov on 4/24/2020.
 */

@IsTest
public with sharing class VT_R4_RemindersBatchTest {
    @TestSetup
    static void setup() {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));
        DomainObjects.Case_t patientCase = new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .addStudy(study);
        patientCase.persist();
        List<DomainObjects.VTD1_Actual_Visit_t> domActualVisits = new List<DomainObjects.VTD1_Actual_Visit_t>();
        Datetime curDate = System.now();
        DomainObjects.VTD1_Actual_Visit_t actualVisitTest = new DomainObjects.VTD1_Actual_Visit_t()
                .setScheduledDateTime(curDate.addMinutes(90)).setVTD1_Name('Test').setVTR5_PatientCaseId(patientCase);
        DomainObjects.VTD1_Actual_Visit_t actualVisitToUpdate = new DomainObjects.VTD1_Actual_Visit_t()
                .setScheduledDateTime(curDate.addHours(2)).setVTD1_Name('TestForUpdate').setVTR5_PatientCaseId(patientCase);
        DomainObjects.VTD1_Actual_Visit_t actualVisitToDelete = new DomainObjects.VTD1_Actual_Visit_t()
                .setScheduledDateTime(curDate.addHours(5)).setVTD1_Name('TestForDelete').setVTR5_PatientCaseId(patientCase);
        domActualVisits.add(actualVisitTest);
        domActualVisits.add(actualVisitToUpdate);
        domActualVisits.add(actualVisitToDelete);
        List<DomainObjects.Visit_Member_t> visitMembers = new List<DomainObjects.Visit_Member_t>();
        List<VTD1_Actual_Visit__c> actualVisits = new List<VTD1_Actual_Visit__c>();
        for (Integer i = 0; i < domActualVisits.size(); i++) {
            visitMembers.add(new DomainObjects.Visit_Member_t().addVTD1_Participant_User(
                    new DomainObjects.User_t()).addVTD1_Actual_Visit(domActualVisits[i]));
            visitMembers.add(new DomainObjects.Visit_Member_t().addVTD1_Participant_User(
                    new DomainObjects.User_t()).addVTD1_Actual_Visit(domActualVisits[i]));
            actualVisits.add((VTD1_Actual_Visit__c) domActualVisits[i].persist());
        }
        for (Integer i = 0; i < actualVisits.size(); i++) {
            actualVisits[i].VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED;
        }
        update actualVisits;
        List<VTR4_Scheduler_Reminders__c> schedulerReminders = new List<VTR4_Scheduler_Reminders__c>{
                new VTR4_Scheduler_Reminders__c(
                        VTR4_Actual_Visit__c = actualVisits[1].Id,
                    VTR4_Hours_Before__c = (actualVisits[1].VTD1_Scheduled_Date_Time__c.getTime() - curDate.getTime())/1000/60/60,
                    VTR4_Scheduled_for__c = actualVisits[1].VTD1_Scheduled_Date_Time__c.addHours(5)),
                new VTR4_Scheduler_Reminders__c(
                    VTR4_Actual_Visit__c = actualVisits[2].Id,
                    VTR4_Hours_Before__c = (actualVisits[2].VTD1_Scheduled_Date_Time__c.getTime() - curDate.getTime())/1000/60/60,
                    VTR4_Scheduled_for__c = actualVisits[2].VTD1_Scheduled_Date_Time__c.addHours(2))
        };
        insert schedulerReminders;
    }


    @IsTest
    static void schedulerRemindersBatchTest()
    {
        List<VTD1_Actual_Visit__c> visits = [
                SELECT Id, (SELECT Id, Name FROM Visit_Members__r)
                FROM VTD1_Actual_Visit__c];
        System.debug(visits);
        System.debug(visits[0].Visit_Members__r);
        Test.startTest();

        VT_R4_RemindersScheduler remindersScheduler = new VT_R4_RemindersScheduler();
        Datetime dt = remindersScheduler.getAlignedTimeJob(System.now());

        Database.executeBatch(new VT_R4_RemindersBatch(remindersScheduler, dt),200);

        Test.stopTest();
        List<VTR4_Scheduler_Reminders__c> sr = [
                SELECT VTR4_Actual_Visit__c, VTR4_Scheduled_for__c, VTR4_Hours_Before__c,
                        VTR4_Actual_Visit__r.VTD1_Scheduled_Date_Time__c, VTR4_Actual_Visit__r.Id
                FROM VTR4_Scheduler_Reminders__c WHERE VTR4_Actual_Visit__r.Name = 'TestForDelete'
        ];
        System.debug('*sr* ' + sr);
        List<VTR4_Scheduler_Reminders__c> schedulerReminders = [
                SELECT VTR4_Actual_Visit__c, VTR4_Scheduled_for__c, VTR4_Hours_Before__c,
                        VTR4_Actual_Visit__r.VTD1_Scheduled_Date_Time__c, VTR4_Actual_Visit__r.Id
                FROM VTR4_Scheduler_Reminders__c
                WHERE VTR4_Actual_Visit__r.Name = 'TestForUpdate'
        ];
        System.debug('***size ' + schedulerReminders.size());
        if (schedulerReminders.size() > 0) {
            System.debug(schedulerReminders[0].VTR4_Actual_Visit__r.VTD1_Scheduled_Date_Time__c + ' *and* ' + schedulerReminders[0].VTR4_Scheduled_for__c
                    + ' ' + schedulerReminders[0].VTR4_Hours_Before__c);
            System.assertEquals(schedulerReminders[0].VTR4_Scheduled_for__c,
                    schedulerReminders[0].VTR4_Actual_Visit__r.VTD1_Scheduled_Date_Time__c.addHours((Integer) -schedulerReminders[0].VTR4_Hours_Before__c));
            System.assertEquals(schedulerReminders[0].VTR4_Actual_Visit__r.Id, schedulerReminders[0].VTR4_Actual_Visit__c);
        }
    }
}