/**
 * Created by Jane Ivanova
 * Edited by Ivan Matskevich
 **/

public class VT_D2_RequestBuilder_SendSubjectStatus implements VT_D1_RequestBuilder {
    
    public class UpdateRequest {
        public String subjectId;
        public String protocolId;
        public String investigatorId;
        public String sourceSystemId;
        public SubjectDetails subjectDetails;
        public AuditLog auditLog;
    }
    
    public class SubjectDetails {
        public String Status;
        public String PreviousStatus;
        public String ReasonCode;
        
        public SubjectDetails(String s, String ps, String rc) {
            status = s;
            previousStatus = ps;
            reasonCode = rc;
        }
    }
    
    public class AuditLog {
        public String UserId;
        public Datetime Timestamp;
        
        public AuditLog() {
            UserId = UserInfo.getUserId();
            Timestamp = Datetime.now();
        }
    }
    
    
    public Id caseId;
    
    public VT_D2_RequestBuilder_SendSubjectStatus(Id caseId) {
        this.caseId = caseId;
    }
    
    public String buildRequestBody() {
    	try{
	        Case subject = [
	                SELECT VTD1_Subject_ID__c,
	                        VTD1_Patient_ID_RRID__c,
	                        VTD1_Study__r.Name,
	                        VTD1_PI_user__r.VTD1_CTMS_Row_ID__c,
	                        Status,
	                        VTD2_PreviousStatus__c,
	                        VTD1_Reason_Code__c
	                FROM Case
	                WHERE Id = :caseId
	        ];
	        
	        UpdateRequest updateRequest = new UpdateRequest();
	        updateRequest.subjectId = subject.VTD1_Subject_ID__c;
	        updateRequest.protocolId = subject.VTD1_Study__r.Name;
	        updateRequest.investigatorId = subject.VTD1_PI_user__r.VTD1_CTMS_Row_ID__c;
	        updateRequest.sourceSystemId = subject.VTD1_Patient_ID_RRID__c;
	        
	        updateRequest.subjectDetails = new SubjectDetails(subject.Status, subject.VTD2_PreviousStatus__c, subject.VTD1_Reason_Code__c);
	        updateRequest.auditLog = new AuditLog();
	        
	        String requestBody = JSON.serializePretty(updateRequest, true);
	        System.debug('REQUEST BODY: \n' + requestBody);
	        return requestBody;
        
        } catch (Exception e) {
    		System.debug('Failed to build request body for Subject Status.');
    		System.debug('Error: ' + e.getMessage());
    		return null;
    	}
    }
}