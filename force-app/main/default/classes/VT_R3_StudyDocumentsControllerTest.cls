@IsTest
public class VT_R3_StudyDocumentsControllerTest {
    
        private static final String SEARCH_STRING = '{"sortingParams": null,"filterParams": [],"searchParams": null,"entriesOnPage": 10,"currentPage": 1}';


    @TestSetup
    static void testDataSetup() {
        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c(
                Name = 'TestName'
        );
        insert study;
        Case cas = new Case(
                VTD1_Study__r = study
        );
        insert cas;

        //VT_R3_StudyDocumentsController.DocumentWrapper documentWrapper = new VT_R3_StudyDocumentsController.DocumentWrapper();

        VTD1_Document__c document = new VTD1_Document__c(
                        VTD1_Clinical_Study_Membership__c = cas.Id,
                        VTD1_Comment__c = 'comment',
                        VTD1_Status__c = 'Pending Certification');


        //List<VTD1_Document__c> documentsList = new List<VTD1_Document__c>{
        List<VTD1_Document__c> documentsList = new List<VTD1_Document__c>{
            new VTD1_Document__c(
               VTD1_Clinical_Study_Membership__c = cas.Id, 
               VTD1_Comment__c = 'comment',
               VTD1_Status__c = 'Pending Certification' 
            )
        };
        
        insert documentsList;
        
    }

	@IsTest
    static void getStudyDocuments_Positive() {
        Id caseId = [SELECT Id FROM Case LIMIT 1].Id;
        System.debug('caseId'+caseId);
        
        Test.startTest();
        VT_R3_StudyDocumentsController.StudyDocumentsWrapper studyDocuments = VT_R3_StudyDocumentsController.getStudyDocuments(caseId, SEARCH_STRING);
        Test.stopTest();
        System.debug('getStudyDocuments_Positive');
        System.debug(JSON.serializePretty(studyDocuments));
        System.assertEquals(1, studyDocuments.documentsList.size(), 'Incorrect Size of Returned Records');

        for (VT_R3_StudyDocumentsController.DocumentWrapper d : studyDocuments.documentsList) {
            System.assertEquals(caseId, d.document.VTD1_Clinical_Study_Membership__c, 'Incorrect caseId');
        }
    }
        
    @IsTest
    static void getStudyDocuments_Negative() {
        Id caseId = null;
        VT_R3_StudyDocumentsController.StudyDocumentsWrapper studyDocuments = null;
        Test.startTest();
        try {
            studyDocuments = VT_R3_StudyDocumentsController.getStudyDocuments(caseId, null);
        } catch (AuraHandledException e) {
            System.assertEquals(null, studyDocuments);
        }
        Test.stopTest();
    }

    







}