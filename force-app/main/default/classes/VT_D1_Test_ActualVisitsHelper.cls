/**
 * Created by shume on 16/10/2018.
 */

@IsTest
private class VT_D1_Test_ActualVisitsHelper {

    @TestSetup
    private static void setupMethod() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
        Case cs = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];

        User patient = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Patient' AND ContactId != NULL AND IsActive = TRUE ORDER BY CreatedDate DESC LIMIT 1];
        User PI = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Primary Investigator' AND ContactId != NULL AND IsActive = TRUE ORDER BY CreatedDate LIMIT 1];
        System.debug('patient:'+patient);
        System.debug('PI:'+PI);

        Id caseId = cs.Id;
        cs.VTD1_Enrollment_Date__c = Datetime.now().addDays(-5).date();
        update cs;
        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Range__c = 2;
        protocolVisit.VTD1_VisitOffset__c = 2;
        protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit.VTD1_VisitDuration__c = '30 minutes'; 
        protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit.VTR2_Visit_Participants__c = 'PI';
        insert protocolVisit;

        VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
        actualVisit.VTD1_Protocol_Visit__c = protocolVisit.Id;
        actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
        actualVisit.VTD1_Case__c = caseId;
        insert actualVisit;

        Task newTask = new Task();
        newTask.VTD1_Actual_Visit__c = actualVisit.Id;
        insert newTask;
    }

    @IsTest
    static void testBehavior() {
        Task newTask = [SELECT Id,VTD1_Actual_Visit__c FROM Task LIMIT 1];
        Test.startTest();
        String stringResult = new VT_D1_ActualVisitsHelper().getAvailableDateTimes(newTask.VTD1_Actual_Visit__c);
        System.assert(stringResult!=null);
        Test.stopTest();
    }
}