/**
 * Created by Alexander Komarov on 28.05.2019.
 */

@RestResource(UrlMapping='/Patient/Visit/Schedule/*')
global with sharing class VT_R3_RestPatientVisitSchedule {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();

    @HttpGet
    global static String getScheduleInfo() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        String temp = request.requestURI.substringAfterLast('/Schedule/');

        if (String.isBlank(temp)) {
            response.statusCode = 400;
            cr.buildResponse('FAILURE. Visit Id expected.');
            return JSON.serialize(cr, true);
        } else {
            Id visitId = temp;

            if (!helper.isIdCorrectType(visitId, 'VTD1_Actual_Visit__c')) {
                response.statusCode = 400;
                cr.buildResponse(helper.forAnswerForIncorrectInput());
                return JSON.serialize(cr, true);
            }
            VT_D1_ActualVisitsHelper.DatesInfo result;
            try {
                result = new VT_D1_ActualVisitsHelper().getDateTimesByMonthWeekHelper(temp);
            } catch (Exception e) {
                response.statusCode = 500;
                cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName());
                return JSON.serialize(cr, true);
            }
            cr.buildResponse(result);
            return JSON.serialize(cr, true).replace('"selected":false,', '');
        }
    }

    @HttpPost
    global static String scheduleVisit(String slots) {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        String temp = request.requestURI.substringAfterLast('/Schedule/');
        if (String.isBlank(temp)) {
            response.statusCode = 400;
            cr.buildResponse('FAILURE. Visit Id expected.');
            return JSON.serialize(cr, true);
        }
        Id visitId = temp;

        if (!helper.isIdCorrectType(visitId, 'VTD1_Actual_Visit__c')) {
            response.statusCode = 400;
            cr.buildResponse(helper.forAnswerForIncorrectInput());
            return JSON.serialize(cr, true);
        }
        String error = '';
        try {
            String chosenTime = slots;

            error = VT_D1_ChooseTimeOnTaskContollerRemote.createTimeSlots(visitId, chosenTime);
        } catch (Exception e) {
            error = e.getMessage();
        }
        if (error == null) {
            VTD1_Actual_Visit__c actualVisit = [SELECT VTD1_Formula_Name__c FROM VTD1_Actual_Visit__c WHERE Id = :visitId LIMIT 1];
            String visitName = actualVisit.VTD1_Formula_Name__c;
            cr.buildResponse('SUCCESS', System.Label.VTR3_Request_Time_Slots.replace('#1', visitName));
            return JSON.serialize(cr, true);
        } else {
            response.statusCode = 500;
            cr.buildResponse('FAILURE' + ' Error: ' + error, System.Label.VT_D1_FailedToSentRequestToSchedule);
            return JSON.serialize(cr, true);
        }
    }
}