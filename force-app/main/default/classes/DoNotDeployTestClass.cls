/** Modified Vijendra: SH-20622 Add the getEDiariesTotalScoreDataTest Method */
@IsTest
public with sharing class DoNotDeployTestClass{
    
    public static void getDataTest(){
        VTR5_DiaryListView__c dlv = new VTR5_DiaryListView__c();
        insert dlv;

        Map<String, Object> params = new Map<String, Object>{'limit' => (Object) '1000'};
        VT_R5_EDiariesTableController eData = VT_R5_EDiariesTableController.getEdiaryRecords(null, JSON.serialize(params), null, true);
        
        List<Id> eDiaryIds = new List<Id>();
        List<VT_R5_EDiariesTableController.EDiaryWrapper> eDiaries = eData.items;
        for(VT_R5_EDiariesTableController.EDiaryWrapper ew : eDiaries){
            eDiaryIds.add(ew.eDiary.Id);
        }
        update new VTD1_Survey__c (Id = eDiaryIds.get(0), VTR5_Response_GUID__c = 'test123');
        Attachment attachment = new Attachment();
        attachment.ParentId = dlv.Id;
        attachment.Name = 'Test Attachment for Diary';
        attachment.Body = blob.valueOf('test123');
        
        insert attachment;

        Test.startTest();
            Map<String, Object> filters = new Map<String, Object>{'markReviewedFilter' => (Object) 'false', 'totalScoreValue' => (Object) '50',
                                                                  'statusFilters' => (Object) new List<String>{eDiaries.get(0).eDiary.VTD1_Status__c}};
            Map<String, Object> order = new Map<String, Object>{'columnName' => 'eDiaryName', 'sorting' => 'asc'};                                                       

            params.put('filterMap', filters);
            String searchName = eDiaries.get(0).eDiary.Name.substring(0, 3);
            eData = VT_R5_EDiariesTableController.getEdiaryRecords(dlv.Id, JSON.serialize(params), searchName, true);
        
            order = new Map<String, Object>{'columnName' => 'phoneNumber', 'sorting' => 'asc'};
            params.put('order', order);
            eData = VT_R5_EDiariesTableController.getEdiaryRecords(dlv.Id, JSON.serialize(params), searchName, true);
            
            params.put('order', order);
            order = new Map<String, Object>{'columnName' => 'dateCompleted', 'sorting' => 'asc'};
            eData = VT_R5_EDiariesTableController.getEdiaryRecords(dlv.Id, JSON.serialize(params), searchName, true);
        
			//Added by :Conquerors, Vijendra Hire SH-20622	
            //START	
        	params.put('order', order);
            order = new Map<String, Object>{'columnName' => 'totalScore', 'sorting' => 'asc'};
            eData = VT_R5_EDiariesTableController.getEdiaryRecords(dlv.Id, JSON.serialize(params), '0', true);
        	List<VT_R5_EDiariesTableController.EDiaryWrapper> eDiary = eData.items;
        	//End
        Test.stopTest();
    }



    public static void getEDiariesVirtualSiteDataTest(){
        VTR5_DiaryListView__c diaryViewList = new VTR5_DiaryListView__c();
        insert diaryViewList;
        
        Map<String, Object> ediaryFilterMap = new Map<String, Object>{'limit' => (Object) '1000'};
        VT_R5_EDiariesTableController ediaryRecords = VT_R5_EDiariesTableController.getEdiaryRecords(null, JSON.serialize(ediaryFilterMap), null, true);
        List<VT_R5_EDiariesTableController.EDiaryWrapper> eDiaries = ediaryRecords.items;      
        Test.startTest();
        List<String> virtualSiteFilterList = new List<String>{eDiaries.get(0).eDiary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name,
                                                                                    eDiaries.get(1).eDiary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name};
                                                                                        
        Map<String, Object> filters = new Map<String, Object>{
                                                                                       'studyId' => eDiaries.get(0).eDiary.VTD1_CSM__r.VTD1_Study__c,   
                                                                                       'virtualSitesFilters' => (Object) virtualSiteFilterList
                                                                                           };                                                     
            ediaryFilterMap.put('filterMap', filters);
        ediaryRecords = VT_R5_EDiariesTableController.getEdiaryRecords(diaryViewList.Id, JSON.serialize(ediaryFilterMap), null, true);
        List<VT_R5_EDiariesTableController.EDiaryWrapper> filtereDiaries = ediaryRecords.items;
        for(VT_R5_EDiariesTableController.EDiaryWrapper ew : filtereDiaries)
        {    
            for(String virtualSiteName : virtualSiteFilterList)
            {
                if(ew.eDiary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name == virtualSiteName){
                    System.assertEquals(ew.eDiary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name,virtualSiteName);
                }
            }  
        } 
        Test.stopTest();
    }
    
    public static void getEDiariesNameDataTest(){
        VTR5_DiaryListView__c diaryViewList = new VTR5_DiaryListView__c();
        insert diaryViewList;
        
        Map<String, Object> ediaryFilterMap = new Map<String, Object>{'limit' => (Object) '1000'};
            VT_R5_EDiariesTableController ediaryRecords = VT_R5_EDiariesTableController.getEdiaryRecords(null, JSON.serialize(ediaryFilterMap), null, true);
        List<VT_R5_EDiariesTableController.EDiaryWrapper> eDiaries = ediaryRecords.items;
        Test.startTest();
        List<String> eDiariesFilterList = new List<String>{eDiaries.get(0).eDiary.Name,eDiaries.get(1).eDiary.Name};
        Map<String, Object> filters = new Map<String, Object>{
                                                                                         'studyId' => eDiaries.get(0).eDiary.VTD1_CSM__r.VTD1_Study__c, 
                                                                                         'eDiaryNameFilters' => (Object) eDiariesFilterList
        };                                                     
        ediaryFilterMap.put('filterMap', filters);
        ediaryRecords = VT_R5_EDiariesTableController.getEdiaryRecords(diaryViewList.Id, JSON.serialize(ediaryFilterMap), null, true);
        List<VT_R5_EDiariesTableController.EDiaryWrapper> filtereDiaries = ediaryRecords.items;
        for(VT_R5_EDiariesTableController.EDiaryWrapper ew : filtereDiaries)
        {
            for(String ediaryName : eDiariesFilterList){
                if(ew.eDiary.Name == ediaryName){
                     System.assertEquals(ew.eDiary.Name ,ediaryName);
                }
            }


         }

        


        Test.stopTest();

    }

    public static void updateEdiaryTest() {
        Id caseId = [SELECT Id FROM Case LIMIT 1].Id;

        VTD1_Survey__c survey = new VTD1_Survey__c(
                Name = 'Test eDiary',
                VTD1_CSM__c = caseId,
                RecordTypeId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('ePRO').getRecordTypeId(),
                VTR4_Percentage_Completed__c = 100,
                VTD1_Status__c = 'Review Required'
        );
        insert survey;

        Test.startTest();
        survey = [SELECT VTR4_Percentage_Completed__c, RecordType.DeveloperName FROM VTD1_Survey__c WHERE Id = :survey.Id];
        survey.VTR5_Reviewed__c = true;
        String result = VT_R5_EDiariesTableController.updateEdiary(survey);
        Test.stopTest();

        System.assertEquals(String.valueOf(true), result);
        System.assert(survey.VTR5_Reviewed__c);
        System.assertEquals(VT_R4_ConstantsHelper_Statuses.SURVEY_REVIEWED, survey.VTD1_Status__c);
    }

    public static void updateEdiaryNegativeTest() {
        VTD1_Survey__c survey = new VTD1_Survey__c();

        Test.startTest();
        String result = VT_R5_EDiariesTableController.updateEdiary(survey);
        Test.stopTest();


        System.assertNotEquals(String.valueOf(true), result);
    }

    public static void prefiltersTest() {
        Case ptCase = [SELECT Id, VTD1_Study__c FROM Case LIMIT 1];
        String surveyName = 'Test eDiary';

        VTD1_Survey__c survey = new VTD1_Survey__c(
                Name = surveyName,
                VTD1_CSM__c = ptCase.Id,
                RecordTypeId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('ePRO').getRecordTypeId(),
                VTR4_Percentage_Completed__c = 100,
                VTD1_Status__c = VT_R4_ConstantsHelper_Statuses.SURVEY_REVIEW_REQUIRED
        );
        insert survey;

        VTR5_DiaryListView__c dlv = new VTR5_DiaryListView__c(
                VTR5_Filter__c = JSON.serialize(new Map<String, String> {
                        'name' => surveyName,
                        'studyId' => String.valueOf(ptCase.VTD1_Study__c),
                        'status' => VT_R4_ConstantsHelper_Statuses.SURVEY_REVIEW_REQUIRED
                })
        );
        insert dlv;

        Map<String, Object> tableParams = new Map<String, Object>{
                'limit' => (Object) '1000',
                'filterMap' => (Object) new Map<String, Object> {
                        'markReviewedFilter' => (Object) 'false',
                        'statusFilters' => null,
                        'eDiaryNameFilters' => (Object) new List<Object>()
                }
        };

        Test.startTest();
        VT_R5_EDiariesTableController result = VT_R5_EDiariesTableController.getEdiaryRecords(dlv.Id, JSON.serialize(tableParams), null, true);
        Test.stopTest();

        System.assertEquals(1, result.items.size());
        System.assertEquals(survey.Id, result.items[0].eDiary.Id);

    }

    /*******************************************************************************************************
    * @author                   Conquerors: Priyanka Ambre (SH-21178)
    * @description              Tests getEdiaryRecords method with Ediary Filter and respective Answer record 
                                exists through PI User.
    
    @IsTest
    public static void ediaryFilterForPITest() {
        Case ptCase = [SELECT Id, VTD1_Study__c FROM Case LIMIT 1];
        List<Study_Team_Member__c> studyTeamMembers = [select Id,
                                                              User__c, 
                                                       		  User__r.Profile.Name 
                                                       from Study_Team_Member__c 
                                                       where Study__c = :ptCase.VTD1_Study__c AND User__r.Profile.Name =: VT_R4_ConstantsHelper_Profiles.PI_PROFILE_NAME
                                                       AND VTD1_VirtualSite__c <> NULL LIMIT 1];
        VT_R5_eDiary_Filter__c objeDiaryFilter = new VT_R5_eDiary_Filter__c(
            VTR5_eDiary_Name__c = 'test2test2' ,
            VTR5_Label_Name__c = 'Test1',
            VTR5_Question_Content__c = 'Unwell?',
            VTR5_Study__c = ptCase.VTD1_Study__c,
            VTR5_Answer_Content__c = 'YES'
        );
        insert objeDiaryFilter;
        System.assertNotEquals(null, objeDiaryFilter.Id);


        List<VTD1_Survey_Answer__c> lstSurveyAnswers = [SELECT    Id,
                                                                  VTD1_Survey__c,
                                                                  VTD1_Answer__c
                                                        FROM VTD1_Survey_Answer__c
                                                        WHERE VT_R5_External_Widget_Data_Key__c  =: objeDiaryFilter.VTR5_Question_Content__c
                                                        AND VTD1_Survey__r.Name = 'test2test2' LIMIT 1];
        
        //Need to create share records as VTD1_Survey_Answer__c records are not shared with PI in test Methods.
        createShareRecord(studyTeamMembers[0].User__c, ptCase.Id);

         Map<String, Object> tableParams = new Map<String, Object>{
                'limit' => (Object) '1000',
                'filterMap' => (Object) new Map<String, Object> {
                        'markReviewedFilter' => (Object) 'false',
                        'statusFilters' => null,
                        'eDiaryNameFilters' => (Object) new List<Object>(),
                        'studyId' => ptCase.VTD1_Study__c
                },
                'selectedResponse' => (Object) new Map<String, Object> {
                        'VTR5_eDiary_Name__c' => 'test2test2',
                        'VTR5_Label_Name__c' => '',
                        'VTR5_Study__c' => ptCase.VTD1_Study__c,
                        'VTR5_Answer_Content__c' => 'YES',
                        'VTR5_Question_Content__c' => 'Unwell?'
                }
        };


        Test.startTest();
        System.runAs(new User(Id = studyTeamMembers[0].User__c)){
           
            VT_R5_EDiariesTableController result = VT_R5_EDiariesTableController.getEdiaryRecords(null, JSON.serialize(tableParams), null, true);
            System.assertEquals(lstSurveyAnswers[0].VTD1_Survey__c, result.items[0].eDiary.Id);
        }
        Test.stopTest();
    }*/

    /*******************************************************************************************************
    * @author                   Conquerors: Priyanka Ambre (SH-21178)
    * @description              Tests getEdiaryRecords method with Ediary Filter and respective Answer record exists
                                through SCR User.
    
    @IsTest
    public static void ediaryFilterForSCRTest() {
        Case ptCase = [SELECT Id, VTD1_Study__c FROM Case LIMIT 1];
        Study_Team_Member__c stmSCR = [SELECT Id, User__c 
                                        FROM Study_Team_Member__c
                                        WHERE Study__c =:  ptCase.VTD1_Study__c LIMIT 1];

        VT_R5_eDiary_Filter__c objeDiaryFilter = new VT_R5_eDiary_Filter__c(
            VTR5_eDiary_Name__c = 'test2test2' ,
            VTR5_Label_Name__c = 'Test1',
            VTR5_Question_Content__c = 'Unwell?',
            VTR5_Study__c = ptCase.VTD1_Study__c,
            VTR5_Answer_Content__c = 'YES'
        );
        insert objeDiaryFilter;
        System.assertNotEquals(null, objeDiaryFilter.Id);

        List<VTD1_Survey_Answer__c> lstSurveyAnswers = [SELECT    Id,
                                                                  VTD1_Survey__c,
                                                                  VTD1_Answer__c
                                                        FROM VTD1_Survey_Answer__c
                                                        WHERE VT_R5_External_Widget_Data_Key__c  =: objeDiaryFilter.VTR5_Question_Content__c
                                                        AND VTD1_Survey__r.Name =: objeDiaryFilter.VTR5_eDiary_Name__c LIMIT 1];
        
        //Need to create share records as VTD1_Survey_Answer__c records are not shared with SCR in test Methods.
        createShareRecord(stmSCR.User__c, ptCase.Id);

         Map<String, Object> tableParams = new Map<String, Object>{
                'limit' => (Object) '1000',
                'filterMap' => (Object) new Map<String, Object> {
                        'markReviewedFilter' => (Object) 'false',
                        'statusFilters' => null,
                        'eDiaryNameFilters' => (Object) new List<Object>(),
                        'studyId' => ptCase.VTD1_Study__c
                },
                'selectedResponse' => (Object) new Map<String, Object> {
                        'VTR5_eDiary_Name__c' => 'test2test2',
                        'VTR5_Label_Name__c' => '',
                        'VTR5_Study__c' => ptCase.VTD1_Study__c,
                        'VTR5_Answer_Content__c' => 'YES',
                        'VTR5_Question_Content__c' => 'Unwell?'
                }
        };

        Test.startTest();
        System.runAs(new User(Id = stmSCR.User__c)){
            VT_R5_EDiariesTableController result = VT_R5_EDiariesTableController.getEdiaryRecords(null, JSON.serialize(tableParams), null, true);
            System.assertEquals(lstSurveyAnswers[0].VTD1_Survey__c, result.items[0].eDiary.Id);
        }
        Test.stopTest();
    }*/

    /*******************************************************************************************************
    * @author                   Conquerors: Priyanka Ambre (SH-21178)
    * @description              Tests getEdiaryRecords method with Ediary Filter and respective Answer record 
                                exists through PI User.
	/*
    
    @IsTest
    public static void ediaryFilterForPINoAnswersTest() {
        Case ptCase = [SELECT Id, VTD1_Study__c FROM Case LIMIT 1];

        Study_Team_Member__c stmSCR = [SELECT Id, User__c 
                                        FROM Study_Team_Member__c
                                        WHERE Study__c =:  ptCase.VTD1_Study__c LIMIT 1];

        VT_R5_eDiary_Filter__c objeDiaryFilter = new VT_R5_eDiary_Filter__c(
            VTR5_eDiary_Name__c = 'test2test2' ,
            VTR5_Label_Name__c = 'Test1',
            VTR5_Question_Content__c = 'Well?',
            VTR5_Study__c = ptCase.VTD1_Study__c,
            VTR5_Answer_Content__c = 'YES'
        );
        insert objeDiaryFilter;
        System.assertNotEquals(null, objeDiaryFilter.Id);


        List<VTD1_Survey_Answer__c> lstSurveyAnswers = [SELECT    Id,
                                                                  VTD1_Survey__c,
                                                                  VTD1_Answer__c
                                                        FROM VTD1_Survey_Answer__c
                                                        WHERE VT_R5_External_Widget_Data_Key__c  =: objeDiaryFilter.VTR5_Question_Content__c
                                                        AND VTD1_Survey__r.Name =: objeDiaryFilter.VTR5_eDiary_Name__c LIMIT 1];
        
        //Need to create share records as VTD1_Survey_Answer__c records are not shared with PI in test Methods.
        createShareRecord(stmSCR.User__c, ptCase.Id);

         Map<String, Object> tableParams = new Map<String, Object>{
                'limit' => (Object) '1000',
                'filterMap' => (Object) new Map<String, Object> {
                        'markReviewedFilter' => (Object) 'false',
                        'statusFilters' => null,
                        'eDiaryNameFilters' => (Object) new List<Object>(),
                        'studyId' => ptCase.VTD1_Study__c
                },
                'selectedResponse' => (Object) new Map<String, Object> {
                        'VTR5_eDiary_Name__c' => 'test2test2',
                        'VTR5_Label_Name__c' => '',
                        'VTR5_Study__c' => ptCase.VTD1_Study__c,
                        'VTR5_Answer_Content__c' => 'YES',
                        'VTR5_Question_Content__c' => 'Well?'
                }
        };


        Test.startTest();
        System.runAs(new User(Id = stmSCR.User__c)){
            VT_R5_EDiariesTableController result = VT_R5_EDiariesTableController.getEdiaryRecords(null, JSON.serialize(tableParams), null, true);
            System.assertEquals(0, result.items.size());
        }
        Test.stopTest();
    }

    public static void createShareRecord(Id userId, Id caseId){
        CaseShare objVirtualShare = new CaseShare();
        objVirtualShare.CaseId = caseId;
        objVirtualShare.UserOrGroupId = userId;
        objVirtualShare.CaseAccessLevel = 'Edit';
        objVirtualShare.RowCause = 'Manual';
        insert objVirtualShare;
    }
    
    /*******************************************************************************************************
    * @author                   Vijendra Hire (SH-20622)
    * @description              Method covers the code coverage of totalScoreValue value filter.
    */
    
    public static void getEDiariesTotalScoreDataTest(){
        
        VTR5_DiaryListView__c dlv = new VTR5_DiaryListView__c();
        insert dlv;
        Case ptCase = [SELECT Id, VTD1_Study__c FROM Case LIMIT 1];       
               
        Map<String, Object> params = new Map<String, Object>{'limit' => (Object) '1000'};
        VT_R5_EDiariesTableController eData = VT_R5_EDiariesTableController.getEdiaryRecords(null, JSON.serialize(params), null, true);
       List<VT_R5_EDiariesTableController.EDiaryWrapper> eDiaries = eData.items;
        Test.startTest();
            Map<String, Object> filters = new Map<String, Object>{'markReviewedFilter' => (Object) 'false', 'totalScoreValue' => (Object) '0',
                                                                  'statusFilters' => (Object) new List<String>{eDiaries.get(0).eDiary.VTD1_Status__c}};
            Map<String, Object> order = new Map<String, Object>{'columnName' => 'eDiaryName', 'sorting' => 'asc'};                                                       

            params.put('filterMap', filters);
            String searchName = eDiaries.get(0).eDiary.Name.substring(0, 3);
            eData = VT_R5_EDiariesTableController.getEdiaryRecords(dlv.Id, JSON.serialize(params), searchName, true);
        
            order = new Map<String, Object>{'columnName' => 'phoneNumber', 'sorting' => 'asc'};
            params.put('order', order);
            eData = VT_R5_EDiariesTableController.getEdiaryRecords(dlv.Id, JSON.serialize(params), searchName, true);
            
            params.put('order', order);
            order = new Map<String, Object>{'columnName' => 'dateCompleted', 'sorting' => 'asc'};
            eData = VT_R5_EDiariesTableController.getEdiaryRecords(dlv.Id, JSON.serialize(params), searchName, true);
        
				
        	params.put('order', order);
            order = new Map<String, Object>{'columnName' => 'totalScore', 'sorting' => 'asc'};
            eData = VT_R5_EDiariesTableController.getEdiaryRecords(dlv.Id, JSON.serialize(params), '0', true);
        	List<VT_R5_EDiariesTableController.EDiaryWrapper> eDiary = eData.items;
        Test.stopTest();
       // System.assertEquals(1, eData.items.size());
           
    }
	
}