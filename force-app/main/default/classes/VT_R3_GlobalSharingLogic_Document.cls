/**
* @author: Carl Judge
* @date: 19-Aug-19
* @description: Unified Apex sharing for all VT_D1_Document__c records. Jira IQVTR2NEO-831 for inital requirements
**/

public without sharing class VT_R3_GlobalSharingLogic_Document extends VT_R3_AbstractGlobalSharingLogic{


    private Map<Id, List<VTD1_Document__c>> documentsByRecTypeId = new Map<Id, List<VTD1_Document__c>>();
    private List<VTD1_Document__c> groupShareDocs = new List<VTD1_Document__c>();
    private Map<Id, Set<Id>> siteStmsByDocId = new Map<Id, Set<Id>>();
    private Map<Id, Id> docToStudyIdMap = new Map<Id, Id>();
    private Map<Id, Map<String, List<Study_Team_Member__c>>> studyMembersByStudyIdAndProfile = new Map<Id, Map<String, List<Study_Team_Member__c>>>();
    private Map<Id, HealthCloudGA__CarePlanTemplate__c> studiesById = new Map<Id, HealthCloudGA__CarePlanTemplate__c>();
    private Map<Id, Case> casesById = new Map<Id, Case>();
    private Map<Id, List<Id>> consentPacketToCaseIds = new Map<Id, List<Id>>();
    private List<ShareDetail> newPatientShares = new List<ShareDetail>();
    public Set<Id> docRecTypeIds = new Set<Id>();

    public override Type getType() {
        return VT_R3_GlobalSharingLogic_Document.class;
    }

    public override VTR3_GlobalSharingConfig__mdt getConfig() {
        return getConfigForObjectType(VTD1_Document__c.getSObjectType().getDescribe().getName());
    }

    protected override Integer getAdjustedQueryLimit() {
        Decimal maxDetailsDecimal = 0.0 + SHARING_SETTINGS.VTR5_MaxDocumentShares__c.intValue();
        Decimal adjustedLimit = Math.ceil(maxDetailsDecimal / includedUserIds.size());
        return adjustedLimit.intValue();
    }

    protected override void addShareDetailsForRecords() {
        getDocumentsByRecTypeId(recordIds);
        if (!groupShareDocs.isEmpty()) {
            generateGroupShares();
        }
        if (!documentsByRecTypeId.isEmpty()) {
            getSiteStmIds();
            getStudyIdsForDocs();
            queryOtherRecords();
            generateNewShareDetails();
        }
    }

    protected override Iterable<Object> getUserBatchIterable() {
        return (Iterable<Object>) Database.getQueryLocator(getUserQuery());
    }

    protected override void doUserQueryPrep() {
        groupUserIdsByProfile();
        if (!profileToUserIds.isEmpty()) {
            getDocRecTypeIds();
        }
    }

    protected override List<SObject> executeUserQuery() {
        if (!profileToUserIds.isEmpty()) {
            String query = getUserQuery();
            if (query != null) {
                query += ' LIMIT ' + getAdjustedQueryLimit();
                return Database.query(query);
            }
        }
        return null;
    }

    protected override void createUserShareDetailsFromRecords(List<SObject> records) {
        recordIds = new Map<Id, SObject>(records).keySet();
        reset();
        addShareDetailsForRecords();
        filterByIncludedUsers();
    }

    private void getDocumentsByRecTypeId(Set<Id> docIds) {
        for (VTD1_Document__c item : [
            SELECT Id,
                RecordTypeId,
                OwnerId,
                VTD1_Study__c,
                VTD1_Clinical_Study_Membership__c,
                CreatedBy.Profile.Name,
                VTD1_Site__r.VTD1_Study_Team_Member__c,
                VTD1_Clinical_Study_Membership__r.VTD1_Study__c,
                VTD1_Status__c, VTD2_Redacted__c,
                VTD1_Regulatory_Document_Type__c,
                VTD1_Regulatory_Binder__r.VTD1_Regulatory_Document__r.Document_Visible_to_Site__c,
                VTD1_Regulatory_Binder__r.VTD1_Regulatory_Document__c,
                VTD1_Site__c,
                VTD1_Site__r.VTR2_Backup_PI_STM__c,
                VTD1_Site__r.VTR2_Primary_SCR__c,
                VTD1_Site__r.VTD1_Study__c,
                VTD1_Site__r.VTR5_EditShareGroupId__c,
                VTD1_Site__r.VTR5_ReadShareGroupId__c,
                VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR2_Backup_PI_STM__c,
                VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR2_Primary_SCR__c,
                VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTD1_Study__c,
                VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c,
                VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c,
                VTD1_Site__r.VTR3_Study_Geography__c,
                VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR3_Study_Geography__c,
                VTD1_Regulatory_Binder__r.VTD1_Regulatory_Document__r.Level__c,
                VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c,
                VTD2_Associated_Medical_Record_id__c,
                VTD2_Associated_Medical_Record_id__r.RecordTypeId,
                VTR3_Category__c
            FROM VTD1_Document__c
            WHERE Id IN :docIds
            AND (RecordTypeId IN :VT_D2_DocumentSharing_Constants.SHARING_CONFIG.keySet() OR RecordTypeId IN :VT_D2_DocumentSharing_Constants.GROUP_SHARE_RECORDTYPE_IDS)
        ]) {
            if (VT_D2_DocumentSharing_Constants.GROUP_SHARE_RECORDTYPE_IDS.contains(item.RecordTypeId)) {
                groupShareDocs.add(item);
            } else {
                if (!documentsByRecTypeId.containsKey(item.RecordTypeId)) {
                    documentsByRecTypeId.put(item.RecordTypeId, new List<VTD1_Document__c>());
                }
                documentsByRecTypeId.get(item.RecordTypeId).add(item);
            }
        }
    }

    private void generateGroupShares() {
        for (VTD1_Document__c doc : groupShareDocs) {
            Virtual_Site__c site = getSite(doc);
            if (site != null) {
                addShareDetail(doc.Id, site.VTR5_ReadShareGroupId__c, site.VTD1_Study__c, 'Read');
                addShareDetail(doc.Id, site.VTR5_EditShareGroupId__c, site.VTD1_Study__c, 'Edit');
            }
        }
    }

    private void getSiteStmIds() {
        List<VTD1_Document__c> docsToShareWithSstms = new List<VTD1_Document__c>(); // docs to be shared with Site PIs, CRAs, and SSTMS related to PIs, otherwise just PI and Sub Is
        Map<Id, Virtual_Site__c> sites = new Map<Id, Virtual_Site__c>();
        for (List<VTD1_Document__c> docList : this.documentsByRecTypeId.values()) {
            for (VTD1_Document__c doc : docList) {
                if (toShareWithSiteMembers(doc)) {
                    Virtual_Site__c site = getSite(doc);
                    if (site != null) {
                        docsToShareWithSstms.add(doc);
                        sites.put(site.Id, site);
                    }
                }
            }
        }
        if (!sites.isEmpty()) {
            Map<Id, Set<Id>> siteToSTMIdsMap = getSiteToSTMIdsMap(new List<Virtual_Site__c>(sites.values()));
            for (VTD1_Document__c doc : docsToShareWithSstms) {
                Virtual_Site__c site = getSite(doc);
                if (siteToSTMIdsMap.containsKey(site.Id)) {
                    siteStmsByDocId.put(doc.Id, siteToSTMIdsMap.get(site.Id));
                }
            }
        }
    }

    private Map<Id, Set<Id>> getSiteToSTMIdsMap(List<Virtual_Site__c> sites) {
        VT_R5_SiteMemberCalculator calc = new VT_R5_SiteMemberCalculator();
        calc.initForSites(new Map<Id, Virtual_Site__c>(sites).keySet());

        List<Id> studyIds = new List<Id>();
        for (Virtual_Site__c site : sites) studyIds.add(site.VTD1_Study__c);
        Map<String, Id> stmByStudyUser = new Map<String, Id>();
        for (Study_Team_Member__c stm :[
            SELECT Id, Study__c, User__c
            FROM Study_Team_Member__c
            WHERE Study__c IN :studyIds
            AND User__c IN :calc.getUserIds()
        ]) {
            stmByStudyUser.put('' + stm.Study__c + stm.User__c, stm.Id);
        }

        Map<Id, Set<Id>> siteToSTMIdsMap = new Map<Id, Set<Id>>();
        for (Virtual_Site__c site : sites) {
            siteToSTMIdsMap.put(site.Id, new Set<Id>());
            for (Id userId : calc.getSiteUserIds(site.Id)) {
                String key = '' + site.VTD1_Study__c + userId;
                if (stmByStudyUser.containsKey(key)) {
                    siteToSTMIdsMap.get(site.Id).add(stmByStudyUser.get(key));
                }
            }
        }

        return siteToSTMIdsMap;
    }

    private Virtual_Site__c getSite(VTD1_Document__c doc) {
        return doc.VTD1_Site__c != null ? doc.VTD1_Site__r : doc.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r;
    }

    private Id getSitePiStmId(VTD1_Document__c doc) {
        return doc.VTD1_Site__r.VTD1_Study_Team_Member__c != null ?
            doc.VTD1_Site__r.VTD1_Study_Team_Member__c : doc.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c;
    }

    private void getStudyIdsForDocs() {
        for (Id recTypeId : documentsByRecTypeId.keySet()) {
            if (recTypeId != VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PA_SIGNATURE_PAGE) {
                for (VTD1_Document__c doc : documentsByRecTypeId.get(recTypeId)) {
                    if (doc.VTD1_Study__c != null) {
                        docToStudyIdMap.put(doc.Id, doc.VTD1_Study__c);
                    } else if (doc.VTD1_Clinical_Study_Membership__r.VTD1_Study__c != null) {
                        docToStudyIdMap.put(doc.Id, doc.VTD1_Clinical_Study_Membership__r.VTD1_Study__c);
                    }
                    else if (doc.VTD1_Site__r.VTD1_Study__c != null) {
                        docToStudyIdMap.put(doc.Id, doc.VTD1_Site__r.VTD1_Study__c);
                    }
                }
            } else {
                for (VTD1_Protocol_Amendment__c pa : [
                    SELECT Id, VTD1_Study_ID__c, VTD1_Protocol_Amendment_Document_Link__c
                    FROM VTD1_Protocol_Amendment__c
                    WHERE VTD1_Protocol_Amendment_Document_Link__c IN :documentsByRecTypeId.get(recTypeId)
                ]) {
                    docToStudyIdMap.put(pa.VTD1_Protocol_Amendment_Document_Link__c, pa.VTD1_Study_ID__c);
                }
            }
        }
    }

    private void queryOtherRecords() {
        Set<String> memberProfiles = new Set<String>();
        Set<String> studyFields = new Set<String>();
        Set<String> caseFields = new Set<String>();
        Set<Id> caseIds = new Set<Id>();

        for (Id recTypeId : documentsByRecTypeId.keySet()) {
            Map<VT_D2_DocumentSharing_Constants.ProfileOrFieldType, Map<String, String>> roleMap = VT_D2_DocumentSharing_Constants.SHARING_CONFIG.get(recTypeId);
            memberProfiles.addAll(roleMap.get(VT_D2_DocumentSharing_Constants.ProfileOrFieldType.MEMBER_PROFILE).keySet());
            studyFields.addAll(roleMap.get(VT_D2_DocumentSharing_Constants.ProfileOrFieldType.STUDY_FIELD).keySet());
            Set<String> caseFieldSet = roleMap.get(VT_D2_DocumentSharing_Constants.ProfileOrFieldType.CASE_FIELD).keySet();
            if (! caseFieldSet.isEmpty()) {
                caseFields.addAll(caseFieldSet);
                for (VTD1_Document__c doc : documentsByRecTypeId.get(recTypeId)) {
                    if (doc.VTD1_Clinical_Study_Membership__c != null) {
                        caseIds.add(doc.VTD1_Clinical_Study_Membership__c);
                    }
                }
            }
        }

        if (! memberProfiles.isEmpty()) { queryMembers(memberProfiles); }
        if (! studyFields.isEmpty()) { queryStudies(studyFields); }

        Set<Id> consentPacketIds = new Set<Id>();
        if (documentsByRecTypeId.containsKey(VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_INFORMED_CONSENT_GUIDE)) {
            consentPacketIds = new Map<Id, VTD1_Document__c>(
                documentsByRecTypeId.get(VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_INFORMED_CONSENT_GUIDE)
            ).keySet();
        }

        if (! caseFields.isEmpty() && (! caseIds.isEmpty() || ! consentPacketIds.isEmpty())) {
            queryCases(caseFields, caseIds, consentPacketIds);
        }
    }

    private void queryMembers(Set<String> memberProfiles) {
        Set<Id> allExtraStmIds = new Set<Id>();
        for (Set<Id> extraStmIdsSet : siteStmsByDocId.values()) {
            allExtraStmIds.addAll(extraStmIdsSet);
        }
        for (Study_Team_Member__c item : [
            SELECT Id, Study__c, User__c, User__r.Profile.Name, Study__r.VTD2_TMA_Blinded_for_Study__c
            FROM Study_Team_Member__c
            WHERE (Study__c IN :docToStudyIdMap.values() AND User__r.Profile.Name IN :memberProfiles)
            OR Id IN :allExtraStmIds
        ]) {
            if (includedUserIds == null || includedUserIds.contains(item.User__c)) {
                if (! studyMembersByStudyIdAndProfile.containsKey(item.Study__c)) {
                    studyMembersByStudyIdAndProfile.put(item.Study__c, new Map<String, List<Study_Team_Member__c>>());
                }
                if (! studyMembersByStudyIdAndProfile.get(item.Study__c).containsKey(item.User__r.Profile.Name)) {
                    studyMembersByStudyIdAndProfile.get(item.Study__c).put(item.User__r.Profile.Name, new List<Study_Team_Member__c>());
                }
                studyMembersByStudyIdAndProfile.get(item.Study__c).get(item.User__r.Profile.Name).add(item);
            }
        }
    }

    private void queryStudies(Set<String> studyFields) {
        List<Id> studyIds = docToStudyIdMap.values();
        for (HealthCloudGA__CarePlanTemplate__c item :Database.query(
            'SELECT Id,' + String.join(new List<String>(studyFields),',') +
                ' FROM HealthCloudGA__CarePlanTemplate__c' +
                ' WHERE Id IN :studyIds'
        )) {
            studiesById.put(item.Id, item);
        }
    }

    private void queryCases(Set<String> caseFields, Set<Id> caseIds, Set<Id> consentPacketIds) {
        for (Case item :Database.query(
            'SELECT Id,VTR2_Consent_Packet__c,' + String.join(new List<String>(caseFields),',') +
                ' FROM Case' +
                ' WHERE Id IN :caseIds' +
                ' OR VTR2_Consent_Packet__c IN :consentPacketIds'
        )) {
            casesById.put(item.Id, item);
            if (consentPacketIds.contains(item.VTR2_Consent_Packet__c)) {
                if (! consentPacketToCaseIds.containsKey(item.VTR2_Consent_Packet__c)) {
                    consentPacketToCaseIds.put(item.VTR2_Consent_Packet__c, new List<Id>());
                }
                consentPacketToCaseIds.get(item.VTR2_Consent_Packet__c).add(item.Id);
            }
        }
    }

    private void generateNewShareDetails() {
        Decimal maxHeapSize = Limits.getLimitHeapSize() * 0.8;
        Decimal maxCpuTime = Limits.getLimitCpuTime() * 0.8;
        for (List<VTD1_Document__c> docList : documentsByRecTypeId.values()) {
            for (VTD1_Document__c doc : docList) {
                if (Limits.getHeapSize() > maxHeapSize || Limits.getCpuTime() > maxCpuTime && !System.isBatch()) {
                    forceBatch = true;
                    return;
                }
                for (VT_D2_DocumentSharing_Constants.ProfileOrFieldType uType : VT_D2_DocumentSharing_Constants.ProfileOrFieldType.values()) {
                    for (String profileOrField : VT_D2_DocumentSharing_Constants.SHARING_CONFIG.get(doc.RecordTypeId).get(uType).keySet()) {
                        String accessLevel = getAccessLevel(doc, uType, profileOrField);
                        if (uType == VT_D2_DocumentSharing_Constants.ProfileOrFieldType.MEMBER_PROFILE) {
                            generateMemberSharesFromParams(doc, profileOrField, accessLevel);
                        } else {
                            generateObjFieldShareFromParams(doc, uType, profileOrField, accessLevel);
                        }
                    }
                }
            }
        }

        if (!newPatientShares.isEmpty()) {
            generateCaregiverShares();
        }
    }

    private String getAccessLevel(VTD1_Document__c doc, VT_D2_DocumentSharing_Constants.ProfileOrFieldType uType, String profileOrField) {
        String accessLevel;
        // default access level
        if (accessLevel == null) {
            accessLevel = VT_D2_DocumentSharing_Constants.SHARING_CONFIG.get(doc.RecordTypeId).get(uType).get(profileOrField);
        }
        return accessLevel;
    }

    private void generateMemberSharesFromParams(VTD1_Document__c doc, String profileOrField, String accessLevel) {
        Id studyId = docToStudyIdMap.get(doc.Id);

        if (studyMembersByStudyIdAndProfile.containsKey(studyId) &&
            studyMembersByStudyIdAndProfile.get(studyId).containsKey(profileOrField))
        {
            for (Study_Team_Member__c member : studyMembersByStudyIdAndProfile.get(studyId).get(profileOrField)) {
                if (toCreateMemberShare(member, doc, profileOrField)) {
                    ShareDetail newShare = getNewShareDetail(member.User__c, doc.Id, accessLevel);
                    String key = '' + doc.Id + member.User__c;
                    shareDetails.put(key, newShare);
                }
            }
        }
    }

    private void generateObjFieldShareFromParams(VTD1_Document__c doc, VT_D2_DocumentSharing_Constants.ProfileOrFieldType uType, String profileOrField, String accessLevel) {
        List<SObject> objs = new List<SObject>();

        if (uType == VT_D2_DocumentSharing_Constants.ProfileOrFieldType.STUDY_FIELD) {
            objs.add(studiesById.get(docToStudyIdMap.get(doc.Id)));
        } else if (consentPacketToCaseIds.containsKey(doc.Id)){
            for (Id caseId : consentPacketToCaseIds.get(doc.Id)) {
                objs.add(casesById.get(caseId));
            }
        } else {
            objs.add(casesById.get(doc.VTD1_Clinical_Study_Membership__c));
        }

        if (! objs.isEmpty()) {
            for (SObject obj : objs) {
                if(obj != null) {
                    Id userId = (Id) obj.get(profileOrField);
                    if (toCreateObjFieldShare(userId, doc, profileOrField)) {
                        ShareDetail newShare = getNewShareDetail(userId, doc.Id, accessLevel);
                        String key = '' + doc.Id + userId;
                        shareDetails.put(key, newShare);
                        if (profileOrField == 'VTD1_Patient_User__c') {
                            newPatientShares.add(newShare);
                        }
                    }
                }
            }
        }
    }

    private void generateCaregiverShares() {
        List<Id> patientIds = new List<Id>();
        for (ShareDetail pShare : newPatientShares) {
            patientIds.add(pShare.userOrGroupId);
        }

        Map<Id, Id> patientToCaregiverIds = VT_D1_PatientCaregiverBound.findCaregiversOfPatients(patientIds);
        for (ShareDetail pShare : newPatientShares) {
            Id uId = patientToCaregiverIds.get(pShare.userOrGroupId);
            if (uId != null) {
                ShareDetail newShare = getNewShareDetail(uId, pShare.recordId, pShare.accessLevel);
                String key = '' + pShare.recordId + uId;
                shareDetails.put(key, newShare);
            }
        }
    }

    @TestVisible
    // any extra conditions for creating shares should go in these 2 methods. For example TMA can only see medical records if they are redacted or study is not blind for TMA.
    private Boolean toCreateMemberShare(Study_Team_Member__c member, VTD1_Document__c doc, String profileName) {
        Boolean isSiteVisible = checkIsSiteVisible(doc);
        return
            member.User__c != null
                //1 TMA can only see medical records if they are redacted or study is not blind for TMA.
                &&  !(profileName == VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME && isMedicalDocument(doc) && ! doc.VTD2_Redacted__c && member.Study__r.VTD2_TMA_Blinded_for_Study__c)

                //2 PI can only be added from STM if it is the STM linked through a linked site. PIs can also be granted access from being on the case or study without this restriction
                &&  !(profileName == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME && member.Id != getSitePiStmId(doc) && !isSiteVisible)

                //3 for medical records, CM STM will only get access if status is approved
                &&  !(profileName == VT_R4_ConstantsHelper_ProfilesSTM.CM_PROFILE_NAME && isMedicalDocument(doc) && doc.VTD1_Status__c != VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED)

                //4 for visit documents, CM STM will only get access if the document has been categorized
                &&  !(profileName == VT_R4_ConstantsHelper_ProfilesSTM.CM_PROFILE_NAME && doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT && doc.VTR3_Category__c == VT_R4_ConstantsHelper_Documents.DOCUMENT_CATEGORY_UNCATEGORIZED)

                //5 for medical records, CRA STM will only get access if document is categorized and status is approved ** removed for SH-8070 ** ** added again for SH-12804 **
                &&  !(profileName == VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME && isMedicalDocument(doc) && (doc.VTD1_Status__c != VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED || doc.VTR3_Category__c == VT_R4_ConstantsHelper_Documents.DOCUMENT_CATEGORY_UNCATEGORIZED))

                //6 if Site Visible is false => invisible for PIs, Patient Guides, Site Coordinators
                &&  !(profileName == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME && !isSiteVisible)
                &&  !(profileName == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME && !isSiteVisible)
                &&  !(profileName == VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME && !isSiteVisible)

                //7 share certain profiles with SSTMs via site rather than STMs on study, if doc is of these types
                &&  !(VT_D2_DocumentSharing_Constants.SITE_RELATED_PROFILES.contains(profileName) && toShareWithSiteMembers(doc) && (!siteStmsByDocId.containsKey(doc.Id) || !siteStmsByDocId.get(doc.Id).contains(member.Id)))

                //8 reg specialists should only see site level docs
                &&  !(profileName == VT_R4_ConstantsHelper_ProfilesSTM.RS_PROFILE_NAME && doc.VTD1_Regulatory_Binder__r.VTD1_Regulatory_Document__r.Level__c != VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_LEVEL_SITE)
            ;
    }

    private ShareDetail getNewShareDetail(Id userOrGroupId, Id docId, String accessLevel) {
        ShareDetail detail = new ShareDetail();
        detail.userOrGroupId = userOrGroupId;
        detail.recordId = docId;
        detail.accessLevel = accessLevel;
        detail.studyId = docToStudyIdMap.get(docId);
        return detail;
    }

    private Boolean toCreateObjFieldShare(Id userId, VTD1_Document__c doc, String objField) {
        return
            userId != null
                // For medical records, patients (and by extension caregivers) can only see documents if they are approved or they were not uploaded from the portal
                &&  (!isMedicalDocument(doc) || objField != 'VTD1_Patient_User__c' || doc.VTD1_Status__c == VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED || ! isPortalUpload(doc))

                // For medical records, CRA as Study Participants will only get access if status is approved
                && !(objField == 'VTD1_Remote_CRA__c' && isMedicalDocument(doc) && (doc.VTD1_Status__c != VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED || doc.VTR3_Category__c == VT_R4_ConstantsHelper_Documents.DOCUMENT_CATEGORY_UNCATEGORIZED))

                // For note of transfer, only share with PGs from Case
                &&  !(doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_NOTE_OF_TRANSFER && (objField == 'VTD1_Primary_PG__c' || objField == 'VTD1_Secondary_PG__c'))

                //
                &&  !(objField == 'VTD1_Regulatory_Specialist__c' && isRegSpecialistDocRecType(doc.RecordTypeId) && doc.VTD1_Regulatory_Binder__r.VTD1_Regulatory_Document__r.Level__c == VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_LEVEL_SITE)
            ;
    }

    private Boolean checkIsSiteVisible(VTD1_Document__c doc){
        if (doc.VTD1_Regulatory_Binder__r.VTD1_Regulatory_Document__c != null) {
            return doc.VTD1_Regulatory_Binder__r.VTD1_Regulatory_Document__r.Document_Visible_to_Site__c;
        }
        return true;
    }

    private Boolean isMedicalDocument(VTD1_Document__c doc) {
        return VT_D2_DocumentSharing_Constants.MEDICAL_DOCUMENT_RECORDTYPE_IDS.contains(doc.RecordTypeId);
    }

    // find whether the document was uploaded through the portal by looking at the profile of creator
    private Boolean isPortalUpload(VTD1_Document__c doc) {
        return ! VT_D2_DocumentSharing_Constants.NON_PORTAL_PROFILES.contains(doc.CreatedBy.Profile.Name);
    }

    private Boolean isRegSpecialistDocRecType(Id recTypeId) {
        return VT_D2_DocumentSharing_Constants.SHARING_CONFIG.get(recTypeId).get(VT_D2_DocumentSharing_Constants.ProfileOrFieldType.MEMBER_PROFILE).containsKey(VT_R4_ConstantsHelper_ProfilesSTM.RS_PROFILE_NAME);
    }

    private Boolean toShareWithSiteMembers(VTD1_Document__c doc) {
        return  doc.VTD1_Regulatory_Binder__r.VTD1_Regulatory_Document__r.Level__c == VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_LEVEL_SITE
            ||  doc.VTD1_Regulatory_Binder__r.VTD1_Regulatory_Document__r.Level__c == VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_LEVEL_SUBJECT
            ||  doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_NOTE_OF_TRANSFER
            ||  doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT
            ||  doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON
            ||  VT_D2_DocumentSharing_Constants.MEDICAL_DOCUMENT_RECORDTYPE_IDS.contains(doc.RecordTypeId)
            ;
    }

    protected override void reset() {
        super.reset();
        documentsByRecTypeId.clear();
        siteStmsByDocId.clear();
        docToStudyIdMap.clear();
        studyMembersByStudyIdAndProfile.clear();
        studiesById.clear();
        casesById.clear();
        consentPacketToCaseIds.clear();
        newPatientShares.clear();
    }

    private String getUserQuery() {
        String query = 'SELECT Id FROM VTD1_Document__c WHERE RecordTypeId IN :docRecTypeIds';
        if (scopedCaseIds != null) {
            query += ' AND VTD1_Clinical_Study_Membership__c IN :scopedCaseIds';
        } else if (scopedSiteIds != null) {
            query += ' AND (VTD1_Site__c IN :scopedSiteIds OR VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__c IN :scopedSiteIds)';
        } else if (scopedStudyIds != null) {
            query += ' AND (VTD1_Study__c IN :scopedStudyIds ' +
                'OR VTD1_Clinical_Study_Membership__r.VTD1_Study__c IN :scopedStudyIds ' +
                'OR VTD1_Site__r.VTD1_Study__c IN :scopedStudyIds)';
        } else {
            return null;
        }
        System.debug(query);
        return query;
    }

    protected override String getRemoveQuery() {
        String removeQuery = 'SELECT Id FROM VTD1_Document__Share WHERE (';
        if (scopedCaseIds != null) {
            removeQuery += 'Parent.VTD1_Clinical_Study_Membership__c IN :scopedCaseIds';
        } else if (scopedSiteIds != null) {
            removeQuery += 'Parent.VTD1_Site__c IN :scopedSiteIds ' +
                'OR Parent.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__c IN :scopedSiteIds';
        } else if (scopedStudyIds != null) {
            removeQuery += 'Parent.VTD1_Study__c IN :scopedStudyIds ' +
                'OR Parent.VTD1_Clinical_Study_Membership__r.VTD1_Study__c IN :scopedStudyIds ' +
                'OR Parent.VTD1_Site__r.VTD1_Study__c IN :scopedStudyIds';
        }
        removeQuery += ') AND UserOrGroupId IN :includedUserIds';
        return removeQuery;
    }

    private void getDocRecTypeIds() {
        for (Id docRecTypeId : VT_D2_DocumentSharing_Constants.SHARING_CONFIG.keySet()) {
            Map<VT_D2_DocumentSharing_Constants.ProfileOrFieldType, Map<String, String>> userTypeToValueMap = VT_D2_DocumentSharing_Constants.SHARING_CONFIG.get(docRecTypeId);
            Set<String> memberProfiles = userTypeToValueMap.get(VT_D2_DocumentSharing_Constants.ProfileOrFieldType.MEMBER_PROFILE).keySet();
            Set<String> studyFields = userTypeToValueMap.get(VT_D2_DocumentSharing_Constants.ProfileOrFieldType.MEMBER_PROFILE).keySet();
            Set<String> caseFields = userTypeToValueMap.get(VT_D2_DocumentSharing_Constants.ProfileOrFieldType.CASE_FIELD).keySet();

            for (String memberProfile : memberProfiles) {
                if (profileToUserIds.containsKey(memberProfile)) {
                    docRecTypeIds.add(docRecTypeId);
                    continue;
                }
            }

            for (String studyField : studyFields) {
                switch on studyField {
                    when 'VTD1_Virtual_Trial_head_of_Operations__c' {
                        if (profileToUserIds.containsKey(VT_R4_ConstantsHelper_ProfilesSTM.VTHO_PROFILE_NAME)) {
                            docRecTypeIds.add(docRecTypeId);
                            continue;
                        }
                    }
                    when 'VTD1_Project_Lead__c' {
                        if (profileToUserIds.containsKey(VT_R4_ConstantsHelper_ProfilesSTM.PL_PROFILE_NAME)) {
                            docRecTypeIds.add(docRecTypeId);
                            continue;
                        }
                    }
                    when 'VTD1_Virtual_Trial_Study_Lead__c' {
                        if (profileToUserIds.containsKey(VT_R4_ConstantsHelper_ProfilesSTM.VTSL_PROFILE_NAME)) {
                            docRecTypeIds.add(docRecTypeId);
                            continue;
                        }
                    }
                    when 'VTD1_Regulatory_Specialist__c' {
                        if (profileToUserIds.containsKey(VT_R4_ConstantsHelper_ProfilesSTM.RS_PROFILE_NAME)) {
                            docRecTypeIds.add(docRecTypeId);
                            continue;
                        }
                    }
                }
            }

            for (String caseField : caseFields) {
                switch on caseField {
                    when 'VTD1_Patient_User__c' {
                        if (profileToUserIds.containsKey(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) || profileToUserIds.containsKey(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME)) {
                            docRecTypeIds.add(docRecTypeId);
                            continue;
                        }
                    }
                    when 'VTD1_PI_user__c' {
                        if (profileToUserIds.containsKey(VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME)) {
                            docRecTypeIds.add(docRecTypeId);
                            continue;
                        }
                    }
                    when 'VTD1_Backup_PI_User__c' {
                        if (profileToUserIds.containsKey(VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME)) {
                            docRecTypeIds.add(docRecTypeId);
                            continue;
                        }
                    }
                    when 'VTD1_Primary_PG__c' {
                        if (profileToUserIds.containsKey(VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME)) {
                            docRecTypeIds.add(docRecTypeId);
                            continue;
                        }
                    }
                    when 'VTD1_Secondary_PG__c' {
                        if (profileToUserIds.containsKey(VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME)) {
                            docRecTypeIds.add(docRecTypeId);
                            continue;
                        }
                    }
                }
            }
        }
    }
}