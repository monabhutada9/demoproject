@IsTest
public with sharing class VT_R2_TreatmentArmSharingControllerTest {

    public static void testGetConfigs() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
//        User user = (User) new DomainObjects.User_t().persist();
//        new DomainObjects.StudyTeamMember_t()
//                .setStudyId(study.Id)
//                .setUserId(user.Id)
//                .persist();

        Test.startTest();
        List<VT_R2_TreatmentArmSharingController.ShareConfigItem> result = VT_R2_TreatmentArmSharingController.getConfigs(study.Id);
        Test.stopTest();

        System.assert(!result.isEmpty());
    }

    public static void testCheckNotSysAdminProfile() {
        Boolean result;
        User u = [SELECT Id FROM User WHERE Profile.Name != 'System Administrator' LIMIT 1];
        Test.startTest();
        System.runAs(u) {
            result = VT_R2_TreatmentArmSharingController.checkNotSysAdminProfile();
        }
        Test.stopTest();

        System.assertEquals(true, result);
    }

    public static void testGetArmAccessByStudy() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];

        Test.startTest();
        String result = VT_R2_TreatmentArmSharingController.getArmAccessByStudy(study.Id);
        Test.stopTest();

        System.assert(String.isNotEmpty(result));
    }

    public static void testGetArmAccessByCase() {
        Case caseRecord = [SELECT Id FROM Case LIMIT 1];

        Test.startTest();
        String result = VT_R2_TreatmentArmSharingController.getArmAccessByCase(caseRecord.Id);
        Test.stopTest();

        System.assert(String.isNotEmpty(result));
    }

    public static void testSetConfigFailed() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        User user = (User) new DomainObjects.User_t().persist();

        Test.startTest();
        VT_R2_TreatmentArmSharingController.setConfig(study.Id, user.Id, '');
        Test.stopTest();
    }

    public static void testSetConfigSuccess() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        User user = (User) new DomainObjects.User_t().persist();
        new DomainObjects.VTR2_Treatment_Arm_Sharing_Configuration_t()
                .setVTR2_Study(study.Id)
                .setVTR2_User(user.Id)
                .persist();

        Test.startTest();
        VT_R2_TreatmentArmSharingController.setConfig(study.Id, user.Id, '');
        Test.stopTest();
    }
}