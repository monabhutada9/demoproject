/**
 * Created by Yevhen Kharchuk on 14-Jan-20.
 */
public with sharing class VT_Apollo_FileUploadController {
    @AuraEnabled
    public static Id uploadFile(String fileName, String base64Data, String linkedEntityId) {
        System.debug(LoggingLevel.DEBUG, 'getHeapSize ' + Limits.getHeapSize());
        Id cvId = insertContentVersion(fileName, base64Data);
        System.debug(LoggingLevel.DEBUG, 'getHeapSize ' + Limits.getHeapSize());
        Id documentId = getDocIdFromCVId(cvId);
        if (linkedEntityId != UserInfo.getUserId()) {
            ContentDocumentLink cdl = new ContentDocumentLink(
                    ContentDocumentId = getDocIdFromCVId(cvId),
                    LinkedEntityId = linkedEntityId,
                    ShareType = 'I'
            );
            insert cdl;
        }
        return documentId;
    }
    @AuraEnabled
    public static String setProfilePicture(Id docId) {
        String originalPhotoURL = getUser().FullPhotoUrl;
        if (!Test.isRunningTest()) ConnectApi.UserProfiles.setPhoto(Network.getNetworkId(), UserInfo.getUserId(), docId, null);
        return originalPhotoURL;
    }
    @AuraEnabled
    public static User getUser() {
        return [SELECT FullPhotoUrl, Name FROM User WHERE Id =: UserInfo.getUserId()];
    }
    private static Id insertContentVersion(String fileName, String base64Data) {
        ContentVersion cv = new ContentVersion(
                ContentLocation = 'S',
                Origin = 'C',
                SharingOption = 'A',
                SharingPrivacy = 'N',
                PathOnClient = fileName,
                VersionData = EncodingUtil.base64Decode(base64Data)
        );
        insert cv;
        return cv.Id;
    }
    private static Id getDocIdFromCVId(Id cvId) {
        return [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cvId].ContentDocumentId;
    }
}