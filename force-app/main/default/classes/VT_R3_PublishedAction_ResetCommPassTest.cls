/**
* @author: Carl Judge
* @date: 22-Nov-19
**/

@IsTest
public with sharing class VT_R3_PublishedAction_ResetCommPassTest {
    
    public static void doTest() {
        User piUser = [SELECT Id FROM User WHERE Profile.Name = 'Primary Investigator' ORDER BY CreatedDate DESC LIMIT 1];
        Test.startTest();
        new VT_R3_PublishedAction_ResetCommunityPass(new List<Id>{piUser.Id}).publish();
        Test.stopTest();
    }
}