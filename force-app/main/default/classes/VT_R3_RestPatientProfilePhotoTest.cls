@isTest
public with sharing class VT_R3_RestPatientProfilePhotoTest {
    @IsTest(SeeAllData=true)
    public static void getPhotoTest() {
        String wrongImg = 'wrongImg';
        String image1x1px = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII';
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/Patient/Profile/Photo/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(wrongImg);
        RestContext.request = request;
        RestContext.response = response;

        String responseString = VT_R3_RestPatientProfilePhoto.setPhoto();
        Map<String, Object>responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
        String failure = (String) responseMap.get('serviceResponse');
        System.assert(failure.contains('FAILURE'));
        System.assertEquals(500, response.statusCode);

        RestContext.request.requestBody = Blob.valueof(image1x1px);
        responseString = VT_R3_RestPatientProfilePhoto.setPhoto();
        responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
        System.assertEquals('SUCCESS', responseMap.get('serviceResponse'));
        System.assertEquals(System.Label.VTR3_Success_Profile_Picture_Upload, responseMap.get('serviceMessage'));

        RestContext.request.httpMethod = 'GET';
        RestContext.request.requestBody = null;
        responseString = VT_R3_RestPatientProfilePhoto.getPhoto();
        responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
//        System.assert(!String.isEmpty((String) responseMap.get('serviceResponse')));

    }
}