@IsTest
private with sharing class VT_R5_AllTests6 {
    //Without Test setup
    @IsTest
    static void VT_R5_EDiaryModalControllerTest() {
        VT_R5_EDiaryModalControllerTest.testBehavior();
    }
    @IsTest
    static void VT_R5_FlagMissedDiariesBatchTest1() {
        VT_R5_FlagMissedDiariesBatchTest.testBatch();
    }
    @IsTest
    static void VT_R5_FlagMissedDiariesBatchTest2() {
        VT_R5_FlagMissedDiariesBatchTest.testRun();
    }
    @IsTest
    static void VT_R5_LoginThemeControllerTest1() {
        VT_R5_LoginThemeControllerTest.getPickListValuesForIMBTest();
    }
    @IsTest
    static void VT_R5_LoginThemeControllerTest2() {
        VT_R5_LoginThemeControllerTest.changeLangTest();
    }
    @IsTest
    static void VT_R5_MobileECoaSSOTest() {
        VT_R5_MobileECoaSSOTest.testGetECoaSSO_v1();
    }
    @IsTest
    static void VT_R5_MobileECoaSSOTest2() {
        VT_R5_MobileECoaSSOTest.testGetECoaSSOInvalid();
    }
    @IsTest
    static void VT_R5_MobileResourcesTest() {
        VT_R5_MobileResourcesTest.getGuidesTest();
    }
    @IsTest
    static void VT_R5_MobileTimezoneUpdaterTest() {
        VT_R5_MobileTimezoneUpdaterTest.test1();
    }
    @IsTest
    static void VT_R5_MobileTimezoneUpdaterTest2() {
        VT_R5_MobileTimezoneUpdaterTest.test2();
    }
    @IsTest
    static void VT_R5_MobileTimezoneUpdaterTest3() {
        VT_R5_MobileTimezoneUpdaterTest.test3();
    }
    @IsTest
    static void VT_R5_MobileTimezoneUpdaterTest4() {
        VT_R5_MobileTimezoneUpdaterTest.test4();
    }
    @IsTest
    static void VT_R5_MobileTimezoneUpdaterTest5() {
        VT_R5_MobileTimezoneUpdaterTest.test5();
    }
    @IsTest
    static void VT_R5_PIandSCRusActivEmailSentBatchTest() {
        VT_R5_PIandSCRusActivEmailSentBatchTest.testBatch();
    }
    @IsTest
    static void VT_R5_SignantJsonProcessingServiceTest() {
        VT_R5_SignantJsonProcessingServiceTest.processSignantRequest();
    }
    @IsTest
    static void VT_R5_StringConditionParserTest() {
        VT_R5_StringConditionParserTest.test();
    }
    @IsTest
    static void VTD2_TMA_Task_TriggerTest() {
        VTD2_TMA_Task_TriggerTest.doTest();
    }
    @IsTest
    static void VTR2_DependentPicklistTest() {
        VTR2_DependentPicklistTest.testBehavior();
    }
    @IsTest
    static void VTR2_SCRContentDocumentLinkProcessHTest() {
        VTR2_SCRContentDocumentLinkProcessHTest.checkIfNeedCreateContainersTest();
    }
    @IsTest
    static void VTR2_SCRMyPatientStudyDocumentsTableTest() {
        VTR2_SCRMyPatientStudyDocumentsTableTest.testGetPatientDocuments();
    }
    @IsTest
    static void VTR2_SCRMyPtntStdDocsTblController_Test() {
        VTR2_SCRMyPtntStdDocsTblController_Test.isDownloadAllowedSuccess();
    }
    @IsTest
    static void VTR3_MapNotificationsControllerTest() {
        VTR3_MapNotificationsControllerTest.mapNotificationsTest();
    }
    @IsTest
    static void VTR3_SCTaskAndTaskBatchTest() {
        VTR3_SCTaskAndTaskBatchTest.testBehavior();
    }
    @IsTest
    static void VT_R5_DueOrMissedDiariesTaskerBatchTest() {
        VT_R5_DueOrMissedDiariesTaskerBatchTest.testBatch();
    }
    @IsTest
    static void VT_R5_BrowserVersionControllerTest() {
        VT_R5_BrowserVersionControllerTest.testBehavior();
    }
    @IsTest
    static void VT_R4_VSiteFilesUploaderControllerTest() {
        VT_R4_VSiteFilesUploaderControllerTest.attachFilesToRecordTest();
    }
    @IsTest
    static void VT_R5_AccountDeactEmailControllerTest() {
        VT_R5_AccountDeactEmailControllerTest.renderStoredEmailTemplateTest();
    }
    @IsTest
    static void VT_R4_VSiteFilesDownloadBlockerTest1() {
        VT_R4_VSiteFilesDownloadBlockerTest.downloadFileSoql();
    }
    @IsTest
    static void VT_R4_VSiteFilesDownloadBlockerTest2() {
        VT_R4_VSiteFilesDownloadBlockerTest.fileDownloadingIsNotAllowedForStudy();
    }
    @IsTest
    static void VT_R4_VSiteFilesDownloadBlockerTest3() {
        VT_R4_VSiteFilesDownloadBlockerTest.fileDownloadingIsAllowedForStudy();
    }
    @IsTest
    static void VT_R4_VSiteFilesDownloadBlockerTest4() {
        VT_R4_VSiteFilesDownloadBlockerTest.fileDownloadingIsNotAllowedForStudyButAdmin();
    }
    @IsTest
    static void VT_R4_StorageLimitsNotificationTest() {
        VT_R4_StorageLimitsNotificationTest.testLimitStorageNotif();
    }
    @IsTest
    static void VT_R4_SCRNavigationPathHeaderCtrlTest1() {
        VT_R4_SCRNavigationPathHeaderCtrlTest.testGetCaseIdByDocumentId();
    }
    @IsTest
    static void VT_R4_SCRNavigationPathHeaderCtrlTest2() {
        VT_R4_SCRNavigationPathHeaderCtrlTest.testGetLabelTranslated();
    }
    @IsTest
    static void VT_R4_SCRNavigationPathHeaderCtrlTest3() {
        VT_R4_SCRNavigationPathHeaderCtrlTest.testGetCaseIdByTreatmentArmId();
    }
    @IsTest
    static void VT_R4_Scheduler_VCTrigger_HandlerTest() {
        VT_R4_Scheduler_VCTrigger_HandlerTest.testSchedulerVideoConferenceTrigger();
    }
    @IsTest
    static void VT_R4_Scheduler_Rem_Trig_HandlerTest() {
        VT_R4_Scheduler_Rem_Trig_HandlerTest.testRemindersTrigger();
    }
    @IsTest
    static void VT_R4_Scheduler_PN_TriggerHandlerTest() {
        VT_R4_Scheduler_PN_TriggerHandlerTest.testNotificationsTrigger();
    }
    @IsTest
    static void VT_R4_RegulatoryDocsCertifyHelperTest() {
        VT_R4_RegulatoryDocsCertifyHelperTest.certifyDocumentTest();
    }
}