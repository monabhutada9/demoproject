/**
* @author: Carl Judge
* @date: 22-Feb-19
* @description: Update visit members when PI/PG on case changes
**/

public without sharing class VT_R2_ChangeCaseParticipantMemberUpdater {

    public static void updateMembers(Map<Id, Map<Id, Id>> oldToNewIdsByCaseId) {
        List<Id> oldUserIds = new List<Id>();
        for (Map<Id, Id> oldToNewMap : oldToNewIdsByCaseId.values()) {
            oldUserIds.addAll(oldToNewMap.keySet());
        }

        List<Visit_Member__c> visitMembersToUpdate = new List<Visit_Member__c>();

        for (Visit_Member__c visitMember : [
            SELECT Id, VTD1_Participant_User__c, VTD1_Actual_Visit__r.VTD1_Case__c
            FROM Visit_Member__c
            WHERE VTD1_Participant_User__c IN :oldUserIds
            AND VTD1_Actual_Visit__r.VTD1_Case__c IN :oldToNewIdsByCaseId.keySet()
            AND VTD1_Actual_Visit__r.VTD1_Status__c NOT IN (
                :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED,
                :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED
            )
        ]) {
            Map<Id, Id> oldToNewIds = oldToNewIdsByCaseId.get(visitMember.VTD1_Actual_Visit__r.VTD1_Case__c);
            if (oldToNewIds.containsKey(visitMember.VTD1_Participant_User__c)) {
                Id oldParticipantId = visitMember.VTD1_Participant_User__c;
                visitMember.VTD1_Participant_User__c = oldToNewIds.get(visitMember.VTD1_Participant_User__c);
                if (oldParticipantId == null || visitMember.VTD1_Participant_User__c != null) {
                visitMembersToUpdate.add(visitMember);
            }
        }
        }

        if (! visitMembersToUpdate.isEmpty()) { update visitMembersToUpdate; }
    }
}