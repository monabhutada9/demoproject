/**
* @author: Carl Judge
* @date: 17-Apr-19
**/

public with sharing class VT_R2_EDiaryTimeCalculator extends VT_D2_AbstractTimeCalculator{
    protected override Map<String, String> getProfileToFieldsMap(){
        return new Map<String, String> {
            VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME => 'VTD2_Patient_s_Due_Time__c'
        };
    }

    protected override String getBaseTimeField(){
        return 'VTD1_Due_Date__c';
    }

    protected override Map<Id, Map<String, User>> getUserMap(){
        List<Id> caseIds = new List<Id>();
        Map<Id, Map<String, User>> userMap = new Map<Id, Map<String, User>>();

        for (VTD1_Survey__c item : (List<VTD1_Survey__c>)this.records) {
            caseIds.add(item.VTD1_CSM__c);
        }

        Map<Id, Case> caseMap = new Map<Id, Case>([
            SELECT VTD1_Patient_User__r.Id, VTD1_Patient_User__r.Profile.Name, VTD1_Patient_User__r.TimeZoneSidKey,
                VTD1_Patient_User__r.LanguageLocaleKey
            FROM Case
            WHERE Id IN :caseIds
        ]);

        for (VTD1_Survey__c item : (List<VTD1_Survey__c>)this.records) {
            if (caseMap.containsKey(item.VTD1_CSM__c)) {
                addToUserMap(userMap, item.Id, caseMap.get(item.VTD1_CSM__c).VTD1_Patient_User__r);
            }
        }

        return userMap;
    }
}