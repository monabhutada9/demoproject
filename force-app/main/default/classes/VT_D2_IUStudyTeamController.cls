public without sharing class VT_D2_IUStudyTeamController {
    public class PatientStudyTeam {
        public User primaryPIUser;
        public User primaryPGUser;
        public User backupPIUser;
        public User backupPGUser;
        /*public Contact primaryPIContact;
        public Contact primaryPGContact;
        public Contact backupPIContact;
        public Contact backupPGContact;*/
        public String primaryPIStatus;
        public String primaryPGStatus;
        public String backupPIStatus;
        public String backupPGStatus;
    }

    @AuraEnabled (Cacheable = true)
    public static String getPatientStudyTeam(String caseId) {
        try{
            caseId = VT_D2_CreatePCFFromCaseController.getCaseId(caseId);
            PatientStudyTeam patientStudyTeam = new PatientStudyTeam();

            List<String> userIdList = new List<String>();
           // List<String> contactIdList = new List<String>();

            Case c = [SELECT Id, VTD1_PI_user__c, VTD1_Primary_PG__c, VTD1_Backup_PI_User__c, VTD1_Secondary_PG__c
                             //, VTD1_PI_contact__c, VTD1_Primary_PG__r.ContactId, VTD1_Backup_PI_Contact__c, VTD1_Secondary_PG__r.ContactId
                      FROM Case WHERE Id =: caseId];

            userIdList.add(c.VTD1_PI_user__c);
            userIdList.add(c.VTD1_Primary_PG__c);
            userIdList.add(c.VTD1_Backup_PI_User__c);
            userIdList.add(c.VTD1_Secondary_PG__c);

            List<User> userList = [SELECT Id, Name, FullPhotoUrl, VTD1_Profile_Name__c, Phone FROM User WHERE Id IN: userIdList];
            for (User u : userList) {
                if(u.Id == c.VTD1_PI_user__c) {
                    patientStudyTeam.primaryPIUser = u;
                } else if(u.Id == c.VTD1_Primary_PG__c) {
                    patientStudyTeam.primaryPGUser = u;
                } else if(u.Id == c.VTD1_Backup_PI_User__c) {
                    patientStudyTeam.backupPIUser = u;
                } else if(u.Id == c.VTD1_Secondary_PG__c) {
                    patientStudyTeam.backupPGUser = u;
                }
            }

            /*contactIdList.add(c.VTD1_PI_contact__c);
            contactIdList.add(c.VTD1_Primary_PG__r.ContactId);
            contactIdList.add(c.VTD1_Backup_PI_Contact__c);
            contactIdList.add(c.VTD1_Secondary_PG__r.ContactId);

            List<Contact> contactList = [SELECT Id, Phone FROM Contact WHERE Id IN: contactIdList];
            for (Contact contact : contactList) {
                if(contact.Id == c.VTD1_PI_contact__c) {
                    patientStudyTeam.primaryPIContact = contact;
                } else if(contact.Id == c.VTD1_Primary_PG__r.ContactId) {
                    patientStudyTeam.primaryPGContact = contact;
                } else if(contact.Id == c.VTD1_Backup_PI_Contact__c) {
                    patientStudyTeam.backupPIContact = contact;
                } else if(contact.Id == c.VTD1_Secondary_PG__r.ContactId) {
                    patientStudyTeam.backupPGContact = contact;
                }
            }*/

            patientStudyTeam.primaryPIStatus = 'Acting';
            patientStudyTeam.primaryPGStatus = 'Acting';
            patientStudyTeam.backupPIStatus = 'Secondary';
            patientStudyTeam.backupPGStatus = 'Secondary';

            Datetime currentDateTime = Datetime.now();
            List<Event> eventOutOfOfficeList = [SELECT OwnerId FROM Event WHERE OwnerId IN: userIdList AND RecordTypeId =: VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_EVENT_OUTOFOFFICE AND StartDateTime <=: currentDateTime AND EndDateTime >=: currentDateTime];
            for (Event ev : eventOutOfOfficeList) {
                if(ev.OwnerId == c.VTD1_PI_user__c) {
                    patientStudyTeam.primaryPIStatus = 'Out of office';
                } else if(ev.OwnerId == c.VTD1_Primary_PG__c) {
                    patientStudyTeam.primaryPGStatus = 'Out of office';
                } else if(ev.OwnerId == c.VTD1_Backup_PI_User__c) {
                    patientStudyTeam.backupPIStatus = 'Out of office';
                } else if(ev.OwnerId == c.VTD1_Secondary_PG__c) {
                    patientStudyTeam.backupPGStatus = 'Out of office';
                }
            }
            if(patientStudyTeam.primaryPIStatus == 'Out of office'){
                patientStudyTeam.backupPIStatus = 'Acting';
            }

            if(patientStudyTeam.primaryPGStatus == 'Out of office'){
                patientStudyTeam.backupPGStatus = 'Acting';
            }
            return JSON.serialize(patientStudyTeam);
        }catch (Exception e){
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
}