@IsTest
public without sharing class VT_R3_RestPatientProfileTest {

    public static void firstTest() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_AllTestsMobilePatient@test.com' LIMIT 1];

        System.runAs(toRun) {
            HttpCalloutMock cMock = new ProfileCalloutMock();
            Test.setMock(HttpCalloutMock.class, cMock);
            VT_D1_PatientFullProfileController.patientLanguagePicklist = new Map<String, String>{
                    'English' => 'en_US', 'Russian' => 'ru'
            };
            Test.startTest();
            String resp = VT_R3_RestPatientProfile.getProfile();
            Test.stopTest();
        }
    }
    public static void secondTest() {
        HttpCalloutMock cMock = new ProfileCalloutMock();
        Test.setMock(HttpCalloutMock.class, cMock);
        User toRun = [SELECT Id, ContactId FROM User WHERE Username = 'VT_R5_AllTestsMobileCG@test.com' LIMIT 1];

        System.runAs(toRun) {
            VT_D1_PatientFullProfileController.patientLanguagePicklist = new Map<String, String>{
                    'English' => 'en_US', 'Russian' => 'ru'
            };
            VT_D1_PatientFullProfileController.caregiverLanguagePicklist = new Map<String, String>{
                    'English' => 'en_US', 'Russian' => 'ru'
            };
            Test.startTest();
            String resp = VT_R3_RestPatientProfile.getProfile();
            Test.stopTest();
            System.debug('final' + JSON.deserializeUntyped(resp));
        }
    }
    public static void thirdTest(){
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        RestContext.request = request;
        RestContext.response = response;

        HttpCalloutMock cMock = new ProfileCalloutMock();
        Test.setMock(HttpCalloutMock.class, cMock);

        request.requestURI = '/Patient/Profile';
        request.httpMethod = 'POST';
        request.params.put('confirmshipment', 'true');

        VT_R3_RestPatientProfile.Phone phone = new VT_R3_RestPatientProfile.Phone();
        phone.isPrimary = false;
        phone.phoneType = 'Home';
        phone.phoneNumber = String.valueOf(DomainObjects.RANDOM.getInteger(10000000, 99999999));

        VT_R3_RestPatientProfile.PatientFullProfile patientFullProfile = new VT_R3_RestPatientProfile.PatientFullProfile();
        VT_R3_RestPatientProfile.Physician physician = new VT_R3_RestPatientProfile.Physician();
        physician.firstName = 'FN';
        physician.lastName = 'LN';

        List<VT_R3_RestPatientProfile.Physician> physicianList = new List<VT_R3_RestPatientProfile.Physician>();
        physicianList.add(physician);
        VT_R3_RestPatientProfile.PhonesSection phonesSection = new VT_R3_RestPatientProfile.PhonesSection();
        phonesSection.phones = new List<VT_R3_RestPatientProfile.Phone>{
                phone
        };
        VT_R3_RestPatientProfile.PhysicianSection physicianSection = new VT_R3_RestPatientProfile.PhysicianSection();
        VT_R3_RestPatientProfile.ShippingSection shippingSection = new VT_R3_RestPatientProfile.ShippingSection();
        VT_R3_RestPatientProfile.DeliveryAvailabilitySection deliveryAvailabilitySection = new VT_R3_RestPatientProfile.DeliveryAvailabilitySection();
        VT_R3_RestPatientProfile.PersonalSection personalSection = new VT_R3_RestPatientProfile.PersonalSection();
        VT_R3_RestPatientProfile.EmergencyContactSection emergencyContactSection = new VT_R3_RestPatientProfile.EmergencyContactSection();


        patientFullProfile.phonesSection = phonesSection;
        patientFullProfile.physicianSection = physicianSection;
        patientFullProfile.deliveryAvailabilitySection = deliveryAvailabilitySection;
        patientFullProfile.personalSection = personalSection;
        patientFullProfile.emergencyContactSection = emergencyContactSection;
        patientFullProfile.shippingSection = shippingSection;
        patientFullProfile.physicianSection.physicians = physicianList;
        patientFullProfile.shippingSection.addressLineOne = 'New line';
        patientFullProfile.deliveryAvailabilitySection.mondayFrom = '10:00';
        patientFullProfile.deliveryAvailabilitySection.mondayTo = '11:00';
        patientFullProfile.personalSection.firstName = 'newName';
        patientFullProfile.personalSection.username = 'VT_R5_AllTestsMobilePatientFixed@test.com';
        patientFullProfile.personalSection.language = 'de';
        patientFullProfile.emergencyContactSection.emergencyContactFirstName = 'newNameForEC';
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(patientFullProfile, true));

        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_AllTestsMobilePatient@test.com' LIMIT 1];
        System.runAs(toRun) {
            Test.startTest();
            String responseString = VT_R3_RestPatientProfile.updateProfile();
            Test.stopTest();
            System.debug(JSON.deserializeUntyped(responseString));
        }
    }
    public static void fourthTest() {
        RestRequest request = new RestRequest();
        request.requestURI = '/Patient/Profile';
        request.httpMethod = 'GET';
        RestContext.request = request;
        request.params.put('confirmshipment', 'true');

        HttpCalloutMock cMock = new ProfileCalloutMock();
        Test.setMock(HttpCalloutMock.class, cMock);

        VT_R3_RestPatientProfile.CaregiverFullProfile caregiverFullProfile = new VT_R3_RestPatientProfile.CaregiverFullProfile();

        VT_R3_RestPatientProfile.PersonalSection personalSection = new VT_R3_RestPatientProfile.PersonalSection();
        personalSection.firstName = 'New CG NAME';
        personalSection.username = 'VT_R5_AllTestsMobileCGFixed@test.com';
        personalSection.language = 'de';
        caregiverFullProfile.personalSection = personalSection;

        VT_R3_RestPatientProfile.PersonalSection patientPersonalSection = new VT_R3_RestPatientProfile.PersonalSection();
        personalSection.firstName = 'New PT NAME';
        personalSection.username = 'VT_R5_AllTestsMobilePatientFixed2@test.com';
        personalSection.language = 'de';
        caregiverFullProfile.patientPersonalSection = patientPersonalSection;

        VT_R3_RestPatientProfile.EmergencyContactSection emergencyContactSection = new VT_R3_RestPatientProfile.EmergencyContactSection();
        emergencyContactSection.emergencyContactFirstName = 'EMCFN';
        caregiverFullProfile.emergencyContactSection = emergencyContactSection;

        VT_R3_RestPatientProfile.ShippingSection shippingSection = new VT_R3_RestPatientProfile.ShippingSection();
        shippingSection.addressLineOne = 'New line';
        caregiverFullProfile.shippingSection = shippingSection;

        VT_R3_RestPatientProfile.DeliveryAvailabilitySection deliveryAvailabilitySection = new VT_R3_RestPatientProfile.DeliveryAvailabilitySection();
        deliveryAvailabilitySection.mondayFrom = '16:00';
        caregiverFullProfile.deliveryAvailabilitySection = deliveryAvailabilitySection;

        VT_R3_RestPatientProfile.PhysicianSection physicianSection = new VT_R3_RestPatientProfile.PhysicianSection();
        List<VT_R3_RestPatientProfile.Physician> listPhysicians = new List<VT_R3_RestPatientProfile.Physician>();
        VT_R3_RestPatientProfile.Physician physician = new VT_R3_RestPatientProfile.Physician();
        physician.firstName = 'FN';
        physician.lastName = 'LN';
        listPhysicians.add(physician);
        physicianSection.physicians = listPhysicians;
        caregiverFullProfile.physicianSection = physicianSection;

        List<VT_R3_RestPatientProfile.Physician> physicianList = new List<VT_R3_RestPatientProfile.Physician>();
        physicianList.add(physician);
        caregiverFullProfile.physicianSection.physicians = physicianList;

        VT_R3_RestPatientProfile.Phone phone = new VT_R3_RestPatientProfile.Phone();
        phone.isPrimary = false;
        phone.phoneType = 'Home';
        phone.phoneNumber = String.valueOf(DomainObjects.RANDOM.getInteger(10000000, 99999999));

        VT_R3_RestPatientProfile.PhonesSection phonesSection = new VT_R3_RestPatientProfile.PhonesSection();
        phonesSection.phones = new List<VT_R3_RestPatientProfile.Phone>{
                phone
        };
        caregiverFullProfile.phonesSection = phonesSection;

        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(caregiverFullProfile, true));

        User toRun = [SELECT Id, ContactId FROM User WHERE Username = 'VT_R5_AllTestsMobileCG@test.com' LIMIT 1];

        System.runAs(toRun) {
            Test.startTest();
            VT_R3_RestPatientProfile.updateProfile();
            Test.stopTest();
        }
    }
    private class ProfileCalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');
            res.setBody('{"controllerValues":{},"defaultValue":{"attributes":null,"label":"English","validFor":[],"value":"en_US"},"eTag":"799a395a6b04d7d9edc8920a806f9755","url":"/services/data/v45.0/ui-api/object-info/Contact/picklist-values/0121N000000oCFsQAM/VTR2_Primary_Language__c","values":[{"attributes":null,"label":"English","validFor":[],"value":"en_US"},{"attributes":null,"label":"Spanish (Spain)","validFor":[],"value":"es"},{"attributes":null,"label":"Italian","validFor":[],"value":"it"},{"attributes":null,"label":"German","validFor":[],"value":"de"},{"attributes":null,"label":"French (France)","validFor":[],"value":"fr"},{"attributes":null,"label":"Dutch","validFor":[],"value":"nl_NL"},{"attributes":null,"label":"French (Canada)","validFor":[],"value":"fr_CA"},{"attributes":null,"label":"Spanish (US)","validFor":[],"value":"es_US"},{"attributes":null,"label":"Japanese","validFor":[],"value":"ja"}]}');
            return res;
        }
    }
}