@isTest
public class VT_R5_CreateSkillAssignmentTest{
    
    @testSetup static void setup() {

        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();
        insert VT_D1_TestUtils.createUserByProfile(VT_R4_ConstantsHelper_Profiles.SC_PROFILE_NAME, 'scUserForTest');
        
    }
    
    static testMethod void TestMethod1(){                    
        String query = 'Select id,Name,IsActive,VT_R5_Primary_Preferred_Language__c,VT_R5_Secondary_Preferred_Language__c,VT_R5_Tertiary_Preferred_Language__c from User where UserName like \'scUserForTest%\'';         
        VT_R5_BatchClassCreateSkillAssignment batch = new VT_R5_BatchClassCreateSkillAssignment(query);
        Test.startTest();
        Database.executeBatch(batch, 100);
        Test.stopTest();

}
}