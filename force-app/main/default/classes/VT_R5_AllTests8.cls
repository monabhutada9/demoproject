/**
 * Created by Alexander on 2020-10-12.
 */

@IsTest
private class VT_R5_AllTests8 {
    @TestSetup
    static void setup() {
        VT_R3_GlobalSharing.disableForTest = true;

        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setName('Study Name')
                /*.addPMA(new DomainObjects.User_t()
                        .addContact(new DomainObjects.Contact_t())
                        .setProfile('Site Coordinator'))*/
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));

        DomainObjects.StudyTeamMember_t stmOnsiteCra = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA')
                )
                .addStudy(study);
        stmOnsiteCra.persist();

        DomainObjects.StudyTeamMember_t stmRemoteCra = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('CRA')
                .addUser(new DomainObjects.User_t()
                        .setProfile('CRA'))
                .addStudy(study);
        stmRemoteCra.persist();



        DomainObjects.StudyTeamMember_t stm = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('SCR')
                .addUser(new DomainObjects.User_t()
                        .addContact(new DomainObjects.Contact_t())
                        .setProfile('Site Coordinator'))
            //START RAJESH SH-17440
                //.setOnsiteCraId(stmOnsiteCra.id)
                //.setRemoteCraId(stmRemoteCra.id)
            //END:sh-17440
                .addStudy(study);

        new DomainObjects.VTD1_Regulatory_Binder_t()
                .addStudy(study)
                .addRegDocument(
                new DomainObjects.VTD1_Regulatory_Document_t()
                        .setLevel('Study')
                        .setWayToSendToDocuSign('Manual')
                        .setDocumentType('Site Activation Approval Form')
                        .setSiteRSU(true)
        );

        Test.startTest();
        new DomainObjects.VTD1_Regulatory_Binder_t()
                .addStudy(study)
                .addRegDocument(
                new DomainObjects.VTD1_Regulatory_Document_t()
                        .setLevel('Site')
                        .setWayToSendToDocuSign('Manual')
                        .setDocumentType('Site Activation Approval Form')
                        .setSiteRSU(true)
        )
                .persist();


        DomainObjects.VirtualSite_t site = new DomainObjects.VirtualSite_t()
                .addStudyTeamMember(stm)
                .setStudySiteNumber('12345')
                .addStudy(study);
        site.persist();



//        System.runAs(studyTeamMember.User__r) {
//            VTD1_Monitoring_Visit__c mv = new VTD1_Monitoring_Visit__c(
//                    VTD1_Study__c = study.Id,
//                    VTD1_Virtual_Site__c = site.Id,
//                    VTR2_Remote_Onsite__c = 'Remote');
//            insert mv;
//        }

        System.debug('test11 ' + Limits.getQueries());
        Test.stopTest();

        DomainObjects.VirtualSite_t virtualSiteWithoutAddresses = new DomainObjects.VirtualSite_t()
                .addStudyTeamMember(stm)
                .setStudySiteNumber('12345')
                .addStudy(study)
                .setName('VS without addresses');
//        virtualSiteWithoutAddresses.persist();


        DomainObjects.VirtualSite_t virtualSiteWithAddresses = new DomainObjects.VirtualSite_t()
                .addStudyTeamMember(stm)
                .setStudySiteNumber('12345')
                .addStudy(study)
                .setName('VS with addresses');
//        virtualSiteWithAddresses.persist();




        new DomainObjects.VTR2_Site_Address_t()
                .addVirtualSite(virtualSiteWithAddresses)
                .setPrimary(true)
                .persist();
        new DomainObjects.VTR2_Site_Address_t()
                .addVirtualSite(virtualSiteWithAddresses)
                .setPrimary(false)
                .persist();
        System.debug('test11 ' + Limits.getQueries());

    }

//    @IsTest
//    static void VT_D1_VirtualSiteHandlerTest1() {
//        VT_D1_VirtualSiteHandlerTest.onBeforeDeleteTest();
//    }
//
//    @IsTest
//    static void VT_D1_VirtualSiteHandlerTest2() {
//        VT_D1_VirtualSiteHandlerTest.onBeforeInsertTest();
//    }
//
//    @IsTest
//    static void VT_D1_VirtualSiteHandlerTest3() {
//        VT_D1_VirtualSiteHandlerTest.onBeforeUpdateTest();
//    }
//
//    @IsTest
//    static void VT_D1_VirtualSiteHandlerTest4() {
//        VT_D1_VirtualSiteHandlerTest.onAfterInsertTest();
//    }

    @IsTest
    static void VT_D1_DocumentProcessHelperTest1() {
        VT_D1_DocumentProcessHelperTest.createMissingPlaceholdersBySiteLevelTest();
    }

    @IsTest
    static void VT_D1_DocumentProcessHelperTest2() {
        VT_D1_DocumentProcessHelperTest.createPlaceholdersBySiteLevelTest();
    }

    @IsTest
    static void VT_D1_DocumentProcessHelperTest3() {
        VT_D1_DocumentProcessHelperTest.createPlaceholdersByStudyLevelTest();
    }
//START RAJESH SH-17440/ this Method does not exsit and not able to remove the CRA method from this class so need to comment the same.
/*    @IsTest
    static void VT_R5_ChangeCRAOnMVBatch_Test1() {
        VT_R5_ChangeCRAOnMVBatch_Test.testOnsiteMV();
    }
*/
//END:SH_17440
    @IsTest
    static void VT_R5_ChangeCRAOnMVBatch_Test2() {
        VT_R5_ChangeCRAOnMVBatch_Test.testRemoteMV();
    }

    @IsTest
    static void VT_R2_SCR_SiteAddressHandlerTest1() {
        VT_R2_SCR_SiteAddressHandlerTest.deleteAddressFail();
    }
    @IsTest
    static void VT_R2_SCR_SiteAddressHandlerTest2() {
        VT_R2_SCR_SiteAddressHandlerTest.deleteAddressSuccess();
    }

    @IsTest
    static void VT_R2_SCR_SiteAddressHandlerTest3() {
        VT_R2_SCR_SiteAddressHandlerTest.insertAddressSuccess();
    }

    @IsTest
    static void VT_R2_SCR_SiteAddressHandlerTest4() {
        VT_R2_SCR_SiteAddressHandlerTest.insertFirstAddressFail();
    }

    @IsTest
    static void VT_R2_SCR_SiteAddressHandlerTest5() {
        VT_R2_SCR_SiteAddressHandlerTest.insertSecondAddressFail();
    }

    @IsTest
    static void VT_R2_SCR_SiteAddressHandlerTest6() {
        VT_R2_SCR_SiteAddressHandlerTest.updateAddressFail();
    }

    @IsTest
    static void VT_R2_SCR_SiteAddressHandlerTest7() {
        VT_R2_SCR_SiteAddressHandlerTest.updateAddressSuccess();
    }

    @IsTest
    static void VT_R2_SCR_SiteAddressHandlerTest8() {
        VT_R2_SCR_SiteAddressHandlerTest.updateSecondAddressSuccess();
    }



}