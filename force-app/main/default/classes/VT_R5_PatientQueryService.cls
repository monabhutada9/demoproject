/**
 * Created by Yuliya Yakushenkova on 9/15/2020.
 */

public class VT_R5_PatientQueryService {

    public static final String BRACKET_L = '(';
    public static final String BRACKET_R = ')';
    public static final String AND_STR = ' AND ';
    public static final String OR_STR = ' OR ';

    private static List<String> userFields = new List<String>{
            'ContactId',
            'TimeZoneSidKey',
            'Username',
            'VTD1_Form_Updated__c',
            'LanguageLocaleKey',
            'Profile.Name',
            'Contact.VTD1_Clinical_Study_Membership__c',
            'toLabel(TimeZoneSidKey)TimeZoneLabel '
    };

    private static List<String> caseFields = new List<String>{
            'Id',
            'Status',
            'Contact.VTD1_FullHomePage__c',
            'Contact.VTD1_UserId__c',
            'VTD1_Patient__r.VTD1_First_Name__c',
            'VTD1_Patient__r.VTD1_Last_Name__c',
            'VTD1_Study__c',
            'VTD1_Study__r.Name',
            'VTD1_Study__r.VTD1_Study_Logo__c',
            'VTD1_Study__r.VTD1_Protocol_Nickname__c',
            'VTD1_Study__r.VTD1_Study_Library__c',
            'VTD1_Study__r.VTR3_LTFU__c',
            'VTD1_Patient_User__c',
            'VTD1_PI_user__c',
            'VTD1_PI_user__r.MediumPhotoUrl',
            'VTD1_PI_contact__r.Name',
            'VTD1_Primary_PG__r.Name',
            'VTD1_Primary_PG__r.MediumPhotoUrl',
            'VTD1_Secondary_PG__c',
            'VTD1_Backup_PI_User__c',
            'VTR2_SiteCoordinator__r.MediumPhotoUrl',
            'VTR2_SiteCoordinator__r.Name',
            'VTR2_StudyPhoneNumber__r.Name',
            'VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c'
    };

    public static User getUserById(Id userId) {
        String filter = VT_R5_DatabaseQueryService.equal('Id', userId);
        List<User> users = VT_R5_DatabaseQueryService.query(userFields, User.class, filter);

        if (!users.isEmpty()) return users[0];
        return null;
    }

    public static Id getPatientUserIdByCaregiverUserId(Id userId) {
        List<User> caregivers = [SELECT Contact.AccountId FROM User WHERE Id = :userId];

        if (!caregivers.isEmpty() && caregivers[0].Contact.AccountId != null) {
            List<User> patients = [
                    SELECT Id
                    FROM User
                    WHERE Contact.AccountId = :caregivers[0].Contact.AccountId
                    AND Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
            ];
            if (!patients.isEmpty()) return patients[0].Id;
        }
        return null;
    }

    public static User getUserByCurrentUser() {
        return getUserById(UserInfo.getUserId());
    }

    public static Case getCaseByCurrentUser() {
        return getCaseByUserId(UserInfo.getUserId());
    }

    public static Case getCaseByUserId(Id userId) {
        User us = getUserById(userId);

        String condition;
        if (String.isNotBlank(us.Contact.VTD1_Clinical_Study_Membership__c)) {
            condition = VT_R5_DatabaseQueryService.equal('Id', us.Contact.VTD1_Clinical_Study_Membership__c);
        } else if (String.isNotBlank(us.ContactId)) {
            condition = VT_R5_DatabaseQueryService.equal('ContactId', us.ContactId);
        }
        if (condition == null) return null;

        String filter = VT_R5_DatabaseQueryService.notNullField('VTD1_Study__c') +
                AND_STR + VT_R5_DatabaseQueryService.equal(
                'RecordType.DeveloperName', VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN
        ) + AND_STR + condition;
        List<Case> cases = VT_R5_DatabaseQueryService.query(caseFields, Case.class, filter);
        if (cases.isEmpty()) return null;
        return cases[0];
    }

    public static Case getCaseByCaseId(Id caseId) {
        String condition = VT_R5_DatabaseQueryService.equal('Id', caseId);
        List<Case> cases = VT_R5_DatabaseQueryService.query(caseFields, Case.class, condition);
        if (cases.isEmpty()) return null;
        return cases[0];
    }

//    public static Case getCaseByContactId(Id contactId) {
//
//    }


    // temporarily for Visits //

    public static List<VTD1_Actual_Visit__c> getVisitsAndMembersWithinRangeAndByCaseId(Id caseId, Datetime startDatetime, Datetime endDatetime) {
        List<String> profiles = new List<String>{
                VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME,
                VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME
        };

        List<String> visitFields = new List<String>{
                'Id',
                'Name',
                'VTD1_Case__c',
                'VTD1_Formula_Name__c',
                'VTD1_Scheduled_Date_Time__c',
                'VTD1_Scheduled_Visit_End_Date_Time__c',
                'VTD1_Protocol_Visit__c',
                'VTD1_Visit_Description__c',
                'VTD1_Pre_Visit_Instructions__c',
                'VTD1_Visit_Label__c', 'VTD1_Status__c',
                'VTD1_Reason_for_Request__c',
                'Unscheduled_Visits__c',
                'VTD1_Unscheduled_Visit_Type__c',
                'VTD1_Onboarding_Type__c',
                'VTD1_Additional_Patient_Visit_Checklist__c',
                'VTR2_Modality__c',
                'VTR2_Loc_Name__c',
                'VTR2_Loc_Address__c',
                'VTR2_Loc_Phone__c',
                'VTD1_Case__r.ContactId',
                'VTD1_Case__r.VTD1_Study__r.VTR3_LTFU__c',

                'VTD1_Protocol_Visit__r.VTD1_EDC_Name__c',
                'VTD1_Protocol_Visit__r.VTD1_Visit_Description__c',
                'VTD1_Protocol_Visit__r.VTD1_PreVisitInstructions__c',
                'VTD1_Protocol_Visit__r.VTD1_Patient_Visit_Checklist__c',
                'VTD1_Protocol_Visit__r.VTD1_Visit_Checklist__c',

                'toLabel(VTR2_Modality__c) modalityLabel',
                'toLabel(VTD1_Protocol_Visit__r.VTD1_Onboarding_Type__c)',
                'toLabel(VTD1_Protocol_Visit__r.VTD1_VisitType__c)',
                'toLabel(VTD1_Unscheduled_Visit_Type__c) unscheduledVisitTypeTranslation'
        };
        String firstCondition = BRACKET_L +
                VT_R5_DatabaseQueryService.equal('VTD1_Case__c', caseId)
                + OR_STR +
                VT_R5_DatabaseQueryService.equal('Unscheduled_Visits__c', caseId) + BRACKET_R;
        String secondCondition = BRACKET_L +
                VT_R5_DatabaseQueryService.falseField('VTD1_Protocol_Visit__r.VTD1_isArchivedVersion__c')
                + OR_STR +
                VT_R5_DatabaseQueryService.equal('VTD1_Status__c', VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED) +
                BRACKET_R;

        String thirdCondition =
                (startDatetime != null && endDatetime != null)
                        ? AND_STR + getConditionByDateRange(startDatetime, endDatetime) : '';

        String filter = firstCondition
                + AND_STR +
                VT_R5_DatabaseQueryService.notNullField('VTD1_Scheduled_Date_Time__c ')
                + AND_STR +
                VT_R5_DatabaseQueryService.notEqual('VTD1_Status__c', VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED)
                + AND_STR + secondCondition + thirdCondition;

        String members = BRACKET_L + 'SELECT Id, Visit_Member_Name__c ' +
                'FROM Visit_Members__r ' +
                'WHERE ' + VT_R5_DatabaseQueryService.fieldInValues('VTD1_Participant_User__r.Profile.Name', profiles) +
                BRACKET_R;
        visitFields.add(members);
        return VT_R5_DatabaseQueryService.query(visitFields, VTD1_Actual_Visit__c.class, filter);
    }

    public static List<VTD1_Actual_Visit__c> getVisitsAndMembersByCaseId(Id caseId) {
        return getVisitsAndMembersWithinRangeAndByCaseId(caseId, null, null);
    }

    private static String getConditionByDateRange(Datetime startDatetime, Datetime endDatetime) {
        String dateFormat = 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'';
        String startDateTimeValue = startDatetime.format(dateFormat);
        String endDateTimeValue = endDatetime.format(dateFormat);
        return BRACKET_L +
                VT_R5_DatabaseQueryService.greaterThan('VTD1_Scheduled_Date_Time__c', startDateTimeValue)
                + AND_STR +
                VT_R5_DatabaseQueryService.lessThan('VTD1_Scheduled_Date_Time__c', endDateTimeValue) + BRACKET_R;
    }

    public static VTD1_Actual_Visit__c getVisitAndMembersByVisitId(Id visitId) {
        List<String> profiles = new List<String>{
                VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME,
                VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME
        };

        List<String> visitFields = new List<String>{
                'Id',
                'Name',
                'VTD1_Case__c',
                'VTD1_Formula_Name__c',
                'VTD1_Scheduled_Date_Time__c',
                'VTD1_Scheduled_Visit_End_Date_Time__c',
                'VTD1_Protocol_Visit__c',
                'VTD1_Visit_Description__c',
                'VTD1_Pre_Visit_Instructions__c',
                'VTD1_Visit_Label__c', 'VTD1_Status__c',
                'VTD1_Reason_for_Request__c',
                'Unscheduled_Visits__c',
                'VTD1_Unscheduled_Visit_Type__c',
                'VTD1_Onboarding_Type__c',
                'VTD1_Additional_Patient_Visit_Checklist__c',
                'VTR2_Modality__c',
                'VTR2_Loc_Name__c',
                'VTR2_Loc_Address__c',
                'VTR2_Loc_Phone__c',
                'VTD1_Case__r.ContactId',
                'VTD1_Case__r.VTD1_Study__r.VTR3_LTFU__c',

                'VTD1_Protocol_Visit__r.VTD1_EDC_Name__c',
                'VTD1_Protocol_Visit__r.VTD1_Visit_Description__c',
                'VTD1_Protocol_Visit__r.VTD1_PreVisitInstructions__c',
                'VTD1_Protocol_Visit__r.VTD1_Patient_Visit_Checklist__c',
                'VTD1_Protocol_Visit__r.VTD1_Visit_Checklist__c',

                'toLabel(VTR2_Modality__c) modalityLabel',
                'toLabel(VTD1_Protocol_Visit__r.VTD1_Onboarding_Type__c)',
                'toLabel(VTD1_Protocol_Visit__r.VTD1_VisitType__c)',
                'toLabel(VTD1_Unscheduled_Visit_Type__c) unscheduledVisitTypeTranslation'
        };
        String filter =
                VT_R5_DatabaseQueryService.equal('Id', visitId);

        String members = BRACKET_L + 'SELECT Id, Visit_Member_Name__c ' +
                'FROM Visit_Members__r ' +
                'WHERE ' + VT_R5_DatabaseQueryService.fieldInValues('VTD1_Participant_User__r.Profile.Name', profiles) +
                BRACKET_R;
        visitFields.add(members);
        List<VTD1_Actual_Visit__c> actualVisits =
                VT_R5_DatabaseQueryService.query(visitFields, VTD1_Actual_Visit__c.class, filter);
        if (!actualVisits.isEmpty()) return actualVisits[0];
        return null;
    }

    public static List<VTD1_Document__c> getDocumentsByVisitId(Id visitId) {
        List<String> documentFields = new List<String>{
                'Id',
                'Name',
                'VTD1_FileNames__c',
                'VTD1_Nickname__c',
                'VTD1_Comment__c',
                'VTD1_Version__c',
                'VTD1_Status__c',
                'toLabel(VTD1_Status__c) translatedStatus',
                'toLabel(VTR3_Category__c) translatedCategory',
                'CreatedBy.FirstName',
                'CreatedBy.LastName',
                'CreatedDate',
                'VTR3_Associated_Visit__c',
                'VTR3_Associated_Visit__r.Name',
                'VTR3_Category__c',
                '(SELECT Id, ContentDocumentId FROM ContentDocumentLinks ORDER BY SystemModstamp DESC LIMIT 1)'
        };
        String filter =
                VT_R5_DatabaseQueryService.equal('VTR3_Associated_Visit__c', visitId);

        return VT_R5_DatabaseQueryService.query(documentFields, VTD1_Document__c.class, filter);
    }

//    temporarily for tasks
    public static List<Task> getTasksByCase(Case patientCase) {
        return getTasksByCaseAndStatusAndCategory(patientCase, null, null);
    }

    public static List<Task> getTasksByCaseAndStatusAndCategory(Case patientCase, String status, String category) {
        List<String> taskFields = new List<String>{
                'Id',
                'OwnerId',
                'Subject',
                'ActivityDate',
                'WhatId',
                'Type',
                'Description',
                'Status',
                'Priority',
                'Category__c',
                'What.Name',
                'CreatedDate',
                'HealthCloudGA__CarePlanTemplate__c',
                'VTD1_Case_lookup__c',
                'VTD1_Type_for_backend_logic__c',
                'VTD1_Caregiver_Clickable__c',
                'VTD2_Study_Name__c',
                'VTD2_My_Task_List_Redirect__c',
                'VTD2_isRequestVisitRedirect__c',
                'VTD2_Description_Parameters__c',
                'VTD2_Subject_Parameters__c',
                'VTR5_EndDate__c',
                'VTR5_AutocompleteWhenClicked__c',
                'toLabel(Category__c) CategoryLabel'
        };

        String condition = BRACKET_L + VT_R5_DatabaseQueryService.equal('OwnerId', UserInfo.getUserId());
        if (String.isNotBlank(patientCase.Contact.VTD1_UserId__c)) {
            condition += OR_STR + VT_R5_DatabaseQueryService.equal('OwnerId', patientCase.Contact.VTD1_UserId__c);
        }
        condition += BRACKET_R;
        if (String.isNotBlank(status)) {
            condition += AND_STR + VT_R5_DatabaseQueryService.equal('Status', status);
        }
        if (String.isNotBlank(category)) {
            condition += AND_STR + VT_R5_DatabaseQueryService.equal('Category__c', category);
        }
        condition += AND_STR + VT_R5_DatabaseQueryService.equal('VTD1_Case_lookup__c', patientCase.Id);
        condition += AND_STR + BRACKET_L + VT_R5_DatabaseQueryService.nullField('VTR5_StartDate__c') +
                OR_STR + VT_R5_DatabaseQueryService.lessOrEqThan('VTR5_StartDate__c', Datetime.now()) + BRACKET_R;

        List<Task> tasks = VT_R5_DatabaseQueryService.query(
                taskFields, Task.class, condition,
                'ActivityDate ASC, VTR5_EndDate__c ASC NULLS FIRST',
                100
        );
        return tasks;
    }
}