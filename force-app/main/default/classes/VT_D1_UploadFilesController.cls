/**
* @author: Carl Judge
* @date: 28-Aug-18
* @description: Controller for VT_D1_UploadFiles.page
**/

global with sharing class VT_D1_UploadFilesController {

    @RemoteAction
    global static String getSessionId() {
        return VT_D1_HelperClass.getSessionIdForGuest();
    }
}