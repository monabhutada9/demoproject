/**
 * Created by Alexander Komarov on 09.10.2019.
 */

@IsTest
private class VT_R3_InvocablePushTest {
    @TestSetup
    static void setupMethod() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        VTD1_ProtocolVisit__c pVisit = new VTD1_ProtocolVisit__c(
                VTD1_Visit_Checklist__c = 'TEST checklist',
                VTD1_EDC_Name__c = 'PVName',
                VTD1_Study__c = study.Id,
                VTR2_Visit_Participants__c = VTD1_ProtocolVisit__c.VTR2_Visit_Participants__c.getDescribe().getPicklistValues()[0].getValue(),
                VTD1_VisitDuration__c = '30 minutes'
        );
        insert pVisit;
        Test.stopTest();

        Case carePlan = [SELECT Id,VTD1_PI_user__c,VTD1_Patient_User__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];

        VTD1_Actual_Visit__c visit = new VTD1_Actual_Visit__c(
                VTD1_Case__c = carePlan.Id,
                VTD1_Protocol_Visit__c = pVisit.Id,
                VTD1_Reason_for_Request__c = 'TestVideoPanel',
                VTD1_Additional_Visit_Checklist__c = 'TEST',
//                VTD1_Scheduled_Date_Time__c = Datetime.now() + 1,
//				Unscheduled_Visits__c = carePlan.Id,
                Name = 'SomeTestVisitName',
                VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today() + 1,
                VTR2_Televisit__c = true
        );
        insert visit;


        User user = [SELECT Id FROM User WHERE Profile.Name = 'Patient' AND IsActive = true AND Name = 'CP1 CP1L' LIMIT 1];
        insert new Visit_Member__c(VTD1_Actual_Visit__c = visit.Id, VTD1_Participant_User__c = carePlan.VTD1_PI_user__c);
        insert new Visit_Member__c(VTD1_Actual_Visit__c = visit.Id, VTD1_Participant_User__c = user.Id);

        visit.VTD1_Scheduled_Date_Time__c = Datetime.now().addHours(1);
        visit.VTD1_Status__c = 'Scheduled';
        update visit;
    }


    @IsTest
    static void testBehavior() {
        Test.startTest();
        User patientUser = [SELECT Id,VTR5_eCOA_Guid__c,VTR5_Create_Subject_in_eCOA__c,VTR3_Verified_Mobile_Devices__c FROM User WHERE Profile.Name = 'Patient' AND IsActive = true AND Name = 'CP1 CP1L' LIMIT 1];
        patientUser.VTR3_Verified_Mobile_Devices__c = 'somedeviceid;';
        patientUser.VTR5_Create_Subject_in_eCOA__c = false;
        patientUser.VTR5_eCOA_Guid__c = 'something';
        update patientUser;

        List<Id> ids = new List<Id>();
        for (Video_Conference__c v : [SELECT Id FROM Video_Conference__c]) {
            ids.add(v.Id);
        }

        VT_R3_InvocablePush.createPushNotifications(ids);
        Test.stopTest();
    }
    @IsTest
    static void testBehavior2() {
        Test.startTest();
        User patientUser = [SELECT Id,VTR5_eCOA_Guid__c,VTR3_Verified_Mobile_Devices__c FROM User WHERE Profile.Name = 'Patient' AND IsActive = true AND Name = 'CP1 CP1L' LIMIT 1];
        patientUser.VTR3_Verified_Mobile_Devices__c = 'somedeviceid;';
        patientUser.VTR5_eCOA_Guid__c = 'something';
        update patientUser;

        List<Id> ids = new List<Id>();
        for (Video_Conference__c v : [SELECT Id FROM Video_Conference__c]) {
            ids.add(v.Id);
        }

        VT_R4_BatchVideoConferencePush batch = new VT_R4_BatchVideoConferencePush(ids);

        Database.executeBatch(batch, 5);
        Test.stopTest();
    }
}