/**
 * Created by User on 19/04/03.
 */

public with sharing class VTR2_SCRLookupController {
    @AuraEnabled
    public static List <sObject> fetchAccount(String objName, String searchKeyWord) {
        String searchKey = '\'' + searchKeyWord + '%\'';
        String query = 'SELECT Id, Name ' +
                ' FROM HealthCloudGA__CarePlanTemplate__c' +
                ' WHERE Name LIKE ' + searchKey +
                ' LIMIT 100';
        System.debug('query = ' + query);
        List<sObject> sobjList = Database.query(query);
        return sobjList;
    }

    @AuraEnabled
    public static List <sObject> getNameById(String objName, Id objId, String lookupField) {
        String childObjName = objId.getSObjectType().getDescribe().getName();
        String searchKey = '\'' + objId + '\'';
        String query = 'SELECT Id, Name ' +
                ' FROM HealthCloudGA__CarePlanTemplate__c' +
                ' WHERE Id = ' + searchKey ;
        List<sObject> sobjList = Database.query(query);
        return sobjList;
    }
}