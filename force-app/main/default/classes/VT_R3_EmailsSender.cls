/**
 * @author: Dmitry Yartsev
 * @date: 25.10.2019
 * @version 1.0
 * @description: Class for sending emails in future to ensure VT_D1_TranslateHelper work in VF template controllers and translate emails subjects
 * @see VT_D1_TranslateHelper test
 */
global class VT_R3_EmailsSender {
    //To maintain organization wide email-address's Id
    private static Id owEmailAddresses;
    //To maintain developer names of email template configured in custom Metadata named Email Template with Static Subject.
    private static final Set<String> emailTemplatesDevName = new Set<String>();
    //To maintain developer names of developer name and its email template's record.
    private static Map<String, EmailTemplate> templateDevNamesToTemplateMapGlobal = new Map<String, EmailTemplate>();
    //To maintain Map of SObject's Id and its queried SObject's record.
    private static Map<Id, SObject> sObjMap = new Map<Id, SObject>();

    public static Boolean syncProcess = false;
    public class ParamsHolder {
        @InvocableVariable(Label = 'Recipient Id' Required = true)
        public String recipientId;
        @InvocableVariable(Label = 'Related To Record' Required = true)
        public String relatedToId;
        @InvocableVariable(Label = 'Template Unique Name' Required = true)
        public String templateDevName;
        @InvocableVariable(Label = 'To Addresses' Required = false Description = 'Optionally')
        public String toAddresses;
        @InvocableVariable(Label = 'To Addresses Only' Required = false Description = 'Optionally. Send email only to addresses')
        public Boolean toAddressesOnly;
        @InvocableVariable(Label = 'Receiver' Required = false Description = 'For removed visit members')
        public String receiver;
        @InvocableVariable(Label = 'Subject is static?' Required = false Description = 'True if subject is static text, false if subject require translation')
        public Boolean subjectIsStatic;
        @InvocableVariable(Label = 'Is translation needed?' Required = false Description = 'True if email requires translation')
        public Boolean isTranslationNeeded = true;
        public ParamsHolder(String recipientId, String relatedToId, String templateDevName) {
            this.recipientId = recipientId;
            this.relatedToId = relatedToId;
            this.templateDevName = templateDevName;
            this.toAddresses = null;
            this.toAddressesOnly = false;
            this.subjectIsStatic = false;
        }
        public ParamsHolder(String recipientId, String relatedToId, String templateDevName, String toAddresses) {
            this.recipientId = recipientId;
            this.relatedToId = relatedToId;
            this.templateDevName = templateDevName;
            this.toAddresses = toAddresses;
            this.toAddressesOnly = false;
            this.subjectIsStatic = false;
        }
        public ParamsHolder(String recipientId, String relatedToId, String templateDevName, String toAddresses, Boolean toAddressesOnly) {
            this.recipientId = recipientId;
            this.relatedToId = relatedToId;
            this.templateDevName = templateDevName;
            this.toAddresses = toAddresses;
            this.toAddressesOnly = toAddressesOnly;
            this.subjectIsStatic = false;
        }
        public ParamsHolder() {
            this.subjectIsStatic = false;
        }
    }
    public class SubjectWrapper {
        public String labelName;
        public String sobjectType;
        public Id sobjectId;
        public List<String> fields;
        public String language;
        public Messaging.SingleEmailMessage mail;
        public String extLang;
        public String translatedLabel; //get label value from template to avoid system.calloutexception
    }
    /*
    **@author:      IQVIA Strikers
    **@date:        12/01/2020
    **@description: The Wrapper class is used to check if the running user has access of recipients(in this case PT and CG) 
                    to which the emails should be sent. "Without Sharing" keyword is used in order to query relevant records from the database.
    */
    public without sharing Class VT_R5_calculateSharingOfRecipientsWrp {

        public VT_R5_calculateSharingOfRecipientsWrp() {
        }

        //If running  user's profile belongs to these profiles then Usershare records will not be reuired 
        //since existing sharing design provides access to these users.
        public Set<String> profileNameSet = new Set<String>{
                VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME,
                VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME,
                VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME,
                VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME,
                VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME,
                VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME,
                VT_R4_ConstantsHelper_ProfilesSTM.CM_PROFILE_NAME,
                VT_R4_ConstantsHelper_ProfilesSTM.RE_PROFILE_NAME,
                VT_R4_ConstantsHelper_ProfilesSTM.AUDITOR_PROFILE_NAME,
                VT_R4_ConstantsHelper_ProfilesSTM.ADMIN_PROFILE_NAME
        };

        /*
        **@author:      IQVIA Strikers
        **@date:        12/01/2020
        **@description: This method queries the UserRecordAccess in order to check running user's access for recipients(in this case PT and CG).
        */
        public Set<Id> checkWithUserRecordAccess(Set<Id> recipientIdSet) {
            Set<Id> recipeintWithoutAccess = new set<Id>();
            Map<Id, UserRecordAccess> userRecordAccessMap = new Map<Id, UserRecordAccess>();
            for (UserRecordAccess ura : [
                    SELECT RecordId,HasReadAccess
                    FROM UserRecordAccess
                    where UserId = :UserInfo.getUserId() and RecordId in:recipientIdSet
            ]) {
                if (ura.HasReadAccess) {
                    userRecordAccessMap.put(ura.RecordId, ura);
                }
            }
            for (id uId : recipientIdSet) {
                if (!userRecordAccessMap.containskey(uId)) {
                    recipeintWithoutAccess.add(uId);
                }
            }
            return recipeintWithoutAccess;
        }

        /*
        **@author:      IQVIA Strikers
        **@date:        12/01/2020
        **@description: This method queries the GroupMember in order to check running user's access for recipients(in this case PT and CG).
        */
        public Set<Id> checkWithGroupSharing(Set<Id> recipientIdSet) {
            Set<Id> recipeintWithoutAccess = new set<Id>();
            Set<Id> recipientWithAccessIdSet = new Set<Id>();
            Set<Id> childGroupsSet = new Set<Id>();
            Set<Id> parentRecipientValues = new Set<Id>();
            Map<Id, Set<Id>> userOrGroupWithUsersMap = new Map<Id, Set<Id>>();
            Map<Id, Set<Id>> recipeintWithUserSetMap = new Map<Id, Set<Id>>();
            for (UserShare Ushr : [SELECT Id, UserId, UserOrGroupId FROM UserShare WHERE UserId IN :recipientIdSet]) {

                if (String.valueof(Ushr.UserOrGroupId).startsWith('00G') && !recipientWithAccessIdSet.contains(Ushr.UserId)) {
                    if (userOrGroupWithUsersMap.containsKey(Ushr.UserOrGroupId)) {
                        userOrGroupWithUsersMap.get(Ushr.UserOrGroupId).add(Ushr.UserId);
                    } else {
                        userOrGroupWithUsersMap.put(Ushr.UserOrGroupId, new set<Id>{
                                Ushr.UserId
                        });
                    }

                    if (recipeintWithUserSetMap.containsKey(Ushr.UserId)) {
                        recipeintWithUserSetMap.get(Ushr.UserId).add(Ushr.UserOrGroupId);
                    } else {
                        recipeintWithUserSetMap.put(Ushr.UserId, new Set<Id>{
                                Ushr.UserOrGroupId
                        });
                    }
                }

                if (Ushr.UserOrGroupId == UserInfo.getUserId()) {
                    recipientWithAccessIdSet.add(Ushr.UserId);
                    if (recipeintWithUserSetMap.containsKey(Ushr.UserId)) {
                        recipeintWithUserSetMap.remove(Ushr.UserId);
                    }
                }
            }
            /*
            ** If Direct sharing is not available for the users then Query on Group member records to check record access.
            */
            if (recipientIdSet.size() != recipientWithAccessIdSet.size()) {

                Set<Id> groupIdSet = new Set<Id>();
                for (Set<Id> grpSet : recipeintWithUserSetMap.values()) {
                    groupIdSet.addAll(grpSet);
                }
                //This will be bring all the groupmember where UserOrGroupId could be user id or group id.
                Map<Id, GroupMember> groupMemberMap = new Map<Id, GroupMember>([
                        select id,GroupId,UserOrGroupId
                        from GroupMember
                        where GroupId in:GroupIdSet
                        or UserOrGroupId = :UserInfo.getUserId()
                ]);
                for (GroupMember GrpMbr : groupMemberMap.values()) {
                    //If the UserOrGroupId is equal to Current user id and GroupId is Parent Group Id.
                    if (userOrGroupWithUsersMap.containsKey(GrpMbr.GroupId) && GrpMbr.UserOrGroupId == UserInfo.getUserId()) {
                        recipientWithAccessIdSet.addAll(userOrGroupWithUsersMap.get(GrpMbr.GroupId));
                    }// If the UserOrGroupId is Group Id
                    else if (String.valueof(GrpMbr.UserOrGroupId).startsWith('00G')) {
                        childGroupsSet.add(GrpMbr.UserOrGroupId);
                        parentRecipientValues = userOrGroupWithUsersMap.get(GrpMbr.GroupId);
                        if (userOrGroupWithUsersMap.containsKey(GrpMbr.UserOrGroupId)) {
                            userOrGroupWithUsersMap.get(GrpMbr.UserOrGroupId).addall(parentRecipientValues);
                        } else {
                            userOrGroupWithUsersMap.put(GrpMbr.UserOrGroupId, parentRecipientValues);
                        }
                    }
                }

                for (GroupMember GrpMbr : groupMemberMap.values()) {
                    //If the UserOrGroupId is equal to Current user id and GroupId is Child Group Id.
                    if (childGroupsSet.contains(GrpMbr.GroupId) && GrpMbr.UserOrGroupId == UserInfo.getUserId()
                            && userOrGroupWithUsersMap.containsKey(GrpMbr.GroupId)) {
                        recipientWithAccessIdSet.addAll(userOrGroupWithUsersMap.get(GrpMbr.GroupId));
                    }
                }
            }

            for (Id uId : recipientIdSet) {
                if (!recipientWithAccessIdSet.contains(uId)) {
                    recipeintWithoutAccess.add(uId);
                }
            }
            return recipeintWithoutAccess;
        }

        /*
        **@author:      IQVIA Strikers
        **@date:        12/01/2020
        **@description: This method is used to create usershare records if the running user doesn't have access to recipients.
        */
        public List<UserShare> createUserSharelist(List<ParamsHolder> params) {
            List<UserShare> UserSharelist = new List<UserShare>();
            Set<Id> recipientIdSet = new Set<Id>();
            Set<Id> recipientIdsToConsiderSet = new Set<Id>();
            Set<Id> recipeintWithoutAccessSet = new set<Id>();
            Map<Id, User> specificUserMap;
            /*
            **@author:      IQVIA Strikers
            **@date:        08/18/2020
            **@description: APEX library function "renderStoredEmailTemplate" fails if current user does not have access to recipient user
                            record. Quick fix solution is to provide current user access to recipient user records (if not having already)
                            temporarily (within transaction) just before sending an email and then delete all inserted share records in
                            same transaction once email is sent successfully.
            */
            User autoProc;
            User currentUserCheck;
            for (ParamsHolder ph : params) {
                if (ph.recipientId != null && ph.recipientId.startsWith('005') && UserInfo.getUserType() != 'Guest') {
                    recipientIdSet.add(ph.recipientId);
                }
            }

            if (!recipientIdSet.isEmpty()) {
                specificUserMap = new Map<Id, user>([
                        SELECT Id,ProfileId,Profile.Name,alias
                        FROM User
                        WHERE alias = :'autoproc'
                        OR (Id in:recipientIdSet AND (Profile.name = :VT_R4_ConstantsHelper_Profiles.PATIENT_PROFILE_NAME
                        OR Profile.name = :VT_R4_ConstantsHelper_Profiles.CAREGIVER_PROFILE_NAME))
                        OR Id = :UserInfo.getUserId()
                ]);

                for (User u : specificUserMap.values()) {
                    if (u.alias == 'autoproc') {
                        autoProc = u;
                    } else if (u.Id == UserInfo.getUserId()) {
                        currentUserCheck = u;
                    } else {
                        recipientIdsToConsiderSet.add(u.Id);// Only PT and CG
                    }
                }

                if (!recipientIdsToConsiderSet.isEmpty() && UserInfo.getUserId() != autoProc.Id
                        && !profileNameSet.contains(currentUserCheck.Profile.Name)) {

                    if (recipientIdsToConsiderSet.size() < 200) {
                        recipeintWithoutAccessSet = checkWithUserRecordAccess(recipientIdsToConsiderSet);
                    } else {
                        recipeintWithoutAccessSet = checkWithGroupSharing(recipientIdsToConsiderSet);
                    }

                    if (!recipeintWithoutAccessSet.isEmpty()) {
                        for (Id UiD : recipeintWithoutAccessSet) {
                            UserShare u = new UserShare();
                            u.UserId = UiD;
                            u.UserOrGroupId = UserInfo.getUserId();
                            u.UserAccessLevel = 'Read';
                            UserSharelist.add(u);
                        }
                    }
                }
            }
            return UserSharelist;
        }
    }

    /**
    * Send emails using VF template
    *
    * @param params A list of ParamsHolder
    * @see ParamsHolder
    */
    @InvocableMethod(Label='Send email' Description='Sends email')
    public static void sendEmails(List<ParamsHolder> params) {
        if (System.isBatch() || System.isFuture() || System.isQueueable() || syncProcess) {
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            List<VT_R3_EmailsSender.SubjectWrapper> subjectWrappersList = new List<VT_R3_EmailsSender.SubjectWrapper>();
            List<UserShare> userSharelist = new List<UserShare>();
            Map<String, EmailTemplate> templatesMap = getTemplatesMap(params);
            if (!templatesMap.isEmpty()) {
                try {
                    getOrgWideEmailAddresses();

                    VT_R5_calculateSharingOfRecipientsWrp calculateSharing = new VT_R5_calculateSharingOfRecipientsWrp();
                    userSharelist = calculateSharing.createUserSharelist(params);

                    if (!UserSharelist.isempty()) {
                        insert userSharelist;
                    }

                    for (ParamsHolder ph : params) {
                        if (templatesMap.get(ph.templateDevName).id == null) {
                            continue;
                        }

                        Messaging.SingleEmailMessage mail = buildEmailMessage(ph, templatesMap);
                        mails.add(mail);

                        if (String.isEmpty(mail.subject) || !ph.isTranslationNeeded) {
                            continue;
                        }

                        try {
                            VT_R3_EmailsSender.SubjectWrapper sw = (VT_R3_EmailsSender.SubjectWrapper) JSON.deserialize(mail.subject.replaceAll('\'', '"'), VT_R3_EmailsSender.SubjectWrapper.class);
                            sw.mail = mail;
                            subjectWrappersList.add(sw);
                        } catch (Exception ex) {
                            System.debug('JSON.deserialize error, ' + ex.getStackTraceString());
                        }
                    }
                    if (!subjectWrappersList.isEmpty()) {
                        try {
                            translateSubjects(subjectWrappersList, false);
                            translateSubjectsWithExtraLang(subjectWrappersList);
                        } catch (Exception ex) {
                            System.debug('translateSubjects error, ' + ex.getStackTraceString());
                        }
                    }

                    if (!mails.isEmpty()) {
                        sendEmails(mails);
                    }

                } catch (Exception ex) {
                    System.debug('Exception occured ' + ex.getMessage());
                    ErrorLogUtility.logException(ex, ErrorLogUtility.ApplicationArea.APP_AREA_EMAIL_SENDER, VT_R3_EmailsSender.class.getName(), JSON.serialize(params));

                } finally {
                    try {
                        //Remove the access once the email instance is created successfully.
                        List<UserShare> deleteShareFinallyList = new List<UserShare>();
                        for (UserShare ushr : UserSharelist) {
                            if (ushr.Id != null) {
                                deleteShareFinallyList.add(ushr);
                            }
                        }
                        delete deleteShareFinallyList;

                    } catch (Exception ex) {
                        System.debug('deleteShare error, ' + ex.getStackTraceString());
                    }
                }
            }
        } else {
            sendEmails(JSON.serialize(params));
        }
    }
    /**
     * Send emails future
     *
     * @param paramsHolderJSON A JSON string of List of ParamsHolder
     * @see ParamsHolder
     */
    @Future(Callout=true)
    private static void sendEmails(String paramsHolderJSON) {
        List<ParamsHolder> phList = (List<ParamsHolder>) JSON.deserialize(paramsHolderJSON, List<ParamsHolder>.class);
        sendEmails(phList);
    }

    private static Messaging.SingleEmailMessage buildEmailMessage(ParamsHolder ph, Map<String, EmailTemplate> templatesMap) {
        Messaging.SingleEmailMessage mail;
        /*getEmailTemplatesDevName() method returns Developer name of email templates that has been configured in custom metadata
        in order to avoid execution of renderStoredEmailTemplate*/
        Set<String> emailTempDevName = getEmailTemplatesDevName();

        /*Added or check to verify if email template is one of the email template that has been configured in custom metadata
        in order to avoid execution of renderStoredEmailTemplate */
        if (ph.subjectIsStatic || emailTempDevName.contains(ph.templateDevName)) {
            mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(ph.recipientId);
            mail.setWhatId(ph.relatedToId);
            mail.setTemplateId(templatesMap.get(ph.templateDevName).id);
        } else {
            try {
                System.debug('YY: ' + templatesMap.get(ph.templateDevName).id);
                System.debug('YY: ' + ph.recipientId);
                System.debug('YY: ' + ph.relatedToId);
                mail = Messaging.renderStoredEmailTemplate(templatesMap.get(ph.templateDevName).id, ph.recipientId, ph.relatedToId);
            } catch (Exception e) {
                System.debug(e.getStackTraceString());
            }
        }
//START un commented SH-15303
        if (ph.toAddressesOnly == true) {
            mail.setTargetObjectId(null);
        }
//END un commented
        if (ph.toAddresses != null) {
            mail.setToAddresses(ph.toAddresses.split(';'));
        }
        if (String.isNotBlank(owEmailAddresses)) {
            mail.setOrgWideEmailAddressId(owEmailAddresses);
        }
        if (ph.receiver != null) {
            replaceRemovedVMname(mail, ph.receiver);
        }
        mail.setSaveAsActivity(false);
        return mail;
    }
    /**
     * Translate emails subjects
     *
     * @param subjectWrappersList A list of SubjectWrapper
     * @param reWrite Boolean
     * @see SubjectWrapper
     */
    private static void translateSubjects(List<VT_R3_EmailsSender.SubjectWrapper> subjectWrappersList, Boolean reWrite) {
        Map<String, Map<String, Set<String>>> subjectMap = new Map<String, Map<String, Set<String>>>();
        Map<String, Map<SObjectType, Map<Id, SObject>>> langToObjMap = new Map<String, Map<SObjectType, Map<Id, SObject>>>();
        Map<SObjectType, Set<String>> sObjTypeToLangMap = new Map<SObjectType, Set<String>>();
        for (VT_R3_EmailsSender.SubjectWrapper sW : subjectWrappersList) {
            SObjectType sObjType = Schema.getGlobalDescribe().get(sW.sobjectType.toLowerCase());
//sObjTypeToLangMap
            if (!sObjTypeToLangMap.containsKey(sObjType)) {
                sObjTypeToLangMap.put(sObjType, new Set<String>());
            }
            sObjTypeToLangMap.get(sObjType).add(sW.language);
//langToObjMap
            if (!langToObjMap.containsKey(sW.language)) {
                langToObjMap.put(sW.language, new Map<SObjectType, Map<Id, SObject>>());
            }
            if (!langToObjMap.get(sW.language).containsKey(sObjType)) {
                langToObjMap.get(sW.language).put(sObjType, new Map<Id, SObject>());
            }
//subjectMap
            if (!subjectMap.containsKey(sW.sobjectType)) {
                subjectMap.put(sW.sobjectType, new Map<String, Set<String>>());
            }
            if (!subjectMap.get(sW.sobjectType).containsKey('sobjectIds')) {
                subjectMap.get(sW.sobjectType).put('sobjectIds', new Set<String>());
            }
            subjectMap.get(sW.sobjectType).get('sobjectIds').add(sW.sobjectId);
            if (!subjectMap.get(sW.sobjectType).containsKey('fields')) {
                subjectMap.get(sW.sobjectType).put('fields', new Set<String>());
            }
            for (String field : sW.fields) {
                subjectMap.get(sW.sobjectType).get('fields').add(field.toLowerCase());
            }
        }
        List<SObject> allSObjList = querySObjects(subjectMap).values();
        for (SObject sObj : allSObjList) {
            for (String lang : sObjTypeToLangMap.get(sObj.getSObjectType())) {
                langToObjMap.get(lang).get(sObj.getSObjectType()).put(sObj.Id, sObj.clone(true, true, false, false));
            }
        }
        for (String lang : langToObjMap.keySet()) {
            List<SObject> sObjList = new List<SObject>();
            for (Map<Id, SObject> SObjectsMap : langToObjMap.get(lang).values()) {
                sObjList.addAll(SObjectsMap.values());
            }
            VT_D1_TranslateHelper.translate(sObjList, lang);
        }
        for (VT_R3_EmailsSender.SubjectWrapper sW : subjectWrappersList) {
            String translatedLabel = String.isNotBlank(sW.translatedLabel) ? sW.translatedLabel
                    : VT_D1_TranslateHelper.getLabelValue(sW.labelName, sW.language);
            List<String> labelParams = new List<String>();
            SObject targetObject = langToObjMap.get(sW.language).get(Schema.getGlobalDescribe().get(sW.sobjectType.toLowerCase())).get(sW.sobjectId);
            for (String field : sW.fields) {
                List<String> fieldPath = field.toLowerCase().split('\\.');
                SObject tempVar = targetObject;
                for (Integer i = 0; i < fieldPath.size(); i++) {
                    if (i < (fieldPath.size() - 1)) {
                        tempVar = tempVar.getSObject(fieldPath[i]);
                    } else {
                        if (tempVar != null) {
                            String param = (String) tempVar.get(fieldPath[i]);
                            labelParams.add(param != null ? param : '');
                        } else {
                            String param = (String) targetObject.get(fieldPath[i]);
                            labelParams.add(param != null ? param : '');
                        }
                    }
                }
            }

            for (Integer i = 0; i < labelParams.size(); i++) {
                translatedLabel = translatedLabel.replace('#' + (i + 1), labelParams[i]);
            }
            if (!reWrite) {
                sW.mail.setSubject(translatedLabel);
            } else if (String.isNotBlank(sW.mail.subject)) {
                String newSubject = translatedLabel + ' (' + sW.mail.subject + ')';
                sW.mail.setSubject(newSubject);
            }
        }
    }
    /**
     * Additional translation for emails subjects
     *
     * @param subjectWrappersList A list of SubjectWrapper
     * @see SubjectWrapper
     */
    private static void translateSubjectsWithExtraLang(List<VT_R3_EmailsSender.SubjectWrapper> subjectWrappersList) {
        List<VT_R3_EmailsSender.SubjectWrapper> subjectsForAdditionalTranslation = new List<VT_R3_EmailsSender.SubjectWrapper>();
        for (VT_R3_EmailsSender.SubjectWrapper subject : subjectWrappersList) {
            if (String.isNotBlank(subject.extLang) && subject.extLang != subject.language) {
                subject.language = subject.extLang;
                subjectsForAdditionalTranslation.add(subject);
            }
        }
        if (!subjectsForAdditionalTranslation.isEmpty()) {
            translateSubjects(subjectsForAdditionalTranslation, true);
        }
    }
    /**
     * Query target SObjects
     *
     * @param subjectMap A Map of SObject Type to Map of Ids to List of field names
     *
     * @return List<SObject>
     */
    private static Map<Id, SObject> querySObjects(Map<String, Map<String, Set<String>>> subjectMap) {
        Set<String> sobjectIds = new Set<String>();
        String query ;
        for (String SObjectType : subjectMap.keySet()) {
            String fields = String.join(new List<String>(subjectMap.get(SObjectType).get('fields')), ',');
            for (String objId : subjectMap.get(SObjectType).get('sobjectIds')) {
                //check added to avoid repetition of query if particular record has already been queried.
                if (!sObjMap.containsKey(objId)) {
                    sobjectIds.add(objId);
                }
            }
            if (!sobjectIds.isEmpty()) {
                query = 'SELECT Id,' + fields +
                        ' FROM ' + SObjectType +
                        ' WHERE Id IN :sobjectIds';
                for (SObject obj : Database.query(query)) {
                    //populated map<id,Sobject> in order to maintain data of already queried records.
                    sObjMap.put(obj.id, obj);
                }

            }
        }


        return sObjMap;
    }


    /**
     * Send emails sync or async
     *
     * @param mails A list of Messaging.SingleEmailMessage
     * @see Messaging.SingleEmailMessage
     */
    public static void sendEmails(List<Messaging.SingleEmailMessage> mails) {
        if (System.isBatch() || System.isFuture() || System.isQueueable() || syncProcess) {
            if (!mails.isEmpty()) {
                List<Messaging.SendEmailResult> results = Messaging.sendEmail(mails, false);
                for (Messaging.SendEmailResult result : results) {
                    System.debug('result = ' + result.isSuccess());
                    for (Messaging.SendEmailError error : result.errors) {
                        System.debug(error.getMessage());
                    }
                }
            }
        } else {
            sendEmailsFuture(JSON.serialize(mails));
        }
    }
    /**
     * Send emails async
     *
     * @param mailsJSON A JSON string of Messaging.SingleEmailMessage list
     */
    @Future
    private static void sendEmailsFuture(String mailsJSON) {
        system.debug('mailsJSON ' + mailsJSON);
        List<Messaging.SingleEmailMessage> mails = (List<Messaging.SingleEmailMessage>) JSON.deserialize(
                mailsJSON.replaceAll('"debug":".*, ",', '')
                        .replaceAll('"numRecipients":\\w+,', '')
                        .replaceAll('"userMail":\\w+,', ''),
                List<Messaging.SingleEmailMessage>.class
        );
        sendEmails(mails);
    }

    /**
     * Get templates map
     *
     * @param params A list of ParamsHolder
     *
     * @return Map<String, Id> A map template dev name to template id
     * @see ParamsHolder
     */
    private static Map<String, EmailTemplate> getTemplatesMap(List<ParamsHolder> params) {
        //Map<String, Id> templateDevNamesToIdsMap = new Map<String, Id>();
        Set<String> templates = new Set<String>();
        for (ParamsHolder ph : params) {
            //check added to avoid repetition of query if particular tempelate has already been queried.
            if (!templateDevNamesToTemplateMapGlobal.containsKey(ph.templateDevName)) {
                templates.add(ph.templateDevName);
            }
        }
        //Added null check before query is done in for loop. 
        if (!templates.isEmpty()) {
            for (EmailTemplate template : [
                    SELECT Id, DeveloperName
                    FROM EmailTemplate
                    WHERE DeveloperName IN :templates
            ]) {
                templateDevNamesToTemplateMapGlobal.put(template.DeveloperName, template);
            }
        }

        return templateDevNamesToTemplateMapGlobal;
    }

    /**
     * Get org wide email
     */
    private static void getOrgWideEmailAddresses() {
        //Organization wide email-address's Id has been specified in Hardcoded Ids Custom Setting.
        if (String.isBlank(owEmailAddresses)) {
            owEmailAddresses = VTD1_RTId__c.getInstance().VTR5_Org_Wide_Email_Id__c;
        }
    }
    private static void replaceRemovedVMname(Messaging.SingleEmailMessage mail, String vmName) {
        String mailBody = mail.getHtmlBody();
        mail.setHtmlBody(mailBody.replace('#Visit_participant', vmName));
    }


    /*
    **@author:      IQVIA Strikers
    **@date:        12/01/2020
    **@description: This method queries the Email Template with static Developer names metadata
             in order to avoid use of renderStoredEmailTemplate for specified email templates in metadata.
    */

    public static Set<String> getEmailTemplatesDevName() {
        if (emailTemplatesDevName.isEmpty()) {
            for (VTR5_Email_templates_with_static_subject__mdt obj : [
                    Select VTR5_Email_Template_Developer_Name__c
                    from VTR5_Email_templates_with_static_subject__mdt
            ]) {
                emailTemplatesDevName.add(obj.VTR5_Email_Template_Developer_Name__c);
            }
        }
        return emailTemplatesDevName;
    }

}