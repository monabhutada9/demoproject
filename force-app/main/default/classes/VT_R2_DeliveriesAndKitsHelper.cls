/**
 * Created by shume on 04.04.2019.
 */

public with sharing class VT_R2_DeliveriesAndKitsHelper {
    public static void validateProtocolKits(List<VTD1_Protocol_Kit__c> kitsNew, List<VTD1_Protocol_Kit__c> kitsOld) {
        System.debug('VT_D1_ProtocolKitTrigger');
        Set<String> deliveriesToCheck = new Set<String>();
        for (VTD1_Protocol_Kit__c k : kitsNew) {
            if (k.VTD1_Protocol_Delivery__c != null) {
                deliveriesToCheck.add(k.VTD1_Protocol_Delivery__c);
            }
        }
        if (kitsOld != null) {
            for (VTD1_Protocol_Kit__c k : kitsOld) {
                if (k.VTD1_Protocol_Delivery__c != null) {
                    deliveriesToCheck.add(k.VTD1_Protocol_Delivery__c);
                }
            }
        }
        Set<String> rtDevNames = new Set<String> ();
        rtDevNames.add(VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_CONNECTED_DEVICES);
        rtDevNames.add(VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_STUDY_HUB_TABLET);
        rtDevNames.add(VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_WELCOME_TO_STUDY_PACKAGE);
        List<VTD1_Protocol_Kit__c> kitsInBase = [SELECT Id, RecordType.DeveloperName, VTD1_Protocol_Delivery__c
                                                    FROM VTD1_Protocol_Kit__c
                                                    WHERE VTD1_Protocol_Delivery__c IN :deliveriesToCheck
                                                    AND VTD1_Protocol_Delivery__c != null];
        Map<Id, Set<String>> mapDeliveryKits = new Map<Id, Set<String>>();
        for(VTD1_Protocol_Kit__c pk : kitsInBase) {
            Set<String> rtDevNamesKits = mapDeliveryKits.get(pk.VTD1_Protocol_Delivery__c);
            if (rtDevNamesKits == null) {
                rtDevNamesKits = new  Set<String>();
                mapDeliveryKits.put(pk.VTD1_Protocol_Delivery__c, rtDevNamesKits);
            }
            if (rtDevNamesKits.size() != 0) {
                System.debug('rtDevNames' + rtDevNames);
                System.debug('rtDevNamesKits' + rtDevNamesKits);
                System.debug('pk.RecordType.DeveloperName' + pk.RecordType.DeveloperName);
                if ((rtDevNames.contains(pk.RecordType.DeveloperName)
                        && !rtDevNamesKits.contains(pk.RecordType.DeveloperName))
                        || (!rtDevNames.contains(pk.RecordType.DeveloperName)
                        && (rtDevNamesKits.contains(VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_CONNECTED_DEVICES)
                        || rtDevNamesKits.contains(VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_STUDY_HUB_TABLET)
                        || rtDevNamesKits.contains(VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_WELCOME_TO_STUDY_PACKAGE)))) {
                    kitsNew[0].VTD1_Kit_Type__c.addError(Label.VTR2_KitsCombinedError);
                    /*throw  new DMLException('The following kit type cannot be combined with any others: ' +
                            'Device Kits, Tablet Kits, Welcome Kits');*/
                }
            }
            rtDevNamesKits.add(pk.RecordType.DeveloperName);
            mapDeliveryKits.put(pk.VTD1_Protocol_Delivery__c, rtDevNamesKits);
        }
    }
}