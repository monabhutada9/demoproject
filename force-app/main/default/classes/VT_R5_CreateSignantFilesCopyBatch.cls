/**
 * @author Aliaksandr Vabishchevich
 * @date 03-Nov-20
 * @description R5.5 Create Signant File Copy Object records for all existing records of Signant File Object
 * Batch runs only once in production
 * SH-19237
 */

public with sharing class VT_R5_CreateSignantFilesCopyBatch implements Database.Batchable<SObject>{
    public static Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
                SELECT Id,
                        Name,
                        VTR5_XMLContent__c,
                        VTR5_StudyHubSubjNum__c,
                        VTR5_IsError__c,
                        VTR5_ScrnNum__c,
                        VTR5_ConsDate__c,
                        VTR5_SubjImmuno__c,
                        VTR5_SubjSafety__c,
                        VTR5_ErrorType__c,
                        VTR5_Day1Dose__c,
                        VTR5_DateProcessed__c,
                        VTR5_IsProcessed__c,
                        VTR5_ErrorDetails__c,
                        VTR5_RandDate__c,
                        VTR5_IsThisARescreening__c,
                        VTR5_PrevScrnNumber__c,
                        VTR5_StudyHubSubjNum_Correction__c,
                        VTR5_ConsDate_Correction__c,
                        VTR5_Day1Dose_Correction__c,
                        VTR5_DateReceived__c,
                        VTR5_RelatedCase__c,
                        VTR5_Day57Dose__c,
                        VTR5_Day57Dose_Correction__c,
                        VTR5_IsRescreeningXML__c,
                        VTR5_IsCorrectionXML__c,
                        VTR5_XMLDate__c
                FROM VTR5_Signant_File__c
        ]);
    }
    public static void execute(Database.BatchableContext bc, List<VTR5_Signant_File__c> signantFiles) {
        Map<String, Id> studyIdByIrtValue = new Map<String, Id>();
        List<VTR5_IRTConfiguration__c> irtConfigurations = [SELECT VTR5_Value__c, VTR5_Study__c FROM VTR5_IRTConfiguration__c];
        for (VTR5_IRTConfiguration__c irtConfig : irtConfigurations) {
            studyIdByIrtValue.put(irtConfig.VTR5_Value__c, irtConfig.VTR5_Study__c);
        }
        List<VTR5_Signant_File_Copy__c> sgFilesCopyToInsert = new List<VTR5_Signant_File_Copy__c>();
        for (VTR5_Signant_File__c currentSignantFile : signantFiles) {
            if (currentSignantFile.VTR5_StudyHubSubjNum__c != null) {
                Id studyId = getStudyIdByAbbrv(currentSignantFile.VTR5_StudyHubSubjNum__c, studyIdByIrtValue);
                System.debug('signantCopy: ' + studyId);

                if (studyId != null) sgFilesCopyToInsert.add(createSignantFileCopyRecord(currentSignantFile, studyId));

            }
        }
        if (sgFilesCopyToInsert.size() > 0) {
            insert sgFilesCopyToInsert;
        }
    }

    private static Id getStudyIdByAbbrv(String subjectNumber, Map<String, Id> studyIdByIrtValue) {
        Id studyId;
        System.debug('sg: ' + studyIdByIrtValue);
            String abbrv = subjectNumber.substring(0, 6);
            if (studyIdByIrtValue.containsKey(abbrv)) {
                studyId = studyIdByIrtValue.get(abbrv);
            }
        System.debug('sg: studyId ' + studyId);
        return studyId;
    }

    private static VTR5_Signant_File_Copy__c createSignantFileCopyRecord(VTR5_Signant_File__c currentSignantFile, Id studyId) {
        VTR5_Signant_File_Copy__c sgFileCopy = new VTR5_Signant_File_Copy__c();
        sgFileCopy.Name = currentSignantFile.Name;
        sgFileCopy.VTR5_XMLDate_Copy__c = currentSignantFile.VTR5_XMLDate__c;
        sgFileCopy.VTR5_XMLContent_Copy__c = currentSignantFile.VTR5_XMLContent__c;
        sgFileCopy.VTR5_DateReceived_Copy__c = currentSignantFile.VTR5_DateReceived__c;
        sgFileCopy.VTR5_DateProcessed_Copy__c = currentSignantFile.VTR5_DateProcessed__c;
        sgFileCopy.VTR5_RelatedCase_Copy__c = currentSignantFile.VTR5_RelatedCase__c;
        sgFileCopy.VTR5_IsProcessed_Copy__c = currentSignantFile.VTR5_IsProcessed__c;
        sgFileCopy.VTR5_IsError_Copy__c = currentSignantFile.VTR5_IsError__c;
        sgFileCopy.VTR5_ErrorType_Copy__c = currentSignantFile.VTR5_ErrorType__c;
        sgFileCopy.VTR5_ErrorDetails_Copy__c = currentSignantFile.VTR5_ErrorDetails__c;
        sgFileCopy.VTR5_RelatedStudy__c = studyId;

        //XML Info
        sgFileCopy.VTR5_StudyHubSubjNum_Copy__c = currentSignantFile.VTR5_StudyHubSubjNum__c;
        sgFileCopy.VTR5_ScrnNum_Copy__c = currentSignantFile.VTR5_ScrnNum__c;
        sgFileCopy.VTR5_ConsDate_Copy__c = currentSignantFile.VTR5_ConsDate__c;
        sgFileCopy.VTR5_RandDate_Copy__c = currentSignantFile.VTR5_RandDate__c;
        sgFileCopy.VTR5_IsThisARescreening_Copy__c = currentSignantFile.VTR5_IsThisARescreening__c;
        sgFileCopy.VTR5_PrevScrnNumber_Copy__c = currentSignantFile.VTR5_PrevScrnNumber__c;
        sgFileCopy.VTR5_SubjImmuno_Copy__c = currentSignantFile.VTR5_SubjImmuno__c;
        sgFileCopy.VTR5_SubjSafety_Copy__c = currentSignantFile.VTR5_SubjSafety__c;
        sgFileCopy.VTR5_Day1Dose_Copy__c = currentSignantFile.VTR5_Day1Dose__c;
        sgFileCopy.VTR5_Day57Dose_Copy__c = currentSignantFile.VTR5_Day57Dose__c;
        sgFileCopy.VTR5_IsRescreeningXML_Copy__c = currentSignantFile.VTR5_IsRescreeningXML__c;

        //Correction
        sgFileCopy.VTR5_IsCorrectionXML_Copy__c = currentSignantFile.VTR5_IsCorrectionXML__c;
        sgFileCopy.VTR5_StudyHubSubjNum_Correction_Copy__c = currentSignantFile.VTR5_StudyHubSubjNum_Correction__c;
        sgFileCopy.VTR5_ConsDate_Correction_Copy__c = currentSignantFile.VTR5_ConsDate_Correction__c;
        sgFileCopy.VTR5_Day1Dose_Correction_Copy__c = currentSignantFile.VTR5_Day1Dose_Correction__c;
        sgFileCopy.VTR5_Day57Dose_Correction_Copy__c = currentSignantFile.VTR5_Day57Dose_Correction__c;

        return sgFileCopy;
    }


    public static void finish(Database.BatchableContext bc){}
}