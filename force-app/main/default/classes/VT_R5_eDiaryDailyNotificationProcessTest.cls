/**
 * Created by Dmitry Kovalev on 13.08.2020.
 */

@IsTest
class VT_R5_eDiaryDailyNotificationProcessTest {
    private class HerokuCalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            return new HttpResponse();
        }
    }

    private static final Id STUDY_ID = generateId(HealthCloudGA__CarePlanTemplate__c.SObjectType);
    private static final String DIARY_NAME_1 = 'Test_eDiary_1';
    private static final String DIARY_NAME_2 = 'Test_eDiary_2';
    private static final String DIARY_NAME_3 = 'Test_eDiary_3';

    @IsTest
    private static void executeTest() {
        setupRestContext(Blob.valueOf('[{"payload":"[{\\"timezoneOffset\\":null,\\"recipient\\":\\"Site Staff\\",\\"dueDate\\":\\"2020-08-14T23:00:00.000Z\\",\\"diaryBased\\":true,\\"alertBuilderId\\":\\"a5U0E000000FgwTUAS\\"}]","handlerClassName":"VT_R5_eCoaAlerts"}]'));
        applyDatetimeStub(2020, 8, 12, 10, 55); // 12th of August (Wed) 2020, 13:30 GMT+0
        applyBuildersStub();
        Test.setMock(HttpCalloutMock.class, new HerokuCalloutMock());
        Test.startTest();
        {
            VT_R5_eDiaryDailyNotificationProcess.runJob();
            VT_R5_HerokuScheduler.doPost();
        }
        Test.stopTest();

        System.assert(RestContext.response.responseBody.toString().contains('SUCCESS'), 'Status should be SUCCESS');
        System.assertEquals(200, RestContext.response.statusCode, 'Request should be with 200 status code');
    }

    @IsTest private static void executeTestNegative() {
        setupRestContext(generateLargeRequestBody());
        applyDatetimeStub(2020, 8, 12, 10, 55); // 12th of August (Wed) 2020, 13:30 GMT+0
        applyBuildersStub();
        Test.startTest();
        {
            VT_R5_eDiaryDailyNotificationProcess.runJob();
            VT_R5_HerokuScheduler.doPost();
        }
        Test.stopTest();

        System.assert(RestContext.response.responseBody.toString().contains('FAILED'), 'Status should be FAILED');
        System.assertEquals(500, RestContext.response.statusCode, 'Request should be with 500 status code');
    }

    private static void setupRestContext(Blob body) {
        RestRequest req = new RestRequest();
        req.requestURI = '/services/apexrest/TestUrl';
        req.httpMethod = 'POST';
        req.requestBody = body;
        RestContext.request = req;
        RestContext.response  = new RestResponse();
    }

    private static void applyDatetimeStub(Integer year, Integer month, Integer day, Integer hour, Integer minute) {
        Integer second = Integer.valueOf(Math.random() * 60);
        Integer millisecond = Integer.valueOf(Math.random() * 999);
        Integer userTimezoneOffset = UserInfo.getTimeZone().getOffset(Datetime.now());
        Datetime dt = Datetime.newInstance(
                Date.newInstance(year, month, day),
                Time.newInstance(hour, minute, second, millisecond).addMilliseconds(userTimezoneOffset)
        );
        VT_Stubber.applyStub('eDiaryProcess_Datetime', dt);
    }

    private static void applyBuildersStub() {
        List<VTR5_eDiaryNotificationBuilder__c> builders = new List<VTR5_eDiaryNotificationBuilder__c>();
        builders.add(createBuilder('5:15PM', 'Tuesday;Wednesday;Thursday', DIARY_NAME_1));
        builders.add(createBuilder('5:45PM', 'Tuesday;Wednesday;Thursday', DIARY_NAME_1));
        builders.add(createBuilder('1:00AM', 'Thursday', DIARY_NAME_2));
        builders.add(createBuilder('11:59PM', 'Tuesday;Wednesday', DIARY_NAME_3));
        builders.add(createBuilder('12:01AM', 'Wednesday;Thursday', DIARY_NAME_3));
        VT_Stubber.applyStub('eDiaryProcess_builders', builders);
    }

    private static VTR5_eDiaryNotificationBuilder__c createBuilder(String alertTime, String days, String eDiaryName) {
        Map<String, Object> eDiaryNotificationBuilderMap = new Map<String, Object> {
               'Id' => generateId(VTR5_eDiaryNotificationBuilder__c.SObjectType),
               'VTR5_AlertTime__c' => alertTime,
               'VTR5_DaysOfWeek__c' => days,
               'VTR5_eDiaryName__c' => eDiaryName,
               'VTR5_Study__c' => STUDY_ID,
               'VTR5_DiaryStudyKey__c' => eDiaryName + STUDY_ID,
               'VTR5_RecipientType__c' => Math.random() > 0.5 ? 'Patient/Caregiver' : 'Site Staff'
        };
        Object builder = JSON.deserialize(JSON.serialize(eDiaryNotificationBuilderMap), VTR5_eDiaryNotificationBuilder__c.class);
        return (VTR5_eDiaryNotificationBuilder__c) builder;
    }

    private static Id generateId(SObjectType sot) {
        return fflib_IDGenerator.generate(sot);
    }

    private static Blob generateLargeRequestBody() {
        String requestBody;
        do {
            requestBody += Crypto.getRandomLong();
        } while (requestBody.length() <= 32768);
        return Blob.valueOf(requestBody);
    }
}