/*
** Author:              IQVIA Strikers ( Eashan Parlewar )
** Date:                02-Nov-2020
** Group:               http://jira.quintiles.net/browse/SH-19347
** Group-content:       Cookies Opt IN for Community users.
** Description:         Test class to cover VT_R5_CookieDetailsController
*/
@isTest(SeeAllData = false)
public with sharing class VT_R5_CookieDetailsControllerTest {
    /*
** Description: Cover the method which generate Cookie details in JSON format.
** Return: return json string.
*/
    private static testmethod void TestURL() {
        test.startTest();
        VT_R5_CookieDetailsController.domainName='https://int60-iqviavirtualtrials.cs107.force.com/patient';
        VT_R5_CookieDetailsController.getfunctionalcookiesdata();
        
        test.stopTest();
    }
    /*
** Description: Covers the exception handling in the method getfunctionalcookiesdata().
** Return: Throws aura exceptioin
*/
    private static testmethod void TestExp() {
        test.startTest();
        try {
            VT_R5_CookieDetailsController.domainName='https://test.com/patient';
            VT_R5_CookieDetailsController.getfunctionalcookiesdata();
        } catch( Exception e ) {}
        test.stopTest();
    }
    /*
    ** Description: Covers the exception handling in the method getRequiredCookiesData().
** Return: Throws aura exceptioin
*/
    
    private static testmethod void TestReqCookies() {
        test.startTest();
        try{
        VT_R5_CookieDetailsController.domainName='testDomain';
        String reqCookies = VT_R5_CookieDetailsController.getRequiredCookiesData('en_US');
        System.assertNotEquals(reqCookies, null);
        }catch( Exception e ) {}
        test.stopTest();
    }
    
    private static testmethod void TestReqCookiesPositive() {
        test.startTest();
        try{
        VT_R5_CookieDetailsController.domainName='https://int60-iqviavirtualtrials.cs107.force.com/patient';
        String reqCookies = VT_R5_CookieDetailsController.getRequiredCookiesData('en_US');
        System.assertNotEquals(reqCookies, null);
        }catch( Exception e ) {}
        test.stopTest();
    }
    
}