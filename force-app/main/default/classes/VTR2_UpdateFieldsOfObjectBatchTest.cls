@IsTest private class VTR2_UpdateFieldsOfObjectBatchTest {
    public static final String PROFILE_NAME = 'Standard User';
/*
    @testSetup
    private static void setupMethod() {
        VT_D1_TestUtils.prepareStudy(1);
        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }*/
    @IsTest static void test_VTR2_UpdateFieldsOfObjectBatch() {
        String userName = VT_D1_TestUtils.generateUniqueUserName();
        Profile p = [SELECT Id FROM Profile WHERE Name = :PROFILE_NAME];
        User u = new User(
                Alias = 'stdu79',
                Email = userName,
                EmailEncodingKey = 'UTF-8',
                FirstName = 'User1Batch',
                LastName = 'Test1',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles',
                Username = userName,
                VTR2_WelcomeEmailSent__c = false
        );
        insert u;
        Test.StartTest();
        String id = u.Id;
        //df.query = 'SELECT Id, Profile.Name, VTR2_WelcomeEmailSent__c FROM User WHERE Id = ' + id;
        VTR2_UpdateFieldsOfObjectBatch df = new VTR2_UpdateFieldsOfObjectBatch('SELECT Id, Profile.Name, VTR2_WelcomeEmailSent__c FROM User WHERE Id =\'' + id + '\'');
        ID batchprocessid = Database.executeBatch(df);
        Test.StopTest();
    }
}