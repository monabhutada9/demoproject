/**
 * Created by User on 19/05/14.
 */
@isTest
public with sharing class VT_D1_ZipArchiveControllerTest {

    public static void doTest(){
        DomainObjects.Contact_t conT = new DomainObjects.Contact_t();
        conT.persist();
        ContentVersion cv = (ContentVersion)new DomainObjects.ContentVersion_t().persist();
        DomainObjects.ContentDocumentLink_t doc = new DomainObjects.ContentDocumentLink_t()
                .withLinkedEntityId(conT.id)
                .withContentDocumentId([SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1].ContentDocumentId);
        doc.persist();
        Test.startTest();
        VT_D1_ZipArchiveController.createZipByRecord(conT.Id);
        Test.stopTest();
    }
}