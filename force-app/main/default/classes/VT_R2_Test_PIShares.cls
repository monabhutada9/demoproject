/**
 * Created by MPlatonov on 11.02.2019.
 */

@isTest
public with sharing class VT_R2_Test_PIShares {
    @isTest
    public static void test() {
        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();

        User PIUser = VT_D1_TestUtils.createUserByProfile('Primary Investigator', 'PIUser');
        Test.startTest();
        VT_D1_TestUtils.persistUsers();
        Test.stopTest();

        List <ContactShare> contactShares = [select Id from ContactShare where UserOrGroupId = : PIUser.Id];
        List <AccountShare> accountShares = [select Id from AccountShare where UserOrGroupId = : PIUser.Id];

        for (ContactShare share : [select ContactId, RowCause, ContactAccessLevel from ContactShare where UserOrGroupId = : PIUser.Id]) {
            system.debug(share);
        }
        for (AccountShare share : [select AccountId, RowCause, AccountAccessLevel from AccountShare where UserOrGroupId = : PIUser.Id]) {
            system.debug(share);
        }

        System.debug('Shares = ' + contactShares.size() + ' ' + accountShares.size());
        System.assert(!contactShares.isEmpty());
        System.assert(!accountShares.isEmpty());
    }
}