/**
 * Created by Danylo Belei on 20.05.2019.
 */

@IsTest
public with sharing class VT_D1_SkillsBasedRoutingTest {

    public static void testBehavior() {
        Test.startTest();

        String orgId = VT_D1_SkillsBasedRouting.getOrganizationId();
        System.assertNotEquals(null, orgId);

        String liveChatDeploymentId = VT_D1_SkillsBasedRouting.getLiveChatDeploymentId();
        System.assertNotEquals(null, liveChatDeploymentId);

        String liveChatURL = VT_D1_SkillsBasedRouting.getLiveChatUrl();
        System.assertNotEquals(null, liveChatURL);


        Map<string,Id> liveChatButton = VT_D1_SkillsBasedRouting.getChatbuttonIdMap();
        //System.assertNotEquals(uId, liveChatButton);


//        ServiceChannel serviceChannelId = [Select id, DeveloperName from ServiceChannel limit 1];
//        System.assertEquals(serviceChannelId.Id, VT_D1_SkillsBasedRouting.getChannelId(serviceChannelId.DeveloperName));

//        Skill s = [SELECT id, MasterLabel from Skill limit 1];
//        System.assertEquals(s.Id, VT_D1_SkillsBasedRouting.getSkillId(s.MasterLabel));

        LiveChatButton lcButton = [SELECT Id, MasterLabel, DeveloperName, SkillId FROM LiveChatButton LIMIT 1];
//        System.assertEquals(lcButton.Id, VT_D1_SkillsBasedRouting.getButtonIdByLabel(lcButton.MasterLabel));
//        System.assertEquals(lcButton.Id, VT_D1_SkillsBasedRouting.getButtonIdBySkillId(lcButton.SkillId));
        System.assertEquals(lcButton.Id, VT_D1_SkillsBasedRouting.getButtonIdByDevName(lcButton.DeveloperName));

        Test.stopTest();
    }
}