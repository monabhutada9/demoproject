/**
 * Created by Danylo Belei on 13.05.2019.
 */

@IsTest
public with sharing class VT_D1_CaseVisitsListController_Test {
    @IsTest
    public static void testBehavior() {
        DomainObjects.User_t user = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5));
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );
        DomainObjects.StudyTeamMember_t stm = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('PI')
                .addUser(user)
                .addStudy(study);
        stm.persist();
        DomainObjects.VirtualSite_t virtSite = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .setStudySiteNumber('12345')
                .addStudyTeamMember(stm);
        virtSite.persist();
        Case cas = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .setSubject('EDiaryAllTests')
                .addStudy(study)
                .addUser(user)
                .addVirtualSite(virtSite)
                .persist();
        VTD1_Actual_Visit__c visit = (VTD1_Actual_Visit__c) new DomainObjects.VTD1_Actual_Visit_t()
                .setVTD1_Case(cas.Id)
                .persist();
        String caseIdToString = ''+ cas.Id;
        Test.startTest();
        VT_D1_CaseVisitsListController.getVisits(caseIdToString);
        Test.stopTest();

    }
}