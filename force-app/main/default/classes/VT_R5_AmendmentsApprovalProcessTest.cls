/**
 * Created by Alexey Mezentsev on 11/26/2020.
 */

@IsTest
private class VT_R5_AmendmentsApprovalProcessTest {
    @TestSetup
    static void setupMethod() {
        Id TNCodeRecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByDeveloperName().get('Task').getRecordTypeId();
        Id taskRecordTypeId = Task.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('SimpleTask').getRecordTypeId();
        new DomainObjects.VTD2_TN_Catalog_Code_t()
                .setVTD2_T_Task_Unique_Code('T602')
                .setRecordTypeId(TNCodeRecordTypeId)
                .setVTD2_T_SObject_Name('VTD1_Document__c')
                .setVTD2_T_Care_Plan_Template('IF (ISNULL(VTD1_Document__c.VTD1_Study__c ),NULL, VTD1_Document__c.VTD1_Study__c )')
                .setVTD2_T_Task_Record_Type_ID(taskRecordTypeId)
                .setVTD2_T_Subject('test')
                .setVTD2_T_Assigned_To_ID('VTD1_Document__c.VTD1_Site__r.VTD1_PI_User__c')
                .setVTD2_T_Related_To_Id('VTD1_Document__c.Id');
        DomainObjects.Study_t sdy = new DomainObjects.Study_t().addAccount(new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor'));
        DomainObjects.VirtualSite_t virtualSiteT = new DomainObjects.VirtualSite_t()
                .addStudy(sdy)
                .setStudySiteNumber('12345');
        DomainObjects.User_t userPI = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Primary Investigator');
        DomainObjects.StudyTeamMember_t stmPI = new DomainObjects.StudyTeamMember_t()
                .addStudy(sdy)
                .addUser(userPI)
                .setRecordTypeByName('PI')
                .addVirtualSite(virtualSiteT);
        stmPI.persist();
        Virtual_Site__c site = [SELECT Id FROM Virtual_Site__c];
        site.VTD1_PI_User__c = userPI.id;
        site.VTD1_Study_Team_Member__c = stmPI.id;
        update site;
        VTD1_Regulatory_Document__c regDoc = new VTD1_Regulatory_Document__c(
                Name = 'Test1',
                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI',
                VTD1_Document_Type__c = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_SIGNATURE_PAGE,
                VTD1_TMF__c = true,
                VTD1_Site_RSU__c = true,
                VTD1_Study_RSU__c = true,
                VTD1_Study_IRB__c = true,
                VTD1_Site_IRB__c = true,
                Level__c = 'Site'
        );
        insert regDoc;
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];
        VTD1_Regulatory_Binder__c regulatoryBinder = new VTD1_Regulatory_Binder__c(
                VTD1_Regulatory_Document__c = regDoc.Id,
                VTD1_Care_Plan_Template__c = study.Id
        );
        insert regulatoryBinder;
        VTD1_Document__c doc = new VTD1_Document__c(
                RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT,
                VTD1_Regulatory_Binder__c = regulatoryBinder.Id,
                VTD1_Study__c = study.Id,
                VTD1_Site__c = site.Id,
                VTR4_IRB_Approved__c = false
        );
        insert doc;
    }

    @IsTest
    static void handleIRBApprovalTest() {
        VTD1_Document__c doc = [SELECT Id, VTD1_Approval_Date__c FROM VTD1_Document__c];
        System.assertEquals(null, doc.VTD1_Approval_Date__c);
        doc.VTR4_IRB_Approved__c = true;
        update doc;
        doc = [SELECT Id, VTD1_Approval_Date__c FROM VTD1_Document__c];
        System.assertEquals(System.today(), doc.VTD1_Approval_Date__c);
    }

    @IsTest
    static void handleSentForSigningTest() {
        VTD1_Document__c doc = [SELECT Id, VTD1_Approval_Date__c FROM VTD1_Document__c];
        doc.VTD2_Sent_for_Signature__c = true;
        update doc;
        System.assert(![SELECT Id FROM Task WHERE VTD2_Task_Unique_Code__c = 'T602'].isEmpty());
    }

    @IsTest
    static void handleEAFCompletedTest() {
        VTD1_Document__c doc = [SELECT Id, VTD1_Approval_Date__c FROM VTD1_Document__c];
        Task t = new Task(VTD2_Task_Unique_Code__c = 'T602', Status = 'Open', WhatId = doc.Id);
        insert t;
        doc.VTD2_Sent_for_Signature__c = true;
        doc.VTD2_EAFStatus__c = 'Completed';
        update doc;
        System.assertEquals('Completed', [SELECT Status FROM Task WHERE Id = :t.Id].Status);
    }
}