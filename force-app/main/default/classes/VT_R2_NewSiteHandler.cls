/**
* @author: Carl Judge
* @date: 11-Feb-19
* @description: Handle a new new Virtual Site being added to Case
**/

public without sharing class VT_R2_NewSiteHandler {

    private List<Case> newSiteCases;
    private Map<Id, Map<String, Id>> binderIdMap = new Map<Id, Map<String, Id>>();
    private Map<Id, Boolean> signatureRequiredMap = new Map<Id, Boolean>();

    public VT_R2_NewSiteHandler(List<Case> newSiteCases) {
        this.newSiteCases = newSiteCases;
    }

    public void execute() {
        getBinderIdsForRegDocs();
        system.debug(this.binderIdMap);
        getPiSignatureRequiredMap();

        createPlaceHolders();

    }

    private void getPiSignatureRequiredMap() {
        Set<Id> bindersIds = new Set<Id>();
        for (Map<String, Id> mappy : binderIdMap.values()) {
            bindersIds.addAll(mappy.values());
        }
        for (VTD1_Regulatory_Binder__c rb : [SELECT Id, VTD1_Signature_Required__c FROM VTD1_Regulatory_Binder__c WHERE Id IN :bindersIds]) {
            this.signatureRequiredMap.put(rb.Id, rb.VTD1_Signature_Required__c);
        }
    }

    private void getBinderIdsForRegDocs() {
        List<Id> studyIds = new List<Id>();
        for (Case item : this.newSiteCases) {
            if (item.VTD1_Study__c != null) { studyIds.add(item.VTD1_Study__c); }
        }

        List<String> regDocTypes = new List<String>{
            VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_SOURCE_NOTE_OF_TRANSFER,
            VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_TARGET_NOTE_OF_TRANSFER
        };

        this.binderIdMap = VT_D1_DocumentProcessHelper.getRegBinderIdsForStudies(studyIds, regDocTypes);
    }

    private void createPlaceHolders() {
        List<VTD1_Document__c> placeHolders = new List<VTD1_Document__c>();

        for (Case item : this.newSiteCases) {
            if (this.binderIdMap.containsKey(item.VTD1_Study__c) && this.binderIdMap.get(item.VTD1_Study__c).size() == 2) {
                placeHolders.add(getNewPlaceHolder(
                    item, VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_SOURCE_NOTE_OF_TRANSFER, item.VTD1_Virtual_Site__c
                ));
                placeHolders.add(getNewPlaceHolder(
                    item, VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_TARGET_NOTE_OF_TRANSFER, item.VTR2_New_Virtual_Site__c
                ));
            } else {
                system.debug('Note of Transfer Reg Docs not found for study ID ' + item.VTD1_Study__c);
            }
        }

        if (! placeHolders.isEmpty()) { insert placeHolders; }
    }

    private VTD1_Document__c getNewPlaceHolder(Case patientCase, String docType, Id siteId) {
        return new VTD1_Document__c(
            VTD1_Clinical_Study_Membership__c = patientCase.Id,
            VTD1_Study__c = patientCase.VTD1_Study__c,
            VTD1_Regulatory_Binder__c = this.binderIdMap.get(patientCase.VTD1_Study__c).get(docType),
            VTD1_Site__c = siteId,
            VTD1_Document_Type__c = docType,
            RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_NOTE_OF_TRANSFER
        );
    }
}