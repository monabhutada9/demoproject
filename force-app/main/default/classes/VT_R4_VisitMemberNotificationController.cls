/**
 * Created by user on 18-Dec-19.
 */
public with sharing class VT_R4_VisitMemberNotificationController {
    public Id visitMemberId { get; set; }
    public String allLabels { get; set; }
    public Id visitId { get; set; }
    public String recipientLang { get; set; }
    public String extraLang { get; set; }
    public Visit_Member__c visitMember;

    private String userLanguage = 'en_US';

    public List<String> getLabelList() {
        List<String> labelList = new List<String>();
        labelList = allLabels.replaceAll(' ', '').split(',');

        return labelList;
    }

    private List<String> translateLabels(String lang) {
        List<String> result = new List<String>();
        String rowText = '';
        for (String labelDevName : getLabelList()) {
            if (labelDevName.contains('{')) {
                labelDevName = labelDevName.replaceAll('\'', '"').replaceAll(';', ',');
                LinkWrapper linkWrapper = new LinkWrapper();
                linkWrapper = (VT_R4_VisitMemberNotificationController.LinkWrapper) JSON.deserialize(labelDevName, VT_R4_VisitMemberNotificationController.LinkWrapper.class);
                if (linkWrapper.linkText == 'VTR2_ReasonForParticipation') {
                    String reason = getVisitMember().VTR2_ReasonForParticipation__c;
                    rowText = '<b>' + VT_D1_TranslateHelper.getLabelValue(linkWrapper.linkText, lang) + '</b><br/>' + (reason == null ? '' : reason);
                } else {
                    rowText = '<a href="' + getLink(linkWrapper.linkURL) + '">' + VT_D1_TranslateHelper.getLabelValue(linkWrapper.linkText, lang) + '</a>';
                }
                if (linkWrapper.linkURL == '#ExternalVideoLink' && result.size() > 0) {
                    result[result.size() - 1] = result[result.size() - 1] + '<br/>' + rowText;
                    continue;
                }
            } else {
                rowText = '';
                List<String> lbls = labelDevName.split('=%=');
                Integer i = 0;
                for (String lbl : lbls) {
                    rowText += (i > 0 ? ' ' : '') + VT_D1_TranslateHelper.getLabelValue(lbl, lang);
                    i++;
                }
                rowText = replaceFields(rowText, lang);
            }
            if (rowText != null && !String.isEmpty(rowText)) {
                rowText = rowText.replace('\r\n', '<br>');
            }
            result.add(rowText);
        }

        return result;
    }

    public List<String> getEmailTextList() {
        List<String> emailTextList = new List<String>();
        if (String.isNotBlank(this.recipientLang)) {
            this.userLanguage = this.recipientLang;
        }
        if (String.isNotBlank(extraLang) && extraLang != this.userLanguage) {
            emailTextList.addAll(translateLabels(extraLang));
            emailTextList.addAll(new List<String>{
                    '<br/><br/>'
            });
        }
        emailTextList.addAll(translateLabels(this.userLanguage));

        return emailTextList;
    }
    private String getLink(String linkURl) {
        if (linkURl == '#ExternalVideoLink') {
            linkURl = getJoinToTelevisitUrlForExternal();
        } else if (linkURl == '#SCRVisitLink') {
            linkURl = VTD1_RTId__c.getInstance().VT_R2_SCR_URL__c + '/s/visit?visitId=' + visitMember.VTD1_Actual_Visit__c + '&caseId=' + visitMember.VTD1_Actual_Visit__r.VTD1_Case__c;
        }

        return linkURl;
    }
    public String getJoinToTelevisitUrlForExternal() {
        VTD1_Conference_Member__c conferenceMember = getConferenceMember(visitMemberId);

        return ExternalVideoController.getVideoDataForExternal(conferenceMember.VTD1_Video_Conference__c, conferenceMember.VTD1_Video_Conference__r.SessionId__c, conferenceMember.Id);
    }
    public String replaceFields(String inputText, String lang) {
        if (inputText.contains('#')) {
            visitMember = getVisitMember();
            if (visitMember != null) {
				inputText = replacePhoneNumber(inputText);
				inputText = replacePatientName(inputText);
				inputText = replaceRecipientName(inputText);
				inputText = replaceVisitName(inputText, lang);
				inputText = replaceScheduledDate(inputText, lang);
                inputText = replaceStudyNicknameAndType(inputText, lang);
            }
        }

        return inputText;
    }
    public String replacePhoneNumber(String inputText) {
        String phoneNumber = '';
        if (visitMember.VTD1_Participant_User__c != null) {
            if (visitMember.VTD1_Actual_Visit__r.VTD1_Case__r.VTR2_StudyPhoneNumber__c != null) {
                phoneNumber = visitMember.VTD1_Actual_Visit__r.VTD1_Case__r.VTR2_StudyPhoneNumber__r.Name;
            } else if (visitMember.VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTR2_StudyPhoneNumber__c != null) {
                phoneNumber = visitMember.VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTR2_StudyPhoneNumber__r.Name;
            } else if (visitMember.VTD1_Actual_Visit__r.VTD2_Study_Phone_Number__c != null) {
                phoneNumber = visitMember.VTD1_Actual_Visit__r.VTD2_Study_Phone_Number__c;
            }
        } else {
            Id studyId;
            if (visitMember.VTD1_Actual_Visit__r.VTD1_Case__c != null) {
                studyId = visitMember.VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Study__c;
            } else {
                studyId = visitMember.VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Study__c;
            }
            List<VTR2_StudyPhoneNumber__c> phones = [
                    SELECT Name, VTR2_Study__c, VTR2_Language__c
                    FROM VTR2_StudyPhoneNumber__c
                    WHERE VTR2_Study__c = :studyId
                    AND VTR2_Language__c = 'en_US'
                    ORDER BY CreatedDate DESC
                    LIMIT 1
            ];
            if (phones.size() > 0) {
                phoneNumber = phones[0].Name;
            }
        }

        return inputText.replace('#Phone_number', phoneNumber);
    }

	public String replacePatientName(String messagePattern) {
		String memberType = visitMember.VTD1_Member_Type__c;
		if (memberType == null) {
			memberType = visitMember.VTD1_Participant_User__r.Profile.Name;
		}
		if (visitMember.VTD1_Actual_Visit__c != null) {
			String patientName;
			if (visitMember.VTD1_Member_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME) {
				patientName = visitMember.VTD1_Actual_Visit__r.VTD1_Contact__r.Name;
			} else if (visitMember.VTD1_Member_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME
					|| visitMember.VTD1_Member_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME
					|| visitMember.VTD1_Participant_User__r == null) {
				patientName = visitMember.VTD1_Actual_Visit__r.VTD2_Patient_ID__c;
			} else if (memberType == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME) {
				patientName = visitMember.VTD1_Actual_Visit__r.VTD2_Patient_ID__c;
			} else {
				patientName = visitMember.VTD1_Actual_Visit__r.VTD2_PatientName_PatientId__c;
			}
			if (patientName == null) {
				patientName = '';
			}
			messagePattern = messagePattern.replace('#Patient_Name', patientName);
		}

		return messagePattern;
	}

	public String replaceRecipientName(String inputText) {
		Visit_Member__c visitMember = getVisitMember();
		String recipientName = ' ';
		String memberType = visitMember.VTD1_Member_Type__c;
		if (memberType == null) {
			memberType = visitMember.VTD1_Participant_User__r.Profile.Name;
		}
		String memberName = visitMember.Visit_Member_Name__c;
		if (memberName == null) {
			memberName = visitMember.VTD1_Participant_User__r.FirstName + ' ' + visitMember.VTD1_Participant_User__r.LastName;
		}
		if (memberType == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME || memberType == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME
				|| memberType == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
			recipientName = visitMember.VTD1_Participant_User__r.FirstName;
		} else if (memberType == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) {
			recipientName = 'Dr. ' + visitMember.VTD1_Participant_User__r.LastName;
		} else {
			recipientName = visitMember.Visit_Member_Name__c;
		}
		if (String.isBlank(recipientName)) {
			return inputText;
		}

		return inputText.replace('#Visit_participant', recipientName);
	}

	public String replaceVisitName(String messagePattern, String lang) {
		Visit_Member__c visitMember = getVisitMember();
		if (messagePattern.indexOf('#VisitName') > -1) { //to handle R2 labels
			messagePattern = messagePattern.replace('#VisitName', '#Visit_name');
		}
		messagePattern = messagePattern.replace('#Visit_name', VT_D1_HelperClass.getActualVisitName(visitMember.VTD1_Actual_Visit__r, lang));

		return messagePattern;
	}

	public String replaceScheduledDate(String messagePattern, String lang) {
		Visit_Member__c visitMember = getVisitMember();
		TimeZone tz = UserInfo.getTimeZone();
		if (visitMember.VTD1_Participant_User__r != null) {
			userLanguage = visitMember.VTD1_Participant_User__r.LanguageLocaleKey;
			tz = TimeZone.getTimeZone(visitMember.VTD1_Participant_User__r.TimeZoneSidKey);
			lang = userLanguage;
		} else if (visitMember.VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Patient_User__r != null) {
			// SH-19321 The date and the day of the week does't translate in email fo external participant 21/10/2020
			tz = TimeZone.getTimeZone(visitMember.VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Patient_User__r.TimeZoneSidKey);
		}
		if (visitMember.VTD1_Actual_Visit__c != null) {
			Datetime visitDateTime;
			visitDateTime = visitMember.VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c != null ?
					visitMember.VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c : visitMember.VTD1_Actual_Visit__r.VTR3_Previous_Schedule_Date__c;
			if (visitDateTime != null) {
				messagePattern = messagePattern.replace('#scheduled_date',
						VT_D1_TranslateHelper.translate(visitDateTime.format('EEEE, MMMM d yyyy', tz.getID()), lang))
						.replace('#scheduled_time', VT_D1_TranslateHelper.translate(visitDateTime.format('hh:mm a', tz.getID()), lang))
						.replace(' #timezone', ' ' + VT_D1_TranslateHelper.translateTimeZone(tz.getID(), lang));
			}
		}

		return messagePattern;
	}

	public String replaceStudyNicknameAndType(String messagePattern, String lang) {
        String Study_Nickname;
        if (visitMember.VTD1_Actual_Visit__r.RecordType.DeveloperName == 'VTD1_Unscheduled') {
            Study_Nickname = visitMember.VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Study__r.VTD1_Protocol_Nickname__c;
        } else {
            Study_Nickname = visitMember.VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Study__r.VTD1_Protocol_Nickname__c;
        }
        if (Study_Nickname != null) {
            messagePattern = messagePattern.replace('#Study_nickname', VT_D1_TranslateHelper.getLabelValue(Study_Nickname, lang));
        }
        if (visitMember.VTD1_Actual_Visit__r.VTD2_Common_Visit_Type__c != null) {
            messagePattern = messagePattern.replace('#Visit_type', VT_D1_TranslateHelper.getLabelValue(visitMember.VTD1_Actual_Visit__r.VTD2_Common_Visit_Type__c, lang));
        }

        return messagePattern;
    }

    public VTD1_Conference_Member__c getConferenceMember(Id visitMemberId) {
        VTD1_Conference_Member__c conferenceMember;
        List <VTD1_Conference_Member__c> conferenceMembers = [
                SELECT Id, VTD1_Video_Conference__c, VTD1_Televisit_Start__c, VTD1_Video_Conference__r.SessionId__c
                FROM VTD1_Conference_Member__c
                WHERE VTD1_Visit_Member__c = :visitMemberId
        ];
        if (!conferenceMembers.isEmpty()) {
            conferenceMember = conferenceMembers[0];
        }

        return conferenceMember;
    }

    public Visit_Member__c getVisitMember() {
        if (visitMember != null) {
            return visitMember;
        }
        if (visitId == null) {
            List<Visit_Member__c> visitMembersList = [
                    SELECT Id,
                            VTD1_Member_Type__c,
                            VTD1_Participant_User__r.Profile.Name,
                            Visit_Member_Name__c,
                            VTR2_ReasonForParticipation__c,
                            VTD1_Participant_User__r.FirstName,
                            VTD1_Participant_User__r.LastName,
                            VTD1_Participant_User__r.LanguageLocaleKey,
                            VTD1_Participant_User__r.TimeZoneSidKey,
                            VTD1_Actual_Visit__r.VTD1_Case__c,
                            VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Study__c,
                            VTD1_Actual_Visit__r.VTD2_Study_Phone_Number__c,
                            VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c,
                            VTD1_Actual_Visit__r.VTR3_Previous_Schedule_Date__c,
                            VTD1_Actual_Visit__r.VTD1_Contact__r.Name,
                            VTD1_Actual_Visit__r.VTD2_Patient_ID__c,
                            VTD1_Actual_Visit__r.VTD2_PatientName_PatientId__c,
                            VTD1_Actual_Visit__r.VTD1_Protocol_Visit__r.VTD1_EDC_Name__c,
                            VTD1_Actual_Visit__r.RecordType.DeveloperName,
                            VTD1_Actual_Visit__r.VTD1_Case__r.VTD1_Study__r.VTD1_Protocol_Nickname__c,
                            VTD1_Actual_Visit__r.VTD1_Case__r.VTR2_StudyPhoneNumber__c,
                            VTD1_Actual_Visit__r.VTD1_Case__r.VTR2_StudyPhoneNumber__r.Name,
                            VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Study__c,
                            VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTR2_StudyPhoneNumber__c,
                            VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTR2_StudyPhoneNumber__r.Name,
                            VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Study__r.VTD1_Protocol_Nickname__c,
                            VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Patient_User__r.LanguageLocaleKey,
                            VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Patient_User__r.TimeZoneSidKey,
                            VTD1_Actual_Visit__r.VTD2_Common_Visit_Type__c,
                            VTD1_Actual_Visit__r.VTD1_Unscheduled_Visit_Type__c,
                            VTD1_Actual_Visit__r.Name,
                            VTD1_Actual_Visit__c
                    FROM Visit_Member__c
                    WHERE Id = :visitMemberId
            ];
            VT_D1_TranslateHelper.translate(visitMembersList,
                    visitMembersList.get(0).VTD1_Participant_User__r.LanguageLocaleKey);
            visitMember = visitMembersList.get(0);
        } else { //for removed member
            List<Visit_Member__c> deletedVisitMembersList = new List<Visit_Member__c>();
            deletedVisitMembersList.add(
                    new Visit_Member__c(
                            VTD1_Actual_Visit__c = visitId,
                            VTD1_Actual_Visit__r = [
                                    SELECT Id,
                                            VTD1_Case__c,
                                            VTD1_Case__r.VTD1_Study__c,
                                            VTD2_Study_Phone_Number__c,
                                            VTD1_Scheduled_Date_Time__c,
                                            VTR3_Previous_Schedule_Date__c,
                                            VTD1_Contact__r.Name,
                                            VTD2_Patient_ID__c,
                                            VTD2_PatientName_PatientId__c,
                                            VTD1_Protocol_Visit__r.VTD1_EDC_Name__c,
                                            RecordType.DeveloperName,
                                            VTD1_Case__r.VTD1_Study__r.VTD1_Protocol_Nickname__c,
                                            VTD1_Case__r.VTR2_StudyPhoneNumber__c,
                                            VTD1_Case__r.VTR2_StudyPhoneNumber__r.Name,
                                            Unscheduled_Visits__r.VTD1_Study__c,
                                            Unscheduled_Visits__r.VTR2_StudyPhoneNumber__c,
                                            Unscheduled_Visits__r.VTR2_StudyPhoneNumber__r.Name,
                                            Unscheduled_Visits__r.VTD1_Study__r.VTD1_Protocol_Nickname__c,
                                            Unscheduled_Visits__r.VTD1_Patient_User__r.LanguageLocaleKey,
                                            Unscheduled_Visits__r.VTD1_Patient_User__r.TimeZoneSidKey,
                                            VTD2_Common_Visit_Type__c,
                                            VTD1_Unscheduled_Visit_Type__c,
                                            Name
                                    FROM VTD1_Actual_Visit__c
                                    WHERE Id = :visitId
                            ]
                    ));
            VT_D1_TranslateHelper.translate(deletedVisitMembersList,
                    deletedVisitMembersList.get(0).VTD1_Actual_Visit__r.Unscheduled_Visits__r.VTD1_Patient_User__r.LanguageLocaleKey);
            visitMember = deletedVisitMembersList.get(0);
        }

        return visitMember;
    }

    public class LinkWrapper {
        public String linkText;
        public String linkURL;
    }
}