/**
 * Created by shume on 23/10/2018.
 */

@IsTest
private class VT_D1_Test_ChangeTaskOwnerController {

    @IsTest
    static void testFirstTake() {
        QueueSobject queue = [SELECT Id,QueueId FROM QueueSobject WHERE SobjectType = 'VTD1_SC_Task__c' LIMIT 1];

        Profile testProfile = [SELECT Id FROM Profile WHERE Name = 'Study Concierge' LIMIT 1];
        User testUser = new User(LastName = 'test user 1',
                Username = 'test.user.1@example.com',
                Email = 'test.1@example.com',
                Alias = 'testu1',
                TimeZoneSidKey = 'GMT',
                LocaleSidKey = 'en_GB',
                EmailEncodingKey = 'ISO-8859-1',
                ProfileId = testProfile.Id,
                LanguageLocaleKey = 'en_US');

        VTD1_RTId__c rtId = new VTD1_RTId__c(VTD1_Task_SimpleTask__c = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('SimpleTask').getRecordTypeId());
        insert rtId;
        VTD1_SC_Task__c newT = new VTD1_SC_Task__c();
        newT.OwnerId = queue.QueueId;

        insert newT;
        Test.startTest();
        VT_D1_ChangeTaskOwnerController.changeOwnerMethod(newT.Id);
        System.runAs(testUser) {
            System.debug(newT.OwnerId);
            System.debug(newT.Id);
            VT_D1_ChangeTaskOwnerController.changeOwnerMethod(newT.Id);
            VT_D1_ChangeTaskOwnerController.changeOwnerMethod(newT.Id);
        }
        Test.stopTest();
    }

    @IsTest
    static void testTMA() {
        QueueSobject queue = [SELECT Id,QueueId FROM QueueSobject WHERE SobjectType = 'VTD2_TMA_Task__c' LIMIT 1];

        Profile testProfile = [SELECT Id FROM Profile WHERE Name = 'Therapeutic Medical Advisor' LIMIT 1];
        User testUser = new User(LastName = 'test user 1',
                Username = 'test.user.1@example.com',
                Email = 'test.1@example.com',
                Alias = 'testu1',
                TimeZoneSidKey = 'GMT',
                LocaleSidKey = 'en_GB',
                EmailEncodingKey = 'ISO-8859-1',
                ProfileId = testProfile.Id,
                LanguageLocaleKey = 'en_US');

        VTD1_RTId__c rtId = new VTD1_RTId__c(VTD1_Task_SimpleTask__c = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('SimpleTask').getRecordTypeId());
        insert rtId;
        VTD2_TMA_Task__c newT = new VTD2_TMA_Task__c();
        newT.OwnerId = queue.QueueId;

        insert newT;
        Test.startTest();
        VT_D1_ChangeTaskOwnerController.changeTMAOwner(newT.Id);
        System.runAs(testUser) {
            System.debug(newT.OwnerId);
            System.debug(newT.Id);
            VT_D1_ChangeTaskOwnerController.changeTMAOwner(newT.Id);
            VT_D1_ChangeTaskOwnerController.changeTMAOwner(newT.Id);
        }
        Test.stopTest();
    }
}