/**
* @author: Olya Baranova
* @date: 15-Jul-20
* @description: get token for eCoa iFrame
**/

public without sharing class VT_R5_eCoaSSO {

    /**
  * Get a token to connect eCoa diaries with StudyHub so that patients could complete eCoa diaries in StudyHub
  * @param subjectUserId - id of the subject that opened the diaries
  * @param alwaysSkipScheduleRefresh - by default we refresh the subject schedule if an SSO token is request and
  *         there is currently no schedule info. This skips this check if we want to for some reason
  * @return string token
  */
    public static String getToken(Id subjectUserId, Boolean alwaysSkipScheduleRefresh) {
        User subjectUser = [
                SELECT Id,
                        Profile.Name,
                        VTR5_eCOA_Guid__c,
                        Contact.Account.VTD2_Patient_s_Contact__r.VTD1_UserId__r.VTR5_eCOA_Guid__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eCOA_Guid__c,
                        Contact.VTD1_Clinical_Study_Membership__c,
                        Contact.Account.VTD2_Patient_s_Contact__r.VTD1_UserId__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_PI_contact__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTR5_eCOA_Consent_Diary_triggered__c
                FROM User
                WHERE Id = :subjectUserId
        ];

        String subjectType;
        String subjectId;
        String patientGuid = subjectUser.Contact.Account.VTD2_Patient_s_Contact__r.VTD1_UserId__r.VTR5_eCOA_Guid__c;


        if (patientGuid == null) {
            return null;
        }


        if (subjectUser.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME) {
            subjectType = 'external'; //caregiver
            subjectId = subjectUserId;
        } else if (subjectUser.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
            subjectType = 'subject'; //patient
            subjectId = subjectUser.VTR5_eCOA_Guid__c;
        }

        Auth.JWT jwt = new Auth.JWT();
        jwt.setAud('ecoa:' + eCOA_IntegrationDetails__c.getInstance().VTR5_eCoaEnvironment__c);
        jwt.setIss(eCOA_IntegrationDetails__c.getInstance().VTR5_eCoa_IssuerGuid__c);
        jwt.setSub('guid:' + patientGuid);
        jwt.setValidityLength(3600);
        System.debug('--subjectUserId-'+subjectUserId);
        jwt.setAdditionalClaims(new Map<String, Object>{
                'ecoa:study_guid' => subjectUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eCOA_Guid__c,
                'ecoa:locales' => new List<String>{VT_R5_eCoaLocaleAdaptor.adaptLocale(subjectUserId)},
                'ecoa:injections' => getInjectionMap(subjectUser),
                'ecoa:actor_type' => subjectType,
                'ecoa:actor_id' => subjectId,
                'ecoa:schedule' => 'epro'
        });
        System.debug('--jwt---'+jwt);


        // check if the user has any external diaries yet. If not, then there is no schedule and this token is probably for
        // their first login, so make a call to tell mulesoft to get the schedule in 15 seconds time
        if (!alwaysSkipScheduleRefresh) {
            if ([
                SELECT COUNT()
                FROM VTD1_Survey__c
                WHERE VTD1_CSM__c = :subjectUser.Contact.VTD1_Clinical_Study_Membership__c
                AND RecordType.DeveloperName = 'VTR5_External'
                LIMIT 1
            ] == 0)
            {
                new VT_R5_eCoaSubjectSchedule(new List<Id>{subjectUser.Contact.Account.VTD2_Patient_s_Contact__r.VTD1_UserId__c}).execute();
            }

        }


        if (! Test.isRunningTest()) {
            Auth.JWS jws = new Auth.JWS(jwt, eCOA_IntegrationDetails__c.getInstance().VTR5_certificateName__c);
            return jws.getCompactSerialization();
        } else {
            return '';
        }
    }

    public static String getToken(Id subjectUserId) {
        return getToken(subjectUserId, false);
    }
        /**
    *@param subjectUser -  user object of the subject that openes the diaries
    *@Description   - Creating injection map that need to pass in jwt
    */
    public static map<String,Object> getInjectionMap(User subjectUser){
        Map<String,Object>  injectionMap = new Map<String,Object>();

        Map<String,VTR5_Sponsor_Specific_Protocol_Config__mdt> sponsorSpecificProtocolMap = new  Map<String,VTR5_Sponsor_Specific_Protocol_Config__mdt>();
         sponsorSpecificProtocolMap = getSponsorSpecificProtocol();

        if((sponsorSpecificProtocolMap.containskey(subjectUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c) && 
            sponsorSpecificProtocolMap.get(subjectUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c).VTR5_Inject_Pi_Parameter__c) ||
             Test.isRunningTest()){
                 Boolean reConsentDiaryTriggered = subjectUser.Contact.VTD1_Clinical_Study_Membership__r.VTR5_eCOA_Consent_Diary_triggered__c;

                 injectionMap = getPiInformation(subjectUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_PI_contact__c,reConsentDiaryTriggered); 
               } 

        return injectionMap;
      
    }

    /**
    *@param piConId -  passing pi contact id 
    *@param reConsentDiaryTriggered -   
    *@Description   - getting name,phone and address of pi and returning in map of injected keyword and respective pi info 
    */
    public static Map<String,Object> getPiInformation(String piConId,Boolean reConsentDiaryTriggered){

    Map<String,Object>  piInformationMap = new  Map<String,Object>();
    String piAddrss='';
    String piPhone='';
    String piName='';
    if(String.isNotBlank(piConId)){
        Contact piCon = [select id,Name, 
                        (Select PhoneNumber__c from Phones__r where IsPrimaryForPhone__c=true  Limit 1),
                        (select id,Name,VTD1_Address2__c,VTR3_AddressLine3__c,City__c,Country__c,State__c,ZipCode__c
                         from Addresses__r where VTD1_Primary__c =true  Limit 1) 
                         from contact where id =:piConId];
        if(String.isNotBlank(piCon.Name)){
            piName =piCon.Name;           
        }
        if(!piCon.Phones__r.isEmpty()){
            piPhone = piCon.Phones__r[0].PhoneNumber__c;        
        }
        if(!piCon.Addresses__r.isEmpty()){
            String concatStringOfPiAddrss = piCon.Addresses__r[0].Name+', '+piCon.Addresses__r[0].VTD1_Address2__c+', '+
                                            piCon.Addresses__r[0].VTR3_AddressLine3__c+', '+piCon.Addresses__r[0].City__c+', '+
                                            piCon.Addresses__r[0].State__c+', '+piCon.Addresses__r[0].ZipCode__c+', '+piCon.Addresses__r[0].Country__c ;
            piAddrss = concatStringOfPiAddrss.remove('null, ');        
        }
        if(reConsentDiaryTriggered){
            piInformationMap.put('injected_rec_pi_name',piName);
            piInformationMap.put('injected_rec_pi_tl',piPhone);
            piInformationMap.put('injected_rec_pi_addr',piAddrss);
        }
        else{
            piInformationMap.put('injected_ec_pi_name',piName);
            piInformationMap.put('injected_ec_pi_tl',piPhone);
            piInformationMap.put('injected_ec_pi_addr',piAddrss);
        }
        
        System.debug('---------'+piInformationMap);
         System.debug('----piAddtrss-----'+piAddrss);  
        return piInformationMap;
        
     }
     return null;
    }

    /**  
    *@Description  - getting Sponsor Specific Protocol config metadata and returning in Map. 
    */
    public static Map<String,VTR5_Sponsor_Specific_Protocol_Config__mdt> getSponsorSpecificProtocol(){
     Map<String,VTR5_Sponsor_Specific_Protocol_Config__mdt> studyIdToSponsorSpecificProtocolMap = new Map<String,VTR5_Sponsor_Specific_Protocol_Config__mdt>();

        for(VTR5_Sponsor_Specific_Protocol_Config__mdt sponserProtocol : [SELECT DeveloperName,VTR5_eCoA_DiagnosisDate_month_datakey__c,VTR5_eCoA_DiagnosisDate_year_datakey__c,
                                                                          Stratifiction_Parameter_Keys__c,Pass_Stratification_During_Randomization__c,VTR5_Inject_Pi_Parameter__c,
                                                                          VTR5_PHX_Site_Id__c,VTR5_Validate_BTX_Study__c
                                                                          FROM VTR5_Sponsor_Specific_Protocol_Config__mdt]){
            studyIdToSponsorSpecificProtocolMap.put(sponserProtocol.DeveloperName,sponserProtocol);
        }
       System.debug('---'+studyIdToSponsorSpecificProtocolMap);
       return studyIdToSponsorSpecificProtocolMap;
    }

}