/**
* @author: Carl Judge
* @date: 26-Aug-19
* @description:
**/

public without sharing class VT_R3_GlobalSharingLogic_Case extends VT_R3_GlobalSharingLogic_PatientRelated{
    public override Type getType() {
        return VT_R3_GlobalSharingLogic_Case.class;
    }

    public override VTR3_GlobalSharingConfig__mdt getConfig() {
        return getConfigForObjectType(Case.getSObjectType().getDescribe().getName());
    }

    protected override void addShareDetailsForRecords() {
        // do regular shares
        super.addShareDetailsForRecords();
        // get pcf shares
        for (VTD1_PCF_Chat_Member__c member : [
            SELECT VTD1_PCF__c, VTD1_User_Id__c, VTD1_PCF__r.VTD1_Study__c
            FROM VTD1_PCF_Chat_Member__c
            WHERE VTD1_PCF__c IN :recordIds
        ]) {
            addShareDetail(member.VTD1_PCF__c, member.VTD1_User_Id__c, member.VTD1_PCF__r.VTD1_Study__c, 'Edit');
        }
    }

    protected override Set<Id> getRecordIdsFromCarePlanIds(Set<Id> caseIds) {
        Set<Id> recordIds = new Set<Id>();
        recordIds.addAll(caseIds);
        recordIds.addAll(new Map<Id, Case>([
            SELECT Id
            FROM Case
            WHERE VTD1_Clinical_Study_Membership__c IN :caseIds
            LIMIT :Limits.getLimitQueryRows() - Limits.getQueryRows()
        ]).keySet());
        return recordIds;
    }

    protected override void fillRecordToCasesIdMap() {
        for (Case cas : [
            SELECT Id, VTD1_Clinical_Study_Membership__c, RecordType.DeveloperName FROM Case WHERE Id IN :recordIds
        ]) {
            addToRecordToCaseIdsMap(cas.Id, cas.RecordType.DeveloperName == 'CarePlan' ? cas.Id : cas.VTD1_Clinical_Study_Membership__c);
        }
    }

    protected override String getUserQuery() {
        String query =
            'SELECT Id,' +
                'VTD1_Study__c,' +
                'VTD1_Patient_User__c,' +
                'VTD1_Virtual_Site__c,' +
                'VTD1_Clinical_Study_Membership__r.Id,' +
                'VTD1_Clinical_Study_Membership__r.VTD1_Study__c,' +
                'VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c,' +
                'VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__c,' +
                'VTD1_Clinical_Study_Membership__c,' +
                'RecordType.DeveloperName' +
            ' FROM Case' +
            ' WHERE (VTD1_Patient_User__c IN :patientIds' +
                ' OR VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c IN :patientIds' +
                ' OR VTD1_Patient_User__c IN :cgPatientIds' +
                ' OR VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c IN :cgPatientIds' +
            ')';
        String scopeCondition = getScopeCondition();
        if (scopeCondition != null) { query += ' AND ' + scopeCondition; }
        System.debug(query);
        return query;
    }

    protected override String getRemoveQuery() {
        String removeQuery = 'SELECT Id FROM CaseShare WHERE (';
        if (scopedCaseIds != null) {
            removeQuery += 'CaseId IN :scopedCaseIds OR Case.VTD1_Clinical_Study_Membership__c IN :scopedCaseIds';
        } else if (scopedSiteIds != null) {
            removeQuery += 'Case.VTD1_Virtual_Site__c IN :scopedSiteIds OR Case.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__c IN :scopedSiteIds';
        } else if (scopedStudyIds != null) {
            removeQuery += 'Case.VTD1_Study__c IN :scopedStudyIds OR Case.VTD1_Clinical_Study_Membership__r.VTD1_Study__c IN :scopedStudyIds';
        }
        removeQuery += ') AND UserOrGroupId IN :includedUserIds';
        return removeQuery;
    }

    private String getScopeCondition() {
        String scopeCondition;
        if (scopedCaseIds != null) {
            scopeCondition = '(Id IN :scopedCaseIds OR VTD1_Clinical_Study_Membership__c IN :scopedCaseIds)';
        } else if (scopedSiteIds != null) {
            scopeCondition = '(VTD1_Virtual_Site__c IN :scopedSiteIds OR VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__c IN :scopedSiteIds)';
        } else if (scopedStudyIds != null) {
            scopeCondition = '(VTD1_Study__c IN :scopedStudyIds OR VTD1_Clinical_Study_Membership__r.VTD1_Study__c IN :scopedStudyIds)';
        }
        return scopeCondition;
    }

    protected override void createUserShareDetailsFromRecords(List<SObject> objs) {
        List<Case> records = (List<Case>)objs;
        List<Case> pcfs = new List<Case>();
        for (Case rec : records) {
            Case patientCase;
            if (rec.VTD1_Clinical_Study_Membership__c != null) {
                patientCase = rec.VTD1_Clinical_Study_Membership__r;
            } else {
                patientCase = rec;
            }
            addUserShareDetailsForCase(rec.Id, patientCase);
            if (rec.RecordType.DeveloperName == 'VTD1_PCF') {
                pcfs.add(rec);
            }
        }
        if (!pcfs.isEmpty()) {
            for (VTD1_PCF_Chat_Member__c member : [
                SELECT VTD1_PCF__c, VTD1_User_Id__c, VTD1_PCF__r.VTD1_Study__c
                FROM VTD1_PCF_Chat_Member__c
                WHERE VTD1_PCF__c IN :pcfs
            ]) {
                addShareDetail(member.VTD1_PCF__c, member.VTD1_User_Id__c, member.VTD1_PCF__r.VTD1_Study__c, 'Edit');
            }
        }
    }
}