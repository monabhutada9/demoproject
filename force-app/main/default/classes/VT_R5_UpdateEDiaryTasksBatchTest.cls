/**
 * Created by Aleksandr Mazan on 02-Dec-20.
 */

@IsTest
public with sharing class VT_R5_UpdateEDiaryTasksBatchTest {

    @IsTest
    public static void VT_R5_UpdateEDiaryTasksBatchTest () {
        User herokuScheduler = new User();
        herokuScheduler.LastName = 'Scheduler';
        herokuScheduler.FirstName = 'Heroku';
        herokuScheduler.Username = VT_D1_TestUtils.generateUniqueUserName();
        herokuScheduler.Email = 'test@mailinator.com';
        herokuScheduler.ProfileId = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'][0].id;
        herokuScheduler.Alias = 'Test';
        herokuScheduler.CommunityNickname = 'Test';
        herokuScheduler.TimeZoneSidKey = 'America/Los_Angeles';
        herokuScheduler.LocaleSidKey = 'en_US';
        herokuScheduler.EmailEncodingKey = 'UTF-8';
        herokuScheduler.LanguageLocaleKey = 'en_US';
        insert herokuScheduler;
        List<Schema.RecordTypeInfo> surveyRecordTypes = VTD1_Survey__c.SObjectType.getDescribe().getRecordTypeInfos();
        List<User> users = new List<User>();
        Id surveyEProId;
        for (Schema.RecordTypeInfo surveyRecordType : surveyRecordTypes) {
            if (surveyRecordType.developerName == VT_R5_eDiaryScheduledActionsHeroku.EPRO) {
                surveyEProId = surveyRecordType.recordTypeId;
            }
        }
        Account account = new Account(Name = 'Test Patient');
        insert account;
        List<SObject> sObjects = new List<SObject>();
        List<Contact> contacts = new List<Contact>();
        contacts.add(new Contact(LastName = 'TestContact1', AccountId = account.Id, VTD1_Primary_CG__c = true, VTD1_Clinical_Study_Membership__c = null));
        contacts.add(new Contact(LastName = 'TestContact2'));
        insert contacts;
        for (Profile prof : [SELECT Id, Name FROM Profile WHERE Name = 'Caregiver' OR Name = 'Patient']) {
            if (prof.Name == 'Patient') {
                users.add(new User(Alias = 'PatUser', Email = 'patientuser@testorg.com',
                        EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                        LocaleSidKey = 'en_US', ProfileId = prof.Id,
                        TimeZoneSidKey = 'America/Los_Angeles', UserName = 'newpatientuser@testorg.com', ContactId = contacts[0].Id));
            }
        }
        sObjects.addAll(users);
        Case newCase = new Case(ContactId = contacts[0].Id);
        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c();
        sObjects.add(newCase);
        sObjects.add(study);
        insert sObjects;
        contacts[0].VTD1_Clinical_Study_Membership__c = newCase.Id;
        update contacts[0];
        VTD1_Protocol_ePRO__c newProtocol = new VTD1_Protocol_ePRO__c(VTD1_Study__c = study.Id, VTD1_Subject__c = 'Patient');
        insert newProtocol;
        VTD1_Survey__c survey = new VTD1_Survey__c();
        for (User oneUser : users) {
            if (oneUser.Alias == 'PatUser') {
                survey.VTD1_Patient_User_Id__c = oneUser.Id;
            } else if (oneUser.Alias == 'CaregiverUser') {
                survey.VTD1_Caregiver_User_Id__c = oneUser.Id;
            }
        }
        survey.Name = 'test';
        survey.VTD1_CSM__c = newCase.Id;
        survey.VTD1_Due_Date__c = Datetime.now();
        survey.VTD1_Date_Available__c = Datetime.now().addDays(-1);
        survey.VTD1_Status__c = VT_R5_eDiaryScheduledActionsHeroku.STATUS_NOT_STARTED;
        survey.RecordTypeId = surveyEProId;
        survey.VTR2_Reviewer_User__c = VT_R5_eDiaryScheduledActionsHeroku.USER_PATIENT_GUIDE;
        survey.VTD1_Protocol_ePRO__c = newProtocol.Id;
        survey.VTR5_CompletedBy__c = UserInfo.getUserId();
        survey.VTD1_Reminder_Due_Date__c = Datetime.now().addDays(-1);
        survey.VTR2_Review_Due_Date__c = Datetime.now().addDays(-1);
        survey.VTD1_Scoring_Due_Date__c = Datetime.now().addDays(-1);
        insert survey;
        System.runAs(herokuScheduler) {
            VT_D1_ePROSchedulingTaskHelper.TaskTemplate newTaskTemplate = new VT_D1_ePROSchedulingTaskHelper.TaskTemplate();
            newTaskTemplate.category = 'Other Tasks';
            newTaskTemplate.dueDate = survey.VTD1_Due_Date__c.date();
            newTaskTemplate.recordTypeDeveloperName = 'SimpleTask';
            newTaskTemplate.redirect = 'my-diary';
            newTaskTemplate.subject = 'VTD2_CompleteeDiary';
            newTaskTemplate.subjectParameters = (String) (Id) survey.VTD1_Protocol_ePRO__c;
            newTaskTemplate.surveyId = survey.Id;
            newTaskTemplate.uniqueCode = '010';
            VT_D1_ePROSchedulingTaskHelper.sendPatientCaregiverTasks(new List<VT_D1_ePROSchedulingTaskHelper.TaskTemplate>{
                    newTaskTemplate
            });
        }
        Test.startTest();
        Database.executeBatch(new VT_R5_UpdateEDiaryTasksBatch());
        Test.stopTest();
    }

}