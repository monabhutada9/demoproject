/**
 * @description : The service for Login Flow for Patients and Caregivers on Mobile application
 *
 * @author : Alexander Komarov
 *
 * @date : 13.02.2020
 */
@RestResource(UrlMapping='/Patient/Loginflow/*')
global without sharing class VT_R4_RestMobileLoginFlow {
    private static final String PATIENT_PROFILE_NAME = 'Patient';
    private static final String CAREGIVER_PROFILE_NAME = 'Caregiver';
    private static final Set<String> HIPAA_PROFILES = new Set<String>{
            PATIENT_PROFILE_NAME, CAREGIVER_PROFILE_NAME
    };

    public static final String ACTIVATION_STAGE = 'Activation';
    public static final String PRIVACY_NOTICES_STAGE = 'PrivacyNotices';
    public static final String AUTHORIZATION_STAGE = 'Authorization';
    public static final String SETUP_PASSWORD_STAGE = 'SetupPassword';
    public static final String ARTICLE_NOT_FOUND_ERROR = 'ARTICLE_NOT_FOUND';
    public static final String VERIFICATION_CODE_STAGE = 'VerificationCode';
    public static final String INITIAL_PROFILE_SETUP_STAGE = 'InitialProfileSetup';
    public static final String SUCCESS_STAGE = 'SUCCESS';

    private static final Integer VERIFICATION_CODE_LENGTH = 6;
    private static final Integer VERIFICATION_CODE_TTL_MINUTES = 15; // 15 minutes
    private static final String VERIFICATION_CODE_SESSION_IDENTIFIER = '2FACode!DEVICE';
    private static final String VERIFICATION_CODE_DATE_SESSION_IDENTIFIER = '2FACodeDate!DEVICE';

    private static final String ERROR_CODE_EXPIRED = 'CODE_EXPIRED ';
    private static final String ERROR_CODE_WRONG = 'CODE_WRONG ';
    private static final String SUCCESS_CODE_SENT = 'CODE_SENT ';
    private static final String SUCCESS_CODE_ACCEPTED = 'CODE_ACCEPTED ';
    private static final String ERROR_CODE_NEED = 'CODE_NEED ';
    private static final String ERROR_SETUP_PASSWORD_NEED = 'SETUP_PASSWORD_NEED ';
    private static final String SUCCESS_ARTICLE_SENT = 'ARTICLE_SENT ';

    private static Cache.SessionPartition cachePartition;
//    private static Cache.OrgPartition cachePartition = Cache.Org.getPartition('local.default');
    public static User currentUser;
    public static Boolean isActivationProcess = false;
    static RestRequest request = RestContext.request;
    static RestResponse response = RestContext.response;
    public static PatientLoginFlowResponse patientLoginFlowResponse = new PatientLoginFlowResponse();
    /**
     * @description Adds 2FACode part, removing '-' out of device id and saving to cache
     * ex. FC9548A4-B2CB-4DF0-8234-DCDB67BA468C -> 2FACodeFC9548A4B2CB4DF08234DCDB67BA468C : CODE
     *
     * @param device mobile identifier ex. FC9548A4-B2CB-4DF0-8234-DCDB67BA468C
     * @param value generated code, 6 digits
     */
    private static void addCodeToOrgCache(String device, Object value) {
        String key = VERIFICATION_CODE_SESSION_IDENTIFIER.replace('!DEVICE', device.replace('-', ''));
        cachePartition.put(key, value, VERIFICATION_CODE_TTL_MINUTES * 60, Cache.Visibility.ALL, false);
    }
    /**
     * @description Adds 2FACodeDate part, removing '-' out of device id and saving to cache
     * ex. FC9548A4-B2CB-4DF0-8234-DCDB67BA468C -> 2FACodeFC9548A4B2CB4DF08234DCDB67BA468C : DATE
     *
     * @param device mobile identifier ex. FC9548A4-B2CB-4DF0-8234-DCDB67BA468C
     * @param value current datetime
     */
    private static void addDateToOrgCache(String device, Object value) {
        String key = VERIFICATION_CODE_DATE_SESSION_IDENTIFIER.replace('!DEVICE', device.replace('-', ''));
        cachePartition.put(key, value, VERIFICATION_CODE_TTL_MINUTES * 60, Cache.Visibility.ALL, false);
    }
    /**
     * @description Returns the cached value for given device
     *
     * @param device mobile identifier ex. FC9548A4-B2CB-4DF0-8234-DCDB67BA468C
     *
     * @return either the actual cached value or NOT_NUMBER constant, never null
     */
    private static String getCachedVerificationCode(String device) {
        String cachedCode = (String) cachePartition.get(VERIFICATION_CODE_SESSION_IDENTIFIER.replace('!DEVICE', device.replace('-', '')));
        return cachedCode == null ? 'NOT_NUMBER' : cachedCode;
    }
    private static void initCache() {
        cachePartition = Cache.Session.getPartition('local.default');
    }
    /**
     * @description Class-holder for params all around the Login Flow. Public for unit test.
     */
    public class RequestParams {
        public String stage;
        public String verificationCode;
        public String deviceUniqueCode;
        public Boolean isPrivacyNoticesAccepted;
        public Boolean isAuthorizationAccepted;
        public String newPassword;
        public String userID;
        public String firstName;
        public String lastName;
        public String middleName;
        public String phoneNumber;
        public String state;
        public String gender;
        public Long dateOfBirth;
        public String birthDate;
        public String zipCode;
        public String preferredContactMethod;
        public String language;
        public String secondaryLanguage;
        public String tertiaryLanguage;
    }
    @TestVisible private static RequestParams parsedParams = new RequestParams();
    private static void parseParams() {
        System.debug(request.requestBody.toString());
        parsedParams = (RequestParams) JSON.deserialize(request.requestBody.toString(), RequestParams.class);
        if (parsedParams.userID == null) parsedParams.userID = UserInfo.getUserId();
        System.debug('parsedParams.userID' + parsedParams.userID);

    }
    public static void setUserAndDevice(String deviceCode, String userId) {
        parsedParams.deviceUniqueCode = deviceCode;
        parsedParams.userID = userId;
    }
    /**
     * @description Class-holder for Initial Profile Setup
     */
    private class InitialProfileData {
        Boolean isPatient;
        String firstName;
        String lastName;
        String middleName;
        Long dateOfBirth;
        String birthDate;
        String dobRestriction;
        String gender;
        String phoneNumber;
        String state;
        String zipCode;
        String preferredContactMethod;
        String language;
        String secondaryLanguage;
        String tertiaryLanguage;
        String caseStatus;
        Boolean isAdditionalMsgDisplay;
        List<MobilePicklistOption> primaryLanguages = new List<VT_R4_RestMobileLoginFlow.MobilePicklistOption>();
        List<MobilePicklistOption> additionalLanguages = new List<VT_R4_RestMobileLoginFlow.MobilePicklistOption>();
        List<VT_D1_PatientProfileIntialController.InputSelectRatioElement> contactPreferredContactMethodSelectRatioElementList;
        List<MobilePicklistOption> states = new List<VT_R4_RestMobileLoginFlow.MobilePicklistOption>();
        List<MobilePicklistOption> genders = new List<VT_R4_RestMobileLoginFlow.MobilePicklistOption>();

        private InitialProfileData() {
            VT_R5_PatientProfileService patientProfile = new VT_R5_PatientProfileService();
            this.firstName = patientProfile.contact.FirstName;
            this.lastName = patientProfile.contact.LastName;
            this.middleName = patientProfile.contact.VTD1_MiddleName__c;
            this.phoneNumber = patientProfile.phone.PhoneNumber__c;
            this.dobRestriction = patientProfile.getDateOfBirthRestriction();
            this.dateOfBirth = patientProfile.contact.Birthdate != null ? Datetime.newInstance(patientProfile.contact.Birthdate, Time.newInstance(0,0,0,0)).getTime() : null;
            fillDateOfBirth(patientProfile.contact.Birthdate, patientProfile.getDateOfBirthRestriction());
            this.isPatient = patientProfile.usr.Profile.Name.equals(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
            this.gender = isPatient ? patientProfile.contact.HealthCloudGA__Gender__c : null;
            setPreferredLanguages(patientProfile.contact);
            this.preferredContactMethod = patientProfile.contact.Preferred_Contact_Method__c;
            this.contactPreferredContactMethodSelectRatioElementList =
                    VT_D1_PatientProfileIntialController.getContactPreferredContactMethodSelectRatioElementList(patientProfile.contact);
            if (this.isPatient) setAddressDataForPatient(patientProfile.address);
            this.isAdditionalMsgDisplay = isAdditionalMessageShouldDisplay(patientProfile.cse);
            this.caseStatus = patientProfile.cse.Status;
            this.genders = getGenderOptions(patientProfile.contact);
        }

        private List<MobilePicklistOption> getGenderOptions(Contact contact) {
            List<MobilePicklistOption> result = new List<MobilePicklistOption>();
            Map<String, String> optionsMap = VT_D1_CustomMultiPicklistController.getSelectOptionsWithLabel(contact, 'HealthCloudGA__Gender__c');
            for (String s : optionsMap.keySet()) {
                result.add(new MobilePicklistOption(s, optionsMap.get(s)));
            }
            return result;
        }

        private void setPreferredLanguages(Contact contact) {
            String primaryLanguage = contact.VTR2_Primary_Language__c;
            String secondaryLanguage = contact.VT_R5_Secondary_Preferred_Language__c;
            String tertiaryLanguage = contact.VT_R5_Tertiary_Preferred_Language__c;

            if (String.isNotBlank(primaryLanguage)) this.language = primaryLanguage;
            if (String.isNotBlank(secondaryLanguage)) this.secondaryLanguage = secondaryLanguage;
            if (String.isNotBlank(tertiaryLanguage)) this.tertiaryLanguage = tertiaryLanguage;

            this.primaryLanguages = filterPrimaryLanguages(getMobilePicklistOptions('Contact', 'VTR2_Primary_Language__c'));
            this.additionalLanguages = getMobilePicklistOptions('Contact', 'VT_R5_Secondary_Preferred_Language__c');
        }

        private List<MobilePicklistOption> filterPrimaryLanguages(List<MobilePicklistOption> income) {
            List<MobilePicklistOption> result = new List<MobilePicklistOption>();
            Set<String> validLanguages = new Set<String>();
            List<String> approved = currentUser.Contact.VTD1_Clinical_Study_Membership__r?.VTD1_Virtual_Site__r?.VTR5_IRBApprovedLanguages__c?.split(';');
            if (approved != null && !approved.isEmpty()) {
                validLanguages.addAll(currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c.split(';'));
            } else {
                validLanguages.add('en_US');
            }
            for (MobilePicklistOption mpo : income) {
                if (validLanguages.contains(mpo.key)) result.add(mpo);
            }
            return result;
        }

        private void setAddressDataForPatient(Address__c address) {
            this.state = address.State__c;
            this.zipCode = address.ZipCode__c;

            if (address.Country__c != null) {
                VTR2_DependentPicklist.Picklist p = VTR2_DependentPicklist.getDependentPicklist(
                        'Address__c', 'Country__c', 'State__c'
                );
                VTR2_DependentPicklist.PicklistOption neededOption;
                for (VTR2_DependentPicklist.PicklistOption po : p.options) {
                    if (po.value.equals(address.Country__c)) {
                        neededOption = po;
                    }
                }
                for (VTR2_DependentPicklist.PicklistOption po : neededOption.dependentOptions) {
                    this.states.add(new MobilePicklistOption(po.value, po.label));

                }
            }
        }

        private Boolean isAdditionalMessageShouldDisplay(Case cse) {
            if (cse == null) return false;
            return cse.VTD2_Patient_Status__c.equals(VT_R4_ConstantsHelper_Statuses.CASE_PRE_CONSENT);
        }

        private void fillDateOfBirth(Date birthDate, String restriction) {
            String YEAR_ONLY = 'Year Only';
            if (birthDate == null) return;
            this.birthDate = restriction.equals(YEAR_ONLY)
                    ? String.valueOf(birthDate.year())
                    : String.valueOf(birthDate);
        }
    }

    private class MobilePicklistOption {
        String key;
        String label;

        MobilePicklistOption(String key, String label) {
            this.key = key;
            this.label = label;
        }
    }

    private static List<MobilePicklistOption> getMobilePicklistOptions(String obj, String field) {
        List<MobilePicklistOption> result = new List<MobilePicklistOption>();
        SObjectType type = Schema.getGlobalDescribe().get(obj);
        Schema.SObjectField sObjectField = type.getDescribe().fields.getMap().get(field);
        for (Schema.PicklistEntry pe : sObjectField.getDescribe().getPicklistValues()) {
            result.add(new MobilePicklistOption(pe.getValue(), pe.getLabel()));
        }
        return result;
    }
    /**
     * @description Checks verification code exists and not expired
     *
     * @param deviceCode device mobile identifier ex. FC9548A4-B2CB-4DF0-8234-DCDB67BA468C
     *
     * @return TRUE if expired, otherwise FALSE
     */
    public static Boolean checkVerificationCodeExpired(String deviceCode) {
        Datetime codeCreated = (Datetime) cachePartition.get(VERIFICATION_CODE_DATE_SESSION_IDENTIFIER.replace('!DEVICE', deviceCode.replace('-', '')));
        System.debug('codeCreated =' + codeCreated);
        if (getCachedVerificationCode(deviceCode).equals('NOT_NUMBER') || getCachedVerificationCode(deviceCode).equals('OLD_CODE')) {
            System.debug('well, null code, cute cache');
            return true;
        }
        System.debug('result=' + (codeCreated.addMinutes(VERIFICATION_CODE_TTL_MINUTES) < System.now()));
        if (codeCreated == null || codeCreated.addMinutes(VERIFICATION_CODE_TTL_MINUTES) < System.now()) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * @description PUT service for 'resend code' button. Re-generates, caches and sends the code.
     */

    @HttpPut
    global static void resendCode() {


        initCache();
        try {
            parseParams();
            setCurrentUser();
            sendVerificationCode(parsedParams.deviceUniqueCode);
        } catch (Exception e) {
            sendResponse(500, 'internal server error');
        }

    }
    /**
     * @description Returns current login flow state (stage) and relevant data.
     */
    @HttpGet
    global static void getLoginFlowState() {
        initCache();
        if (parsedParams.userID == null) {
            parsedParams.userID = UserInfo.getUserId();
        }

        if (parsedParams.deviceUniqueCode != null) {
            System.debug('Got code from parsedParams');
        } else if (!request.params.containsKey('deviceUniqueCode')) {
            patientLoginFlowResponse.serverMessage += 'deviceUniqueCode missing ';
            sendResponse(400, 'deviceUniqueCode missing');
            return;
        } else {
            parsedParams.deviceUniqueCode = request.params.get('deviceUniqueCode');
        }
        try {
            setCurrentStatuses();
            formGetLoginFlowStateResponse();
            sendResponse();
        } catch (Exception e) {
            sendResponse(e);
        }

    }

    private static void formGetLoginFlowStateResponse() {
        String currentStage = patientLoginFlowResponse.currentStage;
        System.debug('currentStage' + currentStage);

        if (currentStage.equals(VERIFICATION_CODE_STAGE)) {
            if (checkVerificationCodeExpired(parsedParams.deviceUniqueCode)) {
                sendVerificationCode(parsedParams.deviceUniqueCode);
                patientLoginFlowResponse.serverMessage += ERROR_CODE_EXPIRED + SUCCESS_CODE_SENT;
            } else {
                patientLoginFlowResponse.serverMessage += ERROR_CODE_NEED;
            }
        } else if (currentStage.equals(PRIVACY_NOTICES_STAGE) || currentStage.equals(AUTHORIZATION_STAGE)) {
            PatientLoginFlowArticle article = getArticle(currentStage);
            if (article != null) {
                patientLoginFlowResponse.setArticle(article, currentStage);
                patientLoginFlowResponse.serverMessage += SUCCESS_ARTICLE_SENT;
            } else {
                patientLoginFlowResponse.errors.add('no article found');

                patientLoginFlowResponse.studyPhoneNumber = currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD2_Study_Phone_Number__c;
                patientLoginFlowResponse.currentStage = ARTICLE_NOT_FOUND_ERROR;

            }
        } else if (currentStage.equals(SETUP_PASSWORD_STAGE) && isActivationProcess) {
            patientLoginFlowResponse.serverMessage += ERROR_SETUP_PASSWORD_NEED;
            patientLoginFlowResponse.username = currentUser.Username;
        } else if (currentStage.equals(INITIAL_PROFILE_SETUP_STAGE)) {
            patientLoginFlowResponse.initialProfileData = new InitialProfileData();
        } else if (currentStage.equals(SUCCESS_STAGE)) {
//            if (currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eDiaryTool__c != null) {
//                if (currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eDiaryTool__c.equalsIgnoreCase('eCOA')) {
//                    currentUser.VTR5_Create_Subject_in_eCOA__c = true;
//                }
//                update currentUser;
//            }
            patientLoginFlowResponse.serverMessage += SUCCESS_STAGE;
        }
    }

    private static Boolean isParamsMissed() {
        if (parsedParams.stage == null || parsedParams.deviceUniqueCode == null) {
            sendResponse(400, 'stage or deviceUniqueCode missing');
            return true;
        } else if (parsedParams.stage.equals(VERIFICATION_CODE_STAGE) && parsedParams.verificationCode == null) {
            sendResponse(400, 'verificationCode is missing');
            return true;
        } else if (parsedParams.stage.equals(PRIVACY_NOTICES_STAGE) && parsedParams.isPrivacyNoticesAccepted == null) {
            sendResponse(400, 'isPrivacyNoticesAccepted is missing');
            return true;
        } else if (parsedParams.stage.equals(AUTHORIZATION_STAGE) && parsedParams.isAuthorizationAccepted == null) {
            sendResponse(400, 'isAuthorizationAccepted is missing');
            return true;
        } else if (parsedParams.stage.equals(INITIAL_PROFILE_SETUP_STAGE) && (parsedParams.firstName == null
                || parsedParams.lastName == null || parsedParams.phoneNumber == null || parsedParams.preferredContactMethod == null)) {
            sendResponse(400, 'initial profile data is missing');
            return true;
        } else {
            return false;
        }
    }

    private static Boolean gotErrorInAction = false;
    private static Boolean userUpdateNeed = false;
    @HttpPost
    global static void postAction() {


        if (request.params.containsKey('activation')) { //to get this info from login call
            isActivationProcess = Boolean.valueOf(request.params.get('activation'));
        }
        parseParams();
        if (!isActivationProcess) {
            initCache();
        }

        if (isParamsMissed()) return;

        setCurrentUser();
        System.debug('parsedParams == ' + JSON.serializePretty(parsedParams));

        gotErrorInAction = false;
        if (parsedParams.stage.equals(VERIFICATION_CODE_STAGE)) {
            processVerificationCodeStage();
        } else if (parsedParams.stage.equals(PRIVACY_NOTICES_STAGE)) {
            processPrivacyNoticeStage();
        } else if (parsedParams.stage.equals(AUTHORIZATION_STAGE)) {
            processAuthorization();
        } else if (parsedParams.stage.equals(ACTIVATION_STAGE)) {
            processActivationStage();
        } else if (parsedParams.stage.equals(INITIAL_PROFILE_SETUP_STAGE)) {
            processInitialProfileSetup();
        } else if (parsedParams.stage.equals(SETUP_PASSWORD_STAGE)) {
            processSetupPasswordStage();
            sendResponse();
            System.debug('plfr ' + patientLoginFlowResponse);
            return;
        }
        if (gotErrorInAction || activationForbidden) {
            System.debug('error 353');
            sendResponse(200);
            return;
        } else {
            if (userUpdateNeed) update currentUser;
            getLoginFlowState();
        }
    }

    private static void processSetupPasswordStage() {
        currentUser.VTR4_Is_Activated__c = true;
        update currentUser;
        patientLoginFlowResponse.newPassword = parsedParams.newPassword;
        patientLoginFlowResponse.username = currentUser.Username;
        System.setPassword(currentUser.Id, parsedParams.newPassword);
        System.debug('SETTING PASSWORD ' + patientLoginFlowResponse.newPassword);
    }

    private static final String ALREADY_ACTIVATED_MESSAGE = 'ALREADY_ACTIVATED';
    private static final String ACTIVATION_EXPIRED_MESSAGE = 'ACTIVATION_EXPIRED';
    private static Boolean activationForbidden = false;

    private static void processActivationStage() {
        if (currentUser.CreatedDate.addDays(7) < System.now()) {
            patientLoginFlowResponse.serverMessage = ACTIVATION_EXPIRED_MESSAGE;
            activationForbidden = true;
        } else if (currentUser.VTR4_Is_Activated__c) {
            System.debug('parsedParams' + parsedParams);
            patientLoginFlowResponse.serverMessage = ALREADY_ACTIVATED_MESSAGE;
            activationForbidden = true;
        } else {
            if (parsedParams.deviceUniqueCode != null && currentUser.VTR3_Verified_Mobile_Devices__c.indexOf(parsedParams.deviceUniqueCode) == -1) {
                currentUser.VTR3_Verified_Mobile_Devices__c += parsedParams.deviceUniqueCode + ';';
            }
            userUpdateNeed = true;
        }

    }


    private static void processAuthorization() {
        if (parsedParams.isAuthorizationAccepted) {
            currentUser.HIPAA_Accepted__c = true;
            currentUser.HIPAA_Accepted_Date_Time__c = System.now();
            userUpdateNeed = true;
        } else {
            patientLoginFlowResponse.serverMessage += 'AUTHORIZATION_HAS_TO_BE_ACCEPTED';
            gotErrorInAction = true;
        }
    }

    private static void processPrivacyNoticeStage() {
        if (parsedParams.isPrivacyNoticesAccepted) {
            currentUser.VTD1_Privacy_Notice_Last_Accepted__c = System.now();
            userUpdateNeed = true;
        } else {
            patientLoginFlowResponse.serverMessage += 'PRIVACY_NOTICES_HAS_TO_BE_ACCEPTED';
            gotErrorInAction = true;
        }
    }

    private static void processVerificationCodeStage() {
        if (!needVerificationCode(parsedParams.deviceUniqueCode)) {
            patientLoginFlowResponse.serverMessage += 'WRONG_STAGE ';
            gotErrorInAction = true;
        }
        if (checkVerificationCodeExpired(parsedParams.deviceUniqueCode)) {
            getLoginFlowState();
        } else {
            if (getCachedVerificationCode(parsedParams.deviceUniqueCode).equals(parsedParams.verificationCode)) {
                patientLoginFlowResponse.serverMessage += SUCCESS_CODE_ACCEPTED;
                if (currentUser.VTR3_Verified_Mobile_Devices__c.indexOf(parsedParams.deviceUniqueCode) == -1) {
                    currentUser.VTR3_Verified_Mobile_Devices__c += parsedParams.deviceUniqueCode + ';';
                }
                currentUser.First_Log_In__c = System.now().date();
                userUpdateNeed = true;
                addCodeToOrgCache(parsedParams.deviceUniqueCode, 'OLD_CODE');

            } else {
                patientLoginFlowResponse.serverMessage += ERROR_CODE_WRONG;
                patientLoginFlowResponse.message = VT_D1_TranslateHelper.getLabelValue(Label.VTR3_InvalidVerificationCode, UserInfo.getLanguage());
                gotErrorInAction = true;
            }
        }
    }
    public static void processInitialProfileSetup() {
        VT_R5_PatientProfileService patientService = new VT_R5_PatientProfileService();
        patientService.contact.FirstName = parsedParams.firstName;
        patientService.contact.Preferred_Contact_Method__c = parsedParams.preferredContactMethod;
        patientService.contact.LastName = parsedParams.lastName;
        if (parsedParams.middleName != null) patientService.contact.VTD1_MiddleName__c = parsedParams.middleName;
        if (parsedParams.language != null) patientService.contact.VTR2_Primary_Language__c = parsedParams.language;
        if (parsedParams.secondaryLanguage != null) patientService.contact.VT_R5_Secondary_Preferred_Language__c = parsedParams.secondaryLanguage;
        if (parsedParams.tertiaryLanguage != null) patientService.contact.VT_R5_Tertiary_Preferred_Language__c = parsedParams.tertiaryLanguage;
        if (parsedParams.gender != null) patientService.contact.HealthCloudGA__Gender__c = parsedParams.gender;

        if (parsedParams.dateOfBirth != null) {
            patientService.contact.Birthdate =
                    Datetime.newInstance(parsedParams.dateOfBirth - UserInfo.getTimeZone().getOffset(System.now())).date();
        }
        if (parsedParams.birthDate != null) {
            try {
                patientService.contact.Birthdate = parsedParams.birthDate.length() == 4
                        ? Date.newInstance(Integer.valueOf(parsedParams.birthDate), 1, 1)
                        : Date.valueOf(parsedParams.birthDate);
            } catch (TypeException te) {System.debug(te.getMessage() + ': ' + te.getStackTraceString());}
        }
        String stateForCreate, zipCodeForCreate, phoneNumberForCreate = parsedParams.phoneNumber;
        if (patientService.usr.Profile.Name.equals('Patient')) {
            if (patientService.address == null) {
                stateForCreate = parsedParams.state;
            } else {
                patientService.address.ZipCode__c = parsedParams.zipCode;
                patientService.address.State__c = parsedParams.state;
            }
            zipCodeForCreate = parsedParams.zipCode;
        }
        patientService.updatePatientProfile(phoneNumberForCreate, stateForCreate, zipCodeForCreate);
    }


    public static PatientLoginFlowArticle getArticle(String stage) {
        String preferredArticleLanguage = currentUser.LanguageLocaleKey;

        String articleType = stage == PRIVACY_NOTICES_STAGE
                ? 'Privacy_Notice'
                : 'HIPAA';
        Knowledge__kav preferredLanguageArticle;

            for (Knowledge__kav item : [
                    SELECT Id, VTD1_Content__c, Language, Title, Summary
                    FROM Knowledge__kav
                    WHERE IsLatestVersion = TRUE
                    AND PublishStatus = 'Online'
                    AND VTD2_Type__c INCLUDES(:articleType)
                    AND VTD2_Study__c = :currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c
                    AND Language = :preferredArticleLanguage
            ]) {
                if (item.Language == preferredArticleLanguage) {
                    preferredLanguageArticle = item;
                    break;
                }
            }

        if (preferredLanguageArticle != null) {
            return new PatientLoginFlowArticle(preferredLanguageArticle);
        } else {
            return null;
        }


    }
    /**
     * @description Send verification code via email and save it to org cache.
     *
     * @param deviceUniqueCode device mobile identifier ex. FC9548A4-B2CB-4DF0-8234-DCDB67BA468C
     */
    public static void sendVerificationCode(String deviceUniqueCode) {
        String verificationCode = '';
        for (Integer i = 0; i < VERIFICATION_CODE_LENGTH; i++) {
            verificationCode += Integer.valueOf(Math.random() * 10);
        }
        OrgWideEmailAddress oea = VT_D1_HelperClass.getOrgWideEmailAddress();
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        if (oea != null) {
            message.setOrgWideEmailAddressId(oea.Id);
        }
        message.setSaveAsActivity(false);
        message.setTargetObjectId(currentUser.Id);
        message.setSubject(System.Label.VTR3_VerificationRequired);
        message.plainTextBody = System.Label.VTR3_VerificationCode + ' ' + verificationCode;
        message.setCharset('UTF-8');
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>{
                message
        };
        addCodeToOrgCache(deviceUniqueCode, verificationCode);
        addDateToOrgCache(deviceUniqueCode, System.now());
        System.debug('Code sended to user id =' + currentUser.Id);
        Messaging.sendEmail(messages);
    }
    public static PatientLoginFlowResponse setCurrentStatuses() {
        System.debug('setting current user ' + parsedParams.userID);
        if (isActivationProcess) patientLoginFlowResponse = new PatientLoginFlowResponse();
        setCurrentUser();
        if (isActivationProcess) {
            patientLoginFlowResponse.isVerificationCodeAccepted = true;
        } else {
            patientLoginFlowResponse.isVerificationCodeAccepted = !needVerificationCode(parsedParams.deviceUniqueCode);
        }
        patientLoginFlowResponse.isPrivacyNoticesAccepted = !needPrivacyNotices();
        patientLoginFlowResponse.isAuthorizationAccepted = !needAuthorization();
        patientLoginFlowResponse.isInitialProfileSetupAccepted = !needInitialProfileSetup();
        patientLoginFlowResponse.currentStage =
                isActivationProcess
                        ? patientLoginFlowResponse.isPrivacyNoticesAccepted
                        ? patientLoginFlowResponse.isAuthorizationAccepted
                                ? SETUP_PASSWORD_STAGE
                                : AUTHORIZATION_STAGE
                        : PRIVACY_NOTICES_STAGE
                        : patientLoginFlowResponse.isVerificationCodeAccepted
                        ? patientLoginFlowResponse.isPrivacyNoticesAccepted
                                ? patientLoginFlowResponse.isAuthorizationAccepted
                                        ? patientLoginFlowResponse.isInitialProfileSetupAccepted
                                                ? SUCCESS_STAGE
                                                : INITIAL_PROFILE_SETUP_STAGE
                                        : AUTHORIZATION_STAGE
                                : PRIVACY_NOTICES_STAGE
                        : VERIFICATION_CODE_STAGE;
        System.debug('patientLoginFlowResponse.currentStage ' + patientLoginFlowResponse.currentStage);
        return patientLoginFlowResponse;
    }
    /**
     * @description SELECT and set this.currentUser with User with given Id
     */
    private static void setCurrentUser() {
        currentUser = [
                SELECT Id,
                        LanguageLocaleKey,
                        Username,
                        Profile.Name,
                        HIPAA_Accepted__c,
                        VTD1_Privacy_Notice_Last_Accepted__c,
                        First_Log_In__c,
                        ContactId,
                        Contact.Account.Candidate_Patient__r.Study__c,
                        VTR3_Verified_Mobile_Devices__c,
                        Email,
                        VTD1_Filled_Out_Patient_Profile__c,

                        Contact.VTD1_Clinical_Study_Membership__r.VTD2_Study_Phone_Number__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTD1_Study_Phone_Number__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_DefaultIRBLanguage__c,

                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eDiaryTool__c,
                        VTR5_Create_Subject_in_eCOA__c,
                        VTR4_Is_Activated__c,
                        CreatedDate
                FROM User
                WHERE Id = :parsedParams.userID
        ];
        if (currentUser.VTR3_Verified_Mobile_Devices__c == null) {
            currentUser.VTR3_Verified_Mobile_Devices__c = '';
        }
    }
    /**
     * @description 2FA code is required once per the device. 2FA code is not required during activation and for autotest-users.
     *
     * @param deviceUniqueCode mobile identifier ex. FC9548A4-B2CB-4DF0-8234-DCDB67BA468C
     *
     * @return TRUE if required, otherwise FALSE
     */
    private static Boolean needVerificationCode(String deviceUniqueCode) {
        if (currentUser.First_Log_In__c == null) {
            return false;
        }
        if (currentUser.VTR3_Verified_Mobile_Devices__c.indexOf('AUTOTEST') == -1) {
            if ((deviceUniqueCode != null && currentUser.VTR3_Verified_Mobile_Devices__c.indexOf(deviceUniqueCode) == -1)) {
                return true;
            }
        }
        return false;
    }
    private static Boolean needInitialProfileSetup() {
        return !currentUser.VTD1_Filled_Out_Patient_Profile__c;
    }
    private static Boolean needPrivacyNotices() {
        return currentUser.VTD1_Privacy_Notice_Last_Accepted__c == null || currentUser.VTD1_Privacy_Notice_Last_Accepted__c.year() < Date.today().year() ;
    }
    private static Boolean needAuthorization() {
        return HIPAA_PROFILES.contains(currentUser.Profile.Name) && !currentUser.HIPAA_Accepted__c ;
    }
    /**
     * @description Class-holder for The login Flow response
     */
    global class PatientLoginFlowResponse {
        public String newPassword;
        public String currentStage;
        public String username;
        public List<String> errors = new List<String>();
        public String serverMessage = '';
        public String message;
        public Boolean isVerificationCodeAccepted;
        public Boolean isPrivacyNoticesAccepted;
        public Boolean isAuthorizationAccepted;
        public Boolean isInitialProfileSetupAccepted;
        public PatientLoginFlowArticle privacyNoticesArticle;
        public PatientLoginFlowArticle authorizationArticle;
        public InitialProfileData initialProfileData;

        public String studyPhoneNumber;


        public void setArticle(PatientLoginFlowArticle article, String stage) {
            if (stage.equals(PRIVACY_NOTICES_STAGE)) {
                privacyNoticesArticle = article;
                return;
            }
            if (stage.equals(AUTHORIZATION_STAGE)) {
                authorizationArticle = article;
            }
        }
    }
    public class PatientLoginFlowArticle {
        private String id;
        private String title;
        private String content;
        private String summary;
        public PatientLoginFlowArticle(String id, String title, String content, String summary) {
            this.id = id;
            this.title = title;
            this.content = content;
            this.summary = summary == null ? '' : summary;
        }

        public PatientLoginFlowArticle(Knowledge__kav kav) {
            this.id = kav.Id;
            this.title = kav.Title;
            this.content = kav.VTD1_Content__c;
            this.summary = kav.Summary == null ? '' : kav.Summary;
        }
    }
    private static void sendResponse(Exception e) {
        patientLoginFlowResponse.errors.add(e.getMessage() + ' ' + e.getStackTraceString());
        sendResponse(500);
    }
    private static void sendResponse(Integer code, String error) {
        patientLoginFlowResponse.errors.add(error);
        sendResponse(code);
    }
    private static void sendResponse(Integer code) {
        response.statusCode = code;
        sendResponse();
    }
    private static void sendResponse() {
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(patientLoginFlowResponse, true));
    }

}