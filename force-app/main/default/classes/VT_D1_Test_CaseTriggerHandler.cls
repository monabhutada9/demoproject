/**
 * Mikhail Rzhevsky created by 27.06.2018 -->
 * Reassign Patient Contact Form project IQVIAVT-387
 */
@IsTest
public class VT_D1_Test_CaseTriggerHandler {
    public class MyException extends Exception {
    }//TODO
	public static User createUser(String fName, String lName, String alias, String email,
								      String uName, String nName, Id profile){
    	User	u = new User(
        	FirstName = fName, 
        	LastName = lName,
        	Alias = alias,
        	Email = email,
        	UserName = uName, 
        	CommunityNickname = nName,
        	ProfileId  =  profile
        );  
        insert u;
        return u;		
	}
	
	public static Case createCase(Id conId, Id parentId, Id ownId){
      	List<RecordType> recordTypePCF  = VT_D1_HelperClass.getRecordTypePCF();
      	Id          recTypeId = recordTypePCF[0].Id;
    	Case	c = new Case(
        	RecordTypeId = recTypeId, 
        	Status = 'New',
        	VTD1_PCF_Safety_Concern_Indicator__c = 'No',
        	ContactId = conId,
        	VTD1_Clinical_Study_Membership__c = parentId,
        	OwnerId = ownId
        ); 
        insert c;
        return c;       		
	}
	
	public static Contact createContact(ID rTypeID, String fName, String lName, String preName,String lng, Id accId ){

    	Contact	c = new Contact(
        	RecordTypeId = rTypeID,
        	FirstNAme    = fName, 
        	LastName 	 = lName,
        	HealthCloudGA__PreferredName__c    = preName,
            VTR2_Primary_Language__c = lng,
        	AccountId		= accId
        ); 
        insert c;
        return c;       		
	}	
	

    /*static testMethod void reassignTest(){
    	// Generate data for test:
		HealthCloudGA__CandidatePatient__c	candidatePatient = VT_D1_Test_DataFactory.createTestStudyAndCPRecords();
    	//if(1==1)//TODO
    	//	throw new MyException('1111!!!'+candidatePatient.HealthCloudGA__AccountId__c);//TODO
    	List<Account>	pAccount = [SELECT Id, Name FROM Account WHERE Id =:candidatePatient.HealthCloudGA__AccountId__c];
    	List<Case>		pCase = [SELECT Id, CaseNumber,VTD1_Primary_PG__c,VTD1_Secondary_PG__c,VTD1_PI_contact__c FROM Case WHERE AccountId =:pAccount[0].Id ];
		List<Contact>   pContact = [SELECT Id,  Name FROM Contact WHERE AccountId =:pAccount[0].Id];
 	
    	//Study Concierge can route to:
    	List<Profile> profSC = [SELECT Id, Name, Description  FROM Profile WHERE Name = 'Study Concierge'];
    	User	uSC  = VT_D1_Test_CaseTriggerHandler.createUser('Brandie','Malave','Clow1935','BrandieTMalave@jourrapide.com','Clow1935','Brandie', profSC[0].Id);
    	Case	c1_1 = VT_D1_Test_CaseTriggerHandler.createCase(pContact[0].Id,pCase[0].Id, uSC.Id);
        // to pool of Study Concierges 
        List<Group>  pool = [SELECT Id, DeveloperName,Name,Type FROM Group WHERE Type = 'Queue' AND DeveloperName =:'SC_Queue']; 
        c1_1.OwnerId = pool[0].Id; //Queue SC Queue     
        update c1_1;
        
    	Case	c1_2 = VT_D1_Test_CaseTriggerHandler.createCase(pContact[0].Id,pCase[0].Id, uSC.Id);
        // to Study Concierge in the study’s pool of SCs
        User	uSCQ  = VT_D1_Test_CaseTriggerHandler.createUser('Pedro','Tillman','Theigh','PedroCTillman@jourrapide.com','Theigh','Tillman', profSC[0].Id);
        c1_2.OwnerId = uSCQ.Id;    
        update c1_2;               
        
        
       Case	c1_3 = VT_D1_Test_CaseTriggerHandler.createCase(pContact[0].Id,pCase[0].Id, uSC.Id);
        //to  patient’s Patient Guide
        c1_3.OwnerId = pCase[0].VTD1_Primary_PG__c; 
        update c1_3; 
        
        Case	c1_4 = VT_D1_Test_CaseTriggerHandler.createCase(pContact[0].Id,pCase[0].Id, uSC.Id);           
        // patient’s Backup Patient Guide
        c1_4.OwnerId = pCase[0].VTD1_Secondary_PG__c; 
        update c1_4;
        
         
        //Patient Guide can route to:
        List<Profile> profPG = [SELECT Id, Name, Description  FROM Profile WHERE Name = 'Patient Guide'];
    	User	uPG  = VT_D1_Test_CaseTriggerHandler.createUser('Frank','House','Nempecovest','FrankMHouse@armyspy.com','Nempecovest','House', profPG[0].Id);
        
        Case	c2_1 = VT_D1_Test_CaseTriggerHandler.createCase(pContact[0].Id,pCase[0].Id, uPG.Id);              
        // pool of Study Concierges
        c2_1.OwnerId = pool[0].Id; //Queue SC Queue 
        update c2_1;
        
      	Case	c2_2 = VT_D1_Test_CaseTriggerHandler.createCase(pContact[0].Id,pCase[0].Id, uPG.Id);
        // specific Study Concierge in the study’s pool of SCs
        c2_2.OwnerId = uSCQ.Id;  
        update c2_2;   
        
		Case	c2_3 = VT_D1_Test_CaseTriggerHandler.createCase(pContact[0].Id,pCase[0].Id, uPG.Id); 
        // patient’s Patient Guide
        c2_3.OwnerId = pCase[0].VTD1_Primary_PG__c; 
        update c2_3;
        
		Case	c2_4 = VT_D1_Test_CaseTriggerHandler.createCase(pContact[0].Id,pCase[0].Id, uPG.Id);  
        //  patient’s Primary Investigator
        c2_4.OwnerId = pCase[0].VTD1_PI_contact__c; 
        update c2_4;      
        
        // Primary Investigator can route to:
        List<RecordType> rtPI = [SELECT ID FROM RecordType WHERE DeveloperName =:'PI'];
        List<Profile> profPI = [SELECT Id, Name, Description  FROM Profile WHERE Name = 'Primary Investigator'];
        User	uPI  = VT_D1_Test_CaseTriggerHandler.createUser('Frank','House','Nempecovest','FrankMHouse@armyspy.com','Nempecovest','House', profPI[0].Id);
        Contact conPI = VT_D1_Test_CaseTriggerHandler.createContact(rtPI[0].Id,'Frank','House','Nempecovest','En',pAccount[0].Id);
        Case	c3_1 = VT_D1_Test_CaseTriggerHandler.createCase(pContact[0].Id,pCase[0].Id, uPI.Id); 
        
        //  The study’s pool of Study Concierges
        c3_1.OwnerId = pool[0].Id; //Queue SC Queue
        update c3_1;
        
        Case	c3_2 = VT_D1_Test_CaseTriggerHandler.createCase(pContact[0].Id,pCase[0].Id, uPI.Id);
        // specific Study Concierge in the study’s pool of SCs
        c3_2.OwnerId = uSCQ.Id;  
        update c3_2;
    
    	Case	c3_3 = VT_D1_Test_CaseTriggerHandler.createCase(pContact[0].Id,pCase[0].Id, uPI.Id); 
        // patient’s Patient Guide
        c3_3.OwnerId = pCase[0].VTD1_Primary_PG__c; 
        update c3_3;
        
        Case	c3_4 = VT_D1_Test_CaseTriggerHandler.createCase(pContact[0].Id,pCase[0].Id, uPI.Id);           
        // patient’s Backup Patient Guide
        c3_4.OwnerId = pCase[0].VTD1_Secondary_PG__c; 
        update c3_4;         
                  
        System.Test.stopTest();   
    }*/
    public class CaseStatusChanger implements Queueable {
        public void execute(QueueableContext context) {
            Case caseObj = [SELECT Id, Status FROM Case];
            
            caseObj.VTD1_Eligibility_Status__c = 'Eligible';
            caseObj.VTD2_Baseline_Visit_Complete__c = true;
            caseObj.VTD1_Screening_Complete__c = true;
            update caseObj;
            //System.debug('point 3 ' + Limits.getQueries());
            caseObj.Status = 'Screened';
            update caseObj;
            caseObj.Status = 'Active/Randomized';
            update caseObj;
            caseObj.Status = 'Dropped';
            caseObj.VTD1_Reason__c = 'Death';
            caseObj.VTD1_End_of_study_visit_completed__c = true;
            update caseObj;
        }
    }

    @testSetup
    static void setup() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        study.VTD1_Randomized__c = 'No';
        update study;
        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }

    @isTest
    public static void testManualSharingInheritance() {
        Case caseObj = [SELECT Id FROM Case];
        Case CasePCF = new Case(RecordTypeId = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF);
        Test.startTest();
        insert CasePCF;
        CasePCF.ParentId = caseObj.Id;
        update CasePCF;
        Test.stopTest();
    }

    // TO REFACTOR
/*    @isTest
    public static void testCheckPatientDeletion() {
        User user = [SELECT ContactId FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = TRUE LIMIT 1];
         
        System.runAs(user) {
            //System.debug('point 1 ' + Limits.getQueries());
            Case caseObj = [SELECT Id, Status FROM Case];
        caseObj.Status = 'Consented';
        Test.startTest();
        update caseObj;


        CaseStatusChanger caseStatusChanger = new CaseStatusChanger();
        System.enqueueJob(caseStatusChanger);
        Test.stopTest();
        *//*System.debug('point 4 ' + Limits.getQueries());
        caseObj.Status = 'Active/Randomized';
        update caseObj;
        System.debug('point 5 ' + Limits.getQueries());
        caseObj.Status = 'Dropped';
        caseObj.VTD1_Reason__c = 'Death';
        update caseObj;
        Test.startTest();
        CaseStatusChanger caseStatusChanger = new CaseStatusChanger(caseObj);
        System.enqueueJob(caseStatusChanger);
        Test.stopTest();*//*
    }
    }*/

    /*@isTest
    public static void testValidateEproByCaregiver() {
        Case caseObj = [SELECT Id, VTD1_Patient_User__c, ContactEmail FROM Case];
        List <Account> accounts = [SELECT Id, Name FROM Account WHERE Name = 'CP1 CP1L'];
        *//*for (Account account : accounts) {
            System.debug('acc = ' + account);
        }*//*
        Id accountId = accounts[0].Id;
        String cgUserName = 'CG_' + accounts[0].Name;

        Id rtId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId();

        //System.debug('creating contact for account = ' + accountId);
        String patientEmail = '' + caseObj.ContactEmail;
        String cgEmail = patientEmail.substring(0, patientEmail.length() - 10) + '0' + patientEmail.substring(patientEmail.length() - 10);
        Contact contact = new Contact(FirstName = cgUserName, VTD1_EmailNotificationsFromCourier__c = true,
                LastName = 'L', Email = cgEmail, RecordTypeId = rtId, AccountId = accountId, VTD1_Clinical_Study_Membership__c = caseObj.Id, VTD1_Primary_CG__c = true);
        insert contact;
        //System.debug('contact = ' + contact);

        Profile profile = [SELECT Id FROM Profile WHERE Name = 'Caregiver'];

        User user = new User(
                ProfileId = profile.Id,
                FirstName = cgUserName,
                LastName = 'L',
                Email = cgEmail,
                Username = VT_D1_TestUtils.generateUniqueUserName(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/New_York',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                IsActive = true,
                ContactId = contact.Id
        );
        Test.startTest();
        insert user;
        //System.debug('user = ' + user);

        Case CasePCF = new Case(
                RecordTypeId = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF,
                ParentId = caseObj.Id,
                AccountId = accounts[0].Id,
                VTD1_Patient_User__c = caseObj.VTD1_Patient_User__c,
                VTD1_Clinical_Study_Membership__c = caseObj.Id,
                VTD1_ePro_can_be_completed_by_Caregiver__c = true
        );

        insert CasePCF;
        Test.stopTest();
    }*/
}