/**
 * @author: Alexander Komarov
 * @date: 29.06.2020
 * @description:
 */

@IsTest
private class VT_R5_MobileRestResponseTest {

    @TestSetup
    static void setup() {
        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
                .setFirstName('Patient')
                .setLastName('Patient')
                .setVTR2_Primary_Language('en_US');

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME)
                .setUsername('VT_R5_MobileRestResponseTest@test.com')
                .setContact(patientContact.id);

        patientUser.persist();

        new DomainObjects.VTD1_NotificationC_t()
                .setVTD1_Receivers(patientUser.id)
                .persist();

        new DomainObjects.VTD1_NotificationC_t()
                .setVTD1_Receivers(patientUser.id)
                .persist();

        new DomainObjects.VTD1_NotificationC_t()
                .setVTD1_Receivers(patientUser.id)
                .persist();

    }

    @IsTest
    static void testBehavior() {
        RestContext.response = new RestResponse();
        RestContext.request = new RestRequest();

        VT_R5_MobileRestResponse response = new VT_R5_MobileRestResponse();
        response.buildResponse();
        response.sendResponse();

        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_MobileRestResponseTest@test.com' LIMIT 1];
        System.runAs(toRun) {
            response = new VT_R5_MobileRestResponse();
            response.buildResponse(new SampleDataObject());
        }

        response = new VT_R5_MobileRestResponse();

        response.buildResponseWOAI(new SampleDataObject());
        response.buildResponse(new SampleDataObject(), 'message');
        response.buildResponseWOAI(new SampleDataObject(), 'message');
        response.buildResponse();

        response.showErrorBanner();
        response.showSuccessBanner();
        response.addBannerMessage('banner message');
        response.sendResponse();
        response.sendResponse(500);

        response = new VT_R5_MobileRestResponse();
        response.addError('another error');
        response.sendResponse(400, 'incorrect input');

        response = new VT_R5_MobileRestResponse();
        response.processDatabaseResults(getDatabaseSaveResult());

        Id notificationId = [SELECT Id FROM VTD1_NotificationC__c LIMIT 1].Id;

        response.processDatabaseResults(getDatabaseDeleteResult(notificationId));
        response.processDatabaseResults(getDatabaseDeleteResult(notificationId));
        response.processDatabaseResults(getDatabaseSaveResultWithError());

        response = new VT_R5_MobileRestResponse();
        Exception e = new DmlException();
        e.setMessage('sample message');
        response.buildResponse(e);
    }

    private class SampleDataObject {
        private final String data = 'SAMPLE_DATA';
    }

    private static Database.DeleteResult[] getDatabaseDeleteResult(String incomeId) {
        return Database.delete(new List<VTD1_NotificationC__c>{new VTD1_NotificationC__c(Id=incomeId)}, false);
    }
    private static Database.SaveResult[] getDatabaseSaveResult() {
        VTD1_NotificationC__c[] notesForUpdate = [SELECT Id FROM VTD1_NotificationC__c LIMIT 1];
        notesForUpdate[0].VTD1_Read__c = true;
        return Database.update(notesForUpdate);
    }
    private static Database.SaveResult[] getDatabaseSaveResultWithError() {
        return Database.insert(new List<Account>{
                new Account(Name='TestAcc1'),
                new Account()
        }, false);
    }
}