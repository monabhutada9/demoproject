@IsTest
private with sharing class VT_R5_AllTests5_Part2 {
    //without TestSetup
    @IsTest
    static void VT_D1_TranslationUpsertTest() {
        VT_D1_TranslationUpsertTest.doTest();
    }

    @IsTest
    static void VT_D1_TranslateHelperTest1() {
        VT_D1_TranslateHelperTest.translateTest();
    }

    @IsTest
    static void VT_D1_TranslateHelperTest2() {
        VT_D1_TranslateHelperTest.prepareLabelFieldsTest();
    }

    @IsTest
    static void VT_D1_TranslateHelperTest3() {
        VT_D1_TranslateHelperTest.translateNotificationsFutureTest();
    }

    @IsTest
    static void VT_D1_TranslateHelperTest4() {
        VT_D1_TranslateHelperTest.translateTasksTest();
    }

    @IsTest
    static void VT_D1_TranslateHelperTest5() {
        VT_D1_TranslateHelperTest.getCachedLabelsTest();
    }
    /*@IsTest
    static void VT_D1_Test_CustomNotificationsController1() {
        VT_D1_Test_CustomNotificationsController.testGetNotifications();
    }
    @IsTest
    static void VT_D1_Test_CustomNotificationsController2() {
        VT_D1_Test_CustomNotificationsController.testUpdateAndDeleteNotifications();
    }
    @IsTest
    static void VT_D1_Test_CustomNotificationsController3() {
        VT_D1_Test_CustomNotificationsController.getSessionIdUserIdTest();
    }
    @IsTest
    static void VT_D1_Test_ContentDocumentProcess() {
        VT_D1_Test_ContentDocumentProcess.testDocument();
    }
    @IsTest
    static void VT_D1_Test_ChangeTaskOwnerController1() {
        VT_D1_Test_ChangeTaskOwnerController.testFirstTake();
    }
    @IsTest
    static void VT_D1_Test_ChangeTaskOwnerController2() {
        VT_D1_Test_ChangeTaskOwnerController.testTMA();
    }
    @IsTest
    static void VT_D1_Test_CandidatePatientProcess() {
        VT_D1_Test_CandidatePatientProcess.newCandidatePatient();
    }*/
    @IsTest
    static void VT_D1_SendLinkToUploadFilesCntrlTest1() {
        VT_D1_SendLinkToUploadFilesCntrlTest.getLinkTest();
    }
    @IsTest
    static void VT_D1_SendLinkToUploadFilesCntrlTest2() {
        VT_D1_SendLinkToUploadFilesCntrlTest.sendLinkByEmailTest();
    }
    @IsTest
    static void VT_D1_scheduler_01Test() {
        VT_D1_scheduler_01Test.doTest();
    }
    @IsTest
    static void VT_D1_scheduler_00Test() {
        VT_D1_scheduler_00Test.doTest();
    }
    @IsTest
    static void VT_D1_ResetVideoConfSignalTest() {
        VT_D1_ResetVideoConfSignalTest.doTest();
    }
    @IsTest
    static void VT_D1_ProtocolVisitTriggerTest() {
        VT_D1_ProtocolVisitTriggerTest.deleteTest();
    }
    @IsTest
    static void VT_D1_ProtocolDeviationTriggerTest() {
        VT_D1_ProtocolDeviationTriggerTest.doTest();
    }
    @IsTest
    static void VT_D1_PatientKitsDevAndAccCountingTest1() {
        VT_D1_PatientKitsDevAndAccCountingTest.recountConnectedDevicesTest();
    }
    @IsTest
    static void VT_D1_PatientKitsDevAndAccCountingTest2() {
        VT_D1_PatientKitsDevAndAccCountingTest.recountStudyHubTabletTest();
    }
    @IsTest
    static void VT_D1_NotifySponsorTMATaskResetTest() {
        VT_D1_NotifySponsorTMATaskResetTest.doTest();
    }
    @IsTest
    static void VT_D1_HelperClassTest() {
        VT_D1_HelperClassTest.testBehaviour();
    }
    @IsTest
    static void VT_D1_FeedCommentTriggerTest() {
        VT_D1_FeedCommentTriggerTest.doTest();
    }
    @IsTest
    static void VT_D1_EmailMessageTriggerTest() {
        VT_D1_EmailMessageTriggerTest.doTest();
    }
    @IsTest
    static void VT_D1_AddressTriggerTest() {
        VT_D1_AddressTriggerTest.updateAddressTest();
    }
    @IsTest
    static void VT_D1_CommunityChangePasswordCntrlTest1() {
        VT_D1_CommunityChangePasswordCntrlTest.getUserTest();
    }
    @IsTest
    static void VT_D1_CommunityChangePasswordCntrlTest2() {
        VT_D1_CommunityChangePasswordCntrlTest.changePasswordTest();
    }
    @IsTest
    static void VT_D1_CommunityChangePasswordTest() {
        VT_D1_CommunityChangePasswordTest.getUserSuccessTest();
    }
    @IsTest
    static void VT_D1_ContentDocumentLinkProcessTest1() {
        VT_D1_ContentDocumentLinkProcessTest.doInsertAndDeleteTest();
    }
    @IsTest
    static void VT_D1_ContentDocumentLinkProcessTest2() {
        VT_D1_ContentDocumentLinkProcessTest.testShareVirtualSiteFilesToCustomers();
    }
    //@IsTest
    //static void VT_D1_ContentDocumentLinkProcessTest3() {
    //    VT_D1_ContentDocumentLinkProcessTest.blockUploadForSignMonVisitTaskTest();
    //}
    @IsTest
    static void VT_D1_CoverMiscTriggers_Test() {
        VT_D1_CoverMiscTriggers_Test.doTest();
    }
    @IsTest
    static void VT_D1_CustomMultiPicklistControllerTest1() {
        VT_D1_CustomMultiPicklistControllerTest.getSelectOptionsTest();
    }
    @IsTest
    static void VT_D1_CustomMultiPicklistControllerTest2() {
        VT_D1_CustomMultiPicklistControllerTest.getSelectOptionsWithLabelTest();
    }
    @IsTest
    static void VT_D1_DocumentControllerTest1() {
        VT_D1_DocumentControllerTest.getDocumentsTest();
    }
    @IsTest
    static void VT_D1_DocumentControllerTest2() {
        VT_D1_DocumentControllerTest.getDocumentsSCRTest();
    }
    @IsTest
    static void VT_D1_DocumentControllerTest3() {
        VT_D1_DocumentControllerTest.uploadFileTest();
    }
    @IsTest
    static void VT_D1_DocumentControllerTest4() {
        VT_D1_DocumentControllerTest.uploadFileTest_NoContentDocumentLink();
    }
    @IsTest
    static void VT_D1_DocumentControllerTest5() {
        VT_D1_DocumentControllerTest.standardUploadTest();
    }
    @IsTest
    static void VT_R5_eDiaryScheduledActionsHerokuTest() {
        VT_R5_eDiaryScheduledActionsHerokuTest.testScheduledActionsTest();
    }
    @IsTest
    static void VT_R5_eDiaryScheduledActionsHerokuTest2() {
        VT_R5_eDiaryScheduledActionsHerokuTest.testScheduledActionsExecuteTest();
    }
}