/**
 * Created by user on 20.07.2020.
 */

global with sharing class VT_R4_NewTransactionHelper {
    public class ParamsHolder {
        @InvocableVariable(Label = 'handlerName' Required = true)
        public String handlerName;
        @InvocableVariable(Label = 'parameters' Required = true)
        public String parameters;
    }

    @InvocableMethod
    public static void handle(List <ParamsHolder> params) {
        List <VTR4_New_Transaction__e> events = new List<VTR4_New_Transaction__e>();
        for (ParamsHolder param : params) {
            events.add(new VTR4_New_Transaction__e(VTR4_HandlerName__c = param.handlerName, VTR4_Parameters__c = param.parameters));
        }
        processEvents(events);
    }

    public static void processEvents(List <VTR4_New_Transaction__e> events) {
        Map <String, List <VTR4_New_Transaction__e>> eventsByHandlerMap = new Map<String, List<VTR4_New_Transaction__e>>();
        for (VTR4_New_Transaction__e event : events) {
            List <VTR4_New_Transaction__e> eventsByHandler = eventsByHandlerMap.get(event.VTR4_HandlerName__c);
            if (eventsByHandler == null) {
                eventsByHandler = new List<VTR4_New_Transaction__e>();
                eventsByHandlerMap.put(event.VTR4_HandlerName__c, eventsByHandler);
            }
            eventsByHandler.add(event);
        }
        for (String handlerName : eventsByHandlerMap.keySet()) {
            System.debug('handlerName = ' + handlerName);
            Callable extension = (Callable) Type.forName(handlerName).newInstance();
            extension.call('processEvents', new Map<String, Object> {'events' => eventsByHandlerMap.get(handlerName)});
        }
    }
}