@isTest
public with sharing class VT_R3_RestPatientEdiaryTest {
	static VT_R3_RestHelper helper = new VT_R3_RestHelper();
	@TestSetup
	static void setup(){
		Test.startTest();
		HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
		VT_D1_TestUtils.createTestPatientCandidate(1);
		Test.stopTest();
	}
	static Id eProRecTypeId = Schema.SObjectType.VTD1_Protocol_ePRO__c.getRecordTypeInfosByDeveloperName().get('ePRO').getRecordTypeId();
	static Id eProRecTypeId2 = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('ePRO').getRecordTypeId();

	@isTest
	public static void getEdiariestest() {
		String responseString;
		Map<String, Object>responseMap = new Map<String, Object>();
		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();
		request.requestURI = '/Patient/Ediaries/';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response  = response;

		Case carePlan = [
				SELECT Id, VTD1_Primary_PG__c, VTD1_Secondary_PG__c, VTR2_SiteCoordinator__c, VTR2_Backup_Site_Coordinator__c, VTD1_Study__c
				FROM Case
				WHERE RecordType.DeveloperName = 'CarePlan'
		];

		VTD1_Protocol_ePRO__c protocolEPRO = new VTD1_Protocol_ePRO__c(
				RecordTypeId = eProRecTypeId,
				VTD1_Study__c = carePlan.VTD1_Study__c,
				VTR2_Protocol_Reviewer__c = 'Patient Guide',
				VTD1_Subject__c = 'Patient',
				VTD1_Response_Window__c = 10
		);
		insert protocolEPRO;

		VTD1_Survey__c survey = new VTD1_Survey__c(RecordTypeId = eProRecTypeId2, VTR2_DocLanguage__c = 'en_US', VTD1_Protocol_ePRO__c = protocolEPRO.Id,	VTD1_CSM__c = carePlan.Id, VTD1_Status__c = 'In Progress', VTD1_Date_Available__c = Datetime.now()-1, VTD1_Due_Date__c = Datetime.now()+10, Name = 'samplename');
		insert survey;

		User user = [SELECT Id FROM User WHERE Profile.Name = 'Patient' AND IsActive = true AND Name = 'CP1 CP1L' LIMIT 1];

		System.runAs(user){
			responseString = VT_R3_RestPatientEdiary.getEdiaries();
			System.debug('HERE! '+responseString);
		}

		survey.VTD1_Date_Available__c = DateTime.now()-1;

		survey.VTD1_Due_Date__c = DateTime.now() + 1;
		survey.VTD1_Status__c = 'Not Started';
		update survey;
		System.runAs(user){
			responseString = VT_R3_RestPatientEdiary.getEdiaries();
			System.assert(!String.isEmpty(responseString));
		}

		RestContext.request.requestURI = '/Patient/Ediaries/0051N000005cleYQAQ'; //invalid id
		responseString = VT_R3_RestPatientEdiary.getEdiaries();
		responseMap = (Map<String, Object>)JSON.deserializeUntyped(responseString);
		System.assertEquals(helper.forAnswerForIncorrectInput(), responseMap.get('serviceResponse'));

		RestContext.request.requestURI = '/Patient/Ediaries/' + survey.Id;
		responseString = VT_R3_RestPatientEdiary.getEdiaries();
	}

	@isTest
	public static void postAnswersTest() {
		String responseString;
		Map<String,Object> responseMap = new Map<String, Object>();
		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueof('[{}]');
		RestContext.request = request;
		RestContext.response  = response;
		Case carePlan = [
				SELECT Id, VTD1_Primary_PG__c, VTD1_Secondary_PG__c, VTR2_SiteCoordinator__c, VTR2_Backup_Site_Coordinator__c, VTD1_Study__c
				FROM Case
				WHERE RecordType.DeveloperName = 'CarePlan'
		];

		VTD1_Survey__c survey = new VTD1_Survey__c(RecordTypeId = eProRecTypeId2, VTD1_Status__c = 'Not Started',VTD1_CSM__c = carePlan.Id);
		insert survey;
		request.requestURI = '/Patient/Ediaries/' + survey.Id;
		responseString = VT_R3_RestPatientEdiary.postAnswers();

		VTD1_Protocol_ePRO__c protocolEPRO = new VTD1_Protocol_ePRO__c(
				RecordTypeId = eProRecTypeId,
				VTD1_Study__c = carePlan.VTD1_Study__c,
				VTR2_Protocol_Reviewer__c = 'Patient Guide',
				VTD1_Subject__c = 'Patient',
				VTD1_Response_Window__c = 10
		);
		insert protocolEPRO;
		survey.VTD1_Protocol_ePRO__c = protocolEPRO.Id;
		survey.VTR2_DocLanguage__c = 'en_US';
		update survey;

		VTD1_Protocol_ePro_Question__c question = new VTD1_Protocol_ePro_Question__c();
		question.VTD1_Protocol_ePRO__c = protocolEPRO.Id;
		question.VTD1_Type__c = 'Short Text';
		insert question;

		VT_D1_SurveyAnswerCreator.createAnswers(new List<VTD1_Survey__c>{survey});
		List<VT_R3_RestPatientEdiary.CustomAnswerWrapper> customAnswerWarperrList = new List<VT_R3_RestPatientEdiary.CustomAnswerWrapper>();
		VT_R3_RestPatientEdiary.CustomAnswerWrapper customAnswerWarperr = new VT_R3_RestPatientEdiary.CustomAnswerWrapper();
		customAnswerWarperrList.add(customAnswerWarperr);

		request.requestURI = '/Patient/Ediaries/' + survey.Id;
		RestContext.request.params.put('complete','false');
		responseString = VT_R3_RestPatientEdiary.postAnswers();

		request.requestURI = '/Patient/Ediaries/Reset/' + survey.Id;
		responseString = VT_R3_RestPatientEdiary.resetEdiary();

		VTD1_Survey__c survey2 = new VTD1_Survey__c(RecordTypeId = eProRecTypeId2, VTD1_Status__c = 'Not Started',	VTD1_CSM__c = carePlan.Id);
		insert survey2;
		VTD1_Survey_Answer__c surveyAnswer2 = new VTD1_Survey_Answer__c();
		surveyAnswer2.VTD1_Survey__c = survey2.Id;
		surveyAnswer2.VTD1_Question__c = question.Id;
		insert surveyAnswer2;

		request.requestURI = '/Patient/Ediaries/' + survey.Id;
		customAnswerWarperrList[0].answerId = surveyAnswer2.Id;
		RestContext.request.requestBody = Blob.valueof(JSON.serialize(customAnswerWarperrList));
		responseString = VT_R3_RestPatientEdiary.postAnswers();
		responseMap = (Map<String, Object>)JSON.deserializeUntyped(responseString);
		System.assertEquals('FAILURE. Some answers not belong to this survey.', responseMap.get('serviceMessage'));

		RestContext.request.params.put('complete','true');
		request.requestURI = '/Patient/Ediaries/' + survey.Id;
		System.debug('survey ID *** ' + survey.Id);
		RestContext.request.requestBody = Blob.valueof(JSON.serialize(customAnswerWarperrList));
		responseString = VT_R3_RestPatientEdiary.postAnswers();
	}
}