public without sharing class VT_D1_ePROSchedulingHelper {

    public static Set<Id> getReceivers(Id surveyId) {
    VTD1_Survey__c survey = [
            SELECT  VTD1_Patient_User_Id__c,
                VTD1_Caregiver_User_Id__c,
                VTD1_Protocol_ePRO__r.VTD1_Subject__c,
                VTD1_Protocol_ePRO__r.VTD1_Caregiver_on_behalf_of_Patient__c,
                VTD1_CSM__r.VTD1_ePro_can_be_completed_by_Caregiver__c
            FROM VTD1_Survey__c
            WHERE Id = :surveyId
        ];

        Set<Id> receivers = new Set<Id>();
        if (toAddPatient(survey)) {
            receivers.add(survey.VTD1_Patient_User_Id__c);
        } else if (toAddCaregiver(survey)) {
            receivers.add(survey.VTD1_Caregiver_User_Id__c);
        }

        return receivers;
    }

    private static Boolean toAddPatient(VTD1_Survey__c survey) {
        return
            survey.VTD1_Patient_User_Id__c != null &&
            survey.VTD1_Protocol_ePRO__r.VTD1_Subject__c != 'Caregiver'
        ;
    }

    private static Boolean toAddCaregiver(VTD1_Survey__c survey) {
        return
            survey.VTD1_Caregiver_User_Id__c != null && (
                survey.VTD1_Protocol_ePRO__r.VTD1_Subject__c == 'Caregiver' || (
                    survey.VTD1_Protocol_ePRO__r.VTD1_Caregiver_on_behalf_of_Patient__c &&
                    survey.VTD1_CSM__r.VTD1_ePro_can_be_completed_by_Caregiver__c
                )
            )
        ;
    }
}