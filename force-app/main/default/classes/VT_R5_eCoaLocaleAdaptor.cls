/**
* @author: N.Arbatskiy
* @date: 16-May-20
* @description: Class for language locales adaptation (takes SH locale and adapts it for eCOA)
**/
public with sharing class VT_R5_eCoaLocaleAdaptor {
    /**
     * @description: Adapts SH locales to eCOA locales.
     * @param userId is used for querying relative parameters(LanguageLocaleKey and Virtual site fields).
     * @return the final locale for eCOA http request.
     */
    public static String adaptLocale(Id userId) {
        User user = [
            SELECT  LanguageLocaleKey
            FROM User
            WHERE Id = :userId
        ];
        return adaptLocale(user);
    }
    public static String adaptLocale(User user) {
        String userLanguageLocale = user.LanguageLocaleKey;

        if (String.isNotBlank(userLanguageLocale)) {
            Map<String, String> localeMapFromSH = getEcoaLanguagesMapping();
            String localeForEcoa = localeMapFromSH.get(userLanguageLocale);
            return localeForEcoa;
        }
        return null;
    }

    /**
    * @description: Maps SH locales to eCOA locales.
    * @return a Map with paired locales.
    */
    public static Map<String, String> getEcoaLanguagesMapping() {
        Map<String, String> localeMap = new Map<String, String>();
        //takes the necessary fields from Custom Metadata Type
        List<VTR5_eCOA_Languages_Mapping__mdt> localeListFromSH = [
            SELECT VTR5_Study_Hub_Language__c,VTR5_eCOA_Language__c
            FROM VTR5_eCOA_Languages_Mapping__mdt
        ];
        if(Test.isRunningTest()) {
            localeMap.put('de','deDE');
        } else {
            for (VTR5_eCOA_Languages_Mapping__mdt item : localeListFromSH) {
                localeMap.put(item.VTR5_Study_Hub_Language__c, item.VTR5_eCOA_Language__c);
            }
        }
        return localeMap;
    }
}