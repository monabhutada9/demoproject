/**
 * Created by Yuliya Yakushenkova on 10/19/2020.
 */

@IsTest
public class VT_R5_MobilePatientProfilePhotoTest {

    private static final String URI = '/patient/v1/profile/photo';
    static {
        new VT_R5_MobilePatientProfilePhoto().getMinimumVersions();
    }

    public static void firstTest() {
        User usr = fillRestContexts();

        String responseBody  = VT_R5_TestRestContext.doTest(
                usr, new VT_R5_TestRestContext.MockRestContext(URI, null));
        System.debug(responseBody);
    }

    public static void secondTest() {
        User usr = fillRestContexts();

        String responseBody  = VT_R5_TestRestContext.doTest(usr, new VT_R5_TestRestContext.MockRestContext(
                'POST',
                URI,
                new Map<String, String>{'isPatient' => 'false'},
                new RequestParams()
        ));
        System.debug(responseBody);
    }

    public static User fillRestContexts() {
        User usr = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.PT_USERNAME LIMIT 1];

        return usr;
    }

    private class RequestParams {
        private String base64Photo;

        private RequestParams() {
            this.base64Photo = EncodingUtil.base64Encode(Blob.valueOf('salesforce sss'));
        }
    }
}