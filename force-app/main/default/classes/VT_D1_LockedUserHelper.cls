/**
* @author: Carl Judge
* @date: 15-Oct-18
* @description: 
**/

public without sharing class VT_D1_LockedUserHelper {

    public static Integer checkForLockedUser(String username) {
        User u;
        for (User item : [SELECT Id, ContactId, ProfileId, LastPasswordChangeDate, IsActive FROM User WHERE Username = :username]) {
            u = item;
        }

        if (u != null) {
            UserLogin uLogin;
            for (UserLogin item : [SELECT IsPasswordLocked, IsFrozen FROM UserLogin WHERE UserId = :u.Id]) {
                uLogin = item;
            }
            if (uLogin != null) {
                if (uLogin.IsFrozen || !u.IsActive) {
                    return 0;
                } else if (uLogin.IsPasswordLocked && !userWasLocked(u.Id)) {
                    sendAdminEmail(u.ContactId);
                    uLogin.IsFrozen = true;
                    update uLogin;
                    return 0;
                } else {
                    return loginAttemtsRemaining(u);
                }
            }
        }
        return null;
    }

    public static Boolean userWasLocked(Id userId) {
        List<LoginHistory> lHistories = [
                SELECT Status
                FROM LoginHistory
                WHERE UserId = :userId
                ORDER BY LoginTime DESC
                LIMIT 1
        ];

        return ! lHistories.isEmpty() && lHistories[0].Status == 'Password Lockout';
    }

    public static Integer loginAttemtsRemaining(User u) {
        Integer maxFailedLoginAttempts = (Integer)VTD1_RTId__c.getOrgDefaults().VTR3_MaxFailedLoginAttempts__c;
        if (maxFailedLoginAttempts == null) {
            maxFailedLoginAttempts = 5;
        }
//        Integer maxFailedLoginAttempts = 10;
        Integer currentLoginAttempts = 0;
        Integer loginAttemptsRemaining;

        List<LoginHistory> lHistories = [
                SELECT Status
                FROM LoginHistory
                WHERE UserId = :u.Id
                AND LoginTime >: u.LastPasswordChangeDate
                ORDER BY LoginTime DESC
                LIMIT 1000
        ];
        for (LoginHistory lHistory : lHistories) {
            if (lHistory.Status == 'Success' || lHistory.Status == 'Password Lockout') {
                break;
            } else if (lHistory.Status == 'Invalid Password') {
                currentLoginAttempts++;
            }
        }

        loginAttemptsRemaining = maxFailedLoginAttempts - Math.Mod(currentLoginAttempts, maxFailedLoginAttempts);

        if (loginAttemptsRemaining < 4) {
            return loginAttemptsRemaining;
        } else {
            return null;
        }
    }

    public static void sendAdminEmail(Id contactId) {
        List<OrgWideEmailAddress> senderView = [select id from OrgWideEmailAddress];
        Id templateId;
        for (EmailTemplate item : [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'VTD1_Patient_Account_Locked']) {
            templateId = item.Id;
        }
        List <OrgWideEmailAddress> owEmailAddresses = [select Id from OrgWideEmailAddress order by CreatedDate limit 1];
        if (templateId != null) {
            Set<String> addresses = new Set<String>();
            for (ApexEmailNotification item : [SELECT User.Email, Email FROM ApexEmailNotification]) {
                if (item.User.Email != null) {
                    addresses.add(item.User.Email);
                }
                if (item.Email != null) {
                    addresses.add(item.Email);
                }
            }

//            addresses.add('carl.judge@gmail.com');
//            addresses.add('mikhail.salamakha@ctconsult.ru');

            if (!addresses.isEmpty()) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(contactId);
                mail.setTreatTargetObjectAsRecipient(false);
                mail.setUseSignature(false);
                if (!senderView.isEmpty()) mail.setOrgWideEmailAddressId(senderView.get(0).id);
                mail.setBccSender(false);
                mail.setSaveAsActivity(false);
                mail.setTemplateId(templateId);
                mail.setToAddresses(new List<String>(addresses));
                if (!owEmailAddresses.isEmpty())
                    mail.setOrgWideEmailAddressId(owEmailAddresses[0].Id);
                List<Messaging.SendEmailResult> result = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
                        mail
                });
                system.debug(result[0].isSuccess());
                system.debug(result[0].getErrors());
            }
        }
    }
}