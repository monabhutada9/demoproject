/**
 * Created by user on 18.04.2019.
 */

public with sharing class VT_TableHelper {

    public class TableQuerySettings {
        public String queryFilter;
        public String queryOrder;
        public String queryLimit;
        public String queryOffset;
        public Integer entriesOnPage;
        public List<String> studiesFromFilter; // Needed for "Additional Files" Table. Becuase Studies info not stored in ContentDocumentLink
    }

    private class QuerySettings {
        private Integer currentPage { get; set; }
        private Integer entriesOnPage { get; set; }
        private List<FilterParams> filterParams { get; set; }
        private SortingParams sortingParams { get; set; }
        private SearchParams searchParams { get; set; }

        private QuerySettings(Integer currentPage, Integer entriesOnPage, List<FilterParams> filterParams, SortingParams sortingParams, SearchParams searchParams) {
            this.currentPage = currentPage;
            this.entriesOnPage = entriesOnPage;
            this.filterParams = filterParams;
            this.sortingParams = sortingParams;
            this.searchParams = searchParams;
        }
    }

    private class SearchParams {
        private String field { get; set; }
        private String searchString { get; set; }

        private SearchParams(String field, String searchString) {
            this.field = field;
            this.searchString = searchString;
        }
    }

    private class SortingParams {
        private String field { get; set; }
        private String order { get; set; }

        private SortingParams(String field, String order) {
            this.field = field;
            this.order = order;
        }
    }

    private class FilterParams {
        private String field { get; set; }
        private List<String> filterValue { get; set; }

        private FilterParams(String field, String[] filterValue) {
            this.field = field;
            this.filterValue = filterValue;
        }
    }

    public static TableQuerySettings parseQuerySettings (String querySettingsString) {
//        String a = '{"sortingParams":{"field":"ISF","order":"DESC"},"filterParams":[{"field":"RegulatoryDocumentType","filterValue":["option1", "option3"]},{"field":"DocuSignStatus","filterValue":["option1"]},{"field":"DocuSignStatus","filterValue":null}],"entriesOnPage":10,"currentPage":2}';
        TableQuerySettings query = new TableQuerySettings();
        QuerySettings querySettings = (QuerySettings)JSON.deserialize(querySettingsString, QuerySettings.class);

        query.queryFilter = parseFilterParams(querySettings.filterParams, querySettings.searchParams);
        query.queryOrder = parseSortingParams(querySettings.sortingParams);
        query.queryLimit = parseLimit(querySettings.entriesOnPage);
        query.queryOffset = parseOffset(querySettings.currentPage, querySettings.entriesOnPage);
        query.entriesOnPage = querySettings.entriesOnPage;
        query.studiesFromFilter = getStudies(querySettings.filterParams);

        return query;
    }

    private static String parseSortingParams (SortingParams params) {
        String sortingString = null;

        if (params != null) {
            sortingString = params.field + ' ' + params.order + ' ';
        }

        return sortingString;
    }

    private static String parseFilterParams (List<FilterParams> params, SearchParams searchParams) {
        String filterString = null;

        if (params != null) {
            filterString = '';

            for (Integer i = 0; i < params.size(); i++) {
                if (params[i].filterValue != null && !String.isBlank(params[i].field)) {
                    if (filterString != '' && params[i].filterValue.size() != 0 && !params[i].field.equals('Study')) {
                        filterString = filterString + 'AND ';
                    }

                    // DateTime has another SOQL syntax
                    // Bacause date in format yyyy-mm-dd comes form Aura - change format to (CreatedDate >= someDate AND CreatedDate < oneDayAfter) OR (... AND ...) OR  ...
                    if (params[i].field.contains('.CreatedDate')) {
                        List<String> createdDates = new List<String>();
                        DateTime beginDate;
                        DateTime oneDayAfter;
                        for (String createdDate : params[i].filterValue) {
                            beginDate = (Datetime) Date.valueOf(createdDate);
                            oneDayAfter = beginDate.addDays(1);
                            createdDates.add(
                                    '( ' + params[i].field + ' >= ' + beginDate.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') + ' AND ' +
                                            params[i].field + ' < ' + oneDayAfter.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') + ' )');
                        }

                        filterString = filterString + ' ( ' + String.join(createdDates, ' OR ') + ') ';
                    } else if (params[i].field.equals('Study')) { // Study will be handled manually
                        continue;
                    } else {
                        filterString = filterString + params[i].field + ' IN (\'' + String.join( params[i].filterValue, '\', \'' ) + '\') ';
                    }


                        /*if (j == 0 && params[i].filterValue.size() > 1) {
                            filterString = filterString + '(';
                        }

                        filterString = filterString + params[i].field + ' = \'' + params[i].filterValue[j] + '\'';

                        if (j < params[i].filterValue.size() - 1) {
                            filterString = filterString + ' OR ';
                        } else if (j == params[i].filterValue.size() - 1 && params[i].filterValue.size() > 1) {
                            filterString = filterString + ') ';
                        } else {
                            filterString = filterString + ' ';
                        }*/
//                    }
                }
            }
        }

        if (searchParams != null && searchParams.searchString != null) {
            if (filterString != null && filterString != '') {
                filterString = filterString + 'AND ';
            }
            filterString = filterString + searchParams.field + ' LIKE \'%' + searchParams.searchString + '%\' ';
        }
        return filterString;
    }

    private static String parseOffset (Integer currentPage, Integer entriesOnPage) {
        String offsetString = null;
        Integer offset = (currentPage * entriesOnPage) - entriesOnPage;

        if (offset > 0) {
            offsetString = 'OFFSET ' + offset + ' ';
        }

        return offsetString;
    }

    private static String parseLimit (Integer entriesOnPage) {
        String limitString = null;
        limitString = 'LIMIT ' + entriesOnPage + ' ';
        return limitString;
    }

    private static List<String> getStudies(List<FilterParams> params) {
        for (FilterParams param : params) {
            if (!String.isBlank(param.field) && param.field.equals('Study') && !param.filterValue.isEmpty()) {
                return param.filterValue;
            }
        }
        return null;
    }

}