/**
 * Created by User on 19/05/31.
 */
@isTest
public with sharing class VT_D1_TranslationUpsertTest {

    public static void doTest(){
        Account acc = new Account(Name='Test');
        insert acc;
        DomainObjects.VTD1_Translation_t transl1 = new DomainObjects.VTD1_Translation_t()
                .setVTD1_Record_Id(acc.Id)
                .setVTD1_Field_Name(null)
                .setVTD1_Language('es')
                .setVTD1_Object_Name('Account')
                .setVTD1_Value('accSpanish');
        DomainObjects.VTD1_Translation_t transl2 = new DomainObjects.VTD1_Translation_t()
                .setVTD1_Record_Id(acc.Id)
                .setVTD1_Field_Name('Name')
                .setVTD1_Language('es')
                .setVTD1_Object_Name('Account')
                .setVTD1_Value('accSpanish');
        DomainObjects.VTD1_Translation_t transl3 = new DomainObjects.VTD1_Translation_t()
                .setVTD1_Record_Id(acc.Id)
                .setVTD1_Field_Name('LastModifiedDate')
                .setVTD1_Language('es')
                .setVTD1_Object_Name('Account')
                .setVTD1_Value('accSpanish');
        DomainObjects.VTD1_Translation_t transl4 = new DomainObjects.VTD1_Translation_t()
                .setVTD1_Record_Id(acc.Id)
                .setVTD1_Field_Name('VTD1_Age__c')
                .setVTD1_Language('es')
                .setVTD1_Object_Name('Contact')
                .setVTD1_Value('accSpanish');
        DomainObjects.VTD1_Translation_t transl5 = new DomainObjects.VTD1_Translation_t()
                .setVTD1_Record_Id('sdgddfhfgjfgj')
                .setVTD1_Field_Name('Name')
                .setVTD1_Language('es')
                .setVTD1_Object_Name('Account')
                .setVTD1_Value('accSpanish');

        Test.startTest();
        Boolean hasException1 = false;
        Boolean hasException2 = false;
        Boolean hasException3 = false;
        Boolean hasException4 = false;
        Boolean hasException5 = false;

        try{
            transl1.persist();
        }catch (Exception ex){
            hasException1 = true;
        }
        try{
            transl2.persist();
        }catch (Exception ex){
            hasException2 = true;
        }
        try{
            transl3.persist();
        }catch (Exception ex){
            hasException3 = true;
        }
        try{
            transl4.persist();
        }catch (Exception ex){
            hasException4 = true;
        }
        try{
            transl5.persist();
        }catch (Exception ex){
            hasException5 = true;
        }

        System.assert(hasException1);
        System.assert(hasException2);
        System.assert(hasException3);
        System.assert(hasException4);
        System.assert(hasException5);
        Test.stopTest();
    }
}