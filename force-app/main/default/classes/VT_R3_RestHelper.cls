/**
 * Created by Alexander Komarov on 18.04.2019.
 */

public with sharing class VT_R3_RestHelper {
    private static final Set<System.StatusCode> CLIENT_ERROR_CODES = new Set<System.StatusCode>{
            System.StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION,
            System.StatusCode.REQUIRED_FIELD_MISSING,
            System.StatusCode.CANNOT_EXECUTE_FLOW_TRIGGER,
            System.StatusCode.STRING_TOO_LONG
    };

    private static final Map<String, String> FIELDS_MAPPING = new Map<String, String>{
            'VTD1_Address2__c' => 'addressLineTwo',
            'Name' => 'addressLineOne',
            'calendar' => 'Calendar',
            'my-diary' => 'Ediary',
            'regulatory-binder' => 'RegulatoryBinder'
    };

    public static final Map<String, String> FILE_CATEGORIES_MAPPING = new Map<String, String>{
            'aif' => 'Audio',
            'cda' => 'Audio',
            'mid' => 'Audio',
            'midi' => 'Audio',
            'mp3' => 'Audio',
            'mpa' => 'Audio',
            'ogg' => 'Audio',
            'wav' => 'Audio',
            'wma' => 'Audio',
            'wpl' => 'Audio',
            'csv' => 'Document',
            'dat' => 'Document',
            'ods' => 'Document',
            'xlr' => 'Document',
            'xls' => 'Document',
            'xlsx' => 'Document',
            'doc' => 'Document',
            'odt' => 'Document',
            'pdf' => 'Document',
            'rtf' => 'Document',
            'tex' => 'Document',
            'txt' => 'Document',
            'wks' => 'Document',
            'wps' => 'Document',
            'wpd' => 'Document',
            'ai' => 'Image',
            'bmp' => 'Image',
            'gif' => 'Image',
            'ico' => 'Image',
            'jpeg' => 'Image',
            'jpg' => 'Image',
            'png' => 'Image',
            'ps' => 'Image',
            'psd' => 'Image',
            'svg' => 'Image',
            'tif' => 'Image',
            'tiff' => 'Image',
            '3g2' => 'Video',
            '3gp' => 'Video',
            'avi' => 'Video',
            'flv' => 'Video',
            'h264' => 'Video',
            'm4v' => 'Video',
            'mkv' => 'Video',
            'mov' => 'Video',
            'mp4' => 'Video',
            'mpg' => 'Video',
            'mpeg' => 'Video',
            'rm' => 'Video',
            'swf' => 'Video',
            'vob' => 'Video',
            'wmv' => 'Video'
    };

    static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());

    public Boolean checkForTypes(DmlException dmlE) {
        Integer numErrors = dmlE.getNumDml();
        for (Integer i = 0; i < numErrors; i++) {
            StatusCode currentCode = dmlE.getDmlType(i);
            if (CLIENT_ERROR_CODES.contains(currentCode)) {
                return true;
            }
        }
        return false;
    }

    public String formAnswerForDMLError(DmlException dmlE, Boolean bool) { // Boolean true for 4xx (Client) and false for 5xx (Server) errors
        Integer numErrors = dmlE.getNumDml();
        String result = bool ? 'FAILURE. Client error: ' : 'FAILURE. Server error: ';
        result += dmlE.getStackTraceString() + '; ';
        for (Integer i = 0; i < numErrors; i++) {
            result += '[' + dmlE.getDmlType(i) + '] ';
            result += 'Fields: ' + dmlE.getDmlFieldNames(i) + '; ';
            result += dmlE.getDmlMessage(i) + '; ';
        }
        return result;
    }

    public Boolean isIdCorrectType(List<String> idsList, String type) {
        Id correctId;
        SObjectType sot;
        String s = '';
        for (String iterateString : idsList) {
            try {
                correctId = iterateString;
                sot = correctId.getSobjectType();
                s = String.valueOf(sot);
                if (!s.equals(type)) return false;
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }

    public Boolean isIdCorrectType(String id, String type) {
        try {
            Id i = id;
            SObjectType soType = i.getSobjectType();
        } catch (Exception e) {
            return false;
        }
        Id correctId = id;
        SObjectType sot = correctId.getSobjectType();
        String s = String.valueOf(sot);
        return s.equals(type);
    }

    public String forAnswerForIncorrectInput() {
        return 'INCORRECT INPUT';
    }

    public String forAnswerForIncorrectInput(String s) {
        return 'INCORRECT INPUT: ' + s;
    }

    public class CustomResponse {
        public Object serviceResponse;
        public String serviceMessage;
        public AdditionalInfo additionalInfo;

        public CustomResponse buildResponse(Object response) {
            this.serviceResponse = response;
            this.additionalInfo = new AdditionalInfo();
            return this;
        }


        public CustomResponse buildResponse(Object response, String message) {
            this.serviceResponse = response;
            this.serviceMessage = message;
            this.additionalInfo = new AdditionalInfo();
            return this;
        }
    }

    public class AdditionalInfo {
        public Integer unreadConversationsCount;
        public Integer unreadNotificationsCount;
        public String userLanguage;
        public String userSecondaryLanguage;
        public String userTertiaryLanguage;

        AdditionalInfo() {
            List<Contact> contacts = [SELECT Id, VTR2_Primary_Language__c, VT_R5_Tertiary_Preferred_Language__c, VT_R5_Secondary_Preferred_Language__c FROM Contact WHERE Id IN (SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId())];
            if (!contacts.isEmpty()) {
                this.userLanguage = contacts[0].VTR2_Primary_Language__c;
                this.userSecondaryLanguage = contacts[0].VT_R5_Secondary_Preferred_Language__c;
                this.userTertiaryLanguage = contacts[0].VT_R5_Tertiary_Preferred_Language__c;
            }
            this.unreadConversationsCount = VT_R3_RestPeriodic.getUnreadConversationsCount();
            this.unreadNotificationsCount = VT_R3_RestPeriodic.getUnreadNotificationsCount();
        }
    }

    public static Datetime getLastLoginDatetime() {
        Boolean gotLastLogin = false;
        Datetime previousSuccessUserLoginTime;
        for (LoginHistory loginHistory : [SELECT Id, LoginTime, UserId, Status, Application FROM LoginHistory WHERE UserId = :UserInfo.getUserId() ORDER BY LoginTime DESC LIMIT 20]) {
            if (loginHistory.Status != 'Success' || loginHistory.Application != 'Browser') continue;
            if (!gotLastLogin) {
                gotLastLogin = true;
                continue;
            } else {
                previousSuccessUserLoginTime = loginHistory.LoginTime;
                break;
            }
        }
        if (previousSuccessUserLoginTime == null) {
            previousSuccessUserLoginTime = [SELECT Id, CreatedDate FROM User WHERE Id = :UserInfo.getUserId()].CreatedDate;
        }
        return previousSuccessUserLoginTime;
    }



    public void addPreviewsWithBatchCallout(List<VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper> income) {
        String contentDocumentsIds = '';
        Integer docsCount = 0;
        for (VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper mdw : income) {
            if (String.isNotBlank(mdw.contentDocumentId)) {
                contentDocumentsIds += mdw.contentDocumentId + ',';
                docsCount++;
            }
        }
        contentDocumentsIds = contentDocumentsIds.removeEnd(',');
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setHeader('Authorization', 'Bearer ' + VT_D1_HelperClass.getSessionIdForGuest());
        req.setEndpoint(Url.getOrgDomainUrl().toExternalForm() + '/services/data/v45.0/connect/communities/' + Network.getNetworkId() + '/files/batch/' + contentDocumentsIds);
        ///services/data/v46.0/connect/communities/0DB1N0000007FQmWAM/files/batch/06922000000QUDHAA4,06922000000QU82AAG,06922000000QU7xAAG,06922000000QS7lAAG,06922000000QS7gAAG
        System.debug('REQUEST: ' + req);
        // Send Tooling API request
        Http http = new Http();
        HttpResponse res = http.send(req);
        System.debug('RESPONSE: ' + res.getStatus() + ' ' + res.getBody());

        if (res.getStatusCode() == 200) {
        Map<Id, String> contentDocumentIdToLink = new Map<Id, String>();


        for (Integer i = 0; i < docsCount; i++) {
            Map<String, Object> m = (Map<String, Object>) ((Map<String, Object>) ((List<Object>) ((Map<String, Object>) JSON.deserializeUntyped(res.getBody())).get('results')).get(i)).get('result');
            String link = String.valueOf(m.get('renditionUrl'));
            String currentContentDocumentId = String.valueOf(m.get('id'));
            contentDocumentIdToLink.put(currentContentDocumentId, link);
        }
        for (VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper mdw : income) {
            if (String.isNotBlank(mdw.contentDocumentId) && contentDocumentIdToLink.get(mdw.contentDocumentId) != null) {
                mdw.filePreviewBase64 = getBase64Preview2(Url.getOrgDomainUrl().toExternalForm() + contentDocumentIdToLink.get(mdw.contentDocumentId));
                mdw.filePreviewStatus = 'Available';
            } else {
                mdw.filePreviewStatus = 'NotAvailable';
                mdw.filePreviewBase64 = '';
            }
        }
        } else {
            for (VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper mdw : income) {
                mdw.filePreviewStatus = 'NotAvailable';
                mdw.filePreviewBase64 = '';
            }
        }


    }

    public String getBase64Preview2(String url) {
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
        req.setEndpoint(url);
        Http http = new Http();
        HttpResponse res = http.send(req);
        return EncodingUtil.base64Encode(res.getBodyAsBlob());
    }

    public class Unread {
        public Set<String> newPersonalChats;
        public Set<String> newBroadcasts;

        public Unread() {

            String unreadFeedsString = [SELECT VTR3_UnreadFeeds__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].VTR3_UnreadFeeds__c;
            Set<String> unreadFeedItemsIds = new Set<String>();
            if (unreadFeedsString != null && unreadFeedsString.length() > 3) {
                unreadFeedItemsIds.addAll(unreadFeedsString.split(';'));
            }

            System.debug('unreadFeedItemsIds ' + unreadFeedItemsIds);
            System.debug('unreadFeedItemsIds.size ' + unreadFeedItemsIds.size());

            Set<String> unreadMessagesIds = new Set<String>();
            for (AggregateResult ar :[SELECT COUNT(Id),VTD2_Feed_Element__c FROM VTD2_Feed_Post_on_PCF__c WHERE VTD2_Feed_Element__c IN :unreadFeedItemsIds GROUP BY VTD2_Feed_Element__c]) {
                Id currentId = (Id)ar.get('VTD2_Feed_Element__c');
                unreadFeedItemsIds.remove(currentId);
                unreadMessagesIds.add(currentId);
            }
//            Integer messageIds = [SELECT COUNT(Id),VTD2_Feed_Element__c FROM VTD2_Feed_Post_on_PCF__c WHERE VTD2_Feed_Element__c IN :unreadFeedItemsIds GROUP BY VTD2_Feed_Element__c].size();
//            System.debug('messageIds ' + messageIds);

            this.newPersonalChats = unreadMessagesIds;
            this.newBroadcasts = unreadFeedItemsIds;
        }
    }
}