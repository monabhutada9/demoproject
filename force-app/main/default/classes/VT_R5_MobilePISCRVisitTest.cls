/**
 * Created by Yuliya Yakushenkova on 11/30/2020.
 */
@IsTest
public class VT_R5_MobilePISCRVisitTest {

    private static Id visitId;

    public static void firstTest() {
        User usr = fillRestContexts();
        VT_R5_TestRestContext.doTest(usr,
                new VT_R5_TestRestContext.MockRestContext('/mobile/v1/visit/' + visitId, null)
        );
    }

    public static void secondTest() {
        User usr = fillRestContexts();

        String preferredDate = Date.today().year() + '-' + Date.today().month() + '-' + Date.today().day();
        Map<String, String> params = new Map<String, String>{'members' => usr.Id + ';', 'preferredDate' => preferredDate};
        String responseBody = VT_R5_TestRestContext.doTest(usr,
                new VT_R5_TestRestContext.MockRestContext('/mobile/v1/visit/' + visitId + '/timeslots', params)
        );
        System.debug(responseBody);
    }

    public static void thirdTest() {
        User usr = fillRestContexts();

        VT_R5_TestRestContext.doTest(usr,
                new VT_R5_TestRestContext.MockRestContext('/mobile/v1/visit/' + visitId + '/participants', null)
        );
    }

    public static void forthTest() {
        User usr = fillRestContexts();

        RequestParams body = new RequestParams(null, null, null, 'reason for cancel visit');

        VT_R5_TestRestContext.doTest(usr,
                new VT_R5_TestRestContext.MockRestContext(
                        'POST','/mobile/v1/visit/' + visitId + '/cancel', body
                )
        );
    }

    public static void fifthTest() {
        User usr = fillRestContexts();
        User pgUser = [SELECT Id, VTD1_Profile_Name__c FROM User WHERE Username LIKE '%@scott.com' AND VTD1_Profile_Name__c = 'Patient Guide' LIMIT 1];


        RequestParams body =
                new RequestParams(
                        System.now().addDays(5).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\''),
                        '45',
                        new Set<Id>{usr.Id, pgUser.Id},
                        'reason for reschedule visit')
                .setExternalParticipant('External Participant', '');

        VT_R5_TestRestContext.doTest(usr,
                new VT_R5_TestRestContext.MockRestContext(
                        'POST','/mobile/v1/visit/' + visitId + '/reschedule', body
                )
        );
    }

    public static User fillRestContexts() {
        User usr = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile2.PI_USERNAME LIMIT 1];
        visitId = [SELECT Id, VTD1_Actual_Visit__c FROM Event LIMIT 1].VTD1_Actual_Visit__c;
        return usr;
    }

    private class RequestParams {
        public Visit visit;
        public Set<Id> members = new Set<Id>();

        public RequestParams(String scheduledDateTime, String duration, Set<Id> members, String reason) {
            this.visit = new Visit(reason, scheduledDateTime, duration, members);
        }

        public RequestParams setExternalParticipant(String name, String email) {
            visit.externalParticipants.add(
                    new ExternalParticipant('External Participant', '')
            );
            return this;
        }
    }

    private class Visit {
        private String reason;
        private String duration;
        private Set<Id> members = new Set<Id>();
        private String scheduledDateTime;
        private List<ExternalParticipant> externalParticipants = new List<ExternalParticipant>();

        public Visit(String reason, String scheduledDateTime, String duration, Set<Id> members) {
            this.reason = reason;
            this.duration = duration;
            this.members = members;
            this.scheduledDateTime = scheduledDateTime;
        }
    }

    private class ExternalParticipant {
        private String name;
        private String email;

        private ExternalParticipant(String name, String email) {
            this.name = name;
            this.email = email;
        }
    }
}