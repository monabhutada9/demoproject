/**
* @author: Carl Judge
* @date: 10-Oct-18
* @description:Queue action to send docuemnts to DocuSign
**/

public class VT_D2_QAction_CertifyDocuments extends VT_D1_AbstractAction {

    private Id docId;
    private Id runningUserId;

    public VT_D2_QAction_CertifyDocuments(Id docId) {
        this.docId = docId;
        this.runningUserId = UserInfo.getUserId();
    }

    public override void execute() {
        if(this.docId != null){
            VT_D1_HTTPConnectionHandler.Result result = VT_D2_CertifyDocumentsCalloutHelper.sendCertifyDocument(this.docId);
            HttpResponse response = result.httpResponse;

            System.debug('response = ' + response);
            if(response == null) {
                //time out, action will not be added to queue:
                setStatus(STATUS_FAILED);
                setMessage(result.log.VTD1_ErrorMessage__c);
            }else if(response.getStatusCode() >= 500 && response.getStatusCode() < 600) {
                //internal server error, action will be added to queue:
                setStatus(STATUS_FAILED);
                setMessage('Server return status code: ' + response.getStatusCode() + ' ' + response.getStatus() + '; Body: ' + response.getBody());
                addToQueue();
            }else if(response.getStatusCode() == 201){
                //success

                setStatus(STATUS_SUCCESS);
                VTD1_Document__c doc = [SELECT RecordTypeId, VTD1_Regulatory_Document_Type__c, VTD1_Protocol_Amendment__c, VTD1_Site__c FROM VTD1_Document__c WHERE Id = :this.docId];
                if (VT_D1_DocumentCHandler.isPASignaturePage(doc)) {
                    doc.VTD2_Sent_for_Signature__c = true;
                    update doc;
                } else {
                    String userProfileName = VT_D1_HelperClass.getUserProfileName(this.runningUserId);
                    if (userProfileName == VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME ||
                            userProfileName == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME) {
//                        VTD1_Document__c doc = [SELECT RecordTypeId FROM VTD1_Document__c WHERE Id = :this.docId];
                        if (doc.RecordTypeId == VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE) {
                            System.debug('runningUserId' + this.runningUserId);
                            List<Task> taskToUpdate = [
                                    SELECT Id, Status
                                    FROM Task
                                    WHERE OwnerId = :this.runningUserId
                                    AND Document__c = :docId AND VTD1_Type_for_backend_logic__c = 'Eligibility Asessment Task'
                            ];
                            if (taskToUpdate.size() != 0) {
                                taskToUpdate[0].Status = 'Completed';
                                update taskToUpdate[0];
                            }
                            VTD1_Document__c docToUpdate = new VTD1_Document__c(Id = docId, VTD2_EAFStatus__c = 'Sent With Docusign');
                            update docToUpdate;
                        }

                    }
                }
            }else{
                //other errors, action will not be added to queue
                setStatus(STATUS_FAILED);
                setMessage('Server return: ' + response.getStatusCode() + ' ' + response.getStatus() + ', Body: ' + response.getBody());
                //TODO ADD CREATION OF TASK HERE
            }

            //after all attempts, do something??? TODO !!
            if(isLastAttempt() && getStatus() != STATUS_SUCCESS){
//                createTasksForCase(cs, 'Investigate Send Subject Failure', 'Send subject failure after all attempts: ' + getMessage());
            }
        }
    }

    public override Type getType() {
        return VT_D2_QAction_CertifyDocuments.class;
    }
}