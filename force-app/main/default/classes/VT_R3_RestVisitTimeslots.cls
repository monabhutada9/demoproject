/**
 * Created by Alexander Komarov on 26.04.2019.
 */

@RestResource(UrlMapping='/Visit/Timeslots')
global with sharing class VT_R3_RestVisitTimeslots extends VT_R5_MobileRestResponse implements VT_R5_MobileRestRouter.Routable{
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();


    @HttpGet
    global static String getRescheduleTimeSlots() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        Map<String, String> paramsMap = request.params;
        Id visitId = paramsMap.get('visitId');

        if (String.isBlank(visitId) || !helper.isIdCorrectType(visitId, 'VTD1_Actual_Visit__c')) {
            response.statusCode = 400;
            cr.buildResponse(helper.forAnswerForIncorrectInput());
            return JSON.serialize(cr, true);
        }
        String result = '';
        try {
            result = new VT_D1_ActualVisitsHelper().getAvailableDateTimes(visitId);
        } catch (Exception e) {
            response.statusCode = 500;
            cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName());
            return JSON.serialize(cr, true);
        }
        cr.buildResponse(result);
        return JSON.serialize(cr, true);

    }


    global List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/timeslots/{visitId}'
        };
    }

    global void versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        String visitId = parameters.get('visitId');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        get_v1(visitId);
                        return;
                    }
                }

            }
        }
        VT_R5_MobileRestRouter.unsupportedVersionResponse();
        this.addError('unsupported version');
        this.sendResponse();
    }

    private void get_v1(String visitId) {
        try {
            VT_D1_ActualVisitsHelper helper = new VT_D1_ActualVisitsHelper();
            this.buildResponse(new VT_R5_VisitTimeSlotsService(
                    helper.getDatesTimesMap(visitId, null),
                    (Map<String, Object>) JSON.deserializeUntyped(helper.getAvailableDateTimes(visitId)))
                    .getDateTimeSlots()
            );
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    global void initService() {
    }

    public class VT_R5_VisitTimeSlotsService {

        private Map<Date, Time[]> dateToTime;
        private Map<String, Object> deserializedData;

        public VT_R5_VisitTimeSlotsService(Map<Date, Time[]> dateToTime, Map<String, Object> deserializedData) {
            this.dateToTime = dateToTime;
            this.deserializedData = deserializedData;
        }

        public List<DateTimeSlot> getDateTimeSlots() {
            List<DateTimeSlot> dateTimeSlots = new List<DateTimeSlot>();
            for (Date key : dateToTime.keySet()) {
                Time[] timeKeys = dateToTime.get(key);
                if (deserializedData.containsKey(key.format())) {
                    List<String> timeValues = getListOfStringsFromObject(deserializedData.get(key.format()));
                    dateTimeSlots.add(new DateTimeSlot(key, timeKeys, timeValues));
                }
            }
            return dateTimeSlots;
        }

        private List<String> getListOfStringsFromObject(Object obj) {
            String value = JSON.serialize(obj);
            value = value.remove('"');
            List<String> timeValues = value
                    .substring(1, value.length() - 1)
                    .split(',');
            return timeValues;
        }
    }
    public class TimeSlot {
        Datetime key;
        String value;

        public TimeSlot(Datetime timeKey, String timeValue) {
            this.key = timeKey;
            this.value = timeValue;
        }
    }
    public class DateTimeSlot {
        String availableDate;
        List<TimeSlot> times;

        public DateTimeSlot(Date availableDate, Time[] timeKeys, List<String> timeValues) {
            this.availableDate = availableDate.format();
            setTimeSlots(availableDate, timeKeys, timeValues);
        }

        private void setTimeSlots(Date availableDate, Time[] timeKeys, List<String> timeValues) {
            this.times = new List<TimeSlot>();
            for (Integer i = 0; i < timeKeys.size(); i++) {
                Datetime key = Datetime.newInstanceGmt(availableDate, timeKeys[i]);
                this.times.add(
                        new TimeSlot(key, timeValues.get(i))
                );
            }
        }
    }
}