/**
* @author: Carl Judge
* @date: 22-Jan-19
* @description: Calculate scheduled date/time for different users/timezones
**/

public without sharing class VT_D2_ActualVisitTimeCalculator extends VT_D2_AbstractTimeCalculator{

    private Map<String, String> profileMap;
    private String srcField;

    public VT_D2_ActualVisitTimeCalculator(Map<String, String> profileMap, String srcField) {
        this.profileMap = profileMap;
        this.srcField = srcField;
    }

    protected override Map<String, String> getProfileToFieldsMap(){
        return this.profileMap;
    }

    protected override String getBaseTimeField(){
        return this.srcField;
    }

    protected override Map<Id, Map<String, User>> getUserMap(){
        Map<Id, Map<String, User>> userMap = new Map<Id, Map<String, User>>();

        for (Visit_Member__c item : [
            SELECT VTD1_Actual_Visit__c, VTD1_Participant_User__r.Profile.Name, VTD1_Participant_User__r.TimeZoneSidKey,
                VTD1_Participant_User__r.LanguageLocaleKey
            FROM Visit_Member__c
            WHERE VTD1_Actual_Visit__c IN :this.records
            AND VTD1_Participant_User__r.Profile.Name IN :this.profileToFieldsMap.keySet()
        ]) {
            addToUserMap(userMap, item.VTD1_Actual_Visit__c, item.VTD1_Participant_User__r);
        }

        return userMap;
    }
}