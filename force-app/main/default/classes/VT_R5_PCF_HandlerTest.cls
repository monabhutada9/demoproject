/**
 * Created by Denis Belkovskii on 8/17/2020.
 */

@IsTest
public class VT_R5_PCF_HandlerTest {
    @TestSetup
    static void setup() {
        DomainObjects.Account_t acc = new DomainObjects.Account_t()
                .setVTD1_LegalFirstName('Test1')
                .setRecordTypeByName('Sponsor');

        User tUsr = (User) new DomainObjects.User_t()
                .setProfile('Patient')
                .addContact(new DomainObjects.Contact_t()
                        .setFirstName('TestUserFN')
                        .setLastName('TestUserLN'))
                .persist();
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(acc)
                .setVTD2_TMA_Queue_ID(tUsr.Id);

        Case patientCase = (Case) new DomainObjects.Case_t()
                .addSCRUser(new DomainObjects.User_t()
                        .setProfile('Site Coordinator')
                        .addContact(new DomainObjects.Contact_t()
                                .setFirstName('TestSCRUserFirstName')
                                .setLastName('TestSCRUserLastName')))
                .addPIUser(new DomainObjects.User_t()
                        .setProfile('Primary Investigator')
                        .addContact(new DomainObjects.Contact_t()))
                .addVTD1_Primary_PG(new DomainObjects.User_t()
                        .setProfile('Patient Guide'))
                .setRecordTypeByName('VTD1_PCF')
                .addStudy(study)
                .persist();

        User domainOwnerUser = (User) new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME)
                .addContact(new DomainObjects.Contact_t())
                .persist();

        Id rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Patient Contact Form').getRecordTypeId();
        new DomainObjects.Case_t()
                .setStatus('Pre-Consent') //Status != 'Closed'
                .setRecordTypeId(rtId)
                .addContact(new DomainObjects.Contact_t()
                .setFirstName('TestFN')
                .setLastName('TestLN'))
                .setVTD1_PCF_Safety_Concern_Indicator('Possible')//VTD1_PCF_Safety_Concern_Indicator__c !=  'No'
                .setVTD1_Clinical_Study_Membership(patientCase.Id)
                .setOwnerId(domainOwnerUser.Id)
                .persist();
    }

    class PCFWorker implements Queueable {
        final User testUser;
        PCFWorker(User testUser) {
            this.testUser = testUser;
        }
        public void execute(QueueableContext context) {
            Case testCase = getPCFCase();
            Case cs = [SELECT VTD2_Reason_Safety_Concern_Indicator_Cha__c FROM Case WHERE Id = :testCase.Id];
            System.assertEquals('Required if indicator changed', cs.VTD2_Reason_Safety_Concern_Indicator_Cha__c);

            System.runAs(testUser) {
                testCase.VTD2_Reason_Safety_Concern_Indicator_Cha__c = 'Test reason';
                testCase.VTD1_PCF_Safety_Concern_Indicator__c = 'Possible';
                testCase.VTD2_Safety_Concern_Reviewed_by_PI__c = false;
                update testCase;
                Case afterUpdateCase = [
                        SELECT LastModifiedBy.VTD1_Profile_Name__c, AccountId
                        FROM Case
                        WHERE Id = :testCase.Id
                ];
                System.assertEquals(VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME, afterUpdateCase.LastModifiedBy.VTD1_Profile_Name__c);
                testCase.IsEscalated = true;
                testCase.VTD2_Safety_Concern_Reviewed_by_PI__c = true;
                testCase.VTD1_PCF_Safety_Concern_Indicator__c = 'Confirmed AE';
                update testCase;
            }
        }
    }

    private static Case getPCFCase() {
        return [
                SELECT Id,
                LastModifiedBy.VTD1_Profile_Name__c,
                OwnerId,
                Status,
                VTD1_PCF_Call_Narrative__c,
                VTD1_PCF_SC_Open_After_Reassignment__c,
                RecordType.Name,
                VTD1_PCF_Safety_Concern_Indicator__c
                FROM Case
                WHERE Contact.FirstName = 'TestFN'
                AND Contact.LastName = 'TestLN'
                AND VTD1_PCF_Safety_Concern_Indicator__c != 'No'
                LIMIT 1
        ];
    }

    @IsTest
    private static void testCase() {
        Test.startTest();
        User piUser = (User) new DomainObjects.User_t()
                .setEmail(DomainObjects.RANDOM.getEmail())
                .setProfile('Primary Investigator')
                .addContact(new DomainObjects.Contact_t()
                        .setFirstName('TestPiUserFirstName')
                        .setLastName('TestPiUserLastName'))
                .persist();
        Case pcf = [
                SELECT Id, VTD1_PCF_Safety_Concern_Indicator__c, VTD1_Clinical_Study_Membership__c
                FROM Case
                WHERE RecordTypeId = :VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF
                AND VTD1_Clinical_Study_Membership__c != null
                LIMIT 1
        ];
        pcf.VTD1_PCF_Safety_Concern_Indicator__c = 'No';
        pcf.VTD2_Reason_Safety_Concern_Indicator_Cha__c = 'Test Comment';
        pcf.IsEscalated = false;
        update pcf;
        CaseShare csh = new CaseShare(UserOrGroupId = piUser.Id, CaseId = pcf.Id, CaseAccessLevel = 'Edit');
        insert csh;
        pcf.OwnerId = piUser.Id;
        System.runAs(piUser) {
            pcf.VTD1_PCF_Safety_Concern_Indicator__c = 'Possible';
            pcf.VTD2_Reason_Safety_Concern_Indicator_Cha__c = 'Test reason';
            pcf.IsEscalated = true;
            update pcf;
        }

        VT_R3_GlobalSharing.disableForTest = true;
        User testUser = (User) new DomainObjects.User_t()
                .setEmail(DomainObjects.RANDOM.getEmail())
                .setProfile('Primary Investigator')
                .addContact(new DomainObjects.Contact_t()
                        .setFirstName('TestFNforUser')
                        .setLastName('TestLNforUser'))
                .persist();
        Case testCase = [
                SELECT Id,
                        LastModifiedBy.VTD1_Profile_Name__c,
                        OwnerId,
                        Status,
                        VTD1_PCF_Call_Narrative__c,
                        VTD1_PCF_SC_Open_After_Reassignment__c,
                        RecordType.Name,
                        VTD1_PCF_Safety_Concern_Indicator__c
                FROM Case
                WHERE Contact.FirstName = 'TestFN'
                AND Contact.LastName = 'TestLN'
                AND VTD1_PCF_Safety_Concern_Indicator__c != 'No'
                LIMIT 1
        ];
        System.assertEquals(VT_R4_ConstantsHelper_AccountContactCase.CASE_OPEN, testCase.Status);
        System.assert(testCase.VTD1_PCF_Call_Narrative__c.contains('VTD1_PCF_Call_Narrative__c'));
        System.assertEquals(true, testCase.VTD1_PCF_SC_Open_After_Reassignment__c);
        testCase.OwnerId = testUser.Id;
        testCase.VTD2_Reason_Safety_Concern_Indicator_Cha__c = 'test reason';
        update testCase;
        System.enqueueJob(new PCFWorker(testUser));
        Test.stopTest();
    }
}