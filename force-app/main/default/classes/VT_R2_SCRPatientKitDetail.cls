/**
 * Created by user on 03.04.2019.
 */

public without sharing class VT_R2_SCRPatientKitDetail {
    class PatientKitDetailWrapper {
        String kitId;
        String kitName;
        String kitType;
        String deliveryName;
        String shipmentType;
        Id deliveryId;
        String protocolKitName;
        Datetime shipmentDate;
        Datetime expectedDeliveryDate;
        Datetime actualDeliveryDate;
        Id caseId;
        String deliveryStatus;
        String deliveryDelayed;
        String destinationType;
        String distributor;
        String kitStatus;
        String courierName;
        String trackingNumber;
        Datetime reportDeliveryDatetime;
        String airwayBillNumber;
        String labRequisitionNumber;
        String kitContainsAllConsents;
        String kitContentsDamaged;
        String kitNotReceived;
        String kitRecalled;
        String reasonNotReturned;
        String verifyBy;
        String numberReturned;
        String labKitId;
        String IfTemperatureControlled;
        String receivedTemperature;
        String comments;
    }

    class ReplacementFormWrapper {
        Id rfId;
        String replacementReason;
        String comments;
        Datetime dateRequested;
        String decision;
        String studyTeamComments;
        Datetime dateDecided;
    }

    @AuraEnabled
    public static String getKitData(Id kitId) {
        Map <String, Object> dataMap = new Map<String, Object>();
        if (kitId == null) return '';

        List<VTD1_Patient_Kit__c> patientKits = [
                SELECT Id,
                        Name, VTD1_Kit_Type__c, VTD1_Patient_Delivery__r.Name, VTD1_Shipment_Type__c,VTD1_Patient_Delivery__c,
                        VTD1_Protocol_Kit__r.Name, VTD1_Shipment_Date__c, VTD1_Case__c, VTD1_Patient_Delivery__r.VTR2_KitsGivenToPatient__c,
                        VTD1_Patient_Delivery__r.VTD1_Status__c, VTD1_Patient_Delivery__r.VTR2_Destination_Type__c,
                        VTD1_Kit_Distributor__c, VTD1_Status__c, VTD1_Patient_Delivery__r.VTD1_TrackingNumber__c,
                        VTD1_Patient_Reported_Delivery_Date_and__c, VTD1_Airway_Bill_Number__c, VTD1_Lab_Requisition_Number__c,
                        VTD1_Kit_Contains_All_Contents__c, VTD1_Patient_Delivery__r.VTR2_DeliveryDelayed__c,
                        VTD1_Kit_Contents_Damaged__c, VTD1_Kit_Not_Received__c, VTD1_Recalled__c,
                        VTD1_Reason_not_Returned_Discrepancies__c, VTD1_Verified_By__c, VTD1_Number_Returned__c, VTD1_Patient_Lab_Kit_ID__c,
                        VTR2_IfTemperatureControlled__c, VTR2_ReceivedTemperature__c, VTD1_Patient_Delivery__r.VTD1_Courier_Name__c,
                        VTD1_Patient_Delivery__r.VTD1_ExpectedDeliveryDate__c, VTD1_Patient_Delivery__r.VTD1_ExpectedDeliveryDateTime__c,
                        VTD1_Comments__c
                FROM VTD1_Patient_Kit__c
                WHERE Id = :kitId
        ];
        if (patientKits.isEmpty()) return '';
        VTD1_Patient_Kit__c patientKit = patientKits[0];

        PatientKitDetailWrapper pkdw = new PatientKitDetailWrapper();
        pkdw.kitId = patientKit.Id;
        pkdw.kitName = patientKit.Name;
        pkdw.kitType = patientKit.VTD1_Kit_Type__c;
        pkdw.deliveryName = patientKit.VTD1_Patient_Delivery__r.Name;
        pkdw.shipmentType = patientKit.VTD1_Shipment_Type__c;
        pkdw.deliveryId = patientKit.VTD1_Patient_Delivery__c;
        pkdw.protocolKitName = patientKit.VTD1_Protocol_Kit__r.Name;
        pkdw.shipmentDate = patientKit.VTD1_Shipment_Date__c;// != null ? ((Datetime) patientKit.VTD1_Shipment_Date__c).format('dd-MMM-YYYY') : '';
        pkdw.expectedDeliveryDate = patientKit.VTD1_Patient_Delivery__r.VTD1_ExpectedDeliveryDate__c;// != null ? ((Datetime) patientKit.VTD1_Patient_Delivery__r.VTD1_ExpectedDeliveryDate__c).format('dd-MMM-YYYY') : '';
        pkdw.actualDeliveryDate = patientKit.VTD1_Patient_Delivery__r.VTD1_ExpectedDeliveryDateTime__c;// != null ? ((Datetime) patientKit.VTD1_Patient_Delivery__r.VTD1_ExpectedDeliveryDateTime__c).format('dd-MMM-YYYY') : '';
        pkdw.caseId = patientKit.VTD1_Case__c;
        pkdw.deliveryStatus = patientKit.VTD1_Patient_Delivery__r.VTD1_Status__c;
        pkdw.deliveryDelayed = patientKit.VTD1_Patient_Delivery__r.VTR2_DeliveryDelayed__c ? 'Yes' : 'No';
        pkdw.destinationType = patientKit.VTD1_Patient_Delivery__r.VTR2_Destination_Type__c;
        pkdw.distributor = patientKit.VTD1_Kit_Distributor__c;
        pkdw.kitStatus = patientKit.VTD1_Status__c;
        pkdw.courierName = patientKit.VTD1_Patient_Delivery__r.VTD1_Courier_Name__c;
        pkdw.trackingNumber = patientKit.VTD1_Patient_Delivery__r.VTD1_TrackingNumber__c;
        pkdw.reportDeliveryDatetime = patientKit.VTD1_Patient_Reported_Delivery_Date_and__c;// != null ? patientKit.VTD1_Patient_Reported_Delivery_Date_and__c.format('dd-MMM-YYYY hh:mm a') : '';
        pkdw.airwayBillNumber = patientKit.VTD1_Airway_Bill_Number__c;
        pkdw.labRequisitionNumber = patientKit.VTD1_Lab_Requisition_Number__c;
        pkdw.kitContainsAllConsents = patientKit.VTD1_Kit_Contains_All_Contents__c == true ? 'Yes' : 'No';
        pkdw.kitContentsDamaged = patientKit.VTD1_Kit_Contents_Damaged__c ? 'Yes' : 'No';
        pkdw.kitNotReceived = patientKit.VTD1_Kit_Not_Received__c ? 'Yes' : 'No';
        pkdw.kitRecalled = patientKit.VTD1_Recalled__c ? 'Yes' : 'No';
        pkdw.reasonNotReturned = patientKit.VTD1_Reason_not_Returned_Discrepancies__c != null ? patientKit.VTD1_Reason_not_Returned_Discrepancies__c : 'N/A';
        pkdw.verifyBy = patientKit.VTD1_Verified_By__c != null ? patientKit.VTD1_Verified_By__c : 'N/A';
        pkdw.numberReturned = patientKit.VTD1_Number_Returned__c != null ? patientKit.VTD1_Number_Returned__c : 'N/A';
        pkdw.labKitId = patientKit.VTD1_Patient_Lab_Kit_ID__c;
        pkdw.IfTemperatureControlled = patientKit.VTR2_IfTemperatureControlled__c ? 'Yes' : 'No';
        pkdw.receivedTemperature = String.valueOf(patientKit.VTR2_ReceivedTemperature__c);
        pkdw.comments = String.valueOf(patientKit.VTD1_Comments__c);
        dataMap.put('kit', pkdw);

        if (patientKit.VTD1_Case__c!=null) {
            Case carePlan = [
                    SELECT Id,Contact.Name,VTD1_Study__r.Name
                    FROM Case
                    WHERE Id = :patientKit.VTD1_Case__c
            ];
            dataMap.put('case', carePlan);
        }

        List <VTD1_Patient_Kit__c> kits = [SELECT Id, Name FROM VTD1_Patient_Kit__c WHERE VTD1_Patient_Delivery__c = :patientKit.VTD1_Patient_Delivery__c];
        dataMap.put('kits', kits);

        dataMap.put('canConfirmHandOff', patientKit.VTD1_Patient_Delivery__r.VTD1_Status__c == 'Delivered'
                                            && patientKit.VTD1_Patient_Delivery__r.VTR2_Destination_Type__c == 'Site'
                                            && !patientKit.VTD1_Patient_Delivery__r.VTR2_KitsGivenToPatient__c);
        dataMap.put('canRecall', !patientKit.VTD1_Recalled__c);
        dataMap.put('canMarkAsDelayed', patientKit.VTD1_Patient_Delivery__r.VTD1_Status__c == 'Shipped'
                                            && !patientKit.VTD1_Patient_Delivery__r.VTR2_DeliveryDelayed__c);

        IMP_Replacement_Form__c replacementForm = null;
        Map <Id, IMP_Replacement_Form__c> replacementForms = new Map<Id, IMP_Replacement_Form__c>([SELECT Id,
                VTD1_Replacement_Reason__c, VTD1_Comments__c, VTD1_isApproved__c, VTD1_Study_Team_Comments__c, VTD1_Date_Decided__c, CreatedDate FROM IMP_Replacement_Form__c WHERE VTD1_Replace__c = :kitId AND VTD1_Replacement_Decision__c = NULL]);

        if (!replacementForms.isEmpty()) {
            List <Task> tasks = [SELECT Id, WhatId FROM Task
                WHERE OwnerId = :UserInfo.getUserId()
                AND WhatId IN : replacementForms.keySet()
                AND VTD2_Task_Unique_Code__c = '510'
                AND Status = 'Open'
            ];
            if (!tasks.isEmpty()) {
                replacementForm = replacementForms.get(tasks[0].WhatId);
            }
        }

        if (replacementForm != null) {
            ReplacementFormWrapper rfw = new ReplacementFormWrapper();
            rfw.rfId = replacementForm.Id;
            rfw.replacementReason = replacementForm.VTD1_Replacement_Reason__c;
            rfw.comments = replacementForm.VTD1_Comments__c;
            rfw.decision = replacementForm.VTD1_isApproved__c != null ? replacementForm.VTD1_isApproved__c : '';
            rfw.studyTeamComments = replacementForm.VTD1_Study_Team_Comments__c;
            rfw.dateDecided = System.now();
            rfw.dateRequested = replacementForm.CreatedDate;
            dataMap.put('replacementForm', rfw);
        }

        return JSON.serialize(dataMap);
    }
    @AuraEnabled
    public static Datetime getCurrentDateTime() {
        return System.now();
    }
    @AuraEnabled
    public static String confirmHandoff(String deliveryId) {
        try {
            VTD1_Order__c delivery = [SELECT Id, VTR2_KitsGivenToPatient__c, VTR2_KitsGivenToPatientDate__c, VTR2_KitsGivenToPatientBy__c FROM VTD1_Order__c WHERE Id = : deliveryId];
            delivery.VTR2_KitsGivenToPatient__c = true;
            delivery.VTR2_KitsGivenToPatientDate__c = System.now();
            delivery.VTR2_KitsGivenToPatientBy__c = UserInfo.getUserId();
            update delivery;
            return '';
        } catch (Exception e) {
            return e.getMessage();
        }
    }
    @AuraEnabled
    public static String confirmMarkAsDelayed(String deliveryId) {
        try {
            VTD1_Order__c delivery = [SELECT Id, VTR2_DeliveryDelayed__c FROM VTD1_Order__c WHERE Id = : deliveryId];
            delivery.VTR2_DeliveryDelayed__c = true;
            update delivery;
            return '';
        } catch (Exception e) {
            return e.getMessage();
        }
    }
    @AuraEnabled
    public static String confirmRecall(String kitId) {
        try {
            VTD1_Patient_Kit__c patientKit = [select Id, VTD1_Recalled__c from VTD1_Patient_Kit__c where Id = :kitId];
            patientKit.VTD1_Recalled__c = true;
            update patientKit;
            return '';
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @AuraEnabled
    public static String confirmReplacementForm(String formId, String decision, String studyTeamComments) {
        try {
            //VTD1_Patient_Kit__c patientKit = [select Id, VTD1_Recalled__c from VTD1_Patient_Kit__c where Id = :kitId];
            //patientKit.VTD1_Recalled__c = true;
            //update patientKit;
            IMP_Replacement_Form__c replacementForm = [select Id, VTD1_isApproved__c, VTD1_Study_Team_Comments__c, VTD1_Date_Decided__c, VTD1_Replacement_Decision__c from IMP_Replacement_Form__c where Id = : formId];
            replacementForm.VTD1_isApproved__c = decision;
            replacementForm.VTD1_Study_Team_Comments__c = studyTeamComments;
            //replacementForm.VTD1_Date_Decided__c = System.today();
            if (decision == 'Yes') {
                replacementForm.VTD1_Replacement_Decision__c = 'Approved';
            } else if (decision == 'No') {
                replacementForm.VTD1_Replacement_Decision__c = 'Rejected';
            }
            update replacementForm;
            return '';
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @AuraEnabled
    public static String updateKit(String kitId, String airwayBillNumber, String labRequisitionNumber) {
        try {
            update new VTD1_Patient_Kit__c(Id = kitId, VTD1_Airway_Bill_Number__c = airwayBillNumber, VTD1_Lab_Requisition_Number__c = labRequisitionNumber);
            return '';
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}