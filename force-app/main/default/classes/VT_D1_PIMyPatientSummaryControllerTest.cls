//Alexander Metelskiy
@IsTest
private class VT_D1_PIMyPatientSummaryControllerTest {

    @TestSetup
    static void setup() {
        Group queue = new Group(Type = 'Queue', Name = 'CM Test Pool');
        insert queue;
        System.enqueueJob(new VT_D1_TestUtils.QueueSobjectCreator(queue.Id));

        new DomainObjects.VTD1_Protocol_Deviation_t().persist();

        HealthCloudGA__CarePlanTemplate__c study = (HealthCloudGA__CarePlanTemplate__c) new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'))
                .persist();
        study.VTD1_CM_Queue_Id__c = queue.Id;
        update study;

        DomainObjects.Contact_t patientContact_t = new DomainObjects.Contact_t()
                .setFirstName('patient1')
                .setLastName('VT_D1_PIMyPatientSummaryControllerTest');
        DomainObjects.User_t patientUser = new DomainObjects.User_t()
                .addContact(patientContact_t)
                .setProfile('Patient');

        DomainObjects.Contact_t piContact = new DomainObjects.Contact_t().setLastName('Con2');
        DomainObjects.User_t piUser = new DomainObjects.User_t()
                .addContact(piContact)
                .setProfile('Primary Investigator')
                .setEmail('test@test2019.test');

        DomainObjects.Case_t patientCase = new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .addPIUser(piUser)
                .addVTD1_Patient_User(patientUser)
                .setStudy(study.Id)
                .addContact(patientContact_t);
        patientCase.persist();

        new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .setStudy(study.Id)
                .addContact(patientContact_t)
                .setVTD1_Clinical_Study_Membership(patientCase.id)
                .setVTD1_PCF_Safety_Concern_Indicator('Possible')
                .persist();
        new DomainObjects.Phone_t(new DomainObjects.Account_t(), '123-4567')
                .addContact(patientContact_t)
                .setPrimaryForPhone(true)
                .persist();

        new DomainObjects.Address_t()
                .setContact(patientContact_t.Id)
                .setPrimary(true)
                .setCity('City')
                .setState('TN')
                .persist();
    }

    @IsTest
    static void piMyPatientSummaryTest() {
        Case c = getPatientCase('patient1', 'VT_D1_PIMyPatientSummaryControllerTest');

        Test.startTest();
        String patientSummaryString = VT_D1_PIMyPatientSummaryController.getPatientSummary(c.Id);
            VT_D1_PIMyPatientSummaryController.PatientSummary patientSummary = (VT_D1_PIMyPatientSummaryController.PatientSummary) JSON.deserialize(patientSummaryString, VT_D1_PIMyPatientSummaryController.PatientSummary.class);
        Test.stopTest();

            System.assertEquals(null, patientSummary.primaryPGUser);
            System.assertNotEquals(null, patientSummary.primaryPIUser);
            System.assertEquals(null, patientSummary.backupPGUser);
            System.assertEquals(null, patientSummary.backupPIUser);
            System.assertNotEquals(null, patientSummary.cas);
            System.assertNotEquals(null, patientSummary.phone);
            System.assertNotEquals(null, patientSummary.address);
            System.assert(patientSummary.actualVisitTypeInputSelectRatioElementList.size() > 0);

        Profile userProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
        System.assertEquals(userProfile.Name, VT_D1_PIMyPatientSummaryController.getCurrentUserProfile());
    }

    @IsTest
    static void createProtocolDeviationTest() {
        Case c = getPatientCase('patient1', 'VT_D1_PIMyPatientSummaryControllerTest');
        String pdDescription = 'Test description';

        Test.startTest();
        VT_D1_PIMyPatientSummaryController.createPD(c.Id, pdDescription);
        Test.stopTest();

        List<VTD1_Protocol_Deviation__c> protocolDeviationList = [
                SELECT Id
                FROM VTD1_Protocol_Deviation__c
                WHERE VTD1_Subject_ID__c = :c.Id AND VTD1_Deviation_Description__c = :pdDescription
        ];
           // System.assertEquals(1, protocolDeviationList.size());
    }

    @IsTest
    static void createAdhocVisitTest() {
        Case c = getPatientCase('patient1', 'VT_D1_PIMyPatientSummaryControllerTest');

            String actualVisitType = 'Labs';
            String actualVisitActivitiesLabs = 'Visit activities';
            String actualVisitModality = 'At Home';
            String actualVisitProceduresOrders = 'Visit procedures';
            String actualVisitReason = 'Reason for request';

        Test.startTest();
        VTD1_Actual_Visit__c actualVisit;
            try {
            actualVisit = VT_D1_PIMyPatientSummaryController.createAdhocVisit(c.Id, actualVisitType, actualVisitModality,
                        actualVisitActivitiesLabs, actualVisitProceduresOrders, actualVisitReason);
        } catch (Exception e) {}
        Test.stopTest();

            List<VTD1_Actual_Visit__c> actualVisitList = [
                    SELECT Id
                    FROM VTD1_Actual_Visit__c
                    WHERE
                RecordTypeId = :VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_ACTUAL_VISIT_UNSCHEDULED
                AND VTD1_Status__c = :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED
                AND Unscheduled_Visits__c = :c.Id
                AND Name = :'Ad Hoc Visit'
                    AND VTD1_Unscheduled_Visit_Type__c = :actualVisitType
                    AND VTD1_Reason_for_Request__c = :actualVisitReason
            ];

    }

    @IsTest
    static void updateCaseStatusTest() {
        Case c = getPatientCase('patient1', 'VT_D1_PIMyPatientSummaryControllerTest');

            String caseStatus = VT_R4_ConstantsHelper_AccountContactCase.CASE_DROPPED_COMPL_FOLLOW_UP;
            String reasonCode = 'Non Compliance';
            String reasonToOther = 'test reason to other';
            String washoutOutcome = 'Successful';

        Test.startTest();
        try {
            VT_D1_PIMyPatientSummaryController.updateCaseStatus(c.Id, caseStatus, reasonCode, reasonToOther);
        } catch (Exception e) {}
        VT_D1_PIMyPatientSummaryController.updateCaseWashoutOutcome(c.Id, washoutOutcome);
        Test.stopTest();

        c = [SELECT Id, VTR2_WashoutRunInOutcome__c FROM Case WHERE Id = :c.Id];
        System.assertEquals(washoutOutcome, c.VTR2_WashoutRunInOutcome__c);
        System.assertNotEquals(null, VT_D1_PIMyPatientSummaryController.randomizeRemote(c.Id));
        System.assertNotEquals(null, VT_D1_PIMyPatientSummaryController.getLink(c.Id));
    }

    /************************
    * @description Test method for updateCaseEligibilityStatus on VT_D1_PIMyPatientSummaryController
    * Modified by Mona Bhutada
    */
    @IsTest
    static void piMyPatientSummaryEligibilityStatusTest() {
        Case c = getPatientCase('patient1', 'VT_D1_PIMyPatientSummaryControllerTest');
        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c(
                Id = c.VTD1_Study__c,
                VTR2_EligibilityOutsideSH__c = true,
                VTR5_Baseline_Visit_After_A_R_Flag__c = true
        );
            update study;

            String eligibilityStatus = 'Eligible';

        Test.startTest();
        VT_D1_PIMyPatientSummaryController.updateCaseEligibilityStatus(c.Id, eligibilityStatus);
        Test.stopTest();

            Case caseEligibilityStatusUpdated = [
                SELECT Id, Status
                    FROM Case
                WHERE Id = :c.Id AND
                    VTD1_Eligibility_Status__c = :eligibilityStatus
            ];
            System.assertNotEquals(null, caseEligibilityStatusUpdated);
    }

    @IsTest
    static void updatePatientIdTest() {
        Case c = getPatientCase('patient1', 'VT_D1_PIMyPatientSummaryControllerTest');
        Test.startTest();
        VT_D1_PIMyPatientSummaryController.updatePatientId(c.Id, 'patientId', 'anything');
        Test.stopTest();
    }


    @IsTest
    static void updateIrtField() {

        HealthCloudGA__CarePlanTemplate__c study = [SELECT VTR5_IRTSystem__c FROM HealthCloudGA__CarePlanTemplate__c];
        study.VTR5_IRTSystem__c = 'Signant';
        update study;
        
        Id caseId = [SELECT Id, VTR5_Day_57_Dose__c, VTD1_Randomized_Date1__c FROM Case LIMIT 1].Id;
        Test.startTest();
        VT_D1_PIMyPatientSummaryController.updateIrtField(caseId, 'VTR5_SubsetImmuno__c', 'Subset Immuno value', 'text');
        VT_D1_PIMyPatientSummaryController.updateIrtField(caseId, 'VTR5_Day_1_Dose__c', String.valueOf(Date.today()), 'date');
        VT_D1_PIMyPatientSummaryController.updateIrtField(caseId, 'VTD1_Randomized_Date1__c', String.valueOf(Date.today() + 1), 'date');

        Test.stopTest();

    }
  
  static Case getPatientCase(String firstName, String lastName) {
        return [
                SELECT Id, VTD1_Study__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
                AND Contact.FirstName = :firstName
                AND Contact.LastName = :lastName
        ];

    }
}