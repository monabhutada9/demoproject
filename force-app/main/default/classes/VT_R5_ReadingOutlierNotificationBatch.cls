/************************************************************************
 * Name  : VT_R5_ReadingOutlierNotificationBatch
 * Author : Mahesh Shimpi
 * Desc  : Batch which is scheduled for fetching the (point in time) PatientDevices whose readings(public$readings) lies in 
 * outliers specified at study level studyMeasurementConfigureations.
 * And call the NOtification & Task handlers.
 *
 *
 * Modification Log:
 * ----------------------------------------------------------------------
 * Developer                Date                description
 * ----------------------------------------------------------------------
 * ---ABC-----             29/05/2020           Original 
 * 
 *************************************************************************/
public without sharing class VT_R5_ReadingOutlierNotificationBatch implements Database.Batchable<SObject>, Database.Stateful, System.Schedulable, Database.AllowsCallouts {
   
    public Datetime outOfRangeBatchCurrentExecutionTime;
    public VTR5_ConnectedDeviceNotifications__c outOfRangeBatchCustomSetting;




    /* 
     * method : start(Database.BatchableContext BC)
     * param : Database.BatchableContext BC - a BatchableContext variable.
     * description :This method is overridden from Batchable interface. 
     *               Used to get all the datalist for processing.
     * return : Database.QueryLocator - a query for fetching (point in time) PatientDevices.VTR5_ConnectedDeviceNotifications__c.getOrgDefaults();
     */
    public Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('In start method');
        List<VTR5_ConnectedDeviceNotifications__c> customSetting = [SELECT Id, Name,
                    VTR5_OutOfRangeBatchLastExecutionTime__c, 
                    VTR5_OutOfRangeBatchPublicReadingQuery__c, 
                    VTR5_OutOfRangeBatchPatientDeviceField__c, 
                    VTR5_OutOfRangeBatchPublicReadingField__c,
                    VTR5_PG_Notification_Catalog_Code__c,
                    VTR5_PG_Task_Catalog_Code__c,
                    VTR5_PI_Notification_Catalog_Code__c,
                    VTR5_SCR_Notification_Catalog_Code__c,
                    VTR5_SCR_Task_Catalog_Code__c
                    FROM VTR5_ConnectedDeviceNotifications__c LIMIT 1];
        
        if(!customSetting.isEmpty()){
            this.outOfRangeBatchCustomSetting = customSetting[0];
            System.debug('Custom setting :' + this.outOfRangeBatchCustomSetting);   
        }else{
            System.debug('No custom setting data found. Returning from start of batch.');
            return null;
        }
        // set the timestamp for next run of batch.     
        this.outOfRangeBatchCurrentExecutionTime = System.now();     





        String query = 'SELECT Id, ' + 
                'Name, ' + 
                'VTD1_Case__c, ' + 
                'VTD1_Case__r.VTD1_Study__c, ' + 
                'VTD1_Case__r.VTD1_PI_contact__c, ' + 
                'VTD1_Case__r.VTD1_Primary_PG__c, ' +
                'VTD1_Case__r.VTR2_SiteCoordinator__c, ' +
                'VTD1_Name__c, ' +  
                'VTD1_Protocol_Device__c, ' + 
                this.outOfRangeBatchCustomSetting.VTR5_OutOfRangeBatchPatientDeviceField__c + ' ,' +
                'VTR5_IMEI__c ' +
                'FROM VTD1_Patient_Device__c ' +
                'WHERE VTR5_Active__c = true ' + 
                'AND VTD1_Protocol_Device__r.VTD1_Device__r.VTR5_IsContinous__c = false '+
                'AND ' + this.outOfRangeBatchCustomSetting.VTR5_OutOfRangeBatchPatientDeviceField__c + ' != null ';




            System.debug('PatientDevice query : ' + query);
        return Database.getQueryLocator(query);
    }
    /* 
     * method : execute(Database.BatchableContext BC, List<VTD1_Patient_Device__c> scope)
     * param : Database.BatchableContext BC - a BatchableContext variable.
     *         List<VTD1_Patient_Device__c> scope - a list of PatientDevices for processing.
     * description : This method is overridden from Batchable interface. 
     *               Used to process the chunk of record specied in scope.
     * return : void
     */
    public void execute(Database.BatchableContext BC, List<VTD1_Patient_Device__c> scope){

        Set<String> availableTypes =new Set<String>{'heart_rate','daily_steps','pressure_sys','pressure_dia'};

        // map of studyId & PatientDevices
        Map<Id, Set<VTD1_Patient_Device__c>> mapStudyPatientDevices = new Map<Id,  Set<VTD1_Patient_Device__c>>();
        // map of studyId & StudyMeasurementConfigs
        Map<Id, Set<VTR5_StudyMeasurementsConfiguration__c>> mapStudyAndStudyMeasurementConfigs = 
                            new Map<Id, Set<VTR5_StudyMeasurementsConfiguration__c>>();  
        // set of reading serial numbers      
        Set<String> setDeviceIds = new Set<String>();

        try{
            for (VTD1_Patient_Device__c device : scope) {
                if(!mapStudyPatientDevices.containsKey(device.VTD1_Case__r.VTD1_Study__c)) {
                    mapStudyPatientDevices.put(device.VTD1_Case__r.VTD1_Study__c, new Set<VTD1_Patient_Device__c>());
                }
                mapStudyPatientDevices.get(device.VTD1_Case__r.VTD1_Study__c).add(device);

                String devId = String.valueOf(device.get(this.outOfRangeBatchCustomSetting.VTR5_OutOfRangeBatchPatientDeviceField__c));
                setDeviceIds.add(devId);
            }
            System.debug('Study & PDs :' + mapStudyPatientDevices);

            List<VTR5_StudyMeasurementsConfiguration__c> lstStudyConfigurations = [
                        SELECT Id, Name, VTR5_LowerBoundaryHigh_Alert__c, VTR5_MeasurementType__c, 
                        VTR5_Measureunit__c, VTR5_ProtocolDevice__c, VTR5_Study__c, VTR5_UpperBoundaryHighAlert__c, 
                        VTR5_HighBoundary__c, VTR5_LowBoundary__c 
                        FROM VTR5_StudyMeasurementsConfiguration__c 
                        WHERE VTR5_Study__c IN: mapStudyPatientDevices.keySet()];
            System.debug('StudyMeasureConfigs' + lstStudyConfigurations);

            for (VTR5_StudyMeasurementsConfiguration__c config : lstStudyConfigurations) {
                if(!mapStudyAndStudyMeasurementConfigs.containsKey(config.VTR5_Study__c)) {
                    mapStudyAndStudyMeasurementConfigs.put(config.VTR5_Study__c, new Set<VTR5_StudyMeasurementsConfiguration__c>());
                }
                mapStudyAndStudyMeasurementConfigs.get(config.VTR5_Study__c).add(config);
            }
            System.debug('Study & StudyMeasureConfigs' + mapStudyAndStudyMeasurementConfigs);   
            // get all the public readings
            String publicReadingQuery =  this.outOfRangeBatchCustomSetting.VTR5_OutOfRangeBatchPublicReadingQuery__c;
            if(this.outOfRangeBatchCustomSetting.VTR5_OutOfRangeBatchLastExecutionTime__c != null){

                publicReadingQuery = publicReadingQuery + ' AND VTR5_type__c IN:availableTypes AND VTR5_date__c > ' + 

                    this.outOfRangeBatchCustomSetting.VTR5_OutOfRangeBatchLastExecutionTime__c.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
            }
            System.debug('PublicReading query: ' + publicReadingQuery);
            List<VTR5_publicreadings__x> lstPublicReadings = Database.query(publicReadingQuery);
            Map<String,Set<VTR5_publicreadings__x>> mapPtDeviceSerialAndReadings = new Map<String,Set<VTR5_publicreadings__x>>();

            
            if(Test.isRunningTest()){

                lstPublicReadings = VT_R5_ReadngOutlrNotificatnBatchTest.createReadingForTest();


            }
            
            for(VTR5_publicreadings__x reading : lstPublicReadings){
                String serialNumberFieldValue = String.valueOf(reading.get(this.outOfRangeBatchCustomSetting.VTR5_OutOfRangeBatchPublicReadingField__c));
                if(!mapPtDeviceSerialAndReadings.containsKey(serialNumberFieldValue)) {
                    mapPtDeviceSerialAndReadings.put(serialNumberFieldValue, new Set<VTR5_publicreadings__x>());
                }
                mapPtDeviceSerialAndReadings.get(serialNumberFieldValue).add(reading);
            }
            System.debug('PatientDevice readings :'+mapPtDeviceSerialAndReadings);
            // information tosetConfigs be return
            this.processNotifications(scope, mapPtDeviceSerialAndReadings, mapStudyAndStudyMeasurementConfigs, BC);

        } catch(Exception ex){
            System.debug('Exception Occured : ' + ex.getMessage());
            ErrorLogUtility.logException(ex, ErrorLogUtility.ApplicationArea.APP_AREA_OUTLIER_READING, 
            VT_R5_ReadingOutlierNotificationBatch.class.getName());
        }
    }
    /* 
     * method : finish(Database.BatchableContext BC, List<VTD1_Patient_Device__c> scope)
     * param : Database.BatchableContext BC - a BatchableContext variable.
     * description : This method is overridden from Batchable interface. 
     *               Used for post processing, after all the chunk(child) jobs finished.
     * return : void
     */
    public void finish(Database.BatchableContext BC){
        this.outOfRangeBatchCustomSetting.VTR5_OutOfRangeBatchLastExecutionTime__c = this.outOfRangeBatchCurrentExecutionTime;
        update this.outOfRangeBatchCustomSetting;
        System.debug('Custom Setting updated.');
    }
    /* 
     * method : execute(SchedulableContext sc)
     * param : SchedulableContext sc - a SchedulableContext variable.
     * description : This method is overridden from Schedulable interface. 
     *               Used for scheduling the apex job. 
     * return : void
     */
    public void execute(SchedulableContext sc){
        VTR5_ConnectedDeviceNotifications__c customSetting = VTR5_ConnectedDeviceNotifications__c.getOrgDefaults();
        VT_R5_ReadingOutlierNotificationBatch batch = new VT_R5_ReadingOutlierNotificationBatch();
        Integer batchSize = Integer.valueOf(customSetting.VTR5_OutOfRangeBatchSize__c); 
        Database.executeBatch(batch, batchSize);
    }
    // public interface
    /* 
     * method : scheduleBatch()
     * param : none
     * description : This method is used to schedule the schedullar job.
     *               It schedules the job at interval of every 15 min.
     * return : void
     */
    public static void scheduleBatch(){
        // String cronExp = '0 5 * * * ? *'; // scheduled job will execute every 30 min interval
        for(Integer i = 0; i < 2; i++) {
            String jobName = 'Reading Outlier Notifications'+i;
            String cronExp = '0 '+i*30+' * * * ? *';
            VT_R5_ReadingOutlierNotificationBatch scheduleJob = new VT_R5_ReadingOutlierNotificationBatch();
            System.schedule(jobName, cronExp, scheduleJob);
        }
    }
    /* 
     * method : processNotifications(List<VTD1_Patient_Device__c> scope, 
     *                  Map<String,Set<VTR5_publicreadings__x>> mapPtDeviceSerialAndReadings, 
     *                  Map<Id, Set<VTR5_StudyMeasurementsConfiguration__c>> mapStudyAndStudyMeasurementConfigs,
     *                  Database.BatchableContext BC)
     * param : List<VTD1_Patient_Device__c> scope - list of PAtienDevices. 
     *         Map<String,Set<VTR5_publicreadings__x>> mapPtDeviceSerialAndReadings - map of patient serial number & PublicReadings.
     *         Map<Id, Set<VTR5_StudyMeasurementsConfiguration__c>> mapStudyAndStudyMeasurementConfigs - a map of studyId & studyMeasumentConfigs.
     *         Database.BatchableContext BC - a batchable context varible.
     * description : This method is used to find the outlier readings for the PatientDevices. And calls the Notification & Task Handeler.
     * return : void
     */
    private void processNotifications(List<VTD1_Patient_Device__c> scope, 
                        Map<String,Set<VTR5_publicreadings__x>> mapPtDeviceSerialAndReadings, 
                        Map<Id, Set<VTR5_StudyMeasurementsConfiguration__c>> mapStudyAndStudyMeasurementConfigs,
                        Database.BatchableContext BC){
        //Set<VTD1_Patient_Device__c> setPtDevices = new Set<VTD1_Patient_Device__c>();
        //list<VT_D2_TNCatalogNotifications.ParamsHolder> NotificationList= new list<VT_D2_TNCatalogNotifications.ParamsHolder>();
        Map<String, VT_D2_TNCatalogNotifications.ParamsHolder> NotificationMap = new Map<String, VT_D2_TNCatalogNotifications.ParamsHolder>();
        //list<VT_D2_TNCatalogTasks.ParamsHolder> TaskList= new list<VT_D2_TNCatalogTasks.ParamsHolder>();
        Map<String, VT_D2_TNCatalogTasks.ParamsHolder> TaskMap = new Map<String, VT_D2_TNCatalogTasks.ParamsHolder>();
        /*VTR5_ConnectedDeviceNotificationsConfig__mdt metaData = [SELECT DeveloperName, VTR5_PI_Notification_Catalog_Code__c,VTR5_PG_Notification_Catalog_Code__c,
                                                                VTR5_PG_Task_Catalog_Code__c,VTR5_SCR_Notification_Catalog_Code__c,VTR5_SCR_Task_Catalog_Code__c
                                                                FROM VTR5_ConnectedDeviceNotificationsConfig__mdt];*/

        for (VTD1_Patient_Device__c device : scope) {
            Set<VTR5_StudyMeasurementsConfiguration__c> setConfigs = mapStudyAndStudyMeasurementConfigs.get(device.VTD1_Case__r.VTD1_Study__c);
            String devSerial = String.valueOf(device.get(this.outOfRangeBatchCustomSetting.VTR5_OutOfRangeBatchPatientDeviceField__c));
            System.debug('DevSerial : ' + devSerial);
            if(!mapPtDeviceSerialAndReadings.KeySet().isEmpty()){
                 if(mapPtDeviceSerialAndReadings.containsKey(devSerial)){
                    for (VTR5_publicreadings__x reading : mapPtDeviceSerialAndReadings.get(devSerial)) {
                        if (setConfigs != null) {
                            for (VTR5_StudyMeasurementsConfiguration__c studyConfig : setConfigs) {
                                if ((studyConfig.VTR5_MeasurementType__c == reading.VTR5_type__c)
                                    && (studyConfig.VTR5_HighBoundary__c < reading.VTR5_value__c ||  studyConfig.VTR5_LowBoundary__c > reading.VTR5_value__c)) {
                                        


                                        System.debug('Gererate notification for PI, PG & SCR' + device);


                                        //setPtDevices.add(device);
                                        //SH-8206 Start
                                        if(device.VTD1_Case__r.VTD1_PI_contact__c!=null && !NotificationMap.ContainsKey(this.outOfRangeBatchCustomSetting.VTR5_PI_Notification_Catalog_Code__c+'-'+device.id)){
                                            
                                            VT_D2_TNCatalogNotifications.ParamsHolder piParam = new VT_D2_TNCatalogNotifications.ParamsHolder();                                        
                                            piParam.tnCode = this.outOfRangeBatchCustomSetting.VTR5_PI_Notification_Catalog_Code__c; 
                                            piParam.sourceId = device.id;
                                            NotificationMap.put(this.outOfRangeBatchCustomSetting.VTR5_PI_Notification_Catalog_Code__c+'-'+device.id, piParam);
                                        } 
                                        //SH-8206 End
                                                                //VTD1_Case__r.VTR2_SiteCoordinator__c VTD1_Case__r.VTD1_Primary_PG__c




                                        if(device.VTD1_Case__r.VTR2_SiteCoordinator__c!=null){
                                            
                                            If(!TaskMap.ContainsKey(this.outOfRangeBatchCustomSetting.VTR5_SCR_Task_Catalog_Code__c+'-'+device.id)){
                                                VT_D2_TNCatalogTasks.ParamsHolder tskScr= new VT_D2_TNCatalogTasks.ParamsHolder();
                                                tskScr.tnCode=this.outOfRangeBatchCustomSetting.VTR5_SCR_Task_Catalog_Code__c;
                                                tskScr.sourceId=device.id;
                                                TaskMap.put(this.outOfRangeBatchCustomSetting.VTR5_SCR_Task_Catalog_Code__c+'-'+device.id,tskScr);
                                            }
                                            /*
                                            if(!NotificationMap.ContainsKey(this.outOfRangeBatchCustomSetting.VTR5_SCR_Notification_Catalog_Code__c+'-'+device.id)){
                                                VT_D2_TNCatalogNotifications.ParamsHolder notifSCr= new VT_D2_TNCatalogNotifications.ParamsHolder();
                                                notifSCr.tnCode=this.outOfRangeBatchCustomSetting.VTR5_SCR_Notification_Catalog_Code__c;
                                                notifSCr.sourceId=device.id;
                                                NotificationMap.put(this.outOfRangeBatchCustomSetting.VTR5_SCR_Notification_Catalog_Code__c+'-'+device.id, notifSCr);
                                            }*/
                                            
                                        }else if(device.VTD1_Case__r.VTD1_Primary_PG__c!=null){
                                             
                                            If(!TaskMap.ContainsKey(this.outOfRangeBatchCustomSetting.VTR5_PG_Task_Catalog_Code__c+'-'+device.id)){
                                                VT_D2_TNCatalogTasks.ParamsHolder tskPg= new VT_D2_TNCatalogTasks.ParamsHolder();
                                                tskPg.tnCode=this.outOfRangeBatchCustomSetting.VTR5_PG_Task_Catalog_Code__c;
                                                tskPg.sourceId=device.id;
                                                TaskMap.put(this.outOfRangeBatchCustomSetting.VTR5_PG_Task_Catalog_Code__c+'-'+device.id, tskPg);
                                            }
                                            
                                            if(!NotificationMap.ContainsKey(this.outOfRangeBatchCustomSetting.VTR5_PG_Notification_Catalog_Code__c+'-'+device.id)){
                                                VT_D2_TNCatalogNotifications.ParamsHolder notifPg= new VT_D2_TNCatalogNotifications.ParamsHolder();
                                                notifPg.tnCode=this.outOfRangeBatchCustomSetting.VTR5_PG_Notification_Catalog_Code__c;
                                                notifPg.sourceId=device.id;
                                                NotificationMap.put(this.outOfRangeBatchCustomSetting.VTR5_PG_Notification_Catalog_Code__c+'-'+device.id, notifPg);
                                            }

                                        }

                                }      
                            }
                        }
                    }
                }else{
                    System.debug('PatientDevice with Serial Number : '+ devSerial +' has no out of range readings.');
                }                 
            }else{
                System.debug('No reading for this device : ' + device.id);            
            }
        }      
        //-----Add the NotificationC or TNcatlog generation code here---------
        if(!NotificationMap.isEmpty()){
            System.debug('### Map of Notification :' + NotificationMap.size());
            List<VTD1_NotificationC__c> lstNoteC = VT_D2_TNCatalogNotifications.generateNotificationProcess(NotificationMap.Values());
            List<VTD1_NotificationC__c> lstNoteCEmail = new List<VTD1_NotificationC__c>();
            
            for(VTD1_NotificationC__c nt : lstNoteC){
                if(nt.VDT2_Unique_Code__c == this.outOfRangeBatchCustomSetting.VTR5_PI_Notification_Catalog_Code__c){
                    lstNoteCEmail.add(nt);
                }
            }
            
            system.debug('#### lstNoteC '+lstNoteCEmail.size());
            VT_R5_EmailGeneratorForTaskNotificationC.SendEmail(lstNoteCEmail);
        }else{
            System.debug('No reading for processing this JobId : ' + BC.getJobId() + ' & ChildJobId : ' + BC.getChildJobId());
        }
        if(!TaskMap.isEmpty()){
            //TaskMap.size();
            list<Task> Task_list=VT_D2_TNCatalogTasks.generateTask(TaskMap.Values());
            system.debug('### Tasklist '+Task_list.size()+' '+Task_list);

            if(!Task_list.isEmpty())
                VT_R5_EmailGeneratorForTaskNotificationC.SendEmail(Task_list);
            
        }
    }
}