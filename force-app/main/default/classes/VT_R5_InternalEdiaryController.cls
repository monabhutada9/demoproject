/**
* @author: N.Arbatskiy
* @date: 20-July-20
* @description: Controller for VT_R5_InternalEdiaryForm component
**/

public with sharing class VT_R5_InternalEdiaryController {
    /**
       * @param caseId is used for querying all eDiaries/Source forms related to a certain patient case
       * @param isSourceForm is used for splitting the SOQL's for eDiaries/Source forms
       * @return List<VTD1_Survey__c> ediaries to display inside a component
       **/
    @AuraEnabled
    public static List<VTD1_Survey__c> getData(String caseId, Boolean isSourceForm) {
        List<VTD1_Survey__c> ediaries = new List<VTD1_Survey__c>();
        if (isSourceForm) {
            ediaries = [
                    SELECT Id, Name,
                            VTD1_Completion_Time__c,
                            VTR5_CompletedbyCaregiver__c,
                            VTR5_Clinician__c,
                            LastModifiedDate
                    FROM VTD1_Survey__c
                    WHERE VTD1_CSM__c = :caseId
                    AND RecordType.DeveloperName = 'VTR5_SourceForm'
                    ORDER BY VTD1_Completion_Time__c DESC NULLS LAST, VTD1_Due_Date__c DESC
            ];
        } else {
            ediaries = [
                    SELECT Id, Name,
                            VTD1_Completion_Time__c,
                            VTR2_Total_Score__c,
                            VTR5_CompletedbyCaregiver__c
                    FROM VTD1_Survey__c
                    WHERE VTD1_CSM__c = :caseId
                    AND RecordType.DeveloperName != 'VTR5_SourceForm'
                    ORDER BY VTD1_Completion_Time__c DESC NULLS LAST, VTD1_Due_Date__c DESC
            ];
        }
        return ediaries;
    }
}