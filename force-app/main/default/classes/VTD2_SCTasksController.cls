/**
 * Created by shume on 22/11/2018.
 */

public with sharing class VTD2_SCTasksController {

    @AuraEnabled
    public static List<VTD1_SC_Task__c> getSCTasksFromPool() {
        return getTasks();
    }

    @AuraEnabled
    public static List<VTD1_SC_Task__c> claimTasksRemote(List<Id> taskIds) {
        List<VTD1_SC_Task__c> tasksToUpdate = new List<VTD1_SC_Task__c>();
        List<VTD1_SC_Task__c> tasks = [
                SELECT
                        Id,
                        OwnerId
                FROM VTD1_SC_Task__c
                WHERE Id IN :taskIds
                    AND OwnerId != :UserInfo.getUserId()
                FOR UPDATE
        ];
        for (VTD1_SC_Task__c t : tasks) {
            if (t.OwnerId.getSobjectType().getDescribe().getName() == 'Group') {
                tasksToUpdate.add(t);
            }
        }
        for (VTD1_SC_Task__c t : tasksToUpdate) {
            t.OwnerId = UserInfo.getUserId();
        }
        try {
            update tasks;
            return getTasks();
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
        }

        /*if (tasks.size()!=taskIds.size()) {
            //add info about tasks taken
        }*/
    }

    private static List <VTD1_SC_Task__c> getTasks() {
        // user groups
        List<Id> groupIds = new List<Id>();
        for (GroupMember gm : [SELECT GroupId FROM GroupMember WHERE UserOrGroupId = :UserInfo.getUserId() AND Group.Type != 'Queue']) {
            groupIds.add(gm.Id);
        }
        return [
                SELECT
                        Id,
                        VTD1_Subject__c,
                        VTD1_CarePlanTemplate__c,
                        VTD1_CarePlanTemplate__r.Name,
                        FORMAT(VTD1_Due_Date__c)
                FROM VTD1_SC_Task__c
                WHERE OwnerId IN (
                        SELECT GroupId
                        FROM GroupMember
                        WHERE ((UserOrGroupId = :UserInfo.getUserId()
                            OR UserOrGroupId IN :groupIds)
                            AND Group.Type = 'Queue')
                )
                    AND VTD1_Due_Date__c != NULL
                    AND VTD1_CarePlanTemplate__c != NULL
                ORDER BY CreatedDate DESC
                LIMIT 100
        ];
    }
}