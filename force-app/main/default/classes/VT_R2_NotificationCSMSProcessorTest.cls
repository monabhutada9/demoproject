/**
 * Created by User on 19/05/21.
 */
@isTest
public with sharing class VT_R2_NotificationCSMSProcessorTest {

    public static void optInRemoteTest(){
        setUpMCSettings();
        List<VT_D1_Phone__c> phone3 = [SELECT Id FROM VT_D1_Phone__c LIMIT 1];
        System.assertNotEquals(0,phone3.size());
        Test.startTest();
        VT_R2_NotificationCSMSProcessor.optInRemote(phone3[0].Id);
        Test.stopTest();
    }

    public static void resendOptInRemoteTest(){
        setUpMCSettings();
        List<VT_D1_Phone__c> phone3 = [SELECT Id FROM VT_D1_Phone__c LIMIT 1];
        System.assertNotEquals(0,phone3.size());
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CalloutMock());
        VT_R2_NotificationCSMSProcessor.resendOptInRemote(phone3[0].Id);
        Test.stopTest();
    }
    public static void optOutRemoteTest(){
        setUpMCSettings();
        List<VT_D1_Phone__c> phone3 = [SELECT Id FROM VT_D1_Phone__c LIMIT 1];
        System.assertNotEquals(0,phone3.size());
        Test.startTest();
        VT_R2_NotificationCSMSProcessor.optOutRemote(phone3[0].Id);
        Test.stopTest();
    }
    public static void sendContactsRemoteTest(){
        setUpMCSettings();
        List<VT_D1_Phone__c> phone3 = [SELECT Id FROM VT_D1_Phone__c LIMIT 1];
        System.assertNotEquals(0,phone3.size());
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CalloutMock());
        Boolean isEx = false;
        try {
            VT_R2_NotificationCSMSProcessor.sendContactsRemote(phone3[0].Id);
        }catch (Exception ex){
            isEx = true;
        }
        //System.assert(isEx);
        Test.stopTest();
    }

    public with sharing class CalloutMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/xml');
            VT_R2_McloudBroadcaster.ResponseBean resB = new VT_R2_McloudBroadcaster.ResponseBean();
            resB.accessToken = 'Test';
            resB.expiresIn = 2;
            resB.message = 'message';
            resB.errorcode = 202;
            resB.documentation = 'documentation';
            resB.errors = new List<String>();
            resB.invalidSubscribers = new List<VT_R2_McloudBroadcaster.InvalidSubscriber>();
            resB.tokenId = '1111';
            res.setBody(JSON.serialize(resB));
            res.setStatusCode(200);
            return res;
        }
    }

    static void setUpMCSettings() {
        List<VTR2_McloudSettings__mdt> mcloudSettings = new List<VTR2_McloudSettings__mdt>();
        mcloudSettings.add(new VTR2_McloudSettings__mdt(VTR2_OutboundMessageKey_API_Triggered__c = 'NjU6Nzg6MA',
                VTR2_RestURI__c = 'https://mc2gwv28hdsr86gd-sldnjzvzzgm.rest.marketingcloudapis.com/',
                VTR2_ShortCode__c = '99808'));
        VT_R2_McloudBroadcaster.setMCsettings(mcloudSettings);
    }
}