/**
 * Created by Yuliya Yakushenkova on 10/28/2020.
 */

public without sharing class VT_R5_PISCRCalendarService {

    public class CalendarServiceException extends Exception {
    }

    private static VT_R5_PISCRCalendarService instance = new VT_R5_PISCRCalendarService();
    private final String VISIT_ACTION_RESCHEDULE = 'reschedule';
    private final String VISIT_ACTION_SCHEDULE = 'schedule';
    private final String VISIT_ACTION_CREATE = 'create';
    private final String VISIT_ACTION_CANCEL = 'cancel';

    public static VT_R5_PISCRCalendarService getInstance() {
        return instance;
    }

    public static Visit getConvertedVisit(String jsonObject) {
        return (Visit) JSON.deserialize(jsonObject, Visit.class);
    }

    public static List<VT_R5_PISCRCalendarService.Participant> getConvertedParticipants(String jsonObject) {
        return (List<VT_R5_PISCRCalendarService.Participant>)
                JSON.deserialize(jsonObject, List<VT_R5_PISCRCalendarService.Participant>.class);
    }

    public List<Visit> getPreviewVisitsAndMembersByUserId(String userId) {
        Map<Event, List<Visit_Member__c>> visitMembersByEvents =
                VT_R5_PISCRQueryService.getScheduledEventsWithVisitMembersByUserId(userId);
        return getVisitsByEventsAndVisitMembers(visitMembersByEvents);
    }

    public List<Visit> getVisitAndMembersByVisitIdAndUserId(String visitId, String userId) {
        Map<Event, List<Visit_Member__c>> visitMembersByEvents =
                VT_R5_PISCRQueryService.getScheduledEventWithVisitMembersByUserIdAndVisitId(userId, visitId);
        return getVisitsByEventsAndVisitMembers(visitMembersByEvents);
    }

    private List<Visit> getVisitsByEventsAndVisitMembers(Map<Event, List<Visit_Member__c>> visitMembersByEvents) {
        List<Visit> visits = new List<Visit>();
        for (Event event : visitMembersByEvents.keySet()) {
            List<Participant> participants = new List<Participant>();
            for (Visit_Member__c member : visitMembersByEvents.get(event)) {
                participants.add(new Participant(member, member.VTD1_Member_Type__c));
            }
            visits.add(new Visit(event, participants));
        }
        return visits;
    }

    public List<Participant> getAllPossibleParticipantsByVisitId(String visitId, String userId) {
        List<Event> events = VT_R5_PISCRQueryService.getScheduledEventsByUserIdAndVisitId(userId, visitId);
        if (events.isEmpty()) return new List<Participant>();

        return getAllPossibleParticipantsByCase(events.get(0).VTD1_Actual_Visit__r.VTD1_Case__r);
    }

    public List<Participant> getAllPossibleParticipantsByCaseId(String caseId) {
        Case patientCase = VT_R5_PatientQueryService.getCaseByCaseId(caseId);
        return getAllPossibleParticipantsByCase(patientCase);
    }

    public List<Participant> getAllPossibleParticipantsByCase(Case patientCase) {
        Set<Id> memberIds = new Set<Id>();
        List<String> caseRoles = new List<String>{
                'VTD1_Primary_PG__c',
                'VTD1_Secondary_PG__c',
                'VTD1_PI_user__c',
                'VTD1_Backup_PI_User__c',
                'VTR2_SiteCoordinator__c',
                'VTD1_Patient_User__c'
        };

        for (String role : caseRoles) memberIds.add(String.valueOf(patientCase.get(role)));

        Id caregiverUserId = VT_D1_PatientCaregiverBound.findCaregiverOfPatient(patientCase.VTD1_Patient_User__c);
        if (caregiverUserId != null) memberIds.add(caregiverUserId);

        List<Study_Team_Member__c> studyTeamMembers = [
                SELECT User__c, VTD1_Type__c, (
                        SELECT User__c
                        FROM Study_Team_Members__r
                ), (
                        SELECT VTR2_Associated_SubI__r.User__c
                        FROM Study_Site_Team_Members2__r
                ), (
                        SELECT VTD1_Associated_PG__r.User__c
                        FROM Study_Team_Member_Relationships1__r
                ), (
                        SELECT VTR2_Associated_SCr__r.User__c
                        FROM Study_Site_Team_Members1__r
                )
                FROM Study_Team_Member__c
                WHERE Id = :patientCase.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c OR
                (Study__c = :patientCase.VTD1_Study__c AND VTD1_Type__c = :VT_R4_ConstantsHelper_ProfilesSTM.RE_PROFILE_NAME)
        ];

        for (Study_Team_Member__c member : studyTeamMembers) {
            memberIds.add(member.User__c);
            for (Study_Team_Member__c backupPIMember : member.Study_Team_Members__r) {
                memberIds.add(backupPIMember.User__c);
            }
            for (Study_Site_Team_Member__c SubIMember : member.Study_Site_Team_Members2__r) {
                memberIds.add(SubIMember.VTR2_Associated_SubI__r.User__c);
            }
            for (Study_Site_Team_Member__c PGMember : member.Study_Team_Member_Relationships1__r) {
                memberIds.add(PGMember.VTD1_Associated_PG__r.User__c);
            }
            for (Study_Site_Team_Member__c SCRMember : member.Study_Site_Team_Members1__r) {
                memberIds.add(SCRMember.VTR2_Associated_SCr__r.User__c);
            }
        }

        List<Participant> participants = new List<Participant>();
        for (User u : [SELECT Id, Name, Profile.Name FROM User WHERE Id IN :memberIds ORDER BY Name]) {
            participants.add(new Participant(u.Id, u.Name, u.Profile.Name));
        }
        return participants;
    }

    public void cancelVisit(String visitId, Visit visit) {
        upsertActualVisit(visit, visitId, null, VISIT_ACTION_CANCEL);
    }

    public void rescheduleVisit(String visitId, Visit visit, List<Participant> externalParticipants, Set<Id> membersIds) {
        if (!isDateTimeStillAvailable(visit, membersIds, visitId)) throw new CalendarServiceException();

        insertOrDeleteVisitMembersByVisitIdAndUserIds(visitId, membersIds);
        insertExternalVisitMembersByVisitId(visitId, externalParticipants);

        upsertActualVisit(visit, visitId, null, VISIT_ACTION_RESCHEDULE);
    }

    public void scheduleVisit(Id caseId, Visit visit, List<Participant> externalParticipants, Set<Id> membersIds) {
        if (!isDateTimeStillAvailable(visit, membersIds, null)) {
            throw new CalendarServiceException();
        }

        Id visitId = upsertActualVisit(visit, null, caseId, VISIT_ACTION_CREATE);

        insertOrDeleteVisitMembersByVisitIdAndUserIds(visitId, membersIds);
        insertExternalVisitMembersByVisitId(visitId, externalParticipants);

        upsertActualVisit(visit, visitId, null, VISIT_ACTION_SCHEDULE);
    }

    private Id upsertActualVisit(Visit visit, String visitId, String caseId, String action) {
        VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
        if (visitId != null) actualVisit.Id = visitId;

        if (action == VISIT_ACTION_RESCHEDULE) {
            actualVisit.VTD1_Reason_for_Reschedule__c = visit.reason;
            actualVisit.VTD1_Scheduled_Date_Time__c = visit.getScheduledDateTime();
            actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED;
        } else if (action == VISIT_ACTION_CANCEL) {
            actualVisit.VTD1_Reason_for_Cancellation__c = visit.reason;
            actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED;
        } else if (action == VISIT_ACTION_CREATE) {
            actualVisit.Name = visit.name;
            actualVisit.Unscheduled_Visits__c = caseId;
            actualVisit.VTD1_Reason_for_Request__c = visit.reason;
            actualVisit.VTD1_Pre_Visit_Instructions__c = visit.instructions;
            actualVisit.VTD1_Unscheduled_Visit_Type__c = visit.unscheduledVisitType;
            actualVisit.VTR2_Modality__c = visit.modality;
            actualVisit.VTR2_Televisit__c = visit.isTelevisit;
            actualVisit.RecordTypeId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_ACTUAL_VISIT_UNSCHEDULED;
        } else if (action == VISIT_ACTION_SCHEDULE) {
            actualVisit.VTD1_Scheduled_Date_Time__c = visit.getScheduledDateTime();
            actualVisit.VTD1_Unscheduled_Visit_Duration__c = visit.duration;
            actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED;
        }
        actualVisit.VTD1_Date_Time_Stamp__c = Datetime.now();
        upsert actualVisit;

        return actualVisit.Id;
    }

    public void insertOrDeleteVisitMembersByVisitIdAndUserIds(String visitId, Set<Id> toInsertOrToDeleteUserIds) {
        List<Visit_Member__c> currentVisitMembers = VT_R5_PISCRQueryService.getVisitMembersByVisitId(visitId);

        System.debug(JSON.serializePretty(toInsertOrToDeleteUserIds));
        List<Visit_Member__c> visitMembersToDelete = new List<Visit_Member__c>();
        for (Visit_Member__c member : currentVisitMembers) {
            Id memberId = member.VTD1_Participant_User__c != null ? member.VTD1_Participant_User__c : member.Id;
            if (toInsertOrToDeleteUserIds.contains(memberId)) {
                visitMembersToDelete.add(member);
                toInsertOrToDeleteUserIds.remove(memberId);
            }
        }
        List<Visit_Member__c> visitMembersToInsert = new List<Visit_Member__c>();
        for (Id userId : toInsertOrToDeleteUserIds) {
            visitMembersToInsert.add(new Visit_Member__c(
                    VTD1_Actual_Visit__c = visitId,
                    VTD1_Participant_User__c = userId
            ));
        }

        insert visitMembersToInsert;
        delete visitMembersToDelete;
    }

    private void insertExternalVisitMembersByVisitId(String visitId, List<Participant> externalParticipants) {
        List<Visit_Member__c> visitMembers = new List<Visit_Member__c>();
        for (Participant participant : externalParticipants) {
            visitMembers.add(new Visit_Member__c(
                    VTD1_Actual_Visit__c = visitId,
                    VTD1_Name_HHN_Phleb__c = participant.name,
                    VTD1_External_Participant_Type__c = 'Other',
                    //VTR2_ReasonForParticipation__c = visitParticipant.reasonForParticipation,
                    VTD1_External_Participant_Email__c = participant.email
            ));
        }
        insert visitMembers;
    }

    private Boolean isDateTimeStillAvailable(Visit visit, Set<Id> membersIds, Id visitId) {
        Datetime startDatetime = visit.getScheduledDateTime();
        Datetime endDatetime = startDatetime.addMinutes(visit.duration.intValue());
        List<Event> events = [
                SELECT Id
                FROM Event
                WHERE ((StartDateTime >= :startDatetime AND StartDateTime < :endDatetime)
                OR (EndDateTime > :startDatetime AND EndDateTime <= :endDatetime)
                OR (StartDateTime <= :startDatetime AND EndDateTime >= :endDatetime))
                AND IsChild = FALSE
                AND OwnerId IN :membersIds
                AND WhatId != :visitId
        ];

        if (events.isEmpty()) return true;
        return false;
    }

    public class Visit {
        private String id;
        private String name;
        private Long startDateTime;
        private String startDateTimeLabel;
        private Long endDateTime;
        private String notes;
        private Decimal duration;
        private String instructions;
        private String reason;
        private String modality;
        private Boolean isTelevisit;
        private String scheduledDateTime;
        private String unscheduledVisitType;
        private List<Participant> participants;

        private Visit(Event event) {
            this.id = event.VTD1_Actual_Visit__c;
            this.name = event.VTD1_Actual_Visit__r.Name;
            setDates(event.StartDateTime, event.EndDateTime);
        }

        private Visit(Event event, List<Participant> participants) {
            this(event);
            this.notes = event.VTD1_Actual_Visit__r.VTD1_Visit_Description__c;
            this.duration = event.VTD1_Actual_Visit__r.VTD1_Visit_Duration__c;
            this.instructions = event.VTD1_Actual_Visit__r.VTD1_Pre_Visit_Instructions__c;
            this.unscheduledVisitType = event.VTD1_Actual_Visit__r.VTD1_Unscheduled_Visit_Type__c;
            this.participants = participants;
        }

        private void setDates(Datetime startDateTime, Datetime endDateTime) {
            Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());
            if (startDateTime != null) this.startDateTime = startDateTime.getTime() + timeOffset;
            //this.startDateTimeLabel = startDateTime != null ? startDateTime.format() : '';
            if (endDateTime != null) this.endDateTime = endDateTime.getTime() + timeOffset;
        }

        private Datetime getScheduledDateTime() {
            if (scheduledDateTime != null) {
                return Datetime.valueOfGmt(scheduledDateTime.replace('T', ' '));
            }
            return null;
        }
    }

    public class Participant {
        private String userId;
        private String name;
        private String photoURL;
        private String role;
        private String email;
        private String externalParticipantType;

        private Participant(Visit_Member__c member) {
            this.userId = member.VTD1_Participant_User__c;
            if (this.userId == null) this.userId = member.Id;
            this.name = member.Visit_Member_Name__c;
            String photoUrl = '';
            if (member.VTD1_Participant_User__c != null && !Test.isRunningTest()) {
                photoUrl = ConnectApi.UserProfiles.getPhoto(null, member.VTD1_Participant_User__c).fullEmailPhotoUrl;
            }
            this.photoURL = photoUrl.contains('default_profile') ? null : photoUrl;
            this.externalParticipantType = member.VTD1_External_Participant_Type__c;
        }

        private Participant(Visit_Member__c member, String role) {
            this(member);
            this.role = role;
        }

        private Participant(String id, String name, String role) {
            this.userId = id;
            this.name = name;
            this.role = role;
        }
    }
}