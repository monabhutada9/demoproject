/**
 * Created by Alexander Medentsov on 8/3/2020.
 */

public without sharing class VT_R5_DueOrMissedDiariesTaskerBatch extends VT_R5_HerokuScheduler.AbstractScheduler implements Database.Batchable<SObject>, Database.AllowsCallouts, Schedulable {

    private static final Set<String> eDIARY_STATUSES = new Set<String>{
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_DUE_SOON,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_READY_TO_START,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_IN_PROGRESS,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_MISSED,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_EDIARY_MISSED
    };

    private static final String BUILDER_STATUS_AFTER = 'AFTER the Expiration Date';
    private static final String BUILDER_TYPE = 'VTR5_ExpirationAlert';
    private Map<Id, VTR5_eDiaryNotificationBuilder__c> buildersMap;
    private Datetime startCheck;
    private Datetime endCheck;
    private Datetime DATE_TIME;
    private Map<String, Set<Id>> studyEdiaryKeyToBuilderIdsMap;

    public VT_R5_DueOrMissedDiariesTaskerBatch() {}

    public VT_R5_DueOrMissedDiariesTaskerBatch(Datetime runDatetime) {
        this.DATE_TIME = runDatetime.addMinutes(2);

//        Datetime minOffset = this.DATE_TIME.addHours(-24);
//        Datetime maxOffset = this.DATE_TIME.addHours(24);

        this.startCheck = this.DATE_TIME;
        this.endCheck = this.DATE_TIME.addMinutes(9).addSeconds(1);

        this.buildersMap = (Map<Id, VTR5_eDiaryNotificationBuilder__c>) new VT_Stubber.ResultStub(
                'Notifications_Builder',
                new Map<Id, VTR5_eDiaryNotificationBuilder__c>([
                        SELECT VTR5_AlertTime__c,
                                VTR5_DiaryStudyKey__c,
                                VTR5_NotificationMessage__c,
                                VTR5_NotificationRecipient__c,
                                VTR5_Notification_Title__c,
                                VTR5_Offset__c,
                                VTR5_OperationType__c,
                                VTR5_RecipientType__c
                        FROM VTR5_eDiaryNotificationBuilder__c
                        WHERE VTR5_Active__c = TRUE
                        AND RecordType.DeveloperName = : BUILDER_TYPE
                ])
        ).getResult();
        this.studyEdiaryKeyToBuilderIdsMap = new Map<String, Set<Id>>();
        for (VTR5_eDiaryNotificationBuilder__c builder : this.buildersMap.values()) {
            if (!this.studyEdiaryKeyToBuilderIdsMap.containsKey(builder.VTR5_DiaryStudyKey__c)) {
                this.studyEdiaryKeyToBuilderIdsMap.put(builder.VTR5_DiaryStudyKey__c, new Set<Id>());
            }
            this.studyEdiaryKeyToBuilderIdsMap.get(builder.VTR5_DiaryStudyKey__c).add(builder.Id);
        }
    }

    public void execute(SchedulableContext sc) {
        System.abortJob(sc.getTriggerId());
        Datetime now = System.now();
        Database.executeBatch(new VT_R5_DueOrMissedDiariesTaskerBatch(now.addSeconds(-1)), 200);
        Datetime nextRun = now.addMinutes(10);
        String day = String.valueOf(nextRun.day());
        String month = String.valueOf(nextRun.month());
        String hour = String.valueOf(nextRun.hour());
        String minute = String.valueOf(nextRun.minute());
        String year = String.valueOf(nextRun.year());

        String strJobName = 'Job VT_R5_DueOrMissedDiariesTaskerBatch ' + nextRun;
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;

        if (!Test.isRunningTest()) {
            System.schedule(strJobName, strSchedule, new VT_R5_DueOrMissedDiariesTaskerBatch());
        }
    }

    public Iterable<SObject> start(Database.BatchableContext bc) {

        Datetime minOffset = this.DATE_TIME.addHours(-24);
        Datetime maxOffset = this.DATE_TIME.addHours(24).addMinutes(9);
        Set<String> studyEdiaryKeyToBuilderIdsKeys = this.studyEdiaryKeyToBuilderIdsMap.keySet();
        return (Iterable<SObject>) new VT_Stubber.ResultStub('eDiaryBuilder_QueryLocator', [
                SELECT VTR5_eCoaAlertsKey__c
                FROM VTD1_Survey__c
                WHERE VTR5_eCoaAlertsKey__c != NULL
                    AND VTD1_Status__c IN :eDIARY_STATUSES
                    AND VTR5_DiaryStudyKey__c IN :studyEdiaryKeyToBuilderIdsKeys
                    AND VTD1_Due_Date__c >= :minOffset
                    AND VTD1_Due_Date__c <= :maxOffset
                GROUP BY VTR5_eCoaAlertsKey__c
        ]).getResult();

    }

    public void execute(Database.BatchableContext bc, List<SObject> ediaries) {
        List<VT_R5_HerokuScheduler.HerokuSchedulerPayload> notifications = new List<VT_R5_HerokuScheduler.HerokuSchedulerPayload>();

        for (SObject surv : ediaries) {
            String diaryStudyKey = getEDiaryStudyKey(String.valueOf(surv.get('VTR5_eCoaAlertsKey__c')));
            Datetime surveyDueDate = getEDiaryDueDate(String.valueOf(surv.get('VTR5_eCoaAlertsKey__c')), diaryStudyKey);

            if (this.studyEdiaryKeyToBuilderIdsMap.get(diaryStudyKey) == null) { continue; }

            for (Id builderId : this.studyEdiaryKeyToBuilderIdsMap.get(diaryStudyKey)) {
                Integer builderOffset = this.buildersMap.get(builderId).VTR5_OperationType__c == BUILDER_STATUS_AFTER
                        ? (Integer) this.buildersMap.get(builderId).VTR5_Offset__c
                        : - (Integer) this.buildersMap.get(builderId).VTR5_Offset__c;

                Datetime surveyDueDateWithOffset = surveyDueDate.addMinutes(builderOffset);

                if (surveyDueDateWithOffset <= endCheck && surveyDueDateWithOffset >= startCheck) {
                    VT_R5_eDiaryAlertsService.RecipientType recipient = this.buildersMap.get(builderId).VTR5_RecipientType__c == 'Site Staff'
                            ? VT_R5_eDiaryAlertsService.RecipientType.SITE_STAFF
                            : VT_R5_eDiaryAlertsService.RecipientType.PT_CG;

                    notifications.add(new VT_R5_HerokuScheduler.HerokuSchedulerPayload(
                            surveyDueDateWithOffset,
                            'VT_R5_eDiaryAlertsService',
                            JSON.serialize(new VT_R5_eDiaryAlertsService.AlertRequestInfo(builderId, recipient, surveyDueDate))
                    ));
                }
            }
        }
        schedule(notifications);
    }

    public void finish(Database.BatchableContext bc) {
    }

    private static String getEDiaryStudyKey(String sourceString) {
        String regExp = '([0-9]{4})-([0-9]{2})-([0-9]{2})\\s([0-9]{2}):([0-9]{2}):([0-9]{2})';
        return sourceString.replace('*-*', '').replaceAll(regExp, '');
    }

    private static Datetime getEDiaryDueDate(String sourceString, String eDiaryStudyKey) {
        return convertDatetime(sourceString.replace('*-*', '').replace(eDiaryStudyKey, ''));
    }

    private static Datetime convertDatetime(String stringDatetime) {
        List<String> parts = stringDatetime.split(' ');
        List<String> dateParts = String.valueOf(parts[0]).split('-');
        List<String> timeParts = String.valueOf(parts[1]).split(':');
        return Datetime.newInstanceGmt(
                Integer.valueOf(dateParts[0]),
                Integer.valueOf(dateParts[1]),
                Integer.valueOf(dateParts[2]),
                Integer.valueOf(timeParts[0]),
                Integer.valueOf(timeParts[1]),
                Integer.valueOf(timeParts[2])
        );
    }
}