/**
* @author: Carl Judge
* @date: 06-Jun-18
* @description: On insert of protocol deviation,
**/

public without sharing class VT_D1_ProtocolDeviationSharingHandler {

    private static final Set<String> EXCLUDED_PROFILES = new Set<String> {
        'Patient Guide',
        'Primary Investigator',
        'Site Coordinator'
    };

    private List<VTD1_Protocol_Deviation__c> recs;
    private List<VTD1_Protocol_Deviation__c> patientRecs = new List<VTD1_Protocol_Deviation__c>();
    private List<VTD1_Protocol_Deviation__c> siteRecs = new List<VTD1_Protocol_Deviation__c>();
    private Map<String, VTD1_Protocol_Deviation__Share> newShares = new Map<String, VTD1_Protocol_Deviation__Share>(); // map by recId+userId to avoid duplicates

    private Map<Id, HealthCloudGA__CarePlanTemplate__c> studyMap = new Map<Id, HealthCloudGA__CarePlanTemplate__c>();
    private Map<Id, Case> patientsById;
    private Map<Id, List<Study_Site_Team_Member__c>> siteMembersBySite = new Map<Id, List<Study_Site_Team_Member__c>>();



    public void shareRecords(List<VTD1_Protocol_Deviation__c> recs) {
        this.recs = recs;

        getStudyMap();
        getSharesFromStudy();
        getRecsWithPatientOrSite();

        if (!this.patientRecs.isEmpty()) {
            getPatientsById();
            getSharesFromPatients();
        }
        if (!this.siteRecs.isEmpty()) {
    //        getSiteMembersBySite();
     //       getSharesFromSiteMembers();
            getSharesFromSites();
        }

        if (!this.newShares.isEmpty()) { Database.insert(newShares.values(), false); }
    }

    private void getStudyMap() {
        List<Id> studyIds = new List<Id>();
        for (VTD1_Protocol_Deviation__c item : this.recs) {
            if (item.VTD1_StudyId__c != null) { studyIds.add(item.VTD1_StudyId__c); }
        }

        if (! studyIds.isEmpty()) {
            this.studyMap = new Map<Id, HealthCloudGA__CarePlanTemplate__c>([
                SELECT Id, VTD1_Project_Lead__c, VTD1_Virtual_Trial_Study_Lead__c, VTD1_Regulatory_Specialist__c,
                    VTD1_Remote_CRA__c, VTD1_Virtual_Trial_head_of_Operations__c,
                (SELECT User__c FROM Study_Team_Members__r WHERE User__r.Profile.Name NOT IN :EXCLUDED_PROFILES)
                FROM HealthCloudGA__CarePlanTemplate__c
                WHERE Id IN :studyIds
            ]);
        }
    }

    private void getSharesFromStudy() {
        for (VTD1_Protocol_Deviation__c item : this.recs) {
            if (this.studyMap.containsKey(item.VTD1_StudyId__c)) {
                HealthCloudGA__CarePlanTemplate__c study = this.studyMap.get(item.VTD1_StudyId__c);

          //      addShare(item.Id, study.VTD1_Project_Lead__c);
                addShare(item.Id, study.VTD1_Virtual_Trial_Study_Lead__c);
                addShare(item.Id, study.VTD1_Regulatory_Specialist__c);
                addShare(item.Id, study.VTD1_Remote_CRA__c);
                addShare(item.Id, study.VTD1_Virtual_Trial_head_of_Operations__c);

                for (Study_Team_Member__c member : study.Study_Team_Members__r) {
                    addShare(item.Id, member.User__c);
                }
            }
        }
    }

    private void addShare(Id recId, Id uId) {
        if (recId != null && uId != null) {
            this.newShares.put('' + recId + uId, new VTD1_Protocol_Deviation__Share(
                ParentId = recId,
                RowCause = Schema.VTD1_Protocol_Deviation__Share.RowCause.VTD1_PD_Form_Sharing_Reason__c,
                AccessLevel = 'Edit',
                UserOrGroupId = uId
            ));
        }
    }

    private void getRecsWithPatientOrSite() {
        for (VTD1_Protocol_Deviation__c item : this.recs) {
            if (item.VTD1_Subject_ID__c != null) {
                this.patientRecs.add(item);
            } else if (item.VTD1_Virtual_Site__c != null) {
                this.siteRecs.add(item);
            }
        }
    }

    private void getPatientsById() {
        List<Id> caseIds = new List<Id>();
        for (VTD1_Protocol_Deviation__c item : this.patientRecs) {
            caseIds.add(item.VTD1_Subject_ID__c);
        }
        this.patientsById = new Map<Id, Case>([
            SELECT Id, VTD1_Primary_PG__c, VTD1_Secondary_PG__c, VTD1_PI_user__c, VTD1_Backup_PI_User__c,
                VTD1_Virtual_Site__r.VTD1_Study_Team_Member__r.User__c
            FROM Case
            WHERE Id IN :caseIds
        ]);
    }

    private void getSharesFromPatients() {
        for (VTD1_Protocol_Deviation__c item : this.patientRecs) {
            if (this.patientsById.containsKey(item.VTD1_Subject_ID__c)) {
                Case cas = this.patientsById.get(item.VTD1_Subject_ID__c);
                if (cas.VTD1_Primary_PG__c != null) { addShare(item.Id, cas.VTD1_Primary_PG__c); }
                if (cas.VTD1_Secondary_PG__c != null) { addShare(item.Id, cas.VTD1_Secondary_PG__c); }
                if (cas.VTD1_PI_user__c != null) { addShare(item.Id, cas.VTD1_PI_user__c); }
                if (cas.VTD1_Backup_PI_User__c != null) { addShare(item.Id, cas.VTD1_Backup_PI_User__c); }
                if (cas.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__r.User__c != null) {
                    addShare(item.Id, cas.VTD1_Virtual_Site__r.VTD1_Study_Team_Member__r.User__c);
                }
            }
        }
    }
/*
    private void getSiteMembersBySite() {
        List<Id> siteIds = new List<Id>();
        for (VTD1_Protocol_Deviation__c item : this.siteRecs) {
            if (item.VTD1_Virtual_Site__c != null) { siteIds.add(item.VTD1_Virtual_Site__c); }
        }

        if (! siteIds.isEmpty()) {
            for (Study_Site_Team_Member__c item : [
                SELECT VTD1_SiteID__c, VTD1_User_Id__c
                FROM Study_Site_Team_Member__c
                WHERE VTD1_SiteID__c IN :siteIds
            ]) {
                if (! this.siteMembersBySite.containsKey(item.VTD1_SiteID__c)) {
                    this.siteMembersBySite.put(item.VTD1_SiteID__c, new List<Study_Site_Team_Member__c>());
                }
                this.siteMembersBySite.get(item.VTD1_SiteID__c).add(item);
            }
        }
    }

    private void getSharesFromSiteMembers() {
        for (VTD1_Protocol_Deviation__c item : this.siteRecs) {
            if (this.siteMembersBySite.containsKey(item.VTD1_Virtual_Site__c)) {
                for (Study_Site_Team_Member__c member : this.siteMembersBySite.get(item.VTD1_Virtual_Site__c)) {
                    addShare(item.Id, member.VTD1_User_Id__c);
                }
            }
        }
    } */

    private void getSharesFromSites() {
        List<Id> siteIds = new List<Id>();
        for (VTD1_Protocol_Deviation__c item : this.siteRecs) {
            siteIds.add(item.VTD1_Virtual_Site__c);
        }
        if (! siteIds.isEmpty()) {
            Map<Id, Virtual_Site__c> siteMap = new Map<Id, Virtual_Site__c>([
                SELECT Id, VTD1_Study_Team_Member__r.User__c
                FROM Virtual_Site__c
                WHERE Id IN :siteIds AND VTD1_Study_Team_Member__c != null
            ]);

            for (VTD1_Protocol_Deviation__c item : this.siteRecs) {
                if (siteMap.containsKey(item.VTD1_Virtual_Site__c)) {
                    addShare(item.Id, siteMap.get(item.VTD1_Virtual_Site__c).VTD1_Study_Team_Member__r.User__c);
                }
            }
        }
    }
}