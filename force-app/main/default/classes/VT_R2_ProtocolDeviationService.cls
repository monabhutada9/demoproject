/**
 * Created by User on 19/05/02.
 */

public with sharing class VT_R2_ProtocolDeviationService {
    private static String OBJECT_NAME_PROTOCOL_DEVIATION = 'VTD1_Protocol_Deviation__c';

    public static Map<String, List<PicklistValueWrapper>> getPicklistValues(List<String> picklistFields) {
        Map<String, List<PicklistValueWrapper>> picklistWrapperMap = new Map<String, List<PicklistValueWrapper>>();
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(new List<String>{OBJECT_NAME_PROTOCOL_DEVIATION});
        for (Schema.DescribeSobjectResult res : results) {
            for (String fieldName : picklistFields) {
                List<PicklistValueWrapper> picklistFieldWrapper = new List<PicklistValueWrapper>();
                for (Schema.PicklistEntry entry : res.fields.getMap().get(fieldName).getDescribe().getPicklistValues()) {
                    if (entry.isActive()) {
                        picklistFieldWrapper.add(new PicklistValueWrapper(entry.getLabel(), entry.getValue()));
                    }
                }
                picklistWrapperMap.put(fieldName,picklistFieldWrapper);
            }
        }
        return picklistWrapperMap;
    }

//-------------------------------------- WRAPPERS --------------------------------------------------//

    public class PicklistValueWrapper {
        @AuraEnabled public String label { get;set; }
        @AuraEnabled public String value { get; set; }

        public PicklistValueWrapper(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
}