/**
* @author: N.Arbatskiy
* @date: 04-May-20
* @description: Class to create JSON body for HTTP request (eCOA API Update Subject Blind).
**/

public class VT_R5_RequestBuilder_eCoaUpdateBlind implements VT_D1_RequestBuilder {
    public String email;
    public String phone;
    public String username;


    // mulesoft fields
    public String studyGuid;
    public String orgGuid = eCOA_IntegrationDetails__c.getInstance().eCOA_OrgGuid__c;

    public VT_R5_RequestBuilder_eCoaUpdateBlind(User user) {
        email = user.Email;
        studyGuid = user.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eCOA_Guid__c;

    }

    public String buildRequestBody() {
        return JSON.serialize(this, true);
    }
}