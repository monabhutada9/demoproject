/**
* @author: Carl Judge
* @date: 17-Aug-20
**/

public class VT_R5_eCoaSubjectSchedule extends VT_R5_eCoaAbstractAction {
    private List<Id> userIds;
    private Boolean isStopEvent = false;
    private Integer delay = VT_R4_ConstantsHelper_Protocol_ePRO.DIARY_SETTINGS.VTR5_ScheduleResponseDelay__c.intValue();

    public VT_R5_eCoaSubjectSchedule(List<Id> userIds, Boolean isStopEvent) {
        this.userIds = userIds;
        this.isStopEvent = isStopEvent;
    }
    public VT_R5_eCoaSubjectSchedule(List<Id> userIds) {
        this.userIds = userIds;
    }

    public VT_R5_eCoaSubjectSchedule setDelay(Integer delay) {
        this.delay = delay;
        return this;
    }

    public VT_R5_eCoaSubjectSchedule setIsStopEvent(Boolean isStopEvent) {
        this.isStopEvent = isStopEvent;
        return this;
    }

    public override VT_D1_HTTPConnectionHandler.Result doCallout() {
        String endpoint = VT_R4_ConstantsHelper_Integrations.ENDPOINT_URL_ECOA_SCHEDULE;
        String method = 'POST';
        String action = 'eCOA Get Subject Schedule';

        return this.userIds.isEmpty() ? null : new VT_R5_eCoaApiRequest()
            .setEndpoint(endpoint)
            .setMethod(method)
            .setAction(action)
            .setBody(new VT_R5_RequestBuilder_SubjectSchedule(this.userIds, this.isStopEvent, this.delay))
            .setSkipLogging(this.skipLogging)
            .send();
    }

    public override Type getType() {
        return VT_R5_eCoaSubjectSchedule.class;
    }
}