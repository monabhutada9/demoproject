/**
 * Created by user on 24.09.2018.
 */

public with sharing class VT_D1_TestDataCleaner implements Schedulable {
    //Integer index;
    List <Integer> indeces;
    Integer attempt = 0;
    public Boolean complete = false;
    public VT_D1_TestDataCleaner(List <Integer> indeces, Integer attempt) {
        this.indeces = indeces;
        this.attempt = attempt;
    }
    public void run() {
        Datetime dt = System.now();
        Integer seconds = dt.second();
        Integer minutes = dt.minute();
        Integer hours = dt.hour();

        seconds += 30;
        if (seconds >= 60) {
            seconds -= 30;
            minutes += 1;
        }

        if (minutes >= 60) {
            minutes -= 60;
            hours += 1;
        }

        System.schedule('CG_' + indeces[0], seconds + ' ' + minutes + ' ' + hours + ' * * ?', this);
    }
    public void execute(SchedulableContext sc) {
        String jobName = 'CG_' + indeces[0];
        List<CronTrigger> jobsToAbort = [select Id from CronTrigger where CronJobDetail.Name = :jobName];
        System.debug('to abort = ' + jobsToAbort);
        for (CronTrigger ct : jobsToAbort) {
            System.abortJob(ct.id);
        }

        String name = 'TestProt-00'+String.valueOf(indeces[0]);

        List <HealthCloudGA__CarePlanTemplate__c> templates = [select VTD1_Legal_Hold__c  from HealthCloudGA__CarePlanTemplate__c where Name = :name limit 1];
        if (!templates.isEmpty() && templates[0].VTD1_Legal_Hold__c) {
            templates[0].VTD1_Legal_Hold__c = false;
            System.debug('removed VTD1_Legal_Hold__c');
            update templates[0];
        }

        for (Integer i : indeces) {
            System.debug('remove ' + i);
            VT_D1_TestData.removePatientCandidate(i);
            VT_D1_TestData.removePatientCandidate(i, 'AF', 'AL');
        }

        List <String> names = new List<String>();
        for (Integer i : indeces) {
            names.add('F' + i + ' L' + i);
            names.add('AF' + i + ' AL' + i);
        }
        List <Account> accounts = [select Id from Account where Name in :names];
        if (!accounts.isEmpty() && attempt < 5) {
            attempt ++;
            run();
        } else {
            deleteStudy(indeces[0]);
            complete = true;
        }
    }
    @future
    public static void deleteStudy(Integer counter) {
        VT_D1_TestData.deleteTestStudy(counter);
    }
}