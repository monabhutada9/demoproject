public class VT_D1_PatientPaymentsControllerRemote {
    @AuraEnabled
    public static List<VTD1_Patient_Payment__c> getPatientPayments(String filter) {
        System.debug('VT_D1_PatientPaymentsControllerRemote filter: ' + filter);
        String recTypeName;
        String status;
        String id;
        if (!String.isBlank(filter)) {
            List<String> elements = filter.split('-');//&#124;
            System.debug(elements);
            if (elements != null && !elements.isEmpty()) {
                if (elements[0] == 'type')  recTypeName = elements.size() > 2 ? elements[1] + '\' OR RecordType.Name = \'' + elements[2]  : elements[1];
                if (elements[0] == 'status') status = elements[1];
                if (elements[0] == 'id') id = elements[1];
            }
        }
        List<VTD1_Patient_Payment__c> paymentsList = new List<VTD1_Patient_Payment__c>();
        User patient = [SELECT Contact.VTD1_Clinical_Study_Membership__c, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
        List<String> fields = new List<String> {
                'Name',
                'RecordTypeId',
                'RecordType.Name',
                'toLabel(RecordType.Name) Label',
                'VTD1_Activity_Complete__c',
                'VTD1_Amount_Formula__c',
                'VTD1_Status__c',
                'VTD1_Amount__c',
                'VTD1_Clinical_Study_Membership__c',
                'VTD1_Currency__c',
                'VTD1_Description__c',
                'VTD1_Patient_Name__c',
                'VTD1_Payment_Issued__c',
                'VTD1_Payment_Mechanism__c',
                'VTD1_Payment_Received__c',
                'VTD1_PG_Approved__c',
                'VTD1_PG_Approver__c',
                'VTD1_Protocol_Payment__c',
                'LastModifiedDate',
                'VTD1_Protocol_Payment__r.VTD1_Description__c'
        };
        String query = 'SELECT ' + String.join(fields, ',') + ' FROM VTD1_Patient_Payment__c ';
        if((patient.Profile.Name == VT_D1_ConstantsHelper.PATIENT_PROFILE_NAME) ||
                (patient.Profile.Name == VT_D1_ConstantsHelper.CAREGIVER_PROFILE_NAME)){
            query += ' WHERE VTD1_Clinical_Study_Membership__c =\'' + patient.Contact.VTD1_Clinical_Study_Membership__c + '\'';
            if (!String.isBlank(status)) query += ' AND VTD1_Status__c = \'' + status + '\'';

            if (!String.isBlank(recTypeName)) query += ' AND ( RecordType.Name = \'' + recTypeName + '\' )';

            if (String.isNotBlank(id)) query += ' AND Id = \'' + id + '\'';
        } else {
            if (!String.isBlank(status)) query += ' WHERE VTD1_Status__c = \'' + status + '\'';
            if (!String.isBlank(recTypeName)) query += ' WHERE RecordType.Name = \'' + recTypeName + '\'';
            if (String.isNotBlank(id)) query += ' AND Id = \'' + id + '\'';
        }
        System.debug('+query+' + query);
        paymentsList = Database.query(query);
        System.debug('+paymentsList+' + paymentsList.size());
        VT_D1_TranslateHelper.translate(paymentsList);
        //System.debug('paymentsList' + paymentsList);
        return paymentsList;
    }
}