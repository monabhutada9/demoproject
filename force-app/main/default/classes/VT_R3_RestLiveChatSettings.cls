/**
 * Created by Alexander Komarov on 29.05.2019.
 */

@RestResource(UrlMapping='/LiveChatSettings/*')
global without sharing class VT_R3_RestLiveChatSettings {
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();

    @HttpGet
    global static String getSettings() {
        LiveChatSettings settings = new LiveChatSettings();

        settings.orgId = VT_D1_SkillsBasedRouting.getOrganizationId();
        settings.deploymentId = VT_D1_SkillsBasedRouting.getLiveChatDeploymentId();
        settings.chatApiEndpoint = VT_D1_SkillsBasedRouting.getLiveChatUrl();

        Map<String, Id> skillBasedButtons = VT_D1_SkillsBasedRouting.getChatbuttonIdMap();
        if (skillBasedButtons.containsKey('tertiary')) {
            settings.buttonId = skillBasedButtons.get('tertiary');
        } else if (skillBasedButtons.containsKey('secondary')) {
            settings.buttonId = skillBasedButtons.get('secondary');
        } else if (skillBasedButtons.containsKey('primary')) {
            settings.buttonId = skillBasedButtons.get('primary');
        }


        if (settings.buttonId != null) settings.buttonId = settings.buttonId.substring(0,15);


        User visitor = [SELECT Name, Email FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];

        settings.visitorEmail = visitor.Email;
        settings.visitorName = visitor.Name;


        cr.buildResponse(settings);
        return JSON.serialize(cr, true);
    }

    private class LiveChatSettings {
        public String deploymentId;
        public String buttonId;
        public String orgId;
        public String chatApiEndpoint;
        public String visitorName;
        public String visitorEmail;
    }
}