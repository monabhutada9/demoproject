public with sharing class TaskListController {
    @AuraEnabled
    public static List<Task> getTasks(String status, String category) {
        List<Task> contacts;
        if(category == null){
            contacts = [
                    SELECT Id, Subject, Status, Priority, Category__c, ActivityDate
                    FROM Task
                    WHERE status=:status and OwnerId=:UserInfo.getUserID()
                    ORDER BY ActivityDate
            ];
        } else {
            contacts = [
                    SELECT Id, Subject, Status, Priority, Category__c, ActivityDate
                    FROM Task
                    WHERE status=:status and OwnerId=:UserInfo.getUserID() and Category__c=:category
                    ORDER BY ActivityDate
            ];
        }
        return contacts;
    }

    @AuraEnabled
    public static List <String> getCategories() {
        List <String> allOpts = new list <String> ();
        Schema.sObjectType objType = Task.getSObjectType();

        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();

        // Get a map of fields for the SObject
        map <String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();

        // Get the list of picklist values for this field.
        list <Schema.PicklistEntry> values =
                fieldMap.get('Category__c').getDescribe().getPickListValues();

        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        // allOpts.sort();
        return allOpts;
    }
}