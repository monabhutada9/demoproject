public with sharing class VT_R4_ConstantsHelper_KitsOrders {
    public static final String RT_PROTOCOL_KIT_AD_HOC_LAB = 'VTD1_Ad_hoc_Lab';
    public static final String RT_PROTOCOL_KIT_CONNECTED_DEVICES = 'VTD1_Connected_Devices';
    public static final String RT_PROTOCOL_KIT_IMP = 'VTD1_IMP';
    public static final String RT_PROTOCOL_KIT_LAB = 'VTD1_Lab';
    public static final String RT_PROTOCOL_KIT_STUDY_HUB_TABLET = 'VTD1_Study_Hub_Tablet';
    public static final String RT_PROTOCOL_KIT_WELCOME_TO_STUDY_PACKAGE = 'VTD1_Welcome_to_Study_Package';

    public static final String RT_PATIENT_KIT_AD_HOC_LAB = 'VTD1_Adhoc_Replacement_Lab';
    public static final String RT_PATIENT_KIT_CONNECTED_DEVICES = 'VTD1_Connected_Devices';
    public static final String RT_PATIENT_KIT_IMP = 'VTD1_IMP';
    public static final String RT_PATIENT_KIT_LAB = 'VTD1_Lab';
    public static final String RT_PATIENT_KIT_STUDY_HUB_TABLET = 'VTD1_Study_Hub_Tablet';
    public static final String RT_PATIENT_KIT_WELCOME_TO_STUDY_PACKAGE = 'VTD1_Welcome_to_Study_Package';

    public static final String RECORD_TYPE_ID_PROTOCOL_KIT_WELCOME_TO_STUDY_PACKAGE = VT_D1_HelperClass.getRTId('VTD1_Protocol_Kit__c', 'VTD1_Welcome_to_Study_Package');
    public static final String RECORD_TYPE_ID_PROTOCOL_KIT_LAB = VT_D1_HelperClass.getRTId('VTD1_Protocol_Kit__c', 'VTD1_Lab');
    public static final String RECORD_TYPE_ID_PROTOCOL_KIT_IMP = VT_D1_HelperClass.getRTId('VTD1_Protocol_Kit__c', 'VTD1_IMP');
    public static final String RECORD_TYPE_ID_ORDER_AD_HOC_DELIVERY = VT_D1_HelperClass.getRTId('VTD1_Order__c', 'VTD1_Ad_hoc_Delivery');
    public static final String RECORD_TYPE_ID_ORDER_PACKING_MATERIALS = VT_D1_HelperClass.getRTId('VTD1_Order__c', 'VTD1_Regular_Kit_Delivery');
    public static final String RECORD_TYPE_ID_ORDER_AD_HOC_RETURN = VT_D1_HelperClass.getRTId('VTD1_Order__c', 'VTD1_Ad_hoc_Return');
    public static final String RECORD_TYPE_ID_ORDER_AD_HOC_REPLACEMENT_DELIVERY = VT_D1_HelperClass.getRTId('VTD1_Order__c', 'VTD1_Ad_hoc_Replacement_Delivery');
    public static final String RECORD_TYPE_ID_PATIENT_KIT_IMP = VT_D1_HelperClass.getRTId('VTD1_Patient_Kit__c', 'VTD1_IMP');
    public static final String RECORD_TYPE_ID_PATIENT_KIT_PACKING_MATERIALS = VT_D1_HelperClass.getRTId('VTD1_Patient_Kit__c', 'VTR3_Packaging_Materials');
    public static final String RECORD_TYPE_ID_REPLACEMENT_ITEM_KIT = VT_D1_HelperClass.getRTId('VTD1_Replacement_Item__c', 'VTD1_Kit');
    public static final String RECORD_TYPE_ID_PATIENT_BLIND = VT_D1_HelperClass.getRTId('VTD1_Patient__c', 'Blind');
    public static final String RECORD_TYPE_ID_DEVICE_AD_HOC_CONNECTED_DEVICE = VT_D1_HelperClass.getRTId('VTD1_Patient_Device__c', 'Ad_hoc_Connected_Device');
    public static final String RECORD_TYPE_ID_DEVICE_AD_HOC_TABLET_DEVICE = VT_D1_HelperClass.getRTId('VTD1_Patient_Device__c', 'Ad_hoc_Tablet_Device');

    public static final String ORDER_STATUS_RECEIVED = 'Received';
    public static final String ORDER_STATUS_REPLACEMENT_ORDERED = 'Replacement Ordered';
}