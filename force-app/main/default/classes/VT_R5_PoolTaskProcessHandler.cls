/*created by Tatiana Terekhova 27-05-2020*/
public with sharing class VT_R5_PoolTaskProcessHandler{
        public void onafterupdate(List<VTR5_Pool_Task__c> newLst,Map <Id, VTR5_Pool_Task__c> oldMap){
                sendDocsAndAlertsToMRR(newLst,oldMap);
        }
        private void sendDocsAndAlertsToMRR(List<VTR5_Pool_Task__c> newLst,Map <Id, VTR5_Pool_Task__c> oldMap){
                Set<Id> docuvisitsIds = new Set<Id>();
                Set<Id> changedTasks = new Set<Id>();
                Set<Id> queueIds = new Set<Id>();
                Map<Id, List<String>> groupMap = new Map<Id, List<String>>();
                Map <String,Id> emailUsrMap = new Map<String,Id>();
                Map <Id,String> emailMap = new Map <Id,String>();
                Id orgWideId;
                Id tUserId;
                OrgWideEmailAddress[]owea=[Select Id From OrgWideEmailAddress limit 1];
                orgWideId=owea[0].Id;
                EmailTemplate et =[Select Id, Name from EmailTemplate where DeveloperName='VT_R5_MV_Report_Task_Claimed' limit 1];
                for(VTR5_Pool_Task__c pt:newLst){
                        if(pt.OwnerId!=oldMap.get(pt.Id).OwnerId&&pt.VTR5_TypeForBackendLogic__c=='Monitoring Visit Report Sent'){
                                    docuvisitsIds.add(pt.VTR5_MonitoringVisit__c);
                                    changedTasks.add(pt.Id);
                        }
                }
                List <VTD1_Monitoring_Visit__c> visitsSend = new List<VTD1_Monitoring_Visit__c>();
                if (changedTasks.size()==0)return;
                visitsSend =[Select Id,OwnerId,Owner.Email,VTD1_Study__c,VTD1_Study__r.VTR5_MRR_Queue_ID__c,(Select Id, OwnerId from Pool_Tasks__r where Id in:changedTasks), (Select Id,ContentDocumentId from ContentDocumentLinks order by ContentDocument.LastModifiedDate DESC limit 1) from VTD1_Monitoring_Visit__c where Id in:docuvisitsIds ]; 
                for(VTD1_Monitoring_Visit__c mv:visitsSend){
                        if(mv.VTD1_Study__r.VTR5_MRR_Queue_ID__c!=null){
                                queueIds.add(mv.VTD1_Study__r.VTR5_MRR_Queue_ID__c);
                        }
                }
                List <User> groupAUsers = [SELECT User.Id, User.Email FROM User WHERE Id IN (SELECT UserOrGroupId FROM GroupMember WHERE Group.Id in:queueIds)];
                for(User us:groupAUsers){
                        emailMap.put(us.Id,us.Email);
                        emailUsrMap.put(us.Email,us.Id);
                }
                List <Group> mrrqueues=new List<Group>();
                mrrqueues =[Select Id, (Select Id, UserOrGroupId from GroupMembers) from Group where Id in:queueIds ];
                system.debug('mrrqueues '+mrrqueues);
                for (Group gr:mrrqueues){
                        if(!groupMap.keyset().contains(gr.Id))groupMap.put(gr.Id,new List<String>());
                        for(GroupMember gm:gr.GroupMembers){
                                groupMap.get(gr.Id).add(emailMap.get(gm.UserorGroupId));
                        }
                }
                 system.debug('groupMap'+groupMap);
                List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
                for (VTD1_Monitoring_Visit__c mvr:visitsSend){
                        for (VTR5_Pool_Task__c pt:mvr.Pool_Tasks__r){
                                if(mvr.ContentDocumentLinks.size()>0){
                                        mvr.VTR5_MRRSignedMVReport__c=pt.OwnerId;
                                        List <String> emails=removeUserFromList(groupMap.get(mvr.VTD1_Study__r.VTR5_MRR_Queue_ID__c), emailMap.get(pt.OwnerId));
                                        if(emails.size()>0) tUserId=emailUsrMap.get(emails[0]);
                                        system.debug('emails '+emails);
                                        new VT_R5_SendDocstoDocuSign(mvr.Id,mvr.ContentDocumentLinks[0].ContentDocumentId,
                                                                    pt.OwnerId,
                                                                    'Documents for your DocuSign Signature',
                                                                    'I am sending you this request for your electronic signature, please review and electronically sign by following the link below.').executeFuture();
                                        allmsg.add(createEmailMessage(tUserId,orgWideId,et.Id,mvr.Id,emails));
                                }
                        }
                }
                 update visitsSend;
                 try{
                        VT_R3_EmailsSender.sendEmails(allmsg);
                }catch(Exception ex){
                        system.debug('ERROR '+ex.getMessage());
                }  
        }
        public Messaging.SingleEmailMessage createEmailMessage(Id tUserId,Id orgWideId,Id templateId,Id mvrId, List <String> sendTo){
                system.debug('sendT0 '+sendTo);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTemplateId(templateId);
                mail.setOrgWideEmailAddressId(orgWideId);
                mail.setToAddresses(sendTo);
                mail.setTargetObjectId(tUserId);
                mail.saveasActivity=false;
                mail.setWhatId(mvrId);
                 return mail;
        }
        public List<String> removeUserFromList(List<String> lst, String u) {
                system.debug('Lst start '+lst);
                for (Integer i = lst.size() - 1; i >= 0 ; --i) {
                        if (lst.get(i) == u) {
                                lst.remove(i);      
                        }    
                }
                system.debug('Lst end '+lst);
                return lst;
        }
}