public with sharing class VT_D2_CaseEscalateController {
    @AuraEnabled
    public static Case getCase(String caseId) {
        try {
            Case cas = new Case();
            List<Case> cases = [SELECT Id, IsEscalated, VTD1_PCF_Safety_Concern_Indicator__c FROM Case WHERE Id = :caseId];
            if (!cases.isEmpty()) {
                cas = cases[0];
            }
            return cas;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    /**
     * @param caseId
     * @author	Alexander Medentsov
     * @date 09.06.2020
     * @version 1.0
     * Just returns the profile name of the current user
     * @return String
     */

    @AuraEnabled
    public static String getProfile(String caseId){
        List<Profile> Profile = [SELECT Id, Name FROM Profile WHERE Id=:UserInfo.getProfileId() LIMIT 1];
        String profileName = Profile[0].Name;
        return profileName;
    }

    @AuraEnabled
    public static void updateCase(String caseId, Boolean escalate) {
        try {
            Case c = [SELECT Id, IsEscalated, VTD1_IsEscalatedDateTime__c FROM Case WHERE Id = :caseId];
            if (escalate) {
                c.IsEscalated = true;
                c.VTD1_IsEscalatedDateTime__c = Datetime.now();
            } else {
                c.IsEscalated = false;
                c.VTD1_IsEscalatedDateTime__c = null;
            }
            update c;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
}