/**
* @author: Carl Judge
* @date: 17-Jul-19
**/

public without sharing class VT_R2_ReplacementItemTriggerHandler {

    public void onBeforeDelete(List<VTD1_Replacement_Item__c> items) {
        VT_D1_LegalHoldDeleteValidator.validateDeletion(items);
    }

    public void onAfterInsert(List<VTD1_Replacement_Item__c> items) {
        processKitReplacements(items);
    }

    private void processKitReplacements(List<VTD1_Replacement_Item__c> items) {
        List<Id> kitIds = new List<Id>();
        List<Id> deliveryIds = new List<Id>();
        List<VTD1_Replacement_Item__c> kitItems = new List<VTD1_Replacement_Item__c>();
        for (VTD1_Replacement_Item__c item : items) {
            if (item.RecordTypeId == VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_REPLACEMENT_ITEM_KIT
                && item.VTD1_Patient_Kit__c != null
                && item.VTD1_Patient_Delivery__c != null)
            {
                kitIds.add(item.VTD1_Patient_Kit__c);
                deliveryIds.add(item.VTD1_Patient_Delivery__c);
                kitItems.add(item);
            }
        }

        if (!kitItems.isEmpty()) {
            List<VTD1_Patient_Kit__c> newKits = new List<VTD1_Patient_Kit__c>();
            Map<Id, VTD1_Patient_Kit__c> oldKitsMap = new Map<Id, VTD1_Patient_Kit__c>([
                SELECT Id, Name, VTD1_Protocol_Kit__c, VTD2_MaterialId__c, RecordTypeId, VTD1_IntegrationId__c,
                    VTD1_Case__c,VTR2_Quantity__c, VTD1_Patient_Delivery__c
                FROM VTD1_Patient_Kit__c
                WHERE Id IN :kitIds
            ]);

            for (VTD1_Patient_Kit__c oldKit : oldKitsMap.values()) {
                deliveryIds.add(oldKit.VTD1_Patient_Delivery__c);
            }
            Map<Id, VTD1_Order__c> deliveryMap = new Map<Id, VTD1_Order__c>([
                SELECT Id, VTD1_ShipmentId__c FROM VTD1_Order__c WHERE Id IN :deliveryIds
            ]);
            List<VTD1_Order__c> deliveriesToUpdate = new List<VTD1_Order__c>();

            for (VTD1_Replacement_Item__c kitItem : kitItems) {
                if (oldKitsMap.containsKey(kitItem.VTD1_Patient_Kit__c)
                    && deliveryMap.containsKey(kitItem.VTD1_Patient_Delivery__c)
                    && deliveryMap.containsKey(oldKitsMap.get(kitItem.VTD1_Patient_Kit__c).VTD1_Patient_Delivery__c))
                {
                    VTD1_Patient_Kit__c oldKit = oldKitsMap.get(kitItem.VTD1_Patient_Kit__c);
                    VTD1_Order__c newDelivery = deliveryMap.get(kitItem.VTD1_Patient_Delivery__c);
                    VTD1_Order__c oldDelivery = deliveryMap.get(oldKit.VTD1_Patient_Delivery__c);

                    if (newDelivery.VTD1_ShipmentId__c == null || newDelivery.VTD1_ShipmentId__c == oldDelivery.VTD1_ShipmentId__c) {
                        VTD1_Patient_Kit__c newKit = oldKit.clone(false, true, false, false);
                        newKit.VTD1_Replacement_For__c = oldKit.Id;
                        newKit.VTD1_Patient_Delivery__c = kitItem.VTD1_Patient_Delivery__c;
                        newKits.add(newKit);
                        oldKit.VTD2_MaterialId__c = null;
                        oldKit.VTD1_IntegrationId__c = null;
                        if (newDelivery.VTD1_ShipmentId__c == null) {
                            newDelivery.VTD1_ShipmentId__c = oldDelivery.VTD1_ShipmentId__c;
                            deliveriesToUpdate.add(newDelivery);
                        }
                        oldDelivery.VTD1_ShipmentId__c = null;
                        deliveriesToUpdate.add(oldDelivery);
                    } else if (newDelivery.VTD1_ShipmentId__c != oldDelivery.VTD1_ShipmentId__c) {
                        kitItem.addError(Label.VTR2_ReplacementItemDifferentShipmentError);
                    }
                }
            }

            update oldKitsMap.values();
            insert newKits;
            update deliveriesToUpdate;
        }
    }
}