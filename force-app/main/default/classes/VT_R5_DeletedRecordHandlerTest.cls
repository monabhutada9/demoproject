/**
 * Created by user on 16-Nov-20.
 */
@isTest
public with sharing class VT_R5_DeletedRecordHandlerTest {

    public static void createDeletedRecordsSurveyTest() {
        List<VTD1_Survey__c> s = [select id,  (Select Id FROM Survey_Answers__r) from VTD1_Survey__c LIMIT 1];

        s[0].VTR5_ReasonForDeletion__c = 'test';
        update s[0];
        Test.startTest();
        delete s;

        Test.stopTest();
        System.assertEquals([SELECT Id FROM VTR5_DeletedRecord__c].size(), 1);
    }

    public static void createDeletedRecordsDocumentTest() {
        VTD1_Document__c placeholder = new VTD1_Document__c();
        insert placeholder;
        Test.startTest();

        VT_R5_DeletedRecordHandler.createDeletedRecords(new List<SObject>{
                placeholder
        });

        Test.stopTest();
        System.assertEquals([SELECT Id FROM VTR5_DeletedRecord__c].size(), 1);
    }
}