public  without sharing class VT_D1_MyStudySuppliesItemController {

    /*
  	@AuraEnabled
  	public static void confDroppedOffKit(String kitId) {
  		VTD1_Patient_Kit__c kit = [SELECT VTD1_Status__c FROM VTD1_Patient_Kit__c WHERE Id =: kitId];
    	kit.VTD1_Status__c = 'Pending Return';
    	update kit;
  	}

    @AuraEnabled
  	public static void confPickupKit(String kitId) {
  		VTD1_Patient_Kit__c kit = [SELECT VTD1_Status__c FROM VTD1_Patient_Kit__c WHERE Id =: kitId];
    	kit.VTD1_Status__c = 'Pending Return';
    	update kit;
  	}
*/
    @AuraEnabled
    public static void confReqReplDelivery(Id caseId, Id itemId, String itemName, String patientName,
            Boolean isIMP, String requestReplacementReason, String requestReplacementComments) {
        IMP_Replacement_Form__c replacementForm = new IMP_Replacement_Form__c();
        replacementForm.VTD1_Case__c = caseId;
        String itemObjectTypeName = itemId!=null? itemId.getSobjectType().getDescribe().getName() : null;
        replacementForm.VTD1_Replace__c = itemObjectTypeName == 'VTD1_Patient_Kit__c'? itemId : null;
        replacementForm.VTR2_ReplacePackagingMaterials__c = itemObjectTypeName == 'VTD1_Order__c'? itemId : null;
        replacementForm.VTR2_Replacement_Form_Name__c = 'Replacement Request of ' + itemName + ' from ' + patientName;
        replacementForm.Name = (replacementForm.VTR2_Replacement_Form_Name__c).left(80);
        //replacementForm.OwnerId = userInfo.getUserId();
        if(isIMP){
            replacementForm.VTD1_Replacement_Reason__c = requestReplacementReason;
            replacementForm.VTD1_Comments__c = requestReplacementComments;
        }
        System.debug(replacementForm);
        insert replacementForm;

    }

    @AuraEnabled
    public static void confPackMatHand(String deliveryId, Boolean needPackagingMaterials) {
        Id patientUserId = getPatientUserId();
        VTD1_Order__c delivery = [SELECT VTD1_isPackagingMaterialsNeeded__c, VTD1_PatientHasPackagingMaterials__c FROM VTD1_Order__c WHERE Id =: deliveryId];
        delivery.VTD1_isPackagingMaterialsNeeded__c = needPackagingMaterials;
        delivery.VTD1_PatientHasPackagingMaterials__c = !needPackagingMaterials;
        update delivery;

        System.debug('deliveryId = ' + deliveryId);

        List<Task> tasks = [SELECT Status
        FROM Task
        WHERE VTD1_Type_for_backend_logic__c = 'Confirm Packaging Materials in Hand'
        AND Status = 'Open'
        AND OwnerId =: patientUserId
        AND WhatId =: deliveryId];

        System.debug('tasks = ' + tasks);

        for (Task task : tasks) {
            task.Status = 'Completed';
        }
        update tasks;
    }

    @AuraEnabled
    public static void confPickup(String deliveryId, Datetime courierPickupDateTime) {
        Id patientUserId = getPatientUserId();
        VTD1_Order__c delivery = [SELECT VTD1_CourierPickupDateTime__c FROM VTD1_Order__c WHERE Id =: deliveryId];
        delivery.VTD1_CourierPickupDateTime__c = courierPickupDateTime;
        update delivery;

        List<Task> tasks = [SELECT Status
        FROM Task
        WHERE VTD1_Type_for_backend_logic__c = 'Confirm Courier Pickup' AND
        Status = 'Open' AND OwnerId =:patientUserId AND WhatId =: deliveryId];
        for (Task task : tasks){
            task.Status = 'Completed';
        }
        update tasks;
    }

    @AuraEnabled
    public static void confDropOff(String deliveryId, Datetime dropOffIMPDateTime) {
        Id patientUserId = getPatientUserId();
        VTD1_Order__c delivery = [SELECT VTD1_Drop_Off_IMP_DateTime__c FROM VTD1_Order__c WHERE Id =: deliveryId];
        delivery.VTD1_Drop_Off_IMP_DateTime__c = dropOffIMPDateTime;
        update delivery;

        List<Task> tasks = [SELECT Status
        FROM Task
        WHERE Status = 'Open' AND OwnerId =:patientUserId AND WhatId =: deliveryId
        AND (VTD1_Type_for_backend_logic__c = 'Confirm Drop Off of Study Medication'
            OR VTD1_Type_for_backend_logic__c = 'Confirm Drop Off Lab')];

        for (Task task : tasks){
            task.Status = 'Completed';
        }
        update tasks;
    }

    private static Id getPatientUserId() {
        Id currentUserId = UserInfo.getUserId();
        String patientId = VT_D1_PatientCaregiverBound.findPatientOfCaregiver(currentUserId);
        if (patientId != null){
            currentUserId = (Id)patientId;
        }
        return currentUserId;
    }

}