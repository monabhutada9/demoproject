@IsTest
private class VT_R2_SCRMyPatientMedicalRecModalTest {

    @TestSetup
    static void setupData() {
        VTD1_Document__c document = (VTD1_Document__c) new DomainObjects.VTD1_Document_t()
                .addCase(new DomainObjects.Case_t())
                .addStudy(new DomainObjects.Study_t())
                .persist();
        ContentVersion contentVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();
        ContentVersion insertedContVersion = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        new DomainObjects.ContentDocumentLink_t()
                .withLinkedEntityId(document.Id)
                .withContentDocumentId(insertedContVersion.ContentDocumentId)
                .persist();
    }

    @IsTest
    static void deleteDocumentByContentDocumentIdTest() {
        VTD1_Document__c document = [SELECT Id FROM VTD1_Document__c LIMIT 1];
        ContentDocumentLink link = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :document.Id LIMIT 1];

        Test.startTest();
            Boolean isSuccess = VT_R2_SCRMyPatientMedicalRecordsModal.deleteDocumentByContentDocumentId(link.ContentDocumentId);
            try {
                List<VTD1_Document__c> docs = [SELECT Id FROM VTD1_Document__c];
            } catch (System.QueryException exc) {
                System.assertEquals('List has no rows for assignment to SObject', exc.getMessage(), 'Incorrect exception');
            }
        Test.stopTest();

        System.assert(isSuccess, 'The record should has been deleted');

    }

    @IsTest
    static void updateDocumentTest_Fail() {
        VTD1_Document__c document = [SELECT Id FROM VTD1_Document__c LIMIT 1];
        ContentDocumentLink link = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :document.Id LIMIT 1];
        VT_R2_SCRMyPatientMedicalRecordsModal.DocumentWrapper wrapper = new VT_R2_SCRMyPatientMedicalRecordsModal.DocumentWrapper();
        VT_R2_SCRMyPatientMedicalRecordsModal.DocumentData documentData = new VT_R2_SCRMyPatientMedicalRecordsModal.DocumentData();
        wrapper.contentDocumentId = document.Id;
        documentData.nickname = 'Test Nickname';
        documentData.comments = 'Test Comments';
        wrapper.documentData = documentData;
        String serializedWrapper = JSON.serialize(wrapper);
        Test.startTest();
            try {
                VT_R2_SCRMyPatientMedicalRecordsModal.updateDocument(serializedWrapper);
            } catch (AuraHandledException exc) {
                System.assertEquals('Script-thrown exception', exc.getMessage(), 'Incorrect exception');
            }
        Test.stopTest();


    }
}