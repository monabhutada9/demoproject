/**
 * Created by Alexander Komarov on 27.02.2019.
 */

@IsTest
public with sharing class VT_D2_EmailDriverTriggerHandlerTest {

    public static void doFirstTest() {
        User user = [SELECT Id,Email FROM User WHERE Email LIKE '%.%' LIMIT 1];

        VTD2_Email_Driver__c driver = new VTD2_Email_Driver__c();

        driver.VTD2_Email_Recepient__c = user.Id;
        driver.VTD2_Email_Subject__c = 'SUBJECT';
        driver.VTD2_Email_Body__c = 'ddddd';
        Test.startTest();
        insert driver;
        Test.stopTest();
    }

    public static void doSecondTest() {
        Contact con = new Contact(
                FirstName = 'First',
                LastName = 'Last',
                Email = 'test@gmail.com'
        );
        insert con;

        VTD2_Email_Driver__c driver = new VTD2_Email_Driver__c();

        driver.VTD2_Email_Recepient__c = con.Id;
        driver.VTD2_Email_Subject__c = 'SUBJECT';
        driver.VTD2_Email_Body__c = 'ddddd';
        driver.VTD2_Record_Id__c = con.Id;
        Test.startTest();
        insert driver;
        Test.stopTest();
    }

}