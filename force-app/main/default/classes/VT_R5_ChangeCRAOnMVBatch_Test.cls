/**
 * Created by user on 6/19/2020.
 */
@isTest
public with sharing class VT_R5_ChangeCRAOnMVBatch_Test {









    @TestSetup
    private static void testSetup() {
        VT_R5_ShareGroupMembershipService.disableMembership = true;
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        DomainObjects.Study_t domainStudy = new DomainObjects.Study_t()
            .setMaximumDaysWithoutLoggingIn(1)
            .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));
        domainStudy.persist();
        
        Group mrrGroup = new Group();
        mrrGroup.Name = 'Test MRR Group';
        insert mrrGroup;
        
        DomainObjects.User_t domainCRAUser = new DomainObjects.User_t()
            .setEmail(VT_D1_TestUtils.generateUniqueUserName())
            .setProfile(VT_R4_ConstantsHelper_Profiles.CRA_PROFILE_NAME);
        domainCRAUser.persist();
        
        DomainObjects.User_t domainMRRUser = new DomainObjects.User_t()
            .setEmail(VT_D1_TestUtils.generateUniqueUserName())
            .setProfile(VT_R4_ConstantsHelper_Profiles.MRR_PROFILE_NAME);
        domainMRRUser.persist();
        
        DomainObjects.User_t domainPIUser = new DomainObjects.User_t()
            .addContact(new DomainObjects.Contact_t())
            .setEmail(VT_D1_TestUtils.generateUniqueUserName())
            .setProfile(VT_R4_ConstantsHelper_Profiles.PI_PROFILE_NAME);
        domainPIUser.persist();
        
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        study.VTR5_MRR_Queue_ID__c = mrrGroup.Id;
        update study;
        
        DomainObjects.StudyTeamMember_t domainCRAStudyTeamMember = new DomainObjects.StudyTeamMember_t()
            .addStudy(domainStudy)
            .addUser(domainCRAUser);
        domainCRAStudyTeamMember.persist();
        
        DomainObjects.StudyTeamMember_t domainPIStudyTeamMember = new DomainObjects.StudyTeamMember_t()
            .addStudy(domainStudy)



            .addUser(domainPIUser);
            //START RAJESH SH-17440
            //.setRemoteCraId(domainCRAStudyTeamMember.Id);
        	//END:SH-17440



        domainPIStudyTeamMember.persist();
        
        DomainObjects.VirtualSite_t domainVirtualSite = new DomainObjects.VirtualSite_t()
            .addStudy(domainStudy)
            .addStudyTeamMember(domainPIStudyTeamMember)




                .setStudySiteNumber('12345');
        domainVirtualSite.persist();
        Test.stopTest();
        //START
        //Vijendra Hire
        //Jira ref - SH-17437
        //Added CRA as SSTM



        Study_Team_Member__c craSTM1 =   [SELECT	Id, 
                                          User__c,
                                          User__r.FirstName, 
                                          User__r.Profile.Name,
                                          Study_Team_Member__c                                           
                                          FROM Study_Team_Member__c
                                          WHERE Study__c = :domainStudy.Id AND User__r.Profile.Name='CRA' LIMIT 1];
        
        Study_Site_Team_Member__c sstm1 = new Study_Site_Team_Member__c();
        sstm1.VTR5_Associated_CRA__c = craSTM1.Id;
        sstm1.VTD1_SiteID__c = domainVirtualSite.Id ;
        insert sstm1;
        
        HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
        objVirtualShare.ParentId = domainStudy.Id;
        objVirtualShare.UserOrGroupId = craSTM1.User__c;
        objVirtualShare.AccessLevel = 'Edit';
        insert objVirtualShare;
        
        VTD1_Monitoring_Visit__c monVisit = new VTD1_Monitoring_Visit__c();
        monVisit.VTD1_Study__c = domainStudy.Id;
        monVisit.VTD1_Virtual_Site__c = domainVirtualSite.Id;
        monVisit.VTR5_MRRSignedMVReport__c = domainMRRUser.Id;
        System.runAs(new User(Id = craSTM1.User__c)){
            insert monVisit;
    }


        //END



        Task task = new Task (VTR2_Monitoring_Visit_del__c = monVisit.Id);
        insert task;
        
        dsfs__DocuSign_Status__c docuSignStatus = new dsfs__DocuSign_Status__c();
        docuSignStatus.VTD1_Monitoring_Visit__c = monVisit.Id;
        insert docuSignStatus;
        
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.PathOnClient = 'test.txt';
        contentVersion.Title = 'Test File';
        contentVersion.VersionData = Blob.valueOf('Test Data');
        insert contentVersion;
        
        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = monVisit.Id;
        contentDocumentLink.ContentDocumentId = [
            SELECT ContentDocumentId
            FROM ContentVersion
            WHERE Id = :contentVersion.Id
        ].ContentDocumentId;
        contentDocumentLink.ShareType = 'I';
        contentDocumentLink.Visibility = 'AllUsers';
        insert contentDocumentLink;
        
        System.runAs(new User(Id = UserInfo.getUserId())) {
            insert new GroupMember(
                UserOrGroupId = domainMRRUser.Id,
                GroupId = mrrGroup.Id
            );
    }

        List <VTD2_TN_Catalog_Code__c> tnCatalogCodes = new List<VTD2_TN_Catalog_Code__c>();
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
            VTD2_T_Task_Unique_Code__c = 'T543',
            RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Task').getRecordTypeId(),
            VTD2_T_SObject_Name__c = 'dsfs__DocuSign_Status__c',
            VTD2_T_Care_Plan_Template__c = 'IF (ISBLANK(dsfs__DocuSign_Status__c.VTD1_Monitoring_Visit__r.VTD1_Study__c ),NULL,dsfs__DocuSign_Status__c.VTD1_Monitoring_Visit__r.VTD1_Study__c )',
            VTD2_T_Related_To_Id__c = 'dsfs__DocuSign_Status__c.VTD1_Monitoring_Visit__c',
            VTD2_T_Task_Record_Type_ID__c = task.getSObjectType().getDescribe().getRecordTypeInfosByName().get('SimpleTask').getRecordTypeId(),
            VTD2_T_Subject__c = 'Review follow-up letter for Monitoring visit Report',
            VTD2_T_Status__c = 'Open',
            VTD2_T_Due_Date__c = 'dsfs__DocuSign_Status__c.LastModifiedDate',
            VTD2_T_Description__c = 'Please, review the Follow-Up Letter submitted'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
            VTD2_T_Task_Unique_Code__c = 'T613',
            RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('PoolTask').getRecordTypeId(),
            VTD2_T_SObject_Name__c = 'VTD1_Monitoring_Visit__c',
            VTD2_T_Care_Plan_Template__c = 'VTD1_Monitoring_Visit__c.VTD1_Virtual_Site__r.VTD1_Study__r.Id',
            VTD2_T_Related_To_Id__c = 'VTD1_Monitoring_Visit__c.Id',
            VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD1_RTId__c.VTR5_Pool_Task_MRR__c',
            VTD2_T_Subject__c = 'Approve or reject',
            VTD2_T_Status__c = 'Open',
            VTD2_T_Due_Date__c = 'TODAY()'
        ));
        insert tnCatalogCodes;
    }

    @IsTest
    public static void testRemoteMV(){
        VT_R5_ShareGroupMembershipService.disableMembership = true;
        Test.startTest();
        Database.executeBatch(new VT_R5_ChangeCRAOnMonitoringVisit_Batch());
        Test.stopTest();
    }
}