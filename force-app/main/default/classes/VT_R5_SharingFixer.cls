/**
* @author: Carl Judge
* @date: 03-Sep-20
* @description:
**/

public without sharing class VT_R5_SharingFixer implements Database.Batchable<SObject>, Schedulable, Database.AllowsCallouts {
    /*
        to schedule every 15 minutes:
        System.schedule('Share fix job 1', '0 0 * * * ?', new VT_R5_SharingFixer());
        System.schedule('Share fix job 2', '0 15 * * * ?', new VT_R5_SharingFixer());
        System.schedule('Share fix job 3', '0 30 * * * ?', new VT_R5_SharingFixer());
        System.schedule('Share fix job 4', '0 45 * * * ?', new VT_R5_SharingFixer());
     */

    public Datetime timeframeOverride;
    public String queryOverride;
    public SObjectType objectType = Case.getSObjectType();
    public Integer nextRunIntervalMinutes;
    public Integer jobSize = 10;

    public void execute(SchedulableContext sc) {
        VT_R5_SharingFixer fixer = new VT_R5_SharingFixer();
        fixer.timeframeOverride = this.timeframeOverride;
        fixer.queryOverride = this.queryOverride;
        fixer.objectType = this.objectType;
        fixer.nextRunIntervalMinutes = this.nextRunIntervalMinutes;
        fixer.jobSize = this.jobSize;
        Database.executeBatch(fixer, this.jobSize);
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        Datetime timeframe = timeframeOverride;

        if (timeframe == null) {
            for (AsyncApexJob previousJob : [
                SELECT CreatedDate, Status, JobType
                FROM AsyncApexJob
                WHERE Status NOT IN('Aborted', 'Failed')
                AND ApexClass.Name = 'VT_R5_SharingFixer'
                AND JobType = 'BatchApex'
                AND Id != :bc.getJobId()
                ORDER BY CreatedDate DESC LIMIT 1
            ]) {
                System.debug(previousJob);
                timeframe = previousJob.CreatedDate;
            }
        }

        if (timeframe == null) { timeframe = Datetime.now().addHours(-1); }

        return queryOverride == null
            ? Database.getQueryLocator([SELECT Id FROM Case WHERE CreatedDate >= :timeframe])
            : Database.getQueryLocator(queryOverride);
    }

    public void execute(Database.BatchableContext bc, List<SObject> scope) {
        Set<Id> recIds = new Map<Id, SObject>(scope).keySet();
        System.debug(recIds);
        System.debug(objectType);
        VT_R3_AbstractGlobalSharingLogic logic = VT_R3_AbstractGlobalSharingLogic.getSharingLogicForObjectType(objectType.getDescribe().getName());
        logic.init(recIds, null, null, false);
        logic.executeLogic();
    }

    public void finish(Database.BatchableContext bc) {
        if (nextRunIntervalMinutes != null) {
            VT_R5_SharingFixer fixer = new VT_R5_SharingFixer();
            fixer.timeframeOverride = timeframeOverride;
            fixer.queryOverride = queryOverride;
            fixer.objectType = objectType;
            fixer.nextRunIntervalMinutes = nextRunIntervalMinutes;
            fixer.jobSize = jobSize;
            if (!Test.isRunningTest()) {
                System.scheduleBatch(fixer, 'Fix Sharing Processing ' + System.now(), nextRunIntervalMinutes, jobSize);
            }
        }
    }
}