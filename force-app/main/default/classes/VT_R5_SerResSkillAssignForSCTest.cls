@isTest
public class VT_R5_SerResSkillAssignForSCTest {
    
    @testSetup static void setup(){
        String orgId = userInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueuserName = orgId + dateString + randomInt;
        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();
        
        User use = new user();
        use =VT_D1_TestUtils.createUserByProfile('Study Concierge',uniqueuserName);
        use.VT_R5_Primary_Preferred_Language__c='en_Us';
        use.VT_R5_Secondary_Preferred_Language__c='af';
        use.VT_R5_Tertiary_Preferred_Language__c='bs';
        use.isactive = true;
        insert use;
        
    }
    
    
    static testMethod void createUpdateServResSkillAssignTest(){ 
        
        
        user u =[select id from user where Profile.Name='Study Concierge' and isactive = true Limit 1];
        u.VT_R5_Tertiary_Preferred_Language__c='ja';
        u.VT_R5_Secondary_Preferred_Language__c='bs';
        u.VT_R5_Primary_Preferred_Language__c='de';   
        update u;
        
    }
    
    
    
}