/**
 * Created by Mikhail Platonov on 10-Apr-20.
 *
 * @description This class is used by vt_r4_study_visit_modal component.
 */
public without sharing class VT_R4_GeneralVisitsController {
    private static AuraHandledExceptionUtils auraExceptionUtil = new AuraHandledExceptionUtils();
    @AuraEnabled
    public static VTR4_General_Visit__c getGeneralVisit(Id visitId) {
        return VT_R4_GeneralVisitsHelper.getGeneralVisit(visitId);
    }
    @AuraEnabled
    public static String updateGeneralVisit(GeneralVisitData data) {
        try {
            return VT_R4_GeneralVisitsHelper.updateGeneralVisit(data);
        } catch (Exception exc) {
            System.debug(LoggingLevel.INFO, 'exc ' + exc);
            throw auraExceptionUtil.create(exc);
        }
    }
    @AuraEnabled
    public static List<String> getAvailableTimeSlots(List <VTR4_General_Visit_Member__c> members, Integer duration, Date selectedDate) {
        return VT_R4_GeneralVisitsHelper.getAvailableTimeSlots(members, duration, selectedDate);
    }
    @AuraEnabled(Cacheable = true)
    public static List<User> getUsersForSearchComponent() {
        try {
            return VT_R4_GeneralVisitsHelper.getUsersForSearchComponent();
        } catch (Exception exc) {
            throw auraExceptionUtil.create(exc);
        }
    }
    @AuraEnabled(Cacheable = true)
    public static User getUserInfo() {
        try {
            return [SELECT Name, Title, Email, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
        } catch (Exception exc) {
            throw auraExceptionUtil.create(exc);
        }
    }
    @AuraEnabled(Cacheable = true)
    public static Integer getTimeZoneOffset(){
        return UserInfo.getTimeZone().getOffset(System.now());
    }
    public class GeneralVisitData {
        @AuraEnabled public VTR4_General_Visit__c visit { get; set; }
        @AuraEnabled public List <VTR4_General_Visit_Member__c> members { get; set; }
    }
    public class VT_R4_GeneralVisitsException extends Exception {
    }
    private class AuraHandledExceptionUtils {
        private AuraHandledException create(Exception exc) {
            System.debug(LoggingLevel.ERROR, 'exception ' + exc);
            AuraHandledException auraExc = new AuraHandledException(exc.getMessage());
            auraExc.setMessage(exc.getMessage());
            return auraExc;
        }
    }
}