/**
 * THIS CLASS IS DEPRECATED! DON'T USE IT!
 *
 * Use the following instead:
 * @see VT_R4_ConstantsHelper_AccountContactCase
 * @see VT_R4_ConstantsHelper_Documents
 * @see VT_R4_ConstantsHelper_Integrations
 * @see VT_R4_ConstantsHelper_KitsOrders
 * @see VT_R4_ConstantsHelper_ProfilesSTM
 * @see VT_R4_ConstantsHelper_Protocol_ePRO
 * @see VT_R4_ConstantsHelper_Tasks
 * @see VT_R4_ConstantsHelper_VisitsEvents
 */
public with sharing class VT_R4_ConstantsHelper_RecordTypes {
    private static List<String> sObjectNames = new List<String>{'Account', 'Contact', 'Case', 'VTD1_Document__c'};
    private static Map<String, Map<String, Schema.RecordTypeInfo>> recordTypesMap = new Map<String, Map<String, Schema.RecordTypeInfo>>();
    static {
        for (String sObjectName : sObjectNames) {
            recordTypesMap.put(sObjectName, Schema.getGlobalDescribe().get(sObjectName).getDescribe().getRecordTypeInfosByDeveloperName());
        }
    }
    private static String getRecordTypeId(String sObjectName, String recordTypeName) {
        return recordTypesMap.get(sObjectName).get(recordTypeName).getRecordTypeId();
    }
    public static final String RECORD_TYPE_ID_ACCOUNT_PATIENT = getRecordTypeId('Account','Patient');
    public static final String RECORD_TYPE_ID_ACCOUNT_SPONSOR = getRecordTypeId('Account','Sponsor');
//    public static final String RECORD_TYPE_ID_ACCOUNT_PATIENT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Patient').getRecordTypeId();
//    public static final String RECORD_TYPE_ID_ACCOUNT_SPONSOR = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Sponsor').getRecordTypeId();

    public static final String RECORD_TYPE_ID_CONTACT_PATIENT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Patient').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CONTACT_CAREGIVER = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Caregiver').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_SIMPLETASK = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('SimpleTask').getRecordTypeId();
    public static final String RECORD_TYPE_ID_EVENT_OUTOFOFFICE = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('OutOfOffice').getRecordTypeId();

    public static final String RECORD_TYPE_ID_PROTOCOL_VISIT_PROTOCOL = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByDeveloperName().get('Protocol').getRecordTypeId();
    public static final String RECORD_TYPE_ID_PROTOCOL_VISIT_ONBOARDING = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByDeveloperName().get('VTD1_Onboarding').getRecordTypeId();
    public static final String RECORD_TYPE_ID_PROTOCOL_VISIT_SUBGROUP_TIMELINE_VISIT = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByDeveloperName().get('VTR5_Subgrouptimelinevisit').getRecordTypeId();
    public static final String RECORD_TYPE_ID_ACTUAL_VISIT_LABS = Schema.SObjectType.VTD1_Actual_Visit__c.getRecordTypeInfosByDeveloperName().get('Labs').getRecordTypeId();
    public static final String RECORD_TYPE_ID_ACTUAL_VISIT_UNSCHEDULED = Schema.SObjectType.VTD1_Actual_Visit__c.getRecordTypeInfosByDeveloperName().get('VTD1_Unscheduled').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CASE_CAREPLAN = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CarePlan').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CASE_PCF_READONLY = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VTD1_PCF_Read_Only').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CASE_PCF = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VTD1_PCF').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CONTACT_PCP = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('PCP').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CONTACT_PI = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('PI').getRecordTypeId();
    public static final String RECORD_TYPE_ID_CONTACT_SPONSOR = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Sponsor_Contact').getRecordTypeId();
    public static final String RECORD_TYPE_ID_PATIENT_BLIND = Schema.SObjectType.VTD1_Patient__c.getRecordTypeInfosByDeveloperName().get('Blind').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_ARCHIVED = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('Medical_Record_Archived').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_REJECTED = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('Medical_Record_Rejected').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD_RELEASE_FORM = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('Medical_Record_Release_Form').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD1_Medical_Record').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_OTHER = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD1_Other').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_CERTIFICATON = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD2_Certification').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD1_Patient_eligibility_assessment_form').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSR = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('Patient_Eligibility_Assessment_Form_Rejected').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD1_Regulatory_Document').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT_LOCKED = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD1_Regulatory_Document_Locked').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT_REJECTED = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('Regulatory_Document_Rejected').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_PA_SIGNATURE_PAGE = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD2_PA_Signature_Page').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_NOTE_OF_TRANSFER = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTR2_Note_of_Transfer').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_INFORMED_CONSENT_GUIDE = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTR2_Informed_Consent_Guide').getRecordTypeId();
    public static final String RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTR3_VisitDocument').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_PAYMENT = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('Payment').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_CANCELLATION = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('Cancellation').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_CONSENT = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('VTD1_Consent').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_DAILY_REPORT_UPLOAD = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('VTD1_Daily_Report_Upload').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_DELIVERY = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('Delivery').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_NOTIFICATION = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('Notification').getRecordTypeId();
    //public static final String RECORD_TYPE_ID_SC_TASK_QUEUE = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('Payment').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_RESCHEDULE = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('Reschedule').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_SCHEDULE_HCP_VISIT = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('VTD1_HCP_Visit').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SC_TASK_INDEPENDENT_RATER = Schema.SObjectType.VTD1_SC_Task__c.getRecordTypeInfosByDeveloperName().get('VTD1_Independent_Rater_schedule_visit_task').getRecordTypeId();

    public static final String RECORD_TYPE_ID_TASK_PCF_TASK = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('VTD1_PCF_Task').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_RESCHEDULE = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Reschedule').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_FOR_COUNTERSIGN_PACKET = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('TaskForCountersignPack').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_FOR_PAYMENT = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('TaskForPayment').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_FOR_SCHEDULING_VISIT = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('VTD1_TaskForSchedulingVisits').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_FOR_VISIT = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('TaskForVisit').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_FOR_VISIT_AD_HOC = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('VTD2_TaskForVisitAdHoc').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_FOR_REPORT_UPLOAD = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Task_for_Report_Upload').getRecordTypeId();
    public static final String RECORD_TYPE_ID_TASK_EAF_TASK = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('EAF_Task').getRecordTypeId();

    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_PI = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('PI').getRecordTypeId();
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_PG = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('PG').getRecordTypeId();
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_SC = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('SC').getRecordTypeId();
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_CM = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('CM').getRecordTypeId();
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_TMA = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('TMA').getRecordTypeId();
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_SCR = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('VTR2_Site_Coordinator').getRecordTypeId();
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_CRA = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('VTR2_CRA').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SITE_METRICS_PATIENT_BY_STATUS_PI = Schema.SObjectType.VTD1_SiteMetricsPatientsbyStatus__c.getRecordTypeInfosByDeveloperName().get('VTR2_PI').getRecordTypeId();
    public static final String RECORD_TYPE_ID_SITE_METRICS_PATIENT_BY_STATUS_VS = Schema.SObjectType.VTD1_SiteMetricsPatientsbyStatus__c.getRecordTypeInfosByDeveloperName().get('VTR2_VS').getRecordTypeId();

    public static final String RECORD_TYPE_ID_VISIT_MEMBER_REGULAR = Schema.SObjectType.Visit_Member__c.getRecordTypeInfosByDeveloperName().get('VTD1_RegularMember').getRecordTypeId();

    public static final String RECORD_TYPE_ID_PROTOCOL_KIT_WELCOME_TO_STUDY_PACKAGE = Schema.SObjectType.VTD1_Protocol_Kit__c.getRecordTypeInfosByDeveloperName().get('VTD1_Welcome_to_Study_Package').getRecordTypeId();
    public static final String RECORD_TYPE_ID_PROTOCOL_KIT_LAB = Schema.SObjectType.VTD1_Protocol_Kit__c.getRecordTypeInfosByDeveloperName().get('VTD1_Lab').getRecordTypeId();
    public static final String RECORD_TYPE_ID_PROTOCOL_KIT_IMP = Schema.SObjectType.VTD1_Protocol_Kit__c.getRecordTypeInfosByDeveloperName().get('VTD1_IMP').getRecordTypeId();

    public static final String RECORD_TYPE_ID_ORDER_AD_HOC_DELIVERY = Schema.SObjectType.VTD1_Order__c.getRecordTypeInfosByDeveloperName().get('VTD1_Ad_hoc_Delivery').getRecordTypeId();
    public static final String RECORD_TYPE_ID_ORDER_PACKING_MATERIALS = Schema.SObjectType.VTD1_Order__c.getRecordTypeInfosByDeveloperName().get('VTD1_Regular_Kit_Delivery').getRecordTypeId();
    public static final String RECORD_TYPE_ID_ORDER_AD_HOC_RETURN = Schema.SObjectType.VTD1_Order__c.getRecordTypeInfosByDeveloperName().get('VTD1_Ad_hoc_Return').getRecordTypeId();
    public static final String RECORD_TYPE_ID_ORDER_AD_HOC_REPLACEMENT_DELIVERY = Schema.SObjectType.VTD1_Order__c.getRecordTypeInfosByDeveloperName().get('VTD1_Ad_hoc_Replacement_Delivery').getRecordTypeId();
    public static final String RECORD_TYPE_ID_PATIENT_KIT_IMP = Schema.SObjectType.VTD1_Patient_Kit__c.getRecordTypeInfosByDeveloperName().get('VTD1_IMP').getRecordTypeId();
    public static final String RECORD_TYPE_ID_PATIENT_KIT_PACKING_MATERIALS = Schema.SObjectType.VTD1_Patient_Kit__c.getRecordTypeInfosByDeveloperName().get('VTR3_Packaging_Materials').getRecordTypeId();

    public static final String RECORD_TYPE_ID_REPLACEMENT_ITEM_KIT = Schema.SObjectType.VTD1_Replacement_Item__c.getRecordTypeInfosByDeveloperName().get('VTD1_Kit').getRecordTypeId();

    public static final String SITE_ENROLLMENT_OPEN = 'Enrollment Open';
    public static final String SITE_CLOSED = 'Closed';
    public static final String SITE_ENROLLMENT_CLOSED = 'Enrollment Closed';
    public static final String SITE_PREMATURELY_CLOSED = 'Prematurely Closed';
    public static final String MONITORING_VISIT_COMPLETE = 'Visit Completed';
    public static final String DOCUMENT_SITE_RSU_COMPLETE = 'Site RSU Complete';
    public static final String ORDER_STATUS_RECEIVED = 'Received';
    public static final String ORDER_STATUS_REPLACEMENT_ORDERED = 'Replacement Ordered';

}