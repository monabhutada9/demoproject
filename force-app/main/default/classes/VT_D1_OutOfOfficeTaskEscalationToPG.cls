global class VT_D1_OutOfOfficeTaskEscalationToPG{
    // NOT USED
    
    /*implements Database.Batchable<sObject>, Database.Stateful {
    global Database.QueryLocator start(Database.BatchableContext bc) {
    DateTime dateForCompare = System.now().addDays(-3);
}
       //DateTime dateForCompare = System.now().addMinutes(-3); 
       return Database.getQueryLocator(
            'SELECT Id, OwnerId, RecordTypeId FROM Task WHERE VTD1_isBatchReminder__c = TRUE AND Status = \'Open\' AND ReminderDateTime <: dateForCompare'
        );
    }

    global void execute(Database.BatchableContext bc, List<Task> scope) {
        List<String> patientUserId = new List<String>();
        for (Task task : scope) {
       		patientUserId.add(task.OwnerId); 		
        }
        Case[] cases = [SELECT VTD1_Patient_User__c, VTD1_Patient_User__r.Name, VTD1_Primary_PG__c FROM Case WHERE VTD1_Patient_User__c IN: patientUserId]; 
        Map<String, String> patientUserIdToPGUserIdMap = new Map<String, String>();
        Map<String, String> patientUserIdToPatientUserNameMap = new Map<String, String>();
        for(Case c : cases) {
            patientUserIdToPGUserIdMap.put(c.VTD1_Patient_User__c, c.VTD1_Primary_PG__c);
            patientUserIdToPatientUserNameMap.put(c.VTD1_Patient_User__c, c.VTD1_Patient_User__r.Name);
        } 
        
        List<Task> newTasks = new List<Task>();
        for (Task task : scope) {
            Task newTask = new Task();
            newTask.ActivityDate = System.today();
            newTask.OwnerId = patientUserIdToPGUserIdMap.get(task.OwnerId);		
            newTask.IsReminderSet = true;
            newTask.Priority = 'Normal';
            newTask.RecordTypeId = task.RecordTypeId;
            newTask.Status = 'Open';
            newTask.Subject = 'Calendar was not updated in patient ' + patientUserIdToPatientUserNameMap.get(task.OwnerId);
            newTask.ReminderDateTime = System.now();
            if (newTask.OwnerId != null) {
            	newTasks.add(newTask);
            }
        }
        insert newTasks;
    }
    
    global void finish(Database.BatchableContext bc){
    }    
    */
}