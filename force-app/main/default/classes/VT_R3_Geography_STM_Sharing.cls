/**
* @author: Carl Judge
* @date: 17-Oct-19
**/

public without sharing class VT_R3_Geography_STM_Sharing extends Handler {
    protected override void onBeforeDelete(TriggerContext context) {
        calcSharing(context.oldList);
    }

    protected override void onAfterInsert(TriggerContext context) {
        calcSharing(context.newList);
    }

    private void calcSharing(List<VTR3_Geography_STM__c> gstms) {
        List<Id> geoIds = new List<Id>();
        List<Id> stmIds = new List<Id>();
        for (VTR3_Geography_STM__c gstm : gstms) {
            geoIds.add(gstm.Study_Geography__c);
            stmIds.add(gstm.VTR3_STM__c);
        }

        Set<Id> docIds = new Map<Id, VTD1_Document__c>([
            SELECT Id FROM VTD1_Document__c WHERE VTD1_Site__r.VTR3_Study_Geography__c IN :geoIds
        ]).keySet();

        Set<Id> userIds = new Set<Id>();
        for (Study_Team_Member__c stm : [SELECT User__c FROM Study_Team_Member__c WHERE Id IN :stmIds]) {
            userIds.add(stm.User__c);
        }

        VT_R3_GlobalSharing.doSharing(docIds, userIds);
    }
}