@IsTest
public without sharing class VT_D1_PatientProfileIntialControllerTest {

    public static void initialProfileTest() {
        Case pCase = [SELECT id, VTD1_Patient_User__c, AccountId FROM Case WHERE Caregiver_s_Name__c != NULL LIMIT 1];
        User caregiverUser = [SELECT Id, AccountId, ContactId FROM User WHERE VTD1_Profile_Name__c = 'Caregiver' AND AccountId = :pCase.AccountId LIMIT 1];
        User patientUser = [SELECT Id, AccountId, ContactId FROM User WHERE VTD1_Profile_Name__c = 'Patient' AND AccountId = :pCase.AccountId LIMIT 1];
        insert new List<AccountShare>{
                new AccountShare(
                        AccountId = caregiverUser.AccountId,
                        UserOrGroupId = caregiverUser.Id,
                        AccountAccessLevel = 'edit',
                        OpportunityAccessLevel = 'edit',
                        ContactAccessLevel = 'edit',
                        RowCause = 'Manual'
                ), new AccountShare(
                        AccountId = patientUser.AccountId,
                        UserOrGroupId = patientUser.Id,
                        AccountAccessLevel = 'edit',
                        OpportunityAccessLevel = 'edit',
                        ContactAccessLevel = 'edit',
                        RowCause = 'Manual'
                )
        };

        delete [SELECT Id FROM VT_D1_Phone__c WHERE VTD1_Contact__c = :patientUser.ContactId AND IsPrimaryForPhone__c = TRUE];

        List<Address__c> addresses = [SELECT Id FROM Address__c WHERE VTD1_Contact__c = :patientUser.ContactId];

        Test.startTest();
        System.runAs(patientUser) {
            String patientProfileInitialString = VT_D1_PatientProfileIntialController.getInitialProfile();

            VT_D1_PatientProfileIntialController.PatientProfileInitial patientProfileInitial =
                    (VT_D1_PatientProfileIntialController.PatientProfileInitial) JSON.deserialize(
                            patientProfileInitialString, VT_D1_PatientProfileIntialController.PatientProfileInitial.class
                    );

            patientProfileInitial.contact.FirstName = 'FN';
            patientProfileInitial.contact.Salutation = 'sal';
            patientProfileInitial.contact.LastName = 'LN';
            patientProfileInitial.contact.VTR2_Primary_Language__c = 'en_US';

            patientProfileInitial.phone = null;

            patientProfileInitial.phoneNumberForCreate = '4444';
            patientProfileInitial.stateForCreate = 'DE';
            patientProfileInitial.zipCodeForCreate = '5555';
            patientProfileInitial.address = null;

            String patientProfileInitialStringUpd = JSON.serialize(patientProfileInitial);
            VT_D1_PatientProfileIntialController.updateProfile(patientProfileInitialStringUpd);

            Boolean profileFilledOut = VT_D1_PatientProfileIntialController.isUserProfileFilledOut();
            Contact contact = [
                    SELECT Id,
                            FirstName, Salutation, LastName,
                            Preferred_Contact_Method__c,
                            VTR2_Primary_Language__c,
                            AccountId
                    FROM Contact
                    WHERE Id = :patientUser.ContactId
            ];
            System.assertEquals(profileFilledOut, false);
            System.assertEquals('FN', contact.FirstName);
            System.assertEquals('sal', contact.Salutation);
            System.assertEquals('LN', contact.LastName);
            System.assertEquals('en_US', contact.VTR2_Primary_Language__c);

            List<Address__c> addressList = [
                    SELECT Id, State__c, ZipCode__c
                    FROM Address__c
                    WHERE VTD1_Contact__c = :patientUser.ContactId
                    AND Id NOT IN :addresses
            ];
            System.assertEquals('DE', addressList.get(0).State__c);
            System.assertEquals('5555', addressList.get(0).ZipCode__c);
        }

//        List<VT_D1_Phone__c> contactPhonesListForDelete = [
//                SELECT Id
//                FROM VT_D1_Phone__c
//                WHERE VTD1_Contact__c = :patientUser.ContactId
//                AND IsPrimaryForPhone__c = TRUE
//        ];
//        List<Address__c> addressListForDelete = [SELECT Id FROM Address__c WHERE VTD1_Contact__c = :patientUser.ContactId];
        //delete contactPhonesListForDelete;
//        try {
//            //delete addressListForDelete;
//        } catch (Exception e) {
//            System.assertEquals(true, e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
//        }

        System.runAs(caregiverUser) {
            String patientProfileInitialString1 = VT_D1_PatientProfileIntialController.getInitialProfile();
            VT_D1_PatientProfileIntialController.PatientProfileInitial patientProfileInitial1 = (VT_D1_PatientProfileIntialController.PatientProfileInitial) JSON.deserialize(patientProfileInitialString1, VT_D1_PatientProfileIntialController.PatientProfileInitial.class);

//            VT_D1_Phone__c userPhone = new VT_D1_Phone__c(
//                    PhoneNumber__c = '4444',
//                    Account__c = caregiverUser.AccountId,
//                    VTD1_Contact__c = caregiverUser.ContactId
//            );
//            insert userPhone;
            //patientProfileInitial1.phone = userPhone;
            Address__c userAddress = new Address__c(
                    State__c = 'DE',
                    ZipCode__c = '5555',
                    VTD1_Primary__c = true,
                    VTD1_Contact__c = caregiverUser.ContactId
            );
            insert userAddress;
            patientProfileInitial1.address = userAddress;
            patientProfileInitial1.phone.PhoneNumber__c = '4545';
            patientProfileInitial1.address.State__c = 'NY';
            patientProfileInitial1.address.ZipCode__c = '1111111';
            //patientProfileInitial1.phoneNumberForCreate = '4444';
            //patientProfileInitial1.stateForCreate = 'DE';
            //patientProfileInitial1.zipCodeForCreate = '5555';
            //patientProfileInitial1.address = null;
            VT_D1_PatientProfileIntialController.updateProfile(JSON.serialize(patientProfileInitial1));

            List<VT_D1_Phone__c> contactPhonesList1 = [
                    SELECT Id, PhoneNumber__c
                    FROM VT_D1_Phone__c
                    WHERE VTD1_Contact__c = :caregiverUser.ContactId
                    AND IsPrimaryForPhone__c = TRUE
            ];
            System.assertEquals(1, contactPhonesList1.size());
            System.debug(contactPhonesList1);
            System.assertEquals('4545', contactPhonesList1.get(0).PhoneNumber__c);  // todo: uncomment

            List<Address__c> addressList1 = [
                    SELECT Id, State__c, ZipCode__c
                    FROM Address__c
                    WHERE VTD1_Contact__c = :caregiverUser.ContactId
                    AND Id NOT IN :addresses
            ];
            System.assertEquals('NY', addressList1.get(0).State__c);
            System.assertEquals('1111111', addressList1.get(0).ZipCode__c);
            System.assertNotEquals(null, VT_D1_PatientProfileIntialController.getPatientProfileInitialWrapper());

            caregiverUser = [SELECT VTD1_Filled_Out_Patient_Profile__c FROM User WHERE Id = :UserInfo.getUserId()];
            caregiverUser.VTD1_Filled_Out_Patient_Profile__c = false;
            update caregiverUser;

            System.assertNotEquals(null, VT_D1_PatientProfileIntialController.getInputSelectOptions('VTD1_Actual_Visit__c', 'VTR2_Modality__c'));
        }
        Test.stopTest();
    }
}