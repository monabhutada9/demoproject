/**
 * @author: Dmitry Yartsev & AK
 * @date: 06.04.2020
 * @description:  Patient Devices service for mobile app
 */



global with sharing class VT_R5_RestPatientDevices extends VT_R5_MobileRestResponse implements VT_R5_MobileRestRouter.Routable {
    Boolean duplicatedAppleWatchFound;
    Boolean successSynchronized;



    /**
     * @description Method for get patient devices
     *
     * @return List<VTD1_Patient_Device__c> A list of patient devices
     */
    private static List<VTD1_Patient_Device__c> getDevices() {
        List<VTD1_Patient_Device__c> devices = [
                SELECT Id,
                        Name,


                        VTR5_DeviceKeyId__c,
                        VTR5_SyncStatus__c,
                        VTD1_Manufacturer__c,
                        VTD1_Model_Name__c,


                        VTR5_LastReadingDate__c,
                        VTR5_Next_Reading_Date__c,
                        VTR5_LastDataSync__c,
                        VTD1_Protocol_Device__r.VTR5_Measurement_Type__c,
                        VTD1_Protocol_Device__r.VTD1_Device__r.VTR5_IsContinous__c,
                        VTD1_Protocol_Device__r.VTD1_Device__r.VTD1_Manufacturer__c,

                        VTD1_Protocol_Device__r.VTD1_Device__r.VTD1_DeviceType__c,
                        VTD1_Protocol_Device__r.VTD1_Device__r.VTR5_Image__c,
                        VTD1_Protocol_Device__r.VTD1_Device__c

                FROM VTD1_Patient_Device__c
                WHERE VTD1_Case__r.VTD1_Patient_User__c = :VT_D1_PatientCaregiverBound.getPatientId()
                AND VTR5_Active__c = TRUE
                AND RecordType.DeveloperName = 'VTD1_Connected_Device'
                AND VTD1_Protocol_Device__c != NULL
                AND VTD1_Protocol_Device__r.VTD1_Device__c != NULL

                AND VTR5_DeviceKeyId__c != NULL

        ];
        VT_D1_TranslateHelper.translate(devices);
        return devices;
    }




    global List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/devices/',
                '/patient/{version}/devices/{type}'
        };
    }



    global void versionRoute(Map<String, String> parameters) {
        String requestVersion = parameters.get('version');
        String requestType = parameters.get('type');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {


                        switch on requestType {
                            when null {
                                getDevices_v1();
                                return;
                            }
                            when 'archived-readings' {
                                getArchivedReadings_v1();
                                return;
                            }
                        }

                    }
                }



            }
            when 'PATCH' {
                switch on requestVersion {
                    when 'v1' {
                        switch on requestType {
                            when 'apple-watch' {
                                patch_v1_apple_watch();
                                return;
                            }
                        }
                    }
                }
            }
        }
        VT_R5_MobileRestRouter.unsupportedVersionResponse();
        this.addError('unsupported version');
        this.sendResponse();
    }



    private Map<String, List<String>> formArchiveReadings() {
        List<String> devicesSerials = new List<String>();

        if (request.params.containsKey('serials')) {
            String paramString = request.params.get('serials').remove(' ');
            devicesSerials.addAll(paramString.split(','));
            System.debug('devicesSerials'+devicesSerials);
        }

        Map<String, List<String>> deviceSerialNumberToArchivedIds = new Map<String, List<String>>();
        List<Id> deviceIds = new List<Id>();
        for (VTD1_Patient_Device__c device : getDevices()) {
            if (!devicesSerials.isEmpty()) {

                if (devicesSerials.contains(device.VTR5_DeviceKeyId__c)) {

                    deviceIds.add(device.Id);
                }
            } else {
                deviceIds.add(device.Id);
            }
        }
        for (VTR5_DeviceReadingAddInfo__c deviceReadingAddInfo : [
                SELECT VTR5_DeviceSerialNumber__c,
                        VTR5_Reading_ID__c,
                        VTR5_Archived__c,

                        VTR5_DeviceID__r.VTR5_DeviceKeyId__c,
                        VTR5_SecondReading_ID__c

                FROM VTR5_DeviceReadingAddInfo__c
                WHERE VTR5_DeviceID__c IN :deviceIds
                AND VTR5_Archived__c = TRUE
                AND VTR5_DeviceID__c != NULL
        ]) {

            String currentDeviceSerialNumber = deviceReadingAddInfo.VTR5_DeviceID__r.VTR5_DeviceKeyId__c;

            if (currentDeviceSerialNumber != null) {
                if (!deviceSerialNumberToArchivedIds.containsKey(currentDeviceSerialNumber)) {
                    deviceSerialNumberToArchivedIds.put(currentDeviceSerialNumber, new List<String>());
                }

                if (String.isNotBlank(deviceReadingAddInfo.VTR5_Reading_ID__c)) {
                    deviceSerialNumberToArchivedIds.get(currentDeviceSerialNumber).add(deviceReadingAddInfo.VTR5_Reading_ID__c);
                }
                if (String.isNotBlank(deviceReadingAddInfo.VTR5_SecondReading_ID__c)) {
                    deviceSerialNumberToArchivedIds.get(currentDeviceSerialNumber).add(deviceReadingAddInfo.VTR5_SecondReading_ID__c);
                }
            }

        }
        return deviceSerialNumberToArchivedIds;
    }

    private void getArchivedReadings_v1() {
        try {
            this.buildResponse(formArchiveReadings());
        } catch (Exception e) {
            this.buildResponse(e);


        }
        this.sendResponse();
    }


    private void patch_v1_apple_watch() {
        successSynchronized = false;

        try {
            List<VTD1_Patient_Device__c> devices = getAppleWatches(getDevices());
            if (!duplicatedAppleWatchFound) {
                SyncRequestParams requestParams = (SyncRequestParams) JSON.deserialize(request.requestBody.toString(), SyncRequestParams.class);
                updateAppleWatchReadings(devices, requestParams);
                successSynchronized = true;

            }
        } catch (Exception e) {

            this.buildResponse(e);
            this.sendResponse();
            return;
        }


        getDevices_v1();
    }




    private void updateAppleWatchReadings(List<VTD1_Patient_Device__c> devices, SyncRequestParams requestParams) {
        for (VTD1_Patient_Device__c device : devices) {
                device.VTR5_SyncStatus__c = requestParams.syncStatus;
                device.VTR5_LastDataSync__c = requestParams.lastSyncDate;
        }
        if (!devices.isEmpty()) update devices;
    }



    private void getDevices_v1() {
        try {

            Boolean singleDeviceNeed = false;
            Id deviceId;
            if (request.params.containsKey('id')) {
                singleDeviceNeed = true;
                deviceId = request.params.get('id');
            }
            List<MobilePatientDevice> result = new List<MobilePatientDevice>();
            List<VTD1_Patient_Device__c> devices = getDevices();
            getAppleWatches(devices);
            for (VTD1_Patient_Device__c device : devices) {
                if (singleDeviceNeed && !device.Id.equals(deviceId)) {
                    continue;
                }


                result.add(new MobilePatientDevice(device));
            }
            this.buildResponse(result);
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }



    private List<VTD1_Patient_Device__c> getAppleWatches(List<VTD1_Patient_Device__c> devices){
        List<VTD1_Patient_Device__c> result = new List<VTD1_Patient_Device__c>();
        for (VTD1_Patient_Device__c device : devices) {
            String manufacturer = device.VTD1_Protocol_Device__r.VTD1_Device__r.VTD1_Manufacturer__c;
            String deviceType = device.VTD1_Protocol_Device__r.VTD1_Device__r.VTD1_DeviceType__c;
            if (manufacturer != null && deviceType != null) {
                if (manufacturer.equals('Apple') && deviceType.equals('Smart Watch')) {
                    result.add(device);
                }
            }
        }
        duplicatedAppleWatchFound = result.size() > 1;
        return result;
    }


    global void initService() {}


    private class SyncRequestParams {
        String syncStatus;
        Datetime lastSyncDate;
    }


    private static String baseUrl;
    private static final String SERVICES_URL_PART = '/services/data/v46.0/sobjects/Product2/';
    private static final String IMAGE_URL_PART = '/richTextImageFields/VTR5_Image__c/';


    private without sharing class MobilePatientDevice {
        String id;
        String syncStatus;
        String manufacturer;
        String modelName;

        String deviceKeyId;
        String deviceImageLink;
        Datetime lastReadingDatetime;
        Datetime nextReadingDatetime;
        Datetime lastSyncDate;
        Boolean isContinuous;



        MobilePatientDevice(VTD1_Patient_Device__c dbDevice) {
            this.id = dbDevice.Id;
            this.syncStatus = dbDevice.VTR5_SyncStatus__c;
            this.manufacturer = dbDevice.VTD1_Manufacturer__c;
            this.modelName = dbDevice.VTD1_Model_Name__c;

            this.deviceKeyId = dbDevice.VTR5_DeviceKeyId__c;
            this.lastReadingDatetime = dbDevice.VTR5_LastReadingDate__c;
            this.nextReadingDatetime = dbDevice.VTR5_Next_Reading_Date__c;
            this.lastSyncDate = dbDevice.VTR5_LastDataSync__c;
            this.isContinuous = dbDevice.VTD1_Protocol_Device__r.VTD1_Device__r.VTR5_IsContinous__c;
            if (dbDevice.VTD1_Protocol_Device__r.VTD1_Device__r.VTR5_Image__c != null) {
                this.deviceImageLink = getImageLink(dbDevice);
            }
        }

        private String getImageLink(VTD1_Patient_Device__c dbDevice) {
            if (baseUrl == null) {
                baseUrl = Url.getOrgDomainUrl().toExternalForm();
            }
            return baseUrl +
                    SERVICES_URL_PART +
                    dbDevice.VTD1_Protocol_Device__r.VTD1_Device__c +
                    IMAGE_URL_PART +
                    dbDevice.VTD1_Protocol_Device__r.VTD1_Device__r.VTR5_Image__c.substringBetween('refid=','">');
        }

    }
}