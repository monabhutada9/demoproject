/**
* @author: Carl Judge
* @date: 22-Aug-19
* @description: Processes sharing calculations in batches. Used as a fallback from queueables when hitting limits
**/

public without sharing class VT_R3_GlobalSharingBatch extends VT_R5_QueueableBatch {
    private static final String QUEUE_NAME = 'Sharing';
    private VT_R3_AbstractGlobalSharingLogic sharingLogic;
    private VT_R5_TypeSerializable.SerializedItem serializedLogic;

    public VT_R3_GlobalSharingBatch (VT_R3_AbstractGlobalSharingLogic sharingLogic) {
        this.sharingLogic = sharingLogic.clone();
        this.sharingLogic.clearNextQueueable();
    }

    public Iterable<Object> start(Database.BatchableContext bc) {
        return sharingLogic.getBatchIterable();
    }

    public void execute(Database.BatchableContext bc, List<Object> scope) {
        sharingLogic.processBatchScope(scope);
    }

    public override Type getType() {
        return VT_R3_GlobalSharingBatch.class;
    }

    public override String getQueueName() {
        return QUEUE_NAME;
    }
    
    public virtual override void actionsBeforeSerialize() {
        super.actionsBeforeSerialize();
        serializeSharingLogic();
    }

    public virtual override void actionsAfterDeserialize() {
        super.actionsAfterDeserialize();
        deserializeSharingLogic();
    }

    private void serializeSharingLogic() {
        if (sharingLogic != null) {
            serializedLogic = sharingLogic.serializeWithType();
            sharingLogic = null;
        }
    }

    private void deserializeSharingLogic() {
        if (serializedLogic != null) {
            sharingLogic = (VT_R3_AbstractGlobalSharingLogic) serializedLogic.deserialize();
            serializedLogic = null;
        }
    }
}