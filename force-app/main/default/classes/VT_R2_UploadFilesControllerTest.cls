@IsTest
public with sharing class VT_R2_UploadFilesControllerTest {

    public static void testGetSessionId() {
        Test.startTest();
        String result = VT_D1_UploadFilesController.getSessionId();
        Test.stopTest();

        System.assert(!String.isEmpty(result));
    }
}