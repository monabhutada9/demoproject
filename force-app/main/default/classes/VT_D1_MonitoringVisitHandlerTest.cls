/**
 * Created by Andrey Pivovarov on 26.03.2019.
 *
 * @description Test class for VT_D1_MonitoringVisitHandler, VT_R3_GlobalSharingLogic_MonitoringVisit
 */

@IsTest
public with sharing class VT_D1_MonitoringVisitHandlerTest {
    public static HealthCloudGA__CarePlanTemplate__c study ;

    @TestSetup
    private static void setupMethod() {
        VT_R5_ShareGroupMembershipService.disableMembership = true;
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);       
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }
	
    @isTest
    public static void testBehavior() {
        Test.startTest();
        //Commented and Done the changes by Priyanka Ambre to remove Remote, Onsite references- SH-17441
        //START
        Virtual_Site__c vs = [SELECT Id, 
                              		 VTD1_Study__c, 
                              		 VTD1_Study__r.VTR5_ReadShareGroupId__c, 
                              		 VTD1_Study__r.VTR5_EditShareGroupId__c, 
                              		 VTD1_PI_User__c 
                              FROM Virtual_Site__c
                              LIMIT 1];
       
        List<Study_Team_Member__c> studyTeamMembers = [select Id, 
                                                       			RecordType.Name, 
                                                       			Study__c, User__c, 
                                                       			User__r.Profile.Name 
                                                       from Study_Team_Member__c 
                                                       where Study__c = :vs.VTD1_Study__c and User__r.Profile.Name = 'CRA' LIMIT 1];
        
        HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
        objVirtualShare.ParentId = vs.VTD1_Study__c;
        objVirtualShare.UserOrGroupId = studyTeamMembers[0].User__c;
        objVirtualShare.AccessLevel = 'Edit';
        insert objVirtualShare;
        
        VTD1_Monitoring_Visit__c mv  = new VTD1_Monitoring_Visit__c();
        mv.VTD1_Site_Visit_Type__c ='Interim Visit';
        //mv.VTR2_Remote_Onsite__c = 'Remote';
        mv.VTD1_Virtual_Site__c = vs.Id;
        mv.VTD1_Study__c = vs.VTD1_Study__c;
        mv.VTD1_Visit_Start_Date__c = System.now().addHours(2);
        try{
            insert mv;
        }catch(Exception exc){
            System.assert(exc.getMessage().containsIgnoreCase(System.Label.VTR5_CRAProfileErrorInMV));
        }
        System.runAs(new User(Id = studyTeamMembers[0].User__c)){
            try{
                insert mv;
            }catch(Exception exc){
                System.assert(exc.getMessage().containsIgnoreCase(System.Label.VTR5_CRANotSSTMErrorInMV));
            }
        }
        
        Study_Site_Team_Member__c sstm1 = new Study_Site_Team_Member__c();
        sstm1.VTR5_Associated_CRA__c = studyTeamMembers[0].Id;
        sstm1.VTD1_SiteID__c = vs.Id ;
        insert sstm1;
        
        System.runAs(new User(Id = studyTeamMembers[0].User__c)){
            insert mv;
            mv.VTD1_Visit_Planned_Date__c = Datetime.now();
            mv.VTD1_Assigned_PI__c = vs.VTD1_PI_User__c;
            update mv;
        }
        //END
        Test.stopTest();
    }
}