/**
 * Created by Alexander Komarov on 17.04.2019.
 */

@RestResource(UrlMapping='/Patient/Calendar/Reschedule/*')
global with sharing class VT_R3_RestPatientCalendarReschedule {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();

    @HttpPost
    global static String rescheduleRequest(String visitId, Long dateTimeFrom, Long dateTimeTo, String reason) {

        RestResponse response = RestContext.response;

        String dateTimeFromConverted = Datetime.newInstance(dateTimeFrom).format();
        String dateTimeToConverted = Datetime.newInstance(dateTimeTo).format();

        if (String.isBlank(visitId) || !helper.isIdCorrectType(visitId, 'VTD1_Actual_Visit__c')) {
            response.statusCode = 400;
            cr.buildResponse(helper.forAnswerForIncorrectInput());
            return JSON.serialize(cr, true);
        }

        try {
            VT_D1_PatientCalendar.updateActualVisit(visitId, dateTimeFromConverted, dateTimeToConverted, reason);
        } catch (Exception e) {
            response.statusCode = 500;
            cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName(),
                    System.Label.VT_D1_ErrorRequestReschedule);
            return JSON.serialize(cr, true);
        }
        cr.buildResponse('SUCCESS',System.Label.VTR2_SchedulingVisitSuccess);
        return JSON.serialize(cr, true);
    }
}