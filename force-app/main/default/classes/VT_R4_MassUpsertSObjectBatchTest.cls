/**
 * Created by maksimfesenko on 12/9/20.
 */

@IsTest
private class VT_R4_MassUpsertSObjectBatchTest {
    @IsTest
    private static void testBehavior() {
        List<VTD1_Document__c> documents = new List<VTD1_Document__c> {
            new VTD1_Document__c(),
            new VTD1_Document__c()
        };

        System.Test.startTest();
        Database.executeBatch(new VT_R4_MassUpsertSObjectBatch(documents));
        System.Test.stopTest();

        System.assertEquals(2, [SELECT COUNT() FROM VTD1_Document__c]);
    }
}