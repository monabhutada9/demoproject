public with sharing class VT_D1_CommunityPrivacyNoticeController {

    @AuraEnabled
    public static void accept() {
        User u = [SELECT Id, VTD1_Filled_Out_Patient_Profile__c, HIPAA_Accepted__c FROM User WHERE Id = :UserInfo.getUserId()];
        u.VTD1_Privacy_Notice_Last_Accepted__c = System.now();
        update u;

        PageReference p = !u.VTD1_Filled_Out_Patient_Profile__c  && u.HIPAA_Accepted__c ?
            new PageReference('/s/initial-profile-set-up') : Network.communitiesLanding();
        if (!Test.isRunningTest()) {
            Aura.redirect(p);
        }
    }

    @AuraEnabled
    public static void cancel() {
        PageReference p = new PageReference(Site.getBaseUrl() + '/secur/logout.jsp');
        if (!Test.isRunningTest()) {
            Aura.redirect(p);
        }
    }
}