/**
 * Created by Slav on 19.02.2019.
 */

@IsTest(SeeAllData = false)
public class VT_R2_McloudBroadcasterTest {
	static void setUpMCSettings() {
		List<VTR2_McloudSettings__mdt> mcloudSettings = new List<VTR2_McloudSettings__mdt>();
		mcloudSettings.add(new VTR2_McloudSettings__mdt(VTR2_OutboundMessageKey_API_Triggered__c = 'NjU6Nzg6MA',
				VTR2_RestURI__c = 'https://mc2gwv28hdsr86gd-sldnjzvzzgm.rest.marketingcloudapis.com/',
				VTR2_ShortCode__c = '99808'));
		VT_R2_McloudBroadcaster.setMCsettings(mcloudSettings);
	}

	@IsTest
	static void runTest() {
		setUpMCSettings();
		VT_R2_HttpCalloutMockImpl httpMock = new VT_R2_HttpCalloutMockImpl();
		Test.setMock(HttpCalloutMock.class, httpMock);
		
		Contact contact = new Contact();
		contact.FirstName = 'Mark';
		contact.LastName = 'Smith';
		insert contact;
		

		Test.startTest();

		VT_R2_McloudBroadcaster broadcaster = new VT_R2_McloudBroadcaster();
		
		httpMock.response = new HttpResponse();
		httpMock.response.setBody('{"accessToken":"ZutjPSYB9CXaNAqpZjdIxCZo","expiresIn":3479}');
		httpMock.response.setStatus('OK');
		httpMock.response.setStatusCode(200);
		broadcaster.authorize();
		httpMock.response.setStatus('ERROR');
		httpMock.response.setStatusCode(400);
		broadcaster.authorize();
		httpMock.response.setStatus('ERROR');
		httpMock.response.setStatusCode(202);
		broadcaster.authorize();

		System.assertEquals('Failed to authorize on Marketing Cloud', broadcaster.getErrorMessage());
		broadcaster.sendMessage(new Map<String, Id> {'12345678900' => contact.Id}, 'Hello, World!', 'NOTIFICATION_ID', null);
		try{
			broadcaster.sendMessage(new Map<String, Id> {'' => contact.Id}, '', '', null);
		}catch(Exception e){
			e.getMessage();
		}
		try{
			httpMock.response.setStatusCode(401);
			broadcaster.sendMessage(new Map<String, Id> {'12345678900' => contact.Id}, 'Hello, World!', 'NOTIFICATION_ID', null);
		}catch (Exception e){
			System.debug('$$$' +e.getMessage());
		}

	}
	@IsTest
	static void sendTest(){
		Contact contacTest = new Contact(
				lastName = 'Test'
		);
		insert contacTest;
		VTR2_SendSMSLog__c testSendSMSLog = new VTR2_SendSMSLog__c();
		testSendSMSLog.VTR2_Status__c ='Success';
		testSendSMSLog.VTR2_PhoneNumber__c = '13423424324';
		testSendSMSLog.VTR2_Contact__c = contacTest.id;
		testSendSMSLog.VTR2_MessageText__c = 'Hello, World!';
		testSendSMSLog.VTR2_NotificationId__c = '647774568';
		insert testSendSMSLog;
		VT_R2_McloudBroadcaster.send(testSendSMSLog.Id);
		testSendSMSLog.VTR2_MessageText__c = null;
		upsert  testSendSMSLog;
		VT_R2_McloudBroadcaster.send(testSendSMSLog.Id);


	}

}