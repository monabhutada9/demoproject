public class VT_R2_PIMyPatientEDiaryItemController {
    @AuraEnabled(Cacheable=true)
    public static List<VTD1_Survey_Answer__c> getDataECOA(Id surveyId) {
        Map<Id, VTD1_Survey_Answer__c> answers = getAnswersECOA(surveyId);
        return new List<VTD1_Survey_Answer__c>(answers.values());
    }
    @AuraEnabled(Cacheable=true)
    public static UserInfoWrapper getCurrentUserInfo() {
        User currentUser = [SELECT Id, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
        return new UserInfoWrapper(currentUser);
    }
    @AuraEnabled(Cacheable=false)
    public static List<GroupedQuestionWrapper> getQuestionsAnswers(Id surveyId, Id protocolId) {
        MyPatientEDiaryItemFactory factory = new MyPatientEDiaryItemFactory();
        factory.addAlgorithm(new PiMyPatientEDiaryItemSelector());
        factory.addAlgorithm(new ScrMyPatientEDiaryItemSelector());
        return factory.getQuestionsAnswers(surveyId, protocolId);
    }
    @AuraEnabled
    public static void flagSurveyAsPossibleConcern(Id surveyId) {
        MyPatientEDiaryItemFactory factory = new MyPatientEDiaryItemFactory();
        factory.addAlgorithm(new PiMyPatientEDiaryItemSelector());
        factory.addAlgorithm(new ScrMyPatientEDiaryItemSelector());
        factory.flagSurveyAsPossibleConcern(surveyId);
    }
    @AuraEnabled
    public static Boolean updateSurveyStatus(Id surveyId, String scores) {
        MyPatientEDiaryItemFactory factory = new MyPatientEDiaryItemFactory();
        factory.addAlgorithm(new PiMyPatientEDiaryItemSelector());
        factory.addAlgorithm(new ScrMyPatientEDiaryItemSelector());
        return factory.updateSurveyStatus(surveyId, scores);
    }

    @AuraEnabled
    public static Boolean updateMissedReason(Id surveyId, String missedReason) {
        MyPatientEDiaryItemFactory factory = new MyPatientEDiaryItemFactory();
        factory.addAlgorithm(new PiMyPatientEDiaryItemSelector());
        factory.addAlgorithm(new ScrMyPatientEDiaryItemSelector());
        return factory.updateMissedReason(surveyId, missedReason);
    }
    public static Map<Id, VTD1_Survey_Answer__c> getAnswersECOA(Id surveyId) {
        return new Map<Id, VTD1_Survey_Answer__c>([
                SELECT
                        VTD1_Answer__c,
                        VTD1_Score__c,
                        VTD1_Question__c,
                        VTD1_Survey__r.Name,
                        VTR5_External_Question__c,
                        VTR5_External_Question_Number__c,
                        RecordType.Name,                      
                        RecordTypeId
                FROM VTD1_Survey_Answer__c
                WHERE VTD1_Survey__c = :surveyId
                ORDER BY VTR5_External_Question_Number__c
        ]);
    }
    public interface MyPatientEDiaryItemSelector {
        Boolean isSuitableProfile();
        List<GroupedQuestionWrapper> getQuestionsAnswers(Id surveyId, Id protocolId);
        void flagSurveyAsPossibleConcern(Id surveyId);
        Boolean updateSurveyStatus(Id surveyId, String scores);
        Boolean updateMissedReason(Id surveyId, String missedReason);

    }
    public abstract without sharing class ProfileMyPatientEDiaryItemSelector implements MyPatientEDiaryItemSelector {
        protected abstract Id getProfileId();
        protected abstract void flagSurveyAsPossibleConcern(Id surveyId);
        protected abstract Boolean updateSurveyStatus(Id surveyId, String scores);
        protected abstract Boolean updateMissedReason(Id surveyId, String missedReason);
        protected void updateScore(String scoreJson) {
            List<ScoresWrapper> scores = (List<ScoresWrapper>) JSON.deserialize(scoreJson, List<ScoresWrapper>.class);
            Set<VTD1_Survey_Answer__c> answers = new Set<VTD1_Survey_Answer__c>();
            for (ScoresWrapper score: scores) {
                answers.add(new VTD1_Survey_Answer__c(
                        Id = Id.valueOf(score.id),
                        VTD1_Score__c = Integer.valueOf(score.score)
                ));
            }
            if (!answers.isEmpty()) {
                update new List<VTD1_Survey_Answer__c>(answers);
            }
        }

        public List<GroupedQuestionWrapper> getQuestionsAnswers(Id surveyId, Id protocolId) {
            Map<Id, VTD1_Protocol_ePro_Question__c> questionMap = getQuestions(surveyId, protocolId);
            VT_D1_TranslateHelper.translate(questionMap.values(), getSurveyById(surveyId)[0].VTR2_DocLanguage__c);
            return getGroupedAnswers(questionMap);
        }
        protected Map<Id, VTD1_Protocol_ePro_Question__c> getQuestions(Id surveyId, Id protocolId) {
            return new Map<Id, VTD1_Protocol_ePro_Question__c>([
                    SELECT  Id,
                            VTD1_Question_Content__c,
                            VTR2_QuestionContentLong__c,
                            VTR4_QuestionContentRichText__c,
                            VTD1_Type__c,
                            VTD1_Options__c,
                            VTD1_Number__c,
                            VTR4_Number__c,
                            VTR4_Branch_Parent__c,
                            VTR4_Response_Trigger__c,
                            VTD1_Protocol_ePRO__r.VTR4_Is_Branching_eDiary__c,
                            VTD1_Group_number__c,
                            VTR2_Scoring_Required__c,
                            VTR2_Scoring_Type__c,
                            VTR2_AllowOverridingAutoscore__c, (
                            SELECT  Id,
                                    VTD1_Answer__c,
                                    VTD1_Score__c
                            FROM    Survey_Answers__r
                            WHERE   VTD1_Survey__c = :surveyId
                            LIMIT 1
                    )
                    FROM    VTD1_Protocol_ePro_Question__c
                    WHERE   VTD1_Protocol_ePRO__c = :protocolId
                    ORDER BY VTD1_Group_number__c, VTR4_Number__c
            ]);
        }
        public Boolean isSuitableProfile() {
            return UserInfo.getProfileId() == this.getProfileId();
        }
        protected List<VTD1_Survey__c> getSurveyById(Id surveyId) {
            return [
                    SELECT  Id,
                            VTD1_Possible_ePRO_Safety_Concern__c,
                            VTD1_Status__c,
                            VTD1_Number_of_Answers__c,
                            VTR2_Number_of_Answered_Answers__c,
                            VTR4_Percentage_Completed__c,
                            RecordType.DeveloperName,
                            VTR2_DocLanguage__c,
                            VTR5_Reviewed__c 
                    FROM    VTD1_Survey__c
                    WHERE   Id = :surveyId
            ];
        }
        protected List<GroupedQuestionWrapper> getGroupedAnswers (Map<Id, VTD1_Protocol_ePro_Question__c> questionsMap) {
            List<GroupedQuestionWrapper> groupedAnswers = new List<GroupedQuestionWrapper>();
            Map<Id, List<QGroupWrapper>> eDiaryGroupQuestionMap = new Map<Id, List<QGroupWrapper>>();
            for (VTR2_Protocol_eDiary_Group_Question__c eDiaryGroupAnswer : [
                    SELECT  Id,
                            VTR2_Protocol_eDiary_Group__c,
                            VTR2_Protocol_eDiary_Group__r.Name,
                            VTR2_Protocol_eDiary_Question__c,
                            VTR2_Protocol_eDiary_Group__r.VTR2_Page_Number__c,
                            VTR2_Protocol_eDiary_Group__r.VTR2_Number_Within_Page__c
                    FROM    VTR2_Protocol_eDiary_Group_Question__c
            ]) {
                if (!eDiaryGroupQuestionMap.containsKey(eDiaryGroupAnswer.VTR2_Protocol_eDiary_Question__c)) {
                    eDiaryGroupQuestionMap.put(eDiaryGroupAnswer.VTR2_Protocol_eDiary_Question__c, new List<QGroupWrapper>());
                }
                eDiaryGroupQuestionMap.get(eDiaryGroupAnswer.VTR2_Protocol_eDiary_Question__c).add(
                        new QGroupWrapper(
                                eDiaryGroupAnswer.VTR2_Protocol_eDiary_Group__c,
                                eDiaryGroupAnswer.VTR2_Protocol_eDiary_Group__r.Name,
                                eDiaryGroupAnswer.VTR2_Protocol_eDiary_Group__r.VTR2_Page_Number__c,
                                eDiaryGroupAnswer.VTR2_Protocol_eDiary_Group__r.VTR2_Number_Within_Page__c
                        )
                );
            }
            for (VTD1_Protocol_ePro_Question__c question : questionsMap.values()) {
                GroupedQuestionWrapper a = new GroupedQuestionWrapper();
                a.question = question;
                a.groups = eDiaryGroupQuestionMap.get(question.Id);
                groupedAnswers.add(a);
            }
            return groupedAnswers;
        }
    }
    public class MyPatientEDiaryItemFactory {
        public List<MyPatientEDiaryItemSelector> algorithms;
        public MyPatientEDiaryItemFactory() {
            this.algorithms = new List<MyPatientEDiaryItemSelector>();
        }
        public List<GroupedQuestionWrapper> getQuestionsAnswers(Id surveyId, Id protocolId) {
            List<GroupedQuestionWrapper> result;
            for (MyPatientEDiaryItemSelector algorithm : this.algorithms) {
                if (algorithm.isSuitableProfile()) {
                    result = algorithm.getQuestionsAnswers(surveyId, protocolId);
                    break;
                }
            }
            return result;
        }
        public void flagSurveyAsPossibleConcern(Id surveyId) {
            for (MyPatientEDiaryItemSelector algorithm : this.algorithms) {
                if (algorithm.isSuitableProfile()) {
                    algorithm.flagSurveyAsPossibleConcern(surveyId);
                    break;
                }
            }
        }
        public Boolean updateSurveyStatus(Id surveyId, String scores) {
            Boolean result;
            for (MyPatientEDiaryItemSelector algorithm : this.algorithms) {
                if (algorithm.isSuitableProfile()) {
                    result = algorithm.updateSurveyStatus(surveyId, scores);
                    break;
                }
            }
            return result;
        }
        public Boolean updatemissedReason(Id surveyId, String missedReason) {
            Boolean result;
            for (MyPatientEDiaryItemSelector algorithm : this.algorithms) {
                if (algorithm.isSuitableProfile()) {
                    result = algorithm.updateMissedReason(surveyId, missedReason);
                    break;
                }
            }
            return result;
        }
        public void addAlgorithm(MyPatientEDiaryItemSelector myPatientEDiaryItemSelector) {
            this.algorithms.add(myPatientEDiaryItemSelector);
        }
    }
    public without sharing class PiMyPatientEDiaryItemSelector extends ProfileMyPatientEDiaryItemSelector {
        public override Id getProfileId() {
            return getPiProfileId();
        }
        public override void flagSurveyAsPossibleConcern(Id surveyId) {
            List<VTD1_Survey__c> surveys = getSurveyById(surveyId);
            if (surveys.isEmpty()) {
                return;
            }
            VTD1_Survey__c survey = surveys[0];
            survey.VTD1_Possible_ePRO_Safety_Concern__c = true;
            update survey;
        }
        public override Boolean updateSurveyStatus(Id surveyId, String scores) {
            List<VTD1_Survey__c> surveys = getSurveyById(surveyId);
            if (surveys.isEmpty()) {
                return null;
            }
            VTD1_Survey__c survey = surveys[0];
            if(survey.RecordType.DeveloperName!='VTR5_External'){
                if (survey.VTR4_Percentage_Completed__c < 100) {
                    survey.VTD1_Status__c = 'Reviewed (Partially Completed)';
                } else {
                    survey.VTD1_Status__c = 'Reviewed';
                }
            }else{
                survey.VTR5_Reviewed__c=true;
            }
            update survey;
            updateScore(scores);
            return surveyId != null;
        }
        public override Boolean updateMissedReason(Id surveyId, String missedReason) {
            List<VTD1_Survey__c> surveys = getSurveyById(surveyId);        
            VTD1_Survey__c survey = surveys[0];
            survey.VTR5_MissedReason__c=missedReason;
            //System.debug('Survey:' + survey);
            update survey;
            return surveyId != null;
        }
        private Id getPiProfileId() {
            return [SELECT Id FROM Profile WHERE Name = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME][0].Id;
        }
    }
    public without sharing class ScrMyPatientEDiaryItemSelector extends ProfileMyPatientEDiaryItemSelector {
        public override Id getProfileId() {
            return getScrProfileId();
        }
        public override void flagSurveyAsPossibleConcern(Id surveyId) {
            List<VTD1_Survey__c> surveys = getSurveyById(surveyId);
            if (surveys.isEmpty()) {
                return;
            }
            VTD1_Survey__c survey = surveys[0];
            survey.VTD1_Possible_ePRO_Safety_Concern__c = true;
            update survey;
        }
        public override Boolean updateSurveyStatus(Id surveyId, String scores) {
            List<VTD1_Survey__c> surveys = getSurveyById(surveyId);
            if (surveys.isEmpty()) {
                return false;
            }
            VTD1_Survey__c survey = surveys[0];
            if(survey.RecordType.DeveloperName!='VTR5_External'){
                System.debug('Status: ' + survey.VTD1_Status__c);
                if (survey.VTR4_Percentage_Completed__c < 100) {
                    survey.VTD1_Status__c = 'Reviewed (Partially Completed)';
                } else {
                    survey.VTD1_Status__c = 'Reviewed';
                }
                System.debug('Status changed to: ' + survey.VTD1_Status__c);
            }else{
                survey.VTR5_Reviewed__c=true;
            }
            update survey;
            updateScore(scores);
            return survey != null;
        }
        public override Boolean updateMissedReason(Id surveyId, String missedReason) {
            List<VTD1_Survey__c> surveys = getSurveyById(surveyId);
            if (surveys.isEmpty()) {
                return false;
            }
            VTD1_Survey__c survey = surveys[0];
            //System.debug('Survey: ' + survey);
            //System.debug('Missed reason: ' + missedReason);
            survey.VTR5_MissedReason__c=missedReason;
            update survey;
            return survey != null;
        }
        private Id getScrProfileId() {
            return [SELECT Id FROM Profile WHERE Name = :VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME][0].Id;
        }
    }
    @TestVisible
    class ScoresWrapper {
        public Id id;
        public String score;
        public ScoresWrapper(Id id, String score) {
            this.id = id;
            this.score = score;
        }
    }
    public class QGroupWrapper {
        @AuraEnabled public Id id;
        @AuraEnabled public String name;
        @AuraEnabled public Decimal page;
        @AuraEnabled public Decimal numberOnPage;
        public QGroupWrapper (Id id, String name, Decimal page, Decimal numberOnPage) {
            this.id = id;
            this.name = name;
            this.page = page;
            this.numberOnPage = numberOnPage;
        }
    }
    public class GroupedQuestionWrapper {
        @AuraEnabled public VTD1_Protocol_ePro_Question__c question;
        @AuraEnabled public List<QGroupWrapper> groups;
    }
    public class UserInfoWrapper {
        @AuraEnabled public String userId;
        @AuraEnabled public String profileName;
        public UserInfoWrapper(User currentUser) {
            this.userId = currentUser.Id;
            this.profileName = currentUser.Profile.Name;
        }
    }
}