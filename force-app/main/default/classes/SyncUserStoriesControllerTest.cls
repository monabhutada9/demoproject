/**
 * Created by user on 12/1/2020.
 */

@IsTest
private class SyncUserStoriesControllerTest {
    @IsTest
    static void testBehavior() {
        SyncUserStoriesController susc = new SyncUserStoriesController();
        PageReference pageRef = Page.SyncUserStories;
        Test.setCurrentPage(pageRef);
        Copado_Integration_Setting__c cis = new Copado_Integration_Setting__c(name = 'JIRA', external_system__c = 'JIRA', Named_Credential__c = 'Test_JIRA');
        insert cis;
        copado__Project__c pro = new copado__Project__c(name = 'MY JIRA Project', Project_External_Id__c = 'MTP', Copado_Integration_Setting__c = cis.id, enable_logs__c = true);
        insert pro;
        ApexPages.currentPage().getParameters().put('pid',pro.Id);
        susc.ScheduleFetchOperationAndReturn2Project();
    }
}