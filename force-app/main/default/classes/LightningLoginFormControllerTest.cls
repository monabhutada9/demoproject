@IsTest
public with sharing class LightningLoginFormControllerTest {

    public static void testLoginWithInvalidUrl() {
       System.assert(((String)LightningLoginFormController.login('testUser', 'fakepwd', null, 'test').get('errorMsg')).contains('Attempt to de-reference a null object'));
    }

    public static void testLoginWithInvalidCredential() {
        String testid = [
                SELECT
                        Id
                FROM Network
                WHERE Name = 'VTD2_PICommunity'
        ].Id;

        LightningLoginFormController.testNetworkId = testid;
        System.assert(((String)LightningLoginFormController.login([SELECT Email FROM User WHERE FirstName='PatientUser1'].Email, 'fakepwd', System.Network.getLoginUrl(testid), 'test').get('errorMsg')).contains(System.Label.VTR3_IncorrectCommunityMessage));
    }

    public static void testLoginWithValidCredential() {
        String testid = [
                SELECT
                        Id
                FROM Network
                WHERE name = 'VTD2_PatientCGCommunity'
        ].Id;

        LightningLoginFormController.testNetworkId = testid;

        User patient = [SELECT Id, Username,Email FROM User WHERE Profile.Name = 'Patient' ORDER BY CreatedDate DESC LIMIT 1];
        System.setPassword(patient.Id,'gfnhbr17');
        System.debug('patient' + patient);
        Test.startTest();
        LightningLoginFormController.login(patient.Username, 'gfnhbr17', System.Network.getLoginUrl(testid), 'test');
        Test.stopTest();
    }

    public static void LightningLoginFormControllerInstantiation() {
       LightningLoginFormController controller = new LightningLoginFormController();
       System.assertNotEquals(controller, null);
    }

    public static void testIsUsernamePasswordEnabled() {
       System.assertEquals(true, LightningLoginFormController.getIsUsernamePasswordEnabled());
    }

    public static void testAuthConfig() {
       Auth.AuthConfiguration authConfig = LightningLoginFormController.getAuthConfig();
       System.assertNotEquals(null, authConfig);
    }

    public static void testSetExperienceId() {
       System.assertEquals(null, LightningLoginFormController.setExperienceId(null));
       System.assertEquals('This method can be invoked only from within a community.', LightningLoginFormController.setExperienceId(''));
    }

    public static void testForgotPassword() {
       System.assertEquals(false, LightningLoginFormController.forgotPassword('username'));
    }
}