/**
 * Created by Eugene Pavlovskiy on 11-May-20.
 */
@IsTest
public with sharing class VT_R5_RunConsentVisitTest {

    public static void sendSignalTest() {
        List<Case> casesList = [SELECT Id, AccountId, VTD1_Primary_PG__c, VTD1_PI_user__c, VTD1_PI_user__r.ContactId, VTD1_Study__c FROM Case WHERE VTD1_Study__c != null];
        System.assert(casesList.size() > 0);
        if (casesList.size() > 0) {
            Case cas = casesList.get(0);

            VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
            protocolVisit.VTD1_Range__c = 4;
            protocolVisit.VTD1_VisitOffset__c = 2;
            protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
            protocolVisit.VTD1_VisitDuration__c = '30 minutes';
            protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
            protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
            protocolVisit.VTD1_Study__c = cas.VTD1_Study__c;
            protocolVisit.VTD1_VisitType__c = 'Labs';
            insert protocolVisit;

            VTD1_Actual_Visit__c visit = new VTD1_Actual_Visit__c(
                VTD1_Protocol_Visit__c = protocolVisit.Id,
                VTD1_Scheduled_Date_Time__c = Datetime.now(),
                VTD1_Status__c = 'To Be Scheduled',
                VTD1_Case__c = cas.Id,
                VTD1_Contact__c = cas.VTD1_PI_user__r.ContactId
            );
            insert visit;
            insert new Visit_Member__c(VTD1_Actual_Visit__c = visit.Id, VTD1_Participant_User__c = cas.VTD1_PI_user__c);
            VT_R5_RunConsentVisit.ParamsVisit paramsVisit = new VT_R5_RunConsentVisit.ParamsVisit();
            paramsVisit.scheduledDate = visit.VTD1_Scheduled_Date_Time__c;
            paramsVisit.accountId = cas.AccountId;
            paramsVisit.visitId = visit.Id;
            List <VT_R5_RunConsentVisit.ParamsVisit> args = new List<VT_R5_RunConsentVisit.ParamsVisit> {
                paramsVisit
            };
            VT_R5_RunConsentVisit.sendSignal(args);
            for (Database.SaveResult result : VT_R5_RunConsentVisit.results) {
                System.assertEquals(true, result.isSuccess());
            }
        }
    }
}