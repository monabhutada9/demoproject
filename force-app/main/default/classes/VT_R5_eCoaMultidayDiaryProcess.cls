/**
* @author: Carl Judge
* @date: 19-Oct-20
* @description: TEMPORARY - fix for Jansen specific multi-day diaries until a more general solution can be made
**/

public without sharing class VT_R5_eCoaMultidayDiaryProcess {
    public static Set<String> multiDayRuleNames {
        get {
            if (multiDayRuleNames == null) {
                multiDayRuleNames = new Set<String>();
                List<eCOA_Multi_Day_Rule__mdt> rules = (List<eCOA_Multi_Day_Rule__mdt>) new VT_Stubber.ResultStub(
                    'VT_R5_eCoaMultidayDiaryProcess.getRules',
                    [SELECT Label FROM eCOA_Multi_Day_Rule__mdt]
                ).getResult();
                for (eCOA_Multi_Day_Rule__mdt rule : rules) {
                    multiDayRuleNames.add(rule.Label);
                }
            }
            return multiDayRuleNames;
        } set;
    }
    private static List<VTD1_Survey__c> deleteDiaries;

    // if we get an upsert for one of these diaries that tries to reduce the window, dont let it happen.
    public static void onBeforeUpdate(List<VTD1_Survey__c> diaries, Map<Id, VTD1_Survey__c> oldMap) {
        for (VTD1_Survey__c diary : getDueSoonMultiDay(diaries)) {
            if (diary.VTD1_Due_Date__c < oldMap.get(diary.Id).VTD1_Due_Date__c) {
                diary.VTD1_Due_Date__c = oldMap.get(diary.Id).VTD1_Due_Date__c;
            }
        }
    }

    public static void onBeforeInsert(List<VTD1_Survey__c> diaries) {
        deleteDiaries = new List<VTD1_Survey__c>();
        List<VTD1_Survey__c> multiDayDiaries = getDueSoonMultiDay(diaries);
        if (!multiDayDiaries.isEmpty()) {
            Map<String, VTD1_Survey__c> existingDiaryMap = getExistingDiaries(multiDayDiaries);
            List<VTD1_Survey__c> combinedTriggerDiaries = mergeTriggerDiaries(multiDayDiaries);
            mergeWithExistingDiaries(existingDiaryMap, combinedTriggerDiaries);
        }
    }

    public static void onAfterInsert(List<VTD1_Survey__c> diaries) {
        if (!deleteDiaries.isEmpty()) {
            VT_D1_SurveyHandler.ignoreDeleteValidation = true;


            List<VTD1_Survey__c> surveyForDelete = [SELECT Id FROM VTD1_Survey__c WHERE Id IN :deleteDiaries];
            delete surveyForDelete;
            Database.emptyRecycleBin(surveyForDelete);


        }
    }

    private static List<VTD1_Survey__c> mergeTriggerDiaries(List<VTD1_Survey__c> diaries) {
        Map<String, VTD1_Survey__c> combinedDiaryMap = new Map<String, VTD1_Survey__c>();
        for (VTD1_Survey__c currentDiary : diaries) {
            String key = currentDiary.VTR5_eCoa_RuleName__c + currentDiary.VTD1_CSM__c;
            if (!combinedDiaryMap.containsKey(key)) {
                combinedDiaryMap.put(key, currentDiary);
            } else {
                VTD1_Survey__c combinedDiary = combinedDiaryMap.get(key);
                if (currentDiary.VTD1_Due_Date__c > combinedDiary.VTD1_Due_Date__c) {
                    combinedDiary.VTD1_Due_Date__c = currentDiary.VTD1_Due_Date__c;
                }
                if (currentDiary.VTD1_Date_Available__c < combinedDiary.VTD1_Date_Available__c) {
                    combinedDiary.VTD1_Date_Available__c = currentDiary.VTD1_Date_Available__c;
                    combinedDiary.VTR5_eCoaCompositeKey__c = currentDiary.VTR5_eCoaCompositeKey__c;
                }
                deleteDiaries.add(currentDiary);
            }
        }
        return combinedDiaryMap.values();
    }

    private static Map<String, VTD1_Survey__c> getExistingDiaries(List<VTD1_Survey__c> diaries) {
        Set<String> ruleNames = new Set<String>();
        Set<Id> caseIds = new Set<Id>();
        for (VTD1_Survey__c diary : diaries) {
            ruleNames.add(diary.VTR5_eCoa_RuleName__c);
            caseIds.add(diary.VTD1_CSM__c);
        }
        Map<String, VTD1_Survey__c> existingDiaryMap = new Map<String, VTD1_Survey__c>();
        for (VTD1_Survey__c diary : [
            SELECT Id, VTD1_Due_Date__c, VTR5_eCoa_RuleName__c, VTD1_CSM__c
            FROM VTD1_Survey__c
            WHERE VTR5_eCoa_RuleName__c IN :ruleNames
            AND VTD1_CSM__c IN :caseIds
            AND RecordType.DeveloperName = 'VTR5_External'
            AND VTD1_Status__c = 'Due Soon'
            AND Id NOT IN :diaries
            ORDER BY VTD1_Due_Date__c
        ]) {
            existingDiaryMap.put(diary.VTR5_eCoa_RuleName__c + diary.VTD1_CSM__c, diary);
        }
        return existingDiaryMap;
    }

    private static List<VTD1_Survey__c> getDueSoonMultiDay(List<VTD1_Survey__c> diaries) {
        List<VTD1_Survey__c> multiDayDiaries = new List<VTD1_Survey__c>();
        for (VTD1_Survey__c diary : diaries) {
            if (diary.VTD1_Status__c == 'Due Soon' && multiDayRuleNames.contains(diary.VTR5_eCoa_RuleName__c)) {
                multiDayDiaries.add(diary);
            }
        }
        return multiDayDiaries;
    }
    
    private static void mergeWithExistingDiaries(Map<String, VTD1_Survey__c> existingDiaryMap, List<VTD1_Survey__c> combinedTriggerDiaries) {
        List<VTD1_Survey__c> existingDiariesToUpdate = new List<VTD1_Survey__c>();
        for (VTD1_Survey__c triggerDiary : combinedTriggerDiaries) {
            String key = triggerDiary.VTR5_eCoa_RuleName__c + triggerDiary.VTD1_CSM__c;
            if (existingDiaryMap.containsKey(key)) {
                VTD1_Survey__c existingDiary = existingDiaryMap.get(key);
                if (existingDiary.VTD1_Due_Date__c < triggerDiary.VTD1_Due_Date__c) {
                    existingDiary.VTD1_Due_Date__c = triggerDiary.VTD1_Due_Date__c;
                    existingDiariesToUpdate.add(existingDiary);
                }
                deleteDiaries.add(triggerDiary);
            }
        }
        if (!existingDiariesToUpdate.isEmpty()) { update existingDiariesToUpdate; }
    }
}