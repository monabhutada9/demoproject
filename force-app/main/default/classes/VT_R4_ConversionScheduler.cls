/**
 * Created by user on 08.07.2020.  not longer needed - switched to batch for conversion instead of platform events
 */

global with sharing class VT_R4_ConversionScheduler {
    /*private Id jobId;
    global void execute(SchedulableContext sc) {
        if (sc != null) {
            this.jobId = sc.getTriggerId();
            if (jobId != null) {
                System.abortJob(jobId);
            }
        }
        List <VTR4_Semaphore__c> semaphores = null;
        try {
            semaphores = [
                    SELECT Id, Name
                    FROM VTR4_Semaphore__c
                    WHERE Name = 'ConversionSemaphore'
                    FOR UPDATE
            ];
        } catch (Exception e) {
        }
        System.debug('semaphores = ' + semaphores);
        if (semaphores != null) {



            VT_R4_PatientConversion.setMaxNumberInProgress();



            Datetime dtInProgressExpired = System.now().addMinutes(-10);
            AggregateResult [] aggResults = [
                    SELECT count(Id) num
                    FROM HealthCloudGA__CandidatePatient__c
                    WHERE VTR4_Convertation_Process_Status__c = :VT_R4_PatientConversion.PROCESS_STATUS_IN_PROGRESS
                    AND LastModifiedDate > :dtInProgressExpired
            ];
            Integer inProgressNum = (Integer)aggResults[0].get('num');
            aggResults = [
                    SELECT count(Id) num
                    FROM HealthCloudGA__CandidatePatient__c
                    WHERE VTR4_Convertation_Process_Status__c IN (:VT_R4_PatientConversion.PROCESS_STATUS_INITIAL,
                            :VT_R4_PatientConversion.PROCESS_STATUS_INITIAL_BULK)
            ];
            Integer initialNum = (Integer) aggResults[0].get('num');
            /*AggregateResult [] results = [select count(Id) num, VTR4_Convertation_Process_Status__c from HealthCloudGA__CandidatePatient__c where
                    VTR4_Convertation_Process_Status__c in (:VT_R4_PatientConversion.PROCESS_STATUS_INITIAL, :VT_R4_PatientConversion.PROCESS_STATUS_IN_PROGRESS) group by VTR4_Convertation_Process_Status__c];
            for (AggregateResult result : results) {
                String status = (String)result.get('VTR4_Convertation_Process_Status__c');
                if (status == VT_R4_PatientConversion.PROCESS_STATUS_INITIAL) {
                    initialNum = (Integer) result.get('num');
                } else {
                    inProgressNum = (Integer) result.get('num');
                }
            }/
            Integer limitValue = 0;
            if (!skipStage() && inProgressNum < VT_R4_PatientConversion.MAX_CPS_NUMBER_IN_PROGRESS) {
                limitValue = VT_R4_PatientConversion.MAX_CPS_NUMBER_IN_PROGRESS - inProgressNum;
                System.debug('limitValue = ' + limitValue);
            }
            System.debug('inProgressNum = ' + inProgressNum);
            System.debug('initialNum = ' + initialNum);
            System.debug('limitValue = ' + limitValue);
            if (limitValue > 0) {
                String query = 'select Id, VTR4_Convertation_Process_Status__c from HealthCloudGA__CandidatePatient__c' +
                        + ' where VTR4_Convertation_Process_Status__c in (\'' + VT_R4_PatientConversion.PROCESS_STATUS_INITIAL + '\',\'' +
                        + VT_R4_PatientConversion.PROCESS_STATUS_INITIAL_BULK + '\') and Conversion__c =\'Draft\'' +
                        ' order by VTR5_Priority__c desc, CreatedDate limit ' + limitValue;
                List <HealthCloudGA__CandidatePatient__c> cps = Database.query(query);
                System.debug('new cps = ' + cps.size());
                if (cps.size() > 0) {
                    //Set<Id> candidatePatientIdsToBeConvertedByNewProcess = new Set<Id>();
                    for (HealthCloudGA__CandidatePatient__c cp : cps) {
                        cp.VTR4_Convertation_Process_Status__c = VT_R4_PatientConversion.PROCESS_STATUS_IN_PROGRESS;
                        cp.VTR5_Conversion_Started__c = System.now();
                    }
                    update cps;
                    VT_R4_PatientConversion.convert(cps);
                    //VT_D1_CandidatePatientProcessHandler.conversionCPRunBatch(candidatePatientIdsToBeConvertedByNewProcess);
                }
            }
            if (sc != null && initialNum > limitValue) {
                scheduleJob(System.now().addMinutes(VT_R4_PatientConversion.SCHEDULER_MINUTES_INTERVAL), jobId);
            }
        } else if (sc != null) {
            scheduleJob(System.now().addMinutes(VT_R4_PatientConversion.SCHEDULER_MINUTES_INTERVAL), jobId);
        }
    }
    public static void scheduleJob(Datetime dt, Id jobId) {
        System.debug('scheduleJob ' + dt);
        List <CronTrigger> triggers = [
                SELECT Id
                FROM CronTrigger
                WHERE CronJobDetail.Name = 'ConversionScheduler'
        ];
        if (!triggers.isEmpty()) {
            System.debug('already scheduled');
            return;
        }
        if (dt == null) {
            dt = System.now().addMinutes(VT_R4_PatientConversion.SCHEDULER_MINUTES_INTERVAL);
        }
        Integer minutes = dt.minute();
        Integer hours = dt.hour();
        String cron = '0 ' + minutes + ' ' + hours + ' * * ?';
        System.debug('cron = ' + cron);
        try {

        } catch (Exception e) {
            System.debug('schedule error ' + e.getMessage());
        }
    }
    private Boolean skipStage() {
        AggregateResult [] aggResults = [
                SELECT count(id) num
                FROM AsyncApexJob
                WHERE ApexClass.Name = 'VT_R3_GlobalSharingBatch'
                AND Status in ('Holding', 'Queued', 'Processing')
        ];
        Integer jobsNumber = (Integer) aggResults[0].get('num');
        return jobsNumber > 10;
    }
    public static void forceNextStage(List<HealthCloudGA__CandidatePatient__c> cps, Map<Id, HealthCloudGA__CandidatePatient__c> oldMap) {
        Boolean isForce = false;
        for (HealthCloudGA__CandidatePatient__c cp : cps) {
            if (cp.VTR4_Convertation_Process_Status__c == VT_R4_PatientConversion.PROCESS_STATUS_COMPLETED
                    && oldMap.get(cp.Id).VTR4_Convertation_Process_Status__c != VT_R4_PatientConversion.PROCESS_STATUS_COMPLETED) {
                isForce = true;
                break;
            }
        }
        if (isForce) {

        }
    }
*/

}