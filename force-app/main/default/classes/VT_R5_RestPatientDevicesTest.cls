/**
 * @author: Alexander Komarov
 * @date: 14.04.2020
 * @description: Unit test for VT_R5_RestPatientDevices
 */

@IsTest
private class VT_R5_RestPatientDevicesTest {
    /**
     * @description Method for setup current rest context
     *
     * @param requestURI String request URL
     * @param method String Http method (GET, POST)
     * @param body String Request body
     */
    private static void setRestContext(String requestURI, String method, String body) {
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        if (body != null) {
            request.requestBody = Blob.valueOf(body);
        }
        request.requestURI = requestURI;
        request.httpMethod = method;
        RestContext.request = request;
        RestContext.response = response;
    }


    @TestSetup
    static void setup() {
        DomainObjects.Account_t account_t = new DomainObjects.Account_t().setRecordTypeByName('Sponsor');
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(account_t);
        study.persist();

        Account account = (Account) account_t.toObject();

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
                .setFirstName('PatientDevices')
                .setLastName('PatientDevices')
                .setAccountId(account.Id);

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile('Patient')
                .setUsername('VT_R5_RestPatientDevicesTest@test.com');
        patientUser.persist();

        Case patientCase = (Case) new DomainObjects.Case_t()
                .addPIUser(new DomainObjects.User_t())
                .addVTD1_Patient_User(patientUser)
                .setStudy(((HealthCloudGA__CarePlanTemplate__c) study.toObject()).Id)
                .setContactId(((User) patientUser.toObject()).ContactId)
                .setRecordTypeByName('CarePlan')
                .setAccountId(account.Id)
                .persist();

        patientContact.setVTD1_Clinical_Study_Membership(patientCase.Id);
        update (Contact) patientContact.toObject();
        DomainObjects.VTD1_Protocol_Delivery_t pDelivery = new DomainObjects.VTD1_Protocol_Delivery_t()
                .addCare_Plan_Template(study);
        DomainObjects.VTD1_Protocol_Kit_t pKit = new DomainObjects.VTD1_Protocol_Kit_t()
                .setRecordTypeByName('VTD1_Connected_Devices')
                .addVTD1_Care_Plan_Template(study)
                .addVTD1_Protocol_Delivery(pDelivery);
        pKit.persist();

        DomainObjects.Product2_t device1 = new DomainObjects.Product2_t()
                .setProductName('Device')
                .setRecordTypeByName('VTD1_Connected_Devices')
                .setVTD1_DeviceType('Smart Watch')
                .setVTD1_Manufacturer('Apple')
                .setVTR5_Image('<img alt="User-added image" src="SOME_URL/servlet/rtaImage?eid=01t2g00000072Ri&amp;feoid=00N2g000000vm1Z&amp;refid=0EM2g000000Cyn7"></img>')
                .setVTR5_IsContinous(true);
        device1.persist();
        DomainObjects.HealthCloudGA_EhrDevice_t ehrDevice = new DomainObjects.HealthCloudGA_EhrDevice_t()
                .addProduct(device1)
                .addVTD1_Care_Plan_Template(study)
                .addVTD1_Protocol_Kit(pKit)
                .setVTD1_Kit_Type('Connected Devices')
                .addMeasurementType('heart_rate');
        ehrDevice.persist();
        DomainObjects.Patient_Device_t patientDevice = new DomainObjects.Patient_Device_t()
                .addEhrDevice(ehrDevice)
                .setModelName('Test Apple Watch')
                .setCaseId(patientCase.Id)
                .setRecordTypeByName('VTD1_Connected_Device')
                .setVTR5_Active(true);

        patientDevice.persist();

        patientDevice.setDeviceKeyId('123iqvia1234');
        update (VTD1_Patient_Device__c) patientDevice.toObject();

        DomainObjects.VTD1_Protocol_Delivery_t pDelivery2 = new DomainObjects.VTD1_Protocol_Delivery_t()
                .addCare_Plan_Template(study);
        DomainObjects.VTD1_Protocol_Kit_t pKit2 = new DomainObjects.VTD1_Protocol_Kit_t()
                .setRecordTypeByName('VTD1_Connected_Devices')
                .addVTD1_Care_Plan_Template(study)
                .addVTD1_Protocol_Delivery(pDelivery2);
        pKit2.persist();

        DomainObjects.Product2_t device2 = new DomainObjects.Product2_t()
                .setProductName('Device2')
                .setRecordTypeByName('VTD1_Connected_Devices')
                .setVTD1_DeviceType('Weight Scale')
                .setVTD1_Manufacturer('BodyTrace')
                .setVTR5_Image('<img alt="User-added image" src="SOME_URL/servlet/rtaImage?eid=01t2g00000072Ri&amp;feoid=00N2g000000vm1Z&amp;refid=0EM2g000000Cyn7"></img>')
                .setVTR5_IsContinous(true);
        device2.persist();
        DomainObjects.HealthCloudGA_EhrDevice_t ehrDevice2 = new DomainObjects.HealthCloudGA_EhrDevice_t()
                .addProduct(device2)
                .addVTD1_Care_Plan_Template(study)
                .addVTD1_Protocol_Kit(pKit2)
                .setVTD1_Kit_Type('Connected Devices')
                .addMeasurementType('heart_rate');
        ehrDevice2.persist();
        DomainObjects.Patient_Device_t patientDevice2 = new DomainObjects.Patient_Device_t()
                .addEhrDevice(ehrDevice2)
                .setModelName('Test Apple Watch 2')
                .setCaseId(patientCase.Id)
                .setRecordTypeByName('VTD1_Connected_Device')
                .setVTR5_Active(true);

        patientDevice2.persist();

        patientDevice2.setDeviceKeyId('iqvia12345');
        update (VTD1_Patient_Device__c) patientDevice2.toObject();


        DomainObjects.VTR5_DeviceReadingAddInfo_t deviceReadingAddInfo = new DomainObjects.VTR5_DeviceReadingAddInfo_t()
                .addDeviceId(patientDevice)
                .setVTR5_Reading_ID('someid12')
                .setVTR5_SecondReading_ID('someid21')
                .setVTR5_Archived(true);
        deviceReadingAddInfo.persist();
    }

    @IsTest
    static void getDevicesTest() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_RestPatientDevicesTest@test.com' LIMIT 1];
        System.runAs(toRun) {
            setRestContext('/patient/v1/devices/', 'GET', null);
            VT_R5_MobileRestRouter.doGET();
        }
    }
    @IsTest
    static void getDevices2Test() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_RestPatientDevicesTest@test.com' LIMIT 1];
        System.runAs(toRun) {
            String deviceId = [SELECT Id FROM VTD1_Patient_Device__c][0].Id;
            setRestContext('/patient/v1/devices', 'GET', null);
            RestContext.request.params.put('id', deviceId);
            VT_R5_MobileRestRouter.doGET();
        }
    }
    @IsTest
    static void syncDevicesTest() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_RestPatientDevicesTest@test.com' LIMIT 1];
        System.runAs(toRun) {
            setRestContext('/patient/v1/devices/apple-watch', 'PATCH', JSON.serialize(getSyncParams()));
            VT_R5_MobileRestRouter.doPATCH();
        }
    }
    @IsTest
    static void syncDevicesRejectedTest() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_RestPatientDevicesTest@test.com' LIMIT 1];
        System.runAs(toRun) {
            setRestContext('/patient/v1/devices/apple-watch', 'PATCH', 'not valid json');
            VT_R5_MobileRestRouter.doPATCH();
        }

    }
    @IsTest
    static void getArchivedReadingsTest() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_RestPatientDevicesTest@test.com' LIMIT 1];
        System.runAs(toRun) {
            setRestContext('/patient/v1/devices/archived-readings', 'GET', null);
            VT_R5_MobileRestRouter.doGET();
        }
    }
    @IsTest
    static void getArchivedReadings2Test() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_RestPatientDevicesTest@test.com' LIMIT 1];
        System.runAs(toRun) {
            setRestContext('/patient/v1/devices/archived-readings', 'GET', null);
            RestContext.request.params.put('serials', '123iqvia1234,123');
            VT_R5_MobileRestRouter.doGET();
        }
    }
    @IsTest
    static void unsupportedVersionTest() {
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_RestPatientDevicesTest@test.com' LIMIT 1];
        System.runAs(toRun) {
            setRestContext('/patient/v14/devices/archived-readings', 'GET', null);
            VT_R5_MobileRestRouter.doGET();
        }
    }

    private static SyncRequestParams getSyncParams() {
        SyncRequestParams syncRequestParams = new SyncRequestParams();
        syncRequestParams.syncStatus = 'Up to Date';
        syncRequestParams.lastSyncDate = System.now();
        return syncRequestParams;
    }

    private class SyncRequestParams {
        String syncStatus;
        Datetime lastSyncDate;
    }
}