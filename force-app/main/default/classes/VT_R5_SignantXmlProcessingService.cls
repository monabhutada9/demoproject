/**
 * @author: Aliaksandr Vabishchevich
 * @date: 20-Aug-2020
 * @description: Debug web service for Signant XMLs
 */
@RestResource(UrlMapping='/SignantXmlProcessingService/*')
global with sharing class VT_R5_SignantXmlProcessingService {

    @HttpPost
    global static String processSignantPatients() {
        RestRequest req = RestContext.request;
        String receivedXml = req.requestBody.toString();
        DOM.Document doc = new DOM.Document();
        doc.load(receivedXml);
        DOM.XmlNode node = doc.getRootElement();

        VTR5_Signant_File__c sigFile = new VTR5_Signant_File__c(
                VTR5_XMLContent__c = receivedXml,
                Name = node.getChildElement('fileName', null).getText(),
                VTR5_DateReceived__c = Datetime.now()
        );
        insert sigFile;
        return Url.getSalesforceBaseUrl() + '/' + sigFile.Id;
    }


}