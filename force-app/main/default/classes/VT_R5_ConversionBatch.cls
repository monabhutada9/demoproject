/**
 * Created by shume on 10/2/2020.
 */

global class VT_R5_ConversionBatch implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts {
    
    private static final String CP_QUERY =
        'SELECT Id ' +
        'FROM HealthCloudGA__CandidatePatient__c ' +
        'WHERE VTR4_Convertation_Process_Status__c IN  (\'Initial\', \'Initial Bulk\') AND Conversion__c =\'Draft\'' +
        'ORDER BY VTR5_Priority__c DESC, CreatedDate';
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(CP_QUERY);
    }
    
    global void execute(Database.BatchableContext bc, List<SObject> cpList) {
        List<HealthCloudGA__CandidatePatient__c> cpListForProcess = Database.query(CP_QUERY + ' LIMIT ' + getRecommendedScopeSize());
        for (HealthCloudGA__CandidatePatient__c cp : cpListForProcess) {
            cp.VTR5_Conversion_Started__c = System.now();
            cp.VTR4_Convertation_Process_Status__c = VT_R4_PatientConversion.PROCESS_STATUS_IN_PROGRESS;
        }
        update cpListForProcess;
        VT_R4_PatientConversion.convert(cpListForProcess);
    }

    public void finish(Database.BatchableContext param1) {
        launchBatch(false);
    }

    public static void launchBatch(Boolean deleteJob) {
        if (deleteJob) {
            List<CronTrigger> jobs = [SELECT Id FROM CronTrigger Where CronJobDetail.Name like'%VT_R5_ConversionBatch%'];
            if (!jobs.isEmpty() && !Test.isRunningTest()) { // added to avoid aborting the actual batch in the org
                System.abortJob(jobs[0].Id);
            }
        }
        List<HealthCloudGA__CandidatePatient__c> cpListForProcess = Database.query(CP_QUERY + ' LIMIT 1');
        if (!cpListForProcess.isEmpty()) {
            Database.executeBatch(new VT_R5_ConversionBatch(), getRecommendedScopeSize());
        } else {
            System.scheduleBatch((Database.Batchable<SObject>)
                    new VT_R5_ConversionBatch(),
                    'VT_R5_ConversionBatch ' + System.now(),
                    1,
                    getRecommendedScopeSize()
            );
        }
    }
    
    public static Integer getRecommendedScopeSize() {
        Integer batch_size = 20;
        List<VTD1_General_Settings__mdt> settings = [
                SELECT VTR5_Conversion_Pool_Size__c
                FROM VTD1_General_Settings__mdt
                LIMIT 1
        ];
        if (!settings.isEmpty()) {
            if (settings[0].VTR5_Conversion_Pool_Size__c != null) batch_size = settings[0].VTR5_Conversion_Pool_Size__c.intValue();
        }
        return batch_size;
    }
}