/**
 * Created by Maksim Fedarenka on 20-May-20.
 */

@IsTest
public class VT_R4_DocumentServiceTest {
	public static final String DOCUMENT_WITH_CASE = 'documentWithCase';
	public static final String DOCUMENT_WITHOUT_CASE = 'documentWithoutCase';
	public static final String DOCUMENT_WITH_OLD_STUDY = 'documentWithOldStudy';
	public static final String UNLINKED_DOCUMENT = 'unlinkedDocument';

	public static void populateStudyAndVirtualSiteTest() {
		Case aCase = getCase();

		Test.startTest();
		Map<String, VTD1_Document__c> documentNameDocumentMap = generateDocuments(aCase);
		Test.stopTest();

		assertResults(aCase, documentNameDocumentMap);
	}

	public static Map<String, VTD1_Document__c> generateDocuments(Case aCase) {
		Map<String, VTD1_Document__c> result = new Map<String, VTD1_Document__c>();
		VTD1_Document__c documentWithCase = new VTD1_Document__c(
				VTD1_Clinical_Study_Membership__c = aCase.Id,
				RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT
		);

		VTD1_Document__c documentWithoutCase = new VTD1_Document__c(
				VTD1_Site__c = aCase.VTD1_Virtual_Site__c,
				RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT
		);

		VTD1_Document__c documentWithOldStudy = new VTD1_Document__c(
				VTD2_Eligibility_Assesment_For__c = aCase.VTD1_Virtual_Site__c,
				VTD1_Study__c = aCase.VTD1_Virtual_Site__r.VTD1_Study__c,
				RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT
		);

		VTD1_Document__c unlinkedDocument = new VTD1_Document__c(
				RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT
		);

		result.put(DOCUMENT_WITH_CASE, documentWithCase);
		result.put(DOCUMENT_WITHOUT_CASE, documentWithoutCase);
		result.put(DOCUMENT_WITH_OLD_STUDY, documentWithOldStudy);
		result.put(UNLINKED_DOCUMENT, unlinkedDocument);

		insert result.values();
		return result;
	}

	public static void assertResults(Case aCase, Map<String, VTD1_Document__c> documentNameDocumentMap) {
		Map<Id, VTD1_Document__c> documentIdDocumentMap = new Map<Id, VTD1_Document__c>([
				SELECT Id, VTR4_AuditStudy__c, VTR4_AuditSite__c
				FROM VTD1_Document__c
				WHERE Id IN :documentNameDocumentMap.values()
		]);

		VTD1_Document__c documentWithCase = documentNameDocumentMap.get(DOCUMENT_WITH_CASE);
		VTD1_Document__c documentWithoutCase = documentNameDocumentMap.get(DOCUMENT_WITHOUT_CASE);
		VTD1_Document__c documentWithOldStudy = documentNameDocumentMap.get(DOCUMENT_WITH_OLD_STUDY);
		VTD1_Document__c unlinkedDocument = documentNameDocumentMap.get(UNLINKED_DOCUMENT);

		System.assertEquals(aCase.VTD1_Virtual_Site__r.VTD1_Study__c,
				documentIdDocumentMap.get(documentWithCase.Id).VTR4_AuditStudy__c);
		System.assertEquals(aCase.VTD1_Virtual_Site__c,
				documentIdDocumentMap.get(documentWithCase.Id).VTR4_AuditSite__c);

		System.assertEquals(aCase.VTD1_Virtual_Site__r.VTD1_Study__c,
				documentIdDocumentMap.get(documentWithoutCase.Id).VTR4_AuditStudy__c);
		System.assertEquals(aCase.VTD1_Virtual_Site__c,
				documentIdDocumentMap.get(documentWithoutCase.Id).VTR4_AuditSite__c);

		System.assertEquals(aCase.VTD1_Virtual_Site__r.VTD1_Study__c,
				documentIdDocumentMap.get(documentWithOldStudy.Id).VTR4_AuditStudy__c);
		System.assertEquals(aCase.VTD1_Virtual_Site__c,
				documentIdDocumentMap.get(documentWithOldStudy.Id).VTR4_AuditSite__c);

		System.assertEquals(null, documentIdDocumentMap.get(unlinkedDocument.Id).VTR4_AuditStudy__c);
		System.assertEquals(null, documentIdDocumentMap.get(unlinkedDocument.Id).VTR4_AuditSite__c);
	}

	public static Case getCase() {
		return [
				SELECT Id, VTD1_Virtual_Site__c, VTD1_Virtual_Site__r.VTD1_Study__c
				FROM Case
				WHERE RecordType.DeveloperName = 'CarePlan'
				LIMIT 1
		];
	}
}