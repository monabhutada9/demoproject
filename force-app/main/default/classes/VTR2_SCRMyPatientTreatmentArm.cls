/**
 * Created by Oleh Berehovskyi on 05-Apr-19.
 */

public without sharing class VTR2_SCRMyPatientTreatmentArm {
    private static final Set<String> ALLOWED_ACCESS_LEVELS = new Set<String>{
            'Edit',
            'Read'
    };

    @AuraEnabled
    public static List<VTR2_Treatment_Arm__c> getTreatmentArms(Id caseId) {
        VT_R2_TreatmentArmSharingController.getArmAccessByCase(caseId);
        return ALLOWED_ACCESS_LEVELS.contains(VT_R2_TreatmentArmSharingController.getArmAccessByCase(caseId))
            ? [SELECT Id,
                        Name,
                        VTR2_Case__c,
                        VTR2_Start_Date__c,
                        VTR2_End_Date__c,
                        VTR2_Days_in_Treatment__c,
                        VTR2_Is_Current__c,
                        VTR2_Case__r.Contact.Name,
                        VTR2_Case__r.VTD1_Study__r.Name
                FROM VTR2_Treatment_Arm__c
                WHERE VTR2_Case__c = :caseId]
            : new List<VTR2_Treatment_Arm__c>();
    }

}