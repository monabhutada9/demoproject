/*
 * SH-14562: This is to create account, contact and patient after PE subscribe 
 * from VT_R4_PatientConversion.createAccountContactPatient() Method.
 * Created by user on 26.08.2020.
 *  not longer needed - switched to batch for conversion instead of platform events
 */

public with sharing class VT_R5_CreateAccountContactPatientHandler {
    /*public void processEvents(List <VTR4_New_Transaction__e> events) {
        try {
            List<VT_R4_PatientConversion.CPWrapper> cpWrapList = new List<VT_R4_PatientConversion.CPWrapper>();
            for (VTR4_New_Transaction__e event : events) {
                VT_R4_PatientConversion.CPWrapper cp = (VT_R4_PatientConversion.CPWrapper)JSON.deserialize(event.VTR4_Parameters__c,
                        VT_R4_PatientConversion.CPWrapper.class);
                cpWrapList.add(cp);
            }
           // try {
               System.debug('==>'+cpWrapList.size());
               VT_R4_PatientConversion.createAccountContactPatient(cpWrapList, true);
           /* } catch (Exception e) {
                for (VT_R4_PatientConversion.CPWrapper cpw : cpWrapList) {
                    VT_R4_PatientConversionHelper.logError(null, cpw.cp, 'VTR4_StatusSTA__c', e);
                }
                VT_R4_PatientConversion.saveErrors();
            }/
        } catch (Exception e) {
            insert new VTR4_Conversion_Log__c(VTR4_Error_Message__c = e.getMessage()
                    + ' ' + e.getStackTraceString()
                    + ' ' + e.getCause() + e.getLineNumber() + ' ' + e.getTypeName());
        }
    }

    public Object call(String action, Map<String, Object> args) {
        processEvents((List <VTR4_New_Transaction__e>)args.get('events'));
        return null;
    }
     */
}