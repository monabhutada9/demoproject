/**
 * Created by User on 19/05/31.
 */
@isTest
private with sharing class VT_R2_OrderTriggerHandlerTest {
    @testSetup
    private static void setupMethod() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);

        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();

        delete [SELECT Id FROM VTD1_Order__c];


    }

    @isTest
    private static void firstTest() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        Case cas = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];
        VTD1_Protocol_Delivery__c protocolDelivery1 = new VTD1_Protocol_Delivery__c();
        protocolDelivery1.Care_Plan_Template__c = study.Id;
        protocolDelivery1.Name = 'Welcome to Study Package';
        protocolDelivery1.VTD1_Delivery_Order__c = '01';
        protocolDelivery1.VTD1_Shipment_Type__c = 'Initial';
        protocolDelivery1.VTD1_Description__c = 'Welcome to Study Package';
        protocolDelivery1.VTR2_Destination_Type__c = 'Site';
        insert protocolDelivery1;
 
        System.runAs(new User(Id = UserInfo.getUserId())) {
            VTD1_Order__c orderPatientdelivery = new VTD1_Order__c(VTD1_Case__c = cas.Id
                    , VTD1_Status__c = 'Pending Shipment'
                    , VTD1_Expected_Shipment_Date__c = Date.today()
                    , VTD1_Delivery_Order__c = 1
                    , VTD1_Kit_Type__c = 'IMP/Lab'
                    , VTR2_Destination_Type__c = 'Patient'
                    , VTD1_Protocol_Delivery__c = protocolDelivery1.Id
                    , RecordTypeId = VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_ORDER_AD_HOC_DELIVERY
            );
            insert orderPatientdelivery;
            Test.startTest();
            insert new VTD1_Order__c(VTD1_Case__c = cas.Id
                    , VTD1_Status__c = 'Pending Shipment'
                    , VTD1_Expected_Shipment_Date__c = Date.today()
                    , VTD1_Delivery_Order__c = 1
                    , VTD1_Kit_Type__c = 'IMP/Lab'
                    , VTR2_Destination_Type__c = 'Site'
                    , VTD1_Patient_Delivery__c = orderPatientdelivery.Id
                    , VTD1_Protocol_Delivery__c = protocolDelivery1.Id
                    , RecordTypeId = VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_ORDER_AD_HOC_DELIVERY
            );
            Test.stopTest();
        }
    }

}