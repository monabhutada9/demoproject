/**
 * Created by Yuliya Yakushenkova on 9/28/2020.
 */

public class VT_R5_DatabaseQueryService {

    private static final String BACKSLASH = '\'';
    private static final String SEPARATOR = '\',\'';

    private static VT_R5_DatabaseQueryService instance;

    private static VT_R5_DatabaseQueryService getInstance() {
        if (instance != null) return instance;
        return new VT_R5_DatabaseQueryService();
    }

    public List<SObject> buildQuery(List<String> fields, Type objectType, String filter, String orderBy, Integer limitNumber, Integer offsetNumber) {
        String query = 'SELECT ' + String.join(fields, ', ') + ' FROM ' + objectType.getName();
        if(String.isNotBlank(filter)) query += ' WHERE ' + filter;
        if(String.isNotBlank(orderBy)) query += ' ORDER BY ' + orderBy;
        if(limitNumber != null) query += ' LIMIT ' + limitNumber;
        if(offsetNumber != null) query += ' OFFSET ' + offsetNumber;
        return getList(query);
    }

    public List<SObject> getList(String query) {
        System.debug('\n==QUERY PREVIEW==\n' + query + '\n====');
        return Database.query(query);
    }

    public static List<SObject> query(List<String> fields, Type sObjectType) {
        return getInstance().buildQuery(fields, sObjectType, null, null, null, null);
    }

    public static List<SObject> query(List<String> fields, Type objectType, String filter) {
        return getInstance().buildQuery(fields, objectType, filter, null, null, null);
    }

    public static List<SObject> query(List<String> fields, Type objectType, String filter, String orderBy) {
        return getInstance().buildQuery(fields, objectType, filter, orderBy, null, null);
    }

    public static List<SObject> query(List<String> fields, Type objectType, String filter, String orderBy, Integer limitNumber) {
        return getInstance().buildQuery(fields, objectType, filter, orderBy, limitNumber, null);
    }

    public static List<SObject> query(List<String> fields, Type objectType, String filter, String orderBy, Integer limitNumber, Integer offsetNumber) {
        return getInstance().buildQuery(fields, objectType, filter, orderBy, limitNumber, offsetNumber);
    }

    public static Map<Id, SObject> queryMap(List<String> fields, Type sObjectType, String filter) {
        return new Map<Id, SObject>(query(fields, sObjectType, filter));
    }

    public static String equal(String field, String value) {
        return ' ' + field + ' = ' + BACKSLASH + value + BACKSLASH + ' ';
    }

    public static String notEqual(String field, String value) {
        return ' ' + field + ' != ' + BACKSLASH + value + BACKSLASH + ' ' ;
    }

    public static String nullField(String field) {
        return ' ' + field + ' = ' + 'NULL' + ' ';
    }

    public static String notNullField(String field) {
        return ' ' + field + ' != ' + 'NULL' + ' ';
    }

    public static String falseField(String field) {
        return ' ' + field + ' = ' + 'FALSE' + ' ';
    }

    public static String trueField(String field) {
        return ' ' + field + ' = ' + 'TRUE' + ' ';
    }

    public static String greaterThan(String field, String value) {
        return ' ' + field + ' > ' + value + ' ' ;
    }

    public static String greaterThan(String field, Datetime value) {
        return greaterThan(field, convertDatetimeToString(value));
    }

    public static String greaterOrEqThan(String field, String value) {
        return ' ' + field + ' >= ' + value + ' ' ;
    }

    public static String greaterOrEqThan(String field, Datetime value) {
        return greaterOrEqThan(field,  convertDatetimeToString(value));
    }

    public static String lessThan(String field, String value) {
        return ' ' + field + ' < ' + value + ' ' ;
    }

    public static String lessThan(String field, Datetime value) {
        return lessThan(field, convertDatetimeToString(value));
    }

    public static String lessOrEqThan(String field, String value) {
        return ' ' + field + ' <= ' + value + ' ' ;
    }

    public static String lessOrEqThan(String field, Datetime value) {
        return lessOrEqThan(field,  convertDatetimeToString(value));
    }

    public static String lessOrEqThanRelative(String field, String relativeValue) {
        return ' ' + field + ' <= \'' + relativeValue + '\' ' ;
    }

    public static String fieldInValues(String field, List<String> values){
        return ' ' + field + ' IN (' + BACKSLASH + String.join(values, SEPARATOR) + BACKSLASH + ') ' ;
    }

    private static String convertDatetimeToString(Datetime value) {
        String dateFormat = 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'';
        return value.formatGmt(dateFormat);
    }
}