public without sharing class ExternalVideoController {
    private class ExternalVideoException extends Exception {}
    private static VTD1_General_Settings__mdt herokuSettings;

    public class VideoData {
        String url;
        Video_Conference__c vc;

        public VideoData(String url) {
            this.url = url;
        }

        public VideoData(String url, Video_Conference__c vc) {
            this.url = url;
            this.vc = vc;
        }
    }

    /**
     * Method for refresh user session for prevent log out if video conference is on
     */
    @AuraEnabled
    public static void refreshSession(){}

    @AuraEnabled
    public static String getCurrentVideoId() {
        return CustomThemeLayoutController.getCurrentVideoId();
    }

    @AuraEnabled
    public static String getVideoData(Boolean empty) {
        try {
            VideoData data = getData(null, null, UserInfo.getUserId(), empty);
            return data == null
                    ? null
                    : JSON.serialize(data);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public static String getVideoDataForExternal(Id confId, String sessionId, Id participantId) {
        VideoData data = getData(confId, sessionId, participantId, false);
        return data == null
                ? null
                : data.url;
    }

    public static void getHerokuSettings() {
        List<VTD1_General_Settings__mdt> generalSettingsList = (List<VTD1_General_Settings__mdt>) new QueryBuilder(VTD1_General_Settings__mdt.class)
                .addField(VTD1_General_Settings__mdt.VTD1_Video_Heroku_App_Url__c)
                .addField(VTD1_General_Settings__mdt.VTR5_VideoHerokuAesKey__c)
                .namedQueryRegister('ExternalVideoController.metadataQuery')
                .toList();
        herokuSettings = generalSettingsList.isEmpty()
                ? new VTD1_General_Settings__mdt()
                : generalSettingsList[0];
    }

    public static String getHerokuAppURL() {
        if (herokuSettings == null) { getHerokuSettings(); }

        return herokuSettings.VTD1_Video_Heroku_App_Url__c;
    }

    private static String getHerokuAesKey() {
        if (herokuSettings == null) { getHerokuSettings(); }

        return herokuSettings.VTR5_VideoHerokuAesKey__c;
    }

    private static VideoData getData(Id confId, String sessionId, Id participantId, Boolean empty) {
        String herokuAppURL = getHerokuAppURL();
        return herokuAppURL == null
                ? null
                : empty
                        ? new VideoData(herokuAppURL + '/video/empty')
                        : getData(confId, sessionId, participantId, herokuAppURL);
    }

    private static VideoData getData(Id confId, String sessionId, Id participantId, String url) {
        Video_Conference__c vc;
        if (confId == null) {
            vc = getConferenceData(getCurrentVideoId());
            if (vc == null) { return null; }
        } else {
            vc = new Video_Conference__c(Id = confId, SessionId__c = sessionId);
        }
        validateRequest(vc);
        String urlParamsTemplate = '/video/conference?sid={0}&payload={1}';
        List<String> urlParams = new List<String>();
        urlParams.add((String) vc.SessionId__c);
        urlParams.add(encryptPayload(getPayloadMap(vc, participantId)));
        return new VideoData(url + String.format(urlParamsTemplate, urlParams), vc);
    }

    public static Video_Conference__c getConferenceData(Id confId) {
        List<Video_Conference__c> videoConferences = (List<Video_Conference__c>) new QueryBuilder(Video_Conference__c.class)
                .addField(Video_Conference__c.SessionId__c)
                .addField(Video_Conference__c.Video_available__c)
                .addConditions()
                .add(new QueryBuilder.CompareCondition(Video_Conference__c.Id).eq(confId == null ? '' : confId))
                .endConditions()
                .namedQueryRegister('ExternalVideoController.fetchData')
                .toList();
        if (!videoConferences.isEmpty()) {
            return videoConferences[0];
        } else {
            return null;
        }
    }

    private static void validateRequest(Video_Conference__c vc) {
        if (vc.SessionId__c == null) {
            throw new ExternalVideoException('Video conf in not available');
        } else if (getHerokuAesKey() == null) {
            throw new ExternalVideoException('No encryption key was found');
        }
    }

    private static Map<String, String> getPayloadMap(Video_Conference__c vc, Id participantId) {
        return new Map<String, String>{
                'pid' => participantId,
                'vcid' => vc.Id,
                'orgId' => 'tokbox' + UserInfo.getOrganizationId().toLowerCase()
        };
    }

    private static String encryptPayload(Map<String, String> payloadMap) {
        String algorithm = 'AES256';
        String encoding = 'UTF-8';
        Blob secretKey = EncodingUtil.base64Decode(getHerokuAesKey());
        Blob clearData = Blob.valueOf(JSON.serialize(payloadMap));
        String payload = JSON.serialize(Crypto.encryptWithManagedIV(algorithm, secretKey, clearData)).replaceAll('"', '');
        return EncodingUtil.urlEncode(payload, encoding);
    }

}