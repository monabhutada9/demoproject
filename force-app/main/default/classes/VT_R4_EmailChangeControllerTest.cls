/**
 * Created by IhorSemenko on 27.01.2020.
 */

@IsTest
public without sharing class VT_R4_EmailChangeControllerTest {
	private static final String PHONE_NUMBER = '+380973332211';
	
	public static void testBehavior() {
		HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
		Case patientCase = [SELECT Id, ContactId FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
		Contact patient = [SELECT Id FROM Contact WHERE id =:patientCase.ContactId LIMIT 1];

		VTD2_Study_Geography__c studyGeography = new VTD2_Study_Geography__c(VTD2_Study__c = study.Id, Name = 'Geo');
		insert studyGeography;

		VTR2_StudyPhoneNumber__c phoneNumber = new VTR2_StudyPhoneNumber__c(VTR2_Study__c = study.Id, VTR2_Study_Geography__c = studyGeography.Id, Name = PHONE_NUMBER);
		insert phoneNumber;

		patientCase.VTR2_StudyPhoneNumber__c = phoneNumber.Id;
		update patientCase;

		patient.VTD1_Clinical_Study_Membership__c = patientCase.Id;
		update patient;

		Test.startTest();
		VT_R4_EmailChangeController obj = new VT_R4_EmailChangeController();
		obj.ContactId = patient.Id;
		System.assertEquals(obj.getStudyPhone(), PHONE_NUMBER);
		Test.stopTest();
	}
}