@IsTest
private class VT_Apollo_FileUploadControllerTest {
	@IsTest
	static void setProfilePictureTest() {
		Contact linkedEntity = VT_D1_TestUtils.createContact(null, 'Gena', 'Bobkov', 'Gena', 'en_US', null);
		insert linkedEntity;

		Id fileId = VT_Apollo_FileUploadController.uploadFile('test_file', 'dGVzdF9kYXRh', linkedEntity.Id);
		System.assert(fileId != null, 'The file must be inserted');

		String originalPhotoURL = VT_Apollo_FileUploadController.setProfilePicture(fileId);
		System.assert(!String.isEmpty(originalPhotoURL), 'The method must return a photo url');
	}

	@IsTest
	static void getUserTest() {
		User user = VT_Apollo_FileUploadController.getUser();
		System.assert(user.Id != null, 'User record must be extracted');
	}
}