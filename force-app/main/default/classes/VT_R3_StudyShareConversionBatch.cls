/**
* @author: Carl Judge
* @date: 08-Oct-19
* @description: For a one-time run after deployment only. Adds a study sharing configuration record for any user who
*               does not have one but has a share for a study
**/

public without sharing class VT_R3_StudyShareConversionBatch implements Database.Batchable<SObject> {
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT UserOrGroupId, ParentId, AccessLevel
            FROM HealthCloudGA__CarePlanTemplate__Share
        ]);
    }

    public void execute(Database.BatchableContext BC, List<HealthCloudGA__CarePlanTemplate__Share> scope) {
        List<Id> userIds = new List<Id>();
        List<Id> studyIds = new List<Id>();
        for (HealthCloudGA__CarePlanTemplate__Share share : scope) {
            userIds.add(share.UserOrGroupId);
            studyIds.add(share.ParentId);
        }

        Set<String> compositeKeys = new Set<String>();
        for (VTD1_Study_Sharing_Configuration__c config : [
            SELECT VTD1_User__c, VTD1_Study__c
            FROM VTD1_Study_Sharing_Configuration__c
            WHERE VTD1_User__c IN :userIds
            AND VTD1_Study__c IN :studyIds
        ]) {
            compositeKeys.add('' + config.VTD1_User__c + config.VTD1_Study__c);
        }

        List<VTD1_Study_Sharing_Configuration__c> newConfigs = new List<VTD1_Study_Sharing_Configuration__c>();
        for (HealthCloudGA__CarePlanTemplate__Share share : scope) {
            if (!compositeKeys.contains('' + share.UserOrGroupId + share.ParentId)) {
                newConfigs.add(new VTD1_Study_Sharing_Configuration__c(
                    VTD1_User__c = share.UserOrGroupId,
                    VTD1_Study__c = share.ParentId,
                    VTD1_Access_Level__c = share.AccessLevel == 'All' ? 'Edit' : share.AccessLevel
                ));
            }
        }
        if (!newConfigs.isEmpty()) { Database.insert(newConfigs, false); }
    }

    public void finish(Database.BatchableContext BC) {
    }
}