/**
* @author: Tatiana Terekhova
* @date: 27-May-20
* @description: 
**/
public class VT_R5_RequestBuilder_CDocument_DocuSign implements VT_D1_RequestBuilder {
    public class Envelope {
        public String emailSubject;
        public String emailBlurb;
        public String status;
        public DSDocument[] documents;
        public Recipients recipients;
        public AccountCustomFields customFields;
        // Add a SFDC record ID to the envelope - the document will be attached to this record after signing
        public void addRecId(Id recId) {
            TextCustomField customField = new TextCustomField();
            customField.name = 'DSFSSourceObjectId';
            customField.value = recId + '~' + recId.getSobjectType().getDescribe().getName();
            customField.show = 'false';
            AccountCustomFields customFields = new AccountCustomFields();
            customFields.textCustomFields = new List<TextCustomField> {
                customField
            };
            this.customFields = customFields;
        }
    }
    public class DSDocument {
        public String documentId;
        public String name;
        public String documentBase64;
        public String fileExtension;
    }
    public class Recipients {
        public Signer[] signers;
    }
    public class Signer {
        public String name;
        public String email;
        public String recipientId;
        public String routingOrder;
        public Tabs tabs;
    }
    public class Tabs {
    }
    public class Tab {
        public String xPosition;
        public String yPosition;
        public String documentId;
        public String pageNumber;
    }
    public class AccountCustomFields {
        public TextCustomField[] textCustomFields;
    }
    public class TextCustomField {
        public String name;
        public String value;
        public String show;
    }
    private Id docId;
    private Id dsUserId;
    public String dsSubject;
    public String dsMessage;
    public String mvrId;
    public String docName;
    public VT_R5_RequestBuilder_CDocument_DocuSign(Id mvrId,Id docId, Id dsUserId, String dsSubject,String dsMessage) {
        this.docId = docId;
        this.dsUserId=dsUserId;
        this.dsSubject=dsSubject;
        this.dsMessage=dsMessage;
        this.mvrId=mvrId;
    }
    public String buildRequestBody() {
        ContentVersion content = getContentVersion(this.docId);
        this.docName=content.ContentDocument.Title;
        User recipient = [Select Id, Name,Email from User where Id=:dsUserId limit 1];
        Envelope env = getEnvelope(content, recipient,this.mvrId, this.docId,this.docName,this.dsSubject,this.dsMessage);
        return JSON.serialize(env, true);
    }
    private static ContentVersion getContentVersion(Id docId) {
        system.debug('!docId'+docId);
        return [
            SELECT Id, VersionData, PathOnClient, FileExtension,ContentDocument.Title
            FROM ContentVersion
            WHERE ContentDocumentId = :docId
            ORDER BY VersionNumber desc
        ][0];
    }
    private static Envelope getEnvelope(ContentVersion content, User recipient,Id mvrId, Id docId,String docName,String dsSubject,String dsMessage) {
        DSDocument dsDoc = new DSDocument();
        dsDoc.documentId = '1';
       // dsDoc.documentId = docId;
        dsDoc.name = docName;
        dsDoc.documentBase64 = EncodingUtil.base64Encode(content.VersionData);
        dsDoc.fileExtension = content.FileExtension;
        Signer signer = new Signer();
        signer.name = recipient.Name;
        signer.email = recipient.Email;      
        signer.recipientId = '1';        
        signer.routingOrder = '1';
        signer.tabs = new Tabs();
        Envelope env = new Envelope();
      //  env.addRecId(docId);
     //   env.addMVRRecId('a1g54000000STCOAA4');
        env.addRecId(mvrId);
        env.emailSubject = dsSubject;
        env.emailBlurb = dsMessage;
        //env.dsfs_DocuSign_Envelope__r.VTD1_Monitoring_Visit__c='a1g54000000STCOAA4';
        env.status = 'sent';
        env.documents = new DSDocument[]{dsDoc};
        env.recipients = new Recipients();
        env.recipients.signers = new Signer[]{signer};
        system.debug('ENV '+env);
        return env;
    }
}