/**
 * Created by Yulia Yakushenkova on 21.04.2020.
 */

@IsTest
private class VT_R4_RestMobileLoginTest {

    @TestSetup
    static void setup() {
        DomainObjects.Account_t firstPatientAccountT = new DomainObjects.Account_t()
                .setRecordTypeByName('Patient');
        firstPatientAccountT.persist();

        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'))
                .setOriginalName('Study Name');


        DomainObjects.Contact_t firstPatientContact = new DomainObjects.Contact_t()
                .setRecordTypeName('Patient')
                .setAccountId(firstPatientAccountT.id)
                .setLastName('ln')
                .setFirstName('fn')
                .setBirthdate(System.now().addYears(-30).date())
                .setVTR2_Primary_Language('en_US')
                .setVT_R5_Tertiary_Preferred_Language('de')
                .setVT_R5_Secondary_Preferred_Language('it');

        new DomainObjects.User_t()
                .addContact(firstPatientContact)
                .setProfile('Patient')
                .setUsername('VT_R4_RestMobileLoginTest@patient.com')
                .setVTR3_UnreadFeeds(fflib_IDGenerator.generate(FeedItem.SObjectType))
                .persist();

    }

    enum MockRequestType {
        SUCCESS_LOGIN, FORGOT_PASSWORD_INVALID_CROSS_REFERENCE, LOGIN_FAILED,
        SUCCESS_ACTIVATION, SUCCESS_ACTIVATION_FINAL, LOGIN_PASSWORD_EXPIRED,
        CHANGE_PASSWORD_INVALID_SESSION, CHANGE_PASSWORD_SUCCESS
    }

    class LoginRequestCalloutMock implements HttpCalloutMock {

        private MockRequestType mockRequestType;

        public LoginRequestCalloutMock(MockRequestType mockRequestType) {
            this.mockRequestType = mockRequestType;
        }
        public LoginRequestCalloutMock() {
        }

        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            Boolean statusSet = false;
            switch on this.mockRequestType {
                when SUCCESS_LOGIN {
                    res.setBody('<sessionId>SESSION_ID</sessionId> <passwordExpired>false</passwordExpired>');
                }
                when SUCCESS_ACTIVATION_FINAL {
                    res.setBody('{"newPassword":"123","username":"123"}');
                }
                when SUCCESS_ACTIVATION {
                    res.setBody('{"stage":"123"}');
                }
                when LOGIN_PASSWORD_EXPIRED {
                    res.setBody('<sessionId>SESSION_ID</sessionId> <passwordExpired>true</passwordExpired>');
                }
                when FORGOT_PASSWORD_INVALID_CROSS_REFERENCE {
                    res.setBody('<sf:exceptionCode>INVALID_CROSS_REFERENCE_KEY</sf:exceptionCode> <sf:exceptionMessage>message</sf:exceptionMessage>');
                    res.setStatusCode(500);
                    statusSet = true;
                }
                when CHANGE_PASSWORD_INVALID_SESSION {
                    res.setBody('<sf:exceptionCode>INVALID_SESSION_ID</sf:exceptionCode> <sf:exceptionMessage>message</sf:exceptionMessage>');
                    res.setStatusCode(500);
                    statusSet = true;
                }
                when CHANGE_PASSWORD_SUCCESS {
                    res.setBody('ok');
                }
                when LOGIN_FAILED {
                    res.setBody('<passwordExpired>true</passwordExpired>');
                }

            }
            res.setStatus('OK');
            if (!statusSet) res.setStatusCode(200);
            return res;
        }
    }

    private static void setRestContext(String URI, String method) {
        VT_R4_RestMobileLogin.request = new RestRequest();
        VT_R4_RestMobileLogin.response = new RestResponse();
        VT_R4_RestMobileLogin.request.requestURI = URI;
        VT_R4_RestMobileLogin.request.httpMethod = method;
    }
    private static void setRestContext(String URI, String method, String body) {
        VT_R4_RestMobileLogin.request = new RestRequest();
        VT_R4_RestMobileLogin.response = new RestResponse();
        VT_R4_RestMobileLogin.request.requestURI = URI;
        VT_R4_RestMobileLogin.request.httpMethod = method;
        VT_R4_RestMobileLogin.request.requestBody = Blob.valueOf(body);
    }
    private static void setRestContext(String URI, String method, String body, Map<String, String> params) {
        VT_R4_RestMobileLogin.request = new RestRequest();
        VT_R4_RestMobileLogin.response = new RestResponse();
        VT_R4_RestMobileLogin.request.requestURI = URI;
        VT_R4_RestMobileLogin.request.httpMethod = method;
        VT_R4_RestMobileLogin.request.requestBody = Blob.valueOf(body);
        for (String s : params.keySet()) {
            VT_R4_RestMobileLogin.request.params.put(s, params.get(s));
        }
    }
    private static void setRestContext(String URI, String method, Map<String, String> params) {
        VT_R4_RestMobileLogin.request = new RestRequest();
        VT_R4_RestMobileLogin.response = new RestResponse();
        VT_R4_RestMobileLogin.request.requestURI = URI;
        VT_R4_RestMobileLogin.request.httpMethod = method;
        for (String s : params.keySet()) {
            VT_R4_RestMobileLogin.request.params.put(s, params.get(s));
        }
    }
    private static void setRestContextNew(String URI, String method, String body,Map<String, String> params) {
        RestContext.request = new RestRequest();
        RestContext.response = new RestResponse();
        RestContext.request.requestURI = URI;
        RestContext.request.httpMethod = method;
        if (body != null) RestContext.request.requestBody = Blob.valueOf(body);
        if (params!=null) {
            for (String s : params.keySet()) {
                RestContext.request.params.put(s, params.get(s));
            }
        }
    }

    @IsTest
    static void test1() {
        Test.startTest();

        setRestContext('/Patient/Auth/Login', VT_RemoteCall.METHOD_GET, new Map<String, String>{
                'lang' => 'de'
        });
        VT_R4_RestMobileLogin.getMobileDictionaryPreLogin();

        setRestContext('/Patient/Auth/Version', VT_RemoteCall.METHOD_GET, new Map<String, String>{
                'lang' => 'de'
        });
        VT_R4_RestMobileLogin.getMobileDictionaryPreLogin();

        Map<String, String> body = new Map<String, String>{
                'not_valid' => 'filed'
        };
        setRestContext('/Patient/Auth/Login', VT_RemoteCall.METHOD_POST, JSON.serialize(body));
        VT_R4_RestMobileLogin.authAction();


        User patientUser = [SELECT Id, Username,VTR3_Verified_Mobile_Devices__c,VTD1_Privacy_Notice_Last_Accepted__c,VTD1_Filled_Out_Patient_Profile__c,HIPAA_Accepted__c FROM User WHERE Username = 'VT_R4_RestMobileLoginTest@patient.com' AND IsActive = TRUE LIMIT 1];
        body = new Map<String, String>{
                'username' => patientUser.Username, 'password' => 'password', 'deviceUniqueCode' => '123'
        };
        setRestContext('/Patient/Auth/Login', VT_RemoteCall.METHOD_POST, JSON.serialize(body));

        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.LOGIN_FAILED));
        VT_R4_RestMobileLogin.authAction();

        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.SUCCESS_LOGIN));
        VT_R4_RestMobileLogin.authAction();

        VT_R4_RestMobileLogin.checkFinishedLoginFlow(patientUser, '123');

        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.LOGIN_PASSWORD_EXPIRED));
        VT_R4_RestMobileLogin.authAction();


        body = new Map<String, String>{
                'password' => 'password', 'deviceUniqueCode' => '123'
        };
        setRestContext('/Patient/Auth/ForgotPassword', VT_RemoteCall.METHOD_POST, JSON.serialize(body));
        VT_R4_RestMobileLogin.authAction();

        body = new Map<String, String>{
                'username' => patientUser.Username
        };
        setRestContext('/Patient/Auth/ForgotPassword', VT_RemoteCall.METHOD_POST, JSON.serialize(body));
        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.FORGOT_PASSWORD_INVALID_CROSS_REFERENCE));
        VT_R4_RestMobileLogin.authAction();

        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock());
        VT_R4_RestMobileLogin.authAction();

        setRestContext('/Patient/Auth/ForgotPassword', VT_RemoteCall.METHOD_POST, JSON.serialize(body));
        body = new Map<String, String>{
                'username' => 'INVALID_patientUser.Username'
        };
        VT_R4_RestMobileLogin.authAction();


        body = new Map<String, String>{
                'userId' => UserInfo.getUserId(), 'stage' => 'SetupPassword', 'deviceUniqueCode' => '123'
        };
        setRestContext('/Patient/Auth/', VT_RemoteCall.METHOD_PUT, JSON.serialize(body), new Map<String, String>{
                'activation' => 'true'
        });
        VT_R4_RestMobileLogin.activation();

        body = new Map<String, String>{
                'stage' => 'Activation', 'deviceUniqueCode' => '123'
        };
        setRestContext('/Patient/Auth/', VT_RemoteCall.METHOD_PUT, JSON.serialize(body), new Map<String, String>{
                'activation' => 'true'
        });
        VT_R4_RestMobileLogin.activation();

        body = new Map<String, String>{
                'userId' => UserInfo.getUserId(), 'stage' => 'Activation', 'deviceUniqueCode' => '123'
        };
        setRestContext('/Patient/Auth/', VT_RemoteCall.METHOD_PUT, JSON.serialize(body), new Map<String, String>{
                'activation' => 'true'
        });

        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.SUCCESS_ACTIVATION));
        VT_R4_RestMobileLogin.activation();

        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.SUCCESS_ACTIVATION_FINAL));
        VT_R4_RestMobileLogin.activation();

        body = new Map<String, String>{
                'userId' => 'UserInfo.getUserId()' //invalid body
        };
        setRestContext('/Patient/Auth/ChangePassword', VT_RemoteCall.METHOD_POST, JSON.serialize(body));
        VT_R4_RestMobileLogin.authAction();

        body = new Map<String, String>{
                'newPassword' => 'newpass', 'oldPassword' => 'oldPassword'
        };
        setRestContext('/Patient/Auth/ChangePassword', VT_RemoteCall.METHOD_POST, JSON.serialize(body));
        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.CHANGE_PASSWORD_INVALID_SESSION));
        VT_R4_RestMobileLogin.authAction();

        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.CHANGE_PASSWORD_SUCCESS));
        VT_R4_RestMobileLogin.authAction();


        body = new Map<String, String>{
                'username' => patientUser.Username, 'oldPassword' => 'password', 'deviceUniqueCode' => '123'
        };
        setRestContext('/Patient/Auth/SetPassword', VT_RemoteCall.METHOD_POST, JSON.serialize(body));
        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.LOGIN_PASSWORD_EXPIRED));
        VT_R4_RestMobileLogin.authAction();

        setRestContext('/Patient/Auth/Invalid', VT_RemoteCall.METHOD_POST, JSON.serialize(body));
        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.SUCCESS_LOGIN));
        VT_R4_RestMobileLogin.authAction();
        Test.stopTest();
    }


    @IsTest
    static void test2() {
        Test.startTest();
        setRestContextNew('/mobile/v1/login-dictionary', VT_RemoteCall.METHOD_GET,null, new Map<String, String>{
                'lang' => 'de'
        });
        VT_R5_MobileRouter.doGET();

        setRestContextNew('/mobile/v1/auth/loginflow', VT_RemoteCall.METHOD_GET, null, new Map<String, String>{
                'lang' => 'de'
        });
        VT_R5_MobileRouter.doGET();

        setRestContextNew('/mobile/v1/auth/invalid', VT_RemoteCall.METHOD_GET,null, new Map<String, String>{
                'lang' => 'de'
        });
        VT_R5_MobileRouter.doGET();

        setRestContextNew('/mobile/v1/auth/', VT_RemoteCall.METHOD_POST, null, null);
        VT_R5_MobileRouter.doPOST();

        Map<String, String> body = new Map<String, String>{
                'not_valid' => 'filed'
        };
        setRestContextNew('/mobile/v1/auth/login', VT_RemoteCall.METHOD_POST,JSON.serialize(body), null);
        VT_R5_MobileRouter.doPOST();

        User patientUser = [SELECT Id, Username,VTR3_Verified_Mobile_Devices__c,VTD1_Privacy_Notice_Last_Accepted__c,VTD1_Filled_Out_Patient_Profile__c,HIPAA_Accepted__c,Profile.Name FROM User WHERE Username = 'VT_R4_RestMobileLoginTest@patient.com' AND IsActive = TRUE LIMIT 1];
        patientUser.VTR3_Verified_Mobile_Devices__c = 'AUTOTEST;abcd;';
        update patientUser;
        body = new Map<String, String>{
                'username' => patientUser.Username, 'password' => 'password', 'deviceUniqueCode' => '123'
        };
        setRestContextNew('/mobile/v1/auth/login', VT_RemoteCall.METHOD_POST, JSON.serialize(body), null);
        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.LOGIN_FAILED));
        VT_R5_MobileRouter.doPOST();

        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.SUCCESS_LOGIN));
        VT_R5_MobileRouter.doPOST();

        VT_R5_MobileAuth.checkFinishedLoginFlow(patientUser, '123');

        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.LOGIN_PASSWORD_EXPIRED));
        VT_R5_MobileRouter.doPOST();

        setRestContextNew('/mobile/v1/auth/Invalid', VT_RemoteCall.METHOD_POST, JSON.serialize(body), null);
        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.SUCCESS_LOGIN));
        VT_R5_MobileRouter.doPOST();
        Test.stopTest();
    }

    @IsTest
    static void thirdTest() {
        Test.startTest();
        Map<String, String> body = new Map<String, String>{
                'newPassword' => 'newpass', 'oldPassword' => 'oldPassword'
        };
        System.debug('start1');
        setRestContextNew('/mobile/v1/auth/change-password', VT_RemoteCall.METHOD_POST, JSON.serialize(body), null);
        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.CHANGE_PASSWORD_SUCCESS));
        VT_R5_MobileRouter.doPOST();
        System.debug('resp: ' + RestContext.response.responseBody.toString());

        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.CHANGE_PASSWORD_INVALID_SESSION));
        VT_R5_MobileRouter.doPOST();
        Test.stopTest();
    }

    @IsTest
    static void fourthTest() {
        VT_R5_MobileAuth.parsedParams = new VT_R5_MobileAuth.RequestParams();
        RestContext.response = new RestResponse();
        System.assert(new VT_R5_MobileAuth().isParamsMissed());

        VT_R5_MobileAuth.parsedParams.stage = VT_R5_MobileLoginFlow.VERIFICATION_CODE_STAGE;
        VT_R5_MobileAuth.parsedParams.deviceUniqueCode = 'somecode';
        RestContext.response = new RestResponse();
        System.assert(new VT_R5_MobileAuth().isParamsMissed());

        VT_R5_MobileAuth.parsedParams.stage = VT_R5_MobileLoginFlow.PRIVACY_NOTICES_STAGE;
        RestContext.response = new RestResponse();
        System.assert(new VT_R5_MobileAuth().isParamsMissed());

        VT_R5_MobileAuth.parsedParams.stage = VT_R5_MobileLoginFlow.INITIAL_PROFILE_SETUP_STAGE;
        RestContext.response = new RestResponse();
        System.assert(new VT_R5_MobileAuth().isParamsMissed());

        VT_R5_MobileAuth.parsedParams.stage = VT_R5_MobileLoginFlow.AUTHORIZATION_STAGE;
        RestContext.response = new RestResponse();
        System.assert(new VT_R5_MobileAuth().isParamsMissed());

        VT_R5_MobileAuth.parsedParams.stage = VT_R5_MobileLoginFlow.AUTHORIZATION_STAGE;
        VT_R5_MobileAuth.parsedParams.isAuthorizationAccepted = true;
        RestContext.response = new RestResponse();
        System.assert(!new VT_R5_MobileAuth().isParamsMissed());
    }

    @IsTest
    static void fifthTest() {
        Test.startTest();
        Map<String, String> body = new Map<String, String>{
                'userId' => UserInfo.getUserId(), 'stage' => 'SetupPassword', 'deviceUniqueCode' => '123'
        };
        setRestContextNew('/mobile/v1/auth/activate', VT_RemoteCall.METHOD_POST, JSON.serialize(body), new Map<String, String>{
                'activation' => 'true'
        });
        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.SUCCESS_ACTIVATION_FINAL));
        VT_R5_MobileRouter.doPOST();

        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.SUCCESS_ACTIVATION));
        VT_R5_MobileRouter.doPOST();
        Test.stopTest();
    }

    @IsTest
    static void sixthTest() {
        Test.startTest();
        User patientUser = [SELECT Id, Username FROM User WHERE Username = 'VT_R4_RestMobileLoginTest@patient.com'  AND IsActive = TRUE LIMIT 1];
        Map<String, String> body = new Map<String, String>{
                'username' => patientUser.Username, 'oldPassword' => 'password', 'deviceUniqueCode' => '123'
        };
        setRestContextNew('/mobile/v1/auth/set-password', VT_RemoteCall.METHOD_POST, JSON.serialize(body), null);
        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.LOGIN_PASSWORD_EXPIRED));
        VT_R5_MobileRouter.doPOST();

        setRestContextNew('/mobile/v1/auth/set-password', VT_RemoteCall.METHOD_POST, JSON.serialize(body), null);
        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.SUCCESS_LOGIN));
        VT_R5_MobileRouter.doPOST();
        Test.stopTest();
    }
    @IsTest
    static void seventhTest() {
        Test.startTest();
        User patientUser = [SELECT Id, Username FROM User WHERE Username = 'VT_R4_RestMobileLoginTest@patient.com' AND IsActive = TRUE LIMIT 1];
        Map<String, String> body = new Map<String, String>{
                'password' => 'password', 'deviceUniqueCode' => '123'
        };
        setRestContextNew('/mobile/v1/auth/forgot-password', VT_RemoteCall.METHOD_POST, JSON.serialize(body), null);
        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.SUCCESS_LOGIN));
        VT_R5_MobileRouter.doPOST();

        body = new Map<String, String>{
                'username' => patientUser.Username
        };
        setRestContextNew('/mobile/v1/auth/forgot-password', VT_RemoteCall.METHOD_POST, JSON.serialize(body), null);
        Test.setMock(HttpCalloutMock.class, new LoginRequestCalloutMock(MockRequestType.FORGOT_PASSWORD_INVALID_CROSS_REFERENCE));
        VT_R5_MobileRouter.doPOST();
        Test.stopTest();
    }
    @IsTest
    static void eighthTest() {
        Test.startTest();
        setRestContextNew('/mobile/v1/support-centers-info', VT_RemoteCall.METHOD_GET, null, null);
        VT_R5_MobileRouter.doGET();
        Test.stopTest();
    }
}