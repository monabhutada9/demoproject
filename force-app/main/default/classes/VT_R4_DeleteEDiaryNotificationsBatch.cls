/**
 * Created by Yulia Yakushenkova on 22.06.2020.
 */

public class VT_R4_DeleteEDiaryNotificationsBatch implements Database.Batchable<SObject> {

    private List<Id> receiversIds;
    private String surveyId;

    public VT_R4_DeleteEDiaryNotificationsBatch(String surveyId) {
        this.surveyId = surveyId;
        VTD1_Survey__c survey = [
                SELECT VTD1_Caregiver_User_Id__c, VTD1_Patient_User_Id__c
                FROM VTD1_Survey__c
                WHERE Id = :surveyId
        ];
        this.receiversIds = new List<Id>{survey.VTD1_Patient_User_Id__c};
        if (String.isNotBlank(survey.VTD1_Caregiver_User_Id__c)) {
            this.receiversIds.add(survey.VTD1_Caregiver_User_Id__c);
        }
    }

    public Iterable<SObject> start(Database.BatchableContext bc) {
        List<VTD1_NotificationC__c> notifications = [
                SELECT VTD1_Parameters__c, Owner.Profile.Name,VTR5_HideNotification__c
                FROM VTD1_NotificationC__c
                WHERE VTD1_Receivers__c IN :receiversIds
        ];

        List<VTD1_NotificationC__c> notificationsToHide = new List<VTD1_NotificationC__c>();
        for (VTD1_NotificationC__c n : notifications) {
            if (n.VTD1_Parameters__c != null && n.VTD1_Parameters__c.contains(surveyId)) {
                n.VTR5_HideNotification__c = true;
                notificationsToHide.add(n);
            }
        }
        return notificationsToHide;
    }

    public void execute(Database.BatchableContext bc, List<VTD1_NotificationC__c> notificationsToHide) {
        update notificationsToHide;
    }

    public void finish(Database.BatchableContext param1) {}
}