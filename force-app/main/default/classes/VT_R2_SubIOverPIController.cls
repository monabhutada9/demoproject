/**
 * Created by Alexander Komarov on 20.02.2019.
 */

public with sharing class VT_R2_SubIOverPIController {
    private Map<String, Id> binderMap;
    private Virtual_Site__c site;
    private Id studyId;


    public VT_R2_SubIOverPIController(Id virtualSiteId) {
        this.site = [SELECT Id,VTD1_Study__c,VTR2_PI_Change_Complete__c FROM Virtual_Site__c Where id = :virtualSiteId];
        this.studyId = this.site.VTD1_Study__c;
    }

    @AuraEnabled
    public static void execute(Id virtualSiteId) {
        VT_R2_SubIOverPIController controller = new VT_R2_SubIOverPIController(virtualSiteId);
        controller.setPIChangeCompleteToFalse();
        controller.getBinderIdsForRegDocs();
        controller.createDocumentPlaceholders();

    }

    private void setPIChangeCompleteToFalse() {
        if (this.site.VTR2_PI_Change_Complete__c) {
            this.site.VTR2_PI_Change_Complete__c = false;
            update site;
        }
    }

    private void getBinderIdsForRegDocs() {

        List<String> regDocTypes = new List<String>{
                VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_DOA_LOG,
                VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_NOTE_TO_FILE_STEP_DOWN,
                VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_1572,
                VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_ACKNOWLEDGEMENT_LETTER
        };

        this.binderMap = VT_D1_DocumentProcessHelper.getRegBinderIdsForStudy(this.studyId, regDocTypes);
    }

    private void createDocumentPlaceholders() {
        List<VTD1_Document__c> placeHolders = new List<VTD1_Document__c>();

        if (this.binderMap.size() >= 4) {
            placeHolders.add(getNewPlaceHolder(
                    VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_DOA_LOG, null));
            placeHolders.add(getNewPlaceHolder(
                    VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_NOTE_TO_FILE_STEP_DOWN, null));
            placeHolders.add(getNewPlaceHolder(
                    VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_1572, null));
        } else {
            system.debug('Some of needed Reg Docs/Binders could be missed for study ID ' + this.studyId);
            throw new AuraHandledException('Please make sure your study have this RegDocs with RegBinders: ' +
                    VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_DOA_LOG + ', ' +
                    VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_NOTE_TO_FILE_STEP_DOWN + ', ' +
                    VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_1572 + ', ' +
                    VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_ACKNOWLEDGEMENT_LETTER);

        }

        if (!placeHolders.isEmpty()) {
            insert placeHolders;
            Id id1572 = placeHolders.get(2).Id;
            VTD1_Document__c acknowledgementLetterDocument = getNewPlaceHolder(VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_ACKNOWLEDGEMENT_LETTER, id1572);
            insert acknowledgementLetterDocument;
        }

    }

    private VTD1_Document__c getNewPlaceHolder(String docType, Id id1572) {
        return new VTD1_Document__c(
                VTD1_Study__c = this.studyId,
                VTD1_Regulatory_Binder__c = this.binderMap.get(docType),
                VTD1_Site__c = this.site.Id,
                VTD1_Document_Type__c = docType,
                VTD1_Status__c = 'Pending Approval',
                RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT,
                VTR2_1572__c = id1572
        );
    }
}