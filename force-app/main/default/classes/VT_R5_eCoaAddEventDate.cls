/**
* @author: Carl Judge
* @date: 12-May-20
**/

public class VT_R5_eCoaAddEventDate extends VT_R5_eCoaAbstractAction {
    private Id userId;
    private String eventName;
    private Boolean incer = false;
    /* Added for SH-16444 */
    public String phoenixTime;
    public String caseId;
    public Boolean ECOA_CONSENT_TRIGGERED = false;
    /* Added for SH-16444 End*/

    public VT_R5_eCoaAddEventDate(Id userId, String eventName) {
        this.userId = userId;
        this.eventName = eventName;
    }
    public VT_R5_eCoaAddEventDate(Id userId, String eventName, Boolean incer) {
        this.userId = userId;
        this.eventName = eventName;
        this.incer = incer;
    }

    public override VT_D1_HTTPConnectionHandler.Result doCallout() {
        String endpoint = VT_R4_ConstantsHelper_Integrations.ENDPOINT_URL_ECOA_SUBJECT_ADD_EVENT;
        String method = 'POST';
        String action = 'eCOA Add Event Date';

        User u = [
            SELECT Id, TimeZoneSidKey, VTR5_eCOA_Guid__c,Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c,
                Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eCOA_Guid__c,Contact.VTD1_Clinical_Study_Membership__c
            FROM User
            WHERE Id = :userId
        ];
        String studyGuid = u.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eCOA_Guid__c;
        caseId = u.Contact.VTD1_Clinical_Study_Membership__c;

        VT_R5_RequestBuilder_AddEventDate body = new VT_R5_RequestBuilder_AddEventDate(u, eventName, incer);
        /* Added for SH-16444 - Set Phoenix Date*/
        if(String.isNotBlank(phoenixTime)){
            body.unixTime = phoenixTime;
        }
        /* Added for SH-16444 End*/

        return new VT_R5_eCoaApiRequest()
            .setStudyGuid(studyGuid)
            .setSubjectGuid(u.VTR5_eCOA_Guid__c)
            .setEndpoint(endpoint)
            .setMethod(method)
            .setAction(action)
            .setBody(body)
            .setSkipLogging(true) // skip logging directly from the callout. Can still save the log later if skipLogging class variable is false
            .send();
    }

    public override Type getType() {
        return VT_R5_eCoaAddEventDate.class;
    }

    protected override void handleSuccess(VT_D1_HTTPConnectionHandler.Result result) {
        // fire a request to get the subject's schedule.
        System.debug('Reconsent');
        if(ECOA_CONSENT_TRIGGERED){
            Case phoenixCase = new Case(id = caseId,VTR5_eCOA_Consent_Diary_triggered__c = true);
            update phoenixCase;
        }
        else{
        new VT_R5_eCoaSubjectSchedule(new List<Id> {this.userId})
            .setIsStopEvent(true)
            .execute();
        }
    }

    protected override void handleFinal(VT_D1_HTTPConnectionHandler.Result result) {
        if (!this.skipLogging) { insert result.log; }
    }
}