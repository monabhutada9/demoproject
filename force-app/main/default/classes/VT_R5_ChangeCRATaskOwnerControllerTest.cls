/*************************************************************************************
@author: Vijendra Hire
@Story: SH-14821
@Description: This is the VT_R5_ChangeCRATaskOwnerController's Test Class. 
@Date: 22.10.2020
****************************************************************************************/
@isTest 
public class VT_R5_ChangeCRATaskOwnerControllerTest {

    @TestSetup
    static void dataSetup() {
        Group queue = new Group(Type = 'Queue', Name = 'CRA Test Pool' + Crypto.getRandomInteger());
        insert queue;

        System.enqueueJob(new VT_R5_CRATaskpoolControllerTest.CRATaskQueueSobjectCreator(queue.Id));
    }

/*********************************************************************************************
 	 * Story  : SH-14821
     * Method : testTakeTask
     * param  : N/A
     * description : This mothod is used to cover the code coverage changeOwnerMethod method.
*********************************************************************************************/    
    @isTest
    static void testTakeTask() {
        QueueSobject queue = [SELECT QueueId FROM QueueSobject WHERE SobjectType = 'VTR5_CRA_Task__c' AND CreatedById = :UserInfo.getUserId() LIMIT 1];
        
        Profile testProfile = [SELECT Id FROM Profile WHERE Name = 'CRA' LIMIT 1];
        
        User testUser = new User(LastName = 'test user 1',
                                 Username = 'testcra.user.1@example.com',
                                 Email = 'testcra.1@example.com',
                                 Alias = 'testu1',
                                 TimeZoneSidKey = 'GMT',
                                 LocaleSidKey = 'en_GB',
                                 EmailEncodingKey = 'ISO-8859-1',
                                 ProfileId = testProfile.Id,
                                 LanguageLocaleKey = 'en_US');
        
        VTD1_RTId__c rtId = new VTD1_RTId__c(VTD1_Task_SimpleTask__c = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('SimpleTask').getRecordTypeId());
        insert rtId;
        
        VTR5_CRA_Task__c newT = new VTR5_CRA_Task__c();
        newT.OwnerId = queue.QueueId;
        insert newT;
        
        Test.startTest();
        VT_R5_ChangeCRATaskOwnerController.changeOwnerMethod(newT.Id);
        System.runAs(testUser) {
            VT_R5_ChangeCRATaskOwnerController.changeOwnerMethod(newT.Id);
            VT_R5_ChangeCRATaskOwnerController.changeOwnerMethod(newT.Id);
            VTR5_CRA_Task__c CRATask  = [SELECT Id, OwnerId from VTR5_CRA_Task__c where Id =:newT.Id];
                System.assertEquals(testUser.Id, CRATask.OwnerId);
        }
        Test.stopTest();
    }
}