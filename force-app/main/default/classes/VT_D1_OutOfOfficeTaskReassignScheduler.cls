global class VT_D1_OutOfOfficeTaskReassignScheduler implements Schedulable {
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new VT_D1_OutOfOfficeTaskReassignReminder(), 10);
    }
}