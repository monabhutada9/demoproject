/*
Created By - Yogesh More
Created Date - 04/05/2020
Version - 1.0
Story No. - SH-9185
Discription - Update user language from contact.
*/
public class VTR5_UpdateUserLanguage{
    public static String UpdateUserLanguage(List<Contact> contactList){
        Savepoint sp = Database.setSavepoint();
        try{    
            //Map <Id, Contact> contactMap = new Map<Id, Contact>([select Id, VTR2_Primary_Language__c, VT_R5_Secondary_Preferred_Language__c, VT_R5_Tertiary_Preferred_Language__c from Contact where RecordType.Name = 'Caregiver' OR RecordType.Name = 'Patient']);
            List<User> userList = [SELECT Id, FirstName, LastName, Name, Email, ContactId, LanguageLocaleKey FROM User WHERE ContactId IN : contactList];
            Map <Id, Contact> contactMap = new Map <Id, Contact>();
            
            for(Contact con : contactList){
                contactMap.put(con.id, con);
            }
            
            if(userList != null && userList.size()>0){
                for(User use : userList){
                    if(contactMap.containsKey(use.contactId)){
                        use.LanguageLocaleKey = contactMap.get(use.contactId).VTR2_Primary_Language__c;
                        use.VT_R5_Secondary_Preferred_Language__c = contactMap.get(use.contactId).VT_R5_Secondary_Preferred_Language__c;
                        use.VT_R5_Tertiary_Preferred_Language__c = contactMap.get(use.contactId).VT_R5_Tertiary_Preferred_Language__c;
                    }
                }
                
                update userList;
            }
            return 'Success';
        }
        catch(Exception ex){
            Database.rollback(sp);
            return 'Error Message =>'+ex.getMessage()+'<br/>Line Number - '+ex.getLineNumber()+'<br/>User List - '+String.ValueOf(contactList);
        }
    }
}