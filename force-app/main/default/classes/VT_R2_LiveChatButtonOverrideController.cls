/**
* @author: Carl Judge
* @date: 02-Oct-19
**/


global with sharing class VT_R2_LiveChatButtonOverrideController {
    @AuraEnabled(Cacheable=true)
    public static String getLiveChatButtonId() {
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Authorization', 'Bearer ' + VT_D1_HelperClass.getSessionIdForGuest());
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(System.Url.getOrgDomainUrl().toExternalForm() + '/services/apexrest/LiveChatButtonId');
        req.setBody(UserInfo.getUserId());
        System.debug(req);

        Http http = new Http();
        String buttonId;
        if (!Test.isRunningTest()) {
            HttpResponse res = http.send(req);
            if (res.getStatusCode()!=200) {
                throw new AuraHandledException(res.getStatus() +' '+ res.getBody());
            }
            buttonId = res.getBody();
        }
        return buttonId;
    }
}