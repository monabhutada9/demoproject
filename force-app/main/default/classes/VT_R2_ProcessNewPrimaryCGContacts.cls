/**
* @author: Carl Judge
* @date: 20-Feb-19
* @description: Process caregivers that are inserted as or updated to be primary CG
**/

public without sharing class VT_R2_ProcessNewPrimaryCGContacts {
    private static Set<String> EXCLUDED_VISIT_STATUES = new Set<String>{
        VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED,
        VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED,
        VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED
    };

    public static void processContacts(List<Contact> cons, Map<Id, Contact> oldMap) {
        List<Contact> primaryCGs = new List<Contact>();
        for (Contact con : cons) {
            if (isPrimaryCG(con) && (oldMap == null || !isPrimaryCG(oldMap.get(con.Id)))) {
                primaryCGs.add(con);
            }
        }

        if (! primaryCGs.isEmpty()) {
            createVisitMembers(primaryCGs);
        }
    }

    private static void createVisitMembers(List<Contact> cons) {
        Map<Id, User> usersByAccountId = new Map<Id, User>();
        List<Visit_Member__c> newMembers = new List<Visit_Member__c>();

        for (User item : [SELECT Id, Contact.AccountId FROM User WHERE ContactId IN :cons]) {
            usersByAccountId.put(item.Contact.AccountId, item);
        }

        for (Visit_Member__c patientMember : [
            SELECT VTD1_Actual_Visit__c, VTD1_Participant_User__r.Contact.AccountId, VTR2_DoNotSendNewMemberEmail__c
            FROM Visit_Member__c
            WHERE VTD1_Participant_User__r.Contact.AccountId IN :usersByAccountId.keySet()
            AND VTD1_Actual_Visit__r.VTD1_Status__c NOT IN :EXCLUDED_VISIT_STATUES
            AND VTD1_Participant_User__r.Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
        ]) {
            User u = usersByAccountId.get(patientMember.VTD1_Participant_User__r.Contact.AccountId);
            newMembers.add(new Visit_Member__c(
                RecordTypeId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_VISIT_MEMBER_REGULAR,
                VTD1_Actual_Visit__c = patientMember.VTD1_Actual_Visit__c,
                VTD1_Participant_User__c = u.Id,
                VTR2_DoNotSendNewMemberEmail__c = patientMember.VTR2_DoNotSendNewMemberEmail__c
            ));
        }

        if (! newMembers.isEmpty()) { insert newMembers; }
    }

    private static Boolean isPrimaryCG(Contact con) {
        return con.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_CAREGIVER && con.VTD1_Primary_CG__c;
    }

}