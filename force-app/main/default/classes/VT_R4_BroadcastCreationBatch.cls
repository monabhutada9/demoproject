/**
 * Created by Alexey Mezentsev on 5/7/2020.
 */

public class VT_R4_BroadcastCreationBatch implements Database.Batchable<SObject> {
    private Id chatId;
    private Set<Id> userIds;
    private String message;

    public VT_R4_BroadcastCreationBatch(Id chatId, Set<Id> userIds, String message) {
        this.chatId = chatId;
        this.userIds = userIds;
        this.message = message;
    }

    public Iterable<SObject> start(Database.BatchableContext bc) {
        List<VTD1_Chat_Member__c> members = new List<VTD1_Chat_Member__c>();
        for (Id userId : userIds) {
            VTD1_Chat_Member__c member = new VTD1_Chat_Member__c();
            member.VTD1_Chat__c = chatId;
            member.VTD1_User__c = userId;
            members.add(member);
        }
        return members;
    }

    public void execute(Database.BatchableContext bc, List<VTD1_Chat_Member__c> members) {
        insert members;
    }

    public void finish(Database.BatchableContext bc) {
        ConnectApi.FeedItemInput feedItemInput = VT_D1_CommunityChatHelper.prepareFeedItemToSend(chatId, message);
        if (!Test.isRunningTest()) {
            ConnectApi.ChatterFeeds.postFeedElement(null, (ConnectApi.FeedElementInput) feedItemInput);
        }
    }
}