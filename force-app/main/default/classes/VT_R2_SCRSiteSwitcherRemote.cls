/**
 * Created by Dmitry Yartsev on 21.03.2019.
 */

public with sharing class VT_R2_SCRSiteSwitcherRemote {

    public class Study {
        public String studyId;
        public String studyName;
        public List<Site> sites;
        public Study(String studyId, String studyName, List<Site> sites){
            this.studyId = studyId;
            this.studyName = studyName;
            this.sites = sites;
        }
    }
    public class Site {
        public String siteId;
        public String siteName;
        public String piName;
        public Site(String siteId, String siteName, String piName){
            this.siteId = siteId;
            this.siteName = siteName;
            this.piName = piName;
        }
    }

    @AuraEnabled(Cacheable=true)
    public static String getStudies(){
        try{
            List<Study> studyList = new List<Study>();
            studyList.add(new Study(null, Label.VTD2_AllMyStudies, new List<Site>{new Site(null, null, Label.VTR2_PrimaryInvestigators)}));
            List<AggregateResult> STMlist = [
                    SELECT Study__r.Id studyId,
                            Study__r.Name studyName,
                            VTD1_VirtualSite__r.Id siteId,
                            VTD1_VirtualSite__r.Name siteName,
                            User__r.Name piName
                    FROM Study_Team_Member__c
                    WHERE id IN (SELECT VTR2_Associated_PI__c
                                 FROM Study_Site_Team_Member__c
                                 WHERE VTR2_Associated_SCr__r.User__c =: UserInfo.getUserId())
                    GROUP BY Study__r.Id,
                             Study__r.Name,
                             VTD1_VirtualSite__r.Id,
                             VTD1_VirtualSite__r.Name,
                             User__r.Name
                    ORDER BY Study__r.Id ASC];
            system.debug(STMlist);
            Set<String> studiesSet = new Set<String>();
            Integer counter = 0;
            for (AggregateResult agr : STMlist){
                if(!studiesSet.contains(String.valueOf(agr.get('studyId')))){
                    counter++;
                    studiesSet.add(String.valueOf(agr.get('studyId')));
                    studyList.add(new Study(
                        String.valueOf(agr.get('studyId')),
                        String.valueOf(agr.get('studyName')),
                        new List<Site>{
                            new Site(
                                null,
                                null,
                                Label.VTR2_PrimaryInvestigators
                            ),
                            new Site(
                                String.valueOf(agr.get('siteId')),
                                String.valueOf(agr.get('siteName')),
                                String.valueOf(agr.get('piName'))
                            )
                        }
                    ));
                } else {
                    studyList[counter].sites.add(
                        new Site(
                            String.valueOf(agr.get('siteId')),
                            String.valueOf(agr.get('siteName')),
                            String.valueOf(agr.get('piName'))
                    ));
                }
            }
            return JSON.serialize(studyList);
        } catch (Exception e){
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

}