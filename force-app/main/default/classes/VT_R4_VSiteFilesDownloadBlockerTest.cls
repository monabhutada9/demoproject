/**
 * Created by Yevhen Kharchuk on 10-Apr-20.
 *
 * @description Test class for the VT_R4_VSiteFilesDownloadBlocker.
 */
@IsTest
public with sharing class VT_R4_VSiteFilesDownloadBlockerTest {
    private static final String TESTED_CLASS_NAME = 'VT_R4_VSiteFilesDownloadBlocker';
    private static final Id CONTENT_VERSION_ID = fflib_IDGenerator.generate(ContentVersion.getSObjectType());
    private static final List<Id> CONTENT_VERSION_IDS = new List<Id>{CONTENT_VERSION_ID};
    private static final Id CONTENT_DOCUMENT_ID = fflib_IDGenerator.generate(ContentDocument.getSObjectType());
    private static final Id VIRTUAL_SITE_ID = fflib_IDGenerator.generate(Virtual_Site__c.getSObjectType());
    private static final String ALLOW_SOQL = 'SOQL context must be allowed';
    private static final String MUST_BE_FORBIDDEN = 'Downloading must be forbidden for the current Study';
    private static final String MUST_BE_ALLOWED = 'Downloading must be forbidden for the current Study';
    private static final String MUST_BE_ALLOWED_FOR_ADMIN = 'Downloading must be allowed for admin';


    public static void downloadFileSoql() {
        Sfc.ContentDownloadHandler handler = new VT_R4_VSiteFilesDownloadBlocker()
                .getContentDownloadHandler(CONTENT_VERSION_IDS, Sfc.ContentDownloadContext.SOQL);
        System.assert(handler.isDownloadAllowed, ALLOW_SOQL);
    }

    public static void fileDownloadingIsNotAllowedForStudy() {
        getDocQueryStub().applyStub();
        getDocLinksQueryStub().applyStub();
        getVirtualSiteQueryStub(false).applyStub();

        System.runAs(getNonAdminLightningUser()) {
            Sfc.ContentDownloadHandler handler = new VT_R4_VSiteFilesDownloadBlocker()
                    .getContentDownloadHandler(CONTENT_VERSION_IDS, Sfc.ContentDownloadContext.CONTENT);
            System.assert(!handler.isDownloadAllowed, MUST_BE_FORBIDDEN);
        }
    }

    public static void fileDownloadingIsAllowedForStudy() {
        getDocQueryStub().applyStub();
        getDocLinksQueryStub().applyStub();
        getVirtualSiteQueryStub(true).applyStub();

        System.runAs(getNonAdminLightningUser()) {
            Sfc.ContentDownloadHandler handler = new VT_R4_VSiteFilesDownloadBlocker()
                    .getContentDownloadHandler(CONTENT_VERSION_IDS, Sfc.ContentDownloadContext.CONTENT);
            System.assert(handler.isDownloadAllowed, MUST_BE_ALLOWED);
        }
    }
    
    public static void fileDownloadingIsNotAllowedForStudyButAdmin() {
        getDocQueryStub().applyStub();
        getDocLinksQueryStub().applyStub();
        getVirtualSiteQueryStub(false).applyStub();

        System.runAs(getAdminUser()){
            Sfc.ContentDownloadHandler handler = new VT_R4_VSiteFilesDownloadBlocker()
                    .getContentDownloadHandler(CONTENT_VERSION_IDS, Sfc.ContentDownloadContext.CONTENT);
            System.assert(handler.isDownloadAllowed, MUST_BE_ALLOWED_FOR_ADMIN);
        }
    }

    private static User getNonAdminLightningUser() {
        return getUserWithProfile('Patient Guide');
    }

    private static User getAdminUser() {
        return getUserWithProfile('System Administrator');
    }

    private static User getUserWithProfile(String profileName) {
        String userName = VT_D1_TestUtils.generateUniqueUserName();
        return new User(
                ProfileId = VT_D1_HelperClass.getProfileIdByName(profileName),
                Username = userName,
                LastName = userName,
                Email = userName,
                Alias = 'Test',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                TimeZoneSidKey = 'America/Los_Angeles'
        );
    }

    private static QueryBuilder.StubbedQueryBuilder getDocQueryStub() {
        return new QueryBuilder()
                .buildStub()
                .addStubToSObject(new ContentVersion(ContentDocumentId = CONTENT_DOCUMENT_ID))
                .namedQueryStub(TESTED_CLASS_NAME + '.getDocumentIdFromCVId');
    }

    private static QueryBuilder.StubbedQueryBuilder getDocLinksQueryStub() {
        return new QueryBuilder()
                .buildStub()
                .addStubToList(
                        new List<ContentDocumentLink>{
                                new ContentDocumentLink(LinkedEntityId = VIRTUAL_SITE_ID)
                        })
                .namedQueryStub(TESTED_CLASS_NAME + '.getDocumentLinks');
    }

    private static QueryBuilder.StubbedQueryBuilder getVirtualSiteQueryStub(Boolean isDownloadAvailableForStudy) {
        return new QueryBuilder()
                .buildStub()
                .addStubToSObject(
                        new Virtual_Site__c(
                                VTD1_Study_Site_Number__c = '12345',
                                VTD1_Study__r = new HealthCloudGA__CarePlanTemplate__c(
                                        VTR4_Download_Site_Files__c = isDownloadAvailableForStudy)))
                .namedQueryStub(TESTED_CLASS_NAME + '.isBlockingEnabledForStudy');
    }
}