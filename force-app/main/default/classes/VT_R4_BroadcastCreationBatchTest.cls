/**
 * Created by Alexey Mezentsev on 5/12/2020.
 */

@IsTest
public with sharing class VT_R4_BroadcastCreationBatchTest {

    public static void createMembersInBatch() {
        Id studyId = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c].Id;
        User userPrimaryPI = [SELECT Id FROM User WHERE FirstName LIKE 'PIUser%'];
            Set<Id> recipients = VT_D1_CommunityChatHelper.findRecipients(studyId, 'PI');
            VTD1_Chat__c chat = new VTD1_Chat__c();
            chat.Name = 'Test';
            chat.VTD1_Chat_Type__c = 'Broadcast';
            chat.VTR2_Study__c = studyId;
            insert chat;
            System.assert([SELECT Id FROM VTD1_Chat_Member__c WHERE VTD1_Chat__c =: chat.Id].isEmpty());
            Test.startTest();
            Database.executeBatch(new VT_R4_BroadcastCreationBatch(chat.Id, recipients, 'message'));
            Test.stopTest();
            System.assert(![SELECT Id FROM VTD1_Chat_Member__c WHERE VTD1_Chat__c =: chat.Id].isEmpty());
    }
}