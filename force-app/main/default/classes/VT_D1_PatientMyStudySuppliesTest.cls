@IsTest
private class VT_D1_PatientMyStudySuppliesTest {
    private static Case cas;
    private static User u;

    @TestSetup
    static void setup() {

        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
        List<Case> casesList = [SELECT VTD1_Patient_User__c, VTD1_Patient__r.VTD1_Patient_Name__c, VTD1_Study__c FROM Case];

        if(casesList.size() > 0){
            new DomainObjects.VTD1_Patient_Kit_t()
                    .setVTD1_Case(casesList.get(0).Id)
                    .setVTD2_MaterialId('materialId' + Crypto.getRandomInteger())
                    .addVTD1_Patient_Delivery(new DomainObjects.VTD1_Order_t()
                            .setVTD1_Status('Delivered')
                            .setVTD1_Case(casesList.get(0).Id)
                            .setVTD1_Expected_Shipment_Date(Date.today())
                            .setVTD1_Delivery_Order(1)
                            .setVTD1_ShipmentId('shipmentId' + Crypto.getRandomInteger())
                    )
                    .persist();
            new DomainObjects.VTD1_Patient_Kit_t()
                    .setVTD1_Case(casesList.get(0).Id)
                    .setVTD2_MaterialId('materialId' + Crypto.getRandomInteger())
                    .addVTD1_Patient_Delivery(new DomainObjects.VTD1_Order_t()
                            .setVTD1_Status('Delivered')
                            .setVTD1_Case(casesList.get(0).Id)
                            .setVTD1_Expected_Shipment_Date(Date.today())
                            .setVTD1_Delivery_Order(2)
                            .setVTD1_ShipmentId('shipmentId' + Crypto.getRandomInteger())
                    )
                    .persist();
            new DomainObjects.VTD1_Patient_Kit_t()
                    .setVTD1_Case(casesList.get(0).Id)
                    .setVTD2_MaterialId('materialId' + Crypto.getRandomInteger())
                    .addVTD1_Patient_Delivery(new DomainObjects.VTD1_Order_t()
                            .setVTD1_Status('Delivered')
                            .setVTD1_Case(casesList.get(0).Id)
                            .setVTD1_Expected_Shipment_Date(Date.today())
                            .setVTD1_Delivery_Order(3)
                            .setVTD1_ShipmentId('shipmentId' + Crypto.getRandomInteger())
                    )
                    .persist();
            new DomainObjects.VTD1_Patient_Kit_t()
                    .setVTD1_Case(casesList.get(0).Id)
                    .setVTD2_MaterialId('materialId' + Crypto.getRandomInteger())
                    .addVTD1_Patient_Delivery(new DomainObjects.VTD1_Order_t()
                            .setVTD1_Status('Delivered')
                            .setVTD1_Case(casesList.get(0).Id)
                            .setVTD1_Expected_Shipment_Date(Date.today())
                            .setVTD1_Delivery_Order(4)
                            .setVTD1_ShipmentId('shipmentId' + Crypto.getRandomInteger())
                    )
                    .persist();
            new DomainObjects.VTD1_Patient_Kit_t()
                    .setVTD1_Case(casesList.get(0).Id)
                    .setVTD2_MaterialId('materialId' + Crypto.getRandomInteger())
                    .addVTD1_Patient_Delivery(new DomainObjects.VTD1_Order_t()
                    .setVTD1_Status('Delivered')
                    .setVTD1_Case(casesList.get(0).Id)
                    .setVTD1_Expected_Shipment_Date(Date.today())
                    .setVTD1_Delivery_Order(4)
                    .setVTD1_ShipmentId('shipmentId' + Crypto.getRandomInteger())
            )
                    .persist();
        }
    }

    static {
        List<Case> casesList = [SELECT VTD1_Patient_User__c FROM Case];
        if(casesList.size() > 0){
            cas = casesList.get(0);
            u = [SELECT Id, ContactId FROM User WHERE Id =: cas.VTD1_Patient_User__c];
        }
    }

    @IsTest
    static void getPatientStudySuppliesTest() {
        //User userSysAdmin = [SELECT ContactId FROM User WHERE Profile.Name = 'System Administrator' AND Id != :UserInfo.getUserId() AND IsActive = TRUE LIMIT 1];
        //System.runAs(userSysAdmin) {
            List<VTD1_Order__c> deliveryList = [SELECT Id, VTD1_Status__c FROM VTD1_Order__c WHERE VTD1_Case__c = :cas.Id];
            System.debug('deliveryList ' + deliveryList);
            System.assert(deliveryList.size() > 4);
            List<VTD1_Order__c> deliveryListToUpdate = new List<VTD1_Order__c>();

            VTD1_Order__c delivery0 = deliveryList.get(0);
            //delivery0.VTD1_isReturnDelivery__c = true;
            delivery0.VTD1_Status__c = 'Received';
            deliveryListToUpdate.add(delivery0);

            VTD1_Order__c delivery1 = deliveryList.get(1);
            delivery1.VTD1_Status__c = 'Received';
            delivery1.VTD1_ExpectedDeliveryDate__c = Date.today();
            delivery1.VTD1_Shipment_Date__c = Date.today();
            deliveryListToUpdate.add(delivery1);

            List<VTD1_Patient_Kit__c> kits = [SELECT Id, VTD1_Recalled__c FROM VTD1_Patient_Kit__c WHERE VTD1_Patient_Delivery__c =: delivery1.Id];
            for (VTD1_Patient_Kit__c kit : kits) {
                kit.VTD1_Recalled__c = true;
            }
            update kits;

            VTD1_Order__c delivery2 = deliveryList.get(2);
            delivery2.VTD1_isReturnDelivery__c = true;
            delivery2.VTD1_Status__c = 'Pending Return';
            delivery2.VTD1_Patient_Delivery__c = delivery0.Id;
            deliveryListToUpdate.add(delivery2);

            VTD1_Order__c delivery3 = deliveryList.get(3);
            delivery3.VTD1_isReturnDelivery__c = true;
            delivery3.VTD1_Status__c = 'Pending Return';
            delivery3.VTD1_Patient_Delivery__c = delivery0.Id;
            deliveryListToUpdate.add(delivery3);

            VTD1_Order__c delivery4 = deliveryList.get(4);
            delivery4.VTD1_isReturnDelivery__c = true;
            delivery4.VTD1_Status__c = 'Pending Return';
            delivery4.VTD1_Patient_Delivery__c = delivery0.Id;
            deliveryListToUpdate.add(delivery4);

            System.debug('deliveryListToUpdate '+deliveryListToUpdate);
            update deliveryListToUpdate;

            List<Task> tasksToInsert = new List<Task>();

            Task task1 = new Task();
            task1.OwnerId = cas.VTD1_Patient_User__c;
            task1.VTD1_Type_for_backend_logic__c  = 'Confirm Packaging Materials in Hand';
            task1.Status = 'Open';
            task1.WhatId = delivery1.Id;
            tasksToInsert.add(task1);

            Task task2 = new Task();
            task2.OwnerId = cas.VTD1_Patient_User__c;
            task2.VTD1_Type_for_backend_logic__c  = 'Confirm Courier Pickup';
            task2.Status = 'Open';
            task2.WhatId = delivery2.Id;
            tasksToInsert.add(task2);

            Task task3 = new Task();
            task3.OwnerId = cas.VTD1_Patient_User__c;
            task3.VTD1_Type_for_backend_logic__c  = 'Confirm Drop Off of Study Medication';
            task3.Status = 'Open';
            task3.WhatId = delivery3.Id;
            tasksToInsert.add(task3);

            Task task4 = new Task();
            task4.OwnerId = cas.VTD1_Patient_User__c;
            task4.VTD1_Type_for_backend_logic__c  = 'Confirm Courier Pickup';
            task4.Status = 'Open';
            task4.WhatId = delivery4.Id;
            tasksToInsert.add(task4);

            insert tasksToInsert;
        //}
        User u = [SELECT Id, ContactId FROM User WHERE Id = :cas.VTD1_Patient_User__c];

        Test.startTest();
        System.runAs(u) {
            VT_D1_PatientMyStudySuppliesController.getPatientStudySupplies();
        }
        Test.stopTest();
    }
}