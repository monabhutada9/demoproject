/**
 * @author: Alexander Komarov
 * @date: 23.10.2020
 * @description:
 */

public with sharing class VT_R5_MobileLoginFlow {
    public static final String ACTIVATION_STAGE = 'Activation';
    public static final String PRIVACY_NOTICES_STAGE = 'PrivacyNotices';
    public static final String AUTHORIZATION_STAGE = 'Authorization';
    public static final String SETUP_PASSWORD_STAGE = 'SetupPassword';
    public static final String ARTICLE_NOT_FOUND_ERROR = 'ARTICLE_NOT_FOUND';
    public static final String VERIFICATION_CODE_STAGE = 'VerificationCode';
    public static final String INITIAL_PROFILE_SETUP_STAGE = 'InitialProfileSetup';
    public static final String SUCCESS_STAGE = 'SUCCESS';

    private static final Integer VERIFICATION_CODE_LENGTH = 6;
    private static final Integer VERIFICATION_CODE_TTL_MINUTES = 15; // 15 minutes
    private static final String VERIFICATION_CODE_SESSION_IDENTIFIER = '2FACode!DEVICE';
    private static final String VERIFICATION_CODE_DATE_SESSION_IDENTIFIER = '2FACodeDate!DEVICE';

    private static final String ERROR_CODE_EXPIRED = 'CODE_EXPIRED ';
    private static final String ERROR_CODE_WRONG = 'CODE_WRONG ';
    private static final String SUCCESS_CODE_SENT = 'CODE_SENT ';
    private static final String SUCCESS_CODE_ACCEPTED = 'CODE_ACCEPTED ';
    private static final String ERROR_CODE_NEED = 'CODE_NEED ';
    private static final String ERROR_SETUP_PASSWORD_NEED = 'SETUP_PASSWORD_NEED ';
    private static final String SUCCESS_ARTICLE_SENT = 'ARTICLE_SENT ';

    private static Cache.SessionPartition cachePartition;
    public static User currentUser;
    public static Boolean isActivationProcess = false;
    public static LoginFlowResponse loginFlowResponse = new LoginFlowResponse();
    /**
     * @description Adds 2FACode part, removing '-' out of device id and saving to cache
     * ex. FC9548A4-B2CB-4DF0-8234-DCDB67BA468C -> 2FACodeFC9548A4B2CB4DF08234DCDB67BA468C : CODE
     *
     * @param device mobile identifier ex. FC9548A4-B2CB-4DF0-8234-DCDB67BA468C
     * @param value generated code, 6 digits
     */
    private static void addCodeToSessionCache(String device, Object value) {
        String key = VERIFICATION_CODE_SESSION_IDENTIFIER.replace('!DEVICE', device.replace('-', ''));
        cachePartition.put(key, value, VERIFICATION_CODE_TTL_MINUTES * 60, Cache.Visibility.ALL, false);
    }
    /**
     * @description Adds 2FACodeDate part, removing '-' out of device id and saving to cache
     * ex. FC9548A4-B2CB-4DF0-8234-DCDB67BA468C -> 2FACodeFC9548A4B2CB4DF08234DCDB67BA468C : DATE
     *
     * @param device mobile identifier ex. FC9548A4-B2CB-4DF0-8234-DCDB67BA468C
     * @param value current datetime
     */
    private static void addDateToSessionCache(String device, Object value) {
        String key = VERIFICATION_CODE_DATE_SESSION_IDENTIFIER.replace('!DEVICE', device.replace('-', ''));
        cachePartition.put(key, value, VERIFICATION_CODE_TTL_MINUTES * 60, Cache.Visibility.ALL, false);
    }
    /**
     * @description Returns the cached value for given device
     *
     * @param device mobile identifier ex. FC9548A4-B2CB-4DF0-8234-DCDB67BA468C
     *
     * @return either the actual cached value or NOT_NUMBER constant, never null
     */
    private static String getCachedVerificationCode(String device) {
        String cachedCode = (String) cachePartition.get(VERIFICATION_CODE_SESSION_IDENTIFIER.replace('!DEVICE', device.replace('-', '')));
        return cachedCode == null ? 'NOT_NUMBER' : cachedCode;
    }
    private static void initCache() {
        cachePartition = Cache.Session.getPartition('local.default');
    }

    public static void setUserAndDevice(String deviceCode, String userId) {
        VT_R5_MobileAuth.parsedParams.deviceUniqueCode = deviceCode;
        VT_R5_MobileAuth.parsedParams.userID = userId;
    }
    /**
     * @description Class-holder for Initial Profile Setup
     */
    private class InitialProfileData {
        Boolean isPatient;
        String firstName;
        String lastName;
        String middleName;
        Long dateOfBirth;
        String birthDate;
        String dobRestriction;
        String gender;
        String phoneNumber;
        String state;
        String zipCode;
        String preferredContactMethod;
        String language;
        String secondaryLanguage;
        String tertiaryLanguage;
        String caseStatus;
        Boolean isAdditionalMsgDisplay;
        List<MobilePicklistOption> primaryLanguages = new List<MobilePicklistOption>();
        List<MobilePicklistOption> additionalLanguages = new List<MobilePicklistOption>();
        List<VT_D1_PatientProfileIntialController.InputSelectRatioElement> contactPreferredContactMethodSelectRatioElementList;
        List<MobilePicklistOption> states = new List<MobilePicklistOption>();
        List<MobilePicklistOption> genders = new List<MobilePicklistOption>();

        private InitialProfileData() {
            VT_R5_PatientProfileService patientProfile = new VT_R5_PatientProfileService();
            this.firstName = patientProfile.contact.FirstName;
            this.lastName = patientProfile.contact.LastName;
            this.middleName = patientProfile.contact.VTD1_MiddleName__c;
            this.phoneNumber = patientProfile.phone.PhoneNumber__c;
            this.dobRestriction = patientProfile.getDateOfBirthRestriction();
            this.dateOfBirth = patientProfile.contact.Birthdate != null ? Datetime.newInstance(patientProfile.contact.Birthdate, Time.newInstance(0, 0, 0, 0)).getTime() : null;
            fillDateOfBirth(patientProfile.contact.Birthdate, patientProfile.getDateOfBirthRestriction());
            this.isPatient = patientProfile.usr.Profile.Name.equals(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
            this.gender = isPatient ? patientProfile.contact.HealthCloudGA__Gender__c : null;
            setPreferredLanguages(patientProfile.contact);
            this.preferredContactMethod = patientProfile.contact.Preferred_Contact_Method__c;
            this.contactPreferredContactMethodSelectRatioElementList =
                    VT_D1_PatientProfileIntialController.getContactPreferredContactMethodSelectRatioElementList(patientProfile.contact);
            if (this.isPatient) setAddressDataForPatient(patientProfile.address);
            this.isAdditionalMsgDisplay = isAdditionalMessageShouldDisplay(patientProfile.cse);
            this.caseStatus = patientProfile.cse.Status;
            this.genders = getGenderOptions(patientProfile.contact);
        }

        private List<MobilePicklistOption> getGenderOptions(Contact contact) {
            List<MobilePicklistOption> result = new List<MobilePicklistOption>();
            Map<String, String> optionsMap = VT_D1_CustomMultiPicklistController.getSelectOptionsWithLabel(contact, 'HealthCloudGA__Gender__c');
            for (String s : optionsMap.keySet()) {
                result.add(new MobilePicklistOption(s, optionsMap.get(s)));
            }
            return result;
        }

        private void setPreferredLanguages(Contact contact) {
            String primaryLanguage = contact.VTR2_Primary_Language__c;
            String secondaryLanguage = contact.VT_R5_Secondary_Preferred_Language__c;
            String tertiaryLanguage = contact.VT_R5_Tertiary_Preferred_Language__c;

            if (String.isNotBlank(primaryLanguage)) this.language = primaryLanguage;
            if (String.isNotBlank(secondaryLanguage)) this.secondaryLanguage = secondaryLanguage;
            if (String.isNotBlank(tertiaryLanguage)) this.tertiaryLanguage = tertiaryLanguage;

            this.primaryLanguages = filterPrimaryLanguages(getMobilePicklistOptions('Contact', 'VTR2_Primary_Language__c'));
            this.additionalLanguages = getMobilePicklistOptions('Contact', 'VT_R5_Secondary_Preferred_Language__c');
        }

        private List<MobilePicklistOption> filterPrimaryLanguages(List<MobilePicklistOption> income) {
            List<MobilePicklistOption> result = new List<MobilePicklistOption>();
            Set<String> validLanguages = new Set<String>();
            if (currentUser.Contact != null &&
                    currentUser.Contact.VTD1_Clinical_Study_Membership__r != null &&
                    currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r != null &&
                    currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c != null) {
                validLanguages.addAll(currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c.split(';'));
            }
            for (MobilePicklistOption mpo : income) {
                if (validLanguages.contains(mpo.key)) result.add(mpo);
            }
            return result;
        }

        /*
        public String getDateOfBirthRestriction() {
        if (contact != null &&
                contact.VTD1_Clinical_Study_Membership__r != null &&
                contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r != null &&
                contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR3_Study_Geography__r != null &&
                contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR3_Study_Geography__r.VT_R3_Date_Of_Birth_Restriction__c != null &&
                contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR3_Study_Geography__r.VT_R3_Date_Of_Birth_Restriction__c.equals('Year Only')) {
            return YEAR_ONLY;
        } else {
            return FULL_DATE_OF_BIRTH;
        }
    }
         */

        private void setAddressDataForPatient(Address__c address) {
            this.state = address.State__c;
            this.zipCode = address.ZipCode__c;

            if (address.Country__c != null) {
                VTR2_DependentPicklist.Picklist p = VTR2_DependentPicklist.getDependentPicklist(
                        'Address__c', 'Country__c', 'State__c'
                );
                VTR2_DependentPicklist.PicklistOption neededOption;
                for (VTR2_DependentPicklist.PicklistOption po : p.options) {
                    if (po.value.equals(address.Country__c)) {
                        neededOption = po;
                    }
                }
                for (VTR2_DependentPicklist.PicklistOption po : neededOption.dependentOptions) {
                    this.states.add(new MobilePicklistOption(po.value, po.label));

                }
            }
        }

        private Boolean isAdditionalMessageShouldDisplay(Case cse) {
            if (cse == null) return false;
            return cse.VTD2_Patient_Status__c.equals(VT_R4_ConstantsHelper_Statuses.CASE_PRE_CONSENT);
        }

        private void fillDateOfBirth(Date birthDate, String restriction) {
            String YEAR_ONLY = 'Year Only';
            if (birthDate == null) return;
            this.birthDate = restriction.equals(YEAR_ONLY)
                    ? String.valueOf(birthDate.year())
                    : String.valueOf(birthDate);
        }
    }

    private class MobilePicklistOption {
        String key;
        String label;

        MobilePicklistOption(String key, String label) {
            this.key = key;
            this.label = label;
        }
    }

    private static List<MobilePicklistOption> getMobilePicklistOptions(String obj, String field) {
        List<MobilePicklistOption> result = new List<MobilePicklistOption>();
        SObjectType type = Schema.getGlobalDescribe().get(obj);
        Schema.SObjectField sObjectField = type.getDescribe().fields.getMap().get(field);
        for (Schema.PicklistEntry pe : sObjectField.getDescribe().getPicklistValues()) {
            result.add(new MobilePicklistOption(pe.getValue(), pe.getLabel()));
        }
        return result;
    }
    /**
     * @description Checks verification code exists and not expired
     *
     * @param deviceCode device mobile identifier ex. FC9548A4-B2CB-4DF0-8234-DCDB67BA468C
     *
     * @return TRUE if expired, otherwise FALSE
     */
    public static Boolean checkVerificationCodeExpired(String deviceCode) {
        Datetime codeCreated = (Datetime) cachePartition.get(VERIFICATION_CODE_DATE_SESSION_IDENTIFIER.replace('!DEVICE', deviceCode.replace('-', '')));
        System.debug('codeCreated =' + codeCreated);
        if (getCachedVerificationCode(deviceCode).equals('NOT_NUMBER') || getCachedVerificationCode(deviceCode).equals('OLD_CODE')) {
            System.debug('well, null code, cute cache');
            return true;
        }
        System.debug('result=' + (codeCreated.addMinutes(VERIFICATION_CODE_TTL_MINUTES) < System.now()));
        if (codeCreated == null || codeCreated.addMinutes(VERIFICATION_CODE_TTL_MINUTES) < System.now()) {
            return true;
        } else {
            return false;
        }
    }

    public static void resendCode() {
        initCache();
        setCurrentUser();
        sendVerificationCode(VT_R5_MobileAuth.parsedParams.deviceUniqueCode);
    }
    public static LoginFlowResponse getLoginFlowState() {
        if (!isActivationProcess) {
            initCache();
        }
        if (VT_R5_MobileAuth.parsedParams.userID == null) {
            VT_R5_MobileAuth.parsedParams.userID = UserInfo.getUserId();
        }
        setCurrentStatuses();
        formGetLoginFlowStateResponse();
        return loginFlowResponse;
    }

    private static void formGetLoginFlowStateResponse() {
        String currentStage = loginFlowResponse.currentStage;
        System.debug('currentStage' + currentStage);

        if (currentStage.equals(VERIFICATION_CODE_STAGE)) {
            if (checkVerificationCodeExpired(VT_R5_MobileAuth.parsedParams.deviceUniqueCode)) {
                sendVerificationCode(VT_R5_MobileAuth.parsedParams.deviceUniqueCode);
                loginFlowResponse.serverMessage += ERROR_CODE_EXPIRED + SUCCESS_CODE_SENT;
            } else {
                loginFlowResponse.serverMessage += ERROR_CODE_NEED;
            }
        } else if (currentStage.equals(PRIVACY_NOTICES_STAGE) || currentStage.equals(AUTHORIZATION_STAGE)) {
            System.debug(loginFlowResponse);
            LoginFlowArticle article = getArticle(currentStage);
            if (article != null) {
                loginFlowResponse.setArticle(article, currentStage);
                loginFlowResponse.serverMessage += SUCCESS_ARTICLE_SENT;
                System.debug(loginFlowResponse);
            } else {
                loginFlowResponse.errors.add('no article found');
                loginFlowResponse.studyPhoneNumber = currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD2_Study_Phone_Number__c;
                loginFlowResponse.currentStage = ARTICLE_NOT_FOUND_ERROR;
            }
        } else if (currentStage.equals(SETUP_PASSWORD_STAGE) && isActivationProcess) {
            loginFlowResponse.serverMessage += ERROR_SETUP_PASSWORD_NEED;
            loginFlowResponse.username = currentUser.Username;
        } else if (currentStage.equals(INITIAL_PROFILE_SETUP_STAGE)) {
            loginFlowResponse.initialProfileData = new InitialProfileData();
        } else if (currentStage.equals(SUCCESS_STAGE)) {
            loginFlowResponse.serverMessage += SUCCESS_STAGE;
        }
    }

    private static Boolean userUpdateNeed = false;
    public static LoginFlowResponse postAction() {
        if (VT_R5_MobileAuth.parsedParams.isActivationProcess != null && VT_R5_MobileAuth.parsedParams.isActivationProcess) isActivationProcess = true;
        if (!isActivationProcess) {
            initCache();
        }
        setCurrentUser();
        if (!VT_R5_MobileAuth.AVAILABLE_PROFILES.contains(currentUser.Profile.Name)) {
            return loginFlowResponse;
        }
        loginFlowResponse.actionFailed = false;
        if (VT_R5_MobileAuth.parsedParams.stage.equals(VERIFICATION_CODE_STAGE)) {
            processVerificationCodeStage();
        } else if (VT_R5_MobileAuth.parsedParams.stage.equals(PRIVACY_NOTICES_STAGE)) {
            processPrivacyNoticeStage();
        } else if (VT_R5_MobileAuth.parsedParams.stage.equals(AUTHORIZATION_STAGE)) {
            processAuthorization();
        } else if (VT_R5_MobileAuth.parsedParams.stage.equals(ACTIVATION_STAGE)) {
            processActivationStage();
        } else if (VT_R5_MobileAuth.parsedParams.stage.equals(INITIAL_PROFILE_SETUP_STAGE)) {
            processInitialProfileSetup();
        } else if (VT_R5_MobileAuth.parsedParams.stage.equals(SETUP_PASSWORD_STAGE)) {
            processSetupPasswordStage();
            return loginFlowResponse;
        }
        System.debug('activationForbidden' + activationForbidden);

        if (loginFlowResponse.actionFailed || activationForbidden) {
            System.debug('1');
            loginFlowResponse.actionFailed = true;
            return loginFlowResponse;
        } else {
            if (userUpdateNeed) update currentUser;
            if (!loginFlowResponse.actionFailed) loginFlowResponse.actionFailed = null;
            System.debug('1');

            return getLoginFlowState();
        }
    }

    private static void processSetupPasswordStage() {
        currentUser.VTR4_Is_Activated__c = true;
        update currentUser;
        loginFlowResponse.newPassword = VT_R5_MobileAuth.parsedParams.newPassword;
        loginFlowResponse.username = currentUser.Username;
        System.setPassword(currentUser.Id, VT_R5_MobileAuth.parsedParams.newPassword);
        System.debug('SETTING PASSWORD ' + loginFlowResponse.newPassword);
    }

    private static final String ALREADY_ACTIVATED_MESSAGE = 'ALREADY_ACTIVATED';
    private static final String ACTIVATION_EXPIRED_MESSAGE = 'ACTIVATION_EXPIRED';
    private static Boolean activationForbidden = false;
    private static void processActivationStage() {
        if (currentUser.VTR4_Is_Activated__c) {
            loginFlowResponse.serverMessage = ALREADY_ACTIVATED_MESSAGE;
            loginFlowResponse.errors.add(ALREADY_ACTIVATED_MESSAGE);
            activationForbidden = true;
            System.debug('set activationForbidden' + activationForbidden);
        } else if (currentUser.CreatedDate.addDays(7) < System.now()) {
            loginFlowResponse.serverMessage = ACTIVATION_EXPIRED_MESSAGE;
            loginFlowResponse.errors.add(ACTIVATION_EXPIRED_MESSAGE);
            activationForbidden = true;
        } else {
            if (VT_R5_MobileAuth.parsedParams.deviceUniqueCode != null && currentUser.VTR3_Verified_Mobile_Devices__c.indexOf(VT_R5_MobileAuth.parsedParams.deviceUniqueCode) == -1) {
                currentUser.VTR3_Verified_Mobile_Devices__c += VT_R5_MobileAuth.parsedParams.deviceUniqueCode + ';';
            }
            userUpdateNeed = true;
        }
    }


    private static void processAuthorization() {
        if (VT_R5_MobileAuth.parsedParams.isAuthorizationAccepted) {
            currentUser.HIPAA_Accepted__c = true;
            currentUser.HIPAA_Accepted_Date_Time__c = System.now();
            userUpdateNeed = true;
        } else {
            loginFlowResponse.serverMessage += 'AUTHORIZATION_HAS_TO_BE_ACCEPTED';
            loginFlowResponse.actionFailed = true;
        }
    }

    private static void processPrivacyNoticeStage() {
        if (VT_R5_MobileAuth.parsedParams.isPrivacyNoticesAccepted) {
            currentUser.VTD1_Privacy_Notice_Last_Accepted__c = System.now();
            userUpdateNeed = true;
        } else {
            loginFlowResponse.serverMessage += 'PRIVACY_NOTICES_HAS_TO_BE_ACCEPTED';
            loginFlowResponse.actionFailed = true;
        }
    }

    private static void processVerificationCodeStage() {
        if (!needVerificationCode(VT_R5_MobileAuth.parsedParams.deviceUniqueCode)) {
            loginFlowResponse.serverMessage += 'WRONG_STAGE ';
            loginFlowResponse.actionFailed = true;
        }
        if (checkVerificationCodeExpired(VT_R5_MobileAuth.parsedParams.deviceUniqueCode)) {
            getLoginFlowState();
        } else {
            if (getCachedVerificationCode(VT_R5_MobileAuth.parsedParams.deviceUniqueCode).equals(VT_R5_MobileAuth.parsedParams.verificationCode)) {
                loginFlowResponse.serverMessage += SUCCESS_CODE_ACCEPTED;
                if (currentUser.VTR3_Verified_Mobile_Devices__c.indexOf(VT_R5_MobileAuth.parsedParams.deviceUniqueCode) == -1) {
                    currentUser.VTR3_Verified_Mobile_Devices__c += VT_R5_MobileAuth.parsedParams.deviceUniqueCode + ';';
                }
                currentUser.First_Log_In__c = System.now().date();
                userUpdateNeed = true;
                addCodeToSessionCache(VT_R5_MobileAuth.parsedParams.deviceUniqueCode, 'OLD_CODE');

            } else {
                loginFlowResponse.serverMessage += ERROR_CODE_WRONG;
                loginFlowResponse.message = VT_D1_TranslateHelper.getLabelValue(Label.VTR3_InvalidVerificationCode, UserInfo.getLanguage());
                loginFlowResponse.actionFailed = true;
            }
        }
    }
    public static void processInitialProfileSetup() {
        VT_R5_PatientProfileService patientService = new VT_R5_PatientProfileService();
        patientService.contact.FirstName = VT_R5_MobileAuth.parsedParams.firstName;
        patientService.contact.Preferred_Contact_Method__c = VT_R5_MobileAuth.parsedParams.preferredContactMethod;
        patientService.contact.LastName = VT_R5_MobileAuth.parsedParams.lastName;
        if (VT_R5_MobileAuth.parsedParams.middleName != null) patientService.contact.VTD1_MiddleName__c = VT_R5_MobileAuth.parsedParams.middleName;
        if (VT_R5_MobileAuth.parsedParams.language != null) patientService.contact.VTR2_Primary_Language__c = VT_R5_MobileAuth.parsedParams.language;
        if (VT_R5_MobileAuth.parsedParams.secondaryLanguage != null) patientService.contact.VT_R5_Secondary_Preferred_Language__c = VT_R5_MobileAuth.parsedParams.secondaryLanguage;
        if (VT_R5_MobileAuth.parsedParams.tertiaryLanguage != null) patientService.contact.VT_R5_Tertiary_Preferred_Language__c = VT_R5_MobileAuth.parsedParams.tertiaryLanguage;
        if (VT_R5_MobileAuth.parsedParams.gender != null) patientService.contact.HealthCloudGA__Gender__c = VT_R5_MobileAuth.parsedParams.gender;

        if (VT_R5_MobileAuth.parsedParams.dateOfBirth != null) {
            patientService.contact.Birthdate =
                    Datetime.newInstance(VT_R5_MobileAuth.parsedParams.dateOfBirth - UserInfo.getTimeZone().getOffset(System.now())).date();
        }
        if (VT_R5_MobileAuth.parsedParams.birthDate != null) {
            try {
                patientService.contact.Birthdate = VT_R5_MobileAuth.parsedParams.birthDate.length() == 4
                        ? Date.newInstance(Integer.valueOf(VT_R5_MobileAuth.parsedParams.birthDate), 1, 1)
                        : Date.valueOf(VT_R5_MobileAuth.parsedParams.birthDate);
            } catch (TypeException te) {
                System.debug(te.getMessage() + ': ' + te.getStackTraceString());
            }
        }

        String stateForCreate, zipCodeForCreate, phoneNumberForCreate = VT_R5_MobileAuth.parsedParams.phoneNumber;
        if (patientService.usr.Profile.Name.equals(VT_R4_ConstantsHelper_Profiles.PATIENT_PROFILE_NAME)) {
            if (patientService.address == null) {
                stateForCreate = VT_R5_MobileAuth.parsedParams.state;
            } else {
                patientService.address.ZipCode__c = VT_R5_MobileAuth.parsedParams.zipCode;
                patientService.address.State__c = VT_R5_MobileAuth.parsedParams.state;
            }
            zipCodeForCreate = VT_R5_MobileAuth.parsedParams.zipCode;
        }
        patientService.updatePatientProfile(phoneNumberForCreate, stateForCreate, zipCodeForCreate);
    }


    public static LoginFlowArticle getArticle(String stage) {
        String preferredArticleLanguage = currentUser.LanguageLocaleKey;

        String articleType = stage == PRIVACY_NOTICES_STAGE
                ? 'Privacy_Notice'
                : 'HIPAA';
        Knowledge__kav preferredLanguageArticle;

        String profileCondition;
        if (currentUser.Profile.Name.equals(VT_R4_ConstantsHelper_Profiles.PI_PROFILE_NAME)) {
            profileCondition = 'AND Primary_For_PI__c = TRUE ';
        } else if (currentUser.Profile.Name.equals(VT_R4_ConstantsHelper_Profiles.SCR_PROFILE_NAME)) {
            profileCondition = 'AND Primary_For_SCR__c = TRUE ';
        } else {
            String studyId = currentUser.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c;
            profileCondition = 'AND VTD2_Study__c = \'' + studyId + '\' ';
        }

        String queryStr = 'SELECT Id, VTD1_Content__c, Language, Title, Summary ' +
                'FROM Knowledge__kav ' +
                'WHERE IsLatestVersion = TRUE ' +
                'AND PublishStatus = \'Online\' ' +
                'AND VTD2_Type__c INCLUDES(:articleType) ' +
                profileCondition +
                'AND Language = :preferredArticleLanguage ';

        List<Knowledge__kav> articles = Database.query(queryStr);
        for (Knowledge__kav item : articles) {
            if (item.Language == preferredArticleLanguage) {
                preferredLanguageArticle = item;
                break;
            }
        }

        if (preferredLanguageArticle != null) {
            return new LoginFlowArticle(preferredLanguageArticle);
        } else if (preferredLanguageArticle == null && !VT_R4_ConstantsHelper_Profiles.HIPAA_PROFILES.contains(currentUser.Profile.Name)) {
            return new LoginFlowArticle();
        } else {
            return null;
        }

    }
    /**
     * @description Send verification code via email and save it to org cache.
     *
     * @param deviceUniqueCode device mobile identifier ex. FC9548A4-B2CB-4DF0-8234-DCDB67BA468C
     */
    public static void sendVerificationCode(String deviceUniqueCode) {
        String verificationCode = '';
        for (Integer i = 0; i < VERIFICATION_CODE_LENGTH; i++) {
            verificationCode += Integer.valueOf(Math.random() * 10);
        }
        OrgWideEmailAddress oea = VT_D1_HelperClass.getOrgWideEmailAddress();
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.optOutPolicy = 'FILTER';
        if (oea != null) {
            message.setOrgWideEmailAddressId(oea.Id);
        }
        message.setSaveAsActivity(false);
        message.setTargetObjectId(currentUser.Id);
        message.setSubject(System.Label.VTR3_VerificationRequired);
        message.plainTextBody = System.Label.VTR3_VerificationCode + ' ' + verificationCode;
        message.setCharset('UTF-8');
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>{
                message
        };
        addCodeToSessionCache(deviceUniqueCode, verificationCode);
        addDateToSessionCache(deviceUniqueCode, System.now());
        System.debug('Code sended to user id =' + currentUser.Id);
        Messaging.sendEmail(messages);
    }
    public static LoginFlowResponse setCurrentStatuses() {
        System.debug('setting current user ' + VT_R5_MobileAuth.parsedParams.userID);
        if (isActivationProcess) loginFlowResponse = new LoginFlowResponse();
        setCurrentUser();
        if (isActivationProcess) {
            loginFlowResponse.isVerificationCodeAccepted = true;
        } else {
            loginFlowResponse.isVerificationCodeAccepted = !needVerificationCode(VT_R5_MobileAuth.parsedParams.deviceUniqueCode);
        }
        loginFlowResponse.isPrivacyNoticesAccepted = !needPrivacyNotices();
        loginFlowResponse.isAuthorizationAccepted = !needAuthorization();
        loginFlowResponse.isInitialProfileSetupAccepted = !needInitialProfileSetup();
        loginFlowResponse.currentStage =
                isActivationProcess
                        ? loginFlowResponse.isPrivacyNoticesAccepted
                        ? loginFlowResponse.isAuthorizationAccepted
                                ? SETUP_PASSWORD_STAGE
                                : AUTHORIZATION_STAGE
                        : PRIVACY_NOTICES_STAGE
                        : loginFlowResponse.isVerificationCodeAccepted
                        ? loginFlowResponse.isPrivacyNoticesAccepted
                                ? loginFlowResponse.isAuthorizationAccepted
                                        ? loginFlowResponse.isInitialProfileSetupAccepted
                                                ? SUCCESS_STAGE
                                                : INITIAL_PROFILE_SETUP_STAGE
                                        : AUTHORIZATION_STAGE
                                : PRIVACY_NOTICES_STAGE
                        : VERIFICATION_CODE_STAGE;
        System.debug('patientLoginFlowResponse.currentStage ' + loginFlowResponse.currentStage);
        return loginFlowResponse;
    }
    /**
     * @description SELECT and set this.currentUser with User with given Id
     */
    private static void setCurrentUser() {
        currentUser = [
                SELECT Id,
                        LanguageLocaleKey,
                        Username,
                        Profile.Name,
                        HIPAA_Accepted__c,
                        VTD1_Privacy_Notice_Last_Accepted__c,
                        First_Log_In__c,
                        ContactId,
                        Contact.Account.Candidate_Patient__r.Study__c,
                        VTR3_Verified_Mobile_Devices__c,
                        Email,
                        VTD1_Filled_Out_Patient_Profile__c,

                        Contact.VTD1_Clinical_Study_Membership__r.VTD2_Study_Phone_Number__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTD1_Study_Phone_Number__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_DefaultIRBLanguage__c,

                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__c,
                        Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eDiaryTool__c,
                        VTR5_Create_Subject_in_eCOA__c,
                        VTR4_Is_Activated__c,
                        CreatedDate
                FROM User
                WHERE Id = :VT_R5_MobileAuth.parsedParams.userID
        ];
        if (currentUser.VTR3_Verified_Mobile_Devices__c == null) {
            currentUser.VTR3_Verified_Mobile_Devices__c = '';
        }
        System.debug('set user' + currentUser);
    }
    /**
     * @description 2FA code is required once per the device. 2FA code is not required during activation and for autotest-users.
     *
     * @param deviceUniqueCode mobile identifier ex. FC9548A4-B2CB-4DF0-8234-DCDB67BA468C
     *
     * @return TRUE if required, otherwise FALSE
     */
    private static Boolean needVerificationCode(String deviceUniqueCode) {
        if (currentUser.First_Log_In__c == null || !VT_R5_MobileAuth.AVAILABLE_PROFILES.contains(currentUser.Profile.Name)) {
            return false;
        }
        if (currentUser.VTR3_Verified_Mobile_Devices__c.indexOf('AUTOTEST') == -1) {
            if ((deviceUniqueCode != null && currentUser.VTR3_Verified_Mobile_Devices__c.indexOf(deviceUniqueCode) == -1)) {
                return true;
            }
        }
        return false;
    }
    private static Boolean needInitialProfileSetup() {
        return VT_R4_ConstantsHelper_Profiles.HIPAA_PROFILES.contains(currentUser.Profile.Name) && !currentUser.VTD1_Filled_Out_Patient_Profile__c;
    }
    private static Boolean needPrivacyNotices() {
        return currentUser.VTD1_Privacy_Notice_Last_Accepted__c == null || currentUser.VTD1_Privacy_Notice_Last_Accepted__c.year() < Date.today().year() ;
    }
    private static Boolean needAuthorization() {
        return VT_R4_ConstantsHelper_Profiles.HIPAA_PROFILES.contains(currentUser.Profile.Name) && !currentUser.HIPAA_Accepted__c ;
    }
    /**
     * @description Class-holder for The login Flow response
     */
    public class LoginFlowResponse {
        public String newPassword;
        public String currentStage;
        public String username;
        public List<String> errors = new List<String>();
        public String serverMessage = '';
        public String message;
        public Boolean isVerificationCodeAccepted;
        public Boolean isPrivacyNoticesAccepted;
        public Boolean isAuthorizationAccepted;
        public Boolean isInitialProfileSetupAccepted;
        public LoginFlowArticle privacyNoticesArticle;
        public LoginFlowArticle authorizationArticle;
        public InitialProfileData initialProfileData;
        public String studyPhoneNumber;
        public Boolean actionFailed;


        public void setArticle(LoginFlowArticle article, String stage) {
            if (stage.equals(PRIVACY_NOTICES_STAGE)) {
                privacyNoticesArticle = article;
                return;
            }
            if (stage.equals(AUTHORIZATION_STAGE)) {
                authorizationArticle = article;
            }
        }
    }
    public class LoginFlowArticle {
        private String id;
        private String title;
        private String content;
        private String summary;
        public LoginFlowArticle() {
            this.id = '';
            this.title = '';
            this.content = '';
            this.summary = '';
        }

        public LoginFlowArticle(Knowledge__kav kav) {
            this.id = kav.Id;
            this.title = kav.Title;
            this.content = kav.VTD1_Content__c;
            this.summary = kav.Summary == null ? '' : kav.Summary;
        }
    }
}