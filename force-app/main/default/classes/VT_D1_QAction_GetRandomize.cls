/**
 * Created by Leonid Bartenev
 *
 * Integration action. Send request to get Randomization Id and put this Id in the case
 *
 * Example of usage:
 *
 * VT_D1_QAction_GetRandomize getRandomizeAction = new VT_D1_QAction_GetRandomize('someCaseIdHere');
 * getRandomizeAction.execute();
 *
 * For async use:
 * getRandomizeAction.executeFuture();
 */

public class VT_D1_QAction_GetRandomize extends VT_D1_AbstractAction {

    private Id caseId;
    private Id initiatingUserId;
    private String initiatingUserProfileName = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
    public transient VT_D1_SubjectIntegration.RandomizeSubjectResponseMessage subject;

    public VT_D1_QAction_GetRandomize(Id caseId) {
        this.caseId = caseId;
        this.initiatingUserId = UserInfo.getUserId();
    }

    //action logic implementation:
    public override void execute() {
        if (caseId == null) {
            return;
        }
            Case cs = [
                SELECT
                        Id,
                        VTD1_Primary_PG__c, Status,
                            VTD1_Randomization_Status__c,
                            //VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c,

                        	VTD1_Study__c,
                        	VTD1_Study__r.VTR5_IRTSystem__c,

                            VTD1_Study__r.VTD1_Study_Admin__c,
                       		VTD1_Virtual_Site__r.VTR3_Study_Geography__r.VTR5_Primary_VTSL__c,
                            VTD1_CreatedInCenduitCSM__c,
                            VTD1_Patient_User__r.Name,
                            VTD1_Subject_ID__c,
                            VTR2_IsAvailableForRandomize__c
                    FROM Case
                    WHERE Id = :caseId
            ];
        // SH-14372 - skip CSM integration for Signant Studies. Vlad Tyazhov
        if (cs.VTD1_Study__r.VTR5_IRTSystem__c == 'Signant') {
            return;
        }

        VT_D1_SubjectIntegration.RandomizeSubjectResponse subjectResponse = VT_D1_SubjectIntegration.randomizeSubject(caseId);
        System.debug('subjectResponse ' + subjectResponse);

            this.subject = subjectResponse.subject;
            HttpResponse response = subjectResponse.httpResponse;
            if (response == null) {
                System.debug('response == null');
                //time out, action will not be added to queue:
                setStatus(STATUS_FAILED);
                setMessage(subjectResponse.log.VTD1_ErrorMessage__c);
                cs.VTD1_Randomization_Status__c = 'Randomization Error';
                cs.VTD1_Reattempts_of_Randomization__c = 0;

            } else if (response.getStatusCode() >= 500 && response.getStatusCode() < 600) {
                System.debug('response.getStatusCode() >= 500 && response.getStatusCode() < 600');

                //internal server error, action will be added to queue:
                setStatus(STATUS_FAILED);
                setMessage('Server return status code: ' + response.getStatusCode() + ' ' + response.getStatus());
                addToQueue();
                cs.VTD1_Reattempts_of_Randomization__c = getAttemptNumber();
                cs.VTD1_Randomization_Status__c = 'Randomization Error';
            } else if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
                //success
                setStatus(STATUS_SUCCESS);
                if (cs.VTD1_CreatedInCenduitCSM__c && (cs.Status == 'Screened' || cs.Status == 'Washout/Run-In') && cs.VTD1_Randomization_Status__c != 'Randomized' && this.subject != null) {
                    String randomizationId = this.subject.randomizationId;
                    if (String.isNotBlank(randomizationId)) {
                        cs.VTD1_Randomized_ID__c = randomizationId.mid(0, 15);
                        cs.VTD1_Randomized_Date1__c = Date.today();
                        cs.Status = 'Active/Randomized';
                        cs.VTD1_Randomization_Status__c = 'Randomized';
                        cs.VTD1_Reattempts_of_Randomization__c = 0;
                    }
                }
            } else {
                System.debug('else');
                System.debug('subjectResponse ' + subjectResponse);
                //other errors, action will not be added to queue
                setStatus(STATUS_FAILED);
                setMessage('Server return: ' + response.getStatusCode() + ' ' + response.getStatus() + ', Body: ' + response.getBody());
                String errorMessage = getMessage();
                cs.VTD1_Randomization_Status__c = 'Randomization Error';
                cs.VTD1_Reattempts_of_Randomization__c = 0;

                //generate Task:
                Set<Id> ownerIds = new Set<Id>{
                        initiatingUserId
                };
                createTasksTroughTnForCase(
                        'T563',
                        new List<String>{cs.Id},
                        new List<Id> (ownerIds),
                        errorMessage);

                //generate notification:
                if (initiatingUserProfileName == 'Site Coordinator') {
                    VT_D2_TNCatalogNotifications.generateNotifications(
                            new List<String>{'N545'},
                            new List<Id>{cs.Id},
                            null,
                            null,
                            new Map<String, List<Id>>{'N545' + cs.Id => new List<Id>(ownerIds)}
                    );
                } else {
                    VT_D2_TNCatalogNotifications.generateNotifications(
                            new List<String>{'N542'},
                            new List<Id>{cs.Id},
                            null,
                            null,
                            new Map<String, List<Id>>{'N542' + cs.Id => new List<Id>(ownerIds)}
                    );
                }
            }
            cs.VTD1_Randomize_Log__c = subjectResponse.log.Id;

            //after all attempts, create tasks:
            if (isLastAttempt() && getStatus() != STATUS_SUCCESS) {
                Set<Id> ownerIds = new Set<Id>{
                        initiatingUserId,
                        cs.VTD1_Study__r.VTD1_Study_Admin__c
                };
                String errorMessage = getMessage();

                //generate tasks:
                createTasksTroughTnForCase(
                        'T562',
                        new List<String>{cs.Id, cs.Id, cs.Id},
                        new List<Id>(ownerIds),
                        errorMessage
                );
                createTasksTroughTnForCase(
                        'T606',
                        new List<String>{cs.Id, cs.Id, cs.Id},
                        new List<Id>{cs.VTD1_Virtual_Site__r.VTR3_Study_Geography__r.VTR5_Primary_VTSL__c},
                        errorMessage
                );

                //generate Notifications for Randomization Initiating User:
                if (initiatingUserProfileName == 'Site Coordinator') {
                    VT_D2_TNCatalogNotifications.generateNotifications(
                            new List<String>{'N544'},
                            new List<Id>{cs.Id},
                            null,
                            null,
                            new Map<String, List<Id>>{'N544' + cs.Id => new List<Id>(ownerIds)}
                    );
                } else {
                    VT_D2_TNCatalogNotifications.generateNotifications(
                            new List<String>{'N541'},
                            new List<Id>{cs.Id},
                            null,
                            null,
                            new Map<String, List<Id>>{'N541' + cs.Id => new List<Id>(ownerIds)}
                    );
                }
            }
            update cs;

    }


    public override Type getType() {
        return VT_D1_QAction_GetRandomize.class;
    }

}