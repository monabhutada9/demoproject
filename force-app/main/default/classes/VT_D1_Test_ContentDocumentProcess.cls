/**
 * @author Ali
 * @date: 06-Sep-18
 * @description: Test for Content Document Process Trigger
 */

@isTest
private class VT_D1_Test_ContentDocumentProcess {

    static testMethod void testDocument() {
        createDocument();

        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        System.assertEquals(documents.size(), 1);

        VTD1_Document__c doc = new VTD1_Document__c();

        insert doc;

        ContentDocumentLink link = new ContentDocumentLink();
        link.LinkedEntityId = doc.Id;
        link.ContentDocumentId = documents[0].id;
        link.ShareType = 'V';
        link.Visibility = 'AllUsers';
        insert link;

        Test.startTest();
        doc.VTD1_Status__c = 'Rejected';
        doc.VTD1_Does_file_needs_deletion__c = 'Yes';
        doc.VTD1_Deletion_Reason__c = 'Not Relevant';
        update doc;
        delete documents[0];
        Test.stopTest();

        documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        System.assertEquals(documents.size(), 0);
    }

    private static void createDocument() {
        ContentVersion contentVersion_1 = new ContentVersion(
                Title = 'Penguins',
                PathOnClient = 'Penguins.jpg',
                VersionData = Blob.valueOf('Test Content'),
                IsMajorVersion = true
        );
        insert contentVersion_1;
    }
}