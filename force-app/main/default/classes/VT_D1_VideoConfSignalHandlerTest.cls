/**
 * @author Vlad Tyazhov
 * @created 6-Sep-2020
 * @see VT_D1_VideoConfSignalHandlerTest
 */
@IsTest
public with sharing class VT_D1_VideoConfSignalHandlerTest {
    public static void testVideoEvent(){
        Test.startTest();
        VT_D1_VideoConfSignalHandler.sendSignal(new Set<Id>(), false);
        Test.stopTest();
    }
}