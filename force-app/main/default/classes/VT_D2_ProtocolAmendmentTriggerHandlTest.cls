/**
 * Created by User on 19/05/10.
 */


@IsTest
public without sharing class VT_D2_ProtocolAmendmentTriggerHandlTest {

    public static void doTest(){
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];

        VTD1_Document__c doc = (VTD1_Document__c) new DomainObjects.VTD1_Document_t().persist();
        VTD1_Regulatory_Document__c regulatoryDocumentPAsignature = new VTD1_Regulatory_Document__c(
                Name = 'TestRegularDocument',
                VTD1_Document_Type__c = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_SIGNATURE_PAGE,
                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI'
        );
        insert regulatoryDocumentPAsignature;
        VTD1_Regulatory_Document__c regulatoryDocumentPA = new VTD1_Regulatory_Document__c(
                Name = 'TestRegularDocument',
                VTD1_Document_Type__c = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_PROTOCOL_AMENDMENT,
                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI'
        );
        insert regulatoryDocumentPA;
        VTD1_Regulatory_Binder__c regulatoryBinderPAsignature = new VTD1_Regulatory_Binder__c(
                VTD1_Regulatory_Document__c = regulatoryDocumentPAsignature.Id,
                VTD1_Care_Plan_Template__c = study.Id
        );
        insert regulatoryBinderPAsignature;
        VTD1_Regulatory_Binder__c regulatoryBinderPA = new VTD1_Regulatory_Binder__c(
                VTD1_Regulatory_Document__c = regulatoryDocumentPA.Id,
                VTD1_Care_Plan_Template__c = study.Id
        );
        insert regulatoryBinderPA;
        VTD1_Protocol_Amendment__c pa = new VTD1_Protocol_Amendment__c(VTD1_Study_ID__c = study.Id) ;
        insert pa;
        pa.Name = 'Test';
        pa.VTD1_Protocol_Amendment_Document_Link__c = doc.Id;
        update pa;
        delete pa;
        Test.stopTest();
    }

    public static void testSignaturePagePH(){
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        Study_Team_Member__c member = [SELECT Id FROM Study_Team_Member__c LIMIT 1];
        VTD1_Document__c doc = (VTD1_Document__c) new DomainObjects.VTD1_Document_t().persist();


        VTD1_Regulatory_Document__c documentPAsignature = new VTD1_Regulatory_Document__c(
                VTD1_Document_Type__c = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_SIGNATURE_PAGE,
                Name = 'Test Reg Doc',
                Level__c = 'Site',
                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI'
        );
        insert documentPAsignature;
        VTD1_Regulatory_Document__c documentPA = new VTD1_Regulatory_Document__c(
                VTD1_Document_Type__c = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_PA_PROTOCOL_AMENDMENT,
                Name = 'Test Reg Doc',
                Level__c = 'Site',
                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI'
        );
        insert documentPA;
        VTD1_Regulatory_Binder__c binderPAsignature = new VTD1_Regulatory_Binder__c(
                VTD1_Care_Plan_Template__c = study.Id,
                VTD1_Regulatory_Document__c = documentPAsignature.Id
        );
        insert binderPAsignature;
        VTD1_Regulatory_Binder__c binderPA = new VTD1_Regulatory_Binder__c(
                VTD1_Care_Plan_Template__c = study.Id,
                VTD1_Regulatory_Document__c = documentPA.Id
        );
        insert binderPA;
        Virtual_Site__c virtualSite = new Virtual_Site__c(

                VTD1_Study_Site_Number__c = '12345',

                VTD1_Study__c = study.Id,
                VTD1_Study_Team_Member__c = member.Id
        );
        insert virtualSite;
        List <Virtual_Site__c> sites = [SELECT Id, Name, VTD1_Study__r.Name FROM Virtual_Site__c];
        System.debug('sites* = ' + sites);



        VTD1_Protocol_Amendment__c amendment = new VTD1_Protocol_Amendment__c(
                VTD1_PA_Document_is_Signed__c = false,
                VTD1_Study_ID__c = study.Id,
                VTD1_Protocol_Amendment_Document_Link__c = doc.Id);
        insert amendment;
        Test.stopTest();
        List <VTD1_Document__c> documents = [
                SELECT Id, VTD1_Study__c, VTD1_Regulatory_Binder__c, VTD1_Site__c, VTD1_Protocol_Amendment__c, RecordType.Name, VTD1_Name__c, VTD1_Document_Type__c
                FROM VTD1_Document__c
                WHERE VTD1_Study__c = :study.Id];
        System.debug('documents = ' + documents);
        System.assert(documents.size() > 0);
    }
}