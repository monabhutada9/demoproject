/**
 * @author: Rishat Shamratov
 * @date: 22.07.2020
 * @description: Batch class for populate VTR5_ActivationEmailSent field on User object
 */

public class VT_R5_PIandSCRusersActivEmailSentBatch implements Database.Batchable<SObject> {
    public Iterable<SObject> start(Database.BatchableContext BC) {
        return (Iterable<SObject>) new VT_Stubber.ResultStub(
                'VT_R5_PIandSCRusersActivEmailSentBatch.getQueryLocator',
                Database.getQueryLocator([
                        SELECT Id, Country, VTD1_Profile_Name__c, VTR5_ActivationEmailSent__c
                        FROM User WHERE IsActive = TRUE
                            AND (VTD1_Profile_Name__c = 'Primary Investigator' OR VTD1_Profile_Name__c = 'Site Coordinator')
                            AND (VTR5_ActivationEmailSent__c = FALSE OR Country = NULL)
                ])
        ).getResult();
    }

    public void execute(Database.BatchableContext BC, List<User> users) {
        for (User u : users) {
            if (u.VTR5_ActivationEmailSent__c == false) {
                u.VTR5_ActivationEmailSent__c = true;
            }
            if (u.Country == '') u.Country = 'United States';
        }
        Database.update(users, false);
    }

    public void finish(Database.BatchableContext BC) {
        System.debug('VT_R5_PIandSCRusersActivEmailSentBatch::finish');
    }
}