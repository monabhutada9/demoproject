@IsTest
private class VTR2_SCRMyPatientTreatmentArmTest {

    @TestSetup
    static void testSetup() {
        Case cas = new Case();
        insert cas;
        List<VTR2_Treatment_Arm__c> treatmentArms = new List<VTR2_Treatment_Arm__c>{
                new VTR2_Treatment_Arm__c(
                        Name = 'TestName',
                        VTR2_Treament_Arm_ID__c = 'Test',
                        VTR2_Case__c = cas.Id,
                        VTR2_Start_Date__c = Date.today().addDays(-7),
                        VTR2_End_Date__c = Date.today().addDays(7)
                ),
                new VTR2_Treatment_Arm__c(
                        Name = 'TestName',
                        VTR2_Treament_Arm_ID__c = 'Test',
                        VTR2_Case__c = cas.Id,
                        VTR2_Start_Date__c = Date.today().addDays(-5),
                        VTR2_End_Date__c = Date.today().addDays(5)
                )
        };
        insert treatmentArms;
    }

    @IsTest
    static void getTreatmentArmsTest_Positive() {
        Case cas = [SELECT Id FROM Case LIMIT 1];

        Test.startTest();
            List<VTR2_Treatment_Arm__c> treatmentArms = VTR2_SCRMyPatientTreatmentArm.getTreatmentArms(cas.Id);
        Test.stopTest();

        System.assertEquals(2, treatmentArms.size(), 'Incorrect size of returned list of records');
        for (VTR2_Treatment_Arm__c arm : treatmentArms) {
            System.assertEquals(cas.Id, arm.VTR2_Case__c, 'Incorrect case Id');
        }

    }
}