/**

 * @author Nikolay Kibalov
 * @date 13.10.2020
 * Modified By Sonam 29.10.2020 for User Story SH - 17965



 */
@IsTest
public with sharing class VT_R5_SSTMauditHandlerTest {

    @TestSetup
    static void setup() {




        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setName('UserAudit1');

        DomainObjects.StudyTeamMember_t stmPI = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(new DomainObjects.User_t()
                        .addContact(new DomainObjects.Contact_t() .setLastName('4test21300'))
                        .setProfile('Primary Investigator'))
                .addVirtualSite(new DomainObjects.VirtualSite_t()
                        .addStudy(study)
                        .setStudySiteNumber('12345'))
                .setRecordTypeByName('PI');
        DomainObjects.StudyTeamMember_t stmSCR = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(new DomainObjects.User_t()
                        .addContact(new DomainObjects.Contact_t() .setLastName('3Ltests231'))
                        .setProfile('Site Coordinator'))
                .setRecordTypeByName('SCR');



        // start SH - 17965 Add CRA for Audit Object
        DomainObjects.StudyTeamMember_t stmCRA_t = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_Profiles.CRA_PROFILE_NAME));

        Study_Team_Member__c remoteCra = (Study_Team_Member__c) stmCRA_t.persist();
        // End SH - 17965




        DomainObjects.StudySiteTeamMember_t sstm = new DomainObjects.StudySiteTeamMember_t()
                .setAssociatedPI2(stmPI)
                .setAssociatedScr(stmSCR);
        sstm.persist();
    }

    @IsTest
    public static void test2DeleteSSTMs() {
        List<Study_Site_Team_Member__c> SSTMsToDelete = [
                SELECT Id
                FROM Study_Site_Team_Member__c
                WHERE VTR2_Associated_SCr__c != NULL
                LIMIT 1
        ];
        System.assertEquals(1, SSTMsToDelete.size());
        delete SSTMsToDelete;
        List<Study_Site_Team_Member__c> SSTMsAfterDelete = [
                SELECT Id
                FROM Study_Site_Team_Member__c
                WHERE VTR2_Associated_SCr__c != NULL
                LIMIT 1
        ];
        System.assertEquals(0, SSTMsAfterDelete.size());
    }

    @IsTest
    private static void testDeletePgSstm() {
        HealthCloudGA__CarePlanTemplate__c study = [
                SELECT Id,
                        VTD1_Remote_CRA__c
                FROM HealthCloudGA__CarePlanTemplate__c
                LIMIT 1
        ];
        Study_Team_Member__c primaryPI = [
                SELECT VTD1_VirtualSite__c
            //START RAJESH SH-17440
                        //VTR2_Remote_CRA__c,
                        //VTR2_Onsite_CRA__c
            //END:Sh-17440
                FROM Study_Team_Member__c
                WHERE VTD1_Type__c = 'Primary Investigator'
                LIMIT 1
        ];
        Test.startTest();

        DomainObjects.Study_Site_Team_Member_t sstmPg_t = new DomainObjects.Study_Site_Team_Member_t()
                .addVTD1_Associated_PG_t(new DomainObjects.StudyTeamMember_t()
                        .addUser(new DomainObjects.User_t()
                                .setProfile('Patient Guide'))
                        .setStudyId(study.Id))
                .setVTD1_Associated_PI_t(primaryPI.Id);
        sstmPg_t.persist();
        Test.stopTest();
        List<Study_Site_Team_Member__c> sstmPgs = [
                SELECT Id,
                        VTD1_Associated_PI__c
                FROM Study_Site_Team_Member__c
                WHERE VTD1_Associated_PI__c = :primaryPI.Id
        ];
        System.assertEquals(1, sstmPgs.size());
//        Study_Site_Team_Member__c sstmPg = (Study_Site_Team_Member__c) sstmPg_t.toObject();
        delete sstmPgs;
        List<Study_Site_Team_Member__c> sstmPgsAfterDelete = [
                SELECT Id,
                        VTD1_Associated_PI__c
                FROM Study_Site_Team_Member__c
                WHERE VTD1_Associated_PI__c = :primaryPI.Id
        ];
        System.assertEquals(0, sstmPgsAfterDelete.size());

    }




    @IsTest
    private static void testDeleteSubISstm() {
        HealthCloudGA__CarePlanTemplate__c study = [
                SELECT Id,
                        VTD1_Remote_CRA__c,
                        VTD1_Name__c
                FROM HealthCloudGA__CarePlanTemplate__c
                LIMIT 1
        ];
        System.debug('-> ' + study);
        Study_Team_Member__c primaryPI = [
                SELECT VTD1_VirtualSite__c
            //START RAJESH SH-17440
                        //VTR2_Remote_CRA__c,
                        //VTR2_Onsite_CRA__c
            //END:SH-17440
                FROM Study_Team_Member__c
                WHERE VTD1_Type__c = 'Primary Investigator'
                LIMIT 1
        ];
        Test.startTest();
        DomainObjects.Study_Site_Team_Member_t sstmSubI_t = new DomainObjects.Study_Site_Team_Member_t()
                .addVTR2_Associated_SubI_t(new DomainObjects.StudyTeamMember_t()
                        .addUser(new DomainObjects.User_t()
                                .setProfile('Primary Investigator')
                                .addContact(new DomainObjects.Contact_t() .setLastName('test21312')))
                        .setStudyId(study.Id))
                .setVTR2_Associated_PI3_t(primaryPI.Id);
        sstmSubI_t.persist();
        Test.stopTest();
        Study_Site_Team_Member__c sstmSubI = [
                SELECT Id,
                        VTR2_Associated_PI3__c,
                        VTR2_Associated_SubI__r.User__r.LastName
                FROM Study_Site_Team_Member__c
                WHERE VTR2_Associated_PI3__c = :primaryPI.Id
                AND VTR2_Associated_SubI__r.Study__c = :study.Id
        ];
        System.debug('sstmSubI -->' + sstmSubI);
        System.assertEquals('Scott', sstmSubI.VTR2_Associated_SubI__r.User__r.LastName);
//        Study_Site_Team_Member__c sstmSubI = (Study_Site_Team_Member__c) sstmSubI_t.toObject();
        delete sstmSubI;
        List<Study_Site_Team_Member__c> sstmSubIafterDelete = [
                SELECT Id
                FROM Study_Site_Team_Member__c
                WHERE VTR2_Associated_PI3__c = :primaryPI.Id
                AND VTR2_Associated_SubI__r.Study__c = :study.Id
        ];
        System.assertEquals(0, sstmSubIafterDelete.size());

    }

    /***********************************************************************************************
    * @description  This test method cover the scenario if SSTM of CRA type created then SSAA uaudit 
                    record will create and after delete existing SSAA field get updated
    * @group        SH-17965 Sonam
    * @param        NA
    * @return       void
    */
    @IsTest
    private static void testInsertAndDeleteCRASSTM() {
        HealthCloudGA__CarePlanTemplate__c study = [
                SELECT Id,
                        VTD1_Remote_CRA__c,
                        VTD1_Name__c
                FROM HealthCloudGA__CarePlanTemplate__c
                LIMIT 1
        ];
         
        Study_Team_Member__c craSTM = [

                SELECT VTD1_VirtualSite__c
            //START RAJESH SH-17440
                        //VTR2_Remote_CRA__c,
                        //VTR2_Onsite_CRA__c
            //END:SH_17440

                FROM Study_Team_Member__c
                WHERE VTD1_Type__c = 'CRA'
                LIMIT 1
        ];

        Virtual_Site__c virtualSite = [ SELECT Id, VTD1_Study__c, VTD1_PI_User__c 
                                                FROM Virtual_Site__c 
                                                LIMIT 1];



        Test.startTest();
        List<Study_Site_Team_Member__c> listOfSSTM = new List<Study_Site_Team_Member__c>();
        for(Integer i = 0; i < 5; i++) {
                Study_Site_Team_Member__c siteTeam = VT_D1_TestUtils.createStudySiteTeamMember(virtualSite.Id, craSTM.Id);
                listOfSSTM.add(siteTeam);
        }

        insert listOfSSTM;
        Test.stopTest();
        
        // one SSAA record will create for SSTM
        List<VTR5_SiteStudyAccessAudit__c> SSAA = [Select Id, 
                                                        VTR5_StudyTeamMember__c, 
                                                        VTR5_StudyName__c, 
                                                        VTR5_SiteName__c, 
                                                        User_Audit__c 
                                                     FROM VTR5_SiteStudyAccessAudit__c
                                                     WHERE VTR5_StudyTeamMember__c =:craSTM.Id
                                                     AND VTR5_SiteName__c!= null
                                                     ];
        System.assertEquals(5, SSAA.size());
        System.assertEquals(craSTM.Id, SSAA[0].VTR5_StudyTeamMember__c, 'Study team member has added in SSAA');
        System.assertEquals(study.Id, SSAA[0].VTR5_StudyName__c, 'Study has added in SSAA');
        System.assertEquals(virtualSite.Id, SSAA[0].VTR5_SiteName__c, 'virtual site has added in SSAA');
        System.assertNotEquals(null, SSAA[0].User_Audit__c, 'User Audit record has created');

        // Now delete the created SSTM so that SSAA will be updated
        delete listOfSSTM;

        List<VTR5_SiteStudyAccessAudit__c> SSAAAfterDelete = [Select Id, 
                                                        VTR5_StudyTeamMember__c, 
                                                        VTR5_SiteAccessStatus__c, 
                                                        VTR5_SiteDeactivatedBy__c, 
                                                        VTR5_SiteDeactivatedDate__c 
                                                     FROM VTR5_SiteStudyAccessAudit__c
                                                     WHERE VTR5_StudyTeamMember__c =:craSTM.Id
                                                     AND VTR5_SiteName__c!= null
                                                     ];
        System.assertEquals(false, SSAAAfterDelete[0].VTR5_SiteAccessStatus__c, 'Site access has set false for SSAA');
        System.assertNotEquals(null, SSAAAfterDelete[0].VTR5_SiteDeactivatedBy__c, 'Site has deactivated');
        System.assertNotEquals(null, SSAAAfterDelete[0].VTR5_SiteDeactivatedDate__c, 'Site has deactivated at current time');

    }



}