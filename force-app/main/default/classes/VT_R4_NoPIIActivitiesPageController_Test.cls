/**
 * @author: Dmitry Yartsev
 * @date: 10.02.2020
 * @description: Unit test for VT_R4_NoPIIActivitiesPageController
 * @see: VT_R4_NoPIIActivitiesPageController
 */
@IsTest
private class VT_R4_NoPIIActivitiesPageController_Test {
    private static final String TASK_SUBJECT = 'VT_R4_NoPIIActivitiesPageController_Test_Task';
    private static final String EVENT_SUBJECT = 'VT_R4_NoPIIActivitiesPageController_Test_Event';
    private static final Set<String> NO_PII_PROFILES = new Set<String>{
            VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME,
            VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME
    };
    @TestSetup
    static void setup() {


        Test.startTest();
        {
            VT_D1_TestUtils.prepareStudy(1);
        }
        Test.stopTest();
    }
    @IsTest
    static void testGetPtCgProfileIds() {
        System.assert(!VT_R4_NoPIIActivitiesPageController.getPtCgProfileIds().isEmpty());
    }
    @IsTest
    static void testWrongId() {
        System.assertEquals(null, VT_R4_NoPIIActivitiesPageController.getActivityRecord(UserInfo.getUserId()));
    }
    @IsTest
    static void testGetEvent() {
        prepareEvent([SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1].Id);
        Map<String, Object> eventObj = (Map<String, Object>) (
                (Map<String, Object>) JSON.deserializeUntyped(
                        VT_R4_NoPIIActivitiesPageController.getActivityRecord(
                                [SELECT Id FROM Event WHERE Subject = :EVENT_SUBJECT LIMIT 1].Id
                        )
                )
        ).get('record');
        System.assert(
                !NO_PII_PROFILES.contains(
                        String.valueOf(
                                ((Map<String, Object>) eventObj.get('Owner'))
                                        .get('Name')
                        )
                )
        );
    }
    @IsTest
    static void testTask() {
        prepareTask();
        String taskCompletedStatus = 'Completed';
        Map<String, Object> taskObj = (Map<String, Object>) (
                (Map<String, Object>) JSON.deserializeUntyped(
                        VT_R4_NoPIIActivitiesPageController.getActivityRecord(
                                [SELECT Id FROM Task WHERE Subject = :TASK_SUBJECT LIMIT 1].Id
                        )
                )
        ).get('record');
        System.assert(
                NO_PII_PROFILES.contains(
                        String.valueOf(
                                ((Map<String, Object>) taskObj.get('Owner'))
                                        .get('Name')
                        )
                )
        );
        taskObj.put('Status', taskCompletedStatus);
        System.assertEquals(
                taskCompletedStatus,
                String.valueOf(
                        VT_R4_NoPIIActivitiesPageController.updateActivity(
                                JSON.serialize(taskObj)
                        ).get('Status')
                )
        );
    }
    private static void prepareTask() {
        insert new Task(
                Subject = TASK_SUBJECT,
                RecordTypeId = VT_R4_ConstantsHelper_Tasks.RECORD_TYPE_ID_TASK_SIMPLETASK,
                OwnerId = (Id) new DomainObjects.User_t()
                        .addContact(
                                new DomainObjects.Contact_t()
                                        .setFirstName('Test' + Datetime.now().getTime())
                                        .setLastName('Patient' + Datetime.now().getTime())
                        )
                        .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME)
                        .persist()
                        .get('Id')
        );
    }
    @IsTest
    static void testAssignedToPlComboboxController(){
        HealthCloudGA__CarePlanTemplate__c study = [SELECT id,VTD1_Project_Lead__r.Id FROM HealthCloudGA__CarePlanTemplate__c];
        insert new Task(
                Subject = TASK_SUBJECT,
                RecordTypeId = VT_R4_ConstantsHelper_Tasks.RECORD_TYPE_ID_TASK_SIMPLETASK,
                HealthCloudGA__CarePlanTemplate__c= study.Id,
                OwnerId = study.VTD1_Project_Lead__r.Id
        );
        List<Task> t = [SELECT Id,HealthCloudGA__CarePlanTemplate__r.Id, OwnerId FROM Task WHERE OwnerId = :study.VTD1_Project_Lead__r.Id];
        String plsId = [SELECT HealthCloudGA__CarePlanTemplate__r.VTD1_Project_Lead__c FROM Task WHERE Id=:t[0].id LIMIT 1][0].HealthCloudGA__CarePlanTemplate__r.VTD1_Project_Lead__c;
        insert new HealthCloudGA__CarePlanTemplate__Share(
                UserOrGroupId = Id.valueOf(plsId),
                ParentId = study.Id,
                AccessLevel = 'Read'
        );
        System.runAs([SELECT Id FROM User WHERE Id =:plsId][0]){
            VT_R5_AssignedToPLComboboxController.getTaskOwnerUsersList(t[0].id);
        }
    }
    private static void prepareEvent(Id studyId) {
        Study_Team_Member__c stmPi = getStmByType(VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME, studyId);
        Id siteId = [SELECT Id FROM Virtual_Site__c WHERE VTD1_Study__c =: studyId].Id;
//        stmPi.VTR2_Remote_CRA__c = getStmByType(VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME, studyId).Id;
//        update stmPi;
//        Virtual_Site__c site = new Virtual_Site__c(
//                VTD1_Study__c = studyId,
//                VTD1_Study_Team_Member__c = stmPi.Id,
//                VTD1_Study_Site_Number__c = 'qweqwd'
//        );
//        insert site;

        Study_Team_Member__c stmCRA = getStmByType(VT_R4_ConstantsHelper_Profiles.CRA_PROFILE_NAME, studyId);
        Study_Site_Team_Member__c sstmCRA = new Study_Site_Team_Member__c();
        sstmCRA.VTR5_Associated_CRA__c = stmCRA.Id;
        sstmCRA.VTD1_SiteID__c = siteId ;
        insert sstmCRA;

        HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
        objVirtualShare.ParentId = studyId;
        objVirtualShare.UserOrGroupId = stmCRA.User__c;
        objVirtualShare.AccessLevel = 'Edit';
        insert objVirtualShare;


        VTD1_Monitoring_Visit__c mVisit = new VTD1_Monitoring_Visit__c(
                VTD1_Study__c = studyId,
                VTD1_Virtual_Site__c = siteId
                //VTR2_Remote_Onsite__c = 'Remote'

        );
        System.runAs(new User(Id = stmCRA.User__c)){
        insert mVisit;
        }
        insert new VTR2_Site_Address__c(
                VTR2_AddressLine1__c = 'VTR2_AddressLine1__c',
                VTR3_AddressLine_2__c = 'VTR3_AddressLine_2__c',
                VTR3_AddressLine_3__c = 'VTR3_AddressLine_3__c',
                VTR2_City__c = 'New York',
                VTR3_Country__c = 'US',
                VTR2_State__c = 'NY',
                VTR2_ZIP__c = '123421',
                VTR2_Primary__c = true,
                VTR2_VirtualSiteLookup__c = siteId
        );
        insert new Event(
                Subject = EVENT_SUBJECT,
                DurationInMinutes = 15,
                VTR2_Monitoring_Visit_del__c = mVisit.Id,
                ActivityDateTime = Datetime.now(),
                OwnerId = stmPi.User__c
        );
    }
    private static Study_Team_Member__c getStmByType(String type, Id studyId) {
        return [
                SELECT Id,
                        User__c,
                        Study_Team_Member__c
                FROM Study_Team_Member__c
                WHERE Study__c = :studyId
                AND VTD1_Type__c = :type
                LIMIT 1
        ];
    }
}