/**
 * Created by Slav on 19.02.2019.
 */

public without sharing class VT_R2_SMSResend implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable {

    private final String FAILURE_1 = VT_R2_McloudBroadcaster.STATUS_FAILURE_1;
    private VT_R2_McloudBroadcaster broadcaster;
    private Integer logCount = 0;

    /******************************************************************************************************************/
    /*** start()                                                  ***/
    /******************************************************************************************************************/
    public Database.QueryLocator start (Database.BatchableContext context) {
        broadcaster = new VT_R2_McloudBroadcaster();
        String queryString = 'SELECT ' +
                'VTR2_Contact__c, VTR2_NotificationId__c, VTR2_Notification__c, VTR2_PhoneNumber__c, VTR2_Status__c, VTR2_MessageText__c FROM VTR2_SendSMSLog__c ' +
                'WHERE VTR2_Status__c = :FAILURE_1 and VTR2_Status__c != \'Do not send\'';
        return Database.getQueryLocator(queryString);
    }

    /******************************************************************************************************************/
    /*** execute()                                                  ***/
    /******************************************************************************************************************/
    public void execute(Database.BatchableContext context, List<VTR2_SendSMSLog__c> logList) {
        if (!hasModifyLogAccess()) {
            System.debug('No access to change VTR2_SendSMSLog__c.VTR2_Status__c !');
            return;
        }
        try {
            for (VTR2_SendSMSLog__c log : logList) {
                broadcaster.sendMessage(new Map<String, Id>{ log.VTR2_PhoneNumber__c => log.VTR2_Contact__c }, log.VTR2_MessageText__c, log.VTR2_NotificationId__c, log.VTR2_Notification__c);
            }
        } catch (Exception e) {
            System.debug('==========> EXCEPTION: ' + e.getMessage() + ' ' + e.getStackTraceString());
            System.abortJob(context.getJobId());
        }

        logCount += logList.size();
    }

    /******************************************************************************************************************/
    /*** finish()                                                  ***/
    /******************************************************************************************************************/
    public void finish(Database.BatchableContext context) {
        System.debug('==========> Total SMS messages resent: ' + logCount);
    }

    /******************************************************************************************************************/
    /*** execute()                                                  ***/
    /******************************************************************************************************************/
    public void execute(System.SchedulableContext context) {
        // -------------------------------------- IMPORTANT NOTICE ---------------------------------------- //
        // DO NOT change the chunk size! It has to be 1 to avoid the 'Uncommitted work pending' exception  //
        // ------------------------------------------------------------------------------------------------ //
        Database.executeBatch(new VT_R2_SMSResend(), 1);
        System.debug('VT_R2_SMSResend.execute');
        schedule(context.getTriggerId());
    }

    // periodic schedule job for every 10 minutes
    public static void schedule(Id jobId) {
        System.debug('to abort = ' + jobId);
        if (jobId != null) {
            System.abortJob(jobId);
        }
        Datetime dt = System.now();
        Integer minutes = dt.minute();
        Integer hours = dt.hour();

        minutes += 10;

        if (minutes >= 60) {
            minutes -= 60;
            hours += 1;
        }

        if (hours >= 24) {
            hours -= 24;
        }

        VT_R2_SMSResend smsResend = new VT_R2_SMSResend();
        if (!Test.isRunningTest()) {
            System.schedule('SMSResendJob', '0 ' + minutes + ' ' + hours + ' * * ?', smsResend);
        }
    }

    private Boolean hasModifyLogAccess() {
        Map<String, Schema.SObjectField> fields = Schema.sObjectType.VTR2_SendSMSLog__c.fields.getMap();
        if (!fields.containsKey('VTR2_Status__c')) {
            return false;
        } else {
            return Schema.sObjectType.VTR2_SendSMSLog__c.fields.VTR2_Status__c.isUpdateable();
        }
    }
}