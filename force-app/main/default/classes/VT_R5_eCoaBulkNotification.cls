/**
* @author: Olya Baranova
* @date: 18-Aug-20
* @description:
**/

@RestResource(UrlMapping='/ecoa-notification')
global without sharing class VT_R5_eCoaBulkNotification {

    global class NotificationsInfo {
        public String subGuid;
        public String displayRuleValue;
        public String displayAlarmValue;
        public String ruleGuid;
    }

    private static Map<String, User> userMap = new Map<String, User>(); // by eCOA subject guid
    private static List<VTD1_NotificationC__c> subjectNotifications = new List<VTD1_NotificationC__c>();

    /**
     * Convert eCOA notification request to NotificationsInfo and create notification
     */
    @HttpPost
    global static void doPost() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        String requestBody = request.requestBody.toString();
        List<NotificationsInfo> eCoaNotificationsInfo = (List<NotificationsInfo>) JSON.deserialize(requestBody, List<NotificationsInfo>.class);
        try {
            getReceivers(eCoaNotificationsInfo);
            sendPatientCaregiverNotifications(eCoaNotificationsInfo);
            sendNotificationsEmails(subjectNotifications);
            sendPushNots(subjectNotifications);
            VT_R2_NotificationCHandler.sendToNotificationsService(subjectNotifications);
            response.statusCode = 200;
            response.responseBody = Blob.valueOf('{"message": "SUCCESS"}');
        } catch (Exception e) {
            response.statusCode = 500;
            response.responseBody = Blob.valueOf('{"message": "FAILED", "error": "' + e.getMessage() + '"}');
        }
    }

    /**
     * Get data from NotificationsInfo to create a notification for patient or caregiver
     * @param eCoaNotificationsInfo - data from eCoa request
     */
    private static void sendPatientCaregiverNotifications(List<NotificationsInfo> eCoaNotificationsInfo) {
        Map<Id, Id> patientCaregiverIds = VT_D1_PatientCaregiverBound.findCaregiversOfPatients(new List<Id>(new Map<Id, User>(new List<User>(userMap.values())).keySet()));
        for (NotificationsInfo n : eCoaNotificationsInfo) {
            if (userMap.containsKey(n.subGuid)) {
                subjectNotifications.add(new VTD1_NotificationC__c(
                        VTD1_Receivers__c = userMap.get(n.subGuid).Id,
                        Message__c = n.displayAlarmValue,
                        OwnerId = userMap.get(n.subGuid).Id,
                        Title__c = n.displayRuleValue,
                        Link_to_related_event_or_object__c = 'my-diary?banner=' + EncodingUtil.urlEncode(n.displayAlarmValue, 'UTF-8'),
                        HasDirectLink__c = true,
                        VTR3_Notification_Type__c = 'eCOA Notification',
                        VTR5_eCoa_ruleGuid__c = n.ruleGuid,
                        VTR4_CurrentPatientCase__c = userMap.get(n.subGuid).Contact.VTD1_Clinical_Study_Membership__c,
                        Type__c = 'General'
                ));
                if (patientCaregiverIds.containsKey(userMap.get(n.subGuid).Id)) {
                    subjectNotifications.add(new VTD1_NotificationC__c(
                            VTD1_Receivers__c = patientCaregiverIds.get(userMap.get(n.subGuid).Id),
                            Message__c = n.displayAlarmValue,
                            OwnerId = patientCaregiverIds.get(userMap.get(n.subGuid).Id),
                            Title__c = n.displayRuleValue,
                            Link_to_related_event_or_object__c = 'my-diary?banner=' + EncodingUtil.urlEncode(n.displayAlarmValue, 'UTF-8'),
                            HasDirectLink__c = true,
                            VTR3_Notification_Type__c = 'eCOA Notification',
                            VTR5_eCoa_ruleGuid__c = n.ruleGuid,
                            VTR4_CurrentPatientCase__c = userMap.get(n.subGuid).Contact.VTD1_Clinical_Study_Membership__c,
                            Type__c = 'General'
                    ));
                }
            }
        }
        VT_R2_NotificationCHandler.eCoaBulkAlerts = true;
        insert subjectNotifications;
    }

    /**
     * Send emails after notification
     * @param notifications
     */
    private static void sendNotificationsEmails(List<VTD1_NotificationC__c> notifications) {
        List<VT_R3_EmailsSender.ParamsHolder> emailsParamsHolder = new List<VT_R3_EmailsSender.ParamsHolder>();
        for (VTD1_NotificationC__c n : notifications) {
            VT_R3_EmailsSender.ParamsHolder ph = new VT_R3_EmailsSender.ParamsHolder(n.OwnerId, n.Id, 'VT_R5_eCOA_Notification');
            ph.subjectIsStatic = true;
            emailsParamsHolder.add(ph);
        }
        if (!emailsParamsHolder.isEmpty()) {
            VT_R3_EmailsSender.sendEmails(emailsParamsHolder);
        }
    }

    /**
     * Send Push after notification
     * @param notifications
     */
    private static void sendPushNots(List<VTD1_NotificationC__c> notifications) {
        try {
            VT_R5_MobilePushNotifications mobilePushNotifications = new VT_R5_MobilePushNotifications(notifications);
            mobilePushNotifications.execute();
        } catch (Exception e) {
            //Log this
            System.debug('eCoa Alerts push notifications error: ' + e.getMessage());
        }
    }

    private static void getReceivers(List<NotificationsInfo> eCoaNotificationsInfo) {
        Set<String> subGuids = new Set<String>();
        for (NotificationsInfo n : eCoaNotificationsInfo) {
            subGuids.add(n.subGuid);
        }
        for (User receiver : [
                SELECT Id, VTR5_eCOA_Guid__c, Contact.VTD1_Clinical_Study_Membership__c
                FROM User
                WHERE VTR5_eCOA_Guid__c IN :subGuids
        ]) {
            userMap.put(receiver.VTR5_eCOA_Guid__c, receiver);
        }

    }
}