/**
 * Created by User on 19/07/01.
 */
@isTest
public with sharing class VT_R2_RegulatoryBinderDocumentPrevTest {

    public static void getRegulatoryBinderDocumentWithDocIdTest(){
        DomainObjects.VTD1_Document_t document = new DomainObjects.VTD1_Document_t();
        document.persist();
        VT_R2_RegulatoryBinderDocumentPreview.RegulatoryBinderDocument regDoc = VT_R2_RegulatoryBinderDocumentPreview.getRegulatoryBinderDocument(document.Id);
        System.assertEquals('null, Version 0.0 (To Do)',regDoc.title);
        System.assertNotEquals(null,regDoc.document);
        System.assertNotEquals(null,regDoc.sections);

    }

    public static void getRegulatoryBinderDocumentWithoutDocIdTest(){
        VT_R2_RegulatoryBinderDocumentPreview.RegulatoryBinderDocument regDoc = VT_R2_RegulatoryBinderDocumentPreview.getRegulatoryBinderDocument(null);
        System.assertEquals('',regDoc.title);
        System.assertEquals(null,regDoc.document);
        System.assertNotEquals(null,regDoc.sections);
    }

    public static void testDocuments(){
        DomainObjects.VTD1_Document_t document = new DomainObjects.VTD1_Document_t();
        document.persist();
        List<VT_R2_AddNewPatientController.InputSelectOption> lstDocsStrings = VT_R2_RegulatoryBinderDocumentPreview.getSignatureTypeValues('VTD1_Document__c', 'VTR4_Signature_Type__c');
        System.assertNotEquals(null, lstDocsStrings);
        VT_R2_RegulatoryBinderDocumentPreview.setDocSignatureDate(document.Id, Date.today());
        VT_R2_RegulatoryBinderDocumentPreview.setDocSignatureType(document.Id, 'Wet');

        VTD1_Document__c doc = [SELECT Id, VTR4_Signature_Type__c,VTD1_Signature_Date__c FROM VTD1_Document__c WHERE Id = :document.Id];
        System.assertEquals(Date.today(), doc.VTD1_Signature_Date__c);
        System.assertEquals('Wet', doc.VTR4_Signature_Type__c);
    }
}