/**
 * Created by Danylo Belei on 14.05.2019.
 */

@IsTest
private class VT_D1_Batch_QueueActionsProcessingTest {

    @TestSetup
    static void testSetup(){
        Case c = new Case();
        insert c;
        VTD1_Queue_Action__c action = new VTD1_Queue_Action__c();
        action.VTD1_Execution_Time__c = Datetime.now().addDays(-1);
        action.VTD1_Action_Class__c = 'VT_D1_QAction_SendSubject';
        action.VTD1_Action_JSON__c = '{"repeatIntervalMin":1,"lastError":"Server return status code: 500 Server Error; Body: {Test}","currentAttempt":2,"attemptsCount":4,"caseId":"'+ c.Id + '"}';
        insert action;
    }

    @isTest
    static void testBatch() {
        Test.startTest();
        Database.executeBatch(new VT_D1_Batch_QueueActionsProcessing());
        Test.stopTest();
        System.assertEquals(true, [SELECT Id FROM VTD1_Queue_Action__c].size() == 0);
    }

    @isTest
    static void testRun() {
        Test.startTest();
        VT_D1_Batch_QueueActionsProcessing.run();
        Test.stopTest();
        System.assertEquals(true, ([SELECT Id FROM CronTrigger WHERE Id = :VT_D1_Batch_QueueActionsProcessing.cronId].size() > 0));
    }
}