/**
 * Created by Aleksandr Mazan on 21-Oct-20.
 */

public without sharing class VT_R5_ConfigurableNotificationController {

    public static final List<String> PROFILE_NAMES = new List<String>{'Caregiver', 'Patient'};
    public static final String WARNING = 'warning';
    public static final String SUCCESS = 'success';

    @AuraEnabled
    public static List<NotificationWrapper> getInitData() {
        String context = UserInfo.getUiThemeDisplayed();
        Id userId = UserInfo.getUserId();
        Id profileId = UserInfo.getProfileId();
        List<Id> profileIds = new List<Id>();
        for (Profile profile : [SELECT Id, Name from Profile WHERE Name IN :PROFILE_NAMES]) {
            profileIds.add(profile.Id);
        }
        if (!profileIds.contains(profileId)) {
            List<Id> studyIds = new List<Id>();
            for (Study_Team_Member__c stm : [SELECT Id, Name, User__c, Study__c FROM Study_Team_Member__c WHERE User__c = :userId]) {
                studyIds.add(stm.Study__c);
            }
            List<VTR5_StudyTeamNotification__c> studyNotifications = [
                    SELECT Id,
                            VTR5_NotificationActive__c,
                            VTR5_ActiveOfSubmitBanner__c,
                            VTR5_NotificationTitle__c,
                            VTR5_NotificationContent__c,
                            VTR5_BannerMessage__c,
                            VTR5_SubmitBannerMessage__c, (SELECT Id FROM Study_Notification_Actions__r WHERE OwnerId = :userId LIMIT 1)
                    FROM VTR5_StudyTeamNotification__c
                    WHERE VTR5_Study__c IN :studyIds
                    AND ((VTR5_NotificationActive__c = TRUE AND VTR5_EndDate__c > :Datetime.now() AND VTR5_StartDate__c < :Datetime.now()) OR (VTR5_ActiveOfSubmitBanner__c = TRUE AND VTR5_EndSubmitDate__c > :Datetime.now()))
                    ORDER BY VTR5_StartDate__c ASC
            ];

            List<NotificationWrapper> notifications = new List<NotificationWrapper>();
            for (VTR5_StudyTeamNotification__c notification : studyNotifications) {
                notifications.add(new NotificationWrapper(
                        context, notification
                ));
            }
            return notifications;
        } else return null;
    }

    @AuraEnabled
    public static void createNotificationAction(String notificationId) {
        VTR5_StudyNotificationAction__c notificationAction = new VTR5_StudyNotificationAction__c(VTR5_StudyTeamNotification__c = notificationId);
        insert notificationAction;
    }

    @AuraEnabled(Cacheable=true)
    public static String getBaseUrl() {
        return System.Url.getSalesforceBaseUrl().toExternalForm()
                + VT_D1_HelperClass.getSandboxPrefix();
    }

    public class NotificationWrapper {
        @AuraEnabled public String context;
        @AuraEnabled public Id studyNotificationId;
        @AuraEnabled public String modalTitle;
        @AuraEnabled public String modalContent;
        @AuraEnabled public String bannerMessage;
        @AuraEnabled public String submitBannerMessage;
        @AuraEnabled public String variant;
        @AuraEnabled public Boolean isShowModal;

        public NotificationWrapper(String context, VTR5_StudyTeamNotification__c notification) {
            if(notification.VTR5_NotificationActive__c) {
                this.variant = WARNING;
            } else if(notification.VTR5_ActiveOfSubmitBanner__c) {
                this.variant = SUCCESS;
            }
            this.context = context;
            this.studyNotificationId = notification.Id;
            this.modalTitle = notification.VTR5_NotificationTitle__c;
            this.modalContent = notification.VTR5_NotificationContent__c;
            this.bannerMessage = notification.VTR5_BannerMessage__c;
            this.submitBannerMessage = notification.VTR5_SubmitBannerMessage__c;
            this.isShowModal = notification.Study_Notification_Actions__r.size() == 0 && notification.VTR5_NotificationActive__c;
            this.variant = variant;
        }
    }
}