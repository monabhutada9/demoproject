public without sharing class VT_D1_ChooseTimeOnTaskContollerRemote {

    public class VisitInfo {
        @AuraEnabled public List<Visit_Member__c> visitMembers;
        @AuraEnabled public VTD1_Actual_Visit__c visit;
        @AuraEnabled public String availableDatesTimes;
        @AuraEnabled public Boolean isUnscheduledVisit = false;
        @AuraEnabled public List<DurationOption> durationOptions = new List<DurationOption>();

        public VisitInfo(VTD1_Actual_Visit__c mVisit) {
            visit = mVisit;
            if (visit.RecordTypeId == VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_ACTUAL_VISIT_UNSCHEDULED) {
                isUnscheduledVisit = true;
            }
            Schema.DescribeSObjectResult objDescribe = VTD1_General_Settings__mdt.getSObjectType().getDescribe();
            Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
            List<Schema.PicklistEntry> values = fieldMap.get('VTD2_VisitDurationsUnscheduledVisits__c').getDescribe().getPicklistValues();
            for (Schema.PicklistEntry d : values) {
                durationOptions.add(new DurationOption(d.getLabel(), Decimal.valueOf(d.getValue())));
            }
        }
    }

    @AuraEnabled
    public static VisitInfo getVisitData(Id recordId) {
        Id visitId = defineVisitId(recordId);
        VisitInfo visitInfo = new VisitInfo(getVisitInfo(visitId));
        visitInfo.visitMembers = VT_D1_ActualVisitsHelper.getVisitMembersList(visitId);
        visitInfo.availableDatesTimes = new VT_D1_ActualVisitsHelper().getAvailableDateTimes(visitId);
        return visitInfo;
    }

    @AuraEnabled
    public static String getAvailableDatesTimes(Id recordId, Boolean excludePIm, Decimal mUnscheduledVisitDuration) {
        Id visitId = defineVisitId(recordId);
        VT_D1_ActualVisitsHelper.excludePI = excludePIm;
        if (mUnscheduledVisitDuration != null) {
            VT_D1_ActualVisitsHelper.unscheduledVisitDuration = Math.round(mUnscheduledVisitDuration);
        }
        return new VT_D1_ActualVisitsHelper().getAvailableDateTimes(visitId);
    }

    private static Id defineVisitId(Id recordId) {
        Id visitId;
        if (recordId.getSobjectType() == Task.getSObjectType()) {
            Task taskWithVisit = [SELECT VTD1_Actual_Visit__c FROM Task WHERE Id = :recordId];
            if (taskWithVisit != null) {
                visitId = taskWithVisit.VTD1_Actual_Visit__c;
            }
        } else if (recordId.getSobjectType() == VTD1_Actual_Visit__c.getSObjectType()) {
            visitId = recordId;
        }
        return visitId;
    }

    private static VTD1_Actual_Visit__c getVisitInfo(Id visitId) {
        VTD1_Actual_Visit__c visit = VT_D1_ActualVisitsHelper.getActualVisitInfo(visitId);
        if (visit.VTD1_Case__r.ContactId != null || visit.Unscheduled_Visits__r.ContactId != null) {
            String contactId;
            if (visit.VTD1_Case__r.ContactId != null) {
                contactId = visit.VTD1_Case__r.ContactId;
            } else {
                contactId = visit.Unscheduled_Visits__r.ContactId;
            }
            VT_D1_Phone__c[] phone = [
                    SELECT PhoneNumber__c
                    FROM VT_D1_Phone__c
                    WHERE VTD1_Contact__c = :contactId
                    AND IsPrimaryForPhone__c = TRUE
            ];
            if (phone.size() > 0) {
                if (visit.VTD1_Case__r.ContactId != null) {
                    visit.VTD1_Case__r.Contact.Phone = phone[0].PhoneNumber__c;
                } else {
                    visit.Unscheduled_Visits__r.Contact.Phone = phone[0].PhoneNumber__c;
                }
            }
        }
        return visit;
    }

    @AuraEnabled
    public static void updateActualVisit(Id actualVisitId, String dateTimeFrom,
            String dateTimeTo, String reasonForReschedule) {
        VT_D1_ActualVisitsHelper.updateActualVisit(actualVisitId, dateTimeFrom, dateTimeTo, reasonForReschedule);
    }

    @AuraEnabled
    public static String createOrUpdateEventRemote(String eventDateTime, Boolean isReschedule, Id actualVisitId,
            Decimal duration, String visitType, Boolean isUnscheduledVisit, Boolean deletePI, String visitStatus, String reasonForReschedule) {
        Map<String, Integer> eventDateTimeMap = new Map<String, Integer>();
        eventDateTimeMap = (Map<String, Integer>) JSON.deserialize(eventDateTime, Map<String, Integer>.class);
        Datetime eventDateT = Datetime.newInstance(
                eventDateTimeMap.get('Year'),
                eventDateTimeMap.get('Month'),
                eventDateTimeMap.get('Date'),
                eventDateTimeMap.get('Hour'),
                eventDateTimeMap.get('Minute'),
                eventDateTimeMap.get('Second')
        );
//		DateTime eventDateT =  DateTime.parse(eventDateTime);

        /*if (isReschedule) {
            VTD1_Actual_Visit__c visit = [select Id, VTD1_Status__c from VTD1_Actual_Visit__c where Id = :actualVisitId];
            if (visit.VTD1_Status__c != VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED) {
                visit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED;
                update visit;
            }
        }*/

        try {
            if (!String.isEmpty(reasonForReschedule)) {
                update new VTD1_Actual_Visit__c(Id = actualVisitId, VTD1_Reason_for_Reschedule__c = reasonForReschedule);
            }
            return VT_D1_ActualVisitsHelper.createUpdateEvent(eventDateT,
                    isReschedule, actualVisitId, duration, visitType, isUnscheduledVisit, deletePI);
        } catch (Exception e) {
            System.debug('VT_D1_ActualVisitTriggerHandler.currentException = ' + e.getStackTraceString());
            if (VT_D1_ActualVisitTriggerHandler.currentException != null) {
                e = VT_D1_ActualVisitTriggerHandler.currentException;
            }
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String createTimeSlots(Id actualVisitId, String dateTimesString) {
        try {
            List<String> dateTimesToParse = dateTimesString.split(';');
            String errorMsg;
            // check if time slots are still available
            Set<String> currentAvailableTimes = new VT_D1_ActualVisitsHelper().getAvailableTimesSet(actualVisitId);
            List<String> timeSlotsTaken = new List<String>();
            for (String tSlt : dateTimesToParse) {
                if (!currentAvailableTimes.contains(tSlt)) {
                    timeSlotsTaken.add(tSlt);
                }
            }
            if (timeSlotsTaken.size() != 0) {
                errorMsg = System.Label.VT_D1_TimeSlotsUnavailable + String.join(timeSlotsTaken, ', ') + '.';
            } else {
                List<VTD2_Time_Slot__c> tSlots = new List<VTD2_Time_Slot__c>();
                for (String dt : dateTimesToParse) {
                    VTD2_Time_Slot__c ts = new VTD2_Time_Slot__c();
                    ts.VTD2_Timeslot_Date_Time__c = parseStringToDate(dt);//SH-20799
                    ts.VTD2_Status__c = 'Proposed';
                    ts.VTD2_Actual_Visit__c = actualVisitId;
                    tSlots.add(ts);
                }
                try {
                    insert tSlots;
                } catch (Exception e) {
                    errorMsg = e.getMessage();
                }
            }
            return errorMsg;
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static Object getDateTimesByWeekMonth(Id actualVisitId) {
        try {
            VT_D1_ActualVisitsHelper.isRequestOfTimeSlots = true;
            return new VT_D1_ActualVisitsHelper().getDateTimesByMonthWeekHelper(actualVisitId);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    public class DurationOption {
        @AuraEnabled public String label;
        @AuraEnabled public Decimal value;
        public DurationOption(String l, Decimal v) {
            label = l;
            value = v;
        }
    }

    // SH-20799
    private static Datetime parseStringToDate(String dt) {
        String[] dividedDateTime = dt.split(' ');
        String[] timeArr = dividedDateTime[1].split(':');
        Date localeDependentDate = Date.parse(dividedDateTime[0]);
        Integer hours = Integer.valueOf(timeArr[0]);
        Integer minutes = Integer.valueOf(timeArr[1]);
        if (dividedDateTime[2] == 'pm') {
            hours = hours == 12 ? hours : hours + 12;
        } else if (hours == 12) {
            hours = 0;
        }
        Time dayTime = Time.newInstance(hours, minutes, 0, 0);
        return Datetime.newInstance(localeDependentDate, dayTime);
    }
}