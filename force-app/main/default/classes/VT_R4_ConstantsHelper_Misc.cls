/**
 * THIS CLASS IS DEPRECATED! DON'T USE IT!
 *
 * Use the following instead:
 * @see VT_R4_ConstantsHelper_AccountContactCase
 * @see VT_R4_ConstantsHelper_Documents
 * @see VT_R4_ConstantsHelper_Integrations
 * @see VT_R4_ConstantsHelper_KitsOrders
 * @see VT_R4_ConstantsHelper_ProfilesSTM
 * @see VT_R4_ConstantsHelper_Protocol_ePRO
 * @see VT_R4_ConstantsHelper_Tasks
 * @see VT_R4_ConstantsHelper_VisitsEvents
 */
public with sharing class VT_R4_ConstantsHelper_Misc {
//VT_R4_ConstantsHelper_Documents
    public static final string SOBJECT_DOC_CONTAINER = 'VTD1_Document__c';
    public static final string SOBJECT_DOCUSIGN_RECIPIENT_STATUS = 'dsfs__DocuSign_Recipient_Status__c';

//VT_R4_ConstantsHelper_AccountContactCase
    public static final String CASE_STUDY_FIELD_NAME = 'VTD1_Study__c';
    public static final String CASE_PG_FIELD_NAME = 'VTD1_Primary_PG__c';
    public static final String CASE_PI_FIELD_NAME = 'VTD1_PI_user__c';
    public static final String CASE_BACKUP_PG_FIELD_NAME = 'VTD1_Secondary_PG__c';
    public static final String CASE_BACKUP_PI_FIELD_NAME = 'VTD1_Backup_PI_User__c';
    public static final string SOBJECT_CASE = 'Case';

//VT_R4_ConstantsHelper_VisitsEvents
    public static final String ACTUAL_VISIT_SUBTYPE_PSC = 'PSC'; //VT_R4_ConstantsHelper_VisitsEvents
    public static final string SOBJECT_ACTUAL_VISIT = 'VTD1_Actual_Visit__c'; //VT_R4_ConstantsHelper_VisitsEvents
    public static final String MONITORING_VISIT_TYPE_CLOSEOUT = 'Closeout Visit'; //VT_R4_ConstantsHelper_VisitsEvents
    public static final String MONITORING_VISIT_TYPE_SELECTION = 'Selection Visit'; //VT_R4_ConstantsHelper_VisitsEvents
    public static final String MONITORING_VISIT_TYPE_INITIATION = 'Initiation Visit'; //VT_R4_ConstantsHelper_VisitsEvents
    public static final String VISIT_REQUEST_TASK_SUBJECT = 'Manually Schedule Visit'; //VT_R4_ConstantsHelper_VisitsEvents
    public static final String PROTOCOL_VISIT_TYPE_LABS = 'Labs'; //VT_R4_ConstantsHelper_VisitsEvents
    public static final String PROTOCOL_VISIT_TYPE_STUDY_TEAM = 'Study Team'; //VT_R4_ConstantsHelper_VisitsEvents
    public static final String PROTOCOL_VISIT_TYPE_HCP = 'Health Care Professional (HCP)'; //VT_R4_ConstantsHelper_VisitsEvents
    public static final string SOBJECT_MONITORING_VISIT = 'VTD1_Monitoring_Visit__c';


//VT_R4_ConstantsHelper_Documents
    public static final String REGULATORY_DOCUMENT_TYPE_SOURCE_NOTE_OF_TRANSFER = 'Source Note of Transfer';
    public static final String REGULATORY_DOCUMENT_TYPE_TARGET_NOTE_OF_TRANSFER = 'Target Note of Transfer';
    public static final String REGULATORY_DOCUMENT_TYPE_DOA_LOG = 'DOA Log';
    public static final String REGULATORY_DOCUMENT_TYPE_NOTE_TO_FILE_STEP_DOWN = 'Note to File Step Down';
    public static final String REGULATORY_DOCUMENT_TYPE_SUBJECT_SCREENING_LOG = 'Subject Screening Log';
    public static final String REGULATORY_DOCUMENT_TYPE_SUBJECT_IDENTIFICATION_LOG = 'Subject Identification Log';
    public static final String REGULATORY_DOCUMENT_TYPE_1572 = '1572 IRB Acknowledgement Letter';
    public static final String REGULATORY_DOCUMENT_TYPE_ACKNOWLEDGEMENT_LETTER = 'Acknowledgement Letter';
    public static final String REGULATORY_DOCUMENT_TYPE_FOLLOW_UP_LETTER = 'Follow-Up Letter';
    public static final String REGULATORY_DOCUMENT_LEVEL_SITE = 'Site';
    public static final String REGULATORY_DOCUMENT_LEVEL_SUBJECT = 'Subject';
    public static final String DOCUMENT_EDPRC_NAME = 'EDPRC';
    public static final String DOCUMENT_CATEGORY_UNCATEGORIZED = 'Uncategorized';

//VT_R4_ConstantsHelper_Tasks
    public static final String TASK_CATEGORIES_OTHER_TASKS = 'Other Tasks';
    public static final String SURVEY_SCORING_TASK_SUBJECT = 'Score ePRO Entry';
    public static final String SURVEY_PATIENT_TASK_SUBJECT = 'ePRO Entry Available';


    //ProfilesSTM
    public static final String SSTM_SCR_TYPE_PRIMARY = 'Primary';
    public static final String SSTM_SCR_TYPE_BACKUP = 'Backup';
    public static final String CANDIDATE_PATIENT_CAREGIVER_STATUS_SUCCESS = 'Success';
    public static final String CANDIDATE_PATIENT_CAREGIVER_STATUS_FAILURE = 'Failure';

    //Case
    public static final String STUDY_MILESTONE_PARENT_FIELD = 'VTD1_Study__c';
    public static final String SITE_MILESTONE_PARENT_FIELD = 'VTD1_Site__c';
    public static final String MILESTONE_DESCRIPTION_FIELD_NAME = 'VTD1_Milestone_Description__c';
//del
    public static final String LOGIN_ERROR_STANDARD = 'Your login attempt has failed. Make sure the username and password are correct.';
    public static final String LOGIN_ERROR_CUSTOM = 'You have entered an invalid Username and/or Password combination.<br/>Please verify that you entered this information correctly.';

    //protocolEPRO
    public static final String PSC_EPRO_DEFAULT_QUESTION = 'Did the PSC visit occur and successfully complete?';
//del
    public static final string SOBJECT_SURVEY = 'VTD1_Survey__c';

//use in retake device reading -Sh-8214 
   public static Boolean isRetakeRequest = false;
// use in eligible status SH-13732
    public static final string STUDY_RANDOMIZED_NO = 'No';
    public static final string CASE_SCREENED = 'Screened';
    public static final string CASE_ELIGIBLITY_STATUS_ELIGIBLE = 'Eligible';
    public static final String CASE_ELIGIBLITY_STATUS_INELIGIBLE = 'Ineligible';
    public static final String CASE_WASHOUT_RUN_IN = 'Washout/Run-In';
    public static final String CASE_WASHOUT_RUN_IN_STATUS = 'Successful';
    public static final String CASE_ACTIVE_RANDOMIZED = 'Active/Randomized';




}