public with sharing class VT_D1_PatientCommunityController {
	@AuraEnabled
	public static User getPatientUser() {
		List<User> users = [SELECT ContactId, FirstName FROM User WHERE Id=:UserInfo.getUserId() AND ContactId != NULL];
		if (users.isEmpty()) return null;

		return users[0];
	}

	@AuraEnabled (Cacheable=true)
	public static Map<String, String> getLabelTranslated(List<String> labels) {
		Map<String, String> translationMap = new Map<String, String>();
		for (String l : labels) {
			String translatedLabel = VT_D1_TranslateHelper.getLabelValue(l);
			translationMap.put(l, translatedLabel);
		}
		return translationMap;
	}

	@AuraEnabled(Cacheable=true)
	public static String getCommunityName() {
		try {
			return ConnectApi.Communities.getCommunity(Network.getNetworkId()).name;
		} catch (Exception e) {
			return null;
		}
	}
}