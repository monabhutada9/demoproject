/**
 * Created by User on 19/06/30.
 */
@isTest
private with sharing class VT_D1_FeedItemTriggerTest {
    @TestSetup
    static void setup() {
        Account acc = (Account) new DomainObjects.Account_t()
                .setName('acc')
                .persist();

        FeedItem fItem = (FeedItem) new DomainObjects.FeedItem_t()
                .setBodyFeedItem('Test')
                .setParentId(acc.Id)
                .persist();
    }

//    @isTest
//    private static void doInsertTest(){
//        Account acc = new Account(Name = 'acc');
//        insert acc;
//        Account acc = (Account) new DomainObjects.Account_t()
//                .setName('acc')
//                .persist();
//
//        FeedItem fItem = new FeedItem(Body = 'Test', ParentId = acc.Id);
//        insert fItem;
//        System.assertNotEquals(null,fItem.Id);
//    }

    @isTest
    private static void doUpdateTest(){
        User admin = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND isActive = true LIMIT 1];
//        Account acc = new Account(Name = 'acc');
//        insert acc;
//
//        FeedItem fItem = new FeedItem(Body = 'Test', ParentId = acc.Id);
//        insert fItem;
//        System.assertNotEquals(null,fItem.Id);
        FeedItem fItem = [SELECT Id,Body FROM FeedItem];

        Test.startTest();
        fItem.Body = 'Update test';
        System.runAs(admin) {
            update fItem;
            List<FeedItem> feedItemList = [SELECT Id,Body FROM FeedItem WHERE Id = :fItem.Id];
            System.assertEquals('Update test', feedItemList[0].Body);
        }
        Test.stopTest();
    }

    @isTest
    private static void doDeleteTest(){
//        Account acc = new Account(Name = 'acc');
//        insert acc;
//
//        FeedItem fItem = new FeedItem(Body = 'Test', ParentId = acc.Id);
//        insert fItem;
//        System.assertNotEquals(null,fItem.Id);
        FeedItem fItem = [SELECT Id,Body FROM FeedItem];
        Test.startTest();
        delete fItem;
        List<FeedItem> feedItemList = [SELECT Id FROM FeedItem];
        System.assertEquals(0,feedItemList.size());
        Test.stopTest();
    }
}