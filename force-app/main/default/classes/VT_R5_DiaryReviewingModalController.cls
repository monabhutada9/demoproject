/**
 * Created by Casablanca Team on 21.10.2020.
 */

public with sharing class VT_R5_DiaryReviewingModalController {
    @AuraEnabled
    public static ResponseWrapper getData(Id surveyId) {
        return new ResponseWrapper(surveyId);
    }

    @AuraEnabled
    public static void reviewSurveyWithScore(Id surveyId, String scoringJson) {
        updateScoring(scoringJson);
        updateSurvey(surveyId);
    }

    private static void updateScoring(String scoringJson) {
        List<AnswerWrapper> answers = (List<AnswerWrapper>) JSON.deserialize(scoringJson, List<AnswerWrapper>.class);
        Map<Id, SObject> answersMap = new Map<Id, SObject>();
        for (AnswerWrapper answer : answers) {
            answersMap.put(answer.id, answer.answerRecord);
        }
        update answersMap.values();
    }

    private static void updateSurvey(Id surveyId) {
        SurveyWrapper survey = new SurveyWrapper(surveyId);
        survey.reviewed = true;
        update survey.surveyRecord;
    }

    public class ResponseWrapper {
        @AuraEnabled
        public SurveyWrapper survey { get; private set; }

        @AuraEnabled
        public List<GroupWrapper> groups {
            get {
                return (groupsMap != null) ? groupsMap.values() : null;
            }
        }

        private transient Map<Integer, GroupWrapper> groupsMap { get; set; }

        public ResponseWrapper(Id surveyId) {
            this.survey = new SurveyWrapper(surveyId);

            Map<Id, VTD1_Survey_Answer__c> answerRecordsMap = getAnswerRecordsMap(surveyId);
            if (answerRecordsMap.isEmpty()) return;

            Map<Id, QuestionWrapper> answerIdToQuestionMap = buildAnswerIdToQuestionMap(answerRecordsMap);

            // branching logic
            if (this.survey.isBranching) {
                Map<Id, QuestionWrapper> questionsMap = buildQuestionsMap(answerIdToQuestionMap.values());
                for (QuestionWrapper question : questionsMap.values()) {
                    if (questionsMap.containsKey(question.parentId)) {
                        question.parent = questionsMap.get(question.parentId);
                    }
                }
            }

            this.groupsMap = buildGroupsMap(answerRecordsMap, answerIdToQuestionMap);
        }

        private Map<Id, VTD1_Survey_Answer__c> getAnswerRecordsMap(Id surveyId) {
            List<VTD1_Survey_Answer__c> answerRecords = [
                    SELECT
                            VTD1_Answer__c,
                            VTD1_Score__c,
                            VTR4_QuestionContentRichText__c,
                            VTR2_Question_content__c,
                            RecordTypeId,
                            RecordType.Name,
                            RecordType.DeveloperName,
                            VTD1_Question__r.Id,
                            VTD1_Question__r.VTR2_QuestionContentLong__c,
                            VTD1_Question__r.VTR4_QuestionContentRichText__c,
                            VTD1_Question__r.VTR2_Scoring_Type__c,
                            VTD1_Question__r.VTR2_AllowOverridingAutoscore__c,
                            VTD1_Question__r.VTR4_Branch_Parent__c,
                            VTD1_Question__r.VTR4_Response_Trigger__c,
                            VTD1_Question__r.VTR4_Number__c,
                            VTD1_Question__r.VTD1_Options__c,
                            VTD1_Question__r.VTD1_Type__c
                    FROM VTD1_Survey_Answer__c
                    WHERE VTD1_Survey__c = :surveyId
                    ORDER BY VTD1_Question__r.VTR4_Number__c ASC
            ];
            VT_D1_TranslateHelper.translate(answerRecords, this.survey.language);
            return new Map<Id, VTD1_Survey_Answer__c> (answerRecords);
        }

        private Map<Id, QuestionWrapper> buildAnswerIdToQuestionMap(Map<Id, VTD1_Survey_Answer__c> answerRecordsMap) {
            Map<Id, QuestionWrapper> result = new Map<Id, QuestionWrapper>();
            for (VTD1_Survey_Answer__c answerRecord : answerRecordsMap.values()) {
                AnswerWrapper answer = new AnswerWrapper(answerRecord);
                QuestionWrapper question = new QuestionWrapper(answerRecord.VTD1_Question__r, answer);
                result.put(answer.id, question);
            }
            return result;
        }

        private Map<Id, QuestionWrapper> buildQuestionsMap(List<QuestionWrapper> questions) {
            Map<Id, QuestionWrapper> result = new Map<Id, QuestionWrapper>();
            for (QuestionWrapper question : questions) {
                result.put(question.id, question);
            }
            return result;
        }

        private Map<Integer, GroupWrapper> buildGroupsMap(Map<Id, VTD1_Survey_Answer__c> answersMap, Map<Id, QuestionWrapper> answerIdToQuestionMap) {
            Map<Integer, GroupWrapper> result = new Map<Integer, GroupWrapper>();

            if (!this.survey.isBranching) {
                Map<Id, Set<VTR2_Patient_eDiary_Group__c>> answerIdToGroupRecordsMap = getAnswerIdToGroupRecordsMap(answersMap.keySet());

                for (Id answerId : answerIdToGroupRecordsMap.keySet()) {
                    for (VTR2_Patient_eDiary_Group__c groupRecord: answerIdToGroupRecordsMap.get(answerId)) {
                        GroupWrapper gr = new GroupWrapper(groupRecord);
                        if (!result.containsKey(gr.num)) {
                            result.put(gr.num, gr);
                        }
                        result.get(gr.num).questions.add(answerIdToQuestionMap.get(answerId));
                    }
                }

                Set<Id> groupedAnswersIds = new Set<Id>();
                for (Id answerId : answerIdToGroupRecordsMap.keySet()) {
                    groupedAnswersIds.add(answerId);
                }

                for (Id answerId: groupedAnswersIds) {
                    answerIdToQuestionMap.remove(answerId);
                }
            }

            if (!answerIdToQuestionMap.isEmpty()) {
                GroupWrapper gr = new GroupWrapper();
                gr.questions.addAll(answerIdToQuestionMap.values());
                result.put(gr.num, gr);
            }

            return result;
        }

        private Map<Id, Set<VTR2_Patient_eDiary_Group__c>> getAnswerIdToGroupRecordsMap(Set<Id>answerIds) {
            List<VTR2_Patient_eDiary_Group_Answer__c> groupAnswerList = [
                    SELECT
                            Id,
                            VTR2_eDiary_Answer__c,
                            VTR2_eDiary_Answer__r.VTD1_Question__r.VTD1_Number__c,
                            VTR2_Patient_eDiary_Group__c,
                            VTR2_Patient_eDiary_Group__r.Name,
                            VTR2_Patient_eDiary_Group__r.VTR2_Protocol_eDiary_Group__r.VTR2_Page_Number__c
                    FROM VTR2_Patient_eDiary_Group_Answer__c
                    WHERE VTR2_eDiary_Answer__c IN :answerIds
                    ORDER BY
                            VTR2_Patient_eDiary_Group__r.VTR2_Protocol_eDiary_Group__r.VTR2_Page_Number__c ASC,
                            VTR2_eDiary_Answer__r.VTD1_Question__r.VTD1_Number__c ASC
            ];
            Map<Id, Set<VTR2_Patient_eDiary_Group__c>> groupMap = new Map<Id, Set<VTR2_Patient_eDiary_Group__c>>();
            for (VTR2_Patient_eDiary_Group_Answer__c groupAnswer : groupAnswerList) {
                Id answerId = groupAnswer.VTR2_eDiary_Answer__c;
                if (!groupMap.containsKey(answerId)) {
                    groupMap.put(answerId, new Set<VTR2_Patient_eDiary_Group__c>());
                }
                groupMap.get(answerId).add(groupAnswer.VTR2_Patient_eDiary_Group__r);
            }
            return groupMap;
        }
    }

    public class SurveyWrapper {
        private transient Id surveyId { get; set; }

        private transient VTD1_Survey__c record { get; set; }

        public transient VTD1_Survey__c surveyRecord {
            get {
                if (this.record == null) {
                    List<VTD1_Survey__c> records = [
                            SELECT
                                    VTD1_Protocol_ePRO__r.VTR4_Is_Branching_eDiary__c,
                                    VTR2_Reviewer_User__c,
                                    VTD1_CSM__r.VTR2_SiteCoordinator__c,
                                    VTD1_CSM__r.VTD1_Primary_PG__c,
                                    VTD1_Status__c,
                                    VTR5_Reviewed__c,
                                    RecordType.DeveloperName,
                                    VTR2_DocLanguage__c,
                                    VTR4_Percentage_Completed__c,
                                    VTD1_Number_of_Answers__c,
                                    VTR2_Number_of_Answered_Answers__c,
                                    Name,
                                    VTD1_Completion_Time__c,
                                    VTD1_Due_Date__c,
                                    VTR5_CompletedbyCaregiver__c
                            FROM VTD1_Survey__c
                            WHERE Id = :this.surveyId
                    ];
                    if (records.isEmpty()) {
                        throw new AuraHandledException('Survey not found.');
                    }
                    this.record = records[0];
                }
                return this.record;
            }
        }

        public transient String language {
            get {
                return this.surveyRecord.VTR2_DocLanguage__c;
            }
        }

        private transient Boolean isExternal {
            get {
                return this.recordType == 'VTR5_External';
            }
        }

        private transient Boolean isTraditional {
            get {
                return this.recordType == 'ePRO';
            }
        }

        private transient String recordType {
            get {
                return this.surveyRecord.RecordType.DeveloperName;
            }
        }

        @AuraEnabled
        public String name {
            get {
                return this.surveyRecord.Name;
            }
        }

        @AuraEnabled
        public Datetime completionTime {
            get {
                return this.surveyRecord.VTD1_Completion_Time__c;
            }
        }

        @AuraEnabled
        public Datetime dueDate {
            get {
                return this.surveyRecord.VTD1_Due_Date__c;
            }
        }

        @AuraEnabled
        public Boolean completedByCaregiver {
            get {
                return this.surveyRecord.VTR5_CompletedbyCaregiver__c;
            }
        }

        private transient Boolean reviewRequired {
            get {
                return !this.reviewed && (
                        this.isTraditional && this.status == VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED
                        || this.isExternal && this.status == VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_COMPLETED
                );
            }
        }

        @AuraEnabled
        public Boolean reviewed {
            get {
                return this.surveyRecord.VTR5_Reviewed__c;
            }
            set {
                if (value) {
                    this.surveyRecord.VTR5_Reviewed__c = true;
                    if (this.isTraditional) {
                        this.status = surveyRecord.VTR4_Percentage_Completed__c == 100
                            ? VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED
                            : VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEWED_PARTIALLY_COMPLETED;
                    }
                } else {
                    this.surveyRecord.VTR5_Reviewed__c = false;
                    if (this.isTraditional) {
                        this.status = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED;
                    }
                }
            }
        }

        private String status {
            get {
                return this.surveyRecord.VTD1_Status__c;
            }
            set {
                this.surveyRecord.VTD1_Status__c = value;
            }
        }

        @AuraEnabled
        public Boolean isBranching {
            get {
                return surveyRecord.VTD1_Protocol_ePRO__r.VTR4_Is_Branching_eDiary__c;
            }
        }

        @AuraEnabled
        public Boolean canUserReview {
            get {
                String userProfileName = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
                if (!Test.isRunningTest() && !VT_R4_ConstantsHelper_ProfilesSTM.SITE_STAFF.contains(userProfileName)) {
                    return false;
                }

                Boolean reviewedAccessible = VT_Utilities.isAccessibleAndUpdatable(VTD1_Survey__c.VTR5_Reviewed__c);
                Boolean statusAccessible = VT_Utilities.isAccessibleAndUpdatable(VTD1_Survey__c.VTD1_Status__c);
                Boolean scoreAccessible = VT_Utilities.isAccessible(VTD1_Survey_Answer__c.VTD1_Score__c); //for copado
                Boolean fieldsAccessible = reviewedAccessible && statusAccessible && scoreAccessible;

                return fieldsAccessible && this.reviewRequired;
            }
        }

        public SurveyWrapper(Id surveyId) {
            this.surveyId = surveyId;
        }
    }

    public class GroupWrapper {
        @AuraEnabled
        public String name { get; private set; }

        @AuraEnabled
        public Integer num { get; private set; }

        @AuraEnabled
        public List<QuestionWrapper> questions { get; private set; }

        public GroupWrapper(VTR2_Patient_eDiary_Group__c groupRecord) {
            this(groupRecord.Name, Integer.valueOf(groupRecord.VTR2_Protocol_eDiary_Group__r.VTR2_Page_Number__c));
        }

        public GroupWrapper() {
            this(null, 0);
        }

        private GroupWrapper(String name, Integer num) {
            this.name = name;
            this.num = num;
            this.questions = new List<QuestionWrapper>();
        }
    }

    public class QuestionWrapper {
        private transient VTD1_Protocol_ePro_Question__c questionRecord { get; set; }

        public transient QuestionWrapper parent { get; set; }

        private transient String responseTrigger {
            get {
                return questionRecord.VTR4_Response_Trigger__c;
            }
        }

        public transient Id parentId {
            get {
                return this.questionRecord.VTR4_Branch_Parent__c;
            }
        }

        public transient Id id {
            get {
                return questionRecord.Id;
            }
        }

        @AuraEnabled
        public Boolean skipped {
            get {
                return !this.answered && !this.suppressed;
            }
        }

        @AuraEnabled
        public Boolean suppressed {
            get {
                return isSuppressed();
            }
        }

        private Boolean isSuppressed() {
            // If the question has no parent it cannot be suppressed
            // The question is suppressed if parent question is answered and this value differs
            // from question response trigger or if parent question is suppressed
            if (parent != null) {
                return this.parent.answered && this.responseTrigger != this.parent.answer.text || this.parent.isSuppressed();
            } else {
                return false;
            }
        }

        @AuraEnabled
        public Boolean answered {
            get {
                return String.isNotEmpty(this.answer.text);
            }
        }

        @AuraEnabled
        public Boolean scoringNecessary {
            get {
                return this.scoringAvailable && !this.suppressed;
            }
        }

        @AuraEnabled
        public Boolean scoringAvailable {
            get {
                return this.questionRecord.VTR2_Scoring_Type__c == 'Manual' || this.questionRecord.VTR2_AllowOverridingAutoscore__c;
            }
        }

        @AuraEnabled
        public String num { // Can be "1", "1.1", "1.1.1", etc.
            get {
                return this.questionRecord.VTR4_Number__c;
            }
        }

        @AuraEnabled
        public AnswerWrapper answer { get; private set; }

        public QuestionWrapper(VTD1_Protocol_ePro_Question__c question, AnswerWrapper answer) {
            this.questionRecord = question;
            this.answer = answer;
        }
    }

    public class AnswerWrapper {
        private transient VTD1_Survey_Answer__c record { get; set; }

        public transient VTD1_Survey_Answer__c answerRecord {
            get {
                if (this.record == null) {
                    this.record = new VTD1_Survey_Answer__c();
                }
                return this.record;
            }
            private set {
                this.record = value;
            }
        }

        @AuraEnabled
        public Id id {
            get {
                return this.answerRecord.Id;
            }
            private set {
                this.answerRecord.Id = value;
            }
        }

        @AuraEnabled
        public Integer score {
            get {
                return Integer.valueOf(this.answerRecord.VTD1_Score__c);
            }
            private set {
                this.answerRecord.VTD1_Score__c = value;
            }
        }

        @AuraEnabled
        public Boolean hasRichQuestionText {
            get {
                return String.isNotEmpty(this.answerRecord.VTR4_QuestionContentRichText__c);
            }
        }

        @AuraEnabled
        public String questionText {
            get {
                return this.hasRichQuestionText
                        ? this.answerRecord.VTR4_QuestionContentRichText__c
                        : this.answerRecord.VTR2_Question_content__c;
            }
        }

        @AuraEnabled
        public String text {
            get {
                return String.isNotEmpty(this.answerRecord.VTD1_Answer__c) ? this.answerRecord.VTD1_Answer__c : null;
            }
        }

        public AnswerWrapper(VTD1_Survey_Answer__c answer) {
            this.answerRecord = answer;
        }
    }
}