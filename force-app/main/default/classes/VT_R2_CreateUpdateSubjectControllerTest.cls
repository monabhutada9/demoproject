@IsTest
public with sharing class VT_R2_CreateUpdateSubjectControllerTest {

    public static void testCreateUpdateCase() {

        Case caseRecord = (Case) new DomainObjects.Case_t().persist();
        PageReference pageRef = new PageReference('/' + caseRecord.Id);
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', String.valueOf(caseRecord.Id));
        Test.startTest();
        new VT_D2_CreateUpdateSubjectController();
        new VT_D2_CreateUpdateSubjectController(new ApexPages.StandardController(caseRecord)).createUpdateCase();
        Test.stopTest();
    }
}