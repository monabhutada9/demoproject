/*
 * Created by shume on 08/10/2018.
 */
public class VT_D2_EmailAboutNewMessagesSchedule implements Schedulable {

    public void execute(SchedulableContext ctx) {
        Database.executeBatch(new VT_D2_SendEmailAboutNewMessages(),100);
    }
}