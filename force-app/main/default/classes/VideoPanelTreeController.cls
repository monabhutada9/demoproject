public without sharing class VideoPanelTreeController {

    public class Participant {
        String photoUrl { get; set; }
        String userName { get; set; }
        String profName { get; set; }
        Boolean isGeneralVisit { get; set; }

        public Participant(String photoUrl, String userName, String profName, Boolean isGeneralVisit) {
            this.photoUrl = photoUrl;
            this.userName = userName;
            this.profName = profName;
            this.isGeneralVisit = isGeneralVisit;
        }
    }

    @AuraEnabled
    public static String getParticipants(Id confId) {
        List<Participant> participants = new List<Participant>();
        for (VTD1_Conference_Member__c item : getConferenceMembersByConferenceId(confId)) {
            participants.add(new Participant(
                    item.VTD1_User__r.SmallPhotoUrl,
                    item.Participant_Name__c,
                    item.VTD1_Participant_Role__c,
                    item.VTD1_Video_Conference__r.VTR4_General_Visit__c != null
            ));
        }
        return JSON.serialize(participants);
    }

    public static List<VTD1_Conference_Member__c> getConferenceMembersByConferenceId(Id confId) {
        if (confId == null) {
            confId = [SELECT Televisit_Buffer__c FROM User WHERE Id = :UserInfo.getUserId()].Televisit_Buffer__c;
        }

        if (confId != null) {
            List<VTD1_Conference_Member__c> confMembers = [
                    SELECT Id, Participant_Name__c, VTD1_Participant_Role__c, VTD1_User__r.SmallPhotoUrl, VTD1_User__c,
                            VTD1_Video_Conference__r.VTR4_General_Visit__c
                    FROM VTD1_Conference_Member__c
                    WHERE VTD1_Video_Conference__c = :confId
            ];

            VT_D1_TranslateHelper.translate(confMembers);
            return confMembers;
        }
        return new List<VTD1_Conference_Member__c>();
    }

    @AuraEnabled
    public static String getChecklists(Id confId) {
        if (confId == null) {
            confId = [SELECT Televisit_Buffer__c FROM User WHERE Id = :UserInfo.getUserId()].Televisit_Buffer__c;
        }

        String checklist;
        if (confId != null) {
            if (UserInfo.getProfileId() == [SELECT Id FROM Profile WHERE Name = 'Primary Investigator'].Id) {
                checklist = getPIChecklist(confId);
            } else {
                checklist = getPatientChecklist(confId);
            }
        }
        return checklist == null ? '' : checklist.replace('\n', '<br />');
    }

    private static String getPIChecklist(Id confId) {
        return [
                SELECT VTD1_Actual_Visit__r.VTD1_Additional_Visit_Checklist__c
                FROM Video_Conference__c
                WHERE Id = :confId
        ].VTD1_Actual_Visit__r.VTD1_Additional_Visit_Checklist__c;
    }

    private static String getPatientChecklist(Id confId) {
        return [
                SELECT VTD1_Actual_Visit__r.VTD1_Additional_Patient_Visit_Checklist__c
                FROM Video_Conference__c
                WHERE Id = :confId
        ].VTD1_Actual_Visit__r.VTD1_Additional_Patient_Visit_Checklist__c;
    }

    //SH-9842
}