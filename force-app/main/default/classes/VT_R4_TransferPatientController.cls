/**
 * Created by O. Berehovskyi on 19-Feb-20.
 */
@RestResource(UrlMapping='/CandidatePatientToTransfer')
global with sharing class VT_R4_TransferPatientController {
    private static SharingIgnoringService sharingIgnoringService = new SharingIgnoringService();
    @AuraEnabled (Cacheable = true) 
    public static StudiesInfo getAllStudies(final Id caseId) {
        try {
            final StudiesInfo studiesInfo = new StudiesInfo();
            studiesInfo.allStudies = [
                    SELECT Id, Name, VTR2_EnrollmentStatus__c
                    FROM HealthCloudGA__CarePlanTemplate__c
                    WHERE HealthCloudGA__Active__c = TRUE
                    WITH SECURITY_ENFORCED
            ];
            List<Case> currentCase = [
                    SELECT VTD1_Study__r.Name
                    FROM Case
                    WHERE Id = :caseId
                    WITH SECURITY_ENFORCED
            ];
            studiesInfo.currentStudy = !currentCase.isEmpty() ? currentCase[0].VTD1_Study__r.Name : null;
            return studiesInfo;
        } catch(Exception exc) {throw new AuraHandledException(exc.getMessage());}
    }
    @AuraEnabled(Cacheable = true)
    public static List<PrimaryInvestigator> findAvailablePIs(final Id studyId) {
        try {
            return sharingIgnoringService.findAvailablePIs(studyId);
        } catch(Exception exc) {throw new AuraHandledException(exc.getMessage());}
    }
    @AuraEnabled(Cacheable = true)
    public static List<VTR2_Pre_Screening_Criteria__c> findCriteria(final Id studyId) {
        try {
            return sharingIgnoringService.findCriteria(studyId);
        } catch(Exception exc) {throw new AuraHandledException(exc.getMessage());}
    }
    @AuraEnabled(Cacheable = true)
    public static Contact getContact(final Id caseId) {
        try {
            return sharingIgnoringService.getContact(caseId);
        } catch(Exception exc) {throw new AuraHandledException(exc.getMessage());}
    }
    @AuraEnabled
    public static void saveCandidatePatient(CandidatePatient candidatePatient) {
        candidatePatient.createdById = UserInfo.getUserId();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Authorization', 'Bearer ' + VT_D1_HelperClass.getSessionIdForGuest());
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(System.Url.getOrgDomainUrl().toExternalForm() + '/services/apexrest/CandidatePatientToTransfer');
        req.setBody(JSON.serialize(candidatePatient));
        req.setTimeout(120000);
        System.debug(req);
        Http http = new Http();
        HttpResponse res = http.send(req);
        System.debug(res.getStatus() +' '+res.getBody());
        if (res.getStatusCode()!=200) {
            throw new AuraHandledException(res.getStatus() +' '+ res.getBody());
        }
    }
    @HttpPost
    global static void doPost() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        String requestBody = request.requestBody.toString();
        try {
            CandidatePatient candidatePatient = (CandidatePatient)JSON.deserialize(requestBody, CandidatePatient.class);
            response.responseBody = Blob.valueOf(createCandidatePatient(candidatePatient));
            response.statusCode = 200;
        } catch (Exception e) {
            response.responseBody = Blob.valueOf(e.getMessage());
            response.statusCode = 500;
        }
    }
    @TestVisible
    private static String createCandidatePatient(CandidatePatient candidatePatient) {
        HealthCloudGA__CandidatePatient__c candidate = new HealthCloudGA__CandidatePatient__c();
        candidate.VTR4_CaseForTransfer__c = candidatePatient.caseForTransfer;
        candidate.VTR2_Created_By_User__c = candidatePatient.createdById;
        candidate.rr_firstName__c = candidatePatient.firstName;
        candidate.rr_lastName__c = candidatePatient.lastName;
        candidate.HealthCloudGA__Address1Country__c = candidatePatient.countryCode;
        candidate.VTD2_Language__c = candidatePatient.preferredLanguage;
        candidate.rr_Email__c  = candidatePatient.email;
        candidate.VTR2_Patient_Status__c = candidatePatient.patientStatus;
        candidate.VTR4_WillMedRecordsCarryOver__c = candidatePatient.willRecordsCarryOver;
        candidate.HealthCloudGA__Address1Line1__c = candidatePatient.addressLine1;
        candidate.HealthCloudGA__Address1Line2__c = candidatePatient.addressLine2;
        candidate.VTR3_AddressLine3__c = candidatePatient.addressLine3;
        candidate.HealthCloudGA__Address1City__c = candidatePatient.cityTownVillage;
        candidate.HealthCloudGA__Address1State__c = candidatePatient.stateProvinceTerritory;
        candidate.HealthCloudGA__Address1PostalCode__c = candidatePatient.zipPostal;
        candidate.VTR2_Subject_ID__c = candidatePatient.subjectId;
        candidate.VTD1_Patient_Phone__c = candidatePatient.phoneNumber;
        candidate.VTR2_Patient_Phone_Type__c = candidatePatient.phoneNumberType;
        candidate.Study__c = candidatePatient.study;
        candidate.VTD1_ProtocolNumber__c = candidatePatient.protocolNumber;
        candidate.VTR2_Pi_Id__c = candidatePatient.pi;
        candidate.VTR5_Priority__c = 2;
        candidate.VT_R4_VirtualSite__c = candidatePatient.virtualSiteId;
        System.debug(LoggingLevel.INFO, 'candidatePatient ' + JSON.serializePretty(candidate));
        List<String> requiredFields = new List<String>{
                candidate.Study__c,
                candidate.VTD1_ProtocolNumber__c,
                candidate.rr_firstName__c,
                candidate.rr_lastName__c,
                candidate.HealthCloudGA__Address1Country__c,
                candidate.VTD2_Language__c,
                candidate.HealthCloudGA__Address1State__c,
                candidate.HealthCloudGA__Address1PostalCode__c,
                candidate.rr_Email__c
        };
        System.debug('CandidatePatient requiredFields '+requiredFields);
        if (!requiredFields.contains(null) && !requiredFields.contains('')) {
            insert candidate;
            System.debug('CandidatePatient inserted '+candidate.Id);
        }
        return candidate.Id;
    }
    public without sharing class SharingIgnoringService {
        public List<PrimaryInvestigator> findAvailablePIs(final Id studyId) {
            final List<Study_Team_Member__c> studyTeamMembers = [
                    SELECT User__c, User__r.Name, VTD1_PI_License__c, VTD1_VirtualSite__c
                    FROM Study_Team_Member__c
                    WHERE VTD1_Type__c = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME
                            AND Study__c = :studyId
                            AND VTD1_VirtualSite__c != NULL
            ];
            final List<PrimaryInvestigator> suitablePIs = new List<PrimaryInvestigator>();
            for (Study_Team_Member__c stm : studyTeamMembers) {
                final PrimaryInvestigator pi = new PrimaryInvestigator(
                        stm.User__c,
                        stm.User__r.Name,
                        stm.VTD1_PI_License__c,
                        stm.VTD1_VirtualSite__c
                );
                if (!suitablePIs.contains(pi)) {
                    suitablePIs.add(pi);
                }
            }
            return suitablePIs;
        }
        public List<VTR2_Pre_Screening_Criteria__c> findCriteria(final Id studyId) {
            return [
                    SELECT VTR2_Criteria__c
                    FROM VTR2_Pre_Screening_Criteria__c
                    WHERE VTR2_Study__c = :studyId
            ];
        }
        public Contact getContact(final Id caseId) {
            return [
                    SELECT
                            FirstName,
                            LastName,
                            VTD1_UserId__r.Country,
                            VTD1_UserId__r.CountryCode,
                            VTR2_Primary_Language__c,
                            Email,
                            VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c,
                            VTD1_Clinical_Study_Membership__r.VTD1_Study__r.Name,
                            VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR2_EnrollmentStatus__c,
                    (
                            SELECT
                                    Name,
                                    VTD1_Address2__c,
                                    VTR3_AddressLine3__c,
                                    City__c,
                                    State__c,
                                    ZipCode__c
                            FROM Addresses__r
                            WHERE VTD1_Primary__c = TRUE
                    ),
                    (
                            SELECT PhoneNumber__c, Type__c
                            FROM Phones__r
                            WHERE IsPrimaryForPhone__c = TRUE
                    )
                    FROM Contact
                    WHERE Id IN (
                            SELECT ContactId
                            FROM Case
                            WHERE Id = :caseId
                    )
            ];
        }
    }
    public class StudiesInfo {
        @AuraEnabled public List<HealthCloudGA__CarePlanTemplate__c> allStudies { get; set; }
        @AuraEnabled public String currentStudy { get; set; }
    }
    public class PrimaryInvestigator {
        @AuraEnabled public Id userId { get; set; }
        @AuraEnabled public String name { get; set; }
        @AuraEnabled public String license { get; set; }
        @AuraEnabled public Id virtualSiteId { get; set; }
        public PrimaryInvestigator(Id userId, String name, String license, Id virtualSiteId) {
            this.userId = userId;
            this.name = name;
            this.license = license;
            this.virtualSiteId = virtualSiteId;
        }
        public Boolean equals(Object pi) {
            if (pi instanceof PrimaryInvestigator) {
                PrimaryInvestigator p = (PrimaryInvestigator) pi;
                return this.userId == p.userId && this.name == p.name;
            }
            return false;
        }
    }
    public class CandidatePatient {
        @AuraEnabled public String caseForTransfer { get; set; }
        @AuraEnabled public String createdById { get; set; }
        @AuraEnabled public String firstName { get; set; }
        @AuraEnabled public String lastName { get; set; }
        @AuraEnabled public String country { get; set; }
        @AuraEnabled public String countryCode { get; set; }
        @AuraEnabled public String preferredLanguage { get; set; }
        @AuraEnabled public String email { get; set; }
        @AuraEnabled public String patientStatus { get; set; }
        @AuraEnabled public Boolean willRecordsCarryOver { get; set; }
        @AuraEnabled public String addressLine1 { get; set; }
        @AuraEnabled public String addressLine2 { get; set; }
        @AuraEnabled public String addressLine3 { get; set; }
        @AuraEnabled public String cityTownVillage { get; set; }
        @AuraEnabled public String stateProvinceTerritory { get; set; }
        @AuraEnabled public String zipPostal { get; set; }
        @AuraEnabled public String phoneNumber { get; set; }
        @AuraEnabled public String phoneNumberType { get; set; }
        @AuraEnabled public String study { get; set; }
        @AuraEnabled public String protocolNumber { get; set; }
        @AuraEnabled public String pi { get; set; }
        @AuraEnabled public String subjectId { get; set; }
        @AuraEnabled public String virtualSiteId { get; set; }
    }
}