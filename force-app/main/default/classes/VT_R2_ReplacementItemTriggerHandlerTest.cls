/**
* @author: Carl Judge
* @date: 17-Jul-19
**/

@IsTest
public with sharing class VT_R2_ReplacementItemTriggerHandlerTest {


    public static void doTest() {
        Case c = [
            SELECT Id
            FROM Case
            WHERE RecordType.DeveloperName = 'CarePlan'
            ORDER BY CreatedDate DESC
            LIMIT 1
        ];

        VTD1_Order__c oldDelivery = new VTD1_Order__c(
            VTD1_ShipmentId__c = 'xyz',
            VTD1_Case__c = c.Id
        );
        insert oldDelivery;
        VTD1_Patient_Kit__c pKit = new VTD1_Patient_Kit__c(
            VTD2_MaterialId__c = '1234',
            VTD1_IntegrationId__c = 'abcd',
            VTD1_Patient_Delivery__c = oldDelivery.Id,
                VTD1_Case__c = c.Id
        );
        insert pKit;

        VTD1_Order__c newDelivery = new VTD1_Order__c(
            VTD1_Case__c = c.Id
        );
        insert newDelivery;
        insert new VTD1_Replacement_Item__c(
            RecordTypeId = VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_REPLACEMENT_ITEM_KIT,
            VTD1_Patient_Kit__c = pKit.Id,
            VTD1_Patient_Delivery__c = newDelivery.Id,
                VTD1_CSM__c = c.Id
        );

        pKit = [SELECT VTD2_MaterialId__c, VTD1_IntegrationId__c FROM VTD1_Patient_Kit__c WHERE Id = :pKit.Id];
        oldDelivery = [SELECT VTD1_ShipmentId__c FROM VTD1_Order__c WHERE Id = :oldDelivery.Id];
        newDelivery = [SELECT VTD1_ShipmentId__c FROM VTD1_Order__c WHERE Id = :newDelivery.Id];
        System.assert(pKit.VTD2_MaterialId__c == null);
        System.assert(pKit.VTD1_IntegrationId__c == null);
        System.assertEquals([SELECT COUNT() FROM VTD1_Patient_Kit__c WHERE VTD1_Replacement_For__c = :pKit.Id], 1);
        System.assertEquals(oldDelivery.VTD1_ShipmentId__c, null);
        System.assertEquals(newDelivery.VTD1_ShipmentId__c, 'xyz');

        VTD1_Order__c differentOldDelivery = new VTD1_Order__c(
            VTD1_ShipmentId__c = '4567',
            VTD1_Case__c = c.Id
        );
        insert differentOldDelivery;
        VTD1_Patient_Kit__c differentKit = new VTD1_Patient_Kit__c(
            VTD2_MaterialId__c = '4321',
            VTD1_IntegrationId__c = 'dcba',
            VTD1_Patient_Delivery__c = differentOldDelivery.Id,
                VTD1_Case__c = c.Id
        );
        insert differentKit;
        String errorMsg = '';
        try {
            insert new VTD1_Replacement_Item__c(
                RecordTypeId = VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_REPLACEMENT_ITEM_KIT,
                VTD1_Patient_Kit__c = pKit.Id,
                VTD1_Patient_Delivery__c = newDelivery.Id,
                    VTD1_CSM__c = c.Id
            );
        } catch (Exception e) {
            errorMsg = e.getMessage();
        }
        System.assert(errorMsg.containsIgnoreCase(Label.VTR2_ReplacementItemDifferentShipmentError));
    }
}