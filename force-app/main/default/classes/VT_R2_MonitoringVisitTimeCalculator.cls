/**
* @author: Carl Judge
* @date: 22-Jan-19
* @description: Calculate scheduled date/time for different users/timezones
**/

public without sharing class VT_R2_MonitoringVisitTimeCalculator extends VT_D2_AbstractTimeCalculator{
    protected override Map<String, String> getProfileToFieldsMap(){
        return new Map<String, String> {
                VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME => 'VTD2_PI_scheduled_Time__c',
                VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME => 'VTD2_PG_scheduled_time__c'
        };
    }

    protected override String getBaseTimeField(){
        return 'VTD1_Visit_Start_Date__c';
    }

    protected override Map<Id, Map<String, User>> getUserMap(){
        List<Id> userIds = new List<Id>();
        Map<Id, Map<String, User>> userMap = new Map<Id, Map<String, User>>();

        for (VTD1_Monitoring_Visit__c item : (List<VTD1_Monitoring_Visit__c>)this.records) {
            if (item.VTD1_Assigned_PI__c != null) { userIds.add(item.VTD1_Assigned_PI__c); }
            if (item.VTD1_Assigned_PG__c != null) { userIds.add(item.VTD1_Assigned_PG__c); }
        }

        if (! userIds.isEmpty()) {
            Map<Id, User> uMap = new Map<Id, User>([
                    SELECT Profile.Name, TimeZoneSidKey, LanguageLocaleKey
                    FROM User
                    WHERE Id IN :userIds
            ]);

            for (VTD1_Monitoring_Visit__c item : (List<VTD1_Monitoring_Visit__c>)this.records) {
                if (uMap.containsKey(item.VTD1_Assigned_PI__c)) {
                    addToUserMap(userMap, item.Id, uMap.get(item.VTD1_Assigned_PI__c));
                }
                if (uMap.containsKey(item.VTD1_Assigned_PG__c)) {
                    addToUserMap(userMap, item.Id, uMap.get(item.VTD1_Assigned_PG__c));
                }
            }
        }

        return userMap;
    }
}