@IsTest


public with sharing class VT_D1_ActionItemsControllerTest {



    public static void getActionItemsTest() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        Virtual_Site__c virtualSite = createVirtualSites(new List<HealthCloudGA__CarePlanTemplate__c>{study}, 1).get(study)[0];
        User PG = [SELECT Id FROM User WHERE Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME AND FirstName LIKE 'PrimaryPGUser%' AND IsActive = TRUE LIMIT 1];
        User PI = [SELECT Id FROM User WHERE Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME AND FirstName LIKE 'PIUser%' AND IsActive = TRUE LIMIT 1];

        List<VTD1_Action_Item__c> actionItems = new List<VTD1_Action_Item__c>();
        for (Integer i=0; i<10; i++) {
            VTD1_Action_Item__c ai = new VTD1_Action_Item__c(
                    VTD1_Study__c = study.Id,
                    VTD1_SiteId__c = virtualSite.Id,
                    VTD2_Assigned_To__c = PG.Id,
                    VTD1_PI_Name__c = PI.Id,
                    OwnerId = PI.Id
            );
            actionItems.add(ai);
        }

        Test.startTest();
        insert actionItems;

        System.runAs(PI) {
            List<VT_D1_ActionItemsController.ActionItemWrapper> actionItemWrapperList = VT_D1_ActionItemsController.getActionItems(study.Id);
            System.assert(!actionItemWrapperList.isEmpty());

            if (!actionItemWrapperList.isEmpty()) {
                //VTD1_Action_Item__c actionItem = actionItemList.get(0);
                VT_D1_ActionItemsController.ActionItemWrapper actionItemWrapper = actionItemWrapperList[0];
                VTD1_Action_Item__c actionItem1 = actionItemWrapper.ai;
                String id = actionItemWrapper.Id;
                String name = actionItemWrapper.Name;
                String study1 = actionItemWrapper.Study;
                String subjectId = actionItemWrapper.SubjectId;
                String status = actionItemWrapper.Status;
                String severity = actionItemWrapper.Severity;
                String actionRequired = actionItemWrapper.ActionRequired;
                String assignedTo = actionItemWrapper.AssignedTo;
                String dueDate = actionItemWrapper.DueDate;
                String primaryInvestigator = actionItemWrapper.PrimaryInvestigator;
                String description = actionItemWrapper.Description;
                String dateOccurred = actionItemWrapper.DateOccurred;
                String monitoringVisit = actionItemWrapper.MonitoringVisit;
                //Rajesh
                //START SH-17440
                //String resolutionMethod = actionItemWrapper.ResolutionMethod;
                //END:SH-17440
                String resolutionDescription = actionItemWrapper.ResolutionDescription;
                String resolutionDate = actionItemWrapper.ResolutionDate;
                String notes = actionItemWrapper.Notes;
            }
        }
        Test.stopTest();
        VT_D1_ActionItemsController.getUsersFilter(study.Id);
        VT_D1_ActionItemsController.updateActionItem(actionItems[0].Id, PG.Id, '', '01/01/2000');
    }

    private static Map <HealthCloudGA__CarePlanTemplate__c, List <Virtual_Site__c>> createVirtualSites(List <HealthCloudGA__CarePlanTemplate__c> studies, Integer index) {
        Map <HealthCloudGA__CarePlanTemplate__c, List <Virtual_Site__c>> studyToSitesMap = new Map <HealthCloudGA__CarePlanTemplate__c, List <Virtual_Site__c>>();
        List <Virtual_Site__c> allSites = new List<Virtual_Site__c>();
        Set <Id> ids = new Set<Id>();
        for (HealthCloudGA__CarePlanTemplate__c study : studies) {
            ids.add(study.Id);
        }
        List <Study_Team_Member__c> studyTeamMembers = [select Id, RecordType.Name, Study__c, User__c
                                                        //START RAJESH SH-17440
                                                        /*, VTR2_Remote_CRA__c, VTR2_Onsite_CRA__c*/
                                                        //END:SH-17440
                                                        from Study_Team_Member__c where Study__c in : ids and ((RecordType.Name = 'PI' and VTD1_PIsAssignedPG__c != null) or (RecordType.Name = 'PG')) order by CreatedDate];

        for (Study_Team_Member__c stm : studyTeamMembers) {
            System.debug('stm = ' + stm);
        }

        Map <String, Id> stmToStudyMap = new Map<String, Id>();
        for (Study_Team_Member__c studyTeamMember : studyTeamMembers) {
            stmToStudyMap.put(studyTeamMember.Study__c + '_' + studyTeamMember.RecordType.Name, studyTeamMember.Id);
        }
        for (HealthCloudGA__CarePlanTemplate__c study : studies) {
            createRegDocument(study.Id);
            List <Virtual_Site__c> sites = new List<Virtual_Site__c>();
            for (Integer i = 0; i < 10; i ++) {
                Virtual_Site__c site = new Virtual_Site__c(
                    VTD1_Study__c = study.Id, 
                        VTD1_Study_Site_Number__c = '12345',
                    Name = 'REG_TEST-' + index + '-' + i,
                    VTD1_Study_Team_Member__c = i < 9 ? stmToStudyMap.get(study.Id + '_' + 'PI') : stmToStudyMap.get(study.Id + '_' + 'PG')
                );
                sites.add(site);
                allSites.add(site);
            }
            studyToSitesMap.put(study, sites);
        }
        insert allSites;
        return studyToSitesMap;
    }

    private static void createRegDocument(Id studyId) {
        VTD1_Regulatory_Document__c regulatoryDocument = new VTD1_Regulatory_Document__c(Level__c = 'Site', VTD1_Document_Type__c = 'Interim Visit Report');
        insert regulatoryDocument;
        VTD1_Regulatory_Binder__c binder = new VTD1_Regulatory_Binder__c(VTD1_Care_Plan_Template__c = studyId, VTD1_Regulatory_Document__c = regulatoryDocument.Id);
        insert binder;
    }
}