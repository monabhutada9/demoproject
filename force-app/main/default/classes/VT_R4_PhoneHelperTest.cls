/**
 * Created by user on 1/15/2020.
 */

@isTest
public with sharing class VT_R4_PhoneHelperTest {
    @testSetup
    private static void setupMethod() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }

    @IsTest
    private static void upsertPrimaryPhone() {
        List<Account> accs = [SELECT Id, VTD2_Patient_s_Contact__r.LastName, HealthCloudGA__PrimaryContact__r.LastName FROM Account];
        Contact c = new Contact(
                VTD1_Account__c = accs[0].Id,
                FirstName    = 'Test',
                LastName     = 'TestTest',
                Email = 'werwe@gasdas.com',
                VTD1_Primary_CG__c = true
        );
        insert c;
        List<Contact> cons = [
                SELECT Id, VTD1_Primary_CG__c, VTD1_Account__c, LastName
                FROM Contact
                WHERE VTD1_Primary_CG__c = true
                AND VTD1_Account__c =: accs[0].Id
        ];
        System.debug('***Contacts ' + cons);
        VT_D1_Phone__c phone = new VT_D1_Phone__c(
                Account__c = cons[0].VTD1_Account__c,
                VTD1_Contact__c = cons[0].Id,
                PhoneNumber__c = '+7123123123',
                IsPrimaryForPhone__c = true
        );
        insert phone;
        VT_D1_Phone__c phone1 = new VT_D1_Phone__c(
                Account__c = cons[0].VTD1_Account__c,
                VTD1_Contact__c = cons[0].Id,
                PhoneNumber__c = '+1778787878',
                IsPrimaryForPhone__c = true
        );
        Database.SaveResult insertResult = Database.insert(phone1, false);
        System.assertEquals(Label.VTR4_OnePrimaryPhone, insertResult.getErrors().get(0).getMessage());
        VT_D1_Phone__c phone2 = new VT_D1_Phone__c(
                Account__c = cons[0].VTD1_Account__c,
                VTD1_Contact__c = cons[0].Id,
                PhoneNumber__c = '+17777777'
        );
        insert phone2;
        phone2.IsPrimaryForPhone__c = true;
        Database.SaveResult updateResult = Database.update(phone2, false);
        System.assertEquals(Label.VTR4_OnePrimaryPhone, updateResult.getErrors().get(0).getMessage());
    }
}