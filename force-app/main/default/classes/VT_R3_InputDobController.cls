/**
 * Created by O. Berehovskyi on 06-Feb-20.
 */

public without sharing class VT_R3_InputDobController {

    @AuraEnabled
    public static Contact getContact(Id contactId) {
        try {
            List<Contact> contacts = [
                    SELECT Id
                            , Birthdate
                            , VTD1_UserId__r.LanguageLocaleKey
                            , VTD1_Clinical_Study_Membership__r.VTD2_Study_Geography__c
                            , VTD1_Clinical_Study_Membership__r.VTD2_Study_Geography__r.VT_R3_Date_Of_Birth_Restriction__c
                    FROM Contact
                    WHERE Id = :contactId
            ];

            return contacts[0];
        } catch (Exception exc) {
            AuraHandledException auraExc = new AuraHandledException('');
            auraExc.setMessage(exc.getTypeName() + ' ' + exc.getMessage());
            throw auraExc;
        }

    }

    @AuraEnabled
    public static VTD2_Study_Geography__c getStudyGeography(Id studyId, String countryCode) {
        VTD2_Study_Geography__c geography;

        if (studyId != null && countryCode != null) {
            try {
                String wildCountryCode = '%' + countryCode + '%';
                List<VTD2_Study_Geography__c> geographies = [
                        SELECT Id, VT_R3_Date_Of_Birth_Restriction__c
                        FROM VTD2_Study_Geography__c
                        WHERE VTD2_Study__c = :studyId
                        AND VTD2_Geographical_Region__c = 'Country'
                        AND VTR3_Country__c LIKE :wildCountryCode
                ];
                if (!geographies.isEmpty()) {
                    geography = geographies[0];
                }
            } catch (Exception exc) {
                AuraHandledException auraExc = new AuraHandledException('');
                auraExc.setMessage(exc.getTypeName() + ' ' + exc.getMessage());

                throw auraExc;
            }
        }

        return geography;
    }
}