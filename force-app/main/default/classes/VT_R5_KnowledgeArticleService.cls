/**
 * Created by Yuliya Yakushenkova on 7/16/2020.
 */

public class VT_R5_KnowledgeArticleService {

    private static final String DEFAULT_LANGUAGE = 'en_US';

    public static final String STUDY_DETAILS_ARTICLE_TYPE = 'Study_Details';
    public static final String PRIVACY_POLICY_ARTICLE_TYPE = 'Privacy_Policy';
    public static final String PRIVACY_NOTICE_ARTICLE_TYPE = 'Privacy_Notice';
    public static final String TRAINING_MATERIALS_ARTICLE_TYPE = 'Training_Materials';
    public static final String HIPAA_ARTICLE_TYPE = 'HIPAA';
    public static final String EULA_ARTICLE_TYPE = 'EULA';

    public static final List<String> TERM_AND_CONDITIONS_ARTICLE_TYPES = new List<String>{
            EULA_ARTICLE_TYPE,
            PRIVACY_POLICY_ARTICLE_TYPE
    };

    private String articleId;
    private String articleType;
    private String primaryLanguage;
    private String patientStudyId;
//    private String irbDefaultApprovedLanguage;
//    private List<String> languageCondition;

    public VT_R5_KnowledgeArticleService(String primaryLanguage, String patientStudyId
//            , String irbDefaultApprovedLanguage,
//            List<String> irbApprovedLanguages
    ) {
        this.primaryLanguage = primaryLanguage;
        this.patientStudyId = patientStudyId;
//        this.irbDefaultApprovedLanguage =
//                String.isNotBlank(irbDefaultApprovedLanguage) ? irbDefaultApprovedLanguage : DEFAULT_LANGUAGE;
//        this.languageCondition = irbApprovedLanguages;
    }

    public KnowledgeArticles getArticle(String articleId, String articleType) {
        this.articleId = articleId;
        this.articleType = articleType;
        return getKnowledgeArticle();
    }

    public List<KnowledgeArticles> getArticlesPreview(List<String> articleTypes) {
        return getPreviewKnowledgeArticles(articleTypes);
    }

    public KnowledgeArticles getKnowledgeArticle() {
        List<String> typeCondition = String.isBlank(articleId) ? new List<String>{
                articleType
        } : null;

        KnowledgeArticles knowledgeArticle;

        for (Knowledge__kav article : getKnowledgeData(typeCondition, null)) {
            if (article.Language == primaryLanguage || String.isNotBlank(articleId)) {
                knowledgeArticle = new KnowledgeArticles(
                        article.Id, article.Title, article.VTD1_Content__c, article.Summary
                );
            }
        }
        return knowledgeArticle;
    }

    public List<KnowledgeArticles> getPreviewKnowledgeArticles(List<String> articleTypes) {
        Map<String, List<KnowledgeArticles>> knowledgeArticlesByType = new Map<String, List<KnowledgeArticles>>{
                PRIVACY_POLICY_ARTICLE_TYPE => new List<KnowledgeArticles>(),
                EULA_ARTICLE_TYPE => new List<KnowledgeArticles>()
        };
        List<KnowledgeArticles> ka;
        for (Knowledge__kav article : getArticlesBasedOnLanguages(articleTypes).values()) {
            ka = knowledgeArticlesByType.get(article.VTD2_Type__c);
            ka.add(new KnowledgeArticles(article.Id, article.Title, article.Language));
            knowledgeArticlesByType.put(article.VTD2_Type__c, ka);
        }
        List<KnowledgeArticles> knowledgeArticles = new List<KnowledgeArticles>();
        for (List<KnowledgeArticles> listArticles : knowledgeArticlesByType.values()) {
            knowledgeArticles.addAll(listArticles);
        }
        return knowledgeArticles;
    }

    public Map<String, Knowledge__kav> getArticlesBasedOnLanguages(List<String> articleTypes) {
        Map<String, Knowledge__kav> articlesByArticleId = new Map<String, Knowledge__kav>();
        for (Knowledge__kav article : getKnowledgeData(articleTypes, null)) {
            if (article.Language == primaryLanguage) {
                if (articlesByArticleId.containsKey(article.KnowledgeArticleId)) {
                    if (articlesByArticleId.get(article.KnowledgeArticleId).Language != primaryLanguage) {
                        articlesByArticleId.put(article.KnowledgeArticleId, article);
                    }
                } else {
                    articlesByArticleId.put(article.KnowledgeArticleId, article);
                }
            }
        }
        return articlesByArticleId;
    }

    public List<Knowledge__kav> getKnowledgeData(List<String> typeCondition, List<String> languageCondition) {
        QueryBuilder.ComplexCondition complex = new QueryBuilder.ComplexCondition();
        if (typeCondition != null) {
            complex
                    .andCondition(new QueryBuilder.CompareCondition(Knowledge__kav.PublishStatus).eq('Online'))
                    .andCondition(new QueryBuilder.CompareCondition(Knowledge__kav.VTD2_Study__c).eq(patientStudyId))
                    .andCondition(new QueryBuilder.CompareCondition(Knowledge__kav.IsLatestVersion).eq(true))
                    .andCondition(new QueryBuilder.InCondition('VTD2_Type__c').inCollection(typeCondition));
            if (languageCondition != null) {
                complex.andCondition(new QueryBuilder.InCondition('Language').inCollection(languageCondition));
            }
        } else {
            complex
                    .andCondition(new QueryBuilder.CompareCondition(Knowledge__kav.Id).eq(articleId));
        }
        //System.debug(JSON.serializePretty([SELECT VTD2_Type__c, VTD1_Content__c, Language, Title, Summary, KnowledgeArticleId FROM Knowledge__kav WHERE (PublishStatus = 'Online' AND VTD2_Study__c = 'a090E000005pyMuQAI' AND IsLatestVersion = true AND VTD2_Type__c IN ('EULA', 'Privacy_Policy') AND Language IN ('en_US', 'en_US', 'de')) ORDER BY Title ASC]));
        List<Knowledge__kav> articles = new QueryBuilder(Knowledge__kav.class)
                .namedQueryRegister('VT_R5_KnowledgeArticleService.getKnowledgeData')
                .addField(Knowledge__kav.VTD2_Type__c)
                .addField(Knowledge__kav.VTD1_Content__c)
                .addField(Knowledge__kav.Language)
                .addField(Knowledge__kav.Title)
                .addField(Knowledge__kav.Summary)
                .addField(Knowledge__kav.KnowledgeArticleId)
                .addOrderAsc('Title')
                .addConditions()
                .add(complex)
                .endConditions()
                .preview().toList();
        System.debug(JSON.serializePretty(articles));
        return articles;
    }

    public class KnowledgeArticles {
        private String id;
        private String title;
        private String content;
        private String summary;
        private String language = '';

        public KnowledgeArticles(String id, String title, String language) {
            setPreviewForArticle(id, title, language);
        }

        public KnowledgeArticles(String id, String title, String content, String summary) {
            setArticle(id, title, content, summary);
        }

        public void setArticle(String id, String title, String content, String summary) {
            this.id = id;
            this.title = title;
            this.content = content;
            this.summary = summary == null ? '' : summary;
        }

        public void setPreviewForArticle(String id, String title, String language) {
            this.id = id;
            this.title = title;
            this.language = language;
        }
    }
}