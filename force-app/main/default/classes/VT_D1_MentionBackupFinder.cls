public without sharing class VT_D1_MentionBackupFinder {

    List<VT_D1_Mention> mentions = new List<VT_D1_Mention>();
    Map<String, List<VT_D1_Mention>> mentionsByParentType = new Map<String, List<VT_D1_Mention>>();

    Map<Id, List<Case>> recIdToCasesMap = new Map<Id, List<Case>>();

    public VT_D1_MentionBackupFinder(List<VT_D1_Mention> mentions) {
        this.mentions = mentions;
    }

    public void getBackupUsers() {
        this.mentionsByParentType = VT_D1_MentionHelper.getMentionsByParentType(this.mentions);
        if (mentionsByParentType.containsKey('Case')) { processCaseMentions(); }
        if (mentionsByParentType.containsKey('VTD1_Actual_Visit__c')) { processActualVisitMentions(); }
        if (mentionsByParentType.containsKey('User')) { processUserMentions(); }

        addBackupsToMentions();
    }

    private static List<Id> getParentIds(List<VT_D1_Mention> mentions) {
        List<Id> parentIds = new List<Id>();
        for (VT_D1_Mention item : mentions) {
            parentIds.add(item.parentId);
        }
        return parentIds;
    }

    private static List<Id> getOooUserIds(List<VT_D1_Mention> mentions) {
        List<Id> oooUserIds = new List<Id>();
        for (VT_D1_Mention item : mentions) {
            oooUserIds.addAll(item.oooUserIds);
        }
        return oooUserIds;
    }

    private void addBackupsToMentions() {
        for (VT_D1_Mention mention : this.mentions) {
            mention.backupUserIdsByOooUsers = new Map<Id, Set<User>>();

            if (this.recIdToCasesMap.containsKey(mention.ParentId)) {
                for (Case cas : this.recIdToCasesMap.get(mention.ParentId)) {
                    if (mention.oooUserIds.contains(cas.VTD1_Primary_PG__c)) {
                        if (!mention.backupUserIdsByOooUsers.containsKey(cas.VTD1_Secondary_PG__c)) {
                            mention.backupUserIdsByOooUsers.put(cas.VTD1_Secondary_PG__c, new Set<User>());
                        }
                        mention.backupUserIdsByOooUsers.get(cas.VTD1_Secondary_PG__c).add(cas.VTD1_Primary_PG__r);
                    }
                }
            }
        }
    }

    private void addCaseToMap(Id recId, Case cas) {
        if (!this.recIdToCasesMap.containsKey(recId)) {
            this.recIdToCasesMap.put(recId, new List<Case>());
        }
        this.recIdToCasesMap.get(recId).add(cas);
    }

    private void processCaseMentions() {
        for (Case item : [
            SELECT Id, VTD1_Primary_PG__c, VTD1_Secondary_PG__c, VTD1_Primary_PG__r.Id, VTD1_Primary_PG__r.Name
            FROM Case
            WHERE Id IN :getParentIds(this.mentionsByParentType.get('Case'))
            AND VTD1_Primary_PG__c IN :getOooUserIds(this.mentionsByParentType.get('Case'))
            AND VTD1_Secondary_PG__c != null
        ]) {
            addCaseToMap(item.Id, item);
        }
    }

    private void processActualVisitMentions() {
        for (VTD1_Actual_Visit__c item : [
            SELECT Id, VTD1_Case__c, VTD1_Case__r.VTD1_Secondary_PG__c, VTD1_Case__r.VTD1_Primary_PG__c,
                VTD1_Case__r.VTD1_Primary_PG__r.Id, VTD1_Case__r.VTD1_Primary_PG__r.Name
            FROM VTD1_Actual_Visit__c
            WHERE Id IN :getParentIds(this.mentionsByParentType.get('VTD1_Actual_Visit__c'))
            AND VTD1_Case__r.VTD1_Primary_PG__c IN :getOooUserIds(this.mentionsByParentType.get('VTD1_Actual_Visit__c'))
            AND VTD1_Case__r.VTD1_Secondary_PG__c != null
        ]) {
            addCaseToMap(item.Id, item.VTD1_Case__r);
        }
    }

    private void processUserMentions() {
        for (Case item : [
            SELECT Id, VTD1_Primary_PG__c, VTD1_Secondary_PG__c, VTD1_Primary_PG__r.Id, VTD1_Primary_PG__r.Name
            FROM Case
            WHERE VTD1_Primary_PG__c IN :getParentIds(this.mentionsByParentType.get('User'))
            AND VTD1_Primary_PG__c IN :getOooUserIds(this.mentionsByParentType.get('User'))
            AND VTD1_Secondary_PG__c != null
        ]) {
            addCaseToMap(item.VTD1_Primary_PG__c, item);
        }
    }
}