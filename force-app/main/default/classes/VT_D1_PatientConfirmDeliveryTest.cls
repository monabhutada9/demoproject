@IsTest
private class VT_D1_PatientConfirmDeliveryTest {
    private static Case cas;
    private static User u;

    @testSetup
    static void setup() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createProtocolDelivery(study.Id);
        //HealthCloudGA__CandidatePatient__c candidatePatient = VT_D1_TestUtils.createPatientCandidate('Mike', 'Birn', 'mikebirn@test.com', 'mikebirn@test.com', 'mikebirn', 'Converted', study.Id, 'PN', '11-11-11', 'US', 'NY');
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();

        List<Case> casesList = [SELECT VTD1_Patient_User__c, VTD1_Patient__r.VTD1_Patient_Name__c, VTD1_Study__c FROM Case];
        if(casesList.size() > 0){
            cas = casesList.get(0);
            VT_R4_GenerateActualVisitsAndKits generator = new VT_R4_GenerateActualVisitsAndKits();
            generator.executeDeliveriesAndKits(new List<Id> {cas.Id});
        }
    }

    static {
        List<Case> casesList = [SELECT VTD1_Patient_User__c FROM Case];
        if(casesList.size() > 0){
            cas = casesList.get(0);
            String userId = cas.VTD1_Patient_User__c;
            u = [SELECT Id, ContactId FROM User WHERE Id =: userId];
        }
    }

    @IsTest
    static void getKitContentsTest() {
        System.assertNotEquals(null, cas);
        System.assertNotEquals(null, u);
        System.runAs(u) {
        List<VTD1_Order__c> deliveryList = [SELECT Id FROM VTD1_Order__c WHERE VTD1_Case__c =: cas.Id];
        System.assert(deliveryList.size() > 0);
        VTD1_Order__c delivery = deliveryList.get(0);
        /*User userSysAdmin = [SELECT ContactId FROM User WHERE Profile.Name = 'System Administrator' AND Id != :UserInfo.getUserId() AND IsActive = true LIMIT 1];
        System.runAs(userSysAdmin) {
            delivery.VTD1_Status__c = 'Pending Shipment';
            update delivery;
        }*/

        String deliveryId = delivery.Id;

        Task task = new Task();
        task.OwnerId = cas.VTD1_Patient_User__c;
        task.Status = 'Open';
        task.WhatId = deliveryId;
        insert task;

            VTD1_Patient_Kit__c pKit = new VTD1_Patient_Kit__c(
                VTD2_MaterialId__c = '1234',
                VTD1_IntegrationId__c = 'abcd',
                VTD1_Patient_Delivery__c = deliveryId,
                VTD1_Case__c = cas.Id
            );
            insert pKit;

            Test.startTest();
                String deliveryDescriptionWithKitItemsString = VT_D1_PatientConfirmDeliveryController.getKitContents(deliveryId);

            VT_D1_PatientConfirmDeliveryController.DeliveryDescriptionWithKitItems deliveryDescriptionWithKitItems = (VT_D1_PatientConfirmDeliveryController.DeliveryDescriptionWithKitItems) JSON.deserialize(deliveryDescriptionWithKitItemsString, VT_D1_PatientConfirmDeliveryController.DeliveryDescriptionWithKitItems.class);

            List<VT_D1_PatientConfirmDeliveryController.KitItem> kitItems = deliveryDescriptionWithKitItems.kitItems;
            System.assert(kitItems.size() > 0);

            List<VT_D1_PatientConfirmDeliveryController.DeviceItem> deviceItems = new List<VT_D1_PatientConfirmDeliveryController.DeviceItem>();
            VT_D1_PatientConfirmDeliveryController.DeviceItem deviceItem = new VT_D1_PatientConfirmDeliveryController.DeviceItem();
            deviceItems.add(deviceItem);
            VT_D1_PatientConfirmDeliveryController.KitItem kitItem = kitItems.get(0);
            kitItem.deviceItems = deviceItems;
            kitItem.kitContainsAllContents = 'true';
            kitItem.kitContentsDamaged = 'true';

            String kitItemsString = JSON.serialize(kitItems);
            String deliveryString = JSON.serialize(deliveryDescriptionWithKitItems.delivery);
            VT_D1_PatientConfirmDeliveryController.updateDelivery(deliveryString, kitItemsString, false, 'true', 'true');

            kitItem.deviceItems = deviceItems;
            kitItem.kitContainsAllContents = 'false';
            kitItem.kitContentsDamaged = 'false';
            String kitItemsString1 = JSON.serialize(kitItems);
            VT_D1_PatientConfirmDeliveryController.updateDelivery(deliveryString, kitItemsString1, true, 'false', 'false');
            String deliveryDescriptionWithKitItemsString1 = VT_D1_PatientConfirmDeliveryController.getKitContents(deliveryId);
            VT_D1_PatientConfirmDeliveryController.DeliveryDescriptionWithKitItems deliveryDescriptionWithKitItems1 = (VT_D1_PatientConfirmDeliveryController.DeliveryDescriptionWithKitItems) JSON.deserialize(deliveryDescriptionWithKitItemsString1, VT_D1_PatientConfirmDeliveryController.DeliveryDescriptionWithKitItems.class);


            String adHocDeliveryRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'VTD1_Ad_hoc_Packaging_Materials' AND SobjectType = 'VTD1_Order__c'].Id;
            VTD1_Order__c delivery1 = deliveryDescriptionWithKitItems.delivery;
            delivery1.RecordTypeId = adHocDeliveryRecordTypeId;
            update delivery1;
            String deliveryString1 = JSON.serialize(delivery1);

            kitItem.deviceItems = deviceItems;
            kitItem.kitContainsAllContents = 'true';
            kitItem.kitContentsDamaged = 'true';
            String kitItemsString2 = JSON.serialize(kitItems);
            VT_D1_PatientConfirmDeliveryController.updateDelivery(deliveryString1, kitItemsString2, true, 'true', 'true');
            String deliveryDescriptionWithKitItemsString2= VT_D1_PatientConfirmDeliveryController.getKitContents(deliveryId);
            VT_D1_PatientConfirmDeliveryController.DeliveryDescriptionWithKitItems deliveryDescriptionWithKitItems2 = (VT_D1_PatientConfirmDeliveryController.DeliveryDescriptionWithKitItems) JSON.deserialize(deliveryDescriptionWithKitItemsString2, VT_D1_PatientConfirmDeliveryController.DeliveryDescriptionWithKitItems.class);
            Test.stopTest();
        }
    }
}