/**
 * Created by user on 29.04.2019.
 */

global class VT_R2_BatchRemoveNotifications implements Database.Batchable<sObject>, Database.Stateful, Schedulable {

    global Map<Id, Integer> patientNotificationCounter;

    global VT_R2_BatchRemoveNotifications() {
        patientNotificationCounter = new Map<Id, Integer>();
        AggregateResult [] result = [select count(Id) cnt, OwnerId from VTD1_NotificationC__c group by OwnerId];
    }

    /*
        Public Interface
    */
    global static void scheduleDaily() {
        VT_R2_BatchRemoveNotifications b = new VT_R2_BatchRemoveNotifications();
        System.schedule('VT_R2_BatchRemoveNotifications', '0 0 0 * * ?', b);
    }

    global static void scheduleWeekly() {

    }

    /*
        Scheduler Interface
    */
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new VT_R2_BatchRemoveNotifications());
    }

    /*
        Batch Interface
    */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //Date boundDate = System.today().addDays(-3);
        //String formattedDate = ((Datetime)boundDate).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        //String query='select Id, OwnerId, CreatedDate, Owner.Profile.Name from VTD1_NotificationC__c where VTD1_Read__c = true and CreatedDate < ' + formattedDate + ' order by VTD1_Receivers__c, CreatedDate desc';
//        'select Id, OwnerId, CreatedDate, Owner.Profile.Name from VTD1_NotificationC__c where VTD1_Read__c = true order by VTD1_Receivers__c, CreatedDate desc'
        return new QueryBuilder(VTD1_NotificationC__c.class)
                .addFields('Id, OwnerId, CreatedDate, Owner.Profile.Name')
                .addField(VTD1_NotificationC__c.VTR3_Recurrence_Number__c)
                .addField(VTD1_NotificationC__c.VTD1_Receivers__c)
                .addField('VTR3_Study_Notification__r.VTR3_Number_of_Occurrences__c')
                .addConditions()
                .add(new QueryBuilder.CompareCondition(VTD1_NotificationC__c.VTD1_Read__c).eq(true))
                .add(new QueryBuilder.CompareCondition(VTD1_NotificationC__c.VTD1_Receivers__c).eq(VTD1_RTId__c.getInstance().VTR2_Backend_Logic_User_ID__c))
                .setConditionOrder('1 OR 2')
                .endConditions()
                .addOrderDesc(VTD1_NotificationC__c.VTD1_Receivers__c)
                .addOrderDesc(VTD1_NotificationC__c.CreatedDate)
                .preview()
                .toQueryLocator();
    }

    global void execute(Database.BatchableContext BC, List<VTD1_NotificationC__c> scope) {
        String backendLogicUserID = VTD1_RTId__c.getInstance().VTR2_Backend_Logic_User_ID__c;
        //read configurations
        Integer maxNumberPerUser = 50;
        Integer daysToExpire = 14;
        VTR2_NotificationsRemovePolicy__c policy = VTR2_NotificationsRemovePolicy__c.getValues('Standard');
        if (policy != null) {
            maxNumberPerUser = (Integer) policy.MaxNumberPerUser__c;
            daysToExpire = (Integer) policy.DaysToExpire__c;
        }

        System.debug(maxNumberPerUser + ' ' + daysToExpire);

        List <VTD1_NotificationC__c> notificationsToDelete = new List<VTD1_NotificationC__c>();
        List <VTD1_NotificationC__c> studyNotificationsToDelete = new List<VTD1_NotificationC__c>();
        Date today = System.today();

        for (VTD1_NotificationC__c notification : scope) {
            if (notification.VTD1_Receivers__c == backendLogicUserID) {
                notificationsToDelete.add(notification);
                continue;
            }

            Integer count = patientNotificationCounter.get(notification.OwnerId);
            if (count == null) {
                count = 1;
            } else {
                count++;
            }
            patientNotificationCounter.put(notification.OwnerId, count);
            System.debug(notification.OwnerId + ' ' + notification.CreatedDate + ' ' + count);
            if (notification.VTR3_Study_Notification__c != null) {
                studyNotificationsToDelete.add(notification);
                continue;
            }
            if (!isPatientOrCaregiver(notification.Owner.Profile.Name)
                    && notification.CreatedDate.date().addDays(daysToExpire - 1) < today || count > maxNumberPerUser) {
                notificationsToDelete.add(notification);
            }
        }
        for (VTD1_NotificationC__c notification : notificationsToDelete) {
            System.debug('to delete ' + notification);
        }
        if (!notificationsToDelete.isEmpty()) {
            delete notificationsToDelete;
        }
        if (!studyNotificationsToDelete.isEmpty()) {
            new OldStudyNotificationsCleanService(studyNotificationsToDelete).deleteNotifications();
        }
    }

    global void finish(Database.BatchableContext BC) {
        for (Id id : patientNotificationCounter.keySet()) {
            //System.debug('id = ' + id + ' ' + patientNotificationCounter.get(id));
        }
    }

    /**
     * Class for removing Study Notification related NotificationC
     */
    @TestVisible
    private without sharing class OldStudyNotificationsCleanService {

        @TestVisible
        public List<VTD1_NotificationC__c> scope;
        @TestVisible
        public List<VTD1_NotificationC__c> notificationsToDelete;

        public OldStudyNotificationsCleanService(List<VTD1_NotificationC__c> scope) {
            this.scope = this.filterNotifications(scope);
            this.notificationsToDelete = new List<VTD1_NotificationC__c>();
        }

        public void deleteNotifications() {
            if (scope.isEmpty()) {
                return;
            }
            Set<Id> receivers = new Set<Id>();
            for (VTD1_NotificationC__c step : this.scope){
                receivers.add(step.VTD1_Receivers__c);
            }

            List<Map<Id, List<VTD1_NotificationC__c>>> usersNotificationC = new List<Map<Id, List<VTD1_NotificationC__c>>>();
            for (Id userId : receivers) {
                Map<Id, List<VTD1_NotificationC__c>> listUserNotifications = new Map<Id, List<VTD1_NotificationC__c>>();
                listUserNotifications.put(userId, new List<VTD1_NotificationC__c>());
                for (VTD1_NotificationC__c item: this.scope){
                    if(item.VTD1_Receivers__c == userId){
                        listUserNotifications.get(userId).add(item);
                    }
                }
                usersNotificationC.add(listUserNotifications);
            }

            for (Map<Id, List<VTD1_NotificationC__c>> mapUser : usersNotificationC){
                Set<Id> userSetId = mapUser.keySet();
                List<Id> userId = new List<Id>(userSetId);
                List<VTD1_NotificationC__c> stepList = mapUser.get(userId.get(0));
                List<VTD1_NotificationC__c> finishedNotifications = new List<VTD1_NotificationC__c>();
                for (VTD1_NotificationC__c ls : stepList){
                    if(ls.VTR3_Recurrence_Number__c == ls.VTR3_Study_Notification__r.VTR3_Number_of_Occurrences__c){
                        finishedNotifications.add(ls);
                    }
                }
                for (Integer a = 0; a < stepList.size()-1; a++){
                    if (!finishedNotifications.isEmpty()) {
                        for (VTD1_NotificationC__c finished :finishedNotifications){
                            if (finished.Id == stepList.get(a).Id) {
                                this.notificationsToDelete.add(stepList.get(a));
                                stepList.remove(a);
                            }
                        }
                    }
                }

                if (stepList.size() > 1) {
                    this.findTechnicalNotifications(stepList);
                }
            }
            delete this.notificationsToDelete;
        }

        private List<VTD1_NotificationC__c> filterNotifications(List<VTD1_NotificationC__c> scope) {
            List<VTD1_NotificationC__c> result = new List<VTD1_NotificationC__c>();
            for (VTD1_NotificationC__c notification : scope) {
                if (String.isNotEmpty(notification.VTR3_Study_Notification__c)) {
                    result.add(notification);
                }
            }
            return result;
        }

        @TestVisible
        private void findTechnicalNotifications(List<VTD1_NotificationC__c> notifications) {
            List<Map<Id, List<VTD1_NotificationC__c>>> technicalNotification = new List<Map<Id, List<VTD1_NotificationC__c>>>();
            Set<Id> setStudyNotifications = new Set<Id>();
            for (VTD1_NotificationC__c step : notifications) {
                setStudyNotifications.add(step.VTR3_Study_Notification__c);
            }
            List<Id> StudyNotificationsList = new List<Id>(setStudyNotifications);

            for (Integer count = 0; count <= setStudyNotifications.size() - 1; count++) {
                Map<Id, List<VTD1_NotificationC__c>> studyNotifications = new Map<Id, List<VTD1_NotificationC__c>>();
                studyNotifications.put(StudyNotificationsList.get(count), new List<VTD1_NotificationC__c>());
                for (Integer step = 0; step < notifications.size(); step++) {
                    if (StudyNotificationsList.get(count) == notifications.get(step).VTR3_Study_Notification__c) {
                        studyNotifications.get(StudyNotificationsList.get(count)).add(notifications.get(step));
                    }
                }
                if (studyNotifications.get(StudyNotificationsList.get(count)).size() > 1) {
                    technicalNotification.add(studyNotifications);
                }
            }
            Integer check = 0;
            for (Map<Id, List<VTD1_NotificationC__c>> item : technicalNotification) {
                List<VTD1_NotificationC__c> technicals = item.get(StudyNotificationsList.get(check));
                check++;
                if (technicals != null && technicals.size() > 1) {
                    List<NotificationC> sortNotificationList = new List<NotificationC>();
                    for (VTD1_NotificationC__c tt : technicals) {
                        sortNotificationList.add(new NotificationC(tt));
                    }
                    sortNotificationList.sort();
                    for (Integer cc = 1; cc < sortNotificationList.size(); cc++) {
                        if (cc < sortNotificationList.size()) {
                            this.notificationsToDelete.add((VTD1_NotificationC__c) sortNotificationList.get(cc).notification);
                        }
                    }
                }
            }
        }
    }

    @TestVisible
    private class NotificationC implements Comparable {

        public final VTD1_NotificationC__c notification;

        public NotificationC(VTD1_NotificationC__c notification) {
            this.notification = notification;
        }

        public Integer compareTo(Object obj) {
            NotificationC compareToNotification = (NotificationC) obj;
            Integer returnValue = 0;
            if (notification.VTR3_Recurrence_Number__c < compareToNotification.notification.VTR3_Recurrence_Number__c) {
                returnValue = 1;
            } else if (notification.VTR3_Recurrence_Number__c > compareToNotification.notification.VTR3_Recurrence_Number__c) {
                returnValue = -1;
            }
            return returnValue;
        }
    }

    /**
     * Checks if passed profile name is Patient or Caregiver
     * @param profileName profile name
     * @return returns true if the passed user is Patient or Caregiver
     */
    public static Boolean isPatientOrCaregiver(final String profileName) {
        return profileName == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
                || profileName == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME;
    }
}