/**
* @author: Carl Judge
* @date: 24-Sep-19
* @description: 
**/

public without sharing class VT_R3_GlobalSharingLogic_Chat extends VT_R3_AbstractGlobalSharingLogic {
    public override Type getType() {
        return VT_R3_GlobalSharingLogic_Chat.class;
    }

    public override VTR3_GlobalSharingConfig__mdt getConfig() {
        return getConfigForObjectType(VTD1_Chat__c.getSObjectType().getDescribe().getName());
    }

    protected override void addShareDetailsForRecords() {
        for (VTD1_Chat_Member__c member : [
            SELECT VTD1_Chat__c, VTD1_User__c, VTD1_Chat__r.VTR2_Study__c
            FROM VTD1_Chat_Member__c
            WHERE VTD1_Chat__c IN :recordIds
        ]) {
            addShareDetail(member.VTD1_Chat__c, member.VTD1_User__c, member.VTD1_Chat__r.VTR2_Study__c, 'Edit');
        }
    }

    protected override Iterable<Object> getUserBatchIterable() {
        return (Iterable<Object>) Database.getQueryLocator(getUserQuery());
    }

    protected override List<SObject> executeUserQuery() {
        String query = getUserQuery() + ' LIMIT ' + getAdjustedQueryLimit();
        return Database.query(query);
    }

    protected override void createUserShareDetailsFromRecords(List<SObject> records) {
        for (SObject memberObj : records) {
            VTD1_Chat_Member__c member = (VTD1_Chat_Member__c)memberObj;
            addShareDetail(member.VTD1_Chat__c, member.VTD1_User__c, member.VTD1_Chat__r.VTR2_Study__c, 'Edit');
        }
    }

    private String getUserQuery() {
        String query = 'SELECT VTD1_Chat__c, VTD1_User__c, VTD1_Chat__r.VTR2_Study__c' +
            ' FROM VTD1_Chat_Member__c' +
            ' WHERE VTD1_User__c IN :includedUserIds';
        String scopeCondition = getScopeCondition();
        if (scopeCondition != null) { query += ' AND VTD1_Chat__r.' + scopeCondition; }
        System.debug(query);
        return query;
    }

    protected override String getRemoveQuery() {
        return 'SELECT Id FROM VTD1_Chat__Share WHERE Parent.'
            + getScopeCondition()
            + ' AND UserOrGroupId IN :includedUserIds';
    }

    private String getScopeCondition() {
        String scopeCondition;
        if (scopedStudyIds != null) {
            scopeCondition = 'VTR2_Study__c IN :scopedStudyIds';
        }
        if (scopeType == VTD1_Chat__c.getSObjectType().getDescribe().getName()) {
            scopeCondition = 'Id IN :scopeRecordIds';
        }
        return scopeCondition;
    }
    // 
}