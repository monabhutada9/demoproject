/**
 * Created by User on 19/05/20.
 */
@isTest
public with sharing class VT_D1_EmailMessageTriggerTest {

    public static void doTest(){
        DomainObjects.EmailMessage_t message = new DomainObjects.EmailMessage_t()
                .setSubject('test')
                .setStatus('3')
                .setToAddress('heim@elastify.eu')
                .setFromAddress(UserInfo.getUserEmail())
                .setFromName(UserInfo.getFirstName()+' - '+UserInfo.getLastName())
                .setHtmlBody('a body')
                .setIncoming(false)
                .setMessageDate(Datetime.now());
        Test.startTest();
        message.persist();
        Boolean hasException = false;
        try {
            delete message.toObject();
        }catch (Exception ex){
            hasException = true;
        }
        System.assert(hasException);
        Test.stopTest();
    }
}