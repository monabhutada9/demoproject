/**
* @author: Carl Judge
* @date: 11-Oct-18
* @description: separate class to use without sharing
**/

public without sharing class VT_D2_MedicalRecordsCertifyDocsHelper {
    public static ContentDocumentLink getCertifyContentDocumentLink(Id caseId) {
        ContentDocumentLink link;

        Case cas = [
                SELECT VTD1_Study__r.VTD2_Certification_form_Medical_Records__c, VTD1_Study__r.Id
                FROM Case
                WHERE Id = :caseId
        ];

        if (cas.VTD1_Study__r.VTD2_Certification_form_Medical_Records__c != null) {
            VTD1_RTId__c rtIds = VTD1_RTId__c.getInstance();
            Set<Id> profileIds = new Set<Id>{
                    rtIds.VTD2_Profile_Patient__c, rtIds.VTD2_Profile_Caregiver__c, rtIds.VTD2_Profile_SCR__c
            };
            String userLanguageSuffix = profileIds.contains(UserInfo.getProfileId()) ? '_' + UserInfo.getLanguage() : '';
            for (ContentDocumentLink item : [
                    SELECT ContentDocument.LatestPublishedVersionId, ContentDocument.LatestPublishedVersion.Title,
                            ShareType, Visibility
                    FROM ContentDocumentLink
                    WHERE LinkedEntityId = :cas.VTD1_Study__r.Id
            ]) {
                system.debug('item Title : '+item.ContentDocument.LatestPublishedVersion.Title);
                system.debug('study med record : '+cas.VTD1_Study__r.VTD2_Certification_form_Medical_Records__c);
                system.debug('userLanguageSuffix : '+userLanguageSuffix);
                if (item.ContentDocument.LatestPublishedVersion.Title == cas.VTD1_Study__r.VTD2_Certification_form_Medical_Records__c + userLanguageSuffix) {
                    link = item;
                    break;
                }
                if (profileIds.contains(UserInfo.getProfileId())) {
                    if (link == null && item.ContentDocument.LatestPublishedVersion.Title == cas.VTD1_Study__r.VTD2_Certification_form_Medical_Records__c) {
                        link = item;
                    }
                    if (item.ContentDocument.LatestPublishedVersion.Title == cas.VTD1_Study__r.VTD2_Certification_form_Medical_Records__c + '_en_US') {
                        link = item;
                    }
                }system.debug('link : '+link);
            }
        }
        return link;
    }

    public static String certifyDoc(Id caseId, Id docToCertifyId) {
        System.debug('certifyDoc started');
        ContentDocumentLink link = getCertifyContentDocumentLink(caseId);
        if (link != null) {
            Case cas = [SELECT VTD1_Primary_PG__c, VTD1_PI_user__c FROM Case WHERE Id = :caseId];
            VTD1_Document__c docToCertify = [SELECT Name FROM VTD1_Document__c WHERE Id = :docToCertifyId];

            VTD1_RTId__c rtIds = VTD1_RTId__c.getInstance();
            Set<Id> patientCaregiverProfileIds = new Set<Id>{
                    rtIds.VTD2_Profile_Patient__c, rtIds.VTD2_Profile_Caregiver__c
            };

            VTD1_Document__c docuSignDoc = new VTD1_Document__c(
                    RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON,
                    VTD1_Clinical_Study_Membership__c = caseId,
                    VTD1_Nickname__c = 'Certification form',
                    VTD1_PG_Approver__c = cas.VTD1_Primary_PG__c
            );
            //if (new List<String>{'Patient', 'Caregiver', 'Primary Investigator', 'Site Coordinator', 'Study Concierge', 'Patient Guide', 'System Administrator'}.contains(VT_D1_HelperClass.getProfileNameOfCurrentUser())) {
                docuSignDoc.VTD2_Associated_Medical_Records__c = docToCertify.Name;
                docuSignDoc.VTD2_Associated_Medical_Record_id__c = docToCertifyId;
            //}
            insert docuSignDoc;

            ContentVersion cvToClone = [
                    SELECT ContentLocation, PathOnClient, Title, VersionData, ContentDocumentId
                    FROM ContentVersion
                    WHERE Id = :link.ContentDocument.LatestPublishedVersionId
            ];

            //ContentVersion newCv = cvToClone.clone(false);
            ContentVersion newCv = new ContentVersion(ContentLocation=cvToClone.ContentLocation, PathOnClient=cvToClone.PathOnClient, Title=cvToClone.Title, VersionData=cvToClone.VersionData);
            insert newCv;
            System.debug('cvToClone.ContentDocumentId == null? ' + cvToClone.ContentDocumentId);

            if(newCv.Id != null){
                insert new ContentDocumentLink(
                        ContentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :newCv.Id].ContentDocumentId,
                        LinkedEntityId = docuSignDoc.Id,
                        ShareType = link.ShareType,
                        Visibility = link.Visibility
                );
                update new VTD1_Document__c(
                        Id = docToCertifyId,
                        VTD2_Certification__c = docuSignDoc.Id
                );
                new VT_D2_QAction_CertifyDocuments(docuSignDoc.Id).executeFuture();
                return '';
            }
        }
        return Label.VTR2_IncorrectStudySettings;
    }
}