@isTest
public with sharing class VT_R3_RestTaskConfirmationTest {
	static VT_R3_RestHelper helper = new VT_R3_RestHelper();

	public static void closeTaskTest() {
		String responseString;
		Map<String,Object> responseMap = new Map<String, Object>();
		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();
		request.httpMethod = 'GET';
		request.requestURI = '/Task/Confirmation/';
		RestContext.request = request;
		RestContext.response  = response;

		responseString = VT_R3_RestTaskConfirmation.closeTask();
		responseMap = (Map<String, Object>)JSON.deserializeUntyped(responseString);
		System.assertEquals(helper.forAnswerForIncorrectInput(), responseMap.get('serviceResponse'));

		Task task = new Task(	Subject='Test subject ',
							Status='New',
							Priority='Normal',
							CallType='Outbound');
		insert task;

		RestContext.request.requestURI = '/Task/Confirmation/' + task.Id;
		responseString = VT_R3_RestTaskConfirmation.closeTask();
		responseMap = (Map<String, Object>)JSON.deserializeUntyped(responseString);
		System.assertEquals('SUCCESS', responseMap.get('serviceResponse'));

		delete task;

		RestContext.request.requestURI = '/Task/Confirmation/'  + task.Id;
		responseString = VT_R3_RestTaskConfirmation.closeTask();
		responseMap = (Map<String, Object>)JSON.deserializeUntyped(responseString);
		System.assert(((String)responseMap.get('serviceResponse')).contains('FAILURE'));

	}

}