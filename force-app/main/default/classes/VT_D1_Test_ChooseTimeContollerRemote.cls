@isTest
private class VT_D1_Test_ChooseTimeContollerRemote {  
   @testSetup
   private static void setupMethod() {
        try {           
            
            HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
    
            User patient = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Patient' AND ContactId != null AND IsActive = true LIMIT 1];
      User PI = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Primary Investigator' AND ContactId != null AND IsActive = true LIMIT 1];
      System.debug('patient:'+patient);
      System.debug('PI:'+PI);

            Case cas = VT_D1_TestUtils.createCase('CarePlan', patient.ContactId, PI.ContactId, null, null, null, null);
            cas.VTD1_Study__c = study.Id;
            Test.startTest();
            insert cas;
      Test.stopTest();    
            Case cs = [SELECT Id FROM Case WHERE ContactId != null LIMIT 1];
            Id caseId = cs.Id;
            cs.VTD1_Enrollment_Date__c = Datetime.now().addDays(-5).date();
            update cs;
            VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
            protocolVisit.VTD1_Range__c = 2;
            protocolVisit.VTD1_VisitOffset__c = 2;
            protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
            protocolVisit.VTD1_VisitDuration__c = '30 minutes';
            protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
            insert protocolVisit;

            VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
            actualVisit.VTD1_Protocol_Visit__c = protocolVisit.Id;
            actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
            actualVisit.VTD1_Case__c = caseId;
            insert actualVisit;
                        
            Task newTask = new Task();
            newTask.VTD1_Actual_Visit__c = actualVisit.Id;
            insert newTask;
            
      List<Visit_Member__c> VMs = new List<Visit_Member__c>();
      VMs.add(new Visit_Member__c(VTD1_Actual_Visit__c = actualVisit.Id, 
                                        VTD1_Participant_User__c = patient.Id, 
                                        Participant_Contact__c = patient.ContactId,
                                        VTD1_Task_ID__c  = newTask.Id));
      VMs.add(new Visit_Member__c(VTD1_Actual_Visit__c = actualVisit.Id, 
                                        VTD1_Participant_User__c = PI.Id, 
                                        Participant_Contact__c = PI.ContactId,
                                        VTD1_Task_ID__c  = newTask.Id));
      insert VMs;
        } catch (Exception e) {
      System.debug(e.getMessage());
    }        
    }
    /*
    @IsTest
    static void testBehavior() {
        Task newTask = [Select Id from Task limit 1];
        VTD1_Actual_Visit__c actualVisit = [SELECT Id FROM VTD1_Actual_Visit__c LIMIT 1];
        Test.startTest();
        Object result = VT_D1_ChooseTimeOnTaskContollerRemote.getTaskData(newTask.Id);
        System.assert(result!=null);
        list<Visit_Member__c> vmms = VT_D1_ChooseTimeOnTaskContollerRemote.getVisitMembers(newTask.Id);
        System.assert(result!=null);           
        
        VT_D1_ChooseTimeOnTaskContollerRemote.updateActualVisit(actualVisit.Id, 
               DateTime.now().date().format() + ' ' + DateTime.now().format('h:mm a'),
               DateTime.now().date().format() + ' ' + DateTime.now().addMinutes(120).format('h:mm a'),
               'reasonForReschedule');
        actualVisit = [SELECT VTD1_Reason_for_Reschedule__c FROM VTD1_Actual_Visit__c 
                                            WHERE Id =:actualVisit.Id LIMIT 1];
        System.assertEquals('reasonForReschedule', actualVisit.VTD1_Reason_for_Reschedule__c);
        
    result = VT_D1_ChooseTimeOnTaskContollerRemote.createOrUpdateEventRemote(newTask.Id, 
                                  DateTime.now().date().format() + ' ' + DateTime.now().format('h:mm a'),
                                  false, actualVisit.Id, 30, 'Study Team');  
        System.assertEquals(null, result);
        
        result = VT_D1_ChooseTimeOnTaskContollerRemote.createOrUpdateEventRemote(newTask.Id, 
                                  DateTime.now().date().format() + ' ' + DateTime.now().format('h:mm a'),
                                  true, actualVisit.Id, 30, 'Study Team');  
        System.assertEquals(null, result);
        
        String stringResult = VT_D1_ActualVisitsHelper.getAvailableDateTimes(newTask.Id);
        System.assert(stringResult!=null);    
        List<sObject> vsts = new List<sObject>();
        vsts.add(actualVisit);
        Map<String, String> resultMap = VT_D1_ActualVisitsHelper.getVisitTaskMap(vsts);
        System.assert(resultMap!=null);
        Test.stopTest();
    }
    */
}