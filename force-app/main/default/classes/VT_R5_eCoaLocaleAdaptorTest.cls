/**
 * Created by N.Arbatskiy on 19.05.2020.
 */

@IsTest
private class VT_R5_eCoaLocaleAdaptorTest {
    @TestSetup
    private static void setupMethod() {

        eCOA_IntegrationDetails__c customSetting = new eCOA_IntegrationDetails__c();
        customSetting.eCOA_OrgGuid__c = 'org_altavoz';
        customSetting.Cognito_UserPoolId__c = 'us-east-1_RpJI95p9g';
        customSetting.Cognito_ClientId__c = '7s29ancc3i683c6fcme3rm2m0f';

        insert customSetting;



        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .setEdiaryTool('eCOA')
                .setEcoaGuid('study_79826f74-6967-4eb0-bc99-000000000000')
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));
        DomainObjects.VirtualSite_t virtualSite = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .setStudySiteNumber('numero_uno')
                .setExternalDiaryLanguage('de')
                .setDefaultExternalDiaryLanguage('de')
                .addStudyTeamMember(new DomainObjects.StudyTeamMember_t()
                        .addStudy(study)
                        .addUser(new DomainObjects.User_t()
                                .addContact(new DomainObjects.Contact_t())
                                .setProfile('Primary Investigator'))
                        .setRecordTypeByName('PI'));
        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
                .setLastName('Patient');
        DomainObjects.User_t userPatient = new DomainObjects.User_t()
                .addContact(patientContact)
                .setEcoaGuid('subject_9ecb75cf-a594-45b8-6667-000000000000')
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
     
        DomainObjects.Case_t casePatient = new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .addVirtualSite(virtualSite)
                .addStudy(study)
                .addUser(userPatient)
                .addContact(patientContact);
        casePatient.persist();
        patientContact.setVTD1_Clinical_Study_Membership(casePatient.id);
        update (Contact) patientContact.toObject();


    }

    @IsTest
    private static void doTest() {
        Map<String, String>localeMap = VT_R5_eCoaLocaleAdaptor.getEcoaLanguagesMapping();
        Test.startTest();
        User user = [


                SELECT LanguageLocaleKey
                FROM User
                WHERE VTR5_eCOA_Guid__c = 'subject_9ecb75cf-a594-45b8-6667-000000000000'
        ];


        System.runAs(new User(Id = UserInfo.getUserId())) {

            user.LanguageLocaleKey = 'de';
            update user;
        }
        System.assertEquals(localeMap.get('de'), VT_R5_eCoaLocaleAdaptor.adaptLocale(user.Id));
        Test.stopTest();
    }
}