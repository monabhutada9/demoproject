/**
* @author Eashan- IQVIA Striker
* @date 24/08/2020
*
* @group Test Class
*
* @description Test Class for VT_R5_CaseUpdateBatch 
*/

@IsTest
public with sharing class VT_R5_CaseUpdateBatchTest {
    /*******************************************************************************************************
    * @description SetUp method to setup required test data
    */
    @TestSetup
    static void setup() {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));
        DomainObjects.User_t piUser = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5));
        DomainObjects.User_t patientUser = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5));
        DomainObjects.User_t pgUser = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5));
        DomainObjects.User_t studyAdminUser = new DomainObjects.User_t()
                .setUsername('studyAdmin@acme.com')
                .setCreatedDate(Date.today().addDays(-5));
        DomainObjects.Contact_t piContact = new DomainObjects.Contact_t()
                        .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));
        DomainObjects.StudyTeamMember_t piMemberT = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(piUser);
        new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .addStudy(study)
                .addPIUser(piUser)
                .addVTD1_Patient_User(patientUser)
                .addStudyGeography(new DomainObjects.VTR2_Study_Geography_t()
                                .addVTD2_Study(study)
                                .setGeographicalRegion('Country')
                                .setCountry('US'))
                .addVTD1_Primary_PG(pgUser)
                .persist();//Task for the Patient Guide to re-submit the randomization, NotificationC when Patient Status is Screen Failure
        //Initiate Tasks
        List<VTD2_Study_Geography__c> sg = [SELECT VTD2_TMA_Review_Required__c FROM VTD2_Study_Geography__c];
        sg[0].VTD2_TMA_Review_Required__c = true;
        update sg;
        VTD1_RTId__c rtId = new VTD1_RTId__c(Name = 'TestRecord',
                VTD1_Case_Patient_Contact_Form__c = '0121N000000oCFY',
                VTD1_Case_Patient_Contact_Form_Read_Only__c = '0121N000000oCFdQAM',
                VTD1_Actual_Visit_Labs__c = '0121N000000oCFiQAM');
        insert rtId;
        new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .addStudy(study)
                .addPIUser(piUser)
                .setStatus(VT_R4_ConstantsHelper_AccountContactCase.CASE_PRE_CONSENT)
                .persist();

        new DomainObjects.Case_t()
                .setRecordTypeId(VTD1_RTId__c.getInstance().VTD1_Case_Patient_Contact_Form__c)
                .addStudy(study)
                .persist();//PCF_Page_Access_Management, Create Case Status History

        //Community Home UI switch for Patient, Notify PI about successful randomization, Study Completion Email 3
    }

    /*******************************************************************************************************
    * @description Test Method to test case Update batch
    */
    @IsTest
    public static void firsttest() {
        List<Case> updateCases = [

                SELECT Status, RecordType.Name, RecordTypeId, VTD1_PI_user__c, VTD1_Patient_User__r.Id,
                (SELECT VTD1_Case__c, VTD1_Is_Active__c, VTD1_Status__c
                FROM Case_Status_Histories__r)
                FROM Case
        ];
        
        Test.startTest();
            Database.executeBatch(new VT_R5_CaseUpdateBatch(updateCases), 5);
        Test.stopTest();
    }
}