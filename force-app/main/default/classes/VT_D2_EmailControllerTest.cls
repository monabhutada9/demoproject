/**
 * Created by Michael Salamakha on 15.02.2019.
 */

@IsTest
public without sharing class VT_D2_EmailControllerTest {
    private final static String PROTOCOL_NICKNAME = 'PN_TEST_NAME';

    @TestSetup
    private static void setupMethod() {
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .setVTD1_Protocol_Nickname(PROTOCOL_NICKNAME)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );

        HealthCloudGA__CandidatePatient__c candidatePatient = (HealthCloudGA__CandidatePatient__c) new DomainObjects.HealthCloudGA_CandidatePatient_t()
                .setRr_firstName('Mike')
                .setRr_lastName('BirnBirnBirn')
                .setConversion('Converted')
                .setCaregiverEmail(DomainObjects.RANDOM.getEmail())
                .addStudy(study)
                .persist();

        VTD2_Study_Geography__c studyGeography = new VTD2_Study_Geography__c(VTD2_Study__c = study.Id, Name = 'Geo');
        insert studyGeography;

        VTR2_StudyPhoneNumber__c phoneNumber = new VTR2_StudyPhoneNumber__c(VTR2_Study__c = study.Id, VTR2_Study_Geography__c = studyGeography.Id, Name = '8888888888');
        insert phoneNumber;

        List<Case> cs = [SELECT Id, RecordType.DeveloperName FROM Case LIMIT 1];//WHERE RecordType.DeveloperName = 'CarePlan'];
        if(cs.size() > 0) {
            cs[0].VTR2_StudyPhoneNumber__c = phoneNumber.Id;
            update cs;
        }
        Test.stopTest();
    }

    @IsTest
    public static void  testBehavior() {
        VTR2_StudyPhoneNumber__c studyPhoneNumber = [SELECT Id, Name FROM VTR2_StudyPhoneNumber__c LIMIT 1];
        Case carePlan = [
                SELECT Id, VTD1_Study__c, RecordType.DeveloperName, VTD2_Study_Phone_Number__c, VTD1_Patient_User__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
                LIMIT 1
        ];

        carePlan.VTR2_StudyPhoneNumber__c = studyPhoneNumber.Id;
        update carePlan;
        Test.startTest();

        VT_D2_EmailController controller = new VT_D2_EmailController();
        controller.usrId = carePlan.VTD1_Patient_User__c;

        System.setPassword(controller.usrId, DomainObjects.RANDOM.getString(8) + DomainObjects.RANDOM.getInteger(9));
        System.assert(controller.getLastPasswordChangeDate() != null);

        User patientUser = [
                SELECT Id, FirstName, ContactId, LastName, LastPasswordChangeDate, ProfileId, Country,
                        Contact.Account.Candidate_Patient__r.Study__r.VTD1_Protocol_Nickname__c
            FROM User
            WHERE Id = :carePlan.VTD1_Patient_User__c
        ];

        System.assertEquals(VTD1_RTId__c.getInstance().PI_URL__c, controller.getCommunityPatientUrl());
        System.assertEquals(VTD1_RTId__c.getInstance().PI_URL__c, controller.getCommunityPiUrl());
        System.assertEquals(patientUser.FirstName, controller.getFirstNameWelcome());
        System.assertEquals(patientUser.LastName, controller.getLastNameWelcome());
        System.assertEquals(patientUser.ProfileId, controller.getProfileId());
        System.assertEquals(patientUser.Contact.Account.Candidate_Patient__r.Study__r.VTD1_Protocol_Nickname__c, controller.getStudyNickname());
        System.assertEquals(null,controller.conId);
        System.assertEquals(null,controller.getCommunityScrUrl());
        System.assertEquals(patientUser.Country, controller.getUserCountry());
        System.assertNotEquals(controller.getDownloadChinaBrowserLabel(), null);
        System.assertNotEquals(controller.getDownloadChinaBrowsersEmailWelcome(), null);

//        System.assertNotEquals(controller.getUserLanguage(), null);


        VTD1_NotificationC__c notificationC = new VTD1_NotificationC__c(
                VTD1_Receivers__c = patientUser.Id,
                VTR3_Notification_Type__c = 'Study Notification',
                Title__c = 'title',
                Message__c = 'msg',
                VTR2_Study__c = carePlan.VTD1_Study__c
        );
        insert notificationC;
        controller.notification = notificationC;
        System.assertEquals(notificationC.Id, controller.notification.Id);
        controller.getStudyPhoneNumberAccordingToUserProfile();

        controller.conId = patientUser.ContactId;
        System.assertEquals(controller.conId,patientUser.ContactId);

        controller.getStudyNickname();
        controller.getPowerPartnerStudyPhone();
        controller.getDownloadChinaBrowserLabel();
        controller.getDeepLinkPart();

        System.assert(controller.getChromeLinkLabel() != null);
        System.assert(controller.getUserLanguage() != null);
        System.assert(controller.getNotificationRecord() != null);

        Test.stopTest();
        System.assertEquals(studyPhoneNumber.Name, controller.getStudyPhoneNumber());
    }

    @IsTest
    static void testNewMessagesTemplateController() {
        VTD1_NotificationC__c testNC = VT_D1_TestUtils.CreateNotificationC(Userinfo.getUserId(), Userinfo.getUserId(), 'Test Title', 'Test Message');
        insert testNC;
        Contact contactT = new Contact(FirstName = UserInfo.getFirstName(), LastName = UserInfo.getLastName(), VTD1_UserId__c = UserInfo.getUserId());
        insert contactT;
        Test.startTest();
        VTD1_NotificationC__c notificationC = [
                SELECT Id,OwnerId,
                        Owner.Name, Owner.FirstName,
                        Owner.LastName, Owner.Profile.Name,
                        Number_of_unread_messages__c
                FROM VTD1_NotificationC__c
                LIMIT 1
        ];
        VT_D2_NewMessagesTemplateController controller = new VT_D2_NewMessagesTemplateController();
        VT_D2_NewMessagesTemplateController.PatientInfo pInfo = new VT_D2_NewMessagesTemplateController.PatientInfo();
        pInfo.firstName = UserInfo.getFirstName();
        controller.notificationId = notificationC.Id;
        System.assertEquals(notificationC.Id, controller.getNot().Id);
        System.assertEquals(notificationC.Number_of_unread_messages__c + ' unread messages', controller.getUnreadMessagesText());
        System.assertEquals(VTD1_RTId__c.getInstance().VTD2_Patient_Community_URL__c + '/messages', controller.getMessagesUrl());
        System.assertEquals(pInfo.firstName, controller.getPatientInfo().firstName);
    }

    @IsTest
    static void testOnBehalfOfScrUser() {
        User scrUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t().setLastName('Con1'))
                .setProfile('Site Coordinator')
                .persist();

        new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('SCR')
                .setUser(scrUser)
                .setStudy(DomainObjects.TEST_UTILS.getTestStudy().Id)
                .setIsActive(true)
                .persist();

        Test.startTest();
        VT_D2_EmailController controller = new VT_D2_EmailController();
        controller.usrId = scrUser.Id;

        System.assert(controller.getPowerPartnerStudyNickname() == PROTOCOL_NICKNAME);
        Test.stopTest();
    }

    @IsTest
    static void testOnBehalfOfCaregiverUser() {
        Case aCase = DomainObjects.TEST_UTILS.getTestCase();
        User caregiver = DomainObjects.TEST_UTILS.getTestCaregiverByCase(aCase);

        Test.startTest();
        System.runAs(caregiver) {
            VT_D2_EmailController controller = new VT_D2_EmailController();
            controller.usrId = caregiver.Id;
            System.debug(controller.getUserCountry());
        }
        Test.stopTest();
    }
}