/********************************************************
* @Class Name   : VT_R5_RemoveSharing
* @Created Date : 15/10/2020 
* @Version      : R5.5
* @group-content: http://jira.quintiles.net/browse/SH-17314
* @Purpose      : This batch class is used to delete the sahre record related to the CRA 
*                This is the one time execution batch
* *****************************************************/
public with Sharing class VT_R5_RemoveSharing implements Database.Batchable<sObject>{
    public Database.QueryLocator start(Database.BatchableContext BC){
        string strQuery = 'SELECT ID,'+
                          +'rowcause,'+
                          +'UserOrGroupId,'+
                          +'Parent.VTD1_Virtual_Site__r.VTR2_Onsite_CRA__c,'+
                          +'Parent.VTD1_Virtual_Site__r.VTD1_Remote_CRA__c '+
                          +'from VTD1_Monitoring_Visit__share '+
                          +'where UserOrGroupId IN (Select User__c '+
                                                  +'from Study_Team_Member__c '+
                                                  +'where VTD1_Type__c=\'CRA\') '+
                          +'AND rowCause =\'GlobalSharing__c\' AND Parent.VTD1_Virtual_Site__c != Null';
        
        return Database.getQueryLocator(strQuery);
    }
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        try{
            List<VTD1_Monitoring_Visit__share>lstMVShareToDelete = new List<VTD1_Monitoring_Visit__share>();
            
            for(VTD1_Monitoring_Visit__share objShare : (LIst<VTD1_Monitoring_Visit__share>)scope){
                if(objShare.UserOrGroupId != objShare.Parent.VTD1_Virtual_Site__r.VTR2_Onsite_CRA__c && 
                  objShare.UserOrGroupId != objShare.Parent.VTD1_Virtual_Site__r.VTD1_Remote_CRA__c ){
                    lstMVShareToDelete.add(objShare);
                }
               
            }
            
           //Delete the Monitoring Visit Share records
            if(lstMVShareToDelete.size() > 0 && lstMVShareToDelete != Null){
                List<Database.DeleteResult> lstResult = Database.delete(lstMVShareToDelete,false);
                for(Database.DeleteResult dr : lstResult) {
                    if (dr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully deleted account with ID: ' + dr.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : dr.getErrors()) {
                            //send mail
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Fields that affected this error: ' + err.getFields());
                        }
                    }
                }
            }
        }Catch(Exception ex){
            
            System.debug('Exception Meesage'+ex.getMessage()+'Line No:'+ex.getLineNumber());
            ErrorLogUtility.logException(ex, null, 'VT_R5_RemoveSharing');
        }   
    }
    public void finish(Database.BatchableContext BC){
    }
    
}