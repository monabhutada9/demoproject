/**
 * @author: Dmitry Yartsev
 * @date: 27.10.2019
 * @description: Test class for VT_R3_EmailsSender
 */

@IsTest
public class VT_R3_EmailsSenderTest {
    @TestSetup
    private static void setupMethod() {
        DomainObjects.Study_t study_t = new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'));
        DomainObjects.Contact_t con_t = new DomainObjects.Contact_t()
                .setLastName('VT_R3_EmailsSenderTest');
        DomainObjects.User_t user_t = new DomainObjects.User_t()
                .addContact(con_t)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        Case c = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName(VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN)
                .addStudy(study_t)
                .addVTD1_Patient_User(user_t)
                .addContact(con_t)
                .persist();

        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c(
                VTD1_Range__c = 4,
                VTD1_VisitOffset__c = 2,
                VTD1_VisitOffsetUnit__c = 'days',
                VTD1_VisitDuration__c = '30 minutes',
                VTD1_Onboarding_Type__c = 'N/A',
                VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient; Site Coordinator'
        );
        insert protocolVisit;

        VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c(
                VTD1_Protocol_Visit__c = protocolVisit.Id,
                VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED,
                VTD1_Case__c = c.Id,
                VTR2_Televisit__c = true
        );
        insert actualVisit;
    }

    @IsTest
    private static void test() {
        Visit_Member__c vm = [
                SELECT Id, VTD1_Participant_User__c
                FROM Visit_Member__c
                WHERE VTD1_Participant_User__c <> NULL
                LIMIT 1
        ];
        List<VT_R3_EmailsSender.ParamsHolder> ph = new List<VT_R3_EmailsSender.ParamsHolder>{
                new VT_R3_EmailsSender.ParamsHolder(
                        vm.VTD1_Participant_User__c,
                        vm.Id,
                        'VisitIsReadyToBeScheduledPatient'
                )};
        Test.startTest();
        VT_R3_EmailsSender.sendEmails(ph);
        Test.stopTest();
    }

    @IsTest
    private static void testTranslation() {
        Visit_Member__c vm = [
                SELECT Id, VTD1_Participant_User__c
                FROM Visit_Member__c
                WHERE VTD1_Participant_User__c <> NULL
                LIMIT 1
        ];
        List<VT_R3_EmailsSender.ParamsHolder> ph = new List<VT_R3_EmailsSender.ParamsHolder>{
                new VT_R3_EmailsSender.ParamsHolder(
                        vm.VTD1_Participant_User__c,
                        vm.Id,
                        'VTR4_Visit_Member_Added_External',
                        'test@test.test'
                ),
                new VT_R3_EmailsSender.ParamsHolder(
                        vm.VTD1_Participant_User__c,
                        vm.Id,
                        'VTR4_Visit_Member_Added_External',
                        'test@test.test',
                        true
                ),
                new VT_R3_EmailsSender.ParamsHolder()
        };
        Test.startTest();
        VT_R3_EmailsSender.sendEmails(ph);
        Test.stopTest();
    }
    
     @IsTest
    private static void testForStaticSubject() {
        Visit_Member__c vm = [
                SELECT Id, VTD1_Participant_User__c
                FROM Visit_Member__c
                WHERE VTD1_Participant_User__c <> NULL
                LIMIT 1
        ];
        List<VT_R3_EmailsSender.ParamsHolder> ph = new List<VT_R3_EmailsSender.ParamsHolder>{
                new VT_R3_EmailsSender.ParamsHolder(
                        vm.VTD1_Participant_User__c,
                        vm.Id,
                        'VisitIsReadyToBeScheduledPatient'
                )};
                    Boolean result = false;
        Test.startTest();
        ph[0].subjectIsStatic = true;
        try{
            VT_R3_EmailsSender.sendEmails(ph);
            result = true; 
        }catch(Exception e){
            result = false;
        }
        Test.stopTest();
        System.assert(result); 
    }

   
}