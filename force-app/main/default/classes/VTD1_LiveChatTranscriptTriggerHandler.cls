public without sharing class VTD1_LiveChatTranscriptTriggerHandler {

    private Map<Id, Case> caseMap = new Map<Id, Case>();
    private Map<Id, List<Id>> patientToCaregiverIds = new Map<Id, List<Id>>();
    private List<LiveChatTranscriptShare> newShares = new List<LiveChatTranscriptShare>();

    public void onAfterInsert(List<LiveChatTranscript> recs) {
        shareTranscripts(recs, null);
    }

    public void onAfterUpdate(List<LiveChatTranscript> recs, Map<Id, LiveChatTranscript> oldMap) {
        shareTranscripts(recs, oldMap);
    }

    private void shareTranscripts(List<LiveChatTranscript> recs, Map<Id, LiveChatTranscript> oldMap) {
        List<LiveChatTranscript> toShareToCaseUsers = new List<LiveChatTranscript>();
        List<LiveChatTranscript> toShareToContacts = new List<LiveChatTranscript>();

        

        if (oldMap == null) {
            toShareToCaseUsers = recs;
            toShareToContacts = recs;
        } else {
            map<Id,set<Id>>ChatShareWithMembersMap=ChatShareWithMembers(recs);
            map<Id,Id> ContactWithUserIdMap=ContactWithUserId(recs);

            for (LiveChatTranscript item : recs) {
                if (item.CaseId != null && oldMap.get(item.Id).CaseId == null) {
                    toShareToCaseUsers.add(item);
                }

                if (item.ContactId != null && oldMap.get(item.Id).ContactId == null) {
                    toShareToContacts.add(item);
                }
                //JIRA: SH-12879
                if(item.ContactId != null 
                   && oldMap.get(item.Id).Status=='Waiting' 
                   && item.Status=='InProgress' 
                   && ContactWithUserIdMap.containskey(item.ContactId) 
                   && ChatShareWithMembersMap.containskey(item.Id) 
                   && !ChatShareWithMembersMap.get(item.Id).contains(ContactWithUserIdMap.get(item.ContactId))){
                        toShareToContacts.add(item);
                }
            }
        }

        if (! toShareToCaseUsers.isEmpty()) { shareTranscriptsToCaseUsers(toShareToCaseUsers); }
        if (! toShareToContacts.isEmpty()) { shareTranscriptsToContacts(toShareToContacts); }

        if (! this.newShares.isEmpty()) { insert this.newShares; }
    }
    /*
    ** Modified By: IQVIA Strikers (Eashan Parlewar)
    ** Comments:    Added inner class in order to query all the chat transcript records 
    **              associated with patient irrespective of sharing settings.
    ** JIRA:        SH-12879
    */
    public map<Id,set<Id>>ChatShareWithMembers (List<LiveChatTranscript> recs){
        
        map<Id,set<Id>>ChatShareWithMembersMap=new map<Id,set<Id>>();
        set<Id> ChatIdSet=new set<Id>();
        
        for(LiveChatTranscript chat:recs){
            ChatIdSet.add(chat.Id);
        }

        for(LiveChatTranscriptShare ChatShare:[select ParentId, UserOrGroupId, AccessLevel 
                                               from LiveChatTranscriptShare where ParentId in: ChatIdSet]){
            
            if(!ChatShareWithMembersMap.containskey(ChatShare.ParentId)){
                ChatShareWithMembersMap.put(ChatShare.ParentId,new set<Id>{ChatShare.UserOrGroupId});
            }else{
                ChatShareWithMembersMap.get(ChatShare.ParentId).add(ChatShare.UserOrGroupId);
            }

        }
        
        return ChatShareWithMembersMap;
    }

    public map<Id,Id> ContactWithUserId(List<LiveChatTranscript> recs){
        map<Id,Id> ContactWithUserIdMap=new map<Id,Id>();
        set<Id> ContactIdSet=new set<Id>();
        
        for(LiveChatTranscript chat:recs){
            ContactIdSet.add(chat.ContactId);
        }

        for(User U:[select Id,contactId from user where contactId in:ContactIdSet]){
            ContactWithUserIdMap.put(U.contactId,U.Id);
        }
        
        return ContactWithUserIdMap;
    }

    private void shareTranscriptsToCaseUsers(List<LiveChatTranscript> recs) {
        List<Id> caseIds = new List<Id>();
        for (LiveChatTranscript item : recs) {
            if (item.CaseId != null) { caseIds.add(item.CaseId); }
        }

        if (! caseIds.isEmpty()) {
            queryCasesAndCaregivers(caseIds);

            for (LiveChatTranscript item : recs) {
                if (this.caseMap.containsKey(item.CaseId)) {
                    Case cas = this.caseMap.get(item.CaseId);
                    addNewShare(item, cas.VTD1_Patient_User__c);
                    addNewShare(item, cas.VTD1_Primary_PG__c);
                    addNewShare(item, cas.VTD1_PI_user__c);

                    if (this.patientToCaregiverIds.containsKey(cas.VTD1_Patient_User__r.Contact.AccountId)) {
                        for (Id caregiverId : this.patientToCaregiverIds.get(cas.VTD1_Patient_User__r.Contact.AccountId)) {
                            addNewShare(item, caregiverId);
                        }
                    }
                }
            }


        }
    }

    private void shareTranscriptsToContacts(List<LiveChatTranscript> recs) {
        List<Id> contactIds = new List<Id>();
        for (LiveChatTranscript item : recs) {
            if (item.ContactId != null) { contactIds.add(item.ContactId); }
        }

        if (! contactIds.isEmpty()) {
            Map<Id, Id> contactToUserIdMap = new Map<Id, Id>();
            for (User item : [SELECT Id, ContactId FROM User WHERE ContactId IN :contactIds]) {
                contactToUserIdMap.put(item.ContactId, item.Id);
            }

            for (LiveChatTranscript item : recs) {
                if (item.ContactId != null && contactToUserIdMap.containsKey(item.ContactId)) {
                    addNewShare(item, contactToUserIdMap.get(item.ContactId));
                }
            }
        }
    }

    private void queryCasesAndCaregivers(List<Id> caseIds) {
        List<Id> patientAccIds = new List<Id>();

        for (Case item : [
            SELECT Id, VTD1_Patient_User__c, VTD1_Primary_PG__c, VTD1_PI_user__c, VTD1_Patient_User__r.Contact.AccountId, ContactId
            FROM Case WHERE Id IN :caseIds
        ]) {
            this.caseMap.put(item.Id, item);
            if (item.VTD1_Patient_User__r.Contact.AccountId != null) { patientAccIds.add(item.VTD1_Patient_User__r.Contact.AccountId); }
        }

        if (! patientAccIds.isEmpty()) {
            for (User item : [SELECT Id, Contact.AccountId FROM User WHERE Profile.Name = 'Caregiver' AND Contact.AccountId IN :patientAccIds]) {
                if (! this.patientToCaregiverIds.containsKey(item.Contact.AccountId)) {
                    this.patientToCaregiverIds.put(item.Contact.AccountId, new List<Id>());
                }
                this.patientToCaregiverIds.get(item.Contact.AccountId).add(item.Id);
            }
        }
    }

    private void addNewShare(LiveChatTranscript rec, Id userId) {
        if (userId != null && userId != rec.OwnerId) {
            this.newShares.add(new LiveChatTranscriptShare(
                ParentId = rec.Id,
                UserOrGroupId = userId,
                AccessLevel = 'Read'
            ));
        }
    }


    // Added in sprint 3
    // Added by Warriors
    // Jra ticket id: 8592
    public static void assignMatechedLanguageToChatTranscript(List<LiveChatTranscript> chatTranscriptList){
        
        Set<Id>  chatButtonId = new Set<Id>();
        for(LiveChatTranscript lct : chatTranscriptList){
            if(lct.LiveChatButtonId != null){
                chatButtonId.add(lct.LiveChatButtonId);
            }
        }
        
        if(chatButtonId != null && chatButtonId.size()>0){
            Set<String> skillDeveloperNameSet = new Set<String>();
            Map<Id,String> liveChatButtonIdToSkillLabelNameMap = new Map<Id,String>();
            // Create Label for the language 
            skillDeveloperNameSet.addAll(getPicklistValue('User','VT_R5_Secondary_Preferred_Language__c'));
           //Create map for chat button  button id and skill name 
            for(LiveChatButtonSkill lcbs:[Select ButtonId,SkillId,Skill.MasterLabel from LiveChatButtonSkill 
                                              where ButtonId IN:chatButtonId and skill.DeveloperName IN:skillDeveloperNameSet and SkillId != null]){
                                                  liveChatButtonIdToSkillLabelNameMap.put(lcbs.ButtonId,lcbs.Skill.MasterLabel);
                                              }
           
   
             //Create map for chat button id and skill name 
            if(liveChatButtonIdToSkillLabelNameMap.isEmpty()){
                for(LiveChatButton lcb : [Select id,SkillId,Skill.MasterLabel from LiveChatButton 
                                      where Id IN:chatButtonId and skill.DeveloperName IN:skillDeveloperNameSet and SkillId != null]){
                                          liveChatButtonIdToSkillLabelNameMap.put(lcb.Id,lcb.Skill.MasterLabel);
                                      }
            }
            System.debug('-----------'+liveChatButtonIdToSkillLabelNameMap);
            if(!liveChatButtonIdToSkillLabelNameMap.isEmpty()){
                for(LiveChatTranscript lct :chatTranscriptList){
                    lct.VTR5_Matched_Chat_Language__c = liveChatButtonIdToSkillLabelNameMap.get(lct.LiveChatButtonId);
                }
            }
        }               
    }
    public  static Set<String> getPicklistValue(String objectName, String fieldName){
        
        Set<String>  allLangugaeSet= new Set<String>();
        Schema.SObjectType sObj = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult sObjResult = sObj.getDescribe() ;
        Map<String,Schema.SObjectField> fields = sObjResult.fields.getMap() ;
        System.debug('fields'+fields);
        System.debug('fields'+fieldName);
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){  
            allLangugaeSet.add(pickListVal.getValue());
        } 
        return allLangugaeSet;
    }
}