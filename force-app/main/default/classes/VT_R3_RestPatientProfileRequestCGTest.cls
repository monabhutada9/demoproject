@isTest
public with sharing class VT_R3_RestPatientProfileRequestCGTest {

	@testSetup
	static void setup() {
		Test.startTest();
		HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
		VT_D1_TestUtils.createTestPatientCandidate(1);
		Test.stopTest();
		User user = [SELECT Id FROM User WHERE Profile.Name = 'Patient' AND IsActive = true AND Name = 'CP1 CP1L' LIMIT 1];

		insert new HealthCloudGA__CarePlanTemplate__Share(
				UserOrGroupId = user.Id,
				ParentId = study.Id,
				AccessLevel = 'Edit'
		);

		insert new VTD2_TN_Catalog_Code__c(
				VTD2_T_Task_Unique_Code__c = 'T547',
				VTD2_T_Due_Date__c = 'TODAY()',
				VTD2_T_SObject_Name__c = 'Case',
				RecordTypeId = VTD2_TN_Catalog_Code__c.getSObjectType().getDescribe().getRecordTypeInfosByName().get('SCTask').getRecordTypeId(),
				VTD2_T_Patient__c = 'Case.Id',
				VTD2_T_Care_Plan_Template__c = 'Case.VTD1_Study__c',
				VTD2_T_Assigned_To_ID__c = 'Case.VTD1_Study__r.VTD1_SC_Tasks_Queue_Id__c',
				VTR3_T_Name__c = 'Caregiver Information Update',
				VTD2_T_Description__c = '"Hello, patient " & Case.VTD1_Patient__r.VTD1_Patient_Name__c & " has requested to update caregiver information." & Case.VTD1_Patient__r.VTD1_Patient_Name__c & " indicated the following data for the caregiver:	First Name: #firstName	Last Name: #lastName	Relationship: #relationship		Email Address: #email	Phone Number: #phone."',
				VTD2_T_Subject__c = '"Hello, patient " & Case.VTD1_Patient__r.VTD1_Patient_Name__c & " has requested to update caregiver information."'
		);
	}

//	static {
//		List<Case> casesList = [SELECT VTD1_Patient_User__c, VTD1_Patient__r.VTD1_Patient_Name__c, VTD1_Study__c FROM Case];
//		if(casesList.size() > 0){
//			cas = casesList.get(0);
//			String userId = cas.VTD1_Patient_User__c;
//			u = [SELECT Id, ContactId, TimeZoneSidKey, Email, VTD1_Form_Updated__c, LanguageLocaleKey FROM User WHERE Id =: userId];
//		}
//	}

	@isTest
	public static void createSCTaskTest() {
		User u = [SELECT id FROM User WHERE Profile.Name = 'Patient' AND IsActive = true AND Name = 'CP1 CP1L' LIMIT 1];

		System.runAs(u) {
			String responseString;
			Map<String, Object>responseMap = new Map<String, Object>();
			RestRequest request = new RestRequest();
			RestResponse response = new RestResponse();
			request.requestURI = '/Patient/Profile/RequestCG/';
			request.httpMethod = 'POST';
			RestContext.request = request;
			RestContext.response = response;

			VT_R3_RestPatientProfileRequestCG.CaregiverData caregiverData = new VT_R3_RestPatientProfileRequestCG.CaregiverData();
			caregiverData.firstName = 'FN';
			caregiverData.relationship = 'Sibling';
			caregiverData.lastName = 'LN';
			caregiverData.emailAddress = 'FN@fn.ru';
			caregiverData.phoneNumber = '82130031122';

			RestContext.request.requestBody = Blob.valueOf(JSON.serialize(caregiverData));
			responseString = VT_R3_RestPatientProfileRequestCG.createSCTask();
			responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
			System.assertEquals('SUCCESS', (String) responseMap.get('serviceResponse'));
			System.assertEquals(System.Label.VTR3_Success_Add_Caregiver_Request, (String) responseMap.get('serviceMessage'));


			caregiverData.relationship = 'Other';
			caregiverData.relationshipOther = 'SOMEOTHER';

			RestContext.request.requestBody = Blob.valueOf(JSON.serialize(caregiverData));
			responseString = VT_R3_RestPatientProfileRequestCG.createSCTask();
			responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
			System.assertEquals('SUCCESS', (String) responseMap.get('serviceResponse'));
			System.assertEquals(System.Label.VTR3_Success_Add_Caregiver_Request, (String) responseMap.get('serviceMessage'));

			RestContext.request.requestBody = Blob.valueOf('string');
			responseString = VT_R3_RestPatientProfileRequestCG.createSCTask();
			responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
			System.assertEquals('Some text for NOT-success-banner "request caregiver" action', (String) responseMap.get('serviceMessage'));
		}

	}
}