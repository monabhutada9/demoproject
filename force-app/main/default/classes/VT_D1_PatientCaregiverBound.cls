global class VT_D1_PatientCaregiverBound {
    public static String getUserProfileName(Id userId) {
        return VT_D1_HelperClass.getUserProfileName(userId);
    }
    public static String getPatientId() {
        String profileName = VT_D1_HelperClass.getProfileNameOfCurrentUser();
        String patientId;
        if (profileName == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME) {
            patientId = findPatientOfCaregiver(UserInfo.getUserId());
        } else {
            patientId = UserInfo.getUserId();
        }
        return patientId;
    }
    public static String findPatientOfCaregiver(Id caregiverId) {
        String patientId;

        List<User> caregivers = [
                SELECT Contact.AccountId
                FROM User
                WHERE Id = :caregiverId
                AND Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME
        ];
        if (!caregivers.isEmpty()) {
            Id accId = caregivers[0].Contact.AccountId;
            if (accId != null) {
                List<User> patients = [
                        SELECT Id FROM User
                        WHERE Contact.AccountId = :accId
                        AND Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
//                        AND Contact.VTD1_Clinical_Study_Membership__r.RecordType.DeveloperName = :VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN
                ];
                if (!patients.isEmpty()) {
                    patientId = patients[0].Id;

                }
            }
        }
        System.debug('patient id = ' + patientId);
        return patientId;
    }
    public static String findCaregiverOfPatient(Id patientId) {
        return findCaregiversOfPatients(new List<Id>{patientId}).get(patientId);
    }
    public static Map<Id, Id> findCaregiversOfPatients(List<Id> patientIds) {
        Map<Id, Id> patientIdCaregiverIdMap = new Map<Id, Id>();
        Map<Id, Id> accIdPatientIdMap = new Map<Id, Id>();
        List<User> patients = [
                SELECT Contact.AccountId
                FROM User
                WHERE Id IN :patientIds
                AND Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
        ];
        if (!patients.isEmpty()) {
            for (User p : patients) {
                if (p.Contact.AccountId!=null) {
                    accIdPatientIdMap.put(p.Contact.AccountId, p.Id);
                }
            }
            if (!accIdPatientIdMap.isEmpty()) {
                List<User> caregivers = [
                        SELECT Id, Contact.AccountId
                        FROM User
                        WHERE Contact.AccountId IN :accIdPatientIdMap.keySet()
                        AND Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME
                ];
                for (User c : caregivers) {
                    if (c.Contact.AccountId!=null) {
                        patientIdCaregiverIdMap.put(accIdPatientIdMap.get(c.Contact.AccountId), c.Id);
                }
            }
        }
    }

        return patientIdCaregiverIdMap;
    }
//    public static Case getCaseForPatient(Id user) {
//        return VT_D1_PatientCaregiverBound.getPatientCase(user);
//    }
//    public static Case getCaseForCaregiver(Id user) {
//        return VT_D1_PatientCaregiverBound.getPatientCase(user);
//    }
    public static Case getPatientCase(Id userId) {
        Case patientCase;
        User currentUser = [SELECT ContactId, Profile.Name, Contact.VTD1_Clinical_Study_Membership__c FROM User WHERE Id = :userId];
        final String RT_CAREPLAN = VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN;
        Id filterById;
        //Fix for SH-22171: Added "ContactId" field in the query. 
        //Modified By: IQVIA Strikers
        String query = 'SELECT Id, VTD1_Study__c, Status, ContactId FROM Case WHERE VTD1_Study__c != null AND RecordType.DeveloperName = :RT_CAREPLAN';
        if (currentUser.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
                || currentUser.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME) {
            filterById = currentUser.Contact.VTD1_Clinical_Study_Membership__c;
            if (filterById != null) {
                query += ' AND Id = :filterById';
            }

        } else {
            filterById = currentUser.ContactId;
            if (filterById != null) {
                query += ' AND ContactId = :filterById';
        }
    }

        if (filterById != null) {
            List<Case> cases = Database.query(query);
            if (!cases.isEmpty()) {
                patientCase = cases[0];
            }

        }
        return patientCase;
    }

    public static Case getCaseForUser() {
        return VT_D1_PatientCaregiverBound.getPatientCase(UserInfo.getUserId());
    }
}