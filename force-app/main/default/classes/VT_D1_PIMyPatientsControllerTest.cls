@IsTest
public with sharing class VT_D1_PIMyPatientsControllerTest {

    public static void getMyPatientsTest_PI() {
        User u;
        Test.startTest();
        List<Case> casesList = [SELECT VTD1_PI_user__c, VTD1_Study__c FROM Case];
        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Range__c = 4;
        protocolVisit.VTD1_VisitOffset__c = 2;
        protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        insert protocolVisit;

        VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
        actualVisit.VTD1_Protocol_Visit__c = protocolVisit.Id;
        actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
        actualVisit.VTD1_Case__c = casesList.get(0).Id;
        actualVisit.VTR5_PatientCaseId__c = casesList.get(0).Id;
        actualVisit.VTD1_Scheduled_Date_Time__c = Datetime.now();
        insert actualVisit;
        String userId;
        if (casesList.size() > 0) {
            Case cas = casesList.get(0);
            userId = cas.VTD1_PI_user__c;
            u = [SELECT Id,VTD1_Profile_Name__c FROM User WHERE Id = :userId];
        }

        String studyId = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c].Id;
        System.debug('casesList ' + casesList);
        System.debug('studyId ' + studyId);
        HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
        objVirtualShare.ParentId = StudyId;
        objVirtualShare.UserOrGroupId = u.Id;
        objVirtualShare.AccessLevel = 'Read';
        insert objVirtualShare;
        CaseShare caseShare = new CaseShare();
        caseShare.CaseId = casesList.get(0).Id;
        caseShare.UserOrGroupId = u.Id;
        caseShare.CaseAccessLevel = 'Read';
        insert caseShare;
        System.runAs(u) {
            //String STMString = VT_D1_PIMyPatientsController.getStudyTeamMembers(userId);
            List<Study_Team_Member__c> listSTM = [SELECT Id, VTD1_Type__c, User__r.LastName, User__r.FirstName FROM Study_Team_Member__c];
            System.debug('listSTM ' + listSTM);
            for (Study_Team_Member__c lstm: listSTM){
                System.debug('lstm ' + lstm);
            }

            for (Study_Team_Member__c lstm: listSTM){
                String STMListString = VT_D1_PIMyPatientsController.getStudyTeamMembers(lstm.Id);
                    List<VT_D1_PIMyPatientsController.StudyTeamMember> STMList = (List<VT_D1_PIMyPatientsController.StudyTeamMember>) JSON.deserialize(STMListString, List<VT_D1_PIMyPatientsController.StudyTeamMember>.class);
                    if (STMList.size() > 0) {
                        for (VT_D1_PIMyPatientsController.StudyTeamMember stml: STMList){
                            System.debug('stml2 ' + stml);
                        }
                        VT_D1_PIMyPatientsController.StudyTeamMember StudyTM = STMList.get(0);
                        System.assertNotEquals(null, StudyTM.stm);
                }
            }
            VT_D1_PIMyPatientsController.Patient patient = new VT_D1_PIMyPatientsController.Patient(actualVisit);
            System.assertNotEquals(null, patient.visit);

            String startDa =  String.valueOf(Date.today()-1);
            String EndDa =  String.valueOf(Date.today()+1);
            String jsonParams = '{"limit": "1000","offset": "0","order": "VTR5_PatientCaseId__r.VTD1_Patient__r.VTD1_First_Name__c, VTR5_PatientCaseId__r.VTD1_Patient__r.VTD1_Last_Name__c","sObjectName" : "VTD1_Actual_Visit__c", "startDate" : "' + startDa + '", "endDate" : "' + EndDa + '"}';

            String myPatientListString = VT_D1_PIMyPatientsController.getMyPatients(studyId, jsonParams);
            System.debug('myPatientListString ' + myPatientListString);
            //VT_R5_PagingQueryHelper.ResultWrapper myPatientList = (VT_R5_PagingQueryHelper.ResultWrapper) JSON.deserialize(myPatientListString, VT_R5_PagingQueryHelper.ResultWrapper.class);
            Map <String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(myPatientListString);
            List<Object> visits = (List<Object>) params.get('records');
            System.assertEquals(1, visits.size());
            /*if (myPatientList.size() > 0) {
                VT_D1_PIMyPatientsController.Patient myPatient = myPatientList.get(0);
                System.debug('myPatient ' + myPatient);
                System.assertNotEquals(null, myPatient.cas);
            }*/
        }
        Test.stopTest();
    }

    public static void getMyPatientsTest_SCR() {
        User u;
        Test.startTest();
        List<Case> casesList = [SELECT VTR2_SiteCoordinator__c FROM Case];
        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Range__c = 4;
        protocolVisit.VTD1_VisitOffset__c = 2;
        protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        insert protocolVisit;

        VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
        actualVisit.VTD1_Protocol_Visit__c = protocolVisit.Id;
        actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
        actualVisit.VTD1_Case__c = casesList.get(0).Id;
        actualVisit.VTR5_PatientCaseId__c = casesList.get(0).Id;
        actualVisit.VTD1_Scheduled_Date_Time__c = Datetime.now();
        insert actualVisit;





        if (casesList.size() > 0) {
            Case cas = casesList.get(0);
            String userId = cas.VTR2_SiteCoordinator__c;
            u = [SELECT Id FROM User WHERE Id = :userId];
        }

        String studyId = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c].Id;

        HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
        objVirtualShare.ParentId = StudyId;
        objVirtualShare.UserOrGroupId = u.Id;
        objVirtualShare.AccessLevel = 'Read';
        insert objVirtualShare;

        CaseShare caseShare = new CaseShare();
        caseShare.CaseId = casesList.get(0).Id;
        caseShare.UserOrGroupId = u.Id;
        caseShare.CaseAccessLevel = 'Read';
        insert caseShare;

        System.runAs(u) {
            String startDa =  String.valueOf(Date.today()-1);
            String EndDa =  String.valueOf(Date.today()+1);
            String jsonParams = '{"limit": "1000","offset": "0","order": "VTR5_PatientCaseId__r.VTD1_Patient__r.VTD1_First_Name__c, VTR5_PatientCaseId__r.VTD1_Patient__r.VTD1_Last_Name__c","sObjectName" : "VTD1_Actual_Visit__c", "startDate" : "' + startDa + '", "endDate" : "' + EndDa + '"}';

            String myPatientListString = VT_D1_PIMyPatientsController.getMyPatients(studyId, jsonParams);
            String pickList = VT_D1_PIMyPatientsController.getPickListValues();
            System.assertNotEquals(null, pickList);
            System.debug('myPatientListString ' + myPatientListString);

            Map <String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(myPatientListString);
            List<Object> visits = (List<Object>) params.get('records');
            System.assertEquals(1, visits.size());

        }
        Test.stopTest();
    }
}