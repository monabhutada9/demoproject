/**
* @author: Olya Baranova
* @date: 06-Mar-20
**/

@IsTest
public with sharing class VT_R4_ValidationMVScheduledTest {

    class ObjectsCreator implements Queueable {
        public void execute(QueueableContext context) {
            Id studyId = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1].Id;
            Study_Team_Member__c stmCRAOnsite = new Study_Team_Member__c(User__c =
            [SELECT Id FROM User WHERE VTD1_Profile_Name__c = 'CRA' LIMIT 1].Id,
                    Study__c = studyId);
            Study_Team_Member__c stmCRARemote = new Study_Team_Member__c(User__c =
            [SELECT Id FROM User WHERE VTD1_Profile_Name__c = 'CRA' AND Id <> :stmCRAOnsite.User__c LIMIT 1].Id,
                    Study__c = studyId);
            insert stmCRAOnsite;
            insert stmCRARemote;
            Study_Team_Member__c stm = new Study_Team_Member__c();
            stm.User__c = [SELECT Id FROM User WHERE VTD1_Profile_Name__c = 'Primary Investigator' LIMIT 1].Id; //and firstname ='PIUser1'
            stm.Study__c = studyId;
            //Commented by Priyanka Ambre to remove Remote, Onsite references- SH-17441
            //stm.VTR2_Remote_CRA__c = stmCRARemote.Id;
            //stm.VTR2_Onsite_CRA__c = stmCRAOnsite.Id;
            insert stm;
            Virtual_Site__c vs = new Virtual_Site__c();
            vs.Name = 'vsTest';
            vs.VTD1_Study_Site_Number__c = '12345';
            vs.VTD1_Study_Team_Member__c = stm.Id;
            vs.VTD1_EDP_In_Progress__c = 0;
            vs.VTD1_EDP_Done__c = 0;
            vs.VTD1_Study__c = studyId;
            vs.VTD1_Closeout_IRB_Submission__c = null;
            insert vs;
        }
    }
    @TestSetup
    static void testSetup() {




        VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();
        Test.startTest();
        VT_D1_TestUtils.testSetupUserContacts();
        VT_D1_TestUtils.createUserByProfile('CRA', 'firstCRA');
        VT_D1_TestUtils.createUserByProfile('CRA', 'secondCRA');
        VT_D1_TestUtils.persistUsers();

        System.enqueueJob(new ObjectsCreator());

        Test.stopTest();
    }

    @IsTest
    static void testBehavior() {
        Test.startTest();

        //System.debug('In testBehavior');

        Virtual_Site__c objSite = [
                SELECT	Id, 
            			VTD1_Study__c,
            			VTD1_Study_Team_Member__r.User__c
                FROM Virtual_Site__c
                WHERE VTD1_Study_Team_Member__r.User__c != NULL
                AND VTD1_Study_Team_Member__r.VTD1_PIsAssignedPG__c != NULL
                AND VTR2_Primary_SCR__c != NULL
                AND VTD1_Study__c != NULL
                LIMIT 1];
        /*Id siteId = [
                SELECT Id, VTD1_Study__c
                FROM Virtual_Site__c
                WHERE VTD1_Study_Team_Member__r.VTR2_Remote_CRA__r.User__c != NULL
                AND VTD1_Study_Team_Member__r.VTR2_Onsite_CRA__r.User__c != NULL
                AND
            	VTD1_Study_Team_Member__r.User__c != NULL
                AND VTD1_Study_Team_Member__r.VTD1_PIsAssignedPG__c != NULL
                AND VTR2_Primary_SCR__c != NULL
                AND VTD1_Study__c != NULL
                LIMIT 1
        ].Id;
        System.debug('siteId: ' + siteId);
        Id studyId = [SELECT Id, VTD1_Study__c FROM Virtual_Site__c WHERE Id = :siteId].VTD1_Study__c;*/
        Id studyId = objSite.VTD1_Study__c;

               /* System.debug('participantsOfEvent: ' + [
                SELECT Id,
                        VTD1_Study_Team_Member__r.VTD1_PIsAssignedPG__c,
                        VTD1_Study_Team_Member__r.VTR2_Onsite_CRA__r.User__c
                FROM Virtual_Site__c
                WHERE Id = :siteId
        ].VTD1_Study_Team_Member__r.VTR2_Onsite_CRA__r.User__c);*/

        //Id piID = [SELECT Id, VTD1_Study_Team_Member__r.User__c FROM Virtual_Site__c WHERE Id = : objSite.Id LIMIT 1].VTD1_Study_Team_Member__r.User__c;
	
        Id piID = objSite.VTD1_Study_Team_Member__r.User__c;
        Study_Team_Member__c craSTM =   [SELECT	Id, 
                                                User__c,
                                                User__r.FirstName, 
                                                User__r.Profile.Name,
                                                Study_Team_Member__c                                           
                                        FROM Study_Team_Member__c
                                        WHERE Study__c = :studyId AND User__r.Profile.Name='CRA' LIMIT 1];
        Study_Site_Team_Member__c sstm1 = new Study_Site_Team_Member__c();
        sstm1.VTR5_Associated_CRA__c = craSTM.Id;
        sstm1.VTD1_SiteID__c = objSite.Id ;
        insert sstm1;

        HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
        objVirtualShare.ParentId = studyId;
        objVirtualShare.UserOrGroupId = craSTM.User__c;
        objVirtualShare.AccessLevel = 'Edit';
        insert objVirtualShare;


        List <VTD1_Monitoring_Visit__c> visits = new List<VTD1_Monitoring_Visit__c>();
        visits.add(new VTD1_Monitoring_Visit__c (
                VTD1_Site_Visit_Type__c = 'Interim Visit',
                //VTR2_Remote_Onsite__c = 'Onsite',
                VTD1_Virtual_Site__c = objSite.Id,
                VTD1_Study__c = studyId,
                VTD1_Visit_Planned_Date__c = Datetime.now().addHours(2),
                VTD1_Visit_Start_Date__c = Datetime.now().addHours(2),
                VTD1_Study_Site_Visit_Status__c = 'Planned',
                VTD1_Assigned_PI__c = piID,
                VTD1_MV_Duration__c = 1
        ));
        visits.add(new VTD1_Monitoring_Visit__c (
                VTD1_Site_Visit_Type__c = 'Interim Visit',
                //VTR2_Remote_Onsite__c = 'Remote',
                VTD1_Virtual_Site__c = objSite.Id,
                VTD1_Study__c = studyId,
                VTD1_Visit_Planned_Date__c = Datetime.now().addHours(4),
                VTD1_Visit_Start_Date__c = Datetime.now().addHours(4),
                VTD1_Study_Site_Visit_Status__c = 'Planned',
                VTD1_Assigned_PI__c = piID,
                VTD1_MV_Duration__c = 1
        ));
        System.runAs(new User(Id = craSTM.User__c)){
        insert visits;
        }
        Test.stopTest();
    }
}