public with sharing class VT_D1_PIStudyDocumentsController {
    public static final Set<Id> STUDY_DOCS_REC_TYPES = new Set<Id>{
            VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE,
            VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSR,
            VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT
    };

    public class PatientDocumentsWithAdditionalInformation {
        public DocumentWrapper[] documents;
        public Case cas;
    }

    public class DocumentWrapper {
        @AuraEnabled public String Id { get; set; }
        @AuraEnabled public String Name { get; set; }
        @AuraEnabled public String DocumentName { get; set; }
        @AuraEnabled public String Type { get; set; }
        @AuraEnabled public String RecordType { get; set; }
        @AuraEnabled public String DateAdded { get; set; }
        @AuraEnabled public String TMAStatus { get; set; }
        @AuraEnabled public String TMAComments { get; set; }
        @AuraEnabled public String Version { get; set; }
        @AuraEnabled public Boolean CurrentVersion { get; set; }
        @AuraEnabled public String ContentDocumentId { get; set; }
        @AuraEnabled public String templateURL { get; set; }
        @AuraEnabled public String DocumentCategory { get; set; }
        @AuraEnabled public List<DocumentWrapper> Versions { get; set; }

        public DocumentWrapper(VTD1_Document__c d, List<ContentVersion> cvList) {
            this.Id = d.Id;
            this.Name = d.Name;
            this.DocumentName = d.VTD1_FileNames__c;
            this.Type = (d.RecordType.DeveloperName == 'VTR3_VisitDocument'? d.VTR3_Category__c : d.RecordType.Name);
            this.RecordType = d.RecordType.DeveloperName;
            this.DateAdded = d.CreatedDate.format('dd-MMM-YYYY');
            this.DocumentCategory = d.VTR3_Category__c;
            this.TMAStatus = d.VTD2_TMA_Eligibility_Decision__c;
            this.TMAComments = d.VTD2_TMA_Comments__c;
            this.Version = d.VTD1_Version__c;
            this.templateURL = d.VTD2_Template_ID__c == null ? null : VT_D1_HelperClass.getSandboxPrefix()+'/sfc/servlet.shepherd/document/download/' + d.VTD2_Template_ID__c;
            this.Versions = new List<DocumentWrapper>();
            if (cvList != null) {
                for (ContentVersion cv : cvList){
                    this.Versions.add(new DocumentWrapper(d,cv));
                }
            }
        }
        public DocumentWrapper(VTD1_Document__c d, ContentVersion cv) {
            this.Id = cv.Id;
            this.Name = d.Name;
            this.DocumentName = cv.Title;
            this.DateAdded = cv.CreatedDate.format('dd-MMM-YYYY');
            this.Version = cv.VTD1_CompoundVersionNumber__c;
            this.CurrentVersion = cv.VTD1_Current_Version__c;
            this.ContentDocumentId = cv.ContentDocumentId;
        }
    }

    @AuraEnabled
    public static String getPatientDocuments(Id caseId) {
        try{
            List<DocumentWrapper> docWrapperList = new List<DocumentWrapper>();
            PatientDocumentsWithAdditionalInformation patientDocumentsWithAdditionalInformation = new PatientDocumentsWithAdditionalInformation();

            Case c = [SELECT Id, VTD1_Study__r.Name, Contact.Name FROM Case WHERE Id =: caseId];
            patientDocumentsWithAdditionalInformation.cas = c;

            Map<Id,VTD1_Document__c> documentsMap = new Map<Id,VTD1_Document__c>([
                    SELECT
                            Id,
                            Name,
                            VTD1_FileNames__c,
                            RecordType.Name,
                            RecordType.DeveloperName,
                            CreatedDate,
                            VTD1_Version__c,
                            VTD2_TMA_Eligibility_Decision__c,
                            VTD2_TMA_Comments__c,
                            VTD2_Template_ID__c,
                            VTR3_Category__c
                    FROM VTD1_Document__c
                    WHERE VTD1_Clinical_Study_Membership__c =: c.Id
                        AND RecordTypeId IN :STUDY_DOCS_REC_TYPES AND VTD1_Flagged_for_Deletion__c = false
                    ORDER BY Name ASC
            ]);
 
            Set<Id> docIdsSet = new Set<Id>(documentsMap.keySet());
            if (!docIdsSet.isEmpty()) {
                List<ContentDocumentLink> contentDocumentLinks = [
                        SELECT Id, ContentDocumentId, LinkedEntityId
                        FROM ContentDocumentLink
                        WHERE LinkedEntityId IN :docIdsSet
                ];

                Map<Id,Id> contentDocumentLinksMap = new Map<Id,Id>();
                for (ContentDocumentLink link : contentDocumentLinks) {
                    contentDocumentLinksMap.put(link.ContentDocumentId, link.LinkedEntityId);
                }

                List<ContentVersion> contentVersions = [
                        SELECT
                                Id,
                                Title,
                                ContentDocumentId,
                                VTD1_CompoundVersionNumber__c,
                                VTD1_Current_Version__c,
                                CreatedDate
                        FROM ContentVersion
                        WHERE ContentDocumentId IN :contentDocumentLinksMap.keySet()
                        //AND VTD1_Current_Version__c = FALSE
                        ORDER BY ContentDocumentId ASC, VTD1_CompoundVersionNumber__c DESC
                ];

                Map<Id,ContentVersion[]> docsToVersMap = new Map<Id,ContentVersion[]>();
                for (ContentVersion version : contentVersions) {
                    Id documentId = contentDocumentLinksMap.get(version.ContentDocumentId);
                    if (docsToVersMap.containsKey(documentId)) {
                        docsToVersMap.get(documentId).add(version);
                    } else {
                        docsToVersMap.put(documentId, new List<ContentVersion>{version});
                    }
                }

                for (VTD1_Document__c document : documentsMap.values()) {
                    docWrapperList.add(new DocumentWrapper(document, docsToVersMap.get(document.Id)));
                }
            }
            patientDocumentsWithAdditionalInformation.documents = docWrapperList;
            return JSON.serialize(patientDocumentsWithAdditionalInformation);
        } catch (Exception e){
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static Boolean isSandbox(){
        return String.isNotEmpty(VT_D1_HelperClass.getSandboxPrefix());
    }
}