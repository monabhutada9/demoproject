/**
 * Created by user on 11.04.2019.
 */

public without sharing class VT_R2_SCRTaskListController {
    @AuraEnabled
    public static String canCloseTask(String whatId) {
        List <IMP_Replacement_Form__c> replacementForms = [select Id, VTD1_Replacement_Decision__c from IMP_Replacement_Form__c where Id = :whatId];
        if (replacementForms.isEmpty()) {
            return '';
        }

        if (replacementForms[0].VTD1_Replacement_Decision__c == null) {
            return System.Label.VTR3_CompleteReplacementFormFirst;
        } else {
            return '';
        }
    }

    @AuraEnabled
    public static String closeTask(String taskId) {
        Task task = new Task(Id = taskId); // [SELECT Id, Status FROM Task WHERE Id =: taskId];
        task.Status = 'Completed';
        update task;
        return task.Status;
    }
}