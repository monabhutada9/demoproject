@IsTest
public class VT_R5_AllTestsMobile2 {

    public static final String PI_USERNAME = 'VT_R5_AllTestsMobilePI@test.com';
    public static final String PG_USERNAME = 'VT_R5_AllTestsMobilePG@test.com';
    public static final String SCR_USERNAME = 'VT_R5_AllTestsMobileSCR@test.com';

    @TestSetup
    static void setup() {
        VT_R3_GlobalSharing.disableForTest = true;
        insert new VTD1_RTId__c(
                Name = 'TestRecordTypeGeneral',
                VTD1_SC_Task_Notification__c = VT_R4_ConstantsHelper_Tasks.RECORD_TYPE_ID_SC_TASK_NOTIFICATION
        );

        Test.startTest();
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'))
                .setOriginalName('tN')
                .setVTD1_SC_Tasks_Queue_Id(UserInfo.getUserId())
                .setStudyAdminId(UserInfo.getUserId());

        DomainObjects.VirtualSite_t virtualSite = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .setStudySiteNumber('12345');

        DomainObjects.User_t piUser = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Primary Investigator')
                .setEmail(DomainObjects.RANDOM.getEmail())
                .setUsername(PI_USERNAME);
        piUser.persist();

        DomainObjects.StudyTeamMember_t stmPI = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(piUser)
                .addVirtualSite(virtualSite)
                .setRecordTypeByName('PI');
        Study_Team_Member__c stmPiObj = (Study_Team_Member__c) stmPI.persist();


        Virtual_Site__c virtualSiteObj = (Virtual_Site__c) virtualSite.persist();
        virtualSiteObj.VTD1_Study_Team_Member__c = stmPiObj.Id;
        update virtualSiteObj;

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t();
        DomainObjects.User_t ptUser = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile('Patient');

        DomainObjects.Case_t patientCase = new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .setSubject('VT_R5_allTest')
                .addPIUser(piUser)
                .addVTD1_Patient_User(ptUser)
                .addStudy(study)
                .addUser(ptUser)
                .addContact(patientContact)
                .addPIUser(piUser)
                .addVirtualSite(virtualSite)
                .addStudyGeography(
                        new DomainObjects.VTR2_Study_Geography_t()
                                .addVTD2_Study(study)
                                .setGeographicalRegion('Country')
                                .setCountry('US')
                                .setDateOfBirthRestriction('Year Only')
                );
        patientCase.persist();

        DomainObjects.User_t pgUser = new DomainObjects.User_t()
                .setEmail(DomainObjects.RANDOM.getEmail())
                .setProfile('Patient Guide');
        pgUser.persist();


        new DomainObjects.StudyTeamMember_t()
                .addUser(new DomainObjects.User_t()
                .setProfile('Primary Investigator')
                .addContact(new DomainObjects.Contact_t()))
                .setStudy_Team_Member_t(stmPiObj.Id)
                .addStudy(study);

        new DomainObjects.Study_Site_Team_Member_t()
                .addVTD1_Associated_PG_t(new DomainObjects.StudyTeamMember_t()
                .addUser(pgUser)
                .addStudy(study)
                .setRecordTypeByName('PG'))
                .setVTD1_Associated_PI_t(stmPiObj.Id);

        new DomainObjects.Study_Site_Team_Member_t()
                .addVTR2_Associated_SCr(new DomainObjects.StudyTeamMember_t()
                .addUser(pgUser)
                .addStudy(study)
                .setRecordTypeByName('SCR'))
                .setVTR2_Associated_PI(stmPiObj.Id);

        new DomainObjects.Study_Site_Team_Member_t()
                .addVTR2_Associated_SubI_t(new DomainObjects.StudyTeamMember_t()
                .addUser(pgUser)
                .addStudy(study)
                .setRecordTypeByName('PI'))
                .setVTR2_Associated_PI3_t(stmPiObj.Id);

        Test.stopTest();

        DomainObjects.VTD1_ProtocolVisit_t protocol = new DomainObjects.VTD1_ProtocolVisit_t()
                .setStudy(study.id)
                .setDuration('45 minutes')
                .setRecordTypeByName('VTD1_Onboarding')
                .setOnboardingType('Consent')
                .setVisitType('Study Team')
                .setVTR2_Visit_Participants('PI; Patient Guide; Patient');

        DomainObjects.VTD1_Actual_Visit_t visit_1 = new DomainObjects.VTD1_Actual_Visit_t()
                .addVTD1_Case(patientCase)
                .addVTD1_ProtocolVisit_t(protocol);
        DomainObjects.VTD1_Actual_Visit_t visit_2 = new DomainObjects.VTD1_Actual_Visit_t()
                .addVTD1_Case(patientCase)
                .addVTD1_ProtocolVisit_t(protocol)
                .setVTD1_Status(VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED);

        new DomainObjects.Visit_Member_t()
                .addVTD1_Actual_Visit(visit_1)
                .addVTD1_Participant_User(ptUser)
                .addVTD1_Participant_User(piUser)
                .setVTD1_Participant_UserId(UserInfo.getUserId());

        new DomainObjects.Visit_Member_t()
                .addVTD1_Actual_Visit(visit_2)
                .addVTD1_Participant_User(ptUser)
                .addVTD1_Participant_User(piUser)
                .setVTD1_Participant_UserId(UserInfo.getUserId());

        VTD1_Actual_Visit__c actualVisit = (VTD1_Actual_Visit__c) visit_1.persist();
        actualVisit.VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today();
        actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED;
        actualVisit.VTD1_Scheduled_Date_Time__c = System.now().addHours(12);
        update actualVisit;

        insert new HealthCloudGA__CarePlanTemplate__Share(
                UserOrGroupId = piUser.id,
                ParentId = study.id,
                AccessLevel = 'Edit'
        );
    }

    //hello copado
    @IsTest
    private static void VT_R5_MobilePISCRCalendarTest() {
        VT_R5_MobilePISCRCalendarTest.firstTest();
    }
    @IsTest
    private static void VT_R5_MobilePISCRVisitTest_1() {
        VT_R5_MobilePISCRVisitTest.firstTest();
    }
    @IsTest
    private static void VT_R5_MobilePISCRVisitTest_2() {
        VT_R5_MobilePISCRVisitTest.secondTest();
    }
    @IsTest
    private static void VT_R5_MobilePISCRVisitTest_3() {
        VT_R5_MobilePISCRVisitTest.thirdTest();
    }
    @IsTest
    private static void VT_R5_MobilePISCRVisitTest_4() {
        VT_R5_MobilePISCRVisitTest.forthTest();
    }
    @IsTest
    private static void VT_R5_MobilePISCRVisitTest_5() {
        VT_R5_MobilePISCRVisitTest.fifthTest();
    }
    //hello copado
    @IsTest
    private static void VT_R5_MobilePISCRUnscheduledVisitTest_1() {
        VT_R5_MobilePISCRUnscheduledVisitTest.firstTest();
    }
    @IsTest
    private static void VT_R5_MobilePISCRUnscheduledVisitTest_2() {
        VT_R5_MobilePISCRUnscheduledVisitTest.secondTest();
    }
    @IsTest
    private static void VT_R5_MobilePISCRUnscheduledVisitTest_3() {
        VT_R5_MobilePISCRUnscheduledVisitTest.thirdTest();
    }
    //hello copado
    @IsTest
    private static void VT_R5_MobilePISCRUnscheduledVisitTest_4() {
        VT_R5_MobilePISCRUnscheduledVisitTest.forthTest();
    }

    //hello copado
    @IsTest
    private static void VT_R5_MobilePISCRHomepageTest1() {
        VT_R5_MobilePISCRHomepageTest.firstTest();
    }
    //hello copado
    @IsTest
    private static void VT_R5_MobilePISCRHomepageTest2() {
        VT_R5_MobilePISCRHomepageTest.secondTest();
    }
}