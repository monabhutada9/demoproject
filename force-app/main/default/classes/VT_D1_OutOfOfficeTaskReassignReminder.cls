global class VT_D1_OutOfOfficeTaskReassignReminder implements Database.Batchable<sObject>, Database.Stateful {
    global List<VT_D1_ProfileForOutOfOfficeTaskReassign__mdt> lstProfiles = [SELECT Profile__c, Create_NotificationC__c FROM VT_D1_ProfileForOutOfOfficeTaskReassign__mdt];

    global Database.QueryLocator start(Database.BatchableContext bc) {
        List<String> lstProfileNames = new List<String>();

        for (VT_D1_ProfileForOutOfOfficeTaskReassign__mdt profile : lstProfiles) {
            lstProfileNames.add(profile.Profile__c);
        }

        return Database.getQueryLocator([
                SELECT Id, Name, Profile.Name
                FROM User
                WHERE IsActive = TRUE AND Profile.Name IN :lstProfileNames
        ]);
    }
    /**
     * Edited by Galiya Khalikova on 14.01.2020.
     * Edited as part of SH-7022
     * @param BC
     * @param scope List<User> contains list of Users selected in start
     * Move unconverted Task to TN Catalog
     */

    global void execute(Database.BatchableContext bc, List<User> scope) {
        Map<String, Boolean> profileNameToCreateNotificationMap = new Map<String, Boolean>();
        for (VT_D1_ProfileForOutOfOfficeTaskReassign__mdt profile : lstProfiles) {
            profileNameToCreateNotificationMap.put(profile.Profile__c, profile.Create_NotificationC__c);
        }

        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Update_IQVIA_Study_Hub_Calendar'];
        Id orgWideEmailAddressId = [SELECT Id, CreatedDate FROM OrgWideEmailAddress ORDER BY CreatedDate ASC LIMIT 1].Id;

        List<Id> sourceIds = new List<Id>();
        List<Id> sourceIdsForNotification = new List<Id>();
        List <String> tnCatalogCodesTask = new List<String>();
        List <String> tnCatalogCodesNotification = new List<String>();
        for (User userFromScope : scope) {
            Boolean createNotificationC = profileNameToCreateNotificationMap.get(userFromScope.Profile.Name);
            if (createNotificationC == true) {

                sourceIdsForNotification.add(userFromScope.Id);
                tnCatalogCodesNotification.add('N540');
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.setTemplateId(template.Id);
                message.setTargetObjectId(userFromScope.Id);
                message.setOrgWideEmailAddressId(orgWideEmailAddressId);
                message.saveAsActivity = false;
                emails.add(message);
            }
            tnCatalogCodesTask.add('T572');
            sourceIds.add(userFromScope.Id);
        }

        List <Task> newTasks = VT_D2_TNCatalogTasks.generateTasks(tnCatalogCodesTask, sourceIds, null, null, false);
        if (!newTasks.isEmpty()) {
            Database.insert(newTasks, false);
        }

        if (!tnCatalogCodesNotification.isEmpty()) {
            List <VTD1_NotificationC__c> newNotifications = VT_D2_TNCatalogNotifications.generateNotifications(tnCatalogCodesNotification, sourceIdsForNotification, null,null,null,null);
            if (!newNotifications.isEmpty()) {
                if (!emails.isEmpty()) {
                    Messaging.sendEmail(emails, false);
                }
            }
        }
    }

    global void finish(Database.BatchableContext bc) {
    }
}