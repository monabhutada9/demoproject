/**
 * Created by maksimfesenko on 12/8/20.
 */

@IsTest
private class VTD1_SC_Task_ExtentionTest {

    @TestSetup
    private static void dataSetup() {
        User taskOwner = [SELECT Id FROM User WHERE IsActive = true LIMIT 1];

        insert new List<VTD1_SC_Task__c> {
            new VTD1_SC_Task__c(OwnerId = taskOwner.Id),
            new VTD1_SC_Task__c(OwnerId = taskOwner.Id)
        };
    }

    @IsTest
    private static void testConstructor() {
        List<VTD1_SC_Task__c> tasks = [SELECT Id FROM VTD1_SC_Task__c];
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(tasks);
        stdSetController.setSelected(new List<VTD1_SC_Task__c> {tasks.get(0)});

        System.Test.startTest();
        VTD1_SC_Task_Extention controller = new VTD1_SC_Task_Extention(stdSetController);
        controller.sctask = new VTD1_SC_Task__c();
        System.Test.stopTest();
    }
}