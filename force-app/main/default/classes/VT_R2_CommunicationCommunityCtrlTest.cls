@IsTest
global class VT_R2_CommunicationCommunityCtrlTest {

    @TestSetup static void setup() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        new DomainObjects.Video_Conference_t().persist();
    }

    @IsTest static void testGetRecord() {
        Test.startTest();
        Video_Conference__c result = VT_D1_CommunicationCommunityController.getRecord([SELECT Id FROM Video_Conference__c][0].Id);
        Test.stopTest();

        System.assert(result != null);
    }

    global class MockHttpResponseGenerator implements HttpCalloutMock {

        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('["test"]');
            res.setStatusCode(200);
            return res;
        }
    }
}