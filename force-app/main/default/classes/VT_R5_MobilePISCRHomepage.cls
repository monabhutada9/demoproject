/**
 * @author: Alexander Komarov
 * @date: 23.11.2020
 * @description:
 */

public with sharing class VT_R5_MobilePISCRHomepage extends VT_R5_MobileRestResponse implements VT_R5_MobileLibrary.Routable {
    public List<String> getMapping() {
        return new List<String>{
                '/mobile/{version}/homepage'
        };
    }

    public VT_R5_MobileRestResponse versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        return get_v1_home();
                    }
                }
            }
        }
        return null;
    }

    private static Integer timeOffset;
    public void initService() {
        timeOffset = UserInfo.getTimeZone().getOffset(System.now());
    }

    public Map<String, String> getMinimumVersions() {
        return null;
    }

    public VT_R5_MobileRestResponse get_v1_home() {
        List<VTD1_Actual_Visit__c> todayVisits = getTodayVisits();
        List<HealthCloudGA__CarePlanTemplate__c> currentStudies = getStudies();
        filesMap = prepareProtocolDocumentsFilesMap(currentStudies);
        String salutation = [SELECT Salutation FROM Contact WHERE Id IN (SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId())]?.Salutation;
        return this.buildResponse(new Homepage(salutation, todayVisits, currentStudies));
    }

    private Map<Id, Id> prepareProtocolDocumentsFilesMap(List<HealthCloudGA__CarePlanTemplate__c> carePlanTemplates) {
        Map<Id, Id> docIdToContentVersionId = new Map<Id, Id>();
        Set<Id> documentsIds = new Set<Id>();
        for (HealthCloudGA__CarePlanTemplate__c study : carePlanTemplates) {
            if (study.VTD1_Protocol_Document_Link__c != null) documentsIds.add(study.VTD1_Protocol_Document_Link__c);
        }
        Map<Id, Id> contentDocumentIdToDocId = new Map<Id, Id>();
        for (VTD1_Document__c doc : [SELECT Id, (SELECT Id, ContentDocumentId FROM ContentDocumentLinks ORDER BY SystemModstamp DESC LIMIT 1) FROM VTD1_Document__c WHERE Id IN :documentsIds]) {
            if (!doc.ContentDocumentLinks.isEmpty()) contentDocumentIdToDocId.put(doc.ContentDocumentLinks[0].ContentDocumentId, doc.Id);
        }
        for (ContentVersion cv : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN :contentDocumentIdToDocId.keySet() ORDER BY CreatedDate ASC]) {
            docIdToContentVersionId.put(contentDocumentIdToDocId.get(cv.ContentDocumentId), cv.Id);
        }
        return docIdToContentVersionId;
    }

    private List<HealthCloudGA__CarePlanTemplate__c> getStudies() {
        Set<Id> studyIds = new Set<Id>();
        if (VT_D1_HelperClass.getUserProfileName(UserInfo.getUserId()) == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) {
            List<Study_Site_Team_Member__c> sstmList = [
                    SELECT VTR2_Associated_PI3__r.Study__c
                    FROM Study_Site_Team_Member__c
                    WHERE VTR2_Associated_SubI__c = :UserInfo.getUserId()
            ];
            if (sstmList.size() > 0) {
                for (Study_Site_Team_Member__c sstm : sstmList) {
                    studyIds.add(sstm.VTR2_Associated_PI3__r.Study__c);
                }
            }
        }
        List<Study_Team_Member__c> stmList = [
                SELECT Study__c
                FROM Study_Team_Member__c
                WHERE User__c = :UserInfo.getUserId()
                AND (VTD1_Type__c = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME OR VTD1_Type__c = :VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME)
                AND Study__c NOT IN :studyIds
        ];
        if (!stmList.isEmpty()) {
            for (Study_Team_Member__c stm : stmList) {
                studyIds.add(stm.Study__c);
            }
        }
        return [SELECT Id,VTD1_Protocol_Document_Link__c, Name, VTD1_Therapeutic_Area__c, VTD1_Study_Logo__c,VTD1_Inclusion_Criteria__c,VTD1_Exclusion_Criteria__c,VTD1_Medical_Records_Checklist__c FROM HealthCloudGA__CarePlanTemplate__c];
    }

    private List<VTD1_Actual_Visit__c> getTodayVisits() {
        return [
                SELECT Id,VTD1_Scheduled_Date_Time__c,VTD1_Scheduled_Visit_End_Date_Time__c,Name, (
                        SELECT VTD1_Participant_User__r.Id,VTD1_Participant_User__r.Profile.Name, VTD1_Participant_User__r.Name
                        FROM Visit_Members__r
                )
                FROM VTD1_Actual_Visit__c
                WHERE Id IN (
                        SELECT VTD1_Actual_Visit__c
                        FROM Visit_Member__c
                        WHERE VTD1_Participant_User__c = :UserInfo.getUserId()
                ) AND VTD1_Scheduled_Date_Time__c > :System.now().addHours(-System.now().hour()).addMinutes(-System.now().minuteGmt())
                AND VTD1_Status__c = :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED
        ];
    }

    private class Homepage {
        String salutation;
        List<Visit> todayVisits = new List<Visit>();
        List<StudyInfo> studyInfos = new List<StudyInfo>();

        Homepage(String salutation, List<VTD1_Actual_Visit__c> visits, List<HealthCloudGA__CarePlanTemplate__c> currentStudies) {
            this.salutation = salutation;
            for (VTD1_Actual_Visit__c visit : visits) {
                todayVisits.add(new Visit(visit));
            }
            for (HealthCloudGA__CarePlanTemplate__c study : currentStudies) {
                studyInfos.add(new StudyInfo(study));
            }
        }
    }
    private class Visit {
        private String id;
        private String name;
        private Long startDateTime;
        private Long endDateTime;
        private String participantPhoto;
        private String participantName;
        private Integer participantsCount;

        Visit(VTD1_Actual_Visit__c visit) {
            this.id = visit.Id;
            this.name = visit.Name;
            this.startDateTime = visit.VTD1_Scheduled_Date_Time__c.getTime() + timeOffset;
            this.endDateTime = visit.VTD1_Scheduled_Visit_End_Date_Time__c.getTime() + timeOffset;
            this.participantsCount = visit.Visit_Members__r?.size();
            Id participantId;
            if (!visit.Visit_Members__r.isEmpty()) {
                for (Visit_Member__c member : visit.Visit_Members__r) {
                    if (member.VTD1_Participant_User__r.Profile.Name.equals(VT_R4_ConstantsHelper_Profiles.PATIENT_PROFILE_NAME)) {
                        participantId = member.VTD1_Participant_User__r.Id;
                        this.participantName = member.VTD1_Participant_User__r.Name;
                        break;
                    } else {
                        participantId = member.VTD1_Participant_User__r.Id;
                        this.participantName = member.VTD1_Participant_User__r.Name;
                    }
                }
                if (participantId != null) {
                    String temp = '';
                    if (!Test.isRunningTest()) temp = ConnectApi.UserProfiles.getPhoto(null, participantId).standardEmailPhotoUrl;
                    this.participantPhoto = temp.contains('default_profile') ? '' : temp;
                }
            }
        }
    }

    private static Map<Id, Id> filesMap;
    private class StudyInfo {
        private String name;
        private String area;
        private String logo;
        private String inclusionCriteria;
        private String exclusionCriteria;
        private String concomitantMedications;
        private String protocolDocumentLink;

        StudyInfo(HealthCloudGA__CarePlanTemplate__c study) {
            this.name = study.Name;
            this.area = study.VTD1_Therapeutic_Area__c;
            this.logo = String.isNotBlank(study.VTD1_Study_Logo__c) ? study.VTD1_Study_Logo__c.substring(study.VTD1_Study_Logo__c.indexOf('src=') + 5, study.VTD1_Study_Logo__c.indexOf('"></img>')).replace('amp;', '') : null;
            this.inclusionCriteria = study.VTD1_Inclusion_Criteria__c;
            this.exclusionCriteria = study.VTD1_Exclusion_Criteria__c;
            this.concomitantMedications = study.VTD1_Medical_Records_Checklist__c;
            Id contentVersion = filesMap.get(study.VTD1_Protocol_Document_Link__c);
            if (contentVersion != null) this.protocolDocumentLink = System.Url.getOrgDomainUrl().toExternalForm() + VT_D1_HelperClass.getSandboxPrefix() + '/services/data/v46.0/sobjects/ContentVersion/' + contentVersion + '/VersionData';
        }
    }
}