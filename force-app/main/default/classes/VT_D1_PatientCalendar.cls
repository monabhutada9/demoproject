/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Get, Upsert and Delete relevent sObject declared in the Lightning and Community Builder
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Randy Grewal   <rgrewal@salesfore.com>
* @modifiedBy     Randy Grewal   <rgrewal@salesfore.com>
* @maintainedBy   Randy Grewal   <rgrewal@salesfore.com>
* @version        1.1
* @created        2017-05-01
* @modified       2017-06-12
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            rgrewal@salesfore.com
* 2017-06-08        Added option to filter query by logged in user and removed System.debug logs
* 2017-06-12        Security Review Fixes:
*                   Added isAccessible(), isCreateable(), isDeletable() for CRUD/FLS Enforcement fix
*                   Added Typecasting to all injected field API names for SOQL Injection fix
*                   Added "with sharing" keyword to Class for Sharing Violation Fix
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public without sharing class VT_D1_PatientCalendar {
    public class DateTimeWrapper {
        @AuraEnabled public String dt { get; set; }
        @AuraEnabled public VTD1_Actual_Visit__c visit { get; set; }
        @AuraEnabled public List <Visit_Member__c> visitMembers { get; set; }
        @AuraEnabled public String dtTranslate {get;set;}
    }
    @AuraEnabled
    public static List<EventObj> getEvents() {
        //String patientId = VT_D1_PatientCaregiverBound.getPatientId();
        Case casePatient = VT_D1_PatientCaregiverBound.getPatientCase(UserInfo.getUserId());
        List<SObject> visitsList = [
                SELECT
                        Id,
                        Name,
                        VTD1_Formula_Name__c,
                        VTD1_Scheduled_Date_Time__c,
                        VTD1_Scheduled_Visit_End_Date_Time__c,
                        VTD1_Visit_Description__c,
                        VTD1_Pre_Visit_Instructions__c,
                        VTD1_Additional_Visit_Checklist__c,
                        VTD1_Case__r.ContactId, VTD1_Visit_Label__c,
                        VTD1_Status__c,
                        toLabel(VTD1_Status__c) statusLabel,
                        VTD1_Reason_for_Request__c,
                        Unscheduled_Visits__c,
                        VTD1_Additional_Patient_Visit_Checklist__c,
                        VTR2_Modality__c,
                        toLabel(VTR2_Modality__c) modalityLabel,
                        VTR2_Loc_Name__c,
                        VTR2_Loc_Address__c,
                        VTR2_Loc_Phone__c,
                        VTD1_Protocol_Visit__c,
                        VTD1_Protocol_Visit__r.VTD1_EDC_Name__c,
                        VTD1_Protocol_Visit__r.VTD1_Visit_Description__c,
                        VTD1_Protocol_Visit__r.VTD1_PreVisitInstructions__c,
                        VTD1_Protocol_Visit__r.VTD1_Patient_Visit_Checklist__c,
                        VTD1_Protocol_Visit__r.VTD1_Visit_Checklist__c,
                        toLabel(VTD1_Unscheduled_Visit_Type__c) unscheduledVisitTypeTranslation,
                        VTD1_Unscheduled_Visit_Type__c,
                        VTD1_Onboarding_Type__c,
                        toLabel(VTD1_Protocol_Visit__r.VTD1_Onboarding_Type__c),
                        toLabel(VTD1_Protocol_Visit__r.VTD1_VisitType__c),
                        VTD1_Case__r.VTD1_Study__r.VTR3_LTFU__c,
                        VTD1_Protocol_Visit__r.RecordType.DeveloperName
                FROM VTD1_Actual_Visit__c
                //WHERE (VTD1_Case__r.VTD1_Patient_User__c = :patientId OR Unscheduled_Visits__r.VTD1_Patient_User__c = :patientId)
                WHERE (VTD1_Case__c = :casePatient.Id
                OR Unscheduled_Visits__c = :casePatient.Id)
                AND VTD1_Scheduled_Date_Time__c != NULL
                AND VTD1_Status__c != :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_CANCELLED
                AND (NOT (VTD1_Protocol_Visit__r.VTD1_isArchivedVersion__c = TRUE AND VTD1_Status__c != 'Completed'))
        ];
        System.debug('Visits for calendar:');
        System.debug(visitsList);
        VT_D1_TranslateHelper.translate(visitsList);
        List<Visit_Member__c> allMembersList = [
                SELECT VTD1_Actual_Visit__c, Visit_Member_Name__c,VTD1_Member_Type__c, VTD1_Participant_User__r.FirstName, VTD1_Participant_User__r.LastName
                FROM Visit_Member__c
                WHERE VTD1_Actual_Visit__c IN :visitsList
                ORDER BY VTD1_Actual_Visit__c ASC
        ];
        VT_D1_TranslateHelper.translate(allMembersList);
        Map<Id, List<Visit_Member__c>> visitToMemberMap = new Map<Id, List<Visit_Member__c>>();
        for (Visit_Member__c participant : allMembersList) {
            if (visitToMemberMap.containsKey(participant.VTD1_Actual_Visit__c)) {
                List<Visit_Member__c> members = visitToMemberMap.get(participant.VTD1_Actual_Visit__c);
                members.add(participant);
                visitToMemberMap.put(participant.VTD1_Actual_Visit__c, members);
            } else {
                visitToMemberMap.put(participant.VTD1_Actual_Visit__c, new List<Visit_Member__c>{
                        participant
                });
            }
        }
        List<EventObj> eventRecords = new List<EventObj>();
        for (SObject visit : visitsList) {
            EventObj newEv = new EventObj(
                    visit.Id,
                    String.valueOf(VT_D1_HelperClass.getActualVisitName((VTD1_Actual_Visit__c)visit)),
                    Datetime.valueOf(visit.get('VTD1_Scheduled_Date_Time__c')),
                    Datetime.valueOf(visit.get('VTD1_Scheduled_Visit_End_Date_Time__c')),
                    getVisitDescription((VTD1_Actual_Visit__c) visit),
                    getPreVisitInstructions((VTD1_Actual_Visit__c) visit),
                    getProtocolVisitLabelTranslated((VTD1_Actual_Visit__c) visit),
                    String.valueOf(visit.get('VTD1_Status__c')),
                    visitToMemberMap.get(visit.Id),
                    getPatientVisitChecklist((VTD1_Actual_Visit__c) visit),
                    String.valueOf(visit.get('modalityLabel')),
                    String.valueOf(visit.get('VTR2_Modality__c')),
                    String.valueOf(visit.get('VTR2_Loc_Name__c')),
                    String.valueOf(visit.get('VTR2_Loc_Address__c')),
                    String.valueOf(visit.get('VTR2_Loc_Phone__c')),
                    getVisitChecklist((VTD1_Actual_Visit__c) visit),
                    String.valueOf(((VTD1_Actual_Visit__c)visit).VTD1_Protocol_Visit__r.RecordType.DeveloperName)
//                    String.valueOf(VT_D1_HelperClass.getActualVisitName((VTD1_Actual_Visit__c)visit))
            );
            newEv.type = 'visit';
            eventRecords.add(newEv);
        }
        System.debug(eventRecords);
        return eventRecords;
    }
    @AuraEnabled
    public static void cancelVisitRemote(Id visitID, String reason) {
        VTD1_Actual_Visit__c vt = new VTD1_Actual_Visit__c();
        vt.Id = visitID;
        vt.VTD1_Reason_for_Cancellation__c = reason;
        // vt.VTD1_Status__c = :VT_D1_ConstatsHelper.ACTUAL_VISIT_STATUS_CANCELLED;
        vt.VTD1_Date_Time_Stamp__c = Datetime.now();
        try {
            if (Schema.SObjectType.VTD1_Actual_Visit__c.isUpdateable()) {
                update vt;
            } else {
                throw new DmlException();
            }
        } catch (Exception e) {
            System.debug('Error while cancelling visit');
            throw new DmlException();
        }
    }
    @AuraEnabled
    public static String getDatesTimes(Id visitId) {// 10/10/2020 04:00 PM
        DateTimeWrapper wrapper = new DateTimeWrapper();
        VT_D1_ActualVisitsHelper helper = new VT_D1_ActualVisitsHelper();
        wrapper.dt = helper.getAvailableDateTimes(visitId);
        wrapper.dtTranslate = getTimeOfDayTranslate();
        wrapper.visit = VT_D1_ActualVisitsHelper.getActualVisitInfo(visitId);
        wrapper.visitMembers = VT_D1_ActualVisitsHelper.getVisitMembersList(visitId);
        return JSON.serialize(wrapper);
        //return new VT_D1_ActualVisitsHelper().getAvailableDateTimes(visitId);
    }
    @AuraEnabled
    public static void updateActualVisit(Id actualVisitId, String dateTimeFrom,
            String dateTimeTo, String reasonForReschedule) {
        VT_D1_ActualVisitsHelper.updateActualVisit(actualVisitId, dateTimeFrom, dateTimeTo, reasonForReschedule);
    }
    @AuraEnabled
    public static String getUserInfo() {
        try {
            List<User> user = [SELECT VTD1_StartDay__c, VTD1_EndOfDay__c, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
            return JSON.serialize(user[0]);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
    public class EventObj {
        @AuraEnabled public String Id { get; set; }
        @AuraEnabled public String name { get; set; }
        @AuraEnabled public Datetime startDateTime { get; set; }
        @AuraEnabled public Datetime endDateTime { get; set; }
        @AuraEnabled public String description { get; set; }
        @AuraEnabled public String instructions { get; set; }
        @AuraEnabled public String label { get; set; }
        @AuraEnabled public List<Visit_Member__c> participants { get; set; }
        @AuraEnabled public String type { get; set; }
        @AuraEnabled public String status { get; set; }
        @AuraEnabled public String visitChecklist { get; set; }
        @AuraEnabled public String modality { get; set; }
        @AuraEnabled public String modalityApiName { get; set; }
        @AuraEnabled public String location { get; set; }
        @AuraEnabled public String address { get; set; }
        @AuraEnabled public String phone { get; set; }
        @AuraEnabled public String recordTypeDevName { get; set; }
        public String title;
        public EventObj(String i, String t, Datetime s, Datetime e, String d, String o, String l,
                String st, List<Visit_Member__c> p, String vcl, String m, String mApi, String loc, String addr, String phone, String title, String recordTypeDevName) {
            this.Id = i;
            this.name = t;
            this.startDateTime = s;
            this.endDateTime = e;
            this.description = d;
            this.instructions = o;
            this.label = l;
            this.status = st;
            this.participants = p;
            this.visitChecklist = vcl;
            this.modality = m;
            this.modalityApiName = mApi;
            this.location = loc;
            this.address = addr;
            this.phone = phone;
            this.title = title;
            this.recordTypeDevName = recordTypeDevName;
        }
    }
    @TestVisible
    private static String getProtocolVisitLabelTranslated(VTD1_Actual_Visit__c visit) {
        if (String.isBlank(visit.VTD1_Protocol_Visit__c)) {
            return (String) visit.get('unscheduledVisitTypeTranslation');
        }
        if (visit.VTD1_Onboarding_Type__c == 'N/A') {
            return visit.VTD1_Protocol_Visit__r.VTD1_VisitType__c;
        }
        if (String.isBlank(visit.VTD1_Case__c)) {
            return 'N/A';
        } else {
            return visit.VTD1_Protocol_Visit__r.VTD1_Onboarding_Type__c;
        }
    }
    @TestVisible
    private static String getVisitDescription(VTD1_Actual_Visit__c visit) {
        String type = visit.VTD1_Unscheduled_Visit_Type__c;
        if (visit.Unscheduled_Visits__c == null) {
            if (visit.VTD1_Protocol_Visit__c != null) {
                return visit.VTD1_Protocol_Visit__r.VTD1_Visit_Description__c;
            } else {
                return visit.VTD1_Visit_Description__c;
            }
        } if (type == 'End of Study' && visit.Name == 'End of Study') {
            return VT_D1_TranslateHelper.getLabelValue(Label.VTR3_ReconsentRefused);
        } else if (type == 'Screening Results Visit' && visit.Name == 'Screening Result Visit') {
            return VT_D1_TranslateHelper.getLabelValue(Label.VTR3_ScreeningResultVisit);
        } else if (type == 'Screening Results Visit' && visit.Name == 'Washout/Run-In Results Visit') {
            return VT_D1_TranslateHelper.getLabelValue(Label.VTR3_WashoutRunInResultsVisit);
        } else {
            return visit.VTD1_Reason_for_Request__c;
        }
    }
    @TestVisible
    private static String getPreVisitInstructions(VTD1_Actual_Visit__c visit) {
        if (visit.VTD1_Protocol_Visit__c != null) {
            return visit.VTD1_Protocol_Visit__r.VTD1_PreVisitInstructions__c;
        } else {
            return visit.VTD1_Pre_Visit_Instructions__c;
        }
    }
    @TestVisible
    private static String getPatientVisitChecklist(VTD1_Actual_Visit__c visit) {
        if (visit.VTD1_Protocol_Visit__c != null) {
            return visit.VTD1_Protocol_Visit__r.VTD1_Patient_Visit_Checklist__c;
        } else {
            return visit.VTD1_Additional_Patient_Visit_Checklist__c;
        }
    }
    @TestVisible
    private static String getVisitChecklist(VTD1_Actual_Visit__c visit) {
        if (visit.VTD1_Protocol_Visit__c != null) {
            return visit.VTD1_Protocol_Visit__r.VTD1_Visit_Checklist__c;
        } else {
            return visit.VTD1_Additional_Visit_Checklist__c;
        }
    }
    @TestVisible
    private static String getTimeOfDayTranslate(){
        return '{' +
                    '\"' + VT_D1_TranslateHelper.translate('am') + '\" : \"am \" ,' +
                    '\"' + VT_D1_TranslateHelper.translate('pm') + '\" : \"pm\"' +
                '}';
    }
}