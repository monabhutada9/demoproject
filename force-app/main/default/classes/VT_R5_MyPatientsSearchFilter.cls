/**
 * Created by Yuliya Yakushenkova on 12/10/2020.
 */

public with sharing class VT_R5_MyPatientsSearchFilter implements VT_R5_SearchService.SearchFilter {

    public String search;
    public Id userId;

    public VT_R5_MyPatientsSearchFilter(Id userId, String search) {
        this.userId = userId;
        this.search = search;
    }

    public List<String> getFields() {
        return new List<String>{
                'Id',
                'Status',
                'VTD1_Patient__c',
                'VTD1_Subject_ID__c',
                'VTD1_Reconsent_Needed__c',
                'VTD1_Patient_User__c',
                'VTD1_Patient_User__r.Name',
                'VTD1_Patient_User__r.SmallPhotoUrl',
                'VTD1_Patient_User__r.MediumPhotoUrl',
                'VTD1_Patient__r.VTD1_First_Name__c',
                'VTD1_Patient__r.VTD1_Last_Name__c',
                'VTD1_Study__r.Name',
                'VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c',
                'VTD1_Virtual_Site__r.VTD1_Study_Team_Member__r.User__r.Name',
                'VTD1_Virtual_Site__r.VTR2_Backup_PI_STM__c',
                'VTD1_Virtual_Site__r.VTR2_Backup_PI_STM__r.Study_Team_Member__c'
        };
    }

    public Type getSObjectType() {
        return Case.class;
    }

    public String getFilterString() {
        List<String> filterItems = new List<String>();
        if (String.isNotBlank(search)) {
            filterItems.add('( ' +
                    'VTD1_Patient_User__r.Name LIKE \'%' + search + '%\' OR ' +
                    'VTD1_Patient__r.VTD1_First_Name__c LIKE \'%' + search + '%\' OR ' +
                    'VTD1_Patient__r.VTD1_Last_Name__c LIKE \'%' + search + '%\' )'
            );
        }

        filterItems.add('VTD1_Virtual_Site__c != NULL');
        filterItems.add('VTD1_Virtual_Site__r.VTD1_Study_Team_Member__c != NULL');
        filterItems.add('VTD1_Study__c IN (SELECT Study__c FROM Study_Team_Member__c WHERE User__c = \'' + userId + '\')');
        return String.join(filterItems, ' AND ');
    }

    public String getOrderByString() {
        return 'VTD1_Patient__r.VTD1_First_Name__c, VTD1_Patient__r.VTD1_Last_Name__c';
    }
}