/**
 * Created by Yuliya Yakushenkova on 12/4/2020.
 */
@IsTest
public class VT_R5_MobilePISCRUnscheduledVisitTest {

    private static Id caseId;

    public static void firstTest() {
        new VT_R5_MobilePISCRUnscheduledVisit().getMinimumVersions();

        User usr = fillRestContexts();
        String preferredDate = Date.today().year() + '-' + Date.today().month() + '-' + Date.today().day();
        Map<String, String> params = new Map<String, String>{
                'members' => usr.Id + ';',
                'preferredDate' => preferredDate,
                'caseId' => caseId
        };
        String responseBody = VT_R5_TestRestContext.doTest(usr,
                new VT_R5_TestRestContext.MockRestContext('/mobile/v1/unscheduled-visit/timeslots', params)
        );
        System.debug(responseBody);
    }

    public static void secondTest() {
        User usr = fillRestContexts();

        Map<String, String> params = new Map<String, String>{
                'caseId' => caseId
        };
        String responseBody = VT_R5_TestRestContext.doTest(usr,
                new VT_R5_TestRestContext.MockRestContext('/mobile/v1/unscheduled-visit/participants', params)
        );
    }

    public static void thirdTest() {
        User usr = fillRestContexts();

        Map<String, String> params = new Map<String, String>{
                'caseId' => caseId
        };
//        String responseBody = VT_R5_TestRestContext.doTest(usr,
//                new VT_R5_TestRestContext.MockRestContext('/mobile/v1/unscheduled-visit/types', params)
//        );
    }

    public static void forthTest() {
        User usr = fillRestContexts();
        User pgUser = [
                SELECT Id, VTD1_Profile_Name__c
                FROM User WHERE Username LIKE '%@scott.com'
                AND VTD1_Profile_Name__c = 'Patient Guide' LIMIT 1
        ];

        RequestParams body =
                new RequestParams(
                        System.now().addDays(5).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\''),
                        '45',
                        new Set<Id>{usr.Id, pgUser.Id},
                        'reason for schedule visit')
                        .setExternalParticipant('External Participant', '');

        Map<String, String> params = new Map<String, String>{
                'caseId' => caseId
        };

        String responseBody = VT_R5_TestRestContext.doTest(usr,
                new VT_R5_TestRestContext.MockRestContext(
                        'POST','/mobile/v1/unscheduled-visit/schedule', params, body
                )
        );
        System.debug(responseBody);
    }

    public static User fillRestContexts() {
        User usr = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile2.PI_USERNAME LIMIT 1];
        caseId = [SELECT Id FROM Case LIMIT 1].Id;
        return usr;
    }

    private class RequestParams {
        public Visit visit;
        public Set<Id> members = new Set<Id>();

        public RequestParams(String scheduledDateTime, String duration, Set<Id> members, String reason) {
            this.visit = new Visit(reason, scheduledDateTime, duration, members);
        }

        public RequestParams setExternalParticipant(String name, String email) {
            visit.externalParticipants.add(
                    new ExternalParticipant('External Participant', '')
            );
            return this;
        }
    }

    private class Visit {
        private String reason;
        private String duration;
        private Set<Id> members = new Set<Id>();
        private String scheduledDateTime;
        private Boolean isTelevisit = false;
        private List<ExternalParticipant> externalParticipants = new List<ExternalParticipant>();

        public Visit(String reason, String scheduledDateTime, String duration, Set<Id> members) {
            this.reason = reason;
            this.duration = duration;
            this.members = members;
            this.scheduledDateTime = scheduledDateTime;
        }
    }

    private class ExternalParticipant {
        private String name;
        private String email;

        private ExternalParticipant(String name, String email) {
            this.name = name;
            this.email = email;
        }
    }
}