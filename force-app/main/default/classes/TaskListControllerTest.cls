@IsTest
private class TaskListControllerTest {
	private static final String TASK_STATUS_OPEN = 'Open';
	@IsTest
	static void getCategoriesTest() {
		List<String> categories = TaskListController.getCategories();
		System.assert(!categories.isEmpty(), 'Category list must not be empty');
	}

	@IsTest
	static void getTasksTest() {
		List<String> categories = TaskListController.getCategories();
		String category = categories.get(0);

		insert new Task(Status = TASK_STATUS_OPEN, OwnerId = UserInfo.getUserId(), Category__c = category);

		List<Task> tasksWithCategory = TaskListController.getTasks(TASK_STATUS_OPEN, category);
		List<Task> otherTasks = TaskListController.getTasks(TASK_STATUS_OPEN, null);

		System.assert(!tasksWithCategory.isEmpty(), 'The getTasks() method must extract tasks');
		System.assert(!otherTasks.isEmpty(), 'The getTasks() method must extract tasks');
	}
}