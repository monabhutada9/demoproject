/**
* @author: Carl Judge
* @date: 30-Jan-19
* @description: Test for VT_D1_OutOfOfficeTaskReassigner
**/

@IsTest
public class VT_D1_OutOfOfficeTaskReassignerTest {

    class ObjectsCreator implements Queueable {
        public void execute(QueueableContext context) {
            Case carePlan = [
                    SELECT Id, VTD1_Primary_PG__c, VTD1_Secondary_PG__c, VTR2_SiteCoordinator__c, VTR2_Backup_Site_Coordinator__c
                    FROM Case
                    WHERE RecordType.DeveloperName = 'CarePlan'
            ];
            HealthCloudGA__CarePlanTemplate__c study = [select Id, VTD1_CM_Queue_Id__c from HealthCloudGA__CarePlanTemplate__c];
            study.VTD1_CM_Queue_Id__c = carePlan.VTD1_Primary_PG__c;
            update study;

            Virtual_Site__c virtualSite = [select Id, VTD1_Study_Team_Member__c, VTD1_Study__c from Virtual_Site__c];

            insert new List<VTD1_Protocol_Deviation__c>{
                    new VTD1_Protocol_Deviation__c(
                            VTD1_StudyId__c = study.Id,
                            VTD1_Subject_ID__c = carePlan.Id
                    ),
                    new VTD1_Protocol_Deviation__c(
                            VTD1_StudyId__c = study.Id,
                            VTD1_Virtual_Site__c = virtualSite.Id
                    )};

            insert new List<VTD1_Document__c>{
                    new VTD1_Document__c(
                            VTD1_Site__c  = virtualSite.Id
                    ),
                    new VTD1_Document__c(
                            VTD1_Clinical_Study_Membership__c = carePlan.Id
                    )};

            User PG = [SELECT Id FROM User WHERE Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME AND FirstName LIKE 'PrimaryPGUser%' AND IsActive = TRUE LIMIT 1];
            User PI = [SELECT Id FROM User WHERE Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME AND FirstName LIKE 'PIUser%' AND IsActive = TRUE LIMIT 1];


            VTD1_Action_Item__c actionItem = new VTD1_Action_Item__c(
                    VTD1_Study__c = study.Id,
                    VTD1_SiteId__c = virtualSite.Id,
                    VTD2_Assigned_To__c = PG.Id,
                    VTD1_PI_Name__c = PI.Id,
                    OwnerId = PI.Id);
            insert actionItem;

            VTD1_Patient_Payment__c patientPayment = new VTD1_Patient_Payment__c(VTD1_Clinical_Study_Membership__c = carePlan.Id);
            insert patientPayment;

            VTD1_Order__c patientDelivery = new VTD1_Order__c(VTD1_Case__c = carePlan.Id);
            insert patientDelivery;

            VTD1_Patient_Kit__c patientKit = new VTD1_Patient_Kit__c(VTD1_Case__c = carePlan.Id);
            insert patientKit;

            IMP_Replacement_Form__c replacementForm = new IMP_Replacement_Form__c(VTD1_Case__c = carePlan.Id,
                    VTD1_Replace__c = patientKit.Id);
            insert replacementForm;

            VTD1_Survey__c survey = new VTD1_Survey__c(VTD1_CSM__c = carePlan.Id);
            insert survey;

            insert new List <Event> {
                    new Event(
                            StartDateTime = Datetime.now(),
                            EndDateTime = Datetime.now().addHours(1),
                            RecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('OutOfOffice').getRecordTypeId(),
                            OwnerId = carePlan.VTD1_Primary_PG__c
                    ),
                    new Event(
                            StartDateTime = Datetime.now(),
                            EndDateTime = Datetime.now().addHours(1),
                            RecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('OutOfOffice').getRecordTypeId(),
                            OwnerId = carePlan.VTR2_SiteCoordinator__c
                    )
            };
            
            //Commented and Done the changes by Priyanka Ambre to remove Remote, Onsite references- SH-17441
            //START
            List<Study_Team_Member__c> studyTeamMembers = [select Id, RecordType.Name, Study__c, User__c, User__r.Profile.Name from Study_Team_Member__c where (Study__c = :study.Id and RecordType.Name in ('PG', 'PI')) OR User__r.Profile.Name = 'CRA' ];
            Map<String, Id> stmToStudyMap = new Map<String, Id>();
            Study_Team_Member__c CRAStm = new Study_Team_Member__c();
            for (Study_Team_Member__c studyTeamMember : studyTeamMembers) {
                if(studyTeamMember.User__c != null && studyTeamMember.User__r.Profile.Name=='CRA'){
                    CRAStm = studyTeamMember;
                }else{
                    stmToStudyMap.put(studyTeamMember.Study__c + '_' + studyTeamMember.RecordType.Name, studyTeamMember.User__c);
                }
            }
            /*
            List <Study_Team_Member__c> studyTeamMembers = [select Id, RecordType.Name, Study__c, User__c from Study_Team_Member__c where Study__c = :study.Id and RecordType.Name in ('PG', 'PI')];
            Map <String, Id> stmToStudyMap = new Map<String, Id>();
            for (Study_Team_Member__c studyTeamMember : studyTeamMembers) {
                stmToStudyMap.put(studyTeamMember.Study__c + '_' + studyTeamMember.RecordType.Name, studyTeamMember.User__c);
            }*/

            Study_Site_Team_Member__c sstm1 = new Study_Site_Team_Member__c();
            sstm1.VTR5_Associated_CRA__c = CRAStm.Id;
            sstm1.VTD1_SiteID__c = virtualSite.Id ;
            insert sstm1;

            HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
            objVirtualShare.ParentId = study.Id;
            objVirtualShare.UserOrGroupId = craSTM.User__c;
            objVirtualShare.AccessLevel = 'Edit';
            insert objVirtualShare;
            
            System.runAs(new User(Id = CRAStm.User__c)){
                    VTD1_Monitoring_Visit__c monitoringVisit = new VTD1_Monitoring_Visit__c(
                    VTD1_Study__c = study.Id,
                    VTD1_Virtual_Site__c = virtualSite.Id,
                    VTD1_Visit_Planned_Date__c = System.now(),
                    //Commented by Priyanka Ambre to remove Remote, Onsite references- SH-17441
                    //VTR2_Remote_Onsite__c = 'Remote',
                    VTD1_Site_Visit_Type__c ='Interim Visit'
                );
                insert monitoringVisit;
            }
            //END


        }
    }

    @TestSetup
    private static void setupMethod() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();

        System.enqueueJob(new ObjectsCreator());

    }

    @IsTest
    private static void doTestPart1() {
        Case carePlan = [
            SELECT Id, VTD1_Primary_PG__c, VTD1_Secondary_PG__c, VTR2_SiteCoordinator__c, VTR2_Backup_Site_Coordinator__c, VTD1_Study__c
            FROM Case
            WHERE RecordType.DeveloperName = 'CarePlan'
        ];
        system.debug('carePlan = ' + carePlan);

        VTD1_Monitoring_Visit__c monitoringVisit = [select Id from VTD1_Monitoring_Visit__c];

        VTD1_Actual_Visit__c visit = (VTD1_Actual_Visit__c) new DomainObjects.VTD1_Actual_Visit_t().setVTD1_Case(carePlan.Id)
                .addVTD1_ProtocolVisit_t(
                        new DomainObjects.VTD1_ProtocolVisit_t()
                                .setStudy(carePlan.VTD1_Study__c)
                )
                .persist();

        VTD1_SC_Task__c scTask = new VTD1_SC_Task__c(VTD1_Patient__c = carePlan.Id);
        insert scTask;
        Test.startTest();
        List <Task> tasks =  new List<Task> {
            new Task(
                OwnerId = carePlan.VTD1_Primary_PG__c,
                WhatId = carePlan.Id
            ),
            new Task(
                OwnerId = carePlan.VTD1_Primary_PG__c,
                WhatId = visit.Id
            ),
            new Task(
                OwnerId = carePlan.VTD1_Primary_PG__c,
                WhatId = scTask.Id
            ),
            new Task(
                OwnerId = carePlan.VTR2_SiteCoordinator__c,
                WhatId = visit.Id
            ),
            new Task(
                OwnerId = carePlan.VTR2_SiteCoordinator__c,
                WhatId = monitoringVisit.Id
            )
        };

        for (Task task : tasks) {
            //System.debug('task = ' + task);
        }

        insert tasks;
        Test.stopTest();
        tasks = [select Id, Owner.Name, VTR2_Redirect_Status__c, WhatId from Task];
        for (Task task : tasks) {
            //System.debug('task afer 1= ' + task);
        }
    }

    @IsTest
    private static void doTestPart2() {
        Case carePlan = [
                SELECT Id, VTD1_Primary_PG__c, VTD1_Secondary_PG__c, VTR2_SiteCoordinator__c, VTR2_Backup_Site_Coordinator__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
        ];
        system.debug('carePlan = ' + carePlan);
        Test.startTest();

        VTD1_Patient_Payment__c patientPayment = [select Id from VTD1_Patient_Payment__c];

        VTD1_Order__c patientDelivery = [select Id from VTD1_Order__c limit 1];

        VTD1_Patient_Kit__c patientKit = [select Id from VTD1_Patient_Kit__c limit 1];

        IMP_Replacement_Form__c replacementForm = [select Id from IMP_Replacement_Form__c];

        VTD1_Survey__c survey = [select Id from VTD1_Survey__c];


        List <Task> tasks =  new List<Task> {
                new Task(
                        OwnerId = carePlan.VTD1_Primary_PG__c,
                        WhatId = carePlan.Id
                ),
                new Task(
                        OwnerId = carePlan.VTR2_SiteCoordinator__c,
                        WhatId = patientPayment.Id
                ),
                new Task(
                        OwnerId = carePlan.VTR2_SiteCoordinator__c,
                        WhatId = patientDelivery.Id
                ),
                new Task(
                        OwnerId = carePlan.VTR2_SiteCoordinator__c,
                        WhatId = patientKit.Id
                ),
                new Task(
                        OwnerId = carePlan.VTR2_SiteCoordinator__c,
                        WhatId = replacementForm.Id
                ),
                new Task(
                        OwnerId = carePlan.VTR2_SiteCoordinator__c,
                        WhatId = survey.Id
                )
        };

        for (Task task : tasks) {
            //System.debug('task = ' + task);
        }
        insert tasks;
        Test.stopTest();
        tasks = [select Id, Owner.Name, VTR2_Redirect_Status__c, WhatId from Task];
        for (Task task : tasks) {
            //System.debug('task afer 2= ' + task);
        }
    }

    @IsTest
    private static void doTestPart3() {
        Case carePlan = [
                SELECT Id, VTD1_Primary_PG__c, VTD1_Secondary_PG__c, VTR2_SiteCoordinator__c, VTR2_Backup_Site_Coordinator__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
        ];
        system.debug('carePlan = ' + carePlan);

        List <VTD1_Document__c> documents = [select Id, VTD1_Site__c from VTD1_Document__c];
        Test.startTest();
        List <Task> tasks =  new List<Task> {
                new Task(
                        OwnerId = carePlan.VTD1_Primary_PG__c,
                        WhatId = carePlan.Id
                ),
                new Task(
                        OwnerId = carePlan.VTR2_SiteCoordinator__c,
                        WhatId = documents[0].Id
                ),
                new Task(
                        OwnerId = carePlan.VTR2_SiteCoordinator__c,
                        WhatId = documents[1].Id
                ),
                new Task(
                        OwnerId = carePlan.VTR2_SiteCoordinator__c,
                        WhatId = documents[0].VTD1_Site__c
                )
        };

        for (Task task : tasks) {
            //System.debug('task = ' + task);
        }

        insert tasks;
        Test.stopTest();
        tasks = [select Id, Owner.Name, VTR2_Redirect_Status__c, WhatId from Task];
        for (Task task : tasks) {
            //System.debug('task afer 1= ' + task);
        }
    }

    @IsTest
    private static void doTestPart4() {
        Case carePlan = [
                SELECT Id, VTD1_Primary_PG__c, VTD1_Secondary_PG__c, VTR2_SiteCoordinator__c, VTR2_Backup_Site_Coordinator__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
        ];
        system.debug('carePlan = ' + carePlan);

        List <VTD1_Protocol_Deviation__c> protocolDeviations = [select Id from VTD1_Protocol_Deviation__c];
        Test.startTest();
        List <Task> tasks =  new List<Task> {
                new Task(
                        OwnerId = carePlan.VTD1_Primary_PG__c,
                        WhatId = carePlan.Id
                ),
                new Task(
                        OwnerId = carePlan.VTR2_SiteCoordinator__c,
                        WhatId = protocolDeviations[0].Id
                ),
                new Task(
                        OwnerId = carePlan.VTR2_SiteCoordinator__c,
                        WhatId = protocolDeviations[1].Id
                )
        };

        for (Task task : tasks) {
            //System.debug('task = ' + task);
        }

        insert tasks;
        Test.stopTest();
        tasks = [select Id, Owner.Name, VTR2_Redirect_Status__c, WhatId from Task];
        for (Task task : tasks) {
            //System.debug('task afer 1= ' + task);
        }
    }

    @IsTest
    private static void doTestWithoutBackUpSCROnCase() {
        Case carePlan = [
                SELECT Id, VTD1_Primary_PG__c, VTD1_Secondary_PG__c, VTR2_SiteCoordinator__c, VTR2_Backup_Site_Coordinator__c, VTD1_Study__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
        ];
        system.debug('carePlan = ' + carePlan);
        carePlan.VTR2_Backup_Site_Coordinator__c = null;
        update carePlan;

        VTD1_Actual_Visit__c visit = (VTD1_Actual_Visit__c) new DomainObjects.VTD1_Actual_Visit_t().setVTD1_Case(carePlan.Id)
                .addVTD1_ProtocolVisit_t(
                        new DomainObjects.VTD1_ProtocolVisit_t()
                                .setStudy(carePlan.VTD1_Study__c)
                )
                .persist();

        VTD1_Monitoring_Visit__c monitoringVisit = [select Id from VTD1_Monitoring_Visit__c];

        List <Task> tasks =  new List<Task> {
                new Task(
                        OwnerId = carePlan.VTR2_SiteCoordinator__c,
                        WhatId = monitoringVisit.Id
                ),
                new Task(
                        OwnerId = carePlan.VTR2_SiteCoordinator__c,
                        WhatId = visit.Id
                )
        };

        for (Task task : tasks) {
            //System.debug('task = ' + task);
        }

        Test.startTest();
        insert tasks;
        Test.stopTest();
        tasks = [select Id, Owner.Name, VTR2_Redirect_Status__c, WhatId from Task];
        for (Task task : tasks) {
            //System.debug('task afer = ' + task);
        }
    }
}