/**
 * Created by Leonid Bartenev
 * Edited by Jane Ivanova
 **/

public class VT_D1_RequestBuilder_SendSubject implements VT_D1_RequestBuilder{
    
    // Data classes: ---------------------------------------------------------------------------------------------------
    public class Subject {
        public String protocolId;
        public String subjectId;
        //public String sourceSystemId;
        //public Boolean createdInCenduitCSM;
        public String investigatorId;
        public SubjectDetails subjectDetails;
        public AuditLog auditLog;
        //SH-16437 - bypassDeliveryPartnerInd attribute is used by mulesoft to bypass CSM call
        public Boolean bypassDeliveryPartnerInd;
    }
    
    public class SubjectDetails {
        public String FirstName;
        public String LastName;
        public Date Birthdate;
        public String LanguageId;
        public String EMail;
        public String Phone1;
        public String TypePhone1;
        public String Phone2;
        public String TypePhone2;
        public Boolean EmailNotifications;
        public Boolean TextMessageCommunications;
        public Boolean PhoneCommunications;
        public String Status;
        //public String PreviousStatus;
        //public String ReasonCode;
        //public String Initials;
        //public Boolean CreatedInCenduitCSM;
        public List<SubjectAddress> Addresses;
        public List<Stratification> Demographics;
    }
    
    public class SubjectAddress {
        public String Type;
        public Boolean Default_replace;		// system restriction, leave it in this spelling
        public String Address1;
        public String Address2;
        public String City;
        public String State;
        public String PostalCode;
        public String Country;
        public SubjectCaregiver Caregiver;
        public List<SubjectAvailability> Availability;
    }
    
    private class Stratification {
        String Key;
        String Value;
    }
    
    public class SubjectCaregiver {
        public String FirstName;
        public String LastName;
        public String EMail;
        public String CellPhone;
        public String Phone;
    }
    
    public class SubjectAvailability {
        public String Rule;
        public String Type;
        public String DayOfWeek;
        public String StartHour;
        public String EndHour;
        public Date StartDate;
        public Date EndDate;
    }
    
    public class AuditLog {
        public String UserId;
        public Datetime Timestamp;
        
        public AuditLog(){
            UserId = UserInfo.getUserId();
            Timestamp = Datetime.now();
        }
    }
    
    
    public Case c;
    
    public VT_D1_RequestBuilder_SendSubject(Id caseId){
        c = getCase(caseId);
        //SH-16437 - Initializing custom metadata
        VT_D1_CaseHandler.getSponsorSpecificProtocolConfig(new Set<Id>{c.VTD1_Study__c});
    }
    
    // Build logic: ----------------------------------------------------------------------------------------------------
    public String buildRequestBody() {
        List<Contact> caseContacts = [
                SELECT Id,
                        //HealthCloudGA__PrimaryLanguage__c,
                        Name,
                        Email,
                        LegalFirstName__c,
                        LastName,
                        RecordType.DeveloperName,
                        VTD1_Account__c,
                        VTD1_EmailNotificationsFromCourier__c,
                        VTD1_TextMessageCommunicationFromCourier__c,
                        VTD1_PhoneCommunicationFromCourier__c,
                        //VTD1_DaysNotAvailable__c,
                        VT_D1_DeliveryTimeMondayFrom__c,
                        VT_D1_DeliveryTimeMondayTo__c,
                        VT_D1_DeliveryTimeTuesdayFrom__c,
                        VT_D1_DeliveryTimeTuesdayTo__c,
                        VT_D1_DeliveryTimeWednesdayFrom__c,
                        VT_D1_DeliveryTimeWednesdayTo__c,
                        VT_D1_DeliveryTimeThursdayFrom__c,
                        VT_D1_DeliveryTimeThursdayTo__c,
                        VT_D1_DeliveryTimeFridayFrom__c,
                        VT_D1_DeliveryTimeFridayTo__c,
                        VT_D1_DeliveryTimeSaturdayFrom__c,
                        VT_D1_DeliveryTimeSaturdayTo__c
                FROM Contact
                WHERE AccountId =: c.AccountId
                AND RecordType.DeveloperName IN ('Caregiver', 'Patient')
                ORDER BY RecordType.DeveloperName
        ];
        Contact addressContact;
        if(caseContacts.size() > 0) addressContact = caseContacts[0];
    
        List<Address__c> addresses = new List<Address__c>();
        List<VT_D1_Phone__c> phones = new List<VT_D1_Phone__c>();
    
        if(addressContact != null){
            System.debug('ADDR CONTACT: ' + addressContact.Id);
            addresses = [
                    SELECT Id, Name,
                            VTD1_Contact__c,
                            VTD1_AddressType__c,
                            VTD1_Primary__c,
                            VTD1_Address2__c,
                            City__c,
                            State__c,
                            ZipCode__c,
                            Country__c
                    FROM Address__c
                    WHERE VTD1_Contact__c = : addressContact.Id
                    AND VTD1_Primary__c = TRUE     // isDeliveryAddress__c replased with VTD1_Primary__c
            ];
        }
    
        //phones of all case contacts:
        if(c.ContactId != null){
            phones = [
                    SELECT Id, PhoneNumber__c, IsPrimaryForPhone__c, Type__c, VTD1_Contact__c
                    FROM VT_D1_Phone__c
                    WHERE VTD1_Contact__c IN: caseContacts
                    ORDER BY IsPrimaryForPhone__c DESC
            ];
        }
    
    
        //init subject details:
        SubjectDetails sd = new SubjectDetails();
        sd.FirstName = c.Account.VTD1_LegalFirstName__c;
        sd.LastName = c.Account.VTD1_LastName_del__c;
        sd.Status = c.Status;
        //sd.PreviousStatus = c.VTD2_PreviousStatus__c;
        //sd.ReasonCode = c.VTD1_Reason_Code__c;
        sd.Birthdate = c.Contact.Birthdate;//c.Account.VTD1_DateOfBirth__c;
        //if (c.Contact.HealthCloudGA__PrimaryLanguage__c != null) sd.LanguageId = c.Contact.HealthCloudGA__PrimaryLanguage__c;
        sd.LanguageId = c.VTD1_Patient_User__r.LanguageLocaleKey.substring(0,2).toUpperCase();
        if (c.Contact.Email != null) sd.EMail = c.Contact.Email;
        sd.EmailNotifications = c.Contact.VTD1_EmailNotificationsFromCourier__c;
        sd.TextMessageCommunications = c.Contact.VTD1_TextMessageCommunicationFromCourier__c;
        sd.PhoneCommunications = c.Contact.VTD1_PhoneCommunicationFromCourier__c;
        
        //set phones:
        Integer index = 1;
        List<VT_D1_Phone__c> caregiverPhones = new List<VT_D1_Phone__c>();
        for(VT_D1_Phone__c phone : phones){
            if(addressContact.RecordType.DeveloperName == 'Caregiver' && phone.VTD1_Contact__c == addressContact.Id) {
                caregiverPhones.add(phone);
                continue;
            }
            if(index == 1){
                sd.Phone1 = phone.PhoneNumber__c;
                if(phone.Type__c != null) sd.TypePhone1 = phone.Type__c;//phoneTypeMap.get(phone.Type__c);
            }
            if(index == 2){
                sd.Phone2 = phone.PhoneNumber__c;
                if(phone.Type__c != null) sd.TypePhone2 = phone.Type__c;//phoneTypeMap.get(phone.Type__c);
                break;
            }
            index++;
        }
    
        //init subject
        Subject subjectObj = new Subject();
        subjectObj.protocolId = c.VTD1_Study__r.Name;
        subjectObj.subjectId = c.VTD1_Subject_ID__c;
        
        // SH-16437 bypassDeliveryPartnerInd will be only true when we define it at custom metadata level as sponsor specific study setting
        // bypassDeliveryPartnerInd is used by to bypass call to cenduit
        VTR5_Sponsor_Specific_Protocol_Config__mdt protocolConfig = VT_D1_CaseHandler.sponsorSpecificProtocolConfig.get(c.VTD1_Study__c);
        subjectObj.bypassDeliveryPartnerInd = false;
        if(protocolConfig != null && protocolConfig.VTR5_Bypass_Delivery_Partner__c ){
            subjectObj.bypassDeliveryPartnerInd = true;
        }
        
        subjectObj.investigatorId = c.VTD1_PI_user__r.VTD1_CTMS_Row_ID__c;
        //subjectObj.sourceSystemId = c.VTD1_Patient_ID_RRID__c;
        //subjectObj.createdInCenduitCSM = c.VTD1_CreatedInCenduitCSM__c;
        subjectObj.subjectDetails = sd;
    
        //init address:
        SubjectAddress subjAddr;
        if (addresses.size() > 0) {
            subjAddr = new SubjectAddress();
            Address__c addr = addresses[0];
            if (!String.isBlank(addr.VTD1_AddressType__c)) subjAddr.Type = 'Residential'; //addr.VTD1_AddressType__c replaced with Residential
            subjAddr.Default_replace = addr.VTD1_Primary__c;
            if (!String.isBlank(addr.Name)) subjAddr.Address1 = addr.Name;
            if (!String.isBlank(addr.VTD1_Address2__c)) subjAddr.Address2 = addr.VTD1_Address2__c;
            if (!String.isBlank(addr.City__c)) subjAddr.City = addr.City__c;
            if (!String.isBlank(addr.State__c)) subjAddr.State = addr.State__c;
            if (!String.isBlank(addr.ZipCode__c)) subjAddr.PostalCode = addr.ZipCode__c;
            if (!String.isBlank(addr.Country__c)) subjAddr.Country = addr.Country__c;
        }
        //init subject caregiver
        if (addressContact != null && addressContact.RecordType.DeveloperName == 'Caregiver') {
            SubjectCaregiver subjCaregiver = new SubjectCaregiver();
            if (!String.isBlank(addressContact.Name)) subjCaregiver.FirstName = addressContact.Name;
            if (!String.isBlank(addressContact.LastName)) subjCaregiver.LastName = addressContact.LastName;
            if (!String.isBlank(addressContact.Email)) subjCaregiver.EMail = addressContact.Email;
            for (VT_D1_Phone__c phone : caregiverPhones){
                if(phone.Type__c == 'Mobile') {
                    subjCaregiver.CellPhone = phone.PhoneNumber__c;
                }else{
                    subjCaregiver.Phone = phone.PhoneNumber__c;
                }
            }
            if(subjAddr != null) subjAddr.Caregiver = subjCaregiver;
        }
        //init availability:
        if(subjAddr != null){
            subjAddr.Availability = generateAvailability(addressContact);
            sd.Addresses = new List<SubjectAddress>{subjAddr};
        }
    
        //init demographics:
        sd.Demographics = generateDemographics(c);
    
        subjectObj.auditLog = new AuditLog();
        String requestBody = JSON.serializePretty(subjectObj, true).replace('Default_replace', 'Default');
        System.debug('REQUEST BODY: \n' + requestBody);
        return requestBody; 
    }
    
    
    // auxiliary methods: ----------------------------------------------------------------------------------------------
    public static Case getCase(Id caseId){
        return [
                SELECT 	Id,
                		Status,
                		VTD2_PreviousStatus__c,
                		//VTD1_Reason_Code__c,
                		//VTD1_Patient_ID_RRID__c,
                		AccountId,
                        VTD1_CreatedInCenduitCSM__c,
                        VTD1_Study__r.Name,
                        VTD1_Subject_ID__c,
                        VTD1_PI_user__r.VTD1_CTMS_Row_ID__c,
                        VTD1_Patient_User__r.LanguageLocaleKey,
                        Account.VTD1_LegalFirstName__c,
                        Account.VTD1_LastName_del__c,
                        Account.VTD1_Age__c,
                        ContactId, Contact.Id,
                        Contact.Birthdate,
                        //Contact.HealthCloudGA__PrimaryLanguage__c,
                        Contact.Name,
                        Contact.Email,
                        Contact.VTD1_EmailNotificationsFromCourier__c,
                        Contact.VTD1_TextMessageCommunicationFromCourier__c,
                        Contact.VTD1_PhoneCommunicationFromCourier__c,
                        Contact.RecordType.DeveloperName,
                        Contact.VTD1_Account__c
                FROM Case
                WHERE Id = :caseId
        ];
    }
    
    public static List<SubjectAvailability> generateAvailability(Contact patientOrCaregiver) {
        List<SubjectAvailability> availabilityList = new List<SubjectAvailability>();

		SubjectAvailability subjAvailability = null;
                
        if( patientOrCaregiver.VT_D1_DeliveryTimeMondayFrom__c != null ) {
        	subjAvailability = new SubjectAvailability();
            subjAvailability.Rule = 'Available';
            subjAvailability.Type = 'Days';
            subjAvailability.DayOfWeek = 'Monday';
            subjAvailability.StartHour = patientOrCaregiver.VT_D1_DeliveryTimeMondayFrom__c;
            subjAvailability.EndHour = patientOrCaregiver.VT_D1_DeliveryTimeMondayTo__c;
            availabilityList.add(subjAvailability);
        }

        if( patientOrCaregiver.VT_D1_DeliveryTimeTuesdayFrom__c != null ) {
        	subjAvailability = new SubjectAvailability();
            subjAvailability.Rule = 'Available';
            subjAvailability.Type = 'Days';
            subjAvailability.DayOfWeek = 'Tuesday';
            subjAvailability.StartHour = patientOrCaregiver.VT_D1_DeliveryTimeTuesdayFrom__c;
            subjAvailability.EndHour = patientOrCaregiver.VT_D1_DeliveryTimeTuesdayTo__c;
            availabilityList.add(subjAvailability);
        }
        
        if( patientOrCaregiver.VT_D1_DeliveryTimeWednesdayFrom__c != null ) {
        	subjAvailability = new SubjectAvailability();
            subjAvailability.Rule = 'Available';
            subjAvailability.Type = 'Days';
            subjAvailability.DayOfWeek = 'Wednesday';
            subjAvailability.StartHour = patientOrCaregiver.VT_D1_DeliveryTimeWednesdayFrom__c;
            subjAvailability.EndHour = patientOrCaregiver.VT_D1_DeliveryTimeWednesdayTo__c;
            availabilityList.add(subjAvailability);
        }
        
        if( patientOrCaregiver.VT_D1_DeliveryTimeThursdayFrom__c != null ) {
        	subjAvailability = new SubjectAvailability();
            subjAvailability.Rule = 'Available';
            subjAvailability.Type = 'Days';
            subjAvailability.DayOfWeek = 'Thursday';
            subjAvailability.StartHour = patientOrCaregiver.VT_D1_DeliveryTimeThursdayFrom__c;
            subjAvailability.EndHour = patientOrCaregiver.VT_D1_DeliveryTimeThursdayTo__c;
            availabilityList.add(subjAvailability);
        }
        
        if( patientOrCaregiver.VT_D1_DeliveryTimeFridayFrom__c != null ) {
        	subjAvailability = new SubjectAvailability();
            subjAvailability.Rule = 'Available';
            subjAvailability.Type = 'Days';
            subjAvailability.DayOfWeek = 'Friday';
            subjAvailability.StartHour = patientOrCaregiver.VT_D1_DeliveryTimeFridayFrom__c;
            subjAvailability.EndHour = patientOrCaregiver.VT_D1_DeliveryTimeFridayTo__c;
            availabilityList.add(subjAvailability);
        }
        
        if( patientOrCaregiver.VT_D1_DeliveryTimeSaturdayFrom__c != null ) {
        	subjAvailability = new SubjectAvailability();
            subjAvailability.Rule = 'Available';
            subjAvailability.Type = 'Days';
            subjAvailability.DayOfWeek = 'Saturday';
            subjAvailability.StartHour = patientOrCaregiver.VT_D1_DeliveryTimeSaturdayFrom__c;
            subjAvailability.EndHour = patientOrCaregiver.VT_D1_DeliveryTimeSaturdayTo__c;
            availabilityList.add(subjAvailability);
        }
        return availabilityList;
    }
    
    public static List<Stratification> generateDemographics(Case c) {
        List<VTD1_Patient_Stratification__c> strats = [
                SELECT Id, Value__c, VTD1_Study_Stratification__r.Name
                FROM VTD1_Patient_Stratification__c
                WHERE VTD1_Patient__c = :c.Id
        ];

        List<Stratification> paramsList = new List<Stratification>();
        for (VTD1_Patient_Stratification__c stratItem : strats) {
            Stratification param = new Stratification();
            param.Key = stratItem.VTD1_Study_Stratification__r.Name;
            param.Value = stratItem.Value__c;
            paramsList.add(param);
        }
        return paramsList;
    }

}