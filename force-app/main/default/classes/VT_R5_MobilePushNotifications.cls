/**
 * @author: Alexander Komarov
 * @date: 14.09.2020
 * @description: android push notifications
 */

public without sharing class VT_R5_MobilePushNotifications {

    private Map<Id, VTD1_NotificationC__c> mapNotificationsToSend;
    private Map<Id, User> notificationWithUser;
    private static Map<Id, Id> notificationToActualVisit = new Map<Id, Id>();
    private static Map<Id, String> notificationsIdToVideoSession = new Map<Id, String>();
    private static final Set<String> VALID_LINKS_TO_OBJECTS_FOR_PATIENT = new Set<String>{
            'calendar',
            'my-diary',
            'full-profile',
            'my-study-supplies',
            'medical-records'
    };

    public VT_R5_MobilePushNotifications(Map<Id, User> users, Map<Id, VTD1_NotificationC__c> mapNotificationsToSend) {
        this.notificationWithUser = users;
        this.mapNotificationsToSend = mapNotificationsToSend;
    }

    public VT_R5_MobilePushNotifications(List<VTD1_NotificationC__c> notifications) {
        Map<Id, VTD1_NotificationC__c> mapNotificationsToSend = new Map<Id, VTD1_NotificationC__c>();
        Map<Id, User> users = new Map<Id, User>();
        Map<Id, User> receiversMap = getReceivers(notifications);
        for (VTD1_NotificationC__c n : notifications) {
            if (receiversMap.containsKey(n.VTD1_Receivers__c)) {
                if (receiversMap.containsKey(n.OwnerId) && !receiversMap.get(n.OwnerId).MobilePushRegistrations__r.isEmpty()) {
                    mapNotificationsToSend.put(n.Id, n);
                    users.put(n.Id, receiversMap.get(n.OwnerId));
                }
            }
        }
        this.notificationWithUser = users;
        this.mapNotificationsToSend = mapNotificationsToSend;
    }

    private Map<Id, User> getReceivers(List<VTD1_NotificationC__c> notifications) {
        Map<Id, User> result = new Map<Id, User>();
        Set<Id> ownerIds = new Set<Id>();
        for (VTD1_NotificationC__c n : notifications) {
            ownerIds.add(n.VTD1_Receivers__c);
        }

        result.putAll(new Map<Id, User>([
                SELECT IsActive,
                        Profile.Name,
                        Contact.VTD1_Clinical_Study_Membership__r.Status, (SELECT VTR5_ConnectionToken__c, VTR5_ServiceType__c, VTR5_User__c FROM MobilePushRegistrations__r ORDER BY CreatedDate DESC LIMIT 50)
                FROM User
                WHERE Id IN :ownerIds
                AND IsActive = TRUE
                AND (Profile.Name IN :VT_R2_NotificationCHandler.piScrProfiles OR Contact.VTD1_Clinical_Study_Membership__r.Status IN :VT_R2_NotificationCHandler.allowedCaseStatuses)
        ]));
        return result;
    }

    private List<Sender> formNotificationsChunks() {
        List<Sender> jobs = new List<VT_R5_MobilePushNotifications.Sender>();
        List<VTR5_Mobile_App_Setting__mdt> generalSettingsList = [
                SELECT VTR5_FCMServerKey__c
                FROM VTR5_Mobile_App_Setting__mdt
        ];
        if (generalSettingsList[0].VTR5_FCMServerKey__c == null) return jobs;

        Integer counterJobs = 0;
        Integer counterCallout = 0;

        Map<String, String> tokenToNotificationId = new Map<String, String>();

        for (Id notificationId : mapNotificationsToSend.keySet()) {
            if (notificationWithUser.containsKey(notificationId) && !notificationWithUser.get(notificationId).MobilePushRegistrations__r.isEmpty())  {
                for (VTR5_MobilePushRegistrations__c registration : notificationWithUser.get(notificationId).MobilePushRegistrations__r) {
                    if (registration.VTR5_ConnectionToken__c != null) {
                        tokenToNotificationId.put(registration.VTR5_ConnectionToken__c, notificationId);
                        counterCallout++;
                        if (counterCallout == 90) {
                            counterCallout = 0;
                            jobs.add(new Sender(tokenToNotificationId, generalSettingsList[0].VTR5_FCMServerKey__c));
                            tokenToNotificationId = new Map<String, String>();
                            counterJobs++;
                        }
                        if (counterJobs > 42) break;
                    }
                }
            }
        }
        jobs.add(new Sender(tokenToNotificationId, generalSettingsList[0].VTR5_FCMServerKey__c));
        return jobs;
    }


    public void execute() {
        System.debug('JOBS SENDING');
        List<Sender> jobs = formNotificationsChunks();
        if (jobs.size() > 1) {
            for (Integer i = 0; i < jobs.size() - 1; i++) {
                jobs[0].addToChain(jobs[i + 1]);
            }
        }
        jobs[0].enqueue();
        System.debug('JOBS SENDED');
    }

    private class Payload {
        String to;
        Map<String, Object> data = new Map<String, String>();
        Map<String, Object> notification = new Map<String, String>();

        public Payload(VTD1_NotificationC__c notification, String target) {
            this.to = target;
            if (notificationsIdToVideoSession.containsKey(notification.Id)) {
                this.data.put('sessionId', notificationsIdToVideoSession.get(notification.Id));
            }
            if (notificationToActualVisit.containsKey(notification.Id)) {
                this.data.put('visitId', notificationToActualVisit.get(notification.Id));
            }
            String relatedObject = '';
            String pushMessage = notification.Message__c;
            if (String.isNotBlank(notification.Type__c)) {
                if (notification.Type__c.equals('Messages')) {
                    if (notification.Number_of_unread_messages__c != null) {
                        pushMessage += ' ' + String.valueOf(Integer.valueOf(notification.Number_of_unread_messages__c));
                    }
                    relatedObject = 'messages';
                }
                if (notification.Type__c.equals('General')) {
                    if (VALID_LINKS_TO_OBJECTS_FOR_PATIENT.contains(notification.Link_to_related_event_or_object__c)) {
                        relatedObject = notification.Link_to_related_event_or_object__c;
                    }
                }
            }

            this.data.put('notificationId', notification.Id);
            this.data.put('userId', notification.VTD1_Receivers__c);
            this.data.put('relatedObject', relatedObject);
            this.data.put('message', pushMessage);
            this.notification.put('body', pushMessage);
            this.notification.put('title', notification.Title__c);

            if (notification.VTR3_Notification_Type__c != null && notification.VTR3_Notification_Type__c.equals('eCOA Notification')) {
                this.data.put('successBannerNeed', 'true');
                this.data.put('successBannerText', notification.Title__c);
                this.data.put('relatedObject', 'my-diary');
            }
        }
    }

    private class Sender extends VT_R3_AbstractChainableQueueable implements Database.AllowsCallouts {
        public override Type getType() {
            return Sender.class;
        }
        private Map<String, Id> tokenToNotificationId = new Map<String, Id>();
        private Set<Id> notificationIds = new Set<Id>();
        private List<Payload> payloads = new List<Payload>();
        private String serverKey;
        private final String FCMEndpoint = 'https://fcm.googleapis.com/fcm/send';

        Sender(Map<String, String> tokenToNotificationId, String serverKey) {
            for (String token : tokenToNotificationId.keySet()) {
                Id notificationId = Id.valueOf(tokenToNotificationId.get(token));
                this.tokenToNotificationId.put(token, notificationId);
                notificationIds.add(notificationId);
            }
            this.serverKey = serverKey;
        }


        public override void executeLogic() {
            Map<Id, VTD1_NotificationC__c> notificationsToSend = getNotificationsToSend();
            try {
                VT_D1_TranslateHelper.translate(notificationsToSend.values(), true);
            } catch (Exception e) {
                System.debug(e.getStackTraceString() + e.getMessage());
                System.debug('TH Fail');
                //log this
            }
            prepareVideoData(notificationsToSend.values());

            for (String token : tokenToNotificationId.keySet()) {
                payloads.add(new Payload(notificationsToSend.get(tokenToNotificationId.get(token)), token));
            }

            for (Payload payload : payloads) {
                callout(payload);
            }
        }

        private Map<Id, VTD1_NotificationC__c> getNotificationsToSend() {
            Map<Id, VTD1_NotificationC__c> notificationsToSend = new Map<Id, VTD1_NotificationC__c>([
                    SELECT Number_of_unread_messages__c,
                            LastModifiedDate,
                            VTR5_HideNotification__c,
                            HasDirectLink__c,
                            Name,
                            CreatedById,
                            OwnerId,
                            isActualVisitLink__c,
                            VTR5_TitleValue__c,
                            VTD2_DoNotSendEmail__c,
                            VTR4_CurrentPatientCase__c,
                            IsDeleted,
                            VTR4_Recurrent_Sequence_Inactive__c,
                            VTR3_Notification_Type__c,
                            Title__c,
                            SystemModstamp,
                            VTD1_Receivers__c,
                            VTD1_CSM__c,
                            VTD2_VisitID__c,
                            VTD2_DoNotDuplicate__c,
                            Type__c,
                            Message__c,
                            CreatedDate,
                            VTD1_Read__c,
                            VTR2_Study__c,
                            Id,
                            VDT2_Unique_Code__c,
                            LastModifiedById,
                            Link_to_related_event_or_object__c,
                            VTD1_Parameters__c
                    FROM VTD1_NotificationC__c
                    WHERE Id IN :notificationIds
            ]);
            return notificationsToSend;
        }

        private void callout(Payload payload) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(FCMEndpoint);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization', 'key=' + serverKey);
            req.setBody(JSON.serialize(payload, true));

            System.debug(req.getBody());
            System.debug(JSON.serialize(payload, true));
            Http http = new Http();
            if (!Test.isRunningTest()) {
                HttpResponse resp = http.send(req);
                System.debug(JSON.serialize(resp.getBody()));
                System.debug(resp.getStatus() + resp.getStatusCode());
            }
        }

        private void prepareVideoData(List<VTD1_NotificationC__c> notificationsToSend) {
            Map<Id, Id> actualVisitToNotification = new Map<Id, Id>();
            for (VTD1_NotificationC__c notification : notificationsToSend) {
                if (String.isNotBlank(notification.VTD2_VisitID__c)) {
                    actualVisitToNotification.put(notification.VTD2_VisitID__c, notification.Id);
                    notificationToActualVisit.put(notification.Id, notification.VTD2_VisitID__c);
                }
            }
            List<Video_Conference__c> videoConferences = [
                    SELECT Id, SessionId__c, VTD1_Actual_Visit__c
                    FROM Video_Conference__c
                    WHERE VTD1_Actual_Visit__c IN :actualVisitToNotification.keySet()
            ];
            for (Video_Conference__c vc : videoConferences) {
                if (String.isNotBlank(vc.SessionId__c)) {
                    notificationsIdToVideoSession.put(actualVisitToNotification.get(vc.VTD1_Actual_Visit__c), vc.SessionId__c);
                }
            }
        }
    }
}