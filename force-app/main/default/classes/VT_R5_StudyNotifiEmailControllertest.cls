/**
* Test Class for VT_R5_StudyNotificationEmailController.

*
* @author OF
*/

@isTest
public class VT_R5_StudyNotifiEmailControllertest {
    
    @testSetup static void setup() {
        VTD1_NotificationC__c ntc = new VTD1_NotificationC__c(VTD1_Receivers__c = UserInfo.getUserId(), OwnerId=UserInfo.getUserId(), Title__c='Study Device',Message__c='Test Message');
        insert ntc;
    }
    
    @isTest
    public  static void controllermethodforelse(){
        VTD1_NotificationC__c notificationC=new VTD1_NotificationC__c();
        notificationC=[Select id from VTD1_NotificationC__c limit 1 ];
        VT_R5_StudyNotificationEmailController studynotify= new VT_R5_StudyNotificationEmailController();
        studynotify.notificationId= notificationC.Id;
        studynotify.getnotificationRecord();
        
    }
    
     @isTest
    public  static void controllermethodforif(){
        VTD1_NotificationC__c notificationC=new VTD1_NotificationC__c();
        notificationC=[Select id,Message__c from VTD1_NotificationC__c limit 1 ];
        notificationC.Message__c= 'VTR5_BTraceDeviceReadingReminderNotification' ;
        update notificationC;
        VT_R5_StudyNotificationEmailController studynotify= new VT_R5_StudyNotificationEmailController();
        studynotify.notificationId= notificationC.Id;
        studynotify.getnotificationRecord();
      
    }
    
    @isTest
    public  static void controllermethodforcatch(){
        
        VT_R5_StudyNotificationEmailController studynotify= new VT_R5_StudyNotificationEmailController();
        studynotify.getnotificationRecord();
      
    }
}