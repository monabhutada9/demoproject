/**
 * @author: Alexander Komarov
 * @date: 22.09.2020
 * @description:
 */

@IsTest
public with sharing class VT_R5_MobileTimezoneUpdaterTest {

    private static User u;
    static {
        u = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = TRUE LIMIT 1];
    }

    public static void test1() {
        System.runAs(u) {
            setRestContext('/patient/v0/timezone-update/', 'POST', 'Unsupported');
            VT_R5_MobileRestRouter.doPOST();
        }
    }
    public static void test2() {
        System.runAs(u) {
            setRestContext('/patient/v1/timezone-update/', 'POST', '{"newTimezone" : null}');
            VT_R5_MobileRestRouter.doPOST();
        }
    }
    public static void test3() {
        System.runAs(u) {
            setRestContext('/patient/v1/timezone-update/', 'POST', '{"newTimezone" : "Pacific/Marquesas"}');
            VT_R5_MobileRestRouter.doPOST();
        }
    }
    public static void test4() {
        System.runAs(u) {
            setRestContext('/patient/v1/timezone-update/', 'POST', '{"newTimezone" : "Pacific/Kiritimati"}');
            VT_R5_MobileRestRouter.doPOST();
        }
    }
    public static void test5() {
        System.runAs(u) {
            setRestContext('/patient/v1/timezone-update/', 'POST', 'not-json');
            VT_R5_MobileRestRouter.doPOST();
        }
    }
    private static void setRestContext(String uri, String method, String body) {
        RestContext.response = new RestResponse();
        RestContext.request = new RestRequest();
        RestContext.request.httpMethod = method;
        RestContext.request.requestURI = uri;
        if (body != null) RestContext.request.requestBody = Blob.valueOf(body);
    }
}