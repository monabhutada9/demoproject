/********************************************************
* @Class Name   : VT_R5_CreateQueueMemeberToSSTM
* @Created Date : 22/10/2020 
* @Version      : R5.5
* @group-content: http://jira.quintiles.net/browse/SH-17314
* @Purpose      : This batch class is used to Create queue member on insert of the SSTM
*                  This is the one time execution batch
* *****************************************************/
public with Sharing class VT_R5_CreateQueueMemeberToSSTM implements Database.Batchable<sObject>{
    public Database.QueryLocator start(Database.BatchableContext BC){
        string strQuery = 'SELECT ID,'+
            +'VTR5_Associated_CRA__c,'+
            +'VTD1_SiteID__c,'+
            +'VTR5_Associated_CRA__r.User__c '+
            +'FROM Study_Site_Team_Member__c '+
            +'WHERE VTR5_Associated_CRA__c != Null AND VTD1_SiteID__c != Null';
        
        return Database.getQueryLocator(strQuery);
    }
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        try{
            Map<string,Group>mapOFGroup = new Map<string,Group>();
            List<QueueSObject> queueSObjectcreation = new List<QueueSObject>();
            List<GroupMember>lstMember = new List<GroupMember>();
            Set<String>setOfGroupName = new Set<String>();
            for(Study_Site_Team_Member__c objSSTM : (List<Study_Site_Team_Member__c>)scope){
                setOfGroupName.add(objSSTM.VTD1_SiteID__c+'_CRAQueue');
            }
            for(Group objGroup : [SELECT ID,
                                  Type,
                                  Name,
                                  DeveloperName
                                  FROM Group 
                                  WHERE Type = 'Queue' AND DeveloperName IN:setOfGroupName
                                 ]){
                                     
                                     if(objGroup != Null){
                                         mapOFGroup.put(objGroup.DeveloperName , objGroup);   
                                     }
                                 }
            for(Study_Site_Team_Member__c objSSTM : (List<Study_Site_Team_Member__c>)scope){
                
                if(mapOFGroup.containsKey(objSSTM.VTD1_SiteID__c+'_CRAQueue')){
                    
                    GroupMember objMember = new GroupMember();
                    objMember.UserOrGroupId = objSSTM.VTR5_Associated_CRA__r.User__c;
                    objMember.GroupId = mapOFGroup.get(objSSTM.VTD1_SiteID__c+'_CRAQueue').Id;
                    lstMember.add(objMember); 
                    
                    QueueSObject craTaskObj = new QueueSObject(SobjectType='VTR5_CRA_Task__c', QueueId = mapOFGroup.get(objSSTM.VTD1_SiteID__c+'_CRAQueue').Id);
                    queueSObjectcreation.add(craTaskObj); 
                    QueueSObject docTaskObj = new QueueSObject(SobjectType='VTD1_Document__c', QueueId = mapOFGroup.get(objSSTM.VTD1_SiteID__c+'_CRAQueue').Id);
                    queueSObjectcreation.add(docTaskObj);
                }
                
            }
            
            if(lstMember.size() > 0 && lstMember != Null){
                List<Database.SaveResult> lstResult = Database.insert(lstMember,false);
                ErrorLogUtility.logErrorsInBulk(lstResult,lstMember,Null,'VT_R5_CreateQueueMemeberToSSTM');
            }  
            //Insert Sobject CRA TASK,Document
            if(queueSObjectcreation.size() > 0 && queueSObjectcreation != Null){
                List<Database.SaveResult> lstResult = Database.insert(queueSObjectcreation,false);
                ErrorLogUtility.logErrorsInBulk(lstResult,queueSObjectcreation,Null,'VT_R5_CreateQueueMemeberToSSTM');
            } 
        }Catch(Exception ex){
            
            System.debug('Exception Meesage'+ex.getMessage()+'Line No:'+ex.getLineNumber());
            ErrorLogUtility.logException(ex, null, 'VT_R5_CreateQueueMemeberToSSTM');
        }   
        
        
    }
    
    
    public void finish(Database.BatchableContext BC){
        List<String>lstClassName =new List<String>{'VT_R5_CreateQueueMemeberToSSTM-Group','VT_R5_CreateSSTMToVirtualSite',
            'VT_R5_CreateSSTMToVirtualSite-SSTM','VT_R5_CreateSSTMToVirtualSite-QueueNameUpdate'};
            List<VTR4_Conversion_Log__c> errorLogs =[SELECT ID,
                                                     Name,
                                                     createdDate,
                                                     VTR5_ClassName__c,
                                                     VTR5_ExceptionType__c,
                                                     VTR4_Error_Message__c
                                                     FROM VTR4_Conversion_Log__c
                                                     WHERE VTR5_ClassName__c IN:lstClassName AND createdDate>=:System.today()];
        if(errorLogs.size() > 0 && errorLogs != Null){
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] {UserInfo.getUserEmail() };
                message.subject = 'Batch Status !!!';
            for(VTR4_Conversion_Log__c objError :errorLogs){
                message.plainTextBody += ''+objError.VTR5_ClassName__c+':'+objError.VTR4_Error_Message__c;
            }
            
            Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        }
        
    }
}