/**
 * Created by Yuliya Yakushenkova on 10/5/2020.
 */

public class VT_R5_MobilePatientVisits extends VT_R5_MobileRestResponse implements VT_R5_MobileLibrary.Routable {

    private static RequestParams requestParams;

    private static final String TIME_SLOTS = 'timeslots';
    private static final String RESCHEDULE = 'reschedule';
    private static final String SCHEDULE = 'schedule';
    private static final String CANCEL = 'cancel';

    public List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/visit/{id}',
                '/patient/{version}/visit/{id}/{action}'
        };
    }

    public VT_R5_MobileRestResponse versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        String requestId = parameters.get('id');
        String requestAction = parameters.get('action');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        if (requestAction == null && requestId != null) {
                            return get_v1(requestId);
                        }
                        if (TIME_SLOTS.equals(requestAction)) {
                            return get_v1_timeSlots(requestId);
                        }
                        if (SCHEDULE.equals(requestAction)) {
                            return get_v1_timeTable(requestId);
                        }
                    }
                }
            }
            when 'POST' {
                switch on requestVersion {
                    when 'v1' {
                        if (RESCHEDULE.equals(requestAction)) {
                            return post_v1_reschedule(requestId);
                        }
                        if (CANCEL.equals(requestAction)) {
                            return post_v1_cancel(requestId);
                        }
                        if (SCHEDULE.equals(requestAction)) {
                            return post_v1_schedule(requestId);
                        }
                    }
                }
            }
        }
        return null;
    }

    public void initService() {
        requestParams = new RequestParams();
        parseBody();
        parseParams();
    }

    public Map<String, String> getMinimumVersions() {
        return new Map<String, String>{
                'v1' => '5.5'
        };
    }

    private VT_R5_MobileRestResponse get_v1(String visitId) {
        VT_R5_PatientCalendarService.CalendarEvent calendarEvent =
                VT_R5_PatientCalendarService.getInstance().getVisitByVisitId(visitId);
        if (!Test.isRunningTest()) new VT_R3_RestHelper().addPreviewsWithBatchCallout(calendarEvent.documents); //for R4

        return this.buildResponse(calendarEvent);
    }

    private VT_R5_MobileRestResponse get_v1_timeSlots(String visitId) {
        VT_D1_ActualVisitsHelper helper = new VT_D1_ActualVisitsHelper();
        return this.buildResponse(
                new VT_R5_VisitTimeSlotsService(
                        helper.getDatesTimesMap(visitId, null),
                        (Map<String, Object>) JSON.deserializeUntyped(helper.getAvailableDateTimes(visitId)))
                        .getDateTimeSlots()
        );
    }

    private VT_R5_MobileRestResponse get_v1_timeTable(String visitId) {
        return this.buildResponse(
                new VT_D1_ActualVisitsHelper().getDateTimesByMonthWeekHelper(visitId)
        );
    }

    private VT_R5_MobileRestResponse post_v1_reschedule(String visitId) {
        try {
            String fromDateTimeValue = getDateTimeString(requestParams.fromDateTime);
            String toDateTimeValue = getDateTimeString(requestParams.toDateTime);
            VT_D1_PatientCalendar.updateActualVisit(visitId, fromDateTimeValue, toDateTimeValue, requestParams.reason);
            return this.buildResponse(System.Label.VTR2_SchedulingVisitSuccess);
        } catch (Exception e) {
            e.setMessage(System.Label.VT_D1_ErrorRequestReschedule);
            return this.buildResponse(e);
        }
    }

    private VT_R5_MobileRestResponse post_v1_schedule(String visitId) {
        try {
            VT_D1_ChooseTimeOnTaskContollerRemote.createTimeSlots(visitId, requestParams.slots);
            return this.buildResponse(
                    System.Label.VTR3_Request_Time_Slots.replace('#1', getVisitNameByVisitId(visitId))
            );
        } catch (Exception e) {
            e.setMessage(System.Label.VT_D1_FailedToSentRequestToSchedule);
            return this.buildResponse(e);
        }
    }

    private VT_R5_MobileRestResponse post_v1_cancel(String visitId) {
        try {
            VT_D1_PatientCalendar.cancelVisitRemote(visitId, requestParams.reason);
            return this.buildResponse(
                    System.Label.VTR3_Submit_Request_To_Cancel.replace('#1', getVisitNameByVisitId(visitId))
            );
        } catch (Exception e) {
            e.setMessage(System.Label.VT_D1_ErrorCancellingVisit);
            return this.buildResponse(e);
        }
    }

    private String getVisitNameByVisitId(String visitId) {
        return [
                SELECT VTD1_Formula_Name__c
                FROM VTD1_Actual_Visit__c
                WHERE Id = :visitId
        ].VTD1_Formula_Name__c;
    }

    private String getDateTimeString(Datetime value) {
        Integer offset = -UserInfo.getTimeZone().getOffset(System.now()) / 1000;
        return value.addSeconds(offset).format();
    }

    private void parseBody() {
        if (request.requestBody != null && request.requestBody.size() > 0) {
            requestParams = (RequestParams) JSON.deserialize(request.requestBody.toString(), RequestParams.class);
        }
    }

    private void parseParams() {}

    private class RequestParams {
        private Datetime fromDateTime;
        private Datetime toDateTime;
        private String reason;
        private String slots;
    }
}