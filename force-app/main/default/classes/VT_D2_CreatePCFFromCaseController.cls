/**
* @author: Carl Judge
* @date: 21-Nov-18
* @description: Controller for VT_D2_CreatePCFFromCase component
**/

public without sharing class VT_D2_CreatePCFFromCaseController {

    public class ContactIdsResponse {
        public Id patientId;
        public Id caregiverId;
    }

    @AuraEnabled (cacheable = true)
    public static String getPcfRecTypeId() {
        return VTD1_RTId__c.getInstance().VTD1_Case_Patient_Contact_Form__c;
    }

    @AuraEnabled (cacheable = true)
    public static String getCaseId(String recId) {
        if (((Id)recId).getSobjectType() == VTD1_Patient__c.getSObjectType()) {
            List<Case> casList = [SELECT Id FROM Case WHERE VTD1_Patient__c =: recId AND RecordType.Name = 'CarePlan'];
            if (!casList.isEmpty()) {
                recId = casList[0].Id;
            }
        }
        return recId;
    }

    @AuraEnabled (cacheable = true)
    public static String getContactIds(Id caseId){
        ContactIdsResponse response = new ContactIdsResponse();
        List<Case> cases = [SELECT VTD1_Patient_User__r.ContactId, VTD1_Patient_User__r.Contact.AccountId FROM Case WHERE Id = :caseId];
        if (!cases.isEmpty()) {
            response.patientId = cases[0].VTD1_Patient_User__r.ContactId;
            if (response.patientId != null && cases[0].VTD1_Patient_User__r.Contact.AccountId != null) {
                for (Contact caregiver : [
                        SELECT Id
                        FROM Contact
                        WHERE AccountId = :cases[0].VTD1_Patient_User__r.Contact.AccountId
                        AND RecordType.DeveloperName = 'Caregiver'
                        ORDER BY VTD1_Primary_CG__c DESC
                        LIMIT 1
                ]) {
                    response.caregiverId = caregiver.Id;
                }
            }
        }
        return JSON.serialize(response);
    }

    @AuraEnabled
    public static String insertNewCaseRecord(Case newCase){
        if(newCase == null){
            return null;
        }
        newCase.RecordTypeId = getPcfRecTypeId();
        List<Case> listOfCase = new List<Case>();
        try {
            listOfCase.add(newCase);
            upsert listOfCase;
            
            return newCase.Id;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

}