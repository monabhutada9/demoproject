/**
 * Created by user on 08-Oct-19.
 */
public with sharing class VT_R3_AddStudyDocumentsController {
    @AuraEnabled
    public static VTD1_Actual_Visit__c getActualVisitById(Id actualVisitId) {
        if (actualVisitId != null) {
            return VT_R3_ActualVisitSelector.getActualVisitById(actualVisitId);
        }
        return null;
    }

    @AuraEnabled
    public static String getProfileNameOfCurrentUser() {
        return VT_D1_HelperClass.getProfileNameOfCurrentUser();
    }

    @AuraEnabled
    public static List<VTD1_Actual_Visit__c> getVisits(String caseId) {
        return getActualVisitsByCaseId(caseId);
    }

    private static List<VTD1_Actual_Visit__c> getActualVisitsByCaseId(String caseId) {
        List<Id> actualVisitList = new List<Id>();
        User user = getUserById(UserInfo.getUserId());
        for (Visit_Member__c visitMember : getVisitMemberByParticipantUser(user.Contact.VTD1_Clinical_Study_Membership__c)) {
            actualVisitList.add(visitMember.VTD1_Actual_Visit__c);
        }
        List<VTD1_Actual_Visit__c> actualVisits = [
                SELECT Id, VTD1_Status__c, toLabel(VTD1_Status__c) translatedStatus, VTD1_Date_for_Timeline__c, VTD1_Visit_Type__c,VTD1_Protocol_Visit__c, VTD1_Protocol_Visit__r.VTD1_EDC_Name__c, VTD1_Unscheduled_Visit_Type__c, Name, VTD1_Protocol_Visit__r.VTD1_VisitNumber__c, To_Be_Scheduled_Date__c, VTD1_Visit_Description__c, VTD1_Scheduled_Date_Time__c, VTR2_Modality__c, toLabel(VTR2_Modality__c) translatedModality, VTD2_Schedule_Request_Submitted__c
                FROM VTD1_Actual_Visit__c
                WHERE Id IN :actualVisitList
                ORDER BY VTD1_Protocol_Visit__r.VTD1_VisitNumber__c
        ];
        VT_D1_TranslateHelper.translate(actualVisits);
        Map<Id, VTD1_ProtocolVisit__c> protocolVisitMap = new Map<Id, VTD1_ProtocolVisit__c>();
        for (VTD1_Actual_Visit__c av : actualVisits) {
            if (av.VTD1_Protocol_Visit__c != null) {
                protocolVisitMap.put(av.VTD1_Protocol_Visit__c, new VTD1_ProtocolVisit__c(Id = av.VTD1_Protocol_Visit__c, VTD1_EDC_Name__c = av.VTD1_Protocol_Visit__r.VTD1_EDC_Name__c));
            }
        }
        for (VTD1_Actual_Visit__c av : actualVisits) {
            av.Name = getActualVisitName(av, protocolVisitMap);
        }
        return actualVisits;
    }

    @AuraEnabled
    public static void saveStudyDocument(String caseId, String activeVisitId, String documentId, String nickName, String comments, String category) {
        updateDocument(caseId, activeVisitId, documentId, nickName, comments, category);
    }

    @AuraEnabled
    public static void saveStudyDocument(String caseId, String activeVisitId, String documentId, String nickName, String comments) {
        updateDocument(caseId, activeVisitId, documentId, nickName, comments, null);
    }

    public static void updateDocument(String caseId, String activeVisitId, String documentId, String nickName, String comments, String category) {
        VTD1_Document__c document = VT_R3_DocumentSelector.getDocumentById(documentId);
        String userProfile = VT_D1_HelperClass.getProfileNameOfCurrentUser();
        Id userId = UserInfo.getUserId();
        List<String> profileList = new List<String>{
                'Patient', 'Caregiver', 'Primary Investigator', 'Site Coordinator', 'Study Concierge', 'Patient Guide', 'System Administrator'
        };
        if (profileList.contains(userProfile)) {
            if (document != null) {
                document.VTR3_Upload_File_Completed__c = true;
                document.VTR3_CreatedProfile__c = new List<String>{
                        'Patient', 'Caregiver'
                }.contains(userProfile);
                document.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT;
                if (category != null) {
                    document.VTR3_Category__c = category;
                } else {
                    document.VTR3_Category__c = 'Uncategorized';
                }
                document.VTD1_Nickname__c = nickName;
                document.VTD1_Comment__c = comments;
                if (String.isNotEmpty(activeVisitId) && activeVisitId != 'None') {
                    document.VTR3_Associated_Visit__c = activeVisitId;
                }
                document.VTD1_Flagged_for_Deletion__c = false;
                try {
                    update document;
                } catch (Exception e) {
                    System.debug('cannot update updateDocument - ' + e.getMessage());
                }
                if (document.VTR3_CreatedProfile__c ||
                        (new List<String>{
                                'Primary Investigator', 'Site Coordinator', 'Study Concierge', 'Patient Guide'
                        }
                                .contains(userProfile) && new List<String>{
                                'Visit Document', 'Source Document', 'Lab Requisition Form'
                        }
                                .contains(document.VTR3_Category__c))) {
                    VT_D2_MedicalRecordsCertifyDocsHelper.certifyDoc(caseId, document.Id);
                }
            }
        }
    }

    //Uncomment for SH-21006
    @AuraEnabled
    public static VTD1_Document__c getRecordByFile(Id fileId) {
        List<ContentDocumentLink> contentDocLinkList = [
                SELECT LinkedEntity.Id, LinkedEntity.Type
                FROM ContentDocumentLink
                WHERE ContentDocumentId = :fileId
        ];
        Id contentDocId;
        for (ContentDocumentLink item : contentDocLinkList) {
            if (item.LinkedEntity.Type == 'VTD1_Document__c') {
                contentDocId = item.LinkedEntity.Id;
                break;
            }
        }
        VTD1_Document__c document = VT_R3_DocumentSelector.getDocumentById(contentDocId);
        if (document != null) {
            VT_D1_TranslateHelper.translate(document);
            return document;
        } else {
            return null;
        }
    }
    //Uncomment for SH-21006

    @AuraEnabled
    public static VTD1_Document__c getRecordByFileAndCase(Id fileId, Id caseId) {
        Case aCase = [SELECT Id, VTD1_Primary_PG__c, VTD1_PI_user__c FROM Case WHERE Id = :caseId];

        ContentDocumentLink documentLink = [
                SELECT Id, ContentDocumentId, ShareType, Visibility, ContentDocument.Title, ContentDocument.FileExtension
                FROM ContentDocumentLink
                WHERE ContentDocumentId = :fileId AND LinkedEntityId = :caseId
                LIMIT 1
        ];

        VTD1_Document__c document = new VTD1_Document__c();
        document.VTD1_Clinical_Study_Membership__c = aCase.Id;
        document.VTD1_PG_Approver__c = aCase.VTD1_Primary_PG__c;
        document.VTD1_PI_Approver__c = aCase.VTD1_PI_user__c;
        document.VTD1_Uploading_Not_Finished__c = false;
        document.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_VISIT_DOCUMENT;
        document.VTD1_Flagged_for_Deletion__c = true;
        insert document;

        ContentDocument ct = documentLink.ContentDocument;
        document.VTD1_FileNames__c = ct.Title + '.' + ct.FileExtension;

        ContentDocumentLink cdlCopy = documentLink.clone(false, true, false, false);
        cdlCopy.LinkedEntityId = document.Id;
        insert cdlCopy;

        delete documentLink;

        return document;
    }

    @AuraEnabled
    public static void remoteDocument(String documentId) {
        try {
            VTD1_Document__c document = VT_R3_DocumentSelector.getDocumentById(documentId);
            if (document != null) {
                delete document;
            }
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage() + '\n' + ex.getStackTraceString());
        }
    }

    private static User getUserById(String userId) {
        List<User> userList = [SELECT Id, Contact.AccountId, Contact.VTD1_Clinical_Study_Membership__c, Profile.Name FROM User WHERE Id = :userId];
        if (userList.size() != 0) {
            return userList[0];
        } else {
            return null;
        }
    }

    private static List<Visit_Member__c> getVisitMemberByParticipantUser(String currentCaseId) {
        // SH-15323 Regression: When click on Category on Visit Upload "ERROR Too many query rows: 50001" is displaying
        if (currentCaseId == null || String.isEmpty(currentCaseId)) {
            return new List<Visit_Member__c>();
        }

        return [
                SELECT Id, VTD1_Actual_Visit__c, VTD1_Participant_User__c
                FROM Visit_Member__c
                WHERE VTD1_Actual_Visit__r.VTD1_Case__c = :currentCaseId OR VTD1_Actual_Visit__r.Unscheduled_Visits__c = :currentCaseId
        ];
    }

    public static String getActualVisitName(VTD1_Actual_Visit__c visit, Map<Id, VTD1_ProtocolVisit__c> protocolVisitMap) {
        String visitName;
        system.debug('--deb-- ' + visit);
        system.debug('--deb-- ' + visit.VTD1_Unscheduled_Visit_Type__c);
        if (visit.VTD1_Protocol_Visit__r.VTD1_EDC_Name__c != null) {
            visitName = protocolVisitMap.get(visit.VTD1_Protocol_Visit__c).VTD1_EDC_Name__c;
        } else {
            switch on visit.VTD1_Unscheduled_Visit_Type__c {
                when 'Screen Failure' {
                    visitName = Label.VTD2_AdHocVisit;
                }
                when 'Re Screen Visit' {
                    visitName = Label.VTD2_ReScreenVisit;
                }
                when else {
                    visitName = visit.Name;
                }
            }
        }
        return visitName;
    }
}