/**
* @author: Carl Judge
* @date: 01-Aug-20
**/
@IsTest
public with sharing class VT_R5_FlagMissedDiariesBatchTest {
    public static void testBatch() {
        Case patientCase = (Case) new DomainObjects.Case_t()
            .setRecordTypeByName('VTD1_PCF')
            .addAccount(new DomainObjects.Account_t())
            .persist();

        VTD1_Survey__c survey = (VTD1_Survey__c)
            new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(patientCase.Id)
                .setRecordTypeByName('VTR5_External')
                .setVTD1_Status(VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_DUE_SOON)
                .setVTD1_Due_Date(Datetime.now().addMinutes(-1))
                .persist();

        Test.startTest();
        Database.executeBatch(new VT_R5_FlagMissedDiariesBatch(), VT_R5_FlagMissedDiariesBatch.SCOPE_SIZE);
        Test.stopTest();
        System.assertEquals(true, ([
            SELECT Id FROM VTD1_Survey__c
            WHERE Id = :survey.Id
            AND VTD1_ePRO_Missed__c = true
            AND VTD1_Status__c = :VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_MISSED
        ].size() > 0));
    }

    public static void testRun() {
        Test.startTest();
        VT_R5_FlagMissedDiariesBatch.run();
        Test.stopTest();
        System.assertEquals(true, ([SELECT Id FROM CronTrigger WHERE Id = :VT_R5_FlagMissedDiariesBatch.cronId].size() > 0));
    }
}