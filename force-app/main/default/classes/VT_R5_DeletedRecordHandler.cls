/**
 * Created by Aleksandr Mazan on 16-Nov-20.
 */

public with sharing class VT_R5_DeletedRecordHandler {

    public static final String SURVEY_C = 'VTD1_Survey__c';
    public static final String DOCUMENT_C = 'VTD1_Document__c';
    public static final String SURVEY_ANSWER_C = 'VTD1_Survey_Answer__c';
    public static final String SURVEY_ANSWER_r = 'Survey_Answers__r';

    public static void createDeletedRecords(List<SObject> recs) {
        List<Id> ids = new List<Id>();
        Integer count = 0;
        for (; count < recs.size(); count++) {
            ids.add(recs[count].Id);
        }
        SObjectType objectType = recs[0].getSObjectType();
        Map<String,Schema.SObjectField> fieldMap = objectType.getDescribe().fields.getMap();
        List<String> fieldNames = new List<String>(fieldMap.keySet());
        String query = 'SELECT ' + string.join(fieldNames, ',');
        if (String.valueOf(objectType) == SURVEY_C) {
            Set<String> answerFieldNames = Schema.getGlobalDescribe().get(SURVEY_ANSWER_C).getDescribe().fields.getMap().keySet();
            if (!answerFieldNames.isEmpty()) {
                query += ',(SELECT ' +  string.join(new List<String>(answerFieldNames), ',') + ' FROM ' + SURVEY_ANSWER_r + ') ';
            }
        }
        query += ' FROM ' + objectType + ' WHERE Id IN :ids';
        System.debug(query);
        List<SObject> dataRecords = Database.query(query);
        if (!dataRecords.isEmpty()) {
            List<VTR5_DeletedRecord__c> deletedRecords = new List<VTR5_DeletedRecord__c>();
            VTR5_DeletedRecord__c deletedRecord;
            for (SObject record : dataRecords) {
                deletedRecord = new VTR5_DeletedRecord__c(
                        VTR5_JSONofFields__c = JSON.serialize(record),
                        VTR5_ObjectType__c = String.valueOf(objectType),
                        VTR5_DeletedTimestamp__c = Datetime.now()
                );
                if (String.valueOf(objectType) == SURVEY_C) {
                    VTD1_Survey__c survey = (VTD1_Survey__c)record;
                    deletedRecord.Name = survey.VTR5_eDiaryName__c;
                    deletedRecord.VTR5_ReasonForDeletion__c = survey.VTR5_ReasonForDeletion__c != null ? survey.VTR5_ReasonForDeletion__c.left(1000) : '';
                }
                if (String.valueOf(objectType) == DOCUMENT_C) {
                    VTD1_Document__c document = (VTD1_Document__c)record;
                    deletedRecord.Name = document.VTD1_Document_Name__c;
                    deletedRecord.VTR5_ReasonForDeletion__c = document.VTR4_DeletionReason__c != null ? document.VTR4_DeletionReason__c.left(1000) : '';
                }
                deletedRecords.add(deletedRecord);
            }
            insert deletedRecords;
        }
    }
}