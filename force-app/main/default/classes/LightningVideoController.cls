public with sharing class LightningVideoController {

    @AuraEnabled
    public static Map<String, String> getPostToPusher(String recordId) {
        HttpRequest req = new HttpRequest();
        Video_Conference__c videoConference = [
                SELECT SessionId__c
                FROM Video_Conference__c
                WHERE Id = :recordId
        ];
        req.setEndpoint('https://90br25mxxb.execute-api.us-east-2.amazonaws.com/beta');//('https://pwte8ptkc3.execute-api.eu-west-1.amazonaws.com/production/token');
        req.setMethod('POST');
        req.setBody('{"sessionId" :"' + videoConference.SessionId__c + '"}');
        Http http = new Http();
        HttpResponse res = http.send(req);
        String resString = res.getBody();
        System.debug('Res json ' + resString);
        Map<String, Object> parsedResponseBody = (Map<String, Object>) JSON.deserializeUntyped(resString);
        Map<String, String> out = new Map<String, String>();
        out.put('apiKey', '46064112');//46050722
        out.put('sessionId', videoConference.SessionId__c);
        out.put('token', (String) parsedResponseBody.get('token'));
        return out;
    }
}