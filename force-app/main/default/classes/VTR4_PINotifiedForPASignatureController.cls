/**
 * Created by user on 27-Feb-20.
 */

public with sharing class VTR4_PINotifiedForPASignatureController {
	public Id documentId {get; set;}

	public String getPiLastName(){
		return getDocument().VTD1_Site__r.VTD1_PI_User__r.LastName;
	}

	public String getProtocolNickName(){
		return getDocument().VTD2_Email_Protocol_Nickname__c;
	}

	private VTD1_Document__c getDocument (){
		return [
			SELECT Id, VTD1_Site__r.VTD1_PI_User__r.LastName, VTD2_Email_Protocol_Nickname__c
			FROM VTD1_Document__c
			WHERE Id = :documentId
		];
	}
}