/**
* @author: Olya Baranova
* @date: 27-Aug-20
* @description:
**/

@IsTest
private class VT_R5_eCoaBulkNotificationTest {

    @TestSetup
    private static void setupMethod() {
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));

        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t();
        patientAccount.persist();
        //patient
        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
                .setAccountId(patientAccount.id);
        DomainObjects.User_t userPatient = new DomainObjects.User_t()
                .addContact(patientContact)
                .setEcoaGuid('subject_eee29443-8464-42f9-a612-4135caf0ca36')
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        userPatient.persist();
        //caregiver
        DomainObjects.Contact_t caregiverContact = new DomainObjects.Contact_t()
                .setVTD1_RelationshiptoPatient('Parent')
                .setAccountId(patientAccount.id)
                .setEmail(DomainObjects.RANDOM.getEmail())
                .setFirstName('Rick')
                .setLastName('Sanchez')
                .setRecordTypeName('Caregiver');
        DomainObjects.User_t caregiverUser = new DomainObjects.User_t()
                .addContact(caregiverContact)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME);
        caregiverUser.persist();
        //case
        DomainObjects.Case_t casePatient = new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .addStudy(study)
                .addUser(userPatient)
                .addContact(patientContact)
                .addAccount(patientAccount);
        casePatient.persist();
        patientContact.addVTD1_Clinical_Study_Membership(casePatient);

    }

    @IsTest
    static void doPostTest() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestBody = Blob.valueOf('[{"subGuid":"subject_eee29443-8464-42f9-a612-4135caf0ca36","displayRuleValue":"Test Heroku","displayAlarmValue":"Your diary is available now. Pass it.","ruleGuid":"responseRule_e8eaa71f-9a5e-47fd-8fb4-bb96d6b28b10"}]');


        req.requestURI = '/services/apexrest/ecoa-notification';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        VT_R5_eCoaBulkNotification.doPost();

        Test.stopTest();
        System.assertEquals(200, res.statusCode, 'Status code should be 200');
    }
    @IsTest
    static void doPostTestBad() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestBody = Blob.valueOf('[{"subGuid":"subect_eee29443-8464-42f9-a612-4135caf0ca36","displayRuleValue":"Test Heroku","displayAlarmValue":"Your diary is available now. Pass it.","ruleGuid":"responseRule_e8eaa71f-9a5e-47fd-8fb4-bb96d6b28b10"}]');


        req.requestURI = '/services/apexrest/ecoa-notification';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        VT_R5_eCoaBulkNotification.doPost();

        Test.stopTest();
        System.assertEquals(200, res.statusCode, 'Status code should be 200');
    }
}