@IsTest
private class VT_D2_IUPatientSummaryControllerTest {
    final static String TEST_PATIENT_ID = 'Test Patient ID';
    final static String TEST_CHANGE_REASON = 'Some Reason';

    @testSetup
    static void setup() {
        Test.startTest();
        {
            VT_R3_GlobalSharing.disableForTest = true;
            VT_D1_TestUtils.prepareStudy(1);
            VT_D1_TestUtils.createTestPatientCandidate(1);
        }
        Test.stopTest();

        Case ptCase = [SELECT Id, ContactId, AccountId FROM Case][0];

        Address__c address = new Address__c();
        address.City__c = 'Australia';
        address.State__c  = 'Victoria';
        address.VTD1_Contact__c = ptCase.ContactId;
        insert address;

        VT_D1_Phone__c phone = new VT_D1_Phone__c();
        phone.PhoneNumber__c = '4444';
        phone.VTD1_Contact__c = ptCase.ContactId;
        phone.Account__c = ptCase.AccountId;
        insert phone;
    }

    @IsTest
    static void getPatientSummaryTest() {
        Test.startTest();
        List<Case> cases = [SELECT Id, VTD1_Study__c FROM Case];
        System.assert(cases.size() > 0);
        Case ptCase = cases.get(0);

        update new HealthCloudGA__CarePlanTemplate__c(
            Id = ptCase.VTD1_Study__c,
            VTR5_Requires_External_Subject_Id__c = true,
            VTR5_External_Subject_ID_Input_Type__c = 'Manual'
        );

        String summaryJson = VT_D2_IUPatientSummaryController.getPatientSummary(ptCase.Id);
        VT_D2_IUPatientSummaryController.PatientSummary summary = (VT_D2_IUPatientSummaryController.PatientSummary)
            JSON.deserialize(summaryJson, VT_D2_IUPatientSummaryController.PatientSummary.class);

        System.assertNotEquals(null, summary.cas);
        System.assertNotEquals(null, summary.phone);
        System.assertNotEquals(null, summary.address);
        System.assert(summary.patientStatusInputSelectRatioElementList.size() > 0);
        System.assert(summary.patientReasonCodeInputSelectRatioElementList.size() > 0);
        System.assertEquals(true, summary.canEditPatientId);
        System.assertEquals(true, summary.isRequiredIdChangeReasonField);

        String caseStatus = 'Open';
        VT_D2_IUPatientSummaryController.updateCaseStatus(ptCase.Id, caseStatus, 'Non Compliance','testReasonForOther');
        cases = [SELECT Id FROM Case WHERE Id =: ptCase.Id AND Status =: caseStatus];
        System.assertEquals(1, cases.size());

        VT_D2_IUPatientSummaryController.getLogReportId();
        VT_D2_IUPatientSummaryController.getLink(ptCase.Id);
        VT_D2_IUPatientSummaryController.randomizeRemote(ptCase.Id);
        Test.stopTest();
    }

    @IsTest
    static void updatePatientIdTest() {
        List<Case> cases = getCases('VTD1_Subject_ID__c, VTR5_Reason_for_Patient_ID_Change__c');
        System.assert(cases.size() > 0);

        Case ptCase = cases[0];
        System.assertNotEquals(TEST_PATIENT_ID, ptCase.VTD1_Subject_ID__c);
        System.assertNotEquals(TEST_CHANGE_REASON, ptCase.VTR5_Reason_for_Patient_ID_Change__c);

        Test.startTest();
        {
            VT_D2_IUPatientSummaryController.updatePatientId(ptCase.Id, TEST_PATIENT_ID, TEST_CHANGE_REASON);
        }
        Test.stopTest();

        ptCase = getCases('VTD1_Subject_ID__c, VTR5_Reason_for_Patient_ID_Change__c')[0];
        System.assertEquals(TEST_PATIENT_ID, ptCase.VTD1_Subject_ID__c);
        System.assertEquals(TEST_CHANGE_REASON, ptCase.VTR5_Reason_for_Patient_ID_Change__c);
    }

    static List<Case> getCases(String fields) {
        return Database.query('SELECT Id, ' + fields + ' FROM Case');
    }

    @IsTest
    static void getCurrentUserProfileTest() {
        System.assertEquals('System Administrator', VT_D2_IUPatientSummaryController.getCurrentUserProfile());
    }

    /************************
    * @description Test method for updateCaseEligibilityStatus on VT_D2_IUPatientSummaryController
    * Modified by Mona Bhutada
    */
    @IsTest
    static void piMyPatientSummaryEligibilityStatusTest() {
        List<Case> cases = [SELECT Id, VTD1_Study__c FROM Case];
        System.assert(cases.size() > 0);
        Case ptCase = cases.get(0);

        update new HealthCloudGA__CarePlanTemplate__c(
            Id = ptCase.VTD1_Study__c,
            VTR2_EligibilityOutsideSH__c = true,
            VTR5_Baseline_Visit_After_A_R_Flag__c = true
        );

        Test.startTest();
        {
            String eligibilityStatus = 'Eligible';
            VT_D2_IUPatientSummaryController.updateCaseEligibilityStatus(ptCase.Id, eligibilityStatus);
            Case caseEligibilityStatusUpdated = [
                SELECT Id, Status
                FROM Case
                WHERE Id = :ptCase.Id AND VTD1_Eligibility_Status__c = :eligibilityStatus
            ];
            System.assertNotEquals(null, caseEligibilityStatusUpdated);
        }
        Test.stopTest();
    }
}