/**
 * Created by Alexander Komarov on 29.04.2019.
 */

@RestResource(UrlMapping='/Patient/Broadcasts')
global with sharing class VT_R3_RestPatientBroadcast {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();
    static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());

    @HttpGet
    global static String getBroadcasts() {
        RestResponse response = RestContext.response;

        List<VT_D1_CommunityChat.PostObj> broadcastsListResult = new List<VT_D1_CommunityChat.PostObj>();

        List<PatientBroadcast> result = new List<PatientBroadcast>();
        // As Broadcast can be responded back by Patient, hence to make a immediate fix we are sending blank array. 
        
       /* try {
            broadcastsListResult = VT_D1_CommunityChat.getPatientBroadcasts();
            if (!broadcastsListResult.isEmpty()) {
                result = getLastCommentsAndFormResult(broadcastsListResult);
            }
        } catch (Exception e) {
            response.statusCode = 500;
            cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName());
            return JSON.serialize(cr, true);
        }*/
        BroadcastsResponse broadcastsResponse = new BroadcastsResponse();
        broadcastsResponse.chatsList = result;

        response.statusCode = 200;
//        cr.buildResponse(result);
        cr.buildResponse(broadcastsResponse);
        return JSON.serialize(cr, true);

    }

    /*
    private static List<PatientBroadcast> getLastCommentsAndFormResult(List<VT_D1_CommunityChat.PostObj> posts) {
        List<PatientBroadcast> result = new List<PatientBroadcast>();
        for (VT_D1_CommunityChat.PostObj post : posts) {
            Boolean haveComments = false;
            ConnectApi.Comment lastComment;
            ConnectApi.User commentAuthor;
            if(!Test.isRunningTest()) {
                ConnectApi.CommentPage commentsPage = ConnectApi.ChatterFeeds.getCommentsForFeedElement(null, post.id, null, 1);
                List<Object> commentsItems = commentsPage.items;
                if (commentsItems.size() > 0) {
                    lastComment = commentsPage.items[0];
                    commentAuthor = (ConnectApi.User) lastComment.user;
                    haveComments = true;
                }
            }
            result.add(new PatientBroadcast(post.id, post.subject, haveComments ? commentAuthor.displayName : post.userName, haveComments ? lastComment.createdDate : post.createdDateTime, haveComments ? commentAuthor.photo.largePhotoUrl : post.userImage, post.isRead, haveComments ? lastComment.body.text : post.bodyText));
        }
        System.debug(result);
        return result;
    }
*/
	@TestVisible
    private class PatientBroadcast {
        public String id;
        public String title;
        public String lastMessageAuthorName;
        public Long lastMessageData;
        public String photo;
        public Boolean read;
        public String textLastMessage;

        public PatientBroadcast(String i, String t, String n, Datetime d, String p, Boolean r, String text) {
            this.id = i;
            this.title = t;
            this.lastMessageAuthorName = n;
            this.lastMessageData = d.getTime();// + timeOffset;
            this.photo = p.contains('default_profile') ? '' : p;
            this.read = r;
            this.textLastMessage = text;
        }
    }
	@TestVisible
    private class BroadcastsResponse {
        private List<PatientBroadcast> chatsList; //broadcasts //liveChatHistories
        private VT_R3_RestHelper.Unread unreadInfo = new VT_R3_RestHelper.Unread();
    }


}