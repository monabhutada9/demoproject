public without sharing class VT_D1_PIMyPatientPcfDetail { 
    @AuraEnabled
    public static Map<String, SObject> getPcfData(Id caseId, Id pcfId) {
        Map<String, SObject> dataMap = new Map<String, SObject>();
        Case carePlan = [
                SELECT Id,Contact.Name,VTD1_Study__r.Name
                FROM Case
                WHERE Id = :caseId
        ];
        Case pcf = [
                SELECT Id, CaseNumber, Owner.Name, OwnerId, Status, VTR5_Stop_eDiary_Schedule__c,
                        VTD1_PCF_Safety_Concern_Indicator__c, VTD2_Safety_Concern_Reviewed_by_PI__c,
                        CreatedDate, VTD1_PCF_Visit_ID__r.Name, IsEscalated, VTD1_PCF_Call_Narrative__c,
                        VTD1_PCF_Resolution_Deadline__c, VTD1_PCF_Time_Remaining_to_Resolution__c,
                        VTD2_PCF_Safety_Concern_Indication_Date__c, VTD2_PCF_Initial_Contact_Notes__c,
                        VTD2_Patient_or_Caregiver__c,VTR5_eDiary_Schedule_Notes__c,VTR5_Stop_eDiary_Date__c
                FROM Case
                WHERE Id = :pcfId
        ];

        List<Case>pcfWithStoppedDiary = [
                SELECT Id,VTR5_Stop_eDiary_Schedule__c
                FROM Case
                WHERE VTD1_Clinical_Study_Membership__c = :caseId AND VTR5_Stop_eDiary_Schedule__c = TRUE
                LIMIT 1
        ];
        dataMap.put('case', carePlan);
        dataMap.put('pcf', pcf);
        if (!pcfWithStoppedDiary.isEmpty())dataMap.put('pcfWithStoppedDiary', pcfWithStoppedDiary[0]);
        return dataMap;
    }
    public class OptionObj {
        @AuraEnabled
        public String value { get; set; }
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String profileName { get; set; }
        public OptionObj(String value, String label, String profileName) {
            this.value = value;
            this.label = label;
            this.profileName = profileName;
        }
    }
    @AuraEnabled(Cacheable=true)
    public static Map<String, String> getVisitPicklistOptions() {
        return Ora.getPicklistOptions(null, 'VTD1_PCF_Safety_Concern_Indicator__c', 'Case');
    }
    @AuraEnabled(Cacheable=true)
    public static List<OptionObj> getOwnerOptions(Id caseId) {
        List<OptionObj> options = new List<OptionObj>();
        List<Case> cases = [
                SELECT VTD1_Primary_PG__c,
                        VTD1_Secondary_PG__c,
                        VTD1_PI_user__c,
                        VTD1_Backup_PI_User__c,
                        VTD1_Study__c, VTD1_Virtual_Site__c
                FROM Case
                WHERE Id = :caseId
        ];
        if (cases.isEmpty()) {
            return options;
        }
        Case c = cases[0];
        Set<Id> userIds = new Set<Id>();
        if (c.VTD1_Primary_PG__c != null) {
            userIds.add(c.VTD1_Primary_PG__c);
        }
        if (c.VTD1_Secondary_PG__c != null) {
            userIds.add(c.VTD1_Secondary_PG__c);
        }
        if (c.VTD1_PI_user__c != null) {
            userIds.add(c.VTD1_PI_user__c);
        }
        if (c.VTD1_Backup_PI_User__c != null) {
            userIds.add(c.VTD1_Backup_PI_User__c);
        }
        List<Study_Team_Member__c> membersSC = [
                SELECT User__c
                FROM Study_Team_Member__c
                WHERE Study__c = :c.VTD1_Study__c AND VTD1_Type__c = 'Study Concierge'
        ];
        for (Study_Team_Member__c m : membersSC) {
            userIds.add(m.User__c);
        }
        List<Study_Team_Member__c> membersSCR = [
                SELECT User__c
                FROM Study_Team_Member__c
                WHERE Id IN (
                        SELECT VTR2_Associated_SCr__c
                        FROM Study_Site_Team_Member__c
                        WHERE VTR2_Associated_PI__r.VTD1_VirtualSite__c = :c.VTD1_Virtual_Site__c
                )
        ];
        for (Study_Team_Member__c m : membersSCR) {
            userIds.add(m.User__c);
        }
        for (User u : [SELECT Id, Name, Profile.Name FROM User WHERE Id IN :userIds ORDER BY Profile.Name, Name]) {
            options.add(new OptionObj(u.Id, u.Name, u.Profile.Name));
        }
        return options;
    }
    @AuraEnabled
    public static void changeOwner(Id pcfId, Id ownerId) {
        Case pcf = [SELECT Id, OwnerId FROM Case WHERE Id = :pcfId];
        pcf.OwnerId = ownerId;
        update pcf;
    }
    @AuraEnabled
    public static void changeEscalated(Id pcfId, Boolean isEscalated) {
        Case pcf = [SELECT Id, IsEscalated FROM Case WHERE Id = :pcfId];
        pcf.IsEscalated = isEscalated;
        pcf.VTD1_IsEscalatedDateTime__c = (isEscalated == true) ? System.now() : null;
        update pcf;
    }
    @AuraEnabled
    public static void changeSafetyReviewedByPI(Id pcfId, Boolean reviewed) {
        Case pcf = [SELECT Id, VTD2_Safety_Concern_Reviewed_by_PI__c FROM Case WHERE Id = :pcfId];
        pcf.VTD2_Safety_Concern_Reviewed_by_PI__c = reviewed;
        update pcf;
    }
    @AuraEnabled
    public static void changeNarrative(Id pcfId, String narrative) {
        Case pcf = [SELECT Id, VTD1_PCF_Call_Narrative__c FROM Case WHERE Id = :pcfId];
        pcf.VTD1_PCF_Call_Narrative__c = narrative;
        update pcf;
    }
    @AuraEnabled
    public static void changeSafetyStatus(Id pcfId, String safetyStatus, String reasonSafetyConcernIndicatorChange) {
        Case pcf = [SELECT Id FROM Case WHERE Id = :pcfId];
        pcf.VTD1_PCF_Safety_Concern_Indicator__c = safetyStatus;
        pcf.VTD2_Reason_Safety_Concern_Indicator_Cha__c = reasonSafetyConcernIndicatorChange;
        update pcf;
    }
    @AuraEnabled
    public static void changeStatus(Id pcfId, String status) {
        try {
            Case pcf = [SELECT Id FROM Case WHERE Id = :pcfId];
            pcf.Status = status;
            update pcf;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
//Modified for SH-19644
    @AuraEnabled
    public static List<Boolean> handleSave(Id pcfId, Boolean checkboxChecked, String pcfeDiaryRecord, String pcfeDiaryDate) {
        Case pcf = [
                SELECT Id, VTR5_Stop_eDiary_Schedule__c, VTD1_Study__r.eCOA_Stop_Schedule_Event__c,
                        VTD1_Clinical_Study_Membership__c , VTD1_Clinical_Study_Membership__r.VTR5_Stop_eDiary_Schedule__c
                FROM Case
                WHERE Id = :pcfId
        ];
//Modified for SH-19644
        pcf.VTR5_eDiary_Schedule_Notes__c = pcfeDiaryRecord;

        if(String.isNotBlank(pcfeDiaryDate)){
            pcf.VTR5_Stop_eDiary_Date__c = Date.valueOf(pcfeDiaryDate);
        }else{
            pcf.VTR5_Stop_eDiary_Date__c = null;
        }       

        List<Boolean>shAndCommunityBooleans = new List<Boolean>();
        if (pcf.VTD1_Study__r.eCOA_Stop_Schedule_Event__c == null && checkboxChecked) {
            return shAndCommunityBooleans;
        }

        shAndCommunityBooleans.add(pcf.VTR5_Stop_eDiary_Schedule__c);
        shAndCommunityBooleans.add(checkboxChecked);


        pcf.VTR5_Stop_eDiary_Schedule__c = checkboxChecked;
        if((!String.isBlank(pcf.VTR5_eDiary_Schedule_Notes__c) || (!String.isBlank(String.valueOf(pcf.VTR5_Stop_eDiary_Date__c))))){
            shAndCommunityBooleans.add(false);
            shAndCommunityBooleans.add(true);
        }
        if (checkboxChecked||(!String.isBlank(pcf.VTR5_eDiary_Schedule_Notes__c) || (!String.isBlank(String.valueOf(pcf.VTR5_Stop_eDiary_Date__c)))) ) {
            List<Case> listOfCase = new  List<Case>{ pcf};
            update listOfCase;
        }
        return shAndCommunityBooleans;
    }

    @AuraEnabled
    public static Id createQueue(Id pcfId) {
        Case pcf = [
                SELECT Id, OwnerId, VTD1_Clinical_Study_Membership__r.VTD1_Study__c, VTD1_Clinical_Study_Membership__r.VTD1_Study__r.Name
                FROM Case
                WHERE Id = :pcfId
        ];
        String queueName = 'SC queue for Study #' + pcf.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.Name;
        List<Group> existingGroup = [SELECT Id FROM Group WHERE Type = 'Queue' AND Name = :queueName];
        if (!existingGroup.isEmpty()) {
            return existingGroup[0].Id;
        }
        Group g = new Group(Type = 'Queue', Name = queueName);
        insert g;
        QueueSObject q = new QueueSObject(SobjectType = 'Case', QueueId = g.Id);
        insert q;
        System.debug('Created new group: ' + g.Id);
        List<Study_Team_Member__c> members = [
                SELECT User__r.Id, User__r.Name
                FROM Study_Team_Member__c
                WHERE Study__c = :pcf.VTD1_Clinical_Study_Membership__r.VTD1_Study__c AND VTD1_Type__c = 'Study Concierge'
        ];
        for (Study_Team_Member__c m : members) {
            GroupMember gm = new GroupMember(GroupId = g.Id, UserOrGroupId = m.User__r.Id);
            insert gm;
            System.debug('Created new member: ' + m.User__r.Id);
        }
        return g.Id;
    }
}