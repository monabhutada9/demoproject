/**
 * @author Ruslan Mullayanov
 * @see VT_R2_ActualVisitParticipantsController
 */
@IsTest
private class VT_R2_ActualVisitParticipantsTest {
    private static User patientUser;
    private static Case caseRecord;

    static {
        List<Case> casesList = [SELECT VTD1_Patient_User__c FROM Case WHERE RecordTypeId = :VT_R4_ConstantsHelper_RecordTypes.RECORD_TYPE_ID_CASE_CAREPLAN];
        if(!casesList.isEmpty()){
            caseRecord = casesList.get(0);
            patientUser = [SELECT Id, ContactId FROM User WHERE Id =: caseRecord.VTD1_Patient_User__c];
        }
    }

    @TestSetup
    static void setup() {
        Test.startTest();
        VT_R3_GlobalSharing.disableForTest = true;

        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t().setRecordTypeByName('Sponsor');
        Account account = (Account) patientAccount.persist();

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
                .setFirstName('Patient')
                .setLastName('Patient')
                //.addAccount(patientAccount);
                .setAccountId(account.Id);

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        //patientUser.persist();

        new DomainObjects.Phone_t(patientAccount, '+1 (999) 123-4567')
                .setType('Mobile')
                .setPrimaryForPhone(true)
                .addContact(patientContact)
                .persist();

        DomainObjects.User_t caregiverUser = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t()
                        .setFirstName('Caregiver')
                        .setLastName('Caregiver')
                        .setEmail(DomainObjects.RANDOM.getEmail())
                        .setRecordTypeName('Caregiver')
                        //.addAccount(patientAccount)
                        .setAccountId(account.Id)
                )
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME);
        //caregiverUser.persist();

        DomainObjects.Case_t patientCase = new DomainObjects.Case_t()
                .addVTD1_Primary_PG(new DomainObjects.User_t())
                .addPIUser(new DomainObjects.User_t())
                .addVTD1_Patient_User(patientUser)
                .addContact(patientContact)
                .setRecordTypeByName('CarePlan')
                //.addAccount(patientAccount);
                .setAccountId(account.Id);
        //patientCase.persist();

        DomainObjects.VTD1_ProtocolVisit_t protocolVisit = new DomainObjects.VTD1_ProtocolVisit_t();

        DomainObjects.VTD1_Actual_Visit_t actualVisit = new DomainObjects.VTD1_Actual_Visit_t()
                .addVTD1_Case(patientCase)
                .addVTD1_ProtocolVisit_t(protocolVisit);

        new DomainObjects.Visit_Member_t()
                .addVTD1_Actual_Visit(actualVisit)
                .addVTD1_Participant_User(patientUser)
                .persist();
        new DomainObjects.Visit_Member_t()
                .addVTD1_Actual_Visit(actualVisit)
                .setVTD1_External_Participant_Type('HHN')
                .persist();
        new DomainObjects.Visit_Member_t()
                .addVTD1_Actual_Visit(actualVisit)
                .addVTD1_Participant_User(caregiverUser)
                .persist();
        Test.stopTest();
    }

    @IsTest
    static void getVisitParticipantsWithNoActualVisitSuccessTest() {
        Test.startTest();
        String avp = VT_R2_ActualVisitParticipantsController.getVisitParticipants(caseRecord.Id, null);
        VT_R2_ActualVisitParticipantsController.ActualVisitParticipants actualVisitParticipants
                = (VT_R2_ActualVisitParticipantsController.ActualVisitParticipants) JSON.deserialize(avp, VT_R2_ActualVisitParticipantsController.ActualVisitParticipants.class);
        System.assertEquals(4, actualVisitParticipants.visitParticipantList.size());
        Test.stopTest();
    }

    @IsTest
    static void getVisitParticipantsWithActualVisitSuccessTest() {
        List<VTD1_Actual_Visit__c> avList = [SELECT Id FROM VTD1_Actual_Visit__c LIMIT 1];
        System.assertNotEquals(0, avList.size());

        Test.startTest();
        String avp = VT_R2_ActualVisitParticipantsController.getVisitParticipants(null, avList[0].Id);
        VT_R2_ActualVisitParticipantsController.ActualVisitParticipants actualVisitParticipants
                = (VT_R2_ActualVisitParticipantsController.ActualVisitParticipants) JSON.deserialize(avp, VT_R2_ActualVisitParticipantsController.ActualVisitParticipants.class);
        System.assertEquals(6, actualVisitParticipants.visitParticipantList.size());
        Test.stopTest();
    }

    @IsTest
    static void updateDatesTimesMapSuccessTest() {
        List<Visit_Member__c> vmList = [SELECT Id, VTD1_Actual_Visit__c FROM Visit_Member__c WHERE VTD1_Actual_Visit__c != NULL];
        System.assertNotEquals(0, vmList.size());

        Test.startTest();
        String datesTimesMap = VT_R2_ActualVisitParticipantsController.updateDatesTimesMap(JSON.serialize(vmList), vmList[0].VTD1_Actual_Visit__c, null);
        System.assert(!String.isEmpty(datesTimesMap));
        Test.stopTest();
    }

    @IsTest
    static void getProtocolVisitIdSuccessTest() {
        List<VTD1_Actual_Visit__c> avList = [SELECT Id, VTD1_Protocol_Visit__c FROM VTD1_Actual_Visit__c WHERE VTD1_Protocol_Visit__c != NULL LIMIT 1];
        System.assertNotEquals(0, avList.size());

        Test.startTest();
        System.assertEquals(avList[0].VTD1_Protocol_Visit__c, VT_R2_ActualVisitParticipantsController.getProtocolVisitId(avList[0].Id));
        Test.stopTest();
    }

    @IsTest
    static void getAllParticipantsSuccessTest() {
        User scrUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME)
                .persist();
        User piUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME)
                .persist();
        List<VTD1_Actual_Visit__c> avList = [SELECT Id, VTD1_Protocol_Visit__c FROM VTD1_Actual_Visit__c WHERE VTD1_Protocol_Visit__c != NULL LIMIT 1];
        System.assertNotEquals(0, avList.size());

        Test.startTest();
        String participants1 = VT_R2_ActualVisitParticipantsController.getAllParticipants(caseRecord.Id, avList[0].Id);
        VT_R2_ActualVisitParticipantsController.ActualVisitParticipants avp1
                = (VT_R2_ActualVisitParticipantsController.ActualVisitParticipants) JSON.deserialize(participants1, VT_R2_ActualVisitParticipantsController.ActualVisitParticipants.class);
        System.assertEquals(4, avp1.visitParticipantList.size());

        System.runAs(scrUser) {
            String participants2 = VT_R2_ActualVisitParticipantsController.getAllParticipants(caseRecord.Id, null);
            System.assertEquals(VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME, VT_R2_ActualVisitParticipantsController.getCurrentUserProfile());
            Set<Id> userIds = VT_R2_ActualVisitParticipantsController.getSCRandSubIUserIds(piUser.Id);
            System.assertEquals(0, userIds.size());
        }
        Test.stopTest();
    }

    @IsTest
    static void createVisitMembersSuccessTest() {
        Id recordTypeIdRegular = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_VISIT_MEMBER_REGULAR;

        List<VT_R2_ActualVisitParticipantsController.VisitParticipant> visitParticipantList
                = new List<VT_R2_ActualVisitParticipantsController.VisitParticipant>();

        VT_R2_ActualVisitParticipantsController.VisitParticipant visitParticipantPG
                = new VT_R2_ActualVisitParticipantsController.VisitParticipant();
        visitParticipantPG.Id = new DomainObjects.User_t().persist().Id;
        visitParticipantPG.Role = VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME;
        visitParticipantPG.type = VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME;
        visitParticipantPG.nameHHNPhleb = 'nameHHNPhleb';
        visitParticipantPG.recordTypeId = recordTypeIdRegular;
        visitParticipantList.add(visitParticipantPG);

        VT_R2_ActualVisitParticipantsController.VisitParticipant visitParticipantPI
                = new VT_R2_ActualVisitParticipantsController.VisitParticipant();
        visitParticipantPI.Id = new DomainObjects.User_t().persist().Id;
        visitParticipantPI.Role = VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME;
        visitParticipantPI.type = VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME;
        visitParticipantPI.nameHHNPhleb = 'nameHHNPhleb';
        visitParticipantPI.recordTypeId = recordTypeIdRegular;
        visitParticipantList.add(visitParticipantPI);

        VT_R2_ActualVisitParticipantsController.VisitParticipant visitParticipantPatient
                = new VT_R2_ActualVisitParticipantsController.VisitParticipant();
        visitParticipantPatient.Id = new DomainObjects.User_t().persist().Id;
        visitParticipantPatient.Role = VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME;
        visitParticipantPatient.type = VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME;
        visitParticipantList.add(visitParticipantPatient);

        VT_R2_ActualVisitParticipantsController.VisitParticipant visitParticipantCaregiver
                = new VT_R2_ActualVisitParticipantsController.VisitParticipant();
        visitParticipantCaregiver.Id = new DomainObjects.User_t().persist().Id;
        visitParticipantCaregiver.Role = VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME;
        visitParticipantCaregiver.type = VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME;
        visitParticipantCaregiver.nameHHNPhleb = 'nameHHNPhleb';
        visitParticipantCaregiver.recordTypeId = recordTypeIdRegular;
        visitParticipantList.add(visitParticipantCaregiver);

        String visitParticipants = JSON.serialize(visitParticipantList);

        List<Visit_Member__c> vmList = [SELECT Id, VTD1_Actual_Visit__c FROM Visit_Member__c WHERE VTD1_Actual_Visit__c != NULL LIMIT 1];
        System.assertNotEquals(0, vmList.size());

        Test.startTest();
        try {
            VT_R2_ActualVisitParticipantsController.createVisitMembers(vmList[0].VTD1_Actual_Visit__c, visitParticipants, new List<Id>{
                    vmList[0].Id
            });
        } catch (Exception e) {
            System.debug(e.getMessage());
        }
        Test.stopTest();
    }

    @IsTest
    static void getVisitMembersSuccessTest() {
        List<Visit_Member__c> vmList = [SELECT Id, VTD1_Actual_Visit__c FROM Visit_Member__c WHERE VTD1_Actual_Visit__c != NULL LIMIT 1];
        System.assertNotEquals(0, vmList.size());

        Test.startTest();
        System.assertNotEquals(null, VT_R2_ActualVisitParticipantsController.getActualVisit(vmList[0].VTD1_Actual_Visit__c));
        String visitMembers = VT_R2_ActualVisitParticipantsController.getVisitMembers(vmList[0].VTD1_Actual_Visit__c);
        List<Visit_Member__c> members = (List<Visit_Member__c>) JSON.deserialize(visitMembers, List<Visit_Member__c>.class);
        System.assertEquals(6, members.size());
        System.assertEquals(vmList[0].Id, members[0].Id);
        Test.stopTest();
    }

    @IsTest
    static void getPatientPhoneSuccessTest() {
        Test.startTest();
        String patientPhone = VT_R2_ActualVisitParticipantsController.getPatientPhone(caseRecord.Id);
        System.assert(!String.isEmpty(patientPhone));
        Test.stopTest();
    }

    @IsTest
    static void getPicklistOptionsSuccessTest() {
        Test.startTest();
        Map<String, String> picklistOptions = VT_R2_ActualVisitParticipantsController.getPicklistOptions('Visit_Member__c', 'VTD1_External_Participant_Type__c');
        System.assert(picklistOptions != null && !picklistOptions.isEmpty());
        Test.stopTest();
    }
}