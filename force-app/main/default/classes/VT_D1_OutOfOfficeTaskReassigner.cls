public without sharing class VT_D1_OutOfOfficeTaskReassigner {

    private static final Id PG_PROFILE_ID = VTD1_RTId__c.getInstance().VTD2_Profile_PG__c;
    private static final Id SCR_PROFILE_ID = VTD1_RTId__c.getInstance().VTD2_Profile_SCR__c;

    private List<Task> tasks = new List<Task>();
    private List<VTD1_NotificationC__c> notifications = new List<VTD1_NotificationC__c>();
    private List<Task> oooTasks = new List<Task>();
    private List<VTD1_NotificationC__c> oooNotifications = new List<VTD1_NotificationC__c>();

    private List<Id> userIds = new List<Id>();
    private Set<Id> oooPgUserIds = new Set<Id>();
    private Set<Id> oooSCRUserIds = new Set<Id>();
    private Map<Id, Case> ownerIdToCasePGMap = new Map<Id, Case>();
    private Map<Id, Case> ownerIdToCaseSCRMap = new Map<Id, Case>();
    //private Map<Id, Id> primaryToBackupSCRMap = new Map<Id, Id>();

    private Map<Id, Id> whatIdToBackupHolderId = new Map<Id, Id>();
    private final Map <String, String> whatIdTypeToBackupHolderFieldNameMap = new Map<String, String>{
            'VTD1_Actual_Visit__c' => 'VTD1_Case__c',
            'VTD1_Monitoring_Visit__c' => 'VTD1_Virtual_Site__c',
            'VTD1_Protocol_Deviation__c' => 'VTD1_Subject_ID__c,VTD1_Virtual_Site__c',
            'VTD1_Action_Item__c' => 'VTD1_SiteId__c',
            'VTD1_Document__c' => 'VTD1_Clinical_Study_Membership__c,VTD1_Site__c',
            'VTD1_Patient_Payment__c' => 'VTD1_Clinical_Study_Membership__c',
            'VTD1_Order__c' => 'VTD1_Case__c',
            'VTD1_Patient_Kit__c' => 'VTD1_Case__c',
            'IMP_Replacement_Form__c' => 'VTD1_Case__c',
            'VTD1_Survey__c' => 'VTD1_CSM__c',
            'Case' => '',
            'Virtual_Site__c' => ''
    };

    public void doReassign(List<Task> tasks) {
        if (tasks != null && !tasks.isEmpty()) {
            this.tasks = tasks;

            getOooTasks();

            if (!this.oooTasks.isEmpty()) {
                reassignOooTasks();
            }
        }
    }

    public void doReassign(List<VTD1_NotificationC__c> notifications) {
        if (notifications != null && !notifications.isEmpty()) {
            this.notifications = notifications;

            getOooTasks();

            if (!this.oooNotifications.isEmpty()) {
                reassignOooNotifications();
            }
        }
    }

    private void getOooTasks() {
        getUserIds();
        getOooPgUserIds();

        for (Task item : this.tasks) {
            if (this.oooPgUserIds.contains(item.OwnerId) || this.oooSCRUserIds.contains(item.OwnerId)) {
                this.oooTasks.add(item);
            }
        }
        for (VTD1_NotificationC__c item : this.notifications) {
            if (this.oooPgUserIds.contains(item.VTD1_Receivers__c) || this.oooSCRUserIds.contains(item.VTD1_Receivers__c)) {
                this.oooNotifications.add(item);
            }
        }
    }

    private void getUserIds() {
        for (Task item : this.tasks) {
            this.userIds.add(item.OwnerId);
        }
        for (VTD1_NotificationC__c item : this.notifications) {
            this.userIds.add(item.VTD1_Receivers__c);
        }
    }

    private void getOooPgUserIds() {
        Datetime now = Datetime.now();
        System.debug('userIds = ' + userIds);
        List<Event> eventList = [
                SELECT OwnerId, Owner.ProfileId
                FROM Event
                WHERE StartDateTime <= :now
                    AND EndDateTime > :now
                    AND RecordType.DeveloperName = 'OutOfOffice'
                    AND OwnerId IN :this.userIds
                    AND Owner.ProfileId IN :new List<Id>{ PG_PROFILE_ID, SCR_PROFILE_ID }
        ];
        for (Event item : eventList) {
            if (item.Owner.ProfileId == PG_PROFILE_ID) {
                this.oooPgUserIds.add(item.OwnerId);
            } else {
                this.oooSCRUserIds.add(item.OwnerId);
            }
        }
    }

    private void reassignOooNotifications() {
        Map <Id, Id> caseIdToBackupSCRIdMap = new Map<Id, Id>();
        for (VTD1_NotificationC__c item : this.oooNotifications) {
            System.debug('item = ' + item);
            if (item.VTD1_CSM__c != null) {
                caseIdToBackupSCRIdMap.put(item.VTD1_CSM__c, null);
            }
        }
        if (!caseIdToBackupSCRIdMap.isEmpty()) {
            for (Case caseObj : [SELECT Id, VTR2_SiteCoordinator__c, VTR2_Backup_Site_Coordinator__c FROM Case
                    WHERE Id IN :caseIdToBackupSCRIdMap.keySet()
                    AND VTR2_SiteCoordinator__c IN :oooSCRUserIds
                    AND VTR2_Backup_Site_Coordinator__c != NULL]) {
                caseIdToBackupSCRIdMap.put(caseObj.Id, caseObj.VTR2_Backup_Site_Coordinator__c);
            }
        }
        List <VTD1_NotificationC__c> unresolvedByCase = new List<VTD1_NotificationC__c>();
        Set <Id> studyIds = new Set<Id>();
        for (VTD1_NotificationC__c notification : this.notifications) {
            if (notification.VTD1_CSM__c == null) {
                if (notification.VTR2_Study__c != null) {
                    unresolvedByCase.add(notification);
                    studyIds.add(notification.VTR2_Study__c);
                }
                continue;
            }
            Id backupSCRId = caseIdToBackupSCRIdMap.get(notification.VTD1_CSM__c);
            if (backupSCRId == null) {
                if (notification.VTR2_Study__c != null) {
                    unresolvedByCase.add(notification);
                    studyIds.add(notification.VTR2_Study__c);
                }
                continue;
            }
            notification.VTD1_Receivers__c = backupSCRId;
        }
        if (!studyIds.isEmpty()) {
            Map<Id, Id> primaryToBackupSCRMap = getReassignMapByStudies(studyIds);
            for (VTD1_NotificationC__c notification : unresolvedByCase) {
                Id backupSCRId = primaryToBackupSCRMap.get(notification.VTD1_Receivers__c);
                if (backupSCRId != null) {
                    notification.VTD1_Receivers__c = backupSCRId;
                    System.debug('changed for SCR from SSTM = ' + notification.VTD1_Receivers__c);
                }
            }
        }
    }

    private Map<Id, Id> getReassignMapByStudies(Set <Id> studyIds) {
        Map<Id, Id> primaryToBackupSCRMap = new Map<Id, Id>();
        List <Study_Site_Team_Member__c> studySiteTeamMembers = [select Id, VTD1_Associated_PI__c, VTR2_Associated_SCr__r.User__c, VTR2_SCr_type__c, VTR2_Associated_SCr__r.Study__c
        from Study_Site_Team_Member__c where VTR2_SCr_type__c in ('Primary', 'Backup') and VTR2_Associated_SCr__r.Study__c in : studyIds order by VTR2_Associated_SCr__r.Study__c];

        if (studySiteTeamMembers.size() != 0) {
            Id studyId = studySiteTeamMembers[0].VTR2_Associated_SCr__r.Study__c;
            Id primarySCRUserId = null;
            Id backupSCRUserId = null;
            for (Study_Site_Team_Member__c studySiteTeamMember : studySiteTeamMembers) {
                if (studyId != studySiteTeamMember.VTR2_Associated_SCr__r.Study__c) {
                    primarySCRUserId = null;
                    backupSCRUserId = null;
                }
                if (studySiteTeamMember.VTR2_SCr_type__c == 'Primary') {
                    primarySCRUserId = studySiteTeamMember.VTR2_Associated_SCr__r.User__c;
                } else {
                    backupSCRUserId = studySiteTeamMember.VTR2_Associated_SCr__r.User__c;
                }
                System.debug('SCR primary backup = ' + primarySCRUserId + ' ' + backupSCRUserId);
                if (primarySCRUserId != null && backupSCRUserId != null) {
                    primaryToBackupSCRMap.put(primarySCRUserId, backupSCRUserId);
                }
            }
        }
        return primaryToBackupSCRMap;
    }

    private void reassignOooTasks() {
        getWhatIdToCaseMap();

        /*for (Task item : this.oooTasks) {
            if (item.Owner.Profile.Name == 'Patient Guide' && this.whatIdToCaseMap.containsKey(item.WhatId) && this.whatIdToCaseMap.get(item.WhatId).VTD1_Primary_PG__c == item.OwnerId) {
                item.OwnerId = this.whatIdToCaseMap.get(item.WhatId).VTD1_Secondary_PG__c;
            }
        }*/

        // separately for Site Coordination
        Set <Id> caseIds = new Set<Id>();
        Set <Id> virtualSiteIds = new Set<Id>();
        for (Id id : whatIdToBackupHolderId.values()) {
            String backupHolderType = id.getSobjectType().getDescribe().getName();
            System.debug('backupHolderType = ' + backupHolderType);
            if (backupHolderType == 'Case') {
                caseIds.add(id);
            } else if (backupHolderType == 'Virtual_Site__c') {
                virtualSiteIds.add(id);
            }
        }

        if (!caseIds.isEmpty()) {
            List <Case> cases = [select Id, VTR2_SiteCoordinator__c, VTD1_Primary_PG__c, VTD1_Secondary_PG__c,
                    VTR2_Backup_Site_Coordinator__c from Case where Id in : caseIds and (VTD1_Primary_PG__c in : oooPgUserIds or VTR2_SiteCoordinator__c in : oooSCRUserIds)];

            System.debug('cases = ' + cases);

            for (Case caseObj : cases) {
                if (oooPgUserIds.contains(caseObj.VTD1_Primary_PG__c))
                    ownerIdToCasePGMap.put(caseObj.VTD1_Primary_PG__c, caseObj);
                if (oooSCRUserIds.contains(caseObj.VTR2_SiteCoordinator__c))
                    ownerIdToCaseSCRMap.put(caseObj.VTR2_SiteCoordinator__c, caseObj);
            }
        }

        Map<Id, Id> primaryToBackupSCRMap = new Map<Id, Id>();

        if (!virtualSiteIds.isEmpty()) {
            List <Virtual_Site__c> virtualSites = [select Id, VTD1_Study__c, VTD1_Study_Team_Member__c from Virtual_Site__c where Id in : virtualSiteIds];
            Set <Id> studyIds = new Set<Id>();
            for (Virtual_Site__c virtualSite : virtualSites) {
                studyIds.add(virtualSite.VTD1_Study__c);
            }
            primaryToBackupSCRMap = getReassignMapByStudies(studyIds);
        }

        System.debug('ownerIdToCasePGMap = ' + ownerIdToCasePGMap);
        System.debug('ownerIdToCaseSCRMap = ' + ownerIdToCaseSCRMap);

        for (Task item : this.oooTasks) {
            Case caseObj = ownerIdToCasePGMap.get(item.OwnerId);
            if (caseObj != null && caseObj.VTD1_Secondary_PG__c != null) {
                item.OwnerId = caseObj.VTD1_Secondary_PG__c;
                item.VTR2_Redirect_Status__c = 'Redirected to backup';
                System.debug('changed for PG = ' + item.OwnerId);
                continue;
            }
            caseObj = ownerIdToCaseSCRMap.get(item.OwnerId);
            if (caseObj != null && caseObj.VTR2_Backup_Site_Coordinator__c != null) {
                item.OwnerId = caseObj.VTR2_Backup_Site_Coordinator__c;
                item.VTR2_Redirect_Status__c = 'Redirected to backup';
                System.debug('changed for SCR = ' + item.OwnerId);
                continue;
            }
            System.debug('primaryToBackupSCRMap' + primaryToBackupSCRMap);
            System.debug('item.OwnerId ' + item.OwnerId);
            Id backupSCRId = primaryToBackupSCRMap.get(item.OwnerId);
            if (backupSCRId != null) {
                item.OwnerId = backupSCRId;
                item.VTR2_Redirect_Status__c = 'Redirected to backup';
                System.debug('changed for SCR from SSTM = ' + item.OwnerId);
            }
            if (String.isBlank(item.VTR2_Redirect_Status__c)) {
                item.VTR2_Redirect_Status__c = 'Not redirected to backup';
            }
        }
    }

    private void getWhatIdToCaseMap() {
        getTasksByWhatType();

        /*if (this.tasksByWhatType.containsKey('Case')) { getCases(); }
        if (this.tasksByWhatType.containsKey('VTD1_Actual_Visit__c')) { getActualVisitCases(); }
        if (this.tasksByWhatType.containsKey('VTD1_SC_Task__c')) { getScTaskCases(); }*/
    }

    private void getTasksByWhatType() {
        Map <String, Set <Id>> queryToBackupHolderIdsMap = new Map<String, Set<Id>>();
        for (Task item : this.tasks) {
            String whatType = item.WhatId.getSobjectType().getDescribe().getName();
            /*if (!this.tasksByWhatType.containsKey(whatType)) {
                this.tasksByWhatType.put(whatType, new List<Task>());
            }
            this.tasksByWhatType.get(whatType).add(item);*/

            // new
            System.debug('whatType = ' + whatType);

            String backupHolderFieldName = whatIdTypeToBackupHolderFieldNameMap.get(whatType);
            if (backupHolderFieldName != null) {
                if (String.isBlank(backupHolderFieldName)) {
                    whatIdToBackupHolderId.put(item.WhatId, item.WhatId);
                    continue;
                }
                String query = 'select Id,' + backupHolderFieldName + ' from ' + whatType;
                Set <Id> backupHolderIds = queryToBackupHolderIdsMap.get(query);
                if (backupHolderIds == null) {
                    backupHolderIds = new Set <Id>();
                    queryToBackupHolderIdsMap.put(query, backupHolderIds);
                }
                backupHolderIds.add(item.WhatId);
            }
        }

        for (String query : queryToBackupHolderIdsMap.keySet()) {
            List <String> whereClause = new List<String>();
            for (Id id : queryToBackupHolderIdsMap.get(query)) {
                whereClause.add('\'' + id + '\'');
            }
            String fullQuery = query + ' where Id in (' + String.join(whereClause, ',') + ')';
            System.debug('query = ' + fullQuery);
            Integer firstCommaPosition = query.indexOf(',');
            String backupHolderFieldNames = query.substring(firstCommaPosition + 1, query.indexOf(' ', firstCommaPosition));
            List <String> backupHolderFieldNameList = backupHolderFieldNames.split(',');
            System.debug('backupHolderFieldNameList = ' + backupHolderFieldNameList);

            List <SObject> sObjects = Database.query(fullQuery);

            for (SObject sobj : sObjects) {
                for (String backupHolderFieldName : backupHolderFieldNameList) {
                    Id backupHolderId = (Id) sobj.get(backupHolderFieldName);
                    System.debug('backupHolderId = ' + backupHolderId);
                    if (backupHolderId != null) {
                        whatIdToBackupHolderId.put(sobj.Id, backupHolderId);
                        break;
                    }
                }
            }
        }
    }

    /*private void getCases() {
        for (Case item : [
            SELECT Id, VTD1_Primary_PG__c, VTD1_Secondary_PG__c
            FROM Case
            WHERE Id IN :getWhatIds('Case')
            AND VTD1_Secondary_PG__c != NULL
            AND VTD1_Primary_PG__c IN :THIS.oooPgUserIds
        ]) {
            this.whatIdToCaseMap.put(item.Id, item);
        }
    }

    private void getActualVisitCases() {
        for (VTD1_Actual_Visit__c item : [
            SELECT Id, VTD1_Case__c, VTD1_Case__r.VTD1_Secondary_PG__c, VTD1_Case__r.VTD1_Primary_PG__c
            FROM VTD1_Actual_Visit__c
            WHERE Id IN :getWhatIds('VTD1_Actual_Visit__c')
            AND VTD1_Case__r.VTD1_Secondary_PG__c != NULL
            AND VTD1_Case__r.VTD1_Primary_PG__c IN :THIS.oooPgUserIds
        ]) {
            this.whatIdToCaseMap.put(item.Id, item.VTD1_Case__r);
        }
    }

    private void getScTaskCases() {
        for (VTD1_SC_Task__c item : [
            SELECT Id, VTD1_Patient__c, VTD1_Patient__r.VTD1_Secondary_PG__c, VTD1_Patient__r.VTD1_Primary_PG__c
            FROM VTD1_SC_Task__c
            WHERE Id IN :getWhatIds('VTD1_SC_Task__c')
                AND VTD1_Patient__r.VTD1_Secondary_PG__c != NULL
                AND VTD1_Patient__r.VTD1_Primary_PG__c IN :THIS.oooPgUserIds
        ]) {
            this.whatIdToCaseMap.put(item.Id, item.VTD1_Patient__r);
        }
    }

    private List<Id> getWhatIds(String whatType) {
        List<Id> recIds = new List<Id>();
        for (Task item : this.tasksByWhatType.get(whatType)) {
            recIds.add(item.WhatId);
        }
        return recIds;
    }*/
}