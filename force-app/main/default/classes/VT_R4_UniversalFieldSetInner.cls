/**
 * Created by shumeyko on 27/01/2020.
 */

public with sharing class VT_R4_UniversalFieldSetInner {
    @AuraEnabled
    public static Object getActualVisitById(Id objectId) {
        Object obj;
        try {
            String ss = 'Select RecordTypeId FROM ' + objectId.getSObjectType() + ' WHERE Id = : objectId';
            List <SObject> objects = Database.query(ss);
            if (!objects.isEmpty()) {
                obj = objects[0];
            }
        } catch (Exception e) {
            System.debug(e.getMessage() + e.getStackTraceString());
        }
        return obj;
    }
}