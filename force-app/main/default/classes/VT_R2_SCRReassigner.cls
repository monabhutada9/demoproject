/**
* @author: Carl Judge
* @date: 26-Jun-19
* @description: Reassign SCRs on Cases (probably after PI change)
**/

public without sharing class VT_R2_SCRReassigner {

    private List<Case> cases;
    private Map<Id, Map<String, Id>> siteToScrMap = new Map<Id, Map<String, Id>>(); // site ID => scr type => scr ID

    private VT_R2_SCRReassigner(List<Case> cases) {
        this.cases = cases;
    }

    public static void reassignSCRs(List<Case> cases) {
        new VT_R2_SCRReassigner(cases).execute();
    }

    private void execute() {
        getSiteToScrMap();
        assignSCRs();
    }

    private void getSiteToScrMap() {
        for (Case item : cases) {
            if (item.VTD1_Virtual_Site__c != null) {
                siteToScrMap.put(item.VTD1_Virtual_Site__c, new Map<String, Id>());
            }
        }

        if (!siteToScrMap.isEmpty()) {
            for (Study_Site_Team_Member__c sstm : [
                SELECT VTR2_Associated_PI__r.VTD1_VirtualSite__c, VTR2_Associated_SCr__r.User__c, VTR2_SCr_type__c
                FROM Study_Site_Team_Member__c
                WHERE VTR2_Associated_PI__r.VTD1_VirtualSite__c IN :siteToScrMap.keySet()
                AND VTR2_SCr_type__c IN (:VT_R4_ConstantsHelper_ProfilesSTM.SSTM_SCR_TYPE_PRIMARY, :VT_R4_ConstantsHelper_ProfilesSTM.SSTM_SCR_TYPE_BACKUP)
            ]) {
                siteToScrMap.get(sstm.VTR2_Associated_PI__r.VTD1_VirtualSite__c).put(sstm.VTR2_SCr_type__c, sstm.VTR2_Associated_SCr__r.User__c);
            }
        }
    }

    private void assignSCRs() {
        for (Case cas : cases) {
            Id priScrId;
            Id secScrId;

            if (siteToScrMap.containsKey(cas.VTD1_Virtual_Site__c)) {
                priScrId = siteToScrMap.get(cas.VTD1_Virtual_Site__c).get(VT_R4_ConstantsHelper_ProfilesSTM.SSTM_SCR_TYPE_PRIMARY);
                secScrId = siteToScrMap.get(cas.VTD1_Virtual_Site__c).get(VT_R4_ConstantsHelper_ProfilesSTM.SSTM_SCR_TYPE_BACKUP);
            }

            cas.VTR2_SiteCoordinator__c = priScrId;
            cas.VTR2_Backup_Site_Coordinator__c = secScrId;
        }
    }
}