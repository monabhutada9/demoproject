/**
 * Created by Galiia Khalikova on 05.11.2019.
 */

public with sharing class VT_R3_UpdateAccessStudy {
    public class StudyUser {
        public Id studyId;
        public Id userId;

        public StudyUser(Id studyId, Id userId) {
            this.studyId = studyId;
            this.userId = userId;
        }
    }

    public static void studySharingOnInsert(List<Study_Team_Member__c> recs) {
        List<StudyUser> studyUsers = getStudyUsersFromStms(recs);
        Map<Id, Set<Id>> studyParticipants = getStudyParticipantsFromStms(recs);
        studyUserSharingOnInsert(studyUsers, studyParticipants);
    }

    public static void studySharingOnDelete(List<Study_Team_Member__c> recs) {
        List<StudyUser> studyUsers = getStudyUsersFromStms(recs);
        Map<Id, Set<Id>> studyParticipants = getStudyParticipantsFromStms(recs);
        studyUserSharingOnDelete(studyUsers, studyParticipants);
    }

    public static void studyParticipantSharingOnAdd(List<StudyUser> studyUsers) {
        Map<Id, Set<Id>> studyMembersByStudy = getStmUsersForParticipants(studyUsers);
        studyUserSharingOnInsert(studyUsers, studyMembersByStudy);
    }

    public static void studyParticipantSharingOnRemove(List<StudyUser> studyUsers) {
        Map<Id, Set<Id>> studyMembersByStudy = getStmUsersForParticipants(studyUsers);
        studyUserSharingOnDelete(studyUsers, studyMembersByStudy);
    }

    private static void studyUserSharingOnInsert(List<StudyUser> studyUsers, Map<Id, Set<Id>> alternativeStudyUsers) {
        Map<String, VTD1_Study_Sharing_Configuration__c> mapConfig = getConfigMap(studyUsers);
        List<VTD1_Study_Sharing_Configuration__c> configs = new List<VTD1_Study_Sharing_Configuration__c>();

        for (StudyUser item : studyUsers) {
            String key = '' + item.studyId + item.userId;
            if (mapConfig.containsKey(key)) {
                if (!hasAlternativeStudyAccess(alternativeStudyUsers, item.studyId, item.userId)) {
                    VTD1_Study_Sharing_Configuration__c config = mapConfig.get(key);
                    config.VTD1_Access_Level__c = 'Edit';
                    configs.add(config);
                }
            }
            else{
                configs.add(new VTD1_Study_Sharing_Configuration__c(
                    VTD1_Access_Level__c = 'Edit',
                    VTD1_Study__c = item.studyId,
                    VTD1_User__c = item.userId
                ));
            }
        }

        Map <String,VTD1_Study_Sharing_Configuration__c> mapSharingConfigurations = new Map<String, VTD1_Study_Sharing_Configuration__c>();
        for (VTD1_Study_Sharing_Configuration__c config : configs) {
            mapSharingConfigurations.put(config.VTD1_Access_Level__c + '_' + config.VTD1_Study__c + '_' + config.VTD1_User__c, config);
        }
		system.debug('mapSharingConfigurations.values() :: '+ mapSharingConfigurations.values());
        upsert mapSharingConfigurations.values();
        //upsert configs;
    }

    private static void studyUserSharingOnDelete(List<StudyUser> studyUsers, Map<Id, Set<Id>> alternativeStudyUsers) {
        Map<String, VTD1_Study_Sharing_Configuration__c> mapConfig = getConfigMap(studyUsers);
        Map<String, VTD1_Study_Sharing_Configuration__c> deleteConfigurationMap = new Map<String, VTD1_Study_Sharing_Configuration__c>();

        for (StudyUser item : studyUsers) {
            String key = '' + item.studyId + item.userId;
            if (mapConfig.containsKey(key) && !hasAlternativeStudyAccess(alternativeStudyUsers, item.studyId, item.userId)) {
                VTD1_Study_Sharing_Configuration__c config = mapConfig.get(key);
                if (!deleteConfigurationMap.containsKey(config.Id)) {
                    config.VTD1_Access_Level__c = 'None';
                    deleteConfigurationMap.put(config.Id, config);
                }
            }
        }
        update deleteConfigurationMap.values();
    }

    private static Map<Id, Set<Id>> getStmUsersForParticipants(List<StudyUser> studyUsers) {
        List <Id> studyIds = new List<Id>();
        List <Id> userIds = new List<Id>();

        for (StudyUser item : studyUsers) {
            studyIds.add(item.studyId);
            userIds.add(item.userId);
        }

        Map<Id, Set<Id>> stmUsersByStudy = new Map<Id, Set<Id>>();
        for (Study_Team_Member__c stm : [
            SELECT Study__c, User__c
            FROM Study_Team_Member__c
            WHERE Study__c IN :studyIds AND User__c IN :userIds
        ]) {
            if (!stmUsersByStudy.containsKey(stm.Study__c)) {
                stmUsersByStudy.put(stm.Study__c, new Set<Id>());
            }
            stmUsersByStudy.get(stm.Study__c).add(stm.User__c);
        }
        return stmUsersByStudy;
    }

    private static List<StudyUser> getStudyUsersFromStms(List<Study_Team_Member__c> stms) {
        List<StudyUser> studyUsers = new List<StudyUser>();
        for (Study_Team_Member__c stm : stms) {
            if(stm.VTD1_Active__c){
            studyUsers.add(new StudyUser(stm.Study__c, stm.User__c));
            }
        }
        return studyUsers;
    }

    private static Map<Id, Set<Id>> getStudyParticipantsFromStms(List<Study_Team_Member__c> stms) {
        List<Id> studyIds = new List<Id>();
        for (Study_Team_Member__c stm : stms) {
            if(stm.VTD1_Active__c){
            studyIds.add(stm.Study__c);
            }
        }

        Map<Id, Set<Id>> participantsByStudy = new Map<Id, Set<Id>>();
        String queryString = String.format(
            'SELECT Id, {0} FROM HealthCloudGA__CarePlanTemplate__c' +//
                ' WHERE Id IN :studyIds',
            new List<String>{String.join(VT_D2_StudyTriggerHandler.STUDY_PARTICIPANT_FIELDS,',')}
        );
        for (HealthCloudGA__CarePlanTemplate__c study : Database.query(queryString)) {
            participantsByStudy.put(study.Id, new Set<Id>());
            for (String fld : VT_D2_StudyTriggerHandler.STUDY_PARTICIPANT_FIELDS) {
                Object fldObj = study.get(fld);
                if (fldObj != null) {
                    participantsByStudy.get(study.Id).add((Id)fldObj);
                }
            }
        }
        return participantsByStudy;
    }

    private static Map<String, VTD1_Study_Sharing_Configuration__c> getConfigMap(List<StudyUser> studyUsers) {
        List <Id> studyIds = new List<Id>();
        List <Id> setUserIds = new List<Id>();

        for (StudyUser item : studyUsers) {
            studyIds.add(item.studyId);
            setUserIds.add(item.userId);
        }

        List<VTD1_Study_Sharing_Configuration__c> listConfigurations = [
            SELECT VTD1_Access_Level__c, VTD1_Study__c, VTD1_User__c
            FROM VTD1_Study_Sharing_Configuration__c
            WHERE VTD1_Study__c IN :studyIds
            AND VTD1_User__c IN :setUserIds
        ];

        Map<String, VTD1_Study_Sharing_Configuration__c> mapConfig = new Map<String, VTD1_Study_Sharing_Configuration__c>();
        for (VTD1_Study_Sharing_Configuration__c item : listConfigurations) {
            mapConfig.put('' + item.VTD1_Study__c + item.VTD1_User__c, item);
        }
        return mapConfig;
    }

    private static Boolean hasAlternativeStudyAccess(Map<Id, Set<Id>> studyParticipants, Id studyId, Id userId) {
        return studyParticipants.containsKey(studyId) && studyParticipants.get(studyId).contains(userId);
    }
}