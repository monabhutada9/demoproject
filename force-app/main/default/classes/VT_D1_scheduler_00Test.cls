/**
 * Created by User on 19/05/16.
 */
@isTest
public with sharing class VT_D1_scheduler_00Test {

    public static void doTest(){
        Test.startTest();
        VT_D1_scheduler_00 myClass = new VT_D1_scheduler_00 ();
        String chron = '0 0 23 * * ?';
        System.schedule('Test Sched', chron, myClass);
        Test.stopTest();
    }
}