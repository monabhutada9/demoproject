/**
* @author: Carl Judge
* @date: 16-Jul-20
* @description: Test VT_R5_eCoaPointToPointHelper and other point to point classes
**/

@IsTest
private class VT_R5_eCoaPointToPointHelperTest {
    @TestSetup
    static void setup() {
        insert new eCOA_IntegrationDetails__c(
            UsePointToPoint__c = true,
            Cognito_ClientId__c = 'someClientId',
            Cognito_UserPoolId__c = 'someUserPoolId'
        );
    }

    @IsTest
    static void testResponse() {
        Test.setMock(HttpCalloutMock.class, new CalloutMock());
        Test.startTest();
            VT_R5_eCoaResponseExport.getResponseExport('testGuid');
        Test.stopTest();
    }

    @IsTest
    static void testSubjectCreate() {
        Test.setMock(HttpCalloutMock.class, new CalloutMock());
        Test.startTest();
            new VT_R5_eCoaCreateSubject(UserInfo.getUserId()).doCallout();
        Test.stopTest();
    }

    @IsTest
    static void testSubjectUpdate() {
        Test.setMock(HttpCalloutMock.class, new CalloutMock());
        Test.startTest();
            new VT_R5_eCoaUpdateSubject(UserInfo.getUserId()).doCallout();
        Test.stopTest();
    }

    class CalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setStatus('OK');
            response.setStatusCode(200);

            switch on request.getEndpoint().substringAfterLast('/') {
                when 'token' {
                    response.setBody(JSON.serialize(
                        new Map<String, Object> {
                            'token' => new Map<String, Object> {
                                'jwtToken' => 'someToken',
                                'payload' => new Map<String, Object> {
                                    'exp' => Datetime.now().addHours(1).getTime() / 1000
                                }
                            },
                            'sessionId' => 'someSessionId'
                        }
                    ));
                }
                when 'userLogin' {
                    response.setHeader('set-cookie', 'someCookie;someStuffWeDontNeed');
                    response.setBody(JSON.serialize(
                        new Map<String, Object> {
                            'maxAge' => 600000
                        }
                    ));
                }
                when 'versions' {
                    response.setBody('"guid":"someVersionGuid"ruleSetGuids":["someRuleSetGuid"');
                }
                when 'rulesets' {
                    response.setBody(JSON.serialize(
                        new Map<String, Object> {
                            'data' => new List<Map<String, Object>>{
                                new Map<String, Object> {
                                    'ruleSetGuid' => 'someEproGuid',
                                    'ruleSetType' => 'epro'
                                },
                                new Map<String, Object> {
                                    'ruleSetGuid' => 'someClinroGuid',
                                    'ruleSetType' => 'clinro'
                                }
                            }
                        }
                    ));
                }
                when else {
                    response.setBody('{}');
                    response.setStatusCode(202);
                }
            }

            return response;
        }
    }
}