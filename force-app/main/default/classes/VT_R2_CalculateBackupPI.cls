/**
 * Created by Alexander Komarov on 22.02.2019.
 */

public with sharing class VT_R2_CalculateBackupPI {

    public void calculateBackupPISTM(List<Virtual_Site__c> recs) {
        List<Id> stmPIIdList = new List<Id>();
        Map<Id, Study_Team_Member__c> piIdSTMBackupPIMap = new Map<Id, Study_Team_Member__c>();

        //get list of new pis
        for (Virtual_Site__c site : recs) {
            stmPIIdList.add(site.VTD1_Study_Team_Member__c);
        }

        //get map PI_ID -> BackupPI_STM
        piIdSTMBackupPIMap = VT_D1_HelperClass.getBackupStmsForPIs(stmPIIdList);

        //assign BackupPI to site
        for (Virtual_Site__c site : recs) {
            if (piIdSTMBackupPIMap.containsKey(site.VTD1_Study_Team_Member__c)) {
                site.VTR2_Backup_PI_STM__c = piIdSTMBackupPIMap.get(site.VTD1_Study_Team_Member__c).Id;
            }
        }
    }

    public void calculateBackupPISTM(List<Virtual_Site__c> recs, Map<Id, Virtual_Site__c> oldMap) {
        List<Virtual_Site__c> sitesToProcess = new List<Virtual_Site__c>();

        // before update, if PI changed on site, recalculate BackupPI for him
        for (Virtual_Site__c site : recs) {
            Virtual_Site__c old = oldMap.get(site.Id);
            if (site.VTD1_Study_Team_Member__c != old.VTD1_Study_Team_Member__c) {
                sitesToProcess.add(site);
            }
        }
        if (!sitesToProcess.isEmpty()) {
            calculateBackupPISTM(sitesToProcess);
        }
    }
}