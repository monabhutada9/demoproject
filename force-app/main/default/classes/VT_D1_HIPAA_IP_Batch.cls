global class VT_D1_HIPAA_IP_Batch implements Database.Batchable<SObject>, Schedulable {

    // To schedule via apex hourly:
    // system.schedule('Fill HIPAA IP Addresses', '0 0 * * * ?',  new VT_D1_HIPAA_IP_Batch());

    global void execute(SchedulableContext sc) {
        Database.executeBatch(new VT_D1_HIPAA_IP_Batch());
    }

    global Iterable<SObject> start(Database.BatchableContext BC) {
        return (Iterable<SObject>) new VT_Stubber.ResultStub(
                'VT_D1_HIPAA_IP_Batch.getQueryLocator',
                Database.getQueryLocator([
                        SELECT Id, HIPAA_Accepted_Date_Time__c, HIPAA_Accepted_from_IP__c
                        FROM User WHERE HIPAA_Accepted_Date_Time__c != NULL
                        AND HIPAA_Accepted_from_IP__c = NULL
                ])
        ).getResult();
    }

    global void execute(Database.BatchableContext BC, List<User> scope) {
        Map<Id, User> userMap = new Map<Id, User>(scope);
        Integer numFound = 0;

        List<LoginHistory> lhList = (List<LoginHistory>) new VT_Stubber.ResultStub(
                'VT_D1_HIPAA_IP_Batch.LoginHistory',
                [
                    SELECT Id, SourceIp, LoginTime, UserId
                    FROM LoginHistory
                    WHERE UserId IN :scope
                    ORDER BY LoginTime DESC
                ]
        ).getResult();

        for (LoginHistory item : lhList) {
            if (userMap.get(item.UserId).HIPAA_Accepted_from_IP__c == null && item.LoginTime <= userMap.get(item.UserId).HIPAA_Accepted_Date_Time__c) {
                userMap.get(item.UserId).HIPAA_Accepted_from_IP__c = item.SourceIp;
                if (++numFound == scope.size()) { break ; }
            }
        }

        update scope;
    }

    global void finish(Database.BatchableContext BC) {

    }

}