/**
 * @author: Aliaksandr Vabishchevich
 * @date: 20-Aug-2020
 * @description: Test for VT_R5_SignantFilesProcessingBatch
 */

@IsTest
private without sharing class VT_R5_SignantFilesProcessingBatchTest {
    static String basePatientNum = 'consent-success';

    static String subsetSafety = 'SubjectSafety';
    static String subsetImmuno = 'SubjectImmuno';

    @TestSetup
    static void setup() {
        DomainObjects.Study_t study_t = new DomainObjects.Study_t()
                .setName('SignantStudyCov3009')
                .setOriginalName('SignantStudyCov3009')
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                        .setName('Janssen Test'))
                .setVTR5_IRTSystem('Signant')
                .setExternalSubjectIdUsed(true)
                .setExternalSubjectIdInputType('Manual')
                .setVTR5_WhenToCreate_eCoaSubject('Randomization')
                .setEcoaRuleSetCriteria('Subset Immuno/Safety')
                .setEdiaryTool('eCOA')
                .setEcoaGuid('study_000000000000000000000000000000000000');

        VTD2_Study_Geography__c geography = (VTD2_Study_Geography__c) new DomainObjects.VTD2_Study_Geography_t(study_t).persist();
        geography.VTR5_Primary_VTSL__c = UserInfo.getUserId();
        update geography;

        DomainObjects.StudyTeamMember_t stm = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('SCR')
                .addUser(new DomainObjects.User_t())
                .addStudy(study_t);

        DomainObjects.VirtualSite_t site_t = new DomainObjects.VirtualSite_t()
                .addStudyTeamMember(stm)
                .addStudy(study_t)
                .setStudySiteNumber('0')
                .setEcoaGuid('site_000000000000000000000000000000000000');
        site_t.persist();
        Test.startTest();
        new DomainObjects.VTR5_ePROClinRORuleset_t()
                .addStudy(study_t)
                .setVTR5_SubsetSafety(subsetSafety)
                .setVTR5_SubsetImmuno(subsetImmuno)
                .setVTR5_ClinRO_GUID('c-guid')
                .setVTR5_ClinRO_ScheduleName('c-name')
                .setVTR5_ePRO_GUID('e-guid')
                .setVTR5_ePRO_ScheduleName('e-name')
                .persist();

        DomainObjects.Contact_t con_t = new DomainObjects.Contact_t()
                .setFirstName(basePatientNum)
                .setLastName('VT_R5_SignantFilesProcessingBatchTest');

        DomainObjects.User_t user_t = new DomainObjects.User_t()
                .addContact(con_t)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        user_t.persist();

        Case c = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .addStudy(study_t)
                .addVirtualSite(site_t)
                .addVTD1_Patient_User(user_t)
                .addContact(con_t)
                .setStatus(VT_R4_ConstantsHelper_AccountContactCase.CASE_PRE_CONSENT)
                .persist();

        c.VTD2_Study_Geography__c = geography.Id;
        update c;

        con_t.setVTD1_Clinical_Study_Membership(c.Id);
        update con_t.toObject();

        DomainObjects.VTD2_TN_Catalog_Code_t tnCatalogCorrection = new DomainObjects.VTD2_TN_Catalog_Code_t()
                .setVTD2_T_Task_Unique_Code('N601')
                .setRecordTypeId(Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('CustomNotification').getRecordTypeId())
                .setVTD2_T_SObject_Name('Task')
                .setVTD2_T_Receiver('Task.OwnerId')
                .setVTD2_T_Message('VT_R5_SignantCorrectionNotifText')
                .setVTD2_T_Title('VT_R5_SignantFileCorrection');
        tnCatalogCorrection.persist();
        DomainObjects.VTD2_TN_Catalog_Code_t tnCatalogRescreening = new DomainObjects.VTD2_TN_Catalog_Code_t()
                .setVTD2_T_Task_Unique_Code('N602')
                .setRecordTypeId(Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('CustomNotification').getRecordTypeId())
                .setVTD2_T_SObject_Name('Task')
                .setVTD2_T_Receiver('Task.OwnerId')
                .setVTD2_T_Message('VT_R5_SignantFileRescreening')
                .setVTD2_T_Title('VT_R5_SignantFileRescreeningNotifMessage');
        tnCatalogRescreening.persist();

        new DomainObjects.VTR5_IRTConfiguration_t()
                .addStudy(study_t)
                .addValue('Jan009')
                .persist();
        Test.stopTest();
    }

    @IsTest
    static void testXmlFullHappyPath() {
        Case c = getCase(basePatientNum);
        System.assertNotEquals(c.VTR5_Internal_Subject_Id__c, null);
        System.assertEquals(c.VTD1_Subject_ID__c, null);
        System.assertEquals(c.VTR5_ConsentDate__c, null);

        String consDate = generateDate(10);
        String randDate = generateDate(20);
        String day1Dose = generateDate(30);
        String day57Dose = generateDate(87);

// insert 1st file (populate ConsDate, SubNum)
        VTR5_Signant_File__c sfScreening = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, basePatientNum, consDate)
                .toObject();
// insert 2nd file (populate SubjImmuno, SubjSafety, RandDate)
        VTR5_Signant_File__c sfRandomization = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, basePatientNum, consDate, subsetImmuno, subsetSafety, randDate, null,null)
                .toObject();
// insert 3rd file (populate Day1Dose)
        VTR5_Signant_File__c sfDay1Dose = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, basePatientNum, consDate, subsetImmuno, subsetSafety, randDate, day1Dose, null)
                .toObject();
        VTR5_Signant_File__c sfDay57Dose = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, basePatientNum, consDate, subsetImmuno, subsetSafety, randDate, day1Dose, day57Dose)
                .toObject();

        List<VTR5_Signant_File__c> filesToInsert = new List<VTR5_Signant_File__c>{
                sfScreening, sfRandomization, sfDay1Dose, sfDay57Dose
        };
        insert filesToInsert;

        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];  // to avoid mixed DML exception
        System.runAs(usr) {
            Test.startTest();

            Database.executeBatch(new VT_R5_SignantFilesProcessingBatch(), 200);
            Test.stopTest();
        }

        Map<Id, VTR5_Signant_File__c> processedSignantFiles = getSignantFiles(filesToInsert);

        sfScreening = processedSignantFiles.get(sfScreening.Id);
        System.assertEquals(sfScreening.VTR5_IsProcessed__c, true);
        System.assertEquals(sfScreening.VTR5_IsError__c, false);

        sfRandomization = processedSignantFiles.get(sfRandomization.Id);
        System.assertEquals(sfRandomization.VTR5_IsProcessed__c, true);
        System.assertEquals(sfRandomization.VTR5_IsError__c, false);

        sfDay1Dose = processedSignantFiles.get(sfDay1Dose.Id);
        System.assertEquals(sfDay1Dose.VTR5_IsProcessed__c, true);
        System.assertEquals(sfDay1Dose.VTR5_IsError__c, false);

        sfDay57Dose = processedSignantFiles.get(sfDay57Dose.Id);
        System.assertEquals(sfDay57Dose.VTR5_IsProcessed__c, true);
        System.assertEquals(sfDay57Dose.VTR5_IsError__c, false);

        c = getCase(c.Id);
        System.assertEquals(c.VTD1_Subject_ID__c, basePatientNum);
        System.assertEquals(c.Status, VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED);
        System.assertEquals(c.VTR5_ConsentDate__c, VT_R5_SignantFilesProcessingBatch.parseSignantDate(consDate));
        System.assertEquals(c.VTR5_SubsetImmuno__c, subsetImmuno);
        System.assertEquals(c.VTR5_SubsetSafety__c, subsetSafety);
        System.assertEquals(c.VTD1_Randomized_Date1__c, VT_R5_SignantFilesProcessingBatch.parseSignantDate(randDate));
        System.assertEquals(c.VTR5_Day_1_Dose__c, VT_R5_SignantFilesProcessingBatch.parseSignantDate(day1Dose));
        System.assertEquals(c.VTR5_Day_57_Dose__c, VT_R5_SignantFilesProcessingBatch.parseSignantDate(day57Dose));

        List<VTR5_ePROClinRORuleset__c> rulesets = [
                SELECT Id, VTR5_ePRO_GUID__c, VTR5_ePRO_ScheduleName__c, VTR5_ClinRO_GUID__c, VTR5_ClinRO_ScheduleName__c
                FROM VTR5_ePROClinRORuleset__c
                WHERE VTR5_SubsetImmuno__c = :subsetImmuno AND VTR5_SubsetSafety__c = :subsetSafety
                LIMIT 1
        ];

        User u = [
                SELECT Id, VTR5_Create_Subject_in_eCOA__c, VTR5_eCOA_Guid__c,
                        VTR5_ePRO_GUID__c, VTR5_ePRO_ScheduleName__c, VTR5_ClinRO_GUID__c, VTR5_ClinRO_ScheduleName__c, Profile.Name, Contact.FirstName,
                        Contact.VTD1_Clinical_Study_Membership__c
                FROM User
                WHERE Contact.FirstName = :basePatientNum
        ];
        System.assertEquals(rulesets.size(), 1);
        VTR5_ePROClinRORuleset__c r = rulesets[0];

//         System.assertEquals(true, u.VTR5_Create_Subject_in_eCOA__c);  // todo: investigate why fail!
        System.assertEquals(u.VTR5_ePRO_GUID__c, r.VTR5_ePRO_GUID__c);
        System.assertEquals(u.VTR5_ePRO_ScheduleName__c, r.VTR5_ePRO_ScheduleName__c);
        System.assertEquals(u.VTR5_ClinRO_GUID__c, r.VTR5_ClinRO_GUID__c);
        System.assertEquals(u.VTR5_ClinRO_ScheduleName__c, r.VTR5_ClinRO_ScheduleName__c);
    }

    @IsTest
    static void testXmlStructureValidationErrors() {
        Case c = getCase(basePatientNum);
        String xmlContent = '<subject><StudyHubSubjNum>test00000</StudyHubSubjNum>' +
                '<ScrnNum>1234567</ScrnNum><ConsDate></ConsDate>' +
                '<Day1Dose></Day1Dose><Day57Dose></Day57Dose>' +
                '<SubjImmuno></SubjImmuno><SubjSafety></SubjSafety>' +
                '<RandDate></RandDate> <IsThisARescreening>No</IsThisARescreening>' +
                '<PrevScrnNumber></PrevScrnNumber></subject>';
        Test.startTest();
// insert 1st file (bad datetime in file name)
        VTR5_Signant_File__c sfInvalidFileNameDate = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setName('Signant.' + basePatientNum + '.' + Datetime.now().format('MMYYYYdd\'T\'HHmmssSSSXX') + '.xml')
                .setXmlContent(xmlContent)
                .toObject();
// insert 2nd file (bad file extension)
        VTR5_Signant_File__c sfBadExtension = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setName('Signant.' + basePatientNum + '.' + Datetime.now().format('yyyy-MM-dd\'T\'HH.mm.ss.SSSXX') + '.pdf')
                .setXmlContent(xmlContent)
                .toObject();
// insert 3rd file (empty content)
        VTR5_Signant_File__c sfInvalidContent = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContent(null)
                .toObject();
// insert 4th file (wrong root tag)
        VTR5_Signant_File__c sfInvalidRoot = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContent('<subjects></subjects>')
                .toObject();
// insert 5th file (missing tags)
        VTR5_Signant_File__c sfMissingTags = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContent('<subject></subject>')
                .toObject();
// insert 6th file (missing required tag values)
        VTR5_Signant_File__c sfMissingValues = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, null, null)
                .toObject();
// insert 7th file (invalid date in tag)
        VTR5_Signant_File__c sfInvalidTagDate = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, basePatientNum, '08-bad-month-2020')
                .toObject();

        List<VTR5_Signant_File__c> filesToInsert = new List<VTR5_Signant_File__c>{
                sfInvalidFileNameDate, sfBadExtension, sfInvalidContent, sfInvalidRoot, sfMissingTags, sfMissingValues,
                sfInvalidTagDate
        };
        insert filesToInsert;

        Database.executeBatch(new VT_R5_SignantFilesProcessingBatch(), 200);
        Test.stopTest();

        Map<Id, VTR5_Signant_File__c> processedSignantFiles = getSignantFiles(filesToInsert);

        sfInvalidFileNameDate = processedSignantFiles.get(sfInvalidFileNameDate.Id);
        System.assertEquals(sfInvalidFileNameDate.VTR5_IsProcessed__c, true);
        System.assertEquals(sfInvalidFileNameDate.VTR5_IsError__c, true);
        System.assertEquals(sfInvalidFileNameDate.VTR5_ErrorType__c, 'Wrong Format');
        System.assert(sfInvalidFileNameDate.VTR5_ErrorDetails__c.startsWith('Wrong Date format in File'));

        sfBadExtension = processedSignantFiles.get(sfBadExtension.Id);
        System.assertEquals(sfBadExtension.VTR5_IsProcessed__c, true);
        System.assertEquals(sfBadExtension.VTR5_IsError__c, true);
        System.assertEquals(sfBadExtension.VTR5_ErrorType__c, 'Wrong Format');
        System.assert(sfBadExtension.VTR5_ErrorDetails__c.startsWith('Wrong File Name'));

        sfInvalidContent = processedSignantFiles.get(sfInvalidContent.Id);
        System.assertEquals(sfInvalidContent.VTR5_IsProcessed__c, true);
        System.assertEquals(sfInvalidContent.VTR5_IsError__c, true);
        System.assertEquals(sfInvalidContent.VTR5_ErrorType__c, 'Wrong Format');
        System.assert(sfInvalidContent.VTR5_ErrorDetails__c.startsWith('Wrong XML content'));

        sfInvalidRoot = processedSignantFiles.get(sfInvalidRoot.Id);
        System.assertEquals(sfInvalidRoot.VTR5_IsProcessed__c, true);
        System.assertEquals(sfInvalidRoot.VTR5_IsError__c, true);
        System.assertEquals(sfInvalidRoot.VTR5_ErrorType__c, 'Wrong Format');
        System.assert(sfInvalidRoot.VTR5_ErrorDetails__c.startsWith('Wrong XML content'));

        sfMissingTags = processedSignantFiles.get(sfMissingTags.Id);
        System.assertEquals(sfMissingTags.VTR5_IsProcessed__c, true);
        System.assertEquals(sfMissingTags.VTR5_IsError__c, true);
        System.assertEquals(sfMissingTags.VTR5_ErrorType__c, 'Wrong Format');
        System.assert(sfMissingTags.VTR5_ErrorDetails__c.startsWith('XML schema violation'));

        sfMissingValues = processedSignantFiles.get(sfMissingValues.Id);
        System.assertEquals(sfMissingValues.VTR5_IsProcessed__c, true);
        System.assertEquals(sfMissingValues.VTR5_IsError__c, true);
        System.assertEquals(sfMissingValues.VTR5_ErrorType__c, 'Wrong Format');
        System.assertEquals(sfMissingValues.VTR5_ErrorDetails__c, 'Missed required tag values (StudyHubSubjNum, ScrnNum or IsThisARescreening)');
    }
/*
    @IsTest
    static void testXmlCorrection() {
        Case c = getCase(basePatientNum);
        String oldConsDate = generateDate(10);
        String randDate = generateDate(20);
        String newConsDate = generateDate(30);

        Test.startTest();

// insert 0 file (initial setup)
        VTR5_Signant_File__c sfRandomization = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, basePatientNum, oldConsDate,
                        null, null, randDate, null,null)
                .toObject();

// insert 1st file (studyHubSubjNumCorrection = true)
        VTR5_Signant_File__c sfInvalidSubjNumCorrection = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, basePatientNum, null, true,
                        false, false, false)
                .toObject();

// insert 2nd file (day1DoseCorrection = true)
        VTR5_Signant_File__c sfInvalidDay1DoseCorrection = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, basePatientNum, null,
                        false, false, true,false)
                .toObject();

// insert 3rd file (consDateCorrection = true)
        VTR5_Signant_File__c sfConsDateCorrection = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, basePatientNum, newConsDate,
                        false, true, false, false)
                .toObject();

 // insert 4th file (day57DoseCorrection = true)
        VTR5_Signant_File__c sfInvalidDay57DoseCorrection = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, basePatientNum, null,
                        false, false, false,true)
                .toObject();

        List<VTR5_Signant_File__c> filesToInsert = new List<VTR5_Signant_File__c>{
                sfRandomization, sfInvalidSubjNumCorrection, sfInvalidDay1DoseCorrection,
                sfConsDateCorrection,sfInvalidDay57DoseCorrection
        };
        insert filesToInsert;

        Database.executeBatch(new VT_R5_SignantFilesProcessingBatch(), 200);
        Test.stopTest();

        Map<Id, VTR5_Signant_File__c> processedSignantFiles = getSignantFiles(filesToInsert);

//        sfInvalidSubjNumCorrection = processedSignantFiles.get(sfInvalidSubjNumCorrection.Id);
//        System.assertEquals(sfInvalidSubjNumCorrection.VTR5_IsProcessed__c, true);
//        System.assertEquals(sfInvalidSubjNumCorrection.VTR5_IsError__c, true);
//        System.assertEquals(sfInvalidSubjNumCorrection.VTR5_ErrorType__c, 'Manual Update Required');
//        System.assertEquals(sfInvalidSubjNumCorrection.VTR5_ErrorDetails__c, 'SH Internal Id can be corrected only manually');

        sfInvalidDay1DoseCorrection = processedSignantFiles.get(sfInvalidDay1DoseCorrection.Id);
        System.assertEquals(sfInvalidDay1DoseCorrection.VTR5_IsProcessed__c, true);
        System.assertEquals(sfInvalidDay1DoseCorrection.VTR5_IsError__c, true);
        System.assertEquals(sfInvalidDay1DoseCorrection.VTR5_ErrorType__c, 'Manual Update Required');
        System.assertEquals(sfInvalidDay1DoseCorrection.VTR5_ErrorDetails__c, 'Day1Dose can be corrected only manually');

        sfInvalidDay57DoseCorrection = processedSignantFiles.get(sfInvalidDay57DoseCorrection.Id);
        System.assertEquals(sfInvalidDay57DoseCorrection.VTR5_IsProcessed__c, true);
        System.assertEquals(sfInvalidDay57DoseCorrection.VTR5_IsError__c, true);
        System.assertEquals(sfInvalidDay57DoseCorrection.VTR5_ErrorType__c, 'Manual Update Required');
        System.assertEquals(sfInvalidDay57DoseCorrection.VTR5_ErrorDetails__c, 'Day57Dose can be corrected only manually');

        sfConsDateCorrection = processedSignantFiles.get(sfConsDateCorrection.Id);
        System.assertEquals(sfConsDateCorrection.VTR5_IsProcessed__c, true);
        System.assertEquals(sfConsDateCorrection.VTR5_IsError__c, false);

        c = getCase(c.Id);
        System.assertEquals(c.Status, VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED);
        System.assertEquals(c.VTD1_Subject_ID__c, basePatientNum);
        System.assertEquals(c.VTR5_ConsentDate__c, VT_R5_SignantFilesProcessingBatch.parseSignantDate(newConsDate));
    }
 */

    @IsTest
    static void testXmlRescreening() {
        Case c = getCase(basePatientNum);
        String oldConsDate = generateDate(10);
        String newConsDate = generateDate(20);
        String randDate = generateDate(30);
        String newPatientNum = 'rescreening-success-path';

        Test.startTest();
// insert 0 file (initial setup)
        VTR5_Signant_File__c sfScreening = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, basePatientNum, oldConsDate)
                .toObject();

// insert 1st file (prevScreenNumber = null)
        VTR5_Signant_File__c sfBlankPrevScrnNumb = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(newPatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, newPatientNum, null, true, null)
                .toObject();

// insert 2nd file (prevScreenNumber != Case.SubjectId)
        VTR5_Signant_File__c sfMismatchPrevScrnNumb = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(newPatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, newPatientNum, newConsDate, true, 'scrn-num-invalid')
                .toObject();

// insert 3rd file (successful rescreening)
        VTR5_Signant_File__c sfPositiveRescreening = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(newPatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, newPatientNum, newConsDate, true, basePatientNum)
                .toObject();

// insert 4th file (successful randomization)
        VTR5_Signant_File__c sfPositiveRandomization = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(newPatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, newPatientNum, newConsDate, null, null, randDate, null, null)
                .toObject();

// insert 5th file (rescreening in Active/Randomized)
        VTR5_Signant_File__c sfInvalidRescreening = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(newPatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, newPatientNum, newConsDate, true, newPatientNum)
                .toObject();

        List<VTR5_Signant_File__c> filesToInsert = new List<VTR5_Signant_File__c>{
                sfScreening, sfBlankPrevScrnNumb, sfMismatchPrevScrnNumb, sfPositiveRescreening, sfPositiveRandomization, sfInvalidRescreening
        };
        insert filesToInsert;

        Database.executeBatch(new VT_R5_SignantFilesProcessingBatch(), 200);
        Test.stopTest();

        Map<Id, VTR5_Signant_File__c> processedSignantFiles = getSignantFiles(filesToInsert);
        /*
        sfBlankPrevScrnNumb = processedSignantFiles.get(sfBlankPrevScrnNumb.Id);
        System.assertEquals(sfBlankPrevScrnNumb.VTR5_IsProcessed__c, true);
        System.assertEquals(sfBlankPrevScrnNumb.VTR5_IsError__c, true);
        System.assertEquals(sfBlankPrevScrnNumb.VTR5_ErrorType__c, 'Wrong Format');
        System.assertEquals(sfBlankPrevScrnNumb.VTR5_ErrorDetails__c, 'PrevScrnNumber on SignantFile is blank');

        sfMismatchPrevScrnNumb = processedSignantFiles.get(sfMismatchPrevScrnNumb.Id);
        System.assertEquals(sfMismatchPrevScrnNumb.VTR5_IsProcessed__c, true);
        System.assertEquals(sfMismatchPrevScrnNumb.VTR5_IsError__c, true);
        System.assertEquals(sfMismatchPrevScrnNumb.VTR5_ErrorType__c, 'Duplicate');
        System.assert(sfMismatchPrevScrnNumb.VTR5_ErrorDetails__c.contains('is different from SubjectId on Case'));
         */

        sfPositiveRescreening = processedSignantFiles.get(sfPositiveRescreening.Id);
        System.assertEquals(sfPositiveRescreening.VTR5_IsProcessed__c, true);
        System.assertEquals(sfPositiveRescreening.VTR5_IsError__c, false);

        sfPositiveRandomization = processedSignantFiles.get(sfPositiveRandomization.Id);
        System.assertEquals(sfPositiveRandomization.VTR5_IsProcessed__c, true);
        System.assertEquals(sfPositiveRandomization.VTR5_IsError__c, false);
        /*
        sfInvalidRescreening = processedSignantFiles.get(sfInvalidRescreening.Id);
        System.assertEquals(sfInvalidRescreening.VTR5_IsProcessed__c, true);
        System.assertEquals(sfInvalidRescreening.VTR5_IsError__c, true);
        System.assertEquals(sfInvalidRescreening.VTR5_ErrorType__c, 'Exception');
        System.assertEquals(sfInvalidRescreening.VTR5_ErrorDetails__c, 'Rescreening XML is not allowed in Active/Randomized status');
         */

        c = getCase(c.Id);
        System.assertEquals(c.Status, VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED);
        System.assertEquals(c.VTD1_Subject_ID__c, newPatientNum);
        System.assertEquals(c.VTR5_ConsentDate__c, VT_R5_SignantFilesProcessingBatch.parseSignantDate(newConsDate));
        System.assertEquals(c.VTD1_Randomized_Date1__c, VT_R5_SignantFilesProcessingBatch.parseSignantDate(randDate));
    }

    @IsTest
    static void testSignantFileTrigger() {
        VTR5_Signant_File__c sf = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .persist();

        String comment = 'comment from test';
        Test.startTest();
        sf.VTR5_Comment__c = comment;
        update sf;
        Test.stopTest();
        System.debug('sft: sf1: ' + sf);

        sf = [
                SELECT VTR5_Comment__c, VTR5_CommentBy__c, VTR5_CommentDate__c
                FROM VTR5_Signant_File__c
                WHERE Id = :sf.Id
        ];
        System.debug('sft: sf2: ' + sf);
        System.assertEquals(sf.VTR5_Comment__c, comment);
        System.assertEquals(sf.VTR5_CommentBy__c, UserInfo.getUserId());
        System.assertNotEquals(sf.VTR5_CommentDate__c, null);
    }

    @IsTest
    static void testSignantFilesCopyBatch() {
        String subjectId = 'Jan009-test22-999';
        Date currentDate = Date.today();
        Case c = getCase(basePatientNum);
        c.VTD1_Subject_ID__c = subjectId;
        c.VTR5_ConsentDate__c = currentDate;
        update c;
        VTR5_Signant_File__c sf = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setCase(c.Id)
                .setConsDate(currentDate)
                .setSubjectId(subjectId)
                .persist();
        Test.startTest();
        Database.executeBatch(new VT_R5_CreateSignantFilesCopyBatch(), 200);
        Test.stopTest();
    }

    @IsTest
    static void generateTaskAndNotifCorrectionTest() {
        Case c = getCase(basePatientNum);
        Date doseDay = Date.today();
        String dateForSignantFile = '28-Nov-2021';
        c.VTR5_Day_57_Dose__c = doseDay;
        c.VTR5_Day_1_Dose__c = doseDay;
        c.VTR5_ConsentDate__c = doseDay;
        update c;
        List<VTR5_Signant_File__c> signantFiles = new List<VTR5_Signant_File__c>();
        VTR5_Signant_File__c sfStudyHubCorrection = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, basePatientNum, dateForSignantFile, 'subim',
                        'subsef', dateForSignantFile, dateForSignantFile, dateForSignantFile,false,
                        null, true, false, false, false)
                .toObject();
        VTR5_Signant_File__c sfDay1DoseCorrection = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, basePatientNum, null, null,
                        null, null, dateForSignantFile, null,false,
                        null, false, false, true, false)
                .toObject();
        VTR5_Signant_File__c sfDay57DoseCorrection = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, basePatientNum, null, null,
                        null, null, null, dateForSignantFile,false,
                        null, false, false, false, true)
                .toObject();
        VTR5_Signant_File__c sfConsentDateCorrection = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, basePatientNum, dateForSignantFile, null,
                        null, null, null, null,false,
                        null, false, true, false, false)
                .toObject();
        VTR5_Signant_File__c sfStudyHubRescreening = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr(basePatientNum)
                .setXmlContentByAttrs(c.VTR5_Internal_Subject_Id__c, basePatientNum, dateForSignantFile, null,
                        null, null, null, null,true,
                        basePatientNum, false, false, false, false)
                .toObject();
        signantFiles = new List<VTR5_Signant_File__c>{sfStudyHubCorrection, sfDay1DoseCorrection, sfDay57DoseCorrection,
                sfConsentDateCorrection, sfStudyHubRescreening};

        insert signantFiles;
        Test.startTest();
        Database.executeBatch(new VT_R5_SignantFilesProcessingBatch(), 200);
        Test.stopTest();

        List<Task> tasks = [SELECT Id, OwnerId, Description, Subject FROM Task];
        System.assertEquals(5 , tasks.size(), 'Should be created 5 tasks for each signant file.');
    }


// HELPERS
    /**
     * @description Generate date strings (+N days from today) in accepted by Signant format
     * @param daysAdded - days in future
     * @return - date string in format 01-Aug-2020
     */
    static String generateDate(Integer daysAdded) {
        return Datetime.now().addDays(daysAdded).format('dd-MMM-YYYY');
    }

    private static Map<Id, VTR5_Signant_File__c> getSignantFiles(List<VTR5_Signant_File__c> signantFiles) {
        Set<Id> fileIds = new Set<Id>();
        for (VTR5_Signant_File__c sf : signantFiles) {
            fileIds.add(sf.Id);
        }
        return new Map<Id, VTR5_Signant_File__c>([
                SELECT Id,
                        Name,
                        VTR5_XMLContent__c,
                        VTR5_XMLDate__c,
                        VTR5_IsProcessed__c,
                        VTR5_IsError__c,
                        VTR5_ErrorType__c,
                        VTR5_ErrorDetails__c,
                        VTR5_IsRescreeningXML__c,
                        VTR5_IsCorrectionXML__c
                FROM VTR5_Signant_File__c
                WHERE Id = :fileIds
        ]);
    }

    private static Case getCase(String firstName) {
        return [
                SELECT Id, Status, VTR5_Internal_Subject_Id__c, VTD1_Subject_ID__c, VTR5_ConsentDate__c,
                        VTD1_Randomized_Date1__c, VTR5_SubsetImmuno__c, VTR5_SubsetSafety__c, VTR5_Day_1_Dose__c,
                        VTR5_Last_Signant_File_Date__c, VTR5_First_Signant_File_Date__c,VTR5_Day_57_Dose__c,VTD1_Patient_User__r.VTR5_eCOA_Guid__c,
                        VTD2_Study_Geography__r.VTR5_Primary_VTSL__c, VTD2_Study_Geography__c
                FROM Case
                WHERE Contact.FirstName = :firstName
        ];
    }

    private static Case getCase(Id caseId) {
        return [
                SELECT Id, Status, VTR5_Internal_Subject_Id__c, VTD1_Subject_ID__c, VTR5_ConsentDate__c,
                        VTD1_Randomized_Date1__c, VTR5_SubsetImmuno__c, VTR5_SubsetSafety__c, VTR5_Day_1_Dose__c,
                        VTR5_Last_Signant_File_Date__c, VTR5_First_Signant_File_Date__c,VTR5_Day_57_Dose__c,VTD1_Patient_User__r.VTR5_eCOA_Guid__c
                FROM Case
                WHERE Id = :caseId
        ];
    }
}