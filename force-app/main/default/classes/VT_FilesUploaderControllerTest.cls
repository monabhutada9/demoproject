@isTest
public with sharing class VT_FilesUploaderControllerTest {

    public static void deleteFilesIsAllowed() {
        Test.startTest();
            emulateUserUploadBehavior();
            List<ContentDocument> contentDocuments = [SELECT Id FROM ContentDocument LIMIT 100];
            System.assertEquals(1, contentDocuments.size());
            System.assertEquals(1, [SELECT Count() FROM ContentDocumentLink WHERE LinkedEntityId = :UserInfo.getUserId()]);
        VT_FilesUploaderController.deleteFiles(new List<Id>{contentDocuments.get(0).Id});
            System.assertEquals(0, [SELECT Count() FROM ContentDocument]);
            System.assertEquals(0, [SELECT Count() FROM ContentDocumentLink WHERE LinkedEntityId = :UserInfo.getUserId()]);
        Test.stopTest();
    }

    private static void emulateUserUploadBehavior() {
        ContentVersion contentVersion = createNewTestContentVersion();
        insert contentVersion;
    }
    private static ContentVersion createNewTestContentVersion() {
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.PathOnClient = 'test.txt';
        contentVersion.Title = 'Test file';
        contentVersion.VersionData = Blob.valueOf('Test Data');
        return contentVersion;
    }
}