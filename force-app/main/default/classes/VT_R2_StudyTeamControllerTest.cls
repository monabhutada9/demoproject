@IsTest
private class VT_R2_StudyTeamControllerTest {
    @IsTest
    static void testGettingStudyTeamMembers() {
        User user = (User) new DomainObjects.User_t().persist();
        System.runAs(user) {
            DomainObjects.Study_t study = new DomainObjects.Study_t()
                    .setVTD1_Project_Lead(user.Id)
                    .setOriginalName('Any other');
            new DomainObjects.StudyTeamMember_t()
                    .addStudy(study)
                    .setUserId(user.Id)
                    .setRecordTypeByName('PI')
                    .persist();
            new DomainObjects.StudyTeamMember_t()
                    .addStudy(study)
                    .setUserId(user.Id)
                    .persist();
            Test.startTest();
            VT_R2_StudyTeamController.getStudyTeamMembers('PI', 'All My Studies');
            VT_R2_StudyTeamController.getStudyTeamMembers('PI', 'Any other');
            VT_R2_StudyTeamController.getStudyTeamMembers('PL', 'All My Studies');
            VT_R2_StudyTeamController.getStudyTeamMembers('PL', 'Any other');
            Test.stopTest();
        }
    }
    @IsTest
    static void getStudyTeamMembersBySiteTest() {
        Contact scrContact = (Contact) new DomainObjects.Contact_t().persist();
        Contact piContact = (Contact) new DomainObjects.Contact_t().persist();
        User userSCR = (User) new DomainObjects.User_t()
                .setContact(scrContact.Id)
                .setProfile('Site Coordinator').persist();
        User userPI = (User) new DomainObjects.User_t()
                .setContact(piContact.Id)
                .setProfile('Primary Investigator').persist();
        User userPG = (User) new DomainObjects.User_t()
                .setProfile('Patient Guide').persist();
        System.runAs(userPI) {
            new DomainObjects.StudyTeamMember_t()
                    .addStudy(
                        new DomainObjects.Study_t()
                        .setVTD1_Project_Lead(userPI.Id)
                        .setOriginalName('PI')
                    )
                    .setUserId(userPI.Id)
                    .setRecordTypeByName('PI')
                    .persist();
            System.assertEquals(0, VT_R2_StudyTeamController.getStudyTeamMembersBySite('PI', 'PI').size());
        }
        System.runAs(userSCR) {
            new DomainObjects.StudyTeamMember_t()
                    .addStudy(
                        new DomainObjects.Study_t()
                        .setVTD1_Project_Lead(userSCR.Id)
                        .setOriginalName('SCR')
                    )
                    .setUserId(userSCR.Id)
                    .setRecordTypeByName('SCR')
                    .persist();
            System.assertEquals(0, VT_R2_StudyTeamController.getStudyTeamMembersBySite('SCR', 'SCR').size());
        }
       System.runAs(userPG) {
           System.assertEquals(0, VT_R2_StudyTeamController.getStudyTeamMembersBySite('PG', 'PG').size());
        }
    }
}