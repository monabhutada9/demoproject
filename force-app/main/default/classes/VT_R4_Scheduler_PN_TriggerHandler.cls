/**
 * Created by Denis Belkovskii on 4/27/2020.
 */
public class VT_R4_Scheduler_PN_TriggerHandler { 
    public static void sendPushNotificationInfo(List<VTR4_Scheduler_PushNotifications__c> schPushNotifications,
            Map<Id, VTR4_Scheduler_PushNotifications__c> schPushNotificationsOldMap){
        if(VTR4_ExternalScheduler__c.getInstance().VTR4_PushNotificationIsActive__c) {
            Set<Id> videoConferenceIds = new Set<Id>();
            for (VTR4_Scheduler_PushNotifications__c schPushNtf : schPushNotifications) {
                if (schPushNtf.VTR4_ReadyToStart__c && (schPushNotificationsOldMap == null || !schPushNotificationsOldMap.get(schPushNtf.Id).VTR4_ReadyToStart__c)) {
                    videoConferenceIds.add(schPushNtf.VTR4_Video_Conference__c);
                }
            }
            if (!videoConferenceIds.isEmpty()) {
                VT_R3_InvocablePush.createPushNotifications(new List<Id>(videoConferenceIds));
                VTD1_Integration_Log__c triggerLog = new VTD1_Integration_Log__c(
                        VTD1_Body_Response__c = String.valueOf(videoConferenceIds.size()),
                        VTD1_Action__c = 'Push notifications'
                );
                insert triggerLog;
            }
        }
    }
}