/**
 * Created by User on 19/05/16.
 */
@isTest
public with sharing class VT_D1_scheduler_01Test {

    public static void doTest(){
        Test.startTest();
        VT_D1_scheduler_01 scheduler01 = new VT_D1_scheduler_01 ();
        String chron = '0 0 23 * * ?';
        System.schedule('Test Sched', chron, scheduler01);
        Test.stopTest();
    }
}