/**
 * Created by User on 19/05/17.
 */
@isTest
public with sharing class VT_R2_SCRPatientVisitDetailTest {
//    

    public static void cancelVisitRemoteTest(){
        VTD1_Actual_Visit__c actualVisit = [SELECT Id FROM VTD1_Actual_Visit__c];
        VT_R2_SCRPatientVisitDetail.cancelVisitRemote(actualVisit.Id,'reason');
        actualVisit = [SELECT Id, VTD1_Reason_for_Cancellation__c FROM VTD1_Actual_Visit__c WHERE Id = :actualVisit.Id];
        System.assertEquals('reason', actualVisit.VTD1_Reason_for_Cancellation__c);
        delete actualVisit;
        try{
            VT_R2_SCRPatientVisitDetail.cancelVisitRemote(actualVisit.Id,'reason');
        }catch(DmlException e){
            System.assertNotEquals(null, e);
        }
    }
    public static void getVisitDataTest(){
        Test.startTest();
        VTD1_Actual_Visit__c actualVisit = [SELECT Id FROM VTD1_Actual_Visit__c];
        Case currentCase = [SELECT Id FROM Case LIMIT 1];
        Map<String, Object> result = VT_R2_SCRPatientVisitDetail.getVisitData(actualVisit.Id,currentCase.Id);
        System.assert(result.containsKey('availableDates'));
        Datetime scheduledDateTime = Datetime.now().addMinutes(10);
        actualVisit.VTD1_Scheduled_Date_Time__c = scheduledDateTime;
        VT_R2_SCRPatientVisitDetail.saveVisitData(actualVisit);
        actualVisit = [SELECT Id, VTD1_Scheduled_Date_Time__c FROM VTD1_Actual_Visit__c WHERE Id = :actualVisit.Id];
        System.assertEquals(scheduledDateTime, actualVisit.VTD1_Scheduled_Date_Time__c);
        VT_R2_SCRPatientVisitDetail.cancelVisit(actualVisit.Id, Label.VTR2_ChangeStatusToCanceled, 'removeReason');
        actualVisit = [SELECT Id, VTD1_Scheduled_Date_Time__c,VTR2_ReasonToRemove__c FROM VTD1_Actual_Visit__c WHERE Id = :actualVisit.Id];
        Test.stopTest();
        System.assertEquals('removeReason', actualVisit.VTR2_ReasonToRemove__c);
    }

    /*********************************************************************************************
    * @description  This test method cover the scenario if study is baseline type and protocol is 
    *               V1 type and user changing the completion date from today without any reason then 
    *               give error
    * @group        SH-13732
    * @param        NA
    * @return       void
    */
    public static void reasonOfBackdatingErrorTest() {
  
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id, 
                                                           name, 
                                                           VTR5_Baseline_Visit_After_A_R_Flag__c 
                                                           FROM HealthCloudGA__CarePlanTemplate__c 
                                                           LIMIT 1]; 
        
        // for baseline type study
        study.VTR5_Baseline_Visit_After_A_R_Flag__c = true;
        update study;

        Case studyCase = [SELECT Id, 
                                 VTD1_Study__c 
                                 FROM Case 
                                 WHERE VTD1_Study__c =: study.Id 
                                 LIMIT 1];

        VTD1_ProtocolVisit__c pVisit = (VTD1_ProtocolVisit__c) new DomainObjects.VTD1_ProtocolVisit_t()
            .setVTR2_Visit_Participants('PI; Patient Guide; Patient')
            .setStudy(study.Id)
            .setVisitNumber('v1')
            .setRecordTypeByName('Protocol')
            .persist();
        
        VTD1_Actual_Visit__c actualVisit = [SELECT Id, 
                                                   VTD1_Case__c, 
                                                   VTD1_Visit_Completion_Date__c, 
                                                   VTD1_Status__c, 
                                                   VTR5_Reason_Of_Backdating__c,
                                                   VTD1_Case__r.VTR5_Baseline_Visit_After_A_R_Flag__c, 
                                                   VTD1_Visit_Number__c, 
                                                   VTD1_Protocol_Visit__r.RecordType.DeveloperName 
                                                   FROM VTD1_Actual_Visit__c 
                                                   LIMIT 1];

        // update the actual visit for protocol visit and case
        actualVisit.VTD1_Protocol_Visit__c = pVisit.Id;
        actualVisit.VTD1_Case__c = studyCase.Id;
        actualVisit.VTD1_Status__c = 'Completed';

        Test.startTest();
            update actualVisit;

            // now if change the completeion date from today without reason then give error
            actualVisit.VTD1_Visit_Completion_Date__c = System.today().addDays(-1);
            
            String errorMessage;
            try  {
                update actualVisit;
            } catch (Exception e) {
                errorMessage = e.getMessage();
            }
        Test.stopTest();
        System.assert(errorMessage != null, 'please Complete reason of backdating field');

    }

    
    /************************************************************************************************
    * @description  This test method cover the scenario if study is not baseline type and not protocol 
    *               type is V1 type and user changing the completion date from today
    * @group        SH-13732
    * @param        NA
    * @return       void
    */
    public static void reasonOfBackdatingWithoutErrorTest() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id, 
                                                           name, 
                                                           VTR5_Baseline_Visit_After_A_R_Flag__c 
                                                           FROM HealthCloudGA__CarePlanTemplate__c 
                                                           LIMIT 1]; 
        
        Case studyCase = [SELECT Id, 
                                 VTD1_Study__c 
                                 FROM Case 
                                 WHERE VTD1_Study__c =: study.Id 
                                 LIMIT 1];

        VTD1_ProtocolVisit__c pVisit = (VTD1_ProtocolVisit__c) new DomainObjects.VTD1_ProtocolVisit_t()
            .setVTR2_Visit_Participants('PI; Patient Guide; Patient')
            .setStudy(study.Id)
            .setVisitNumber('v1')
            .setRecordTypeByName('Protocol')
            .persist();
        
        VTD1_Actual_Visit__c actualVisit = [SELECT Id, 
                                                   VTD1_Case__c, 
                                                   VTD1_Visit_Completion_Date__c, 
                                                   VTD1_Status__c, 
                                                   VTR5_Reason_Of_Backdating__c, 
                                                   VTD1_Case__r.VTR5_Baseline_Visit_After_A_R_Flag__c, 
                                                   VTD1_Visit_Number__c, 
                                                   VTD1_Protocol_Visit__r.RecordType.DeveloperName 
                                                   FROM VTD1_Actual_Visit__c 
                                                   LIMIT 1];
                                                   
        actualVisit.VTD1_Protocol_Visit__c = pVisit.Id;
        actualVisit.VTD1_Case__c = studyCase.Id;
        update actualVisit;

        // As this study is not special study so backdate from today will not give error
        actualVisit.VTD1_Status__c = 'Completed';
        actualVisit.VTD1_Visit_Completion_Date__c = System.today().addDays(-1);
        
        String errorMessage;
        try  {
            update actualVisit;
        } catch (Exception e) {
            errorMessage = e.getMessage();
        }

        Test.stopTest();

        System.assert(errorMessage == null, 'There is error in updating completion date field');
        System.assertEquals(System.today().addDays(-1), actualVisit.VTD1_Visit_Completion_Date__c, 'Cmpletion Date field has not updated');

    }
}