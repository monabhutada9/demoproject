/**
 * Created by dmitry on 10.08.2020.
 */

@RestResource(UrlMapping='/heroku-scheduler')
global without sharing class VT_R5_HerokuScheduler {

    private static final String REQUEST_BODY_PLACEHOLDER = 'Request body too large. See attachments';
    private static final String STATUS_SUCCESS = 'SUCCESS';
    private static final String STATUS_FAILED = 'FAILED';

    private class ResponseBody {
        private String status { get; private set; }
        private String message { get; private set; }

        private ResponseBody(String status, String message) {
            this.status = status;
            this.message = message;
        }
        private ResponseBody(String status) {
            this.status = status;
        }
    }

    public class HerokuSchedulerPayload {
        public final String orgUrl = Url.getOrgDomainUrl().toExternalForm();
        public final Id orgId = UserInfo.getOrganizationId();
        public Datetime runDatetime { get; private set; }
        public String handlerClassName { get; private set; }
        public String payload { get; private set; }
        public Boolean separateRequest { get; private set; }

        public HerokuSchedulerPayload(Datetime runDatetime, String handlerClassName, String payload) {
            this.runDatetime = runDatetime;
            this.handlerClassName = handlerClassName;
            this.payload = payload;
            this.separateRequest = false;
        }
    }

    public interface HerokuExecutable {
        void herokuExecute(String payload);
    }

    public abstract inherited sharing class AbstractScheduler {
        public void schedule(List<HerokuSchedulerPayload> schedulerPayloadList) {
            List<HerokuSchedulerPayload> listToSchedule = new List<HerokuSchedulerPayload>();
            for(HerokuSchedulerPayload schedulerPayload : schedulerPayloadList) {
                if (schedulerPayload.runDatetime != null) {
                    listToSchedule.add(schedulerPayload);
                }
            }

            if (listToSchedule.isEmpty()) { return; }

            HttpRequest req;
            HttpResponse res;
            String errorMessage;
            Datetime requestDT = Datetime.now();
            try {
                req = getScheduleRequest(listToSchedule);
                res = new Http().send(req);
                if (res.getStatusCode() >= 400) {
                    Map<String, Object> responseBodyMap = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
                    errorMessage = String.valueOf(responseBodyMap.get('message'));
                }
            } catch (Exception e) {
                errorMessage = e.getMessage();
            } finally {
                logOutboundRequest(req, res, requestDT, errorMessage);
            }
        }
    }

    @HttpPost
    global static void doPost() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        String errorMessage;
        try {
            List<HerokuSchedulerPayload> schedulerPayloadList = (List<HerokuSchedulerPayload>) JSON.deserialize(request.requestBody.toString(), List<HerokuSchedulerPayload>.class);
            for (HerokuSchedulerPayload schedulerPayload : schedulerPayloadList) {
                HerokuExecutable handlerClass = (HerokuExecutable) Type.forName(schedulerPayload.handlerClassName).newInstance();
                handlerClass.herokuExecute(schedulerPayload.payload);
            }
            response.responseBody = Blob.valueOf(JSON.serialize(new ResponseBody(STATUS_SUCCESS), true));
            response.statusCode = 200;
        } catch (Exception e) {
            errorMessage = e.getMessage() + ' ' + e.getStackTraceString() + ' ' + e.getLineNumber();
            response.responseBody = Blob.valueOf(JSON.serialize(new ResponseBody(STATUS_FAILED, errorMessage)));
            response.statusCode = 500;
        } finally {
            logInboundRequest(request, response, errorMessage);
        }
    }

    private static HttpRequest getScheduleRequest(List<HerokuSchedulerPayload> listToSchedule) {
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Authorization', 'Basic ' + VTR5_HerokuSchedulerSettings__c.getInstance().VTR5_AccessKey__c);
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(VTR5_HerokuSchedulerSettings__c.getInstance().VTR5_URL__c + '/schedule');
        req.setBody(JSON.serialize(listToSchedule));
        req.setTimeout(120000);
        return req;
    }

    private static void logOutboundRequest(HttpRequest req, HttpResponse res, Datetime startDT, String errorMessage) {
        if (String.isEmpty(errorMessage) && !VTR5_HerokuSchedulerSettings__c.getInstance().VTR5_LogSuccessfulRequestsOutbound__c) { return; }

        Datetime responseDT = Datetime.now();
        VTD1_Integration_Log__c log = new VTD1_Integration_Log__c(
                VTD1_Action__c = 'schedule',
                VTD1_Timestamp_Request__c = startDT.formatGmt(VT_D1_HTTPConnectionHandler.INTEGRATION_TIMESTAMP_FORMAT),
                VTD1_Timestamp_Response__c = responseDT.formatGmt(VT_D1_HTTPConnectionHandler.INTEGRATION_TIMESTAMP_FORMAT),
                VTD1_Call_Duration__c = (responseDT.getTime() - startDT.getTime()) / 1000
        );
        if (req != null) {
            log.VTD1_REST_Method__c = req.getMethod();
            log.VTD1_EndPoint_URL__c = req.getEndpoint();
            log.VTD1_Timestamp_Request__c = req.getHeader('Date');
            log.VTD1_Body_Request__c = req.getBody().mid(0, 32768);
            log.VTD1_Body_Request_Length__c = req.getBody().length();
        }
        if (res != null) {
            log.VTD1_Status_Code_Response__c = res.getStatusCode();
            log.VTD1_Status_Response__c = res.getStatus();
            log.VTD1_Body_Response__c = res.getBody();
            log.VTD1_Body_Response_Length__c = res.getBody().length();
        }
        if (!String.isEmpty(errorMessage)) {
            log.VTD1_ErrorMessage__c = errorMessage;
            log.VTD1_Request_Timeout_Encountered__c = errorMessage.equalsIgnoreCase('Read timed out') ? '1' : '0';
        }
        insert log;
    }

    private static void logInboundRequest(RestRequest request, RestResponse response, String errorMessage) {
        if (String.isEmpty(errorMessage) && !VTR5_HerokuSchedulerSettings__c.getInstance().VTR5_LogSuccessfulRequestsInbound__c) { return; }

        VTD1_IntegrationInbound_Log__c log;
        Attachment logAttachment;

        if (request.requestBody.size() > 32768) { //VTD1_IntegrationInbound_Log__c.VTD1_Body_Response__c field length limit
            logAttachment = new Attachment(
                    Body = request.requestBody,
                    Name = 'body ' + Datetime.now()
            );
        }

        log = VT_D1_HTTPConnectionHandler.createIntegrationInboundLogRecord(
                logAttachment == null ? request.requestBody.toString() : REQUEST_BODY_PLACEHOLDER,
                response.responseBody.toString(),
                VT_R5_HerokuScheduler.class.toString().substringAfterLast('_'),
                Datetime.now(),
                errorMessage
        );
        insert log;

        if (logAttachment != null) {
            logAttachment.ParentId = log.Id;
            insert logAttachment;
        }
    }

}