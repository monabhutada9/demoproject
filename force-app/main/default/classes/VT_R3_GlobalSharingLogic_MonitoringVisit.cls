/**
* @author: Carl Judge
* @date: 23-Sep-19
**/
public without sharing class VT_R3_GlobalSharingLogic_MonitoringVisit extends VT_R3_AbstractGlobalSharingLogic {
    public static final Set<String> PROFILES_TO_SHARE_WITH = new Set<String>{
        VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.CM_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.MRR_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.PL_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.VTSL_PROFILE_NAME,
        VT_R4_ConstantsHelper_ProfilesSTM.AUDITOR_PROFILE_NAME // Added to enable sharing for Auditor Profile, Epic# SH-8126
    };

    public static final Set<String> SITE_LEVEL_PROFILES = new Set<String>{
        VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME
    };
    
    public override Type getType() {
        return VT_R3_GlobalSharingLogic_MonitoringVisit.class;
    }

    public override VTR3_GlobalSharingConfig__mdt getConfig() {
        return getConfigForObjectType(VTD1_Monitoring_Visit__c.getSObjectType().getDescribe().getName());
    }

    protected override void addShareDetailsForRecords() {
        List<VTD1_Monitoring_Visit__c> monitoringVisits = new List<VTD1_Monitoring_Visit__c>([
            SELECT Id, VTD1_Study__c, VTR2_Visit_CRA__r.User__c, VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c,
                VTD1_Study__r.VTD1_Project_Lead__c,
                // Conquerors
                // Abhay & Akansksha
                // Jira Ref: SH-17438, SH-17437 
                VTD1_Virtual_Site__c
                // End of code: Conquerors
            FROM VTD1_Monitoring_Visit__c
            WHERE Id IN :recordIds
        ]);
        Map<Id, HealthCloudGA__CarePlanTemplate__c> studiesById = getStudiesById(monitoringVisits);
        Map<Id, Set<Id>> mvSiteIdAndCraUserIdSetMap = getCraIdsBySiteIds(monitoringVisits);
        /**** Add something here to get site users. There is a method getSiteUserIdsForSites in the parent class that might help *****/
        for (VTD1_Monitoring_Visit__c visit : monitoringVisits) {
            if (visit.VTR2_Visit_CRA__r.User__c != null) {
                addShareDetail(visit.Id, visit.VTR2_Visit_CRA__r.User__c, visit.VTD1_Study__c, 'Edit');
            }
            if (studiesById.containsKey(visit.VTD1_Study__c)) {
                HealthCloudGA__CarePlanTemplate__c study = studiesById.get(visit.VTD1_Study__c);
                for (Study_Team_Member__c stm : study.Study_Team_Members__r) {
                    // Conquerors
                    // Abhay & Akansksha
                    // Jira Ref: SH-17438, SH-17437 
                    // Description: Added a code to skip the CRAs at STM level for sharing
                    if (stm.User__r.Profile.Name != VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME) {
                        addShareDetail(visit.Id, stm.User__c, visit.VTD1_Study__c, 'Edit');
                    }
                }
                if (study.VTD1_Virtual_Trial_Study_Lead__c != null) {
                    addShareDetail(visit.Id, study.VTD1_Virtual_Trial_Study_Lead__c, visit.VTD1_Study__c, 'Edit');
                }
                if (study.VTD1_Project_Lead__c != null) {
                    addShareDetail(visit.Id, study.VTD1_Project_Lead__c, visit.VTD1_Study__c, 'Edit');
                }
            }
            if (mvSiteIdAndCraUserIdSetMap != null && mvSiteIdAndCraUserIdSetMap.containsKey(visit.VTD1_Virtual_Site__c)) {
                for (Id craUserId : mvSiteIdAndCraUserIdSetMap.get(visit.VTD1_Virtual_Site__c)) {
                    addShareDetail(visit.Id, craUserId, visit.VTD1_Study__c, 'Edit');
                } 
            }
            // End of code: Conquerors
        }
    }

    protected override Iterable<Object> getUserBatchIterable() {
        return (Iterable<Object>) Database.getQueryLocator(getUserQuery());
    }

    protected override void doUserQueryPrep() {
        groupUserIdsByProfile(PROFILES_TO_SHARE_WITH);
        if (!profileToUserIds.isEmpty()) {
            Set<Id> studyLevelUserIds = new Set<Id>();
            Set<Id> siteLevelUserIds = new Set<Id>();
            for (String profileName : profileToUserIds.keySet()) {
                if (SITE_LEVEL_PROFILES.contains(profileName)) {
                    siteLevelUserIds.addAll(profileToUserIds.get(profileName));
                } else {
                    studyLevelUserIds.addAll(profileToUserIds.get(profileName));
                }
            }
            if (!studyLevelUserIds.isEmpty()) {
                getStmUserIdByStudy(studyLevelUserIds);
            }
            if (!siteLevelUserIds.isEmpty()) {
                // Conquerors
                // Abhay & Akansksha
                // Jira Ref: SH-17438, SH-17437

                // Description: The method getSiteUserIdsForUsers() is prepping the pre-requisite data and
                //              creating map of site and associated user in AbstractGlobalSharing class.

                getSiteUserIdsForUsers();
                // End of code: Conquerors
                
            }
        }
    }

    protected override List<SObject> executeUserQuery() {
        if (!profileToUserIds.isEmpty()) {
            String query = getUserQuery() + ' LIMIT ' + getAdjustedQueryLimit();
            return Database.query(query);
        } else {
            return null;
        }
    }

    private String getUserQuery() { 
        String query =
            'SELECT Id, VTD1_Study__c, VTR2_Visit_CRA__r.User__c, VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c,' +
                'VTD1_Study__r.VTD1_Project_Lead__c,' +
                // Abhay, added one column
                ' VTD1_Virtual_Site__c ' +
                ' FROM VTD1_Monitoring_Visit__c' +

                // Commented by Abhay for now // 11/03/2020
                //' WHERE (VTR2_Visit_CRA__r.User__c IN :craIds' +
                ' WHERE (VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c IN :vtslIds' +
                ' OR VTD1_Study__r.VTD1_Project_Lead__c IN :projLeadIds' +
                ' OR VTD1_Study__c IN :studyFromStmIds' +
                

                 // Conquerors
                // Abhay & Akansksha
                // Jira Ref: SH-17438, SH-17437
                ' OR VTD1_Virtual_Site__c IN :userSiteIds)';
                // End of code: Conquerors

        if (scopedStudyIds != null && !scopedStudyIds.isEmpty()) {
            query += ' AND VTD1_Study__c IN :scopedStudyIds';
        }
        System.debug(query);
        return query;
    }

    protected override void createUserShareDetailsFromRecords(List<SObject> records) { 
        List<VTD1_Monitoring_Visit__c> visits = (List<VTD1_Monitoring_Visit__c>)records;
        for (VTD1_Monitoring_Visit__c visit : visits) {
            if (visit.VTR2_Visit_CRA__r.User__c != null && craIds != null && craIds.contains(visit.VTR2_Visit_CRA__r.User__c)) {
                addShareDetail(visit.Id, visit.VTR2_Visit_CRA__r.User__c, visit.VTD1_Study__c, 'Edit');
            }
            if (visit.VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c != null && vtslIds != null && vtslIds.contains(visit.VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c)) {
                addShareDetail(visit.Id, visit.VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c, visit.VTD1_Study__c, 'Edit');
            }
            if (visit.VTD1_Study__r.VTD1_Project_Lead__c != null && projLeadIds != null && projLeadIds.contains(visit.VTD1_Study__r.VTD1_Project_Lead__c)) {
                addShareDetail(visit.Id, visit.VTD1_Study__r.VTD1_Project_Lead__c, visit.VTD1_Study__c, 'Edit');
            }
            // Conquerors
            // Abhay & Akansksha
            // Jira Ref: SH-17438, SH-17437 
            // Description: Remove the CRA reference at STM level and add it to the SSTM level
            if (studyToStmUserIds.containsKey(visit.VTD1_Study__c)) {
                for (Id userId : studyToStmUserIds.get(visit.VTD1_Study__c)) { 
                    if (craIds != null && !craIds.contains(userId)) {
                        addShareDetail(visit.Id, userId, visit.VTD1_Study__c, 'Edit');
                    } 
                }
            }
            if (visit.VTD1_Virtual_Site__c != null 
                && craIds != null
                && siteToCraUserIds != null
                && siteToCraUserIds.containsKey(visit.VTD1_Virtual_Site__c)
                && siteToCraUserIds.get(visit.VTD1_Virtual_Site__c) != null) {
                
                for(Id craUserId: siteToCraUserIds.get(visit.VTD1_Virtual_Site__c)) {
                    addShareDetail(visit.Id, craUserId, visit.VTD1_Study__c, 'Edit');
                } 
            }
            // End of code: Conquerors
        }
    }

    private Map<Id, HealthCloudGA__CarePlanTemplate__c> getStudiesById(List<VTD1_Monitoring_Visit__c> monitoringVisits) {
        List<Id> studyIds = new List<Id>();
        for (VTD1_Monitoring_Visit__c visit : monitoringVisits) {
            studyIds.add(visit.VTD1_Study__c);
        }
        return new Map<Id, HealthCloudGA__CarePlanTemplate__c>([
            SELECT Id, VTD1_Virtual_Trial_Study_Lead__c, VTD1_Project_Lead__c,
            (SELECT User__c, User__r.Profile.Name FROM Study_Team_Members__r WHERE User__r.Profile.Name IN :PROFILES_TO_SHARE_WITH)
            FROM HealthCloudGA__CarePlanTemplate__c
            WHERE Id IN :studyIds
        ]);
    }

    // Conquerors
    // Abhay & Akansksha
    // Jira Ref: SH-17438, SH-17437 
    private Map<Id, Set<Id>> getCraIdsBySiteIds(List<VTD1_Monitoring_Visit__c> monitoringVisits) {
        Map<Id, Set<Id>> mvSiteIdAndCraIdMap = new Map<Id, Set<Id>>();
        Set<Id> virtualSiteIdSet = new Set<Id>();
        for(VTD1_Monitoring_Visit__c mvObj: monitoringVisits) {
            if(!String.isEmpty(mvObj.VTD1_Virtual_Site__c)) {
                virtualSiteIdSet.add(mvObj.VTD1_Virtual_Site__c);
            }   
        }

        for(Study_Site_Team_Member__c sstmObj : [SELECT Id, 
                                                        VTR5_Associated_CRA__c,
                                                        VTR5_Associated_CRA__r.User__c, 
                                                        VTD1_SiteID__c 
                                                    FROM Study_Site_Team_Member__c 
                                                    WHERE VTD1_SiteID__c IN: virtualSiteIdSet
                                                    AND VTR5_Associated_CRA__c <> NULL
                                                    AND VTR5_Associated_CRA__r.User__c <> NULL
                                                    ]) {
            if(!mvSiteIdAndCraIdMap.containsKey(sstmObj.VTD1_SiteID__c)) {
                mvSiteIdAndCraIdMap.put(sstmObj.VTD1_SiteID__c, new Set<Id>());
            }
            mvSiteIdAndCraIdMap.get(sstmObj.VTD1_SiteID__c).add(sstmObj.VTR5_Associated_CRA__r.User__c);
        }
        return mvSiteIdAndCraIdMap;
    }
    // End of code: Conquerors

    protected override String getRemoveQuery() {
        // Abhay
        String query = 'SELECT Id' +
                        ' FROM VTD1_Monitoring_Visit__Share' +
                        ' WHERE UserOrGroupId IN :includedUserIds AND ';
        query += (scopedSiteIds != null && scopedSiteIds.size() > 0) ? 
                                                        'Parent.VTD1_Virtual_Site__c IN: scopedSiteIds':
                                                        'Parent.VTD1_Study__c IN :scopedStudyIds';
        return query;
    }
}