public with sharing class PatientTimeLineController { // Code commented by Roman G as related object to be deleted
    /*static boolean isPatient = false;
    static boolean isCaregiver = false;
    public class PatientTimeLineWrapper {
       @AuraEnabled public String accountId { get; set; }       
       @AuraEnabled public String patientName { get; set; }
       @AuraEnabled public String fullPhotoUrl { get; set; }
       @AuraEnabled public list<Timeline_Item__c> timelineItems { get; set; }       
        
       public PatientTimeLineWrapper(String patientName_p, String fullPhotoUrl_p) {
            accountId = '';
            patientName = patientName_p;
            fullPhotoUrl = fullPhotoUrl_p;
            system.debug('!!!!! patientName_p2 ' + this.patientName);
            timelineItems = new Timeline_Item__c[]{};
            isPatient = false;
       }       
    }
    
    
    @AuraEnabled
    public static Boolean getUserType() {
        setIsPatient();     
        return isPatient || isCaregiver;
    }
    
    @AuraEnabled
    public static List<PatientTimeLineWrapper>  getTimeLineItems() { 
        setIsPatient(); 
        Id userID = UserInfo.getUserId(); //patient - '0053D000000iWbbQAE' pi Id userID = '0053D000000ikTN';
        return getTLines(userID);
    }
    
    
    public static List<PatientTimeLineWrapper> getTLines(Id userID) { 
        List<PatientTimeLineWrapper> results = new List<PatientTimeLineWrapper>();          
        list<User> contacts = [Select ContactId from User where Id=:userID];
        system.debug('!!!!! contacts ' +    contacts);
        list<String> accIDs = new list<String>();
        list<Case> accounts = new list<Case>();
        map<String, String> accPhoto = new map<String, String>();
        list<String> contactsIds = new list<String>();
        if (contacts.size() > 0) {
            String contactId = contacts[0].ContactId; system.debug('!!!!! isPatient2' + isPatient);
            if (isPatient) {                
                accounts = [Select AccountId from Case where ContactId=:contactId];
                if (accounts.size() > 0) {
                    accIDs.add(accounts[0].AccountId);                                  
                }
                system.debug('!!!!! accIDs1 ' + accIDs);    
            } else if (isCaregiver) {   
                list <Contact>  conts = [Select AccountId from Contact Where Contact.RecordType.Name = 'Caregiver' and Id=:contactId]; 
                if (conts.size() > 0) {
                    accIDs.add(conts[0].AccountId);                                 
                }
                system.debug('!!!!! accIDs1 ' + accIDs);
            } else { 
                accounts = [Select AccountId, ContactId from Case where VTD1_PI_contact__c=:contactId]; 
                
                for (Case acc : accounts) {
                    accIDs.add(acc.AccountId);
                    contactsIds.add(acc.ContactId);
                    accPhoto.put(acc.ContactId, acc.AccountId);
                    system.debug('!!!!! ContactId ' + acc.ContactId);
                }           
            }  
        }   
        // get user photos
        list<User> users = [Select ContactId, SmallPhotoUrl, Id from User where ContactId IN :contactsIds];
        for (User u : users) {
            String acc = accPhoto.get(u.ContactId);
            system.debug('!!!!! accPhoto acc ' + accPhoto);
            if (acc != null) {
                accPhoto.remove(u.ContactId);
                
                accPhoto.put(acc, '/customer/profilephoto/' + u.SmallPhotoUrl.right(17)); //u.FullPhotoUrl
                system.debug('!!!!! accPhoto ' + accPhoto);
            }
        }
        system.debug('!!!!! accPhoto ' + accPhoto);
        
        list<Timeline_Item__c> tmlns = [SELECT VTD1_Account__c, VTD1_Account__r.Name, Name, VTD1_Title__c, VTD1_Scheduled_Date__c, VTD1_Display_Date__c, 
                                            VTD1_Complete_By_Date__c, VTD1_Complete_On_Date__c, VTD1_Status__c 
                                        FROM Timeline_Item__c WHERE VTD1_Account__c IN :accIDs ORDER BY VTD1_Account__c,VTD1_Scheduled_Date__c];
        system.debug('!!!!! tmlns ' + tmlns);
        if (tmlns.size() > 0) {
            system.debug('!!!!! patientName_p 1 ' + tmlns[0].VTD1_Account__r.Name + ' url ' + accPhoto.get(tmlns[0].VTD1_Account__c));
            PatientTimeLineWrapper pt = new PatientTimeLineWrapper(tmlns[0].VTD1_Account__r.Name, accPhoto.get(tmlns[0].VTD1_Account__c));
            list<Timeline_Item__c> tls = new list<Timeline_Item__c>();
            Id acId = tmlns[0].VTD1_Account__c;
            
            for (Timeline_Item__c tl : tmlns) {
                if (tl.VTD1_Account__c != acId) {                                       
                    pt.accountId = acId;
                    pt.timelineItems = tls; 
                    results.add(pt);
                    system.debug('!!!!! patientName_p 3 ' + tl.VTD1_Account__r.Name);
                    pt = new PatientTimeLineWrapper(tl.VTD1_Account__r.Name, accPhoto.get(tl.VTD1_Account__c));
                    tls = new list<Timeline_Item__c>();
                    acId = tl.VTD1_Account__c;
                }
                tls.add(tl);                
            }
            pt.accountId = acId;
            pt.timelineItems = tls; 
            results.add(pt);
        }
        system.debug('!!!!! results ' + results);
        return results;        
    }
    
    private static void setIsPatient() {
        Id profileId = UserInfo.getProfileId(); //patient 00e3D000000P04aQAC pi Id profileId = '00e3D000000EC7BQAW'; 
        list<Profile> profile = [Select Name from Profile where Id=:profileId];     
        system.debug('!!!!! profileId' + profileId + '   ' + profile[0].Name); 
        isPatient = (profile[0].Name.equals('Patient'));
        isCaregiver = (profile[0].Name.equals('Caregiver'));
        system.debug('!!!!! isPatient' + isPatient);
    }   
    */
}