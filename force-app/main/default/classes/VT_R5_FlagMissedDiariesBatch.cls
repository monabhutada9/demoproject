/**
* @author: Carl Judge
* @date: 01-Aug-20
**/

public without sharing class VT_R5_FlagMissedDiariesBatch implements Database.Batchable<SObject> {
    @TestVisible
    private static String cronId;
    public static final Integer REPEAT_INTERVAL_MINUTES = VT_R4_ConstantsHelper_Protocol_ePRO.DIARY_SETTINGS.VTR5_MissedDiaryProcessingInterval__c.intValue();
    public static final Integer SCOPE_SIZE = VT_R4_ConstantsHelper_Protocol_ePRO.DIARY_SETTINGS.VTR5_MissedDiaryBatchScopeSize__c.intValue();
    private Datetime startTime = Datetime.now();

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
            SELECT Id
            FROM VTD1_Survey__c
            WHERE VTD1_Due_Date__c <=: Datetime.now()
            AND VTD1_Status__c = :VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_DUE_SOON
        ]);
    }

    public void execute(Database.BatchableContext bc, List<VTD1_Survey__c> diaries) {
        for (VTD1_Survey__c diary : diaries) {
            diary.VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_MISSED;
            diary.VTD1_ePRO_Missed__c = true;
        }
        update diaries;
    }

    public void finish(Database.BatchableContext bc) {
        if(!Test.isRunningTest() && cronId == null){
            reschedule();
        }
    }

    private void reschedule(){
        // most diaries are expected to expire on the hour, so we want to run the job as soon as possible after this, regardless of repeat interval
        Integer minutesFromNow = 0;

        if (this.startTime.hour() >= Datetime.now().hour()) { // if we have passed an hour mark while the job was running, we want to run again right away
            Integer minutesPastHour = System.now().addMinutes(REPEAT_INTERVAL_MINUTES).minute();
            minutesFromNow = minutesPastHour < REPEAT_INTERVAL_MINUTES // if repeat interval takes us part an hour mark, reduce interval so that the job runs on the hour
                ? REPEAT_INTERVAL_MINUTES - minutesPastHour
                : REPEAT_INTERVAL_MINUTES;
        }

        if (minutesFromNow == 0) {  // system.scheduleBatch won't accept 0 as a param
            Database.executeBatch(new VT_R5_FlagMissedDiariesBatch(), SCOPE_SIZE);
        }  else {
            run(minutesFromNow);
        }
    }

    public static void run(Integer minutesFromNow){
        cronId = System.scheduleBatch(new VT_R5_FlagMissedDiariesBatch(), 'Flag Missed Diaries Processing ' + System.now(), minutesFromNow, SCOPE_SIZE);
    }
    public static void run(){
        run(REPEAT_INTERVAL_MINUTES);
    }
}