/**
 * @author: Alexander Komarov
 * @date: 11.08.2020
 * @description: Mobile related constants
 */

public with sharing class VT_R5_ConstantsHelper_Mobile {
    public static final String ACTIVATION_DEEP_LINK_PATH = '/activate?activationLink=';
    public static final String MY_DIARY_DEEP_LINK_PATH = '/my-diary?originalLink=';
    public static final String NEWS_FEED_RT_DEV_NAME = 'PatientsNewsFeed';

}