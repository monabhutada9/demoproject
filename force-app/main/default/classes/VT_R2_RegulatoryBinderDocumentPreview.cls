public with sharing class VT_R2_RegulatoryBinderDocumentPreview {

    @AuraEnabled
    public static RegulatoryBinderDocument getRegulatoryBinderDocument(String documentId) {
        if (String.isEmpty(documentId)) {
            return new RegulatoryBinderDocument();
        }
        return new RegulatoryBinderDocument(documentId);
    }

    @AuraEnabled
    public static List<VT_R2_AddNewPatientController.InputSelectOption> getSignatureTypeValues(String sObjName, String fieldName) {
        try{
            List<VT_R2_AddNewPatientController.InputSelectOption> signatureTypeValues = VT_R2_AddNewPatientController.getInputSelectOptions(sObjName, fieldName);
            return signatureTypeValues;
        } catch (Exception e) {
            System.debug('eee');
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static void setDocSignatureType(Id docId, String docSignType) {
        try{
            VTD1_Document__c doc = [SELECT Id, VTR4_Signature_Type__c FROM VTD1_Document__c WHERE Id = :docId];
            doc.VTR4_Signature_Type__c = docSignType;
            update doc;
        } catch (Exception e) {
            System.debug('eee');
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static void setDocSignatureDate(Id docId, Date docSignatureDate) {
        try {
            VTD1_Document__c document = [SELECT Id FROM VTD1_Document__c WHERE Id = :docId];
            document.VTD1_Signature_Date__c = docSignatureDate;
            update document;
        } catch (DmlException de) {
            throw new AuraHandledException(de.getDmlMessage(0));

        }
    }


    public class RegulatoryBinderDocument {

        @AuraEnabled
        public String title;

        @AuraEnabled
        public VTD1_Document__c document;

        @AuraEnabled
        public List<DocumentDataSection> sections;

        public RegulatoryBinderDocument() {
            this.title = '';
            this.sections = new List<DocumentDataSection>();
        }

        public RegulatoryBinderDocument(String documentId) {
            this.document = this.getDocument(documentId);
            this.title = this.getTitle();
            this.sections = this.getSections();
        }

        private VTD1_Document__c getDocument(Id documentId) {
            List<VTD1_Document__c> document = [
                    SELECT
                            Id,
                            VTD1_Does_file_needs_deletion__c,
                            VTD1_Deletion_Reason__c,
                            VTD1_Why_file_is_irrelevant__c,
                            VTD1_Eligibility_Assessment_Status__c,
                            VTD2_Previous_PI_Decision__c,
                            VTD2_PI_Decision_Comment__c,
                            VTD2_Final_Eligibility_Decision__c,
                            VTD2_TMA_Eligibility_Decision__c,
                            VTD2_TMA_Comments__c,
                            VTD1_Clinical_Study_Membership__r.Contact.Name,
                            VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c,
                            VTD1_Study__r.Name,
                            VTD1_Study__r.VTD1_StudyStatus__c,
                            VTD1_Name__c,
                            VTD1_FileNames__c,
                            VTD1_Version__c,
                            VTD1_Nickname__c,
                            VTD1_Status__c,
                            VTD1_Comments__c,
                            VTD2_Patient__r.VTD1_Patient_Name__c,
                            VTD2_Patient__r.Name,
                            RecordTypeId,
                            RecordType.Name,
                            VTD1_Regulatory_Document_Type__c,
                            VTD1_Document_Type__c,
                            VTD1_IsArchived__c,
                            VTD1_Lifecycle_State__c,
                            VTD1_Current_Workflow__c,
                            VTD2_PI_Signature_required__c,
                            VTR4_Signature_Type__c,
                            VTD1_Document_Name__c,
                            VTD2_Certification__r.Name,
                            VTD1_Signature_Date__c,
                            VTD1_ISF__c,
                            VTD1_ISF_Complete_Date__c,
                            (
                                    SELECT
                                            dsfs__Envelope_Status__c,
                                            dsfs__Declined_Reason__c
                                    FROM DocuSign_Status__r
                                    ORDER BY LastModifiedDate DESC
                                    LIMIT 1
                            )
                    FROM VTD1_Document__c
                    WHERE Id = :documentId
            ];
            if (document.isEmpty()) {
                return null;
            }
            return document[0];
        }

        private String getTitle() {
            if (document == null) {
                return '';
            }
            return this.document.VTD1_Regulatory_Document_Type__c
                    + ', '
                    + Label.VTD1_Version
                    + ' '
                    + (String.isEmpty(this.document.VTD1_Version__c) ? '0.0' : this.document.VTD1_Version__c) + ' '
                    + '(' + Label.VT_D1_CurrentSurveys + ')';
        }

        private List<DocumentDataSection> getSections() {
            List<DocumentDataSection> result = new List<DocumentDataSection>();
            result.add(new OverviewSection(document));
            result.add(new SignatureRequiredSection(document));
            result.add(new IsfSection(document));
            return result;
        }
    }

    public virtual class DocumentDataSection {

        @AuraEnabled
        public String label;

        @AuraEnabled
        public Boolean isVisible;

        @AuraEnabled
        public List<SectionItem> items;

        public DocumentDataSection(String label) {
            this(label, null);
        }

        public DocumentDataSection(String label, VTD1_Document__c document) {
            this.isVisible = true;
            this.label = label + this.appendTitleStatus(document);
            this.items = this.addSectionItems(document);
        }

        protected virtual String appendTitleStatus(VTD1_Document__c document) {
            return '';
        }

        protected virtual List<SectionItem> addSectionItems(VTD1_Document__c document) {
            return new List<SectionItem>();
        }
    }

    public virtual class OverviewSection extends DocumentDataSection {

        public OverviewSection(VTD1_Document__c document) {
            super('Overview', document);
        }

        protected override List<SectionItem> addSectionItems(VTD1_Document__c document) {
            List<SectionItem> result = new List<SectionItem>();

            result.add(new SectionItem(VTD1_Document__c.VTD1_Study__c, document.VTD1_Study__r.Name));
            result.add(new SectionItem(VTD1_Document__c.VTD1_Document_Type__c, document.VTD1_Regulatory_Document_Type__c));
            result.add(new SectionItemVersion(VTD1_Document__c.VTD1_Version__c, document.VTD1_Version__c).setArchived(document.VTD1_IsArchived__c));
            result.add(new SectionItem(VTD1_Document__c.VTD2_Certification__c, document.VTD2_Certification__r.Name));
            result.add(new SectionItem('Lifecycle Status', document.VTD1_Lifecycle_State__c));
            result.add(new SectionItem(VTD1_Document__c.VTD1_Current_Workflow__c, document.VTD1_Current_Workflow__c));
            result.add(new SectionItem('Workflow Status', document.VTD1_Status__c));
            result.add(new SectionItem('File Name', document.VTD1_FileNames__c));
            if (document.VTD1_Status__c == 'Rejected') {
                result.add(new SectionItem('Rejection Comments', document.DocuSign_Status__r[0].dsfs__Declined_Reason__c));
            }

            return result;
        }
    }

    public virtual class SignatureRequiredSection extends DocumentDataSection {

        public SignatureRequiredSection(VTD1_Document__c document) {
            super('Signature Required', document);
        }

        protected override List<SectionItem> addSectionItems(VTD1_Document__c document) {
            List<SectionItem> result = new List<SectionItem>();

            result.add(new SectionItem(VTD1_Document__c.VTR4_Signature_Type__c, document.VTR4_Signature_Type__c));
            result.add(new DocusignSectionItem('Signature Completed', document.Id));
            result.add(new SectionItem('Signature Date', document.VTD1_Signature_Date__c));

            return result;
        }

        protected override String appendTitleStatus(VTD1_Document__c document) {
            if (document.VTD2_PI_Signature_required__c) {
                return ' (Yes)';
            }
            this.isVisible = false;
            return ' (No)';
        }

    }

    public virtual class IsfSection extends DocumentDataSection {

        public IsfSection(VTD1_Document__c document) {
            super('Part of ISF', document);
        }

        protected override List<SectionItem> addSectionItems(VTD1_Document__c document) {
            List<SectionItem> result = new List<SectionItem>();

            result.add(new SectionItem('ISF Completed Date/Time', document.VTD1_ISF_Complete_Date__c));

            return result;
        }

        protected override String appendTitleStatus(VTD1_Document__c document) {
            if (document.VTD1_ISF__c) {
                return ' (Yes)';
            }
            this.isVisible = false;
            return ' (No)';
        }
    }

    public virtual class SectionItem {

        @AuraEnabled
        public String label;

        @AuraEnabled
        public Object value;

        public SectionItem(SObjectField field, Object value) {
            this.label = field.getDescribe().getLabel();
            this.value = value;
        }

        public SectionItem(String label, Object value) {
            this.label = label;
            this.value = value;
        }
    }

    public class SectionItemVersion extends SectionItem {

        public SectionItemVersion(SObjectField field, Object value) {
            super(field, value);
        }

        public SectionItemVersion setArchived(Boolean isArchived) {
            if (isArchived) {
                this.label += ' (Archived)';
            } else {
                this.label += ' (Current)';
            }
            return this;
        }
    }

    public class DocusignSectionItem extends SectionItem {

        public DocusignSectionItem(String field, String documentId) {
            super(field, '');
            this.value = this.getDocuSignStatus(documentId);
        }

        public DocusignSectionItem(SObjectField field, String documentId) {
            super(field, '');
            this.value = this.getDocuSignStatus(documentId);
            //this.getDocuSignStatus(documentId);
        }

        private String getDocuSignStatus(String documentId) {
            List<dsfs__DocuSign_Status__c> statuses = [
                    SELECT Id, Name, dsfs__Envelope_Status__c
                    FROM dsfs__DocuSign_Status__c
                    WHERE VTD1_Document__c =: documentId
            ];
            Integer totalStatuses = statuses.size();
            Integer completedStatuses = 0;
            for (dsfs__DocuSign_Status__c status : statuses) {
                if (status.dsfs__Envelope_Status__c == 'Completed') {
                    completedStatuses++;
                }
            }
            return totalStatuses > 0 ? '' + completedStatuses + '/' + totalStatuses : '';
        }
    }
}