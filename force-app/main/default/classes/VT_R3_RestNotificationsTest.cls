@isTest
public with sharing class VT_R3_RestNotificationsTest {
	static VT_R3_RestHelper helper = new VT_R3_RestHelper();

	public static void getNotificationsTest() {
		String responseString;
		Map<String, Object> responseMap = new Map<String, Object>();
		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();
		request.httpMethod = 'GET';
		request.requestURI = '/Notifications/*';
		RestContext.request = request;
		RestContext.response = response;
//		Case cas = VT_D1_TestUtils.createCase('CarePlan', null, null, null, null, null, null);
//		insert cas;
//		VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c(
//				VTD1_Case__c = cas.Id
//		);
//		insert actualVisit;
		createnotif('a1P2a0000002DCCEA2');
//
//		Video_Conference__c conf = new Video_Conference__c(VTD1_Actual_Visit__c = actualVisit.Id);
//		insert conf;
		responseString = VT_R3_RestNotifications.getNotifications();
		responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
		List<Object> objList = (List<Object>) responseMap.get('serviceResponse');
		responseMap = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(objList[0]));
		System.assert(!(Boolean) responseMap.get('read'));
		String notifId = (String) responseMap.get('id');
		if(!String.isEmpty(notifId)) {
			RestContext.request.requestBody = Blob.valueof('notifId');
			responseString = VT_R3_RestNotifications.updateNotifications();
			responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
			System.assertEquals(helper.forAnswerForIncorrectInput(), responseMap.get('serviceResponse'));

			RestContext.request.requestBody = Blob.valueof(notifId);
			responseString = VT_R3_RestNotifications.updateNotifications();
			responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
			List<VTD1_NotificationC__c> notificationList = [SELECT Id, VTD1_Read__c FROM VTD1_NotificationC__c WHERE Id = :notifId];
			System.assert(notificationList[0].VTD1_Read__c);
			System.debug('hello copado');

			responseString = VT_R3_RestNotifications.hideNotifications();
			responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);

			notificationList = [SELECT Id, VTD1_Read__c FROM VTD1_NotificationC__c WHERE Id = :notifId];

			RestContext.request.requestBody = Blob.valueof('id!, id2');
			responseString = VT_R3_RestNotifications.hideNotifications();
			responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
		}

	}

	private static void createnotif(String caseId){
		VTD1_NotificationC__c n = new VTD1_NotificationC__c();
		n.Type__c = 'General';
		n.VTD1_Receivers__c = UserInfo.getUserId();
		n.VTD2_New_Message_Added_DateTme__c = Datetime.now();
//		n.VTR2_PatientNotificationId__c = '11111';
		n.VTD2_VisitID__c = caseId;
		n.Message__c = 'Some message';
		insert n;
		VTD1_NotificationC__c notification2 = new VTD1_NotificationC__c();
		notification2.Type__c = 'Messages';
		notification2.Number_of_unread_messages__c = 3;
		notification2.VTD2_New_Message_Added_DateTme__c = Datetime.now();
		notification2.VTD1_Receivers__c = UserInfo.getUserId();
		notification2.Message__c = 'Some message';
		insert notification2;

		VTD1_NotificationC__c notification3 = new VTD1_NotificationC__c();
		notification3.Type__c = 'Messages';
		notification3.Number_of_unread_messages__c = 1;
		notification3.VTD2_New_Message_Added_DateTme__c = Datetime.now();
		notification3.VTD1_Receivers__c = UserInfo.getUserId();
		notification3.Message__c = 'Some message';
		insert notification3;

		System.debug('hello copado');
	}

}