/**
 * Created by user on 16.04.2019.
 */

public with sharing class VT_R2_CustomLookupController {
    class QueryResult {
        List <Object> data = new List<Object>();
        String error;
    }
    @AuraEnabled
    public static String queryRecords(String jsonParams) {
        Map <String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(jsonParams);
        QueryResult result = new QueryResult();
        try {
            String sObjectName = (String)params.get('sobject');
            String term = (String)params.get('term');
            String filter = (String)params.get('filter');
            String order = (String)params.get('order');
            String lim = (String)params.get('limit');
            String searchField = (String)params.get('searchField');
            String menuImageURLField = (String)params.get('menuImageURLField');

            List <String> filterList = new List<String>();
            if (filter != null) {
                filterList.add(filter);
            }
            if (!String.isEmpty(term)) {
                filterList.add(searchField + ' LIKE \'%' + term + '%\'');
            }

            Set<String> fields = new Set<String>{'Id'};

            if (searchField != null) {
                fields.add(searchField);
            }

            if (menuImageURLField != null) {
                fields.add(menuImageURLField);
            }

            String query = 'select ' + String.join(new List<String>(fields), ', ') + ' from ' + sObjectName;

            if (!filterList.isEmpty()) {
                query += ' where ' + String.join(filterList, ' and ');
            }

            if (order != null) {
                query += ' order by ' + order;
            }

            result.data = Database.query(query);

        } catch (Exception e) {
            result.error = e.getMessage();
        }
        return JSON.serialize(result);
    }
}