/**
 * Created by Leonid Bartenev
 *
 * Integration Action for create/update subject in CSM.
 *
 * Example of usage:
 *
 * VT_D1_QAction_SendSubject sendSubjectAction = new VT_D1_QAction_SendSubject('someCaseIdHere');
 * sendSubjectAction.execute();
 *
 * For async use:
 * sendSubjectAction.executeFuture();
 *
 *
 * Changed by Jane Ivanova a little (October 15, 2018)
 */

public class VT_D1_QAction_SendSubject extends VT_D1_AbstractAction {

    private Id caseId;

    public VT_D1_QAction_SendSubject(Id caseId) {
        this.caseId = caseId;
    }

    //action logic implementation:
    public override void execute() {
        if (caseId == null) {
            return;
        }

        Case cs = [
                SELECT Id,
                        VTD1_Primary_PG__c,
                        Status,
                        VTD1_Study__c,
                        VTD1_Study__r.VTR5_IRTSystem__c,
                        VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c,
                        VTD1_Study__r.VTD1_Study_Admin__c,
                        VTD1_Patient_User__r.Name,
                        VTD1_Subject_ID__c
                FROM Case
                WHERE Id = :caseId
        ];
        // SH-14372 - skip integration for Signant Studies. Vlad Tyazhov
        if (cs.VTD1_Study__r.VTR5_IRTSystem__c == 'Signant') {
            return;
        }

        VT_D1_SubjectIntegration.SubjectIntegrationResponse result = VT_D1_SubjectIntegration.upsertSubject(caseId);
        HttpResponse response = result.httpResponse;

        System.debug('response = ' + response);
        if (response == null) {
            //time out, action will not be added to queue:
            setStatus(STATUS_FAILED);
            setMessage(result.log.VTD1_ErrorMessage__c);
        } else if (response.getStatusCode() >= 500 && response.getStatusCode() < 600) {
            //internal server error, action will be added to queue:
            setStatus(STATUS_FAILED);
            setMessage('Server return status code: ' + response.getStatusCode() + ' ' + response.getStatus() + '; Body: ' + response.getBody());
            addToQueue();
        } else if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
            //success
            setStatus(STATUS_SUCCESS);
            cs.VTD1_CreatedInCenduitCSM__c = true; // should be the value from the response field
        } else {
            //other errors, action will not be added to queue
            setStatus(STATUS_FAILED);
            setMessage('Server return: ' + response.getStatusCode() + ' ' + response.getStatus() + ', Body: ' + response.getBody());
            //TODO ADD CREATION OF TASK HERE
        }
        cs.VTD1_Send_Subject_Log__c = result.log.Id;

        //after all attempts, generate tasks:
        if (isLastAttempt() && getStatus() != STATUS_SUCCESS) {
            Set<Id> ownerIds = new Set<Id>{
                    cs.VTD1_Primary_PG__c,
                    cs.VTD1_Study__r.VTD1_Virtual_Trial_Study_Lead__c,
                    cs.VTD1_Study__r.VTD1_Study_Admin__c
            };
            String errorMessage = getMessage();
            createTasksTroughTnForCase(
                    'T564',
                    new List<String>{cs.Id, cs.Id, cs.Id},
                    new List<Id> (ownerIds),
                    errorMessage);
        }
        update cs;
    }

    public override Type getType() {
        return VT_D1_QAction_SendSubject.class;
    }

}