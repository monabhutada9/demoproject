@IsTest
public with sharing class VT_D1_ContentDocumentLinkProcessTest {
    public static void doInsertAndDeleteTest() {
        Case carePlan = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName(VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN)
                .persist();
        Case patientContactForm = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName(VT_R4_ConstantsHelper_AccountContactCase.RT_PCF)
                .setVTD1_Clinical_Study_Membership(carePlan.Id)
                .persist();

        Test.startTest();
        ContentVersion cvOfCarePlan = createFile('CP test file', carePlan.Id, 'I', 'AllUsers');
        ContentVersion cvOfPatientContactForm = createFile('PCF test file', patientContactForm.Id, 'I', 'AllUsers');

        List<ContentDocumentLink> cdlList = [
                SELECT Id, LinkedEntityId, Visibility
                FROM ContentDocumentLink
                WHERE ContentDocumentId = :cvOfCarePlan.ContentDocumentId
                OR ContentDocumentId = :cvOfPatientContactForm.ContentDocumentId
        ];
        Set<Id> cdlIdSet = new Set<Id>();
        for (ContentDocumentLink cdl : cdlList) {
            cdlIdSet.add(cdl.Id);
            System.assertNotEquals(null, cdl);
        }

        delete cdlList;
        List<ContentDocumentLink> cdlAfterDeleteList = [SELECT Id FROM ContentDocumentLink WHERE Id IN :cdlIdSet];
        System.assertEquals(0, cdlAfterDeleteList.size());

        List<VTD1_Document__c> docToDeleteList = VT_D1_ContentDocumentLinkProcessHandler.getDocContainersList(cdlList);
        System.assertEquals(1, docToDeleteList.size());
        Test.stopTest();
    }

    public static void testShareVirtualSiteFilesToCustomers() {
        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c();
        study.VTD2_Certification_form_Medical_Records__c = 'Study\'s file';
        insert study;

        Virtual_Site__c virtualSite = new Virtual_Site__c();
        virtualSite.VTD1_Study_Site_Number__c = '12345';
        virtualSite.VTD1_Study__c = study.Id;
        insert virtualSite;

        Test.startTest();
        ContentVersion cvOfStudy = createFile('Study\'s file', study.Id, 'I', 'AllUsers');
        ContentVersion cvOfSite = createFile('Virtual Site\'s file', virtualSite.Id, 'V', 'InternalUsers');

        for (ContentDocumentLink cdl : [
                SELECT Id, LinkedEntityId, Visibility
                FROM ContentDocumentLink
                WHERE ContentDocumentId = :cvOfStudy.ContentDocumentId
                OR ContentDocumentId = :cvOfSite.ContentDocumentId
        ]) {
            System.assertEquals('AllUsers', cdl.Visibility);
        }
        Test.stopTest();
    }

    public static void blockUploadForSignMonVisitTaskTest() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c();
        insert study;

        Virtual_Site__c virtualSite = new Virtual_Site__c();
        virtualSite.VTD1_Study_Site_Number__c = '12345';
        virtualSite.VTD1_Study__c = study.Id;
        insert virtualSite;

        User craUser = (User) new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_Profiles.CRA_PROFILE_NAME)
                .persist();

        Study_Team_Member__c craStudyTeamMember = new Study_Team_Member__c();
        craStudyTeamMember.Study__c = study.Id;
        craStudyTeamMember.User__c = craUser.Id;
        insert craStudyTeamMember;

        insert new Study_Site_Team_Member__c(VTR5_Associated_CRA__c = craStudyTeamMember.Id, VTD1_SiteID__c = virtualSite.Id);

        insert new HealthCloudGA__CarePlanTemplate__Share(ParentId = virtualSite.VTD1_Study__c, UserOrGroupId = craUser.Id, AccessLevel = 'Edit');

        System.runAs(craUser) {
            VTD1_Monitoring_Visit__c monitoringVisit = new VTD1_Monitoring_Visit__c();
            monitoringVisit.VTR5_SignMVReportTaskSent__c = true;
            monitoringVisit.VTD1_Virtual_Site__c = virtualSite.Id;
            insert monitoringVisit;

            try {
                insert new ContentDocumentLink(LinkedEntityId = monitoringVisit.Id);
                System.assert(false);
            } catch (Exception e) {
                System.assertEquals(true, e.getMessage().contains('A file already exists for this task'));
            }
        }
        Test.stopTest();
    }

    private static ContentVersion createFile(String title, Id parentId, String shareType, String visibility) {
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.PathOnClient = 'test.txt';
        contentVersion.Title = title;
        contentVersion.VersionData = Blob.valueOf('Test Data');
        insert contentVersion;

        contentVersion = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];

        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = parentId;
        contentDocumentLink.ContentDocumentId = contentVersion.ContentDocumentId;
        contentDocumentLink.ShareType = shareType;
        contentDocumentLink.Visibility = visibility;
        insert contentDocumentLink;
        return contentVersion;
    }
}