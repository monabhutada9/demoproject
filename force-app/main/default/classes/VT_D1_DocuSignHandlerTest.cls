@IsTest
private with sharing class VT_D1_DocuSignHandlerTest {
    
    @TestSetup
    static void setupData() {
        Virtual_Site__c site = (Virtual_Site__c) new DomainObjects.VirtualSite_t()
            .addStudy(new DomainObjects.Study_t())
            .setStudySiteNumber('12345')
            .persist();
        VTD1_Document__c document = (VTD1_Document__c) new DomainObjects.VTD1_Document_t()
            .addCase(new DomainObjects.Case_t())
            .setRecordTypeId(VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_NOTE_OF_TRANSFER)
            .addStudy(new DomainObjects.Study_t())
            .persist();
        document.VTD1_Document_Type__c = VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_TARGET_NOTE_OF_TRANSFER;
        document.VTD1_Site__c = site.Id;
        ContentVersion contentVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
            .setPathOnClient('test.pdf')
            .setTitle('Test')
            .setVersionData(Blob.valueOf('TestData'))
            .persist();
        ContentVersion insertedContVersion = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        new DomainObjects.ContentDocumentLink_t()
            .withLinkedEntityId(document.Id)
            .withContentDocumentId(insertedContVersion.ContentDocumentId)
            .persist();
        dsfs__DocuSign_Status__c docuSignStatus = new dsfs__DocuSign_Status__c(
            VTD1_Document__c = document.Id
        );
        insert docuSignStatus;
        
       
        Profile profiles = [SELECT Id, Name FROM Profile WHERE Name = 'Site Coordinator' limit 1];
        
        HealthCloudGA__CarePlanTemplate__c study = [select id from HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();
       
        Contact scrCon = new Contact(LastName = 'SCR contact', Email = 'scr1@email.com');
        insert scrCon;
                
        List<User> userLst = new List<User>();
        User CRAUser = VT_D1_TestUtils.createUserByProfile('CRA');
        //userLst.add(CRAUser);
        User PGUser = VT_D1_TestUtils.createUserByProfile('Patient Guide');
        //userLst.add(PGUser);
        
         User SCRUser = new User(
                ProfileId = profiles.Id,
                FirstName = 'SCR Test',
                LastName = 'LT' ,
                Email = VT_D1_TestUtils.generateUniqueUserName(),
                Username = VT_D1_TestUtils.generateUniqueUserName(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US', 
                LocaleSidKey = 'en_US',
                IsActive = true,
                VTR2_WelcomeEmailSent__c = true,
                ContactId = scrCon.Id 
        );
        //User SCRUser = VT_D1_TestUtils.createUserByProfile('Site Coordinator');
        userLst.add(SCRUser);
        //insert CRAUser;
        
        User RMUser = VT_D1_TestUtils.createUserByProfile('Regulatory Specialist');
        //userLst.add(RMUser );
        User admin= [SELECT Id from user where profile.name = 'System Administrator' and IsActive= true limit 1];
        
        System.debug('User List '+ userLst);
        system.runAs(admin){
            insert userLst;
        }
        VT_D1_TestUtils.persistUsers();
        
        HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
        objVirtualShare.ParentId = study.Id;
        objVirtualShare.UserOrGroupId = CRAUser.Id;
        objVirtualShare.AccessLevel = 'Edit';
        insert objVirtualShare;
        
       
        
        
        system.debug('CRAUser'+CRAUser);
        Study_Team_Member__c stmCRA = new Study_Team_Member__c(User__c = CRAUser.Id, 
                                                               Study__c = study.Id,
                                                               RecordTypeId = VT_R4_ConstantsHelper_ProfilesSTM.RECORD_TYPE_ID_STUDY_TEAM_MEMBER_CRA,
                                                               VTD1_Active__c = True);
        insert stmCRA;
        
                
        VTD2_Study_Geography__c studyGeography = new VTD2_Study_Geography__c();
        studyGeography.VTD2_Geographical_Region__c = 'Country';
        studyGeography.Name = 'US';
        studyGeography.VTD2_Study__c = study.Id;
        
        insert studyGeography;
        
        studyGeography.VTR3_Primary_RS__c = RMUser.Id;
        update studyGeography;
        
        site.VTR3_Study_Geography__c = studyGeography.Id;
        site.VTD1_Remote_CRA__c = CRAUser.Id;
        //site.VTR2_Onsite_CRA__c = stmOnsiteCra.Id;
        update site;
        
        system.debug('stmCRA'+stmCRA);
        Study_Site_Team_Member__c sstm = new Study_Site_Team_Member__c();
        sstm.VTR5_Associated_CRA__c = stmCRA.Id;
        sstm.VTD1_SiteID__c = site.Id ;
        insert sstm;
        system.debug('sstm'+sstm);
        
        VTD1_Monitoring_Visit__c mv  = new VTD1_Monitoring_Visit__c();
        mv.VTD1_Site_Visit_Type__c ='Interim Visit';
        mv.VTD1_Virtual_Site__c = site.Id;
        mv.VTD1_Study__c = site.VTD1_Study__c;
        mv.VTD1_Visit_Start_Date__c = System.now().addHours(2); 
        mv.VTD1_DocuSignStatus__c = docuSignStatus.Id;
        mv.VTD1_Assigned_PG__c = PGUser.Id;
        mv.VTR5_PrimarySCRUser__c = SCRUser.Id;
        
        system.runAs(CRAUser){
            insert mv;
        }
 
        docuSignStatus.VTD1_Monitoring_Visit__c = mv.Id;
        update docuSignStatus;        
        
        List<Task> taskLst = new List<Task>();
        taskLst.add(new Task(Status = 'Open', whatId = mv.Id, VTD2_Task_Unique_Code__c='103'));
        taskLst.add(new Task(Status = 'Open', whatId = docuSignStatus.Id, VTD2_Task_Unique_Code__c ='102'));
        insert taskLst;
    }
 
    @IsTest
    static void sendDocWithDocuSignTest() {
        VTD1_Document__c document = [SELECT Id FROM VTD1_Document__c LIMIT 1];
        
        Test.startTest();
        String error = VT_D1_DocuSignHandler.sendDocWithDocuSign(document.Id);
        Test.stopTest();
        
        System.assertEquals(null, error, 'Incorrect error value');
    }
    
    @IsTest
    static void getDocuSignStatusesTest() {
        VTD1_Document__c document = [SELECT Id FROM VTD1_Document__c LIMIT 1];
        
        Test.startTest();
        String serializedStatuses = VT_D1_DocuSignHandler.getDocuSignStatuses(document.Id);
        Test.stopTest();
        
        System.assertEquals(true, String.isNotBlank(serializedStatuses), 'Incorrect serializedStatuses value');
    }
    
    @IsTest
    static void processMonitoringVisitIdsTest_Null() {
        dsfs__DocuSign_Status__c status =  [SELECT Id FROM dsfs__DocuSign_Status__c LIMIT 1];
        Test.startTest();
        status.dsfs__Envelope_Status__c = 'Completed';
        update status;
        Test.stopTest();
    }
    
/***********************************************************************
* @author              IQVIA
* JIRA                 SH - 21445 
* @date                8-Dec-2020
* @group               As a part of test coverage, updateMonitoringVisitRejected() is covered from VT_D1_DocuSignHandler
* @description         Sets the monitoring visit status to rejected and emails CRA for rejection.
*/    
    @IsTest
    static void updateMonitoringVisitRejectedTest()
    {
        
        Test.startTest();
        dsfs__DocuSign_Status__c docuSignStatus = [SELECT ID, dsfs__Envelope_Status__c,  VTD1_Monitoring_Visit__c FROM dsfs__DocuSign_Status__c
                                                   where VTD1_Monitoring_Visit__c != null LIMIT 1];
        
        system.debug('docuSignStatus ' +docuSignStatus );
        Study_Team_Member__c CRA = [select Id from Study_Team_Member__c limit 1];
        
        //Scenario 1
        VTD1_Monitoring_Visit__c mvisit = new VTD1_Monitoring_Visit__c();
        mvisit.Id = docuSignStatus.VTD1_Monitoring_Visit__c;
        mvisit.VTD1_Remote_Monitoring_Visit_Report_Stat__c = 'Submitted';
        mvisit.VTD1_Study_Site_Visit_Status__c = 'Planned' ;
        mvisit.VTR2_Visit_CRA__c = CRA.Id;
        update mvisit;
        
        docuSignStatus.dsfs__Envelope_Status__c = 'Declined';
        update docuSignStatus;
        VTD1_Monitoring_Visit__c mv = [ select Id, VTD1_Remote_Monitoring_Visit_Report_Stat__c ,
                                       VTD1_Study_Site_Visit_Status__c from VTD1_Monitoring_Visit__c where id =: mvisit.Id ];
        
        System.assertEquals('Rejected', mv.VTD1_Remote_Monitoring_Visit_Report_Stat__c);
        Test.stopTest();
    }

/***********************************************************************
* @author              IQVIA RDS
* JIRA                 SH - 21445 
* @date                8-Dec-2020
* @group               As a part of test coverage, updateMonitoringVisitApproved() is covered from VT_D1_DocuSignHandler
* @description         Sets the monitoring visit status to approved and emails CRA for status approved.
*/
    @IsTest
    static void updateMonitoringVisitApprovedTest()
    {
        Test.startTest();
        Study_Team_Member__c stm = [SELECT id from Study_Team_Member__c where User__c != null ];
        //VTD1_Monitoring_Visit__c mv = [ select Id from VTD1_Monitoring_Visit__c LIMIT 1];
        dsfs__DocuSign_Status__c docuSignStatus = [SELECT ID, dsfs__Envelope_Status__c, VTD1_Monitoring_Visit__c FROM dsfs__DocuSign_Status__c WHERE VTD1_Monitoring_Visit__c != null];
        Study_Team_Member__c CRA = [select Id from Study_Team_Member__c limit 1];
        
        //Scanrio 2
        VTD1_Monitoring_Visit__c mvisit = new VTD1_Monitoring_Visit__c();
        mvisit.Id = docuSignStatus.VTD1_Monitoring_Visit__c;
        mvisit.VTD1_Remote_Monitoring_Visit_Report_Stat__c = 'Submitted';
        mvisit.VTD1_Study_Site_Visit_Status__c = 'Planned' ;
        mvisit.VTR2_Visit_CRA__c = stm.Id;
        
        update mvisit;
        
        docuSignStatus.dsfs__Envelope_Status__c = 'Completed';
        update docuSignStatus;
        
        VTD1_Monitoring_Visit__c mv = [ select Id, VTD1_Remote_Monitoring_Visit_Report_Stat__c ,
                                       VTD1_Study_Site_Visit_Status__c from VTD1_Monitoring_Visit__c where id =: mvisit.Id ];
        
        test.stopTest();
        System.assertEquals('Approved', mv.VTD1_Remote_Monitoring_Visit_Report_Stat__c);
        
    }

/***********************************************************************
* @author              IQVIA
* JIRA                 SH - 21445
* @date                8-Dec-2020
* @group               As a part of test coverage, updateVisitFollowUpStatus() is covered from VT_D1_DocuSignHandler
* @description         Sets the monitoring visit Follow up status to approved and updates task as completed
*/

    @IsTest
    static void updateVisitFollowUpStatusTest()
    {
        Test.startTest();
        Study_Team_Member__c stm = [SELECT id from Study_Team_Member__c where User__c != null ];
        //VTD1_Monitoring_Visit__c mv = [ select Id from VTD1_Monitoring_Visit__c LIMIT 1];
        dsfs__DocuSign_Status__c docuSignStatus = [SELECT ID, dsfs__Envelope_Status__c,  VTD1_Monitoring_Visit__c FROM dsfs__DocuSign_Status__c WHERE VTD1_Monitoring_Visit__c != null];
        Study_Team_Member__c CRA = [select Id from Study_Team_Member__c limit 1];
        
        //Scanrario 3
        VTD1_Monitoring_Visit__c mvisit = new VTD1_Monitoring_Visit__c();
        // VTD1_Monitoring_Visit__c mvisit = new VTD1_Monitoring_Visit__c();
        mvisit.Id = docuSignStatus.VTD1_Monitoring_Visit__c;
        mvisit.VTD1_Remote_Monitoring_Visit_Report_Stat__c = 'Approved';
        mvisit.VTD1_Study_Site_Visit_Status__c = 'Visit Completed';
        mvisit.Follow_Up_Letter_Status__c = 'Rejected';
        mvisit.VTR2_Visit_CRA__c = stm.Id;
        update mvisit;
        docuSignStatus.dsfs__Envelope_Status__c ='Completed';
        update docuSignStatus;
        
        VTD1_Monitoring_Visit__c mv = [ select Id, VTD1_Remote_Monitoring_Visit_Report_Stat__c ,
                                       VTD1_Study_Site_Visit_Status__c, Follow_Up_Letter_Status__c from VTD1_Monitoring_Visit__c where id =: mvisit.Id ];
         test.stopTest();
        System.assertEquals('Approved', mv.Follow_Up_Letter_Status__c);        
    }

/***********************************************************************
* @author              IQVIA
* JIRA                 SH - 21445
* mapOfTask            map of task
* @date                8-Dec-2020
* @group               As a part of test coverage, updateMonitoringVisitSubmitted() is covered from VT_D1_DocuSignHandler
* @description         Sets the monitoring visit status to  completed and updates task completed
*/
    @IsTest
    static void updateMonitoringVisitSubmittedTest()
    {
        Test.startTest();
        Study_Team_Member__c stm = [SELECT id from Study_Team_Member__c where User__c != null ];
        system.debug('STM IN '+ stm );
        //VTD1_Monitoring_Visit__c mv = [ select Id from VTD1_Monitoring_Visit__c LIMIT 1];
        
        dsfs__DocuSign_Status__c docuSignStatus = [SELECT ID, dsfs__Envelope_Status__c,  VTD1_Monitoring_Visit__c FROM dsfs__DocuSign_Status__c WHERE VTD1_Monitoring_Visit__c != null];
        Study_Team_Member__c CRA = [select Id from Study_Team_Member__c limit 1];
        
        // Scanario 4
        VTD1_Monitoring_Visit__c mvisit = new VTD1_Monitoring_Visit__c();
        mvisit.Id = docuSignStatus.VTD1_Monitoring_Visit__c;
        mvisit.VTD1_Study_Site_Visit_Status__c= 'Visit Completed';
        mvisit.VTD1_Remote_Monitoring_Visit_Report_Stat__c = 'Approved';
        mvisit.Follow_Up_Letter_Status__c = 'Rejected';
        mvisit.VTR2_Visit_CRA__c = stm.Id;
        update mvisit;        
        
        docuSignStatus.dsfs__Envelope_Status__c = 'Sent' ;
        update docuSignStatus;
        
        VTD1_Monitoring_Visit__c mv = [ select Id, VTD1_Remote_Monitoring_Visit_Report_Stat__c ,
                                       VTD1_Study_Site_Visit_Status__c, Follow_Up_Letter_Status__c from VTD1_Monitoring_Visit__c where id =: mvisit.Id ];
        

         test.stopTest();
        System.assertEquals('Submitted', mv.Follow_Up_Letter_Status__c);
    }

/***********************************************************************
* @author              IQVIA
* JIRA                 SH - 21445 
* @date                8-Dec-2020
* @group               As a part of test coverage, updateFollowUpStatusRejected() is covered from VT_D1_DocuSignHandler
* @description         Sets the monitoring visit follow up status  to rejected and generates task with 103 code
*/
    @IsTest
    static void updateFollowUpStatusRejectedTest()
    {
        Test.startTest();
        //VTD1_Monitoring_Visit__c mv = [ select Id from VTD1_Monitoring_Visit__c LIMIT 1];
        dsfs__DocuSign_Status__c docuSignStatus = [SELECT ID, dsfs__Envelope_Status__c, VTD1_Monitoring_Visit__c FROM dsfs__DocuSign_Status__c WHERE VTD1_Monitoring_Visit__c != null];
        Study_Team_Member__c CRA = [select Id from Study_Team_Member__c limit 1];
        
        //Scenarios 5
        VTD1_Monitoring_Visit__c mvisit = new VTD1_Monitoring_Visit__c();
        mvisit.Id = docuSignStatus.VTD1_Monitoring_Visit__c;
        mvisit.VTD1_Remote_Monitoring_Visit_Report_Stat__c = 'Approved';
        mvisit.VTD1_Study_Site_Visit_Status__c = 'Visit Completed';
        update mvisit;
        
        docuSignStatus.dsfs__Envelope_Status__c = 'Declined';
        update docuSignStatus;        
        
        VTD1_Monitoring_Visit__c mv = [ select Id, VTD1_Remote_Monitoring_Visit_Report_Stat__c ,
                                       VTD1_Study_Site_Visit_Status__c, Follow_Up_Letter_Status__c from VTD1_Monitoring_Visit__c where id =: mvisit.Id ];

        
        Test.stopTest();
        System.assertEquals('Rejected', mv.Follow_Up_Letter_Status__c);
    }
    
    /***********************************************************************
* @author              IQVIA
* JIRA                 SH - 21445 
* @date                8-Dec-2020
* @group               As a part of test coverage, negativeTestingMethodTest() covers negative testing
* @description         Checks negative scenarios
*/
    @IsTest
    static void negativeTestingMethodTest()
    {
        Test.startTest();
        //VTD1_Monitoring_Visit__c mv = [ select Id from VTD1_Monitoring_Visit__c LIMIT 1];
        dsfs__DocuSign_Status__c docuSignStatus = [SELECT ID, dsfs__Envelope_Status__c, VTD1_Monitoring_Visit__c FROM dsfs__DocuSign_Status__c WHERE VTD1_Monitoring_Visit__c != null];
        Study_Team_Member__c CRA = [select Id from Study_Team_Member__c limit 1];
        
        //Scenarios 5
        VTD1_Monitoring_Visit__c mvisit = new VTD1_Monitoring_Visit__c();
        mvisit.Id = docuSignStatus.VTD1_Monitoring_Visit__c;
        mvisit.VTD1_Remote_Monitoring_Visit_Report_Stat__c = 'Rejected';
        mvisit.VTD1_Study_Site_Visit_Status__c = 'Visit Completed';
        update mvisit;
        
        docuSignStatus.dsfs__Envelope_Status__c = 'Declined';
        update docuSignStatus;        
        
        VTD1_Monitoring_Visit__c mv = [ select Id, VTD1_Remote_Monitoring_Visit_Report_Stat__c ,
                                       VTD1_Study_Site_Visit_Status__c, Follow_Up_Letter_Status__c from VTD1_Monitoring_Visit__c where id =: mvisit.Id ];

        
        Test.stopTest();
        System.assertNotEquals('Rejected', mv.Follow_Up_Letter_Status__c);
    }
}