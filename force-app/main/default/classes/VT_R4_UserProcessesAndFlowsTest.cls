/**
 * Created by Alexey Mezentsev on 6/29/2020.
 */

@IsTest
private class VT_R4_UserProcessesAndFlowsTest {
    @TestSetup
    static void setup() {

        Test.startTest();
        VT_R3_GlobalSharing.disableForTest = true;



        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor');
        Account account = (Account) patientAccount.persist();

        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(patientAccount)
                .setVTD1_SC_Tasks_Queue_Id(UserInfo.getUserId());

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
                .setFirstName('Patient')
                .setLastName('Patient')
                .setAccountId(account.Id);

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        patientUser.persist();

        Case patientCase = (Case) new DomainObjects.Case_t()
                .addPIUser(new DomainObjects.User_t())
                .addVTD1_Patient_User(patientUser)
                .addContact(patientContact)
                .setRecordTypeByName('CarePlan')
                .setAccountId(account.Id)
                .addStudy(study)
                .persist();

        account.HealthCloudGA__CarePlan__c = patientCase.Id;
        update account;

        User caregiverUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t()
                        .setFirstName('Caregiver')
                        .setLastName('Caregiver')
                        .setEmail(DomainObjects.RANDOM.getEmail())
                        .setRecordTypeName('Caregiver')
                        .setAccountId(account.Id)
                )
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME)
                .persist();

        Contact caregiverContact = [SELECT VTD1_Primary_CG__c FROM Contact WHERE FirstName = 'Caregiver'];
        caregiverContact.VTD1_Primary_CG__c = true;
        caregiverContact.VTD1_UserId__c = caregiverUser.Id;
        update caregiverContact;

        VTD1_RTId__c settings = new VTD1_RTId__c(
                VTD2_Profile_Caregiver__c = VT_D1_HelperClass.getProfileIdByName('Caregiver'),
                VTD2_Profile_PG__c = VT_D1_HelperClass.getProfileIdByName('Patient Guide'),
                VTD1_Task_SimpleTask__c = VT_R4_ConstantsHelper_Tasks.RECORD_TYPE_ID_TASK_SIMPLETASK,
                VTD1_Case_CarePlan__c = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN,
                VTD1_SC_Task_Delivery__c = VT_R4_ConstantsHelper_Tasks.RECORD_TYPE_ID_SC_TASK_DELIVERY
        );
        insert settings;

        insert new Task(
                RecordTypeId = settings.VTD1_Task_SimpleTask__c,
                Status = 'Open',
                Type = 'Confirm Kit Shipping Address',
                OwnerId = patientUser.id
        );


        Test.stopTest();

    }

    @IsTest
    static void handleChangeTelevisitBufferOnUserTest() {
        insert new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'N578',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Notification').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'User',
                VTD2_T_Receiver__c = 'User.Id',
                VTD2_T_Reminder_Set__c = false,
                VTD2_T_IsRequestVisitRedirect__c = false,
                VTD2_T_My_Task_List_Redirect__c = null,
                VTD2_T_Title__c = 'VTD2_Visits',
                VTD2_T_Message__c = 'VTD2_YourTelevisitStarting',
                VTD2_T_Link_to_related_event_or_object__c = 'User.Televisit_Buffer__c',
                VTD2_T_Type__c = 'Televisit',
                VTR2_Pass_if_empty_recipient__c = true
        );
        Video_Conference__c videoConference = new Video_Conference__c();

        Test.startTest();

        insert videoConference;
        User caregiver = [
                SELECT ProfileId, Televisit_Buffer__c
                FROM User
                WHERE Contact.FirstName = 'Caregiver'
        ];
        User mockOldUserCaregiver = new User(Id = caregiver.Id);
        caregiver.Televisit_Buffer__c = videoConference.Id;
        System.assert([SELECT COUNT() FROM VTD1_NotificationC__c] == 0);
        VT_R4_UserProcessesAndFlows.handleChangeTelevisitBufferOnUser(
                new List<User>{caregiver},
                new Map<Id, User>{mockOldUserCaregiver.Id => mockOldUserCaregiver});

        Test.stopTest();

        System.assert([SELECT COUNT() FROM VTD1_NotificationC__c] == 1);
    }

    @IsTest
    static void handleWelcomeKitSCTaskGenerationTest() {
        User patient = [
                SELECT ProfileId, VTD1_Form_Updated__c, Account.HealthCloudGA__CarePlan__c
                FROM User
                WHERE Contact.FirstName = 'Patient'
        ];
        Id caseId = patient.Account.HealthCloudGA__CarePlan__c;
        List<VTD1_Order__c> patientDeliveries = new List<VTD1_Order__c>();
        patientDeliveries.add(new VTD1_Order__c(
                VTD1_Case__c = caseId,
                VTD1_isWelcomeToStudyPackage__c = true,
                VTD1_Status__c = 'Pending Shipment'
        ));
        patientDeliveries.add(new VTD1_Order__c(
                VTD1_Case__c = caseId,
                VTD1_containsDevice__c = true,
                VTD1_Status__c = 'Pending Shipment'
        ));
        patientDeliveries.add(new VTD1_Order__c(
                VTD1_Case__c = caseId,
                VTD1_isPackageDelivery__c = true,
                VTD1_Kit_Type__c = 'IMP',
                VTD1_Status__c = 'Pending Shipment'
        ));
        patientDeliveries.add(new VTD1_Order__c(
                VTD1_Case__c = caseId,
                VTD1_isPackageDelivery__c = true,
                VTD1_Kit_Type__c = 'Lab',
                VTD1_Status__c = 'Pending Shipment'
        ));
        // parent delivery for recurring scenario
        patientDeliveries.add(new VTD1_Order__c(
                VTD1_Case__c = caseId,
                VTD1_isIMP__c = true
        ));
        // parent delivery for recurring scenario
        patientDeliveries.add(new VTD1_Order__c(
                VTD1_Case__c = caseId,
                VTD1_isLab__c = true
        ));

        Test.startTest();

        insert patientDeliveries;
        insert new List<VTD1_Order__c>{
                new VTD1_Order__c(
                        VTD1_Case__c = caseId,
                        VTD1_Patient_Delivery__c = patientDeliveries[4].Id,
                        VTD1_isPackageDelivery__c = true,
                        VTD1_Status__c = 'Pending Shipment'
                ),
                new VTD1_Order__c(
                        VTD1_Case__c = caseId,
                        VTD1_Patient_Delivery__c = patientDeliveries[5].Id,
                        VTD1_isPackageDelivery__c = true,
                        VTD1_Status__c = 'Pending Shipment'
                )
        };
        User mockOldUser = new User(Id = patient.Id, VTD1_Form_Updated__c = false);
        patient.VTD1_Form_Updated__c = true;
        Task t = [SELECT Status FROM Task WHERE OwnerId = :patient.Id];
        System.assertEquals('Open', t.Status);



        delete [SELECT Id FROM VTD1_SC_Task__c];
        System.debug('Limits.getQueries() '+Limits.getQueries());
        VT_R4_UserProcessesAndFlows.handleWelcomeKitSCTaskGeneration(new List<User>{patient}, new Map<Id, User>{mockOldUser.Id => mockOldUser});
        Test.stopTest();



        System.assertEquals('Completed', [SELECT Status FROM Task WHERE Id = :t.Id].Status);
        System.assert([SELECT COUNT() FROM VTD1_SC_Task__c] == 6);
    }

    @IsTest
    static void handleCreatePGCapacityTest() {
        System.assert([SELECT COUNT() FROM VTD1_Patient_Guide_Capacity__c] == 0);
        Test.startTest();
        Id userId = new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME)
                .persist().Id;
        System.assert(VT_R4_UserProcessesAndFlows.getRandomInteger() != null);
        System.schedule(
                'PGCapacityScheduler Test',
                '0 0 0 * * ?',
                new VT_R4_PGCapacityScheduler(new Set<Id>{userId})
        );
        Test.stopTest();
        System.assert([SELECT COUNT() FROM VTD1_Patient_Guide_Capacity__c] == 2);
    }
}