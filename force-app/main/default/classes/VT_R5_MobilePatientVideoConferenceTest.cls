/**
 * @author: Alexander Komarov
 * @date: 25.11.2020
 * @description:
 */

@IsTest
public with sharing class VT_R5_MobilePatientVideoConferenceTest {

    public static void firstTest() {
        User user = [SELECT Id, ContactId, Contact.Account.Id FROM User WHERE Username = :VT_R5_AllTestsMobile.PT_USERNAME LIMIT 1];

        VT_R2_HttpCalloutMockImpl httpMock = new VT_R2_HttpCalloutMockImpl();
        Test.setMock(HttpCalloutMock.class, httpMock);
        httpMock.response = new HttpResponse();
        httpMock.response.setStatus('OK');
        httpMock.response.setStatusCode(200);
        httpMock.response.setBody('{"message":"SOMETOKEN"}');

        System.runAs(user) {
            Test.startTest();
            VT_R5_AllTestsMobile.setRestContext('/patient/v1/videoConference/token', 'GET');
            RestContext.request.params.put('sessionId', 'sessionId');
            VT_R5_MobileRestRouter.doGET();
            Test.stopTest();
        }

    }
    public static void secondTest() {
        User user = [SELECT Id, ContactId, Contact.Account.Id FROM User WHERE Username = :VT_R5_AllTestsMobile.PT_USERNAME LIMIT 1];
        VTD1_Actual_Visit__c av = [SELECT Id FROM VTD1_Actual_Visit__c LIMIT 1];

        System.runAs(user) {
            Test.startTest();
            VT_R5_AllTestsMobile.setRestContext('/patient/v1/videoConference/sessionId', 'GET');
            RestContext.request.params.put('visitId', av.Id);
            VT_R5_MobileRestRouter.doGET();
            Test.stopTest();
        }
    }
    public static void thirdTest() {
        User user = [SELECT Id, ContactId, Contact.Account.Id FROM User WHERE Username = :VT_R5_AllTestsMobile.PT_USERNAME LIMIT 1];
        System.runAs(user) {
            Test.startTest();
            VT_R5_AllTestsMobile.setRestContext('/patient/v1/videoConference/info', 'GET');
            VT_R5_MobileRestRouter.doGET();
            Test.stopTest();
        }
    }
}