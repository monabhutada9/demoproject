@IsTest
private class VT_D2_PatientPaymentsControllerTest {
    public static final String RECORD_TYPE_ID_PATIENT_PAYMENT_COMPENSATION = Schema.SObjectType.VTD1_Patient_Payment__c.getRecordTypeInfosByDeveloperName().get('VTD1_Compensation').getRecordTypeId();
    public static final String RECORD_TYPE_ID_PATIENT_PAYMENT_REIMBURSEMENT = Schema.SObjectType.VTD1_Patient_Payment__c.getRecordTypeInfosByDeveloperName().get('VTD1_Reimbursement').getRecordTypeId();
    public static final String PATIENT_PAYMENT_NOT_STARTED = System.Label.VTD1_NotStarted;
    public static final String PATIENT_PAYMENT_PAYMENT_ISSUED = System.Label.VTD1_PaymentIssued;
    public static final String PATIENT_PAYMENT_PATIENT_GUIDE_APPROVED = System.Label.VTD1_PatientGuideApproved;
    public static final String PATIENT_PAYMENT_IN_PROGRESS = System.Label.VTD2_InProgress;

    @testSetup
    static void setup() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
        Case cas = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];

        VTD1_Patient_Payment__c patientPayment1 = new VTD1_Patient_Payment__c();
        patientPayment1.VTD1_Clinical_Study_Membership__c = cas.Id;
        patientPayment1.RecordTypeId = RECORD_TYPE_ID_PATIENT_PAYMENT_COMPENSATION;
        patientPayment1.VTD1_Activity_Complete__c = false;
        patientPayment1.VTD1_Amount__c = 10;
        insert patientPayment1;

        VTD1_Patient_Payment__c patientPayment2 = new VTD1_Patient_Payment__c();
        patientPayment2.VTD1_Clinical_Study_Membership__c = cas.Id;
        patientPayment2.RecordTypeId = RECORD_TYPE_ID_PATIENT_PAYMENT_REIMBURSEMENT;
        patientPayment2.VTD1_Activity_Complete__c = true;
        patientPayment2.VTD1_Payment_Issued__c = true;
        patientPayment2.VTD1_Amount__c = 20;
        insert patientPayment2;
    }

    @IsTest
    static void getPatientPaymentsTest(){
        Test.startTest();
        Case cas = [SELECT Id, VTD1_PI_user__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];
        insert new CaseShare(CaseId = cas.Id, UserOrGroupId = cas.VTD1_PI_user__c, CaseAccessLevel = 'Edit', RowCause = 'Manual');

        System.runAs(new User(Id = cas.VTD1_PI_user__c)) {
            System.assertEquals(1, VT_D1_PatientPaymentsControllerRemote.getPatientPayments('type-Compensation').size());
            System.assertEquals(1, VT_D1_PatientPaymentsControllerRemote.getPatientPayments('type-Reimbursement').size());
            System.assertEquals(1, VT_D1_PatientPaymentsControllerRemote.getPatientPayments('status-' + PATIENT_PAYMENT_NOT_STARTED).size());
            System.assertEquals(1, VT_D1_PatientPaymentsControllerRemote.getPatientPayments('status-' + PATIENT_PAYMENT_PAYMENT_ISSUED).size());
            System.assertEquals(0, VT_D1_PatientPaymentsControllerRemote.getPatientPayments('status-' + PATIENT_PAYMENT_PATIENT_GUIDE_APPROVED).size());
            System.assertEquals(0, VT_D1_PatientPaymentsControllerRemote.getPatientPayments('status-' + PATIENT_PAYMENT_IN_PROGRESS).size());
        }
    }
}