/***********************************************************************
* @author              Akanksha Singh <akanksha.singh@quintiles.com>
* @date                17-Aug-2020
* @group               SH-13732
* @group-content       http://jira.quintiles.net/browse/SH-13732
* @description         update 'Baseline Visit','First Onboarding Visit Complete Date' and 
*                      'Last Onboarding Visit Complete Date' fields of Case object 
*/
public class VT_R5_UpdateCaseFieldForExistingRecord implements Database.Batchable<sObject> {
    
    // Getting the record type id of the Protocol Visit of type onboarding
	public static Id onboardingRecordTypeId = Schema.SObjectType.VTD1_ProtocolVisit__c
        									  .getRecordTypeInfosByDeveloperName()
        									  .get('VTD1_Onboarding')
        									  .getRecordTypeId();
	
    public static Database.QueryLocator start(Database.BatchableContext BC) {
        
        String query = ' SELECT Id, '
                    +           ' VTD1_Case__c, '
            		+           ' VTD1_Status__c, '
                    +           ' VTD1_Protocol_Visit__r.RecordTypeId, '
                    +           ' VTD1_Visit_Completion_Date__c, '
					+  			' VTD1_Case__r.VTR5_LastOnboardingVisitCompleteDate__c, '
					+ 			' VTD1_Case__r.VTR5_FirstOnboardingVisitCompleteDate__c '
                    +           ' FROM VTD1_Actual_Visit__c '
                    +           ' WHERE VTD1_Case__r.VTD2_Baseline_Visit_Complete__c = True'
                    +           ' AND VTD1_Case__r.VTR5_Baseline_Visit_After_A_R_Flag__c = TRUE '
					+			' AND ((VTD1_Protocol_Visit__r.VTD1_VisitNumber__c = \'v1\') '
            		+			' OR (VTD1_Protocol_Visit__r.VTD1_Onboarding_Type__c <> \'N/A\')) '
            		+           ' ORDER BY VTD1_Case__c, ' 
                    +           ' VTD1_Visit_Completion_Date__c  '
                    +           ' ASC ' ;
       return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<VTD1_Actual_Visit__c> scope) {
        
        if(scope.size() > 0) {
            Map<Id, List<VTD1_Actual_Visit__c>> caseAndOnboardingActualVisitMap = new Map<Id, List<VTD1_Actual_Visit__c>>();
			Map<Id, VTD1_Actual_Visit__c> caseAndProtocolActualVisitMap = new Map<Id, VTD1_Actual_Visit__c>();
			
            List<Case> caseToUpdateList = new List<Case>(); 
			
		// Processing all the Actual Visit records amongst which the dates would be recorded	
        for(VTD1_Actual_Visit__c actualVisitObj : scope) {
			if (actualVisitObj.VTD1_Protocol_Visit__r.RecordTypeId != onboardingRecordTypeId )
			{
				caseAndProtocolActualVisitMap.put(actualVisitObj.VTD1_Case__c,actualVisitObj);
            }
			else
			{
				 if(!caseAndOnboardingActualVisitMap.containsKey(actualVisitObj.VTD1_Case__c)) 
                 {
                    caseAndOnboardingActualVisitMap.put(actualVisitObj.VTD1_Case__c, new List<VTD1_Actual_Visit__c>());
                 }
                caseAndOnboardingActualVisitMap.get(actualVisitObj.VTD1_Case__c).add(actualVisitObj);
            }
			}
            
            if(caseAndOnboardingActualVisitMap.keySet().size() > 0) { 
                Integer size;
                for(Id caseId: caseAndOnboardingActualVisitMap.keySet()) {
                    
                   // Getting the size of the list of the Actual Visits related to the case 
                    size = caseAndOnboardingActualVisitMap.get(caseId).size();
                    Case caseRecordToUpdate = new Case();
                    caseRecordToUpdate.Id = caseId;
                    
				    // Getting the last onboarding visit completion date
                    if(caseAndProtocolActualVisitMap.containsKey(caseId))
                    {
					if(caseAndProtocolActualVisitMap.get(caseId).VTD1_Status__c != 'Completed')
					{
					caseRecordToUpdate.VTR5_LastOnboardingVisitCompleteDate__c = caseAndOnboardingActualVisitMap
                                                                                    .get(caseId)[size-1]
                                                                                    .VTD1_Visit_Completion_Date__c;
                                                                                    
                    // Getting the first onboarding visit completion date
                    caseRecordToUpdate.VTR5_FirstOnboardingVisitCompleteDate__c = caseAndOnboardingActualVisitMap
                                                                                    .get(caseId)[0]
                                                                                    .VTD1_Visit_Completion_Date__c;
					caseRecordToUpdate.VTD2_Baseline_Visit_Complete__c = False;																
                    caseToUpdateList.add(caseRecordToUpdate);
                       
					}
                    }
                }
            }
            
            // If the case to update list is not blank then performing Database.update
            if(caseToUpdateList.size() > 0) {
                try {
                    Database.SaveResult[] srList = Database.update(caseToUpdateList, false);
                    for (Database.SaveResult sr : srList) {
                        if (sr.isSuccess()) {
                          System.debug('Successfully updated case ID: ' + sr.getId());
                        } else {
                            System.debug('Error in case ID: ' + sr.getId());
                        }
                   }
                }
                catch(Exception ex) {
                    String message = 'Exception for updating the case record with First OR Last Onboarding Visit Completion date.';
                    System.debug(message + ' Error message: '+ ex.getMessage());
                }
            }
        }
    }
    
    public void finish(Database.BatchableContext BC) {
    }
    
    
    
}