/**
 * Created by User on 19/05/20.
 */
@isTest
public with sharing class VT_D2_EmailAboutNewMessagesScheduleTest {

    public static void doTest(){
        Test.startTest(); 
        System.schedule('VT_D2_EmailAboutNewMessagesSchedule', '0 0 * * * ?',  new VT_D2_EmailAboutNewMessagesSchedule());
        Test.stopTest();
    }
}