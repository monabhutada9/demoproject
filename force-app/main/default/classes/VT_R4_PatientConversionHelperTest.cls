@IsTest
private class VT_R4_PatientConversionHelperTest {

    @TestSetup
    static void setup() {
        System.debug('setup: ' + Limits.getQueries());
        VT_D1_TestUtils.prepareStudy(1);
        System.debug('setup: ' + Limits.getQueries());
        //VT_D1_TestUtils.createTestPatientCandidate(1);
        System.debug('setup: ' + Limits.getQueries());
        Test.startTest();
        System.debug('setup before: ' + Limits.getQueries());
        VT_D1_TestUtils.createTestPatientCandidate(1, true);
        System.debug('setup1: ' + Limits.getQueries());
        Test.stopTest();
    }

    @IsTest static void finalizeTransferSuccess() {
        List<HealthCloudGA__CandidatePatient__c> candidatePatientList = getCandidatePatientTransfer();
        if (candidatePatientList.isEmpty()) {
            return;
        }
        HealthCloudGA__CandidatePatient__c candidatePatient = candidatePatientList[0];
        candidatePatient.Conversion__c = 'Converted';
        Test.startTest();
        update candidatePatient;
        Test.stopTest();
        System.assertEquals('Converted', [SELECT Conversion__c FROM  HealthCloudGA__CandidatePatient__c WHERE VTR4_CaseForTransfer__c != NULL][0].Conversion__c);
    }

    @IsTest static void finalizeTransferSuccessByEvent() {
        List<HealthCloudGA__CandidatePatient__c> candidatePatientList = getCandidatePatientTransfer();
        if (candidatePatientList.isEmpty()) {
            return;
        }
        HealthCloudGA__CandidatePatient__c candidatePatient = candidatePatientList[0];
        candidatePatient.Conversion__c = 'Converted';
        Test.startTest();
        List <VTR4_New_Transaction__e> events = new List<VTR4_New_Transaction__e>();
        VT_R4_PatientTransferService.PatientTransferServiceWrapper wrapper = new VT_R4_PatientTransferService.PatientTransferServiceWrapper();
        wrapper.candidatePatientsTransfer = candidatePatient;
        wrapper.isSuccess = true;
        String wrapperJSON = JSON.serialize(wrapper);
        events.add(new VTR4_New_Transaction__e(VTR4_HandlerName__c = 'VT_R4_PatientConversionTransferHandler', VTR4_Parameters__c = wrapperJSON));
        VT_R4_PatientConversionTransferHandler handler = new VT_R4_PatientConversionTransferHandler();
        handler.call('', new Map<String, Object>{'events' => events});
        Test.stopTest();
    }

    @IsTest static void finalizeTransferFailedByEvent() {
        List<HealthCloudGA__CandidatePatient__c> candidatePatientList = getCandidatePatientTransfer();
        if (candidatePatientList.isEmpty()) {
            return;
        }
        HealthCloudGA__CandidatePatient__c candidatePatient = candidatePatientList[0];
        candidatePatient.Conversion__c = 'Converted';
        Test.startTest();
        List <VTR4_New_Transaction__e> events = new List<VTR4_New_Transaction__e>();
        VT_R4_PatientTransferService.PatientTransferServiceWrapper wrapper = new VT_R4_PatientTransferService.PatientTransferServiceWrapper();
        wrapper.candidatePatientsTransfer = candidatePatient;
        wrapper.isSuccess = false;
        String wrapperJSON = JSON.serialize(wrapper);
        events.add(new VTR4_New_Transaction__e(VTR4_HandlerName__c = 'VT_R4_PatientConversionTransferHandler', VTR4_Parameters__c = wrapperJSON));
        VT_R4_PatientConversionTransferHandler handler = new VT_R4_PatientConversionTransferHandler();
        handler.call('', new Map<String, Object>{'events' => events});
        Test.stopTest();
    }

    @IsTest static void finalizeTransferFail() {
        Test.startTest();
        Case caseRecord = new Case();
        insert caseRecord;
        HealthCloudGA__CandidatePatient__c candidatePatient = new HealthCloudGA__CandidatePatient__c();
        candidatePatient.rr_firstName__c = 'test';
        candidatePatient.rr_lastName__c = 'test';
        candidatePatient.rr_Username__c = 'test';
        candidatePatient.rr_Email__c = 'test@test.com';
        candidatePatient.rr_Alias__c = 'test';
        candidatePatient.Conversion__c = 'Draft';
        candidatePatient.VTD1_ProtocolNumber__c = 'test';
        candidatePatient.VTD1_Patient_Phone__c = 'test';
        candidatePatient.HealthCloudGA__Address1Country__c = 'test';
        candidatePatient.HealthCloudGA__Address1PostalCode__c = 'test';
        candidatePatient.HealthCloudGA__Address1State__c = 'test';
        candidatePatient.HealthCloudGA__SourceSystemId__c = 'test';
        candidatePatient.VTD2_Language__c = 'en_US';

        candidatePatient.VT_R5_Secondary_Preferred_Language__c = 'es_US';
        candidatePatient.VT_R5_Tertiary_Preferred_Language__c = 'fr_CA';
        candidatePatient.VT_R5_Caregiver_Secondary_Language__c = 'pt_PT';
        candidatePatient.VT_R5_Caregiver_Tertiary_Language__c = 'ko';

        candidatePatient.VTR4_CaseForTransfer__c = caseRecord.Id;
        candidatePatient.VTR4_WillMedRecordsCarryOver__c = true;
        candidatePatient.VTR4_StatusCreate__c = 'Failed';
        candidatePatient.VTR4_StatusGPP__c = 'Failed';
        candidatePatient.VTR4_StatusSharing__c = 'Failed';
        candidatePatient.VTR4_StatusSTA__c = 'Failed';
        candidatePatient.VTR4_StatusStudyStratification__c = 'Failed';
        candidatePatient.VTR4_StatusStudyPhoneNumber__c = 'Failed';
        
        insert candidatePatient;

        candidatePatient.VTR4_StatusVisits__c = 'Failed';
        update candidatePatient;

        Case caseRecord2 = new Case();
        insert caseRecord2;
        HealthCloudGA__CandidatePatient__c candidatePatient2 = new HealthCloudGA__CandidatePatient__c();
        candidatePatient2.rr_firstName__c = 'test';
        candidatePatient2.rr_lastName__c = 'test';
        candidatePatient2.rr_Username__c = 'test';
        candidatePatient2.rr_Email__c = 'test@test.com';
        candidatePatient2.rr_Alias__c = 'test';
        candidatePatient2.Conversion__c = 'Draft';
        candidatePatient2.VTD1_ProtocolNumber__c = 'test';
        candidatePatient2.VTD1_Patient_Phone__c = 'test';
        candidatePatient2.HealthCloudGA__Address1Country__c = 'test';
        candidatePatient2.HealthCloudGA__Address1PostalCode__c = 'test';
        candidatePatient2.HealthCloudGA__Address1State__c = 'test';
        candidatePatient2.HealthCloudGA__SourceSystemId__c = 'test';
        candidatePatient2.VTD2_Language__c = 'en_US';

        candidatePatient2.VT_R5_Secondary_Preferred_Language__c = 'es_US';
        candidatePatient2.VT_R5_Tertiary_Preferred_Language__c = 'ja';
        candidatePatient2.VT_R5_Caregiver_Secondary_Language__c = 'pt_PT';
        candidatePatient2.VT_R5_Caregiver_Tertiary_Language__c = 'ko';

        candidatePatient2.VTR4_CaseForTransfer__c = caseRecord2.Id;
        candidatePatient2.VTR4_WillMedRecordsCarryOver__c = true;
        insert candidatePatient2;

        candidatePatient2.VTR4_StatusCreate__c = 'Finished';
        candidatePatient2.VTR4_StatusGPP__c = 'Finished';
        candidatePatient2.VTR4_StatusSharing__c = 'Finished';
        candidatePatient2.VTR4_StatusSTA__c = 'Finished';
        candidatePatient2.VTR4_StatusStudyStratification__c = 'Finished';
        candidatePatient2.VTR4_StatusStudyPhoneNumber__c = 'Finished';
        candidatePatient2.VTR4_StatusVisits__c = 'Failed';
        update candidatePatient;
        Test.stopTest();
        System.assert('Converted' != [SELECT Conversion__c FROM  HealthCloudGA__CandidatePatient__c WHERE VTR4_CaseForTransfer__c != NULL][0].Conversion__c);
    }

    @IsTest static void finalizeTransferCloneDocumentsBatch() {
        HealthCloudGA__CandidatePatient__c candidatePatientTransfer = getCandidatePatientTransfer()[0];
        Case oldCaseWithContId = [SELECT ContactId FROM Case WHERE Id = :candidatePatientTransfer.VTR4_CaseForTransfer__c][0];
        VTD1_Document__c doc1 = new VTD1_Document__c();
        doc1.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD;
        doc1.VTD1_Clinical_Study_Membership__c = candidatePatientTransfer.VTR4_CaseForTransfer__c;
        insert doc1;
        VTD1_Document__c doc = new VTD1_Document__c();
        doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD;
        doc.VTD1_Clinical_Study_Membership__c = candidatePatientTransfer.VTR4_CaseForTransfer__c;
        doc.VTD2_Certification__c = doc1.Id;
        insert doc;
        Id newCasedId = [SELECT Id, VTD1_Subject_ID__c, ContactId FROM Case WHERE ContactId = :oldCaseWithContId.ContactId ORDER BY CreatedDate DESC][0].Id;
        Database.executeBatch(new VT_R4_PatientTransferCloneDocuments(
                candidatePatientTransfer.VTR4_CaseForTransfer__c,
                newCasedId
        ), 3);
        System.assertEquals(newCasedId, doc.VTD1_Clinical_Study_Membership__c);
    }

    @IsTest static void finalizeTransferCloneFilesBatch() {
        HealthCloudGA__CandidatePatient__c candidatePatientTransfer = getCandidatePatientTransfer()[0];
        Case oldCaseWithContId = [SELECT ContactId FROM Case WHERE Id = :candidatePatientTransfer.VTR4_CaseForTransfer__c][0];
        VTD1_Document__c doc = new VTD1_Document__c();
        doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD;
        doc.VTD1_Clinical_Study_Membership__c = candidatePatientTransfer.VTR4_CaseForTransfer__c;
        insert doc;
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.PathOnClient = 'test.txt';
        contentVersion.Title = 'Test file';
        contentVersion.VersionData = Blob.valueOf('Test Data');
        insert contentVersion;

        Id contentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id].ContentDocumentId;

        /*
        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = doc.Id;
        contentDocumentLink.ContentDocumentId = contentDocumentId;
        contentDocumentLink.ShareType = 'I';
        contentDocumentLink.Visibility = 'AllUsers';
        insert contentDocumentLink;
        -- Validation is triggering: VT_D1_ContentDocumentLinkProcessHandler.validateOnlyOneContentDocumentOnDocument
        */
        Database.executeBatch(new VT_R4_PatientTransferCloneFiles(
                new Map<Id, VTD1_Document__c>{doc.Id => doc},
                [SELECT Id, VTD1_Subject_ID__c, ContactId FROM Case WHERE ContactId = :oldCaseWithContId.ContactId ORDER BY CreatedDate DESC][0].Id
        ), 1);
        System.assert(!new List<VTD1_Document__Share>([SELECT Id FROM VTD1_Document__Share WHERE ParentId = :doc.Id]).isEmpty());
    }

    private static List<HealthCloudGA__CandidatePatient__c> getCandidatePatientTransfer() {
        return [
                SELECT Id,
                        VTR4_CaseForTransfer__c,
                        rr_firstName__c,
                        rr_lastName__c,
                        VTD1_Patient_Phone__c,
                        VTR2_Patient_Phone_Type__c,
                        Conversion__c,
                        VTD1_ProtocolNumber__c,
                     VTR2_Caregiver_Email__c,
                        VTR4_CaseForTransfer__r.VTD1_Patient_User__c
                    
                FROM HealthCloudGA__CandidatePatient__c
                WHERE VTR4_CaseForTransfer__c != NULL
        ];
    }

}