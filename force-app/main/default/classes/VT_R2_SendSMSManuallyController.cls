/**
 * Created by user on 25.02.2019.
 */

public with sharing class VT_R2_SendSMSManuallyController {
    @AuraEnabled
    public static String sendSMS(Id logSMSId) {
        return VT_R2_McloudBroadcaster.send(logSMSId);
    }
}