/*************************************************************************************
    @author: Vijendra Hire
    @Story: SH-14821
    @Description: This is the CRA Task Pool Controller Class. 
    @Date: 22.10.2020
****************************************************************************************/

public with sharing class VT_R5_CRATaskPoolController {
    public class TaskInfo {
        @AuraEnabled public List<String> categories = new List<String>();
        @AuraEnabled public VT_R5_PagingQueryHelper.ResultWrapper result = new VT_R5_PagingQueryHelper.ResultWrapper();
/*****************************************************************************
 	 * Story  : SH-14821
     * Method : setCategories
     * param  : N/A
     * description : This mothod is used to set the values categories variable.
*******************************************************************************/       
        public void setCategories() {
            VTD2_TaskCategoriesByProfile__c catsForProfileSetting = VTD2_TaskCategoriesByProfile__c.getInstance();
            if (catsForProfileSetting != null && catsForProfileSetting.VTD2_Categories__c != null) {
                Set<String> picklistVals = new Set<String>();
                for (Schema.PicklistEntry picklistItem : VTR5_CRA_Task__c.VTR5_Category__c.getDescribe().getPicklistValues()) {
                    picklistVals.add(picklistItem.getValue());
                }

                for (String cat : catsForProfileSetting.VTD2_Categories__c.split(';')) {
                    if (! String.iSBlank(cat)) {
                        cat = cat.trim();
                        if (picklistVals.contains(cat)) { this.categories.add(cat); }
                    }
                }
            }
        }
    }
    
/*******************************************************************************************
 	 * Story  : SH-14821
     * Method : getCRATasksFromPool
     * param  : - String category : Specifies the VTR5_Category__c value 
     * 			- String jsonParams : Specifies the Sobject name, offset value and Limit
     * description : This mothod is used to set the values categories variable.
*********************************************************************************************/
    @AuraEnabled
    public static String getCRATasksFromPool(String category, String jsonParams) {
        
        TaskInfo taskInfo = new TaskInfo();
        if (category == null) {
            taskInfo.setCategories();
        }
        taskInfo.result = getTasks(category, jsonParams);
        return JSON.serialize(taskInfo);
    }

/*******************************************************************************************
 	 * Story  : SH-14821
     * Method : getTasks
     * param  : - String category : Specifies the VTR5_Category__c value 
     * 			- String jsonParams : Specifies the Sobject name, offset value and Limit
     * description : This mothod is used to return the getTasks records.
*********************************************************************************************/    
    public static VT_R5_PagingQueryHelper.ResultWrapper getTasks(String category, String jsonParams) {
        // user groups
        List<String> groupIds = new List<String>();
        for (GroupMember gm : [SELECT GroupId
                                FROM GroupMember
                                WHERE UserOrGroupId = :UserInfo.getUserId() AND Group.Type != 'Queue']) {
            groupIds.add(gm.Id);
        }
        String query = 'SELECT Id, ' +
                'VTR5_Subject__c, ' +
                'VTR5_Study__c, ' +
                'VTR5_Study__r.Name, VTR5_Due_Date__c,' +
                'VTR5_Category__c ' +
                'FROM VTR5_CRA_Task__c ';
        
        String filter = 'OwnerId IN (SELECT GroupId ' +
                        'FROM GroupMember ' +
                		'WHERE ((UserOrGroupId = \'' + 
            			UserInfo.getUserId() + 
            			'\' OR UserOrGroupId IN  (\'' + 
            			String.join(groupIds,'\',\'') +  
            			'\'))' +
                		' AND Group.Type = \'Queue\'))';
        
        filter += category != null ? ' AND VTR5_Category__c IN (\'' + category + '\')' : '' ;
        String order = 'CreatedDate DESC';
        
        Map <String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(jsonParams);//new Map<String, Object>();
        params.put('filter', filter);
        params.put('order', order);
        params.put('groupBy', 'VTR5_Category__c');
        
        String sObjectName = (String)params.get('sObjectName');
        
        try {
            VT_R5_PagingQueryHelper.ResultWrapper wrapper = VT_R5_PagingQueryHelper.query(query, sObjectName, JSON.serialize(params));
            return wrapper;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
        }
    }

/****************************************************************************************************
 	 * Story  : SH-14821
     * Method : claimTasksRemote
     * param  : - List<Id> taskIds : Specifies the List of CRA Tasks Id's.
     * description : This mothod is used to Change the CRA Task Owner based passed List of taskIds.
*****************************************************************************************************/
    @AuraEnabled
    public static void claimTasksRemote (List<Id> taskIds) {
        List<VTR5_CRA_Task__c> tasksToUpdate = new List<VTR5_CRA_Task__c>();
        List<VTR5_CRA_Task__c> tasks =  [SELECT Id, OwnerId
                                        FROM VTR5_CRA_Task__c
                                        WHERE Id IN :taskIds AND OwnerId!=:UserInfo.getUserId()
                                        WITH SECURITY_ENFORCED ];
        for (VTR5_CRA_Task__c t : tasks) {
            if (t.OwnerId.getSobjectType().getDescribe().getName() == 'Group') {
                tasksToUpdate.add(t);
            }
        }
        for (VTR5_CRA_Task__c t : tasksToUpdate) {
            t.OwnerId = UserInfo.getUserId();
        }
        try {
            if (VT_Utilities.isUpdateable('VTR5_CRA_Task__c')) {
                update tasks;
            }
        } catch (Exception e) {
            ErrorLogUtility.logException(e, null, VT_R5_CRATaskPoolController.class.getName());
        }
    }
}