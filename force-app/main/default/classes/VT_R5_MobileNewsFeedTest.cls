/**
 * @author: Alexander Komarov
 * @date: 08.12.2020
 * @description:
 */

@IsTest
public with sharing class VT_R5_MobileNewsFeedTest {

    public static void firstTest() {
        User user = [SELECT Id, ContactId, Contact.Account.Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        Test.startTest();
        extendSetup();
        System.runAs(user) {
            new VT_R5_MobileNewsFeed().getMinimumVersions();
            VT_R5_AllTestsMobile.setRestContext('/mobile/v1/newsfeed', 'GET');
            RestContext.request.params.put('offset', '0');
            RestContext.request.params.put('limit', '10');
            VT_R5_MobileRouter.doGET();
            Map<String, Object> firstLevel = (Map<String, Object>) JSON.deserializeUntyped(RestContext.response.responseBody.toString());
            Map<String, Object> secondLevel = (Map<String, Object>) firstLevel.get('data');
            List<Object> thirdLevel = (List<Object>) secondLevel.get('items');
            System.assertEquals(1, thirdLevel.size());
        }
        Test.stopTest();
    }

    public static void secondTest() {
        User user = [SELECT Id, ContactId, Contact.Account.Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        Test.startTest();
        extendSetup();
        System.runAs(user) {
            VT_R5_AllTestsMobile.setRestContext('/mobile/v0/newsfeed', 'GET');
            VT_R5_MobileRouter.doGET();
            Map<String, Object> firstLevel = (Map<String, Object>) JSON.deserializeUntyped(RestContext.response.responseBody.toString());
            System.assertEquals('error_unsupported_version', firstLevel.get('error'));
        }
        Test.stopTest();
    }

    public static void thirdTest() {
        User user = [SELECT Id, ContactId, Contact.Account.Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        Test.startTest();
        extendSetup();
        System.runAs(user) {
            Knowledge__kav kk = [SELECT Id, VTR5_Source__c,Title FROM Knowledge__kav LIMIT 1];
            VT_R5_AllTestsMobile.setRestContext('/mobile/v1/newsfeed/' + kk.Id, 'GET');
            VT_R5_MobileRouter.doGET();
            Map<String, Object> firstLevel = (Map<String, Object>) JSON.deserializeUntyped(RestContext.response.responseBody.toString());
            Map<String, Object> secondLevel = (Map<String, Object>) firstLevel.get('data');
            System.assertEquals(kk.VTR5_Source__c, secondLevel.get('source'));
            System.assertEquals(kk.Title, secondLevel.get('title'));
        }
        Test.stopTest();
    }

    private static void extendSetup() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        List<Knowledge__kav> articlesToInsert = new List<Knowledge__kav>();
        Id recTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'PatientsNewsFeed' LIMIT 1].Id;
        articlesToInsert.add(new Knowledge__kav(
                RecordTypeId = recTypeId, IsVisibleInCsp = true, IsVisibleInPkb = true, IsVisibleInPrm = true, VTD2_Study__c = study.Id,
                Language = 'en_US', Title = 'News feed title 1', UrlName = 'News-feed-title-1',
                VTR5_ArticlePreviewImage__c = 'documentforce.com/servlet/rtaImage?eid=ka01w0000000bu8&amp;feoid=00N1w000001YF2n&amp;refid=0EM1w0000001chC" width="500"></img>',
                VTR5_Source__c = 'Youtube',
                VTR5_VisibleInStatus__c = 'Both',
                VTD1_Content__c = 'ARTICLE CONTENT News feed title 1'));
        articlesToInsert.add(new Knowledge__kav(
                RecordTypeId = recTypeId, IsVisibleInCsp = true, IsVisibleInPkb = true, IsVisibleInPrm = true, VTD2_Study__c = study.Id,
                Language = 'en_US', Title = 'News feed title 2', UrlName = 'News-feed-title-2',
                VTR5_ArticlePreviewImage__c = 'documentforce.com/servlet/rtaImage?eid=ka01w0000000bu8&amp;feoid=00N1w000002YF2n&amp;refid=0EM1w0000001chC" width="500"></img>',
                VTR5_Source__c = 'SomeSource',
                VTR5_VisibleInStatus__c = 'Pre-randomized',
                VTD1_Content__c = 'ARTICLE CONTENT News feed title 2'));
        insert articlesToInsert;

        List<Knowledge__kav> articlesToPublish = [SELECT KnowledgeArticleId FROM Knowledge__kav WHERE VTD2_Study__c = :study.Id];
        List<User> listUsers = [SELECT Id, ContactId FROM User WHERE UserPermissionsKnowledgeUser = TRUE AND IsActive = TRUE AND Profile.Name = 'System Administrator'];
        if (!listUsers.isEmpty()) {
            User userToPublishArticles = listUsers.get(0);
            System.runAs(userToPublishArticles) {
                for (Knowledge__kav k : articlesToPublish) {
                    KbManagement.PublishingService.publishArticle(k.KnowledgeArticleId, true);
                }
            }
        }
    }
}