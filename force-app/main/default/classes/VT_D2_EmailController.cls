public without sharing class VT_D2_EmailController {
    private User currentUser;
    private final String QQ_BROWSER_URL = VTD1_RTId__c.getInstance().VTR5_QQ_Browser_Install_Url__c;
    private final String SOGOU_BROWSER_URL = VTD1_RTId__c.getInstance().VTR5_Sogou_Browser_Install_Url__c;
    private final String CHROME_BROWSER_URL = VTD1_RTId__c.getInstance().VTR5_Google_Browser_Install_Url__c;
    public Id usrId { get; set; }

    // SH-17282 Study notification`s email has no phone number
    public VTD1_NotificationC__c notification {
        get;
        set {
            VT_R5_StudyNotificationEmailController controller = new VT_R5_StudyNotificationEmailController();
            controller.notificationId = value.Id;
            this.notification = controller.getnotificationRecord();
            this.usrId = value.OwnerId;
        }
    }

    public Id conId {
        get;
        set {
            this.conId = value;
            this.usrId = [
                    SELECT Id
                    FROM User
                    WHERE ContactId = :this.conId
                    LIMIT 1
            ][0].Id;
        }
    }

    public String getStudyPhoneNumber() {
        List<Case> cas = getCases();
        if (cas.size() > 0) {
            return cas[0].VTD2_Study_Phone_Number__c;
        } else {
            return '';
        }
    }
    public String getCommunityPatientUrl() {
        return VTD1_RTId__c.getInstance().VTD2_Patient_Community_URL__c;
    }
    public String getCommunityPiUrl() {
        return VTD1_RTId__c.getInstance().PI_URL__c;
    }
    public String getCommunityScrUrl() {
        return VTD1_RTId__c.getInstance().SCR_COMMUNITY_URL__c;
    }
    public String getFirstNameWelcome() {
        return getUserInfo().FirstName;
    }
    public String getLastNameWelcome() {
        return getUserInfo().LastName;
    }
    public String getProfileId() {
        return getUserInfo().ProfileId;
    }
    public String getUserCountry() {
        if (getUserInfo().Profile.Name == 'Caregiver') {
            List<Address__c> address = [SELECT Country__c FROM Address__c WHERE VTD1_Contact__r.AccountId = :getUserInfo().Contact.AccountId LIMIT 1];
            if (address[0].Country__c != null) {
                return address[0].Country__c;
            }
        }
        return getUserInfo().Country;
    }
    public String getStudyNickname() {
        VT_D1_TranslateHelper.translate(new List<User>{getUserInfo()}, getUserInfo().Contact.VTR2_Primary_Language__c);
        return getUserInfo().Contact.Account.Candidate_Patient__r.Study__r.VTD1_Protocol_Nickname__c;
    }
    public String getPowerPartnerStudyNickname(){
        String studyNickName = '';
        Datetime activeDateTime;
        for(Study_Team_Member__c stm : [SELECT Id, Study__r.VTD1_Protocol_Nickname__c, CreatedDate, 
                (SELECT Id, CreatedDate, NewValue FROM Histories WHERE Field = 'VTD1_Active__c') 
                FROM Study_Team_Member__c 
                WHERE User__c =: usrId 
                AND VTD1_Active__c = TRUE]){
            if(activeDateTime == null || stm.CreatedDate > activeDateTime){
                activeDateTime = stm.CreatedDate;
                studyNickName = stm.Study__r.VTD1_Protocol_Nickname__c;
            }
            for(Study_Team_Member__History stmHistory : stm.Histories){
                if(stmHistory.NewValue == true && (activeDateTime == null || stmHistory.CreatedDate > activeDateTime)){
                    activeDateTime = stmHistory.CreatedDate;
                    studyNickName = stm.Study__r.VTD1_Protocol_Nickname__c;
                }
            }
        }
        return studyNickName;
    }

    public String getPowerPartnerStudyPhone(){
        String userCountryCode = getUserInfo().CountryCode;
        String defaultCountryCode = 'US';
        Map<String, String> phoneMap = new Map<String, String>();

        for(VTR5_GeneralPhoneConfiguration__c phone : [SELECT VTR5_Phone_Number__c, VTR5_Country__c 
                FROM VTR5_GeneralPhoneConfiguration__c 
                WHERE VTR5_Country__c =: userCountryCode 
                OR VTR5_Country__c =: defaultCountryCode]){
            phoneMap.put(phone.VTR5_Country__c, phone.VTR5_Phone_Number__c);
        }

        return phoneMap.containsKey(userCountryCode) && String.isNotBlank(phoneMap.get(userCountryCode)) ? phoneMap.get(userCountryCode) :
                phoneMap.containsKey(defaultCountryCode) ? phoneMap.get(defaultCountryCode) : '';
    }

    public String getStudyPhoneNumberAccordingToUserProfile() {
        String profileName = getUserInfo().Profile.Name;
        String result = '';
        try {
            if (profileName == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME
                    || profileName == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME) {
                // SH-19862 Optimize Study Notification template
                result = notification.VTR4_CurrentPatientCase__r.VTD2_Study_Phone_Number__c;
            } else {
                result = getPowerPartnerStudyPhone();
            }
        } catch(Exception ex) { }
        return result;
    }

    public String getDownloadChinaBrowserLabel() {
        String label = System.Label.VTR5_DownloadChinaBrowser;
        label = label.replace('#LINK1_TAG_START', '<a href="' + QQ_BROWSER_URL + '">');
        label = label.replace('#LINK2_TAG_START', '<a href="' + SOGOU_BROWSER_URL + '">');
        return label.replaceAll('#LINK_TAG_END', '</a>');
    }
    
    public String getChromeLinkLabel() {
        String chromeLinkLabel = VT_D1_TranslateHelper.getLabelValue('VT_R5_Instructions', currentUser.LanguageLocaleKey);
        chromeLinkLabel = '<a href="' + CHROME_BROWSER_URL + '">' + chromeLinkLabel + '</a>';
        return chromeLinkLabel;
    }

    public String getDownloadChinaBrowsersEmailWelcome() {
        String chinaBrowsersLabel = VT_D1_TranslateHelper.getLabelValue('VTR5_downloadChinaBrowsersWelcome', currentUser.LanguageLocaleKey);
        String qqBrowserLabel = VT_D1_TranslateHelper.getLabelValue('VTR5_QQBrowser_China', currentUser.LanguageLocaleKey);
        String SogouBrowserLabel = VT_D1_TranslateHelper.getLabelValue('VTR5_SogouBrowser_China', currentUser.LanguageLocaleKey);
        chinaBrowsersLabel = chinaBrowsersLabel.replace('#QQ_Browser_link', '<a href="' + QQ_BROWSER_URL + '">' + qqBrowserLabel + '</a>');
        chinaBrowsersLabel = chinaBrowsersLabel.replace('#Sogou_Browser_link', '<a href="' + SOGOU_BROWSER_URL + '">' + SogouBrowserLabel + '</a>');
        return chinaBrowsersLabel;
    }

    public String getLastPasswordChangeDate() {
        return String.valueOf(
                getUserInfo().LastPasswordChangeDate
                        .format('MM/dd/yyyy HH:mm:ss', UserInfo.getTimeZone().toString())
        );
    }

    private User getUserInfo() {
        if (currentUser == null) {
            currentUser = [
                    SELECT
                            Id,
                            FirstName,
                            LastName,
                            Country,
                            CountryCode,
                            LanguageLocaleKey,
                            Contact.Account.Candidate_Patient__r.Study__r.VTD1_Protocol_Nickname__c,
                            Contact.VTR2_Primary_Language__c,
                            Contact.AccountId,
                            LastPasswordChangeDate,
                            ProfileId,
                            Profile.Name
                    FROM User
                    WHERE Id = :usrId
            ];
        }
        return currentUser;
    }
    public String getDeepLinkPart () {
        List<VTD1_General_Settings__mdt> generalSettingsList = [
                SELECT VTR4_DeepActivationLink__c
                FROM VTD1_General_Settings__mdt
        ];
        if (generalSettingsList.isEmpty()) return '';

        String herokuAppURL = generalSettingsList[0].VTR4_DeepActivationLink__c;
        if (herokuAppURL.endsWith('/')) {
            herokuAppURL = herokuAppURL.removeEnd('/');
        }
        return herokuAppURL + VT_R5_ConstantsHelper_Mobile.ACTIVATION_DEEP_LINK_PATH;
    }

    public String getUserLanguage () {
        return currentUser.Contact.VTR2_Primary_Language__c == null ? 'en_US' : currentUser.Contact.VTR2_Primary_Language__c;
    }

    public List<Case> getCases() {
        Id accId = [SELECT ContactId, Contact.AccountId FROM User WHERE Id = :usrId LIMIT 1].Contact.AccountId;

        return [
                SELECT Id,
                       VTD2_Study_Phone_Number__c,
                       VTD1_Virtual_Site__r.VTD1_Study__r.VTR5_eDiaryTool__c
                FROM Case
                WHERE VTD1_Patient_User__r.Contact.AccountId = :accId
        ];
    }


    public VTD1_NotificationC__c getNotificationRecord() { //to merge
        VT_R5_StudyNotificationEmailController controller = new VT_R5_StudyNotificationEmailController();//to merge
        controller.notificationId = notification.Id;//to merge
        return controller.getnotificationRecord();//to merge
    }//to merge


}