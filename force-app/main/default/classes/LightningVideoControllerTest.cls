@IsTest
private class LightningVideoControllerTest {

    public with sharing class CalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            if (request.getEndpoint().endsWith('/session')) {
                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'application/json');
                response.setBody('["test"]');
                response.setStatus('OK');
                response.setStatusCode(200);
                return response;
            } else if (request.getEndpoint().endsWith('/beta')) {
                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'application/json');
                response.setBody('{"token":"test-token"}');
                response.setStatus('OK');
                response.setStatusCode(200);
                return response;
            } else {
                throw new CalloutException('Unknown callout endpoint');
            }
        }
    }

    @TestSetup
    static void setupData() {
        Test.setMock(HttpCalloutMock.class, new CalloutMock());
        Video_Conference__c vc = new Video_Conference__c(
                SessionId__c = 'test-session-id'
        );

        insert vc;
    }

    @IsTest
    static void getPostToPusherData() {
        Video_Conference__c videoConference = [SELECT Id FROM Video_Conference__c LIMIT 1];

        Test.setMock(HttpCalloutMock.class, new CalloutMock());
        Test.startTest();
            Map<String, String> someMap = LightningVideoController.getPostToPusher(videoConference.Id);
        Test.stopTest();

        System.assertEquals('test-token', someMap.get('token'), 'Incorrect token');
    }
}