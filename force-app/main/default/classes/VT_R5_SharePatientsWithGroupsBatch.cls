/**
* @author: Carl Judge
* @date: 12-Oct-20
* @description:
**/

public without sharing class VT_R5_SharePatientsWithGroupsBatch implements Database.Batchable<SObject> {
    public static final String READ_ACCESS = 'Read';
    public static final String EDIT_ACCESS = 'Edit';

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
            SELECT Id,
                VTD1_Patient_User__c,
                Contact.AccountId,
                VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c,
                VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c
            FROM Case
            WHERE RecordType.DeveloperName = 'CarePlan'
            AND VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c != NULL
            AND Contact.AccountId != NULL
            AND VTD1_Patient_User__c != NULL
        ]);
    }

    public void execute(Database.BatchableContext bc, List<Case> carePlans) {
        List<Id> accIds = new List<Id>();
        for (Case cas : carePlans) {
            accIds.add(cas.Contact.AccountId);
        }
        Map<Id, List<Id>> cgsByAccount = new Map<Id, List<Id>>();
        for (User cgUser : [SELECT Id, Contact.AccountId FROM User WHERE Profile.Name = 'Caregiver' AND Contact.AccountId IN :accIds]) {
            if (!cgsByAccount.containsKey(cgUser.Contact.AccountId)) {
                cgsByAccount.put(cgUser.Contact.AccountId, new List<Id>());
            }
            cgsByAccount.get(cgUser.Contact.AccountId).add(cgUser.Id);
        }

        Map<Id, List<Id>> childCasesByCareplan = new Map<Id, List<Id>>();
        for (Case childCase : [
            SELECT Id, VTD1_Clinical_Study_Membership__c
            FROM Case
            WHERE VTD1_Clinical_Study_Membership__c IN :carePlans
            LIMIT :Limits.getLimitQueryRows() - Limits.getQueryRows()
        ]) {
            if (!childCasesByCareplan.containsKey(childCase.VTD1_Clinical_Study_Membership__c)) {
                childCasesByCareplan.put(childCase.VTD1_Clinical_Study_Membership__c, new List<Id>());
            }
            childCasesByCareplan.get(childCase.VTD1_Clinical_Study_Membership__c).add(childCase.Id);
        }

        List<CaseShare> caseShares = new List<CaseShare>();
        List<AccountShare> accShares = new List<AccountShare>();
        List<UserShare> userShares = new List<UserShare>();

        for (Case carePlan : carePlans) {
            caseShares.add(getCaseShare(carePlan.Id, carePlan.VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c, READ_ACCESS));
            caseShares.add(getCaseShare(carePlan.Id, carePlan.VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c, EDIT_ACCESS));
            accShares.add(getAccountShare(carePlan.Contact.AccountId, carePlan.VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c, READ_ACCESS));
            accShares.add(getAccountShare(carePlan.Contact.AccountId, carePlan.VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c, EDIT_ACCESS));
            userShares.add(getUserShare(carePlan.VTD1_Patient_User__c, carePlan.VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c, READ_ACCESS));
            userShares.add(getUserShare(carePlan.VTD1_Patient_User__c, carePlan.VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c, EDIT_ACCESS));
            if (childCasesByCareplan.containsKey(carePlan.Id)) {
                for (Id childCaseId : childCasesByCareplan.get(carePlan.Id)) {
                    caseShares.add(getCaseShare(childCaseId, carePlan.VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c, READ_ACCESS));
                    caseShares.add(getCaseShare(childCaseId, carePlan.VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c, EDIT_ACCESS));
                }
            }
            if (cgsByAccount.containsKey(carePlan.Contact.AccountId)) {
                for (Id cgUserId : cgsByAccount.get(carePlan.Contact.AccountId)) {
                    userShares.add(getUserShare(cgUserId, carePlan.VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c, READ_ACCESS));
                    userShares.add(getUserShare(cgUserId, carePlan.VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c, EDIT_ACCESS));
                }
            }
        }

        insert caseShares;
        insert accShares;
        insert userShares;
    }

    public void finish(Database.BatchableContext bc) {

    }

    private CaseShare getCaseShare(Id caseId, Id groupId, String accessLevel) {
        return new CaseShare(
            CaseId = caseId,
            CaseAccessLevel = accessLevel,
            UserOrGroupId = groupId
        );
    }

    private AccountShare getAccountShare(Id accountId, Id groupId, String accessLevel) {
        return new AccountShare(
            AccountId = accountId,
            UserOrGroupId = groupId,
            AccountAccessLevel = accessLevel,
            ContactAccessLevel = accessLevel,
            OpportunityAccessLevel = accessLevel
        );
    }

    private UserShare getUserShare(Id userId, Id groupId, String accessLevel) {
        return new UserShare(
            UserId = userId,
            UserAccessLevel = accessLevel,
            UserOrGroupId = groupId
        );
    }
}