@IsTest
private class VT_D1_EventListControllerTest {
    @testSetup
    static void setup() {
        HealthCloudGA__CarePlanTemplate__c study1 = VT_D1_TestUtils.prepareStudy(1);

        Study_Team_Member__c stm = [
            SELECT VTD1_Backup_PI__c, VTD1_PI_License__c, VTD1_Ready_For_Assignment__c,VTD1_Type__c, User__r.ContactId
            FROM Study_Team_Member__c
            WHERE VTD1_Backup_PI__c = false
            AND VTD1_Type__c = 'Primary Investigator'
        ];

        Test.startTest();
        //HealthCloudGA__CandidatePatient__c candidatePatient1 = VT_D1_TestUtils.createPatientCandidate('Mike1', 'Birn1', 'mikebirn1@test.com', 'mikebirn1@test.com', 'mikebir1', 'Converted', study1.Id, 'PN', '11-11-11', 'US', 'NY');
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }

    @IsTest
    static void getEventsTest() {
        User u;
        Test.startTest();
        List<Case> casesList = [SELECT VTD1_PI_user__c FROM Case];
        System.assert(casesList.size() > 0);
        if(casesList.size() > 0){
            Case cas = casesList.get(0);
            String userId = cas.VTD1_PI_user__c;
            u = [SELECT Id FROM User WHERE Id =: userId];

            Event event = new Event();
            event.OwnerId = u.Id;
            event.DurationInMinutes = 15;
            event.ActivityDateTime = System.now();
            insert event;

            Event eventBuffer = new Event();
            eventBuffer.OwnerId = u.Id;
            eventBuffer.DurationInMinutes = 15;
            eventBuffer.ActivityDateTime = System.now();
            eventBuffer.VTD1_IsBufferEvent__c = true;
            insert eventBuffer;

        }
        System.runAs(u){
            String eventObjListString = VT_D1_EventListController.getEvents();
            List<VT_D1_EventListController.EventObj> eventObjList = (List<VT_D1_EventListController.EventObj>) JSON.deserialize(eventObjListString, List<VT_D1_EventListController.EventObj>.class);

            String userTimeZone = VT_D1_EventListController.getTimeZone();

        }
        Test.stopTest();
    }
}