/**
* @author: Carl Judge
* @date: 13-Sep-18
* @description: 
**/

public without sharing class VT_D2_VisitRequestedTemplateController extends VT_D2_VisitEmailsAbstract{

    public Id visitId {get; set;}
    private VTD1_Actual_Visit__c visit;

    public override VTD1_Actual_Visit__c getVisit() {
        if (this.visit == null) {
            this.visit = [
                SELECT Id,
                    VTD1_Case__r.VTD1_PI_user__r.Name,
                    VTD1_Case__r.VTD1_PI_user__r.Title,
                    VTD1_Case__r.VTD1_PI_user__r.TimeZoneSidKey,
                    Unscheduled_Visits__r.VTD1_PI_user__r.Name,
                    Unscheduled_Visits__r.VTD1_PI_user__r.Title,
                    Unscheduled_Visits__r.VTD1_PI_user__r.TimeZoneSidKey,
                    VTD2_Case_Contact_Name__c,
                    VTD1_Study_name_formula__c,
                    RecordType.DeveloperName,
                    VTD1_Formula_Name__c,
                    VTD1_Protocol_Visit__r.VTD1_Range__c,
                    To_Be_Scheduled_Date__c,
                    VTD1_Visit_Duration__c,
                    VTD2_Patient_ID__c
                FROM VTD1_Actual_Visit__c
                WHERE Id = :this.visitId
            ];
        }
        return this.visit;
    }

    public List<VT_D2_VisitRequestedTemplate_Day> getDays() {
        List<VT_D2_VisitRequestedTemplate_Day> days = new List<VT_D2_VisitRequestedTemplate_Day>();

        String currentDate;
        VT_D2_VisitRequestedTemplate_Day currentDay;

        for (VTD2_Time_Slot__c item : [
            SELECT Id, VTD2_Timeslot_Date_Time__c
            FROM VTD2_Time_Slot__c
            WHERE VTD2_Actual_Visit__c = :this.visitId
            AND VTD2_Status__c = 'Proposed'
            ORDER BY VTD2_Timeslot_Date_Time__c
        ]) {
            String itemDate = item.VTD2_Timeslot_Date_Time__c.format('EEEE, d-MMM-yyyy', getPiTimezone());

            if (currentDate != itemDate) {
                if (currentDay != null) { days.add(currentDay); }

                currentDate = itemDate;
                currentDay = new VT_D2_VisitRequestedTemplate_Day(itemDate);
            }

            currentDay.timeSlots.add(getTimeSlot(item));
        }

        if (currentDay != null) { days.add(currentDay); }

        return days;
    }

    private VT_D2_VisitRequestedTemplate_Day.TimeSlot getTimeSlot(VTD2_Time_Slot__c slot) {
        Datetime startTime = slot.VTD2_Timeslot_Date_Time__c;
        Integer duration = 30;
        if (getVisit().VTD1_Visit_Duration__c != null) {
            duration = getVisit().VTD1_Visit_Duration__c.intValue();
        }
        Datetime endTime = slot.VTD2_Timeslot_Date_Time__c.addMinutes(duration);

        String piTimezone = getPiTimezone();

        return new VT_D2_VisitRequestedTemplate_Day.TimeSlot(
            (startTime.format('h:mm aa', piTimezone) +
            ' - ' + endTime.format('h:mm aa', piTimezone)).toLowerCase(),
            VisitRequestConfirmSite__c.getInstance().SiteURL__c + '?id=' + slot.Id + '&action=schedule'
        );
    }
}