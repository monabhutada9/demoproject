/**
 * Created by Viktar Bobich on 04-May-20.
 */



public with sharing class VT_R5_EConsentButtonController {
	static Contact cont = (Contact) new QueryBuilder(Contact.class)
			.addField(Contact.VTD1_Clinical_Study_Membership__c)
			.addConditions()
			.add(new QueryBuilder.CompareCondition(Contact.VTD1_UserId__c).eq(UserInfo.getUserId()))
			.endConditions()
			.namedQueryRegister('VT_R5_EConsentButtonController.cont')
			.toSObject();
	static List<VTD1_Actual_Visit__c> consentVitList = [
			SELECT Id, Unscheduled_Visits__r.VTD1_Virtual_Site__r.VTR5_ConsentPacketLink__c, VTD1_Case__r.VTD1_Virtual_Site__r.VTR5_ConsentPacketLink__c
			FROM VTD1_Actual_Visit__c
			WHERE (Unscheduled_Visits__c = :cont.VTD1_Clinical_Study_Membership__c OR VTD1_Case__c = :cont.VTD1_Clinical_Study_Membership__c)

            	AND VTD1_Status__c ='Scheduled'

					AND ((VTD1_Unscheduled_Visit_Type__c = 'Consent' OR VTD1_Unscheduled_Visit_Type__c = 'Re-Consent') OR VTD1_Onboarding_Type__c = 'Consent')
					AND VTD1_Scheduled_Date_Time__c <= :Datetime.now().addMinutes(1)
					AND VTD1_Scheduled_Visit_End_Date_Time__c > :Datetime.now()
	];



	/**
     * Check available scheduled at this mament eConsent visit and get eCosent packet link
     * @author Viktar Bobich
     * @param -
     * @date 04/05/2020
     * @issue 
     * @return ConsentWrapper
     */



	@AuraEnabled (cacheable=false)
	public static ConsentWrapper getConsentWrapper (){
		ConsentWrapper wrapper = new ConsentWrapper();
		wrapper.consentVisitStatus = !consentVitList.isEmpty();
		if(consentVitList.isEmpty()){
			wrapper.consentLink = null;
		}else{
			if(consentVitList.get(0).Unscheduled_Visits__r.VTD1_Virtual_Site__r.VTR5_ConsentPacketLink__c != null){
				wrapper.consentLink = consentVitList.get(0).Unscheduled_Visits__r.VTD1_Virtual_Site__r.VTR5_ConsentPacketLink__c;
			}else{
				wrapper.consentLink = consentVitList.get(0).VTD1_Case__r.VTD1_Virtual_Site__r.VTR5_ConsentPacketLink__c;
			}
		}
		return wrapper;
	}
	public class ConsentWrapper{
		@AuraEnabled
		public Boolean consentVisitStatus;
		@AuraEnabled
		public String consentLink;
	}
}