public class VT_D1_PatientKitsDevAndAccCounting {
    @InvocableMethod
    public static void recount(List<Id> caseIds){
    	// Recount Devices in Patient Kits
    	Map<Id, VTD1_Patient_Kit__c> patientKitsMap = new Map<Id, VTD1_Patient_Kit__c>([
    		SELECT Id  
        	FROM VTD1_Patient_Kit__c 
        	WHERE VTD1_Case__c IN :caseIds 
        	AND VTD1_Kit_Type__c IN ('Connected Devices', 'Study Hub Tablet')
        ]);
        for (Id pkId : patientKitsMap.keySet()) {
        	patientKitsMap.get(pkId).VTD1_Devices_Count__c = 0;
        }
        for (AggregateResult ar : [
        	SELECT VTD1_Patient_Kit__r.Id patientKitId, count(Id) devicesCount 
        	FROM VTD1_Patient_Device__c 
        	WHERE VTD1_Patient_Kit__c IN :patientKitsMap.keySet() 
        	GROUP BY VTD1_Patient_Kit__r.Id
        ]) {
        	patientKitsMap.get((Id)ar.get('patientKitId')).VTD1_Devices_Count__c = (Decimal)ar.get('devicesCount');
        }
        
        // Recount Accessories in Patient Devices
        Map<Id, VTD1_Patient_Device__c> patientDevicesMap = new Map<Id, VTD1_Patient_Device__c>([
    		SELECT Id  
        	FROM VTD1_Patient_Device__c 
        	WHERE VTD1_Patient_Kit__c IN :patientKitsMap.keySet() 
        ]);
        for (Id pdId : patientDevicesMap.keySet()) {
        	patientDevicesMap.get(pdId).VTD1_Accessories_Count__c = 0;
        }
        for (AggregateResult ar : [
        	SELECT VTD1_Patient_Device__r.Id patientDeviceId, count(Id) accessoriesCount 
        	FROM VTD1_Patient_Accessories__c 
        	WHERE VTD1_Patient_Device__r.Id IN :patientDevicesMap.keySet() 
        	GROUP BY VTD1_Patient_Device__r.Id
        ]) {
        	patientDevicesMap.get((Id)ar.get('patientDeviceId')).VTD1_Accessories_Count__c = (Decimal)ar.get('accessoriesCount');
        }
        
        try {
            update patientKitsMap.values();
            update patientDevicesMap.values();
        } catch (DmlException e){
            System.debug(e.getMessage());
        }
    }
}