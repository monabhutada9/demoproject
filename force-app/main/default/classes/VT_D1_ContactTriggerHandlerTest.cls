/**
 * Created by User on 19/06/06.
 */
@isTest
public with sharing class VT_D1_ContactTriggerHandlerTest {

    public static void doTest() {
        Boolean isInsert = false;
        Boolean isUpdate = true;
        Boolean isDelete = false;
        Boolean isUndelete = false;

        Contact con = initData();

        Test.startTest();
        update con;
        List<Contact> oldList = new List<Contact>();
        oldList.add(con);
        con.LastName = 'Test 2';
        update con;
        List<Contact> newList = new List<Contact>();
        newList.add(con);
        Map<Id, Contact> oldMap = new Map<Id, Contact>(oldList);
        Map<Id, Contact> newMap = new Map<Id, Contact>(newList);
        VT_D1_ContactTriggerHandler.processAfter(isInsert, isUpdate, isDelete, isUndelete, oldList, newList, oldMap, newMap);
        VT_D1_ContactTriggerHandler.updateUserData(newList, oldMap);
        Test.stopTest();
    }

    public static void testLanguage() {
        User patUser = [SELECT Id, ContactId, Email FROM User WHERE CreatedDate = TODAY AND ContactId != NULL AND CreatedById = :UserInfo.getUserId() AND Contact.RecordTypeId = :VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PATIENT LIMIT 1];
        //insert patUser;
        ///System.runAs(patUser) {
        update new Contact(Id = patUser.ContactId, VTR2_Primary_Language__c = 'fr');
        //System.assertEquals('fr', [SELECT LanguageLocaleKey FROM User WHERE Id = :patUser.Id].LanguageLocaleKey);
        //}
    }

    private static Contact initData() {
        //bind case to patient
        Case carePlan = getCase();
        List<Contact> patients = getContactList();
        patients[0].VTD1_Clinical_Study_Membership__c = carePlan.Id;
        update patients;

        //create Physician
        new DomainObjects.VTD1_Physician_Information_t()
                .setVTD1_Clinical_Study_Membership(carePlan.Id)
                .persist();

        //create Caregiver
        DomainObjects.Contact_t contactCaregiverT = new DomainObjects.Contact_t()
                .setVTD1_RelationshiptoPatient('Parent')
                .setEmail('test@email.com')
                .setFirstName('Tom')
                .setLastName('Jary')
                .setPhone('654654654')
                .addAccount(new DomainObjects.Account_t())
                .setRecordTypeName('Caregiver');
        Contact conCaregiver = (Contact) contactCaregiverT.persist();
        conCaregiver.VTD1_Account__c = patients[0].AccountId;
        update conCaregiver;
        return patients[0];
    }

    //-----------------SOQL-------------------//

    private static List<Contact> getContactList() {
        return [
                SELECT Id,
                        VTD1_Clinical_Study_Membership__c,
                        AccountId,
                        RecordType.Name
                FROM Contact
                WHERE AccountId != NULL
        ];
    }

    private static Case getCase() {
        return [
                SELECT Id
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
        ];
    }
}