/**
 * Created by User on 19/05/14.
 */
@isTest
public without sharing class VT_D1_PatientCommunityControllerTest {

    public static void getPatientUserPositiveTest() {
        User userSCR = [SELECT Id FROM User WHERE Profile.Name = 'Site Coordinator' AND Contact.LastName = 'Con1' LIMIT 1];

        Test.startTest();
        System.runAs(userSCR) {
            System.assertNotEquals(null, VT_D1_PatientCommunityController.getPatientUser());
        }
        Test.stopTest();
    }

    public static void getPatientUserNegativeTest() {
        Test.startTest();
        System.assertEquals(null, VT_D1_PatientCommunityController.getPatientUser());
        Test.stopTest();
    }

    public static void testNavigationTranslation() {
        Test.startTest();
        List<String> labelsToTest = new List<String>{'VTD2_Home', 'VTD1_Messages'};
        Map<String, String> getLabelTranslated = VT_D1_PatientCommunityController.getLabelTranslated(labelsToTest);
        System.assertEquals(getLabelTranslated.size(), 2);
        System.assertNotEquals(getLabelTranslated.get('VTD2_Home'), null);
        System.assertNotEquals(getLabelTranslated.get('VTD1_Messages'), null);
        Test.stopTest();
    }

    public static void testCommunityName() {
        Test.startTest();
        String communityName = VT_D1_PatientCommunityController.getCommunityName();
        Test.stopTest();
    }
}