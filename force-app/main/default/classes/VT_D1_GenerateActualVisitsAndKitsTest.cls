/**
 * Created by User on 19/06/30.
 */
@IsTest
private with sharing class VT_D1_GenerateActualVisitsAndKitsTest {
    @TestSetup
    static void setup() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        VT_D1_TestUtils.createProtocolDelivery(study.id);
        Test.stopTest();
        study.VTD1_Return_Process__c = 'Packaging Materials Needed';
        update study;

        List <VTD1_ProtocolVisit__c> visitsOriginal = new List<VTD1_ProtocolVisit__c>();
        List <VTD1_ProtocolVisit__c> visitsGeneric = new List<VTD1_ProtocolVisit__c>();
        VTD1_ProtocolVisit__c pVisitOriginal1 = new VTD1_ProtocolVisit__c(
                VTD1_Study__c = study.Id,
                VTD1_VisitDuration__c = '1 hour',
                VTR2_Visit_Participants__c = VTD1_ProtocolVisit__c.VTR2_Visit_Participants__c.getDescribe().getPicklistValues()[0].getValue(),
                VTD1_VisitType__c = 'Labs',
                VTD1_isArchivedVersion__c = false,
                VTR2_Modality__c = 'At Home',
                VTD1_Visit_Description__c = 'Original visit 1'
        );
        visitsOriginal.add(pVisitOriginal1);

        VTD1_ProtocolVisit__c pVisitOriginal2 = new VTD1_ProtocolVisit__c(
                VTD1_Study__c = study.Id,
                VTD1_VisitDuration__c = '1 hour',
                VTR2_Visit_Participants__c = VTD1_ProtocolVisit__c.VTR2_Visit_Participants__c.getDescribe().getPicklistValues()[0].getValue(),
                VTD1_VisitType__c = 'Labs',
                VTD1_isArchivedVersion__c = false,
                VTR2_Modality__c = 'At Home',
                VTD1_Visit_Description__c = 'Original visit 2'
        );
        visitsOriginal.add(pVisitOriginal2);

        insert visitsOriginal;

        List<Case> casesList = [
                SELECT VTD1_Patient_User__c, VTD1_Patient__r.VTD1_Patient_Name__c, VTD1_Study__c, VTD1_Virtual_Site__c
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
        ];
        System.assertNotEquals(0, casesList.size());
        Case cas = casesList[0];

        VTD1_Regulatory_Document__c regulatoryDocument1 = new VTD1_Regulatory_Document__c(
                VTD1_Document_Type__c = 'PA Signature Page',
                Name = 'Test Reg Doc',
                Level__c = 'Site',
                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI');
        VTD1_Regulatory_Document__c regulatoryDocument2 = new VTD1_Regulatory_Document__c(
                VTD1_Document_Type__c = 'Protocol Amendment',
                Name = 'Test Reg Doc 2',
                Level__c = 'Study',
                VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI');
        insert new List<VTD1_Regulatory_Document__c>{
                regulatoryDocument1, regulatoryDocument2
        };

        VTD1_Regulatory_Binder__c binder = new VTD1_Regulatory_Binder__c(
                VTD1_Care_Plan_Template__c = study.Id,
                VTD1_Regulatory_Document__c = regulatoryDocument1.Id);
        VTD1_Regulatory_Binder__c binder2 = new VTD1_Regulatory_Binder__c(
                VTD1_Care_Plan_Template__c = study.Id,
                VTD1_Regulatory_Document__c = regulatoryDocument2.Id);
        insert new List<VTD1_Regulatory_Binder__c>{
                binder, binder2
        };

        VTD1_Protocol_Amendment__c amendment1 = new VTD1_Protocol_Amendment__c(VTD1_Study_ID__c = study.Id);
        VTD1_Protocol_Amendment__c amendment2 = new VTD1_Protocol_Amendment__c(VTD1_Study_ID__c = study.Id);
        insert new List <VTD1_Protocol_Amendment__c>{
                amendment1, amendment2
        };

        VTD1_ProtocolVisit__c pVisitGeneric1 = new VTD1_ProtocolVisit__c(
                VTD1_Study__c = study.Id,
                VTD1_VisitDuration__c = '2 hour',
                VTR2_Visit_Participants__c = VTD1_ProtocolVisit__c.VTR2_Visit_Participants__c.getDescribe().getPicklistValues()[0].getValue(),
                VTD1_VisitType__c = 'Labs',
                VTD1_isArchivedVersion__c = false,
                VTR2_Modality__c = 'At Home',
                VTR4_Original_Version__c = pVisitOriginal1.Id,
                VTD1_Visit_Description__c = 'Generated visit 1',
                VTD1_Protocol_Amendment__c = amendment1.Id
        );
        visitsGeneric.add(pVisitGeneric1);

        VTD1_ProtocolVisit__c pVisitGeneric2 = new VTD1_ProtocolVisit__c(
                VTD1_Study__c = study.Id,
                VTD1_VisitDuration__c = '2 hour',
                VTR2_Visit_Participants__c = VTD1_ProtocolVisit__c.VTR2_Visit_Participants__c.getDescribe().getPicklistValues()[0].getValue(),
                VTD1_VisitType__c = 'Labs',
                VTD1_isArchivedVersion__c = false,
                VTR2_Modality__c = 'At Home',
                VTR4_Original_Version__c = pVisitOriginal2.Id,
                VTD1_Visit_Description__c = 'Generated visit 2',
                VTD1_Protocol_Amendment__c = amendment1.Id
        );
        visitsGeneric.add(pVisitGeneric2);

        VTD1_ProtocolVisit__c pVisitGeneric3 = new VTD1_ProtocolVisit__c(
                VTD1_Study__c = study.Id,
                VTD1_VisitDuration__c = '2 hour',
                VTR2_Visit_Participants__c = VTD1_ProtocolVisit__c.VTR2_Visit_Participants__c.getDescribe().getPicklistValues()[0].getValue(),
                VTD1_VisitType__c = 'Labs',
                VTD1_isArchivedVersion__c = false,
                VTR2_Modality__c = 'At Home',
                VTR4_Original_Version__c = pVisitOriginal2.Id,
                VTD1_Visit_Description__c = 'Generated visit 3',
                VTD1_Protocol_Amendment__c = amendment2.Id,
                VTR4_Remove_from_Protocol__c = true
        );
        visitsGeneric.add(pVisitGeneric3);

        insert visitsGeneric;

        dsfs__DocuSign_Status__c docuSignStatus = new dsfs__DocuSign_Status__c(
                dsfs__Envelope_Status__c = 'Completed'
        );
        insert docuSignStatus;

        VTD1_Document__c document1 = new VTD1_Document__c(
                VTD1_Site__c = cas.VTD1_Virtual_Site__c,
                VTD1_Protocol_Amendment__c = amendment1.Id,
                VTD1_Regulatory_Binder__c = binder.Id,
                VTD1_DocuSignStatus__c = docuSignStatus.Id,
                VTR4_IRB_Approved__c = true,
                RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT
        );

        VTD1_Document__c document2 = new VTD1_Document__c(
                VTD1_Site__c = cas.VTD1_Virtual_Site__c,
                VTD1_Protocol_Amendment__c = amendment2.Id,
                VTD1_Regulatory_Binder__c = binder.Id,
                VTD1_DocuSignStatus__c = docuSignStatus.Id,
                VTR4_IRB_Approved__c = true,
                RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT
        );

        insert new List <VTD1_Document__c>{
                document1, document2
        };

    }

    @IsTest
    private static void doTest() {
        List<Case> casesList = [SELECT VTD1_Patient_User__c, VTD1_Patient__r.VTD1_Patient_Name__c, VTD1_Study__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];
        System.assertNotEquals(0, casesList.size());
        Case cas = casesList[0];
        VT_R4_GenerateActualVisitsAndKits generator =new VT_R4_GenerateActualVisitsAndKits();
        Test.startTest();
        generator.execute(new List<Id> {cas.Id});
        generator.executeDeliveriesAndKits(new List<Id> {cas.Id});
        Test.stopTest();
        /**This for VT_R4_PatientKitTriggerHandler, VT_R4_PatientDeviceTriggerHandler, VT_R2_OrderTriggerHandler*/
        VTD1_Protocol_Kit__c protocolKit = [SELECT Id FROM VTD1_Protocol_Kit__c LIMIT 1];
        VTD1_Order__c order = [SELECT Id FROM VTD1_Order__c LIMIT 1];
        VTD1_ProtocolVisit__c protocolVisits = [SELECT VTD1_Study__c FROM VTD1_ProtocolVisit__c LIMIT 1];
        HealthCloudGA__EhrDevice__c ehrDevice = new HealthCloudGA__EhrDevice__c(
                VTD1_Protocol_Kit__c = protocolKit.Id,
                VTD1_Kit_Type__c = 'Connected Devices',
                VTD1_Care_Plan_Template__c = protocolVisits.VTD1_Study__c);
        insert ehrDevice;
        VTD1_Patient_Device__c pDevice = new VTD1_Patient_Device__c(
                VTD1_Device_Status__c = 'Pending',
                VTD1_Protocol_Device__c = ehrDevice.Id,
                VTD1_Patient_Delivery__c = order.Id,
                VTD1_Case__c = cas.Id,
                RecordTypeId = VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_DEVICE_AD_HOC_CONNECTED_DEVICE
        );
        insert pDevice;
        List<VTD1_Order__c> ordersList = [
                SELECT VTD1_IntegrationId__c, Name, VTD1_Case__r.VTD1_Subject_ID__c
                FROM VTD1_Order__c
                WHERE VTD1_Case__c = :cas.Id
        ];
        List<VTD1_Patient_Kit__c> kitList = [
                SELECT VTD1_Kit_Name__c, VTD1_IntegrationId__c
                FROM VTD1_Patient_Kit__c
                WHERE VTD1_Patient_Delivery__r.RecordTypeId = :VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_ORDER_PACKING_MATERIALS
        ];
        List<HealthCloudGA__EhrDevice__c> ehrDevices = [
                SELECT Name, VTD1_Protocol_Kit__c
                FROM HealthCloudGA__EhrDevice__c
                WHERE VTD1_Protocol_Kit__c = :protocolKit.Id
        ];
        List<VTD1_Patient_Device__c> patientDevices = [
                SELECT VTD1_IntegrationId__c, VTD1_Protocol_Device__r.Name
                FROM VTD1_Patient_Device__c
                WHERE VTD1_Protocol_Device__c = :ehrDevice.Id
                AND VTD1_Patient_Delivery__c = :order.Id
        ];
        List<Case> casList = [SELECT VTD1_Subject_ID__c FROM Case];
        List <VTD1_Actual_Visit__c> visits = [SELECT Id, VTD1_Protocol_Visit__r.VTD1_Visit_Description__c FROM VTD1_Actual_Visit__c];
        for (VTD1_Actual_Visit__c visit : visits) {
            System.debug('actual visit description = ' + visit.VTD1_Protocol_Visit__r.VTD1_Visit_Description__c);
        }
        /*System.assertEquals(ordersList[0].VTD1_IntegrationId__c, casList[0].VTD1_Subject_ID__c + '-' + ordersList[0].Name);
        System.assertEquals(kitList[0].VTD1_IntegrationId__c, ordersList[0].VTD1_Case__r.VTD1_Subject_ID__c + '-' + kitList[0].VTD1_Kit_Name__c);
        System.assertEquals(patientDevices[0].VTD1_IntegrationId__c, ordersList[0].VTD1_Case__r.VTD1_Subject_ID__c + '-' + ehrDevices[0].Name);*/
    }
}