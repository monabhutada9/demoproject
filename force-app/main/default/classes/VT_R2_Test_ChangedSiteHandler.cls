/**
* @author: Dmitry Prozorovskiy
* @date: 14-Feb-19
* @description: Test for ChangedSiteHandler class
**/

@isTest
public class VT_R2_Test_ChangedSiteHandler {
    private static Id accountRecordTypeIdOfSponsor = Account.getSObjectType().getDescribe().getRecordTypeInfosByName().get('Sponsor').getRecordTypeId();

    @TestSetup
    private static void setupMethod() { 
        Test.startTest();
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeId(accountRecordTypeIdOfSponsor)
                );
        DomainObjects.User_t user = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Site Coordinator');
        DomainObjects.Case_t caseT = new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .addVTD1_Primary_PG(new DomainObjects.User_t())
                .addPIUser(new DomainObjects.User_t())
                .addStudy(study)
                .addUser(user);
        DomainObjects.User_t userPI = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Primary Investigator');
        DomainObjects.StudyTeamMember_t stmPI = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(userPI)
                .setRecordTypeByName('PI');
        DomainObjects.StudyTeamMember_t stmPI2 = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(userPI)
                .setRecordTypeByName('PI');
        DomainObjects.VirtualSite_t virtSite = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .addStudyTeamMember(stmPI)
                .setStudySiteNumber('12345');
        DomainObjects.VirtualSite_t virtSite2 = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .addStudyTeamMember(stmPI2)
                .setStudySiteNumber('25245');
        virtSite2.persist();
        Test.stopTest();
    }

    @IsTest
    private static void changePatientPi() {
        List<Case> patients = [
                SELECT
                        id,
                        VTD1_Virtual_Site__c,
                        VTD1_PI_user__c
                FROM    Case
        ];
        List<Virtual_Site__c> virtualSites = [
                SELECT  Id,
                        VTD1_Study_Team_Member__c,
                        VTD1_Study_Team_Member__r.User__c,
                        VTD1_Study_Team_Member__r.User__r.ContactId
                FROM Virtual_Site__c
        ];
        if (patients.size() > 0 && virtualSites.size() > 1) {
            Case patient = patients[0];
            patient.VTD1_Virtual_Site__c = virtualSites[0].Id;
            update patient;

            Case patient2 = [
                    SELECT
                            Id,
                            VTD1_PI_user__c,
                            VTD1_Backup_PI_User__c
                    FROM    Case
                    WHERE   Id = :patient.Id
            ];
            Test.startTest();
            patient2.VTD1_Virtual_Site__c = virtualSites[1].Id;
            update patient2;

            Case patientToCheck = [
                    SELECT  Id,
                            VTD1_PI_user__c,
                            VTD1_Backup_PI_User__c
                    FROM    Case
                    WHERE   Id = :patient.Id
            ];
            Test.stopTest();
            System.assertEquals(patientToCheck.VTD1_PI_user__c, virtualSites[1].VTD1_Study_Team_Member__r.User__c,
                    'PI on case does not equal to PI on VS');
        }
    }
}