/**
* @author: Carl Judge
* @date: 10-Oct-18
* @description:Queue action to send docuemnts to DocuSign
**/
public class VT_R5_SendDocstoDocuSign extends VT_D1_AbstractAction {
    private Id docId;
    private Id dUserId;
    private String dSubject;
    private String dMessage;
    private String mvrId;
    public VT_R5_SendDocstoDocuSign(Id mvrId, Id docId, Id dUserId,String dSubject,String dMessage) {
        this.docId = docId;
        this.dUserId = dUserId;
        this.dSubject = dSubject;
        this.dMessage= dMessage;
        this.mvrId=mvrId;
    }
    public override void execute() {
        if(this.docId != null){
        system.debug('dSubject '+dSubject);
            VT_D1_HTTPConnectionHandler.Result result = VT_R5_CDocumentsCalloutHelper.sendCertifyDocument(this.mvrId,this.docId,this.dUserId,this.dSubject,this.dMessage);
            HttpResponse response = result.httpResponse;
            System.debug('response vtr5= ' + response);
            if(response == null) {
                //time out, action will not be added to queue:
                setStatus(STATUS_FAILED);
                setMessage(result.log.VTD1_ErrorMessage__c);
            }else if(response.getStatusCode() >= 500 && response.getStatusCode() < 600) {
                //internal server error, action will be added to queue:
                setStatus(STATUS_FAILED);
                setMessage('Server return status code: ' + response.getStatusCode() + ' ' + response.getStatus() + '; Body: ' + response.getBody());
                addToQueue();
            }else if(response.getStatusCode() == 201){
                //success
                setStatus(STATUS_SUCCESS);
            }else{
                //other errors, action will not be added to queue
                setStatus(STATUS_FAILED);
                setMessage('Server return: ' + response.getStatusCode() + ' ' + response.getStatus() + ', Body: ' + response.getBody());
                //TODO ADD CREATION OF TASK HERE
            }
        }
    }
    public override Type getType() {
        return VT_R5_SendDocstoDocuSign.class;
    }
}