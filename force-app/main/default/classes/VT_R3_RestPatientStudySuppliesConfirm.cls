/**
 * Created by Alexander Komarov on 20.05.2019.
 */

@RestResource(UrlMapping='/Patient/StudySupplies/Confirm/*')
global with sharing class VT_R3_RestPatientStudySuppliesConfirm {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();

    @HttpGet
    global static String getConfirmDetails() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        String temp = request.requestURI.substringAfterLast('/Confirm/');

        if (!helper.isIdCorrectType(temp, 'VTD1_Order__c')) {
            response.statusCode = 400;
            cr.buildResponse(helper.forAnswerForIncorrectInput());
            return JSON.serialize(cr, true);
        }
        try {
            Id deliveryId = temp;

            VTD1_Order__c delivery = [
                    SELECT Id, Name, VTD1_isPackageDelivery__c, RecordTypeId, (SELECT Id, Name FROM Patient_Kits__r)
                    FROM VTD1_Order__c
                    WHERE Id = :deliveryId
                    LIMIT 1
            ];

            String adHocDeliveryRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'VTD1_Ad_hoc_Packaging_Materials' AND SobjectType = 'VTD1_Order__c'].Id;

            Map<Id, String> kitsIdsToNames = new Map<Id, String>();

            for (VTD1_Patient_Kit__c pc : delivery.Patient_Kits__r) {
                kitsIdsToNames.put(pc.Id, pc.Name);
            }

            Map<Id, VTD1_Patient_Device__c> devicesMap = new Map<Id, VTD1_Patient_Device__c>([
                    SELECT Id, Name, VTD1_Model_Name__c, VTR5_DeviceKeyId__c,VTD1_Patient_Kit__c
                    FROM VTD1_Patient_Device__c
                    WHERE VTD1_Patient_Kit__c IN :kitsIdsToNames.keySet()
            ]);

            Map<Id, Set<Id>> kitToDevices = new Map<Id, Set<Id>>();
            for (VTD1_Patient_Device__c pd : devicesMap.values()) {
                if (!kitToDevices.containsKey(pd.VTD1_Patient_Kit__c)) {
                    kitToDevices.put(pd.VTD1_Patient_Kit__c, new Set<Id>());
                }
                kitToDevices.get(pd.VTD1_Patient_Kit__c).add(pd.Id);
            }

            Map<Id, String> deviceToAccessories = new Map<Id, String>();
            String strToFill;

            System.debug('devicesMap *** ' + devicesMap);
            for (VTD1_Patient_Accessories__c pa : [
                    SELECT
                            Id,
                            Name,
                            VTD1_Accessory_Qty__c,
                            VTD1_Patient_Device__c
                    FROM VTD1_Patient_Accessories__c
                    WHERE VTD1_Patient_Device__c IN :devicesMap.keySet()
            ]) {
                if (!deviceToAccessories.containsKey(pa.VTD1_Patient_Device__c)) {
                    strToFill = '';
                    strToFill += pa.Name + ' ' + '(Qty: ' + pa.VTD1_Accessory_Qty__c + ')';
                } else {
                    strToFill = deviceToAccessories.get(pa.VTD1_Patient_Device__c);
                    strToFill += ', ' + pa.Name + ' ' + '(Qty: ' + pa.VTD1_Accessory_Qty__c + ')';
                }
                deviceToAccessories.put(pa.VTD1_Patient_Device__c, strToFill);
            }

            Map<Id, DeviceItem> idToDeviceItems = new Map<Id, DeviceItem>();

            for (VTD1_Patient_Device__c pd : devicesMap.values()) {
                DeviceItem deviceItem = new DeviceItem();
                deviceItem.id = pd.Id;
                deviceItem.dName = pd.Name;
                deviceItem.dModel = pd.VTD1_Model_Name__c;
                deviceItem.serialNumber = pd.VTR5_DeviceKeyId__c;
                deviceItem.accessories = deviceToAccessories.get(pd.Id);
                idToDeviceItems.put(pd.Id, deviceItem);
            }

            DeliveryDetails dd = new DeliveryDetails();
            dd.deliveryId = delivery.Id;
            dd.isPackage = delivery.VTD1_isPackageDelivery__c;
            dd.isAdhoc = delivery.RecordTypeId.equals(adHocDeliveryRecordTypeId);
            dd.kits = new List<VT_R3_RestPatientStudySuppliesConfirm.KitsDetails>();

            List<DeviceItem> listDeviceItemsResult = new List<VT_R3_RestPatientStudySuppliesConfirm.DeviceItem>();

            //id kits
            if (kitToDevices.size() > 0) {
                for (Id i : kitToDevices.keySet()) {
                    KitsDetails kd = new KitsDetails();
                    kd.kitId = i;
                    kd.kitName = kitsIdsToNames.get(i);
                    listDeviceItemsResult.clear();
                    for (Id device : kitToDevices.get(i)) {
                        listDeviceItemsResult.add(idToDeviceItems.get(device));
                    }
                    kd.devices = listDeviceItemsResult;
                    dd.kits.add(kd);
                }
            } else {
                for (Id i : kitsIdsToNames.keySet()) {
                    KitsDetails kd = new KitsDetails();
                    kd.kitId = i;
                    kd.kitName = kitsIdsToNames.get(i);
                    kd.devices = listDeviceItemsResult;
                    dd.kits.add(kd);
                }
            }
            cr.buildResponse(dd);
            return JSON.serialize(cr, true);
        } catch (Exception e) {
            response.statusCode = 500;
            cr.buildResponse('FAILURE. Something is wrong.');
            return JSON.serialize(cr, true);
        }
    }

    @HttpPost
    global static String confirmDelivery() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        String temp = request.requestURI.substringAfterLast('/Confirm/');

        Map<String, String> paramsMap = request.params;
        Boolean updateDelivery = Boolean.valueOf(paramsMap.get('updateDelivery'));
        Boolean kitContainsAll;
        Boolean kitContentsDamaged;
        if (updateDelivery) {
            kitContainsAll = Boolean.valueOf(paramsMap.get('kitContainsAll'));
            kitContentsDamaged = Boolean.valueOf(paramsMap.get('kitContentsDamaged'));
        }


        if (!helper.isIdCorrectType(temp, 'VTD1_Order__c')) {
            response.statusCode = 400;
            cr.buildResponse(helper.forAnswerForIncorrectInput());
            return JSON.serialize(cr, true);
        }
        Id deliveryId = temp;
        VT_R3_RestPatientStudySuppliesConfirm.DeliveryData deliveryDataDeserialized;
        try {
            String requestBody = request.requestBody.toString();
            deliveryDataDeserialized = (VT_R3_RestPatientStudySuppliesConfirm.DeliveryData) JSON.deserialize(requestBody, VT_R3_RestPatientStudySuppliesConfirm.DeliveryData.class);
        } catch (Exception e) {
            response.statusCode = 452;
            cr.buildResponse('FAILURE. Bad request body.');
            return JSON.serialize(cr, true);
        }
        VTD1_Order__c delivery = [
                SELECT Id,VTD1_CommentsNotes__c, VTD1_ExpectedDeliveryDateTime__c,
                        VTD1_Kit_Contains_All_Contents__c, VTD1_Kit_Contents_Damaged__c, VTD1_isPackageDelivery__c,
                        RecordTypeId, Name
                FROM VTD1_Order__c
                WHERE Id = :deliveryId
        ];

        List<VTD1_Patient_Kit__c> kitsToUpdate = new List<VTD1_Patient_Kit__c>();
        if (!updateDelivery) {
            for (KitData kd : deliveryDataDeserialized.kitData) {
                VTD1_Patient_Kit__c k = new VTD1_Patient_Kit__c();
                k.Id = kd.kitId;
                k.VTD1_Kit_Contains_All_Contents__c = kd.containsAll;
                k.VTD1_Kit_Contents_Damaged__c = kd.damaged;
                k.VTD1_Kit_Not_Received__c = kd.notReceived;
                kitsToUpdate.add(k);
            }
        }
        delivery.VTD1_ExpectedDeliveryDateTime__c = Datetime.newInstance(deliveryDataDeserialized.deliveryDate - timeOffset);
        delivery.VTD1_CommentsNotes__c = deliveryDataDeserialized.comment;

        if (updateDelivery) {
            if (!kitContainsAll) {
                delivery.VTD1_Kit_Contains_All_Contents__c = false;
            } else {
                delivery.VTD1_Kit_Contains_All_Contents__c = true;
            }
            if (kitContentsDamaged) {
                delivery.VTD1_Kit_Contents_Damaged__c = true;
            } else {
                delivery.VTD1_Kit_Contents_Damaged__c = false;
            }
        }
        try {
            update kitsToUpdate;
            update delivery;

            Task [] tasks = [SELECT Status FROM Task WHERE OwnerId = :UserInfo.getUserId() AND WhatId = :delivery.Id];
            if (tasks.size() > 0) {
                for (Task task : tasks) {
                    task.Status = 'Completed';
                }
                update tasks;
            }
        } catch (Exception e) {
            response.statusCode = 500;
            cr.buildResponse('FAILURE.', 'Server error. Something went wrong.');
            return JSON.serialize(cr, true);
        }

        cr.buildResponse(VT_R3_RestPatientStudySupplies.getStudySuppliesList(), System.Label.VTR3_Success_Confirm_Delivery.replace('#1', delivery.Name));
        return JSON.serialize(cr, true);


    }

    public class DeliveryData {
        public Long deliveryDate;
        public String comment;
        public List<KitData> kitData;
    }

    public class KitData {
        public String kitId;
        public Boolean containsAll;
        public Boolean damaged;
        public Boolean notReceived;
    }

    private class DeliveryDetails {
        public String deliveryId;
        public Boolean isPackage;
        public Boolean isAdhoc;
        public List<KitsDetails> kits;
    }

    private class KitsDetails {
        public String kitId;
        public String kitName;
        public List<DeviceItem> devices;
    }

    private class DeviceItem {
        public String id;
        public String dName;
        public String dModel;
        public String serialNumber;
        public String accessories;
    }
}