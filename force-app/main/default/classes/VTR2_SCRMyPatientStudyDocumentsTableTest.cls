@IsTest
public with sharing class VTR2_SCRMyPatientStudyDocumentsTableTest {

    public static void testGetPatientDocuments() {
        Case caseRecord = (Case) new DomainObjects.Case_t().persist();
        VTD1_Document__c document = (VTD1_Document__c) new DomainObjects.VTD1_Document_t()
                .setVTD1_Clinical_Study_Membership(caseRecord.Id)
                .persist();
        new DomainObjects.ContentVersion_t().persist();
        new DomainObjects.ContentDocumentLink_t()
                .withLinkedEntityId(document.Id)
                .withContentDocumentId([SELECT ContentDocumentId FROM ContentVersion][0].ContentDocumentId)
                .persist();

        Test.startTest();
        String result = VTR2_SCRMyPatientStudyDocumentsTableCntr.getPatientDocuments(caseRecord.Id);
        Test.stopTest();

        System.assert(String.isNotEmpty(result));
    }
}