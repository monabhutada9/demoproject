/**
 * Created by Dmitry Yartsev on 12.03.2019.
 */

public with sharing class VT_R2_SiteCustomHeaderController {

    @AuraEnabled(Cacheable=true)
    public static String getCurrentUserProfile() {
        return [SELECT Name FROM Profile WHERE Id=:UserInfo.getProfileId()].Name;
    }

    @AuraEnabled(Cacheable=true)
    public static String getReconciliationLogId() {
        return VTD1_RTId__c.getInstance().VTD1_Report_Study_Drug_Account_Log_Site__c;
    }

}