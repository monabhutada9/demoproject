/**
 * Created by Dmitry Gavrilov on 2020.01.20.
 */

public with sharing class VT_R4_RegulatoryDocsCertifyHelper {
    public static VTD1_Document__c getDocument(Id docToCertifyId) {
        List<VTD1_Document__c> documents = [SELECT  Name,
                                                    VTD1_Study__c,
                                                    VTR4_Signature_Type__c,
                                                    VTD1_Clinical_Study_Membership__c,
                                                    VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c,
                                                    VTD1_Regulatory_Binder__c
                                            FROM VTD1_Document__c
                                            WHERE Id = :docToCertifyId];
        VTD1_Document__c result = new VTD1_Document__c();
        if(!documents.isEmpty()){
            result = documents[0];
        }
        return result;
    }


    public static ContentDocumentLink getCertifyContentDocumentLink(Id studyId) {
        List<VTR4_TemplateContainer__c> templateContainers = [
              SELECT VTR4_Type__c, VTR4_Study__c, (SELECT ContentDocumentId FROM ContentDocumentLinks LIMIT 1)
              FROM VTR4_TemplateContainer__c
              WHERE VTR4_Type__c = 'Regulatory Document' AND VTR4_Study__c = :studyId
              ORDER BY CreatedDate
              LIMIT 1
        ];
        if (!templateContainers.isEmpty()) {
            System.debug('templateContainers ' + templateContainers);

            List<ContentDocumentLink> contentDocumentLinks = [
                    SELECT ContentDocument.LatestPublishedVersionId, ContentDocument.LatestPublishedVersion.Title,ShareType, Visibility
                    FROM ContentDocumentLink
                    WHERE LinkedEntityId = :templateContainers[0].Id // 'a4x2i0000005mgeAAA' // 'a4x2i0000005mgjAAA'//:templateContainer[0].Id
            ];
            System.debug('contentDocumentLinkS ' + contentDocumentLinks);
            if (!contentDocumentLinks.isEmpty()) {
                return contentDocumentLinks[0];
            }
        } 
        return null;

    }

    @AuraEnabled
    public static Id certifyDocument(Id docToCertifyId) {
        System.debug('certifyDoc 002 ');
        try {
            VTD1_Document__c document = getDocument(docToCertifyId);
            ContentDocumentLink link = getCertifyContentDocumentLink(document.VTD1_Study__c);
            System.debug('link ' + link);
            List<ContentDocumentLink> documentCDLs = [
                    SELECT ContentDocument.LatestPublishedVersionId
                    FROM ContentDocumentLink
                    WHERE LinkedEntityId = :docToCertifyId
            ];
            if (link != null && !documentCDLs.isEmpty()) {
                VTD1_Document__c certifyCopy = new VTD1_Document__c(
                        RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON,
                        VTD1_Clinical_Study_Membership__c = document.VTD1_Clinical_Study_Membership__c,
                        VTD1_Study__c = document.VTD1_Study__c,
                        VTD1_Nickname__c = 'Certification form',
                        VTD1_PG_Approver__c = document.VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c,
                        VTD2_Associated_Medical_Records__c = document.Name,
                        VTD2_Associated_Medical_Record_id__c = docToCertifyId
                        //,VTD1_Regulatory_Binder__c = document.VTD1_Regulatory_Binder__c
                );
                insert certifyCopy;

                ContentVersion cvToClone = [
                        SELECT ContentLocation, PathOnClient, Title, VersionData
                        FROM ContentVersion
                        WHERE Id = :link.ContentDocument.LatestPublishedVersionId
                ];

                ContentVersion newCv = new ContentVersion(
                        ContentLocation = cvToClone.ContentLocation,
                        PathOnClient = cvToClone.PathOnClient,
                        Title = cvToClone.Title,
                        VersionData = cvToClone.VersionData
                );
                insert newCv;

                insert new ContentDocumentLink(
                        ContentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :newCv.Id].ContentDocumentId,
                        LinkedEntityId = certifyCopy.Id,
                        ShareType = link.ShareType,
                        Visibility = link.Visibility
                );
                System.debug('certifyCopy.Id ' + certifyCopy.Id);

                update new VTD1_Document__c(
                        Id = docToCertifyId,
                        VTD2_Certification__c = certifyCopy.Id,
                        VTD2_Sent_for_Signature__c = true
                );

                update new ContentVersion(
                        Id = documentCDLs[0].ContentDocument.LatestPublishedVersionId,
                        VTR4_Certification__c = certifyCopy.Id
                );
                new VT_D2_QAction_CertifyDocuments(certifyCopy.Id).executeFuture();
                return certifyCopy.Id;
            }
            return null;
        } catch (Exception e){
            System.debug('AuraHandledException = '+e.getMessage() + '\n' + e.getStackTraceString());
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled(cacheable=true)
    public static Boolean hasFileUploaded(Id recordId){
        List<ContentDocumentLink> documentCDLs = [
                SELECT Id, ContentDocument.title
                FROM ContentDocumentLink
                WHERE LinkedEntityId = :recordId];
        return documentCDLs.isEmpty();
    }
}