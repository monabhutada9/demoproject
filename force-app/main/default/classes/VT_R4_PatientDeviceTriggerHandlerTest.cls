@IsTest
public with sharing class VT_R4_PatientDeviceTriggerHandlerTest {

    public static void createPatientDeviceNotificationForRetakeReqTest() {
        Test.startTest();
        VT_D1_TestUtils.createProtocolDelivery([SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1].Id);

        HealthCloudGA__EhrDevice__c ehrDevice = VT_D1_TestUtils.createEherDevice([SELECT Id FROM VTD1_Protocol_Kit__c WHERE Name = 'Connected Device' LIMIT 1].Id);
        VTD1_Patient_Device__c patientDevice = VT_D1_TestUtils.createpatientDevice(ehrDevice.Id, [SELECT Id FROM Case LIMIT 1].Id);
        VT_R4_ConstantsHelper_Misc.isRetakeRequest = true;
        patientDevice.VTR5_SyncStatus__c = 'Retake Requested';
        update patientDevice;
        Test.stopTest();
    }
}