/**
 * @description Created by Alexander Komarov on 29.04.2019.
 */
@RestResource(UrlMapping='/Patient/Broadcasts/*')
global with sharing class VT_R3_RestPatientBroadcastComments {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();
    static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());

    @HttpGet
    global static String getComments() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        String temp = request.requestURI.substringAfterLast('/');

        if (String.isBlank(temp) || !helper.isIdCorrectType(temp, 'FeedItem')) {
            response.statusCode = 400;
            cr.buildResponse(helper.forAnswerForIncorrectInput());
            return JSON.serialize(cr, true);
        }
        Id broadcastId = temp;

        List<CommentObj> commentsList = new List<CommentObj>();

        try {
            commentsList = getCommentsProcess(broadcastId);
        } catch (Exception e) {
            response.statusCode = 500;
            cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName());
            return JSON.serialize(cr, true);
        }
        cr.buildResponse(commentsList);
        return JSON.serialize(cr, true);

    }

    @HttpPost
    global static String sendComment(String message, String fileName, String based64FileData) {
//        RestResponse response = RestContext.response;
//        RestRequest request = RestContext.request;
//        String broadcastId = request.requestURI.substringAfterLast('/');
//
//        if (String.isBlank(broadcastId) || !helper.isIdCorrectType(broadcastId, 'FeedItem')) {
//            response.statusCode = 400;
//            cr.serviceResponse = helper.forAnswerForIncorrectInput();
//            return JSON.serialize(cr, true);
//        } else if (message.length() > 10000) {
//            response.statusCode = 400;
//            cr.serviceResponse = 'FAILURE.  Your comment can not have more than 10000 characters.';
//            return JSON.serialize(cr, true);
//        }
//        try {
//            if (String.isBlank(fileName) || String.isBlank(based64FileData)) {
//                VT_D1_CommunityChat.sendComment(broadcastId, message);
//            } else {
//                if (based64FileData.length() > 5000000) {
//                    response.statusCode = 400;
//                    cr.serviceResponse = 'FAILURE.  Attachment file can not have more than 5000000 characters.';
//                    return JSON.serialize(cr, true);
//                }
//                VT_D1_CommunityChat.sendCommentWithFile(broadcastId, message, fileName, based64FileData);
//            }
//        } catch (DmlException dml) {
//            if (helper.checkForTypes(dml)) {
//                response.statusCode = 400;
//                cr.serviceResponse = helper.formAnswerForDMLError(dml, true);
//                return JSON.serialize(cr, true);
//            }
//            response.statusCode = 500;
//            cr.serviceResponse = helper.formAnswerForDMLError(dml, false);
//            return JSON.serialize(cr, true);
//
//        } catch (Exception e) {
//            response.statusCode = 500;
//            cr.serviceResponse = 'FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName();
//            return JSON.serialize(cr, true);
//        }
//        cr.serviceResponse = getNewComment(broadcastId);
        cr.serviceMessage = 'No more comments in broadcasts for patient, sorry ¯\\_(ツ)_/¯';
        return JSON.serialize(cr, true);

    }

    private static List<CommentObj> getCommentsProcess(String postId) {
        List<CommentObj> comments = new List<CommentObj>();

        FeedItem firstComment = ((List<FeedItem>) new VT_Stubber.ResultStub('FirstComment',
        [
                SELECT Id, Body, CreatedDate, CreatedById
                FROM FeedItem
                WHERE Id = :postId
                LIMIT 1
        ]
        ).getResult()).get(0);

        User firstCommentAuthor = ((List<User>) new VT_Stubber.ResultStub('FirstCommentAuthor',
        [
                SELECT Name
                FROM User
                WHERE Id = :firstComment.CreatedById
                LIMIT 1
        ]
        ).getResult()).get(0);

        ConnectApi.CommentPage commentsPage;
        ConnectApi.Photo firstCommentAuthorPhoto;

        if (!Test.isRunningTest()) {
            ConnectApi.ChatterFeeds.setIsReadByMe(null, postId, true);
            commentsPage = ConnectApi.ChatterFeeds.getCommentsForFeedElement(null, postId, null, 100);
            firstCommentAuthorPhoto = ConnectApi.UserProfiles.getPhoto(null, firstComment.CreatedById);
        }

        String photoUrl = firstCommentAuthorPhoto == null ? '' : firstCommentAuthorPhoto.standardEmailPhotoUrl;

        comments.add(new CommentObj(
                firstComment.Id,
                firstCommentAuthor.Name,
                photoUrl,
                firstComment.CreatedDate,
                firstComment.Body));

        Set<Id> filesIds = new Set<Id>();

        if(commentsPage != null) {
            for (ConnectApi.Comment comment : commentsPage.items) {
                if (String.valueOf(comment.type) == 'ContentComment') {
                    filesIds.add(comment.capabilities.content.id);
                }
                comments.add(new CommentObj(comment));
            }
        }

        Map<Id, ContentVersion> contentDocumentIdToVersionsMap = new Map<Id, ContentVersion>();

        List<ContentVersion> contentVersions = (List<ContentVersion>) new VT_Stubber.ResultStub('ContentVersions',
        [
                SELECT Id, VersionData, ContentSize, ContentDocumentId
                FROM ContentVersion
                WHERE ContentDocumentId IN :filesIds
        ]
        ).getResult();

        for (ContentVersion cv : contentVersions) {
            contentDocumentIdToVersionsMap.put(cv.ContentDocumentId, cv);
        }

        for (CommentObj commentObj : comments) {
            if (commentObj.file != null) {
                Id fileId = commentObj.file.fileId;
                ContentVersion currentCV = contentDocumentIdToVersionsMap.get(fileId);
                Integer fileSizeB64 = EncodingUtil.base64Encode(currentCV.VersionData).length();
                commentObj.file.fileSizeBase64 = String.valueOf(fileSizeB64);
            }
        }

        User u = ((List<User>) new VT_Stubber.ResultStub('User',
        [
                SELECT Id, VTR3_UnreadFeeds__c
                FROM User
                WHERE Id = :UserInfo.getUserId()
                LIMIT 1
        ]
        ).getResult()).get(0);

        String unreadFeeds = u.VTR3_UnreadFeeds__c;
        if (unreadFeeds != null) {
            if (unreadFeeds.contains(String.valueOf(postId))) {
                u.VTR3_UnreadFeeds__c = unreadFeeds.remove(String.valueOf(postId) + ';');
                update u;
            }
        }

        return comments;
    }

    private class CommentObj {
        public String id;
        public String authorName;
        public String photo;
        public Long commentData;
        public String text;
        public CommentFileObj file;

        public CommentObj(String i, String n, String p, Datetime d, String t) {
            this.id = i;
            this.authorName = n;
            String tempPhotoString = p.startsWith('https') ? p : Url.getSalesforceBaseUrl().toExternalForm() + p;
            this.photo = tempPhotoString.contains('default_profile') ? '' : tempPhotoString;
            this.commentData = d.getTime();// + timeOffset;
            this.text = t;
            this.file = null;
        }

        public CommentObj(ConnectApi.Comment comment) {
            ConnectApi.User commentAuthor = (ConnectApi.User) comment.user;
            this.id = comment.id;
            this.authorName = commentAuthor.displayName;
            String tempPhotoString = commentAuthor.photo.largePhotoUrl;
            this.photo = tempPhotoString.contains('default_profile') ? '' : tempPhotoString;
            this.commentData = comment.createdDate.getTime();// + timeOffset;
            this.text = comment.body.text;
            if (String.valueOf(comment.type) == 'ContentComment') {
                this.file = new CommentFileObj(comment.capabilities.content);
            }
        }

    }

    private class CommentFileObj {
        public String fileExtension;
        public String fileSize;
        public String title;
        public String fileId;
        public String fileSizeBase64;


        public CommentFileObj(ConnectApi.ContentCapability content) {
            this.fileExtension = content.fileExtension;
            this.fileSize = content.fileSize;
            this.title = content.title;
            this.fileId = content.id;
        }
    }
}