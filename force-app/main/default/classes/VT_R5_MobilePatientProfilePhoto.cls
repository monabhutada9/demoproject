/**
 * Created by Yuliya Yakushenkova on 10/19/2020.
 */

public class VT_R5_MobilePatientProfilePhoto extends VT_R5_MobileRestResponse implements VT_R5_MobileLibrary.Routable {

    private static RequestParams requestParams;

    public List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/profile/photo' //?isPatient=false
        };
    }

    public VT_R5_MobileRestResponse versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        return get_v1();
                    }
                }
            }
            when 'POST' {
                switch on requestVersion {
                    when 'v1' {
                        return post_v1();
                    }
                }
            }
        }
        return null;
    }

    public void initService() {
        requestParams = new RequestParams();
        parseBody();
        parseParams();
    }

    public Map<String, String> getMinimumVersions() {
        return null;
    }

    private VT_R5_MobileRestResponse get_v1() {
        String userId = getUserId();
        String photoUrl = '';
        if (!Test.isRunningTest()) photoUrl = ConnectApi.UserProfiles.getPhoto(null, userId).fullEmailPhotoUrl;
        photoUrl = photoUrl.contains('default_profile') ? '' : photoUrl;
        return this.buildResponse(new Photo(photoUrl));
    }


    private VT_R5_MobileRestResponse post_v1() {
        String userId = getUserId();

        Blob b = EncodingUtil.base64Decode(requestParams.base64Photo);
        if (!Test.isRunningTest()) {
            ConnectApi.UserProfiles.deletePhoto(null, userId);
            ConnectApi.BinaryInput file = new ConnectApi.BinaryInput(b, 'image/jpg', 'User' + userId + 'Photo.jpg');
            ConnectApi.Photo photo = ConnectApi.UserProfiles.setPhoto(null, userId, file);
        }
        return this.buildResponse(VT_D1_TranslateHelper.getLabelValue('VTR3_Success_Profile_Picture_Upload'));
    }

    public String getUserId() {
        String userId = UserInfo.getUserId();
        if (requestParams.isNotPatient) {
            userId = VT_R5_PatientQueryService.getPatientUserIdByCaregiverUserId(userId);
        }
        return userId;
    }

    private void parseParams() {
        if (request.params.containsKey('isPatient')) {
            requestParams.setIsNotPatient(request.params.get('isPatient'));
        }
    }

    private void parseBody() {
        if (request.requestBody != null && request.requestBody.size() > 0) {
            requestParams = (RequestParams) JSON.deserialize(request.requestBody.toString(), RequestParams.class);
        }
    }

    private class RequestParams {
        private Boolean isNotPatient = false;
        private String base64Photo;

        private void setIsNotPatient(String isPatient) {
            this.isNotPatient = !Boolean.valueOf(isPatient);
        }
    }

    private class Photo {
        private String photoURL;

        private Photo(String photoURL) {
            this.photoURL = photoURL;
        }
    }
}