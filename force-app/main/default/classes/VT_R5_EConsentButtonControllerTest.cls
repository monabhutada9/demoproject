/**
 * Created by user on 21-May-20.
 */
@IsTest
public with sharing class VT_R5_EConsentButtonControllerTest {

	@TestSetup
	private static void setupMethod() {

		Test.startTest();
		DomainObjects.Study_t domainStudy = new DomainObjects.Study_t()
				.setMaximumDaysWithoutLoggingIn(1)
				.addAccount(new DomainObjects.Account_t()
						.setRecordTypeByName('Sponsor')
				);
		VT_R3_GlobalSharing.disableForTest = true;
		HealthCloudGA__CandidatePatient__c candidatePatient = (HealthCloudGA__CandidatePatient__c) new DomainObjects.HealthCloudGA_CandidatePatient_t()
				.setRr_firstName('MikeConsentButton')
				.setRr_lastName(DomainObjects.RANDOM.getString())
				.setConversion('Converted')
				.setCaregiverEmail(DomainObjects.RANDOM.getEmail())
				.addStudy(domainStudy)
				.persist();

		Test.stopTest();
		User patientUser = [
				SELECT Id, Contact.VTD1_Clinical_Study_Membership__c
				FROM User
				WHERE VTD1_Profile_Name__c = 'Patient'
				AND FirstName = 'MikeConsentButton'
				ORDER BY CreatedDate
				LIMIT 1
		];

		Case currentCase = [SELECT Id, Contact.VTD1_UserId__c FROM Case LIMIT 1];
		DomainObjects.VTD1_Actual_Visit_t visit = new DomainObjects.VTD1_Actual_Visit_t()
				.setVTD1_Case(currentCase.id)
				.setVTD1_Status('To Be Scheduled')
				.setRecordTypeByName('VTD1_Unscheduled');

		new DomainObjects.Visit_Member_t()
				.setVTD1_Participant_UserId(patientUser.Id)
				.setVTD1_External_Participant_Type('Patient')
				.addVTD1_Actual_Visit(visit)
				.persist();
		new DomainObjects.Visit_Member_t()
				.setVTD1_External_Participant_Type('HHN')
				.addVTD1_Actual_Visit(visit)
				.persist();
	}

	@IsTest
	public static void getConsentWrapperTest() {
		User patientUser = [SELECT Id, Contact.VTD1_Clinical_Study_Membership__c
				FROM User
				WHERE VTD1_Profile_Name__c = 'Patient'
				AND FirstName = 'MikeConsentButton'
				ORDER BY CreatedDate DESC
				LIMIT 1
		];
		applyContactStub(patientUser.Contact);
		System.runAs(patientUser) {
			VT_R5_EConsentButtonController.ConsentWrapper wrapper = new VT_R5_EConsentButtonController.ConsentWrapper();
			wrapper = VT_R5_EConsentButtonController.getConsentWrapper();
			System.assertEquals(false, wrapper.consentVisitStatus);
			System.assertEquals(null, wrapper.consentLink);
		}
	}

	@IsTest
	public static void getConsentWrapperWithLinkTest() {

		User patientUser = [
				SELECT Id, Contact.VTD1_Clinical_Study_Membership__c
				FROM User
				WHERE VTD1_Profile_Name__c = 'Patient'
				AND FirstName = 'MikeConsentButton'
				ORDER BY CreatedDate DESC
				LIMIT 1
		];

		VTD1_Actual_Visit__c actualVisit = [
				SELECT VTD1_Status__c, Unscheduled_Visits__c, VTD1_Unscheduled_Visit_Duration__c, VTD1_Unscheduled_Visit_Type__c, VTD1_Scheduled_Date_Time__c
				FROM VTD1_Actual_Visit__c
				LIMIT 1
		];
		Case currentCase = [SELECT Id, VTD1_Virtual_Site__c FROM Case LIMIT 1];
		actualVisit.VTD1_Status__c = 'Scheduled';
		actualVisit.Unscheduled_Visits__c = currentCase.Id;
		actualVisit.VTD1_Unscheduled_Visit_Duration__c = 60;
		actualVisit.VTD1_Unscheduled_Visit_Type__c = 'Consent';
		actualVisit.VTD1_Scheduled_Date_Time__c = Datetime.now().addMinutes(-5);
		Test.startTest();
		update actualVisit;
		Test.stopTest();
		HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM healthcloudga__CarePlanTemplate__c];
		Virtual_Site__c virtualSite = new Virtual_Site__c(
				VTD1_Study__c = study.Id,
				VTD1_Study_Site_Number__c = '12345',
				VTR5_ConsentPacketLink__c = 'test.url');
		insert virtualSite;
		currentCase.VTD1_Virtual_Site__c = virtualSite.Id;
		update currentCase;

		applyContactStub(patientUser.Contact);
		System.runAs(patientUser) {
			VT_R5_EConsentButtonController.ConsentWrapper wrapper = new VT_R5_EConsentButtonController.ConsentWrapper();
			wrapper = VT_R5_EConsentButtonController.getConsentWrapper();
			System.assertEquals(true, wrapper.consentVisitStatus);
			//System.assertEquals('test.url', wrapper.consentLink);
		}
	}
	
	@IsTest
	public static void getConsentWrapperNoLinkTest() {
		User patientUser = [
				SELECT Id, Contact.VTD1_Clinical_Study_Membership__c
				FROM User
				WHERE VTD1_Profile_Name__c = 'Patient'
				AND FirstName = 'MikeConsentButton'
				ORDER BY CreatedDate DESC
				LIMIT 1
		];

		VTD1_Actual_Visit__c actualVisit = [
				SELECT VTD1_Status__c, Unscheduled_Visits__c, VTD1_Unscheduled_Visit_Duration__c, VTD1_Unscheduled_Visit_Type__c, VTD1_Scheduled_Date_Time__c
				FROM VTD1_Actual_Visit__c
				LIMIT 1
		];
		Case currentCase = [SELECT Id, VTD1_Virtual_Site__c FROM Case LIMIT 1];
		actualVisit.VTD1_Status__c = 'Scheduled';
		actualVisit.Unscheduled_Visits__c = currentCase.Id;
		actualVisit.VTD1_Unscheduled_Visit_Duration__c = 60;
		actualVisit.VTD1_Unscheduled_Visit_Type__c = 'Consent';
		actualVisit.VTD1_Scheduled_Date_Time__c = Datetime.now().addMinutes(-5);

		Test.startTest();
		update actualVisit;
		Test.stopTest();
		applyContactStub(patientUser.Contact);
		System.runAs(patientUser) {
			VT_R5_EConsentButtonController.ConsentWrapper wrapper = new VT_R5_EConsentButtonController.ConsentWrapper();
			wrapper = VT_R5_EConsentButtonController.getConsentWrapper();
			System.assertEquals(true, wrapper.consentVisitStatus);
			System.assertEquals(null, wrapper.consentLink);
		}
	}

	private static void applyContactStub(Contact cont) {
		new QueryBuilder()
				.buildStub()
				.addStubToSObject(cont)
				.namedQueryStub('VT_R5_EConsentButtonController.cont')
				.applyStub();

	}
}