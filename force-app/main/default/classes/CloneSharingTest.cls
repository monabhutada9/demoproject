@IsTest
public class CloneSharingTest {
    @TestSetup
    static void setup() {
       

        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );
        DomainObjects.User_t user = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5));
        DomainObjects.User_t piuser = new DomainObjects.User_t()
                .setProfile('Primary Investigator')
                .setEmail('test@email.com')
                .addContact(new DomainObjects.Contact_t());
            piuser.persist();
        DomainObjects.StudyTeamMember_t stm = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('PI')
                .addUser(piuser)
                .addStudy(study);
             stm.persist();
        //Added by :Conquerors, Priyanka Ambre SH-21178 
        //START
        DomainObjects.User_t scruser = new DomainObjects.User_t()
                .setProfile('Site Coordinator')
                .setEmail('testscr@email.com')
                .addContact(new DomainObjects.Contact_t());
            piuser.persist();        
        DomainObjects.StudyTeamMember_t stm1 = new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('SCR')
                .addUser(scruser)
                .addStudy(study);
                stm1.persist();
        //END
        DomainObjects.VirtualSite_t virtSite = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .setStudySiteNumber('12345')
                .addStudyTeamMember(stm);
            virtSite.persist();
           DomainObjects.VirtualSite_t virtSite2 = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .setStudySiteNumber('12345')
                .addStudyTeamMember(stm);
            virtSite.persist();
            DomainObjects.VirtualSite_t virtSite3 = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .setStudySiteNumber('12345')
                .addStudyTeamMember(stm);
            virtSite.persist();
            
         Test.startTest();
        Case cas = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .setSubject('EDiaryAllTests')
                .addStudy(study)
                .addUser(user)
                .addVirtualSite(virtSite)
                .persist();
           Case cas2 = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .setSubject('EDiaryAllTests')
                .addStudy(study)
                .addUser(user)
                .addVirtualSite(virtSite2)
                .persist();
        DomainObjects.VTD1_Protocol_ePRO_t protocolEPROT = new DomainObjects.VTD1_Protocol_ePRO_t()
                .setRecordTypeByName('SatisfactionSurvey')
                .addVTD1_Study(study)
                .setVTD1_Type('Non-PSC')
                .setVTR4_Is_Branching_eDiary(true)
                .setVTR2_Protocol_Reviewer('Patient Guide')
                .setVTD1_Subject('Patient')
                .setVTD1_Response_Window(10);
        DomainObjects.VTD1_Protocol_ePRO_t protocolEPROT2 = new DomainObjects.VTD1_Protocol_ePRO_t()
                .setRecordTypeByName('SatisfactionSurvey')
                .addVTD1_Study(study)
                .setVTR2_Protocol_Reviewer('Patient Guide')
                .setVTD1_Subject('Patient')
                .setVTD1_Response_Window(10);
        DomainObjects.VTD1_Survey_t survey = new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(cas.Id)
                .setName('testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttest')
                .addVTD1_Protocol_ePRO(protocolEPROT);
        survey.persist();
        DomainObjects.VTD1_Survey_t survey2 = new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(cas.Id)
                .addVTD1_Protocol_ePRO(protocolEPROT2)
                .setName('test2test2')
                .setRecordTypeByName('VTR5_External');
        survey2.persist();
          DomainObjects.VTD1_Survey_t survey3 = new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(cas2.Id)
                .addVTD1_Protocol_ePRO(protocolEPROT2)
                .setName('test4test4')
                .setRecordTypeByName('VTR5_External');
        survey3.persist();
          DomainObjects.VTD1_Survey_t survey4 = new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(cas2.Id)
                .addVTD1_Protocol_ePRO(protocolEPROT2)
                .setName('test4test4')
                .setRecordTypeByName('VTR5_External');
        survey4.persist();

        DomainObjects.VTD1_Protocol_ePRO_t standardProtocol = new DomainObjects.VTD1_Protocol_ePRO_t()
                .setRecordTypeByName('ePRO')
                .addVTD1_Study(study)
                .setVTR2_Protocol_Reviewer('Patient Guide')
                .setVTD1_Subject('Patient')
                .setVTD1_Response_Window(10);
        standardProtocol.persist();
        DomainObjects.VTD1_Survey_t standardSurvey = new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(cas.Id)
                .setRecordTypeByName('ePRO')
                .setName('standardSurvey')
                .addVTD1_Protocol_ePRO(standardProtocol);
        standardSurvey.persist();
        DomainObjects.VTD1_Survey_t standardSurvey2 = new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(cas.Id)
                .setRecordTypeByName('ePRO')
                .setName('standardSurvey2')
                .addVTD1_Protocol_ePRO(standardProtocol);
        standardSurvey2.persist();

        DomainObjects.VTD1_Survey_Answer_t answer = new DomainObjects.VTD1_Survey_Answer_t()
                .addVTD1_Question(new DomainObjects.VTD1_Protocol_ePro_Question_t()
                        .addVTD1_Protocol_ePRO(new DomainObjects.VTD1_Protocol_ePRO_t()
                                .setRecordTypeByName('SatisfactionSurvey')
                                .addVTD1_Study(study)
                                .setVTD1_Type('PSC')
                                .setVTR2_Protocol_Reviewer('Patient Guide')
                                .setVTD1_Subject('Patient')
                                .setVTD1_Response_Window(10)))
                .addVTD1_Survey(survey);
        answer.persist();

        DomainObjects.VTD1_Survey_Answer_t answer1 = new DomainObjects.VTD1_Survey_Answer_t()
                .addVTD1_Question(new DomainObjects.VTD1_Protocol_ePro_Question_t()
                        .addVTD1_Protocol_ePRO(new DomainObjects.VTD1_Protocol_ePRO_t()
                                .addVTD1_Study(study)
                                .setVTR2_Protocol_Reviewer('Patient Guide')
                                .setVTD1_Subject('Patient')
                                .setVTD1_Response_Window(10)))
                                .setVT_R5_External_Widget_Data_Key('Unwell?')
                                .setVTD1_Answer('YES')
                .addVTD1_Survey(survey2);
        answer.persist();    

         Test.stoptest();
    }

     @IsTest
    static void VT_R5_EDiariesTableControllerEdiaryFilterForGetRecordsTest() {
        VT_R5_EDiariesTableControllerTest.ediaryFilterForGetRecordsTest();
    }
    
    @IsTest
    static void VT_R5_EDiariesTableControllerEdiaryFilterForBlankEdiaryTest() {
        VT_R5_EDiariesTableControllerTest.ediaryFilterForBlankEdiaryTest();
    }

     @IsTest
    static void VT_R5_EDiariesTableControllerEdiaryFilterForNoAnswersTest() {
        VT_R5_EDiariesTableControllerTest.ediaryFilterForNoAnswersTest();
    }
     @IsTest
    static void VT_R5_EDiariesTableControllerGetEdiaryFiltersTest() {
        VT_R5_EDiariesTableControllerTest.getEdiaryFiltersTest();
    }
     @IsTest
    static void VT_R5_EDiariesTableControllerGetEdiaryFiltersForNullTest() {
        VT_R5_EDiariesTableControllerTest.getEdiaryFiltersForNullTest();
    }
}