/**
* @author: N.Arbatskiy
* @date: 27-May-20
* @description: Class to hold the values from deserialized ruleset JSON
**/

public with sharing class VT_R5_eCoaRuleResponseObject {

    public static List<Data> data;

    public class Data {
        public String ruleSetGuid;
        public String ruleSetType;
    }

    /**
    * @description: Parses eCOA JSON(rulesets) and populates List<Data> data with values
    * @param json - eCOA response from an API call (made by VT_R5_eCoaRules)
    * @return deserialized passed JSON
    */
    private static VT_R5_eCoaRuleResponseObject parse(String json) {
        return (VT_R5_eCoaRuleResponseObject) System.JSON.deserialize(json, VT_R5_eCoaRuleResponseObject.class);
    }

    /**
    * @description: Creates a map with ruleSetType as a key and ruleSetGuid as a value
    * (gets them with a @method parse())
    * @param json - described above
    * @return a map with ruleSetType binded with ruleSetGuid
    */
    public static Map<String, String> getRulesMap(String json) {
        parse(json);
        Map<String, String> rulesMap = new Map<String, String>();
        for (Data item : data) {
            rulesMap.put(item.ruleSetType, item.ruleSetGuid);
        }
        return rulesMap;
    }
}