@IsTest
private with sharing class VT_R5_AllTests5 {
    //Without Test setup
    @IsTest
    static void VT_R2_SiteCustomHeaderTest1() {
        VT_R2_SiteCustomHeaderTest.getCurrentUserProfileSuccessTest();
    }
    @IsTest
    static void VT_R2_SiteCustomHeaderTest2() {
        VT_R2_SiteCustomHeaderTest.getReconciliationLogIdSuccessTest();
    }
    @IsTest
    static void VT_R2_SendSMSManuallyTest() {
        VT_R2_SendSMSManuallyTest.testBehavior();
    }
    @IsTest
    static void VT_R2_RegulatoryBinderDocumentPrevTest1() {
        VT_R2_RegulatoryBinderDocumentPrevTest.getRegulatoryBinderDocumentWithDocIdTest();
    }
    @IsTest
    static void VT_R2_RegulatoryBinderDocumentPrevTest2() {
        VT_R2_RegulatoryBinderDocumentPrevTest.getRegulatoryBinderDocumentWithoutDocIdTest();
    }
    @IsTest
    static void VT_R2_RegulatoryBinderDocumentPrevTest3() {
        VT_R2_RegulatoryBinderDocumentPrevTest.testDocuments();
    }
    @IsTest
    static void VT_R2_PatientContactDeliveryCtrlTest() {
        VT_R2_PatientContactDeliveryCtrlTest.testGetPatientContactUser();
    }
    @IsTest
    static void VT_R2_MonitoringVisitTimeCalculatorTest() {
        VT_R2_MonitoringVisitTimeCalculatorTest.calculateTimesTest();
    }
    @IsTest
    static void VT_R2_LiveChatButtonOverrideContTest() {
        VT_R2_LiveChatButtonOverrideContTest.doTest();
    }
    @IsTest
    static void VT_R2_GetPIAsPatientTestControllerTest() {
        VT_R2_GetPIAsPatientTestControllerTest.testGetPIUser();
    }
    @IsTest
    static void VT_R2_DuplicateUsernameCheckerTest() {
        VT_R2_DuplicateUsernameCheckerTest.doTest();
    }
    @IsTest
    static void VT_R2_DraftFilesListControllerTest() {
        VT_R2_DraftFilesListControllerTest.testGetFiles();
    }
    @IsTest
    static void VT_R2_DocuSignRecipientStatusHandlerTest1() {
        VT_R2_DocuSignRecipientStatusHandlerTest.testTaskAndNotificationCreation();
    }
    @IsTest
    static void VT_R2_DocuSignRecipientStatusHandlerTest2() {
        VT_R2_DocuSignRecipientStatusHandlerTest.testClosingTask();
    }
    @IsTest
    static void VT_R2_DocuSignRecipientStatusHandlerTest3() {
        VT_R2_DocuSignRecipientStatusHandlerTest.coverageForTriggerWhenHandlerIsDisabled();
    }
    @IsTest
    static void VT_R2_DocuSignCommunityProcessCtrlTest() {
        VT_R2_DocuSignCommunityProcessCtrlTest.test1();
    }
    @IsTest
    static void VT_R2_DocumentVersionsListControllerTest() {
        VT_R2_DocumentVersionsListControllerTest.testGetVersions();
    }
    @IsTest
    static void VT_R2_CustomLookupControllerTest() {
        VT_R2_CustomLookupControllerTest.test();
    }
    @IsTest
    static void VT_R2_CreateUpdateSubjectControllerTest() {
        VT_R2_CreateUpdateSubjectControllerTest.testCreateUpdateCase();
    }
    @IsTest
    static void VT_R2_ConsentPacketControllerTest() {
        VT_R2_ConsentPacketControllerTest.getDocumentTest();
    }
    @IsTest
    static void VT_FilesUploaderControllerTest() {
        VT_FilesUploaderControllerTest.deleteFilesIsAllowed();
    }
    @IsTest
    static void VT_D2_StudyUpdateHelperTest() {
        VT_D2_StudyUpdateHelperTest.convertCandidatePatientsOnHoldTest();
    }
    @IsTest
    static void VT_D2_PostToChatterTest() {
        VT_D2_PostToChatterTest.doTest();
    }
    @IsTest
    static void VT_D2_LiveAgentOnlineStatusContTest() {
        VT_D2_LiveAgentOnlineStatusContTest.doTest();
    }
    @IsTest
    static void VT_D2_GetPIAsPatientTestControllerTest1() {
        VT_D2_GetPIAsPatientTestControllerTest.getSessionIdUserIdTest();
    }
    @IsTest
    static void VT_D2_GetPIAsPatientTestControllerTest2() {
        VT_D2_GetPIAsPatientTestControllerTest.sendSignalTest();
    }
    @IsTest
    static void VT_D2_EmailAboutNewMessagesScheduleTest() {
        VT_D2_EmailAboutNewMessagesScheduleTest.doTest();
    }
    @IsTest
    static void VT_D2_DeliveryTrackingTest1() {
        VT_D2_DeliveryTrackingTest.httpPostErrorMissingFieldsTest();
    }
    @IsTest
    static void VT_D2_DeliveryTrackingTest2() {
        VT_D2_DeliveryTrackingTest.httpPostSuccessTest();
    }
    @IsTest
    static void VT_D1_ZipArchiveControllerTest() {
        VT_D1_ZipArchiveControllerTest.doTest();
    }
    @IsTest
    static void VT_D1_VideoConfSignalHandlerTest() {
        VT_D1_VideoConfSignalHandlerTest.testVideoEvent();
    }
    @IsTest
    static void VT_D1_VideoConfConnectionProcessTest() {
        VT_D1_VideoConfConnectionProcessTest.doTest();
    }
    @IsTest
    static void VT_D1_UserProfileMenuControllerTest() {
        VT_D1_UserProfileMenuControllerTest.userProfileMenuTest();
    }
    @IsTest
    static void VT_D1_UploadFilesControllerTest() {
        VT_D1_UploadFilesControllerTest.doTest();
    }
}