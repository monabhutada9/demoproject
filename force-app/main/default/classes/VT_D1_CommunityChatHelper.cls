public without sharing class VT_D1_CommunityChatHelper {
    @AuraEnabled
    public static String createBroadcast(String subject, Id studyId, String recipientsType, String message) {
        Set<Id> recipients = findRecipients(studyId, recipientsType);
        // create Chat
        VTD1_Chat__c chat = new VTD1_Chat__c();
        chat.Name = subject;
        chat.VTD1_Chat_Type__c = 'Broadcast';
        chat.VTR2_Study__c = studyId;
        insert chat;
        System.debug('Created Chat #' + chat.Id);
        if (recipients.size() > 50) {
            Database.executeBatch(new VT_R4_BroadcastCreationBatch(chat.Id, recipients, message));
            return null;
        } else {
            // create Chat members
            createChatMembers(chat.Id, recipients);
            // create post
            ConnectApi.FeedItemInput feedItemInput = prepareFeedItemToSend(chat.Id, message);
            // send post
            if (!Test.isRunningTest()) {
                ConnectApi.FeedElement feedElement =
                        ConnectApi.ChatterFeeds.postFeedElement(null, (ConnectApi.FeedElementInput) feedItemInput);
                System.debug('Created Feed Element #' + feedElement.id);
                //createFeedPostOnChat(feedElement.id, chat.Id);
                return feedElement.id;
            } else {
                return chat.Id;
            }
        }
    }

    @AuraEnabled
    public static String createPost(String subject, Id patientUserId, Id pcfId, String message) {
        System.debug('post creating '+subject+patientUserId+pcfId+message);
        Case pcf = getPCF(pcfId, subject, patientUserId);
        System.debug('Created PCF #' + pcf.Id);
        // create post
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        System.debug('feedItemInput ' + feedItemInput);
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        System.debug('feedItemInput.feedElementType  ' + feedItemInput.feedElementType );

        feedItemInput.subjectId = pcf.Id;
        System.debug('feedItemInput.subjectId  ' + feedItemInput.subjectId);

        feedItemInput.visibility = ConnectApi.FeedItemVisibilityType.AllUsers;
        System.debug('feedItemInput.visibility   ' + feedItemInput.visibility );


        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        System.debug('fmessageBodyInput   ' + messageBodyInput);

        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

        // add text
        messageBodyInput.messageSegments.add(VT_D1_MentionHelper.getTextSeg(message));
        // add patient mention
        messageBodyInput.messageSegments.add(VT_D1_MentionHelper.getMentionSeg(patientUserId));
        // add space
        messageBodyInput.messageSegments.add(VT_D1_MentionHelper.getTextSeg(' '));
        // add self mention
        messageBodyInput.messageSegments.add(VT_D1_MentionHelper.getMentionSeg(UserInfo.getUserId()));

        feedItemInput.body = messageBodyInput;
        System.debug('feedItemInput.body   ' + feedItemInput.body);

        if (!Test.isRunningTest()) {
            // send post
            ConnectApi.FeedElement feedElement =
                    ConnectApi.ChatterFeeds.postFeedElement(null, (ConnectApi.FeedElementInput) feedItemInput);
            System.debug('Created Feed Element #' + feedElement.id);
            //System.debug('share record: if' + [SELECT Id FROM CaseShare WHERE UserOrGroupId = :patientUserId AND CaseId = :pcf.Id]);
            return feedElement.id;
        } else {
            System.debug('PCF ID: ' + pcf.Id);
            //System.debug('share record: else' + [SELECT Id FROM CaseShare WHERE UserOrGroupId = :patientUserId AND CaseId = :pcf.Id]);
            return pcf.Id;
        }
    }

    public static Case getPCF(Id pcfId, String subject, Id patientUserId) {
        // get PCF record ID
        String caseObjectName = VT_R4_ConstantsHelper_AccountContactCase.SOBJECT_CASE;
        String profileName = VT_D1_HelperClass.getUserProfileName(patientUserId);
        String pcfRecordTypeName = profileName == 'Patient' || profileName == 'Caregiver' ? VT_R4_ConstantsHelper_AccountContactCase.RT_PCF : VT_R4_ConstantsHelper_AccountContactCase.RT_GCF;
        //String careplanRecordTypeName = VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN;
        Id pcfRecordTypeId = VT_D1_HelperClass.getRTMap().get(caseObjectName).get(pcfRecordTypeName);
        //Id careplanRecordTypeId = VT_D1_HelperClass.getRTMap().get(caseObjectName).get(careplanRecordTypeName);
        //	List<Case> casesToInsert = new List<Case>();

        // get or create PCF
        Case pcf;
        if (pcfId != null) {
            pcf = [SELECT Id FROM Case WHERE Id = :pcfId AND RecordTypeId = :pcfRecordTypeId];
        } else {
            // get patient Care Plan
            Id carePlanCaseId;
            try {
                carePlanCaseId = [SELECT Contact.VTD1_Clinical_Study_Membership__c FROM User WHERE Id = :patientUserId][0].Contact.VTD1_Clinical_Study_Membership__c;
            } catch (Exception e) {
                System.debug('No Care Plan found for user #' + patientUserId);
            }

            // create PCF
            try {
                pcf = new Case();
                pcf.Subject = subject;
                pcf.RecordTypeId = pcfRecordTypeId;
                System.debug('carePlanCaseId::: ' + carePlanCaseId);
                pcf.VTD1_Clinical_Study_Membership__c = carePlanCaseId;
                insert pcf;
                // create PCF chat members
                Set<Id> userIds = new Set<Id>{
                        patientUserId, UserInfo.getUserId()
                };
                createPCFChatMembers(pcf.Id, userIds);
                System.debug('carePlanCaseId:::after ' + carePlanCaseId);
                //shareToPatientCaregiver(patientUserId, pcf);
            } catch (Exception e) {
                System.debug(e.getMessage() + ' ' + e.getStackTraceString());
            }
            /*
            VTD1_PCF_Chat_Member__c member = new VTD1_PCF_Chat_Member__c();
            member.VTD1_User_Id__c = patientUserId;
            member.VTD1_PCF__c = pcf.Id;
            insert member;

            // add PI as PCF chat member
            member = new VTD1_PCF_Chat_Member__c();
            member.VTD1_User_Id__c = UserInfo.getUserId();
            member.VTD1_PCF__c = pcf.Id;
            insert member;
            */
        }
        return pcf;
    }

    private static void shareToPatientCaregiver(Id patientUserId, Case pcf) {
        CaseShare share = new CaseShare(
                UserOrGroupId = patientUserId,
                CaseId = pcf.Id,
                RowCause = 'Manual',
                CaseAccessLevel = 'Edit');

        insert share;
        System.debug('share id ' + share);
        System.debug('share record: ' + [SELECT Id FROM CaseShare WHERE UserOrGroupId = :patientUserId AND CaseId = :pcf.Id]);
    }

    public static Set<Id> findRecipients(Id studyId, String recipientsType) {
        Id userId = UserInfo.getUserId();
        Set<Id> recipients = new Set<Id>();
        if (recipientsType == 'PI' || recipientsType == 'SC' || recipientsType == 'PG') {
            List<Study_Team_Member__c> studyMembers = [
                    SELECT Id, User__c
                    FROM Study_Team_Member__c
                    WHERE Study__c = :studyId AND RecordType.DeveloperName = :recipientsType AND User__c != NULL
            ];
            for (Study_Team_Member__c m : studyMembers) {
                recipients.add(m.User__c);
            }
        } else if (recipientsType == 'Patient') {
            // find all cases
            List<Case> cases = [
                    SELECT Id, VTD1_Patient_User__c
                    FROM Case
                    WHERE RecordTypeId = :VT_D1_HelperClass.getRecordTypeCarePlan() AND VTD1_Study__c = :studyId AND VTD1_Patient_User__c != NULL
            ];
            // find patients
            List<Id> patientIds = new List<Id>();
            for (Case c : cases) {
                patientIds.add(c.VTD1_Patient_User__c);
            }
            // find caregivers
            List<Id> caregiverIds = VT_D1_PatientCaregiverBound.findCaregiversOfPatients(patientIds).values();
            recipients.addAll(patientIds);
            recipients.addAll(caregiverIds);
        }
        recipients.add(userId);
        return recipients;
    }

    public static void createChatMembers(Id chatId, Set<Id> userIds) {
        try {
            List<VTD1_Chat_Member__c> members = new List<VTD1_Chat_Member__c>();
            for (Id userId : userIds) {
                VTD1_Chat_Member__c member = new VTD1_Chat_Member__c();
                member.VTD1_Chat__c = chatId;
                member.VTD1_User__c = userId;
                members.add(member);
            }
            if (!members.isEmpty()) {
                insert members;
            }
        } catch (Exception e) {
            System.debug(e.getMessage() + ' ' + e.getStackTraceString());
        }
    }

    public static ConnectApi.FeedItemInput prepareFeedItemToSend(Id chatId, String message) {
        // create post
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = chatId;
        feedItemInput.visibility = ConnectApi.FeedItemVisibilityType.AllUsers;
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        // add text
        messageBodyInput.messageSegments.add(VT_D1_MentionHelper.getTextSeg(message));
        feedItemInput.body = messageBodyInput;
        return feedItemInput;
    }

    public static void createPCFChatMembers(Id pcfId, Set<Id> userIds) {
        try {
            List<VTD1_PCF_Chat_Member__c> members = new List<VTD1_PCF_Chat_Member__c>();
            for (Id userId : userIds) {
                VTD1_PCF_Chat_Member__c member = new VTD1_PCF_Chat_Member__c();
                member.VTD1_PCF__c = pcfId;
                member.VTD1_User_Id__c = userId;
                members.add(member);
            }
            if (!members.isEmpty()) {
                insert members;
                System.debug('PCFChatMembers: ' + members);
            }
        } catch (Exception e) {
            System.debug(e.getMessage() + ' ' + e.getStackTraceString());
        }
    }
//    public static List<VTD1_Chat_Member__c> getChatMembers(Id relatedId) {
//        List<VTD1_Chat_Member__c> members = new List<VTD1_Chat_Member__c>();
//        try {
//            if (relatedId!=null) {
//                members = [SELECT Id, VTD1_User__c, VTD1_Chat__c FROM VTD1_Chat_Member__c WHERE VTD1_Chat__c = :relatedId];
//            }
//        } catch (Exception e) {
//            System.debug(e.getMessage() + ' ' + e.getStackTraceString());
//        }
//        return members;
//    }
//    public static List<VTD1_PCF_Chat_Member__c> getPCFChatMembers(Id relatedId) {
//        List<VTD1_PCF_Chat_Member__c> members = new List<VTD1_PCF_Chat_Member__c>();
//        try {
//            if (relatedId!=null) {
//                members = [SELECT Id, VTD1_User_Id__c, VTD1_PCF__c FROM VTD1_PCF_Chat_Member__c WHERE VTD1_PCF__c = :relatedId];
//            }
//        } catch (Exception e) {
//            System.debug(e.getMessage() + ' ' + e.getStackTraceString());
//        }
//        return members;
//    }
}