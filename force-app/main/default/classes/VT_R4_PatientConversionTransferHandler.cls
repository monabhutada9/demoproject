/**
 * Created by user on 17.07.2020.
 */

public with sharing class VT_R4_PatientConversionTransferHandler  implements Callable {
    public static void processEvents(List <VTR4_New_Transaction__e> events) {
        for (VTR4_New_Transaction__e event : events) {
            VT_R4_PatientTransferService.PatientTransferServiceWrapper wrapper = (VT_R4_PatientTransferService.PatientTransferServiceWrapper)JSON.deserialize(event.VTR4_Parameters__c, VT_R4_PatientTransferService.PatientTransferServiceWrapper.class);

            System.debug('wrapper = ' + wrapper);
            //VT_R4_PatientTransferService.sendTNCatalogNotifications(wrapper.candidatePatientsTransfer, wrapper.isSuccess, wrapper.newCaseId);
            if (wrapper.isSuccess) {
                VT_R4_PatientTransferService.finalizeTransferSuccess(wrapper.candidatePatientsTransfer);
            } else {
                VT_R4_PatientTransferService.finalizeTransferFail(wrapper.candidatePatientsTransfer);
            }

        }
    }
    public Object call(String action, Map<String, Object> args) {
        processEvents((List <VTR4_New_Transaction__e>)args.get('events'));
        return null;
    }
}