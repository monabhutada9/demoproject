/**
 * @author: Dmitry Yartsev
 * @date: 27.04.2020
 * @description: Prevent CRA & CM from Downloading Patient Files when the Study level flag to allow Patient-level file download is set to TRUE
 */

public without sharing class VT_R4_PatientFilesDownloadBlocker implements Sfc.ContentDownloadHandlerFactory {

    private static String PATIENT_FILES_DOWNLOAD_ERROR = VT_D1_TranslateHelper.getLabelValue('VTR4_PatientFileDownloadForbidden');
    private static Set<String> PROFILES_FOR_RESTRICT_DOWNLOAD = new Set<String>{
            VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME,
            VT_R4_ConstantsHelper_ProfilesSTM.CM_PROFILE_NAME
    };

    /**
     * Get custom download handler that controls file download access for CRA and CM
     *
     * @param ids list of content Ids
     * @param context download context
     *
     * @return ContentDownloadHandler for a given list of content Ids and a download context
     */
    public Sfc.ContentDownloadHandler getContentDownloadHandler(List<Id> ids, Sfc.ContentDownloadContext context) {
        if (isNotSoqlContext(context)
                && isUserForRestrict()
                && isFileForBlocking(ids[0])) {
            return forbidDownloading();
        }
        return allowDownloading();
    }

    /**
     * Check SOQL context
     *
     * @param context download context
     *
     * @return true if not SOQL context, false if SOQL context
     */
    private Boolean isNotSoqlContext(Sfc.ContentDownloadContext context) {
        return context != Sfc.ContentDownloadContext.SOQL;
    }

    /**
     * Check that the current user have restricted access to patient files
     *
     * @return true if user profile in PROFILES_FOR_RESTRICT_DOWNLOAD, false if not
     */
    private Boolean isUserForRestrict() {
        return [
                SELECT Id
                FROM User
                WHERE Id = :UserInfo.getUserId()
                    AND Profile.Name IN :PROFILES_FOR_RESTRICT_DOWNLOAD
        ].size() > 0;
    }

    /**
     * Check that current file should not be downloaded
     *
     * @param contentVersionId Content version Id
     *
     * @return true if file should be blocked, false if not
     */
    private Boolean isFileForBlocking(Id contentVersionId) {
        List<ContentDocumentLink> contDocLinks = getDocumentLinks(getDocumentIdFromCVId(contentVersionId));
        Set<Id> relatedDocsIds = new Set<Id>();
        for (ContentDocumentLink link : contDocLinks) {
            if (isDocument(link.LinkedEntityId)) {
                relatedDocsIds.add(link.LinkedEntityId);
            }
        }
        if (!relatedDocsIds.isEmpty()) {
            if (!getFilteredRelatedDocuments(relatedDocsIds).isEmpty()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get content document Id from content version
     *
     * @param contentVersionId Content version Id
     *
     * @return Content version Id
     */
    private Id getDocumentIdFromCVId(Id contentVersionId) {
        return ((ContentVersion) new QueryBuilder(ContentVersion.class)
                .addField(ContentVersion.ContentDocumentId)
                .addConditions()
                .add(new QueryBuilder.CompareCondition('Id').eq(contentVersionId))
                .endConditions()
                .namedQueryRegister('VT_R4_PatientFilesDownloadBlocker.getDocumentIdFromCVId')
                .toSObject()).ContentDocumentId;
    }

    /**
     * Get list of content document links
     *
     * @param documentId Content version Id
     *
     * @return List of content document links
     */
    private List<ContentDocumentLink> getDocumentLinks(Id documentId) {
        return new QueryBuilder(ContentDocumentLink.class)
                .addField(ContentDocumentLink.LinkedEntityId)
                .addConditions()
                .add(new QueryBuilder.CompareCondition('ContentDocumentId').eq(documentId))
                .endConditions()
                .namedQueryRegister('VT_R4_PatientFilesDownloadBlocker.getDocumentLinks')
                .toList();
    }

    /**
     * Get list of documents satisfying the conditions
     *
     * @param relatedDocsIds Set of VTD1_Document__c Ids
     *
     * @return list of documents satisfying the conditions, empty list if there are no documents satisfying the conditions
     */
    private List<VTD1_Document__c> getFilteredRelatedDocuments(Set<Id> relatedDocsIds) {
        return new QueryBuilder(VTD1_Document__c.class)
                .addField('VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR4_DownloadPatientFiles__c')
                .addConditions()
                .add(new QueryBuilder.InCondition('Id').inCollection(relatedDocsIds))
                .add(new QueryBuilder.CompareCondition('VTD1_Clinical_Study_Membership__r.RecordTypeId').eq(VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN))
                .add(new QueryBuilder.NullCondition('VTD1_Clinical_Study_Membership__r.VTD1_Study__c').notNull())
                .add(new QueryBuilder.CompareCondition('VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR4_DownloadPatientFiles__c').eq(false))
                .setConditionOrder('1 AND 2 AND 3 AND 4')
                .endConditions()
                .namedQueryRegister('VT_R4_PatientFilesDownloadBlocker.getFilteredRelatedDocuments')
                .toList();
    }

    /**
     * Check that the given Id is VTD1_Document__c.Id
     *
     * @param sObjectId sObject Id
     *
     * @return true if given Id is VTD1_Document__c.Id, false if not
     */
    private Boolean isDocument(Id sObjectId) {
        return sObjectId.getSobjectType().getDescribe().getName() == 'VTD1_Document__c';
    }

    /**
     * Forbid downloading
     *
     * @return ContentDownloadHandler
     */
    private Sfc.ContentDownloadHandler forbidDownloading() {
        Sfc.ContentDownloadHandler contentDownloadHandler = new Sfc.ContentDownloadHandler();
        contentDownloadHandler.isDownloadAllowed = false;
        contentDownloadHandler.downloadErrorMessage = PATIENT_FILES_DOWNLOAD_ERROR;
        return contentDownloadHandler;
    }

    /**
     * Allow downloading
     *
     * @return ContentDownloadHandler
     */
    private Sfc.ContentDownloadHandler allowDownloading() {
        Sfc.ContentDownloadHandler contentDownloadHandler = new Sfc.ContentDownloadHandler();
        contentDownloadHandler.isDownloadAllowed = true;
        return contentDownloadHandler;
    }
}