/**
 * @description Manually add known candidate patient
 * @author Ruslan Mullayanov
 */
@RestResource(UrlMapping='/CandidatePatient')
global with sharing class VT_R2_AddNewPatientController {
    public static Map<Id, List<Study_Team_Member__c>> mapUserSTMs = new Map<Id, List<Study_Team_Member__c>>();
    global class PatientInfo {
        public String virtualSiteId;
        public String firstName;
        public String lastName;
        public String addressCountry;
        public String preferredLanguage;
        public String secondaryLanguage;
        public String tertioryLanguage;
        public String addressLine1;
        public String addressLine2;
        public String addressLine3;
        public String cityTownVillage;
        public String stateProvince;
        public String zipCode;
        public String email;
        public String phone;
        public String phoneType;
        public String patientId;
        public String patientStatus;
        public String createdById;
        public CaregiverInfo cg;
        public Date ltfuLastDoseDate;
        public String ltfuStudyNumber;
        public String ltfuCohortName;
        public String ltfuPatientId;
        public String gender;
        public Date birthdate;
    }
    global class CaregiverInfo {
        public String firstName;
        public String lastName;
        public String preferredLanguage;
        public String cgsecondaryLanguage;
        public String cgtertioryLanguage;
        public String email;
        public String phone;
        public String phoneType;
    }
    global class VirtualSiteWrapper {
        Virtual_Site__c virtualSite;
        Date currentDate;
    }
   /*Added by IQVIA Strikers for SH-8120*/
    @AuraEnabled
    public static String fetchProfileInfo(){
        Id profileId = UserInfo.getProfileId();
        String profileName=[Select Name from Profile where Id=:profileId].Name;
        //String profileName=[Select Name from Profile where Id='00e3m000001b1ePAAQ'].Name;
        System.debug('ProfileName'+profileName);//00e3m000001b1ePAAQ
        //return profileName;
        if(profileName=='Site Coordinator'){
            return 'External User';
        }
        else{
            return 'Internal User';
        }
       //return null;
    }
    @AuraEnabled
    public static String createCandidatePatientRemote(String patientString) {
        PatientInfo pInfo = (PatientInfo)JSON.deserialize(patientString, PatientInfo.class);
        pInfo.createdById = UserInfo.getUserId();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Authorization', 'Bearer ' + VT_D1_HelperClass.getSessionIdForGuest());
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(System.Url.getOrgDomainUrl().toExternalForm() + '/services/apexrest/CandidatePatient');
        req.setBody(JSON.serialize(pInfo));
        req.setTimeout(120000);
        System.debug(req);
        System.debug(req.getBody());
        Http http = new Http();
        HttpResponse res = http.send(req);
        System.debug(res.getStatus() +' '+res.getBody());
        if (res.getStatusCode()!=200) {
            throw new AuraHandledException(res.getStatus() +' '+ res.getBody());
        }
        return res.getBody();
    }
    @HttpPost
    global static void doPost() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        String requestBody = request.requestBody.toString();
        try {
            PatientInfo patientInfo = (PatientInfo)JSON.deserialize(requestBody, PatientInfo.class);
            response.responseBody = Blob.valueOf(createCandidatePatient(patientInfo));
            response.statusCode = 200;
        } catch (Exception e) {
            response.responseBody = Blob.valueOf(e.getMessage());
            response.statusCode = 500;
        }
    }
	@TestVisible
    private static String createCandidatePatient(PatientInfo patientInfo) {
        system.debug(patientInfo);
        HealthCloudGA__CandidatePatient__c patient = new HealthCloudGA__CandidatePatient__c();
        Boolean studyOnHold = false;
        //try {
        if (patientInfo!=null && patientInfo.virtualSiteId!=null) {
            Virtual_Site__c virtualSite = getVirtualSiteRemote(patientInfo.virtualSiteId, null, null);
            Id studyId = virtualSite.VTD1_Study__c;
            if (studyId!=null) {
                studyOnHold = virtualSite.VTD1_Study__r.VTD1_Recruitment_Hold__c;
            }
            patient.Study__c = studyId;
            patient.VTD1_ProtocolNumber__c = studyId!=null? virtualSite.VTD1_Study__r.Name : null;
            patient.VTD1_One_Key_ID__c = virtualSite.VTD1_PI_User__r.People_Soft_ID__c;
            patient.VTR2_Pi_Id__c = virtualSite.VTD1_PI_User__c;

            patient.VT_R4_VirtualSite__c = patientInfo.virtualSiteId; // SH-20046

            //if (patient.VTD1_One_Key_ID__c == null) {patient.VTD1_One_Key_ID__c = '123softId';}
            patient.rr_firstName__c = patientInfo.firstName;
            patient.rr_lastName__c = patientInfo.lastName;
            patient.HealthCloudGA__Address1Country__c = patientInfo.addressCountry;
            patient.VTD2_Language__c = patientInfo.preferredLanguage;
            /*Added by IQVIA Strikers SH-8120 for Add New Patient Wizard*/
            patient.VT_R5_Secondary_Preferred_Language__c = patientInfo.secondaryLanguage;
            patient.VT_R5_Tertiary_Preferred_Language__c = patientInfo.tertioryLanguage;
            patient.HealthCloudGA__Address1State__c = patientInfo.stateProvince;
            patient.HealthCloudGA__Address1PostalCode__c = patientInfo.zipCode;
            patient.VTR4_Gender__c = patientInfo.gender;
            patient.HealthCloudGA__BirthDate__c = patientInfo.birthdate;
            patient.HealthCloudGA__Address1Line1__c = patientInfo.addressLine1;
            patient.HealthCloudGA__Address1Line2__c = patientInfo.addressLine2;
            patient.VTR3_AddressLine3__c = patientInfo.addressLine3;
            patient.HealthCloudGA__Address1City__c = patientInfo.cityTownVillage;
            patient.rr_Email__c = patientInfo.email;
            patient.VTD1_Patient_Phone__c = patientInfo.phone;
            patient.VTR2_Patient_Phone_Type__c = patientInfo.phoneType;
            patient.VTR2_Subject_ID__c = patientInfo.patientId;
            patient.VTR2_Patient_Status__c = patientInfo.patientStatus;
            patient.VTR2_Created_By_User__c = patientInfo.createdById;
            patient.VTR3_Study_Number__c = patientInfo.ltfuStudyNumber;
            patient.VTR3_CohortName__c = patientInfo.ltfuCohortName;
            patient.VTR3_LTFU_PatientID__c = patientInfo.ltfuPatientId;
            patient.VTR3_LastDoseDate__c = patientInfo.ltfuLastDoseDate;
            patient.VTR5_Priority__c = 2;

            if(virtualSite.VTD1_Study__r.VT_R4_RescueTelVisitStudy__c){
                patient.VT_R4_VirtualSite__c = virtualSite.Id;
                //SH-15071: according the logic of VT_R4_StudyTeamAssignment.getStudyTeam(see PIsByStudyRescueMap), virtual site should be populated on patient of rescue study. Otherwise, virual site on case is always empty
            }


            if (patientInfo.cg!=null) {
                CaregiverInfo caregiverInfo = patientInfo.cg;
                patient.VTR2_Caregiver_First_Name__c = caregiverInfo.firstName;
                patient.VTR2_Caregiver_Last_Name__c = caregiverInfo.lastName;
                patient.VTR2_Caregiver_Preferred_Language__c = caregiverInfo.preferredLanguage;
                patient.VT_R5_Caregiver_Secondary_Language__c = caregiverInfo.cgsecondaryLanguage;
                patient.VT_R5_Caregiver_Tertiary_Language__c = caregiverInfo.cgtertioryLanguage;
                patient.VTR2_Caregiver_Email__c = caregiverInfo.email;
                patient.VTR2_Caregiver_Phone__c = caregiverInfo.phone;
                patient.VTR2_Caregiver_Phone_Type__c = caregiverInfo.phoneType;
            }
        }
        System.debug('patient '+patient);
        List<String> requiredFields = new List<String>{
                patient.Study__c,
                patient.VTD1_ProtocolNumber__c,
                patient.rr_firstName__c,
                patient.rr_lastName__c,
                patient.HealthCloudGA__Address1Country__c,
                patient.VTD2_Language__c,
                patient.HealthCloudGA__Address1State__c,
                patient.HealthCloudGA__Address1PostalCode__c,
                patient.rr_Email__c
        };
        System.debug('requiredFields '+requiredFields);
        if (!requiredFields.contains(null) && !requiredFields.contains('') && !studyOnHold) {
            insert patient;
            System.debug('patient inserted '+patient.Id);
        }
        //} catch (Exception e) {
        //System.debug(e.getMessage() + '\n' + e.getStackTraceString());//
        //return e.getMessage() + '\n' + e.getStackTraceString();
        //throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        //}
        //Test Comment
        return patient.Id;
    }
    @AuraEnabled
    public static String getVirtualSiteWrapperRemote(Id virtualSiteId, Id piUserId, Id studyId) {
        VirtualSiteWrapper wrapper = new VirtualSiteWrapper();
        wrapper.virtualSite = getVirtualSiteRemote(virtualSiteId, piUserId, studyId);
        wrapper.currentDate = Datetime.newInstance(System.currentTimeMillis()).date();
        return JSON.serialize(wrapper);
    }
    @AuraEnabled
    public static Virtual_Site__c getVirtualSiteRemote(Id virtualSiteId, Id piUserId, Id studyId) {
        Virtual_Site__c virtualSite = new Virtual_Site__c();
        String whereCriteria;
        if (virtualSiteId!=null) {
            //virtualSite = getVirtualSite(virtualSiteId);
            whereCriteria = 'Id=:virtualSiteId';
        } else if (piUserId!=null && studyId!=null) {
            //virtualSite = getVirtualSite(piUserId, studyId);
            whereCriteria = 'VTD1_PI_User__c=:piUserId AND VTD1_Study__c=:studyId';
        }
        if (whereCriteria!=null) {
            List<Virtual_Site__c> virtualSites = Database.query('SELECT Name, VTD1_Study_Site_Number__c, VTD1_Study__c, VTD1_Study__r.Name,VTD1_Study__r.VTR5_External_Subject_ID_Input_Type__c, VTD1_Study__r.VTR5_Requires_External_Subject_Id__c, VTD1_Study__r.VTD1_Recruitment_Hold__c, VTD1_Study__r.VTR3_LTFU__c, VTD1_Study__r.VTR2_EnrollmentStatus__c, VTD1_Study__r.VTR2_Caregiver__c, VTD1_Study__r.VT_R4_RescueTelVisitStudy__c, VTD1_PI_User__c, VTD1_PI_User__r.Name, VTD1_PI_User__r.People_Soft_ID__c FROM Virtual_Site__c WHERE ' + whereCriteria + ' LIMIT 1');
            if (!virtualSites.isEmpty()) {
                virtualSite = virtualSites[0];
            }
        }
        return virtualSite;
    }
    public class PIUserInfo {
        @AuraEnabled public String Id;
        @AuraEnabled public String Name;
        @AuraEnabled public List<StudyInfo> StudyList;
    }
    public class StudyInfo {
        @AuraEnabled public String Id;
        @AuraEnabled public String Name;
        @AuraEnabled public Boolean OnHold;
        @AuraEnabled public Boolean AssignedToSCR;
        @AuraEnabled public String VTSLName;
        @AuraEnabled public String VTSLPhone;
        @AuraEnabled public Boolean isLTFU;
        //@AuraEnabled public Boolean PatientIDConfigurable;
        //@AuraEnabled public String EnrollmentStatus;
    }

    /*******************************************************************************************************
    * @author                   Priyanka Ambre (SH-19460)
    * @description              get the Primary PI and Studies associated to the logged in PI User.
    */
    @AuraEnabled(Cacheable=true)
    public static List<PIUserInfo> getPIAndStudyInfoForPI() {
        Map<Id, List<Study_Team_Member__c>> mapUserSTMs = new Map<Id, List<Study_Team_Member__c>>(); 
        //get logged in User
        Id userId = UserInfo.getUserId();


        List<Study_Team_Member__c> lstSTMs =    [SELECT //Fetching fields for Primary PI
                                                        Id, 
                                                        VTD1_Backup_PI__c, 
                                                        VTD1_UserName__c,
                                                        User__c, 
                                                        Study__c, 
                                                        VTD1_VirtualSite__c,                                                             
                                                        Study__r.Name,
                                                        Study__r.VTR3_LTFU__c,
                                                        Study__r.VTD1_Recruitment_Hold__c,
                                                        Study__r.VTD1_Virtual_Trial_Study_Lead__r.Name,
                                                        Study__r.VTD1_Virtual_Trial_Study_Lead__r.Phone,
                                                        //Fetching fields for Backup PI                                                            
                                                        Study_Team_Member__c,
                                                        Study_Team_Member__r.VTD1_UserName__c, 
                                                        Study_Team_Member__r.User__c, 
                                                        Study_Team_Member__r.VTD1_VirtualSite__c, 
                                                        Study_Team_Member__r.Study__c,
                                                        Study_Team_Member__r.Study__r.VTR3_LTFU__c,
                                                        Study_Team_Member__r.Study__r.VTR2_EnrollmentStatus__c,                                                            
                                                        Study_Team_Member__r.Study__r.Name,
                                                        Study_Team_Member__r.Study__r.VTD1_Recruitment_Hold__c,
                                                        Study_Team_Member__r.Study__r.VTD1_Virtual_Trial_Study_Lead__r.Name,
                                                        Study_Team_Member__r.Study__r.VTD1_Virtual_Trial_Study_Lead__r.Phone                                                            
                                                FROM Study_Team_Member__c WHERE User__c =: userId

                                                WITH SECURITY_ENFORCED ORDER BY Study__r.Id ASC ];            


            for(Study_Team_Member__c objSTM : lstSTMs){
                    //If logged in User is Primary PI
                    if(objSTM.User__c == userId && objSTM.VTD1_Backup_PI__c == false && objSTM.Study__c != null &&
                    objSTM.VTD1_VirtualSite__c != null){
                        addSTMInMap(objSTM, mapUserSTMs);    
                    }
                    
                    //If logged in User is Backup PI
                    if(objSTM.User__c == userId && objSTM.VTD1_Backup_PI__c == true && //if logged in user is a Backup PI.
                            objSTM.Study_Team_Member__c != null &&
                            objSTM.Study_Team_Member__r.Study__c != null &&
                            objSTM.Study_Team_Member__r.VTD1_VirtualSite__c != null){
                                addSTMInMap(objSTM.Study_Team_Member__r, mapUserSTMs); 
                    }
                }
                //Fetching fields for Sub-I
                List<Study_Site_Team_Member__c> lstSSTMs = [SELECT  VTR2_Associated_PI3__c, 
                                                                VTR2_Associated_SubI__c,
                                                                VTR2_Associated_PI3__r.VTD1_UserName__c, 
                                                                VTR2_Associated_PI3__r.Study__c,
                                                                VTR2_Associated_PI3__r.Study__r.Name,
                                                                VTR2_Associated_PI3__r.Study__r.VTR3_LTFU__c,
                                                                VTR2_Associated_PI3__r.Study__r.VTD1_Recruitment_Hold__c,
                                                                VTR2_Associated_PI3__r.Study__r.VTD1_Virtual_Trial_Study_Lead__r.Name,
                                                                VTR2_Associated_PI3__r.Study__r.VTD1_Virtual_Trial_Study_Lead__r.Phone,
                                                                VTR2_Associated_PI3__r.VTD1_VirtualSite__c, 
                                                                VTR2_Associated_PI3__r.User__c FROM Study_Site_Team_Member__c 
                                                            WHERE VTR2_Associated_PI3__r.VTD1_VirtualSite__c <> Null

                                                            AND VTR2_Associated_SubI__r.User__c=: userId WITH SECURITY_ENFORCED];

                for(Study_Site_Team_Member__c objSSTM : lstSSTMs){
                    Study_Team_Member__c primarySTM = objSSTM.VTR2_Associated_PI3__r;
                    if(primarySTM.Study__c != null && 
                        primarySTM.VTD1_VirtualSite__c != null && 
                        primarySTM.User__c != null){
                            addSTMInMap(primarySTM, mapUserSTMs);
                        }  
                }                                


            return getPIStudyInfoWrapperMap(mapUserSTMs);
    }

    /*******************************************************************************************************
    * @author                   Priyanka Ambre (SH-19460)
    * @description              Add STM in the map
    */
    public static void addSTMInMap(Study_Team_Member__c objSTM, Map<Id, List<Study_Team_Member__c>> mapUserSTMs){
        if (!mapUserSTMs.containsKey(objSTM.User__c)) {
            mapUserSTMs.put(objSTM.User__c, new List<Study_Team_Member__c>());
        }
        mapUserSTMs.get(objSTM.User__c).add(objSTM);
    }

    /*******************************************************************************************************
    * @author                   Priyanka Ambre (SH-19460)
    * @description              Used to create List of PIUserInfo.
    */
    public static List<PIUserInfo> getPIStudyInfoWrapperMap(Map<Id, List<Study_Team_Member__c>> mapUserSTMs){
        List<PIUserInfo> piUserInfoList = new List<PIUserInfo>();
        for(Id piUserId : mapUserSTMs.keyset()){

            if(mapUserSTMs.get(piUserId) != null && mapUserSTMs.get(piUserId).size()>0){
            PIUserInfo piUserInfo = new PIUserInfo();
            piUserInfo.Id = piUserId;
            //As all the STM's have same Users on it and will have the same usernames.
            piUserInfo.Name = mapUserSTMs.get(piUserId)[0].VTD1_UserName__c;
                        

            piUserInfo.StudyList = new List<StudyInfo>();            
                for (Study_Team_Member__c stm : mapUserSTMs.get(piUserId)) {
                    StudyInfo studyInfo = new StudyInfo();
                    studyInfo.Id = stm.Study__c;
                    studyInfo.Name = stm.Study__r.Name;
                    studyInfo.AssignedToSCR = true;
                    studyInfo.OnHold = stm.Study__r.VTD1_Recruitment_Hold__c;
                    studyInfo.VTSLName = stm.Study__r.VTD1_Virtual_Trial_Study_Lead__r.Name;
                    studyInfo.VTSLPhone = stm.Study__r.VTD1_Virtual_Trial_Study_Lead__r.Phone;
                    studyInfo.isLTFU = stm.Study__r.VTR3_LTFU__c;
                    //studyInfo.EnrollmentStatus = stm.Study__r.VTR2_EnrollmentStatus__c;
                    piUserInfo.StudyList.add(studyInfo);
                }
                piUserInfoList.add(piUserInfo);
            }

         }

            return piUserInfoList;
        }

    
    
    @AuraEnabled(Cacheable=true)
    public static List<PIUserInfo> getPIAndStudyInfoList() {
        List<PIUserInfo> piUserInfoList = new List<PIUserInfo>();
        Id SCRId = UserInfo.getUserId(); //'0052a000001QoM9AAK'
        List<Study_Site_Team_Member__c> piMembers = [
                SELECT VTR2_Associated_PI__c, VTR2_Associated_PI__r.User__c, VTR2_Associated_PI__r.VTD1_UserName__c,
                        VTR2_Associated_PI__r.Study__c
                FROM Study_Site_Team_Member__c
                WHERE VTR2_Associated_SCr__r.User__c = :SCRId AND VTR2_Associated_PI__c!=NULL
                ORDER BY VTR2_Associated_PI__r.VTD1_UserName__c ASC
        ];
        System.debug('piMembers '+piMembers.size());
        Set<Id> piSTMIdsAssignedToSCR = new Set<Id>();
        Map<Id,Study_Site_Team_Member__c> piToSSTM = new Map<Id,Study_Site_Team_Member__c>();
        for (Study_Site_Team_Member__c sstm : piMembers) {
            piToSSTM.put(sstm.VTR2_Associated_PI__r.User__c, sstm);
            piSTMIdsAssignedToSCR.add(sstm.VTR2_Associated_PI__c);
        }
        System.debug('piUserIds ' +piToSSTM.keySet());
        System.debug('piSTMIdsAssignedToSCR ' +piSTMIdsAssignedToSCR);
        List<Study_Team_Member__c> stmList = [
                SELECT User__c, VTD1_UserName__c, Study__c, Study__r.Name, Study__r.VTD1_Recruitment_Hold__c, Study__r.VTR3_LTFU__c,
                        Study__r.VTD1_Virtual_Trial_Study_Lead__r.Name, Study__r.VTD1_Virtual_Trial_Study_Lead__r.Phone,
                        VTD1_VirtualSite__c, VTD1_VirtualSite__r.Name
                FROM Study_Team_Member__c
                WHERE User__c IN :piToSSTM.keySet()
                AND VTD1_VirtualSite__c != NULL
                //AND VTD1_Type__c =: VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME
                ORDER BY Study__r.Name ASC
        ];
        Map<Id,List<Study_Team_Member__c>> stmMap = new Map<Id,List<Study_Team_Member__c>>();
        for (Study_Team_Member__c stm : stmList) {
            if (!stmMap.containsKey(stm.User__c)) {
                stmMap.put(stm.User__c, new List<Study_Team_Member__c>());
            }
            stmMap.get(stm.User__c).add(stm);
        }
        for (Id piUserId : piToSSTM.keySet()) {
            PIUserInfo piUserInfo = new PIUserInfo();
            piUserInfo.Id = piUserId;
            piUserInfo.Name = piToSSTM.get(piUserId).VTR2_Associated_PI__r.VTD1_UserName__c;
            piUserInfo.StudyList = new List<StudyInfo>();
            if (stmMap.containsKey(piUserId)) {
                for (Study_Team_Member__c stm : stmMap.get(piUserId)) {
                    StudyInfo studyInfo = new StudyInfo();
                    studyInfo.Id = stm.Study__c;
                    studyInfo.Name = stm.Study__r.Name;
                    studyInfo.AssignedToSCR = piSTMIdsAssignedToSCR.contains(stm.Id);
                    studyInfo.OnHold = stm.Study__r.VTD1_Recruitment_Hold__c;
                    studyInfo.VTSLName = stm.Study__r.VTD1_Virtual_Trial_Study_Lead__r.Name;
                    studyInfo.VTSLPhone = stm.Study__r.VTD1_Virtual_Trial_Study_Lead__r.Phone;
                    studyInfo.isLTFU = stm.Study__r.VTR3_LTFU__c;
                    //studyInfo.EnrollmentStatus = stm.Study__r.VTR2_EnrollmentStatus__c;
                    piUserInfo.StudyList.add(studyInfo);
                }
                piUserInfoList.add(piUserInfo);
            }
        }
        return piUserInfoList;
    }
    /*    public static Virtual_Site__c getVirtualSite(Id virtualSiteId) {
        Virtual_Site__c virtualSite = new Virtual_Site__c();
        if (virtualSiteId!=null) {
            List<Virtual_Site__c> virtualSites = [SELECT Name, VTD1_Study__c, VTD1_Study__r.Name,
                    VTD1_Study__r.VTD1_Recruitment_Hold__c, VTD1_PI_User__c, VTD1_PI_User__r.Name, VTD1_PI_User__r.People_Soft_ID__c
            FROM Virtual_Site__c WHERE Id=:virtualSiteId];
            if (!virtualSites.isEmpty()) {
                virtualSite = virtualSites[0];
            }
        }
        return virtualSite;
    }
    public static Virtual_Site__c getVirtualSite(Id piUserId, Id studyId) {
        Virtual_Site__c virtualSite = new Virtual_Site__c();
        if (piUserId!=null && studyId!=null) {
            List<Virtual_Site__c> virtualSites = [SELECT Name, VTD1_Study__c, VTD1_Study__r.Name,
                    VTD1_Study__r.VTD1_Recruitment_Hold__c, VTD1_PI_User__c, VTD1_PI_User__r.Name, VTD1_PI_User__r.People_Soft_ID__c
            FROM Virtual_Site__c WHERE VTD1_PI_User__c=:piUserId AND VTD1_Study__c=:studyId];
            if (!virtualSites.isEmpty()) {
                virtualSite = virtualSites[0];
            }
        }
        return virtualSite;
    }*/
	//this is comment to fix incirrect merge
    @AuraEnabled(Cacheable=true)
    public static String getPreScreeningCriteriaListRemote(Id virtualSiteId) {
        List<VTR2_Pre_Screening_Criteria__c> criteriaList = new List<VTR2_Pre_Screening_Criteria__c>();
        if (virtualSiteId!=null) {
            Virtual_Site__c virtualSite = getVirtualSiteRemote(virtualSiteId, null, null);
            criteriaList = getPreScreeningCriteriaList(virtualSite.VTD1_Study__c);
        }
        return JSON.serialize(criteriaList);
    }
    @AuraEnabled
    public static String createPreScreening(Id virtualSiteId, String failedCriteria) {
        VTR2_Pre_Screening__c preScreening = new VTR2_Pre_Screening__c(
                VTR2_Virtual_Site__c=virtualSiteId,
                VTR2_Status__c='Pass',
                VTR2_Failed_Criteria__c = 'N/A'
        );
        if (failedCriteria!=null) {
            preScreening.VTR2_Status__c='Fail';
            preScreening.VTR2_Failed_Criteria__c = failedCriteria;
        }
        try {
            insert preScreening;
            return preScreening.Id;
        } catch (Exception e) {
            System.debug(e.getMessage() + '\n' + e.getStackTraceString());
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
    public class InputSelectOption{
        @AuraEnabled public String value;
        @AuraEnabled public String label;
        public InputSelectOption(String value, String label){
            this.value = value;
            this.label = label;
        }
    }
    @AuraEnabled(Cacheable=true)
    public static  List<InputSelectOption> getInputSelectOptions(String sObjName, String fieldName){
        List<InputSelectOption> options = new List<InputSelectOption>();
        if (!String.isEmpty(sObjName) && !String.isEmpty(fieldName)) {
            Schema.SObjectType objType = Schema.getGlobalDescribe().get(sObjName);
            SObject sObj = objType.newSObject();
            List<List<String>> optionsPairsList = VT_D1_CustomMultiPicklistController.getSortedSelectOptionsWithLabel(sObj, fieldName);
            for (List<String> optionPair : optionsPairsList) {
                InputSelectOption option = new InputSelectOption(optionPair[0], optionPair[1]);
                options.add(option);
            }
        }
        return options;
    }
    public static List<VTR2_Pre_Screening_Criteria__c> getPreScreeningCriteriaList(Id studyId) {
        return [SELECT VTR2_Criteria__c FROM VTR2_Pre_Screening_Criteria__c WHERE VTR2_Study__c=:studyId];
    }
    @AuraEnabled
    public static Boolean isEmailUsed(String email) {
        return VT_R2_DuplicateUsernameChecker.doPost(email);
    }
    @AuraEnabled(Cacheable=true)
    public static Boolean isMobileUsed(String phoneString) {
        VT_D1_Phone__c potentialPhone = new VT_D1_Phone__c(PhoneNumber__c = phoneString);
        potentialPhone.recalculateFormulas();
        return ![
                SELECT Id
                FROM VT_D1_Phone__c
                WHERE VTR2_NumberWithoutDigits__c = : potentialPhone.VTR2_NumberWithoutDigits__c
                AND Type__c = 'Mobile'
        ].isEmpty();
    }
}