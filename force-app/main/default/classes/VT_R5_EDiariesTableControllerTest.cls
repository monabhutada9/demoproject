/** Modified Vijendra: SH-20622 Add the getEDiariesTotalScoreDataTest Method */
/** Modified by Priyanka: SH-21178 Added test methods for getEdiaryFilters and getEdiaryIds */
@IsTest
public with sharing class VT_R5_EDiariesTableControllerTest{
    
    public static void getDataTest(){
        VTR5_DiaryListView__c dlv = new VTR5_DiaryListView__c();
        insert dlv;

        Map<String, Object> params = new Map<String, Object>{'limit' => (Object) '1000'};
        VT_R5_EDiariesTableController eData = VT_R5_EDiariesTableController.getEdiaryRecords(null, JSON.serialize(params), null, true);
        
        List<Id> eDiaryIds = new List<Id>();
        List<VT_R5_EDiariesTableController.EDiaryWrapper> eDiaries = eData.items;
        for(VT_R5_EDiariesTableController.EDiaryWrapper ew : eDiaries){
            eDiaryIds.add(ew.eDiary.Id);
        }
        update new VTD1_Survey__c (Id = eDiaryIds.get(0), VTR5_Response_GUID__c = 'test123');
        Attachment attachment = new Attachment();
        attachment.ParentId = dlv.Id;
        attachment.Name = 'Test Attachment for Diary';
        attachment.Body = blob.valueOf('test123');
        
        insert attachment;

        Test.startTest();
            Map<String, Object> filters = new Map<String, Object>{'markReviewedFilter' => (Object) 'false', 'totalScoreValue' => (Object) '50',
                                                                  'statusFilters' => (Object) new List<String>{eDiaries.get(0).eDiary.VTD1_Status__c}};
            Map<String, Object> order = new Map<String, Object>{'columnName' => 'eDiaryName', 'sorting' => 'asc'};                                                       

            params.put('filterMap', filters);
            String searchName = eDiaries.get(0).eDiary.Name.substring(0, 3);
            eData = VT_R5_EDiariesTableController.getEdiaryRecords(dlv.Id, JSON.serialize(params), searchName, true);
        
            order = new Map<String, Object>{'columnName' => 'phoneNumber', 'sorting' => 'asc'};
            params.put('order', order);
            eData = VT_R5_EDiariesTableController.getEdiaryRecords(dlv.Id, JSON.serialize(params), searchName, true);
            
            params.put('order', order);
            order = new Map<String, Object>{'columnName' => 'dateCompleted', 'sorting' => 'asc'};
            eData = VT_R5_EDiariesTableController.getEdiaryRecords(dlv.Id, JSON.serialize(params), searchName, true);
        
			//Added by :Conquerors, Vijendra Hire SH-20622	
            //START	
        	params.put('order', order);
            order = new Map<String, Object>{'columnName' => 'totalScore', 'sorting' => 'asc'};
            eData = VT_R5_EDiariesTableController.getEdiaryRecords(dlv.Id, JSON.serialize(params), '0', true);
        	List<VT_R5_EDiariesTableController.EDiaryWrapper> eDiary = eData.items;
        	//End
        Test.stopTest();
    }







    public static void getEDiariesVirtualSiteDataTest(){
        VTR5_DiaryListView__c diaryViewList = new VTR5_DiaryListView__c();
        insert diaryViewList;
        
        Map<String, Object> ediaryFilterMap = new Map<String, Object>{'limit' => (Object) '1000'};
        VT_R5_EDiariesTableController ediaryRecords = VT_R5_EDiariesTableController.getEdiaryRecords(null, JSON.serialize(ediaryFilterMap), null, true);
        List<VT_R5_EDiariesTableController.EDiaryWrapper> eDiaries = ediaryRecords.items;      
        List<VT_R5_EDiariesTableController.ColumnWrapper> virtualsites = ediaryRecords.virtualsite;
        List<VT_R5_EDiariesTableController.ColumnWrapper> ediaryNames = ediaryRecords.ediaryName;
        set<String> actualvirtualsitecolumn = new set<String>();
        for(VT_R5_EDiariesTableController.ColumnWrapper vs : virtualsites){
            actualvirtualsitecolumn.add(vs.columnName);
        }
        set<String> actualediarycolumn = new set<String>();
        for(VT_R5_EDiariesTableController.ColumnWrapper edn : ediaryNames){
            actualediarycolumn.add(edn.columnName);
        }
        for(VT_R5_EDiariesTableController.EDiaryWrapper ediaryRecord : eDiaries){
            system.assertEquals(true, actualediarycolumn.contains(ediaryRecord.eDiary.Name));
            system.assertEquals(true, actualvirtualsitecolumn.contains(ediaryRecord.eDiary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name));
        }
        Test.startTest();
        List<String> virtualSiteFilterList = new List<String>{eDiaries.get(0).eDiary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name,
                                                                                    eDiaries.get(1).eDiary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name};
                                                                                        
        Map<String, Object> filters = new Map<String, Object>{
                                                                                       'studyId' => eDiaries.get(0).eDiary.VTD1_CSM__r.VTD1_Study__c,   
                                                                                       'virtualSitesFilters' => (Object) virtualSiteFilterList
                                                                                           };                                                     
            ediaryFilterMap.put('filterMap', filters);
        ediaryRecords = VT_R5_EDiariesTableController.getEdiaryRecords(diaryViewList.Id, JSON.serialize(ediaryFilterMap), null, true);
        List<VT_R5_EDiariesTableController.EDiaryWrapper> filtereDiaries = ediaryRecords.items;
        Set<String> ActualVirtualSites = new Set<String>();
        for(VT_R5_EDiariesTableController.EDiaryWrapper ew : filtereDiaries)
        {    
            ActualVirtualSites.add(ew.eDiary.VTD1_CSM__r.VTD1_Virtual_Site__r.Name);
        }
        for(String virtualSiteString: virtualSiteFilterList){
        	system.assertEquals(true, ActualVirtualSites.contains(virtualSiteString));
                }
        //System.assertEquals(ActualVirtualSites, virtualSiteFilterList);
        List<String> eDiariesFilterList = new List<String>{eDiaries.get(1).eDiary.Name};
        filters.put('eDiaryNameFilters', (Object) eDiariesFilterList);
        ediaryFilterMap.put('filterMap', filters);
        ediaryRecords = VT_R5_EDiariesTableController.getEdiaryRecords(diaryViewList.Id, JSON.serialize(ediaryFilterMap), null, true);
        filtereDiaries = ediaryRecords.items;
        Set<String> ActualeDiaryNames = new Set<String>();
        for(VT_R5_EDiariesTableController.EDiaryWrapper ew : filtereDiaries)
        {    
            ActualeDiaryNames.add(ew.eDiary.Name);
            }  
        for(String ediaryNameString: eDiariesFilterList){
        	system.assertEquals(true, ActualeDiaryNames.contains(ediaryNameString));
        } 
        Test.stopTest();
    }
    
    public static void getEDiariesNameDataTest(){
        VTR5_DiaryListView__c diaryViewList = new VTR5_DiaryListView__c();
        insert diaryViewList;
        
        Map<String, Object> ediaryFilterMap = new Map<String, Object>{'limit' => (Object) '1000'};
            VT_R5_EDiariesTableController ediaryRecords = VT_R5_EDiariesTableController.getEdiaryRecords(null, JSON.serialize(ediaryFilterMap), null, true);
        List<VT_R5_EDiariesTableController.EDiaryWrapper> eDiaries = ediaryRecords.items;
        Test.startTest();
        List<String> eDiariesFilterList = new List<String>{eDiaries.get(0).eDiary.Name,eDiaries.get(1).eDiary.Name};
        Map<String, Object> filters = new Map<String, Object>{
                                                                                         'studyId' => eDiaries.get(0).eDiary.VTD1_CSM__r.VTD1_Study__c, 
                                                                                         'eDiaryNameFilters' => (Object) eDiariesFilterList
        };                                                     
        ediaryFilterMap.put('filterMap', filters);
        ediaryRecords = VT_R5_EDiariesTableController.getEdiaryRecords(diaryViewList.Id, JSON.serialize(ediaryFilterMap), null, true);
        List<VT_R5_EDiariesTableController.EDiaryWrapper> filtereDiaries = ediaryRecords.items;
        Set<String> ActualeDiaryNames = new Set<String>();
        for(VT_R5_EDiariesTableController.EDiaryWrapper ew : filtereDiaries)
        {
            ActualeDiaryNames.add(ew.eDiary.Name);
        }
        for(String ediaryNameString: eDiariesFilterList){
        	system.assertEquals(true, ActualeDiaryNames.contains(ediaryNameString));
        }
        Test.stopTest();
                }

    public static void updateEdiaryTest() {
        Id caseId = [SELECT Id FROM Case LIMIT 1].Id;

        VTD1_Survey__c survey = new VTD1_Survey__c(
                Name = 'Test eDiary',
                VTD1_CSM__c = caseId,
                RecordTypeId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('ePRO').getRecordTypeId(),
                VTR4_Percentage_Completed__c = 100,
                VTD1_Status__c = 'Review Required'
        );
        insert survey;

        Test.startTest();
        survey = [SELECT VTR4_Percentage_Completed__c, RecordType.DeveloperName FROM VTD1_Survey__c WHERE Id = :survey.Id];
        survey.VTR5_Reviewed__c = true;
        String result = VT_R5_EDiariesTableController.updateEdiary(survey);
        Test.stopTest();

        System.assertEquals(String.valueOf(true), result);
        System.assert(survey.VTR5_Reviewed__c);
        System.assertEquals(VT_R4_ConstantsHelper_Statuses.SURVEY_REVIEWED, survey.VTD1_Status__c);
    }

    public static void updateEdiaryNegativeTest() {
        VTD1_Survey__c survey = new VTD1_Survey__c();

        Test.startTest();
        String result = VT_R5_EDiariesTableController.updateEdiary(survey);
        Test.stopTest();


        System.assertNotEquals(String.valueOf(true), result);
    }

    public static void prefiltersTest() {
        Case ptCase = [SELECT Id, VTD1_Study__c FROM Case LIMIT 1];
        String surveyName = 'Test eDiary';

        VTD1_Survey__c survey = new VTD1_Survey__c(
                Name = surveyName,
                VTD1_CSM__c = ptCase.Id,
                RecordTypeId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('ePRO').getRecordTypeId(),
                VTR4_Percentage_Completed__c = 100,
                VTD1_Status__c = VT_R4_ConstantsHelper_Statuses.SURVEY_REVIEW_REQUIRED
        );
        insert survey;

        VTR5_DiaryListView__c dlv = new VTR5_DiaryListView__c(
                VTR5_Filter__c = JSON.serialize(new Map<String, String> {
                        'name' => surveyName,
                        'studyId' => String.valueOf(ptCase.VTD1_Study__c),
                        'status' => VT_R4_ConstantsHelper_Statuses.SURVEY_REVIEW_REQUIRED
                })
        );
        insert dlv;

        Map<String, Object> tableParams = new Map<String, Object>{
                'limit' => (Object) '1000',
                'filterMap' => (Object) new Map<String, Object> {
                        'markReviewedFilter' => (Object) 'false',
                        'statusFilters' => null,
                        'eDiaryNameFilters' => (Object) new List<Object>()
                }
        };

        Test.startTest();
        VT_R5_EDiariesTableController result = VT_R5_EDiariesTableController.getEdiaryRecords(dlv.Id, JSON.serialize(tableParams), null, true);
        Test.stopTest();

        System.assertEquals(1, result.items.size());
        System.assertEquals(survey.Id, result.items[0].eDiary.Id);

    }

    /*******************************************************************************************************
    * @author                   Vijendra Hire (SH-20622)
    * @description              Method covers the code coverage of totalScoreValue value filter.
    */
    
    public static void getEDiariesTotalScoreDataTest(){
        
        Case ptCase = [SELECT Id, VTD1_Study__c FROM Case LIMIT 1];
        Study_Team_Member__c stmSCR = [SELECT Id, User__c 
                                       FROM Study_Team_Member__c
                                       WHERE Study__c =:  ptCase.VTD1_Study__c LIMIT 1];
        
        VTR5_DiaryListView__c dlv = new VTR5_DiaryListView__c();
        insert dlv;       
        VT_R5_EDiariesTableController eData2 = new VT_R5_EDiariesTableController();
        VT_R5_EDiariesTableController eData3 = new VT_R5_EDiariesTableController();
        Map<String, Object> params = new Map<String, Object>{'limit' => (Object) '1000'};
        VT_R5_EDiariesTableController eData = VT_R5_EDiariesTableController.getEdiaryRecords(null, JSON.serialize(params), null, true);
       List<VT_R5_EDiariesTableController.EDiaryWrapper> eDiaries = eData.items;
        Test.startTest();
        System.runAs(new User(Id = stmSCR.User__c)){
            Map<String, Object> filters = new Map<String, Object>{'markReviewedFilter' => (Object) 'false', 'totalScoreValue' => (Object) '0',
                                                                  'statusFilters' => (Object) new List<String>{eDiaries.get(0).eDiary.VTD1_Status__c}};
            Map<String, Object> order = new Map<String, Object>{'totalScore' => 'eDiaryName', 'sorting' => 'asc'};  
            params.put('filterMap', filters);
			params.put('order', order);
            order = new Map<String, Object>{'columnName' => 'totalScore', 'sorting' => 'asc'};
            eData2 = VT_R5_EDiariesTableController.getEdiaryRecords(dlv.Id, JSON.serialize(params), 'Not', true);
        	        	
            order = new Map<String, Object>{'columnName' => 'totalScore', 'sorting' => 'asc'};
            params.put('order', order);
            eData3 = VT_R5_EDiariesTableController.getEdiaryRecords(dlv.Id, JSON.serialize(params), '0', true);
        }
        Test.stopTest();
        system.assert(eData.items.size() > 0);
        system.assert(eData2.items.size() == 0);
        system.assertNotEquals(1, eData3.items.size());
           
    }

/*******************************************************************************************************
    * @author                   Conquerors: Priyanka Ambre (SH-21178)
    * @description              Tests getEdiaryRecords method with Ediary Filter and respective Answer record 
                                exists.
    */
    public static void ediaryFilterForGetRecordsTest() {
        Case ptCase = [SELECT Id, VTD1_Study__c FROM Case LIMIT 1];
                                                       
        VTR5_eDiary_Filter__c objeDiaryFilter = (VTR5_eDiary_Filter__c) new DomainObjects.VTR5_eDiary_Filter_t()
            .setStudyId(ptCase.VTD1_Study__c)
            .setVTR5_External_Widget_Key('Unwell?')
            .setVTR5_Answer_Content('YES')

            .setVTR5_eDiary_Name('Test eDiary 2')

            .setVTR5_Label_Name('Test1')
            .persist();

        
        System.assertNotEquals(null, objeDiaryFilter.Id);


        List<VTD1_Survey_Answer__c> lstSurveyAnswers = [SELECT    Id,
                                                                  VTD1_Survey__c,
                                                                  VTD1_Answer__c
                                                        FROM VTD1_Survey_Answer__c
                                                        WHERE VT_R5_External_Widget_Data_Key__c  =: objeDiaryFilter.VTR5_External_Widget_Key__c

                                                        AND VTD1_Survey__r.Name = 'Test eDiary 2' LIMIT 1];


         Map<String, Object> tableParams = new Map<String, Object>{
                'limit' => (Object) '1000',
                'filterMap' => (Object) new Map<String, Object> {
                        'markReviewedFilter' => (Object) 'false',
                        'statusFilters' => null,
                        'eDiaryNameFilters' => (Object) new List<Object>(),
                        'studyId' => ptCase.VTD1_Study__c
                },
                'selectedResponse' => (Object) new Map<String, Object> {

                        'VTR5_eDiary_Name__c' => 'Test eDiary 2',

                        'VTR5_Label_Name__c' => '',
                        'VTR5_Study__c' => ptCase.VTD1_Study__c,
                        'VTR5_Answer_Content__c' => 'YES',
                        'VTR5_External_Widget_Key__c' => 'Unwell?'
                }
        };


        Test.startTest();
        VT_R5_EDiariesTableController result = VT_R5_EDiariesTableController.getEdiaryRecords(null, JSON.serialize(tableParams), null, true);
        System.assertEquals(lstSurveyAnswers[0].VTD1_Survey__c, result.items[0].eDiary.Id);
        Test.stopTest();
    }


    /*******************************************************************************************************
    * @author                   Conquerors: Priyanka Ambre (SH-21178)
    * @description              Negative Tests: getEdiaryRecords method with Ediary Filter if No Answer record 
                                exists
	*/
    
   
    public static void ediaryFilterForNoAnswersTest() {
        Case ptCase = [SELECT Id, VTD1_Study__c FROM Case LIMIT 1];

         VTR5_eDiary_Filter__c objeDiaryFilter = (VTR5_eDiary_Filter__c) new DomainObjects.VTR5_eDiary_Filter_t()
            .setStudyId(ptCase.VTD1_Study__c)
            .setVTR5_External_Widget_Key('Well?')
            .setVTR5_Answer_Content('YES')

            .setVTR5_eDiary_Name('Test eDiary 2')

            .setVTR5_Label_Name('Test1')
            .persist();
        System.assertNotEquals(null, objeDiaryFilter.Id);


        List<VTD1_Survey_Answer__c> lstSurveyAnswers = [SELECT    Id,
                                                                  VTD1_Survey__c,
                                                                  VTD1_Answer__c
                                                        FROM VTD1_Survey_Answer__c
                                                        WHERE VT_R5_External_Widget_Data_Key__c  =: objeDiaryFilter.VTR5_External_Widget_Key__c
                                                        AND VTD1_Survey__r.Name =: objeDiaryFilter.VTR5_eDiary_Name__c LIMIT 1];

         Map<String, Object> tableParams = new Map<String, Object>{
                'limit' => (Object) '1000',
                'filterMap' => (Object) new Map<String, Object> {
                        'markReviewedFilter' => (Object) 'false',
                        'statusFilters' => null,
                        'eDiaryNameFilters' => (Object) new List<Object>(),
                        'studyId' => ptCase.VTD1_Study__c
                },
                'selectedResponse' => (Object) new Map<String, Object> {

                        'VTR5_eDiary_Name__c' => 'Test eDiary 2',

                        'VTR5_Label_Name__c' => '',
                        'VTR5_Study__c' => ptCase.VTD1_Study__c,
                        'VTR5_Answer_Content__c' => 'YES',
                        'VTR5_External_Widget_Key__c' => 'Well?'
                }
        };

        Test.startTest();
            VT_R5_EDiariesTableController result = VT_R5_EDiariesTableController.getEdiaryRecords(null, JSON.serialize(tableParams), null, true);
            System.assertEquals(0, result.items.size());
        Test.stopTest();
    }

    /*******************************************************************************************************
    * @author                   Conquerors: Priyanka Ambre (SH-21178)
    * @description              Negative Tests: getEdiaryRecords method if ediary Filter is blank.
	*/
    
    
    public static void ediaryFilterForBlankEdiaryTest() {
        Case ptCase = [SELECT Id, VTD1_Study__c FROM Case LIMIT 1];

        VTR5_eDiary_Filter__c objeDiaryFilter = (VTR5_eDiary_Filter__c) new DomainObjects.VTR5_eDiary_Filter_t()
        .setStudyId(ptCase.VTD1_Study__c)
        .setVTR5_External_Widget_Key('')
        .setVTR5_Answer_Content('')
        .setVTR5_eDiary_Name('')
        .setVTR5_Label_Name('')
        .persist();
        System.assertNotEquals(null, objeDiaryFilter.Id);


        List<VTD1_Survey_Answer__c> lstSurveyAnswers = [SELECT    Id,
                                                                  VTD1_Survey__c,
                                                                  VTD1_Answer__c
                                                        FROM VTD1_Survey_Answer__c
                                                        WHERE VT_R5_External_Widget_Data_Key__c  =: objeDiaryFilter.VTR5_External_Widget_Key__c
                                                        AND VTD1_Survey__r.Name =: objeDiaryFilter.VTR5_eDiary_Name__c LIMIT 1];

         Map<String, Object> tableParams = new Map<String, Object>{
                'limit' => (Object) '1000',
                'filterMap' => (Object) new Map<String, Object> {
                        'markReviewedFilter' => (Object) 'false',
                        'statusFilters' => null,
                        'eDiaryNameFilters' => (Object) new List<Object>(),
                        'studyId' => ptCase.VTD1_Study__c
                },
                'selectedResponse' => (Object) new Map<String, Object> {
                        'VTR5_eDiary_Name__c' => '',
                        'VTR5_Label_Name__c' => '',
                        'VTR5_Study__c' => ptCase.VTD1_Study__c,
                        'VTR5_Answer_Content__c' => '',
                        'VTR5_External_Widget_Key__c' => ''
                }
        };

        Test.startTest();
            VT_R5_EDiariesTableController result = VT_R5_EDiariesTableController.getEdiaryRecords(null, JSON.serialize(tableParams), null, true);
            System.assertEquals(0, result.items.size());
        Test.stopTest();
    }

    /*******************************************************************************************************
    * @author                   Conquerors: Priyanka Ambre (SH-21178)
    * @description              Tests getEdiaryFiltters method.
    */
   
    public static void getEdiaryFiltersTest() {
        Case ptCase = [SELECT Id, VTD1_Study__c FROM Case LIMIT 1];
        update new HealthCloudGA__CarePlanTemplate__c(Id = ptCase.VTD1_Study__c, VTR5_eDiaryTool__c = 'eCOA');

                                                       
        VTR5_eDiary_Filter__c objeDiaryFilter = (VTR5_eDiary_Filter__c) new DomainObjects.VTR5_eDiary_Filter_t()
        .setStudyId(ptCase.VTD1_Study__c)
        .setVTR5_External_Widget_Key('Unwell?')
        .setVTR5_Answer_Content('YES')

        .setVTR5_eDiary_Name('Test eDiary 2')

        .setVTR5_Label_Name('Test1')
        .persist();

        Test.startTest();
            List<VTR5_eDiary_Filter__c> lstEdiaryFilters = VT_R5_EDiariesTableController.getEdiaryFilters(ptCase.VTD1_Study__c);
            System.assertEquals(1, lstEdiaryFilters.size());
            System.assertEquals(objeDiaryFilter.VTR5_Study__c, lstEdiaryFilters[0].VTR5_Study__c);
        Test.stopTest();
    }

    /*******************************************************************************************************
    * @author                   Conquerors: Priyanka Ambre (SH-21178)
    * @description              Negative Tests: For getEdiaryFiltters method studyId is null.
    */
  
    public static void getEdiaryFiltersForNullTest() {   
        Test.startTest();
        List<VTR5_eDiary_Filter__c> lstEdiaryFilters = VT_R5_EDiariesTableController.getEdiaryFilters(null);
        System.assertEquals(null, lstEdiaryFilters);
        Test.stopTest();
    }

}