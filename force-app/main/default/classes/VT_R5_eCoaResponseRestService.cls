/**
* @author: Carl Judge
* @date: 15-Sep-20
**/
@RestResource(UrlMapping='/eCoaResponseService')
global without sharing class VT_R5_eCoaResponseRestService {
    @HttpPost
    global static void processResponse() {
        RestResponse response = RestContext.response;
        response.statusCode = 200;
        response.responseBody = Blob.valueOf('OK');

        RestRequest req = RestContext.request;
        String body = req.requestBody.toString();

        System.debug(body);

        VT_R5_eCoaResponseExport.Response resp = (VT_R5_eCoaResponseExport.Response)JSON.deserialize(body, VT_R5_eCoaResponseExport.Response.class);
        new VT_R5_eCOAeDiaryCreator(resp).doCreate();
    }
}