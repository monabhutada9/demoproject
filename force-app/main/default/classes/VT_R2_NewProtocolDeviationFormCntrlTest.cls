/**
 * Created by Riabchenko Alona on 19/04/07.
 */
@isTest
public without sharing class VT_R2_NewProtocolDeviationFormCntrlTest {
    public static void getPDDataTest() {
        User userSCR = [SELECT Id FROM User WHERE VTD1_Profile_Name__c = 'Site Coordinator' ORDER BY CreatedDate DESC LIMIT 1];
        Test.startTest();
        System.runAs(userSCR) {
            String res = VTR2_SCRNewProtocolDeviationFormCntrl.getPDData();
            System.assertNotEquals(null,res);
        }
        Test.stopTest();
    }

    public static void createNewPDDraftRecordTest() {
        User userSCR = [SELECT Id FROM User WHERE VTD1_Profile_Name__c = 'Site Coordinator' ORDER BY CreatedDate DESC LIMIT 1];
        DomainObjects.VTD1_Protocol_Deviation_t protocolDeviationT= new DomainObjects.VTD1_Protocol_Deviation_t();

        VTD1_Protocol_Deviation__c pd = (VTD1_Protocol_Deviation__c) protocolDeviationT.persist();
        pd.Id = null;
        Test.startTest();
        System.runAs(userSCR) {
            VTR2_SCRNewProtocolDeviationFormCntrl.createNewPDDraftRecord(JSON.serialize(new List<VTD1_Protocol_Deviation__c>{pd}));
        }
        Test.stopTest();
    }
}