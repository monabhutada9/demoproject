/************************************************************************
 * Name	 : VTR5_SkillAndChatButtonController
 * Author : Mahesh Shimpi
 * Desc	 : Controller for creating Study Skill, LiveChatButtons & LiveChatButtonSkills.
 * 	        
 *
 * Modification Log:
 * ----------------------------------------------------------------------
 * Developer		        Date		 Story         	    Description
 * ----------------------------------------------------------------------
 * Mahesh Shimpi 		   05/04/2020    SH-8125           New Labels for AutoGreeting 
 * message are added to show new Auto greeting.
 * 
 *************************************************************************/
public with sharing class VTR5_SkillAndChatButtonController {



    public static final String COMPOSITE_API_URL = '/services/data/v47.0/composite';
    public static final String SKILL_API_URL = '/services/data/v47.0/sobjects/Skill';
    public static final String CHATBUTTON_AND_CHATBUTTONSKILL_API_URL = '/services/data/v47.0/composite/sobjects';
    public static Boolean isRunningFromBatch = false;



    public class ResponseJSON {
        public Boolean skillAndChatButtonStatus = false;
        public Boolean chtButtonSkillStatus = false;
    }
    /* 
     * Method Name : createStudySkillAndChatButtons(String studyId)
     * Description : This method is used to create the Skill, LiveChatButtons, & LiveChatButtonSkills +for the respective Study.
     * param : String studyId - Specifies Id of the study     
     * return : ResponseJSON =>  on successfull Skill creation of Skill, LiveChatButtons, & LiveChatButtonSkills +for the respective Study.
     *    {
     *        skillStatus =>  whether study skill & ChatButtons created or not. (true, false)
     *        buttonStatus => whether LiveChatButtonSkill for study created or not (true, false)
     *    }
     */
    public static ResponseJSON createStudySkillAndChatButtons(String studyId){
        HealthCloudGA__CarePlanTemplate__c study = [
                    SELECT Id, Name, VT_R5_Is_study_skill_created__c, VT_R5_Are_chatbuttons_created__c
                    FROM HealthCloudGA__CarePlanTemplate__c 
                    WHERE Id =: studyId];
        ResponseJSON result = new ResponseJSON();
        System.debug('######## Check point 1');
        try {
            if (isRunningFromBatch) {   
                System.debug('######## Check point 3');
                result.skillAndChatButtonStatus = createStudySkillAndChatButtonsForStudy(study.Id,study.Name);
                result.chtButtonSkillStatus = createChatButtonSkills(studyId);  
            }else{
            if (!study.VT_R5_Is_study_skill_created__c) {
                result.skillAndChatButtonStatus = createStudySkillAndChatButtonsForStudy(study.Id,study.Name);
            }
            if (!study.VT_R5_Are_chatbuttons_created__c){ 
                System.debug('######## Check point 2');
                result.chtButtonSkillStatus = createChatButtonSkills(studyId);
            }
            }
        } catch (Exception ex) {
                System.debug('VTR5_SkillAndChatButtonController : Exception occured :'+ex.getMessage());

            ErrorLogUtility.logException(ex, ErrorLogUtility.ApplicationArea.APP_AREA_CHAT_AUTOMATION, 
                VTR5_SkillAndChatButtonController.class.getName());

        }
        return result;
    }
    //----------------Composite Api test code --------------------
     /* 
     * Method Name : createChatButtonsForStudy2(String studyId, String studyName)
     * param : String studyId - Specifies Id of the study 
     * param : String studyName - Specifies Id of the protocal number (Name) of the study
     * Description : This method is used to create the LiveChatButtons for the respective Study.
     * return : String => 'true' - on successfull callout & response status code 200 or 201.
     *          String => 'false' - if callout returns null value or callout exception is occured.
     */
    public static Boolean createStudySkillAndChatButtonsForStudy(String studyId, String studyName){
        Id routingConfigurationId = [
                SELECT Id, DeveloperName 
                FROM QueueRoutingConfig 
                WHERE DeveloperName =: VT_D1_ConstantsHelper.ROUTING_CONFIGURATION].Id;
        List<Skill> lstSkill = [SELECT Id, DeveloperName, MasterLabel 
                        FROM Skill 
                        WHERE DeveloperName =: studyId];
        CompositeRequest composite = new CompositeRequest();        
        if (lstSkill.size() == 0) {            
            SkillSubRequest skillRequest = new SkillSubRequest();
            skillRequest.method = 'POST';
            skillRequest.referenceId = 'SkillRecord';
            skillRequest.url = SKILL_API_URL;
            skillRequest.body = createStudySkillInstance(studyId, studyName);
            ChatButtonSubRequest chatButtonRequest = new ChatButtonSubRequest();
            chatButtonRequest.method = 'POST';
            chatButtonRequest.referenceId = 'ChatButtonRecord';
            chatButtonRequest.url = CHATBUTTON_AND_CHATBUTTONSKILL_API_URL;
            chatButtonRequest.body =  createChatButtonInstance(studyId, studyName, null, routingConfigurationId);
            composite.compositeRequest.add(skillRequest);
            composite.compositeRequest.add(chatButtonRequest);
        }else {
            ChatButtonSubRequest chatButtonRequest = new ChatButtonSubRequest();
            chatButtonRequest.method = 'POST';
            chatButtonRequest.referenceId = 'ChatButtonRecord';
            chatButtonRequest.url = CHATBUTTON_AND_CHATBUTTONSKILL_API_URL;
            chatButtonRequest.body =  createChatButtonInstance(studyId, studyName, lstSkill[0].Id, routingConfigurationId);
            composite.compositeRequest.add(chatButtonRequest);
        }
        String skillAndButtonInfo = JSON.serializePretty(composite);
        // Get & prepare endPoint for the callout      
        HttpResponse response = VT_R5_SkillAndChatButtonUtils.restApiCallout(COMPOSITE_API_URL, skillAndButtonInfo,'POST');
        if (response == null) {
            return false;
        }else if (response.getStatusCode() == 201 || response.getStatusCode() == 200) {
            System.debug('VTR5_SkillAndChatButtonController : Skill + ChatButton Callout Response');  
            System.debug(response.getBody());
            List<Skill> lstLangSkills = getLangsSkills();
            List<String> lstButtonIdsToBeFetched = getListOfButtonIdsToBeFetched(studyId);
            List<LiveChatButton> lstChatbtns = [SELECT Id, DeveloperName, MasterLabel, WindowLanguage 
                                        FROM LiveChatButton 
                                        WHERE DeveloperName IN : lstButtonIdsToBeFetched];
            System.debug('Lang Skill size :' + lstLangSkills.size());
            System.debug('Buttons size :' + lstButtonIdsToBeFetched.size());
            return (lstButtonIdsToBeFetched.size() == lstLangSkills.size()) ? true : false;
        } else {
            System.debug('VTR5_SkillAndChatButtonController : Skill + ChatButton Callout Response');  
            System.debug(response.getBody());
            return false;
        }   
    }
    /* 
     * Method Name : createChatButtonSkills(String studyId)
     * param : String studyId - Specifies Id of the study 
     * Description : This method is used to associate the LiveChatButtonSkills to the respective LiveChatButtons
     * return : String => 'true' - on successfull callout & response status code 200 or 201.
     *          String => 'false' - if callout returns null value or callout exception is occured or
     *                            - if callout returns other than {201 or 200} stutus code.
     */
    public static Boolean createChatButtonSkills(String studyId){
        List<Skill> lstLangSkills = getLangsSkills();
        // associate/add language skills to chat buttons
        List<String> lstButtonIdsToBeFetched = getListOfButtonIdsToBeFetched(studyId);

        

        List<LiveChatButton> lstChatbtns = [
                SELECT Id, DeveloperName, MasterLabel, WindowLanguage 
                FROM LiveChatButton 
                WHERE DeveloperName IN : lstButtonIdsToBeFetched];
        Integer i = 0;
        ChatButtonSkillRecords chatBtnSkillRecords = new ChatButtonSkillRecords();
        for(Skill sk : lstLangSkills){
            ChatButtonSkill chtButtonSkill = new ChatButtonSkill();
            for(LiveChatButton button: lstChatbtns){
                if(button.DeveloperName.contains(sk.DeveloperName)){
                    chtButtonSkill.attributes.type = 'LiveChatButtonSkill';
                    chtButtonSkill.attributes.referenceId = 'ref' + i++;                    
                    chtButtonSkill.ButtonId = button.Id;
                    chtButtonSkill.SkillId = sk.Id;
                    chatBtnSkillRecords.records.add(chtButtonSkill);
                    break;
                }
            }   
        }
        String liveChatButtonSkillInfo = JSON.serialize(chatBtnSkillRecords);
        // System.debug('2 callout body :' + liveChatButtonSkillInfo);
        // Get & prepare endPoint for the callout
        // String endPoint = '/services/data/v47.0/composite/sobjects';                    
        //Send the request (callout) & get the response.
        HttpResponse response = VT_R5_SkillAndChatButtonUtils.restApiCallout(CHATBUTTON_AND_CHATBUTTONSKILL_API_URL, 
                                liveChatButtonSkillInfo,'POST');
        // Parse the JSON response
        if (response == null) {
            return false;
        }else if (response.getStatusCode() == 201 || response.getStatusCode() == 200) {
            System.debug('VTR5_SkillAndChatButtonController : LiveChatButtonSkill Callout Response');  
            System.debug(response.getBody());
            return true;
        } else {
            System.debug('VTR5_SkillAndChatButtonController : LiveChatButtonSkill Callout Response');  
            System.debug(response.getBody());
            return false;
        }
    }
    /* 
     * Method Name : getLangsSkills()
     * Description : This method is used to get list of al language skills in the stystem.
     * return : List<Skill> - list of all language skills
     */
    private static List<Skill> getLangsSkills(){
        Set<String> lstLangs = new Set<String>(); // Contact - CpntactPrefeed language, secondary prefredlangage
        List<Schema.PicklistEntry> lstPickListEntries = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap()
                        .get('VT_R5_Secondary_Preferred_Language__c').getDescribe().getPicklistValues(); // 
        for(Schema.PicklistEntry pickListVal : lstPickListEntries){
            if (pickListVal.isActive()) {
                lstLangs.add(pickListVal.getValue());
            }
        }  
        lstPickListEntries.clear();
        lstPickListEntries = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap()
                        .get('VTR2_Primary_Language__c').getDescribe().getPicklistValues();
        for(Schema.PicklistEntry pickListVal : lstPickListEntries){
            if (pickListVal.isActive()) {
                lstLangs.add(pickListVal.getValue());
            }
        }  
        List<Skill> lstLangSkills = [SELECT Id, DeveloperName, MasterLabel 
                                    FROM Skill 
                                    WHERE DeveloperName IN : lstLangs];
        return lstLangSkills;
    }
    /* 
     * Method Name : getListOfButtonIdsToBeFetched(String studyId)
     * Description : This method is used to get list of al all buttons to be fetched.
     * return : List<String> - list of DeveloperName of chatbuttons.
     */
    private static List<String> getListOfButtonIdsToBeFetched(String studyId){
        List<String> lstButtonIdsToBeFetched = new List<String>();
        List<Skill> lstLangSkills = getLangsSkills();
        for (Skill sk : lstLangSkills) {              
            String button = studyId + '_' + sk.DeveloperName;
            lstButtonIdsToBeFetched.add(button);
        }
        return lstButtonIdsToBeFetched;
    }
    /* 
     * Method Name : createChatButtonInstance(List<Skill> lstLangSkills, String studyId, 
     *                  String studyName, Id routingConfigId, Skill studySkill)
     * Description : This method is used to get list of al language skills in the stystem.
     * return : List<Skill> - list of all language skills
     */
    private static SkillBody createStudySkillInstance(String developerName, String masterLabel){
        SkillBody studySkl = new SkillBody();
        studySkl.DeveloperName = developerName;
        studySkl.MasterLabel = masterLabel;
        studySkl.Description = 'Skill for study ' + masterLabel;
        return studySkl;
    }
    /* 
     * method : createChatButtonInstance(List<Skill> lstLangSkills, String studyId, 
     *                  String studyName, Id routingConfigId, Skill studySkill)
     * Description : This Method Name :is used to get list of al language skills in the stystem.
     * return : List<Skill> - list of all language skills
     */
    private static ChatButtonBody createChatButtonInstance(String studyId, String studyName, Id skillId, Id routingConfigId){
        List<Skill> lstLangSkills = getLangsSkills();        
        ChatButtonBody body = new ChatButtonBody();
        List<ChatButton> lstChatButtons = new List<ChatButton>();
        Integer i = 0;
        for(Skill sk : lstLangSkills){
            ChatButton chtButton = new ChatButton();
            chtButton.attributes.type = 'LiveChatButton';
            chtButton.attributes.referenceId = 'ChatButton' + i++;
            chtButton.DeveloperName = studyId + '_' + sk.DeveloperName;
            chtButton.MasterLabel = studyName + '-' + sk.MasterLabel;
            chtButton.SkillId = (skillId == null) ? '@{SkillRecord.id}' : skillId;
            chtButton.RoutingType = 'OmniSkills';
            chtButton.WindowLanguage = 'en_US';
            chtButton.Type = 'Standard';
            chtButton.OptionsIsInviteAutoRemove = false;
            chtButton.OptionsHasInviteAfterAccept = false;
            chtButton.OptionsHasInviteAfterReject = false;
            chtButton.OptionsHasRerouteDeclinedRequest = false;
            chtButton.OptionsIsAutoAccept = false;
            chtButton.OptionsHasChasitorIdleTimeout = false;
            chtButton.RoutingConfigurationId = routingConfigId;
            // added new auto greeting message along with prefered match language.
            //custom label for languages have been created in format VTR5_language-code.
            // e.g. English = VTR5_en_US
            chtButton.AutoGreeting = '{!$Label.VTD2_Hello} {!LiveAgent_VisitorName}, {!$Label.VTD2_MyNameIs} '+
                                    '{!User_FirstName} {!User_LastName} {!$Label.VT_R5_AndISpeak} '+
                                    '{!$Label.VTR5_'+ sk.DeveloperName +'}. {!$Label.VTD2_HowCanIHelpYou}';              
            lstChatButtons.add(chtButton);
        }
        body.records = lstChatButtons;
        return body;
    }



    /* 
     * Method Name : updateStudies(HealthCloudGA__CarePlanTemplate__c study, ResponseJSON result) 
     * Description : THis method is used to updated the Skill & chatButtong flags on the study.
     * return : none
     */
    public static Boolean updateStudies(HealthCloudGA__CarePlanTemplate__c study, ResponseJSON result){
        Boolean isSkillFlagSet = false;
        Boolean isButtonFlagSet = false;
        try {
        if(result.skillAndChatButtonStatus){ 
            study.VT_R5_Is_study_skill_created__c = true;
            isSkillFlagSet = true;
        }
        if(result.chtButtonSkillStatus){
            study.VT_R5_Are_chatbuttons_created__c = true;
            isButtonFlagSet = true; 
        }
            if (isSkillFlagSet || isButtonFlagSet){ 
                update study; 
                System.debug('Stuyd after update '+ study);               
                return true;
            }
        } catch (Exception ex) {
            System.debug('Exception occured : '+ ex.getMessage());

            ErrorLogUtility.logException(ex, ErrorLogUtility.ApplicationArea.APP_AREA_CHAT_AUTOMATION, 
                VTR5_SkillAndChatButtonController.class.getName());        
        }
        return false;
    }
 

    // Wrapper classes for creating LiveChatButtonSkill
    public class Attribute {
        public String type {get;set;}
        public String referenceId {get;set;}
    }     
    public class ChatButtonSkillRecords {
        public List<ChatButtonSkill> records {get;set;}
        public ChatButtonSkillRecords(){
            this.records = new List<ChatButtonSkill>();
        }
    }
    public class ChatButtonSkill {
        public Attribute attributes {get;set;}
        public String ButtonId {get;set;}
        public String SkillId {get;set;}
        public ChatButtonSkill(){
            this.attributes = new Attribute();
        }
    }
    // Wrapper classes for creating LiveChatButton
    public class ChatButton {
        public Attribute attributes {get;set;}
        public String DeveloperName {get;set;}
        public String MasterLabel {get;set;}
        public String SkillId {get;set;}
        public String RoutingType {get;set;}
        public String WindowLanguage {get;set;}
        public String Type {get;set;}
        public Boolean OptionsIsInviteAutoRemove {get;set;}
        public Boolean OptionsHasInviteAfterAccept {get;set;}
        public Boolean OptionsHasInviteAfterReject {get;set;}
        public Boolean OptionsHasRerouteDeclinedRequest {get;set;}
        public Boolean OptionsIsAutoAccept {get;set;}
        public Boolean OptionsHasChasitorIdleTimeout {get;set;}
        public String RoutingConfigurationId {get;set;}
        public String AutoGreeting {get;set;}
        public ChatButton(){
            this.attributes = new Attribute();
        }
    }
    public class ChatButtonBody {
        public List<ChatButton> records {get;set;}
        public ChatButtonBody(){
            this.records = new List<ChatButton>();
        }
    }
    // Wrapper classes for Creating Study Skill
    public class SkillBody {
        public String DeveloperName {get;set;}
        public String MasterLabel {get;set;}
        public String Description {get;set;}
    }
    public class CompositeRequest {
        public Boolean allOrNone {get;set;}
        public List<SubRequest> compositeRequest {get;set;}
        public CompositeRequest(){
            this.allOrNone = false;
            this.compositeRequest = new List<SubRequest>();
        }
    }
    public virtual class SubRequest {
        public String method {get;set;}
        public String url {get;set;}
        public String referenceId {get;set;}
    }
    public class SkillSubRequest extends SubRequest {
        public SkillBody body {get;set;}
        public SkillSubRequest(){
            this.body = new SkillBody();
        }
    }
    public class ChatButtonSubRequest extends SubRequest {
        public ChatButtonBody body {get;set;}
        public ChatButtonSubRequest(){
            this.body = new ChatButtonBody();
        }
    }
}