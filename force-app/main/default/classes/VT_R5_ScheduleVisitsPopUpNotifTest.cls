/**

 * @author Andrey Pivovarov
 * @date 10/16/2020
 * @description Test class for rewrited process builder 'Pop Up Notifications for scheduled visits'

 */
@IsTest
public with sharing class VT_R5_ScheduleVisitsPopUpNotifTest {
    @TestSetup
    static void setup() {
        Test.startTest();


        DomainObjects.VTD2_TN_Catalog_Code_t tnCatalogCode_t = new DomainObjects.VTD2_TN_Catalog_Code_t()
                .setVTD2_T_Task_Unique_Code('N008')
                .setRecordTypeId(Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Notification').getRecordTypeId())
                .setVTD2_T_SObject_Name('VTD1_Actual_Visit__c')
                .setVTD2_T_Receiver('VTD1_Actual_Visit__c.VTD1_Case__r.VTD1_Patient_User__c')
                .setVTD2_T_Care_Plan_Template('VTD1_Actual_Visit__c.VTD1_Case__r.VTD1_Study__c')
                .setVTD2_T_Subject('test');
        tnCatalogCode_t.persist();


        DomainObjects.Study_t sdy = new DomainObjects.Study_t().addAccount(new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor'));
        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor');
        Account account = (Account) patientAccount.persist();
        Case cs = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .addStudy(sdy)
                .addVTD1_Patient_User(new DomainObjects.User_t()
                        .addContact(new DomainObjects.Contact_t())
                        .setProfile('Patient'))
                .addVTD1_Patient(new DomainObjects.VTD1_Patient_t())
                .setAccountId(account.Id)
                .persist();

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t()
                .setFirstName('Patient')
                .setLastName('Patient')
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));

        DomainObjects.Contact_t caregiverContact = new DomainObjects.Contact_t()
                .setFirstName('Caregiver')
                .setLastName('Caregiver')
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));

        DomainObjects.User_t patientUser = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        patientUser.persist();

        DomainObjects.User_t caregiverUser = new DomainObjects.User_t()
                .addContact(caregiverContact)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME);
        patientUser.persist();

        DomainObjects.VTD1_Actual_Visit_t visit = new DomainObjects.VTD1_Actual_Visit_t()
                .setVTD1_Case(cs.Id)


                .setVTD1_Status('Future Visit')
                .setVTD1_Name('ToBeScheduled')
                .setScheduledDateTime(System.now().addMinutes(5))
                .setRecordTypeByName('VTD1_Unscheduled')
                .setVTD1_UnscheduledVisitType('Study Team');



        new DomainObjects.Visit_Member_t()
                .setVTD1_Participant_UserId(patientUser.Id)
                .setVTD1_External_Participant_Type('Patient')
                .addVTD1_Actual_Visit(visit)
                .persist();
        new DomainObjects.Visit_Member_t()
                .setVTD1_External_Participant_Type('HHN')
                .addVTD1_Actual_Visit(visit)
                .persist();


        new DomainObjects.Visit_Member_t()
                .setVTD1_Participant_UserId(caregiverUser.Id)
                .setVTD1_External_Participant_Type('Caregiver')
                .addVTD1_Actual_Visit(visit)
                .persist();


        Test.stopTest();
    }

    @IsTest
    private static void popUpTest1() {
        Case currentCase = [SELECT Id, VTD1_Study__c FROM Case LIMIT 1];
        VTD1_ProtocolVisit__c pVisit = new VTD1_ProtocolVisit__c(
                VTD1_Study__c = currentCase.VTD1_Study__c,

                VTR2_Visit_Participants__c = 'Patient; PI',
                VTD1_Onboarding_Type__c = 'N/A'

        );
        insert pVisit;
        List<VTD1_Actual_Visit__c> actualVisits = [
                SELECT VTD1_Status__c, Unscheduled_Visits__c, VTD1_Unscheduled_Visit_Duration__c,
                        VTD1_Unscheduled_Visit_Type__c, VTD1_Scheduled_Date_Time__c, VTR3_Previous_Schedule_Date__c,


                        VTR3_is_Rescheduling__c, VTD1_Protocol_Visit__c, VTR2_ScheduleVisitTaskPatient__c
                FROM VTD1_Actual_Visit__c
        ];
        actualVisits[0].Unscheduled_Visits__c = currentCase.Id;
        actualVisits[0].VTD1_Status__c = 'To Be Scheduled';
        actualVisits[0].VTD1_Protocol_Visit__c = pVisit.Id;
        actualVisits[0].VTR2_ScheduleVisitTaskPatient__c = Datetime.now().date();
        update actualVisits;
        actualVisits[0].VTD1_Status__c = 'Scheduled';


        actualVisits[0].VTD1_Unscheduled_Visit_Duration__c = 60;
        actualVisits[0].VTD1_Unscheduled_Visit_Type__c = 'Consent';
        actualVisits[0].VTD1_Scheduled_Date_Time__c = Datetime.now().addMinutes(-5);
        Test.startTest();
        update actualVisits; //2,3 diamond


        actualVisits[0].VTD1_Scheduled_Date_Time__c = Datetime.now().addMinutes(30);
        update actualVisits; //4,6,7,8 diamond
        actualVisits = [
                SELECT VTD1_Status__c, Unscheduled_Visits__c, VTD1_Unscheduled_Visit_Duration__c,
                        VTD1_Unscheduled_Visit_Type__c, VTD1_Scheduled_Date_Time__c, VTR3_Previous_Schedule_Date__c,
                        VTR3_is_Rescheduling__c, VTR2_ScheduleVisitTaskPatient__c, VTD1_Onboarding_Type__c,
                        VTD2_Common_Visit_Type__c, VTD1_Visit_Name__c
                FROM VTD1_Actual_Visit__c
        ];
        for (VTD1_Actual_Visit__c av : actualVisits) {


            if (av.VTD1_Visit_Name__c == 'ToBeScheduled') {
                System.assertEquals(av.VTR3_Previous_Schedule_Date__c, av.VTD1_Scheduled_Date_Time__c);
                System.assertEquals(true, av.VTR3_is_Rescheduling__c);
            }
        }
        actualVisits[0].VTD1_Status__c = 'Completed';
        update actualVisits;

        List<VTD1_NotificationC__c> notifications = [
                SELECT Id, VDT2_Unique_Code__c FROM VTD1_NotificationC__c
                WHERE VDT2_Unique_Code__c = 'N008'];
        System.assertEquals(1, notifications.size());

        Test.stopTest();
    }
}