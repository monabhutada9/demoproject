/**
 * Created by shejn on 15.01.2019.
 */

public with sharing class VT_D2_GetPIAsPatientTestController {
    @AuraEnabled
    public static String getPIUser(){
        List<User> PIUserList = [select Id, TimeZoneSidKey from User where Id='0053D000000fKei'];
        if(PIUserList.isEmpty()) {
            system.debug('Is Empty');
            return null;
        }
        else{
            system.debug('PI User = '+ PIUserList[0]);
            return PIUserList[0].TimeZoneSidKey;
        }
    }
}