/**
 * Created by Alexander Komarov on 15.04.2019.
 */
@RestResource(UrlMapping='/Patient/Calendar/*')
global without sharing class VT_R3_RestPatientCalendar {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();
    private static final String EDIARY_EVENT_TYPE = 'ePRO';
    private static final String VISIT_EVENT_TYPE = 'visit';
    @HttpGet
    global static String getSpecificVisit() {
        // RestContext variables initiating
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        Map<String, String> paramsMap = request.params;
        // If branching: get single visit data or list of visits
        // GET single visit
        if (request.params.containsKey('visitId')) {
            String temp = paramsMap.get('visitId');
            // Check passed id for validity, form and fire response if ID not belongs to VTD1_Actual_Visit__c object
            if (String.isBlank(temp) || !helper.isIdCorrectType(temp, 'VTD1_Actual_Visit__c')) {
                response.statusCode = 400;
                cr.buildResponse(helper.forAnswerForIncorrectInput());
                return JSON.serialize(cr, true);
            }
            Id visitId = temp;
            VTD1_Actual_Visit__c actualVisit;
            List<Visit_Member__c> members = new List<Visit_Member__c>();
            List<VTD1_Document__c> visitDocuments;
            try {
                actualVisit = [
                        SELECT
                                Id,
                                Name,
                                VTD1_Formula_Name__c,
                                VTD1_Scheduled_Date_Time__c,
                                VTD1_Scheduled_Visit_End_Date_Time__c,
                                VTD1_Visit_Description__c,
                                VTD1_Reason_for_Request__c,
                                Unscheduled_Visits__c,
                                VTD1_Pre_Visit_Instructions__c,
                                VTD1_Status__c,
                                toLabel(VTD1_Status__c) statusLabel,
                                toLabel(VTR2_Modality__c) modalityLabel,
                                VTD1_Onboarding_Type__c,
                                VTD1_Protocol_Visit__c,
                                VTD1_Case__c,
                                toLabel(VTD1_Protocol_Visit__r.VTD1_Onboarding_Type__c),
                                toLabel(VTD1_Protocol_Visit__r.VTD1_VisitType__c),
                                VTD1_Additional_Patient_Visit_Checklist__c,
                                VTR2_Modality__c,
                                VTR2_Loc_Name__c,
                                VTR2_Loc_Address__c,
                                VTR2_Loc_Phone__c,
                                VTD1_Protocol_Visit__r.VTD1_EDC_Name__c,
                                VTD1_Protocol_Visit__r.VTD1_Visit_Description__c,
                                VTD1_Protocol_Visit__r.VTD1_PreVisitInstructions__c,
                                VTD1_Protocol_Visit__r.VTD1_Patient_Visit_Checklist__c,
                                VTD1_Protocol_Visit__r.VTD1_Visit_Checklist__c,
                                VTD1_Unscheduled_Visit_Type__c,
                                toLabel(VTD1_Unscheduled_Visit_Type__c) unscheduledVisitTypeTranslation
                        //,VTD1_Case__r.VTD1_Study__r.VTR3_LTFU__c
                        FROM VTD1_Actual_Visit__c
                        WHERE Id = :visitId
                        LIMIT 1
                ];
                VT_D1_TranslateHelper.translate(actualVisit);
                members = [
                        SELECT Id, Visit_Member_Name__c
                        FROM Visit_Member__c
                        WHERE VTD1_Actual_Visit__c = :actualVisit.Id
                ];
                visitDocuments = [
                        SELECT
                                Id,
                                Name,
                                VTD1_FileNames__c,
                                VTD1_Nickname__c,
                                VTD1_Comment__c,
                                VTD1_Version__c,
                                VTD1_Status__c,
                                toLabel(VTD1_Status__c) translatedStatus,
                                toLabel(VTR3_Category__c) translatedCategory,
                                CreatedBy.FirstName,
                                CreatedBy.LastName,
                                CreatedDate,
                                VTR3_Associated_Visit__c,
                                VTR3_Associated_Visit__r.Name,
                                VTR3_Category__c, (SELECT Id, ContentDocumentId FROM ContentDocumentLinks ORDER BY SystemModstamp DESC LIMIT 1)
                        FROM VTD1_Document__c
                        WHERE VTR3_Associated_Visit__c = :visitId
                ];
                System.debug('visitDocuments' + visitDocuments);
            } catch (QueryException q) {
                response.statusCode = 400;
                cr.buildResponse('FAILURE. Server error! Can not find a visit with this ID: ' + temp + '.');
                return JSON.serialize(cr, true);
            } catch (Exception e) {
                response.statusCode = 500;
                cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName());
                return JSON.serialize(cr, true);
            }
            List<ParticipantObj> participants = convertParticipants(members);
            CalendarEventObj v = new CalendarEventObj(
                    actualVisit.Id,
//                    getProtocolVisitLabelTranslated(actualVisit),
                    VT_D1_HelperClass.getActualVisitName(actualVisit),
                    actualVisit.VTD1_Scheduled_Date_Time__c,
                    actualVisit.VTD1_Scheduled_Visit_End_Date_Time__c,
                    getVisitDescription(actualVisit),
                    getPreVisitInstructions(actualVisit),
                    actualVisit.VTD1_Status__c,
                    participants,
                    getPatientVisitChecklist(actualVisit),
                    actualVisit.VTR2_Modality__c,
                    String.valueOf(actualVisit.get('modalityLabel')),
                    actualVisit.VTR2_Loc_Name__c,
                    actualVisit.VTR2_Loc_Address__c,
                    actualVisit.VTR2_Loc_Phone__c,
                    VISIT_EVENT_TYPE);
            v.addDocuments(visitDocuments);
            if(!Test.isRunningTest()) {
                helper.addPreviewsWithBatchCallout(v.documents); //for R4
            }
            cr.buildResponse(v);
            return JSON.serialize(cr, true);
        }
        // GET list of visits
        else {
            Datetime startDatetime = System.now().addHours(UserInfo.getTimeZone().getOffset(System.now()) / 1000 / 60 / 60).date().toStartOfWeek();
            Datetime endDatetime = startDatetime.addDays(7);
            Set<Id> visitsIds = new Set<Id>();
            Case casePatient = VT_D1_PatientCaregiverBound.getPatientCase(UserInfo.getUserId());
            List<VTD1_Actual_Visit__c> fiveDaysVisits = [
                    SELECT Id, Name, VTD1_Formula_Name__c, VTD1_Scheduled_Date_Time__c, VTD1_Scheduled_Visit_End_Date_Time__c,
                            VTD1_Visit_Description__c,
                            VTD1_Pre_Visit_Instructions__c,
                            VTD1_Case__r.ContactId, VTD1_Visit_Label__c,
                            VTD1_Status__c, VTD1_Reason_for_Request__c,
                            Unscheduled_Visits__c,
                            VTD1_Onboarding_Type__c,
                            VTD1_Additional_Patient_Visit_Checklist__c,
                            VTR2_Modality__c,
                            toLabel(VTR2_Modality__c) modalityLabel,
                            VTR2_Loc_Name__c,
                            VTD1_Case__c,
                            VTR2_Loc_Address__c,
                            toLabel(VTD1_Protocol_Visit__r.VTD1_Onboarding_Type__c),
                            toLabel(VTD1_Protocol_Visit__r.VTD1_VisitType__c),
                            VTR2_Loc_Phone__c,
                            VTD1_Protocol_Visit__c,
                            VTD1_Case__r.VTD1_Study__r.VTR3_LTFU__c,
                            VTD1_Unscheduled_Visit_Type__c,
                            VTD1_Protocol_Visit__r.VTD1_EDC_Name__c,
                            VTD1_Protocol_Visit__r.VTD1_Visit_Description__c,
                            VTD1_Protocol_Visit__r.VTD1_PreVisitInstructions__c,
                            VTD1_Protocol_Visit__r.VTD1_Patient_Visit_Checklist__c,
                            VTD1_Protocol_Visit__r.VTD1_Visit_Checklist__c,
                            toLabel(VTD1_Unscheduled_Visit_Type__c) unscheduledVisitTypeTranslation
                    FROM VTD1_Actual_Visit__c
                    WHERE (VTD1_Case__c = :casePatient.Id OR Unscheduled_Visits__c = :casePatient.Id)
                    AND VTD1_Scheduled_Date_Time__c != NULL
                    AND VTD1_Status__c != 'Cancelled'
                    AND (NOT (VTD1_Protocol_Visit__r.VTD1_isArchivedVersion__c = TRUE AND VTD1_Status__c != 'Completed'))
                    AND (VTD1_Scheduled_Date_Time__c > :startDatetime AND VTD1_Scheduled_Date_Time__c < :endDatetime)
            ];
            VT_D1_TranslateHelper.translate(fiveDaysVisits);
            for (VTD1_Actual_Visit__c av : fiveDaysVisits) {
                visitsIds.add(av.Id);
            }
            Map<Id, List<Visit_Member__c>> visitIdToMemberList = new Map<Id, List<Visit_Member__c>>();
            for (Visit_Member__c vm : [
                    SELECT Id, Visit_Member_Name__c,VTD1_Actual_Visit__c
                    FROM Visit_Member__c
                    WHERE VTD1_Actual_Visit__c IN:visitsIds
            ]) {
                if (!visitIdToMemberList.containsKey(vm.VTD1_Actual_Visit__c)) {
                    visitIdToMemberList.put(vm.VTD1_Actual_Visit__c, new List<Visit_Member__c>());
                }
                visitIdToMemberList.get(vm.VTD1_Actual_Visit__c).add(vm);
            }
            List<CalendarEventObj> visitObjs = buildResponse(fiveDaysVisits, visitIdToMemberList);
            cr.buildResponse(visitObjs);
            return JSON.serialize(cr, true);
        }
    }
    @HttpPost
    global static String getCalendarEvents(Long startDate, Long endDate, String eventsIds) {
        RestResponse response = RestContext.response;
        Datetime startDateValue = Datetime.newInstance(startDate - timeOffset);
        Datetime endDateValue = Datetime.newInstance(endDate - timeOffset);
        if (startDateValue > endDateValue) {
            response.statusCode = 400;
            cr.buildResponse('FAILURE. startDate - ' + startDate + ' is after endDate - ' + endDate);
            return JSON.serialize(cr, true);
        }
        List<VT_D1_PatientCalendar.EventObj> eventsList = VT_D1_PatientCalendar.getEvents();
        Set<Id> clientIds = parseRawIds(eventsIds);
        CalendarResponse calendarResponse = new CalendarResponse();
        List<CalendarEventObj> withinRange = new List<CalendarEventObj>();
        List<CalendarEventObj> outOfRange = new List<CalendarEventObj>();
        Set<Id> toDel = new Set<Id>();
        for (VT_D1_PatientCalendar.EventObj eventObj : eventsList) {
            System.debug('type *** ' + eventObj.type);
            if (eventObj.startDateTime >= startDateValue && eventObj.startDateTime <= endDateValue) {
                if (clientIds.contains(eventObj.Id)) {
                    clientIds.remove(eventObj.Id);
                }
                withinRange.add(formCalendarEventObj(eventObj));
            } else if (clientIds.contains(eventObj.Id)) {
                outOfRange.add(formCalendarEventObj(eventObj));
                clientIds.remove(eventObj.Id);
            }
        }
        toDel.addAll(clientIds);
        calendarResponse.eventsOutOfRange = outOfRange;
        calendarResponse.eventsWithinRange = withinRange;
        calendarResponse.eventsToDelete = toDel;
        cr.buildResponse(calendarResponse);
        return JSON.serialize(cr, true);
    }
    /**
     * the method forms CalendarEventObj out of VT_D1_PatientCalendar.EventObj
     *
     * @param eventObj @see VT_D1_PatientCalendar.EventObj
     *
     * @return CalendarEventObj
     */
    private static CalendarEventObj formCalendarEventObj(VT_D1_PatientCalendar.EventObj eventObj) {
        if (eventObj.type.equals(VISIT_EVENT_TYPE)) {
            List<Visit_Member__c> visitMembers = new List<Visit_Member__c>();
            for (Integer i = 0; i < eventObj.participants.size(); i++) {
                if (eventObj.participants.get(i).VTD1_Member_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME) {
                    visitMembers.add(eventObj.participants.get(i));
                }
            }
            List<ParticipantObj> convertedParticipants = convertParticipants(visitMembers);
            return new CalendarEventObj(
                    eventObj.Id,
                    eventObj.name,
//                    eventObj.label,
//                    VT_D1_HelperClass.getActualVisitName(eventObj);
                    eventObj.startDateTime,
                    eventObj.endDateTime,
                    eventObj.description,
                    eventObj.instructions,
                    eventObj.status,
                    convertedParticipants,
                    eventObj.visitChecklist,
                    eventObj.modalityApiName,
                    eventObj.modality,
                    eventObj.location,
                    eventObj.address,
                    eventObj.phone,
                    VISIT_EVENT_TYPE
            );
        } else {  // if (eventObj.type.equals(EDIARY_EVENT_TYPE)) {
            return new CalendarEventObj(
                    eventObj.Id,
                    eventObj.label,
                    eventObj.startDateTime,
                    eventObj.endDateTime,
                    eventObj.description,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    EDIARY_EVENT_TYPE
            );
        }
    }
    /**
     * method parses a raw string to set of VTD1_Actual_Visit__c ONLY ids
     *
     * @param rawIds raw String with ids, separated by commas
     *
     * @return Set of valid VTD1_Actual_Visit__c ids
     */
    private static Set<Id> parseRawIds(String rawIds) {
        Set<Id> result = new Set<Id>();
        String visitIdPrefix = VTD1_Actual_Visit__c.getSObjectType().getDescribe().getKeyPrefix();
        if (rawIds != null) {
            for (String s : rawIds.remove(' ').remove('"').remove('\'').split(',')) {
                if (s.startsWith(visitIdPrefix)) {
                    result.add(s);
                }
            }
        }
        return result;
    }
    /**
     * Adds a value ( @param valueToAdd ) to list ( @param listToAdd )
     *
     * @param listToAdd
     * @param valueToAdd
     */
    private static void addToList(List<Id> listToAdd, Id valueToAdd) {
        if (listToAdd == null) {
            listToAdd = new List<Id>();
        }
        listToAdd.add(valueToAdd);
    }
    /**
     * the method builds List<CalendarEventObj> out of list<VTD1_Actual_Visit__c> visits and relevant visit members
     *
     * @param visits income List<VTD1_Actual_Visit__c>
     * @param membersMap income Map<Id, List<Visit_Member__c>> map visit Id to list of visit members
     *
     * @return List<CalendarEventObj>
     */
    private static List<CalendarEventObj> buildResponse(List<VTD1_Actual_Visit__c> visits, Map<Id, List<Visit_Member__c>> membersMap) {
        List<CalendarEventObj> result = new List<VT_R3_RestPatientCalendar.CalendarEventObj>();
        for (VTD1_Actual_Visit__c av : visits) {
            List<ParticipantObj> participants = new List<ParticipantObj>();
            if (membersMap.containsKey(av.Id)) {
                participants = convertParticipants(membersMap.get(av.Id));
            }
            CalendarEventObj singleVisitObj = new CalendarEventObj(
                    av.Id,
//                    getProtocolVisitLabelTranslated(av),
                    VT_D1_HelperClass.getActualVisitName(av),
                    av.VTD1_Scheduled_Date_Time__c,
                    av.VTD1_Scheduled_Visit_End_Date_Time__c,
                    getVisitDescription(av),
                    getPreVisitInstructions(av),
                    av.VTD1_Status__c,
                    participants,
                    getPatientVisitChecklist(av),
                    av.VTR2_Modality__c,
                    String.valueOf(av.get('modalityLabel')),
                    av.VTR2_Loc_Name__c,
                    av.VTR2_Loc_Address__c,
                    av.VTR2_Loc_Phone__c,
                    VISIT_EVENT_TYPE);
            result.add(singleVisitObj);
        }
        return result;
    }
    private static String getVisitDescription(VTD1_Actual_Visit__c visit){
        String type = visit.VTD1_Unscheduled_Visit_Type__c;
        if(visit.Unscheduled_Visits__c == null) {
            if(visit.VTD1_Protocol_Visit__c != null) {
                return visit.VTD1_Protocol_Visit__r.VTD1_Visit_Description__c;
            }
        } if (type == 'End of Study' && visit.Name == 'End of Study') {
            return VT_D1_TranslateHelper.getLabelValue(Label.VTR3_ReconsentRefused);
        } else if (type == 'Screening Results Visit' && visit.Name == 'Screening Result Visit') {
            return VT_D1_TranslateHelper.getLabelValue(Label.VTR3_ScreeningResultVisit);
        } else if (type == 'Screening Results Visit' && visit.Name == 'Washout/Run-In Results Visit') {
            return VT_D1_TranslateHelper.getLabelValue(Label.VTR3_WashoutRunInResultsVisit);
        } else {
            return visit.VTD1_Reason_for_Request__c;
        }
    }
    @TestVisible
    private static String getPreVisitInstructions(VTD1_Actual_Visit__c visit) {
        if(visit.VTD1_Protocol_Visit__c != null) {
            return visit.VTD1_Protocol_Visit__r.VTD1_PreVisitInstructions__c;
        } else {
            return visit.VTD1_Pre_Visit_Instructions__c;
        }
    }
    @TestVisible
    private static String getPatientVisitChecklist(VTD1_Actual_Visit__c visit) {
        if(visit.VTD1_Protocol_Visit__c != null) {
            return visit.VTD1_Protocol_Visit__r.VTD1_Patient_Visit_Checklist__c;
        } else {
            return visit.VTD1_Additional_Patient_Visit_Checklist__c;
        }
    }
    @TestVisible
    private static String getVisitChecklist(VTD1_Actual_Visit__c visit) {
        if(visit.VTD1_Protocol_Visit__c != null) {
            return visit.VTD1_Protocol_Visit__r.VTD1_Visit_Checklist__c;
        } else {
            return visit.VTD1_Additional_Visit_Checklist__c;
        }
    }

    /**
     * the method converts a list of Visit_Member__c object to list of ParticipantObj
     *
     * @param members income List<Visit_Member__c>
     *
     * @return List<ParticipantObj>
     */
    private static List<ParticipantObj> convertParticipants(List<Visit_Member__c> members) {
        List<ParticipantObj> result = new List<ParticipantObj>();
        for (Visit_Member__c member : members) {
            if (String.isNotBlank(member.Visit_Member_Name__c)) {
                ParticipantObj po = new ParticipantObj(member.Id, member.Visit_Member_Name__c);
                result.add(po);
            }
        }
        return result;
    }
    public class CalendarResponse {
        public List<CalendarEventObj> eventsWithinRange;
        public List<CalendarEventObj> eventsOutOfRange;
        public Set<Id> eventsToDelete;
    }
    public class ParticipantObj {
        public String id;
        public String name;
        public ParticipantObj(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }
    public class CalendarEventObj {
        public String id;
        public String name;
        public Long startDateTime;
        public Long endDateTime;
        public String description;
        public String instructions;
        public List<ParticipantObj> participants;
        public String type;
        public String status;
        public String visitChecklist;
        public String modality;
        public String modalityLabel;
        public String location;
        public String address;
        public String phone;
        public String eventType;
        public List<VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper> documents;
        public CalendarEventObj(String i, String nameString, Datetime startDate, Datetime endDate, String descr, String instructions,
                String status, List<ParticipantObj> participants, String visitChecklist, String modality, String modalityLabel, String locationName, String addr, String phone, String eventType) {
            this.id = i;
            this.name = nameString;
            this.startDateTime = startDate != null ? startDate.getTime() + timeOffset : null;
            this.endDateTime = endDate != null ? endDate.getTime() + timeOffset : null;
            this.description = descr;
            this.instructions = instructions;
            this.status = status;
            this.participants = participants;
            this.visitChecklist = visitChecklist;
            this.modality = modality;
            this.modalityLabel = modalityLabel;
            this.location = locationName;
            this.address = addr;
            this.phone = phone;
            this.eventType = eventType;
        }
        public void addDocuments(List<VTD1_Document__c> docs) {
            List<VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper> result = new List<VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper>();
            for (VTD1_Document__c d : docs) {
                VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper mdw = new VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper(d);
//                mdw.addDownloadLink();
                result.add(mdw);
            }
            System.debug('result ' + result);
            result = addDownloadLinks(result);
            this.documents = result;
        }
        public List<VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper> addDownloadLinks(List<VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper> income) {
            Map<Id, VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper> contentDocumentIdsToWrappers = new Map<Id, VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper>();
            for (VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper mdw : income) {
                if (String.isNotBlank(mdw.contentDocumentId)) {
                    contentDocumentIdsToWrappers.put(mdw.contentDocumentId, mdw);
                }
            }
            //TODO: logic can be better
            for (ContentVersion cv : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN :contentDocumentIdsToWrappers.keySet() ORDER BY CreatedDate ASC]) {
                VT_R3_RestPatientStudyDocuments.MobileDocumentWrapper current = contentDocumentIdsToWrappers.get(cv.ContentDocumentId);
                current.fileDownloadLink = System.Url.getSalesforceBaseUrl().toExternalForm() + VT_D1_HelperClass.getSandboxPrefix() + '/services/data/v46.0/sobjects/ContentVersion/' + cv.Id + '/VersionData';
            }
            return contentDocumentIdsToWrappers.values();
        }
    }
}