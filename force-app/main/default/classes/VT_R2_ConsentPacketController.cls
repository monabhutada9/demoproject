/**
 * Created by Dmitry Yartsev on 25.02.2019.
 */

public with sharing class VT_R2_ConsentPacketController {

    /**
    * Get pdf document and download url
    * @author Viktar Bobich
    * @param documentID
    * @date 29/06/2020
    * @issue SH-13030
    * @return ConsentPacketWrapper
    */
    @AuraEnabled
    public static ConsentPacketWrapper getConsentPacketWrapper(String documentID){
        ConsentPacketWrapper wrapper = new ConsentPacketWrapper();
        wrapper.consentPacketData = getConsentPacket(documentID);
        wrapper.downloadUrl = System.Url.getSalesforceBaseUrl().toExternalForm()
                + VT_D1_HelperClass.getSandboxPrefix() + '/sfc/servlet.shepherd/document/download/'
                + documentID;
        return wrapper;
    }

    private static String getConsentPacket(String documentID) {
        try {
            String pdfData;
            pdfData = getContentAsPDF(documentID);
            String data = JSON.serialize(pdfData);
            return data;
        } catch (Exception e){
            System.debug('VT_R2_ConsentPacketController ' + e);
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    private static String getContentAsPDF(String documentID) {
        if (documentID == null) return null;
        PageReference pageRef = new PageReference('/sfc/servlet.shepherd/document/download/' + documentID);
        if (Test.IsRunningTest()) {
            return null;
        } else {
            return EncodingUtil.base64Encode(pageRef.getContentAsPDF());
        }
    }

    public class ConsentPacketWrapper{
        @AuraEnabled
        public String consentPacketData;
        @AuraEnabled
        public String downloadUrl;
    }
}