/**
 * Created by User on 19/05/10.
 */
@isTest
public with sharing class VTD2_TMA_Task_TriggerTest {
    public static void doTest(){
        User pgUser = (User) new DomainObjects.User_t()
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME)
                .persist();
        System.runAs(pgUser) {
            VTD2_TMA_Task__c tma_task = new VTD2_TMA_Task__c(OwnerId = pgUser.Id);
            insert tma_task;
            tma_task.VTD2_Status__c = 'Open';
            update tma_task;
        }
    }
}