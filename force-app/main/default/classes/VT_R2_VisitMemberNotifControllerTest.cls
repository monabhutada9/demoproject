/**
 * Created by user on 26.03.2019.
 */

@IsTest
private class VT_R2_VisitMemberNotifControllerTest {
    @TestSetup
    static void setupMethod(){
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c testStudy = VT_D1_TestUtils.prepareStudy(1);
        VTD2_Study_Geography__c studyGeography = new VTD2_Study_Geography__c(VTD2_Study__c = testStudy.Id, Name = 'Geo');
        insert studyGeography;

        VTR2_StudyPhoneNumber__c phoneNumber = new VTR2_StudyPhoneNumber__c(VTR2_Study__c = testStudy.Id,
                VTR2_Study_Geography__c = studyGeography.Id,
                Name = '8888888888',
                VTR2_Language__c = 'en_US');
        insert phoneNumber;
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
        Case cs = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];
        testStudy.VTD1_Protocol_Nickname__c = 'Study nickName';
        update testStudy;
        cs.VTR2_StudyPhoneNumber__c = phoneNumber.Id;
        cs.VTD1_Study__r = testStudy;

        Id caseId = cs.Id;
        cs.VTD1_Enrollment_Date__c = Datetime.now().addDays(-5).date();
        update cs;
        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Range__c = 4;
        protocolVisit.VTD1_VisitOffset__c = 2;
        protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient; Site Coordinator';
        insert protocolVisit;
        VTD1_ProtocolVisit__c pv = [SELECT VTD1_Range__c FROM VTD1_ProtocolVisit__c where id =:protocolVisit.Id];

        VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
        actualVisit.VTD1_Protocol_Visit__c = protocolVisit.Id;
        actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
        actualVisit.VTD1_Case__c = caseId;
        actualVisit.VTR2_Televisit__c = true;
        insert actualVisit;

//        Video_Conference__c conf = new Video_Conference__c(VTD1_Actual_Visit__c = actualVisit.Id);
//        insert conf;
//
//        List<VTD1_Conference_Member__c> confMembers = new List<VTD1_Conference_Member__c>();
//        for (User item : [
//                SELECT Id FROM User
//                WHERE FirstName IN ('PrimaryPGUser1','SecondaryPGUser1','PIUser1','PIBackupUser1','CP1')
//        ]) {
//            confMembers.add(new VTD1_Conference_Member__c(
//                    VTD1_User__c = item.Id,
//                    VTD1_Video_Conference__c = conf.Id
//            ));
//        }
//        insert confMembers;
    }

//    @IsTest
//    static void testBehaviorParticipantUser() {
//        User patient = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Patient' AND ContactId != null AND IsActive = true LIMIT 1];
//        User PI = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'Primary Investigator' AND ContactId != null AND IsActive = true LIMIT 1];
//
//        Id actualVisitId = [select Id from VTD1_Actual_Visit__c LIMIT 1].Id;
//        List<Visit_Member__c> VMs = new List<Visit_Member__c>();
//        VMs.add(new Visit_Member__c(VTD1_Actual_Visit__c = actualVisitId,
//                VTD1_Participant_User__c = patient.Id,
//                Participant_Contact__c = patient.ContactId));
//        VMs.add(new Visit_Member__c(VTD1_Actual_Visit__c = actualVisitId,
//                VTD1_Participant_User__c = PI.Id,
//                Participant_Contact__c = PI.ContactId));
//        VMs.add(new Visit_Member__c(VTD1_Actual_Visit__c = actualVisitId,
//                VTD1_External_Participant_Email__c = 'test@test.com',
//                VTD1_Proposed_Visit_Date_Time__c = Datetime.now().addDays(2)));
////        insert VMs;
//    }
//
    @IsTest
    static void testBehaviorParticipantUserDontUse() {
        Test.startTest();
        VT_R2_VisitMemberNotificationController vmnc = new VT_R2_VisitMemberNotificationController();
        vmnc.visitMemberId = [select id from Visit_Member__c where VTD1_Participant_User__c <> null limit 1].id;
        vmnc.getVisitMember();
        Visit_Member__c visitMember = vmnc.getVisitMember();
        visitMember.VTD1_Actual_Visit__r.VTD1_Contact__r = [SELECT Id, Name FROM Contact LIMIT 1];
//        update visitMember;
        visitMember.VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c = DateTime.parse('10/25/2018 11:11 AM');
        vmnc.getReasonForParticipation();
        visitMember.RecordTypeId = null;
        visitMember.VTR2_ReasonForParticipation__c = 'test text';
        System.assertEquals('test text', vmnc.getReasonForParticipation());
        vmnc.getSalutaion();
        System.debug(VisitRequestConfirmSite__c.getInstance().SalesforceBaseURL__c);
        [select id from Visit_Member__c limit 1][0].recalculateFormulas();
        vmnc.getMessageBody();
        visitMember.VTD1_Actual_Visit__r.VTD1_Status__c = 'Cancelled';
        visitMember.VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c = Datetime.now();
        visitMember.VTD1_Actual_Visit__r.VTD1_Unscheduled_Visit_Type__c = 'Screening Results Visit';
        visitMember.VTD1_Participant_User__r.Profile.Name = 'Caregiver';
        vmnc.getMessageBody();

        visitMember.VTD1_Participant_User__r.Profile.Name = 'Caregiver';
        visitMember.VTD1_Participant_User__c = null;
        visitMember.VTD1_Actual_Visit__r.VTD1_Case__c = null;
        visitMember.VTR2_ReasonForParticipation__c = 'Test Reason for Participation';
        vmnc.getMessageBody();

        vmnc.getMessageBody(vmnc.getVisitMember());
        //System.assertEquals('PIUser1 L Visit Cancelled for The TestProt-001 Study', vmnc.getEmailSubject('cancel'));
        // System.assertEquals('Added to PIUser1 L Visit for The TestProt-001 Study', vmnc.getEmailSubject('add'));
        //System.assertEquals('Removed from PIUser1 L Visit for The TestProt-001 Study', vmnc.getEmailSubject('remove'));
        Test.stopTest();
    }
//
//    @isTest
//    public static void test (){
//        List<Visit_Member__c> visitMemberList = [SELECT id, VTD1_Member_Type__c, VTD1_Actual_Visit__r.VTD2_Common_Visit_Type__c FROM Visit_Member__c];
//        VT_R2_VisitMemberNotificationController vmnc = new VT_R2_VisitMemberNotificationController();
//        for (Visit_Member__c visitMember : visitMemberList) {
//            if(visitMember.VTD1_Member_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME ||
//                    visitMember.VTD1_Member_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME ){
//                vmnc.visitMemberId = visitMember.Id;
//            }
//        }
//
//        vmnc.getSalutaion();
//        vmnc = new VT_R2_VisitMemberNotificationController();
//        for (Visit_Member__c visitMember : visitMemberList) {
//            if(visitMember.VTD1_Member_Type__c != VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME &&
//                    visitMember.VTD1_Member_Type__c != VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME &&
//                    visitMember.VTD1_Member_Type__c != VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME){
//                vmnc.visitMemberId = visitMember.Id;
//            }
//        }
//        vmnc.getSalutaion();
//        vmnc = new VT_R2_VisitMemberNotificationController();
//        for (Visit_Member__c visitMember : visitMemberList) {
//            if(visitMember.VTD1_Member_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME ){
//                vmnc.visitMemberId = visitMember.Id;
//            }
//        }
//
//        vmnc = new VT_R2_VisitMemberNotificationController();
//        for (Visit_Member__c vM : visitMemberList) {
//            if(vM.VTD1_Member_Type__c != VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME ){
//                vmnc.visitMemberId = vM.Id;
//            }
//        }
//        Visit_Member__c visitMember = vmnc.getVisitMember();
//        visitMember.VTD1_Actual_Visit__r.VTD1_Status__c = 'Cancelled';
//        visitMember.VTD1_Participant_User__c = null;
//        visitMember.VTD1_Actual_Visit__r.VTD1_Protocol_Visit__r.VTD1_EDC_Name__c = 'edc name';
//        VTD1_Actual_Visit__c actualVisit = [SELECT VTD1_Unscheduled_Visit_Type__c, VTD1_Protocol_Visit__r.VTD1_VisitType__c
//                                            FROM VTD1_Actual_Visit__c WHERE Id = :visitMember.VTD1_Actual_Visit__c];
//        actualVisit.VTD1_Unscheduled_Visit_Type__c = 'Labs';
//        actualVisit.VTD1_Protocol_Visit__r.VTD1_VisitType__c = 'Study Team';
//        VTD1_ProtocolVisit__c protocolVisit = [SELECT VTD1_VisitType__c FROM VTD1_ProtocolVisit__c WHERE Id = :actualVisit.VTD1_Protocol_Visit__c];
//        protocolVisit.VTD1_VisitType__c = 'Labs';
//        update actualVisit;
//        update protocolVisit;
//        System.debug('sdsdsdsdsd ' + actualVisit.Id);
//        System.debug('sdsdsdsdsd ' + actualVisit.VTD1_Unscheduled_Visit_Type__c);
//        System.debug('sdsdsdsdsd ' + actualVisit.VTD1_Protocol_Visit__r.VTD1_VisitType__c);
//        vmnc.getMessageBody();
//    }
//
//    @IsTest
//    static void testBehaviorNoParticipantUserDontUse() {
//        Test.startTest();
//        VT_R2_VisitMemberNotificationController vmnc = new VT_R2_VisitMemberNotificationController();
////        vmnc.visitMemberId = [select id from Visit_Member__c where VTD1_Participant_User__c = null limit 1].id;
////        vmnc.getVisitMember();
////        vmnc.getVisitDescription();
////        vmnc.getVisitTimeScheduled();
////        vmnc.getReasonForParticipation();
//
//        System.debug(VisitRequestConfirmSite__c.getInstance().SalesforceBaseURL__c);
//        [select id from Visit_Member__c limit 1][0].recalculateFormulas();
////        vmnc.getMessageBody();
////        vmnc.getMessageBody(vmnc.getVisitMember());
////        vmnc.getEmailSubject('cancel');
////        vmnc.getEmailSubject('add');
////        vmnc.getEmailSubject('remove');
//        Test.stopTest();
//    }
//
//    @isTest
//    public static void ExceptionTest(){
//        try {
//            VT_R2_VisitMemberNotificationController vmnc = new VT_R2_VisitMemberNotificationController();
//            vmnc.getMessageBody();
//        }
//        catch(Exception e){
//        }
//    }

    @isTest
    public static void getSalutaionTest() {
        String visitMemeberName;
        VT_R2_VisitMemberNotificationController notificationController = new VT_R2_VisitMemberNotificationController();
        List<Visit_Member__c> visitMemberList = [SELECT id, VTD1_Member_Type__c, VTD1_Actual_Visit__r.VTD2_Common_Visit_Type__c, Visit_Member_Name__c, VTD1_Participant_User__r.FirstName FROM Visit_Member__c];
        for (Visit_Member__c visitMember : visitMemberList) {
            System.debug('******** ' + visitMember.VTD1_Member_Type__c);
            if(visitMember.VTD1_Member_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME ){
                notificationController.visitMemberId = visitMember.Id;
            }
            else if(visitMember.VTD1_Member_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME ) {
                System.debug('visitMember.Id ' + visitMember.Id);
                notificationController.visitMemberId = visitMember.Id;
            }
            else if(visitMember.VTD1_Member_Type__c != VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME && visitMember.VTD1_Member_Type__c != VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME ) {
                notificationController.visitMemberId = visitMember.Id;
            }
            else if(visitMember.VTD1_Member_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME || visitMember.VTD1_Member_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME ) {
                notificationController.visitMemberId = visitMember.Id;
            }
            notificationController.getSalutaion();
        }
    }

    @isTest
    public static void getMessageBodyTest() {
        Visit_Member__c visitMember2 = [SELECT Id, VTD1_Actual_Visit__c FROM Visit_Member__c WHERE VTD1_Participant_User__c != null LIMIT 1];
        VT_R2_VisitMemberNotificationController notificationController = new VT_R2_VisitMemberNotificationController();
        notificationController.visitMemberId = visitMember2.id;
        Visit_Member__c visitMember = notificationController.getVisitMember();
        notificationController.isVisitMemberAdded = true;
        visitMember.VTR2_ReasonForParticipation__c = 'reason';
//        notificationController.mode = 'Internal';
//        notificationController.timeOffset = '2HourBefore';
        notificationController.getMessageBody();
        notificationController.isVisitSoftCanceled = true;
        notificationController.isVisitReqReschedule = true;
        notificationController.isVisitMemberRemoved = false;
//        notificationController.mode = 'External';
        notificationController.mode = 'Site Coordinator';
        notificationController.getMessageBody();

        notificationController.isVisitSoftCanceled = false;
        notificationController.getMessageBody();

        notificationController.isVisitSoftCanceled = false;
        notificationController.isVisitReqReschedule = false;
        notificationController.mode = null;
        notificationController.getMessageBody();

        notificationController.isVisitSoftCanceled = true;
        notificationController.isVisitReqReschedule = true;
        notificationController.mode = 'Main Site Coordinator';

        notificationController.getMessageBody();

        notificationController.mode = 'Primary Investigator';
        notificationController.getMessageBody();

        notificationController.mode = 'PatientCaregiver';
        notificationController.getMessageBody();

        notificationController.mode = 'External';
        notificationController.getMessageBody();
        notificationController.getClosingMail();

        notificationController.mode = 'PatientCaregiver';
        notificationController.timeOffset = '1HourBefore';
        notificationController.getMessageBody();

        notificationController.mode = 'PatientCaregiverPSC';
        notificationController.timeOffset = '1HourBefore';
        notificationController.getMessageBody();

        notificationController.mode = 'PatientCaregiver';
        notificationController.timeOffset = '2DaysBefore';
        notificationController.getMessageBody();

        notificationController.mode = 'External';
        notificationController.timeOffset = '1HourBefore';
        notificationController.getMessageBody();

        notificationController.mode = 'Primary Investigator';
        notificationController.timeOffset = '1HourBefore';
        notificationController.getMessageBody();

        notificationController.mode = 'SCR';
        notificationController.timeOffset = '2HourBefore';
        notificationController.getMessageBody();

        notificationController.mode = 'External';
        notificationController.timeOffset = '2DaysBefore';
        notificationController.getMessageBody();

        notificationController.mode = '--External--';
        notificationController.timeOffset = '--2DaysBefore--';
        notificationController.isVisitMemberRemoved = false;
        notificationController.isVisitMemberAdded = true;
//        notificationController.isVisitMemberRemoved = false;
//        notificationController.isVisitMemberRemoved = false;
        notificationController.getMessageBody();

        notificationController.mode = null;
        notificationController.timeOffset = null;
        visitMember.VTD1_Actual_Visit__r.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED;
        notificationController.getMessageBody();

        notificationController.visitId = visitMember2.VTD1_Actual_Visit__c;
        notificationController.getMessageBody(null);
    }

    @isTest
    public static void getJoinToTelevisitTest() {
        VTD1_Actual_Visit__c actualVisit = [SELECT VTR2_Televisit__c FROM VTD1_Actual_Visit__c LIMIT 1];
        VT_R2_VisitMemberNotificationController notificationController = new VT_R2_VisitMemberNotificationController();
        notificationController.visitMemberId = [SELECT Id FROM Visit_Member__c WHERE VTD1_Participant_User__c != null LIMIT 1].id;
        Visit_Member__c visitMember = notificationController.getVisitMember();
        visitMember.VTD1_Actual_Visit__r = actualVisit;
        notificationController.getJoinToTelevisit();
    }

    @isTest
    public static void replacePhoneNumberTest(){
        VT_R2_VisitMemberNotificationController notificationController = new VT_R2_VisitMemberNotificationController();
        Id visitMemberId = [SELECT Id FROM Visit_Member__c WHERE VTD1_Participant_User__c != null LIMIT 1].id;
        notificationController.visitMemberId = visitMemberId;
        Visit_Member__c visitMember = notificationController.getVisitMember();
        visitMember.VTD1_Participant_User__c = null;
        notificationController.NonStudyHubText = 'phone number #Phone_number';
        notificationController.replacePhoneNumber('phone number #Phone_number');
        System.assertEquals('phone number 8888888888', notificationController.NonStudyHubText);
        VTD1_Actual_Visit__c visit = [SELECT Id, VTD1_Case__c FROM VTD1_Actual_Visit__c WHERE Id = :visitMember.VTD1_Actual_Visit__c];
        visit.VTD1_Case__c = null;
        update visit;
        notificationController = new VT_R2_VisitMemberNotificationController();
        notificationController.visitMemberId = visitMemberId;
        visitMember = notificationController.getVisitMember();
        visitMember.VTD1_Participant_User__c = null;
        notificationController.NonStudyHubText = 'no number #Phone_number';
        notificationController.replacePhoneNumber('phone number #Phone_number');
    }

    @isTest
    public static void getLinkToStudyHubTest(){
        VT_R2_VisitMemberNotificationController notificationController = new VT_R2_VisitMemberNotificationController();
        List<Visit_Member__c> visitMemberList = [SELECT Id, VTD1_Participant_User__r.Profile.Name FROM Visit_Member__c WHERE VTD1_Participant_User__c != null];
        Visit_Member__c visitMember;
        for(Visit_Member__c vm : visitMemberList){
            System.debug('!!!!!!!! ' + vm.VTD1_Participant_User__r.Profile.Name);
            if (vm.VTD1_Participant_User__r.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME){
                notificationController.visitMemberId = vm.Id;
                visitMember = notificationController.getVisitMember();
                notificationController.getLinkToStudyHub();
            }
        }
    }

}