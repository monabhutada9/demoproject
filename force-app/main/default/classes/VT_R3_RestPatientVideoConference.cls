/**
 * Created by Alexander Komarov on 05.06.2019.
 */

@RestResource(UrlMapping='/Patient/VideoConference/*')
global without sharing class VT_R3_RestPatientVideoConference {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();


    @HttpGet
    global static String getVideoMethod() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        System.debug('request.requestURI '+request.requestURI);
        System.debug('request.params '+request.params);

        String path = request.requestURI.substringAfter('/VideoConference/').substringBefore('?');
        if (path.equals('Token')) {
            Map<String, String> params = request.params;
            System.debug('params '+params);
            String sessionId = params.get('sessionId');
            System.debug('session  '+sessionId);

            if (sessionId == null) {
                response.statusCode = 400;
                cr.buildResponse('FAILURE. sessionId parameter is missing');
                return JSON.serialize(cr, true);
            }

            String herokuAppURL = ExternalVideoController.getHerokuAppURL();

            HttpRequest req = new HttpRequest();

            req.setEndpoint(herokuAppURL + '/api/token');
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setBody('{"sessionId":"'+sessionId+'"}');
            Http http = new Http();
            HttpResponse res = http.send(req);

            String token = ((Token)JSON.deserialize(res.getBody(), Token.class)).message;
            cr.buildResponse(new Map<String, String>{'token' => token});

            return JSON.serialize(cr, true);
        } else if (path.equals('Info')) {
            Info info = new Info();
            try {
                info.participants = (List<Participant>) JSON.deserialize(VideoPanelTreeController.getParticipants(null), List<Participant>.class);
                info.checklist = VideoPanelTreeController.getChecklists(null);
            } catch (Exception e) {
                response.statusCode = 500;
                cr.buildResponse('FAILURE. Can not get video checklist or video-conference participants');
                return JSON.serialize(cr, true);
            }

            cr.buildResponse(info);
            return JSON.serialize(cr, true);

        } else if (path.equals('SessionId')) {
            Map<String, String> params = request.params;
            String temp;
            Id visitId;
            try {
                temp = params.get('visitId');
                visitId = temp;
            } catch (Exception e) {
                response.statusCode = 452;
                cr.buildResponse('FAILURE. visitId parameter is missing');
                return JSON.serialize(cr, true);
            }

            if (!helper.isIdCorrectType(visitId, 'VTD1_Actual_Visit__c')) {
                response.statusCode = 400;
                cr.buildResponse(helper.forAnswerForIncorrectInput());
                return JSON.serialize(cr, true);
            }

            try {
                String sessionId = [
                        SELECT Id, SessionId__c, VTD1_Actual_Visit__c
                        FROM Video_Conference__c
                        WHERE VTD1_Actual_Visit__c = :visitId
                        ORDER BY CreatedDate DESC
                        LIMIT 1
                ].SessionId__c;
                cr.buildResponse(sessionId);
                return JSON.serialize(cr, true);
            } catch (QueryException queryException) {
                response.statusCode = 500;
                cr.buildResponse('No video session id for this visit.');
                return JSON.serialize(cr, true);
            }


        } else {
            cr.buildResponse('No such method. Try /Info or /Token');
            return JSON.serialize(cr, true);
        }
    }

    private class Token {
        public String message;
    }

    private class Info {
        public List<Participant> participants;
        public String checklist;
    }

    public class Participant {
        public String photoUrl;
        public String userName;
        public String profName;
    }
}