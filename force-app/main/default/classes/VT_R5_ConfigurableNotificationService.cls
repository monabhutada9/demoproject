/**
 * Created by K.Latysh on 22-Oct-20.
 */

public with sharing class VT_R5_ConfigurableNotificationService extends VT_R5_HerokuScheduler.AbstractScheduler implements VT_R5_HerokuScheduler.HerokuExecutable {



    /**
    * Configure notifications for Schedule or to display right now
    * @author Kirill Latysh
    * @param notifications
    * @date 10/26/2020
    * @issue SH-17939
    */
    public void configureNotifications(List<VTR5_StudyTeamNotification__c> notifications){
        List<Id> studyIds = new List <Id>();
        List<VTR5_StudyTeamNotification__c> notificationsForSchedule = new List<VTR5_StudyTeamNotification__c>();
        List<VTR5_StudyTeamNotification__c> herokuSchedulerNotifications = new List<VTR5_StudyTeamNotification__c>();



        for(VTR5_StudyTeamNotification__c notification : notifications){
            if(notification.VTR5_NotificationActive__c == false){
                if(notification.VTR5_ActiveOfSubmitBanner__c == true){
                    herokuSchedulerNotifications.add(notification);
                }


                notificationsForSchedule.add(notification);
                studyIds.add(notification.VTR5_Study__c);
            } else {
                if(notification.VTR5_StartDate__c <= Datetime.now()){


                    notificationsForSchedule.add(notification);
                    herokuSchedulerNotifications.add(notification);
                    studyIds.add(notification.VTR5_Study__c);
                }  else {


                    herokuSchedulerNotifications.add(notification);
                }
            }
        }
        if(!notificationsForSchedule.isEmpty()){

            createNotificationData(notificationsForSchedule, studyIds);

        }
        if(!herokuSchedulerNotifications.isEmpty()){
            scheduleActions(herokuSchedulerNotifications);
        }
    }



    /**
    * Create notifications Data to display it
    * @author Kirill Latysh
    * @param notifications
    * @param studyIds
    * @date 10/26/2020
    * @issue SH-17939
    */
    public void createNotificationData(List<VTR5_StudyTeamNotification__c> notifications, List<Id> studyIds) {
        List<VT_R2_NotificationCHandler.NotificationData> notificationDataList = new List<VT_R2_NotificationCHandler.NotificationData>();
        Map<Id, List<Id>> studyToUserIdsMap = new Map<Id, List<Id>>();
        List<Study_Team_Member__c> stmsList = [SELECT Id, User__c, Study__c FROM Study_Team_Member__c WHERE Study__c IN :studyIds];



        for(Study_Team_Member__c stm : stmsList){
            if(studyToUserIdsMap.containsKey(stm.Study__c)){
                studyToUserIdsMap.get(stm.Study__c).add(stm.User__c);
            } else {
                studyToUserIdsMap.put(stm.Study__c, new List<Id>{stm.User__c});
            }
        }


        for(VTR5_StudyTeamNotification__c notification : notifications){
            List<Id> userIds = studyToUserIdsMap.get(notification.VTR5_Study__c);
            System.debug('userIds= ' + userIds);
            if(userIds.size() > 250){
                List<Id> userIdsForServise = new List<Id>();
                for(Id userId : userIds){
                    userIdsForServise.add(userId);
                    if( userIdsForServise.size() == 250){
                        notificationDataList.add(new VT_R2_NotificationCHandler.NotificationData(notification.Id, JSON.serialize(userIdsForServise)));
                        userIdsForServise = new List<Id>();
                    } 
                }

                if(!userIdsForServise.isEmpty()){
                    notificationDataList.add(new VT_R2_NotificationCHandler.NotificationData(notification.Id, JSON.serialize(userIdsForServise)));
                }

            } else {
                notificationDataList.add(new VT_R2_NotificationCHandler.NotificationData(notification.Id, JSON.serialize(userIds)));
            }
        }


        if (!notificationDataList.isEmpty()){
            VT_R2_NotificationCHandler.sendToNotificationsService(new List<VT_R2_NotificationCHandler.NotificationWrapper>{
                    new VT_R2_NotificationCHandler.NotificationWrapper('ConfigurableNotification', notificationDataList)
            });
        }


    }

    /**
    * Schedule notification to display it in future
    * @author Kirill Latysh
    * @param notifications
    * @date 10/26/2020
    * @issue SH-17939
    */
    public static void scheduleActions(List<VTR5_StudyTeamNotification__c> notifications) {
        System.debug(notifications);
        List<VT_R5_HerokuScheduler.HerokuSchedulerPayload> herokuSchedulerNotifications = new List<VT_R5_HerokuScheduler.HerokuSchedulerPayload>();

        //for active notifications
        for(VTR5_StudyTeamNotification__c notification : notifications) {
            if(notification.VTR5_NotificationActive__c == true){
                herokuSchedulerNotifications.add(new VT_R5_HerokuScheduler.HerokuSchedulerPayload(


                        notification.VTR5_EndDate__c,
                        'VT_R5_ConfigurableNotificationService',
                        JSON.serialize(
                                notification.Id
                        )));

                if (notification.VTR5_StartDate__c > Datetime.now()){

                    herokuSchedulerNotifications.add(new VT_R5_HerokuScheduler.HerokuSchedulerPayload(

                            notification.VTR5_StartDate__c,
                            'VT_R5_ConfigurableNotificationService',
                            JSON.serialize(
                                    notification.Id
                            )));
                }
            } else if(notification.VTR5_ActiveOfSubmitBanner__c == true){

                herokuSchedulerNotifications.add(new VT_R5_HerokuScheduler.HerokuSchedulerPayload(

                        notification.VTR5_EndSubmitDate__c,
                        'VT_R5_ConfigurableNotificationService',
                        JSON.serialize(
                                notification.Id
                        )));
            }


        }
        System.debug('herokuSchedulernotifications=' + herokuSchedulerNotifications);
        if (herokuSchedulerNotifications.size() > 0) {
            System.enqueueJob(new VT_R5_asincHeroku(herokuSchedulerNotifications));
        }
    }

    /**
    * Receive Data from Heroku Scheduler and process it to update or display
    * we get List JSON Objects
    * @author Kirill Latysh
    * @param payload
    * @date 10/26/2020
    * @issue SH-17939
    */
    public void herokuExecute(String payload) {


        List<Id> studyIds = new List <Id>();
        Datetime datetimeNow = Datetime.now();
        Set <VTR5_StudyTeamNotification__c> notificationsForUpdate = new Set<VTR5_StudyTeamNotification__c>();
        List <VTR5_StudyTeamNotification__c> notificationsForSchedule = new List<VTR5_StudyTeamNotification__c>();
        List<Id> notificationIds = (List<Id>) JSON.deserialize(payload, List<Id>.class);
        List <VTR5_StudyTeamNotification__c> notifications = [SELECT VTR5_EndDate__c,
                                                                    VTR5_NotificationActive__c,
                                                                    VTR5_ActiveOfSubmitBanner__c,
                                                                    VTR5_Study__c,
                                                                    VTR5_EndSubmitDate__c,
                                                                    VTR5_DateOfDeactivation__c,
                                                                    VTR5_StartDate__c,
                                                                    VTR5_DurationSubmitMessage__c
                                                              FROM VTR5_StudyTeamNotification__c
                                                              WHERE Id IN :notificationIds];


        if (!notifications.isEmpty()){

            for(VTR5_StudyTeamNotification__c notification : notifications){
                 if (notification.VTR5_EndSubmitDate__c > datetimeNow) {
                    System.debug('VTR5_EndSubmitDate__c > now ');
                    notificationsForSchedule.add(notification);
                    studyIds.add(notification.VTR5_Study__c);
                } else if(notification.VTR5_EndSubmitDate__c <= datetimeNow) {
                    System.debug('VTR5_EndSubmitDate__c < now ');
                    notification.VTR5_ActiveOfSubmitBanner__c =false;
                    notificationsForUpdate.add(notification);


                } else  if(notification.VTR5_EndDate__c > datetimeNow){
                    System.debug('VTR5_EndDate__c > now ');
                    notificationsForSchedule.add(notification);
                    studyIds.add(notification.VTR5_Study__c);
                } else  if(notification.VTR5_EndDate__c <= datetimeNow){
                    System.debug('VTR5_EndDate__c < now ');


                    notification.VTR5_NotificationActive__c =false;
                    notificationsForUpdate.add(notification);
                }

            }
            System.debug('notificationsForUpdate= ' + notificationsForUpdate);
            if(!notificationsForUpdate.isEmpty()){
                update new List<VTR5_StudyTeamNotification__c>(notificationsForUpdate);
            }


            createNotificationData(notificationsForSchedule, studyIds);
        }
    }

    /**
    * Class for async execute scheduled notifications
    * @author Kirill Latysh
    * @date 10/26/2020
    * @issue SH-17939
    */


    public with sharing class VT_R5_asincHeroku implements Queueable, Database.AllowsCallouts {
        private List<VT_R5_HerokuScheduler.HerokuSchedulerPayload> notifications;

        public VT_R5_asincHeroku(List<VT_R5_HerokuScheduler.HerokuSchedulerPayload> notifications) {
            this.notifications = new List<VT_R5_HerokuScheduler.HerokuSchedulerPayload>(notifications);
        }

        public void execute(System.QueueableContext qc) {
            (new VT_R5_ConfigurableNotificationService()).schedule(notifications);
        }
    }
}