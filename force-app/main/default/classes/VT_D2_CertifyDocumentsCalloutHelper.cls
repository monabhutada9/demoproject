/**
* @author: Carl Judge
* @date: 11-Oct-18
* @description: Used by VT_D2_QAction_CertifyDocuments to send documents to docusign
**/

public class VT_D2_CertifyDocumentsCalloutHelper {

    public static VT_D1_HTTPConnectionHandler.Result sendCertifyDocument(Id docId) {
        VT_D2_DocuSignAuthHelper.Result tokenResponse = VT_D2_DocuSignAuthHelper.getAuthToken();
        if (tokenResponse.value == null) {
            insert tokenResponse.calloutResult.log;
            return tokenResponse.calloutResult;
        }

        VT_D2_DocuSignAuthHelper.Result baseUriResponse = VT_D2_DocuSignAuthHelper.getBaseUri(tokenResponse.value);
        if (baseUriResponse.value == null) {
            insert baseUriResponse.calloutResult.log;
            return baseUriResponse.calloutResult;
        }

        String method = 'POST';
        String endpointURL = baseUriResponse.value + '/envelopes';
        VT_D1_RequestBuilder requestBuilder = new VT_D2_RequestBuilder_DocuSign(docId);
        String action = 'Send DocuSign Envelope';
        Map<String, String> headerMap = new Map<String, String>{
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' + tokenResponse.value
        };

        VT_D1_HTTPConnectionHandler.Result certifyResult = VT_D1_HTTPConnectionHandler.send(method, endpointURL, requestBuilder, action, headerMap);
        return certifyResult;
    }
}