@IsTest
private class VT_D2_RequestBuilder_SendSubjectTest {
    @TestSetup
    static void setup() {
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        Test.startTest();
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }

    @IsTest
    static void setRegDocTemplateWithTMA() {
        List<Case> casesList = [SELECT Id, VTD1_Patient_User__c, VTD1_Study__c, ContactId, VTD1_Study__r.Name,
            VTD1_Subject_ID__c, VTD1_PI_user__r.VTD1_CTMS_Row_ID__c, Account.VTD1_LegalFirstName__c,
            Account.VTD1_LastName_del__c, Status, VTD1_Patient_User__r.LanguageLocaleKey,
            Contact.Email, Contact.Name, Contact.LastName
        FROM Case];
        System.assert(casesList.size() > 0);
        Test.startTest();
        if (casesList.size() > 0) {
            Case cas = casesList.get(0);

            List<VTD1_Study_Stratification__c> studyStratifications = new List<VTD1_Study_Stratification__c>();

            VTD1_Study_Stratification__c studyStratification1 = new VTD1_Study_Stratification__c();
            studyStratification1.VTD1_Study__c = cas.VTD1_Study__c;
            studyStratification1.Name = 'str1';
            studyStratifications.add(studyStratification1);

            VTD1_Study_Stratification__c studyStratification2 = new VTD1_Study_Stratification__c();
            studyStratification2.VTD1_Study__c = cas.VTD1_Study__c;
            studyStratification2.Name = 'str2';
            studyStratifications.add(studyStratification2);

            insert studyStratifications;

            List<VTD1_Patient_Stratification__c> patientStratifications = new List<VTD1_Patient_Stratification__c>();

            VTD1_Patient_Stratification__c patientStratification1 = new VTD1_Patient_Stratification__c();
            patientStratification1.VTD1_Patient__c = cas.Id;
            patientStratification1.VTD1_Study_Stratification__c = studyStratification1.Id;
            patientStratification1.Value__c = 'val1';
            patientStratifications.add(patientStratification1);

            VTD1_Patient_Stratification__c patientStratification2 = new VTD1_Patient_Stratification__c();
            patientStratification2.VTD1_Patient__c = cas.Id;
            patientStratification2.VTD1_Study_Stratification__c = studyStratification2.Id;
            patientStratification2.Value__c = 'val2';
            patientStratifications.add(patientStratification2);

            insert patientStratifications;

            Address__c address = new Address__c();
            address.VTD1_Contact__c = cas.ContactId;
            address.VTD1_AddressType__c = 'Home';
            address.Name = 'Address1';
            address.VTD1_Address2__c = 'Address2';
            address.City__c = 'City';
            address.State__c  = 'TN';
            address.ZipCode__c  = '12345';
            address.Country__c = 'US';

            insert address;

            List<Address__c> oldPrimaryAddress = [SELECT Id FROM Address__c WHERE VTD1_Contact__c = :cas.ContactId AND VTD1_Primary__c = TRUE];
            if (!oldPrimaryAddress.isEmpty()) {
                oldPrimaryAddress[0].VTD1_Primary__c = false;
                address.VTD1_Primary__c = true;
                update new List<Address__c>{oldPrimaryAddress[0], address};
            }

            List<VT_D1_Phone__c> phoneList = [SELECT Id FROM VT_D1_Phone__c];
            delete phoneList;

            List<VT_D1_Phone__c> phones = new List<VT_D1_Phone__c>();

            VT_D1_Phone__c phone1 = new VT_D1_Phone__c();
            phone1.VTD1_Contact__c = cas.ContactId;
            phone1.Account__c = cas.AccountId;
            phone1.IsPrimaryForPhone__c = true;
            phone1.PhoneNumber__c = '11';
            phone1.Type__c = 'Mobile';
            phones.add(phone1);

            VT_D1_Phone__c phone2 = new VT_D1_Phone__c();
            phone2.VTD1_Contact__c = cas.ContactId;
            phone2.Account__c = cas.AccountId;
            phone2.PhoneNumber__c = '22';
            phone2.Type__c = 'Home';
            phones.add(phone2);

            insert phones;

            Contact contact = cas.Contact;
            contact.RecordTypeId = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_CAREGIVER;
            contact.Birthdate = Date.newInstance(1960, 2, 17);
            contact.VTD1_EmailNotificationsFromCourier__c = false;
            contact.VTD1_TextMessageCommunicationFromCourier__c = true;
            contact.VTD1_PhoneCommunicationFromCourier__c = true;
            contact.VT_D1_DeliveryTimeMondayFrom__c = '18:00';
            contact.VT_D1_DeliveryTimeMondayTo__c = '19:00';
            contact.VT_D1_DeliveryTimeTuesdayFrom__c = '08:00';
            contact.VT_D1_DeliveryTimeTuesdayTo__c = '09:00';
            contact.VT_D1_DeliveryTimeWednesdayFrom__c = '10:00';
            contact.VT_D1_DeliveryTimeWednesdayTo__c = '11:00';
            contact.VT_D1_DeliveryTimeThursdayFrom__c = '13:00';
            contact.VT_D1_DeliveryTimeThursdayTo__c = '20:00';
            contact.VT_D1_DeliveryTimeFridayFrom__c = '14:00';
            contact.VT_D1_DeliveryTimeFridayTo__c = '15:00';
            contact.VT_D1_DeliveryTimeSaturdayFrom__c = '16:00';
            contact.VT_D1_DeliveryTimeSaturdayTo__c = '17:00';
            update contact;

            VT_D1_RequestBuilder_SendSubject requestBuilderSendSubject = new VT_D1_RequestBuilder_SendSubject(cas.Id);
            String requestBodyString = requestBuilderSendSubject.buildRequestBody();
            VT_D1_RequestBuilder_SendSubject.Subject subject = (VT_D1_RequestBuilder_SendSubject.Subject) JSON.deserialize(requestBodyString, VT_D1_RequestBuilder_SendSubject.Subject.class);
            System.assertEquals(cas.VTD1_Study__r.Name, subject.protocolId);
            System.assertEquals(cas.VTD1_Subject_ID__c, subject.subjectId);
            System.assertEquals(cas.VTD1_PI_user__r.VTD1_CTMS_Row_ID__c, subject.investigatorId);

            VT_D1_RequestBuilder_SendSubject.AuditLog subjectAuditLog = subject.auditLog;
            System.assertNotEquals(null, subjectAuditLog);
            if(subjectAuditLog != null){
                System.assertNotEquals(null, subjectAuditLog.Timestamp);
                System.assertEquals(UserInfo.getUserId(), subjectAuditLog.UserId);
            }

            VT_D1_RequestBuilder_SendSubject.SubjectDetails subjectDetails = subject.subjectDetails;
            System.assertNotEquals(null, subjectDetails);
            if(subjectDetails != null){
                System.assertEquals(cas.Account.VTD1_LegalFirstName__c, subjectDetails.FirstName);
                System.assertEquals(cas.Account.VTD1_LastName_del__c, subjectDetails.LastName);
                System.assertEquals(cas.Status, subjectDetails.Status);
                System.assertEquals(Date.newInstance(1960, 2, 17), subjectDetails.Birthdate);
                System.assertEquals(cas.VTD1_Patient_User__r.LanguageLocaleKey.substring(0,2).toUpperCase(), subjectDetails.LanguageId);
                System.assertEquals(cas.Contact.Email, subjectDetails.EMail);
                System.assertEquals(false, subjectDetails.EmailNotifications);
                System.assertEquals(true, subjectDetails.TextMessageCommunications);
                System.assertEquals(true, subjectDetails.PhoneCommunications);

                List<VT_D1_RequestBuilder_SendSubject.SubjectAddress> subjectAddressList = subjectDetails.Addresses;
                System.assertNotEquals(null, subjectAddressList);
                if (subjectAddressList != null) {
                    System.assertEquals(1, subjectAddressList.size());
                    if(subjectAddressList.size() > 0){
                        VT_D1_RequestBuilder_SendSubject.SubjectAddress subjectAddress = subjectAddressList.get(0);
                        System.assertEquals('Residential', subjectAddress.Type);
                        System.assertEquals('Address1', subjectAddress.Address1);
                        System.assertEquals('Address2', subjectAddress.Address2);
                        System.assertEquals('City', subjectAddress.City);
                        System.assertEquals('TN', subjectAddress.State);
                        System.assertEquals('12345', subjectAddress.PostalCode);
                        System.assertEquals('US', subjectAddress.Country);
                        VT_D1_RequestBuilder_SendSubject.SubjectCaregiver subjectAddressCaregiver = subjectAddress.Caregiver;
                        System.assertNotEquals(null, subjectAddressCaregiver);
                        if(subjectAddressCaregiver != null){
                            System.assertEquals(cas.Contact.Name, subjectAddressCaregiver.FirstName);
                            System.assertEquals(cas.Contact.LastName, subjectAddressCaregiver.LastName);
                            System.assertEquals(cas.Contact.Email, subjectAddressCaregiver.EMail);
                            System.assertEquals('11', subjectAddressCaregiver.CellPhone);
                            System.assertEquals('22', subjectAddressCaregiver.Phone);
                        }

                        List<VT_D1_RequestBuilder_SendSubject.SubjectAvailability> subjectAvailabilityList = subjectAddress.Availability;
                        System.assertNotEquals(null, subjectAvailabilityList);
                        if (subjectAvailabilityList != null) {
                            System.assertEquals(6, subjectAvailabilityList.size());

                            VT_D1_RequestBuilder_SendSubject.SubjectAvailability subjectAvailabilityMonday;
                            VT_D1_RequestBuilder_SendSubject.SubjectAvailability subjectAvailabilityTuesday;
                            VT_D1_RequestBuilder_SendSubject.SubjectAvailability subjectAvailabilityWednesday;
                            VT_D1_RequestBuilder_SendSubject.SubjectAvailability subjectAvailabilityThursday;
                            VT_D1_RequestBuilder_SendSubject.SubjectAvailability subjectAvailabilityFriday;
                            VT_D1_RequestBuilder_SendSubject.SubjectAvailability subjectAvailabilitySaturday;
                            for(VT_D1_RequestBuilder_SendSubject.SubjectAvailability subjectAvailability : subjectAvailabilityList){
                                if(subjectAvailability.Rule == 'Available' && subjectAvailability.Type == 'Days'){
                                    if(subjectAvailability.DayOfWeek == 'Monday'){
                                        subjectAvailabilityMonday = subjectAvailability;
                                    }
                                    if(subjectAvailability.DayOfWeek == 'Tuesday'){
                                        subjectAvailabilityTuesday = subjectAvailability;
                                    }
                                    if(subjectAvailability.DayOfWeek == 'Wednesday'){
                                        subjectAvailabilityWednesday = subjectAvailability;
                                    }
                                    if(subjectAvailability.DayOfWeek == 'Thursday'){
                                        subjectAvailabilityThursday = subjectAvailability;
                                    }
                                    if(subjectAvailability.DayOfWeek == 'Friday'){
                                        subjectAvailabilityFriday = subjectAvailability;
                                    }
                                    if(subjectAvailability.DayOfWeek == 'Saturday'){
                                        subjectAvailabilitySaturday = subjectAvailability;
                                    }
                                }
                            }
                            System.assertNotEquals(null, subjectAvailabilityMonday);
                            if (subjectAvailabilityMonday != null){
                                System.assertEquals('18:00', subjectAvailabilityMonday.StartHour);
                                System.assertEquals('19:00', subjectAvailabilityMonday.EndHour);
                            }
                            System.assertNotEquals(null, subjectAvailabilityTuesday);
                            if (subjectAvailabilityTuesday != null){
                                System.assertEquals('08:00', subjectAvailabilityTuesday.StartHour);
                                System.assertEquals('09:00', subjectAvailabilityTuesday.EndHour);
                            }
                            System.assertNotEquals(null, subjectAvailabilityWednesday);
                            if (subjectAvailabilityWednesday != null){
                                System.assertEquals('10:00', subjectAvailabilityWednesday.StartHour);
                                System.assertEquals('11:00', subjectAvailabilityWednesday.EndHour);
                            }
                            System.assertNotEquals(null, subjectAvailabilityThursday);
                            if (subjectAvailabilityThursday != null){
                                System.assertEquals('13:00', subjectAvailabilityThursday.StartHour);
                                System.assertEquals('20:00', subjectAvailabilityThursday.EndHour);
                            }
                            System.assertNotEquals(null, subjectAvailabilityFriday);
                            if (subjectAvailabilityFriday != null){
                                System.assertEquals('14:00', subjectAvailabilityFriday.StartHour);
                                System.assertEquals('15:00', subjectAvailabilityFriday.EndHour);
                            }
                            System.assertNotEquals(null, subjectAvailabilitySaturday);
                            if (subjectAvailabilitySaturday != null){
                                System.assertEquals('16:00', subjectAvailabilitySaturday.StartHour);
                                System.assertEquals('17:00', subjectAvailabilitySaturday.EndHour);
                            }
                        }
                    }
                }

                System.assertNotEquals(null, subjectDetails.Demographics);
                if (subjectDetails.Demographics != null) {
                    System.assertEquals(2, subjectDetails.Demographics.size());

                    if (subjectDetails.Demographics.size() > 1) {
                        String subjectStratificationString1 = subjectDetails.Demographics.get(0).toString();
                        System.assertNotEquals(null, subjectStratificationString1);
                        String subjectStratificationString2 = subjectDetails.Demographics.get(1).toString();
                        System.assertNotEquals(null, subjectStratificationString2);

                        String subjectStratificationStringForKey1;
                        String subjectStratificationStringForKey2;
                        if(subjectStratificationString1 == 'Stratification:[Key=str1, Value=val1]') {
                            subjectStratificationStringForKey1 = subjectStratificationString1;
                        } else if (subjectStratificationString2 == 'Stratification:[Key=str1, Value=val1]') {
                            subjectStratificationStringForKey1 = subjectStratificationString2;
                        }
                        if(subjectStratificationString2 == 'Stratification:[Key=str2, Value=val2]') {
                            subjectStratificationStringForKey2 = subjectStratificationString2;
                        } else if(subjectStratificationString1 == 'Stratification:[Key=str2, Value=val2]'){
                            subjectStratificationStringForKey2 = subjectStratificationString1;
                        }
                        System.assertEquals('Stratification:[Key=str1, Value=val1]', subjectStratificationStringForKey1);
                        System.assertEquals('Stratification:[Key=str2, Value=val2]', subjectStratificationStringForKey2);
                    }
                }
            }

            Contact contact1 = [SELECT Id FROM Contact WHERE Id =: cas.ContactId];
            contact1.RecordTypeId = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PATIENT;
            update contact1;
            VT_D1_RequestBuilder_SendSubject requestBuilderSendSubject1 = new VT_D1_RequestBuilder_SendSubject(cas.Id);
            String requestBodyString1 = requestBuilderSendSubject1.buildRequestBody();
            VT_D1_RequestBuilder_SendSubject.Subject subject1 = (VT_D1_RequestBuilder_SendSubject.Subject) JSON.deserialize(requestBodyString1, VT_D1_RequestBuilder_SendSubject.Subject.class);
            VT_D1_RequestBuilder_SendSubject.SubjectDetails subjectDetails1 = subject1.subjectDetails;
            System.assertNotEquals(null, subjectDetails1);
            if (subjectDetails1 != null){
                System.assertEquals('11', subjectDetails1.Phone1);
                System.assertEquals('Mobile', subjectDetails1.TypePhone1);
                System.assertEquals('22', subjectDetails1.Phone2);
                System.assertEquals('Home', subjectDetails1.TypePhone2);
            }
            Test.stopTest();
        }
    }
}