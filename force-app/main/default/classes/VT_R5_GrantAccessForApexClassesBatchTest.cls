/**
 * Created by Alexey Mezentsev on 9/28/2020.
 */

@IsTest
private class VT_R5_GrantAccessForApexClassesBatchTest {
    @TestSetup
    static void setup() {
        Test.startTest();
        User newUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setEmail('test@test2020.test')
                .setProfile('Site Coordinator').persist();
        Test.stopTest();
    }

    @IsTest
    static void testBehavior() {
        User newUser = [SELECT Id FROM User WHERE Email = 'test@test2020.test'];
        Id apexAccessPermissionSetId = [SELECT Id FROM PermissionSet WHERE Name = 'UserApexAccess'].Id;
        List<PermissionSetAssignment> permissionAssignment = [
                SELECT Id
                FROM PermissionSetAssignment
                WHERE PermissionSetId = :apexAccessPermissionSetId AND AssigneeId = :newUser.Id];
        System.assertEquals(1, permissionAssignment.size());

        delete permissionAssignment;

        permissionAssignment = [
                SELECT Id
                FROM PermissionSetAssignment
                WHERE PermissionSetId = :apexAccessPermissionSetId AND AssigneeId = :newUser.Id];
        System.assertEquals(0, permissionAssignment.size());

        Test.startTest();
        Database.executeBatch(new VT_R5_GrantAccessForApexClassesBatch());
        Test.stopTest();

        permissionAssignment = [
                SELECT Id
                FROM PermissionSetAssignment
                WHERE PermissionSetId = :apexAccessPermissionSetId AND AssigneeId = :newUser.Id];
        System.assertEquals(1, permissionAssignment.size());
    }
}