@IsTest
private class VT_D1_PIMyPatientsStatusBarTest {
    @testSetup
    static void setup() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study1 = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
        Case cas = [SELECT Id, VTD1_PI_user__c, VTD1_Primary_PG__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];
        insert new CaseShare(CaseId = cas.Id, CaseAccessLevel = 'Edit', UserOrGroupId = cas.VTD1_PI_user__c);
        insert new CaseShare(CaseId = cas.Id, CaseAccessLevel = 'Edit', UserOrGroupId = cas.VTD1_Primary_PG__c);
    }

    @IsTest
    static void getStatusBarTest() {
        User userPI;

        List<HealthCloudGA__CarePlanTemplate__c> studyList = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];
        Test.startTest();
        System.assert(studyList.size() > 0);
        String studyId = studyList.get(0).Id;

        List<Case> casesList = [SELECT Status, VTD1_PI_user__c, VTD1_Primary_PG__c FROM Case WHERE VTD1_Study__c =: studyId];
        if(casesList.size() > 0){
            Case cas1 = casesList.get(0);
            cas1.Status = 'Does Not Qualify';
            User pgToRun = [SELECT Id FROM User WHERE Id =:cas1.VTD1_Primary_PG__c];
            System.runAs(pgToRun) {
                update cas1;
            }

            String userId = cas1.VTD1_PI_user__c;
            userPI = [SELECT Id FROM User WHERE Id =: userId];

        }
        System.runAs(userPI){
            System.debug('PI = ' + userPI);
            List <Case> cases = [select Id, Status, VTD1_PI_user__c, VTD1_Backup_PI_User__c from Case];
            for (Case caseObj : cases) {
                System.debug('caseObj = ' + caseObj);
            }
            String patientStatusBarString = VT_D1_PIMyPatientsStatusBarController.getStatusBar(studyId);
            VT_D1_PIMyPatientsStatusBarController.PatientStatusBar patientStatusBar = (VT_D1_PIMyPatientsStatusBarController.PatientStatusBar) JSON.deserialize(patientStatusBarString,  VT_D1_PIMyPatientsStatusBarController.PatientStatusBar.class);

            System.assertEquals(1, patientStatusBar.quantityNotActivePatientStatusList);
            System.assertEquals(1, patientStatusBar.notActivePatientStatusList.size());

            VT_D1_PIMyPatientsStatusBarController.PatientStatusItem closedPatientStatusItem = patientStatusBar.notActivePatientStatusList.get(0);
            System.assertEquals('Does Not Qualify', closedPatientStatusItem.status);
            System.assertEquals(1, closedPatientStatusItem.quantity);
            System.assertEquals(100, closedPatientStatusItem.percentageOfTotal);
        }
        Test.stopTest();
    }

    @IsTest
    static void getStatusBarTest2() {
        User userPI;

        List<HealthCloudGA__CarePlanTemplate__c> studyList = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];
        Test.startTest();
        System.assert(studyList.size() > 0);
        String studyId = studyList.get(0).Id;

        List<Case> casesList = [SELECT Status, VTD1_PI_user__c FROM Case WHERE VTD1_Study__c =: studyId];
        if(casesList.size() > 0){
            Case cas2 = casesList.get(0);
            cas2.Status = 'Pre-Consent';
            update cas2;

            String userId = cas2.VTD1_PI_user__c;
            userPI = [SELECT Id FROM User WHERE Id =: userId];
        }
        System.runAs(userPI){
            String patientStatusBarString = VT_D1_PIMyPatientsStatusBarController.getStatusBar(studyId);
            VT_D1_PIMyPatientsStatusBarController.PatientStatusBar patientStatusBar = (VT_D1_PIMyPatientsStatusBarController.PatientStatusBar) JSON.deserialize(patientStatusBarString,  VT_D1_PIMyPatientsStatusBarController.PatientStatusBar.class);

            System.assertEquals(1, patientStatusBar.quantityActivePatientStatusList);
            System.assertEquals(1, patientStatusBar.activePatientStatusList.size());

            VT_D1_PIMyPatientsStatusBarController.PatientStatusItem openPatientStatusItem = patientStatusBar.activePatientStatusList.get(0);
            System.assertEquals('Pre-Consent', openPatientStatusItem.status);
            System.assertEquals(1, openPatientStatusItem.quantity);
            System.assertEquals(100, openPatientStatusItem.percentageOfTotal);
        }
        Test.stopTest();
    }
}