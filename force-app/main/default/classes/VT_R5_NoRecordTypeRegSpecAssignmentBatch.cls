/**
 * @author: Stanislav Frolov
 * @date: 29.04.2020
 * @description:
*/
public with sharing class VT_R5_NoRecordTypeRegSpecAssignmentBatch implements Database.Batchable<sObject>, Database.Stateful {

    private static final Id RS_REC_TYPE_ID = Schema.SObjectType.VTR3_Geography_STM__c.getRecordTypeInfosByDeveloperName().get('RS').getRecordTypeId();

    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query = '';
        if (!test.isRunningTest()) {
            query = 'SELECT Id,RecordTypeId FROM VTR3_Geography_STM__c  WHERE RecordTypeId = Null';
        } else {
            query = 'SELECT Id,RecordTypeId FROM VTR3_Geography_STM__c';
        }
        return Database.getQueryLocator(query);

    }
    public void execute(Database.BatchableContext bc, List<VTR3_Geography_STM__c> scope) {
        List<VTR3_Geography_STM__c> geographySTMS = new List<VTR3_Geography_STM__c>();
        for (VTR3_Geography_STM__c stm : scope) {
            stm.RecordTypeId = RS_REC_TYPE_ID;
            geographySTMS.add(stm);
        }
        update geographySTMS;
    }
    public void finish(Database.BatchableContext bc) {

    }

}