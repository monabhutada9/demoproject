/**
* @author: Carl Judge
* @date: 14-Sep-18
* @description: 
**/

public class VT_D2_VisitRequestedTemplate_ListItem {
    public String imageUrl {get; set;}
    public String title {get; set;}
    public String subTitle {get; set;}

    public VT_D2_VisitRequestedTemplate_ListItem(Id imageId, String title, String subTitle) {
        this.imageUrl = VisitRequestConfirmSite__c.getInstance().SalesforceBaseURL__c
            + '/servlet/servlet.ImageServer?id=' + imageId + '&oid=' + UserInfo.getOrganizationId();
        this.title = title;
        this.subTitle = subTitle;
    }
}