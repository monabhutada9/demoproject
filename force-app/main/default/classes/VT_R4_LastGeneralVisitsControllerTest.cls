/**
 * Created by alexa on 16.04.2020.
 */

@isTest
public with sharing class VT_R4_LastGeneralVisitsControllerTest {

    @TestSetup
    static void testData(){
        VTR4_General_Visit__c testItem = new VTR4_General_Visit__c();
        testItem.Name = 'testVisit';
        testItem.VTR4_Duration__c = 123;
        testItem.VTR4_scheduled_Date_Time__c = System.today();
        insert testItem;
    }

    @isTest
    static void getGeneralVisit() {
        List<VTR4_General_Visit__c> generalVisits = new List<VTR4_General_Visit__c>();
        generalVisits = VT_R4_LastGeneralVisitsController.getRecentlyVieweds();
        System.assertEquals(1, generalVisits.size());
    }
}