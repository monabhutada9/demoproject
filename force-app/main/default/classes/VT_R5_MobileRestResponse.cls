/**
 * @author: Alexander Komarov
 * @date: 16.04.2020
 * @description: virtual class for responses for mobile app, extendable by particular response realization
 */
public with sharing virtual class VT_R5_MobileRestResponse {
    private Object data;
    private String bannerMessage;
    public List<String> errors = new List<String>();
    private AdditionalInfo additionalInfo;
    private Boolean errorBanner;
    private Boolean successBanner;
    public RestRequest request = RestContext.request;
    public virtual VT_R5_MobileRestResponse buildResponseWOAI(Object response) {
        this.data = response;
        return this;
    }
    public virtual VT_R5_MobileRestResponse buildResponseWOAI(Object response, String message) {
        this.data = response;
        this.bannerMessage = message;
        return this;
    }
    public virtual VT_R5_MobileRestResponse buildResponse(Exception e) {
        this.bannerMessage = ('Internal server error. Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName() + '.');
        this.showErrorBanner();
        RestContext.response.statusCode = 500;
        return this;
    }
    public virtual VT_R5_MobileRestResponse buildResponseWOAI(Exception e) {
        this.bannerMessage = ('Internal server error. Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName() + '.');
        this.showErrorBanner();
        RestContext.response.statusCode = 500;
        return this;
    }
    public virtual VT_R5_MobileRestResponse buildResponse(Object response) {
        this.data = response;
        this.additionalInfo = new AdditionalInfo();
        return this;
    }
    public virtual VT_R5_MobileRestResponse buildResponse() {
        this.additionalInfo = new AdditionalInfo();
        return this;
    }
    public virtual VT_R5_MobileRestResponse buildResponse(Object response, String message) {
        this.data = response;
        this.bannerMessage = message;
        this.additionalInfo = new AdditionalInfo();
        return this;
    }
    public class AdditionalInfo {
        public Integer unreadConversationsCount;
        public Integer unreadNotificationsCount;
        public String userLanguage;
        public String userSecondaryLanguage;
        public String userTertiaryLanguage;
        AdditionalInfo() {
            List<Contact> contacts = [SELECT Id, VTR2_Primary_Language__c, VT_R5_Tertiary_Preferred_Language__c, VT_R5_Secondary_Preferred_Language__c FROM Contact WHERE Id IN (SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId())];
            if (!contacts.isEmpty()) {
                this.userLanguage = contacts[0].VTR2_Primary_Language__c;
                this.userSecondaryLanguage = contacts[0].VT_R5_Secondary_Preferred_Language__c;
                this.userTertiaryLanguage = contacts[0].VT_R5_Tertiary_Preferred_Language__c;
            }
            this.unreadConversationsCount = VT_R3_RestPeriodic.getUnreadConversationsCount();
            this.unreadNotificationsCount = VT_R3_RestPeriodic.getUnreadNotificationsCount();
        }
    }
    public virtual Boolean processDatabaseResults(Database.SaveResult[] updateResult) {
        Boolean hasError = false;
        for (Database.SaveResult sr : updateResult) {
            if (!sr.isSuccess()) {
                hasError = true;
                for (Database.Error err : sr.getErrors()) {
                    addError('SObject Id - ' + sr.getId() + ' update failed ' + err.message + ' ' + err.fields);
                }
            }
        }
        return hasError;
    }
    public virtual Boolean processDatabaseResults(Database.DeleteResult[] deleteResult) {
        Boolean hasError = false;
        for (Database.DeleteResult dr : deleteResult) {
            if (!dr.isSuccess()) {
                hasError = true;
                for (Database.Error err : dr.getErrors()) {
                    addError('SObject Id - ' + dr.getId() + ' update failed ' + err.message + ' ' + err.fields);
                }
            }
        }
        return hasError;
    }
    public virtual void showSuccessBanner(){
        this.successBanner = true;
    }
    public virtual void showErrorBanner(){
        this.errorBanner = true;
    }
    public virtual void addBannerMessage(String bannerMessage) {
        this.bannerMessage = bannerMessage;
    }
    public virtual void addError(String error) {
        this.errors.add(error);
    }
    public virtual void sendResponse(Integer code, String error) {
        this.bannerMessage = error;
        this.errors.add(error);
        sendResponse(code);
    }
    public virtual void sendResponse(Integer code) {
        RestContext.response.statusCode = code;
        sendResponse();
    }
    public virtual void sendResponse() {
        if (this.errors != null && this.errors.isEmpty()) this.errors = null;
        if (this.request != null) this.request = null;
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(this, true));
    }
}