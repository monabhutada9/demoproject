@IsTest
public class VTR2_SCRLookupControllerTest {

    public static void testFetchAccount() {
        Test.startTest();
        List<SObject> result = VTR2_SCRLookupController.fetchAccount('', 'tN');
        Test.stopTest();

        System.assert(!result.isEmpty());
    }

    public static void testGetNameById() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT id FROM HealthCloudGA__CarePlanTemplate__c study LIMIT 1];
        Test.startTest();
        List<SObject> result = VTR2_SCRLookupController.getNameById('', study.Id, '');
        Test.stopTest();

        System.assert(!result.isEmpty());
    }
}