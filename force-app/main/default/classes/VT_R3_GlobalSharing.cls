/**
* @author: Carl Judge
* @date: 15-Aug-19
* @description: Class to unify Apex sharing in the system. To calculate sharing for any group of record or record Ids, use this class.
**/

public without sharing class VT_R3_GlobalSharing {
    /**
     *  The chained queueable pattern of calculating sharing presents a problem for test classes, as any more than 1 queueable in a chain will
     *  thrown a max queue depth exception. So, if we are in a test class then by default, additional queueables in the chain will
     *  just execute in the same context instead of launching another queueable.
     *  This presents another problem as we can easily hit limits this way. For example if test setup requires separate inserts of STMs
     *  which would trigger multiple sharing calculations. To get round this, if all else fails, it's possible to skip execution of these
     *  queueables in a test class by setting the static variable below to true.
     */
    public static Boolean disableForTest = true;

    /**
     * If set to true, sharing will run synchronously instead of in a queueable context
     */
    public static Boolean doSynchronous = false;

    public static Boolean testBatchError = false;

    /******* DEPRECATED - Use VT_R5_SharingChainFactory.createSharingQueueable directly ************/
    /**
     * Invoke sharing calculation for some records.
     *
     * @param recordIdList - A list of lists of record IDs. Each list should be IDs of the same object type.
     * @param includedUserIds - If not null, sharing will only be recalculated for these users
     */
    public static void doSharing(List<Id> recordIdList, Set<Id> includedUserIds) {
        if (Test.isRunningTest() && disableForTest) {
            return;
        }

        if (recordIdList != null && !recordIdList.isEmpty()) {
            VT_R3_AbstractChainableQueueable sharingQueueable = VT_R5_SharingChainFactory.createSharingQueueable(
                new Set<Id>(recordIdList),
                includedUserIds,
                null,
                recordIdList[0].getSobjectType()
            );

            if (sharingQueueable != null) {
                sharingQueueable.enqueue();
            }

        }
    }
    public static void doSharing (Id recordId) {
        doSharing(new List<Id>{recordId}, null);
    }
    public static void doSharing(List<Id> recordIds) {
        doSharing(recordIds, null);
    }
    public static void doSharing(Set<Id> recordIds) {
        doSharing(new List<Id>(recordIds), null);
    }
    public static void doSharing(Set<Id> recordIds, Set<Id> includedUserIds) {
        doSharing(new List<Id>(recordIds), includedUserIds);
    }
    public static void doSharing (SObject record) {
        doSharing(new List<Id>{record.Id}, null);
    }
    public static void doSharing(List<SObject> records) {
        doSharing(VT_D1_HelperClass.getIdsFromObjs(records), null);
    }
    public static void doSharing(List<SObject> records, Set<Id> includedUserIds) {
        doSharing(VT_D1_HelperClass.getIdsFromObjs(records), includedUserIds);
    }

    @InvocableMethod(Label='Global Sharing')
    public static void doSharing(List<ParamsHolder> params) {

        VT_R3_AbstractChainableQueueable chainQueueable = VT_R5_SharingChainFactory.createFromInvocableMethod(params);

        if (chainQueueable != null) {
            chainQueueable.enqueue();
        }
    }

    public class ParamsHolder {
        @InvocableVariable(Label = 'Record ID' Required = true)
        public Id recordId;
    }
}