/**
 * Created by Eugene Pavlovskiy on 27-Jul-20.
 */

public without sharing class VT_R5_SignantFileTriggerHandler {
    /**
     * @author  Eugene Pavlovskiy
     * @date    28-Jul-20
     * @param newSignantFiles trigger.oldMap
     * @param oldSignantFiles trigger.newMap
     * @description  http://jira.quintiles.net/browse/SH-14316
     *   On SignantFile beforeUpdate if Comment field changed
     *   set CommentBy (current user) and CommentDate (current time)
     */
    public static void beforeUpdateComment(Map <Id, VTR5_Signant_File__c> newSignantFiles, Map <Id, VTR5_Signant_File__c> oldSignantFiles) {
        for (Id recordId : newSignantFiles.keySet()) {
            if (oldSignantFiles.containsKey(recordId)) {
                if (newSignantFiles.get(recordId).VTR5_Comment__c != oldSignantFiles.get(recordId).VTR5_Comment__c) {
                    newSignantFiles.get(recordId).VTR5_CommentBy__c = UserInfo.getUserId();
                    newSignantFiles.get(recordId).VTR5_CommentDate__c = Datetime.now();

                }
            }
        }
    }
}