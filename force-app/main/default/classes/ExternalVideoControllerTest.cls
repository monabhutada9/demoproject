/**
 * Created by user on 05.03.2019.
 */

@IsTest
private class ExternalVideoControllerTest {

    private enum MetadataQueryStubType { NORMAL, EMPTY_LIST, EMPTY_VALUES, EMPTY_URL, EMPTY_AES_KEY }
    private enum VideoConfQueryStubType { NORMAL, EMPTY_LIST, EMPTY_SESSION_ID }
    private static final String testedClassName = 'ExternalVideoController';
    private static final Id videoConfId = fflib_IDGenerator.generate(Video_Conference__c.getSObjectType());
    private static final String sessionId = String.valueOf(Crypto.getRandomInteger());
    private static final String herokuAppURL = 'tokbox-test-url';
    private static final String herokuAesKeyString = JSON.serialize(Crypto.generateAesKey(256)).replaceAll('"', '');

    @IsTest
    static void refreshSession() {
        ExternalVideoController.refreshSession();
    }

    @IsTest
    static void getCurrentVideoId() {
        update new User(Id = UserInfo.getUserId(), Televisit_Buffer__c = videoConfId);
        System.assertEquals(videoConfId, ExternalVideoController.getCurrentVideoId());
    }

    @IsTest
    static void getVideoData() {
        applyMetadataQueryStub(MetadataQueryStubType.NORMAL);
        applyVideoConfQueryStub(VideoConfQueryStubType.NORMAL);
        System.assertNotEquals(null, ExternalVideoController.getVideoData(false));
        System.assertNotEquals(null, ExternalVideoController.getVideoData(true));
    }

    @IsTest
    static void getVideoDataForExternal() {
        applyMetadataQueryStub(MetadataQueryStubType.NORMAL);
        applyVideoConfQueryStub(VideoConfQueryStubType.NORMAL);
        System.assertNotEquals(null, ExternalVideoController.getVideoDataForExternal(videoConfId, sessionId, UserInfo.getUserId()));
    }

    @IsTest
    static void edgeCaseEmptyMetadata() {
        applyMetadataQueryStub(MetadataQueryStubType.EMPTY_LIST);
        System.assertEquals(null, ExternalVideoController.getVideoDataForExternal(null, null, null));
        System.assertEquals(null, String.valueOf(ExternalVideoController.getVideoData(false)));
    }

    @IsTest
    static void edgeCaseEmptyConference() {
        applyMetadataQueryStub(MetadataQueryStubType.NORMAL);
        applyVideoConfQueryStub(VideoConfQueryStubType.EMPTY_LIST);
        System.assertEquals(null, ExternalVideoController.getVideoDataForExternal(null, null, null));
        System.assertEquals(null, String.valueOf(ExternalVideoController.getVideoData(false)));
    }

    @IsTest
    static void edgeCaseEmptyHerokuAppUrl() {
        applyMetadataQueryStub(MetadataQueryStubType.EMPTY_VALUES);
        System.assertEquals(null, ExternalVideoController.getVideoDataForExternal(null, null, null));
        System.assertEquals(null, String.valueOf(ExternalVideoController.getVideoData(false)));
    }

    @IsTest
    static void edgeCaseEmptySession() {
        applyMetadataQueryStub(MetadataQueryStubType.NORMAL);
        applyVideoConfQueryStub(VideoConfQueryStubType.EMPTY_SESSION_ID);
        try {
            ExternalVideoController.getVideoData(false);
            System.assert(false);
        } catch(Exception e) {
            System.assertEquals('Script-thrown exception', e.getMessage());
        }
        try {
            ExternalVideoController.getVideoDataForExternal(videoConfId, null, UserInfo.getUserId());
            System.assert(false);
        } catch(Exception e) {
            System.assertEquals('Video conf in not available', e.getMessage());
        }
    }

    @IsTest
    static void edgeCaseEmptyAesKey() {
        applyMetadataQueryStub(MetadataQueryStubType.EMPTY_AES_KEY);
        applyVideoConfQueryStub(VideoConfQueryStubType.NORMAL);
        try {
            ExternalVideoController.getVideoData(false);
            System.assert(false);
        } catch(Exception e) {
            System.assertEquals('Script-thrown exception', e.getMessage());
        }
        try {
            ExternalVideoController.getVideoDataForExternal(videoConfId, sessionId, UserInfo.getUserId());
            System.assert(false);
        } catch(Exception e) {
            System.assertEquals('No encryption key was found', e.getMessage());
        }
    }

    private static void applyMetadataQueryStub(MetadataQueryStubType type) {
        List<VTD1_General_Settings__mdt> stub;
        switch on type {
            when EMPTY_LIST{
                stub = new List<VTD1_General_Settings__mdt>();
            }
            when EMPTY_VALUES{
                stub = new List<VTD1_General_Settings__mdt>{
                        new VTD1_General_Settings__mdt(VTD1_Video_Heroku_App_Url__c = null, VTR5_VideoHerokuAesKey__c = null)
                };
            }
            when EMPTY_URL{
                stub = new List<VTD1_General_Settings__mdt>{
                        new VTD1_General_Settings__mdt(VTD1_Video_Heroku_App_Url__c = null, VTR5_VideoHerokuAesKey__c = herokuAesKeyString)
                };
            }
            when EMPTY_AES_KEY{
                stub = new List<VTD1_General_Settings__mdt>{
                        new VTD1_General_Settings__mdt(VTD1_Video_Heroku_App_Url__c = herokuAppURL, VTR5_VideoHerokuAesKey__c = null)
                };
            }
            when NORMAL {
                stub = new List<VTD1_General_Settings__mdt>{
                        new VTD1_General_Settings__mdt(VTD1_Video_Heroku_App_Url__c = herokuAppURL, VTR5_VideoHerokuAesKey__c = herokuAesKeyString)
                };
            }
        }
        new QueryBuilder()
                .buildStub()
                .addStubToList(stub)
                .namedQueryStub(testedClassName + '.metadataQuery')
                .applyStub();
    }

    private static void applyVideoConfQueryStub(VideoConfQueryStubType type) {
        List<Video_Conference__c> stub;
        switch on type {
            when EMPTY_LIST {
                stub = new List<Video_Conference__c>();
            }
            when EMPTY_SESSION_ID {
                stub = new List<Video_Conference__c>{
                        new Video_Conference__c(SessionId__c = null)
                };
            }
            when NORMAL {
                stub = new List<Video_Conference__c>{
                        new Video_Conference__c(SessionId__c = sessionId)
                };
            }
        }
        new QueryBuilder()
                .buildStub()
                .addStubToList(stub)
                .namedQueryStub(testedClassName + '.fetchData')
                .applyStub();
    }
}