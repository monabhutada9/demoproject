@IsTest
public without sharing class VT_D2_StudyTeamByStudyControllerTest {
    
    public static void getMyStudiesTest() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id, Name FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];

        String jString = VT_D2_StudyTeamByStudyController.getMyStudies();
        List<HealthCloudGA__CarePlanTemplate__c> studies = (List<HealthCloudGA__CarePlanTemplate__c>) JSON.deserialize(jString, List<HealthCloudGA__CarePlanTemplate__c>.class);

        System.assertEquals(study.Name, studies[0].Name);
        Test.stopTest();
    }

    public static void getMyStudiesExceptionTest() {
        try {
            Test.startTest();
            List<Study_Team_Member__c> stmList = [SELECT Id FROM Study_Team_Member__c];
            delete stmList;
            VT_D2_StudyTeamByStudyController.getMyStudies();
            Test.stopTest();
        } catch (Exception e) {
        }
    }

    public static void getStudyTeamTest() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        VT_D2_StudyTeamByStudyController.getStudyTeam(study.Id);
        Test.stopTest();
    }

    public static void getStudyTeamExceptionTest() {

        try {
            Test.startTest();
            Id Id;
            VT_D2_StudyTeamByStudyController.getStudyTeam(Id);
            Test.stopTest();
        } catch (Exception e) {
        }
    }

    public static void getOtherRolesMapTest() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        User cra = (User) new DomainObjects.User_t().setProfile(VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME).persist();
        DomainObjects.StudyTeamMember_t studyTeamMember_cra = new DomainObjects.StudyTeamMember_t()
                .setUserId(cra.Id)
                .setRecordTypeByName('CRA')
                .setStudy(study.Id);
        studyTeamMember_cra.persist();
        VT_D2_StudyTeamByStudyController.getStudyTeam(study.Id);
        Test.stopTest();
    }
}