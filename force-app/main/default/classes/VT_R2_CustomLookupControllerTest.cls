/**
 * Created by user on 18.04.2019.
 */

@isTest
public with sharing class VT_R2_CustomLookupControllerTest {

    public static void test() {
        List <Account> accounts = new List<Account>();
        accounts.add(new Account(Name = 'account1', Description = 'description'));
        insert accounts;
        Map <String, String> params = new Map<String, String>{
                'sobject' => 'Account',
                'term' => 'acc',
                'filter' => 'Description = \'description\'',
                'order' => 'Name',
                'limit' => '10',
                'searchField' => 'Name',
                'menuImageURLField' => 'Description'
        };
        String jsonParams = JSON.serialize(params);
        VT_R2_CustomLookupController.queryRecords(jsonParams);
    }
}