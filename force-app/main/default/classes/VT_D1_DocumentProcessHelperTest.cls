/**
 * Created by somebody
 * updated by Galiya Khalikova on 03/02/2020
 */

@IsTest
public with sharing class VT_D1_DocumentProcessHelperTest {

//    @TestSetup
//    static void dataSetup() {
//
//        DomainObjects.Study_t study = new DomainObjects.Study_t()
//                .setName('Study Name')
//                /*.addPMA(new DomainObjects.User_t()
//                        .addContact(new DomainObjects.Contact_t())
//                        .setProfile('Site Coordinator'))*/
//                .addAccount(new DomainObjects.Account_t()
//                        .setRecordTypeByName('Sponsor'));
//
//        VT_R3_GlobalSharing.disableForTest = true;
//        DomainObjects.StudyTeamMember_t stmOnsiteCra = new DomainObjects.StudyTeamMember_t()
//                .setRecordTypeByName('CRA')
//                .addUser(new DomainObjects.User_t()
//                        .setProfile('CRA')
//                )
//                .addStudy(study);
//        stmOnsiteCra.persist();
//
//        DomainObjects.StudyTeamMember_t stmRemoteCra = new DomainObjects.StudyTeamMember_t()
//                .setRecordTypeByName('CRA')
//                .addUser(new DomainObjects.User_t()
//                        .setProfile('CRA'))
//                .addStudy(study);
//        stmRemoteCra.persist();
//
//        DomainObjects.StudyTeamMember_t studyTeamMember = new DomainObjects.StudyTeamMember_t()
//                .setRecordTypeByName('SCR')
//                .addUser(new DomainObjects.User_t()
//                        .addContact(new DomainObjects.Contact_t())
//                        .setProfile('Site Coordinator'))
//                .setOnsiteCraId(stmOnsiteCra.id)
//                .setRemoteCraId(stmRemoteCra.id)
//                .addStudy(study);
//
//        new DomainObjects.VTD1_Regulatory_Binder_t()
//                .addStudy(study)
//                .addRegDocument(
//                new DomainObjects.VTD1_Regulatory_Document_t()
//                        .setLevel('Study')
//                        .setWayToSendToDocuSign('Manual')
//                        .setDocumentType('Site Activation Approval Form')
//                        .setSiteRSU(true)
//        )
//                .persist();
//
//        new DomainObjects.VTD1_Regulatory_Binder_t()
//                .addStudy(study)
//                .addRegDocument(
//                new DomainObjects.VTD1_Regulatory_Document_t()
//                        .setLevel('Site')
//                        .setWayToSendToDocuSign('Manual')
//                        .setDocumentType('Site Activation Approval Form')
//                        .setSiteRSU(true)
//        )
//                .persist();
//
//        Test.startTest();
//        DomainObjects.VirtualSite_t virtualSite = new DomainObjects.VirtualSite_t()
//                .addStudy(study)
//                .setStudySiteNumber('12345')
//                .addStudyTeamMember(studyTeamMember);
//
//        virtualSite.persist();
//        Test.stopTest();
//
//        new DomainObjects.VTD1_Regulatory_Binder_t()
//                .addStudy(study)
//                .addRegDocument(
//                new DomainObjects.VTD1_Regulatory_Document_t()
//                        .setLevel('Site')
//                        .setWayToSendToDocuSign('Manual')
//                        .setDocumentType('Site Activation Approval Form')
//                        .setSiteRSU(true)
//        )
//                .persist();
//
//        /** Test for 'PA Signature Page*/
//        new DomainObjects.VTD1_Regulatory_Binder_t()
//                .addStudy(study)
//                .addRegDocument(
//                new DomainObjects.VTD1_Regulatory_Document_t()
//                        .setLevel('Site')
//                        .setWayToSendToDocuSign('Auto to PI')
//                        .setDocumentType('PA Signature Page')
//                        .setSiteRSU(false)
//        )
//                .persist();
//
//    }


    public static void createPlaceholdersByStudyLevelTest() {
        List<VTD1_Regulatory_Binder__c> binders = [SELECT Id, VTD1_Level__c, VTD1_Care_Plan_Template__c FROM VTD1_Regulatory_Binder__c];

        Test.startTest();
        VT_D1_DocumentProcessHelper.createPlaceholdersByStudyLevel(binders);
        Test.stopTest();

        List<VTD1_Document__c> placeholderList = [SELECT Id, VTD1_Current_Workflow__c, VTD1_Regulatory_Binder__c, VTD1_Study__c, VTD1_Status__c, VTD1_IsActive__c FROM VTD1_Document__c];
        System.assertEquals('Not Yet Started', placeholderList[0].VTD1_Current_Workflow__c, 'Incorrect VTD1_Current_Workflow__c value');
        System.assertEquals(binders[0].Id, placeholderList[0].VTD1_Regulatory_Binder__c, 'Incorrect VTD1_Regulatory_Binder__c value');
        System.assertEquals(binders[0].VTD1_Care_Plan_Template__c, placeholderList[0].VTD1_Study__c, 'Incorrect VTD1_Study__c value');
        System.assertEquals('Pending Approval', placeholderList[0].VTD1_Status__c, 'Incorrect VTD1_Status__c value');
        System.assertEquals(true, placeholderList[0].VTD1_IsActive__c, 'Incorrect VTD1_Status__c value');
    }


    public static void createPlaceholdersBySiteLevelTest() {
        List<Virtual_Site__c> virtualSites = [SELECT Id, VTD1_Study__c FROM Virtual_Site__c];
        List<VTD1_Regulatory_Binder__c> binders = [SELECT Id, VTD1_Level__c, VTD1_Care_Plan_Template__c, VTD1_Document_Type__c FROM VTD1_Regulatory_Binder__c];

        Test.startTest();
        VT_D1_DocumentProcessHelper.createPlaceholdersBySiteLevel(virtualSites);
        Test.stopTest();

        List<VTD1_Document__c> placeholderList = [SELECT Id, VTD1_Current_Workflow__c, VTD1_Regulatory_Binder__c, VTD1_Study__c, VTD1_Status__c, VTD1_IsActive__c, VTD1_Regulatory_Binder__r.VTD1_Regulatory_Document__r.VTD1_Document_Type__c FROM VTD1_Document__c];
        System.assertEquals('Not Yet Started', placeholderList[0].VTD1_Current_Workflow__c, 'Incorrect VTD1_Current_Workflow__c value');
        System.assertEquals(binders[0].Id, placeholderList[0].VTD1_Regulatory_Binder__c, 'Incorrect VTD1_Regulatory_Binder__c value');
        System.assertEquals(binders[0].VTD1_Care_Plan_Template__c, placeholderList[0].VTD1_Study__c, 'Incorrect VTD1_Study__c value');
        System.assertEquals('Pending Approval', placeholderList[0].VTD1_Status__c, 'Incorrect VTD1_Status__c value');
        System.assertEquals(true, placeholderList[0].VTD1_IsActive__c, 'Incorrect VTD1_Status__c value');
        Integer placeholdersWithPa = [SELECT COUNT() FROM VTD1_Document__c WHERE VTD1_Regulatory_Binder__r.VTD1_Regulatory_Document__r.VTD1_Document_Type__c = 'PA Signature Page'];
        System.assertEquals(0, placeholdersWithPa);
    }


    public static void createMissingPlaceholdersBySiteLevelTest() {
        List<Virtual_Site__c> virtualSites = [SELECT Id, VTD1_Study__c, VTD1_EDP_In_Progress__c,VTR3_Update_RB__c FROM Virtual_Site__c];
        List<VTD1_Regulatory_Binder__c> binders = [SELECT Id, VTD1_Level__c, VTD1_Care_Plan_Template__c,VTD1_Site_RSU__c FROM VTD1_Regulatory_Binder__c WHERE VTD1_Regulatory_Document__r.VTD1_Document_Type__c != 'PA Signature Page'];
        System.debug('virtualSites:' + virtualSites);
        
        Test.startTest();
        VT_D1_DocumentProcessHelper.createMissingPlaceholdersBySiteLevel(virtualSites);
        Test.stopTest();

        List<VTD1_Document__c> placeholderList = [SELECT Id, VTD1_Current_Workflow__c, VTD1_Regulatory_Binder__c, VTD1_Study__c, VTD1_Status__c, VTD1_IsActive__c, VTD1_Site__c FROM VTD1_Document__c];
        for (VTD1_Document__c doc : placeholderList) {
            system.debug('doc after test: ' + doc);
        }
        System.debug('placeholderList:' + placeholderList);
        System.assertEquals(placeholderList[1].VTD1_Site__c, virtualSites[0].Id, 'Incorrect VTD1_Study__c value');
        System.assertEquals(binders[0].Id, placeholderList[0].VTD1_Regulatory_Binder__c, 'Incorrect VTD1_Regulatory_Binder__c value');
        System.assertEquals(binders[0].VTD1_Care_Plan_Template__c, placeholderList[0].VTD1_Study__c, 'Incorrect VTD1_Study__c value');
        System.assertEquals('Not Yet Started', placeholderList[0].VTD1_Current_Workflow__c, 'Incorrect VTD1_Current_Workflow__c value');
        System.assertEquals('Pending Approval', placeholderList[0].VTD1_Status__c, 'Incorrect VTD1_Status__c value');
        System.assertEquals(true, placeholderList[1].VTD1_IsActive__c, 'Incorrect VTD1_Status__c value');
        System.assertEquals(3, virtualSites.size(), 'Incorrect virtualSites list size');
    }

}