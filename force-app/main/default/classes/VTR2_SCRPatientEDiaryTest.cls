/**
 * Created by User on 19/05/13.
 */
@isTest
public with sharing class VTR2_SCRPatientEDiaryTest {

    public static void getSurveysTest(){
        Case cas = [SELECT id FROM Case WHERE Subject = 'EDiaryAllTests' LIMIT 1];
        Test.startTest();
        VTR2_SCRPatientEDiary.getSurveys(cas.Id);
        Test.stopTest();
    }

    public static void getQuestionsAnswersTest(){
        VTD1_Survey__c survey = [SELECT Id FROM VTD1_Survey__c LIMIT 1];
        VTD1_Protocol_ePRO__c protocolEPROT = [SELECT Id FROM VTD1_Protocol_ePRO__c LIMIT 1];
        Test.startTest();
        VTR2_SCRPatientEDiary.getQuestionsAnswers(survey.Id,protocolEPROT.Id); //Id surveyId, Id protocolId
        Test.stopTest();
    }
    public static void saveAnswersTest(){
        VTD1_Survey_Answer__c answer = [SELECT Id FROM VTD1_Survey_Answer__c LIMIT 1];
        VTD1_Survey__c survey = [SELECT Id FROM VTD1_Survey__c LIMIT 1];
        VTR2_SCRPatientEDiary.AnswerWrapper answ = new VTR2_SCRPatientEDiary.AnswerWrapper();
        answ.answer = 'Test';
        answ.answerId = answer.Id;
        VTR2_SCRPatientEDiary.AnswerWrapper answ2 = new VTR2_SCRPatientEDiary.AnswerWrapper();
        answ2.answer = 'Test2';
        Test.startTest();
        VTR2_SCRPatientEDiary.saveAnswers(JSON.serialize(new List<VTR2_SCRPatientEDiary.AnswerWrapper>{answ,answ2}),survey.Id,true,VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED); //String answers, Id surveyId, Boolean submit, String surveyStatus
        Test.stopTest();
    }

    public static void updateDateTimeTest(){
        VTD1_Survey__c survey = [SELECT Id FROM VTD1_Survey__c LIMIT 1];
        Test.startTest();
        VTR2_SCRPatientEDiary.updateDateTime(survey.Id,VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_REVIEW_REQUIRED);
        Test.stopTest();
    }

    public static void updateSurveyStatusTest(){
        VTD1_Survey__c survey = [SELECT Id FROM VTD1_Survey__c LIMIT 1];
        Test.startTest();
        VTR2_SCRPatientEDiary.updateSurveyStatus(survey.Id);
        Test.stopTest();
    }
}