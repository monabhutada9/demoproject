/**
 * Created by Andrey Pivovarov on 4/23/2020.
 */

global with sharing class VT_R4_RemindersBatch implements Database.Batchable<SObject> {
    private Datetime curDate;
    private Datetime oneHour;
    private Datetime twoHours;
    private Datetime twoDays;
    private VT_R4_RemindersScheduler context;

    public VT_R4_RemindersBatch(VT_R4_RemindersScheduler context, Datetime dt) {


        curDate = dt.addSeconds(-dt.second());
        oneHour = curDate.addHours(1);
        twoHours = curDate.addHours(2);
        twoDays = curDate.addHours(48);
        curDate = curDate.addMinutes(-2);


        this.context = context;
    }
    /*for single run*/
    public VT_R4_RemindersBatch() {
        curDate = System.now();
        curDate = curDate.addSeconds(-curDate.second());

        oneHour = curDate.addHours(1);
        twoHours = curDate.addHours(2);
        twoDays = curDate.addHours(48);

    }

    global Iterable<SObject> start(Database.BatchableContext BC) {
        List<VTD1_Actual_Visit__c> actualVisits = new List<VTD1_Actual_Visit__c>();
        if (Test.isRunningTest() || (VTR4_ExternalScheduler__c.getInstance().VTR4_RemindersIsActive__c)) {
            System.debug('curDate* ' + curDate + ' - ' + oneHour + ' - ' + twoDays);
            actualVisits = [
                    SELECT VTD1_Scheduled_Date_Time__c, (
                            SELECT VTR4_Actual_Visit__c, VTR4_Scheduled_for__c, VTR4_Hours_Before__c
                            FROM Scheduler_Reminders__r
                    )
                    FROM VTD1_Actual_Visit__c
                    WHERE (VTD1_Status__c =: VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED
                    OR VTD1_Status__c =: VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED)
                    AND ((VTD1_Scheduled_Date_Time__c >= :oneHour
                    AND VTD1_Scheduled_Date_Time__c <:oneHour.addHours(1))
                    OR (VTD1_Scheduled_Date_Time__c >= :twoHours
                    AND VTD1_Scheduled_Date_Time__c <:twoHours.addHours(1))
                    OR (VTD1_Scheduled_Date_Time__c >= :twoDays
                    AND VTD1_Scheduled_Date_Time__c <:twoDays.addHours(1)))
                    ORDER BY VTD1_Scheduled_Date_Time__c
            ];
        }
        System.debug('actualVisits size = ' + actualVisits.size());
        return actualVisits;
    }

    global void execute(Database.BatchableContext BC, List<VTD1_Actual_Visit__c> scope) {
        if (scope == null || scope.isEmpty()) return;
        Integer countSchedulerReminders = [SELECT COUNT() FROM VTR4_Scheduler_Reminders__c];
        List<VTR4_Scheduler_Reminders__c> schedulerReminders = new List<VTR4_Scheduler_Reminders__c>();
        for (VTD1_Actual_Visit__c actualVisit : scope) {
            if (actualVisit.Scheduler_Reminders__r.size() == 0 && countSchedulerReminders < 9500) {
                VTR4_Scheduler_Reminders__c sr = new VTR4_Scheduler_Reminders__c();
                sr.VTR4_Actual_Visit__c = actualVisit.Id;
                sr.VTR4_Hours_Before__c = (actualVisit.VTD1_Scheduled_Date_Time__c.getTime() - curDate.getTime())/1000/60/60;
                sr.VTR4_Scheduled_for__c = actualVisit.VTD1_Scheduled_Date_Time__c.addHours((Integer) -sr.VTR4_Hours_Before__c);
                schedulerReminders.add(sr);
            }
            else if (actualVisit.Scheduler_Reminders__r.size() != 0 && (actualVisit.Scheduler_Reminders__r[0].VTR4_Scheduled_for__c !=
                    actualVisit.VTD1_Scheduled_Date_Time__c.addHours((Integer) -actualVisit.Scheduler_Reminders__r[0].VTR4_Hours_Before__c) ||
                    actualVisit.Scheduler_Reminders__r[0].VTR4_Hours_Before__c !=
                    (actualVisit.VTD1_Scheduled_Date_Time__c.getTime() - curDate.getTime())/1000/60/60)) {
                    Datetime scheduledDtPrev = actualVisit.Scheduler_Reminders__r[0].VTR4_Scheduled_for__c;
                    actualVisit.Scheduler_Reminders__r[0].VTR4_Hours_Before__c =
                            (actualVisit.VTD1_Scheduled_Date_Time__c.getTime() - curDate.getTime())/1000/60/60;
                    actualVisit.Scheduler_Reminders__r[0].VTR4_Scheduled_for__c =
                            actualVisit.VTD1_Scheduled_Date_Time__c.addHours((Integer) -actualVisit.Scheduler_Reminders__r[0].VTR4_Hours_Before__c);
                    if (actualVisit.Scheduler_Reminders__r[0].VTR4_Scheduled_for__c != scheduledDtPrev) {
                        actualVisit.Scheduler_Reminders__r[0].VTR4_ReadyToStart__c = false;
                    }
                    schedulerReminders.add(actualVisit.Scheduler_Reminders__r[0]);
            }
        }
        if (!schedulerReminders.isEmpty()) upsert schedulerReminders;
    }
    global void finish(Database.BatchableContext BC) {
        try {
            System.debug('curDate** ' + curDate + ' - ' + oneHour + ' - ' + twoDays);
            List<VTR4_Scheduler_Reminders__c> remindersToDelete = [
                    SELECT Id FROM VTR4_Scheduler_Reminders__c
                    WHERE (VTR4_Actual_Visit__r.VTD1_Status__c !=: VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED
                    AND VTR4_Actual_Visit__r.VTD1_Status__c !=: VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED)
                    OR (VTR4_Actual_Visit__r.VTD1_Scheduled_Date_Time__c <: oneHour.addMinutes(-15)
                    OR (VTR4_Actual_Visit__r.VTD1_Scheduled_Date_Time__c >: twoHours.addHours(1)
                    AND VTR4_Actual_Visit__r.VTD1_Scheduled_Date_Time__c <: twoDays.addMinutes(-15))
                    OR VTR4_Actual_Visit__r.VTD1_Scheduled_Date_Time__c >: twoDays.addHours(1))
            ];
            delete remindersToDelete;
        }
        catch (Exception e) {
            System.debug('VTR4_Scheduler_Reminders__c delete exception ' + e);
        }
        finally {
            if (this.context != null) this.context.scheduleJob(context.getAlignedTimeJob(System.now()));
        }
    }
}