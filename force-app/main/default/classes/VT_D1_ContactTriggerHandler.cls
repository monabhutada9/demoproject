public with sharing class VT_D1_ContactTriggerHandler {
    private static Boolean hasBeforeRanOnce = false;
    private static Boolean hasAfterRanOnce = false;
    public static Boolean isLanguageUpdate = false;

    public static void processBefore(Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, List<Contact> oldList, List<Contact> newList, Map<Id, Contact> oldMap, Map<Id, Contact> newMap) {
        if (isDelete) {
            updateHistory(oldList, null);
        }
        if (!hasBeforeRanOnce) {
            hasBeforeRanOnce = true;
            if (isInsert) {
                populateDocLanguageOnInsert(newList);
            }
        }
        if (isUpdate) {
            populateDocLanguage(oldMap, newMap);
        }
    }

    public static void processAfter(Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, List<Contact> oldList, List<Contact> newList, Map<Id, Contact> oldMap, Map<Id, Contact> newMap) {
        if (!hasAfterRanOnce) {
            hasAfterRanOnce = true;
            if (isUpdate) {
                updateUserData(newList, oldMap);
                //updateVisitAndConferenceMembers(newList, oldMap);
            }
            if (!VT_R4_PatientConversion.patientConversionInProgress) {
            if (isInsert || isUpdate) {
                updateAccountData(newList);
                updatePatientData(newList);
            }
        }
        }

        if (isInsert) {
            if (!VT_R4_PatientConversion.patientConversionInProgress) {
            VT_D2_ContactTriggerHelper.createPortalUsersForPatients(newList);
            }
            updateHistory(newList, null);
        }
        if (isUpdate && !VT_R4_PatientConversion.patientConversionInProgress) {
            VT_R2_ProcessNewPrimaryCGContacts.processContacts(newList, oldMap);
            updateHistory(newList, oldMap);
            //sendWelcomeAfterPatient(newList, oldMap);
        }
    }

    public static void updateAccountData(List<Contact> contactList) {
        Set<String> accountIdSet = new Set<String>();
        List<Contact> contactFilteredList = new List<Contact>();
        for (Contact contact : contactList) {
            if (contact.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PATIENT
                    || contact.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PI) {
                if (contact.AccountId != null) {
                    accountIdSet.add(contact.AccountId);
                }
                contactFilteredList.add(contact);
            }
        }
        if (accountIdSet.isEmpty() || contactFilteredList.isEmpty()) return;

        Map<Id, Account> accountIdToAccountMap = new Map<Id, Account>([SELECT Id, VTD1_LegalFirstName__c, VTD1_LastName_del__c, VTD1_MiddleName__c, VTD1_DateOfBirth__c, VTD1_Gender__c, RecordType.Name FROM Account WHERE Id IN:accountIdSet]);
        if (accountIdToAccountMap.isEmpty()) return;

        List<Account> accountListToUpdate = new List<Account>();
        for (Contact contact : contactFilteredList) {
            Account account = accountIdToAccountMap.get(contact.AccountId);
            if ((contact.FirstName != account.VTD1_LegalFirstName__c
                || contact.LastName != account.VTD1_LastName_del__c
                || contact.VTD1_MiddleName__c != account.VTD1_MiddleName__c
                || contact.HealthCloudGA__Gender__c != account.VTD1_Gender__c
                || contact.Birthdate != account.VTD1_DateOfBirth__c)
                    && account.RecordType.Name != 'Research Facility') {
                account.VTD1_LegalFirstName__c = contact.FirstName;
                account.VTD1_LastName_del__c = contact.LastName;
                account.VTD1_MiddleName__c = contact.VTD1_MiddleName__c;
                account.VTD1_DateOfBirth__c = contact.Birthdate;
                account.VTD1_Gender__c = contact.HealthCloudGA__Gender__c;
                account.Name = account.VTD1_LegalFirstName__c + ' ' + account.VTD1_LastName_del__c;
                accountListToUpdate.add(account);
            }
        }

        if (!accountListToUpdate.isEmpty()) {
            update accountListToUpdate;
        }
    }

    public static void updateUserData(List<Contact> contactList, Map<Id, Contact> contactOldMap) {
        system.debug('==>'+contactList);
        List<Contact> contactFilteredList = new List<Contact>();
        for (Contact contact : contactList) {
            if (contact.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PATIENT
                    || contact.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_CAREGIVER
                    || contact.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PI) {
                contactFilteredList.add(contact);
            }
        }
        if (contactFilteredList.isEmpty()) return;

        List<User> userList = [SELECT Id, FirstName, LastName, Name, Email, ContactId, LanguageLocaleKey FROM User WHERE ContactId IN:contactFilteredList];
        if (userList.isEmpty()) return;

        Map<Id, User> contactIdToUserMap = new Map<Id, User>();
        for (User user : userList) {
            contactIdToUserMap.put(user.ContactId, user);
        }

        List<User> userListToUpdate = new List<User>();
        for (Contact contact : contactFilteredList) {
            User user = contactIdToUserMap.get(contact.Id);
            Contact contactOld = contactOldMap.get(contact.Id);
            if (contact.FirstName != contactOld.FirstName || contact.LastName != contactOld.LastName || contact.Email != contactOld.Email) {
                user.FirstName = contact.FirstName;
                user.LastName = contact.LastName;
                if (contact.Email != contactOld.Email) {
                    user.Email = contact.Email;
                }
                userListToUpdate.add(user);
            }
        }
        if(!isLanguageUpdate){
            updateUserLanguage(contactOldMap, new Map<Id, Contact>(contactList), userList, userListToUpdate);
        }
        if (!userListToUpdate.isEmpty() && !Test.isRunningTest()) {
            System.debug('==> languages: ');
            for (User u: userListToUpdate) {
                System.debug(' ' + u.LanguageLocaleKey);
            }
            update userListToUpdate;
        }
    }

    public static void updatePatientData(List<Contact> contactList) {
        List<Contact> contactFilteredList = new List<Contact>();
        for (Contact contact : contactList) {
            if (contact.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PATIENT) {
                contactFilteredList.add(contact);
            }
        }
        if (contactFilteredList.isEmpty()) return;

        List<VTD1_Patient__c> patientList = [SELECT Id, VTD1_First_Name__c, VTD1_Last_Name__c, VTD1_Patient_Name__c, VTD1_Contact__c FROM VTD1_Patient__c WHERE VTD1_Contact__c IN:contactFilteredList];
        if (patientList.isEmpty()) return;

        Map<Id, VTD1_Patient__c> contactIdToPatientMap = new Map<Id, VTD1_Patient__c>();
        for (VTD1_Patient__c patient : patientList) {
            contactIdToPatientMap.put(patient.VTD1_Contact__c, patient);
        }

        List<VTD1_Patient__c> patientListToUpdate = new List<VTD1_Patient__c>();
          System.debug('contactIdToPatientMap'+contactIdToPatientMap);
        for (Contact contact : contactFilteredList) {
            VTD1_Patient__c patient = contactIdToPatientMap.get(contact.Id);
            if ( patient != null && (contact.FirstName != patient.VTD1_First_Name__c || contact.LastName != patient.VTD1_Last_Name__c)) {
            System.debug('patient '+patient);
                patient.VTD1_First_Name__c = contact.FirstName;
                patient.VTD1_Last_Name__c = contact.LastName;
                patientListToUpdate.add(patient);
            }
        }

        if (!patientListToUpdate.isEmpty()) {
            update patientListToUpdate;
        }

    }

/*    public static void updateVisitAndConferenceMembers(List<Contact> contactList, Map<Id, Contact> contactOldMap){
        List<Contact> contactFilteredList = new List<Contact>();
        for(Contact contact : contactList){
            if (contact.Email != contactOldMap.get(contact.Id).Email
                    && (contact.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PATIENT
                        || contact.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_CAREGIVER)){
                contactFilteredList.add(contact);
            }
        }
        if (contactFilteredList.isEmpty()) return;

        List<Visit_Member__c> visitMemberList = [SELECT VTD1_Email__c, Participant_Contact__c FROM Visit_Member__c WHERE Participant_Contact__c IN :contactFilteredList];
        List<VTD1_Conference_Member__c> conferenceMemberList = [SELECT VTD1_External_Participant_Email__c, VTD1_User__r.ContactId FROM VTD1_Conference_Member__c WHERE VTD1_User__r.ContactId IN :contactFilteredList];
        if (visitMemberList.isEmpty() && conferenceMemberList.isEmpty()) return;

        Map<Id,List<Visit_Member__c>> contactIdToVisitMembersMap = new Map<Id,List<Visit_Member__c>>();
        for (Visit_Member__c vm : visitMemberList){
            Id memberContactId = vm.Participant_Contact__c;
            if (!contactIdToVisitMembersMap.containsKey(memberContactId)){
                contactIdToVisitMembersMap.put(memberContactId, new List<Visit_Member__c>());
            }
            contactIdToVisitMembersMap.get(memberContactId).add(vm);
        }
        Map<Id,List<VTD1_Conference_Member__c>> contactIdToConferenceMembersMap = new Map<Id,List<VTD1_Conference_Member__c>>();
        for (VTD1_Conference_Member__c cm : conferenceMemberList){
            Id memberContactId = cm.VTD1_User__r.ContactId;
            if (!contactIdToConferenceMembersMap.containsKey(memberContactId)){
                contactIdToConferenceMembersMap.put(memberContactId, new List<VTD1_Conference_Member__c>());
            }
            contactIdToConferenceMembersMap.get(memberContactId).add(cm);
        }

        List<Visit_Member__c> visitMemberListToUpdate = new List<Visit_Member__c>();
        List<VTD1_Conference_Member__c> conferenceMemberListToUpdate = new List<VTD1_Conference_Member__c>();

        for (Contact contact : contactFilteredList) {
            //Contact contactOld = contactOldMap.get(contact.Id);
            //if (contact.Email != contactOld.Email){
            if (contactIdToVisitMembersMap.containsKey(contact.Id)) {
                for (Visit_Member__c vm : contactIdToVisitMembersMap.get(contact.Id)) {
                    vm.VTD1_Email__c = contact.Email;
                    visitMemberListToUpdate.add(vm);
                }
            }
            if (contactIdToConferenceMembersMap.containsKey(contact.Id)) {
                for (VTD1_Conference_Member__c cm : contactIdToConferenceMembersMap.get(contact.Id)) {
                    cm.VTD1_External_Participant_Email__c = contact.Email;
                    conferenceMemberListToUpdate.add(cm);
                }
            }
            //}
        }

        if (!visitMemberListToUpdate.isEmpty()) {
            update visitMemberListToUpdate;
        }
        if (!conferenceMemberListToUpdate.isEmpty()) {
            update conferenceMemberListToUpdate;
        }
    }*/

    //SH-4224
    private static void populateDocLanguage(Map<Id, Contact> oldMap, Map<Id, Contact> newMap) {
        Contact currentUser;
        for (Id contactId : newMap.keySet()) {
            currentUser = newMap.get(contactId);
            if (currentUser.VTR2_Primary_Language__c != oldMap.get(contactId).VTR2_Primary_Language__c) {
                currentUser.VTR2_Doc_Language__c = currentUser.VTR2_Primary_Language__c;
            }
        }
    }

    private static void populateDocLanguageOnInsert(List<Contact> newContacts) {
        for (Contact con : newContacts) {
            if (con.VTR2_Primary_Language__c != con.VTR2_Doc_Language__c) {
                con.VTR2_Doc_Language__c = con.VTR2_Primary_Language__c;
            }
        }
    }


    //SH-4224
    private static void updateUserLanguage(Map<Id, Contact> oldMap, Map<Id, Contact> newMap, List<User> userList, List<User> userListToUpdate) {
        System.debug('==> VT_D1_ContactTriggerHandler.updateUserLanguage');
        Map<Id, User> userMapToUpdate = new Map<Id, User>(userListToUpdate);

        Boolean isChanged;
        for (User u : userList) {
            isChanged = false;
            
            Contact newContact = newMap.get(u.ContactId);
            Contact oldContact = oldMap.get(u.ContactId);

            if (oldContact.VTR2_Primary_Language__c != newContact.VTR2_Primary_Language__c) {
                System.debug('==> Before Contact update:');
                System.debug('==> u.ContactId: ' + u.ContactId);
                System.debug('==> u.LanguageLocaleKey: ' + u.LanguageLocaleKey);
                System.debug('==> newContact.VTR2_Primary_Language: ' + newContact.VTR2_Primary_Language__c);
                u.LanguageLocaleKey = newContact.VTR2_Primary_Language__c;
                System.debug('==> changing u.LanguageLocaleKey to: ' + u.LanguageLocaleKey);

                isChanged = true;                

            }

            // SH-9185 - Start - reflect Secondary and Tertiary Preferred Language on user record.            
            if (newContact.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PATIENT ||
                    newContact.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_CAREGIVER) {

                if (oldContact.VT_R5_Secondary_Preferred_Language__c != newContact.VT_R5_Secondary_Preferred_Language__c) {
                    u.VT_R5_Secondary_Preferred_Language__c = newContact.VT_R5_Secondary_Preferred_Language__c;
                    isChanged = true;
                }
                if (oldContact.VT_R5_Tertiary_Preferred_Language__c != newContact.VT_R5_Tertiary_Preferred_Language__c) {
                    u.VT_R5_Tertiary_Preferred_Language__c = newContact.VT_R5_Tertiary_Preferred_Language__c;
                    isChanged = true;
                }
            }

            // SH-9185 - End
            
            if (!userMapToUpdate.containsKey(u.Id) && isChanged) {
                isLanguageUpdate = true;
                userListToUpdate.add(u);
            }
        }
    }

    public static void updateHistory(List <Contact> cons, Map <Id, Contact> oldMap) {
        List<Contact> patients = new List<Contact>();
        for (Contact con : cons) {
            if (con.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CONTACT_PATIENT) {
                patients.add(con);
            }
        }

        if (!patients.isEmpty()) {
            VT_R2_PatientHistoryHandler historyHandler = new VT_R2_PatientHistoryHandler();
            switch on Trigger.operationType {
                when BEFORE_DELETE {
                    historyHandler.onBeforeDelete(patients);
                }
                when AFTER_INSERT {
                    historyHandler.onAfterInsert(patients);
                }
                when AFTER_UPDATE {
                    historyHandler.onAfterUpdate(patients, oldMap);
                }
            }
        }
    }
//    public static void sendWelcomeAfterPatient(List<Contact> contactList, Map <Id, Contact> oldMap) {
//
//        List<Contact> userIdContacts = new List<Contact>();
//        for (Contact con : contactList) {
//            Contact old = oldMap.get(con.id);
//            if (con.VTD1_UserId__c != null && old.VTD1_UserId__c == null) {
//                userIdContacts.add(con);
//            }
//        }
//        if (!userIdContacts.isEmpty()) {
//            VT_R3_WelcomeEmailTemplateforProfile__mdt [] WelcomeEmailTemplateforProfile = [
//                    SELECT VTR3_Email_Template__c,
//                            VTR3_User_profile__c
//                    FROM VT_R3_WelcomeEmailTemplateforProfile__mdt
//            ];
//            Map<String, String> mapProfileTemplate = new Map<String, String>();
//            for (VT_R3_WelcomeEmailTemplateforProfile__mdt item : WelcomeEmailTemplateforProfile) {
//                mapProfileTemplate.put(item.VTR3_User_profile__c, item.VTR3_Email_Template__c);
//            }
//
//            //VTR2_WelcomeEmailSent__c
//            List<Contact> userList = [
//                    SELECT Id, VTD1_UserId__c, VTD1_UserId__r.Profile.Name, VTD1_UserId__r.isActive
//                    FROM Contact
//                    WHERE Id IN :userIdContacts
//                    AND VTD1_UserId__r.isActive = true
//
//                    AND VTD1_UserId__r.Profile.Name IN :mapProfileTemplate.keyset()
//            ];//AND VTR2_WelcomeEmailSent__c != true
//
//            Map<String, Id> templateIdMap = new Map<String, Id>();
//            for (EmailTemplate item : [
//                    SELECT Id, DeveloperName
//                    FROM EmailTemplate
//                    WHERE DeveloperName IN :mapProfileTemplate.values()
//            ]) {
//                templateIdMap.put(item.DeveloperName, item.Id);
//            }
//            Id orgWideEmailAddressId = [SELECT Id, CreatedDate From OrgWideEmailAddress ORDER BY CreatedDate ASC LIMIT 1].Id;
//
//            //send email to user
//            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
//            for (Contact u : userList) {
//                string key = u.VTD1_UserId__r.Profile.Name;
//                string templateDevName = mapProfileTemplate.get(key);//need this field
//                Id templId = templateIdMap.get(templateDevName);
//                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
//                mail.setTargetObjectId(u.VTD1_UserId__c);
//                mail.setUseSignature(false);
//                mail.setOrgWideEmailAddressId(orgWideEmailAddressId);
//                mail.setBccSender(false);
//                mail.setSaveAsActivity(false);
//                mail.setTemplateId(templId);
//                mail.setWhatId(u.VTD1_UserId__c);
//                mails.add(mail);
//                //u.VTR2_WelcomeEmailSent__c = true;
//            }
//            if (!userList.isEmpty()) {
//                Messaging.sendEmail(mails);
//                update userList;
//            }
//        }
//    }
//Added by IQVIA Strikers 
public static void byPassContactTrigger(){
    hasAfterRanOnce = true;
    hasBeforeRanOnce = true;
}

}