@IsTest
private class VT_D2_OrderStageProcessHandlerTest {
    @testSetup
    static void setup() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        //HealthCloudGA__CandidatePatient__c candidatePatient = VT_D1_TestUtils.createPatientCandidate('Mike', 'Birn', 'mikebirn@test.com', 'mikebirn@test.com', 'mikebirn', 'Converted', study.Id, 'PN', '11-11-11', 'US', 'NY');
        VT_D1_TestUtils.createTestPatientCandidate(1);
        VT_D1_TestUtils.createProtocolDelivery(study.Id);
        Test.stopTest();

        List<Case> casesList = [SELECT Id FROM Case];
        if(casesList.size() > 0){
            Case cas = casesList.get(0);
            VT_R4_GenerateActualVisitsAndKits generator = new VT_R4_GenerateActualVisitsAndKits();
            generator.executeDeliveriesAndKits(new List<Id> {cas.Id});
        }
    }

    @IsTest
    static void createPatientDeliveryRecordsTest(){
        List<Case> casesList = [SELECT Id FROM Case];
        System.assert(casesList.size() > 0);
        if(casesList.size() > 0){
            Case cas = casesList.get(0);
            List<VTD1_Order__c> deliveryList = [SELECT Id FROM VTD1_Order__c WHERE VTD1_Case__c =: cas.Id];
            System.assert(deliveryList.size() > 4);

            deliveryList.get(0).VTD1_ShipmentId__c = '11111';
            deliveryList.get(1).VTD1_ShipmentId__c = '22222';
            deliveryList.get(2).VTD1_ShipmentId__c = '33333';
            deliveryList.get(3).VTD1_ShipmentId__c = '33333';
            update deliveryList;

            Set<String> processedFields = new Set<String>();
            List<Schema.FieldSetMember> fieldMembers = SObjectType.VTD1_OrderStage__c.FieldSets.Fields_to_Order.getFields();
            for(FieldSetMember fsm : fieldMembers) processedFields.add(fsm.getFieldPath());
            Date date1 = System.today().addDays(1);
            Date date2 = System.today().addDays(2);

            Test.startTest();
            List<VTD1_OrderStage__c> orderStageList = new List<VTD1_OrderStage__c>();

            VTD1_OrderStage__c orderStage1 = new VTD1_OrderStage__c();
            orderStage1.VTD1_ShipmentId__c = '11111';
            orderStage1.VTD1_SubjectId__c  = '44';
            orderStage1.VTD1_ShipmentName__c  = '444';
            orderStage1.VTD1_ProtocolId__c = '1';
            orderStage1.VTD1_Carrier__c = 'VTD1_Carrier__c11111';
            orderStage1.VTD1_ChangeSource__c = 'SH';
            orderStage1.VTD1_ExpectedDeliveryDate__c = date1;
            orderStage1.VTD1_Status__c = 'Reconciliation';
            orderStage1.VTD1_TrackingNumber__c = 'VTD1_TrackingNumber__c11111';
            orderStage1.VTD1_TrackingURL__c = 'https://url.com/11111';
            orderStageList.add(orderStage1);

            VTD1_OrderStage__c orderStage2 = new VTD1_OrderStage__c();
            orderStage2.VTD1_ShipmentId__c = '66666';
            orderStage2.VTD1_SubjectId__c  = '22';
            orderStage2.VTD1_ShipmentName__c  = '222';
            orderStage2.VTD1_ProtocolId__c = '2';
            orderStage2.VTD1_Carrier__c = 'VTD1_Carrier__c22222';
            orderStage2.VTD1_ChangeSource__c = 'CSM';
            orderStage2.VTD1_ExpectedDeliveryDate__c = date2;
            orderStage2.VTD1_Status__c = 'Delivered';
            orderStage2.VTD1_TrackingNumber__c = 'VTD1_TrackingNumber__c22222';
            orderStage2.VTD1_TrackingURL__c = 'https://url.com/22222';
            orderStageList.add(orderStage2);

            VTD1_OrderStage__c orderStage3 = new VTD1_OrderStage__c();
            orderStage3.VTD1_ShipmentId__c = '33333';
            orderStage3.VTD1_SubjectId__c  = '55';
            orderStage3.VTD1_ShipmentName__c  = '555';
            orderStage3.VTD1_ProtocolId__c = '3';
            orderStage3.VTD1_Carrier__c = 'VTD1_Carrier__c33333';
            orderStage3.VTD1_ChangeSource__c = 'SH';
            orderStage3.VTD1_ExpectedDeliveryDate__c = date1;
            orderStage3.VTD1_Status__c = 'Received';
            orderStage3.VTD1_TrackingNumber__c = 'VTD1_TrackingNumber__c33333';
            orderStage3.VTD1_TrackingURL__c = 'https://url.com/33333';
            orderStageList.add(orderStage3);
            insert orderStageList;
            Test.stopTest();

            List<VTD1_Order__c> deliveryList1 = [SELECT Id, VTD1_ShipmentId__c, VTD1_IntegrationId__c, VTD1_Carrier__c, VTD1_ChangeSource__c,
                                                        VTD1_ExpectedDeliveryDate__c, VTD1_Status__c, VTD1_TrackingNumber__c, VTD1_TrackingURL__c
                                                 FROM VTD1_Order__c WHERE VTD1_Case__c =: cas.Id];

            VTD1_Order__c delivery1;
            for(VTD1_Order__c delivery : deliveryList1){
                if(delivery.VTD1_ShipmentId__c == '11111'){
                    delivery1 = delivery;
                }
            }

            if (processedFields.contains('VTD1_Carrier__c')){
                System.assertEquals('VTD1_Carrier__c11111', delivery1.VTD1_Carrier__c);
            }
            if (processedFields.contains('VTD1_ChangeSource__c')){
                System.assertEquals('SH', delivery1.VTD1_ChangeSource__c);
            }
            if (processedFields.contains('VTD1_ExpectedDeliveryDate__c')){
                System.assertEquals(date1, delivery1.VTD1_ExpectedDeliveryDate__c);
            }
            if (processedFields.contains('VTD1_Status__c')){
                System.assertEquals('Reconciliation', delivery1.VTD1_Status__c);
            }
            if (processedFields.contains('VTD1_TrackingNumber__c')){
                System.assertEquals('VTD1_TrackingNumber__c11111', delivery1.VTD1_TrackingNumber__c);
            }
            if (processedFields.contains('VTD1_TrackingURL__c')){
                System.assertEquals('https://url.com/11111', delivery1.VTD1_TrackingURL__c);
            }

            VTD1_OrderStage__c orderStage11;
            VTD1_OrderStage__c orderStage22;
            VTD1_OrderStage__c orderStage33;
            List<VTD1_OrderStage__c> orderStageList1 = [SELECT VTD1_ShipmentId__c, VTD1_IntegrationId__c, VTD1_ProcessingResult__c FROM VTD1_OrderStage__c];
            for(VTD1_OrderStage__c orderStage : orderStageList1){
                if(orderStage.VTD1_ShipmentId__c == '11111'){
                    orderStage11 = orderStage;
                }
                if(orderStage.VTD1_ShipmentId__c == '66666'){
                    orderStage22 = orderStage;
                }
                if(orderStage.VTD1_ShipmentId__c == '33333'){
                    orderStage33 = orderStage;
                }
            }
            System.assertEquals('OK', orderStage11.VTD1_ProcessingResult__c);
            System.assertEquals('Delivery not found', orderStage22.VTD1_ProcessingResult__c);
            System.assertEquals('Several deliveries found', orderStage33.VTD1_ProcessingResult__c);
        }
    }
}