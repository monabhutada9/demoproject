/**
 * Created by Maksim Fedarenka on 12/4/2020.
 */

@RestResource(UrlMapping='/ediaries-table-service/*')
global with sharing class VT_R5_EDiariesTableRestService {
    private static final String NOT_STARTED = 'Not Started';

    @HttpGet @RemoteAction @ReadOnly
    global static String getEDiaryAndSiteNames() {
        List<String> errors = new List<String>();
        Set<VT_R5_EDiariesTableController.ColumnWrapper> sites = new Set<VT_R5_EDiariesTableController.ColumnWrapper>();
        Set<VT_R5_EDiariesTableController.ColumnWrapper> eDiaries = new Set<VT_R5_EDiariesTableController.ColumnWrapper>();

        String jsonParams = '';

        try {
            jsonParams = RestContext.request.params.get('filter');
            Map <String, Object> params = (Map<String, Object>) JSON.deserializeUntyped(jsonParams.replace('+', ' '));
            String filter = 'WHERE VTD1_Status__c <> :NOT_STARTED ';
            List<Object> virtualSiteFiltersList = new List<Object>();
            if (params.containsKey('filterMap')) {
                Map<String, Object> filterMap = (Map<String, Object>) params.get('filterMap');
                if (filterMap.containsKey('studyId') && filterMap.get('studyId') != null) {
                    filter += ' AND VTD1_CSM__r.VTD1_Study__c = \'' + ((String) filterMap.get('studyId')) + '\'';
                }
                if (filterMap.containsKey('virtualSitesFilters')) {
                    virtualSiteFiltersList = (List<Object>) filterMap.get('virtualSitesFilters');
                }
            }

            String query = '' +
                    'SELECT Name, VTD1_CSM__r.VTD1_Virtual_Site__r.Name SiteName ' +
                    'FROM VTD1_Survey__c ' +
                    + filter.replace('+', ' ') + ' ' +
                    'GROUP BY Name, VTD1_CSM__r.VTD1_Virtual_Site__r.Name ' +
                    'ORDER BY Name ';

            List<AggregateResult> records = Database.query(query);

            Integer MAX_NAME_LEN = 40;
            for (AggregateResult ar : records) {
                String eDiaryName = String.valueOf(ar.get('Name'));
                String eDiaryShortName =
                        eDiaryName.length() > MAX_NAME_LEN ? eDiaryName.left(MAX_NAME_LEN) + '...' : eDiaryName;
                String siteName = String.valueOf(ar.get('SiteName'));

                if (virtualSiteFiltersList.isEmpty()) {
                    eDiaries.add(new VT_R5_EDiariesTableController.ColumnWrapper(eDiaryShortName, eDiaryName));
                } else if (!virtualSiteFiltersList.isEmpty() && virtualSiteFiltersList.contains(siteName)) {
                    eDiaries.add(new VT_R5_EDiariesTableController.ColumnWrapper(eDiaryShortName, eDiaryName));
                }

                if (String.isNotBlank(siteName)) {
                    String siteNameShort =
                            siteName.length() > MAX_NAME_LEN ? siteName.left(MAX_NAME_LEN) + '...' : siteName;
                    sites.add(new VT_R5_EDiariesTableController.ColumnWrapper(siteNameShort, siteName));
                }
            }
        } catch (Exception ex) {
            errors.add(ex.getMessage() + ' ' + ex.getStackTraceString());
        }

        return JSON.serialize(new Map<String, Object>{
                'sites' => sites,
                'eDiaries' => eDiaries,
                'errors' => errors
        });
    }
}