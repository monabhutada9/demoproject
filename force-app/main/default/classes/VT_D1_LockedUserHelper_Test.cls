/**
* @author: Carl Judge
* @date: 11-Sep-18
* @description:
**/

@IsTest
private class VT_D1_LockedUserHelper_Test {

    @testSetup
    private static void setupMethod() {
        Contact con = new Contact(
            FirstName = 'test',
            LastName = 'L'
        );
        insert con;

        insert new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Patient'].Id,
            FirstName = 'LockedTestPatient',
            LastName = 'L' ,
            Email = VT_D1_TestUtils.generateUniqueUserName(),
            Username = VT_D1_TestUtils.generateUniqueUserName(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            IsActive = true,
            ContactId = con.Id
        );
    }

    @isTest
    private static void firstTest() {
        User usr = [select id,ContactId, Username from User where LastName ='L' and FirstName='LockedTestPatient' limit 1];
        UserLogin uLogin;
        for (UserLogin item : [SELECT IsPasswordLocked, IsFrozen FROM UserLogin WHERE UserId = :usr.Id]) {
            uLogin = item;
        }
        System.debug('!!!!!!!!!!username ::::: '+usr.Username);
        Test.startTest();
        VT_D1_LockedUserHelper.checkForLockedUser(usr.Username);
        VT_D1_LockedUserHelper.sendAdminEmail(usr.ContactId);
        VT_D1_LockedUserHelper.userWasLocked(usr.Id);
        uLogin.IsFrozen = true;
        update uLogin;
        VT_D1_LockedUserHelper.checkForLockedUser(usr.Username);
        VT_D1_LockedUserHelper.checkForLockedUser(null);
        Test.stopTest();
    }


    @isTest
    private static void testSendWelcomeAfterCreateUser() {
        Test.startTest();
        User user = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Patient Guide'].Id,
            FirstName = 'MTest Patient G',
            LastName = 'LT' ,
            Email = VT_D1_TestUtils.generateUniqueUserName(),
            Username = VT_D1_TestUtils.generateUniqueUserName(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            IsActive = true,
            VTR2_WelcomeEmailSent__c = true
        );
        insert user;
        VT_D1_UserTriggerHandler.sendWelcomeAfterCreateUser(new List<User>{user},null);
        
        user = [select id,ContactId, Username, VTR2_WelcomeEmailSent__c from User where Id =: user.Id limit 1];
        system.assertEquals(false, user.VTR2_WelcomeEmailSent__c);

        Test.stopTest();
    }

    @isTest
    private static void testLanguage() {
        User patUser = [SELECT Id, ContactId, Email FROM User WHERE Username LIKE '%@iqviavttest.com' AND ContactId != null LIMIT 1];
        System.debug(patUser);
        //insert patUser;
        System.runAs(new User(Id = UserInfo.getUserId())) {
            update new Contact(Id = patUser.ContactId,  VTD1_UserId__c = patUser.Id);
            update new User(Id = patUser.Id, LanguageLocaleKey = 'es');
            System.assertEquals('es', [SELECT VTR2_Primary_Language__c FROM Contact WHERE VTD1_UserId__c = :patUser.Id].VTR2_Primary_Language__c);
        }
    }

}