/**
* @author: Carl Judge
* @date: 03-Sep-18
* @description: 
**/

public with sharing class VT_D1_ObjectiveTriggerHandler {

    public void onBeforeDelete(List<Objective__c> recs) {
        Map<Id, Id> objectiveToStudyIds = new Map<Id, Id>();

        for (Objective__c item : recs) {
            if (item.Study__c != null) {
                objectiveToStudyIds.put(item.Id, item.Study__c);
            } else if (item.Study_for_Primary_Objective__c != null) {
                objectiveToStudyIds.put(item.Id, item.Study_for_Primary_Objective__c);
            } else if (item.Study_for_Secondary_Objective__c != null) {
                objectiveToStudyIds.put(item.Id, item.Study_for_Secondary_Objective__c);
            }
        }

        if (! objectiveToStudyIds.isEmpty()) {
            Set<Id> legalHoldStudyIds = new Set<Id>(new Map<Id, HealthCloudGA__CarePlanTemplate__c>([
                SELECT Id
                FROM HealthCloudGA__CarePlanTemplate__c
                WHERE VTD1_Legal_Hold__c = true
                AND Id IN :objectiveToStudyIds.values()
            ]).keySet());

            for (Objective__c item : recs) {
                if (objectiveToStudyIds.containsKey(item.Id) && legalHoldStudyIds.contains(objectiveToStudyIds.get(item.Id))) {
                    item.addError(VT_D1_LegalHoldDeleteValidator.ERROR_MSG);
                }
            }
        }
    }

}