/**
 * @description Created by Alexander Komarov on 09.04.2020.
 */
@isTest
public with sharing class VT_R4_RestMobileLoginFlowTest {
    public static void firstTest() {
        extendSetup();
        User user = [SELECT Id, ContactId, Contact.Account.Id FROM User WHERE Username = :VT_R5_AllTestsMobile.PT_USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(user) {
            RestContext.request = new RestRequest();
            RestContext.response = new RestResponse();

            RestContext.request.requestURI = '/Patient/Loginflow';
            RestContext.request.httpMethod = 'GET';

            VT_R4_RestMobileLoginFlow.getLoginFlowState();
            VT_R4_RestMobileLoginFlow.parsedParams = new VT_R4_RestMobileLoginFlow.RequestParams();

            System.debug('test iteration 1');
            RestContext.request.params.put('deviceUniqueCode', 'testDeviceId');
            VT_R4_RestMobileLoginFlow.getLoginFlowState();
            System.debug('test iteration 2');
            user.First_Log_In__c = System.now().date();
            update user;
            VT_R4_RestMobileLoginFlow.getLoginFlowState();
            System.debug('test iteration 3');
            user.VTR3_Verified_Mobile_Devices__c = 'AUTOTEST;';
            update user;
            VT_R4_RestMobileLoginFlow.getLoginFlowState();
            System.debug('test iteration 4');

            VT_R4_RestMobileLoginFlow.RequestParams params = new VT_R4_RestMobileLoginFlow.RequestParams();
            params.stage = VT_R4_RestMobileLoginFlow.VERIFICATION_CODE_STAGE;
            RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
            VT_R4_RestMobileLoginFlow.postAction();
            System.debug('test iteration 5');

            params.deviceUniqueCode = 'testDeviceId';
            RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
            VT_R4_RestMobileLoginFlow.postAction();
            System.debug('test iteration 6');

            user.VTR3_Verified_Mobile_Devices__c = '';
            update user;
            VT_R4_RestMobileLoginFlow.postAction();
            System.debug('test iteration 7');

            params.verificationCode = 'wrong_code_';
            RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
            VT_R4_RestMobileLoginFlow.postAction();
            System.debug('test iteration 8');

            params.stage = VT_R4_RestMobileLoginFlow.PRIVACY_NOTICES_STAGE;
            RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
            VT_R4_RestMobileLoginFlow.postAction();
            System.debug('test iteration 9');

            params.isPrivacyNoticesAccepted = false;
            RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
            VT_R4_RestMobileLoginFlow.postAction();
            System.debug('test iteration 10');

            user.VTR3_Verified_Mobile_Devices__c = 'AUTOTEST;';
            update user;
            params.isPrivacyNoticesAccepted = true;
            RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
            VT_R4_RestMobileLoginFlow.postAction();
            System.debug('test iteration 11');


            params.stage = VT_R4_RestMobileLoginFlow.VERIFICATION_CODE_STAGE;
            RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
            VT_R4_RestMobileLoginFlow.postAction();
            System.debug('test iteration 12');

            params.stage = VT_R4_RestMobileLoginFlow.AUTHORIZATION_STAGE;
            RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
            VT_R4_RestMobileLoginFlow.postAction();
            System.debug('test iteration 13');

            params.isAuthorizationAccepted = false;
            RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
            VT_R4_RestMobileLoginFlow.postAction();
            System.debug('test iteration 14');

            params.isAuthorizationAccepted = true;
            RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
            VT_R4_RestMobileLoginFlow.postAction();
            System.debug('test iteration 15');
        }
        Test.stopTest();
    }

    private static void extendSetup() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        List<Knowledge__kav> articlesToInsert = new List<Knowledge__kav>();
        articlesToInsert.add(new Knowledge__kav(
                IsVisibleInCsp = true,
                IsVisibleInPkb = true,
                IsVisibleInPrm = true,
                Title = 'Privacy_Notice knowledge Title',
                VTD2_Study__c = study.Id,
                UrlName = 'UrlNamePrivacy-Notice',
                VTD2_Type__c = 'Privacy_Notice',
                Summary = 'Some summary for Privacy_Notice',
                VTD1_Content__c = 'ARTICLE CONTENT Privacy_Notice'));
        articlesToInsert.add(new Knowledge__kav(
                IsVisibleInCsp = true,
                IsVisibleInPkb = true,
                IsVisibleInPrm = true,
                Title = 'HIPAA knowledge Title',
                VTD2_Study__c = study.Id,
                UrlName = 'UrlName-HIPAA',
                VTD2_Type__c = 'HIPAA',
                Summary = 'Some summary for HIPAA',
                VTD1_Content__c = 'ARTICLE CONTENT HIPAA'));
        insert articlesToInsert;

        List<Knowledge__kav> articlesToPublish = [SELECT KnowledgeArticleId FROM Knowledge__kav WHERE VTD2_Study__c = :study.Id];
        List<User> listUsers = [SELECT Id, ContactId FROM User WHERE UserPermissionsKnowledgeUser = TRUE AND IsActive = TRUE AND Profile.Name = 'System Administrator'];
        if (!listUsers.isEmpty()) {
            User userToPublishArticles = listUsers.get(0);
            System.runAs(userToPublishArticles) {
                for (Knowledge__kav k : articlesToPublish) {
                    KbManagement.PublishingService.publishArticle(k.KnowledgeArticleId, true);
                }
            }
        }
    }

    public static void secondTest() {
        extendSetup();
        User user = [SELECT Id, ContactId, Contact.Account.Id FROM User WHERE Username = :VT_R5_AllTestsMobile.PT_USERNAME LIMIT 1];
        Test.startTest();

        System.runAs(user) {
            user.VTR3_Verified_Mobile_Devices__c = 'AUTOTEST;';
            user.VTD1_Privacy_Notice_Last_Accepted__c = System.now();
            user.HIPAA_Accepted__c = true;
            user.HIPAA_Accepted_Date_Time__c = System.now();
            update user;
            RestContext.request = new RestRequest();
            RestContext.response = new RestResponse();

            RestContext.request.requestURI = '/Patient/Loginflow';
            RestContext.request.httpMethod = 'GET';
            RestContext.request.params.put('deviceUniqueCode', 'testDeviceId');
            VT_R4_RestMobileLoginFlow.getLoginFlowState();
            System.debug('test iteration 16');

            VT_R4_RestMobileLoginFlow.RequestParams params = new VT_R4_RestMobileLoginFlow.RequestParams();
            System.debug('before initial setup');
            RestContext.request.httpMethod = 'POST';
            params.deviceUniqueCode = 'testDeviceId';
            params.stage = VT_R4_RestMobileLoginFlow.INITIAL_PROFILE_SETUP_STAGE;
            params.firstName = 'newFirstName';
            params.lastName = 'newLastName';
            params.middleName = 'newMiddleName';
            params.phoneNumber = '+12(31)23-32-13';
            params.zipCode = '123123';
            params.state = 'NY';
            params.dateOfBirth = System.now().addYears(-40).getTime();
            params.birthDate = '1960';
            RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
            VT_R4_RestMobileLoginFlow.postAction();

            params.preferredContactMethod = 'Email';
            RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
            VT_R4_RestMobileLoginFlow.postAction();

            RestContext.request.httpMethod = 'GET';
            VT_R4_RestMobileLoginFlow.getLoginFlowState();


            RestContext.request.httpMethod = 'PUT';
            params.userID = user.Id;
            RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
            VT_R4_RestMobileLoginFlow.resendCode();

            RestContext.request.requestBody = Blob.valueOf('deserialization fail');
            VT_R4_RestMobileLoginFlow.resendCode();
        }
        Test.stopTest();
    }
    public static void thirdTest() {
        extendSetup();
        User user = [SELECT Id, ContactId, Contact.Account.Id FROM User WHERE Username = :VT_R5_AllTestsMobile.PT_USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(user) {
            VT_R5_MobileLoginFlow.getLoginFlowState();
            user.First_Log_In__c = System.now().date();
            update user;
            VT_R5_MobileLoginFlow.getLoginFlowState();
            user.VTR3_Verified_Mobile_Devices__c = 'AUTOTEST;';
            update user;
            VT_R5_MobileLoginFlow.getLoginFlowState();
            VT_R5_MobileAuth.parsedParams = new VT_R5_MobileAuth.RequestParams();
            VT_R5_MobileAuth.parsedParams.stage = VT_R5_MobileLoginFlow.VERIFICATION_CODE_STAGE;
            VT_R5_MobileLoginFlow.setUserAndDevice('testDeviceId', user.Id);
            VT_R5_MobileLoginFlow.postAction();
            VT_R5_MobileAuth.parsedParams.verificationCode = 'wrong_code_';
            VT_R5_MobileLoginFlow.postAction();
            VT_R5_MobileAuth.parsedParams.stage = VT_R5_MobileLoginFlow.PRIVACY_NOTICES_STAGE;
            VT_R5_MobileAuth.parsedParams.isPrivacyNoticesAccepted = false;
            VT_R5_MobileLoginFlow.postAction();

            user.VTR3_Verified_Mobile_Devices__c = 'AUTOTEST;';
            update user;
            VT_R5_MobileAuth.parsedParams.isPrivacyNoticesAccepted = true;
            VT_R5_MobileLoginFlow.postAction();

            VT_R5_MobileAuth.parsedParams.stage = VT_R5_MobileLoginFlow.AUTHORIZATION_STAGE;
            VT_R5_MobileAuth.parsedParams.isAuthorizationAccepted = false;
            VT_R5_MobileLoginFlow.postAction();

            VT_R5_MobileAuth.parsedParams.isAuthorizationAccepted = true;
            VT_R5_MobileLoginFlow.postAction();

            VT_R5_MobileLoginFlow.resendCode();
        }
        Test.stopTest();
    }

    public static void fourthTest() {
        extendSetup();
        User user = [SELECT Id, ContactId, Contact.Account.Id FROM User WHERE Username = :VT_R5_AllTestsMobile.PT_USERNAME LIMIT 1];
        Test.startTest();

        System.runAs(user) {
            user.VTR3_Verified_Mobile_Devices__c = 'AUTOTEST;';
            user.VTD1_Privacy_Notice_Last_Accepted__c = System.now();
            user.HIPAA_Accepted__c = true;
            user.HIPAA_Accepted_Date_Time__c = System.now();
            update user;

            VT_R5_MobileAuth.parsedParams = new VT_R5_MobileAuth.RequestParams();
            VT_R5_MobileAuth.parsedParams.userID = user.Id;

            VT_R5_MobileLoginFlow.getLoginFlowState();

            VT_R5_MobileAuth.parsedParams.deviceUniqueCode = 'testDeviceId';
            VT_R5_MobileAuth.parsedParams.stage = VT_R5_MobileLoginFlow.INITIAL_PROFILE_SETUP_STAGE;
            VT_R5_MobileAuth.parsedParams.firstName = 'newFirstName';
            VT_R5_MobileAuth.parsedParams.lastName = 'newLastName';
            VT_R5_MobileAuth.parsedParams.middleName = 'newMiddleName';
            VT_R5_MobileAuth.parsedParams.phoneNumber = '+12(31)23-32-13';
            VT_R5_MobileAuth.parsedParams.zipCode = '123123';
            VT_R5_MobileAuth.parsedParams.state = 'NY';
            VT_R5_MobileAuth.parsedParams.dateOfBirth = System.now().addYears(-40).getTime();
            VT_R5_MobileAuth.parsedParams.birthDate = '1960';
            VT_R5_MobileLoginFlow.postAction();

            VT_R5_MobileAuth.parsedParams.preferredContactMethod = 'Email';
            VT_R5_MobileLoginFlow.postAction();
        }
        Test.stopTest();
    }
}