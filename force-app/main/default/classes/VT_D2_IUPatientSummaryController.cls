public without sharing class VT_D2_IUPatientSummaryController {
    public class InputSelectRatioElement implements Comparable {
        public String key;
        public Boolean value;

        public Integer compareTo(Object compareTo) {
            InputSelectRatioElement compareToItem = (InputSelectRatioElement)compareTo;
            if (key != compareToItem.key) {
                return CASE_STATUS_ORDER.get(key) < CASE_STATUS_ORDER.get(compareToItem.key) ? -1 : 1;
            }
            return 0;
        }
    }

    public class PatientSummary {
        public Case cas;
        public String phone;
        public String address;
        public String internalSubjectId;
        public String eligibilityStatus = '';//Added By Mona Bhutada SH-13732
        public List<InputSelectRatioElement> patientStatusInputSelectRatioElementList;
        public List<InputSelectRatioElement> patientReasonCodeInputSelectRatioElementList;
        public List<InputSelectRatioElement> patientEligibilityStatusInputSelectRatioElementList;

        public Boolean requiresExternalSubjectId = false;

        public Boolean canReadPersonalInfo = true;
        public Boolean canReadName = true;

        public Boolean canEdit = false;
        public Boolean canRandomize = false;
        public Boolean canTransfer = false;
        public Boolean isTransferInProgress = false;
        public Boolean isVieweligibilityStatus = true;//Added By Mona Bhutada SH-13732

        public Boolean canEditPatientId = false;
        public Boolean isRequiredIdChangeReasonField = false;
    }

    private static final Map<String, Integer> CASE_STATUS_ORDER = new Map<String, Integer> {
            'Pre-Consent' => 1,
            'Consented' => 2,
            'Does Not Qualify' => 3,
            'Screened' => 4,
            'Screen Failure' => 5,
            'Washout/Run-In' => 6,
            'Washout/Run-In Failure' => 7,
            'Active/Randomized' => 8,
            'Dropped' => 9,
            'Dropped Treatment,In F/Up' => 10,
            'Dropped Treatment,Compl F/Up' => 11,
            'Completed' => 12,
            'Compl Treatment,In F/Up' => 13,
            'Compl Treatment,Dropped F/Up' => 14
    };

    @AuraEnabled
    public static String getPatientSummary(String caseId) {
        try {
            caseId = VT_D2_CreatePCFFromCaseController.getCaseId(caseId);
            PatientSummary patientSummary = new PatientSummary();

            Case c = [
                    SELECT
                            Id,
                            Status,
                            ContactId,
                            Contact.Name,
                            Contact.Birthdate,
                            Contact.HealthCloudGA__Gender__c,
                            VTD1_Randomized_Date1__c,
                            VTD1_Enrollment_Date__c,
                            VTD1_Subject_ID__c,
                            VTD1_Days_Since_Referral__c,
                            VTD1_SecureConsentID__c,
                            VTD1_Patient_User__r.LastLoginDate,
                            VTD1_Patient_User__r.FullPhotoUrl,
                            VTD1_Patient_User__r.ProfileId,
                            VTD1_CreatedInCenduitCSM__c,
                            VTD1_Randomization_Status__c,
                            VTD1_Reattempts_of_Randomization__c,
                            VTR5_Internal_Subject_Id__c,
                            VTD1_Study__r.Name,
                            VTD1_Study__r.VTD2_TMA_Blinded_for_Study__c,
                            VTD1_Study__r.VTR5_Requires_External_Subject_Id__c,
                            VTD1_Study__r.VTR5_External_Subject_ID_Input_Type__c,
                            VTD1_Study__r.VTR5_IRTSystem__c,
                            VTD2_Study_Geography__r.VT_R3_Date_Of_Birth_Restriction__c,
                            VTD1_Reason_Code__c,
                            VTR2_Receive_SMS_Visit_Reminders__c,
                            VTR2_Receive_SMS_New_Notifications__c,
                            VTR2_Receive_SMS_eDiary_Notifications__c,
                            VTR3_Dropped_OtherReason_Description__c,
                            VTR2_IsAvailableForRandomize__c,
                            VTR2_Eligibility_Conducted_Outside_SH__c,
                            VTD1_Eligibility_Status__c
                    FROM Case
                    WHERE Id = :caseId
            ];
            System.debug('c--> ' + c.VTD1_Study__r.VTR5_Requires_External_Subject_Id__c);
            VT_D1_Phone__c [] phones = [SELECT PhoneNumber__c FROM VT_D1_Phone__c WHERE VTD1_Contact__c = :c.ContactId AND IsPrimaryForPhone__c = TRUE];
            if (phones.size() > 0) {
                patientSummary.phone = phones[0].PhoneNumber__c;
            }

            Address__c [] addresses = [SELECT City__c, toLabel(State__c), toLabel(Country__c) FROM Address__c WHERE VTD1_Contact__c = :c.ContactId AND VTD1_Primary__c = TRUE];
            if (addresses.size() > 0) {
                patientSummary.address = ((addresses[0].City__c != null) ? addresses[0].City__c + ', ' : '') + addresses[0].State__c + ', ' + addresses[0].Country__c;
            }

            patientSummary.cas = c;
            patientSummary.requiresExternalSubjectId = c.VTD1_Study__r.VTR5_Requires_External_Subject_Id__c;
            patientSummary.internalSubjectId = c.VTR5_Internal_Subject_Id__c;
            patientSummary.isVieweligibilityStatus = c.VTR2_Eligibility_Conducted_Outside_SH__c;
            //Added By Mona Bhutada SH-13732
            patientSummary.eligibilityStatus = c.VTD1_Eligibility_Status__c;
            //Added By Mona Bhutada SH-13732
            List<InputSelectRatioElement> patientStatusInputSelectRatioElementListAll = getInputSelectRatioList(c, 'Status', c.Status, false);
            List<InputSelectRatioElement> patientStatusInputSelectRatioElementList = new List<InputSelectRatioElement>();
            for (InputSelectRatioElement inputSelectRatioElement : patientStatusInputSelectRatioElementListAll){
                if (inputSelectRatioElement.key != 'Open' && inputSelectRatioElement.key != 'Closed'){
                    patientStatusInputSelectRatioElementList.add(inputSelectRatioElement);
                }
            }
            patientStatusInputSelectRatioElementList.sort();
            patientSummary.patientStatusInputSelectRatioElementList = patientStatusInputSelectRatioElementList;
            patientSummary.patientReasonCodeInputSelectRatioElementList = getInputSelectRatioList(c, 'VTD1_Reason_Code__c', c.VTD1_Reason_Code__c, false);
            patientSummary.patientEligibilityStatusInputSelectRatioElementList=  getInputSelectRatioList(c, 'VTD1_Eligibility_Status__c', c.VTD1_Eligibility_Status__c, false);

            // *******
            // set all permissions in one place (SH-4943). Vlad Tyazhov. 2019-11-19
            String profileName = VT_D1_HelperClass.getUserProfileName(UserInfo.getUserId());

            // hide patient name from all TMAs (SH-6268)
            if (profileName == VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME) {
                patientSummary.canReadName = false;
            }

            // hide PII from TMAs on blinded studies (SH-6268)
            if (profileName == VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME && c.VTD1_Study__r.VTD2_TMA_Blinded_for_Study__c) {
                patientSummary.canReadPersonalInfo = false;
            }

            Boolean caseReadOnlyAccess = false;
            Id profileId = UserInfo.getProfileId();
            for (Id pId : VTD1_RTId__c.getInstance().VTD2_READ_ONLY_Access_to_Case__c.split(',')) {
                if(pId == profileId){
                    caseReadOnlyAccess = true;
                    break;
                }
            }
            if (!caseReadOnlyAccess && (
                    profileName == VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME || profileName == VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME ||
                    profileName == 'System Administrator' || profileName == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME)
            ) {
                patientSummary.canEdit = true;
            }


            { // SH-15999
                Boolean manuallyEditable = c.VTD1_Study__r.VTR5_Requires_External_Subject_Id__c
                        && c.VTD1_Study__r.VTR5_External_Subject_ID_Input_Type__c == 'Manual';
                if (manuallyEditable) {
                    // SC/Admin can edit if populated
                    List<String> whoCanEditIdIfPopulated = new List<String>{
                            VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME, 'System Administrator'
                    };
                    Boolean canUserEditIfPopulated = String.isNotEmpty(c.VTD1_Subject_ID__c) && whoCanEditIdIfPopulated.contains(profileName);
                    // SC/PG/Admin can edit if empty
                    List<String> whoCanEditIdIfEmpty = new List<String>{ VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME };
                    whoCanEditIdIfEmpty.addAll(whoCanEditIdIfPopulated);
                    Boolean canUserEditIfEmpty = String.isEmpty(c.VTD1_Subject_ID__c) && whoCanEditIdIfEmpty.contains(profileName);
                    // Editability condition
                    if (canUserEditIfEmpty || canUserEditIfPopulated) {
                        patientSummary.canEditPatientId = true;
                    }
                    if (canUserEditIfPopulated) {
                        patientSummary.isRequiredIdChangeReasonField = true;
                    }
                }
            } // SH-15999


            if (c.VTR2_IsAvailableForRandomize__c) {
                patientSummary.canRandomize = true;
            }

            if (profileName == VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME || profileName == 'System Administrator') {
                patientSummary.canTransfer = true;
            }
            patientSummary.isTransferInProgress = !([
                    SELECT Id
                    FROM HealthCloudGA__CandidatePatient__c
                    WHERE VTR4_CaseForTransfer__c = :caseId
                            AND Conversion__c = 'Draft'
            ].isEmpty());
            return JSON.serialize(patientSummary);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static void updateCaseStatus(String caseId, String caseStatus, String reasonCode, String reasonToOther) {
        try {
            Case c = [SELECT Id, Status, VTR3_Dropped_OtherReason_Description__c FROM Case WHERE Id = :caseId];
            c.Status = caseStatus;
            if (caseStatus == VT_R4_ConstantsHelper_AccountContactCase.CASE_DROPPED
                    || caseStatus == VT_R4_ConstantsHelper_AccountContactCase.CASE_DROPPED_IN_FOLLOW_UP
                    || caseStatus == VT_R4_ConstantsHelper_AccountContactCase.CASE_DROPPED_COMPL_FOLLOW_UP
                    || caseStatus == VT_R4_ConstantsHelper_AccountContactCase.CASE_COMPL_DROPPED_FOLLOW_UP) {
                c.VTD1_Reason_Code__c = reasonCode;
                c.VTR3_Dropped_OtherReason_Description__c = reasonToOther;
            }
            System.debug(c);
            update c;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }


    @AuraEnabled (Cacheable = true)
    public static String getLogReportId(){
        return VT_D1_CaseControllerRemote.getLogReportId();
    }

    @AuraEnabled
    public static String randomizeRemote(Id caseId){
        return VT_D1_CaseControllerRemote.randomizeRemote(caseId);
    }

    @AuraEnabled
    public static String getLink(Id caseId){
        return VT_R3_SecurePortalFilesUploadController.generateLink(caseId, 8);
    }

    public static List<InputSelectRatioElement> getInputSelectRatioList(SObject sObj, String fieldName, String fieldValue, Boolean addEmptyKey){
        //Boolean inputSelectRatioElementEmpty = true;
        List<InputSelectRatioElement> selectRatioElementList = new List<InputSelectRatioElement>();
        List<String> optionsList = VT_D1_CustomMultiPicklistController.getSelectOptions(sObj, fieldName);
        for (String option : optionsList) {
            InputSelectRatioElement inputSelectRatioElement = new InputSelectRatioElement();
            inputSelectRatioElement.key = option;
            if (fieldValue == option){
                inputSelectRatioElement.value = true;
                //inputSelectRatioElementEmpty = false;
            } else {
                inputSelectRatioElement.value = false;
            }
            selectRatioElementList.add(inputSelectRatioElement);
        }
        if (addEmptyKey){
            InputSelectRatioElement inputSelectRatioElement = new InputSelectRatioElement();
            inputSelectRatioElement.key = '';
            inputSelectRatioElement.value = true;
            selectRatioElementList.add(inputSelectRatioElement);
        }
        return selectRatioElementList;
    }

    @AuraEnabled(Cacheable=true)
    public static String getCurrentUserProfile() {
        return [SELECT Name FROM Profile WHERE Id=:UserInfo.getProfileId()].Name;
    }

    //region SH-15999
    @AuraEnabled
    public static Boolean updatePatientId(Id caseId, String newPatientId, String idChangeReason) {
        Case ptCase = new Case(
            Id = caseId,
            VTD1_Subject_ID__c = newPatientId
        );
        if (String.isNotEmpty(idChangeReason)) {
            ptCase.VTR5_Reason_for_Patient_ID_Change__c = idChangeReason;
        }
        try {
            update ptCase;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
        // returns true if user has ability to edit again
        return (new List<String>{
            VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME,
            'System Administrator'
        }).contains(getCurrentUserProfile());
    }
    //endregion SH-15999

    /************************
    * @description Updated case with Eligibility Status Created by Mona Bhutada
    * @param Referenced on Case Object
    * @param RecordId and VTD1_Eligibility_Status__c fields
    */
    @AuraEnabled
    public static void updateCaseEligibilityStatus(String caseId, String eligibilityStatus) {
        try {
            List<Case> casList = [
                SELECT Id, VTD1_Eligibility_Status__c
                FROM Case
                WHERE Id = :caseId AND VTR2_Eligibility_Conducted_Outside_SH__c = TRUE
            ];
           
            if (!casList.isEmpty()) {
                casList[0].VTD1_Eligibility_Status__c = eligibilityStatus;
                update casList;
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
}