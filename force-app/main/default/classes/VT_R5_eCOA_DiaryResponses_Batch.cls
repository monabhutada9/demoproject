/**
* @author: Carl Judge
* @date: 18-Apr-20
* @description: Scheduled job to pull newly completed diaries from eCOA and create matching diaries in Salesforce
**/

public without sharing class VT_R5_eCOA_DiaryResponses_Batch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {
    /*
        to run:
        VT_R5_eCOA_DiaryResponses_Batch.run();
     */

    @TestVisible
    private static String cronId;
    public static final Integer REPEAT_INTERVAL_MINUTES = VT_R4_ConstantsHelper_Protocol_ePRO.DIARY_SETTINGS.VTR5_DiaryResponseInterval__c.intValue();
    public static final Integer SCOPE_SIZE = 1;
    private Datetime startTime = Datetime.now();
    private Boolean maxPagesReached = false;

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
            SELECT Id, VTR5_eCOA_Guid__c, VTR5_eCOA_Export_Timestamp__c, VTR5_eCoaOffsetTracker__c
            FROM HealthCloudGA__CarePlanTemplate__c
            WHERE VTR5_eCOA_Guid__c != NULL
            AND (VTR5_eDiaryTool__c = 'eCOA' OR VTR5_SourceFormToolSelection__c = 'eCOA ClinRO')
        ]);
    }

    public void execute(Database.BatchableContext bc, List<HealthCloudGA__CarePlanTemplate__c> scope){
        for (HealthCloudGA__CarePlanTemplate__c study : scope) {
            VT_R5_eCoaQueryBuilder queryBuilder = VT_R5_eCoaQueryBuilder.getDefaultQuery();
            if (study.VTR5_eCOA_Export_Timestamp__c != null) {
                Long lastRunTimeStamp = (Long)study.VTR5_eCOA_Export_Timestamp__c;
                VT_R5_eCoaQueryBuilder.TypeDbFilterNumberRange minCompletionDateFilter = new VT_R5_eCoaQueryBuilder.TypeDbFilterNumberRange('recordTimeUtc')
                    .setMin(lastRunTimeStamp)
                    .setMinInclusive(false);
                queryBuilder.addFilter(minCompletionDateFilter);
            }
            Long jobRunTimestamp = Datetime.now().getTime();
            VT_R5_eCoaQueryBuilder.TypeDbFilterNumberRange maxCompletionDateFilter = new VT_R5_eCoaQueryBuilder.TypeDbFilterNumberRange('recordTimeUtc')
                .setMax(jobRunTimestamp)
                .setMaxInclusive(true);
            queryBuilder.addFilter(maxCompletionDateFilter);

            Integer offset = study.VTR5_eCoaOffsetTracker__c != null ? study.VTR5_eCoaOffsetTracker__c.intValue() : 0;
            Integer totalRows = 0; // track total rows retrieved in this execute
            Integer maxRows = VT_R4_ConstantsHelper_Protocol_ePRO.DIARY_SETTINGS.VTR5_DiaryResponseMaxExecuteRows__c.intValue();
            
            VT_R5_eCoaResponseExport.Response masterResponse; // merge paged responses here

            // stop doing callouts if we have no more pages to get(offset will be null), or if we reach a defined number of rows (we start to reach CPU limit if we insert too manny diaries at once) or if we reach callout limits
            while (offset != null && Limits.getCallouts() < Limits.getLimitCallouts() && totalRows + queryBuilder.pageSize <= maxRows) {
                queryBuilder.setOffset(offset);
                VT_D1_HTTPConnectionHandler.Result result = VT_R5_eCoaResponseExport.getResponseExport(study.VTR5_eCOA_Guid__c, queryBuilder);
                VT_R5_eCoaResponseExport.Response currentResponse = VT_R5_eCoaResponseExport.getResponseFromResult(result);

                // merge response to master response
                if (masterResponse == null) {
                    masterResponse = currentResponse;
                } else {
                    masterResponse.data.rows.addAll(currentResponse.data.rows);
                }

                totalRows += currentResponse.data.rows.size();
                offset = currentResponse.queryResults.paging.offsetNext;
            }

            if (offset != null) { // we dropped out of the loop because of limits. we need to continue in another batch run
                this.maxPagesReached = true;
                study.VTR5_eCoaOffsetTracker__c = offset;
            } else {
                study.VTR5_eCoaOffsetTracker__c = 0;
                study.VTR5_eCOA_Export_Timestamp__c = jobRunTimestamp;
            }
            
            new VT_R5_eCOAeDiaryCreator(masterResponse).doCreate();
            update study;
        }
    }

    public void finish(Database.BatchableContext bc) {
        if(!Test.isRunningTest() && cronId == null){
            if (this.maxPagesReached) {
                Database.executeBatch(new VT_R5_eCOA_DiaryResponses_Batch(), SCOPE_SIZE);
            } else {
                reschedule();
            }
        }
    }

    @TestVisible
    private void reschedule(){
        // most diaries are expected to expire on the hour, so we want to run the job as soon as possible after this, regardless of repeat interval
        Integer minutesFromNow = 0;

        if (this.startTime.hour() >= Datetime.now().hour()) { // if we have passed an hour mark while the job was running, we want to run again right away
            Integer minutesPastHour = System.now().addMinutes(REPEAT_INTERVAL_MINUTES).minute();
            minutesFromNow = minutesPastHour < REPEAT_INTERVAL_MINUTES // if repeat interval takes us part an hour mark, reduce interval so that the job runs on the hour
                ? REPEAT_INTERVAL_MINUTES - minutesPastHour
                : REPEAT_INTERVAL_MINUTES;
        }

        if (minutesFromNow == 0) {  // system.scheduleBatch won't accept 0 as a param
            Database.executeBatch(new VT_R5_eCOA_DiaryResponses_Batch(), SCOPE_SIZE);
        }  else {
            run(minutesFromNow);
        }
    }

    public static void run(Integer minutesFromNow){
        cronId = System.scheduleBatch(new VT_R5_eCOA_DiaryResponses_Batch(), 'Diary responses Processing ' + System.now(), minutesFromNow, SCOPE_SIZE);
    }
    public static void run(){
        run(REPEAT_INTERVAL_MINUTES);
    }
}