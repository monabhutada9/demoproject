/**
 * @author: Alexander Komarov
 * @date: 02.12.2020
 * @description:
 */
@IsTest
public with sharing class VT_R5_MobilePISCRHomepageTest {
    static User u;
    static {
        u = [SELECT Id FROM User WHERE Username =:VT_R5_AllTestsMobile.PI_USERNAME];
        Case c = [SELECT Id FROM Case LIMIT 1];
        insert new CaseShare(CaseId =c.Id, UserOrGroupId = u.Id, CaseAccessLevel = 'Read');
    }
    public static void firstTest() {
        System.runAs(u) {
            System.debug([SELECT Id FROM Case]);
            System.debug([SELECT Id, VTD1_Status__c FROM VTD1_Actual_Visit__c]);
            new VT_R5_MobilePISCRHomepage().getMinimumVersions();
            VT_R5_AllTestsMobile.setRestContext('/mobile/v1/homepage', 'GET');
            VT_R5_MobileRouter.doGET();
        }
    }
    public static void secondTest() {
        System.runAs(u) {
            VT_R5_AllTestsMobile.setRestContext('/mobile/v0/homepage', 'GET');
            VT_R5_MobileRouter.doPOST();
        }
    }
}