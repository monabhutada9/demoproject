/**
 * Created by user on 05-Feb-20.
 */

global class VT_R4_MoveUsersToGroupBatch implements Database.Batchable<sObject> {
	global Database.QueryLocator start(Database.BatchableContext bc) {
		String queryString = Test.isRunningTest() ? 'SELECT Id, ProfileId, VTD1_Profile_Name__c, VTR4_Profile_UserType__c FROM User WHERE Profile.Name = \'System Administrator\' LIMIT 1'
													: 'SELECT Id, ProfileId, VTD1_Profile_Name__c, VTR4_Profile_UserType__c FROM User';
		return Database.getQueryLocator(queryString);
	}
	global void execute(Database.BatchableContext bc, List<User> userList){
		VT_D1_UserTriggerHandler.moveUsersToGroup(userList);
	}
	global void finish(Database.BatchableContext bc){
	}
}