global class VT_D1_batch_None_Logged_In_Patient_Check implements Database.Batchable<SObject> {
    // Task notifications batch class for SHA-1062
    // Notifies RE about users not logged in agreed time
    private final RecruitmentTaskCreationService creationService;

    public VT_D1_batch_None_Logged_In_Patient_Check() {
        this.creationService = new RecruitmentTaskCreationService();
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(new QueryBuilder(Case.class)
                .addField(Case.VTD1_StudyMaximumDaysWithoutLoggingIn__c)
                .addField(Case.ContactId)
                .addField(Case.VTD_1_Not_Logged_In_For__c)
                .addField(Case.VTD1_Primary_PG__c)
                .addField(Case.VTD1_Patient_User__c)
                .addField('VTD1_Patient_User__r.Name')
                .addField(Case.VTD1_Subject_ID__c)
                .addField(Case.VTD1_Study__c)
                .addConditions()
                .add(new QueryBuilder.CompareCondition(Case.VTD_1_Not_Logged_In_For__c).gt(0))
                .add(new QueryBuilder.CompareCondition(Case.VTD1_StudyMaximumDaysWithoutLoggingIn__c).gt(0))
                .add(new QueryBuilder.NullCondition(Case.VTD1_Patient_User__c).notNull())
                .add(new QueryBuilder.NullCondition(Case.VTD1_Study__c).notNull())
                .setConditionOrder('1 AND 2 AND 3 AND 4')
                .endConditions()
                .addOrderAsc(Case.VTD1_Study__c)
                .toString()
        );

    }

    global void execute(Database.BatchableContext bc, List<Case> scope) {
        this.creationService.createTasks(scope);
    }

    global void finish(Database.BatchableContext bc) {

    }

    public class RecruitmentTaskCreationService {

        private final Id recordTypeIdReTask;
        private final Id recordTypeIdReStm;

        public RecruitmentTaskCreationService() {
            this.recordTypeIdReTask = Task.getSObjectType().getDescribe().getRecordTypeInfosByName().get('Task For RE').getRecordTypeId();
            this.recordTypeIdReStm = Study_Team_Member__c.getSObjectType().getDescribe().getRecordTypeInfosByName().get('RE').getRecordTypeId();
        }

        public void createTasks(List<Case> cases) {
            List<Task> result = new List<Task>();
            List<HealthCloudGA__CarePlanTemplate__c> studies = this.getStudies(cases);
            for (HealthCloudGA__CarePlanTemplate__c study : studies) {
                result.addAll(this.generateTask(study));
            }
            Database.insert(result);
        }

        private List<Task> generateTask(HealthCloudGA__CarePlanTemplate__c study) {
            List<Task> tasks = new List<Task>();
            if (study.Study_Team_Members__r == null || study.Study_Team_Members__r.isEmpty()) {
                return tasks;
            }
            for (Study_Team_Member__c studyTeamMember : study.Study_Team_Members__r) {
                for (Case caseRecord : study.Cases__r) {
                    tasks.add(this.createTask(studyTeamMember, caseRecord));
                }
            }
            return tasks;
        }

        private Task createTask(Study_Team_Member__c studyTeamMember, Case caseRecord) {
            return new Task(
                    OwnerId = studyTeamMember.User__c
                    , WhatId = caseRecord.Id
                    , RecordTypeId = this.recordTypeIdReTask
                    , WhoId = caseRecord.ContactId
                    , Subject = 'Investigate why ' + caseRecord.VTD1_Patient_User__r.Name + ' (' + caseRecord.VTD1_Subject_ID__c + ') hasn\'t logged in yet'
                    , VTD1_Case_lookup__c = caseRecord.Id
                    , HealthCloudGA__CarePlanTemplate__c = caseRecord.VTD1_Study__c
                    , Category__c = 'Follow Up Required'
                    , ActivityDate = Date.today()
            );
        }

        private List<HealthCloudGA__CarePlanTemplate__c> getStudies(List<Case> cases) {
            return [
                    SELECT
                    (SELECT User__c FROM Study_Team_Members__r WHERE RecordTypeId = :this.recordTypeIdReStm)
                            , (
                            SELECT
                                    Id
                                    , ContactId
                                    , VTD1_Patient_User__r.Name
                                    , VTD1_Subject_ID__c
                                    , VTD1_Study__c
                            FROM Cases__r
                    )
                    FROM HealthCloudGA__CarePlanTemplate__c
                    WHERE Id IN (SELECT VTD1_Study__c FROM Case WHERE Id IN :cases)
            ];
        }
    }
}