/**
 * Created by triestelaporte on 12/15/20.
 */

@IsTest(SeeAllData = false)
private class FutureActionProcessorTest {
    @IsTest
    static void testSimpleInsert () {

        Account testAccount = new Account();
        testAccount.Name = 'test account';

        String testAccountSerialized = JSON.serialize(testAccount);

        FutureActionQueueRecord__c testFutureAction = new FutureActionQueueRecord__c();
        testFutureAction.ActionDate__c = Date.today().addDays(-1);
        testFutureAction.ObjectType__c = 'Account';
        testFutureAction.ActionType__c = String.valueOf(FutureActionSettings.ACTION_TYPE.CREATE_RECORD);
        testFutureAction.SerializedRecord__c = testAccountSerialized;
        insert testFutureAction;

        Test.startTest();
        Database.executeBatch(new FutureActionProcessor(), 200);
        Test.stopTest();

        List<FutureActionQueueRecord__c> futureActionsAfterTest = [
                SELECT Id,
                        ErrorMessage__c
                FROM FutureActionQueueRecord__c
        ];
        System.assertEquals(0, futureActionsAfterTest.size(), 'Should be no queue records left, found:  ' + JSON.serializePretty(futureActionsAfterTest));

        List<Account> accountsAfterTest = [
                SELECT Id,
                        Name
                FROM Account
        ];
        System.assertEquals(1, accountsAfterTest.size(), 'Should be one account after test.');
        System.assertEquals(testAccount.Name, accountsAfterTest[0].Name, 'Account should be the proper name.');

    }

    @IsTest
    static void testSimpleUpdate () {

        Account testAccount = new Account();
        testAccount.Name = 'test account';
        insert testAccount;

        testAccount.Name = 'NOT TEST ACCOUNT';

        String testAccountSerialized = JSON.serialize(testAccount);

        FutureActionQueueRecord__c testFutureAction = new FutureActionQueueRecord__c();
        testFutureAction.ActionDate__c = Date.today().addDays(-1);
        testFutureAction.ObjectType__c = 'Account';
        testFutureAction.ActionType__c = String.valueOf(FutureActionSettings.ACTION_TYPE.UPDATE_RECORD);
        testFutureAction.SerializedRecord__c = testAccountSerialized;
        insert testFutureAction;

        Test.startTest();
        Database.executeBatch(new FutureActionProcessor(), 200);
        Test.stopTest();

        List<FutureActionQueueRecord__c> futureActionsAfterTest = [
                SELECT Id,
                        ErrorMessage__c
                FROM FutureActionQueueRecord__c
        ];
        System.assertEquals(0, futureActionsAfterTest.size(), 'Should be no queue records left, found:  ' + JSON.serializePretty(futureActionsAfterTest));

        List<Account> accountsAfterTest = [
                SELECT Id,
                        Name
                FROM Account
        ];
        System.assertEquals(1, accountsAfterTest.size(), 'Should be one account after test.');
        System.assertEquals(testAccount.Name, accountsAfterTest[0].Name, 'Account should be the proper name.');

    }

    @IsTest
    static void testSimpleDelete () {

        Account testAccount = new Account();
        testAccount.Name = 'test account';
        insert testAccount;

        String testAccountSerialized = JSON.serialize(testAccount);

        FutureActionQueueRecord__c testFutureAction = new FutureActionQueueRecord__c();
        testFutureAction.ActionDate__c = Date.today().addDays(-1);
        testFutureAction.ObjectType__c = 'Account';
        testFutureAction.ActionType__c = String.valueOf(FutureActionSettings.ACTION_TYPE.DELETE_RECORD);
        testFutureAction.SerializedRecord__c = testAccountSerialized;
        insert testFutureAction;

        Test.startTest();
        Database.executeBatch(new FutureActionProcessor(), 200);
        Test.stopTest();

        List<FutureActionQueueRecord__c> futureActionsAfterTest = [
                SELECT Id,
                        ErrorMessage__c
                FROM FutureActionQueueRecord__c
        ];
        System.assertEquals(0, futureActionsAfterTest.size(), 'Should be no queue records left, found:  ' + JSON.serializePretty(futureActionsAfterTest));

        List<Account> accountsAfterTest = [
                SELECT Id,
                        Name
                FROM Account
        ];
        System.assertEquals(0, accountsAfterTest.size(), 'Should be zero accounts after test.');

    }
}