public without sharing class VT_D1_SurveyHandler {

    private List<VTD1_Survey__c> internalDiaries = new List<VTD1_Survey__c>();
    private List<VTD1_Survey__c> externalDiaries = new List<VTD1_Survey__c>();
    public static final String ADD_DELETION_REASON_ERROR = 'Please add a reason for deletion before deleting this eDiary';


    public static Boolean ignoreDeleteValidation = false;






    //SH-19377: temporary workaround for "Oxygen Level and Heart Rate" diaries


    private final static String OXYGEN_AND_HEART = 'Oxygen Level and Heart Rate'; //*
    private final static Integer CLONE_COUNT = 2;//*

    public static Boolean skipOxygenCloning = false;//*




    public void onBeforeDelete(List<VTD1_Survey__c> old) {

        if (!ignoreDeleteValidation) {
            validateReasonForDeletion(old);

            VT_R5_DeletedRecordHandler.createDeletedRecords(old);
        }


        separateDiaries(old);
        if (!this.externalDiaries.isEmpty()) {
            beforeDeleteExternal(this.externalDiaries);
        }
    }

    public void onBeforeInsert(List<VTD1_Survey__c> recs) {
        separateDiaries(recs);

        VT_D1_SurveyHandlerHelper.populateAlertKey(recs, null);
        if (!this.internalDiaries.isEmpty()) {
            beforeInsertInternal(this.internalDiaries);
        }
        if (!this.externalDiaries.isEmpty()) {
            beforeInsertExternal(this.externalDiaries);
        }
    }

    public void onBeforeUpdate(List<VTD1_Survey__c> recs, Map<Id, VTD1_Survey__c> oldMap) {
        separateDiaries(recs);

        VT_D1_SurveyHandlerHelper.populateAlertKey(recs, oldMap);
        if (!this.internalDiaries.isEmpty()) {
            beforeUpdateInternal(this.internalDiaries, oldMap);
        }
        if (!this.externalDiaries.isEmpty()) {
            beforeUpdateExternal(this.externalDiaries, oldMap);
        }
    }

    public void onAfterInsert(List<VTD1_Survey__c> recs) {
        separateDiaries(recs);
        if (!this.internalDiaries.isEmpty()) {
            afterInsertInternal(this.internalDiaries);
        }
        if (!this.externalDiaries.isEmpty()) {
            afterInsertExternal(this.externalDiaries);
        }

        //SH-19377: temporary workaround for "Oxygen Level and Heart Rate" diaries
        cloneOxygenEDiaries(recs); //some comment to deploy

    }

    public void onAfterUpdate(List<VTD1_Survey__c> recs, Map<Id, VTD1_Survey__c> oldMap) {
        separateDiaries(recs);
        if (!this.internalDiaries.isEmpty()) {
            afterUpdateInternal(this.internalDiaries, oldMap);
        }
        if (!this.externalDiaries.isEmpty()) {
            afterUpdateExternal(this.externalDiaries, oldMap);
        }
    }

    private void separateDiaries(List<VTD1_Survey__c> recs) {
        for (VTD1_Survey__c diary : recs) {
            if (VT_D1_SurveyHandlerHelper.isEcoaEproDiary(diary) || VT_D1_SurveyHandlerHelper.isEcoaClinRoDiary(diary)) {
                this.externalDiaries.add(diary);
            } else {
                this.internalDiaries.add(diary);
            }
        }
    }

    public void beforeDeleteExternal(List<VTD1_Survey__c> recs) {
        VT_D1_SurveyHandlerHelper.closeTasks(recs);
    }

    private void beforeInsertInternal(List<VTD1_Survey__c> recs) {
        new VT_R2_EDiaryTimeCalculator().calculateTimes(recs);
        for (VTD1_Survey__c item : recs) {
            item.VTR5_eDiaryLookup__c = item.VTD1_CSM__c;
        }
    }

    private void beforeInsertExternal(List<VTD1_Survey__c> recs) {


        VT_R5_eCoaMultidayDiaryProcess.onBeforeInsert(recs);//*


        for (VTD1_Survey__c item : recs) {
            if (!VT_D1_SurveyHandlerHelper.isEcoaClinRoDiary(item)) {
                item.VTR5_eDiaryLookup__c = item.VTD1_CSM__c;
            } else {
                item.VTR5_SourceFormLookup__c = item.VTD1_CSM__c;
            }
        }
    }

    private void beforeUpdateInternal(List<VTD1_Survey__c> recs, Map<Id, VTD1_Survey__c> oldMap) {
        List<VTD1_Survey__c> started = new List<VTD1_Survey__c>();
        List<VTD1_Survey__c> reset = new List<VTD1_Survey__c>();
        List<VTD1_Survey__c> scoringDone = new List<VTD1_Survey__c>();
        List<VTD1_Survey__c> submitted = new List<VTD1_Survey__c>();
        List<VTD1_Survey__c> dueDateChanged = new List<VTD1_Survey__c>();
        List<VTD1_Survey__c> reviewedDiaries = new List<VTD1_Survey__c>();


        for (VTD1_Survey__c item : recs) {
            VTD1_Survey__c old = oldMap.get(item.Id);

            if (item.VTD1_Status__c == VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_READY_TO_START &&
                    item.VTR2_Number_of_Answered_Answers__c > 0 && old.VTR2_Number_of_Answered_Answers__c == 0) {
                started.add(item);
            }
            if (item.VTD1_Status__c == VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_IN_PROGRESS &&
                    item.VTR2_Number_of_Answered_Answers__c == 0 && old.VTR2_Number_of_Answered_Answers__c > 0) {
                system.debug('RESET!!!');
                reset.add(item);
            }
            if (VT_D1_SurveyHandlerHelper.isScoringDone(item) && !VT_D1_SurveyHandlerHelper.isScoringDone(old)) {
                scoringDone.add(item);
            }
            if (VT_D1_SurveyHandlerHelper.isSubmitted(item) && !VT_D1_SurveyHandlerHelper.isSubmitted(old)) {
                submitted.add(item);
            }
            if (item.VTD1_Due_Date__c != old.VTD1_Due_Date__c) {
                dueDateChanged.add(item);
            }
            if (VT_D1_SurveyHandlerHelper.isReviewed(item) && !VT_D1_SurveyHandlerHelper.isReviewed(old)) {
                reviewedDiaries.add(item);
            }
        }

        if (!started.isEmpty()) {
            VT_D1_SurveyHandlerHelper.handleStarted(started);
        }
        if (!reset.isEmpty()) {
            VT_D1_SurveyHandlerHelper.handleReset(reset);
        }
        if (!scoringDone.isEmpty()) {
            VT_D1_SurveyHandlerHelper.setFieldsWhenScoringDone(scoringDone);
        }
        if (!submitted.isEmpty()) {
            new VT_D1_SurveySubmissionHandler().handleSubmissions(submitted);
            VT_D1_SurveyHandlerHelper.updateCaregiverCheckbox(submitted);
        }
        if (!dueDateChanged.isEmpty()) {
            new VT_R2_EDiaryTimeCalculator().calculateTimes(dueDateChanged);
        }
        if (!reviewedDiaries.isEmpty()) {
            VT_D1_SurveyHandlerHelper.updateReviewedCheckbox(reviewedDiaries);
        }
    }

    private void beforeUpdateExternal(List<VTD1_Survey__c> recs, Map<Id, VTD1_Survey__c> oldMap) {



        VT_R5_eCoaMultidayDiaryProcess.onBeforeUpdate(recs, oldMap);

    }

    private void afterInsertInternal(List<VTD1_Survey__c> recs) {


        VT_R5_eDiaryScheduledActionsHeroku.scheduleNotifications(recs);




        VT_D1_SurveyAnswerCreator.createAnswers(recs);


    }

    private void afterInsertExternal(List<VTD1_Survey__c> recs) {


        VT_R5_eCoaMultidayDiaryProcess.onAfterInsert(recs);//*



        List<VTD1_Survey__c> notCompletedSurveys = new List<VTD1_Survey__c>();
        List<VTD1_Survey__c> completedOrMissed = new List<VTD1_Survey__c>();

        for (VTD1_Survey__c item : recs) {
            if (VT_D1_SurveyHandlerHelper.isEcoaEproDiary(item) && !VT_D1_SurveyHandlerHelper.isCompleted(item)) {
                notCompletedSurveys.add(item);
            } else if (VT_D1_SurveyHandlerHelper.isCompleted(item) || VT_D1_SurveyHandlerHelper.isMissed(item)) {
                completedOrMissed.add(item);
            }
        }

        if (!notCompletedSurveys.isEmpty()) {
            VT_D1_SurveyHandlerHelper.createTasks(notCompletedSurveys);
        }

        if (!completedOrMissed.isEmpty()) {
            VT_D1_SurveyHandlerHelper.hideNotifications(completedOrMissed);
            VT_D1_SurveyHandlerHelper.closeTasks(completedOrMissed);
        }
    }

    private void afterUpdateInternal(List<VTD1_Survey__c> recs, Map<Id, VTD1_Survey__c> oldMap) {
        List<VTD1_Survey__c> hasSafetyConcern = new List<VTD1_Survey__c>();
        List<VTD1_Survey__c> submitted = new List<VTD1_Survey__c>();


        VT_R5_eDiaryScheduledActionsHeroku.isNeedScheduledAction(recs, oldMap.values());




        for (VTD1_Survey__c item : recs) {
            VTD1_Survey__c old = oldMap.get(item.Id);
            if (item.VTD1_Possible_ePRO_Safety_Concern__c && !old.VTD1_Possible_ePRO_Safety_Concern__c) {
                hasSafetyConcern.add(item);
            }
            if (VT_D1_SurveyHandlerHelper.isSubmitted(item) && !VT_D1_SurveyHandlerHelper.isSubmitted(old)) {
                submitted.add(item);
            }
        }

        if (!hasSafetyConcern.isEmpty()) {
            VT_D1_SurveyHandlerHelper.createPcfsForSafetyConcerns(hasSafetyConcern);
        }
        if (!submitted.isEmpty()) {
            VT_D1_SurveyHandlerHelper.autoScoreAnswers(submitted);
        }
    }

    private void afterUpdateExternal(List<VTD1_Survey__c> recs, Map<Id, VTD1_Survey__c> oldMap) {
        List<VTD1_Survey__c> completedOrMissed = new List<VTD1_Survey__c>();
        List<VTD1_Survey__c> hasSafetyConcern = new List<VTD1_Survey__c>();

        for (VTD1_Survey__c item : recs) {
            VTD1_Survey__c old = oldMap.get(item.Id);
            if (item.VTD1_Possible_ePRO_Safety_Concern__c && !old.VTD1_Possible_ePRO_Safety_Concern__c) {
                hasSafetyConcern.add(item);
            }
            if ((VT_D1_SurveyHandlerHelper.isMissed(item) && !VT_D1_SurveyHandlerHelper.isMissed(old))
                    || (VT_D1_SurveyHandlerHelper.isCompleted(item) && !VT_D1_SurveyHandlerHelper.isCompleted(old))) {
                completedOrMissed.add(item);
            }
        }
        if (!hasSafetyConcern.isEmpty()) {
            VT_D1_SurveyHandlerHelper.createPcfsForSafetyConcerns(hasSafetyConcern);
        }

        if (!completedOrMissed.isEmpty()) {
            VT_D1_SurveyHandlerHelper.hideNotifications(completedOrMissed);
            VT_D1_SurveyHandlerHelper.closeTasks(completedOrMissed);
        }
    }






    //SH-19377: temporary workaround for "Oxygen Level and Heart Rate" diaries


    private void cloneOxygenEDiaries(List<VTD1_Survey__c> recs){//*
        List<VTD1_Survey__c> eDiariesToInsert = new List<VTD1_Survey__c>();//*

        if(!skipOxygenCloning){//*
            for(VTD1_Survey__c eDiary : recs){//*
                if(eDiary.VTR5_eCoa_RuleName__c != null && eDiary.VTR5_eCoa_RuleName__c.startsWith(OXYGEN_AND_HEART)){//*

                    for(Integer i = 0; i < CLONE_COUNT; i++){//*
                        eDiariesToInsert.add(eDiary.clone());//*
                    }//*
                }//*
            }//*
        }//*
        if(!eDiariesToInsert.isEmpty()){//*

            skipOxygenCloning = true;//*
            insert eDiariesToInsert;//*
            skipOxygenCloning = false;//*

        }//*
    }//*
    //* - to merge with 5.5

    private void validateReasonForDeletion(List<VTD1_Survey__c> surveyList) {
        for(VTD1_Survey__c survey : surveyList) {
            if(String.isBlank(survey.VTR5_ReasonForDeletion__c)) {

                survey.addError(ADD_DELETION_REASON_ERROR);

            }
        }
    }




}