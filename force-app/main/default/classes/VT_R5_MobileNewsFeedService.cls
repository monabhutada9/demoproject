/**
 * @author: Alexander Komarov
 * @date: 07.12.2020
 * @description:
 */

public with sharing class VT_R5_MobileNewsFeedService {

    private static final Set<String> preRandomizedStatuses = new Set<String>{
            VT_R4_ConstantsHelper_AccountContactCase.CASE_PRE_CONSENT,
            VT_R4_ConstantsHelper_AccountContactCase.CASE_CONSENTED,
            VT_R4_ConstantsHelper_AccountContactCase.CASE_SCREENED
    };

    private static final String PRE_RANDOMIZED_VISIBILITY = 'Pre-randomized';
    private static final String RANDOMIZED_VISIBILITY = 'Active/Randomized';

    private Integer queryLimit;
    private Integer queryOffset;
    private String language;
    private String patientStudyId;
    private String patientStatusVisibility = '';

    public void setListQueryParams(Integer queryLimit, Integer queryOffset, String language, String studyId, String caseStatus) {
        this.queryLimit = queryLimit;
        this.queryOffset = queryOffset;
        this.language = language;
        this.patientStudyId = studyId;
        if (preRandomizedStatuses.contains(caseStatus)) {
            this.patientStatusVisibility = PRE_RANDOMIZED_VISIBILITY;
        } else if (caseStatus.equals(VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED)){
            this.patientStatusVisibility = RANDOMIZED_VISIBILITY;
        }
    }

    public List<NewsFeedItem> getNewsFeedList() {
        List<Knowledge__kav> articles = queryArticles();
        List<NewsFeedItem> result = new List<NewsFeedItem>();
        for (Knowledge__kav knowledgeArticle : articles) {
            result.add(new NewsFeedItem(knowledgeArticle, false));
        }
        return result;
    }

    public NewsFeedItem getNewsFeedItem (String articleId) {
        return new NewsFeedItem(getArticleWithContent(articleId), true);
    }

    private List<Knowledge__kav> queryArticles() {
        return [
                SELECT Id, VTR5_Source__c, VTR5_ArticlePreviewImage__c,
                        FirstPublishedDate, CreatedBy.Name, Title
                FROM Knowledge__kav
                WHERE RecordType.DeveloperName = :VT_R5_ConstantsHelper_Mobile.NEWS_FEED_RT_DEV_NAME
                AND (VTR5_VisibleInStatus__c = 'Both' OR VTR5_VisibleInStatus__c = :patientStatusVisibility)
                AND IsLatestVersion = TRUE AND PublishStatus = 'Online'
                AND Language = :language AND VTD2_Study__c = :patientStudyId ORDER BY FirstPublishedDate DESC
                LIMIT :queryLimit OFFSET :queryOffset
        ];
    }
    public Integer countArticles() {
        return [
                SELECT count()
                FROM Knowledge__kav
                WHERE RecordType.DeveloperName = :VT_R5_ConstantsHelper_Mobile.NEWS_FEED_RT_DEV_NAME
                AND (VTR5_VisibleInStatus__c = 'Both' OR VTR5_VisibleInStatus__c = :patientStatusVisibility)
                AND IsLatestVersion = TRUE AND PublishStatus = 'Online'
                AND Language = :language AND VTD2_Study__c = :patientStudyId
        ];
    }
    private Knowledge__kav getArticleWithContent(String articleId) {
        return [
                SELECT Id, VTR5_Source__c, VTR5_ArticlePreviewImage__c, VTD1_Content__c,
                        FirstPublishedDate, CreatedBy.Name, Title
                FROM Knowledge__kav
                WHERE RecordType.DeveloperName = :VT_R5_ConstantsHelper_Mobile.NEWS_FEED_RT_DEV_NAME
                AND Id = :articleId
        ];
    }

    public class NewsFeedItem {
        String previewImageLink;
        String authorName;
        String addedDate;
        String content;
        String source;
        String title;
        String id;

        NewsFeedItem(Knowledge__kav kk, Boolean withContent) {
            this.addedDate = String.valueOf(kk.FirstPublishedDate.date());
            this.source = kk.VTR5_Source__c;
            this.title = kk.Title;
            this.id = kk.Id;
            if (kk.VTR5_ArticlePreviewImage__c != null && kk.VTR5_ArticlePreviewImage__c.contains('refid=')) {
                this.previewImageLink = getImageLink(kk);
            }
            if (withContent) {
                this.authorName = kk.CreatedBy.Name;
                this.content = kk.VTD1_Content__c;
            }

        }

        private String getImageLink(Knowledge__kav kk) {
            if (baseUrl == null) {
                baseUrl = Url.getOrgDomainUrl().toExternalForm();
            }
            Integer refIndex = kk.VTR5_ArticlePreviewImage__c.indexOf('refid=') + 6;
            return baseUrl +
                    SERVICES_URL_PART +
                    kk.Id +
                    IMAGE_URL_PART +
                    kk.VTR5_ArticlePreviewImage__c.substring(refIndex, refIndex + 15);
        }
    }
    private static String baseUrl;
    private static final String SERVICES_URL_PART = '/services/data/v46.0/sobjects/Knowledge__kav/';
    private static final String IMAGE_URL_PART = '/richTextImageFields/VTR5_ArticlePreviewImage__c/';
}