public class VT_D1_PatientShipmentDetailsController {
    //NOT USED Controller for LC VT_D1_PatientShipmentDetailsForm
	 /*public class PatientShipmentDetails {
        public Contact contact;
        public Address__c address;
        public VT_D1_Phone__c phone;
        public VT_D1_Phone__c cell;
    }
    
      @AuraEnabled 
    public static String getShipmentDetails(String accountId) {
     	try{
            List<PatientShipmentDetails> patientShipmentDetailsList = new List<PatientShipmentDetails>();
            Contact [] contacts = [SELECT Id, Name, VTD1_LatestTimeAvailabletoAcceptPackage__c, VTD1_TimesNotAvailable__c, VTD1_DaysNotAvailable__c,
                               VTD1_EmailNotificationsFromCourier__c, VTD1_TextMessageCommunicationFromCourier__c, VTD1_PhoneCommunicationFromCourier__c
                               FROM Contact WHERE (AccountId =: accountId OR VTD1_Account__c =: accountId)];          
            List<String> contactsIdList = new List<String>();
            for (Contact contact : contacts) {
              contactsIdList.add(contact.Id);  
            } 

            Map<String, Address__c> contactIdToAddressMap = new Map<String, Address__c>(); 
            Address__c [] addresses = [SELECT Name, VTD1_Contact__c, City__c, State__c, ZipCode__c, VTD1_AddressType__c 
                                       FROM Address__c  WHERE VTD1_Contact__c IN : contactsIdList AND VTD1_Primary__c = TRUE];
            for (Address__c address : addresses) {
              contactIdToAddressMap.put(address.VTD1_Contact__c, address);
            } 

            Map<String, VT_D1_Phone__c> contactIdToPhonePhoneMap = new Map<String, VT_D1_Phone__c>(); 
            Map<String, VT_D1_Phone__c> contactIdToPhoneTextMap = new Map<String, VT_D1_Phone__c>(); 
            VT_D1_Phone__c [] phones = [SELECT VTD1_Contact__c, IsPrimaryForPhone__c, IsPrimaryForText__c, PhoneNumber__c 
                                        FROM VT_D1_Phone__c  WHERE VTD1_Contact__c IN : contactsIdList AND (IsPrimaryForPhone__c = TRUE OR IsPrimaryForText__c = TRUE)];
            for (VT_D1_Phone__c phone : phones) {
              if(phone.IsPrimaryForPhone__c == true) {
                contactIdToPhonePhoneMap.put(phone.VTD1_Contact__c, phone);
              }
              if(phone.IsPrimaryForText__c == true) {
                contactIdToPhoneTextMap.put(phone.VTD1_Contact__c, phone);
              }
            } 

            for (Contact contact : contacts) {
              PatientShipmentDetails patientShipmentDetails = new PatientShipmentDetails();
              patientShipmentDetails.contact = contact;
              patientShipmentDetails.address = contactIdToAddressMap.get(contact.Id);
              patientShipmentDetails.phone = contactIdToPhonePhoneMap.get(contact.Id);
              patientShipmentDetails.cell = contactIdToPhoneTextMap.get(contact.Id);
              patientShipmentDetailsList.add(patientShipmentDetails);
            }
            return JSON.serialize(patientShipmentDetailsList);
        }catch (Exception e){
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }    
    }*/
}