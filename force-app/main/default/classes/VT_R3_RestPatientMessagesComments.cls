/**
 * Created by Alexander Komarov on 25.04.2019.
 */

@RestResource(UrlMapping='/Patient/Messages/*')
global with sharing class VT_R3_RestPatientMessagesComments {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static final String COMMUNITY_URL = Url.getSalesforceBaseUrl().toExternalForm();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();
    static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());

    @HttpGet
    global static String getComments() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        String temp = request.requestURI.substringAfterLast('/');

        if (String.isBlank(temp) || !helper.isIdCorrectType(temp, 'FeedItem')) {
            response.statusCode = 400;
            cr.buildResponse(helper.forAnswerForIncorrectInput());
            return JSON.serialize(cr, true);
        }

        List<CommentObj> commentsList = new List<CommentObj>();

        Id postId = temp;
        commentsList = getCommentsProcess(postId);

        cr.buildResponse(commentsList);
        return JSON.serialize(cr, true);

    }

    @HttpPost
    global static String sendComment(String message, String fileName, String based64FileData) {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        String postId = request.requestURI.substringAfterLast('/');


        if (String.isBlank(postId) || !helper.isIdCorrectType(postId, 'FeedItem')) {
            response.statusCode = 400;
            return helper.forAnswerForIncorrectInput();
        } else if (message.length() > 10000) {
            response.statusCode = 400;
            cr.buildResponse('FAILURE.  Your comment can not have more than 10000 characters.');
            return JSON.serialize(cr, true);
        }
        try {
            if (String.isBlank(fileName) || String.isBlank(based64FileData)) {
                VT_D1_CommunityChat.sendComment(postId, message);
            } else {
                if (based64FileData.length() > 5000000) {
                    response.statusCode = 452;
                    cr.buildResponse('FAILURE.  Attachment file can not have more than 5000000 characters.');
                    return JSON.serialize(cr, true);
                }
            }
            VT_D1_CommunityChat.sendCommentWithFile(postId, message, fileName, based64FileData);

        } catch (Exception e) {
            response.statusCode = 500;
            cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName());
            return JSON.serialize(cr, true);
        }

        cr.buildResponse(getNewComment(postId));
        return JSON.serialize(cr, true);

    }

    private static CommentObj getNewComment(Id postId) {
        ConnectApi.CommentPage commentsPage = ConnectApi.ChatterFeeds.getCommentsForFeedElement(null, postId, null, 1);
        ConnectApi.Comment lastComment = commentsPage.items[0];
        CommentObj result = new CommentObj(lastComment);
        return result;

    }

    private static List<CommentObj> getCommentsProcess(String postId) {
        List<CommentObj> comments = new List<CommentObj>();
        FeedItem firstComment = [
                SELECT Id, Body, CreatedDate, CreatedById
                FROM FeedItem
                WHERE Id = :postId
                LIMIT 1
        ];

        ConnectApi.FeedElement firstFeedElement = ConnectApi.ChatterFeeds.getFeedElement(null, firstComment.Id);
        ConnectApi.FeedItem feedItem = (ConnectApi.FeedItem) firstFeedElement;
        ConnectApi.User feedAuthor = (ConnectApi.User) feedItem.actor;
        String photo = String.valueOf(feedAuthor.photo.standardEmailPhotoUrl);
        comments.add(new CommentObj(firstComment.Id, feedAuthor.displayName, photo, firstComment.CreatedDate, feedItem.body.text));

        ConnectApi.ChatterFeeds.setIsReadByMe(null, postId, true);
        ConnectApi.CommentPage commentsPage = ConnectApi.ChatterFeeds.getCommentsForFeedElement(null, postId, null, 100);

        Set<Id> filesIds = new Set<Id>();

        for (ConnectApi.Comment comment : commentsPage.items) {
            if (String.valueOf(comment.type) == 'ContentComment') {
                filesIds.add(comment.capabilities.content.id);
                System.debug('comment.capabilities.content.id '+comment.capabilities.content.id);
            }
            comments.add(new CommentObj(comment));
        }

        System.debug('filesIds' +filesIds);

        Map<Id, ContentVersion> contentDocumentIdToVersionsMap = new Map<Id, ContentVersion>();

        for (ContentVersion cv : [SELECT Id, VersionData, ContentSize, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN :filesIds]) {
            contentDocumentIdToVersionsMap.put(cv.ContentDocumentId, cv);
        }

        System.debug('contentVersionsMap '+contentDocumentIdToVersionsMap);
        for (CommentObj commentObj : comments) {
            if (commentObj.file != null && commentObj.file.fileId != null) {
                System.debug('in files loop '+commentObj);
                Id fileId = commentObj.file.fileId;
                ContentVersion currentCV = contentDocumentIdToVersionsMap.get(fileId);
                System.debug('currentCV '+currentCV);
                Integer fileSizeB64 = EncodingUtil.base64Encode(currentCV.VersionData).length();
                commentObj.file.fileSizeBase64 = String.valueOf(fileSizeB64);
            }
        }

        User u = [SELECT Id, VTR3_UnreadFeeds__c FROM User WHERE Id =:UserInfo.getUserId() LIMIT 1];
        String unreadFeeds = u.VTR3_UnreadFeeds__c;
        if (unreadFeeds != null) {
            if (unreadFeeds.contains(String.valueOf(postId))) {
                u.VTR3_UnreadFeeds__c = unreadFeeds.remove(String.valueOf(postId)+';');
                update u;
            }
        }

        return comments;
    }

    private class CommentObj {
        public String id;
        public String authorName;
        public String photo;
        public Long commentData;
        public String text;
        public CommentFileObj file;

        public CommentObj(String i, String n, String p, Datetime d, String t) {
            this.id = i;
            this.authorName = n;
            String tempPhotoString = p.startsWith('https') ? p : COMMUNITY_URL + p;
            this.photo = tempPhotoString.contains('default_profile') ? '' : tempPhotoString;
            this.commentData = d.getTime();
            this.text = t;
            this.file = null;
        }

        public CommentObj(ConnectApi.Comment comment) {
            ConnectApi.User commentAuthor = (ConnectApi.User) comment.user;
            this.id = comment.id;
            this.authorName = commentAuthor.displayName;
            String tempPhotoString = commentAuthor.photo.largePhotoUrl;
            this.photo = tempPhotoString.contains('default_profile') ? '' : tempPhotoString;
            this.commentData = comment.createdDate.getTime();
            this.text = String.valueOf(comment.body.text);
            if (String.valueOf(comment.type) == 'ContentComment') {
                this.file = new CommentFileObj(comment.capabilities.content);
            }
        }

    }

    private class CommentFileObj {
        public String fileExtension;
        public String fileSize;
        public String title;
        public String fileId;
        public String fileSizeBase64;


        public CommentFileObj(ConnectApi.ContentCapability content) {
            this.fileExtension = content.fileExtension;
            this.fileSize = content.fileSize;
            this.title = content.title;
            this.fileId = content.id;
        }
    }
}