public class VT_D1_CommunityChangePasswordController {

    @AuraEnabled
    public static User getUser() {
        User u = [SELECT Id, Username, LastPasswordChangeDate, TimeZoneSidKey FROM User WHERE Id = :UserInfo.getUserId()];
        return u;
    }

    @AuraEnabled
    public static String changePassword(String newPassword, String verifyNewPassword, String oldPassword) {
        String returnString = '';
        try {
            Site.changePassword(newPassword, verifyNewPassword, oldPassword);
        } catch (Exception e) {
            returnString = e.getMessage();
        }

        return returnString;
    }
}