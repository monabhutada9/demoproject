/**
 * Created by user on 05-Aug-20.
 */
@IsTest
public with sharing class VT_R5_AdditionalVisitsTest {

	@TestSetup
	private static void setupMethod() { 
		Test.startTest();
		DomainObjects.Study_t domainStudy = new DomainObjects.Study_t()
				.setMaximumDaysWithoutLoggingIn(1)
				.addAccount(new DomainObjects.Account_t()
						.setRecordTypeByName('Sponsor')
				);
		HealthCloudGA__CandidatePatient__c candidatePatient = (HealthCloudGA__CandidatePatient__c) new DomainObjects.HealthCloudGA_CandidatePatient_t()
				.setConversion('Converted')
				.setCaregiverEmail(DomainObjects.RANDOM.getEmail())
				.addStudy(domainStudy)
				.persist();
		Test.stopTest();
	}

	@IsTest
	public static void test() {
		HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM healthcloudga__CarePlanTemplate__c];
		Case patientCase = [SELECT Id, VTR5_SubsetImmuno__c, ContactId FROM Case];

		VTD1_ProtocolVisit__c  protocolVisit = (VTD1_ProtocolVisit__c)new DomainObjects.VTD1_ProtocolVisit_t()
				.setStudy(study.Id)
				.setRecordTypeByName('VTR5_Subgrouptimelinevisit')
				.setVisitType('Study Team')
				.setEdcName('Test EDC Name')
				.setStartLogic('6')//Hello Copado
				.setNotificationTimeForPG(1)
				.setNotificationTimeForPatient(1)
				.addOffsetFrom(
						new DomainObjects.VTR5_FieldParameter_t()
								.setFieldApiName('VTR5_Day_1_Dose__c')
								.setFieldName('Day 1 dose')
								.setFieldType('date'))
				.persist();

		VTR5_FieldParameter__c fieldParameter = createFieldParameter('VTR5_SubsetImmuno__c', 'SubsetImmuno', 'text');
		createVisitConfiguration(protocolVisit.Id,'testIndicator', 'equals', fieldParameter, '2');
		createVisitConfiguration(protocolVisit.Id,'testIndicator', 'contains', fieldParameter, '3');
		createVisitConfiguration(protocolVisit.Id,'is null', fieldParameter, '4');
		createVisitConfiguration(protocolVisit.Id,'is not null', fieldParameter, '5');
		createVisitConfiguration(protocolVisit.Id, 'testIndicatorFalse', 'does not equal', fieldParameter, '6');


		VTD1_ProtocolVisit__c  protocolVisit2 = (VTD1_ProtocolVisit__c)new DomainObjects.VTD1_ProtocolVisit_t()
				.setStudy(study.Id)
				.setRecordTypeByName('VTR5_Subgrouptimelinevisit')
				.setVisitType('Study Team')
				.setEdcName('Test EDC Name')
				.setStartLogic('1 AND 2 AND 3')
				.setNotificationTimeForPG(1)
				.setNotificationTimeForPatient(1)
				.persist();

		VTR5_FieldParameter__c day57FieldParameter = createFieldParameter('VTR5_Day_57_Dose__c', '57 day', 'date');
		createVisitConfiguration(protocolVisit2.Id, String.valueOf(Date.today()), 'equals', day57FieldParameter, '1');
		createVisitConfiguration(protocolVisit2.Id, String.valueOf(Date.today().addDays(-1)), 'does not equal', day57FieldParameter, '2');

		VTR5_FieldParameter__c enrollmentDateFieldParameter = createFieldParameter('VTD1_Enrollment_Date__c', 'Enrollment Date', 'date');
		createVisitConfiguration(protocolVisit2.Id, 'is null', enrollmentDateFieldParameter, '3');

		Map<String, Schema.RecordTypeInfo> recTypeMap = Schema.SObjectType.VTD1_Actual_Visit__c.getRecordTypeInfosByDeveloperName();

		insert new VTD1_Actual_Visit__c(
				Name = protocolVisit2.VTD1_EDC_Name__c,
				RecordTypeId = recTypeMap.get(VT_R4_ConstantsHelper_VisitsEvents.RT_ACTUAL_VISIT_STUDY_TEAM).getRecordTypeId(),
				Sub_Type__c = null,
				VTD1_Case__c = patientCase.Id,
				VTD1_Contact__c = patientCase.ContactId,
				VTD1_Protocol_Visit__c = protocolVisit.Id,
				VTD1_Pre_Visit_Instructions__c = protocolVisit.VTD1_PreVisitInstructions__c,
				VTR2_Independent_Rater_Flag__c = protocolVisit.VTD1_Independent_Rater_Flag__c,
				VTD1_Additional_Patient_Visit_Checklist__c = protocolVisit.VTD1_Patient_Visit_Checklist__c,
				VTD1_Additional_Visit_Checklist__c = protocolVisit.VTD1_Visit_Checklist__c,
				VTR2_Televisit__c = protocolVisit.VTR2_SH_TelevisitNeeded__c,

				VTR2_Modality__c = protocolVisit.VTR2_Modality__c,
				VTR5_OffsetDate__c = Date.today());

		System.assertEquals(1,[SELECT Id FROM VTD1_Actual_Visit__c WHERE Name = 'Test EDC Name'].size());

		patientCase.VTR5_SubsetImmuno__c = 'testIndicator';
		patientCase.VTR5_Day_57_Dose__c = Date.today();
		update patientCase;
		System.assertEquals(1, [SELECT Id FROM VTD1_Queue_Action__c WHERE VTD1_Action_Class__c = 'VT_R5_QAction_AdditionalVisits'].size());
		Test.startTest();
		Database.executeBatch(new VT_D1_Batch_QueueActionsProcessing());
		Test.stopTest();
		System.assertNotEquals(null, [SELECT Id, Name FROM VTD1_Actual_Visit__c WHERE Name = 'Test EDC Name']);

		VT_R5_QAction_AdditionalVisits qAction = new VT_R5_QAction_AdditionalVisits();
		System.assertEquals('VT_R5_QAction_AdditionalVisits', String.valueOf(qAction.getType()));
	}

	private static void createVisitConfiguration(Id protocolVisitId, String operator, VTR5_FieldParameter__c fieldParameter, String conditionNumber){
		createVisitConfiguration(protocolVisitId, null, operator, fieldParameter, conditionNumber);
	}

	private static void createVisitConfiguration(Id protocolVisitId, String value, String operator, VTR5_FieldParameter__c fieldParameter, String conditionNumber){
		new DomainObjects.Visit_Configurations_t()
				.setProtocolVisit(protocolVisitId)
				.setConditionNumber(conditionNumber)
				.setRecordTypeByName('VTR5_SubGroupindicator')
				.setOperator(operator)
				.setValue(value)
				.addFieldParameter(
						new DomainObjects.VTR5_FieldParameter_t()
							.setFieldApiName(fieldParameter.VTR5_FieldAPIName__c)
							.setFieldName(fieldParameter.Name)
							.setFieldType(fieldParameter.VTR5_FieldType__c))
				.persist();
	}

	private static VTR5_FieldParameter__c createFieldParameter(String apiName, String name, String type){
		VTR5_FieldParameter__c fieldParameter = (VTR5_FieldParameter__c) new DomainObjects.VTR5_FieldParameter_t()
				.setFieldApiName(apiName)
				.setFieldName(name)
				.setFieldType(type)
				.persist();
		return fieldParameter;
	}
}