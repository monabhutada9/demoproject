@isTest(seeAllData=false)
public class TestUserShare {
    @isTest
    public static void quickTest()
    {
        Id vtslProfileId =[SELECT Id FROM Profile WHERE Name =:VT_R4_ConstantsHelper_ProfilesSTM.VTSL_PROFILE_NAME].id;
        
        DomainObjects.User_t userVTSL = new DomainObjects.User_t()
            .setProfile(vtslProfileId);
            userVTSL.persist();
        System.assert(userVTSL.record.Id!=NULL);
        System.debug('##userVTSL.id'+userVTSL.record.Id);
        
        DomainObjects.Contact_t con_t = new DomainObjects.Contact_t()
                .setLastName('VT_R3_EmailsSenderTest');
        DomainObjects.User_t user_t = new DomainObjects.User_t()
                .addContact(con_t)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        user_t.persist();
        System.assert(user_t.record.Id!=NULL);
        
        
        //User patientUser =[select id from user where profile.name='Patient' and Isactive=true limit 1];
        
        //System.assert(patientUser!=NULL);
		        
        UserShare userShareRecord = new UserShare();
            userShareRecord.UserId = user_t.record.Id;
            userShareRecord.UserOrGroupId = userVTSL.record.Id;
            userShareRecord.UserAccessLevel = 'Read';
            //userShareRecord.RowCause = 'Manual';
            insert userShareRecord;
        System.assert(userShareRecord.id!=NUll);
        System.debug('##userShareRecord.id'+userShareRecord.id);
        
        List<UserShare> usersharelist=[select id,UserId,UserOrGroupId,UserAccessLevel from UserShare];
        System.debug('##usersharelist.id'+usersharelist);
        System.debug('##usersharelist.id'+usersharelist.size());
        
        List<UserShare> usersharelist1=[select id from UserShare where id=:userShareRecord.id];
        System.assertEquals(usersharelist1.size(),0);
       // System.debug('##usershare'+usersharelist1[0].id);
    }

}