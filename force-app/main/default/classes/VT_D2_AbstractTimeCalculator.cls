/**
* @author: Carl Judge
* @date: 22-Feb-19
* @description: Abstract for classes that calculate locally formatted datetimes
**/

public abstract class VT_D2_AbstractTimeCalculator {

    protected List<SObject> records;
    protected String baseTimeField;
    protected Map<String, String> profileToFieldsMap;
    protected Map<Id, Map<String, User>> userMap; // record ID => profile name => User


    public void calculateTimes(List<SObject> records) {
        this.records = records;
        this.profileToFieldsMap = getProfileToFieldsMap();
        this.baseTimeField = getBaseTimeField();
        this.userMap = getUserMap();

        calculateFieldValues();
    }

    protected abstract Map<String, String> getProfileToFieldsMap();
    protected abstract String getBaseTimeField();
    protected abstract Map<Id, Map<String, User>> getUserMap();

    private void calculateFieldValues() {
        for (SObject item : this.records) {
            for (String profileName : this.profileToFieldsMap.keySet()) {
                item.put(this.profileToFieldsMap.get(profileName), getFieldValue(item, profileName));
            }
        }
    }

    private String getFieldValue(SObject record, String profileName) {
        String fieldValue = '';

        if (record.get(this.baseTimeField) != null &&
                this.userMap.containsKey(record.Id) &&
                this.userMap.get(record.Id).containsKey(profileName))
        {
            User memberUser = this.userMap.get(record.Id).get(profileName);

            String dateTimeFormat = VT_D2_LocaleDatetimeFormats.getDateTimeFormat(memberUser.LanguageLocaleKey);
            String timeFormatOnly = dateTimeFormat.replaceAll('[dMy/,.-]','').trim();

            Datetime dt = (Datetime)record.get(this.baseTimeField);
            fieldValue = dt.format(timeFormatOnly, memberUser.TimeZoneSidKey);
        }

        return fieldValue;
    }

    protected void addToUserMap(Map<Id, Map<String, User>> userMap, Id recordId, User user) {
        if (user != null) {
            if (! userMap.containsKey(recordId)) {
                userMap.put(recordId, new Map<String, User>());
            }
            userMap.get(recordId).put(user.Profile.Name, user);
        }
    }
}