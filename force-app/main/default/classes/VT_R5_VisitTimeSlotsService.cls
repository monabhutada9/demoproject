/**
 * Created by Yuliya Yakushenkova on 9/10/2020.
 */

public class VT_R5_VisitTimeSlotsService {

    private Map<Date, Time[]> dateToTime;
    private Map<String, Object> deserializedData;

    public VT_R5_VisitTimeSlotsService(Map<Date, Time[]> dateToTime, Map<String, Object> deserializedData) {
        this.dateToTime = dateToTime;
        this.deserializedData = deserializedData;
    }

    public List<DateTimeSlot> getDateTimeSlots() {
        List<DateTimeSlot> dateTimeSlots = new List<DateTimeSlot>();
        for (Date key : dateToTime.keySet()) {
            Time[] timeKeys = dateToTime.get(key);
            if (deserializedData.containsKey(key.format())) {
                List<String> timeValues = getListOfStringsFromObject(deserializedData.get(key.format()));
                dateTimeSlots.add(new DateTimeSlot(key, timeKeys, timeValues));
            }
        }
        return dateTimeSlots;
    }

    private List<String> getListOfStringsFromObject(Object obj) {
        String value = JSON.serialize(obj);
        value = value.remove('"');
        List<String> timeValues = value
                .substring(1, value.length() - 1)
                .split(',');
        return timeValues;
    }


    public class TimeSlot {
        Datetime key;
        String value;

        public TimeSlot(Datetime timeKey, String timeValue) {
            this.key = timeKey;
            this.value = timeValue;
        }
    }

    public class DateTimeSlot {
        String availableDate;
        List<TimeSlot> times;

        public DateTimeSlot(Date availableDate, Time[] timeKeys, List<String> timeValues) {
            this.availableDate = availableDate.format();
            setTimeSlots(availableDate, timeKeys, timeValues);
        }

        private void setTimeSlots(Date availableDate, Time[] timeKeys, List<String> timeValues) {
            this.times = new List<TimeSlot>();
            for (Integer i = 0; i < timeKeys.size(); i++) {
                Datetime key = Datetime.newInstance(availableDate, timeKeys[i]);
                this.times.add(
                        new TimeSlot(key, timeValues.get(i))
                );
            }
        }
    }

    public class DateTimeSlotsHelper {

        private String visitId;
        private List<Id> visitMembersIds;
        private Date preferredDate;

        public DateTimeSlotsHelper(String visitId) {
            this.visitId = visitId;
        }

        public DateTimeSlotsHelper(String visitId, Set<Id> membersIds) {
            this(visitId);
            this.visitMembersIds = new List<Id>(membersIds);
        }

        public DateTimeSlotsHelper(String visitId, Set<Id> membersIds, Date preferredDate) {
            this(visitId, membersIds);
            this.preferredDate = preferredDate;
        }

        public List<DateTimeSlot> getDateTimeSlots() {
            VT_D1_ActualVisitsHelper helper = new VT_D1_ActualVisitsHelper();
            if (visitMembersIds != null) helper.visitMembersIdUpdatedList = visitMembersIds;
            Map<String, Object> dateTimes =
                    (Map<String, Object>) JSON.deserializeUntyped(helper.getAvailableDateTimes(visitId));

            return new VT_R5_VisitTimeSlotsService(helper.getDatesTimesMap(visitId, preferredDate), dateTimes)
                    .getDateTimeSlots();
        }
    }

}