/**
 * @author: Dmitry Yartsev
 * @date: 27.06.2020
 * @description: Test class for VT_R4_PrivacyEulaViewController
 */

@IsTest
public with sharing class VT_R4_PrivacyEulaViewControllerTest {

    private static final String TESTED_CLASS_NAME = 'VT_R4_PrivacyEulaViewController';
    private static final Id ARTICLE_STUDY_ID = fflib_IDGenerator.generate(HealthCloudGA__CarePlanTemplate__c.getSObjectType());
    private static final Id ARTICLE_MASTER_ID = fflib_IDGenerator.generate(Knowledge__kav.getSObjectType());
    private static final String ARTICLE_LANGUAGE = 'en_US';
    private static final String ARTICLE_TYPE = 'EULA';
    private static final String UNSUPPORTED_ARTICLE_TYPE = 'Training_Materials';
    private static final String ARTICLE_CONTENT = 'Test content';
    private static final String PAGE_REF_URL_TEMPLATE = '/apex/VT_R4_PrivacyEulaViewPage?studyId={0}&language={1}';

    public static void testGetContent() {
        System.assert(String.isBlank(VT_R4_PrivacyEulaViewController.getContent(ARTICLE_STUDY_ID, ARTICLE_LANGUAGE, ARTICLE_TYPE)));

        applyKavQueryStub(new List<Knowledge__kav>{ new Knowledge__kav(VTD1_Content__c = ARTICLE_CONTENT) }, '.getContent');

        System.assertEquals(
                ARTICLE_CONTENT,
                VT_R4_PrivacyEulaViewController.getContent(ARTICLE_STUDY_ID, ARTICLE_LANGUAGE, ARTICLE_TYPE)
        );

        System.assert(String.isBlank(VT_R4_PrivacyEulaViewController.getContent(ARTICLE_STUDY_ID, ARTICLE_LANGUAGE, UNSUPPORTED_ARTICLE_TYPE)));
    }

    public static void testGetVFContent() {
        setupPageReference(ARTICLE_STUDY_ID, ARTICLE_LANGUAGE);

        VT_R4_PrivacyEulaViewController controller = new VT_R4_PrivacyEulaViewController();

        System.assert(String.isBlank(controller.getVFContent()));

        applyKavQueryStub(new List<Knowledge__kav>{ new Knowledge__kav(VTD1_Content__c = ARTICLE_CONTENT) }, '.getContent');

        System.assertEquals(
                ARTICLE_CONTENT,
                controller.getVFContent()
        );

        setupPageReference(null, null);
        System.assert(String.isBlank(VT_R4_PrivacyEulaViewController.getContent(ARTICLE_STUDY_ID, ARTICLE_LANGUAGE, UNSUPPORTED_ARTICLE_TYPE)));
    }

    public static void testExceptions() {
        String content = '';
        applyKavQueryStub(null, '.getContent');
        try {
            content = VT_R4_PrivacyEulaViewController.getContent(ARTICLE_STUDY_ID, ARTICLE_LANGUAGE, ARTICLE_TYPE);
        } catch (Exception e) {
        } finally {
            System.assert(String.isEmpty(content));
        }

    }

    public static void testGetArticles() {
        Map<String, String> articleFieldMap = new Map<String, String>{
                'VTD1_Content__c' => ARTICLE_CONTENT,
                'Language' =>  'es',
                'Title' => 'Test',
                'VTD2_Study__c' => ARTICLE_STUDY_ID,
                'MasterVersionId' => ARTICLE_MASTER_ID
        };
        applyKavQueryStub(new List<Knowledge__kav>{(Knowledge__kav) JSON.deserialize(JSON.serialize(articleFieldMap), Knowledge__kav.class)}, '.queryArticles');
        applyUserQueryStub(new User(
                Profile = (Profile) JSON.deserialize(JSON.serialize(new Map<String, String>{'Name' => 'Patient'}), Profile.class),
                Contact = new Contact(
                        VTD1_Clinical_Study_Membership__r = new Case(
                                VTD1_Study__c = ARTICLE_STUDY_ID,
                                VTD1_Virtual_Site__r = new Virtual_Site__c(
                                        VTR5_IRBApprovedLanguages__c = 'en_US;es',
                                        VTR5_DefaultIRBLanguage__c = 'en_US'
                                )
                        )
                ),
                LanguageLocaleKey = 'es'
        ));
        System.assertNotEquals(null, VT_R4_PrivacyEulaViewController.getArticles(null));
        System.assert(!VT_R4_PrivacyEulaViewController.isPi());
    }

    private static void applyKavQueryStub(List<Knowledge__kav> result, String name) {
        new QueryBuilder()
                .buildStub()
                .addStubToList(result)
                .namedQueryStub(TESTED_CLASS_NAME + name)
                .applyStub();
    }

    private static void applyUserQueryStub(User result) {
        new QueryBuilder()
                .buildStub()
                .addStubToSObject(result)
                .namedQueryStub(TESTED_CLASS_NAME + '.getUser')
                .applyStub();
    }

    private static void setupPageReference(Id studyId, String lang) {
        Test.setCurrentPage(new PageReference(String.format(PAGE_REF_URL_TEMPLATE, new String[] { studyId, lang })));
    }


}