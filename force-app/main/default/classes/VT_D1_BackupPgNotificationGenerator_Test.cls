/**
* @author: Carl Judge
* @date: 11-Sep-18
* @description: 
**/

@IsTest
public class VT_D1_BackupPgNotificationGenerator_Test {

    public static void firstTest() {
        Case cas = [SELECT VTD1_Primary_PG__c, VTD1_Secondary_PG__c FROM Case WHERE VTD1_Primary_PG__c != null AND RecordType.DeveloperName = 'CarePlan'];


        Event ev = new Event(
            OwnerId = cas.VTD1_Primary_PG__c,
            RecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('OutOfOffice').getRecordTypeId(),
            StartDateTime = Datetime.now(),
            DurationInMinutes = 60
        );

        Test.startTest();
        System.debug(ev);
        insert ev;
        Test.stopTest();
    }
}