public with sharing class VT_R5_TaskPoolController {
    @AuraEnabled
    public static String getcmpHeader() {
        Profile_PoolTaskRT_Mapping__mdt profileSettings=getProfilesettings();
        String cmpheader=getHeader(profileSettings);
        system.debug('cmpHeader '+cmpHeader);
        return cmpheader;
    }
    public class TaskInfo {
        @AuraEnabled public List<VTR5_Pool_Task__c> tasks;
        @AuraEnabled public List<String> categories = new List<String>();
        public void setCategories() {
            Schema.DescribeSObjectResult objDescribe = VTR5_Pool_Task__c.getSObjectType().getDescribe();
            Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
            List<Schema.PicklistEntry> values = fieldMap.get('VTR5_Category__c').getDescribe().getPickListValues();
            for (Schema.PicklistEntry d: values) {
                this.categories.add(d.getValue());
            }
        }
    }
    @AuraEnabled
    public static TaskInfo getTaskPool(String category) {
        TaskInfo taskInfo = new TaskInfo();
        if (category == null) {
            taskInfo.setCategories();
        }
        Profile_PoolTaskRT_Mapping__mdt profileSettings=getProfilesettings();
        taskInfo.tasks = getTasks(category,profileSettings);
        return taskInfo;
    }
    public static Profile_PoolTaskRT_Mapping__mdt getProfilesettings() {
        Profile_PoolTaskRT_Mapping__mdt profilesettings=new Profile_PoolTaskRT_Mapping__mdt();
        Profile curProfile=[SELECT Name
                            FROM Profile 
                            WHERE Id=:UserInfo.getProfileId()];
        profilesettings=[SELECT Id,Pool_Task_record_type__c,Profile_Name__c   
                                                               FROM Profile_PoolTaskRT_Mapping__mdt
                                                               WHERE Profile_Name__c=:curProfile.Name
                                                               LIMIT 1 ];
        return profilesettings;
    }
    public static String getHeader(Profile_PoolTaskRT_Mapping__mdt profilesettings) { 
        String cmpheader=profilesettings.Profile_Name__c;
        return cmpheader;
    }
    public static List<VTR5_Pool_Task__c> getTasks(String category, Profile_PoolTaskRT_Mapping__mdt profilesettings) {
        String profileRT='';
        if(profilesettings!=null){
            profileRT=profilesettings.Pool_Task_record_type__c;
        }
        // user groups
        List<Id> groupIds = new List<Id>();
        for (GroupMember gm : [SELECT GroupId,UserOrGroupId 
        FROM GroupMember
        WHERE UserOrGroupId = :UserInfo.getUserId() AND Group.Type = 'Queue']) {
          //  groupIds.add(gm.Id);
           groupIds.add(gm.GroupId);
        }
        String queryStr='SELECT Id, '+
        'VTR5_Category__c, '+
        'VTR5_Document__c, '+
        'VTR5_DocuSignStatus__c, '+
        'VTR5_DueDate__c, '+
        'VTR5_MonitoringVisit__c, '+
        'VTR5_Study__c, '+
        'VTR5_Study__r.Name, '+
        'VTR5_Subject__c, '+
        'VTR5_TaskUniqueCode__c, '+
        'VTR5_Type__c, '+
        'VTR5_VirtualSite__c '+
        'FROM VTR5_Pool_Task__c '+
        'WHERE RecordType.DeveloperName=:profileRT '+
        'AND OwnerId IN: groupIds';
        queryStr += category != null ? ' AND VTR5_Category__c =:category ' : '' + '';
        queryStr += ' ORDER BY CreatedDate DESC';
        System.debug('!!! queryStr' + queryStr);
        List<VTR5_Pool_Task__c> pooltasks = Database.query(queryStr);
        return pooltasks;
    }
    @AuraEnabled
    public static List<VTR5_Pool_Task__c> claimTasksRemote (List<Id> taskIds) {
        List<VTR5_Pool_Task__c> tasksToUpdate = new List<VTR5_Pool_Task__c>();
        List<VTR5_Pool_Task__c> tasks =  [SELECT Id, OwnerId
        FROM VTR5_Pool_Task__c
      //  WHERE Id IN :taskIds AND OwnerId!=:UserInfo.getUserId()
        WHERE Id IN :taskIds 
        FOR UPDATE ];
        Profile_PoolTaskRT_Mapping__mdt profilesettings =getProfilesettings() ;
        for (VTR5_Pool_Task__c t : tasks) {
            if (t.OwnerId.getSobjectType().getDescribe().getName() == 'Group') {
                tasksToUpdate.add(t);
            }
        }
        for (VTR5_Pool_Task__c t : tasksToUpdate) {
            t.OwnerId = UserInfo.getUserId();
        }
        try {
            update tasks;
            return getTasks(null,profilesettings);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
        }
    }
}