/**
* @author: Carl Judge
* @date: 22-Feb-19
* @description: 
**/

public without sharing class VT_R2_ConferenceMemberTriggerHandler {

    public void onBeforeDelete(List<VTD1_Conference_Member__c> recs) {
        VT_D1_LegalHoldDeleteValidator.validateDeletion(recs);
    }

    public void onBeforeInsert(List<VTD1_Conference_Member__c> recs) {
        setEmails(recs, null);
    }

    public void onBeforeUpdate(List<VTD1_Conference_Member__c> recs, Map<Id, VTD1_Conference_Member__c> oldMap) {
        setEmails(recs, oldMap);
    }

    public void setEmails(List<VTD1_Conference_Member__c> members, Map<Id, VTD1_Conference_Member__c> oldMap) {
        List<Id> userIds = new List<Id>();
        List<VTD1_Conference_Member__c> membersToUpdate = new List<VTD1_Conference_Member__c>();
        for (VTD1_Conference_Member__c member : members) {
            if (oldMap == null || member.VTD1_User__c != oldMap.get(member.Id).VTD1_User__c) {
                userIds.add(member.VTD1_User__c);
                membersToUpdate.add(member);
            }
        }

        if (! membersToUpdate.isEmpty()) {
            Map<Id, User> userMap = new Map<Id, User>([SELECT Id, Email FROM User WHERE Id IN :userIds]);

            for (VTD1_Conference_Member__c member : membersToUpdate) {
                if (userMap.containsKey(member.VTD1_User__c)) {
                    member.VTD1_External_Participant_Email__c = userMap.get(member.VTD1_User__c).Email;
                }
            }
        }
    }

    public void onAfterInsert(List<VTD1_Conference_Member__c> recs) {

        List <VTD1_Conference_Member__c> conferenceMembers = [select Id, VTD1_Visit_Member__c from VTD1_Conference_Member__c
            where Id in : recs and VTD1_Visit_Member__r.VTD1_Actual_Visit__r.VTD1_Status__c = 'Scheduled'
            and VTD1_Visit_Member__r.VTD1_Actual_Visit__r.VTD1_Scheduled_Date_Time__c <= :System.now().addHours(1)];

        if (!conferenceMembers.isEmpty()) {
            Set <Id> visitMemberIds = new Set<Id>();
            for (VTD1_Conference_Member__c conferenceMember : conferenceMembers) {
                visitMemberIds.add(conferenceMember.VTD1_Visit_Member__c);
            }
            System.debug('visitMemberIds =' + visitMemberIds);
            VT_R2_VisitMemberTriggerHandler.sendNotifications(visitMemberIds);
        }
    }
}