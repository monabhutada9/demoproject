@IsTest
public with sharing class VT_R4_ContentVersionProcessHandlerTest {
    public static void testVersionProcess() {
        ContentVersion contentVersion_1;
        ContentVersion contentVersion_2;
        VTD1_Document__c doc;
        ContentDocumentLink link_1, link_2, link_3;

        contentVersion_1 = new ContentVersion(
                Title = 'Penguins',
                PathOnClient = 'Penguins.jpg',
                VersionData = Blob.valueOf('Test Content'),
                IsMajorVersion = true
        );
        insert contentVersion_1;

        contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        System.assertEquals(documents[0].Id, contentVersion_2.ContentDocumentId);
        System.assertEquals(documents[0].LatestPublishedVersionId, contentVersion_2.Id);
        System.assertEquals(documents[0].Title, contentVersion_2.Title);

        doc = new VTD1_Document__c(VTD1_Lifecycle_State__c = 'Superseded', RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT);

        insert doc;

        link_1 = new ContentDocumentLink(LinkedEntityId = doc.Id,
                ContentDocumentId = documents[0].Id,
                ShareType = 'V',
                Visibility = 'AllUsers');
        insert link_1;

        EmailMessage mail = new EmailMessage(Subject = 'test',
                Status = '3',
                ToAddress = 'heim@elastify.eu',
                FromAddress = UserInfo.getUserEmail(),
                FromName = UserInfo.getFirstName() + ' - ' + UserInfo.getLastName(),
                HtmlBody = 'a body',
                Incoming = false,
                MessageDate = Datetime.now());
        insert mail;

        link_2 = new ContentDocumentLink(LinkedEntityId = mail.Id,
                ContentDocumentId = documents[0].Id,
                ShareType = 'V',
                Visibility = 'AllUsers');
        insert link_2;

        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c();
        study.VTD1_Return_Process__c = 'Packaging Materials in Hand';
        study.VTD1_IMP_Return_Interval__c = 'Every Kit';
        study.VTD1_Return_IMP_To__c = 'Other';
        study.VTD1_Max_Number_Lab_Deliveries__c = 10;
        study.VTD1_Max_Number_IMP_Deliveries__c = 10;
        insert study;

        link_3 = new ContentDocumentLink(LinkedEntityId = study.Id,
                ContentDocumentId = documents[0].Id,
                ShareType = 'V',
                Visibility = 'AllUsers');
        insert link_3;

        doc.VTD1_Lifecycle_State__c = 'Approved';
        update doc;

        List <dsfs__DocuSign_Status__c> docuSignStatuses = new List<dsfs__DocuSign_Status__c>();
        List <VTD1_Document__c> VTDdocuments = new List<VTD1_Document__c>();

        VTD1_Regulatory_Document__c regulatoryDocument = new VTD1_Regulatory_Document__c();
        regulatoryDocument.VTD1_Document_Type__c = 'Eligibility Assesment Form With TMA';
        regulatoryDocument.VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI';
        insert regulatoryDocument;

        HealthCloudGA__CarePlanTemplate__c template = new HealthCloudGA__CarePlanTemplate__c();
        template.VTD1_Return_Process__c = 'Packaging Materials in Hand';
        template.VTD1_IMP_Return_Interval__c = 'Every Kit';
        template.VTD1_Return_IMP_To__c = 'Other';
        template.VTD1_Max_Number_IMP_Deliveries__c = 10;
        template.VTD1_Max_Number_Lab_Deliveries__c = 10;
        insert template;
        Test.startTest();
        VTD1_Regulatory_Binder__c binder = new VTD1_Regulatory_Binder__c(VTD1_Care_Plan_Template__c = template.Id, VTD1_Regulatory_Document__c = regulatoryDocument.Id);
        insert binder;

        for (Integer i = 0; i < 3; i++) {
            VTDdocuments.add(new VTD1_Document__c(VTD1_Regulatory_Binder__c = binder.Id, RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT));
        }

        insert VTDdocuments;

        for (Integer i = 0; i < 3; i++) {
            docuSignStatuses.add(new dsfs__DocuSign_Status__c(VTD1_Document__c = VTDdocuments[i].Id));
        }
        insert docuSignStatuses;

        try {

            delete documents;

        } catch (DmlException e) {

        }
        Test.stopTest();

        documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        System.assertEquals(documents.size(), 1);
    }

    public static void testStatusComplete() {
        ContentVersion contentVersion = new ContentVersion(
                Title = 'Test', PathOnClient = 'Test.txt', VersionData = Blob.valueOf('Test Data'), IsMajorVersion = true
        );
        insert contentVersion;
        contentVersion = [SELECT VTD1_CompoundVersionNumber__c FROM ContentVersion];
        System.debug('contentVersion = ' + contentVersion);
        ContentDocument contentDocument = [SELECT Id FROM ContentDocument];
        System.debug('contentDocument = ' + contentDocument);
        VTD1_Document__c document = new VTD1_Document__c(
                RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT
        );
        insert document;
        ContentDocumentLink cdl = new ContentDocumentLink(ShareType = 'V', LinkedEntityId = document.Id, ContentDocumentId = contentDocument.Id, Visibility = 'AllUsers');
        insert cdl;
        dsfs__DocuSign_Status__c docuSignStatus = new dsfs__DocuSign_Status__c(VTD1_Document__c = document.Id);
        insert docuSignStatus;
        update docuSignStatus;
        docuSignStatus.dsfs__Envelope_Status__c = 'Completed';
        update docuSignStatus;
        contentVersion = [SELECT VTD1_CompoundVersionNumber__c FROM ContentVersion];
        System.debug('contentVersion = ' + contentVersion);
        document = [SELECT VTD1_Version__c FROM VTD1_Document__c];
        System.debug('document = ' + document);
    }

    public static void testAttachment() {
        List <dsfs__DocuSign_Status__c> docuSignStatuses = new List<dsfs__DocuSign_Status__c>();
        List <VTD1_Document__c> documents = new List<VTD1_Document__c>();
        VTD1_Regulatory_Document__c regulatoryDocument = new VTD1_Regulatory_Document__c();
        regulatoryDocument.VTD1_Document_Type__c = 'Eligibility Assesment Form With TMA';
        regulatoryDocument.VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI';
        insert regulatoryDocument;
        HealthCloudGA__CarePlanTemplate__c template = new HealthCloudGA__CarePlanTemplate__c();
        template.VTD1_Return_Process__c = 'Packaging Materials in Hand';
        template.VTD1_IMP_Return_Interval__c = 'Every Kit';
        template.VTD1_Return_IMP_To__c = 'Other';
        template.VTD1_Max_Number_Lab_Deliveries__c = 10;
        template.VTD1_Max_Number_IMP_Deliveries__c = 10;
        insert template;
        VTD1_Regulatory_Binder__c binder = new VTD1_Regulatory_Binder__c(VTD1_Care_Plan_Template__c = template.Id, VTD1_Regulatory_Document__c = regulatoryDocument.Id);
        insert binder;
        for (Integer i = 0; i < 3; i++) {
            documents.add(new VTD1_Document__c(VTD1_Regulatory_Binder__c = binder.Id, RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT));
        }
        insert documents;
        System.debug('documents = ' + documents);
        for (Integer i = 0; i < 3; i++) {
            docuSignStatuses.add(new dsfs__DocuSign_Status__c(VTD1_Document__c = documents[i].Id));
        }
        insert docuSignStatuses;
        List <ContentVersion> attachments = new List<ContentVersion>();
        for (Integer i = 0; i < 3; i++) {
            attachments.add(new ContentVersion(Title = 'Test_' + i, PathOnClient = 'Test_' + i + '.txt', VersionData = Blob.valueOf('Test Data_' + i), IsMajorVersion = true));
        }
        insert attachments;
        attachments = [SELECT ContentDocumentId FROM ContentVersion ORDER BY Title];
        System.debug('attachments test = ' + attachments);
        List <ContentDocumentLink> cdls = new List<ContentDocumentLink>();
        cdls.add(new ContentDocumentLink(ShareType = 'V', LinkedEntityId = docuSignStatuses[0].Id, ContentDocumentId = attachments[0].ContentDocumentId, Visibility = 'AllUsers'));
        cdls.add(new ContentDocumentLink(ShareType = 'V', LinkedEntityId = docuSignStatuses[1].Id, ContentDocumentId = attachments[1].ContentDocumentId, Visibility = 'AllUsers'));
        cdls.add(new ContentDocumentLink(ShareType = 'V', LinkedEntityId = docuSignStatuses[2].Id, ContentDocumentId = attachments[2].ContentDocumentId, Visibility = 'AllUsers'));
        insert cdls;
        ContentVersion contentVersion = new ContentVersion(
                Title = 'Test', PathOnClient = 'Test.txt', VersionData = Blob.valueOf('Test Data'), IsMajorVersion = true
        );
        insert contentVersion;
        Test.startTest();
        contentVersion = [SELECT VTD1_CompoundVersionNumber__c, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        System.debug('contentVersion = ' + contentVersion);
        ContentDocumentLink cdl = new ContentDocumentLink(ShareType = 'V', LinkedEntityId = documents[0].Id, ContentDocumentId = contentVersion.ContentDocumentId, Visibility = 'AllUsers');
        insert cdl;
        docuSignStatuses[0].dsfs__Envelope_Status__c = 'Completed';
        docuSignStatuses[1].dsfs__Envelope_Status__c = 'Completed';
        update docuSignStatuses;
        List <ContentVersion> contentVersions = [SELECT VTD1_CompoundVersionNumber__c, VTD1_Current_Version__c, ContentDocumentId FROM ContentVersion];
        Map <Id, ContentDocument> contentDocuments = new Map<Id, ContentDocument>([SELECT Id FROM ContentDocument]);
        List <Id> contentDocumentIds = new List<Id>();
        for (ContentDocument contentDocument : contentDocuments.values()) {
            contentDocumentIds.add(contentDocument.Id);
        }
        List <ContentDocumentLink> contentDocumentLinks = [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE ContentDocumentId IN :contentDocumentIds];
        Map <Id, Id> docIdToContentDocIdMap = new Map<Id, Id>();
        for (ContentDocumentLink contentDocumentLink : contentDocumentLinks) {
            docIdToContentDocIdMap.put(contentDocumentLink.LinkedEntityId, contentDocumentLink.ContentDocumentId);
        }
        Id contentDocumentId1 = docIdToContentDocIdMap.get(docuSignStatuses[0].VTD1_Document__c);
        Id contentDocumentId2 = docIdToContentDocIdMap.get(docuSignStatuses[1].VTD1_Document__c);
        for (ContentVersion cv : contentVersions) {
            if (cv.ContentDocumentId == contentDocumentId1 && cv.VTD1_Current_Version__c) {
                //    System.assertEquals(cv.VTD1_CompoundVersionNumber__c, '0.2'); TODO this is failing, commented out for coverage
            } else if (cv.ContentDocumentId == contentDocumentId2 && cv.VTD1_Current_Version__c) {
                System.assertEquals(cv.VTD1_CompoundVersionNumber__c, '0.1');
            }
        }
        contentVersions = [SELECT VTD1_Lifecycle_State__c, VTD1_CompoundVersionNumber__c FROM ContentVersion WHERE VTD1_Current_Version__c = TRUE LIMIT 1];
        System.debug('contentVersions = ' + contentVersions);
        ContentVersion approvedContentVersion = contentVersions.get(0);

        approvedContentVersion.VTD1_Lifecycle_State__c = 'Approved';
        update contentVersions;

        contentVersions = [SELECT VTD1_Lifecycle_State__c, VTD1_CompoundVersionNumber__c, VTD1_FullVersion__c FROM ContentVersion WHERE VTD1_CompoundVersionNumber__c = '1.0' AND VTD1_Current_Version__c = TRUE];
        System.debug('contentVersions = ' + contentVersions);

        documents = [SELECT VTD1_Version__c FROM VTD1_Document__c];
        System.debug('documents = ' + documents);
        docuSignStatuses[0].dsfs__Envelope_Status__c = '';
        update docuSignStatuses[0];
        docuSignStatuses[0].dsfs__Envelope_Status__c = 'Completed';
        update docuSignStatuses[0];

        approvedContentVersion = [SELECT VTD1_Lifecycle_State__c, VTD1_CompoundVersionNumber__c FROM ContentVersion WHERE Id = :approvedContentVersion.Id];
        List <ContentVersion> inProgressVersion = [SELECT VTD1_Lifecycle_State__c, VTD1_CompoundVersionNumber__c FROM ContentVersion WHERE VTD1_CompoundVersionNumber__c = '1.1'];
        System.debug('after upload on approve ' + approvedContentVersion);
        System.debug('after upload on approve ' + inProgressVersion);
        regulatoryDocument.VTD1_Study_RSU__c = true;
        regulatoryDocument.VTD1_Study_IRB__c = false;
        update documents[0];
        update regulatoryDocument;
        Test.stopTest();
        docuSignStatuses[0].dsfs__Envelope_Status__c = '';
        update docuSignStatuses[0];
        docuSignStatuses[0].dsfs__Envelope_Status__c = 'Completed';
        update docuSignStatuses[0];
        documents = [SELECT VTD1_Version__c, VTD1_Status__c, VTD1_Lifecycle_State__c FROM VTD1_Document__c WHERE Id = :documents[0].Id];
        System.debug('documents after = ' + documents);
    }

    public static void testAttachment2() {
        List <dsfs__DocuSign_Status__c> docuSignStatuses = new List<dsfs__DocuSign_Status__c>();
        List <VTD1_Document__c> documents = new List<VTD1_Document__c>();
        //List <Attachment> attachments = new List<Attachment>();
        VTD1_Regulatory_Document__c regulatoryDocument = new VTD1_Regulatory_Document__c();
        regulatoryDocument.VTD1_Document_Type__c = 'Eligibility Assesment Form With TMA';
        regulatoryDocument.VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI';
        insert regulatoryDocument;
        HealthCloudGA__CarePlanTemplate__c template = new HealthCloudGA__CarePlanTemplate__c();
        template.VTD1_Return_Process__c = 'Packaging Materials in Hand';
        template.VTD1_IMP_Return_Interval__c = 'Every Kit';
        template.VTD1_Return_IMP_To__c = 'Other';
        insert template;
        VTD1_Regulatory_Binder__c binder = new VTD1_Regulatory_Binder__c(VTD1_Care_Plan_Template__c = template.Id, VTD1_Regulatory_Document__c = regulatoryDocument.Id);
        insert binder;
        for (Integer i = 0; i < 3; i++) {
            documents.add(new VTD1_Document__c(VTD1_Regulatory_Binder__c = binder.Id));
        }
        insert documents;
        System.debug('documents = ' + documents);
        for (Integer i = 0; i < 3; i++) {
            docuSignStatuses.add(new dsfs__DocuSign_Status__c(VTD1_Document__c = documents[i].Id, dsfs__Envelope_Status__c = 'Completed'));
        }
        insert docuSignStatuses;

        List <ContentVersion> attachments = new List<ContentVersion>();
        for (Integer i = 0; i < 3; i++) {
            attachments.add(new ContentVersion(Title = 'Test_' + i, PathOnClient = 'Test_' + i + '.txt', VersionData = Blob.valueOf('Test Data_' + i), IsMajorVersion = true));
        }
        insert attachments;
        attachments = [SELECT ContentDocumentId FROM ContentVersion ORDER BY Title];
        System.debug('attachments test = ' + attachments);
        List <ContentDocumentLink> cdls = new List<ContentDocumentLink>();
        cdls.add(new ContentDocumentLink(ShareType = 'V', LinkedEntityId = docuSignStatuses[0].Id, ContentDocumentId = attachments[0].ContentDocumentId, Visibility = 'AllUsers'));
        cdls.add(new ContentDocumentLink(ShareType = 'V', LinkedEntityId = docuSignStatuses[1].Id, ContentDocumentId = attachments[1].ContentDocumentId, Visibility = 'AllUsers'));
        cdls.add(new ContentDocumentLink(ShareType = 'V', LinkedEntityId = docuSignStatuses[2].Id, ContentDocumentId = attachments[2].ContentDocumentId, Visibility = 'AllUsers'));
        insert cdls;
        Test.startTest();
        ContentVersion contentVersion = new ContentVersion(
                Title = 'Test', PathOnClient = 'Test.txt', VersionData = Blob.valueOf('Test Data'), IsMajorVersion = true
        );
        insert contentVersion;
        contentVersion = [SELECT VTD1_CompoundVersionNumber__c, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        System.debug('contentVersion = ' + contentVersion);
        ContentDocumentLink cdl = new ContentDocumentLink(ShareType = 'V', LinkedEntityId = documents[0].Id, ContentDocumentId = contentVersion.ContentDocumentId, Visibility = 'AllUsers');
        insert cdl;
        docuSignStatuses[0].dsfs__Envelope_Status__c = 'Completed';
        docuSignStatuses[1].dsfs__Envelope_Status__c = 'Completed';
        update docuSignStatuses;
        List <ContentVersion> contentVersions = [SELECT VTD1_CompoundVersionNumber__c, VTD1_Current_Version__c, ContentDocumentId FROM ContentVersion];
        Map <Id, ContentDocument> contentDocuments = new Map<Id, ContentDocument>([SELECT Id FROM ContentDocument]);
        List <Id> contentDocumentIds = new List<Id>();
        for (ContentDocument contentDocument : contentDocuments.values()) {
            contentDocumentIds.add(contentDocument.Id);
        }
        List <ContentDocumentLink> contentDocumentLinks = [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE ContentDocumentId IN :contentDocumentIds];
        Map <Id, Id> docIdToContentDocIdMap = new Map<Id, Id>();
        for (ContentDocumentLink contentDocumentLink : contentDocumentLinks) {
            docIdToContentDocIdMap.put(contentDocumentLink.LinkedEntityId, contentDocumentLink.ContentDocumentId);
        }
        test.stopTest();
    }

    public static void test() {
        VTD1_Regulatory_Document__c regulatoryDocument = new VTD1_Regulatory_Document__c();
        regulatoryDocument.VTD1_Document_Type__c = 'Eligibility Assesment Form With TMA';
        regulatoryDocument.VTR2_Way_to_Send_with_DocuSign__c = 'Auto to PI';
        insert regulatoryDocument;
        //RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'VTD1_Patient_eligibility_assessment_form'];
        Id devRecordTypeId = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByName().get('Regulatory Document').getRecordTypeId();
        HealthCloudGA__CarePlanTemplate__c template = new HealthCloudGA__CarePlanTemplate__c();
        template.VTD1_Return_Process__c = 'Packaging Materials in Hand';
        template.VTD1_IMP_Return_Interval__c = 'Every Kit';
        template.VTD1_Return_IMP_To__c = 'Other';
        insert template;
        VTD1_Regulatory_Binder__c binder = new VTD1_Regulatory_Binder__c(VTD1_Care_Plan_Template__c = template.Id,
                VTD1_Regulatory_Document__c = regulatoryDocument.Id);
        insert binder;

        List <VTD1_Document__c> documents = new List<VTD1_Document__c>();
        for (Integer i = 0; i < 3; i++) {
            documents.add(new VTD1_Document__c(VTD1_Current_Workflow__c = 'Study RSU No IRB', RecordTypeId = devRecordTypeId, VTD1_Lifecycle_State__c = 'Draft', VTD1_Regulatory_Binder__c = binder.Id));
        }
        insert documents;
        List <ContentVersion> attachments = new List<ContentVersion>();

        for (Integer i = 0; i < 3; i++) {
            attachments.add(new ContentVersion(Title = 'Test_' + i, PathOnClient = 'Test_' + i + '.txt', VersionData = Blob.valueOf('Test Data_' + i), IsMajorVersion = true));
        }
        insert attachments;
        attachments = [SELECT ContentDocumentId FROM ContentVersion ORDER BY Title];
        Set <Id> contentDocIds = new Set<Id>();
        for (ContentVersion contentVersion : attachments) {
            contentDocIds.add(contentVersion.ContentDocumentId);
        }

        System.debug('attachments test = ' + attachments);
        List <ContentDocumentLink> cdls = new List<ContentDocumentLink>();
        cdls.add(new ContentDocumentLink(ShareType = 'V', LinkedEntityId = documents[0].Id, ContentDocumentId = attachments[0].ContentDocumentId, Visibility = 'AllUsers'));
        cdls.add(new ContentDocumentLink(ShareType = 'V', LinkedEntityId = documents[1].Id, ContentDocumentId = attachments[1].ContentDocumentId, Visibility = 'AllUsers'));
        cdls.add(new ContentDocumentLink(ShareType = 'V', LinkedEntityId = documents[2].Id, ContentDocumentId = attachments[2].ContentDocumentId, Visibility = 'AllUsers'));
        insert cdls;

        Test.startTest();
        List <ContentVersion> attachmentsNewVersion = new List<ContentVersion>();

        for (Integer i = 0; i < 3; i++) {
            attachmentsNewVersion.add(new ContentVersion(ContentDocumentId = attachments[i].ContentDocumentId, Title = 'Test_' + i, PathOnClient = 'Test_' + i + '.txt', VersionData = Blob.valueOf('Test Data_' + i), IsMajorVersion = true));
        }
        insert attachmentsNewVersion;

        List <ContentDocument> contentDocuments = [SELECT Id FROM ContentDocument WHERE Id IN :contentDocIds];
        VT_D1_ContentVersionProcessHandler.onDeleteBefore(contentDocuments);

        for (ContentVersion contentVersion : attachmentsNewVersion) {
            contentVersion.VTD1_Lifecycle_State__c = 'Approved';
        }

        update attachmentsNewVersion;
        documents[0].VTD1_Lifecycle_State__c = 'Approved';
        documents[0].VTD1_TMF_Complete__c = true;
        update documents;
        documents[0].VTD1_TMF_Complete__c = false;
        documents[0].VTD1_Lifecycle_State__c = 'Draft';
        update documents;
        System.debug('test end');
        Test.stopTest();
    }

    public static void test1() {
        ContentVersion contentVersion_1 = new ContentVersion(
                Title = 'Penguins',
                PathOnClient = 'Penguins.jpg',
                VersionData = Blob.valueOf('Test Content'),
                IsMajorVersion = true
        );
        insert contentVersion_1;
        VTD1_Document__c doc = new VTD1_Document__c(VTD1_Lifecycle_State__c = 'Superseded', RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT);

        insert doc;
        ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
        List<ContentVersion> conVerList = new List<ContentVersion>();
        conVerList.add(contentVersion_1);
        conVerList.add(contentVersion_2);
        Map<Id, ContentVersion> conVerMap = new Map<Id, ContentVersion>();
        conVerMap.put(doc.Id, contentVersion_2);
    }

    public static void test2() {
        VT_R3_GlobalSharing.disableForTest = true;
        Id devRecordTypeId = Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByName().get('Regulatory Document').getRecordTypeId();
        VTD1_Regulatory_Document__c regulatoryDocument = new VTD1_Regulatory_Document__c(VTD1_Document_Type__c = 'EDPRC', VTR2_Way_to_Send_with_DocuSign__c = 'Manual');
        insert regulatoryDocument;

        HealthCloudGA__CarePlanTemplate__c template = new HealthCloudGA__CarePlanTemplate__c();
        template.VTD1_Return_Process__c = 'Packaging Materials in Hand';
        template.VTD1_IMP_Return_Interval__c = 'Every Kit';
        template.VTD1_Return_IMP_To__c = 'Other';
        insert template;

        VTD1_Regulatory_Binder__c binder = new VTD1_Regulatory_Binder__c(VTD1_Care_Plan_Template__c = template.Id,
                VTD1_Regulatory_Document__c = regulatoryDocument.Id);
        insert binder;

        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c testStudy = VT_D1_TestUtils.prepareStudy(1);
        Virtual_Site__c site = new Virtual_Site__c(VTD1_Study__c = testStudy.Id, VTD1_Study_Site_Number__c = '12345');
        insert site;
        Test.stopTest();

        List <VTD1_Document__c> documents = new List<VTD1_Document__c>();
        for (Integer i = 0; i < 3; i++) {
            documents.add(new VTD1_Document__c(VTD1_Current_Workflow__c = 'Study RSU No IRB', RecordTypeId = devRecordTypeId, VTD1_Lifecycle_State__c = 'Draft', VTD1_Regulatory_Binder__c = binder.Id));
        }
        insert documents;

        documents[0].VTD1_Site__c = site.Id;
        update documents[0];

        documents[0].VTD1_Lifecycle_State__c = 'Approved';
        update documents[0];
    }

    public static void sendNotificationsTest() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c testStudy = VT_D1_TestUtils.prepareStudy(1);
        String userName = System.now().getTime() + 'sendnotificationstest@vtd1.com';



        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
        // for Secure Portal file uploading testing
        insert new VTD1_FileUploadUserCredentials__c(username__c = UserInfo.getUserName());
        Integer countCase = [SELECT COUNT() FROM Case];
        System.debug('Case count: ' + countCase);
        Case cas = [SELECT Id, VTD1_Subject_ID__c, VTD1_PI_contact__c FROM Case LIMIT 1];
//        Contact piContact = [SELECT Id, Email FROM Contact WHERE Id = :cas.VTD1_PI_contact__c LIMIT 1];
//        piContact.Email = userName;
//        update piContact;
        VTD1_Document__c document = new VTD1_Document__c(
                VTD1_Current_Workflow__c = 'Medical Records flow',
                RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_MEDICAL_RECORD,
                VTD1_Lifecycle_State__c = 'Draft',
                //VTD1_Clinical_Study_Membership__c = cas.Id,
                VTD1_PG_Approver__c = UserInfo.getUserId()
        );
        insert document;
        document.VTD1_Status__c = 'Approved';
        update document;
    }
}