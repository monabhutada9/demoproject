@IsTest
private class VT_D1_ZipSampleControllerTest {

    @TestSetup
    static void dataSetup() {
        Contact con = (Contact) new DomainObjects.Contact_t()
                .persist();
        new DomainObjects.Attachment_t()
                .setName('testName')
                .setParentId(con.Id)
                .setBody(Blob.valueOf('test'))
                .persist();
    }

    @IsTest
    static void uploadZipTest() {

        Test.startTest();
        try {
            VT_D1_ZipSampleController controller = new VT_D1_ZipSampleController();
            controller.zipFileName = 'test.zip';
            controller.zipContent = 'testData';
            PageReference pageReference = controller.uploadZip();
        } catch (Exception exc) {
            System.assert(String.isNotBlank(exc.getMessage()), 'Incorrect exception');
        }
        Test.stopTest();
    }

    @IsTest
    static void uploadZipTest_EmptyZipFileName() {

        Test.startTest();
        try {
            VT_D1_ZipSampleController controller = new VT_D1_ZipSampleController();
            controller.zipContent = 'testData';
            PageReference pageReference = controller.uploadZip();
        } catch (Exception exc) {
            System.assert(String.isNotBlank(exc.getMessage()), 'Incorrect exception');
        }
        Test.stopTest();
    }

    @IsTest
    static void getAttachmentTest() {
        Attachment att = [SELECT Id, Name, ParentId, Body FROM Attachment LIMIT 1];

        Test.startTest();
            VT_D1_ZipSampleController.AttachmentWrapper attachmentWrapper = VT_D1_ZipSampleController.getAttachment(att.Id);
        Test.stopTest();

        System.assertEquals(att.Name, attachmentWrapper.attName, 'Incorrect attachment name');
    }

    @IsTest
    static void getAttachmentsTest() {

        Test.startTest();
            VT_D1_ZipSampleController controller = new VT_D1_ZipSampleController();
            List<Attachment> attachments = controller.getAttachments();
        Test.stopTest();

        System.assertEquals(0, attachments.size(), 'Incorrect attachments list size');
    }
}