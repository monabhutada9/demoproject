/**
 * Created by Belkovskii Denis on 4/23/2020.
 */

public class VT_R4_Scheduler_Rem_Trig_Handler {





    public static void sendNotificationInfo(List<VTR4_Scheduler_Reminders__c> schReminders, Map<Id, VTR4_Scheduler_Reminders__c> schRemindersOldMap){




        if (VTR4_ExternalScheduler__c.getInstance().VTR4_RemindersIsActive__c){
            List<VT_R2_RemindersSender.ParamsHolder> paramsToSend = new List<VT_R2_RemindersSender.ParamsHolder>();
            for (VTR4_Scheduler_Reminders__c schReminder: schReminders){
                if (schReminder.VTR4_ReadyToStart__c && (schRemindersOldMap == null || !schRemindersOldMap.get(schReminder.Id).VTR4_ReadyToStart__c)){
                    paramsToSend.add(new VT_R2_RemindersSender.ParamsHolder(schReminder.VTR4_Actual_Visit__c,
                            (Integer)schReminder.VTR4_Hours_Before__c));
                }
            } 
            if(!paramsToSend.isEmpty()){
               VT_R2_RemindersSender.sendReminders(paramsToSend);



                VTD1_Integration_Log__c triggerLog = new VTD1_Integration_Log__c(



                        VTD1_Body_Response__c = String.valueOf(paramsToSend.size()),
                        VTD1_Action__c = 'Reminders'
               );
               insert triggerLog;
           }
        }
    }

    /**
     * The method push the eConsent platform event to show/hide the eConsnet button
     * @author  Aleh Lutsevich
     * @param schReminders Trigger.new
     * @param schRemindersOldMap Trigger.oldMap
     * @date 23/06/2020
     * @issue SH-12465
     */
    public static void sendConsentEvent(List<VTR4_Scheduler_Reminders__c> schReminders, Map<Id, VTR4_Scheduler_Reminders__c> schRemindersOldMap) {
        if (VTR4_ExternalScheduler__c.getInstance().VTR4_RemindersIsActive__c){
            Set<Id> visitIdSet = new Set<Id>();
            for (VTR4_Scheduler_Reminders__c schReminder: schReminders){
                if (schReminder.VTR4_ReadyToStart__c && (schRemindersOldMap == null || !schRemindersOldMap.get(schReminder.Id).VTR4_ReadyToStart__c)){
                    visitIdSet.add(schReminder.VTR4_Actual_Visit__c);
                }
            }
            sendConsentEvent(visitIdSet);
        }
    }

    /**
     * The method push the eConsent platform event to show/hide the eConsnet button for visits
     * @author  Aleh Lutsevich
     * @param visitIdSet Trigger.new
     * @date 23/06/2020
     * @issue SH-12465
     */
    public static void sendConsentEvent(Set<Id> visitIdSet) {
        if (!visitIdSet.isEmpty() && VTR4_ExternalScheduler__c.getInstance().VTR4_RemindersIsActive__c){
            List<VTD1_Actual_Visit__c> consentVisitList = [
                    SELECT  Id,
                            Unscheduled_Visits__r.AccountId,
                            VTD1_Case__r.AccountId
                    FROM    VTD1_Actual_Visit__c
                    WHERE   Id IN :visitIdSet
                    AND (
                            (VTD1_Unscheduled_Visit_Type__c = 'Consent' OR VTD1_Unscheduled_Visit_Type__c = 'Re-Consent')
                            OR VTD1_Onboarding_Type__c = 'Consent'
                    )
            ];
            Set<Id> currentAccountIds = new Set<Id>();
            for(VTD1_Actual_Visit__c currentVisit : consentVisitList) {
                if(currentVisit.Unscheduled_Visits__r != null) {
                    currentAccountIds.add(currentVisit.Unscheduled_Visits__r.AccountId);
                }
                if(currentVisit.VTD1_Case__r != null) {
                    currentAccountIds.add(currentVisit.VTD1_Case__r.AccountId);
                }

            }
            List<Id> userIds = VT_R5_RunConsentVisit.getUserIds(currentAccountIds);
            List<VTR4_CNotification__e> eventList = new List<VTR4_CNotification__e>();
            for(Id userId : userIds) {
                if(String.isBlank(userId)) {
                    continue;
                }
                eventList.add(new VTR4_CNotification__e(VTR4_UserId__c = userId, VTR4_Type__c = VT_R5_RunConsentVisit.EVENT_TYPE));
            }
            EventBus.publish(eventList);
        }
    }
}