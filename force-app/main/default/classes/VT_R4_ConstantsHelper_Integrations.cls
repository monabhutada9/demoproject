public with sharing class VT_R4_ConstantsHelper_Integrations {
    public static final String MULESOFT_SERVICE_URL = 'callout:Mulesoft_Subject';
    public static final String MULESOFT_SERVICE_DELIVERY_URL ='callout:Mulesoft_Delivery';
    public static final String MULESOFT_SERVICE_DELIVERY_URL_1_1 ='callout:Mulesoft_Delivery_1_1';
    public static final String MULESOFT_SERVICE_REPLACEMENT_URL ='callout:Mulesoft_Replacement';
    public static final String MULESOFT_ECOA_RESPONSE_URL ='callout:eCOA_Mulesoft_Response_API';
    public static final String MULESOFT_ECOA_SUBJECT_URL ='callout:eCOA_Mulesoft_Subject_API';
    public static final String MULESOFT_ECOA_SCHEDULE_URL ='callout:eCOA_Mulesoft_Schedule_API';

    public static final String INTEGRATION_TIMESTAMP_FORMAT = 'yyyy-MM-dd HH:mm:ss.SSS';
    public static final String INTEGRATION_ACTION_CREATE_SUBJECT = 'Create Subject';
    public static final String INTEGRATION_ACTION_UPDATE_SUBJECT = 'Update Subject';
    public static final String INTEGRATION_ACTION_RANDOMIZE_SUBJECT = 'Randomize Subject';
    public static final String INTEGRATION_ACTION_RECEIVED_ORDER = 'Received Order';
    public static final String INTEGRATION_ACTION_ADHOC_REPLACEMENT = 'Ad-hoc Replacement';
    public static final String ENDPOINT_URL_CREATE_UPDATE_SUBJECT = MULESOFT_SERVICE_URL+'/subject';//'http://neoexpapis.cloudhub.io/api/subject';
    public static final String ENDPOINT_URL_RANDOMIZE_SUBJECT = MULESOFT_SERVICE_URL+'/subject/randomize';//'https://rds-neo-subject-exp-1-0-dev.cloudhub.io/api/randomize';//'http://neoexpapis.cloudhub.io/api/randomize';
    public static final String ENDPOINT_URL_RECEIVED_ORDER = MULESOFT_SERVICE_DELIVERY_URL+'/shipment/received-state';
    public static final String ENDPOINT_URL_RECEIVED_ORDER_1_1 = MULESOFT_SERVICE_DELIVERY_URL_1_1+'/shipment/received-state';
    public static final String ENDPOINT_URL_REPLACEMENT = MULESOFT_SERVICE_REPLACEMENT_URL+'/shipment/replacement';
    public static final String ENDPOINT_URL_ECOA_RESPONSES = MULESOFT_ECOA_RESPONSE_URL + '/studies';
    public static final String ENDPOINT_URL_ECOA_SUBJECT_CREATE = MULESOFT_ECOA_SUBJECT_URL + '/subjects';
    public static final String ENDPOINT_URL_ECOA_SUBJECT_UPDATE = MULESOFT_ECOA_SUBJECT_URL + '/subjects/:subjectGuid';
    public static final String ENDPOINT_URL_ECOA_SUBJECT_UPDATE_BLIND = MULESOFT_ECOA_SUBJECT_URL + '/subjects/:subjectGuid/contact';
    public static final String ENDPOINT_URL_ECOA_SUBJECT_ADD_EVENT = MULESOFT_ECOA_SUBJECT_URL + '/subjects/:subjectGuid/events';
    public static final String ENDPOINT_URL_ECOA_SCHEDULE = MULESOFT_ECOA_SCHEDULE_URL + '/schedules';
    public static final String POST_METHOD = 'POST';
    public static final String PUT_METHOD = 'PUT';
    public static final String test1 = '1';
    public static final String ECOA_LOGIN_TYPE = 'SSO';
    public static final String STRINGDIAGNOSISDATE = 'DiagnosisDate';
    public static final String STRINGSTATE = 'State';
    public static final String STRINGGENDER = 'Gender';
    public static final String STRINGDEFAULTDATE = '01';

}