/**
 * Created by User on 19/05/16.
 */
@IsTest
public with sharing class VT_D1_NotifySponsorTMATaskResetTest {

    public static void doTest() {
        DomainObjects.User_t user = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5));

        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'));

        DomainObjects.Case_t cas = new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .addUser(user)
                .addStudy(study);

        new DomainObjects.StudyTeamMember_t()
                .setRecordTypeByName('RE')
                .addUser(user)
                .addStudy(study);

        //User user2 = (User) user;
        User user2 = (User) new DomainObjects.User_t()
                .setEmail('test@test2019.test')
                .setProfile('System Administrator')
                .persist();

        System.debug('user2 ' + user2.Id);

        List<VTD1_Protocol_Deviation__c> pdList = new List<VTD1_Protocol_Deviation__c>();
        List<Task>taskList = new List<Task>();

        VTD1_Protocol_Deviation__c pd = new VTD1_Protocol_Deviation__c(
                OwnerId = user2.Id,
                VTD1_Site_Subject_Level__c = 'Site'
        );
        pdList.add(pd);

        VTD1_Protocol_Deviation__c pd2 = new VTD1_Protocol_Deviation__c(
                OwnerId = user2.Id,
                VTD1_Site_Subject_Level__c = 'Subject'
        );
        pdList.add(pd2);

        insert pdList;

        Task task = (Task) new DomainObjects.Task_t()
                .addUser(user)
                .addCase(cas)
                .addStudy(study)
                .setRecordTypeByName('VTR2_RE_Task')
                .setStatus('Open')
                .persist();
        task.WhatId = pd.Id;
        taskList.add(task);

        Task task2 = (Task) new DomainObjects.Task_t()
                .addUser(user)
                .addCase(cas)
                .addStudy(study)
                .setRecordTypeByName('VTR2_RE_Task')
                .setStatus('Open')
                .persist();
        task2.WhatId = pd2.Id;
        taskList.add(task2);

        update taskList;

        Test.startTest();
        String query = 'select id, Subject, Status, CreatedDate, WhatId, OwnerId from Task';
        Database.executeBatch(new VT_D1_NotifySponsorTMATaskReset(query));
        Test.stopTest();

    }
}