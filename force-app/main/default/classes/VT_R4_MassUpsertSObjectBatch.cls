/**
 * Created by Alexey Mezentsev on 5/8/2020.
 */

public class VT_R4_MassUpsertSObjectBatch implements Database.Batchable<SObject> {
    private List<SObject> recordsToUpsert;

    public VT_R4_MassUpsertSObjectBatch(List<SObject> recordsToUpsert) {
        this.recordsToUpsert = recordsToUpsert;
    }

    public Iterable<SObject> start(Database.BatchableContext bc) {
        return recordsToUpsert;
    }

    public void execute(Database.BatchableContext bc, List<SObject> recordsToUpsert) {
        upsert recordsToUpsert;
    }

    public void finish(Database.BatchableContext bc) {
    }
}