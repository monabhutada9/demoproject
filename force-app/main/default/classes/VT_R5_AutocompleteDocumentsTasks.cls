/**
 * Created by Denis Belkovskii on 9/7/2020.
 */

public without sharing class VT_R5_AutocompleteDocumentsTasks extends Handler {

    /*Ids lists to filter Task records*/
    public static List<Id> virtualSiteIds = new List<Id>();
    public static List<Id> certificationSignedDocIds = new List<Id>();
    public static List<Id> associatedDocIds = new List<Id>();
    public static List<Id> categoryChangedDocIds = new List<Id>();
    public static List<Id> approvedMedicalRecordsIds = new List<Id>();
    public static List<Id> piDecisionDocIds = new List<Id>();
    public static List<Id> finalPiDecisionDocIds = new List<Id>();
    public static List<Id> tmaDecisionDocIds = new List<Id>();
    public static List<String> MEDICAL_RECORDS_TO_CLOSE_TASKS = new List<String>{
            VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED,
            VT_R4_ConstantsHelper_Documents.DOCUMENT_REJECTED
    };

    protected override void onAfterUpdate(Handler.TriggerContext context) {
        List<VTD1_Document__c> newDocuments = (List<VTD1_Document__c>) context.newList;
        Map<Id, VTD1_Document__c> oldDocumentsMap = (Map<Id, VTD1_Document__c>) context.oldMap;
        Map<Id, VTD1_Document__c> documentsMap = getDocumentsMap(context.newMap.keySet());

        for(VTD1_Document__c doc : newDocuments){
            String docRecordTypeDevName = '';
            if(!documentsMap.isEmpty()
                    && documentsMap.get(doc.Id) != null){
                docRecordTypeDevName = documentsMap.get(doc.Id).RecordType.DeveloperName;
            }

            /*Close related tasks - PI Closeout Submission complete*/
            if(docRecordTypeDevName.contains('VTD1_Regulatory_Document')
                    && doc.VTD1_Regulatory_Document_Type__c == 'PI Closeout IRB Submission'
                    && doc.VTD1_Status__c == 'TMF Started'
                    && oldDocumentsMap.get(doc.Id).VTD1_Status__c != 'TMF Started'){
                virtualSiteIds.add(doc.VTD1_Site__c);
            }

            /*Certification signed*/
            if(doc.VTD1_Files_have_not_been_edited__c
                    && !oldDocumentsMap.get(doc.Id).VTD1_Files_have_not_been_edited__c){
                certificationSignedDocIds.add(doc.Id);
            }

            /*Regulatory Document is Certified*/
            if(docRecordTypeDevName == 'VTD2_Certification'
                    && oldDocumentsMap.get(doc.Id).VTD1_Status__c == 'Pending Certification'
                    && doc.VTD1_Status__c == 'Certified'){
                associatedDocIds.add(doc.VTD2_Associated_Medical_Record_id__c);
            }

            /*Change categories*/
            if(oldDocumentsMap.get(doc.Id).VTR3_Category_add__c == 'Uncategorized'
                    && (doc.VTR3_Category_add__c != 'Uncategorized'
                    || doc.RecordTypeId != oldDocumentsMap.get(doc.Id).RecordTypeId)){
                categoryChangedDocIds.add(doc.Id);
            }

            /*Medical Record is Approved*/
            if(docRecordTypeDevName.contains('Medical')
                    && MEDICAL_RECORDS_TO_CLOSE_TASKS.contains(doc.VTD1_Status__c)
                    && oldDocumentsMap.get(doc.Id).VTD1_Status__c != doc.VTD1_Status__c){
                approvedMedicalRecordsIds.add(doc.Id);
            }

            if(docRecordTypeDevName == 'VTD1_Patient_eligibility_assessment_form'){
                if(doc.VTD1_Eligibility_Assessment_Status__c != null
                        && oldDocumentsMap.get(doc.Id).VTD1_Eligibility_Assessment_Status__c == null){
                    piDecisionDocIds.add(doc.Id);
                }

                if(doc.VTD2_Final_Eligibility_Decision__c
                        != oldDocumentsMap.get(doc.Id).VTD2_Final_Eligibility_Decision__c){
                    finalPiDecisionDocIds.add(doc.Id);
                }

                if(doc.VTD2_TMA_Eligibility_Decision__c
                        != oldDocumentsMap.get(doc.Id).VTD2_TMA_Eligibility_Decision__c){
                    tmaDecisionDocIds.add(doc.Id);
                }
            }
        }
        completeTasks();
    }

    public static Map<Id, VTD1_Document__c> getDocumentsMap(Set<Id> documentIds){
        Map<Id, VTD1_Document__c> documentsMap = new Map<Id, VTD1_Document__c>([
                SELECT Id, RecordType.DeveloperName
                FROM VTD1_Document__c
                WHERE Id IN :documentIds
        ]);
        return documentsMap;
    }

    public static void completeTasks(){
        List<String> addToQueryStrings = new List<String>();

        if(!virtualSiteIds.isEmpty()){
            addToQueryStrings.add('(VTR5_VirtualSite__c IN :virtualSiteIds AND VTD2_Task_Unique_Code__c = \'006\' ' +
                    'AND Status = \'Open\')');
        }
        if(!certificationSignedDocIds.isEmpty()){
            addToQueryStrings.add('(WhatId IN :certificationSignedDocIds  AND VTD2_Task_Unique_Code__c = \'T534\' ' +
                    'AND Status = \'Open\')');
        }
        if(!associatedDocIds.isEmpty()){
            addToQueryStrings.add('(VTD2_Task_Unique_Code__c IN (\'T612\', \'T611\') ' +
                    'AND WhatId IN :associatedDocIds AND Status = \'Open\')');
        }
        if(!categoryChangedDocIds.isEmpty()){
            addToQueryStrings.add('(WhatId IN :categoryChangedDocIds AND VTD2_Task_Unique_Code__c = \'T533\' ' +
                    'AND Status = \'Open\')');
        }
        if(!approvedMedicalRecordsIds.isEmpty()){
            addToQueryStrings.add('(WhatId IN :approvedMedicalRecordsIds AND VTD2_Task_Unique_Code__c = \'001\' ' +
                    'AND Status = \'Open\')');
        }

        if(!piDecisionDocIds.isEmpty()){
            addToQueryStrings.add('(Document__c IN :piDecisionDocIds AND ' +
                    'VTD1_Type_for_backend_logic__c = \'Sign Patient Eligibility Assessment Form task\')');
        }

        if(!finalPiDecisionDocIds.isEmpty()){
            addToQueryStrings.add('(Document__c IN :finalPiDecisionDocIds AND ' +
                    'VTD1_Type_for_backend_logic__c = \'Final Eligibility Decision Task\')');
        }

        if(!tmaDecisionDocIds.isEmpty()){
            addToQueryStrings.add('(Document__c IN :tmaDecisionDocIds AND ' +
                    'VTD1_Type_for_backend_logic__c = \'TMA task\')');
        }

        if(!addToQueryStrings.isEmpty()){
            String queryTasksString = 'SELECT Id FROM Task WHERE ' + String.join(addToQueryStrings, ' OR ');
            if(!String.isEmpty(queryTasksString)){
                System.debug('query string is: ' + queryTasksString);
                List<Task> tasksToClose = Database.query(queryTasksString);
                if(!tasksToClose.isEmpty()){
                    for(Task task : tasksToClose){
                        task.Status = 'Completed';
                    }
                    update tasksToClose;
                }
            }
        }
    }

}