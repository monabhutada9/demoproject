/**
 * Created by Alexander Komarov on 18.03.2019.
 * @see VT_R2_PatientHistoryHandler
 * @see VT_R2_PatientHistoryScheduler
 * @see VT_R5_PatientHistoryBatch
 */

@IsTest
public class VT_R2_PatientHistoryHandlerTest {

    public static void firstTest() {
        String error = '';
        Test.startTest();
        Address__c add = [SELECT Id FROM Address__c LIMIT 1];
        add.Name = '';
        add.City__c = 'MYPERFECTCITY';
        update add;

        add.Name = 'Another';
        add.City__c = 'MY_PERFECT_CITY';
        update add;

        add.Name = 'And another';
        add.City__c = null;
        update add;

        add.Name = '';
        add.City__c = null;
        update add;

        add.Name = '';
        add.City__c = 'MY_CITY';
        update add;

        try {
            delete add;
        } catch (Exception e) {
            if (e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) {
                error = 'validation';
            }
        }
        System.assertEquals(error,'validation');

        Case carePlan = [
                SELECT Id
                FROM Case
                WHERE RecordType.DeveloperName = 'CarePlan'
                LIMIT 1
        ];
        VTD1_Physician_Information__c pInfo = new VTD1_Physician_Information__c();
        pInfo.VTD1_Clinical_Study_Membership__c = carePlan.Id;
        insert pInfo;

        Integer temporaryRecordsCount = [SELECT COUNT() FROM VTR6_PatientFieldHistoryTemporary__c];
        System.assert(temporaryRecordsCount>0);

        System.schedule('testScheduledApex1', '0 0 0 1 1 ?', new VT_R2_PatientHistoryScheduler());
        System.schedule('testScheduledApex2', '0 0 0 1 1 ?', new VT_R2_PatientHistoryScheduler(10));
        Database.executeBatch(new VT_R5_PatientHistoryBatch(),200);
        Test.stopTest();

        temporaryRecordsCount = [SELECT COUNT() FROM VTR6_PatientFieldHistoryTemporary__c];
      //  System.assertEquals(0, temporaryRecordsCount);
    }

//    @IsTest
//    private static void secondTest() {
//        Test.startTest();
//        List<VTR2_Patient_Field_History__b> bList = new List<VTR2_Patient_Field_History__b>();
//        VTR2_Patient_Field_History__b pfh = new VTR2_Patient_Field_History__b();
//        pfh.Date__c = System.now();
//        pfh.Patient_Id__c = 'sampleString';
//        pfh.Hash__c = 'sampleHash';
//        bList.add(pfh);
//
//        String CRON_EXP = '0 0 0 1 1 ? 2025';
//        System.schedule('testScheduledApex', CRON_EXP, new VT_R2_PatientHistoryScheduler(bList) );
//
//        Test.stopTest();
//    }
}