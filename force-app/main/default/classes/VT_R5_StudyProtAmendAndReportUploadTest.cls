/**
 * Created by Denis Belkovskii on 10/15/2020.
 */

@IsTest
public with sharing class VT_R5_StudyProtAmendAndReportUploadTest {

    @TestSetup
    static void setup() {
        List<VTD2_TN_Catalog_Code__c> tnCatalogCodes = new List<VTD2_TN_Catalog_Code__c>();
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'N555',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Notification').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'HealthCloudGA__CarePlanTemplate__c',
                VTD2_T_Has_direct_link__c = true,
                 VTD2_T_Link_to_related_event_or_object__c = 'HealthCloudGA__CarePlanTemplate__c.Id',
                VTR4_T_Number_of_unread_messages__c = 1,
                VTD2_T_Type__c = 'General',
                VTD2_T_Title__c = 'test',
                VTD2_T_Message__c = 'test'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'N564',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Notification').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'HealthCloudGA__CarePlanTemplate__c',
                VTD2_T_Has_direct_link__c = true,
                VTD2_T_Link_to_related_event_or_object__c = 'HealthCloudGA__CarePlanTemplate__c.Id',
                VTR4_T_Number_of_unread_messages__c = 1,
                VTD2_T_Type__c = 'General',
                VTD2_T_Title__c = 'test2',
                VTD2_T_Message__c = 'test2'
        ));
        insert tnCatalogCodes;
        DomainObjects.Study_t domainStudy = new DomainObjects.Study_t()
                .setOriginalName('TestStudyForProtocolAmendmentsUploadTest')
                .addVTD1_Virtual_Trial_Study_Lead(new DomainObjects.User_t());
        HealthCloudGA__CarePlanTemplate__c testStudy = (HealthCloudGA__CarePlanTemplate__c) domainStudy.persist();

        List<Study_Team_Member__c> stms = new List<Study_Team_Member__c>();

        Study_Team_Member__c scStm = (Study_Team_Member__c) new DomainObjects.StudyTeamMember_t()
                .setStudy(testStudy.Id)
                .addUser(new DomainObjects.User_t()
                        .setProfile('Study Concierge'))
                .persist();
        scStm.RecordTypeId = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('SC').getRecordTypeId();
        stms.add(scStm);

        Study_Team_Member__c piStm = (Study_Team_Member__c) new DomainObjects.StudyTeamMember_t()
                .setStudy(testStudy.Id)
                .addUser(new DomainObjects.User_t()
                        .setProfile('Primary Investigator')
                        .addContact(new DomainObjects.Contact_t()))
                .persist();
        piStm.RecordTypeId = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('PI').getRecordTypeId();
        stms.add(piStm);

        Study_Team_Member__c pgStm = (Study_Team_Member__c) new DomainObjects.StudyTeamMember_t()
                .setStudy(testStudy.Id)
                .addUser(new DomainObjects.User_t()
                        .setProfile('Patient Guide'))
                .persist();
        pgStm.RecordTypeId = Schema.SObjectType.Study_Team_Member__c.getRecordTypeInfosByDeveloperName().get('PG').getRecordTypeId();
        stms.add(pgStm);
        update stms;
    }

    @IsTest
    public static void testTasks() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c testStudy = [
                SELECT Id
                FROM HealthCloudGA__CarePlanTemplate__c
                WHERE Name = 'TestStudyForProtocolAmendmentsUploadTest'
                LIMIT 1
        ];
        User lead = (User) new DomainObjects.User_t().persist();
        testStudy.VTD1_Virtual_Trial_Study_Lead__c = lead.Id;
        testStudy.VTD1_ProtocolAmendment_DocumentsUploaded__c = true;
        update testStudy;
        testStudy.VTR5_Patient_Data_Deletion_Allowed__c = true;
        testStudy.VTD1_StudyStatus__c = 'Completed';
        update testStudy;

        List<VTD1_NotificationC__c> notifications = [
                SELECT Id FROM VTD1_NotificationC__c
                WHERE  VDT2_Unique_Code__c IN ('N555', 'N564')
        ];
        System.assertEquals(false, notifications.isEmpty());
        Test.stopTest();
    }
}