/**
 * @author: Alexander Komarov
 * @date: 14.09.2020
 * @description: service for mobile app, works with push notification registrations
 *
 * ACTIVE VERSION => v1
 *
 */

public without sharing class VT_R5_MobilePushRegService extends VT_R5_MobileRestResponse implements VT_R5_MobileRestRouter.Routable {

    public Boolean success = false;

    public List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/push-service/'
        };
    }

    public void versionRoute(Map<String, String> parameters) {
        initService();
        String requestVersion = parameters.get('version');
        switch on this.request.httpMethod {
            when 'POST' {
                switch on requestVersion {
                    when 'v1' {
                        post_v1();
                        return;
                    }
                }
            }
            when 'DELETE' {
                switch on requestVersion {
                    when 'v1' {
                        delete_v1();
                        return;
                    }
                }
            }
        }
        VT_R5_MobileRestRouter.unsupportedVersionResponse();
        this.addError('unsupported version');
        this.sendResponse();
    }

    private void delete_v1() {
        try {
            RequestParams requestParams = (RequestParams) JSON.deserialize(request.requestBody.toString(), RequestParams.class);
//            removeToken(requestParams);
            this.success = true;
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private void removeToken(RequestParams requestParams) {
        List<VTR5_MobilePushRegistrations__c> toDel = [SELECT Id FROM VTR5_MobilePushRegistrations__c WHERE VTR5_DeviceIdentifier__c = :requestParams.deviceUniqueCode AND VTR5_User__c = :UserInfo.getUserId()];
        if (!toDel.isEmpty()) delete toDel;
    }

    private void post_v1() {
        try {
            RequestParams requestParams = (RequestParams) JSON.deserialize(request.requestBody.toString(), RequestParams.class);
            removeOldTokens(requestParams);
            tokenRegistration(requestParams);
            this.success = true;
        } catch (Exception e) {
            this.buildResponse(e);
        }
        this.sendResponse();
    }

    private void removeOldTokens(RequestParams requestParams) {
        List<VTR5_MobilePushRegistrations__c> currentUserRegistrations = [
                SELECT
                        Id,
                        VTR5_ConnectionToken__c,
                        VTR5_DeviceIdentifier__c,
                        VTR5_ServiceType__c,
                        VTR5_User__c
                FROM VTR5_MobilePushRegistrations__c
                WHERE VTR5_User__c = :UserInfo.getUserId()
        ];
        List<VTR5_MobilePushRegistrations__c> toDelete = new List<VTR5_MobilePushRegistrations__c>();
        for (VTR5_MobilePushRegistrations__c mpr : currentUserRegistrations) {
            if (requestParams.deviceUniqueCode.equalsIgnoreCase(mpr.VTR5_DeviceIdentifier__c)) {
                toDelete.add(mpr);
            }
        }
        if (!toDelete.isEmpty()) delete toDelete;
    }

    private void tokenRegistration(RequestParams requestParams) {
        insert new VTR5_MobilePushRegistrations__c(
                VTR5_ConnectionToken__c = requestParams.connectionToken,
                VTR5_DeviceIdentifier__c = requestParams.deviceUniqueCode,
                VTR5_ServiceType__c = requestParams.serviceType,
                VTR5_User__c = UserInfo.getUserId()
        );
    }

    public void initService() {
    }


    private class RequestParams {
        private String deviceUniqueCode;
        private String connectionToken;
        private String serviceType;
    }
}