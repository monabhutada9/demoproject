/**
 * Created by User on 19/05/13.
 */
@IsTest
public with sharing class VT_R2_PIMyPatientEDiaryControllerTest {
    
    public static void getSurveysTest(){
        String type = 'eDiary';
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );
        DomainObjects.User_t user = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5));
        Case cas = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .addStudy(study)
                .addUser(user)
                .persist();
        DomainObjects.VTD1_Protocol_ePRO_t protocolEPROT = new DomainObjects.VTD1_Protocol_ePRO_t()
                .setRecordTypeByName('SatisfactionSurvey')
                .addVTD1_Study(study)
                .setVTD1_Type('Non-PSC')
                .setVTR2_Protocol_Reviewer('Patient Guide')
                .setVTD1_Subject('Patient')
                .setVTD1_Response_Window(10);
        DomainObjects.VTD1_Survey_t survey = new DomainObjects.VTD1_Survey_t()
                .setVTD1_CSM(cas.Id)
                .addVTD1_Protocol_ePRO(protocolEPROT);
        survey.persist();
        User scrUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Site Coordinator')
                .persist();
        User piUser = (User) new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Primary Investigator')
                .persist();
        Test.startTest();
        System.runAs(scrUser) {
          // Updated for epic : 16680
             System.assertNotEquals(null, VT_R2_PIMyPatientEDiaryController.getSurveys(cas.Id, type, 'DESC', null, null));
             System.assertNotEquals(null, VT_R2_PIMyPatientEDiaryController.getSurveys(cas.Id, type, 'DESC', 'allEdiaryFilter', null));
             System.assertNotEquals(null, VT_R2_PIMyPatientEDiaryController.getSurveys(cas.Id, type, 'DESC', 'missedEDiariesFilter', null));
             System.assertNotEquals(null, VT_R2_PIMyPatientEDiaryController.getSurveys(cas.Id, type, 'DESC', 'futureEDiariesFilter', 'testSearch'));
        }
        System.runAs(piUser) {
          // Updated for epic : 16680   
             System.assertNotEquals(null, VT_R2_PIMyPatientEDiaryController.getSurveys(cas.Id, type, 'ASC', null, null));
        }
        Test.stopTest();
    }

    public static void updateSurveyTest() {
        VTD1_Survey__c survey = [SELECT Id, VTR5_Reviewed__c FROM VTD1_Survey__c LIMIT 1];
        survey.VTR5_Reviewed__c = true;

        Test.startTest();
        VT_R2_PIMyPatientEDiaryController.updateSurvey(survey);
        Test.stopTest();

        survey = [SELECT Id, VTR5_Reviewed__c FROM VTD1_Survey__c WHERE Id = :survey.Id];
        System.assert(survey.VTR5_Reviewed__c, true);
    }
}