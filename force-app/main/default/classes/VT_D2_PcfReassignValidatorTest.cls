/**
* @author: Carl Judge
* @date: 25-Jan-19
* @description: Test for VT_D2_PcfReassignValidator
**/

@IsTest
public class VT_D2_PcfReassignValidatorTest {

    @TestSetup
    private static void setupMethod() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);

        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();

        Case carePlan = [
            SELECT Id, VTD1_Primary_PG__c, VTD1_PI_user__c
            FROM Case
            WHERE RecordType.DeveloperName = 'CarePlan'
            ORDER BY CreatedDate DESC
            LIMIT 1
        ];

        Case pcf = new Case(
            OwnerId = carePlan.VTD1_Primary_PG__c,
            VTD1_Clinical_Study_Membership__c = carePlan.Id,
            RecordTypeId = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF,
            VTD1_PCF_Safety_Concern_Indicator__c = 'No'
        );
        insert pcf;
        pcf.VTD1_PCF_Safety_Concern_Indicator__c = 'Possible';
        pcf.VTD2_Reason_Safety_Concern_Indicator_Cha__c = 'test';
        update pcf;
    }

    @IsTest
    private static void doTest() {
        Case carePlan = [
            SELECT Id, VTD1_Primary_PG__c, VTD1_PI_user__c
            FROM Case
            WHERE RecordType.DeveloperName = 'CarePlan'
            ORDER BY CreatedDate DESC
            LIMIT 1
        ];

        Case pcf = [SELECT Id FROM Case WHERE VTD1_Clinical_Study_Membership__c = :carePlan.Id];

        User u = [SELECT Id FROM User WHERE Id =: carePlan.VTD1_Primary_PG__c];
        Test.startTest();
        System.runAs(u) {
            pcf.OwnerId = carePlan.VTD1_PI_user__c;
            update pcf;
        }
        Test.stopTest();
    }
}