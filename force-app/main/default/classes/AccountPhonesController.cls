public with sharing class AccountPhonesController {
	public class AccountPhoneWrapper {
        @AuraEnabled VT_D1_Phone__c phoneRec{ get; set; }
        @AuraEnabled Boolean isEdited { get; set; }
        
        public AccountPhoneWrapper() {

        }
    }
    
    @AuraEnabled
    public static List<VT_D1_Phone__c> getAccPhonesList(Id accId) {
    	system.debug('!!! 1accId='+accId);
    	if(accId==null) return new List<VT_D1_Phone__c>();
    	
        return [Select Id, Name, PhoneNumber__c, IsPrimaryForPhone__c, IsPrimaryForText__c, Type__c, Account__c	 
                From VT_D1_Phone__c where Account__c=:accId order by CreatedDate];
        //else
        //return [Select Id, Name, PrimaryPhoneNumber__c, Type__c	 
                //From VT_D1_Phone__c order by CreatedDate desc];
    }
    @AuraEnabled
    public static List<AccountPhoneWrapper> getAccPhonesWrapList(Id accId) {
    	system.debug('!!! 2accId='+accId);//getSobjectType //globalDescribe
    	if(accId==null) return new List<AccountPhoneWrapper>();
    	
        List<VT_D1_Phone__c> accPhoneList = [Select Id, Name, PhoneNumber__c, IsPrimaryForPhone__c, IsPrimaryForText__c, Type__c, Account__c	 
                From VT_D1_Phone__c where Account__c=:accId order by CreatedDate];
                
        if(accPhoneList==null || accPhoneList.isEmpty()) return new List<AccountPhoneWrapper>();
        
        List<AccountPhoneWrapper> accPhoneWrapList = new List<AccountPhoneWrapper>();
        for(VT_D1_Phone__c ap:accPhoneList){
        	AccountPhoneWrapper apw = new AccountPhoneWrapper();
        	apw.phoneRec = ap;
        	apw.isEdited = false;
        	accPhoneWrapList.add(apw);
        }
        
        return accPhoneWrapList;
        
        //else
        //return [Select Id, Name, PrimaryPhoneNumber__c, Type__c	 
                //From VT_D1_Phone__c order by CreatedDate desc];
    }
 
    @AuraEnabled
    public static void saveAccPhones(String accPhoneListWrapStr, String accPhoneDeleteListWrapStr){//List<VT_D1_Phone__c> accPhoneList
    	system.debug('!!! In saveAccPhones');
    	system.debug('list = '+accPhoneListWrapStr);
    	system.debug('list = '+accPhoneDeleteListWrapStr);
	
		if(String.isBlank(accPhoneListWrapStr)) return;
    	
    	List<AccountPhoneWrapper> accPhoneListWrap = (List<AccountPhoneWrapper>)JSON.deserialize(accPhoneListWrapStr, List<AccountPhoneWrapper>.class);
    	
    	List<AccountPhoneWrapper> accPhoneDeleteListWrap = (List<AccountPhoneWrapper>)JSON.deserialize(accPhoneDeleteListWrapStr, List<AccountPhoneWrapper>.class);
    	
    	//upsert rows
    	if(accPhoneListWrap!=null && !accPhoneListWrap.isEmpty()){
    	
    	system.debug('!!! accPhoneListWrap[0]='+accPhoneListWrap[0]);
    	system.debug('!!! accPhoneListWrap[0].phoneRec='+accPhoneListWrap[0].phoneRec);
    	
    	List<VT_D1_Phone__c> updateList = new List<VT_D1_Phone__c>();
    	List<VT_D1_Phone__c> insertList = new List<VT_D1_Phone__c>();
    	
    	for(AccountPhoneWrapper apwItem:accPhoneListWrap){
    		system.debug('In for');
    		system.debug('apwItem='+apwItem);
    		system.debug('phoneRec='+apwItem.phoneRec);
    		if(apwItem==null || apwItem.phoneRec==null) continue;
    		
    		system.debug('111');
    		if(apwItem.phoneRec.Id!=null){
    				system.debug('222');
        			updateList.add(apwItem.phoneRec);
    		}
        		else{
        			system.debug('333');
        			insertList.add(apwItem.phoneRec);  	
        		}	
        	system.debug('444');
    	}
    	  	
    	system.debug('list = '+insertList);
    	system.debug('list = '+updateList);
    	
    	if(insertList!=null && !insertList.isEmpty())
    		insert insertList;
    	if(updateList!=null && !updateList.isEmpty())
    		update updateList;
    		
    	}
    	
    	//delete rows
    	if(accPhoneDeleteListWrap!=null && !accPhoneDeleteListWrap.isEmpty()){
    	
    		List<VT_D1_Phone__c> deleteList = new List<VT_D1_Phone__c>();
    	
    		for(AccountPhoneWrapper apwItem:accPhoneDeleteListWrap){
    			if(apwItem==null || apwItem.phoneRec==null) continue;
    		
    			system.debug('111_1');
    			if(apwItem.phoneRec.Id!=null){
    				system.debug('222_1');
        			deleteList.add(apwItem.phoneRec);
    			}
    		}
    		
    		if(deleteList!=null && !deleteList.isEmpty())
    			delete deleteList;
    	}
    		
    }
}