public without sharing class VT_D1_ChangeTaskOwnerController {
    @AuraEnabled
    public static String changeOwnerMethod(Id taskId) {
        String result = Label.TakeTaskSuccess;
        if(taskId != null) {
        if (UserInfo.getProfileId() == VT_D1_HelperClass.getProfileIdByName(VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME)) {

            VTD1_SC_Task__c task = [SELECT OwnerId FROM VTD1_SC_Task__c WHERE Id = :taskId];

            if (task.OwnerId.getSobjectType().getDescribe().getName() == 'User') {
                result = 'The task is already taken.';
            } else{
                result = 'The task is successfully assigned to you';
                task.OwnerId = UserInfo.getUserId();
                update task;
        }
        } else {
            result = 'Sorry, only SC can take this task!';
        }
        }
        return result;
    }

    @AuraEnabled
    public static String changePoolTaskOwner(Id taskId) {
        String result = Label.TakeTaskSuccess; 
        if(taskId != null) {
        if ((UserInfo.getProfileId() == VT_D1_HelperClass.getProfileIdByName(VT_R4_ConstantsHelper_ProfilesSTM.MRR_PROFILE_NAME))||
           (UserInfo.getProfileId() == VT_D1_HelperClass.getProfileIdByName(VT_R4_ConstantsHelper_ProfilesSTM.PMA_PROFILE_NAME))) {

            VTR5_Pool_Task__c task = [SELECT OwnerId FROM VTR5_Pool_Task__c WHERE Id = :taskId];

            if (task.OwnerId.getSobjectType().getDescribe().getName() == 'User') {
                result = 'The task is already taken.';
            } else{
                result = 'The task is successfully assigned to you';
                task.OwnerId = UserInfo.getUserId();
                update task;
        }
        } else {
            result = 'Sorry, only MMR or PMA can take this task!';
        }
        }
        return result;
    }

    @AuraEnabled
    public static String changeTMAOwner(Id tma_taskId) {
        String result = Label.TakeTaskSuccess;
        if (tma_taskId == null) return null;
        if (UserInfo.getProfileId() ==
                        VT_D1_HelperClass.getProfileIdByName(VT_R4_ConstantsHelper_ProfilesSTM.TMA_PROFILE_NAME)) {
            VTD2_TMA_Task__c taskWithDocs = [SELECT VTD2_Document__r.Id, OwnerId,
                                                    VTD2_Document__r.VTD2_TMA_Eligibility_Decision__c,
                                                    VTD2_Document__r.VTD2_TMA__c,
                                                    VTD2_Document__r.RecordTypeId,
                                                    VTD2_PCF__r.Id,
                                                    VTD2_PCF__r.RecordTypeId,
                                                    VTD2_PCF__r.OwnerId
                                                    FROM VTD2_TMA_Task__c
                                                    WHERE
                                                    Id = :tma_taskId];

            if (taskWithDocs.OwnerId.getSobjectType().getDescribe().getName() == 'User') {
                result = 'The task is already taken.';
            }  else {
                if (taskWithDocs.VTD2_PCF__r.RecordTypeId == VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_PCF) {
                    Case cToUpd = taskWithDocs.VTD2_PCF__r;
                    cToUpd.OwnerId = UserInfo.getUserId();
                    update cToUpd;
                }
                VTD2_TMA_Task__c t = new VTD2_TMA_Task__c(Id = tma_taskId, OwnerId = UserInfo.getUserId());
                update t;
            }
        } else {
            result = 'Sorry, only TMA can take this task!';
        }
        return result;
    }
}