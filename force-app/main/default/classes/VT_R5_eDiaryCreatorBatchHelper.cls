/**
 * Created by Aliaksei Yamayeu on 04.11.2020.
 */
// SH-19506
public without sharing class VT_R5_eDiaryCreatorBatchHelper extends VT_R4_AbstractEproCreator {
    private List<Case> cases = new List<Case>();
    private Map<String, List<VTD1_Protocol_ePRO__c>> eDiaryStatusToProtocolEDiaries;

    public List<VTD1_Survey__c> createEpros(List<Case> cases, Map<String, List<VTD1_Protocol_ePRO__c>> eDiaryStatusToProtocolEDiaries) {
        this.cases = cases;
        this.eDiaryStatusToProtocolEDiaries = eDiaryStatusToProtocolEDiaries;

        if (!this.cases.isEmpty()) {
            getCaregiverMap();
            initialiseProtocolAmendmentVersioning();
            generateEpros();
        }
        return ePros;
    }

    private void getCaregiverMap() {
        List<Id> patientAccIds = new List<Id>();
        for (Case item : cases) {
            if (item.VTD1_Patient_User__r.Contact.AccountId != null) {
                patientAccIds.add(item.VTD1_Patient_User__r.Contact.AccountId);
            }
        }
        if (!patientAccIds.isEmpty()) {
            for (User item : [
                    SELECT Id, Contact.AccountId
                    FROM User
                    WHERE Contact.AccountId IN :patientAccIds
                    AND Contact.RecordType.DeveloperName = 'Caregiver'
                    ORDER BY Contact.VTD1_Primary_CG__c ASC
            ]) {
                caregiverMap.put(item.Contact.AccountId, item);
            }
        }
    }

    private void initialiseProtocolAmendmentVersioning() {
        List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrappedRecords = new List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper>();
        wrappedRecords.addAll(wrapListOfLists(eDiaryStatusToProtocolEDiaries.values()));
        paVersionHelper = new VT_R4_ProtocolAmendmentVersionHelper(wrappedRecords);
    }

    private void generateEpros() {
        for (Case cas : cases) {
            if (!eDiaryStatusToProtocolEDiaries.containsKey(cas.Status)) {
                continue;
            }
            List<VTD1_Protocol_ePRO__c> unVersionedTemplates = eDiaryStatusToProtocolEDiaries.get(cas.Status);
            List<VTD1_Protocol_ePRO__c> correctVersionProtocols = paVersionHelper.getCorrectVersions(cas.VTD1_Virtual_Site__c, unVersionedTemplates);
            for (VTD1_Protocol_ePRO__c template : correctVersionProtocols) {
                if (isCaregiverSubjectWithNoCaregiver(cas, template)) {
                    continue;
                }

                if (template.RecordType.DeveloperName != 'VTR5_External') {
                    String patientTimeZone = cas.VTD1_Patient_User__r.VTD2_UserTimezone__c;
                    ePros.add(getNewEpro(cas, template, getUsersEventDateTime(template, patientTimeZone))); //create SH Diaries
                }
            }
        }
    }

    private VTD1_Survey__c getNewEpro(Case cas, VTD1_Protocol_ePRO__c template, Datetime availableDate) {
        if (template.VTR2_eDiary_delay__c != null && template.VTR2_eDiary_delay__c > 0) {
            availableDate = availableDate.addDays(template.VTR2_eDiary_delay__c.intValue());
        }
        VTD1_Survey__c newSurvey = new VTD1_Survey__c(
                Name = template.Name,
                VTD1_CSM__c = cas.Id,
                VTD1_Date_Available__c = availableDate,
                VTD1_Protocol_ePRO__c = template.Id,
                VTD1_Patient_User_Id__c = cas.VTD1_Patient_User__c,
                VTD1_Caregiver_User_Id__c = getCaregiverId(cas, template),
                RecordTypeId = surveyRecTypeMap.get(template.RecordType.DeveloperName).getRecordTypeId(),
                VTD1_Due_Date__c = template.VTD1_Response_Window__c != null ?
                        availableDate.addMinutes(Math.round(template.VTD1_Response_Window__c * 60)) : availableDate,
                VTR2_Reviewer_User__c = getReviewerForEDiary(cas.VTD1_Primary_PG__c, cas.VTR2_SiteCoordinator__c, template.VTR2_Protocol_Reviewer__c)
        );
        newSurvey.VTD1_Reminder_Due_Date__c = getReminderDate(newSurvey.VTD1_Due_Date__c, template.VTD1_Reminder_Window__c);
        return newSurvey;
    }

    private Boolean isCaregiverSubjectWithNoCaregiver(Case cas, VTD1_Protocol_ePRO__c template) {
        return
                isSubjectCaregiver(template) &&
                        !caregiverMap.containsKey(cas.VTD1_Patient_User__r.Contact.AccountId);
    }
    
    private Datetime getUsersEventDateTime(VTD1_Protocol_ePRO__c template, String patientTimeZone) {
        Datetime eventDateTime = System.now();
        TimeZone tz = TimeZone.getTimeZone(patientTimeZone);
        Integer timeToAdd = template.VTR4_EventOffset__c != null ? template.VTR4_EventOffset__c.intValue() : 0;
        switch on template.VTR4_EventOffsetPeriod__c {
            when 'Hours' {
                eventDateTime = eventDateTime.addHours(timeToAdd);
            }
            when 'Days' {
                eventDateTime = eventDateTime.addDays(timeToAdd);
            }
            when 'Weeks' {
                eventDateTime = eventDateTime.addDays(timeToAdd * 7);
            }
            when 'Months' {
                eventDateTime = eventDateTime.addMonths(timeToAdd);
            }
        }
        //if the event time is set, then the patient's eDiary should be available at this time, taking into account time zones.
        if (template.VTR4_EventTime__c != null) {
            eventDateTime = Datetime.newInstanceGmt(Date.today(), template.VTR4_EventTime__c);
            Integer usersOffset = tz.getOffset(eventDateTime) / 1000;
            eventDateTime = Datetime.newInstanceGMT(Date.today(), template.VTR4_EventTime__c.addSeconds(-usersOffset));
        }
        return eventDateTime;
    }
}