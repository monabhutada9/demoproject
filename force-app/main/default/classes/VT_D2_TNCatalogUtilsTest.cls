/**
 * Created by MPlatonov on 25.01.2019.
 * Modified by Harshita Khandelwal on 19.10.2020 for SH-17444 (Multiple CRA)
 */

@isTest
public with sharing class VT_D2_TNCatalogUtilsTest {
    @isTest
    public static void test1() {
        List <String> queryParts = new List<String>();
        Set <String> fields = new Set<String>();
        String text = '"Approve or reject medical records for" & VTD1_Document__c.VTD1_Clinical_Study_Membership__r.VTD1_Patient__r.VTD1_First_Name__c & " " & VTD1_Document__c.VTD1_Clinical_Study_Membership__r.VTD1_Patient__r.VTD1_Last_Name__c';
        List <VT_D2_TNCatalogUtils.ExpressionComponent> components = VT_D2_TNCatalogUtils.parseExpression(text, 'VTD1_Document__c', fields, queryParts,false);
        System.debug('components = ' + components);
    }
    @isTest
    public static void test2() {
        List <String> queryParts = new List<String>();
        Set <String> fields = new Set<String>();
        String text = 'LEFT(VTD1_Actual_Visit__c.VTD1_Pre_Visit_Instructions__c, 255)';
        List <VT_D2_TNCatalogUtils.ExpressionComponent> components = VT_D2_TNCatalogUtils.parseExpression(text, 'VTD1_Actual_Visit__c', fields, queryParts,false);
        System.debug('components = ' + components);
    }
    @isTest
    public static void test3() {
        List <String> queryParts = new List<String>();
        Set <String> fields = new Set<String>();
        String text = 'VTD1_Order__c.VTD1_Case__c.VTD1_Patient_User__c.LastName & ";" & VTD1_Order__c.VTD1_Case__c.VTD1_Patient_User__c.FirstName & ";" & IF(VTD1_Order__c.VTD1_Protocol_Delivery__c <> null,VTD1_Order__c.VTD1_Protocol_Delivery__c & "Name", VTD1_Order__c.Name)';
        List <VT_D2_TNCatalogUtils.ExpressionComponent> components = VT_D2_TNCatalogUtils.parseExpression(text, 'VTD1_Order__c', fields, queryParts,false);
        System.debug('components = ' + components);
    }
    @testSetup
    public static void setup() {
        VT_D1_TestUtils.prepareStudy(1);
        List <VTD2_TN_Catalog_Code__c> tnCatalogCodes = new List<VTD2_TN_Catalog_Code__c>();
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = '001',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Task').getRecordTypeId(),
                VTD2_T_Assigned_To_ID__c = 'VTD1_Document__c.VTD1_PG_Approver__c',
                VTD2_T_Related_To_Id__c = 'VTD1_Document__c.Id',
                VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD_RTId__c.VTD2_Task_TaskForDocumentReview__c',
                VTD2_T_Description__c = '"Approve or reject medical records for" & VTD1_Document__c.VTD1_Clinical_Study_Membership__r.VTD1_Patient__r.VTD1_First_Name__c & " " & VTD1_Document__c.VTD1_Clinical_Study_Membership__r.VTD1_Patient__r.VTD1_Last_Name__c',
                VTD2_T_Subject__c = 'VTD2_Review_And_Approve_Medical_Records & FORMAT(VTD1_Document__c.CreatedDate, \'EEE, MMM d yyyy\') ',
                VTD2_T_Care_Plan_Template__c = 'VTD1_Document__c.VTD1_Clinical_Study_Membership__r.VTD1_Study__c',
                VTD2_T_Due_Date__c = 'today()',
                VTD2_T_Reminder_Date_Time__c = 'NOW()',
                VTD2_T_Category__c = 'Enrollment Related',
                VTD2_T_Priority__c = 'Normal',
                VTD2_T_Status__c = 'Open',
                VTD2_T_Reminder_Set__c = true,
                VTD2_T_Document__c = 'VTD1_Document__c.Id',
                VTD2_T_Patient__c = 'VTD1_Document__c.VTD1_Clinical_Study_Membership__c',
                VTD2_T_Pre_Visit_Instructions__c = null,
                VTD2_T_IsRequestVisitRedirect__c = false,
                VTD2_T_My_Task_List_Redirect__c = null,
                VTD2_T_Actual_Visit__c = null,
                VTD2_T_Description_Parameters__c = 'test',
                VTD2_T_Type_for_Backend_Logic__c = 'Confirm Drop Off of Study Medication',
                VTD2_T_Subject_Parameter_Field__c = 'test'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTR2_Pass_if_empty_recipient__c = true,
                VTD2_T_Task_Unique_Code__c = '002',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Task').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Opportunity',
                VTD2_T_Assigned_To_ID__c = 'Opportunity.Account.Owner.Id',
                VTD2_T_Related_To_Id__c = 'Opportunity.Id',
                VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD_RTId__c.VTD1_Task_SimpleTask__c',
                VTD2_T_Description__c = 'Opportunity.Account.Name & " and " & LEFT(Opportunity.Account.Name, 255)',
                VTD2_T_Subject__c = 'IF(ISBLANK(Opportunity.CampaignId),Opportunity.Account.Name,Opportunity.Account.Name))',
                VTD2_T_Care_Plan_Template__c = null,
                VTD2_T_Due_Date__c = 'today()',
                VTD2_T_Reminder_Date_Time__c = 'NOW()',
                VTD2_T_Category__c = 'Enrollment Related',
                VTD2_T_Priority__c = 'Normal',
                VTD2_T_Status__c = 'Open',
                VTD2_T_Reminder_Set__c = true,
                VTD2_T_Document__c = null,
                VTD2_T_Patient__c = null,
                VTD2_T_Pre_Visit_Instructions__c = null,
                VTD2_T_IsRequestVisitRedirect__c = false,
                VTD2_T_My_Task_List_Redirect__c = null,
                VTD2_T_Actual_Visit__c = null,
                VTD2_T_Description_Parameters__c = 'FORMAT(Opportunity.CreatedDate, \'EEE, MMM d yyyy\')',
                VTD2_T_Type_for_Backend_Logic__c = 'Confirm Drop Off of Study Medication',
                VTD2_T_Subject_Parameter_Field__c = 'test'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'SC2',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('SCTask').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Opportunity',
                VTD2_T_Assigned_To_ID__c = 'Opportunity.Account.Owner.Id',
                VTD2_T_Related_To_Id__c = 'Opportunity.Id',
                VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD_RTId__c.VTD1_Task_SimpleTask__c',
                VTD2_T_Description__c = 'Opportunity.Account.Name & " and " & LEFT(Opportunity.Account.Name, 255)',
                VTD2_T_Subject__c = 'IF(ISBLANK(Opportunity.CampaignId),Opportunity.Account.Name,Opportunity.Account.Name))',
                VTD2_T_Care_Plan_Template__c = null,
                VTD2_T_Due_Date__c = 'today()',
                VTD2_T_Reminder_Date_Time__c = 'NOW()',
                VTD2_T_Category__c = 'Other Tasks',
                VTD2_T_Priority__c = 'Normal',
                VTD2_T_Status__c = 'Open',
                VTD2_T_Reminder_Set__c = true,
                VTD2_T_Document__c = null,
                VTD2_T_Patient__c = null,
                VTD2_T_Pre_Visit_Instructions__c = null,
                VTD2_T_IsRequestVisitRedirect__c = false,
                VTD2_T_My_Task_List_Redirect__c = null,
                VTD2_T_Actual_Visit__c = null,
                VTD2_T_Description_Parameters__c = 'FORMAT(Opportunity.CreatedDate, \'EEE, MMM d yyyy\')',
                VTD2_T_Type_for_Backend_Logic__c = 'Confirm Drop Off of Study Medication',
                VTD2_T_Subject_Parameter_Field__c = 'test'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'TM2',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('TMATask').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Opportunity',
                VTD2_T_Assigned_To_ID__c = 'Opportunity.Account.Owner.Id',
                VTD2_T_Related_To_Id__c = 'Opportunity.Id',
                //VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD_RTId__c.VTD1_Task_SimpleTask__c',
                VTD2_T_Description__c = 'Opportunity.Account.Name & " and " & LEFT(Opportunity.Account.Name, 255)',
                VTD2_T_Subject__c = 'IF(ISBLANK(Opportunity.CampaignId),Opportunity.Account.Name,Opportunity.Account.Name))',
                VTD2_T_Care_Plan_Template__c = null,
                VTD2_T_Due_Date__c = 'today()',
                VTD2_T_Reminder_Date_Time__c = 'NOW()',
                VTD2_T_Category__c = 'Protocol Deviation',
                VTD2_T_Priority__c = 'Normal',
                VTD2_T_Status__c = 'Open',
                VTD2_T_Reminder_Set__c = true,
                VTD2_T_Document__c = null,
                VTD2_T_Patient__c = null,
                VTD2_T_Pre_Visit_Instructions__c = null,
                VTD2_T_IsRequestVisitRedirect__c = false,
                VTD2_T_My_Task_List_Redirect__c = null,
                VTD2_T_Actual_Visit__c = null,
                VTD2_T_Description_Parameters__c = 'FORMAT(Opportunity.CreatedDate, \'EEE, MMM d yyyy\')',
                VTD2_T_Type_for_Backend_Logic__c = 'Confirm Drop Off of Study Medication',
                VTD2_T_Subject_Parameter_Field__c = 'test'
        ));
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = '004',
                VTD2_T_SObject_Name__c = 'Opportunity',
                VTD2_T_Assigned_To_ID__c = 'Opportunity.Account.Owner.Id',
                VTD2_T_Related_To_Id__c = 'Opportunity.Id',
                VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD_RTId__c.VTD1_Task_SimpleTask__c',
                VTD2_T_Description__c = 'Opportunity.Account.Name & " and " & LEFT(Opportunity.Account.Name, 1)',
                VTD2_T_Subject__c = 'IF(ISBLANK(Opportunity.Id),Opportunity.Account.Name,Opportunity.Account.Name))',
                VTD2_T_Care_Plan_Template__c = null,
                VTD2_T_Due_Date__c = null,
                VTD2_T_Reminder_Date_Time__c = null,
                VTD2_T_Category__c = 'Enrollment Related',
                VTD2_T_Priority__c = 'Normal',
                VTD2_T_Status__c = 'Open',
                VTD2_T_Reminder_Set__c = true,
                VTD2_T_Document__c = null,
                VTD2_T_Patient__c = null,
                VTD2_T_Pre_Visit_Instructions__c = null,
                VTD2_T_IsRequestVisitRedirect__c = false,
                VTD2_T_My_Task_List_Redirect__c = null,
                VTD2_T_Actual_Visit__c = null,
                VTD2_T_Description_Parameters__c = 'test',
                VTD2_T_Type_for_Backend_Logic__c = 'Confirm Drop Off of Study Medication',
                VTD2_T_Subject_Parameter_Field__c = 'test'
        ));
        Map <String, VTD2_TN_Catalog_Code__c> notificationCatalogMap = new Map<String, VTD2_TN_Catalog_Code__c>();
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'N003',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('Notification').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Opportunity',
                VTD2_T_Assigned_To_ID__c = 'Opportunity.Account.Owner.Id',
                VTD2_T_Related_To_Id__c = 'Opportunity.Id',
                VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD_RTId__c.VTD1_Task_SimpleTask__c',
                VTD2_T_Description__c = 'Opportunity.Account.Name & " and " & LEFT(Opportunity.Account.Name, 1)',
                VTD2_T_Subject__c = 'IF(Opportunity.Id != null AND NOT(ISBLANK(Opportunity.Id)),Opportunity.Account.Name,Opportunity.Account.Name))',
                VTD2_T_Care_Plan_Template__c = null,
                VTD2_T_Due_Date__c = null,
                VTD2_T_Reminder_Date_Time__c = null,
                VTD2_T_Category__c = 'Enrollment Related',
                VTD2_T_Priority__c = 'Normal',
                VTD2_T_Receiver__c ='Opportunity.OwnerId',
                VTD2_T_Status__c = 'Open',
                VTD2_T_Reminder_Set__c = true,
                VTD2_T_Document__c = null,
                VTD2_T_Message_Parameters__c = null, //'IF(Opportunity.Id != null AND NOT(ISBLANK(Opportunity.Id)),Opportunity.Account.Name,Opportunity.Account.Name))',
                VTD2_T_Patient__c = null,
                VTD2_T_Pre_Visit_Instructions__c = null,
                VTD2_T_IsRequestVisitRedirect__c = false,
                VTD2_T_My_Task_List_Redirect__c = null,
                VTD2_T_Actual_Visit__c = null,
                VTD2_T_Description_Parameters__c = 'test',
                VTD2_T_Type_for_Backend_Logic__c = 'Confirm Drop Off of Study Medication',
                VTD2_T_Subject_Parameter_Field__c = 'test'
        ));
        notificationCatalogMap.put('N003', tnCatalogCodes[tnCatalogCodes.size() - 1]);
        // SH-17444_Start
        tnCatalogCodes.add(new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = '100',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('CRATask').getRecordTypeId(),
                VTD2_T_Category__c = 'Visit Related',
                VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD_RTId__c.VTR5_CRA_Task_SimpleTask__c',
                VTD2_T_Due_Date__c = 'today()',
                VTD2_T_SObject_Name__c = 'VTD1_Monitoring_Visit__c',
                VTD2_T_Assigned_To_ID__c = 'VTD1_Monitoring_Visit__c.VTR2_Visit_CRA__r.User__c',
                VTD2_T_Related_To_Id__c = 'VTD1_Monitoring_Visit__c.Id',
                VTD2_T_Subject__c = 'CRA Task Test',
                VTD2_T_Reminder_Date_Time__c = 'NOW()',
                VTD2_T_Priority__c = 'Normal',
                VTD2_T_Status__c = 'Open',
                VTD2_T_Reminder_Set__c = true,
                VTD2_T_Document__c = null,
                VTD2_T_Patient__c = null,
                VTD2_T_Pre_Visit_Instructions__c = null,
                VTD2_T_IsRequestVisitRedirect__c = false,
                VTD2_T_My_Task_List_Redirect__c = null,
                VTD2_T_Actual_Visit__c = null
        ));
        notificationCatalogMap.put('100', tnCatalogCodes[tnCatalogCodes.size() - 1]);
        // SH-17444_End
        
        insert tnCatalogCodes;
        tnCatalogCodes[1].VTR3_T_TN_Notification__c = notificationCatalogMap.get('N003').Id;
        update tnCatalogCodes[1];
        User PIUser = [select Id from User where FirstName = 'PIUser1'];
        Account account = new Account(Name = 'Test', OwnerId = PIUser.Id);
        insert account;
        Opportunity opportunity = new Opportunity(Name = 'Test', StageName = 'Test', CloseDate = System.today(), AccountId = account.Id);
        insert opportunity;
    }
    @isTest
    public static void testTasks() {
        Test.startTest();
        Opportunity opportunity = [select Id from Opportunity];
        Map <String, String> params = new Map<String, String>();
        params.put('VTD2_T_Subject_Parameter_Field__c', 'erwer');
        params.put('VTD2_T_Subject_Parameter_Field_2__c', 'hthrth');
        Account account = [select Id, OwnerId, Owner.Profile.Name from Account where Name = 'Test' limit 1];
        System.debug('account owner id = ' + account.Owner.Profile.Name);
        VT_D2_TNCatalogTasks.ParamsHolder paramsHolder = new VT_D2_TNCatalogTasks.ParamsHolder();
        paramsHolder.tnCode = '002';
        paramsHolder.sourceId = opportunity.Id;
        paramsHolder.subjectParamId_1 = 'test';
        //paramsHolder.assignedToID = account.OwnerId;
        List <VT_D2_TNCatalogTasks.ParamsHolder> args = new List<VT_D2_TNCatalogTasks.ParamsHolder>{paramsHolder};
        VT_D2_TNCatalogTasks.generateTask(args);
        VTD2_TN_Catalog_Code__c tnCatalogCode = [select Id, VTD2_T_Assigned_To_ID__c from VTD2_TN_Catalog_Code__c where VTD2_T_Task_Unique_Code__c = '002'];
        tnCatalogCode.VTD2_T_Assigned_To_ID__c = null;
        update tnCatalogCode;
        paramsHolder.assignedToID = account.OwnerId;
        VT_D2_TNCatalogTasks.generateTask(args);
        VT_R5_TNCatalogTasksVoid.generateTask(args);
        List <VTD1_NotificationC__c> notifications = [select Id from VTD1_NotificationC__c];
        System.debug('notifications number = ' + notifications.size());
        System.assert(notifications.size() == 3);
        Test.stopTest();

        /*paramsHolder = new VT_D2_TNCatalogTasks.ParamsHolder();
        paramsHolder.tnCode = 'SC2';
        paramsHolder.sourceId = opportunity.Id;
        paramsHolder.subjectParamId_1 = 'test';
        args = new List<VT_D2_TNCatalogTasks.ParamsHolder>{paramsHolder};

        VT_D2_TNCatalogTasks.generateTask(args);

        paramsHolder = new VT_D2_TNCatalogTasks.ParamsHolder();
        paramsHolder.tnCode = 'TM2';
        paramsHolder.sourceId = opportunity.Id;
        paramsHolder.subjectParamId_1 = 'test';
        args = new List<VT_D2_TNCatalogTasks.ParamsHolder>{paramsHolder};

        VT_D2_TNCatalogTasks.generateTask(args);

        paramsHolder = new VT_D2_TNCatalogTasks.ParamsHolder();
        paramsHolder.tnCode = '003';
        paramsHolder.sourceId = opportunity.Id;
        paramsHolder.subjectParamId_1 = 'test';

        args = new List<VT_D2_TNCatalogTasks.ParamsHolder>{paramsHolder};

        VT_D2_TNCatalogTasks.generateTask(args);

        paramsHolder = new VT_D2_TNCatalogTasks.ParamsHolder();
        paramsHolder.tnCode = '004';
        paramsHolder.sourceId = opportunity.Id;
        paramsHolder.subjectParamId_1 = 'test';

        args = new List<VT_D2_TNCatalogTasks.ParamsHolder>{paramsHolder};*/

        /*VT_D2_TNCatalogTasks.generateTask(args);

        VT_D2_TNCatalogNotifications.ParamsHolder paramsHolder1 = new VT_D2_TNCatalogNotifications.ParamsHolder();
        paramsHolder1.tnCode = 'N003';
        paramsHolder1.sourceId = opportunity.Id;
        paramsHolder1.subjectParamId_1 = 'test';

        List <VT_D2_TNCatalogNotifications.ParamsHolder> args1 = new List<VT_D2_TNCatalogNotifications.ParamsHolder>{paramsHolder1};

        VT_D2_TNCatalogNotifications.generateNotification(args1);
        VT_D2_TNCatalogNotifications.bulkMode = true;
        VT_D2_TNCatalogNotifications.generateNotification(args1);
        List <VTD1_NotificationC__c> notifications = VT_D2_TNCatalogNotifications.flushNotifications();
        System.debug('notifications = ' + notifications);*/

        //List <VTD1_NotificationC__c> notifications = VT_D2_TNCatalogNotifications.generateNotifications(new List<String>{'N003'}, new List<Id>{opportunity.Id}, null, null, null, new Map <Id, String>{opportunity.Id => 'dfsfsdfs;fdsfsdfds'});

        //System.debug('notifications = ' + notifications);
    }
    @isTest
    public static void testSCTasks() {
        Test.startTest();
        Opportunity opportunity = [select Id from Opportunity];
        Map <String, String> params = new Map<String, String>();
        params.put('VTD2_T_Subject_Parameter_Field__c', 'erwer');
        params.put('VTD2_T_Subject_Parameter_Field_2__c', 'hthrth');
        Account account = [select Id, OwnerId, Owner.Profile.Name from Account where Name = 'Test' limit 1];
        System.debug('account owner id = ' + account.Owner.Profile.Name);
        VT_D2_TNCatalogTasks.ParamsHolder paramsHolder = new VT_D2_TNCatalogTasks.ParamsHolder();
        paramsHolder.tnCode = 'SC2';
        paramsHolder.sourceId = opportunity.Id;
        paramsHolder.subjectParamId_1 = 'test';
        //List <VT_D2_TNCatalogTasks.ParamsHolder> args = new List<VT_D2_TNCatalogTasks.ParamsHolder>{paramsHolder};
        List <VTD1_SC_Task__c> tasks = VT_D2_TNCatalogTasks.generateSCTasks(new List<String>{'SC2'}, new List<Id>{opportunity.Id}, null, null, false);
        Test.stopTest();
    }
    @isTest
    public static void testTMATasks() {
        Test.startTest();
        Opportunity opportunity = [select Id from Opportunity];
        Map <String, String> params = new Map<String, String>();
        params.put('VTD2_T_Subject_Parameter_Field__c', 'erwer');
        params.put('VTD2_T_Subject_Parameter_Field_2__c', 'hthrth');
        Account account = [select Id, OwnerId, Owner.Profile.Name from Account where Name = 'Test' limit 1];
        System.debug('account owner id = ' + account.Owner.Profile.Name);
        VT_D2_TNCatalogTasks.ParamsHolder paramsHolder = new VT_D2_TNCatalogTasks.ParamsHolder();
        paramsHolder.tnCode = 'TM2';
        paramsHolder.sourceId = opportunity.Id;
        paramsHolder.subjectParamId_1 = 'test';
        List <VT_D2_TNCatalogTasks.ParamsHolder> args = new List<VT_D2_TNCatalogTasks.ParamsHolder>{paramsHolder};
        VT_D2_TNCatalogTasks.generateTask(args);
        Test.stopTest();
    }

    @isTest
    public static void testNotifications() {
        Test.startTest();
        Opportunity opportunity = [select Id from Opportunity];
        Map <String, String> params = new Map<String, String>();
        params.put('VTD2_T_Subject_Parameter_Field__c', 'erwer');
        params.put('VTD2_T_Subject_Parameter_Field_2__c', 'hthrth');
        Account account = [select Id, OwnerId, Owner.Profile.Name from Account where Name = 'Test' limit 1];
        System.debug('account owner id = ' + account.Owner.Profile.Name);
        VT_D2_TNCatalogNotifications.ParamsHolder paramsHolder = new VT_D2_TNCatalogNotifications.ParamsHolder();
        paramsHolder.tnCode = 'N003';
        paramsHolder.sourceId = opportunity.Id;
        paramsHolder.subjectParamId_1 = 'test';
        VT_D2_TNCatalogNotifications.bulkMode = true;
        List <VT_D2_TNCatalogNotifications.ParamsHolder> args = new List<VT_D2_TNCatalogNotifications.ParamsHolder>{paramsHolder};
        VT_D2_TNCatalogNotifications.generateNotification(args);
        List <VTD1_NotificationC__c> notifications = VT_D2_TNCatalogNotifications.flushNotifications();
        VT_D2_TNCatalogNotifications.bulkMode = false;
        VT_D2_TNCatalogNotifications.futureMode = true;
        VT_D2_TNCatalogNotifications.generateNotification(args);
        VT_D2_TNCatalogNotifications.futureMode = false;
        VTD2_TN_Catalog_Code__c tnCatalogCode = [select Id, RecordTypeId from VTD2_TN_Catalog_Code__c where VTD2_T_Task_Unique_Code__c = 'N003'];
        tnCatalogCode.RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('PostToChatter').getRecordTypeId();
        update tnCatalogCode;
        VT_D2_TNCatalogNotifications.generateNotification(args);
        tnCatalogCode = [select Id, VTR2_Pass_if_empty_recipient__c from VTD2_TN_Catalog_Code__c where VTD2_T_Task_Unique_Code__c = 'N003'];
        tnCatalogCode.VTR2_Pass_if_empty_recipient__c = true;
        update tnCatalogCode;
        VT_D2_TNCatalogNotifications.generateNotification(args);
        User PIUser = [select Id from User where FirstName = 'PIUser1'];
        paramsHolder.Receiver = PIUser.Id;
        VT_D2_TNCatalogNotifications.generateNotification(args);
        Test.stopTest();
    }

    @IsTest
    private static void testCustomNotifications() {
        insert new VTD2_TN_Catalog_Code__c(
                VTD2_T_Task_Unique_Code__c = 'N005',
                RecordTypeId = Schema.SObjectType.VTD2_TN_Catalog_Code__c.getRecordTypeInfosByName().get('CustomNotification').getRecordTypeId(),
                VTD2_T_SObject_Name__c = 'Opportunity',
                VTD2_T_Receiver__c ='Opportunity.OwnerId',
                VTD2_T_Title__c = 'Title',
                VTD2_T_Message__c = 'Message',
                VTD2_T_Link_to_related_event_or_object__c = 'Opportunity.Id',
                VTD2_T_Has_direct_link__c = true
        );
        VT_D2_TNCatalogNotifications.ParamsHolder paramsHolder = new VT_D2_TNCatalogNotifications.ParamsHolder();
        paramsHolder.tnCode = 'N005';
        paramsHolder.sourceId = [SELECT Id FROM Opportunity].Id;
        Test.startTest();
        VT_D2_TNCatalogNotifications.generateNotification(new List<VT_D2_TNCatalogNotifications.ParamsHolder>{paramsHolder});
        Test.stopTest();
    }
    @isTest
    public static void testTasksvoid() {
        Test.startTest();
        List<Opportunity> opportunity = [select Id from Opportunity];
        List<Account> accounts = [select Id, OwnerId, Owner.Profile.Name from Account where Name = 'Test'];
        VT_R4_TNCatalogTasksVoid.ParamsHolder paramsHolder = new VT_R4_TNCatalogTasksVoid.ParamsHolder();
        paramsHolder.tnCode = '002';
        System.debug(opportunity);
        List<String> accs = new List<String>();
        for(Account acc : accounts){
            accs.add((String)acc.id);
        }
        String opp = (String)opportunity[0].id;
        paramsHolder.sourceId= opp;
        List <VT_R4_TNCatalogTasksVoid.ParamsHolder> args = new List<VT_R4_TNCatalogTasksVoid.ParamsHolder>{
                paramsHolder
        };
       /* VT_D2_TNCatalogTasksVoid.generateTasksOrNotifications(args);*/

        paramsHolder.assignedToIDs = accs;
        paramsHolder.isNotification = true;
        List <VTD1_NotificationC__c> notifications = [select Id from VTD1_NotificationC__c];
        System.debug('notifications number = ' + notifications.size());
        VT_R4_TNCatalogTasksVoid.generateTasksOrNotifications(args);
        VTD2_TN_Catalog_Code__c tnCatalogCode = [select Id, VTD2_T_Assigned_To_ID__c from VTD2_TN_Catalog_Code__c where VTD2_T_Task_Unique_Code__c = '002'];
        tnCatalogCode.VTD2_T_Assigned_To_ID__c = null;
        update tnCatalogCode;
        paramsHolder.isNotification = false;
        VT_R4_TNCatalogTasksVoid.generateTasksOrNotifications(args);

        /*System.assert(notifications.size() == 2);*/
        Test.stopTest();
    }
    
    // Method to test CRA Task creation from VT_D2_TNCatalogTasks Class
    // Harshita Khandelwal 19.10.2020
    // Jira Ref: SH-17444
    @isTest
    public static void testCRATasks() {
        
            // Query test study record
            HealthCloudGA__CarePlanTemplate__c testStudy = [SELECT Id from HealthCloudGA__CarePlanTemplate__c LIMIT 1]; 
        
            // Query Virtual site record
            Virtual_Site__c testVirtualSite = [ SELECT Id, VTD1_Study__c, VTD1_PI_User__c 
                                                FROM Virtual_Site__c 
                                                LIMIT 1];
            
            // Query Study Team Member
            Study_Team_Member__c craMember = [  SELECT Id, User__c
                                                FROM Study_Team_Member__c 
                                                WHERE User__r.Profile.Name =: VT_R4_ConstantsHelper_Profiles.CRA_PROFILE_NAME AND User__r.IsActive = true
                                                LIMIT 1];
        
            // Query CRA User record
            User CRAUser = [SELECT Id, IsActive FROM User WHERE Id =: craMember.User__c LIMIT 1];

        Test.startTest();
            // Create SSTM Record
            Study_Site_Team_Member__c siteTeam = VT_D1_TestUtils.createStudySiteTeamMember(testVirtualSite.Id, craMember.Id);
            insert siteTeam;

            //create sharing record to share the monitor visit
            HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
            objVirtualShare.ParentId = testStudy.Id;
            objVirtualShare.UserOrGroupId = craMember.User__c;
            objVirtualShare.AccessLevel = 'Edit';
            insert objVirtualShare;
        
        	// Create Monitoring visit record Positive Use Case in bulk
            List<VTD1_Monitoring_Visit__c> testMvisitLst  = new List<VTD1_Monitoring_Visit__c>();
        	VTD1_Monitoring_Visit__c testMvisit = new VTD1_Monitoring_Visit__c();
            system.runAs(CRAUser) {
                for(Integer i = 0; i < 10; i++) {
                   	testMvisit = VT_D1_TestUtils.createMonitoringVisit(testVirtualSite.Id, testStudy.Id, 'Interim Visit', 'Planned',
                                                      	System.now().addDays(i), Datetime.now().addDays(i));
                testMvisit.VTD1_Remote_Monitoring_Visit_Report_Stat__c = 'Approved';
                    testMvisitLst.add(testMvisit);
                }
                insert testMvisitLst;
            }
        
            // Verify CRA Task
            List<VTR5_CRA_Task__c> testCRATaskLst = [SELECT Id FROM VTR5_CRA_Task__c WHERE VTR5_Subject__c = 'CRA Task Test'];
            system.assertEquals(testMvisitLst.size(), testCRATaskLst.size());	
        
        	// Create Monitoring visit record Negative Use Case
            VTD1_Monitoring_Visit__c testMvisit1  = new VTD1_Monitoring_Visit__c();
            system.runas(CRAUser) {
                testMvisit1 = VT_D1_TestUtils.createMonitoringVisit(testVirtualSite.Id, testStudy.Id, 'Interim Visit', 'Planned',
                                                      	null, Datetime.now().addDays(11));
                insert testMvisit1;
            }
        
            // Verify CRA Task, No new CRA Task insertionn
            List<VTR5_CRA_Task__c> testCRATaskLst1 = [SELECT Id FROM VTR5_CRA_Task__c WHERE VTR5_Subject__c = 'CRA Task Test'];
            system.assertEquals(11, testCRATaskLst1.size());
        Test.stopTest(); 
    }
    
}