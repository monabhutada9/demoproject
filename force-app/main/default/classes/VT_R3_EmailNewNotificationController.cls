/**
 * Created by user on 06-Nov-19.
 */

public with sharing class VT_R3_EmailNewNotificationController {
  public User user {get; set;}
  public String userLanguage {get; set;}
  public String profileName {get; set;}
  public String firstName {get; set;}
  public String lastName {get; set;}

  public String sincerely {
    get{return VT_D1_TranslateHelper.getLabelValue('VTD2_Sincerely', userLanguage);}
    set;}
  public String closingMail {
    get {return VT_D1_TranslateHelper.getLabelValue('VTD2_StudyHub', userLanguage);}
    set;
  }



  public String getSalutation (){
    String salutationText;
    String participantName;
    salutationText = VT_D1_TranslateHelper.getLabelValue('VT_R2_VisitEmailSalutaion', userLanguage) + ' ';
    if(profileName == 'Primary Investigator'){
      participantName = 'Dr. ' + lastName;
    }
    else if(profileName == 'Caregiver'|| profileName== 'Patient') {
      participantName = firstName;
    }
    else {
      participantName = lastName;
    }

    //return salutationText.replace('#Visit_participant', participantName);
    return salutationText.replace('#Visit_participant', participantName).replace('<Participant First Last Name>', participantName);
  }

  public String getBodyText (){
    return VT_D1_TranslateHelper.getLabelValue('VTD2_YouHaveANewNotification', userLanguage);
  }

  public String getLinkLabel (){
    return VT_D1_TranslateHelper.getLabelValue('VTD2_ViewNotificationInStudyHub', userLanguage);
  }

  public String getLinkToStudyHub() {
    if(profileName == 'Primary Investigator') {
      //return String.valueOf(VTD2_NewMessagesEmailNotification__c.getInstance().Community_home_page_pi__c);
      return VTD1_RTId__c.getInstance().PI_URL__c + '/s';
    }else if(profileName=='Patient Guide'){
      return System.URL.getSalesforceBaseURL().toExternalForm();
    }else if(profileName=='Site Coordinator'){
      return VTD1_RTId__c.getInstance().VT_R2_SCR_URL__c;
    }else{
      //return String.valueOf(VTD2_NewMessagesEmailNotification__c.getInstance().Community_home_page_PT_CG__c);
      return VTD1_RTId__c.getInstance().VTD2_Patient_Community_URL__c;
    }
  }

}