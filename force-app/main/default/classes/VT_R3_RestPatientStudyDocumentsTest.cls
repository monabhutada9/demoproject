/**
 * Created by Alexander Komarov on 10.11.2019.
 */

@IsTest
public class VT_R3_RestPatientStudyDocumentsTest {

    public static void testBehavior() {
        VT_R5_AllTestsMobile.setRestContext('/Patient/StudyDocuments', 'GET');
        RestContext.request.params.put('offset', '0');
        RestContext.request.params.put('limit', '20');
        User patientUser = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        List<VTD1_Document__c> docs = [SELECT Id FROM VTD1_Document__c];
        Id documentId;
        if (docs.size() > 0) {
            documentId = docs.get(0).Id;
        }
        insert new VTD1_Document__Share(ParentId = documentId, UserOrGroupId = patientUser.Id, AccessLevel = 'Edit', RowCause = 'Manual');

        System.runAs(patientUser) {
            Test.startTest();
            VT_R3_RestPatientStudyDocuments.getMethod();
            RestContext.request.requestURI = '/Patient/StudyDocuments/' + documentId;
            HttpCalloutMock cOutMock = new MyCalloutMock();
            Test.setMock(HttpCalloutMock.class, cOutMock);
            VT_R3_RestPatientStudyDocuments.getMethod();
            RestContext.request.requestURI = '/Patient/StudyDocuments/Visits';
            VT_R3_RestPatientStudyDocuments.getMethod();
            Test.stopTest();
        }
    }

    public static void processDocumentTest() {
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        Id docId = [SELECT Id FROM VTD1_Document__c LIMIT 1].Id;
        Test.startTest();
        ContentVersion contentVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();
        ContentVersion insertedContVersion = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        new DomainObjects.ContentDocumentLink_t()
                .withLinkedEntityId(docId)
                .withContentDocumentId(insertedContVersion.ContentDocumentId)
                .persist();
        insert new VTD1_Document__Share(ParentId = docId, UserOrGroupId = toRun.Id, AccessLevel = 'Edit', RowCause = 'Manual');
        System.runAs(toRun) {
            Id visitId = [SELECT Id FROM VTD1_Actual_Visit__c LIMIT 1].Id;
            VT_R5_AllTestsMobile.setRestContext('/Patient/StudyDocuments', 'POST');
            VT_R3_RestPatientStudyDocuments.processDocument(insertedContVersion.Id, 'somecomment', 'somenickname', visitId);
        }
        Test.stopTest();
    }

    public static void patchDocumentTest() {
        User toRun = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.USERNAME LIMIT 1];
        Id docId = [SELECT Id FROM VTD1_Document__c LIMIT 1].Id;
        Test.startTest();
        HttpCalloutMock cMock = new MyCalloutMock();
        Test.setMock(HttpCalloutMock.class, cMock);
        ContentVersion contentVersion = (ContentVersion) new DomainObjects.ContentVersion_t()
                .setPathOnClient('test.pdf')
                .setTitle('Test')
                .setVersionData(Blob.valueOf('TestData'))
                .persist();
        ContentVersion insertedContVersion = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        new DomainObjects.ContentDocumentLink_t()
                .withLinkedEntityId(docId)
                .withContentDocumentId(insertedContVersion.ContentDocumentId)
                .persist();
        insert new VTD1_Document__Share(ParentId = docId, UserOrGroupId = toRun.Id, AccessLevel = 'Edit', RowCause = 'Manual');
        System.runAs(toRun) {
            VT_R5_AllTestsMobile.setRestContext('/Patient/StudyDocuments/'+docId, 'PATCH');
            VT_R3_RestPatientStudyDocuments.updateDocument('newNickName', 'newComment');
        }
    }

    private with sharing class MyCalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setBody('test');
            res.setStatus('OK');
            res.setStatusCode(201);
            return res;
        }
    }
    // hello Copado

}