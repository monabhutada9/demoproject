/**
 * Created by User on 19/05/17.
 */
@isTest
public with sharing class VT_R2_EDiaryStudyHubControllerTest {

    public static void getDataTest(){
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        VTD1_Survey_Answer__c answer = [SELECT Id FROM VTD1_Survey_Answer__c LIMIT 1];
        VTD1_Survey__c survey = [SELECT Id FROM VTD1_Survey__c LIMIT 1];
        List <VTD1_ProtocolVisit__c> protocolVisits = createTestVisitProtocols(study.Id);
        List <VTD1_Protocol_ePRO__c> protocolEPROS = createEPROtocols(study.Id, protocolVisits);
        List <VTR2_Protocol_eDiary_Group__c> groups = createEDiaryGroups(protocolEPROS);
        VTR2_Patient_eDiary_Group__c pg = new VTR2_Patient_eDiary_Group__c(VTR2_eDiary__c = survey.Id, VTR2_Protocol_eDiary_Group__c = groups[0].Id);
        insert pg;
        insert new VTR2_Patient_eDiary_Group_Answer__c(VTR2_eDiary_Answer__c = answer.Id, VTR2_Patient_eDiary_Group__c = pg.Id);

        Test.startTest();
        VT_R2_EDiaryStudyHubController.getData(survey.Id);
        Test.stopTest();
    }
    public static void updateScoreTest(){
        VTD1_Survey_Answer__c answer = [SELECT Id FROM VTD1_Survey_Answer__c LIMIT 1];
        Test.startTest();
        VT_R2_EDiaryStudyHubController.updateScore(answer.Id,'2');
        Test.stopTest();
    }
    public static void markReviewedTest(){
        VTD1_Survey__c survey = [SELECT Id FROM VTD1_Survey__c LIMIT 1];
        Test.startTest();
        VT_R2_EDiaryStudyHubController.markReviewed(survey.Id);
        Test.stopTest();
    }

    private static List <VTD1_ProtocolVisit__c> createTestVisitProtocols(Id studyId) {
        List <VTD1_ProtocolVisit__c> protocolVisits = new List<VTD1_ProtocolVisit__c>();
        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Study__c = studyId;
        protocolVisit.RecordTypeId = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByName().get('Onboarding').getRecordTypeId();
        protocolVisit.VTD1_Onboarding_Type__c = 'Initial Greeting';
        protocolVisit.VTD1_VisitType__c = 'Study Team';
        protocolVisit.VTD1_VisitDuration__c = '1 hour';
        protocolVisit.VTD1_Range__c = 1;
        protocolVisit.VTR2_Visit_Participants__c = 'Patient Guide;Patient';
        protocolVisit.VTD1_VisitNumber__c = 'O1';
        protocolVisits.add(protocolVisit);

        protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Study__c = studyId;
        protocolVisit.RecordTypeId = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByName().get('Onboarding').getRecordTypeId();
        protocolVisit.VTD1_Onboarding_Type__c = 'Consent';
        protocolVisit.VTD1_VisitType__c = 'Study Team';
        protocolVisit.VTD1_VisitDuration__c = '15 minutes';
        protocolVisit.VTD1_Range__c = 1;
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisit.VTD1_VisitNumber__c = 'O2';
        protocolVisits.add(protocolVisit);

        protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Study__c = studyId;
        protocolVisit.RecordTypeId = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByName().get('Onboarding').getRecordTypeId();
        protocolVisit.VTD1_Onboarding_Type__c = 'Screening';
        protocolVisit.VTD1_VisitType__c = 'Study Team';
        protocolVisit.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit.VTD1_Range__c = 1;
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisit.VTD1_VisitNumber__c = 'O3';
        protocolVisits.add(protocolVisit);

        protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Study__c = studyId;
        protocolVisit.RecordTypeId = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByName().get('Protocol').getRecordTypeId();
        protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit.VTD1_VisitType__c = 'Labs';
        protocolVisit.VTD1_VisitDuration__c = '15 minutes';
        protocolVisit.VTD1_Range__c = 1;
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisits.add(protocolVisit);

        protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Study__c = studyId;
        protocolVisit.RecordTypeId = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByName().get('Protocol').getRecordTypeId();
        protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit.VTD1_VisitType__c = 'Health Care Professional (HCP)';
        protocolVisit.VTD1_VisitDuration__c = '15 minutes';
        protocolVisit.VTD1_Range__c = 1;
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisits.add(protocolVisit);

        protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Study__c = studyId;
        protocolVisit.RecordTypeId = Schema.SObjectType.VTD1_ProtocolVisit__c.getRecordTypeInfosByName().get('Protocol').getRecordTypeId();
        protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit.VTD1_VisitType__c = 'Health Care Professional (HCP)';
        protocolVisit.VTD1_VisitDuration__c = '45 minutes';
        protocolVisit.VTD1_Range__c = 1;
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisits.add(protocolVisit);

        insert protocolVisits;
        return protocolVisits;
    }

    private static List <VTD1_Protocol_ePRO__c> createEPROtocols(Id studyId, List <VTD1_ProtocolVisit__c> protocolVisits) {
        List <VTD1_Protocol_ePRO__c> protocols = new List<VTD1_Protocol_ePRO__c>();

        VTD1_Protocol_ePRO__c protocol = new VTD1_Protocol_ePRO__c();
        protocol.VTD1_Study__c = studyId;
        protocol.Name = 'TestAsk1';
        protocol.VTD1_Type__c = 'eDiary';
        protocol.VTD1_Trigger__c = 'Event';
        protocol.VTD1_Period__c = 'Daily';
        protocol.VTD1_Subject__c = 'Patient';
        protocol.VTR4_EventType__c = 'Visit Completion';
        protocol.VTD1_Event__c = protocolVisits[0].Id;
        protocols.add(protocol);

        protocol = new VTD1_Protocol_ePRO__c();
        protocol.VTD1_Study__c = studyId;
        protocol.Name = 'TestAsk2';
        protocol.VTD1_Type__c = 'eDiary';
        protocol.VTD1_Trigger__c = 'Event';
        protocol.VTD1_Period__c = 'Daily';
        protocol.VTD1_Subject__c = 'Patient';
        protocol.VTR4_EventType__c = 'Visit Completion';
        protocol.VTD1_Caregiver_on_behalf_of_Patient__c = true;
        protocol.VTD1_Event__c = protocolVisits[0].Id;
        protocols.add(protocol);

        protocol = new VTD1_Protocol_ePRO__c();
        protocol.VTD1_Study__c = studyId;
        protocol.Name = 'TestAsk3';
        protocol.VTD1_Type__c = 'eDiary';
        protocol.VTD1_Trigger__c = 'Event';
        protocol.VTD1_Period__c = 'Daily';
        protocol.VTD1_Subject__c = 'Patient';
        protocol.VTR4_EventType__c = 'Visit Completion';
        //protocol.VTD1_First_Available__c = protocolVisits[0].Id;
        protocol.VTD1_Event__c = protocolVisits[0].Id;
        protocols.add(protocol);

        protocol = new VTD1_Protocol_ePRO__c();
        protocol.VTD1_Study__c = studyId;
        protocol.Name = 'TestAsk4';
        protocol.VTD1_Type__c = 'eDiary';
        protocol.VTD1_Trigger__c = 'Event';
        protocol.VTD1_Period__c = 'Daily';
        protocol.VTD1_Subject__c = 'Caregiver';
        protocol.VTR4_EventType__c = 'Visit Completion';
        protocol.VTD1_Event__c = protocolVisits[0].Id;
        protocol.VTD1_Safety_Keywords__c = 'TestWord';
        protocols.add(protocol);

        protocol = new VTD1_Protocol_ePRO__c();
        protocol.VTD1_Study__c = studyId;
        protocol.Name = 'TestAsk5';
        protocol.VTD1_Type__c = 'eDiary';
        protocol.VTD1_Trigger__c = 'Event';
        protocol.VTD1_Period__c = 'Daily';
        protocol.VTD1_Subject__c = 'Caregiver';
        protocol.VTD1_Reminder_Window__c = 0.02;
        protocol.VTR4_EventType__c = 'Visit Completion';
        //protocol.VTD1_First_Available__c = protocolVisits[0].Id;
        protocol.VTD1_Event__c = protocolVisits[0].Id;
        protocols.add(protocol);


        for (VTD1_Protocol_ePRO__c protocolEPRO : protocols) {

            if (protocolEPRO.Name == 'TestAsk1')
                protocolEPRO.VDD1_Scoring_Window__c = 0.04;
            else
                    protocolEPRO.VDD1_Scoring_Window__c = 4;

            if (protocolEPRO.Name == 'TestAsk5')
                protocolEPRO.VTD1_Response_Window__c = 0.04;
            else
                    protocolEPRO.VTD1_Response_Window__c = 4;
        }

        insert protocols;
        return protocols;
    }

    private static List <VTR2_Protocol_eDiary_Group__c> createEDiaryGroups(List <VTD1_Protocol_ePRO__c> protocolEPROS) {
        List <VTR2_Protocol_eDiary_Group__c> groups = new List<VTR2_Protocol_eDiary_Group__c>();
        Integer num = 1;
        for (VTD1_Protocol_ePRO__c protocol : protocolEPROS) {
            VTR2_Protocol_eDiary_Group__c eDiarygroup = new VTR2_Protocol_eDiary_Group__c(
                    Name = 'TestGroup_' + (num ++),
                    VTR2_Protocol_eDiary__c = protocol.Id,
                    VTR2_Page_Number__c = 3,
                    VTR2_Number_Within_Page__c = 1
            );
            groups.add(eDiarygroup);
        }
        insert groups;
        return groups;
    }
}