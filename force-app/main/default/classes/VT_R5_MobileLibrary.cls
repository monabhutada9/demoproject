/**
 * @author: Alexander Komarov
 * @date: 21.10.2020
 * @description:
 */

global class VT_R5_MobileLibrary {
    public interface Routable {
        List<String> getMapping();
        VT_R5_MobileRestResponse versionRoute(Map<String, String> parameters);
        void initService();
        Map<String, String> getMinimumVersions(); // to be changed after client decision
    }

    List<Routable> currentRoutes = new List<Routable>();
    public List<Routable> getRoutes() {
        return currentRoutes;
    }
    public VT_R5_MobileLibrary() {
        Profile profile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
        System.debug(profile);
        if (profile.Name != null) {
            if (profile.Name.equals(VT_R4_ConstantsHelper_Profiles.PATIENT_PROFILE_NAME) || profile.Name.equals(VT_R4_ConstantsHelper_Profiles.CAREGIVER_PROFILE_NAME)) {
                loadPatientCGRoutes();
            } else if (profile.Name.equals(VT_R4_ConstantsHelper_Profiles.PI_PROFILE_NAME) || profile.Name.equals(VT_R4_ConstantsHelper_Profiles.SCR_PROFILE_NAME)) {
                loadPISCRRoutes();
            } else {
                loadAuthRoutes();
            }
        }
    }
    private void loadAuthRoutes() {
        currentRoutes.add(
                new VT_R5_MobileAuth()
        );
    }

    private void loadPISCRRoutes() {
        currentRoutes.add(new VT_R5_MobileAuth());
        currentRoutes.add(new VT_R5_MobilePISCRCalendar());
        currentRoutes.add(new VT_R5_MobilePISCRVisit());
        currentRoutes.add(new VT_R5_MobilePISCRHomepage());
        currentRoutes.add(new VT_R5_MobilePISCRUnscheduledVisit());
    }

    private void loadPatientCGRoutes() {
        currentRoutes.add(new VT_R5_MobilePatientHomePage());
        currentRoutes.add(new VT_R5_MobileNewsFeed());
        currentRoutes.add(new VT_R5_MobilePatientProfile());
        currentRoutes.add(new VT_R5_MobilePatientPayments());
        currentRoutes.add(new VT_R5_MobilePatientProfilePhoto());
        currentRoutes.add(new VT_R5_MobilePatientCalendar());
        currentRoutes.add(new VT_R5_MobilePatientVisits());
        currentRoutes.add(new VT_R5_MobileAuth());
    }

}