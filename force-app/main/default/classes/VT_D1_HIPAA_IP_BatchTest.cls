/**
 * Created by Danylo Belei on 16.05.2019.
 * @updater: Rishat Shamratov
 */

@IsTest
private class VT_D1_HIPAA_IP_BatchTest {

    @IsTest
    private static void testBehavior() {
        Test.startTest();
        applyQueryStubs();
        Database.executeBatch(new VT_D1_HIPAA_IP_Batch());
        Test.stopTest();
    }

    private static void applyQueryStubs() {
        VT_Stubber.applyStub(
                'VT_D1_HIPAA_IP_Batch.getQueryLocator',
                new List<User>{ new User(Id = UserInfo.getUserId(), HIPAA_Accepted_Date_Time__c = Datetime.now(), HIPAA_Accepted_from_IP__c = null)}
        );
        Map<String, Object> loginHistoryRecordMap = new Map<String, Object>{
                'Id' => fflib_IDGenerator.generate(LoginHistory.getSObjectType()),
                'SourceIp' => '0.0.0.0',
                'LoginTime' => Datetime.now().addMinutes(-5),
                'UserId' => UserInfo.getUserId()
        };
        VT_Stubber.applyStub(
                'VT_D1_HIPAA_IP_Batch.LoginHistory',
                new List<LoginHistory>{ (LoginHistory) JSON.deserialize(JSON.serialize(loginHistoryRecordMap), LoginHistory.class) }
        );
    }
}