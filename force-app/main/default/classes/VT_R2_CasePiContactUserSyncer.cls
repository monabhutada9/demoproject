/**
* @author: Carl Judge
* @date: 01-Mar-19
* @description: When a PI contact ID is updated, update the user ID to match (or vice versa)
**/

public without sharing class VT_R2_CasePiContactUserSyncer {

    private static Map<String, String> fieldPairs = new Map<String, String> {
        'VTD1_PI_user__c' => 'VTD1_PI_contact__c'
    };

    public static void syncIds(List<Case> cases, Map<Id, Case> oldMap) {
        List<Case> toSync = new List<Case>();
        List<Id> usrIds = new List<Id>();
        List<Id> conIds = new List<Id>();

        for (Case item : cases) {
            Case old = oldMap.get(item.Id);
            for (String usrFld : fieldPairs.keySet()) {
                String conFld = fieldPairs.get(usrFld);
                if (item.get(usrFld) != old.get(usrFld) && item.get(conFld) == old.get(conFld)) {
                    toSync.add(item);
                    usrIds.add((id)item.get(usrFld));
                }
                else if (item.get(conFld) != old.get(conFld) && item.get(usrFld) == old.get(usrFld)) {
                    toSync.add(item);
                    conIds.add((id)item.get(conFld));
                }
            }
        }

        if (! toSync.isEmpty()) {
            Map<Id, User> usersById = new Map<Id, User>();
            Map<Id, User> usersByConId = new Map<Id, User>();
            for (User item : [SELECT Id, ContactId FROM User WHERE Id IN :usrIds OR ContactId IN :conIds]) {
                usersById.put(item.Id, item);
                usersByConId.put(item.ContactId, item);
            }

            for (Case item : cases) {
                Case old = oldMap.get(item.Id);
                for (String usrFld : fieldPairs.keySet()) {
                    String conFld = fieldPairs.get(usrFld);
                    if (item.get(usrFld) != old.get(usrFld) && item.get(conFld) == old.get(conFld)) {
                        if (item.get(usrFld) != null && usersById.get((Id)item.get(usrFld)) != null) {
                            item.put(conFld, usersById.get((Id)item.get(usrFld)).ContactId);
                        }
                    }
                    else if (item.get(conFld) != old.get(conFld) && item.get(usrFld) == old.get(usrFld)) {
                        if (item.get(conFld) != null && usersByConId.get((Id)item.get(conFld)) != null) {
                            item.put(usrFld, usersByConId.get((Id)item.get(conFld)).Id);
                        }
                    }
                }
            }
        }
    }
}