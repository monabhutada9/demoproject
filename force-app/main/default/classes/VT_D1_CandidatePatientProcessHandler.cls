public without sharing class VT_D1_CandidatePatientProcessHandler {
    public static Set<Id> rescueStudyIDs;
    /*function to set Study__c on CandidatePatient if it is null. Study__c is populated by finding appropriate Study according VTD1_ProtocolNumber__c
    Should be study.Name == candidatePatient.VTD1_ProtocolNumber__c
    Invoke from trigger VT_D1_CandidatePatientProcess
    */
    public static void setStudyByProtocolName(List<HealthCloudGA__CandidatePatient__c> candidatePatientList) {
        if (candidatePatientList == null || candidatePatientList.isEmpty()) return;
        List<HealthCloudGA__CandidatePatient__c> candidatePatientListToUpd = new List<HealthCloudGA__CandidatePatient__c>();
        //compose set of all VTD1_ProtocolNumber__c
        Set<String> protocolNameSet = new Set<String>();
        Set<String> siteIds = new Set<String>();
        Set<Id> setStudyId = new set<Id>();
        for (HealthCloudGA__CandidatePatient__c cpItem : candidatePatientList) {
            if(!String.isBlank(cpItem.VTD1_ProtocolNumber__c)){
                protocolNameSet.add(cpItem.VTD1_ProtocolNumber__c);
            }
            if (String.isNotBlank(cpItem.VTR5_Study_Site_Number__c)) {
                siteIds.add(cpItem.VTR5_Study_Site_Number__c);
            }
            if(cpItem.Study__c != null){
                setStudyId.add(cpItem.Study__c);
            }
            if (cpItem.Study__c == null && !String.isBlank(cpItem.VTD1_ProtocolNumber__c)) {
                candidatePatientListToUpd.add(cpItem);
            }
        }

        //if (protocolNameSet.isEmpty()) return;

        //find all study records according to protocolNameSet
        List<HealthCloudGA__CarePlanTemplate__c> studyList = [
            SELECT Name, VT_R4_RescueTelVisitStudy__c,
                (SELECT VTD1_Study_Site_Number__c, VTD1_Study_Team_Member__r.User__c, VTD1_Country_Code__c
                FROM Virtual_Sites__r
                WHERE VTD1_Study_Site_Number__c IN :siteIds)
            FROM HealthCloudGA__CarePlanTemplate__c
            WHERE Name IN :protocolNameSet OR ID IN : setStudyId
        ];

        if (studyList.isEmpty()) {
            return;
        }

        Map<String, Id> protocolName_studyId = new Map<String, Id>();
        Map<String,Map<String,Virtual_Site__c>> virtualSiteByCandidateKey = new Map<String, Map<String,Virtual_Site__c>>();
        for (HealthCloudGA__CarePlanTemplate__c studyItem : studyList) {
            protocolName_studyId.put(studyItem.Name, studyItem.Id);
            if(rescueStudyIDs ==null){
                rescueStudyIDs = new Set<Id>();
            }
            if(studyItem.VT_R4_RescueTelVisitStudy__c){
                rescueStudyIDs.add(studyItem.Id);
            }
            for (Virtual_Site__c site : studyItem.Virtual_Sites__r) {
                String siteCountryCode = site.VTD1_Country_Code__c != null ? site.VTD1_Country_Code__c : '';
                if ( ! virtualSiteByCandidateKey.containsKey(studyItem.Name + ':' + site.VTD1_Study_Site_Number__c)) {
                    virtualSiteByCandidateKey.put(studyItem.Name + ':' + site.VTD1_Study_Site_Number__c, new Map<String,Virtual_Site__c>());
                }
                virtualSiteByCandidateKey.get(studyItem.Name + ':' + site.VTD1_Study_Site_Number__c).put(siteCountryCode, site);
            }
        }

        //fill studyId on CandidatePatient
        for (HealthCloudGA__CandidatePatient__c cpItem : candidatePatientListToUpd) {
            if (protocolName_studyId.containsKey(cpItem.VTD1_ProtocolNumber__c)) {
                cpItem.Study__c = protocolName_studyId.get(cpItem.VTD1_ProtocolNumber__c);
            }

            String candidateKeyBySite = cpItem.VTD1_ProtocolNumber__c + ':' + cpItem.VTR5_Study_Site_Number__c;
            if (virtualSiteByCandidateKey.containsKey(candidateKeyBySite)) {
                if (cpItem.VTR5_Country_Code__c != null) {
                    cpItem.VT_R4_VirtualSite__c = virtualSiteByCandidateKey.get(candidateKeyBySite).get(cpItem.VTR5_Country_Code__c).Id;
                    cpItem.VTR2_Pi_Id__c = virtualSiteByCandidateKey.get(candidateKeyBySite).get(cpItem.VTR5_Country_Code__c).VTD1_Study_Team_Member__r.User__c;
                } else {
                    cpItem.VT_R4_VirtualSite__c = virtualSiteByCandidateKey.get(candidateKeyBySite).values().get(0).Id;
                    cpItem.VTR2_Pi_Id__c = virtualSiteByCandidateKey.get(candidateKeyBySite).values().get(0).VTD1_Study_Team_Member__r.User__c;
                }
            }
            //Info: SH-21894 contains patient country code but still not confirmed for implementation.
//            String candidateKeyByPatientCountryCode = cpItem.VTD1_ProtocolNumber__c + ':' + cpItem.VTR5_Study_Site_Number__c + ':' + cpItem.HealthCloudGA__Address1Country__c;
//            if (virtualSiteByCandidateKey.containsKey(candidateKeyByPatientCountryCode)) {
//                cpItem.VT_R4_VirtualSite__c = virtualSiteByCandidateKey.get(candidateKeyByPatientCountryCode).Id;
//                cpItem.VTR2_Pi_Id__c = virtualSiteByCandidateKey.get(candidateKeyByPatientCountryCode).VTD1_Study_Team_Member__r.User__c;
//            }
        }
    }

    public static void generateUsernameAliasForUser(List<HealthCloudGA__CandidatePatient__c> candidatePatientList) {
        if (candidatePatientList == null || candidatePatientList.isEmpty()) return;

        for (HealthCloudGA__CandidatePatient__c cpItem : candidatePatientList) {
            if (String.isBlank(cpItem.rr_Alias__c)) {
                cpItem.rr_Alias__c = getAlias(cpItem.rr_firstName__c, cpItem.rr_lastName__c);
            }

            if (String.isBlank(cpItem.rr_Username__c)) {
                cpItem.rr_Username__c = cpItem.rr_Email__c;
            }
        }
    }

    public static String getAlias(String firstName, String lastName) {
        String alias;
        if (!String.isBlank((firstName)) && !String.isBlank(lastName)) {
            alias = firstName.substring(0, 1);
            alias += lastName.length() >= 4 ? lastName.substring(0, 4) : lastName;
            alias = alias.toLowerCase();
        }
        return alias;
    }

    public static void setConvertionProcessStatus(List<HealthCloudGA__CandidatePatient__c> candidatePatientList) {
        List<HealthCloudGA__CandidatePatient__c> candidatePatientsToBeConverted = new List<HealthCloudGA__CandidatePatient__c>();
        for (HealthCloudGA__CandidatePatient__c cp : candidatePatientList) {
            if(!cp.VTD2_Study_Recruitment_Is_On_Hold__c && !cp.VTD1_DuplicatedRecord__c && cp.Study__c != null) {
                candidatePatientsToBeConverted.add(cp);
            } else {
                cp.VTR4_Convertation_Process_Status__c = VT_R4_PatientConversion.PROCESS_STATUS_COMPLETED;
            }
        }
        Id integrationProfileId = null;
        List <Profile> profiles = [select Id from Profile where Name = 'Integration Profile'];
        if (!profiles.isEmpty()) {
            integrationProfileId = profiles[0].Id;
        }

        String convertionStatus = candidatePatientsToBeConverted.size() == 1 && UserInfo.getProfileId() != integrationProfileId ?
                VT_R4_PatientConversion.PROCESS_STATUS_INITIAL :
                VT_R4_PatientConversion.PROCESS_STATUS_INITIAL_BULK;
        for (HealthCloudGA__CandidatePatient__c cp : candidatePatientsToBeConverted) {
            cp.VTR4_Convertation_Process_Status__c = convertionStatus;
            if (cp.VTR5_Priority__c == null) {
                cp.VTR5_Priority__c = 0;
            }
        }
    }

    public static void processCandidateConversion(List<HealthCloudGA__CandidatePatient__c> candidatePatientList) {
        if (candidatePatientList == null || candidatePatientList.isEmpty()) return;

        List<HealthCloudGA__CandidatePatient__c> candidatePatientsToBeConverted = new List<HealthCloudGA__CandidatePatient__c>();
        List<HealthCloudGA__CandidatePatient__c> candidatePatientsToBeNotConverted = new List<HealthCloudGA__CandidatePatient__c>();
        //Set<Id> candidatePatientIdsToBeConvertedByNewProcess = new Set<Id>();
        for (HealthCloudGA__CandidatePatient__c cp : candidatePatientList) {
            // FR-ELI-205 If study is on hold the candidate patient won't be converted
            if(!cp.VTD2_Study_Recruitment_Is_On_Hold__c && !cp.VTD1_DuplicatedRecord__c && cp.Study__c != null) {
                //if (cp.VTR4_New_Conversion_Process__c) {
                //candidatePatientIdsToBeConvertedByNewProcess.add(cp.Id);
                candidatePatientsToBeConverted.add(cp);
            } else {
                candidatePatientsToBeNotConverted.add(new HealthCloudGA__CandidatePatient__c(Id = cp.Id, VTR4_Convertation_Process_Status__c = VT_R4_PatientConversion.PROCESS_STATUS_COMPLETED));
            }
        }
        if (!candidatePatientsToBeNotConverted.isEmpty())
            update candidatePatientsToBeNotConverted;



        if (Test.isRunningTest()) {
            VT_R4_PatientConversion.convert(candidatePatientsToBeConverted);
        }

    }

    /*public static void createCaregivers(List<HealthCloudGA__CandidatePatient__c> recs, Map<Id, HealthCloudGA__CandidatePatient__c> oldMap) {
        List<HealthCloudGA__CandidatePatient__c> convertedRecsWithCaregivers = new List<HealthCloudGA__CandidatePatient__c>();
        for (HealthCloudGA__CandidatePatient__c item : recs) {
            if (item.HealthCloudGA__IsConvertedToPatient__c && ! oldMap.get(item.Id).HealthCloudGA__IsConvertedToPatient__c) {
                if (item.VTR2_Caregiver_Email__c != null) {
                    convertedRecsWithCaregivers.add(item);
                }
            }
        }

        if (! convertedRecsWithCaregivers.isEmpty()) {
            VT_R3_PublishedAction_CreateCaregivers publishedAction = new VT_R3_PublishedAction_CreateCaregivers(convertedRecsWithCaregivers);
            publishedAction.publish();
        }
    }*/

    public static Boolean isTransferProcess(HealthCloudGA__CandidatePatient__c candidatePatient) {
        return candidatePatient.VTR4_CaseForTransfer__c != null;
    }

    /* Method to validate that candidate patients are provided with Virtual Site ,
if it's associated with Rescue Study, PI ID is specified and the PI is associated with more than two sites*/
    public static void validateRescueStudySTM(List<HealthCloudGA__CandidatePatient__c> cpList){

        Set<Id> PIIDUser = new Set<Id>();
        for(HealthCloudGA__CandidatePatient__c cpitem : cpList){
            if(cpitem.Study__c != null && rescueStudyIDs!= null && rescueStudyIDs.contains(cpitem.Study__c)){
                rescueStudyIDs.add(cpitem.Study__c);
                PIIDUser.add(cpitem.VTR2_Pi_Id__c);
            }
        }

        if(rescueStudyIDs !=null && rescueStudyIDs.size() == 0)
            return;


        Map<Id,List<Study_Team_Member__c>> piIdToStm = new Map<Id,List<Study_Team_Member__c>>();
        for(Study_Team_Member__c stm :[Select id,user__c,Study__c from Study_Team_Member__c where user__c IN : PIIDUser and Study__r.VT_R4_RescueTelVisitStudy__c= true])
        {
            list<Study_Team_Member__c>  templist= new list<Study_Team_Member__c>();
            if(piIdToStm.containsKey(stm.user__c)){
                templist = piIdToStm.get(stm.user__c);
                templist.add(stm);
                piIdToStm.put(stm.user__c,templist);
            }
            else{
                templist.add(stm);
                piIdToStm.put(stm.user__c,templist);
            }
        }
        for(HealthCloudGA__CandidatePatient__c cp : cpList){
            if(!piIdToStm.isEmpty() && cp.VTR2_Pi_Id__c != null && piIdToStm.get(cp.VTR2_Pi_Id__c).size() > 1 && cp.VT_R4_VirtualSite__c == null){
                cp.addError('This PI is associated with more than one virtual site please add the virtual site');
            }
        }

    }

    public static void hideBirthDateParams(List <HealthCloudGA__CandidatePatient__c> candidatePatients) {
        VT_R4_PatientConversion.setStudyGeographies(candidatePatients);

        for (HealthCloudGA__CandidatePatient__c candidatePatient : candidatePatients) {
            VTD2_Study_Geography__c geography = VT_R4_PatientConversion.getGeography(candidatePatient);

            if (candidatePatient.healthcloudga__BirthDate__c != null && geography != null
                    && geography.VT_R3_Date_Of_Birth_Restriction__c == 'Year Only') {
                candidatePatient.healthcloudga__BirthDate__c = Date.newInstance(
                        candidatePatient.healthcloudga__BirthDate__c.year(), 1, 1
                );
            }
        }
    }
}