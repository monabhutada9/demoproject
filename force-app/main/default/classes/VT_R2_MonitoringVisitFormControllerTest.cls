/**
 * Created by User on 19/05/17.
 */
@isTest
public without sharing class VT_R2_MonitoringVisitFormControllerTest {

    public static void getNewMonitoringVisitTest() {
        Test.startTest();
        System.assertNotEquals('{}', VT_R2_MonitoringVisitFormController.getNewMonitoringVisit([SELECT Id FROM Virtual_Site__c LIMIT 1].Id));
        System.assertEquals('{}', VT_R2_MonitoringVisitFormController.getNewMonitoringVisit(null));
        Test.stopTest();
    }
    /*public static void getVisitCRATest() {

        Test.startTest();
        System.assertNotEquals('{}', VT_R2_MonitoringVisitFormController.getVisitCRA([SELECT Id FROM Virtual_Site__c LIMIT 1].Id, 'Onsite'));
        System.assertNotEquals('{}', VT_R2_MonitoringVisitFormController.getVisitCRA([SELECT Id FROM Virtual_Site__c LIMIT 1].Id, 'Remote'));
        System.assertEquals('{}', VT_R2_MonitoringVisitFormController.getVisitCRA(null, 'Remote'));
        Test.stopTest();
    }*/
    public static void getVisitDatesTest() {
        Test.startTest();
        System.assertNotEquals('{}', VT_R2_MonitoringVisitFormController.getVisitDates(Datetime.now().addDays(-1), Datetime.now(), 2));
        Test.stopTest();
    }
}