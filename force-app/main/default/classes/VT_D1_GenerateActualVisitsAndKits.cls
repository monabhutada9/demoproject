public class VT_D1_GenerateActualVisitsAndKits { //implements Queueable {
//    Need to cover next string, this class will be deleted
   String t = '';


/* replaced with VT_R4_GenerateActualVisitsAndKits
    private static final Map<String, String> PROTOCOL_TO_ACTUAL_VISIT_TYPES = new Map<String, String>{
            VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_LABS => VT_R4_ConstantsHelper_VisitsEvents.RT_ACTUAL_VISIT_LABS,
            VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_STUDY_TEAM => VT_R4_ConstantsHelper_VisitsEvents.RT_ACTUAL_VISIT_STUDY_TEAM,
            VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_HCP => VT_R4_ConstantsHelper_VisitsEvents.RT_ACTUAL_VISIT_HCP
    };

    private static final Map<String, String> PROTOCOL_TO_PATIENT_KIT_TYPES = new Map<String, String>{
            VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_AD_HOC_LAB => VT_R4_ConstantsHelper_KitsOrders.RT_PATIENT_KIT_AD_HOC_LAB,
            VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_CONNECTED_DEVICES => VT_R4_ConstantsHelper_KitsOrders.RT_PATIENT_KIT_CONNECTED_DEVICES,
            VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_IMP => VT_R4_ConstantsHelper_KitsOrders.RT_PATIENT_KIT_IMP,
            VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_LAB => VT_R4_ConstantsHelper_KitsOrders.RT_PATIENT_KIT_LAB,
            VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_STUDY_HUB_TABLET => VT_R4_ConstantsHelper_KitsOrders.RT_PATIENT_KIT_STUDY_HUB_TABLET,
            VT_R4_ConstantsHelper_KitsOrders.RT_PROTOCOL_KIT_WELCOME_TO_STUDY_PACKAGE => VT_R4_ConstantsHelper_KitsOrders.RT_PATIENT_KIT_WELCOME_TO_STUDY_PACKAGE
    };

    private static final String DEFAULT_MODALITY = 'At Home';

    //Commented out because SH-3929
    /*private static final Map<String, String> MODALITY_MAP = new Map<String, String>{
            'Study Team' => 'At Home',
            'Phlebotomist' => 'At Home',
            'Home Health Nurse' => 'At Home',
            'PSC' => 'At Location',
            'Health Care Professional (HCP)' => 'At Location',
            'Other' => 'At Location'
    };

    public Id caseId;
    public Id cpId;
    private Boolean sendSubject = false;
    private Boolean createVisits = false;
    private Boolean createDeliveries = false;

    private Case cas;
    private Set<String> returnNumbersIMP = new Set<String>();
    private Set<String> returnNumbersLab = new Set<String>();
    private List<VTD1_Order__c> newDeliveries = new List<VTD1_Order__c>();
    private Map<Id, VTD1_Patient_Kit__c> newKitsByProtocolId = new Map<Id, VTD1_Patient_Kit__c>();
    private Map<Id, VTD1_Patient_Device__c> newDevicesByProtocolId = new Map<Id, VTD1_Patient_Device__c>();
    private List<Patient_LKC__c> newLKCs = new List<Patient_LKC__c>();
    private List<VTD1_Patient_Lab_Sample__c> newSamples = new List<VTD1_Patient_Lab_Sample__c>();
    private List<VTD1_Patient_Accessories__c> newAccessories = new List<VTD1_Patient_Accessories__c>();
    private List<VTD1_Order__c> childDeliveries = new List<VTD1_Order__c>();
    private List<VTD1_Order__c> childImpLabDeliveries = new List<VTD1_Order__c>();
    private List<VTD1_Patient_Kit__c> packMatKits = new List<VTD1_Patient_Kit__c>();

    private String statusKitsDeliveries = VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_FINISHED;
    private String statusVisits = VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_FINISHED;
    public VT_D1_GenerateActualVisitsAndKits(Id caseId, Id cpId, Boolean sendSubject, Boolean createVisits, Boolean createDeliveries) {
        this.caseId = caseId;
        this.cpId = cpId;
        this.sendSubject = sendSubject;
        this.createVisits = createVisits;
        this.createDeliveries = createDeliveries;
    }

    public VT_D1_GenerateActualVisitsAndKits() {
    }

    public void execute(QueueableContext context) {
        try {
        queryCase();
        if (createVisits) {
            createActualVisitsBatch();
        }
        if (createDeliveries) {
            createDeliveries();
            createKits();
            createDevices();
            createLKCs();
            createAccessories();
            createLabSamples();
            VT_D1_PatientKitsDevAndAccCounting.recount(new Id[]{
                    this.cas.Id
            });
            updateDeliveries();
        }
        } catch (Exception e) {
            System.debug('VT_D1_GenerateActualVisitsAndKits execute ' + e.getMessage());
            HealthCloudGA__CandidatePatient__c cp = [select Id from HealthCloudGA__CandidatePatient__c where Id = :cpId];
            System.debug('cp = ' + cp);
            if (cp != null) {
                VT_R4_PatientConversionHelper.logError(null, cp, 'VTR4_StatusSTA__c', e);
                VT_R4_PatientConversion.saveErrors();
            }
        }
    }

    public void execute() {
        try {
        queryCase();
        if (createVisits) {
        createActualVisits();
        }
        if (createDeliveries) {
        createDeliveries();
        createKits();
        createDevices();
        createLKCs();
        createAccessories();
        createLabSamples();
        VT_D1_PatientKitsDevAndAccCounting.recount(new Id[]{
                this.cas.Id
        });
        updateDeliveries();
        }
        if (createVisits || createDeliveries) {
            if (cpId != null) {
                HealthCloudGA__CandidatePatient__c cp = new HealthCloudGA__CandidatePatient__c(
                        Id = cpId
                );
                    if (createVisits) {
                        cp.VTR4_StatusVisits__c = statusVisits;
                    }
                    if (createDeliveries) {
                        cp.VTR4_StatusDKDALS__c = statusKitsDeliveries;
                    }
                update (SObject) cp;
            }
        }
        } catch (Exception e) {
            System.debug('VT_D1_GenerateActualVisitsAndKits execute ' + e.getMessage());
            HealthCloudGA__CandidatePatient__c cp = [select Id from HealthCloudGA__CandidatePatient__c where Id = :cpId];
            System.debug('cp = ' + cp);
            if (cp != null) {
                VT_R4_PatientConversionHelper.logError(null, cp, 'VTR4_StatusSTA__c', e);
                VT_R4_PatientConversion.saveErrors();
            }
        }
        /*if (!System.isFuture() && !System.isBatch()) {
            initiateSharingCreationFuture(this.cas.Id);
        } else {
            initiateSharingCreation();
        }
    }

    private void queryCase() {
        this.cas = [
                SELECT Id, ContactId, Preferred_Lab_Visit__c, VTD1_Study__c, VTD1_Study__r.VTD1_IMP_Return_Numbers__c,
                        VTD1_Study__r.VTD1_Lab_Return_Numbers__c,
                        VTD1_Study__r.VTD1_Return_Process__c, VTD1_Subject_ID__c,
                        VTD1_Study__r.VTD1_Lab_Return_Process__c, VTD1_Virtual_Site__c
                FROM Case
                WHERE Id = :this.caseId
                FOR UPDATE
        ];

        if (this.cas.VTD1_Study__r.VTD1_IMP_Return_Numbers__c != null) {
            this.returnNumbersIMP = new Set<String>(this.cas.VTD1_Study__r.VTD1_IMP_Return_Numbers__c.replace(' ', '').split(','));
        }
        if (this.cas.VTD1_Study__r.VTD1_Lab_Return_Numbers__c != null) {
            this.returnNumbersLab = new Set<String>(this.cas.VTD1_Study__r.VTD1_Lab_Return_Numbers__c.replace(' ', '').split(','));
        }
    }

    private List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrapProtocolVisits(List<VTD1_ProtocolVisit__c> protocolVisits) {
        List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrappedRecords = new List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper>();
        for (VTD1_ProtocolVisit__c protocol : protocolVisits) {
            wrappedRecords.add(new VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper(
                    protocol, protocol.VTR4_Original_Version__c, protocol.VTD1_Protocol_Amendment__c, protocol.VTR4_Remove_from_Protocol__c
            ));
        }
        for (VTD1_ProtocolVisit__c protocol : protocolVisits) {
            System.debug('protocol = ' + protocol);
        }
        return wrappedRecords;
    }

    private void createActualVisitsBatch() {
        Database.executeBatch(new VT_R4_BatchActualVisitsCreation(cas, cpId, sendSubject));
    }

    private void createActualVisits() {
        String statusVisits = VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_FINISHED;
        List<VTD1_ProtocolVisit__c> pVisits = [
                SELECT Id, Name, VTD1_VisitType__c, VTD1_EDC_Name__c, VTD1_PreVisitInstructions__c, VTR2_SH_TelevisitNeeded__c,
                        VTD1_Independent_Rater_Flag__c, VTD1_Patient_Visit_Checklist__c, VTD1_Visit_Checklist__c, VTR2_Modality__c,
                        RecordTypeId, VTD1_VisitNumber__c, VTD1_Protocol_Amendment__c, VTR4_Original_Version__c, VTR4_Remove_from_Protocol__c
                FROM VTD1_ProtocolVisit__c
                WHERE VTD1_Study__c = :this.cas.VTD1_Study__c
                AND VTD1_VisitType__c IN ('Labs', 'Health Care Professional (HCP)', 'Study Team')
                AND RecordTypeId != :VT_R4_ConstantsHelper_RecordTypes.RECORD_TYPE_ID_PROTOCOL_VISIT_SUBGROUP_TIMELINE_VISIT
                FOR UPDATE
        ];
        if (!pVisits.isEmpty()) {
            List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrappedRecords = wrapProtocolVisits(pVisits);
            VT_R4_ProtocolAmendmentVersionHelper paVersionHelper = new VT_R4_ProtocolAmendmentVersionHelper(wrappedRecords);
            pVisits = paVersionHelper.getCorrectVersions(cas.VTD1_Virtual_Site__c, pVisits);
            Map<String, Schema.RecordTypeInfo> recTypeMap = Schema.SObjectType.VTD1_Actual_Visit__c.getRecordTypeInfosByDeveloperName();
            List<VTD1_Actual_Visit__c> aVisits = new List<VTD1_Actual_Visit__c>();
            for (VTD1_ProtocolVisit__c pVisit : pVisits) {
                VTD1_Actual_Visit__c newVisit = new VTD1_Actual_Visit__c(
                        Name = pVisit.VTD1_EDC_Name__c,
                        RecordTypeId = recTypeMap.get(PROTOCOL_TO_ACTUAL_VISIT_TYPES.get(pVisit.VTD1_VisitType__c)).getRecordTypeId(),
                        Sub_Type__c = pVisit.VTD1_VisitType__c == 'Labs' ? this.cas.Preferred_Lab_Visit__c : null,
                        VTD1_Case__c = this.cas.Id,
                        VTD1_Contact__c = this.cas.ContactId,
                        VTD1_Protocol_Visit__c = pVisit.Id,
                        VTD1_Pre_Visit_Instructions__c = pVisit.VTD1_PreVisitInstructions__c,
                        VTR2_Independent_Rater_Flag__c = pVisit.VTD1_Independent_Rater_Flag__c,
                        VTD1_Additional_Patient_Visit_Checklist__c = pVisit.VTD1_Patient_Visit_Checklist__c,
                        VTD1_Additional_Visit_Checklist__c = pVisit.VTD1_Visit_Checklist__c,
                        VTR2_Televisit__c = pVisit.VTR2_SH_TelevisitNeeded__c,
                        VTR2_Modality__c = pVisit.VTR2_Modality__c != null ? pVisit.VTR2_Modality__c : DEFAULT_MODALITY
                );
                aVisits.add(newVisit);
            }
            if (!aVisits.isEmpty()) {
                try {
                    insert aVisits;
                } catch (Exception dbE) {
                    insert ((SObject) new VTR4_Conversion_Log__c(
                            VTR4_Candidate_Patient__c = cpId,
                            VTR4_Error_Message__c = dbE.getMessage() + ' ' + dbE.getStackTraceString()));
                    statusVisits = VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_FAILED;
                }
            }
        }
    }

    private List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrapProtocolDeliveries(List<VTD1_Protocol_Delivery__c> protocolDeliveries) {
        List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrappedRecords = new List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper>();
        for (VTD1_Protocol_Delivery__c delivery : protocolDeliveries) {
            wrappedRecords.add(new VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper(
                    delivery, delivery.VTR4_Original_Version__c, delivery.VTR4_Protocol_Amendment__c, delivery.VTR4_Remove_from_Protocol__c
            ));
        }
        return wrappedRecords;
    }

    private void createDeliveries() {
        String statusKitsDeliveries = VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_FINISHED;
        List<VTD1_Protocol_Delivery__c> pDels = [
                SELECT
                        Id,
                        Name,
                        VTR4_Original_Version__c,
                        VTR4_Protocol_Amendment__c,
                        VTR4_Remove_from_Protocol__c
                FROM VTD1_Protocol_Delivery__c
                WHERE Care_Plan_Template__c = :this.cas.VTD1_Study__c
        ];
        if (!pDels.isEmpty()) {
            List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrappedRecords = wrapProtocolDeliveries(pDels);
            VT_R4_ProtocolAmendmentVersionHelper paVersionHelper = new VT_R4_ProtocolAmendmentVersionHelper(wrappedRecords);
            pDels = paVersionHelper.getCorrectVersions(cas.VTD1_Virtual_Site__c);
            for (VTD1_Protocol_Delivery__c pDel : pDels) {
                this.newDeliveries.add(new VTD1_Order__c(
                        VTD1_Case__c = this.cas.Id,
                        VTD1_Protocol_Delivery__c = pDel.Id,
                        Name = pDel.Name,
                        VTD1_Status__c = 'Not Started'
                ));
            }
            if (!newDeliveries.isEmpty()) {
                try {
                    insert newDeliveries;
                } catch (Exception dbE) {
                    insert ((SObject) new VTR4_Conversion_Log__c(
                            VTR4_Candidate_Patient__c = cpId,
                            VTR4_Error_Message__c = dbE.getMessage() + ' ' + dbE.getStackTraceString()));
                    statusKitsDeliveries = VT_R4_ConstantsHelper_AccountContactCase.CANDIDATE_PATIENT_CONVERSION_FAILED;
                }
            }
        }
    }

    private void createKits() {
        if (this.newDeliveries.isEmpty()) {
            return;
        }
        Map<Id, VTD1_Order__c> deliveriesByProtocolId = new Map<Id, VTD1_Order__c>();
        for (VTD1_Order__c item : this.newDeliveries) {
            deliveriesByProtocolId.put(item.VTD1_Protocol_Delivery__c, item);
        }

        Map<String, Schema.RecordTypeInfo> recTypeMap = Schema.SObjectType.VTD1_Patient_Kit__c.getRecordTypeInfosByDeveloperName();

        for (VTD1_Protocol_Kit__c pKit : [
                SELECT Id, Name, VTD1_Protocol_Delivery__c, RecordType.DeveloperName, VTD1_HHN_Phleb_Visit_Activity_Checklist__c,
                        VTD1_PSC_Visit_Activity_Checklist__c, VTR2_Quantity__c,VTR2_Description__c, VTR2_TemperatureControlled__c
                FROM VTD1_Protocol_Kit__c
                WHERE VTD1_Care_Plan_Template__c = :this.cas.VTD1_Study__c
                AND VTD1_Protocol_Delivery__c IN :deliveriesByProtocolId.keySet()
        ]) {
            System.debug('***protocolKit ' + pKit);
            this.newKitsByProtocolId.put(pKit.Id, new VTD1_Patient_Kit__c(
                    Name = pKit.Name,
                    VTD1_Case__c = this.cas.Id,
                    VTD1_Patient_Delivery__c = deliveriesByProtocolId.get(pKit.VTD1_Protocol_Delivery__c).Id,
                    VTD1_Protocol_Kit__c = pKit.Id,
                    RecordTypeId = recTypeMap.get(PROTOCOL_TO_PATIENT_KIT_TYPES.get(pKit.RecordType.DeveloperName)).getRecordTypeId(),
                    VTD1_Status__c = 'Pending',
                    VTD1_HHN_Phleb_Visit_Activity_Checklist__c = pKit.VTD1_HHN_Phleb_Visit_Activity_Checklist__c,
                    VTD1_PSC_Visit_Activity_Checklist__c = pKit.VTD1_PSC_Visit_Activity_Checklist__c,
                    VTD1_IntegrationId__c = this.cas.VTD1_Subject_ID__c + '-' + pKit.Name,
                    VTR2_Quantity__c = pKit.VTR2_Quantity__c,
                    VTR2_Description__c = pKit.VTR2_Description__c,
                    VTR2_IfTemperatureControlled__c = pKit.VTR2_TemperatureControlled__c
            ));
        }

        if (!this.newKitsByProtocolId.isEmpty()) {
            insert this.newKitsByProtocolId.values();
        }
    }

    private void createDevices() {
        if (this.newKitsByProtocolId.isEmpty()) {
            return;
        }
        Map<String, Schema.RecordTypeInfo> recTypeMap = Schema.SObjectType.VTD1_Patient_Device__c.getRecordTypeInfosByDeveloperName();

        for (HealthCloudGA__EhrDevice__c pDevice : [
                SELECT Id, VTD1_Protocol_Kit__c, VTD1_Manufacturer__c, VTD1_ModelName__c, VTD1_Device_Type__c,
                        VTD1_Device_Name__c, VTD1_Kit_Type__c
                FROM HealthCloudGA__EhrDevice__c
                WHERE VTD1_Care_Plan_Template__c = :this.cas.VTD1_Study__c
                AND VTD1_isArchivedVersion__c = false
                AND VTD1_Protocol_Kit__c IN :this.newKitsByProtocolId.keySet()
        ]) {
            System.debug('***pDEVICE ' + pDevice + ' ***DeliveryId ' + this.newKitsByProtocolId);
            this.newDevicesByProtocolId.put(pDevice.Id, new VTD1_Patient_Device__c(
                    VTD1_Device_Status__c = 'Pending',
                    VTD1_Protocol_Device__c = pDevice.Id,
                    VTD1_Patient_Kit__c = this.newKitsByProtocolId.get(pDevice.VTD1_Protocol_Kit__c).Id,
                    VTD1_Patient_Delivery__c = this.newKitsByProtocolId.get(pDevice.VTD1_Protocol_Kit__c).VTD1_Patient_Delivery__c,
                    VTD1_Case__c = this.cas.Id,
                    VTD1_Manufacturer__c = pDevice.VTD1_Manufacturer__c,
                    VTD1_Model_Name__c = pDevice.VTD1_ModelName__c,
                    RecordTypeId = recTypeMap.get(pDevice.VTD1_Kit_Type__c == 'Connected Devices' ? 'VTD1_Connected_Device' : 'VTD1_Tablet_Device').getRecordTypeId()
            ));
        }

        if (!this.newDevicesByProtocolId.isEmpty()) {
            insert this.newDevicesByProtocolId.values();
        }
    }

    private void createLKCs() {
        if (this.newKitsByProtocolId.isEmpty()) {
            return;
        }
        for (VTD1_Protocol_LKC__c pLKC : [
                SELECT Id, VTD1_Lab_Kit__c, VTD1_Content_Item_Description__c, VTD1_Content_Item_Type__c
                FROM VTD1_Protocol_LKC__c
                WHERE VTD1_Lab_Kit__c IN :this.newKitsByProtocolId.keySet()
                AND VTD1_Care_Plan_Template__c = :this.cas.VTD1_Study__c
                AND VTD1_isArchivedVersion__c = FALSE
        ]) {
            this.newLKCs.add(new Patient_LKC__c(
                    VTD1_CSM__c = this.cas.Id,
                    VTD1_Lab_Kit__c = this.newKitsByProtocolId.get(pLKC.VTD1_Lab_Kit__c).Id,
                    VTD1_Patient_Delivery__c = this.newKitsByProtocolId.get(pLKC.VTD1_Lab_Kit__c).VTD1_Patient_Delivery__c,
                    VTD1_Protocol_LKC__c = pLKC.Id,
                    VTD1_Description__c = pLKC.VTD1_Content_Item_Description__c,
                    VTD1_Type__c = pLKC.VTD1_Content_Item_Type__c
            ));
        }

        if (!this.newLKCs.isEmpty()) {
            insert this.newLKCs;
        }
    }

    private void createLabSamples() {
        if (this.newKitsByProtocolId.isEmpty()) {
            return;
        }
        for (VTD1_Protocol_Lab_Sample__c pSample : [
                SELECT Id, VTD1_Lab_Kit__c
                FROM VTD1_Protocol_Lab_Sample__c
                WHERE VTD1_Lab_Kit__c IN :this.newKitsByProtocolId.keySet()
                AND VTD1_Care_Plan_Template__c = :this.cas.VTD1_Study__c
                AND VTD1_isArchivedVersion__c = FALSE
        ]) {
            this.newSamples.add(new VTD1_Patient_Lab_Sample__c(
                    VTD1_CSM__c = this.cas.Id,
                    VTD1_Lab_Kit__c = this.newKitsByProtocolId.get(pSample.VTD1_Lab_Kit__c).Id,
                    VTD1_Patient_Delivery__c = this.newKitsByProtocolId.get(pSample.VTD1_Lab_Kit__c).VTD1_Patient_Delivery__c,
                    VTD1_Protocol_Lab_Sample__c = pSample.Id
            ));
        }

        if (!this.newSamples.isEmpty()) {
            insert this.newSamples;
        }
    }

    private void createAccessories() {
        if (this.newDevicesByProtocolId.isEmpty()) {
            return;
        }
        Id recTypeId = Schema.SObjectType.VTD1_Patient_Accessories__c.getRecordTypeInfosByDeveloperName().get('Scheduled_Accessory').getRecordTypeId();

        for (VTD1_Protocol_Accessories__c pAccessories : [
                SELECT Id, VTD1_Protocol_Device__c, VTD1_Accessory_Qty__c, VTD1_Accessory_Description__c
                FROM VTD1_Protocol_Accessories__c
                WHERE VTD1_Care_Plan_Template__c = :this.cas.VTD1_Study__c
                AND VTD1_isArchivedVersion__c = FALSE
                AND VTD1_Protocol_Device__c IN :this.newDevicesByProtocolId.keySet()
        ]) {
            this.newAccessories.add(new VTD1_Patient_Accessories__c(
                    VTD1_Accessory_Status__c = 'Pending',
                    VTD1_Protocol_Accessories__c = pAccessories.Id,
                    VTD1_Patient_Kit__c = this.newDevicesByProtocolId.get(pAccessories.VTD1_Protocol_Device__c).VTD1_Patient_Kit__c,
                    VTD1_Patient_Device__c = this.newDevicesByProtocolId.get(pAccessories.VTD1_Protocol_Device__c).Id,
                    VTD1_Patient_Delivery__c = this.newDevicesByProtocolId.get(pAccessories.VTD1_Protocol_Device__c).VTD1_Patient_Delivery__c,
                    VTD1_Case__c = this.cas.Id,
                    VTD1_Accessory_Qty__c = pAccessories.VTD1_Accessory_Qty__c,
                    VTD1_Accessory_Description__c = pAccessories.VTD1_Accessory_Description__c,
                    RecordTypeId = recTypeId
            ));
        }

        if (!this.newAccessories.isEmpty()) {
            insert this.newAccessories;
        }
    }

    private void updateDeliveries() {
        this.newDeliveries = [SELECT Id FROM VTD1_Order__c WHERE Id IN :this.newDeliveries ORDER BY VTD1_Protocol_Delivery_Order__c];

        updateDeliveriesAndCreateChildren();
        sortDeliveries();

        List<VTD1_Order__c> allDeliveries = new List<VTD1_Order__c>();
        allDeliveries.addAll(this.newDeliveries);
        allDeliveries.addAll(this.childDeliveries);
        update allDeliveries;
    }

    private void initiateSharingCreation() {
        update new Case(Id = this.cas.Id, VTD1_Create_Sharing__c = true);
    }

    @Future
    private static void initiateSharingCreationFuture(Id caseId) {
        update new Case(Id = caseId, VTD1_Create_Sharing__c = true);
    }

    private void updateDeliveriesAndCreateChildren() {
        Map<Id, VTD1_Order__c> deliveryMap = new Map<Id, VTD1_Order__c>(this.newDeliveries);

        Set<Id> impDeliveryIds = new Set<Id>();
        Set<Id> labDeliveryIds = new Set<Id>();
        Integer impDeliveryCounter = 0;
        Integer labDeliveryCounter = 0;
        Integer impReturnCounter = 0;
        Integer labReturnCounter = 0;

        for (VTD1_Patient_Kit__c kit : [
                SELECT Id, VTD1_Patient_Delivery__c, VTD1_Kit_Type__c, Name
                FROM VTD1_Patient_Kit__c
                WHERE Id IN :this.newKitsByProtocolId.values()
                ORDER BY VTD1_Delivery_Order__c
        ]) {
            VTD1_Order__c delivery = deliveryMap.get(kit.VTD1_Patient_Delivery__c);

            delivery.VTD1_isWelcomeToStudyPackage__c = kit.VTD1_Kit_Type__c == 'Welcome to Study Package';
            delivery.VTD1_containsDevice__c = kit.VTD1_Kit_Type__c == 'Connected Devices';
            delivery.VTD1_containsTablet__c = kit.VTD1_Kit_Type__c == 'Study Hub Tablet';
            if (delivery.VTD1_isWelcomeToStudyPackage__c) {
                delivery.VTD1_Kit_Type__c = 'Welcome to Study Package';
            } else if (delivery.VTD1_containsDevice__c) {
                delivery.VTD1_Kit_Type__c = 'Connected Devices';
            } else if (delivery.VTD1_containsTablet__c) {
                delivery.VTD1_Kit_Type__c = 'Study Hub Tablet';
            }
            if (kit.VTD1_Kit_Type__c == 'IMP') {
                Boolean isImpDeliveryProcessed = impDeliveryIds.contains(kit.VTD1_Patient_Delivery__c);
                if (!isImpDeliveryProcessed) {
                    impDeliveryIds.add(kit.VTD1_Patient_Delivery__c);
                }
                impDeliveryCounter = impDeliveryIds.size();
                delivery.VTD1_IMP_Delivery_Order__c = impDeliveryCounter;
                delivery.VTD1_CountIMPPatientDeliveryComma__c = impDeliveryCounter + ',';
                delivery.VTD1_isIMP__c = true;
                delivery.VTD1_Kit_Type__c = 'IMP/Lab';

                if (!isImpDeliveryProcessed && createReturnItemIMP(impDeliveryCounter)) {
                    impReturnCounter++;
                    this.childImpLabDeliveries.add(getReturnDelivery(delivery.Id, kit.VTD1_Kit_Type__c, impReturnCounter));
                    delivery.VTD1_ContainsReturn__c = true;
                }
            } else if (kit.VTD1_Kit_Type__c == 'Lab') {
                Boolean isLabDeliveryProcessed = labDeliveryIds.contains(kit.VTD1_Patient_Delivery__c);
                if (!isLabDeliveryProcessed) {
                    labDeliveryIds.add(kit.VTD1_Patient_Delivery__c);
                }
                labDeliveryCounter = labDeliveryIds.size();
                delivery.VTD1_Lab_Delivery_Order__c = labDeliveryCounter;
                delivery.VTD1_CountLabPatientDeliveryComma__c = labDeliveryCounter + ',';
                delivery.VTD1_isLab__c = true;
                delivery.VTD1_Kit_Type__c = 'IMP/Lab';

                if (!isLabDeliveryProcessed && createReturnItemLab(labDeliveryCounter)) {
                    labReturnCounter++;
                    this.childImpLabDeliveries.add(getReturnDelivery(delivery.Id, kit.VTD1_Kit_Type__c, labReturnCounter));
                    delivery.VTD1_ContainsReturn__c = true;
                }
            }

            if (kit.VTD1_Kit_Type__c == 'Connected Devices' || kit.VTD1_Kit_Type__c == 'Study Hub Tablet') {
                delivery.VTD1_Kit_Type__c = kit.VTD1_Kit_Type__c;
//                if (this.cas.VTD1_Study__r.VTD1_Return_Process__c == 'Packaging Materials Needed') {
//                    this.childDeliveries.add(getChildDelivery(delivery.Id, 'Package Delivery', kit));
//                    delivery.VTD1_ContainsPackage__c = true;
//                }
                this.childDeliveries.add(getReturnDelivery(delivery.Id, kit.VTD1_Kit_Type__c, null));
                delivery.VTD1_ContainsReturn__c = true;
            }
        }
        if (!this.childImpLabDeliveries.isEmpty()) {
            insert this.childImpLabDeliveries;

            Map<Id, VTD1_Order__c> parentDeliveryMap = new Map<Id, VTD1_Order__c>(this.newDeliveries);
            for (VTD1_Order__c returnDelivery : this.childImpLabDeliveries) {
                if (createPackageIMP(returnDelivery.VTR2_Return_Kit_Type__c) || createPackageLab(returnDelivery.VTR2_Return_Kit_Type__c)) {
                    this.childDeliveries.add(getPackageDelivery(returnDelivery.Id));
                    parentDeliveryMap.get(returnDelivery.VTD1_Patient_Delivery__c).VTD1_ContainsPackage__c = true;
                }
            }
        }

        if (!this.childDeliveries.isEmpty()) {
            insert this.childDeliveries;
            for (VTD1_Order__c packageDelivery : this.childDeliveries) {
                if (packageDelivery.RecordTypeId == VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_ORDER_PACKING_MATERIALS) {
                    this.packMatKits.add(getPatientKit(packageDelivery.Id));
                }
            }
            if (!this.packMatKits.isEmpty()) {
                insert this.packMatKits;
                List<VTD1_Order__c> packDelToUpdateList = new List <VTD1_Order__c>();
                for (VTD1_Order__c packageDelivery : this.childDeliveries) {
                    for (VTD1_Patient_Kit__c pMKit : this.packMatKits) {
                        if (packageDelivery.Id == pMKit.VTD1_Patient_Delivery__c) {
                            packageDelivery.VTD1_Patient_Kit__c = pMKit.Id;
                            packDelToUpdateList.add(packageDelivery);
                        }
                    }
                }
                if (!packDelToUpdateList.isEmpty()) {
                    update packDelToUpdateList;
                }
            }
        }
        if (!this.childImpLabDeliveries.isEmpty()) {
            this.childDeliveries.addAll(this.childImpLabDeliveries);
        }
    }

    private Boolean createPackageIMP(String kitType) {
        return this.cas.VTD1_Study__r.VTD1_Return_Process__c == 'Packaging Materials Needed' &&
                kitType == 'IMP';
    }

    private Boolean createReturnItemIMP(Integer counter) {
        return this.returnNumbersIMP.contains('' + counter);
    }

    private Boolean createPackageLab(String kitType) {
        return this.cas.VTD1_Study__r.VTD1_Lab_Return_Process__c == 'Packaging Materials Needed' &&
                kitType == 'Lab';
    }

    private Boolean createReturnItemLab(Integer counter) {
        return this.returnNumbersLab.contains('' + counter);
    }

//    private VTD1_Order__c getChildDelivery(Id deliveryId, String name, VTD1_Patient_Kit__c kit) {
//        Map<Id, VTD1_Order__c> deliveryMap = new Map<Id, VTD1_Order__c>(this.newDeliveries);
//        Boolean isReturn = name == 'Return';
//        Boolean isPackage = name == 'Package Delivery';
//        return new VTD1_Order__c(
//                Name = isReturn? name+' for '+kit.VTD1_Kit_Type__c : name,
//                VTD1_Case__c = this.cas.Id,
//                VTD1_Patient_Delivery__c = deliveryId,
//                VTD1_Status__c = 'Not Started',
//                VTD1_isPackageDelivery__c = isPackage,
//                VTD1_isReturnDelivery__c = isReturn,
//                RecordTypeId = isPackage? VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_ORDER_PACKING_MATERIALS : VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_ORDER_AD_HOC_RETURN,
//                //VTD1_Kit_Type__c = name == 'Package Delivery' ? deliveryMap.get(deliveryId).VTD1_Kit_Type__c : null
//                VTD1_Kit_Type__c = deliveryMap.get(deliveryId).VTD1_Kit_Type__c,
//                VTR2_Return_Kit_Type__c = kit.VTD1_Kit_Type__c
//                //VTD1_Patient_Kit__c = isReturn? kit.Id : null
//                //VTR2_For_Return_Kit__c = isPackage? kit.Id : null
//        );
//    }

    private VTD1_Order__c getReturnDelivery(Id deliveryId, String kitType, Integer counter) {
        //Map<Id, VTD1_Order__c> parentDeliveryMap = new Map<Id, VTD1_Order__c>(this.newDeliveries);
        VTD1_Order__c parentDelivery = new Map<Id, VTD1_Order__c>(this.newDeliveries).get(deliveryId);
        return new VTD1_Order__c(
                Name = 'Return of ' + kitType + (counter != null ? ' #' + counter : ''),
                VTD1_Case__c = this.cas.Id,
                VTD1_Patient_Delivery__c = deliveryId,
                VTD1_Status__c = 'Not Started',
                VTD1_isReturnDelivery__c = true,
                RecordTypeId = VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_ORDER_AD_HOC_RETURN,
                VTD1_Kit_Type__c = parentDelivery.VTD1_Kit_Type__c,
                //VTR2_Destination_Type__c = 'Patient',
                VTR2_Return_Kit_Type__c = kitType,
                VT_R3_ReturnNumber__c = counter != null ? ' #' + counter : ''
        );
    }

    private VTD1_Order__c getPackageDelivery(Id deliveryId) {
        //Map<Id, VTD1_Order__c> returnDeliveryMap = new Map<Id, VTD1_Order__c>(this.childImpLabDeliveries);
        VTD1_Order__c returnDelivery = new Map<Id, VTD1_Order__c>(this.childImpLabDeliveries).get(deliveryId);
        return new VTD1_Order__c(
                Name = 'Package Delivery',
                VTD1_Case__c = this.cas.Id,
                VTD1_Patient_Delivery__c = returnDelivery.VTD1_Patient_Delivery__c,
                VTD1_Status__c = 'Not Started',
                VTD1_isPackageDelivery__c = true,
                RecordTypeId = VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_ORDER_PACKING_MATERIALS,
                VTD1_Kit_Type__c = returnDelivery.VTD1_Kit_Type__c,
                //VTR2_Destination_Type__c = 'Patient',
                VTR2_For_Return_Kit__c = deliveryId
        );
    }

    private VTD1_Patient_Kit__c getPatientKit(Id packageDeliveryId) {
        return new VTD1_Patient_Kit__c(
                Name = 'Packaging Materials',
                VTD1_Patient_Delivery__c = packageDeliveryId,
                VTD1_Patient_Kits__c = packageDeliveryId,
                RecordTypeId = VT_R4_ConstantsHelper_KitsOrders.RECORD_TYPE_ID_PATIENT_KIT_PACKING_MATERIALS,
                VTD1_Status__c = 'Pending',
                VTD1_Case__c = this.cas.Id,
                VTD1_Kit_Contains_All_Contents__c = true
        );
    }

    private void sortDeliveries() {
        Integer idx = 1;

        Map<Id, List<VTD1_Order__c>> packageMap = new Map<Id, List<VTD1_Order__c>>();
        Map<Id, List<VTD1_Order__c>> returnMap = new Map<Id, List<VTD1_Order__c>>();
        for (VTD1_Order__c item : this.childDeliveries) {
            addToChildMap(item.VTD1_isPackageDelivery__c ? packageMap : returnMap, item);
        }

        for (VTD1_Order__c delivery : this.newDeliveries) {
            delivery.VTD1_Delivery_Order__c = idx++;

            if (packageMap.containsKey(delivery.Id)) {
                for (VTD1_Order__c child : packageMap.get(delivery.Id)) {
                    child.VTD1_Delivery_Order__c = idx++;
                }
            }

            if (returnMap.containsKey(delivery.Id)) {
                for (VTD1_Order__c child : returnMap.get(delivery.Id)) {
                    child.VTD1_Delivery_Order__c = idx++;
                }
            }
        }
    }

    private void addToChildMap(Map<Id, List<VTD1_Order__c>> childMap, VTD1_Order__c item) {
        if (!childMap.containsKey(item.VTD1_Patient_Delivery__c)) {
            childMap.put(item.VTD1_Patient_Delivery__c, new List<VTD1_Order__c>());
        }
        childMap.get(item.VTD1_Patient_Delivery__c).add(item);
    }
    */
}