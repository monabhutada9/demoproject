@IsTest
private class VT_R3_CasePatientFlagHandlerTest {

    @TestSetup
    static void setup() {
        Test.startTest();
        Case caseRecord = new Case(
                VTD1_Randomized_ID__c = 't',
                Status = 'Active/Randomized',
                VTD1_End_of_study_visit_completed__c = true
        );
        insert caseRecord;

        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c(VTD1_VisitType__c = 'Health Care Professional (HCP)', VTR2_Visit_Participants__c = 'Patient Guide');
        insert protocolVisit;

        List<VTD1_Actual_Visit__c> actualVisitsToInsert = new List<VTD1_Actual_Visit__c>();
        VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c(VTD1_Case__c = caseRecord.Id, VTD1_Status__c = 'Requested', VTD1_Protocol_Visit__c = protocolVisit.Id, VTD1_Patient_Removed_Flag__c = false);
        VTD1_Actual_Visit__c actualVisitUnscheduled = new VTD1_Actual_Visit__c(Unscheduled_Visits__c = caseRecord.Id, VTD1_Status__c = 'Requested', VTD1_Protocol_Visit__c = protocolVisit.Id, VTD1_Patient_Removed_Flag__c = false);

        actualVisitsToInsert.add(actualVisit);
        actualVisitsToInsert.add(actualVisitUnscheduled);
        insert actualVisitsToInsert;
        Test.stopTest();
    }

    @IsTest
    static void testPatientRemovalFlagActualVisit() {
        Case caseRecord = [SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        caseRecord.Status = 'Dropped';
        update caseRecord;
        Test.stopTest();
    }
}