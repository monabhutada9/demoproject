/**
 * Created by Alexander Komarov on 18.06.2019.
 */

@RestResource(UrlMapping='/Patient/Profile/Phones/SMS')
global with sharing class VT_R3_RestPatientPhoneSMS {
	static VT_R3_RestHelper helper = new VT_R3_RestHelper();
	static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());

	@HttpPost
	global static String updatePhoneOpt() {
		RestResponse response = RestContext.response;
		RestRequest request = RestContext.request;

		String temp = request.requestBody.toString();
		Id phoneId = temp.remove('"').remove(' ');

		String action = request.params.get('action');

		if (!helper.isIdCorrectType(phoneId, 'VT_D1_Phone__c')) {
			response.statusCode = 400;
			return helper.forAnswerForIncorrectInput();
		}
		VT_D1_Phone__c phone = [SELECT Id, PhoneNumber__c, Type__c, VTR2_OptInFlag__c, VTR2_SMS_Opt_In_Status__c, IsPrimaryForPhone__c FROM VT_D1_Phone__c WHERE Id = :phoneId LIMIT 1];
		try {
			if (action.equalsIgnoreCase('optIn')) {
				if (!(phone.VTR2_SMS_Opt_In_Status__c == null)) {
					if(!(phone.VTR2_SMS_Opt_In_Status__c.equals('Inactive'))) {
						response.statusCode = 417;
						return 'Phone should be in optIn-status "Inactive" or with null status';
					}
				}

				VT_D1_PatientFullProfileController.optInRemote(phoneId);
			} else if (action.equalsIgnoreCase('optOut')) {
				if (!phone.VTR2_SMS_Opt_In_Status__c.equals('Active')) {
					response.statusCode = 417;
					return 'Phone should be in optIn-status "Active"';
				}
				VT_D1_PatientFullProfileController.optOutRemote(phoneId);
			} else if (action.equalsIgnoreCase('resendOptIn')) {
				if (!(phone.VTR2_SMS_Opt_In_Status__c.equals('Pending'))) {
					response.statusCode = 417;
					return 'Phone should be in optIn-status "Pending"';
				}
				VT_D1_PatientFullProfileController.resendOptInRemote(phoneId);
			} else if (action.equalsIgnoreCase('sendContacts')) {
				if (!(phone.VTR2_SMS_Opt_In_Status__c.equals('Active'))) {
					response.statusCode = 417;
					return 'Phone should be in optIn-status "Active"';
				}
				VT_D1_PatientFullProfileController.sendContactsRemote(phoneId);
			} else {
				if (action != null) {
					response.statusCode = 400;
					return 'Please, specify one of these actions: optIn, optOut, resendOptIn, sendContacts. Got action: "' + action + '"';
				} else {
					response.statusCode = 417;
					return 'Impossible to ' + action + ' on phone with such status ' + phone.VTR2_SMS_Opt_In_Status__c + ' and ID - ' + phoneId;
				}
			}
		} catch (Exception e) {
			response.statusCode = 500;
			return 'SERVER FAILURE. ' + e.getMessage() + ' ' + e.getStackTraceString();
		}

		VT_D1_Phone__c toReturn = [SELECT Id, PhoneNumber__c, Type__c, VTR2_OptInFlag__c, VTR2_SMS_Opt_In_Status__c, IsPrimaryForPhone__c FROM VT_D1_Phone__c WHERE Id = :phoneId LIMIT 1];

		Phone result = new Phone();
		result.id = toReturn.Id;
		result.phoneNumber = toReturn.PhoneNumber__c;
		result.isPrimary = toReturn.IsPrimaryForPhone__c;
		result.optInStatus = toReturn.VTR2_SMS_Opt_In_Status__c;
		result.optInFlag = toReturn.VTR2_OptInFlag__c;
		result.phoneType = toReturn.Type__c;

		return JSON.serialize(result);
	}

	private class Phone {
		public String id;
		public String phoneNumber;
		public String phoneType;
		public Boolean isPrimary;
		public Boolean optInFlag;
		public String optInStatus;
	}

}