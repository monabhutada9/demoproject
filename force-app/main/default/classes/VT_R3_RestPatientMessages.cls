/**
 * Created by Alexander Komarov on 23.04.2019.
 */

@RestResource(UrlMapping='/Patient/Messages')
global with sharing class VT_R3_RestPatientMessages {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();
    static Integer timeOffset = UserInfo.getTimeZone().getOffset(System.now());

    @HttpGet
    global static String getPosts() {

        List<VT_D1_CommunityChat.PostObj> postsListResult = new List<VT_D1_CommunityChat.PostObj>();

        List<PatientMessage> result = new List<PatientMessage>();
        postsListResult = VT_D1_CommunityChat.getPatientPosts();

        result = getLastCommentsAndFormResult(postsListResult);


        MessagesResponse messagesResponse = new MessagesResponse();
        messagesResponse.chatsList = result;
//        messagesResponse.unreadInfo = new Unread();

        cr.buildResponse(messagesResponse);
//        cr.buildResponse(result);
        return JSON.serialize(cr, true);

    }

    private static List<PatientMessage> getLastCommentsAndFormResult(List<VT_D1_CommunityChat.PostObj> posts) {
        List<PatientMessage> result = new List<PatientMessage>();
        for (VT_D1_CommunityChat.PostObj post : posts) {
            Boolean haveComments = false;
            ConnectApi.Comment lastComment;
            ConnectApi.User commentAuthor;
            if (!Test.isRunningTest()) {
                ConnectApi.CommentPage commentsPage = ConnectApi.ChatterFeeds.getCommentsForFeedElement(null, post.id, null, 1);
                List<Object> commentsItems = commentsPage.items;
                if (commentsItems.size() > 0) {
                    lastComment = commentsPage.items[0];
                    commentAuthor = (ConnectApi.User) lastComment.user;
                    haveComments = true;
                }
            }
            result.add(new PatientMessage(post.id, post.subject, haveComments ? commentAuthor.displayName : post.userName, haveComments ? lastComment.createdDate : post.createdDateTime, haveComments ? commentAuthor.photo.largePhotoUrl : post.userImage, post.isRead, haveComments ? lastComment.body.text : post.bodyText));
        }
        System.debug(result);
        return result;
    }

    public class PatientMessage {
        public String id;
        public String title;
        public String lastMessageAuthorName;
        public Long lastMessageData;
        public String photo;
        public Boolean read;
        public String textLastMessage;

        public PatientMessage(String i, String t, String n, Datetime d, String p, Boolean r, String text) {
            this.id = i;
            this.title = t;
            this.lastMessageAuthorName = n;
            this.lastMessageData = d.getTime();
            this.photo = p.contains('default_profile') ? '' : p;
            this.read = r;
            this.textLastMessage = text;
        }
    }

    private class MessagesResponse {
        private List<PatientMessage> chatsList; //broadcasts //liveChatHistories
        private VT_R3_RestHelper.Unread unreadInfo = new VT_R3_RestHelper.Unread();
    }


}