/**
 * Created by Alexander Komarov on 18.06.2019.
 */

@RestResource(UrlMapping='/Patient/Profile/Photo/*')
global with sharing class VT_R3_RestPatientProfilePhoto {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();


    @HttpGet
    global static String getPhoto() {
        RestRequest request = RestContext.request;

        String temp = request.requestURI.substringAfterLast('/Photo/');

        Id userToGetPhoto = UserInfo.getUserId();
        if (temp.indexOf('Patient') != -1) {
            userToGetPhoto = VT_D1_PatientCaregiverBound.findPatientOfCaregiver(UserInfo.getUserId());
            System.debug('got id ' + userToGetPhoto);
        }
        String result = ConnectApi.UserProfiles.getPhoto(null, userToGetPhoto).fullEmailPhotoUrl;
        result = result.contains('default_profile') ? '' : result;
        cr.buildResponse(result);
        return JSON.serialize(cr, true);

    }

    @HttpPost
    global static String setPhoto() {
        RestResponse response = RestContext.response;
        RestRequest request = RestContext.request;

        String base64Photo = request.requestBody.toString();

        String temp = request.requestURI.substringAfterLast('/Photo/');
        Id userToChangePhoto = UserInfo.getUserId();
        if (temp.indexOf('Patient') != -1) {
            userToChangePhoto = VT_D1_PatientCaregiverBound.findPatientOfCaregiver(UserInfo.getUserId());
        }
        try {
            ConnectApi.UserProfiles.deletePhoto(null, userToChangePhoto);
        } catch (Exception e) {
            System.debug('Photo delete failed');
        }
        ConnectApi.Photo pp;
        try {
            Blob b = EncodingUtil.base64Decode(base64Photo);

            ConnectApi.BinaryInput file = new ConnectApi.BinaryInput(b, 'image/jpg', 'User' + userToChangePhoto + 'Photo.jpg');
            pp = ConnectApi.UserProfiles.setPhoto(null, userToChangePhoto, file);
        } catch (Exception e) {
            System.debug('EXCEPTION ' + e);
            response.statusCode = 500;
            cr.buildResponse('FAILURE. ' + e.getStackTraceString() + ' ' + e.getMessage());
            return JSON.serialize(cr, true);
        }

        cr.buildResponse('SUCCESS', System.Label.VTR3_Success_Profile_Picture_Upload);
        return JSON.serialize(cr, true);
    }
}