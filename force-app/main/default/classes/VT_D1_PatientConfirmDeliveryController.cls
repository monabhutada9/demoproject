public class VT_D1_PatientConfirmDeliveryController {
    public class DeviceItem {
        public String deviceName;
        public String ancillaries;
        public Double quantity;
    }
    
    public class KitItem {
        public VTD1_Patient_Kit__c kit;
        public List<DeviceItem> deviceItems;
        public String kitContainsAllContents;        
        public String kitContentsDamaged;
        public Boolean kitNotReceived;
        public Boolean IfTemperatureControlled;
        public Double receivedTemperature;
    }

    public class DeliveryDescriptionWithKitItems {
        public List<KitItem> kitItems;
        public VTD1_Order__c delivery;
        public Integer kitItemsSize;
        public String deliveryKitContainsAllContents;        
        public String deliveryKitContentsDamaged;
        public Boolean isAdHocDelivery;
    }
    
    @AuraEnabled 
    public static String getKitContents(String deliveryId) {
        try{
            List<KitItem> kitItems = new List<KitItem>();   
            
            String adHocDeliveryRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'VTD1_Ad_hoc_Packaging_Materials' AND SobjectType = 'VTD1_Order__c'].Id;
            VTD1_Order__c delivery = [SELECT VTD1_CommentsNotes__c, VTD1_ExpectedDeliveryDateTime__c,
                                             VTD1_Kit_Contains_All_Contents__c, VTD1_Kit_Contents_Damaged__c, VTD1_isPackageDelivery__c,
                                             RecordTypeId, VTR2_For_Return_Kit__c, VTR2_For_Return_Kit__r.VTD1_PatientHasPackagingMaterials__c
                                      FROM VTD1_Order__c WHERE Id =: deliveryId];
            
            String deliveryKitContainsAllContents;
            if (delivery.VTD1_Kit_Contains_All_Contents__c){
                deliveryKitContainsAllContents = 'true';
            } else {    
                deliveryKitContainsAllContents = 'false';
            }
            String deliveryKitContentsDamaged;
            if (delivery.VTD1_Kit_Contents_Damaged__c){
                deliveryKitContentsDamaged = 'true';
            } else {    
                deliveryKitContentsDamaged = 'false';
            }
            Boolean isAdHocDelivery = false;
            if (delivery.RecordTypeId == adHocDeliveryRecordTypeId){
                isAdHocDelivery = true;
            }

            List<VTD1_Patient_Kit__c> kits = [SELECT Id, VTD1_Patient_Delivery__c, VTD1_Kit_Type__c, VTD1_Kit_Name__c,
                                                VTD1_Kit_Contains_All_Contents__c, VTD1_Kit_Contents_Damaged__c, VTD1_Kit_Not_Received__c,
                                                VTR2_IfTemperatureControlled__c, VTR2_ReceivedTemperature__c,
                                                VTD1_Protocol_Kit__c, VTD1_Protocol_Kit__r.Name, Name
                                              FROM VTD1_Patient_Kit__c 
                                              WHERE VTD1_Patient_Delivery__c =: delivery.Id];
            VT_D1_TranslateHelper.translate(kits);
            for (VTD1_Patient_Kit__c kit : kits) {
                KitItem kitItem = new KitItem();
                kitItem.kit = kit;
                if (kit.VTD1_Kit_Contains_All_Contents__c){
                    kitItem.kitContainsAllContents = 'true';
                } else {    
                    kitItem.kitContainsAllContents = 'false';
                }
                if (kit.VTD1_Kit_Contents_Damaged__c){
                    kitItem.kitContentsDamaged = 'true';
                } else {    
                    kitItem.kitContentsDamaged = 'false';
                }
                kitItem.kitNotReceived = kit.VTD1_Kit_Not_Received__c;
                kitItem.IfTemperatureControlled = kit.VTR2_IfTemperatureControlled__c;
                kitItem.receivedTemperature = kit.VTR2_ReceivedTemperature__c;
                kitItems.add(kitItem);
            }
            /*                                  
            AggregateResult[] groupedResults = [SELECT COUNT(Id) quantity, VTD1_StudyDevice__c, VTD1_SerialNumber__c
                                    FROM HealthCloudGA__EhrDevice__c WHERE VTD1_Order__c =: deliveryId
                                    GROUP BY VTD1_StudyDevice__c, VTD1_SerialNumber__c];
            
            List<String> studyDeviceIdLst = new List<String>();
            for (AggregateResult ar : groupedResults) {
                studyDeviceIdLst.add(String.valueOf(ar.get('VTD1_StudyDevice__c')));
            }
            
            Map<String, String> studyDeviceIdToModelNameMap =  new Map<String, String>(); 
            VTD1_StudyDevice__c[] studyDeviceLst = [SELECT Id, VTD1_Device__r.VTD1_ModelName__c FROM VTD1_StudyDevice__c WHERE Id IN: studyDeviceIdLst];
            for (VTD1_StudyDevice__c studyDevice : studyDeviceLst) {
                studyDeviceIdToModelNameMap.put(studyDevice.Id, studyDevice.VTD1_Device__r.VTD1_ModelName__c);    
            }
         
            for (AggregateResult ar : groupedResults) {
                KitItem kitItem = new KitItem();
                if (ar.get('VTD1_SerialNumber__c') == null) {
                    kitItem.deviceName = studyDeviceIdToModelNameMap.get(String.valueOf(ar.get('VTD1_StudyDevice__c')));
                } else {
                    kitItem.deviceName = studyDeviceIdToModelNameMap.get(String.valueOf(ar.get('VTD1_StudyDevice__c'))) + 
                                                              ' (' + String.valueOf(ar.get('VTD1_SerialNumber__c')) + ')';
                }
                kitItem.ancillaries = 'Charger';
                kitItem.quantity = (Double) Integer.valueOf(ar.get('quantity'));
                kitItems.add(kitItem); 
            }
            */                 
            DeliveryDescriptionWithKitItems deliveryDescriptionWithKitItems = new DeliveryDescriptionWithKitItems();
            deliveryDescriptionWithKitItems.kitItems = kitItems;     
            deliveryDescriptionWithKitItems.delivery = delivery;
            deliveryDescriptionWithKitItems.kitItemsSize = kitItems.size();
            deliveryDescriptionWithKitItems.deliveryKitContainsAllContents = deliveryKitContainsAllContents;
            deliveryDescriptionWithKitItems.deliveryKitContentsDamaged = deliveryKitContentsDamaged;
            deliveryDescriptionWithKitItems.isAdHocDelivery = isAdHocDelivery;

            return JSON.serialize(deliveryDescriptionWithKitItems);
        }catch (Exception e){
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }    
    }  
    
    @AuraEnabled 
    public static void updateDelivery(String deliveryString, String kitItemsString, Boolean updateKitContainsAllContentsAndDamagedOnDelivery,
                                      String deliveryKitContainsAllContents, String deliveryKitContentsDamaged ) {
        VTD1_Order__c delivery = (VTD1_Order__c) JSON.deserialize(deliveryString, VTD1_Order__c.class);
        List<KitItem> kitItems = (List<KitItem>) JSON.deserialize(kitItemsString, List<KitItem>.class);
        System.debug('updateDelivery kitItems: '+kitItems);

        List<VTD1_Patient_Kit__c> kits = [SELECT Id, VTD1_Kit_Contains_All_Contents__c, VTD1_Kit_Contents_Damaged__c, VTD1_Kit_Not_Received__c, VTR2_ReceivedTemperature__c
            FROM VTD1_Patient_Kit__c
            WHERE VTD1_Patient_Delivery__c =: delivery.Id];
        Map<String, VTD1_Patient_Kit__c> kitIdToKitMap = new Map<String, VTD1_Patient_Kit__c>();
        for (VTD1_Patient_Kit__c kit : kits) {
            kitIdToKitMap.put(kit.Id, kit);    
        } 

        for (KitItem kitItem : kitItems) {
            VTD1_Patient_Kit__c kitToUpdate = kitItem.kit != null ? kitIdToKitMap.get(kitItem.kit.Id) : null;
            if( kitToUpdate != null ){
                kitToUpdate.VTD1_Comments__c = delivery.VTD1_CommentsNotes__c;

                if (kitItem.kitContainsAllContents == 'true'){
                    kitToUpdate.VTD1_Kit_Contains_All_Contents__c = true;
                } else {    
                    kitToUpdate.VTD1_Kit_Contains_All_Contents__c = false;
                }
                if (kitItem.kitContentsDamaged == 'true'){
                    kitToUpdate.VTD1_Kit_Contents_Damaged__c = true;
                } else {    
                    kitToUpdate.VTD1_Kit_Contents_Damaged__c = false;
                }
                kitToUpdate.VTD1_Kit_Not_Received__c = kitItem.kitNotReceived;
                if (kitItem.IfTemperatureControlled) {
                    kitToUpdate.VTR2_ReceivedTemperature__c = kitItem.receivedTemperature;
                }

                kitIdToKitMap.put(kitToUpdate.Id, kitToUpdate);
            }
        }

        List<VTD1_Patient_Kit__c> kitsToUpdate = kitIdToKitMap.values();

        List<VTD1_Order__c> deliveriesToUpdate = new List<VTD1_Order__c>();
        if (updateKitContainsAllContentsAndDamagedOnDelivery) {
            System.debug('Package delivery: '+delivery.Id);
            System.debug('For Return Kit: '+delivery.VTR2_For_Return_Kit__c);

            if (deliveryKitContainsAllContents == 'true'){
                delivery.VTD1_Kit_Contains_All_Contents__c = true;
            } else {    
                delivery.VTD1_Kit_Contains_All_Contents__c = false;
            }
            if (deliveryKitContentsDamaged == 'true'){
                delivery.VTD1_Kit_Contents_Damaged__c = true;
            } else {    
                delivery.VTD1_Kit_Contents_Damaged__c = false;
            }

            if (delivery.VTD1_Kit_Contains_All_Contents__c && !delivery.VTD1_Kit_Contents_Damaged__c) {
                if (delivery.VTR2_For_Return_Kit__c!=null) {
                    delivery.VTR2_For_Return_Kit__r.VTD1_PatientHasPackagingMaterials__c = true;
                    deliveriesToUpdate.add(delivery.VTR2_For_Return_Kit__r);
                }
            }
        }
        deliveriesToUpdate.add(delivery);

        update kitsToUpdate;
        update deliveriesToUpdate;

        Task [] tasks = [SELECT Status FROM Task WHERE OwnerId=: UserInfo.getUserId() AND WhatId=:delivery.Id];
        if (tasks.size() > 0) {
            for (Task task:tasks) {
                task.Status = 'Completed';
            }
            update tasks;
        }
    }
}