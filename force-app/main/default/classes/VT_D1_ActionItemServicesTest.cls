@IsTest
public with sharing class VT_D1_ActionItemServicesTest {
    
    public static void testBehavior() {
        User u;
        Test.startTest();
        Case cas;
        List<Case> casesList = [SELECT VTD1_PI_user__c, VTD1_Primary_PG__c FROM Case];
        if(casesList.size() > 0){
            cas = casesList.get(0);
            String userId = cas.VTD1_PI_user__c;
            u = [SELECT Id, VTD1_QId__c FROM User WHERE Id =: userId];
        }

        String studyTeamMemberId;
        List<Study_Team_Member__c> studyTeamMemberList = [SELECT Id, RecordTypeId FROM Study_Team_Member__c
        WHERE  RecordTypeId =: VT_R4_ConstantsHelper_ProfilesSTM.RECORD_TYPE_ID_STUDY_TEAM_MEMBER_PI];

        if(studyTeamMemberList.size() > 0){
            studyTeamMemberId = studyTeamMemberList.get(0).Id;
        }

        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id, Name FROM HealthCloudGA__CarePlanTemplate__c];
        Virtual_Site__c virtualSite = new Virtual_Site__c();

        virtualSite.VTD1_Study__c = study.Id;
        virtualSite.VTD1_Study_Site_Number__c = '00001';
        virtualSite.VTD1_Study_Team_Member__c = studyTeamMemberId;
        insert virtualSite;


        Virtual_Site__c virtualSite1 = VT_D1_ActionItemServices.getVSiteByProtocolSiteId(virtualSite.Id);
        String virtualSiteId = VT_D1_ActionItemServices.getVSiteIdBySiteNumber(virtualSite.VTD1_Study_Site_Number__c);
        Virtual_Site__c virtualSite2 = VT_D1_ActionItemServices.getVSiteBySiteNumber(virtualSite.VTD1_Study_Site_Number__c);

        VT_D1_ActionItemServices.AuthorisedAssignees authorisedAssignees  = VT_D1_ActionItemServices.retrieveAuthorisedAssignees(u.Id, virtualSite.VTD1_Study_Site_Number__c);

        List< VT_D1_ActionItemServices.AuthorisedAssigneeWrapper> assigneeList = VT_D1_ActionItemServices.getAssignees(virtualSite, u.VTD1_QId__c);

        VT_D1_ActionItemServices.ActionItem actionItem = new VT_D1_ActionItemServices.ActionItem();
        String alertId = actionItem.AlertId;
        String actionItemId = actionItem.ActionItemId;
        String referenceId = actionItem.ReferenceId;
        String title = actionItem.Title;
        String category = actionItem.Category;
        String createdById = actionItem.CreatedById;
        String createdByFirstName = actionItem.CreatedByFirstName;
        String createdByLastName = actionItem.CreatedByLastName;
        String assignedTo = actionItem.AssignedTo;
        String aiSeverity = actionItem.AISeverity;
        String comments = actionItem.Comments;
        String dueDate = actionItem.DueDate;
        String createdDate = actionItem.CreatedDate;
        String author = actionItem.Author;
        String piFirstName = actionItem.PIFirstName;
        String piLastName = actionItem.PILastName;
        String assignedCPMFirstName = actionItem.AssignedCPMFirstName;
        String assignedCPMLastName = actionItem.AssignedCPMLastName;
        String siteName = actionItem.SiteName;
        String status = actionItem.Status;
        String resolutionComments = actionItem.ResolutionComments;
        String resolutionDate = actionItem.ResolutionDate;
        String updatedDate = actionItem.UpdatedDate;
        String assignedToFirstName = actionItem.AssignedToFirstName;
        String assignedToLastName = actionItem.AssignedToLastName;

        VT_D1_ActionItemServices.ActionItem actionItem1 = new VT_D1_ActionItemServices.ActionItem('AlertId', null);

        String actionItemString = VT_D1_ActionItemServices.createActionItem(virtualSite.Id, 'AlertId', 'Minor', '2018-11-23T05:00:00Z', 'ActionRequired', cas.VTD1_Primary_PG__c, 'ReferenceId', u.VTD1_QId__c);
        VT_D1_ActionItemServices.retrieveListOfActionItem(null, virtualSite.VTD1_Study_Site_Number__c, null, null);
        Test.stopTest();
    }

    public static void getDateFromStringTest(){
        Date dateExpected = Date.newInstance(2018, 10, 08);
        Date dateActual =  VT_D1_ActionItemServices.getDateFromString('2018-10-08');
        System.assertEquals(dateExpected, dateActual);
    }

    public static void createActionItemTest(){
        User u;
        Test.startTest();
        Case cas;
        List<Case> casesList = [SELECT VTD1_PI_user__c, VTD1_Primary_PG__c FROM Case];
        if(casesList.size() > 0){
            cas = casesList.get(0);
            String userId = cas.VTD1_PI_user__c;
            u = [SELECT Id, VTD1_QId__c FROM User WHERE Id =: userId];
        }

        String studyTeamMemberId;
        List<Study_Team_Member__c> studyTeamMemberList = [SELECT Id, RecordTypeId FROM Study_Team_Member__c
        WHERE  RecordTypeId =: VT_R4_ConstantsHelper_ProfilesSTM.RECORD_TYPE_ID_STUDY_TEAM_MEMBER_PI];

        if(studyTeamMemberList.size() > 0){
            studyTeamMemberId = studyTeamMemberList.get(0).Id;
        }

        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id, Name FROM HealthCloudGA__CarePlanTemplate__c];
        Virtual_Site__c virtualSite = new Virtual_Site__c();

        virtualSite.VTD1_Study__c = study.Id;
        virtualSite.VTD1_Study_Site_Number__c = '00001';
        virtualSite.VTD1_Study_Team_Member__c = studyTeamMemberId;
        insert virtualSite;

        String actionItemString = VT_D1_ActionItemServices.createActionItem(null, 'AlertId', 'Minor', '2018-11-23T05:00:00Z', 'ActionRequired', cas.VTD1_Primary_PG__c, 'ReferenceId', u.VTD1_QId__c);
        System.debug(actionItemString);
        VT_D1_ActionItemServices.retrieveListOfActionItem('', virtualSite.VTD1_Study_Site_Number__c, '', '');
        Test.stopTest();
    }

    public static void getVSiteByProtocolSiteIdTest(){
        Test.startTest();
            Virtual_Site__c virtualSite = VT_D1_ActionItemServices.getVSiteByProtocolSiteId(null);
        Test.stopTest();

        System.assertEquals(null, virtualSite);
    }

    public static void retrieveListOfActionItemTest(){

        String studyTeamMemberId;
        List<Study_Team_Member__c> studyTeamMemberList = [SELECT Id, RecordTypeId FROM Study_Team_Member__c
        WHERE  RecordTypeId =: VT_R4_ConstantsHelper_ProfilesSTM.RECORD_TYPE_ID_STUDY_TEAM_MEMBER_PI];

        if(studyTeamMemberList.size() > 0){
            studyTeamMemberId = studyTeamMemberList.get(0).Id;
        }

        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id, Name FROM HealthCloudGA__CarePlanTemplate__c];
        Virtual_Site__c virtualSite = new Virtual_Site__c();
//        virtualSite.Name = 'testName';
        virtualSite.VTD1_Study__c = study.Id;
        virtualSite.VTD1_Study_Site_Number__c = '00001';
        virtualSite.VTD1_Study_Team_Member__c = studyTeamMemberId;
        insert virtualSite;

        User pg = [SELECT Id FROM User WHERE Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PG_PROFILE_NAME AND FirstName LIKE 'PrimaryPGUser%' AND IsActive = TRUE LIMIT 1];
        User pi = [SELECT Id FROM User WHERE Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME AND FirstName LIKE 'PIUser%' AND IsActive = TRUE LIMIT 1];

        VTD1_Action_Item__c actionItem = new VTD1_Action_Item__c(
                VTD1_Study__c = study.Id,
                VTD1_SiteId__c = virtualSite.Id,
                VTD2_Assigned_To__c = pg.Id,
                VTD1_PI_Name__c = pi.Id,
                OwnerId = pi.Id,
                VTD1_Author_First_Name__c = 'testFirstName',
                VTD1_Author_Last_Name__c = 'testLastName',
                VTD1_Due_Date__c = Date.newInstance(2018, 10, 08),
                VTD1_Resolution_Date__c = Date.newInstance(2018, 10, 08),
                VTD1_ReferenceId__c = 'test',
                VTD1_Status_Description__c = 'test'
        );
        insert actionItem;

        Test.startTest();
        VT_D1_ActionItemServices.ListOfActionItemDetailsWrapper detailsWrapper = VT_D1_ActionItemServices.retrieveListOfActionItem(null, '00001', 'test', 'test');
        Test.stopTest();
    }
}