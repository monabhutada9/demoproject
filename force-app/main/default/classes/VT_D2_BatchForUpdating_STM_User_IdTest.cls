/**
* @author: Michael Salamakha
* @date: 30-Jan-19
* @description:
**/

@IsTest
public with sharing class VT_D2_BatchForUpdating_STM_User_IdTest {

    public static void testBatchBehavior() {
        Test.startTest();
        VT_D2_BatchForUpdating_STM_User_Id batch = new VT_D2_BatchForUpdating_STM_User_Id();
        Database.executeBatch(batch);
        Test.stopTest();
    }
}