/**
 * Created by user on 16.07.2018.
 */

@IsTest
public class VT_D1_TestDocuSignStatus {
    @testSetup
    static void setup() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);

        //HealthCloudGA__CandidatePatient__c candidatePatient = VT_D1_TestUtils.createPatientCandidate('Mike', 'Birn', 'mikebirn@test.com', 'mikebirn@test.com', 'mikebirn', 'Converted', study.Id, 'PN', '11-11-11', 'US', 'NY');
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }
    @isTest
    public static void testStatusComplete() {
        ContentVersion contentVersion = new ContentVersion(
                Title = 'Test', PathOnClient = 'Test.txt', VersionData = Blob.valueOf('Test Data'), IsMajorVersion = true
        );
        insert contentVersion;
        contentVersion = [SELECT VTD1_CompoundVersionNumber__c FROM ContentVersion];
        //System.debug('contentVersion = ' + contentVersion);
        ContentDocument contentDocument = [SELECT Id FROM ContentDocument];
        //System.debug('contentDocument = ' + contentDocument);
        VTD1_Document__c document = new VTD1_Document__c();
        insert document;
        ContentDocumentLink cdl = new ContentDocumentLink(ShareType = 'V', LinkedEntityId = document.Id, ContentDocumentId = ContentDocument.Id);
        insert cdl;
        dsfs__DocuSign_Status__c docuSignStatus = new dsfs__DocuSign_Status__c(VTD1_Document__c = document.Id);
        insert docuSignStatus;
        update docuSignStatus;
        docuSignStatus.dsfs__Envelope_Status__c = 'Completed';
        update docuSignStatus;
        contentVersion = [SELECT VTD1_CompoundVersionNumber__c FROM ContentVersion];
        //System.debug('contentVersion = ' + contentVersion);
        document = [SELECT VTD1_Version__c FROM VTD1_Document__c];
        //System.debug('document = ' + document);
    }
    /*@testSetup
    public static void setup() {
        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.testSetupUserContacts();
        VT_D1_TestUtils.testSetupRoles();
        VT_D1_TestUtils.testSetupUsers();
        VT_D1_TestUtils.setup();
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        List <User> users = new List<User>();
        User user = new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
                FirstName = 'first',
                LastName = 'last',
                Email = 'puser000@amamama.com',
                Username = 'puser000@amamama.com' + System.currentTimeMillis(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                UserRoleId = r.Id,
                ForecastEnabled = true
        );
        users.add(user);

        System.debug('adding ' + users.size());
        insert users;
    }

    @isTest
    public static void testAttachment() {
        List <dsfs__DocuSign_Status__c> docuSignStatuses = new List<dsfs__DocuSign_Status__c>();
        List <VTD1_Document__c> documents = new List<VTD1_Document__c>();
        //List <Attachment> attachments = new List<Attachment>();
        VTD1_Regulatory_Document__c regulatoryDocument = new VTD1_Regulatory_Document__c();
        insert regulatoryDocument;
        HealthCloudGA__CarePlanTemplate__c template = new HealthCloudGA__CarePlanTemplate__c();
        template.VTD1_Return_Process__c = 'Packaging Materials in Hand';
        template.VTD1_IMP_Return_Interval__c = 'Every Kit';
        template.VTD1_Return_IMP_To__c = 'Other';
        //template.VTD1_Study_Phone_Number__c = '33434324324';
        insert template;
        VTD1_Regulatory_Binder__c binder = new VTD1_Regulatory_Binder__c(VTD1_Care_Plan_Template__c = template.Id, VTD1_Regulatory_Document__c = regulatoryDocument.Id);
        insert binder;
        for (Integer i = 0; i < 3; i ++) {
            documents.add(new VTD1_Document__c(VTD1_Regulatory_Binder__c = binder.Id));
        }
        insert documents;
        System.debug('documents = ' + documents);
        for (Integer i = 0; i < 3; i ++) {
            docuSignStatuses.add(new dsfs__DocuSign_Status__c(VTD1_Document__c = documents[i].Id));
        }
        insert docuSignStatuses;
        /*for (Integer i = 0; i < 3; i ++) {
            attachments.add(new Attachment(Name='Test_' + i, ParentId = docuSignStatuses[i].Id, Body = Blob.valueOf('Test Data ' + i)));
        }
        insert attachments;

        List <ContentVersion> attachments = new List<ContentVersion>();
        for (Integer i = 0; i < 3; i ++) {
            attachments.add(new ContentVersion(Title = 'Test_' + i, PathOnClient = 'Test_' + i + '.txt', VersionData = Blob.valueOf('Test Data_' + i), IsMajorVersion = true));
        }
        insert attachments;
        attachments = [SELECT ContentDocumentId FROM ContentVersion order by Title];
        System.debug('attachments test = ' + attachments);
        List <ContentDocumentLink> cdls = new List<ContentDocumentLink>();
        cdls.add(new ContentDocumentLink(ShareType='V', LinkedEntityId = docuSignStatuses[0].Id, ContentDocumentId = attachments[0].ContentDocumentId));
        cdls.add(new ContentDocumentLink(ShareType='V', LinkedEntityId = docuSignStatuses[1].Id, ContentDocumentId = attachments[1].ContentDocumentId));
        cdls.add(new ContentDocumentLink(ShareType='V', LinkedEntityId = docuSignStatuses[2].Id, ContentDocumentId = attachments[2].ContentDocumentId));
        insert cdls;

        ContentVersion contentVersion = new ContentVersion(
            Title = 'Test', PathOnClient = 'Test.txt', VersionData = Blob.valueOf('Test Data'), IsMajorVersion = true
        );
        insert contentVersion;
        contentVersion = [SELECT VTD1_CompoundVersionNumber__c, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        System.debug('contentVersion = ' + contentVersion);
        ContentDocumentLink cdl = new ContentDocumentLink(ShareType='V', LinkedEntityId = documents[0].Id, ContentDocumentId = contentVersion.ContentDocumentId);
        insert cdl;
        docuSignStatuses[0].dsfs__Envelope_Status__c = 'Completed';
        docuSignStatuses[1].dsfs__Envelope_Status__c = 'Completed';
        update docuSignStatuses;
        List <ContentVersion> contentVersions = [SELECT VTD1_CompoundVersionNumber__c, VTD1_Current_Version__c, ContentDocumentId FROM ContentVersion];
        Map <Id, ContentDocument> contentDocuments = new Map<Id, ContentDocument>([SELECT Id FROM ContentDocument]);
        List <Id> contentDocumentIds = new List<Id>();
        for (ContentDocument contentDocument : contentDocuments.values()) {
            contentDocumentIds.add(contentDocument.Id);
        }
        List <ContentDocumentLink> contentDocumentLinks = [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE ContentDocumentId in : contentDocumentIds];
        Map <Id, Id> docIdToContentDocIdMap = new Map<Id, Id>();
        for (ContentDocumentLink contentDocumentLink : contentDocumentLinks) {
            docIdToContentDocIdMap.put(contentDocumentLink.LinkedEntityId, contentDocumentLink.ContentDocumentId);
        }
        Id contentDocumentId1 = docIdToContentDocIdMap.get(docuSignStatuses[0].VTD1_Document__c);
        Id contentDocumentId2 = docIdToContentDocIdMap.get(docuSignStatuses[1].VTD1_Document__c);
        for (ContentVersion cv : contentVersions) {
            if (cv.ContentDocumentId == contentDocumentId1 && cv.VTD1_Current_Version__c) {
                System.assertEquals(cv.VTD1_CompoundVersionNumber__c, '0.2');
            } else if (cv.ContentDocumentId == contentDocumentId2 && cv.VTD1_Current_Version__c) {
                System.assertEquals(cv.VTD1_CompoundVersionNumber__c, '0.1');
            }
        }
        //documents[0].VTD1_Lifecycle_State__c = 'Approved';
        //update documents[0];
        //VTD1_Document__c document = [SELECT VTD1_Version__c FROM VTD1_Document__c WHERE Id = : documents[0].Id];
        //System.debug('document = ' + document);
        contentVersions = [SELECT VTD1_Lifecycle_State__c, VTD1_CompoundVersionNumber__c FROM ContentVersion WHERE VTD1_CompoundVersionNumber__c = '0.2' AND VTD1_Current_Version__c = true];
        System.debug('contentVersions = ' + contentVersions);
        ContentVersion approvedContentVersion = contentVersions.get(0);
        Test.startTest();
        approvedContentVersion.VTD1_Lifecycle_State__c = 'Approved';
        update contentVersions;
        Test.stopTest();
        contentVersions = [SELECT VTD1_Lifecycle_State__c, VTD1_CompoundVersionNumber__c, VTD1_FullVersion__c FROM ContentVersion WHERE VTD1_CompoundVersionNumber__c = '1.0' AND VTD1_Current_Version__c = true];
        System.debug('contentVersions = ' + contentVersions);

        documents = [SELECT VTD1_Version__c FROM VTD1_Document__c];
        System.debug('documents = ' + documents);
        docuSignStatuses[0].dsfs__Envelope_Status__c = '';
        update docuSignStatuses[0];
        docuSignStatuses[0].dsfs__Envelope_Status__c = 'Completed';
        update docuSignStatuses[0];
        approvedContentVersion = [SELECT VTD1_Lifecycle_State__c, VTD1_CompoundVersionNumber__c FROM ContentVersion WHERE Id = : approvedContentVersion.Id];
        List <ContentVersion> inProgressVersion = [SELECT VTD1_Lifecycle_State__c, VTD1_CompoundVersionNumber__c FROM ContentVersion WHERE VTD1_CompoundVersionNumber__c = '1.1'];
        System.debug('after upload on approve ' + approvedContentVersion);
        System.debug('after upload on approve ' + inProgressVersion);
        regulatoryDocument.VTD1_Study_RSU__c = true;
        regulatoryDocument.VTD1_Study_IRB__c = false;
        //documents[0].VTD1_Current_Workflow__c = 'Study RSU no IRB';
        update documents[0];
        update regulatoryDocument;
        docuSignStatuses[0].dsfs__Envelope_Status__c = '';
        update docuSignStatuses[0];
        docuSignStatuses[0].dsfs__Envelope_Status__c = 'Completed';
        update docuSignStatuses[0];
        documents = [SELECT VTD1_Version__c, VTD1_Status__c, VTD1_Lifecycle_State__c FROM VTD1_Document__c WHERE Id = :documents[0].Id];
        System.debug('documents after = ' + documents);
    }
    @isTest
    public static void testEnvelopStatus() {
        User user = [SELECT Id FROM User limit 1];
        System.debug('user = ' + user);

        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();
        VT_D1_TestUtils.testSetupUserContacts();
        User PIUser = VT_D1_TestUtils.createUserByProfile('Primary Investigator', 'PIUser');
        User PIBackupUser = VT_D1_TestUtils.createUserByProfile('Primary Investigator', 'PIBackupUser');
        User PrimaryPGUser = VT_D1_TestUtils.createUserByProfile('Patient Guide', 'PrimaryPGUser');
        User SecondaryPGUser = VT_D1_TestUtils.createUserByProfile('Patient Guide', 'SecondaryPGUser');
        User PatientUser = VT_D1_TestUtils.createUserByProfile('Patient', 'PatientUser');
        VT_D1_TestUtils.persistUsers();

        Id rtTaskId = VT_R4_ConstantsHelper_Tasks.RECORD_TYPE_ID_TASK_SIMPLETASK;//Schema.SObjectType.Task.getRecordTypeInfosByName().get('SimpleTask').getRecordTypeId();
        Id rtVisitId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_ACTUAL_VISIT_UNSCHEDULED;//Schema.SObjectType.VTD1_Actual_Visit__c.getRecordTypeInfosByName().get('Unscheduled').getRecordTypeId();
        //VTD1_RTId__c settings = new VTD1_RTId__c(VTD1_Task_SimpleTask__c = rtTaskId, VTD1_Actual_Visit_Unscheduled__c = rtVisitId);
        //insert settings;
        VT_D1_TestUtils.setupRecordTypes();
        Account account = new Account(Name = 'Test account');
        insert account;
        Id rtSponsorAccountId = VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_ACCOUNT_SPONSOR;//Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sponsor').getRecordTypeId();
        Account sponsorAccount = new Account(Name = 'Test sponsor account', RecordTypeId = rtSponsorAccountId);
        insert sponsorAccount;
        List <dsfs__DocuSign_Status__c> docuSignStatuses = new List<dsfs__DocuSign_Status__c>();

        //RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'CarePlan'];
        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'VTD1_PCF'];
        Contact contact = new Contact(FirstName = 'Test contact', LastName = 'Last name');
        insert contact;
        VTD1_Patient__c patient = new VTD1_Patient__c(VTD1_First_Name__c = 'First', VTD1_Last_Name__c = 'Test');
        insert patient;

        List <HealthCloudGA__CarePlanTemplate__c> templates = new List<HealthCloudGA__CarePlanTemplate__c>();
        for (Integer i = 0; i < 3; i ++) {
            HealthCloudGA__CarePlanTemplate__c template = new HealthCloudGA__CarePlanTemplate__c();
            template.VTD1_Return_Process__c = 'Packaging Materials in Hand';
            template.VTD1_IMP_Return_Interval__c = 'Every Kit';
            template.VTD1_Return_IMP_To__c = 'Other';
            //template.VTD1_Study_Phone_Number__c = '33434324324';
            template.VTD1_Primary_Sponsor__c = sponsorAccount.Id;
            template.OwnerId = user.Id;
            templates.add(template);
        }
        insert templates;
        List <Case> caseList = new List<Case>();
        for (Integer i = 0; i < 3; i ++) {
            Case caseObj = new Case(AccountId = account.Id, VTD1_Primary_PG__c = PrimaryPGUser.Id, VTD1_Secondary_PG__c = SecondaryPGUser.Id, Status = 'Open',
                    VTD1_PI_contact__c = contact.Id, ContactId = contact.Id, VTD1_Backup_PI_User__c = PIBackupUser.Id, VTD1_Patient_User__c = PatientUser.Id, VTD1_Patient__c = patient.Id,
                    RecordTypeId = rt.Id, VTD1_PI_user__c = PIUser.Id, VTD1_Study__c = templates[i].Id);

            caseList.add(caseObj);
        }
        insert caseList;
        caseList = [SELECT Id, VTD1_SecureConsentID__c FROM Case];
        System.debug('caseList = ' + caseList);



        VTD1_Protocol_Amendment__c amendment = new VTD1_Protocol_Amendment__c(VTD1_PA_Document_is_Signed__c = false);
        insert amendment;
        rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'VTD1_Regulatory_Document'];
        List <VTD1_Document__c> documents = new List<VTD1_Document__c>();
        //List <Attachment> attachments = new List<Attachment>();
        VTD1_Regulatory_Document__c regulatoryDocument = new VTD1_Regulatory_Document__c();
        insert regulatoryDocument;

        Id rtId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_PATIENT_ELIGIBILITY_ASSE;//Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByName().get('Patient Eligibility Assessment Form').getRecordTypeId();
        VTD1_Regulatory_Binder__c binder = new VTD1_Regulatory_Binder__c(VTD1_Care_Plan_Template__c = templates[0].Id, VTD1_Regulatory_Document__c = regulatoryDocument.Id);
        insert binder;
        for (Integer i = 0; i < 3; i ++) {
            documents.add(new VTD1_Document__c(RecordTypeId = rtId, VTD1_Study__c = templates[i].Id, VTD1_Regulatory_Binder__c = binder.Id, VTD1_Clinical_Study_Membership__c = caseList[i].Id,/* VTD1_Document_Type__c='Medical Record', VTD1_Signature_Date__c = System.now().date(), VTD1_Protocol_Amendment__c = amendment.Id));
        }
        insert documents;


        System.debug('documents = ' + documents);
        for (Integer i = 0; i < 3; i ++) {
            docuSignStatuses.add(new dsfs__DocuSign_Status__c(VTD1_Document__c = documents[i].Id));
        }
        insert docuSignStatuses;
        docuSignStatuses[0].dsfs__Envelope_Status__c = 'Completed';
        docuSignStatuses[1].dsfs__Envelope_Status__c = 'Declined';
        docuSignStatuses[1].dsfs__Declined_Reason__c = 'Additional Information Requested';
        update docuSignStatuses;
        System.debug('run as ' + user);
        documents = [SELECT VTD1_Eligibility_Assessment_Status__c, VTD1_Clinical_Study_Membership__c FROM VTD1_Document__c];
        System.debug('documents = ' + documents);
    }
    @isTest
    public static void test() {
        Contact contact = VT_D1_TestUtils.createContact(null, 'First', 'Last', 'Mr', null, null);
        insert contact;
        Case caseObj = VT_D1_TestUtils.createCase('VTD1_PCF', contact.Id, null, null, null, null, null);
        System.debug('case = ' + caseObj);
        insert caseObj;
        VTD1_Patient__c patient = VT_D1_TestUtils.createPatient('Standard', 'first', 'last', null, null);
        insert patient;
    }*/
    @isTest
    public static void test() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c];

        //Commented and Done the changes by Priyanka Ambre to remove Remote, Onsite references- SH-17441
        //Start
        Set<String> setStrings = new Set<String>{'PIUser1', 'PIBackupUser1'};
		Study_Team_Member__c pi;
        Study_Team_Member__c pi2;
        Study_Team_Member__c craSTM;
        for(Study_Team_Member__c objSTM : [SELECT	Id, 
                                           			User__c,
                                           			User__r.FirstName, 
                                           			User__r.Profile.Name,
                                           			Study_Team_Member__c                                           
                						   FROM Study_Team_Member__c
                                           WHERE (Study__c = :study.Id AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName IN : setStrings) OR User__r.Profile.Name = 'CRA']){
                                               if(objSTM.User__r.Profile.Name == 'CRA'){
                                                   craSTM = objSTM;
                                               }else if(objSTM.User__r.FirstName == 'PIBackupUser1'){
                                                   pi2 = objSTM;
                                               }else if(objSTM.User__r.FirstName == 'PIUser1'){
                                                   pi = objSTM;
                                               }         
                                           }
        /*Study_Team_Member__c pi = [
                SELECT Id,User__r.FirstName,Study_Team_Member__c
                FROM Study_Team_Member__c
                WHERE Study__c = :study.Id AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName = 'PIUser1'
        ];

        Study_Team_Member__c pi2 = [
                SELECT Id,User__r.FirstName,Study_Team_Member__c, User__r.Email
                FROM Study_Team_Member__c
                WHERE Study__c = :study.Id AND VTD1_Type__c = 'Primary Investigator' AND User__r.FirstName = 'PIBackupUser1'
        ];
        //START:Rajesh CRA fields are not going to be used on STM level
        //pi.VTR2_Remote_CRA__c = pi2.Id;
        //pi.VTR2_Onsite_CRA__c = pi2.Id;
        
        update pi;*/

        //List <Study_Team_Member__c> studyTeamMembers = [SELECT Id, RecordType.Name, Study__c, User__c FROM Study_Team_Member__c WHERE Study__c = : study.Id AND RecordType.Name = 'PI' AND VTD1_PIsAssignedPG__c != null order by CreatedDate];
        Virtual_Site__c virtualSite = new Virtual_Site__c(
                VTD1_Study_Site_Number__c = '12345',
                VTD1_Study__c = study.Id,
                Name = 'REG_TEST',
                VTD1_Study_Team_Member__c = pi.Id//studyTeamMembers[0].Id
        );
        insert virtualSite;

        Study_Site_Team_Member__c sstm1 = new Study_Site_Team_Member__c();
        sstm1.VTR5_Associated_CRA__c = craSTM.Id;
        sstm1.VTD1_SiteID__c = virtualSite.Id ;
        insert sstm1;
        
        HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
        objVirtualShare.ParentId = study.Id;
        objVirtualShare.UserOrGroupId = craSTM.User__c;
        objVirtualShare.AccessLevel = 'Edit';
        insert objVirtualShare;

        VTD1_Monitoring_Visit__c monitoringVisit = new VTD1_Monitoring_Visit__c(VTD1_Site_Visit_Type__c = 'Selection Visit', VTD1_Virtual_Site__c = virtualSite.Id, VTD1_Study_Site_Visit_Status__c = 'Visit Completed'/*, VTR2_Remote_Onsite__c = 'Remote'*/);
        System.runAs(new User(Id = craSTM.User__c)){
        insert monitoringVisit;
        }
        //End

        //System.debug('study = ' + study);
        Case caseObj = [SELECT Id FROM Case LIMIT 1];
        Id rtIdCertification = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_CERTIFICATON;
        List <VTD1_Document__c> documents = new List<VTD1_Document__c>();
        documents.add(new VTD1_Document__c(RecordTypeId = rtIdCertification, VTD1_Study__c = study.Id, VTD1_Clinical_Study_Membership__c = caseObj.Id));
        insert documents;

        List <dsfs__DocuSign_Status__c> docuSignStatuses = new List<dsfs__DocuSign_Status__c>();
        docuSignStatuses.add(new dsfs__DocuSign_Status__c(VTD1_Document__c = documents[0].Id));
        insert docuSignStatuses;
        update docuSignStatuses;

        docuSignStatuses.add(new dsfs__DocuSign_Status__c(VTD1_Monitoring_Visit__c = monitoringVisit.Id, dsfs__Envelope_Status__c = 'Completed'));
        insert docuSignStatuses[docuSignStatuses.size() - 1];
        update docuSignStatuses;
    }
}