/**
 * Created by Maksim Fedarenka on 7/20/2020.
 */

@IsTest
private class VT_R5_InactivePatientsDeactBatchTest {
    @TestSetup
    private static void setupMethod() {
        DomainObjects.Study_t domainStudy = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .setVTR5_Sponsor_Logo('<img src="https://domain.com/image.png" />')
                .setVTD1_Protocol_Nickname('protocol_nickname')
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );
        VT_R3_GlobalSharing.disableForTest = true;

        HealthCloudGA__CandidatePatient__c candidatePatient = (HealthCloudGA__CandidatePatient__c) new DomainObjects.HealthCloudGA_CandidatePatient_t()
                .setConversion('Converted')
                .setCaregiverEmail(DomainObjects.RANDOM.getEmail())
                .addStudy(domainStudy)
                .persist();

        Case aCase = DomainObjects.TEST_UTILS.getTestCase();
        aCase.Status = VT_R4_ConstantsHelper_Statuses.CASE_DOES_NOT_QUALIFY;
        update aCase;
    }

    @IsTest
    static void testDeactivationProcess() {
        Case aCase = DomainObjects.TEST_UTILS.getTestCase();

        // Since the patient User becomes Inactive during case status change to CASE_DOES_NOT_QUALIFY (see @testSetup),
        // it's necessary to re-activate it before the batch execution
        // [UNUSED] Since R5.1 BugFix. PT/CG don't become deactivated by UserService, due to callouts
        //User user = DomainObjects.TEST_UTILS.getTestPatientByCase(aCase);
        //user.IsActive = true;
        //update user;
        User user = DomainObjects.TEST_UTILS.getTestPatientByCase(aCase);
        System.assert(user.IsActive == false, 'Since R5.1 BugFix, PT/CG users should not be deactivated by UserService (Case after update)');

        User patient = getUser(aCase.VTD1_Patient_User__c);
        System.assert(patient.IsActive == true, 'Patient User must be active before the batch execution');

        Test.startTest();
        VT_R5_InactivePatientsDeactivationBatch.run(200);
        Test.stopTest();

        patient = getUser(aCase.VTD1_Patient_User__c);
        System.assert(patient.IsActive == false, 'Patient User must be deactivated after the batch execution');
    }

    private static User getUser(Id userId) {
        return [SELECT Id, Name, IsActive FROM User WHERE Id = :userId];
    }
}