/**
 * Created by Aliaksei Yamayeu on 02.11.2020.
 */
// SH-19506
global without sharing class VT_R5_eDiaryCreatorBatch implements Database.Batchable<sObject>, Schedulable, Database.Stateful {

    private List<Id> protocolEDiariesIds;
    private Map<String, List<VTD1_Protocol_ePRO__c>> eDiaryStatusToProtocolEDiaries = new Map<String, List<VTD1_Protocol_ePRO__c>>();
    @TestVisible
    private static String ACTIVE_RANDOMIZED_STATUS = VT_R4_ConstantsHelper_AccountContactCase.CASE_ACTIVE_RANDOMIZED;

    global void execute(SchedulableContext sc) {
        Database.executeBatch(new VT_R5_eDiaryCreatorBatch(protocolEDiariesIds), 15);
    }

    public VT_R5_eDiaryCreatorBatch(List<Id> protocolEDiariesIds) {
        this.protocolEDiariesIds = protocolEDiariesIds;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        if (protocolEDiariesIds == null || protocolEDiariesIds.isEmpty()) {
            return null;
        }

        String query = 'SELECT Name,' +
                    'VTR4_EventType__c, ' +
                    'VTD1_Event__c, ' +
                    'VTR4_PatientMilestone__c, ' +
                    'VTD1_Response_Window__c,' +
                    'VTR4_RootProtocolId__c,' +
                    'VTD1_Protocol_Amendment__c,' +
                    'VTR4_RemoveFromProtocol__c,' +
                    'VTR4_EventOffset__c,' +
                    'VTD1_Reminder_Window__c,' +
                    'VTD1_Caregiver_on_behalf_of_Patient__c,' +
                    'RecordType.DeveloperName,' +
                    'VTR5_eCOAEventName__c,' +
                    'VTD1_Study__c,' +
                    'VTD1_Subject__c,' +
                    'VTR2_Protocol_Reviewer__c,' +
                    'VTR2_eDiary_delay__c,' +
                    'VTR4_EventOffsetPeriod__c,' +
                    'VTR4_EventTime__c,' +
                    'VTR5_SubsetSafetyID__c,' +
                    'VTR5_SubsetImmunoID__c ' +
                'FROM VTD1_Protocol_ePRO__c ' +
                'WHERE Id IN :protocolEDiariesIds';

        List<Id> studyIds = new List<Id>();
        for (VTD1_Protocol_ePRO__c pEDiary : Database.query(query)) {
            if (eDiaryStatusToProtocolEDiaries.get(ACTIVE_RANDOMIZED_STATUS) == null) {
                eDiaryStatusToProtocolEDiaries.put(ACTIVE_RANDOMIZED_STATUS, new List<VTD1_Protocol_ePRO__c>());
            }
            eDiaryStatusToProtocolEDiaries.get(ACTIVE_RANDOMIZED_STATUS).add(pEDiary);
            studyIds.add(pEDiary.VTD1_Study__c);
        }

        query = 'SELECT Status,' +
                    'VTD1_ePro_can_be_completed_by_Caregiver__c,' +
                    'VTD1_Patient_User__r.Contact.AccountId,' +
                    'VTD1_Patient_User__c,' +
                    'VTD1_Patient_User__r.VTD2_UserTimezone__c,' +
                    'VTD1_Study__c,' +
                    'VTD1_Virtual_Site__c,' +
                    'VTD1_Primary_PG__c,' +
                    'VTR2_SiteCoordinator__c,' +
                    'VTR5_SubsetSafety__c,' +
                    'VTR5_SubsetImmuno__c ' +
                    'FROM Case ' +
                    'WHERE VTD1_Study__c IN :studyIds AND Status = :ACTIVE_RANDOMIZED_STATUS AND VTD1_Patient_User__r.IsActive = true';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Case> scope) {
        List<VTD1_Survey__c> eDiaries = new VT_R5_eDiaryCreatorBatchHelper().createEpros(scope, eDiaryStatusToProtocolEDiaries);
        insert eDiaries;
    }

    global void finish(Database.BatchableContext bc) {   }
}