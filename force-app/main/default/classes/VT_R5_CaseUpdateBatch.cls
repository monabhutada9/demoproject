/**
* @author:      IQVIA Strikers
* @date:        08/10/2020
*
* @group:       PI Transfer change Update
*
* @description: Asynchronous dml operation on case object in order to update the Primary and Backup PI.
*/
public class VT_R5_CaseUpdateBatch implements Database.Batchable<sObject> {
    public List <Case> casesToUpdate = new List<Case>();
    
    public VT_R5_CaseUpdateBatch(List<Case> cases){
        casesToUpdate = cases;          
    }
    
    public List<SObject> start(Database.BatchableContext context)
    {
        return casesToUpdate;
    }
    
    public void execute(Database.BatchableContext BC, 
                        List<sObject> scope)
    {
        VT_R5_CaseProcessHandler.skipFlows=true;
         update scope;
    }
    
    public void finish(Database.BatchableContext BC){}
}