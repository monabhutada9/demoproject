/**
 * Created by Aleksandr Mazan on 05-Aug-20.
 */
@IsTest
public with sharing class VT_R5_eDiaryScheduledActionsHerokuTest {

    public static List<VT_R5_eDiaryScheduledActionsHeroku.FlowInput> createTestData() {
        List<VT_R5_eDiaryScheduledActionsHeroku.FlowInput> FlowInputs = new List<VT_R5_eDiaryScheduledActionsHeroku.FlowInput>();
        List<Schema.RecordTypeInfo> surveyRecordTypes = VTD1_Survey__c.SObjectType.getDescribe().getRecordTypeInfos();
        List<User> users = new List<User>();
        Id surveyEProId;
        for (Schema.RecordTypeInfo surveyRecordType : surveyRecordTypes) {
            if (surveyRecordType.developerName == VT_R5_eDiaryScheduledActionsHeroku.EPRO) {
                surveyEProId = surveyRecordType.recordTypeId;
            }
        }
        Account account = new Account(Name = 'Test Patient');
        insert account;
        List<SObject> sObjects = new List<SObject>();
        List<Contact> contacts = new List<Contact>();
        contacts.add(new Contact(LastName = 'TestContact1', AccountId = account.Id, VTD1_Primary_CG__c = true, VTD1_Clinical_Study_Membership__c = null));
        contacts.add(new Contact(LastName = 'TestContact2'));
        insert contacts;
        for (Profile prof : [SELECT Id, Name FROM Profile WHERE Name = 'Caregiver' OR Name = 'Patient']) {
            if (prof.Name == 'Patient') {
                users.add(new User(Alias = 'PatUser', Email = 'patientuser@testorg.com',
                        EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                        LocaleSidKey = 'en_US', ProfileId = prof.Id, IsActive = true,
                        TimeZoneSidKey = 'America/Los_Angeles', UserName = 'newpatientuser@testorg.com', ContactId = contacts[0].Id));
//            } else if (prof.Name == 'Caregiver') {
//                users.add(new User(Alias = 'CarUser', Email='caregiveruser@testorg.com',
//                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
//                        LocaleSidKey='en_US', ProfileId = prof.Id,
//                        TimeZoneSidKey='America/Los_Angeles', UserName='newCcregiveruser@testorg.com', ContactId = contacts[1].Id));
            }
        }
        sObjects.addAll(users);
        Case newCase = new Case(ContactId = contacts[0].Id);
        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c();
        sObjects.add(newCase);
        sObjects.add(study);
        insert sObjects;
        contacts[0].VTD1_Clinical_Study_Membership__c = newCase.Id;
        update contacts[0];
        VTD1_Protocol_ePRO__c newProtocol = new VTD1_Protocol_ePRO__c(VTD1_Study__c = study.Id, VTD1_Subject__c = 'Patient');
        insert newProtocol;
        List<VTD1_Survey_Answer__c> surveyAnswers = new List<VTD1_Survey_Answer__c>();
        VTD1_Survey__c survey = new VTD1_Survey__c();
        VTD1_Survey__c surveyReadyToStartPG = new VTD1_Survey__c();
        VTD1_Survey__c surveyInProgressPG = new VTD1_Survey__c();
        VTD1_Survey__c surveyReviewRequiredPG = new VTD1_Survey__c();
        VTD1_Survey__c surveyReviewRequiredPG2 = new VTD1_Survey__c();
        for (User oneUser : users) {
            if (oneUser.Alias == 'PatUser') {
                survey.VTD1_Patient_User_Id__c = oneUser.Id;
                surveyReadyToStartPG.VTD1_Patient_User_Id__c = oneUser.Id;
                surveyInProgressPG.VTD1_Patient_User_Id__c = oneUser.Id;
            } else if (oneUser.Alias == 'CaregiverUser') {
                survey.VTD1_Caregiver_User_Id__c = oneUser.Id;
                surveyReadyToStartPG.VTD1_Caregiver_User_Id__c = oneUser.Id;
                surveyInProgressPG.VTD1_Caregiver_User_Id__c = oneUser.Id;
            }
        }
        survey.Name = 'test';
        survey.VTD1_CSM__c = newCase.Id;
        survey.VTD1_Due_Date__c = Datetime.now();
        survey.VTD1_Date_Available__c = Datetime.now().addDays(-1);
        survey.VTD1_Status__c = VT_R5_eDiaryScheduledActionsHeroku.STATUS_NOT_STARTED;
        survey.RecordTypeId = surveyEProId;
        survey.VTR2_Reviewer_User__c = VT_R5_eDiaryScheduledActionsHeroku.USER_PATIENT_GUIDE;
        survey.VTD1_Protocol_ePRO__c = newProtocol.Id;
        survey.VTR5_CompletedBy__c = UserInfo.getUserId();
        survey.VTD1_Reminder_Due_Date__c = Datetime.now().addDays(-1);
        survey.VTR2_Review_Due_Date__c = Datetime.now().addDays(-1);
        survey.VTD1_Scoring_Due_Date__c = Datetime.now().addDays(-1);

        surveyReadyToStartPG.Name = 'testReadyToStart';
        surveyReadyToStartPG.VTD1_CSM__c = newCase.Id;
        surveyReadyToStartPG.VTD1_Due_Date__c = Datetime.now().addHours(-1);
        surveyReadyToStartPG.VTD1_Reminder_Due_Date__c = Datetime.now().addDays(-1);
        surveyReadyToStartPG.VTD1_Status__c = VT_R5_eDiaryScheduledActionsHeroku.STATUS_READY_TO_START;
        surveyReadyToStartPG.VTR2_Reviewer_User__c = VT_R5_eDiaryScheduledActionsHeroku.USER_PATIENT_GUIDE;
        surveyReadyToStartPG.RecordTypeId = surveyEProId;
        surveyReadyToStartPG.VTD1_Protocol_ePRO__c = newProtocol.Id;
        surveyReadyToStartPG.VTR5_CompletedBy__c = UserInfo.getUserId();

        surveyInProgressPG.Name = 'testInProgress';
        surveyInProgressPG.VTD1_CSM__c = newCase.Id;
        surveyInProgressPG.VTD1_Due_Date__c = Datetime.now().addHours(-1);
        surveyInProgressPG.VTD1_Reminder_Due_Date__c = Datetime.now().addDays(-1);
        surveyInProgressPG.VTD1_Status__c = VT_R5_eDiaryScheduledActionsHeroku.STATUS_IN_PROGRESS;
        surveyInProgressPG.VTR2_Reviewer_User__c = VT_R5_eDiaryScheduledActionsHeroku.USER_PATIENT_GUIDE;
        surveyInProgressPG.RecordTypeId = surveyEProId;
        surveyInProgressPG.VTD1_Protocol_ePRO__c = newProtocol.Id;
        surveyInProgressPG.VTR5_CompletedBy__c = UserInfo.getUserId();

        surveyReviewRequiredPG.Name = 'testReviewRequired';
        surveyReviewRequiredPG.VTD1_CSM__c = newCase.Id;
        surveyReviewRequiredPG.VTD1_Due_Date__c = Datetime.now().addHours(-1);
        surveyReviewRequiredPG.VTD1_Reminder_Due_Date__c = Datetime.now().addDays(-1);
        surveyReviewRequiredPG.VTR2_Review_Due_Date__c = Datetime.now().addDays(-1);
        surveyReviewRequiredPG.VTD1_Status__c = VT_R5_eDiaryScheduledActionsHeroku.STATUS_REVIEW_REQUIRED;
        surveyReviewRequiredPG.VTR2_Reviewer_User__c = VT_R5_eDiaryScheduledActionsHeroku.USER_PATIENT_GUIDE;
        surveyReviewRequiredPG.RecordTypeId = surveyEProId;
        surveyReviewRequiredPG.VTD1_Protocol_ePRO__c = newProtocol.Id;
        surveyReviewRequiredPG.VTD1_Patient_User_Id__c = users[0].Id;
        surveyReviewRequiredPG.VTR5_CompletedBy__c = UserInfo.getUserId();

        surveyReviewRequiredPG2.Name = 'testReviewRequired2';
        surveyReviewRequiredPG2.VTD1_CSM__c = newCase.Id;
        surveyReviewRequiredPG2.VTD1_Due_Date__c = Datetime.now().addHours(-1);
        surveyReviewRequiredPG2.VTD1_Reminder_Due_Date__c = Datetime.now().addDays(-1);
        surveyReviewRequiredPG2.VTD1_Scoring_Due_Date__c = Datetime.now().addDays(-1);
        surveyReviewRequiredPG2.VTD1_Status__c = VT_R5_eDiaryScheduledActionsHeroku.STATUS_REVIEW_REQUIRED;
        surveyReviewRequiredPG2.VTR2_Reviewer_User__c = VT_R5_eDiaryScheduledActionsHeroku.USER_PATIENT_GUIDE;
        surveyReviewRequiredPG2.RecordTypeId = surveyEProId;
        surveyReviewRequiredPG2.VTD1_Protocol_ePRO__c = newProtocol.Id;
        surveyReviewRequiredPG2.VTD1_Patient_User_Id__c = users[0].Id;
        surveyReviewRequiredPG2.VTR5_CompletedBy__c = UserInfo.getUserId();

        List<VTD1_Survey__c> surveys = new List<VTD1_Survey__c>{
                survey, surveyReadyToStartPG, surveyInProgressPG, surveyReviewRequiredPG, surveyReviewRequiredPG2
        };
        try {
            insert surveys;
            FlowInputs.add(new VT_R5_eDiaryScheduledActionsHeroku.FlowInput(survey, survey.VTD1_Date_Available__c));
            FlowInputs.add(new VT_R5_eDiaryScheduledActionsHeroku.FlowInput(surveyReadyToStartPG, surveyReadyToStartPG.VTD1_Reminder_Due_Date__c));
            FlowInputs.add(new VT_R5_eDiaryScheduledActionsHeroku.FlowInput(surveyInProgressPG, surveyInProgressPG.VTD1_Reminder_Due_Date__c));
            FlowInputs.add(new VT_R5_eDiaryScheduledActionsHeroku.FlowInput(surveyReviewRequiredPG, surveyReviewRequiredPG.VTR2_Review_Due_Date__c));
            FlowInputs.add(new VT_R5_eDiaryScheduledActionsHeroku.FlowInput(surveyReviewRequiredPG2, surveyReviewRequiredPG2.VTD1_Scoring_Due_Date__c));
        } catch (Exception e) {
            System.debug(e.getMessage());
        }
        for (VTD1_Survey__c oneSurvey : surveys) {
            if (oneSurvey.Name == 'testInProgress') {
                surveyAnswers.add(new VTD1_Survey_Answer__c(VTD1_Survey__c = oneSurvey.Id));
                surveyAnswers.add(new VTD1_Survey_Answer__c(VTD1_Survey__c = oneSurvey.Id));
                surveyAnswers.add(new VTD1_Survey_Answer__c(VTD1_Survey__c = oneSurvey.Id, VTR2_Is_Answered__c = true));
                break;
            }
        }
        insert surveyAnswers;
        return FlowInputs;
    }

    public static void testScheduledActionsTest() {
        List<VT_R5_eDiaryScheduledActionsHeroku.FlowInput> FlowInputs = createTestData();
        Test.startTest();
        Database.executeBatch(new VT_R5_eDiaryScheduledActionsHerokuBatch(JSON.serialize(FlowInputs)), 50);
        Test.stopTest();
    }

    public static void testScheduledActionsExecuteTest() {
        List<VT_R5_eDiaryScheduledActionsHeroku.FlowInput> FlowInputs = createTestData();
        Test.startTest();
        VT_R5_eDiaryScheduledActionsHeroku instance = new VT_R5_eDiaryScheduledActionsHeroku();
        instance.herokuExecute(JSON.serialize(FlowInputs));
        Test.stopTest();
    }

}