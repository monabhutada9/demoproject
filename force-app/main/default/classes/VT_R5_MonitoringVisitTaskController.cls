/**
 * Created by user on 5/25/2020.
 */

@SuppressWarnings('ApexUnresolvableReference')
public with sharing class VT_R5_MonitoringVisitTaskController {
    private static final String TN_CATALOG_CODE_FOR_POOL = 'T613';
    private static final String TN_CATALOG_CODE_FOR_TASK = 'T639';

    @AuraEnabled
    public static Boolean sendVisitReport(Id recordId, String mvrStatus){
        if (mvrStatus == 'Rejected') {
            return resendVisitReport(recordId);
        } else {
            sendNewVisitReport(recordId);
        }
        return true;
    }

    @AuraEnabled
    public static ResponseWrapper getDocuments(Id recordId, String objectName) {
        VTD1_Monitoring_Visit__c monitoringVisit = [
                SELECT Id, VTR5_SignMVReportTaskSent__c, VTD1_Remote_Monitoring_Visit_Report_Stat__c,
                        Follow_Up_Letter_Status__c, VTR5_FollowUpLetterSubmitted__c, VTD1_Study_Site_Visit_Status__c
                FROM VTD1_Monitoring_Visit__c
                WHERE Id = :recordId
        ];

        List<ContentDocumentLink> docLinks = [
                SELECT ContentDocumentId
                FROM ContentDocumentLink
                WHERE LinkedEntityId = :recordId AND LinkedEntity.Type = :objectName
        ];
        List<Id> docLinkIds = new List<Id>();
        for (ContentDocumentLink contentDocumentLink : docLinks) {
            docLinkIds.add(contentDocumentLink.ContentDocumentId);
        }
        List<ContentDocument> docs = [
                SELECT Title,
                        CreatedDate,
                        FileExtension,
                        ContentSize,
                        LatestPublishedVersionId
                FROM ContentDocument
                WHERE Id IN :docLinkIds
                ORDER BY CreatedDate DESC
        ];

        return new ResponseWrapper()
                .setMonitoringVisit(monitoringVisit)
                .setDocuments(docs)
                .setDocuSignStatus(getLatestDocusignStatus(recordId));
    }

    public static dsfs__DocuSign_Status__c getLatestDocusignStatus(final Id monitoringVisitId) {
        dsfs__DocuSign_Status__c status = null;
        List<dsfs__DocuSign_Status__c> statuses = [
                SELECT Id, VTD1_Monitoring_Visit__c, dsfs__Envelope_Status__c, dsfs__Completed_Date_Time__c,
                        dsfs__Viewed_Date_Time__c, dsfs__Declined_Date_Time__c
                FROM dsfs__DocuSign_Status__c
                WHERE VTD1_Monitoring_Visit__c = :monitoringVisitId
                ORDER BY CreatedDate DESC
                LIMIT 1
        ];
        if (!statuses.isEmpty()) {
            status = statuses.get(0);
        }
        return status;
    }

    public class ResponseWrapper {
        @AuraEnabled
        public VTD1_Monitoring_Visit__c monitoringVisit;
        @AuraEnabled
        public List<ContentDocument> documents;
        @AuraEnabled
        public dsfs__DocuSign_Status__c docusignStatus;


        public ResponseWrapper setMonitoringVisit(final VTD1_Monitoring_Visit__c monitoringVisit) {
            this.monitoringVisit = monitoringVisit;
            return this;
        }

        public ResponseWrapper setDocuments(final List<ContentDocument> documents) {
            this.documents = documents;
            return this;
        }

        public ResponseWrapper setDocuSignStatus(final dsfs__DocuSign_Status__c docusignStatus) {
            this.docusignStatus = docusignStatus;
            return this;
        }
    }

    private static void sendNewVisitReport(Id recordId) {
        VT_D2_TNCatalogTasks.generateTasks(
                new List<String>{
                        TN_CATALOG_CODE_FOR_POOL
                }, new List<String>{
                        recordId
                }, null, null);


        List<VT_R3_EmailsSender.ParamsHolder> emails = new List<VT_R3_EmailsSender.ParamsHolder>();

        VTD1_Monitoring_Visit__c mv = [
                SELECT VTR5_SignMVReportTaskSent__c,
                        VTD1_Study__r.VTR5_MRR_Queue_ID__c
                FROM VTD1_Monitoring_Visit__c
                WHERE Id = :recordId
                LIMIT 1
        ];

        List<User> mrrUsers = [
                SELECT User.Email
                FROM User
                WHERE Id IN (
                        SELECT UserOrGroupId
                        FROM GroupMember
                        WHERE Group.Id = :mv.VTD1_Study__r.VTR5_MRR_Queue_ID__c
                )
        ];

        for (User u : mrrUsers) {
            emails.add(
                    new VT_R3_EmailsSender.ParamsHolder(u.Id, recordId, 'VT_R5_MV_Report_Ready_For_Review', u.Email, true)
            );
        }
        VT_R3_EmailsSender.sendEmails(emails);

        mv.VTR5_SignMVReportTaskSent__c = true;
        mv.VTD1_Remote_Monitoring_Visit_Report_Stat__c = 'Submitted';
        update mv;
    }

    @AuraEnabled
    public static Boolean toShowFollowUpButton(Id recordId) {
        Boolean toShow = false;
        Datetime completedDt;
        Datetime lastDocDT;
        List <dsfs__DocuSign_Status__c> docStatuses = new List <dsfs__DocuSign_Status__c>();
        docStatuses = [
                SELECT Id,
                        dsfs__Completed_Date_Time__c,
                        VTD1_Monitoring_Visit__c
                FROM dsfs__DocuSign_Status__c
                WHERE VTD1_Monitoring_Visit__c = :recordId
                AND dsfs__Completed_Date_Time__c != NULL
                ORDER BY dsfs__Completed_Date_Time__c DESC
        ];

        VTD1_Monitoring_Visit__c mvr = [
                SELECT Id,
                        VTD1_Remote_Monitoring_Visit_Report_Stat__c
                FROM VTD1_Monitoring_Visit__c
                WHERE Id = :recordId
                LIMIT 1
        ];

        List <ContentDocumentLink> cdls = new List<ContentDocumentLink>();

        cdls = [
                SELECT ContentDocumentId,
                        LinkedEntityId,
                        ContentDocument.LastModifiedDate
                FROM ContentDocumentLink
                WHERE LinkedEntityId = :recordId
                ORDER BY ContentDocument.LastModifiedDate DESC
        ];

        if (cdls.size() > 0) lastDocDT = cdls[0].ContentDocument.LastModifiedDate;
        if (docStatuses.size() > 0) completedDt = docStatuses[0].dsfs__Completed_Date_Time__c;


        // if there is no Completed DocuSign Statuses, button will not be displayed.
        if (docStatuses.size() == 0) {
             toShow = false;

        } else if (lastDocDT != null && completedDt != null && mvr.VTD1_Remote_Monitoring_Visit_Report_Stat__c == 'Approved'
                && lastDocDT > completedDt) {
             toShow = true;

        }

        System.debug('toShow ' + toShow);
        return toShow;
    }

    @AuraEnabled
    public static Boolean sendSubmitFollowUpLetter(Id mvId) {
        Boolean status;
        if (mvId == null) {
            return status;
        }

        VTD1_Monitoring_Visit__c mvWithFile = [
                SELECT Id,
                        VTR5_MRRSignedMVReport__c, (
                        SELECT Id, ContentDocumentId
                        FROM ContentDocumentLinks
                        ORDER BY ContentDocument.LastModifiedDate DESC
                        LIMIT 1
                )
                FROM VTD1_Monitoring_Visit__c
                WHERE Id = :mvId
        ];

        if (mvWithFile != null) {
            new VT_R5_SendDocstoDocuSign(mvWithFile.Id,
                    mvWithFile.ContentDocumentLinks[0].ContentDocumentId,
                    mvWithFile.VTR5_MRRSignedMVReport__c,
                    'Documents for your DocuSign Signature',
                    'I am sending you this request for your electronic signature, please review and electronically sign by following the link below.').execute();

            dsfs__DocuSign_Status__c dsStatus = [
                    SELECT Id
                    FROM dsfs__DocuSign_Status__c
                    WHERE VTD1_Monitoring_Visit__c = :mvWithFile.Id
                    ORDER BY LastModifiedDate DESC
                    LIMIT 1
            ];

            VT_D2_TNCatalogTasks.generateTasks(
                    new List<String>{
                            'T543'
                    }, new List<String>{
                            dsStatus.Id
                    },
                    null,
                    new Map<String, String>{
                            'T543' + dsStatus.Id => mvWithFile.VTR5_MRRSignedMVReport__c
                    }
            );

            Task sentTask = [
                    SELECT Id,
                            VTD1_Status_Is_changed__c
                    FROM Task
                    WHERE WhatId = :mvWithFile.Id
                    ORDER BY LastModifiedDate DESC
                    LIMIT 1
            ];

            status = String.valueOf(sentTask.VTD1_Status_Is_changed__c) == String.valueOf(System.now().format('yyyy-MM-dd')) ? true : false;

            mvWithFile.VTR5_FollowUpLetterSubmitted__c = true;
            update mvWithFile;
        }

        return status;
    }

    private static Boolean resendVisitReport(Id recordId) {

        Datetime taskCreatedDate = [
                SELECT CreatedDate
                FROM Task
                WHERE VTR2_Monitoring_Visit_del__c = :recordId
                ORDER BY CreatedDate DESC
                LIMIT 1
        ].CreatedDate;
        ContentDocumentLink lastUploadedFile = [
                SELECT ContentDocumentId, ContentDocument.CreatedDate
                FROM ContentDocumentLink
                WHERE LinkedEntityId = :recordId
                ORDER BY ContentDocument.CreatedDate DESC
                LIMIT 1
        ];
        Id docId = lastUploadedFile.ContentDocumentId;
        if (lastUploadedFile.ContentDocument.CreatedDate > taskCreatedDate) {
            generateTaskAndDsfs(recordId, docId);
            return true;
        }
        return false;
    }
    @TestVisible
    private static void generateTaskAndDsfs(Id mvrId, Id docId) {
        VTD1_Monitoring_Visit__c mvr = [
                SELECT VTR5_SignMVReportTaskSent__c,
                        VTR5_MRRSignedMVReport__c,
                        VTD1_Remote_Monitoring_Visit_Report_Stat__c
                FROM VTD1_Monitoring_Visit__c
                WHERE Id = :mvrId
        ];
        Id signedMRR = mvr.VTR5_MRRSignedMVReport__c;
        VT_D2_TNCatalogTasks.generateTasks(
                new List<String>{
                        TN_CATALOG_CODE_FOR_TASK
                }, new List<Id>{
                        mvrId
                }, null, new Map<String, String>{
                        TN_CATALOG_CODE_FOR_TASK + mvrId => signedMRR
                }
        );
        new VT_R5_SendDocstoDocuSign(
                mvrId,
                docId,
                signedMRR,
                'Documents for your DocuSign Signature',
                'I am sending you this request for your electronic signature,' +
                        ' please review and electronically sign by following the link below.')
                .executeFuture();
        mvr.VTR5_SignMVReportTaskSent__c = true;
        mvr.VTD1_Remote_Monitoring_Visit_Report_Stat__c = 'Submitted';
        update mvr;
    }
}