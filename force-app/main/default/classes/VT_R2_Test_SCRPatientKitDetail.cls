/**
 * Created by user on 11.04.2019.
 */

@IsTest
public with sharing class VT_R2_Test_SCRPatientKitDetail {
    @TestSetup
    static void setup() {
        Test.startTest();
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
        Test.stopTest();
    }
    @IsTest
    public static void test() {
        Case caseObj = [SELECT Id FROM Case LIMIT 1];
        VTD1_Order__c delivery = new VTD1_Order__c(VTD1_Case__c = caseObj.Id, VTD1_Status__c = 'Shipped');
        VTD1_Order__c delivery2 = new VTD1_Order__c(VTD1_Case__c = caseObj.Id,
                                                    VTD1_Status__c = 'Delivered',
                                                    VTR2_Destination_Type__c = 'Site',
                                                    VTR2_KitsGivenToPatient__c = false);
        insert delivery;
        insert delivery2;
        VTD1_Patient_Kit__c kit = new VTD1_Patient_Kit__c(VTD1_Case__c = caseObj.Id, VTD1_Patient_Delivery__c = delivery.Id);
        insert kit;
        VTD1_Patient_Kit__c kit2 = new VTD1_Patient_Kit__c(VTD1_Case__c = caseObj.Id, VTD1_Patient_Delivery__c = delivery2.Id);
        insert kit2;
        IMP_Replacement_Form__c replacementForm = new IMP_Replacement_Form__c(VTD1_Replace__c = kit.Id, VTD1_Case__c = caseObj.Id);
        insert replacementForm;
        Task task = new Task(Status = 'Open', WhatId = replacementForm.Id, VTD2_Task_Unique_Code__c = '510', OwnerId = UserInfo.getUserId());
        insert task;
        VT_R2_SCRPatientKitDetail.getKitData(kit.Id);
        System.assert(!String.isEmpty(VT_R2_SCRPatientKitDetail.getKitData(kit2.Id)));
        Test.startTest();
        VT_R2_SCRPatientKitDetail.getCurrentDateTime();
        VT_R2_SCRPatientKitDetail.confirmHandoff(delivery.Id);
        VT_R2_SCRPatientKitDetail.confirmMarkAsDelayed(delivery.Id);
        VT_R2_SCRPatientKitDetail.confirmRecall(kit.Id);
        VT_R2_SCRPatientKitDetail.confirmReplacementForm(replacementForm.Id, 'Yes', '');
        VT_R2_SCRPatientKitDetail.confirmReplacementForm(replacementForm.Id, 'No', '');
        Test.stopTest();
        System.assert(String.isEmpty(VT_R2_SCRPatientKitDetail.getKitData(null)));
        System.assert(String.isEmpty(VT_R2_SCRPatientKitDetail.updateKit(kit.Id, null, null)));
    }

    @IsTest
    public static void exceptionTest (){
        System.assertEquals('List has no rows for assignment to SObject', VT_R2_SCRPatientKitDetail.confirmHandoff(''));
        System.assertEquals('List has no rows for assignment to SObject', VT_R2_SCRPatientKitDetail.confirmMarkAsDelayed(''));
        System.assertEquals('List has no rows for assignment to SObject', VT_R2_SCRPatientKitDetail.confirmRecall(''));
        System.assertEquals('List has no rows for assignment to SObject', VT_R2_SCRPatientKitDetail.confirmReplacementForm('','',''));
        System.assertEquals('Invalid id: ', VT_R2_SCRPatientKitDetail.updateKit('','',''));
    }
}