/**
 * Created by User on 19/05/14.
 */
@isTest
public with sharing class VT_D1_ProtocolDeviationTriggerTest {


    // Conquerors
    // Jira Ref: SH-17437 Akanksha Singh
    // Description: Only Assigned CRA should be able to create Protocol Deviation record
    @TestSetup
    static void testSetup() {
        Test.startTest();
        //Create Study
        HealthCloudGA__CarePlanTemplate__c study = VT_D1_TestUtils.prepareStudy(1);
        //Create CRA User
        User CRAUser = VT_D1_TestUtils.createUserByProfile('CRA', 'CRATestUser');       
        VT_D1_TestUtils.persistUsers();
        // Create Virtual Site
        Virtual_Site__c virtualSite = new Virtual_Site__c(Name = 'TestSite', VTD1_Study__c= study.Id,VTD1_Study_Site_Number__c = '1');
        insert virtualSite;
        
        Test.stopTest();
        
        // Create study team member
        List<Study_Team_Member__c> stmList = new List<Study_Team_Member__c>();
        Study_Team_Member__c stmCRA = new Study_Team_Member__c(User__c = CRAUser.Id, Study__c = study.Id);
        stmList.add(stmCRA);
        insert stmList;
        
        // Adde CRA SSTM on site
        List<Study_Site_Team_Member__c> sstmList = new List<Study_Site_Team_Member__c>();
        sstmList.add(new Study_Site_Team_Member__c(VTR5_Associated_CRA__c = stmCRA.Id,VTD1_SiteID__c = virtualSite.Id));
        insert sstmList;
        
    }
    // End of code: Conquerors


    public static void doTest(){
        DomainObjects.VTD1_Protocol_Deviation_t protocolDeviationT= new DomainObjects.VTD1_Protocol_Deviation_t();
        VTD1_Protocol_Deviation__c pd = (VTD1_Protocol_Deviation__c) protocolDeviationT.persist();
        pd.VTD1_Other_Type__c = 'test';
        update pd;
        delete pd;
    }

    // Conquerors
    // Jira Ref: SH-17437 Akanksha Singh
    // Description: Only Assigned CRA should be able to create Protocol Deviation record
    @IsTest
    static void testBeforeInsertPD(){

        //Fetch Virtual site
        Virtual_Site__c oldVS = [SELECT Id,
                                VTD1_Study__c
                                FROM Virtual_Site__c 
                                WHERE Name = 'TestSite'
                                LIMIT 1];
        Id virId = oldVS.Id;
        
        //Fetch existing CRA of related virtual site
        List<Study_Team_Member__c> studyTeamMembers = [SELECT Id, 
                                                       RecordType.Name, 
                                                       Study__c, 
                                                       User__c, 
                                                       User__r.Profile.Name 
                                                       FROM Study_Team_Member__c 
                                                       WHERE Study__c = :oldVS.VTD1_Study__c 
                                                       AND User__r.Profile.Name = 'CRA' LIMIT 1];
        

        Id craUserId = studyTeamMembers[0].User__c;
        VTD1_Protocol_Deviation__c pd  = new VTD1_Protocol_Deviation__c();
        pd.VTD1_Virtual_Site__c = virId;
        pd.VTD1_Site_Subject_Level__c = 'Site';
        
        System.runAs(new User(Id = craUserId)){
            try{
                insert pd;
            }catch(Exception exc){
                System.assert(exc.getMessage().containsIgnoreCase(System.Label.VTR5_notAssociatedCRA));
            }
        }
    }       
    // End of code: Conquerors
}