public with sharing class VT_R4_ConstantsHelper_ProfilesSTM {

    public static final String PG_PROFILE_NAME = 'Patient Guide';
    public static final String PI_PROFILE_NAME = 'Primary Investigator';
    public static final String PATIENT_PROFILE_NAME = 'Patient';
    public static final String CAREGIVER_PROFILE_NAME = 'Caregiver';
    public static final Set<String> HIPAA_PROFILES = new Set<String>{PATIENT_PROFILE_NAME, CAREGIVER_PROFILE_NAME};
    public static final String TMA_PROFILE_NAME = 'Therapeutic Medical Advisor';
    public static final String SC_PROFILE_NAME = 'Study Concierge';
    public static final String CM_PROFILE_NAME = 'Monitor';
    public static final String CRA_PROFILE_NAME = 'CRA';
    public static final String SCR_PROFILE_NAME = 'Site Coordinator';
    public static final String MRR_PROFILE_NAME = 'Monitoring Report Reviewer';
    public static final String PMA_PROFILE_NAME = 'Project Management Analyst';
    public static final String PL_PROFILE_NAME = 'Project Lead';
    public static final String VTSL_PROFILE_NAME = 'Virtual Trial Study Lead';
    public static final String RE_PROFILE_NAME = 'Recruitment Expert';
    public static final String SA_PROFILE_NAME = 'Study Admin';
    public static final String ADMIN_PROFILE_NAME = 'System Administrator';

    public static final String RS_PROFILE_NAME = 'Regulatory Specialist';
    public static final String VTHO_PROFILE_NAME = 'Virtual Trial Head of Operations';

    public static final String AUDITOR_PROFILE_NAME = 'Auditor'; // Added to enable sharing for Auditor Profile, Epic# SH-8126

    public static final Set<String> INTERNAL_PROFILES = new Set<String>{
            CRA_PROFILE_NAME,
            CM_PROFILE_NAME,
            MRR_PROFILE_NAME,
            PG_PROFILE_NAME,
            PI_PROFILE_NAME,
            PL_PROFILE_NAME,
            PMA_PROFILE_NAME,
            RE_PROFILE_NAME,
            RS_PROFILE_NAME,
            SCR_PROFILE_NAME,
            SA_PROFILE_NAME,
            ADMIN_PROFILE_NAME,
            TMA_PROFILE_NAME,
            VTHO_PROFILE_NAME,
            VTSL_PROFILE_NAME,
            SC_PROFILE_NAME,
            AUDITOR_PROFILE_NAME //R5.5
    };

    // R5.5 These people can review eCOA diary
    public static final Set<String> SITE_STAFF = new Set<String> {
            PI_PROFILE_NAME,
            SCR_PROFILE_NAME,
            PG_PROFILE_NAME,
            SC_PROFILE_NAME
    };

    public static final String SSTM_SCR_TYPE_PRIMARY = 'Primary';
    public static final String SSTM_SCR_TYPE_BACKUP = 'Backup';

    public static final String CANDIDATE_PATIENT_CAREGIVER_STATUS_SUCCESS = 'Success';
    public static final String CANDIDATE_PATIENT_CAREGIVER_STATUS_FAILURE = 'Failure';

    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_PI = VT_D1_HelperClass.getRTId('Study_Team_Member__c', 'PI');
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_PG = VT_D1_HelperClass.getRTId('Study_Team_Member__c', 'PG');
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_SC = VT_D1_HelperClass.getRTId('Study_Team_Member__c', 'SC');
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_CM = VT_D1_HelperClass.getRTId('Study_Team_Member__c', 'CM');
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_TMA = VT_D1_HelperClass.getRTId('Study_Team_Member__c', 'TMA');
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_SCR = VT_D1_HelperClass.getRTId('Study_Team_Member__c', 'VTR2_Site_Coordinator');
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_CRA = VT_D1_HelperClass.getRTId('Study_Team_Member__c', 'VTR2_CRA');
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_PL = VT_D1_HelperClass.getRTId('Study_Team_Member__c', 'PL');
    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_RE = VT_D1_HelperClass.getRTId('Study_Team_Member__c', 'RE');

    public static final String RECORD_TYPE_ID_STUDY_TEAM_MEMBER_Auditor = VT_D1_HelperClass.getRTId('Study_Team_Member__c', 'Auditor'); // Added to enable sharing for Auditor Profile, Epic# SH-8126

    public static final String RECORD_TYPE_ID_SITE_METRICS_PATIENT_BY_STATUS_PI = VT_D1_HelperClass.getRTId('VTD1_SiteMetricsPatientsbyStatus__c', 'VTR2_PI');
    public static final String RECORD_TYPE_ID_SITE_METRICS_PATIENT_BY_STATUS_VS = VT_D1_HelperClass.getRTId('VTD1_SiteMetricsPatientsbyStatus__c', 'VTR2_VS');

}