/**
 * @author Ruslan Mullayanov
 */

public class VT_R2_RequestBuilder_AdHocReplacement implements VT_D1_RequestBuilder {
    
    // Data classes: ---------------------------------------------------------------------------------------------------
    public class Delivery {
        public String protocolId;
        public String subjectId;
        public String shipmentId;
        public String shipmentName;
        public List<Kit> kits = new List<Kit>();
        public AuditLog auditLog;
    }

    public class Kit {
        public String kitName;
        public String materialId;
        public String replacementMaterialId;
        public Integer quantity;
        public String kitType;
    }
    
    public class AuditLog {
        public String UserId;
        public Datetime Timestamp;
        
        public AuditLog(){
            UserId = UserInfo.getUserId();
            Timestamp = Datetime.now();
        }
    }
    
    private Id orderId;
    
    public VT_R2_RequestBuilder_AdHocReplacement(Id orderId) {
        this.orderId = orderId;
    }
    
    // Build logic: ----------------------------------------------------------------------------------------------------
    public String buildRequestBody() {
        VTD1_Order__c orderDelivery = [
                SELECT Id, VTD1_ShipmentId__c,
                        VTD1_Case__c,
                        VTD1_Case__r.VTD1_Subject_ID__c,
                        VTD1_Case__r.VTD1_Study__r.Name,
                        VTD1_Status__c,
                        Name
                FROM VTD1_Order__c
                WHERE Id = :orderId
        ];
        Delivery deliveryObj = new Delivery();
        deliveryObj.protocolId = orderDelivery.VTD1_Case__r.VTD1_Study__r.Name;
        deliveryObj.subjectId = orderDelivery.VTD1_Case__r.VTD1_Subject_ID__c;
        deliveryObj.shipmentId = orderDelivery.VTD1_ShipmentId__c;
        deliveryObj.shipmentName = orderDelivery.Name;
        List<VTD1_Patient_Kit__c> patientKits = [
                SELECT VTD1_Kit_Name__c,
                        VTD2_MaterialId__c,
                        VTR2_Quantity__c,
                        VTD1_Kit_Type__c,
                        VTD1_Delivery_Status__c
                FROM VTD1_Patient_Kit__c
                WHERE VTD1_Patient_Delivery__c =: orderId
        ];
        if (!patientKits.isEmpty()) {
            for (VTD1_Patient_Kit__c patientKit : patientKits) {
                Kit kit = new Kit();
                kit.kitName = patientKit.VTD1_Kit_Name__c;
                kit.materialId = patientKit.VTD2_MaterialId__c;
                kit.quantity = (Integer)patientKit.VTR2_Quantity__c;
                kit.kitType = patientKit.VTD1_Kit_Type__c;
                deliveryObj.kits.add(kit);
            }
        }
        deliveryObj.auditLog = new AuditLog();
        
        String body = JSON.serializePretty(deliveryObj, true);
        //System.debug('AdHocReplacement request body: '+body);
        return body;
    }
}