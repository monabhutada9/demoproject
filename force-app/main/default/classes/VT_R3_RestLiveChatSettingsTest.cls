/**
 * @description Created by user on 02-Jul-19.
 */
@isTest
public with sharing class VT_R3_RestLiveChatSettingsTest {

    public static void getSettingsTest() {
        String responsString;
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/LiveChatSettings/';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = response;

        responsString = VT_R3_RestLiveChatSettings.getSettings();
        System.assert(!String.isEmpty(responsString));
    }
}