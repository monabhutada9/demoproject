/**
 * Created by Danylo Belei on 20.05.2019.
 */

@IsTest
public with sharing class VT_D2_TranslateParametersTest {
    
    public static  void testBehavior() {
        Case carePlan = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan' ORDER BY CreatedDate DESC LIMIT 1];
        VTD1_ProtocolVisit__c pVisit = new VTD1_ProtocolVisit__c(
                VTD1_Visit_Checklist__c = 'TEST',
                VTR2_Visit_Participants__c = VTD1_ProtocolVisit__c.VTR2_Visit_Participants__c.getDescribe().getPicklistValues()[0].getValue()
        );
        insert pVisit;

        VTD1_Actual_Visit__c visit = new VTD1_Actual_Visit__c(
                VTD1_Case__c = carePlan.Id,
                VTD1_Protocol_Visit__c = pVisit.Id,
                VTD1_Reason_for_Request__c = 'TestVideoPanel',
                VTD1_Additional_Visit_Checklist__c = 'TEST'
        );
        insert visit;

        User patient = [SELECT Id, ContactId FROM User WHERE Profile.Name=:VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME AND IsActive=TRUE AND ContactId!=NULL LIMIT 1];
        VTD1_NotificationC__c patientNotification1 = new VTD1_NotificationC__c(
                VTD1_Receivers__c = patient.Id,
                Number_of_unread_messages__c = 3,
                VTD1_Parameters__c = 'Title__c;Test1;America/New_York;America/New_York11;#date;#VisitName;;',
                Message__c='test1',
                Title__c = 'Title'

        );
        insert patientNotification1;
        VTD1_NotificationC__c patientNotification2 = new VTD1_NotificationC__c(
                VTD1_Receivers__c = patient.Id,
                Number_of_unread_messages__c = 3,
                VTD1_Parameters__c = patient.Id + 'ContactId;' + patientNotification1 + 'Title__c' + patient.Id + 'Cases;',
                VTD2_VisitID__c = visit.Id,
                Message__c='test2',
                Title__c = 'Title'
        );
        insert patientNotification2;
        List<VTD1_NotificationC__c> notifList = [SELECT VTD1_Receivers__c, Number_of_unread_messages__c, VTD1_Parameters__c, VTD2_VisitID__c, Title__c, Message__c FROM VTD1_NotificationC__c];
        VT_D1_TranslateHelper.translate(notifList);
        
    }
}