/**
 * Created by user on 10/6/2020.
 */

@isTest
public without sharing class VT_R5_eCoaScheduleRestServiceTest {

    @isTest
    private static void httpPostErrorMissingFieldsTest() {

        VT_R5_eCoaScheduleRestService.ScheduleResponse reqst = new VT_R5_eCoaScheduleRestService.ScheduleResponse();
        reqst.subjectsScheduleStatus = new List<VT_R5_eCoaScheduleRestService.ScheduleItem>();
        VT_R5_eCoaScheduleRestService.ScheduleItem testList = new VT_R5_eCoaScheduleRestService.ScheduleItem();

        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );

        DomainObjects.User_t user = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5));

        Case cas = (Case) new DomainObjects.Case_t()
                .addStudy(study)
                .addUser(user)
                .persist();

        Contact cont =  (Contact )new DomainObjects.Contact_t()
                .setVTD1_Clinical_Study_Membership(cas.Id)
                .persist();

        testList.userId = user.Id;
        testList.subjectGuid = 'responseRule_abf191cf-a78d-47a5-b723-5c4c55945fae';
        testList.clinicalStudyMembership = cas.Id;
        testList.stopDiary = true;
        testList.ruleGuid = 'responseRule_abf291ff-a76d-43a0-b733-5c4c55945fae';
        testList.diaryName = 'test';
        testList.windowBegin = 2;
        testList.windowEnd = 3;
        reqst.subjectsScheduleStatus.add(testList);
        String JsonMsg = JSON.serialize(reqst);
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/eCoaScheduleService';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        VT_R5_eCoaScheduleRestService.ScheduleResponse resp = new VT_R5_eCoaScheduleRestService.ScheduleResponse();
            VT_R5_eCoaScheduleRestService.processSchedule();
        System.assertEquals(200,  res.statusCode);
        Test.stopTest();
    }
}