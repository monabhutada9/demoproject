/**
* @author: Carl Judge
* @date: 26-Sep-18
* @description: When an out-of-office Event is added for a Patient Guide, if PG is a primary PG anywhere, inform backup PGs of the Out-Of-Office
**/


    public without sharing class VT_D1_BackupPgNotificationGenerator {

    private List<Event> events = new List<Event>();
    private List<Event> oooPgEvents = new List<Event>();
    private Map<Id, List<Case>> casesByPrimaryPg = new Map<Id, List<Case>>();

    public void generateNotifications(List<Event> events) {
        this.events = events;
        getOooPgEvents();

        if (!this.oooPgEvents.isEmpty()) {
            getCases();

            if (!this.casesByPrimaryPg.isEmpty()) {
                createNotificationsFromEvents();
            }
        }
    }

    private void getOooPgEvents() {
        this.oooPgEvents = [
            SELECT Id, OwnerId, ActivityDate, EndDateTime
            FROM Event
            WHERE Id IN :this.events
            AND RecordType.DeveloperName = 'OutOfOffice'
            AND Owner.Profile.Name = 'Patient Guide'
        ];
        system.debug(oooPgEvents);
    }

    private void getCases() {
        List<Id> ownerIds = new List<Id>();
        for (Event item : this.oooPgEvents) {
            ownerIds.add(item.OwnerId);
        }

        system.debug(ownerIds);

        for (Case item : [
            SELECT Id, VTD1_Primary_PG__c, VTD1_Secondary_PG__c, VTD1_Primary_PG__r.Name, VTD1_Secondary_PG__r.Name,
                VTD1_Patient_User__r.Name
            FROM Case
            WHERE VTD1_Secondary_PG__c != null
            AND VTD1_Primary_PG__c IN :ownerIds
        ]) {
            if (!this.casesByPrimaryPg.containsKey(item.VTD1_Primary_PG__c)) {
                this.casesByPrimaryPg.put(item.VTD1_Primary_PG__c, new List<Case>());
            }
            this.casesByPrimaryPg.get(item.VTD1_Primary_PG__c).add(item);
        }
    }

    private void createNotificationsFromEvents() {
        List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();

        for (Event ev : this.oooPgEvents) {
            Map<Id, ConnectApi.BatchInput> batchInputsByUserId = new Map<Id, ConnectApi.BatchInput>(); // only want one notification per backup
            for (Case cas : this.casesByPrimaryPg.get(ev.Ownerid)) {
                if (!batchInputsByUserId.containsKey(cas.VTD1_Secondary_PG__c)) {
                    ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                    ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();

                    messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
                    messageBodyInput.messageSegments.add(VT_D1_MentionHelper.getMentionSeg(cas.VTD1_Secondary_PG__c));
                    messageBodyInput.messageSegments.add(VT_D1_MentionHelper.getTextSeg(
                        ' Please be aware that you are backup for ' + cas.VTD1_Primary_PG__r.Name +
                        ' while they are out of the office from ' + ev.ActivityDate.format() +
                        ' to ' + ev.EndDateTime.dateGmt().format()
                    ));

                    feedItemInput.body = messageBodyInput;
                    feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                    feedItemInput.subjectId = cas.VTD1_Secondary_PG__c;

                    batchInputsByUserId.put(cas.VTD1_Secondary_PG__c, new ConnectApi.BatchInput(feedItemInput));
                }
            }
            batchInputs.addAll(batchInputsByUserId.values());
        }

        if (! Test.isRunningTest()) {
            ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
        }
    }
}