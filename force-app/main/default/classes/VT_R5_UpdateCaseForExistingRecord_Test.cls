@isTest
private class VT_R5_UpdateCaseForExistingRecord_Test {
    @testSetup
    private static void setupMethod(){
        /*
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        DomainObjects.Study_t domainStudy = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );
        HealthCloudGA__CandidatePatient__c candidatePatient1 = (HealthCloudGA__CandidatePatient__c) new DomainObjects.HealthCloudGA_CandidatePatient_t()
                .setRr_firstName('Mike')
                .setRr_lastName('BirnBirnBirn')
                .setConversion('Converted')
                .setCaregiverEmail(DomainObjects.RANDOM.getEmail())
                .addStudy(domainStudy)
                .persist();
        User userPG1 = (User) new DomainObjects.User_t()
                //.setUsername('PGTest@acme.com')
                .setProfile('Patient Guide').persist();
        DomainObjects.Account_t scrAccount1 = new DomainObjects.Account_t();

        DomainObjects.Contact_t scrContact1 = new DomainObjects.Contact_t().addAccount(scrAccount1);

        User scrUser1 = (User) new DomainObjects.User_t()
                .setProfile('Site Coordinator')
                .setUsername('SCRTest@acme.com')
                .addContact(scrContact1)
                .persist();

        DomainObjects.Account_t patientAccount1 = new DomainObjects.Account_t();

        DomainObjects.Contact_t patientContact1 = new DomainObjects.Contact_t().addAccount(patientAccount1);

        User patientUser1 = (User) new DomainObjects.User_t()
                .setProfile('Patient')
                .setUsername('PatientTest@acme.com')
                .addContact(patientContact1)
                .persist();HealthCloudGA__CandidatePatient__c candidatePatient = (HealthCloudGA__CandidatePatient__c) new DomainObjects.HealthCloudGA_CandidatePatient_t()
                .setRr_firstName('Mike')
                .setRr_lastName('BirnBirnBirn')
                .setConversion('Converted')
                .setCaregiverEmail(DomainObjects.RANDOM.getEmail())
                .addStudy(domainStudy)
                .persist();
        Test.stopTest();

        User userPG = (User) new DomainObjects.User_t()
                //.setUsername('PGTest@acme.com')
                .setProfile('Patient Guide').persist();
        DomainObjects.Account_t scrAccount = new DomainObjects.Account_t();

        DomainObjects.Contact_t scrContact = new DomainObjects.Contact_t().addAccount(scrAccount);

        User scrUser = (User) new DomainObjects.User_t()
                .setProfile('Site Coordinator')
                .setUsername('SCRTest@acme.com')
                .addContact(scrContact)
                .persist();

        DomainObjects.Account_t patientAccount = new DomainObjects.Account_t();

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t().addAccount(patientAccount);

        User patientUser = (User) new DomainObjects.User_t()
                .setProfile('Patient')
                .setUsername('PatientTest@acme.com')
                .addContact(patientContact)
                .persist();
        DomainObjects.User_t userPI = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setUsername('PITest@acme.com')
                .setProfile('Primary Investigator');
        userPI.persist();


        Case cs = [SELECT Id, VTR2_SiteCoordinator__c, VTD1_Primary_PG__c, VTD1_PI_user__c FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];
        Id caseId = cs.Id;
        cs.VTD1_Enrollment_Date__c = Datetime.now().addDays(-5).date();
        cs.VTD1_Primary_PG__c = userPG.Id;
        cs.VTR2_SiteCoordinator__c = scrUser.Id;
        cs.VTD1_PI_user__c = userPI.Id;
        update cs;

        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Range__c = 4;
        protocolVisit.VTD1_VisitOffset__c = 2;
        protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisit.VTD1_Study__c = (Id)domainStudy.toObject().get('Id');
        protocolVisit.VTD1_VisitType__c = 'Labs';
        insert protocolVisit;

        VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
        actualVisit.VTD1_Protocol_Visit__c = protocolVisit.Id;
        actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
        actualVisit.Sub_Type__c = 'Phlebotomist';
        actualVisit.VTD1_Case__c = caseId;
        insert actualVisit;
 */
    }
    @isTest
    private static void testUpdateCaseFieldForExistingRecord(){
        /*
         Test.startTest();
         HealthCloudGA__CarePlanTemplate__c study = [SELECT Id,
                                                         name,
                                                         VTR5_Baseline_Visit_After_A_R_Flag__c
                                                         FROM HealthCloudGA__CarePlanTemplate__c
                                                         LIMIT 1];
        study.VTR5_Baseline_Visit_After_A_R_Flag__c = true;
        study.VTD1_Randomized__c = 'No';
        study.VTR3_LTFU__c = false;
        study.VTD2_Washout_Run_In__c = false;
        update study;

        List<Case> cases = [SELECT Id
                                FROM Case
                                WHERE RecordTypeId =: VT_R4_ConstantsHelper_AccountContactCase.RECORD_TYPE_ID_CASE_CAREPLAN
                            ];

        // Create list of Protocol visits with onboarding and protocol record types
        List<VTD1_ProtocolVisit__c> protocolvistList = new List<VTD1_ProtocolVisit__c>();

        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Range__c = 4;
        protocolVisit.VTD1_VisitOffset__c = 2;
        protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisit.RecordTypeId = VT_R4_ConstantsHelper_RecordTypes.RECORD_TYPE_ID_PROTOCOL_VISIT_ONBOARDING;
        protocolVisit.VTD1_Onboarding_Type__c = 'Consent';
        protocolVisit.VTD1_VisitType__c = 'Study Team';
        protocolVisit.VTD1_VisitNumber__c = 'o1';
        protocolVisit.VTD1_Study__c = study.Id;
        protocolvistList.add(protocolVisit);

        VTD1_ProtocolVisit__c protocolVisit1 = new VTD1_ProtocolVisit__c();
        protocolVisit1.VTD1_Range__c = 4;
        protocolVisit1.VTD1_VisitOffset__c = 2;
        protocolVisit1.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit1.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit1.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        protocolVisit1.VTD1_VisitNumber__c = 'v1';
        protocolVisit1.VTD1_VisitType__c = 'Labs';
        protocolVisit1.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit1.RecordTypeId = VT_R4_ConstantsHelper_RecordTypes.RECORD_TYPE_ID_PROTOCOL_VISIT_PROTOCOL;
        protocolVisit1.VTD1_Study__c = study.Id;
        protocolvistList.add(protocolVisit1);


        VTD1_ProtocolVisit__c screeningPVisit = new VTD1_ProtocolVisit__c();
        screeningPVisit.VTD1_Range__c = 4;
        screeningPVisit.VTD1_VisitOffset__c = 2;
        screeningPVisit.VTD1_VisitOffsetUnit__c = 'days';
        screeningPVisit.VTD1_VisitDuration__c = '30 minutes';
        screeningPVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient';
        screeningPVisit.RecordTypeId = VT_R4_ConstantsHelper_RecordTypes.RECORD_TYPE_ID_PROTOCOL_VISIT_ONBOARDING;
        screeningPVisit.VTD1_Onboarding_Type__c = 'Screening';
        screeningPVisit.VTD1_VisitNumber__c = 'o2';
        screeningPVisit.VTD1_Study__c = study.Id;
        protocolvistList.add(screeningPVisit);
        insert protocolvistList;


        // Create a list Of Actual Visit types with Scheduled and unscheduled type
        List<VTD1_Actual_Visit__c> visitList = new List<VTD1_Actual_Visit__c>();

        VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
        actualVisit.VTD1_Protocol_Visit__c = protocolvistList[0].Id;
        actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
        actualVisit.VTD1_Case__c = cases[0].id;
        visitList.add(actualVisit);

        VTD1_Actual_Visit__c actualVisitUns = new VTD1_Actual_Visit__c();
        actualVisitUns.VTD1_Protocol_Visit__c = protocolvistList[1].Id;
        actualVisitUns.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT;
        actualVisitUns.RecordTypeId = VT_R4_ConstantsHelper_VisitsEvents.RECORD_TYPE_ID_ACTUAL_VISIT_LABS;
        actualVisitUns.Sub_Type__c = 'Phlebotomist';
        actualVisitUns.VTD1_Case__c = cases[0].id;
        visitList.add(actualVisitUns);

        VTD1_Actual_Visit__c actualVisit1 = new VTD1_Actual_Visit__c();
        actualVisit1.VTD1_Protocol_Visit__c = protocolvistList[2].Id;
        actualVisit1.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_FUTURE_VISIT;
        actualVisit1.VTD1_Case__c = cases[0].id;
        visitList.add(actualVisit1);
        insert visitList;

        Case caseDb = [SELECT Id,
                              VTR5_FirstOnboardingVisitCompleteDate__c,
                              VTR5_LastOnboardingVisitCompleteDate__c
                        FROM Case
                        WHERE Id =: visitList[0].VTD1_Case__c];

        VTD1_Actual_Visit__c actualVisitDb = [SELECT Id,
                                                     Name,
                                                     VTD1_Visit_Completion_Date__c,
                                                     VTD1_Status__c
                                                FROM VTD1_Actual_Visit__c
                                                WHERE Id =: visitList[0].Id];

            // Now completed the first onboarding visit to Completed
            VTD1_Actual_Visit__c acv = visitList[0];
            acv.id = visitList[0].id;
            acv.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED;
            acv.VTD1_Visit_Completion_Date__c = System.today();
            update acv;

            VTD1_Actual_Visit__c updatedActualVisit = [SELECT Id,
                                                            Name,
                                                            VTD1_Visit_Completion_Date__c,
                                                            VTD1_Status__c,
                                                            VTD1_Case__c
                                                        FROM VTD1_Actual_Visit__c
                                                        WHERE Id =: acv.Id];

            Case updatedCase = [SELECT Id,
                                    VTR5_FirstOnboardingVisitCompleteDate__c,
                                    VTR5_LastOnboardingVisitCompleteDate__c
                                FROM Case
                                WHERE Id =: acv.VTD1_Case__c];

            System.assertEquals(VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED,
                                updatedActualVisit.VTD1_Status__c,
                                'FIrst Actual Visit is completed');
            System.assertEquals(updatedActualVisit.VTD1_Visit_Completion_Date__c,
                                updatedCase.VTR5_FirstOnboardingVisitCompleteDate__c,
                                'First onboarding Completion field is not null');
            System.assertEquals(null,
                                updatedCase.VTR5_LastOnboardingVisitCompleteDate__c,
                                'Last onboarding Completion field is null');


            // This is the last actual visit after completion this last Completion date field will update
            VTD1_Actual_Visit__c acv1 = visitList[2];
            acv1.id = visitList[2].id;
            acv.VTD1_Visit_Completion_Date__c = System.today().addYears(-1);
            acv1.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED;
            update acv1;

            VTD1_Actual_Visit__c updatedLastActualVisit = [SELECT Id,
                                                                Name,
                                                                VTD1_Visit_Completion_Date__c,
                                                                VTD1_Status__c,
                                                                VTD1_Case__c
                                                            FROM VTD1_Actual_Visit__c
                                                            WHERE Id =: acv1.Id];

            Case updatedLastCase = [SELECT Id,
                                        VTR5_FirstOnboardingVisitCompleteDate__c,
                                        VTR5_LastOnboardingVisitCompleteDate__c
                                    FROM Case
                                    WHERE Id =: acv1.VTD1_Case__c];

            System.assertEquals(VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED,
                                updatedLastActualVisit.VTD1_Status__c,
                                'Last Actual Visit is completed');
            System.assertEquals(updatedLastActualVisit.VTD1_Visit_Completion_Date__c,
                                updatedLastCase.VTR5_LastOnboardingVisitCompleteDate__c,
                                'Last onboarding Completion field is not null');
        Test.stopTest();
        Test.startTest();
            VT_R5_UpdateCaseFieldForExistingRecord varBatch = new VT_R5_UpdateCaseFieldForExistingRecord();
            Id batchId = Database.executeBatch(varBatch);
        Test.stopTest(); */
    }
}