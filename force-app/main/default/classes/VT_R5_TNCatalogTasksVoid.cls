/**
 * Created by A. Yamayeu on 30.11.2020 for SH-21448
 */

public without sharing class VT_R5_TNCatalogTasksVoid {

    @InvocableMethod
    public static void generateTask(List<VT_D2_TNCatalogTasks.ParamsHolder> params) {
        List<Task> t = VT_D2_TNCatalogTasks.generateTask(params);
    }
}