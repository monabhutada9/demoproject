/**
* @author: Carl Judge
* @date: 20-Apr-20
* @description: Create eDiaries from eCOA JSON. Example JSON can be found attached to SH-9996
**/



public without sharing class VT_R5_eCOAeDiaryCreator {
    public static final String HEADER_RESPONSE_GUID = 'Response GUID';
    public static final String HEADER_SUBJECT_GUID = 'Subject GUID';
    public static final String HEADER_DIARY_GUID = 'Diary GUID';
    public static final String HEADER_RESPONSE_COMPLETED = 'Response Completed';
    public static final String HEADER_RESPONSE_STARTED = 'Response Started';
    public static final String HEADER_RECEIVED_TIME = 'Received Time';
    public static final String HEADER_RULE_TRIGGERED = 'Rule Triggered';
    public static final String HEADER_DIARY_NAME = 'Diary Name';
    public static final String HEADER_RULE_NAME = 'Rule Name';
    public static final String HEADER_WIDGET_TYPE = 'Widget Type';
    public static final String HEADER_WIDGET_VALUE = 'Value';
    public static final String HEADER_WIDGET_NAME = 'Widget Name';
    public static final String WIDGET_TYPE_CALCULATION = 'WIDGET_CALCULATION';
    public static final String HEADER_WIDGET_DATA_KEY = 'Widget Data Key';
    public static final String HEADER_CLINICIAN_NAME = 'Clinician Name';
    public static final String HEADER_REPORTING_USER = 'Reporting User';
    public static final String HEADER_SSO_ID = 'SSO ID';
    public static final String HEADER_EXTERNAL_ACTOR_ID = 'External Actor ID';
    public static final String HEADER_RULE_GUID = 'Rule GUID';
    public static final String HEADER_WIDGET_TYPE_INJECTED = 'injected';

    private Map<String, Integer> headerMap = new Map<String, Integer>();
    private List<List<String>> widgets = new List<List<String>>();
    private Map<String, List<List<String>>> groupedWidgets = new Map<String, List<List<String>>>(); // response(diary) => widgets => widget fields
    private List<String> subjectIds = new List<String>();
    private Map<String, User> userMap = new Map<String, User>(); // by eCOA subject guid
    private Map<String, VTD1_Survey__c> diariesByResponseGuid = new Map<String, VTD1_Survey__c>(); // indexed by eCOA response guid
    private Map<String, VTD1_Survey__c> diariesBySubjectAndRule = new Map<String, VTD1_Survey__c>();
    private Map<String, VTD1_Protocol_ePRO__c> protocolsByDiaryGuids = new Map<String, VTD1_Protocol_ePRO__c>();
    private Set<String> ruleGuids = new Set<String>();
    private List<VTD1_Survey__c> safetyConcernDiaries = new List<VTD1_Survey__c>();

    public VT_R5_eCOAeDiaryCreator(VT_R5_eCoaResponseExport.Response response) {
        widgets = response.data.rows;
        for (Integer i = 0; i < response.data.headers.size(); i++) {
            headerMap.put(response.data.headers[i], i);
        }
    }

    /**
     * Create eDiaries and answers
     */
    public void doCreate() {
        groupWidgets();
        if (!groupedWidgets.isEmpty()) {
            getUsers();
            getProtocols();
            createDiaries();
            refreshSchedules();
            upsertDiaries();
            createAnswers();
            triggerSafetyConcerns();
        }
    }

    /**
     * group rows(widgets) by response and get subject guids
     */
    private void groupWidgets() {
        for (List<String> widget : widgets) {
            String responseId = widget[headerMap.get(HEADER_RESPONSE_GUID)];
            if (!groupedWidgets.containsKey(responseId)) {
                groupedWidgets.put(responseId, new List<List<String>>());
                subjectIds.add(widget[headerMap.get(HEADER_SUBJECT_GUID)]);
            }
            groupedWidgets.get(responseId).add(widget);
        }
    }



    /**
     * get users based on eCOA subject guid
     */
    private void getUsers() {
        for (User usr : [
                SELECT Id,
                    VTR5_eCOA_Guid__c,
                    Contact.VTD1_Clinical_Study_Membership__c,
                    Contact.VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c,
                    Contact.VTD1_Clinical_Study_Membership__r.VTR2_SiteCoordinator__c,
                    Profile.Name,
                    Contact.Account.VTD2_Patient_s_Contact__r.VTD1_UserId__c

                FROM User
                WHERE VTR5_eCOA_Guid__c IN :subjectIds
        ]) {
            userMap.put(usr.VTR5_eCOA_Guid__c, usr);
        }
    }


    /**
     * Query protocol eDiaries, ready to match with created eDiaries
     */
    private void getProtocols() {
        Set<String> diaryGuids = new Set<String>();
        for (String responseId : groupedWidgets.keySet()) {
            List<String> firstWidgetOfDiary = groupedWidgets.get(responseId)[0];
            diaryGuids.add(firstWidgetOfDiary[headerMap.get(HEADER_DIARY_GUID)]);
        }

        if (!diaryGuids.isEmpty()) {
            for (VTD1_Protocol_ePRO__c protocol : [
                SELECT Id, VTR5_eCOADiaryGUID__c, VTR2_Protocol_Reviewer__c 
                FROM VTD1_Protocol_ePRO__c
                WHERE VTR5_eCOADiaryGUID__c IN :diaryGuids
            ]) {
                protocolsByDiaryGuids.put(protocol.VTR5_eCOADiaryGUID__c, protocol);
            }
        }
    }

    /**
     * create eDiaries, map by eCOA response ID so they can be joined with answers later
     */
    private void createDiaries() {
        List<VTR5_eCOA_Rule_to_Diary_Name_Mapping__mdt> mappingsList =  (List<VTR5_eCOA_Rule_to_Diary_Name_Mapping__mdt>) new VT_Stubber.ResultStub(
            'VT_R5_eCOAeDiaryCreator.ruleToDiaryMapping',
            [
                SELECT VTR5_RuleName__c, VTR5_DiaryName__c
                FROM VTR5_eCOA_Rule_to_Diary_Name_Mapping__mdt
            ]
        ).getResult();

        Map<String, String> ruleToDiaryName = new Map<String, String>();
        for (VTR5_eCOA_Rule_to_Diary_Name_Mapping__mdt mapping : mappingsList) {
            ruleToDiaryName.put(mapping.VTR5_RuleName__c, mapping.VTR5_DiaryName__c);
        }

        for (String responseId : groupedWidgets.keySet()) {
            List<String> firstWidgetOfDiary = groupedWidgets.get(responseId)[0];
            String subjectGuid = firstWidgetOfDiary[headerMap.get(HEADER_SUBJECT_GUID)];
            if (userMap.containsKey(subjectGuid)) {
                VTD1_Protocol_ePRO__c protocol = protocolsByDiaryGuids.get(firstWidgetOfDiary[headerMap.get(HEADER_DIARY_GUID)]);
                String protocolReviewer = protocol != null ? protocol.VTR2_Protocol_Reviewer__c : VT_D1_ConstantsHelper.PG_PROFILE_NAME;
                User usr = userMap.get(subjectGuid);
                String ruleName = firstWidgetOfDiary[headerMap.get(HEADER_RULE_NAME)];
                String diaryName = ruleToDiaryName.containsKey(ruleName)
                    ? ruleToDiaryName.get(ruleName)
                    : firstWidgetOfDiary[headerMap.get(HEADER_DIARY_NAME)];

                VTD1_Survey__c diary = new VTD1_Survey__c(
                    Name = diaryName,
                    VTR5_Response_GUID__c = firstWidgetOfDiary[headerMap.get(HEADER_RESPONSE_GUID)],
                    VTD1_Completion_Time__c = parseDatetime(firstWidgetOfDiary[headerMap.get(HEADER_RESPONSE_COMPLETED)]),
                    VTD1_Due_Date__c = parseDatetime(firstWidgetOfDiary[headerMap.get(HEADER_RESPONSE_COMPLETED)]),
                    VTD1_Date_Available__c = parseDatetime(firstWidgetOfDiary[headerMap.get(HEADER_RULE_TRIGGERED)]),
                    VTD1_Start_Time__c = parseDatetime(firstWidgetOfDiary[headerMap.get(HEADER_RESPONSE_STARTED)]),
                    VTR5_eCoa_ruleGuid__c = firstWidgetOfDiary[headerMap.get(HEADER_RULE_GUID)],
                    VTR5_eCoa_RuleName__c = firstWidgetOfDiary[headerMap.get(HEADER_DIARY_NAME)],
                    VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_COMPLETED,
                    VTD1_CSM__c = usr.Contact.VTD1_Clinical_Study_Membership__c,
                    VTD1_Protocol_ePRO__c = protocol != null ? protocol.Id : null,
                    VTR2_Reviewer_User__c = VT_R4_AbstractEproCreator.getReviewerForEDiary(
                        usr.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Primary_PG__c,
                        usr.Contact.VTD1_Clinical_Study_Membership__r.VTR2_SiteCoordinator__c,
                        protocolReviewer)
                );

                //if user that passed the diary is patient VTR5_CompletedBy__c = patientId, else VTR5_CompletedBy__c = caregiverId
                if (firstWidgetOfDiary[headerMap.get(HEADER_REPORTING_USER)] == 'subject') {
                    diary.VTR5_CompletedBy__c = firstWidgetOfDiary[headerMap.get(HEADER_SSO_ID)];
                } else if (firstWidgetOfDiary[headerMap.get(HEADER_REPORTING_USER)] == 'external') {
                    diary.VTR5_CompletedBy__c = firstWidgetOfDiary[headerMap.get(HEADER_EXTERNAL_ACTOR_ID)];
                }

                // link to pt/cg
                if (usr.Profile.Name == VT_R4_ConstantsHelper_Profiles.CAREGIVER_PROFILE_NAME && usr.Contact.Account.VTD2_Patient_s_Contact__r.VTD1_UserId__c != null) {
                    diary.VTD1_Caregiver_User_Id__c = usr.Id;
                    diary.VTD1_Patient_User_Id__c = usr.Contact.Account.VTD2_Patient_s_Contact__r.VTD1_UserId__c;
                } else {
                    diary.VTD1_Patient_User_Id__c = usr.Id;
                }

                // set clinician and record type
                if (firstWidgetOfDiary[headerMap.get(HEADER_CLINICIAN_NAME)] != null){
                    diary.VTR5_Clinician__c = firstWidgetOfDiary[headerMap.get(HEADER_CLINICIAN_NAME)];
                    diary.RecordTypeId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('VTR5_SourceForm').getRecordTypeId();
                } else {
                    diary.RecordTypeId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('VTR5_External').getRecordTypeId();
                }

                diariesByResponseGuid.put(responseId, diary);
                diariesBySubjectAndRule.put(subjectGuid + diary.VTR5_eCoa_ruleGuid__c, diary);
                ruleGuids.add(diary.VTR5_eCoa_ruleGuid__c);
            }
        }
    }

    /**
     * upsert diaries
     */
    private void upsertDiaries() {
        Set<String> updatedEDiaries = new Set<String>();
        for (VTD1_Survey__c existingDiary : [
            SELECT Id, VTD1_Patient_User_Id__r.VTR5_eCOA_Guid__c, VTR5_eCoa_ruleGuid__c, VTD1_Due_Date__c,
                VTD1_Status__c, VTR5_Response_GUID__c, VTD1_Date_Available__c 
            FROM VTD1_Survey__c
            WHERE VTD1_Patient_User_Id__r.VTR5_eCOA_Guid__c IN :userMap.keySet()
            AND VTR5_eCoa_ruleGuid__c IN :ruleGuids
            AND (VTD1_Status__c != :VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_COMPLETED OR VTR5_Response_GUID__c IN :diariesByResponseGuid.keySet())
            ORDER BY VTR5_Response_GUID__c NULLS LAST, VTD1_Due_Date__c ASC
        ]) {
            if (existingDiary.VTR5_Response_GUID__c != null) {
                if (diariesByResponseGuid.containsKey(existingDiary.VTR5_Response_GUID__c)) {
                    diariesBySubjectAndRule.get(existingDiary.VTR5_Response_GUID__c).Id = existingDiary.Id;
                    String key = existingDiary.VTD1_Patient_User_Id__r.VTR5_eCOA_Guid__c + existingDiary.VTR5_eCoa_ruleGuid__c;
                    updatedEDiaries.add(key);
                }
            } else {
                String key = existingDiary.VTD1_Patient_User_Id__r.VTR5_eCOA_Guid__c + existingDiary.VTR5_eCoa_ruleGuid__c;
                VTD1_Survey__c diaryBySubjectAndRule = !updatedEDiaries.contains(key) ? diariesBySubjectAndRule.get(key) : null;
                if (diaryBySubjectAndRule != null && diaryBySubjectAndRule.VTD1_Due_Date__c >= existingDiary.VTD1_Date_Available__c 
                                                  && diaryBySubjectAndRule.VTD1_Due_Date__c < existingDiary.VTD1_Due_Date__c) {
                    diariesBySubjectAndRule.get(key).Id = existingDiary.Id;
                    diariesBySubjectAndRule.get(key).VTD1_Due_Date__c = existingDiary.VTD1_Due_Date__c;
                    diariesBySubjectAndRule.get(key).VTD1_Date_Available__c = existingDiary.VTD1_Date_Available__c;
                    updatedEDiaries.add(key);
                }
            }
        }
        VT_D1_SurveyHandler.skipOxygenCloning = true;
        upsert diariesByResponseGuid.values();
    }

    /**
     * create ediary answers and link with eDiaries created earlier
     */
    private void createAnswers() {
        List<VTD1_Survey_Answer__c> answers = new List<VTD1_Survey_Answer__c>();

        for (String responseId : groupedWidgets.keySet()) {
            if (diariesByResponseGuid.containsKey(responseId)) {
                Integer questionNum = 1;
                for (List<String> widget : groupedWidgets.get(responseId)) {
                    String value = widget[headerMap.get(HEADER_WIDGET_VALUE)];

                    if(widget[this.headerMap.get(HEADER_WIDGET_TYPE)] != HEADER_WIDGET_TYPE_INJECTED){

                    if (widget[headerMap.get(HEADER_WIDGET_TYPE)] == WIDGET_TYPE_CALCULATION && widget[headerMap.get(HEADER_WIDGET_DATA_KEY)].startsWith('safety_')) {//widget name start with 'safety_' keyword
                        if (value != '0.00') {
                            diariesByResponseGuid.get(responseId).VTD1_Possible_ePRO_Safety_Concern__c = true; // will get updated when diaries are submitted
                            safetyConcernDiaries.add(diariesByResponseGuid.get(responseId));
                        }
                    } else if (widget[headerMap.get(HEADER_WIDGET_TYPE)] == WIDGET_TYPE_CALCULATION && widget[headerMap.get(HEADER_WIDGET_DATA_KEY)].startsWith('score_')) {
                        // assume that a score will come directly after an answer
                        if (!answers.isEmpty()) {
                            answers[answers.size() - 1].VTD1_Score__c = Decimal.valueOf(value);
                        }
                    } else {
                       answers.add(new VTD1_Survey_Answer__c(
                            VTD1_Survey__c = diariesByResponseGuid.get(responseId).Id,
                            VTR5_External_Question_Number__c = Decimal.valueOf(questionNum++),
                            VTR5_External_Question__c = widget[headerMap.get(HEADER_WIDGET_NAME)],
                            RecordTypeId = Schema.SObjectType.VTD1_Survey_Answer__c.getRecordTypeInfosByDeveloperName().get('External').getRecordTypeId(),
                                VT_R5_External_Widget_Data_Key__c = widget[this.headerMap.get(HEADER_WIDGET_DATA_KEY)],
                            VTD1_Answer__c = value
                        ));
                    }
                }
            }
        }
        }
        insert answers;
    }

    private void triggerSafetyConcerns() {
        if (!safetyConcernDiaries.isEmpty()) {
            update safetyConcernDiaries;
        }
    }

    private void refreshSchedules() {
        List<Id> userIds = VT_D1_HelperClass.getIdsFromObjs(userMap.values());
        new VT_R5_eCoaSubjectSchedule(userIds).setDelay(30).setIsStopEvent(true).execute();
    }

    /**
     * Convert eCOA datetime to Salesforce datetime
     * @param dtString - eCOA formatted datetime string
     * @return Datetime
     */
    public static Datetime parseDatetime(String dtString) {
        String dateString = dtString.substringBefore('T');
        String timeString = dtString.substringAfter('T').mid(0, 8);

        List<String> dateSegments = dateString.split('-');
        List<String> timeSegments = timeString.split(':');
        Date d = Date.newInstance(Integer.valueOf(dateSegments[0]), Integer.valueOf(dateSegments[1]), Integer.valueOf(dateSegments[2]));
        Time t = Time.newInstance(Integer.valueOf(timeSegments[0]), Integer.valueOf(timeSegments[1]), Integer.valueOf(timeSegments[2]), 00);
        Datetime dt = Datetime.newInstanceGmt(d, t);

        String offsetString = dtString.substringAfter(timeString);
        if (offsetString != 'Z') {
            Integer offsetMinutes = (Integer.valueOf(offsetString.mid(1, 2)) * 60) + Integer.valueOf(offsetString.mid(4, 5));
            if (offsetString.left(1) == '+') {
                offsetMinutes = -offsetMinutes;
            }
            dt = dt.addMinutes(offsetMinutes);
        }
        return dt;
    }
}