/********************************************************
* @Class Name   : VT_R5_CreateSSTMToVirtualSite
* @Created Date : 15/10/2020 
* @Version      : R5.5
* @group-content: http://jira.quintiles.net/browse/SH-17314

* @Author       : Rajesh Narode

* @Purpose      : This batch class is used to map all exsiting CRA related to the STM of Virtual site in the virtual site related SSTM.
*                   This is the one time execution batch
*****************************************************/
public with Sharing class VT_R5_CreateSSTMToVirtualSite implements Database.Batchable<sObject>{
    Set<Id>setOfVSIDs = new Set<Id>();
    public VT_R5_CreateSSTMToVirtualSite(Set < Id > setOfVSIds) {
        this.setOfVSIDs = setOfVSIds;
    }
    public VT_R5_CreateSSTMToVirtualSite() {
        
    }
    public Database.QueryLocator start(Database.BatchableContext BC){
        string strQuery = 'SELECT ID,'+

            +'VTR2_Onsite_CRA__c,'+
            +'VTD1_Remote_CRA__c,'+
            +'VTD1_Study_Team_Member__c,'+ 
            +'VTD1_Study__c,'+
            +'VTD1_Study_Team_Member__r.VTR2_Remote_CRA__c,'+
            +'VTD1_Study_Team_Member__r.VTR2_Onsite_CRA__c '+
            +'FROM Virtual_Site__c '+
            +'WHERE (VTR2_Onsite_CRA__c != Null '+
            +'OR VTD1_Remote_CRA__c != Null)';
        
        if(setOfVSIDs.Size() > 0){

        strQuery +='AND ID IN :setOfVSIDs';
        }
        
        return Database.getQueryLocator(strQuery);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> scope){
       
        try{

            VT_R2_StudySiteTeamMemberHandler.isRun = true;
            
            List<Study_Site_Team_Member__c>lstSSTM = new List<Study_Site_Team_Member__c>();
            List<Group>lstGroup = new List<Group>();

            set<ID>setOfStudyId = new set<Id>();
            Set<ID>setOfUserID = new Set<ID>();
            Map<String,ID>mapOFSTudyIdWithUserId = new Map<String,ID>();
            for(Virtual_Site__c objSite : (List<Virtual_Site__c>)scope){

                if((objSite.VTD1_Study_Team_Member__c == Null || objSite.VTD1_Study_Team_Member__r.VTR2_Remote_CRA__c == Null) &&
                objSite.VTD1_Remote_CRA__c != Null ){

                    setOfStudyId.add(objSite.VTD1_Study__c);
                    setOfUserID.add(objSite.VTD1_Remote_CRA__c);
                }

                if((objSite.VTD1_Study_Team_Member__c == Null ||objSite.VTD1_Study_Team_Member__r.VTR2_Onsite_CRA__c == Null) &&
                objSite.VTR2_Onsite_CRA__c != Null ){

                    setOfStudyId.add(objSite.VTD1_Study__c);
                    setOfUserID.add(objSite.VTR2_Onsite_CRA__c);
                }
            }

            for(Study_Team_Member__c objSTM : [SELECT ID,
                                               Study__c,
                                               User__c,
                                               VTD1_Type__c 
                                               FROM  Study_Team_Member__c
                                               WHERE User__c IN : setOfUserID
                                               AND Study__c IN : setOfStudyId
                                               AND VTD1_Type__c = 'CRA']){
                                                
                                                mapOFSTudyIdWithUserId.put(objSTM.User__c+'_'+objSTM.Study__c,objSTM.Id);
                                                
                                               }
            System.debug('mapOFSTudyIdWithUserId'+mapOFSTudyIdWithUserId);
            
            for(Virtual_Site__c objSite : (List<Virtual_Site__c>)scope){
                
                if(objSite.VTR2_Onsite_CRA__c!= Null && objSite.VTD1_Remote_CRA__c != Null 
                   && (objSite.VTR2_Onsite_CRA__c == objSite.VTD1_Remote_CRA__c)){

                    Study_Site_Team_Member__c objSSTM = new Study_Site_Team_Member__c();
                    if(objSite.VTD1_Study_Team_Member__r.VTR2_Onsite_CRA__c != NULL){

                        objSSTM.VTR5_Associated_CRA__c = objSite.VTD1_Study_Team_Member__r.VTR2_Onsite_CRA__c;
                    }   
                    else if((objSite.VTD1_Study_Team_Member__c == Null || objSite.VTD1_Study_Team_Member__r.VTR2_Onsite_CRA__c == NULL) && 
                       mapOFSTudyIdWithUserId.containsKey(objSite.VTR2_Onsite_CRA__c+'_'+objSite.VTD1_Study__c)){

                        objSSTM.VTR5_Associated_CRA__c = mapOFSTudyIdWithUserId.get(objSite.VTR2_Onsite_CRA__c+'_'+objSite.VTD1_Study__c);

                    }else if((objSite.VTD1_Study_Team_Member__c == Null ||objSite.VTD1_Study_Team_Member__r.VTR2_Remote_CRA__c == NULL) &&
                            mapOFSTudyIdWithUserId.containsKey(objSite.VTD1_Remote_CRA__c+'_'+objSite.VTD1_Study__c)){

                        objSSTM.VTR5_Associated_CRA__c = mapOFSTudyIdWithUserId.get(objSite.VTD1_Remote_CRA__c+'_'+objSite.VTD1_Study__c);

                    }else{
                        continue;
                    }
                       

                        objSSTM.VTD1_SiteID__c = objSite.Id;
                        lstSSTM.add(objSSTM);
                   
                }else{
                      if(objSite.VTR2_Onsite_CRA__c != Null){

                           System.debug('OnsiteCheck-mapOFSTudyIdWithUserId=>'+mapOFSTudyIdWithUserId);
                           
                           Study_Site_Team_Member__c objSSTM = new Study_Site_Team_Member__c();
                          if(objSite.VTD1_Study_Team_Member__r.VTR2_Onsite_CRA__c != Null){

                            objSSTM.VTR5_Associated_CRA__c = objSite.VTD1_Study_Team_Member__r.VTR2_Onsite_CRA__c;

                          }  
                          else if((objSite.VTD1_Study_Team_Member__c == Null ||objSite.VTD1_Study_Team_Member__r.VTR2_Onsite_CRA__c == NULL) && 
                           mapOFSTudyIdWithUserId.containsKey(objSite.VTR2_Onsite_CRA__c+'_'+objSite.VTD1_Study__c)){

                            System.debug('If -RemoteCheck-mapOFSTudyIdWithUserId=>'+mapOFSTudyIdWithUserId.get(objSite.VTR2_Onsite_CRA__c+'_'+objSite.VTD1_Study__c));
                            objSSTM.VTR5_Associated_CRA__c = mapOFSTudyIdWithUserId.get(objSite.VTR2_Onsite_CRA__c+'_'+objSite.VTD1_Study__c);

                           }else{
                            continue;
                        }
                           

                        objSSTM.VTD1_SiteID__c = objSite.Id;
                        lstSSTM.add(objSSTM);
                    }
                    if(objSite.VTD1_Remote_CRA__c != Null){

                        System.debug('RemoteCheck-mapOFSTudyIdWithUserId=>'+mapOFSTudyIdWithUserId);
                           Study_Site_Team_Member__c objSSTM = new Study_Site_Team_Member__c();

                           if(objSite.VTD1_Study_Team_Member__r.VTR2_Remote_CRA__c != Null){

                            objSSTM.VTR5_Associated_CRA__c = objSite.VTD1_Study_Team_Member__r.VTR2_Remote_CRA__c;

                           }else if((objSite.VTD1_Study_Team_Member__c == Null ||objSite.VTD1_Study_Team_Member__r.VTR2_Remote_CRA__c == Null) && 
                                    mapOFSTudyIdWithUserId.containsKey(objSite.VTD1_Remote_CRA__c+'_'+objSite.VTD1_Study__c)){

                                System.debug('If -RemoteCheck-mapOFSTudyIdWithUserId=>'+mapOFSTudyIdWithUserId.get(objSite.VTD1_Remote_CRA__c+'_'+objSite.VTD1_Study__c));        
                                 objSSTM.VTR5_Associated_CRA__c = mapOFSTudyIdWithUserId.get(objSite.VTD1_Remote_CRA__c+'_'+objSite.VTD1_Study__c);

                           }else{
                            continue;
                        }
 

                        objSSTM.VTD1_SiteID__c = objSite.Id;
                        lstSSTM.add(objSSTM);
                    }
                }
                //Create Group 
                Group queueObj = new Group();
                    queueObj.Name = objSite.ID + '_CRAQueue';
                    queueObj.DeveloperName = objSite.ID + '_CRAQueue';
                    queueObj.Type='Queue'; 
                lstGroup.add(queueObj);
              
            }
            //Insert SSTM
            if(lstSSTM != Null && lstSSTM.size() > 0){
                
                List<Database.SaveResult> lstResult = Database.insert(lstSSTM,false);
                ErrorLogUtility.logErrorsInBulk(lstResult,lstSSTM,Null,'VT_R5_CreateSSTMToVirtualSite-SSTM');
            }
            //Insert Group
            if(lstGroup.size() > 0 && lstGroup != Null){
                Set<ID>idOfGroup = new Set<ID>();
                List<Database.SaveResult> lstResult = Database.insert(lstGroup,false);
                ErrorLogUtility.logErrorsInBulk(lstResult,lstGroup,Null,'VT_R5_CreateSSTMToVirtualSite-Group');
                for (Database.SaveResult sr : lstResult) {
                    if (sr.isSuccess()) {
                        idOfGroup.add(sr.getId());
                    }
                }
                Map<String,Group>mapOfGroup  = new Map<String,Group>();
                For(Group objGroup : [SELECT ID,
                                      Name
                                     FROM Group
                                      WHERE ID IN:idOfGroup]){
                                          mapOfGroup.put(objGroup.Name,objGroup);
                                      }
                //Map CRA Queue Filed of the virtual sit as Queue Id
                for(Virtual_Site__c objSite : (List<Virtual_Site__c>)scope){
                    if(mapOfGroup.containsKey(objSite.Id+'_CRAQueue')){
                       objSite.VTR5_CRA_Queue__c =  mapOfGroup.get(objSite.Id+'_CRAQueue').Id;
                    }
                }
                
            }
            //Update CRA Queue Field on Virtual Site
            if(scope.size() > 0 && scope != Null){
                List<Database.SaveResult> lstResult = Database.update(scope,false);
                ErrorLogUtility.logErrorsInBulk(lstResult,scope,Null,'VT_R5_CreateSSTMToVirtualSite-QueueNameUpdate');
            }
            
            
        }Catch(Exception ex){
            
            System.debug('Exception Meesage'+ex.getMessage()+'Line No:'+ex.getLineNumber());
            ErrorLogUtility.logException(ex, null, 'VT_R5_CreateSSTMToVirtualSite');
        }   
    }
    public void finish(Database.BatchableContext BC){
       
       Database.executeBatch(new VT_R5_CreateQueueMemeberToSSTM());
        
    }
}