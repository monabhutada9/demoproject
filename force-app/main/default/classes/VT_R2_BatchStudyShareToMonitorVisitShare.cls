/**
 * !!! WILL BE REMOVED AT THE NEXT DESTRUCTIVE CHANGES SESSION, SH-12784 !!!
 */


/**
 * Created by Michael Salamakha on 25.03.2019.
 */

global class VT_R2_BatchStudyShareToMonitorVisitShare implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('');
    }

    global void execute(Database.BatchableContext BC, List<HealthCloudGA__CarePlanTemplate__share> scope) {
//        /*List<VTD1_Monitoring_Visit__share> mvsList = [select id,ParentId,UserOrGroupId from VTD1_Monitoring_Visit__share];
//        Set <id> mvSetIds = new Set<Id>();
//        for (VTD1_Monitoring_Visit__share m:mvsList){
//            mvSetIds.add(m.ParentId);
//        }*/
//        List<VTD1_Monitoring_Visit__c> mvList = [select id, VTD1_Study__c from VTD1_Monitoring_Visit__c ]; //where id not in:mvSetIds
//
//        List<VTD1_Monitoring_Visit__share> mvsNewList = new List<VTD1_Monitoring_Visit__share>();
//        for (HealthCloudGA__CarePlanTemplate__share s : scope) {
//            for (VTD1_Monitoring_Visit__c m: mvList) {
//                if(s.ParentId ==m.VTD1_Study__c) { // && !mvSetIds.contains(m.id)) {
//                    VTD1_Monitoring_Visit__share mvsObject = new VTD1_Monitoring_Visit__share(
//                            ParentId=m.id,
//                            UserOrGroupId=s.UserOrGroupId,
//                            AccessLevel =  s.AccessLevel == 'All' ? 'Edit' : s.AccessLevel);
//                    mvsNewList.add(mvsObject);
//                }
//            }
//        }
//        insert mvsNewList;
    }

    global void finish(Database.BatchableContext BC) {
    }
}