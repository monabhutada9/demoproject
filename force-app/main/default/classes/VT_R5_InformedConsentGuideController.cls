/**
 * Created by Eugene Pavlvoskiy on 13-May-20.
 */
public without sharing class VT_R5_InformedConsentGuideController {



    public static final Id INFORMED_CONSENT_GUIDE_RECORDTYPE_ID = VT_R4_ConstantsHelper_RecordTypes.RECORD_TYPE_ID_DOCUMENT_INFORMED_CONSENT_GUIDE;//hello copado



    public static final String DOCUMENT_ACCESS_LEVEL = 'Read';
    public static final String DOCUMENT_ROW_CAUSE = 'Manual'; 
    public static final String PATIENT_KEY = 'Patient';
    public static final String CAREGIVER_KEY = 'Caregiver';

    public static final Set<String> SUPPORTED_DOC_FORMAT = new Set<String>{'pdf'};

    /**
     * The method is applied to get all documents which get params from table on component





     * @author Eugene Pavlovskiy
     * @param querySettingsString
     * @date 28/05/2020
     * @issue SH-11760
     * @return DocumentsWrapper
     */
    @AuraEnabled
    public static DocumentsWrapper getDocuments(String querySettingsString) {
        DocumentsWrapper documentsWrapper = new DocumentsWrapper();
        try {
            User currentUser = [SELECT Contact.VTR2_Primary_Language__c, Contact.VTD1_Clinical_Study_Membership__c FROM User WHERE Id = :UserInfo.getUserId()];
            if(currentUser.Contact.VTD1_Clinical_Study_Membership__c == null){
                return documentsWrapper;
            }
            List<Case> patientCaseList = [
                        SELECT  Id,
                        VTD1_Virtual_Site__r.VTR3_Study_Geography__c,
                        VTD1_Virtual_Site__r.VTD1_Study__c
                        FROM Case
                        WHERE Id = :currentUser.Contact.VTD1_Clinical_Study_Membership__c
                        LIMIT 1
            ];
            if (!patientCaseList.isEmpty()){
                Case patientCase = patientCaseList.get(0);
                String primaryLanguage = currentUser.Contact.VTR2_Primary_Language__c;



                Boolean isPrimaryLanguage = isPrimaryLanguageOnDocuments(patientCase, primaryLanguage);



                VT_TableHelper.TableQuerySettings querySettings = VT_TableHelper.parseQuerySettings(querySettingsString);
                documentsWrapper.documentsList = getDocumentsList(querySettings, patientCase, primaryLanguage, isPrimaryLanguage);
                documentsWrapper.totalPages = getTotalPages(patientCase, querySettings, primaryLanguage, isPrimaryLanguage);
            }
            return documentsWrapper;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
    private static List<ConsentPacketWrapper> getDocumentsList(VT_TableHelper.TableQuerySettings querySettings, Case patientCase, String primaryLanguage, Boolean isPrimaryLanguage) {
        List<ConsentPacketWrapper> wrapperList = new List<ConsentPacketWrapper>();
        List<VTD1_Document__c> contentDocumentLinkList = getStudyGeographyDocumentList(querySettings, patientCase, primaryLanguage, isPrimaryLanguage);
        if (contentDocumentLinkList.isEmpty()){
            return new List<ConsentPacketWrapper>();
        }
        shareDocuments(contentDocumentLinkList);
        for (VTD1_Document__c currentDocument : contentDocumentLinkList) {
            if (currentDocument.ContentDocumentLinks!=null &&  !currentDocument.ContentDocumentLinks.isEmpty()) {
                wrapperList.add(new ConsentPacketWrapper(currentDocument, currentDocument.ContentDocumentLinks[0]));
            }
        }
        return wrapperList;
    }







    /**
    * The method is applied to share list of documents to current user to view each document view
    * @author  Aleh Lutsevich
    * @param docToShare list of document to share
    * @date 21/05/2020
    * @issue SH-10834
    */
    private static void shareDocuments(List<VTD1_Document__c> docToShare) {
        List<VTD1_Document__Share> shareList = new List<VTD1_Document__Share>();
        for(VTD1_Document__c currentDoc : docToShare) {
            shareList.add(new VTD1_Document__Share(ParentId = currentDoc.Id, UserOrGroupId = UserInfo.getUserId(), AccessLevel = DOCUMENT_ACCESS_LEVEL, RowCause = DOCUMENT_ROW_CAUSE));
        }
        insert shareList;
    }

    /**
    * The method is applied to get study geography document a list documents which get params from table on component and needed case, primary language in patient and checkbox about this language






    * @author Eugene Pavlovskiy
    * @param querySettings
    * @param patientCase
    * @param primaryLanguage
    * @param isPrimaryLanguage
    * @date 28/05/2020
    * @issue SH-11760
    * @return list documents
    */
    private static List<VTD1_Document__c> getStudyGeographyDocumentList(VT_TableHelper.TableQuerySettings querySettings, Case patientCase, String primaryLanguage, Boolean isPrimaryLanguage){
        Id studyGeographyId = patientCase.VTD1_Virtual_Site__r.VTR3_Study_Geography__c;
        Id studyId = patientCase.VTD1_Virtual_Site__r.VTD1_Study__c;
        String queryFilter = (querySettings.queryFilter != null && querySettings.queryFilter != '') ? ' AND ' + querySettings.queryFilter : '';
        String queryOffset = (querySettings.queryOffset != null && querySettings.queryOffset != '') ? ' ' + querySettings.queryOffset : '';
        String queryLimit = (querySettings.queryLimit != null && querySettings.queryLimit != '') ? ' ' + querySettings.queryLimit : '';
        String orderByField = (String.isNotBlank(querySettings.queryOrder)) ? ' ' +  querySettings.queryOrder : ' LastModifiedDate DESC';
        orderByField = orderByField.replace('LastModifiedDate', 'VTR5_LastFileUploadedDate__c');
        String primaryLanguageOrDefault = isPrimaryLanguage ? ' VTR2_Language__c =: primaryLanguage ' : ' VTR5_DefaultDocument__c = true ';
        String query =
                'SELECT   Id, '+
                        ' VTR5_DocumentTitle__c, ' +
                        ' VTR5_DocumentVersion__c, ' +
                        ' VTR2_Language__c, '+
                        ' VTR5_LastFileUploadedDate__c, ' +
                        '(SELECT Id,' +
                        ' LinkedEntityId,' +
                        ' ContentDocumentId,' +

                        ' ContentDocument.LastModifiedDate, ' +
                        ' ContentDocument.FileExtension ' +
                        'FROM   ContentDocumentLinks ' +
                        'ORDER BY ContentDocument.LastModifiedDate DESC ' +
                        'LIMIT 1) ' +





                'FROM   VTD1_Document__c ' +
                'WHERE  VTR2_Geography__c = :studyGeographyId ' +
                'AND    VTR5_LastFileUploadedDate__c != null ' +
                'AND    RecordTypeId = :INFORMED_CONSENT_GUIDE_RECORDTYPE_ID AND ' +
                 primaryLanguageOrDefault +
                'AND VTD1_Study__c =: studyId  ' +
                + queryFilter
                + ' ORDER BY ' + orderByField
                + queryLimit + queryOffset;
        return Database.query(query);
    }




    /**
    * The method is applied to get the total number of pages depending on how many documents meet the criteria from the parameters



    * @author Eugene Pavlovskiy
    * @param querySettings
    * @param patientCase
    * @param primaryLanguage
    * @param isPrimaryLanguage
    * @date 28/05/2020
    * @issue SH-11760
    * @return total pages
    */
    private static Integer getTotalPages(Case patientCase, VT_TableHelper.TableQuerySettings querySettings, String primaryLanguage, Boolean isPrimaryLanguage) {
        Id studyGeographyId = patientCase.VTD1_Virtual_Site__r.VTR3_Study_Geography__c;
        Id studyId = patientCase.VTD1_Virtual_Site__r.VTD1_Study__c;
        String queryFilter = (querySettings.queryFilter != null && querySettings.queryFilter != '') ? ' AND ' + querySettings.queryFilter : '';
        String primaryLanguageOrDefault = isPrimaryLanguage ? ' VTR2_Language__c =: primaryLanguage ' : ' VTR5_DefaultDocument__c = true ';
        String query =
                'SELECT  COUNT(Id) recordCount ' +
                        'FROM VTD1_Document__c ' +
                        'WHERE VTR2_Geography__c = :studyGeographyId ' +
                        'AND VTR5_LastFileUploadedDate__c != null ' +
                        'AND    RecordTypeId = :INFORMED_CONSENT_GUIDE_RECORDTYPE_ID AND ' +
                         primaryLanguageOrDefault +
                        'AND VTD1_Study__c =: studyId  ' +
                + queryFilter;
        AggregateResult[] ar = Database.query(query);
        Integer totalPages = (Integer) Math.ceil((Decimal) ar[0].get('recordCount') / querySettings.entriesOnPage);
        return totalPages;
    }
    public class DocumentsWrapper {
        @AuraEnabled
        public List<ConsentPacketWrapper> documentsList { get; set; }
        @AuraEnabled
        public Integer totalPages { get; set; }
    }
    public class ConsentPacketWrapper {
        @AuraEnabled
        public String documentTitle;
        @AuraEnabled
        public String documentVersion;
        @AuraEnabled
        public String documentId;
        @AuraEnabled
        public String urlForDownload;
        @AuraEnabled
        public String lastModifiedDate;

        @AuraEnabled
        public String fileExtension;
        @AuraEnabled
        public Boolean allowDownload;

        public ConsentPacketWrapper(VTD1_Document__c document, ContentDocumentLink link){
            this.documentId = link.ContentDocumentId;
            this.documentVersion = document.VTR5_DocumentVersion__c;
            this.documentTitle = document.VTR5_DocumentTitle__c;
            Datetime lastModifiedDatetime =  link.ContentDocument.LastModifiedDate;
//            Datetime lastModifiedDatetime =  document.VTR5_LastFileUploadedDate__c;
            this.lastModifiedDate = VT_D1_TranslateHelper.translate(lastModifiedDatetime.format('dd-MMM-YYYY'));

            this.fileExtension = link.ContentDocument.FileExtension.toLowerCase();
            this.allowDownload = SUPPORTED_DOC_FORMAT.contains(link.ContentDocument.FileExtension.toLowerCase());

            this.urlForDownload = System.Url.getSalesforceBaseUrl().toExternalForm()
                    + VT_D1_HelperClass.getSandboxPrefix() + '/sfc/servlet.shepherd/document/download/'
                    + link.ContentDocumentId;
        }
    }







    /**
    * Checked if the documents have the primary language or not
    * @author Kirill Latysh
    * @param patientCase
    * @param primaryLanguage
    * @date 03/06/2020
    * @issue SH-11795
    * @return isPrimaryLanguage
    */
    public static Boolean isPrimaryLanguageOnDocuments(Case patientCase, String primaryLanguage) {
        Id studyGeographyId = patientCase.VTD1_Virtual_Site__r.VTR3_Study_Geography__c;
        Id studyId = patientCase.VTD1_Virtual_Site__r.VTD1_Study__c;







            List<VTD1_Document__c> targetDocumentList = [
                    SELECT VTR2_Language__c
                    FROM VTD1_Document__c
                    WHERE VTR5_LastFileUploadedDate__c != null
                    AND VTR2_Language__c = :primaryLanguage



                AND RecordTypeId = :INFORMED_CONSENT_GUIDE_RECORDTYPE_ID



                    AND VTR2_Geography__c = :studyGeographyId
                    AND VTD1_Study__c =: studyId
                    LIMIT 1
            ];







        return !targetDocumentList.isEmpty();
    }

    /**
    * The method is applied to get the Map of last uploaded VTD1_Document__c by patient user Id
    * @author Viktar Bobich
    * @param patientIDList (List user Id)
    * @date 28/05/2020
    * @issue
    * @return Map<Id, VTD1_Document__c>
    */






    public static Map<Id, VTD1_Document__c> getLastAppropriateDocumentByPTMap(List<Id> patientIDList){
        List<User> currentUserList = [SELECT ContactId, Contact.VTR2_Primary_Language__c, Contact.VTD1_Clinical_Study_Membership__c FROM User WHERE Id IN :patientIDList];
        Map<Id, String> userLangByIdMap = new Map<Id, String>();
        for(User currentUser : currentUserList){
            userLangByIdMap.put(currentUser.Id, currentUser.Contact.VTR2_Primary_Language__c);
        }
        Map<Id, Id> userIdByCaseIdMap = new Map<Id, Id>();
        for(User currentUser : currentUserList){
            userIdByCaseIdMap.put(currentUser.Contact.VTD1_Clinical_Study_Membership__c, currentUser.Id);
        }
        List<Case> patientCaseList = [
                SELECT  Id,
                        Account.VTD2_Caregiver_s_ID__c,
                        Account.VTD2_Caregiver_s_ID__r.LanguageLocaleKey,
                        VTD1_Patient_User__c,
                        VTD1_Virtual_Site__r.VTR3_Study_Geography__c,
                        VTD1_Virtual_Site__r.VTD1_Study__c,
                        VTD1_Patient_User__r.ContactId,
                        VTD1_Patient_User__r.Contact.VTR2_Primary_Language__c
                FROM Case
                WHERE Id IN :userIdByCaseIdMap.keySet()
        ];







        for(Case currentCase : patientCaseList){
            userLangByIdMap.put(currentCase.VTD1_Patient_User__c, currentCase.VTD1_Patient_User__r.Contact.VTR2_Primary_Language__c);
            if(currentCase.Account.VTD2_Caregiver_s_ID__c != null){
                userLangByIdMap.put(currentCase.Account.VTD2_Caregiver_s_ID__c, currentCase.Account.VTD2_Caregiver_s_ID__r.LanguageLocaleKey);
            }
        }






        return prepareAppropriateDocumentMap(patientCaseList, userLangByIdMap);
    }

    /**
		* The method is applied to get the Map of last uploaded VTD1_Document__c(dependent of Language and default document) by patient user Id
		* @author Viktar Bobich
		* @param caseList
		* @param userLangByIdMap
		* @date 28/05/2020
		* @issue
		* @return Map<Id, VTD1_Document__c>
		*/
    private static Map<Id, VTD1_Document__c>  prepareAppropriateDocumentMap(List<Case> caseList, Map<Id, String> userLangByIdMap) {






        Set<Id> studyGeographyIdSet = new Set<Id>();
        Set<Id> studyIdSet = new Set<Id>();
        Set<String> primaryLanguageSet = new Set<String>();
        primaryLanguageSet.addAll(userLangByIdMap.values());
        Map<String, Id> userIdByKeyMap = new Map<String, Id>();
        Map<String, Id> userIdByLangKeyMap = new Map<String, Id>();
        Map<Id, VTD1_Document__c> resultMap = new Map<Id, VTD1_Document__c>();
        String key;
        String langKey;
        String caregiverLangKey;
        for (Case patientCase : caseList) {
            String caregiverId = patientCase.Account.VTD2_Caregiver_s_ID__c;
            String patientId = patientCase.VTD1_Patient_User__c;
            studyGeographyIdSet.add(patientCase.VTD1_Virtual_Site__r.VTR3_Study_Geography__c);
            studyIdSet.add(patientCase.VTD1_Virtual_Site__r.VTD1_Study__c);
            key = '' + patientCase.VTD1_Virtual_Site__r.VTD1_Study__c + patientCase.VTD1_Virtual_Site__r.VTR3_Study_Geography__c;






            langKey = PATIENT_KEY + patientCase.VTD1_Virtual_Site__r.VTD1_Study__c
                         + patientCase.VTD1_Virtual_Site__r.VTR3_Study_Geography__c
                         + userLangByIdMap.get(patientId);
            if(caregiverId != null){
                caregiverLangKey = CAREGIVER_KEY + patientCase.VTD1_Virtual_Site__r.VTD1_Study__c






                            + patientCase.VTD1_Virtual_Site__r.VTR3_Study_Geography__c
                            + userLangByIdMap.get(caregiverId);
                userIdByLangKeyMap.put(caregiverLangKey, caregiverId);
            }





            userIdByKeyMap.put(key, patientId);
            userIdByLangKeyMap.put(langKey, patientId);





        }
        List<VTD1_Document__c> documentList = [
                SELECT  Id, VTR5_DocumentTitle__c, VTR2_Geography__c, VTD1_Study__c
                FROM    VTD1_Document__c
                WHERE   VTR2_Geography__c IN :studyGeographyIdSet
                    AND VTD1_Study__c IN :studyIdSet
                    AND VTR5_LastFileUploadedDate__c != null
                    AND RecordTypeId = :INFORMED_CONSENT_GUIDE_RECORDTYPE_ID
                    AND VTR5_DefaultDocument__c = true
                ORDER BY VTR5_LastFileUploadedDate__c ASC
        ];
        for (VTD1_Document__c document : documentList) {
            key = '' + document.VTD1_Study__c + document.VTR2_Geography__c;
            if (userIdByKeyMap.containsKey(key)) {
                resultMap.put(userIdByKeyMap.get(key), document);
            }
        }







        documentList = [
                SELECT  Id, VTR5_DocumentTitle__c, VTR2_Geography__c, VTD1_Study__c, VTR2_Language__c
                FROM    VTD1_Document__c
                WHERE   VTR2_Geography__c IN :studyGeographyIdSet
                    AND VTD1_Study__c IN :studyIdSet
                    AND VTR5_LastFileUploadedDate__c != null
                    AND RecordTypeId = :INFORMED_CONSENT_GUIDE_RECORDTYPE_ID
                    AND VTR2_Language__c IN :primaryLanguageSet
                ORDER BY VTR5_LastFileUploadedDate__c ASC
        ];
        for (VTD1_Document__c document : documentList) {






            langKey = PATIENT_KEY + document.VTD1_Study__c + document.VTR2_Geography__c + document.VTR2_Language__c;
            if (userIdByLangKeyMap.containsKey(langKey)) {
                resultMap.put(userIdByLangKeyMap.get(langKey), document);
            }
            langKey = CAREGIVER_KEY + document.VTD1_Study__c + document.VTR2_Geography__c + document.VTR2_Language__c;






            if (userIdByLangKeyMap.containsKey(langKey)) {
                resultMap.put(userIdByLangKeyMap.get(langKey), document);
            }
        }
        return resultMap;
    }







}