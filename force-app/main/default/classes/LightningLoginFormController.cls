global class LightningLoginFormController {

    public static Id testNetworkId;//only for test
    private static  Map<String,String> correctCommunityForProfiles = new Map<String,String>{
            'Patient' =>    'VTD2_PatientCGCommunity',
            'Caregiver' =>    'VTD2_PatientCGCommunity',
            'Primary Investigator' =>    'VTD2_PICommunity',
            'Site Coordinator' =>    'VTR2_SCRCommunity'
    };

    public LightningLoginFormController() {
    }

    @AuraEnabled
    public static Map<String, Object> login(String username, String password, String startUrl, String cookieAcceptedInfo) {
        String exceptionType;
        String errorMsg;
        String profileName;
        String apiNameNetwork;

        /* To provide access to user record in context of community guest user. */
        /*List<User> users = [
                SELECT Profile.Name, ContactId
                FROM User
                WHERE Username = :username
        ];*/
        List<User> users = new LoginFormWithoutSharingService().getUsersByUsername(username);


        if (!users.isEmpty()) {
            profileName = users[0].Profile.Name;
            apiNameNetwork = [
                    SELECT Name
                    FROM Network
                    WHERE Id = :Network.getNetworkId() OR Id =: testNetworkId
            ].Name;
        }

        Map<String, Object> response = new Map<String, Object>();
        String correctCommunityForProfile = correctCommunityForProfiles.get(profileName);

        if(correctCommunityForProfile == apiNameNetwork) {
            try {
                if(!users.isEmpty() && users[0].ContactId != null && cookieAcceptedInfo != null) {
                    insert new VTR3_Temporary_SObject__c(VTR3_SObjectID__c = users[0].ContactId, VTR4_CookieAcceptedText__c = cookieAcceptedInfo);
                }
                ApexPages.PageReference lgn = Site.login(username, password, startUrl);
                response.put('url', lgn.getUrl());
            } catch (SecurityException ex) {
                exceptionType = ex.getTypeName();
            } catch (Exception ex) {
                exceptionType = ex.getTypeName();
                response.put('errorMsg', ex.getMessage());
                response.put('loginAttemptsRemaining', null);
            }

            if (exceptionType == 'System.SecurityException') {
                errorMsg = System.Label.VTR3_UsernameOrPassIncorrect;
                try {
                    Integer checkForLockedUser = VT_D1_LockedUserHelper.checkForLockedUser(username);
                    response.put('errorMsg', errorMsg);
                    response.put('loginAttemptsRemaining', checkForLockedUser);
                } catch (Exception ex) {
                    errorMsg = ex.getMessage();
                    response.put('errorMsg', errorMsg);
                    response.put('loginAttemptsRemaining', null);
                }
            }

        } else {
            response.put('errorMsg', Label.VTR3_IncorrectCommunityMessage);
        }
        return response;
    }

    @AuraEnabled
    public static Boolean forgotPassword (String username) {
        try{
            boolean success = Site.forgotPassword(username);
            return success;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static Boolean getIsUsernamePasswordEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getUsernamePasswordEnabled();
    }


    @TestVisible
    private static Auth.AuthConfiguration getAuthConfig(){
        Id networkId = Network.getNetworkId();
        Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(networkId,'');
        return authConfig;
    }

    @AuraEnabled
    global static String setExperienceId(String expId) {
        // Return null if there is no error, else it will return the error message
        try {
            if (expId != null) {
                Site.setExperienceId(expId);
            }
            return null;
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    /*****************************************
* @author Divya Mirani
* @date 02/11/2020
*
* @group Cookies Opt in/out

* @description To provide access to user record in context of community guest user. 
Accomadate changes in this class if it is required to run in without sharing context.
*/
    public without sharing class LoginFormWithoutSharingService{ 

        /*******************************************************************************************************
* @description Used to query user
* @return returns List<User>
* @example List<User> users = new LoginFormWithoutSharingService().getUsersByUsername(username);
*/
        public List<User> getUsersByUsername(String username)
        {
            return [
                SELECT Profile.Name, ContactId
                FROM User
                WHERE Username = :username
        ];
        }
    }
}