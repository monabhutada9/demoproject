/*************************************************************************************
@author: Vijendra Hire
@Story: SH-14821
@Description: This is the VT_R5_CRATaskPoolController's Test Class. 
@Date: 22.10.2020
****************************************************************************************/
@isTest
public class VT_R5_CRATaskpoolControllerTest {

    public class CRATaskQueueSobjectCreator implements Queueable {
        final Id queueId;
        public CRATaskQueueSobjectCreator(Id queueId) {
            this.queueId = queueId;
        }
        public void execute(QueueableContext qc) {
            insert new QueueSobject(SobjectType = 'VTR5_CRA_Task__c', QueueId = queueId);
        }
    }

/*****************************************************************************
 	 * Story  : SH-14821
     * Method : dataSetup
     * param  : N/A
     * description : This mothod is used to create the data for test class.
*******************************************************************************/

    @TestSetup
    static void dataSetup() {
        List<String> taskCategories = new List<String>{'Action Item', 
                                                        'Document Review', 
                                                        'Protocol Amendment', 
                                                        'Submission Required', 
                                                        'Visit Related'
                                                        };
         Insert new VTD2_TaskCategoriesByProfile__c(VTD2_Categories__c = String.join(taskCategories, ';'));
        
        List<VTR5_CRA_Task__c> CRATlist = new List<VTR5_CRA_Task__c>();
        VTR5_CRA_Task__c CraTask = new VTR5_CRA_Task__c(Name = 'Test Task',  
                                                        VTR5_Subject__c = 'Test Task sub', 
                                                        VTR5_Type__c = 'Notification', 
                                                        VTR5_Category__c = 'Document Review');
        CRATlist.add(CraTask);
        
        VTR5_CRA_Task__c task = new VTR5_CRA_Task__c(OwnerId = UserInfo.getUserId());
        CRATlist.add(task);
        
        insert CRATlist;

        Group queue = new Group(Type = 'Queue', Name = 'CRA Test Pool' + Crypto.getRandomInteger());
        insert queue;

        System.enqueueJob(new CRATaskQueueSobjectCreator(queue.Id));
    }

/*********************************************************************************************
 	 * Story  : SH-14821
     * Method : claimTasksRemoteTest
     * param  : N/A
     * description : This mothod is used to cover the code coverage claimTasksRemote method.
*********************************************************************************************/    
    @isTest
    static void claimTasksRemoteTest() {
       // VTR5_CRA_Task__c CRATask = [SELECT Id FROM VTR5_CRA_Task__c LIMIT 1];
        Profile testProfile 	 = [SELECT Id FROM Profile WHERE Name = 'CRA' LIMIT 1];

        QueueSobject queue = [SELECT QueueId FROM QueueSobject WHERE SobjectType = 'VTR5_CRA_Task__c' AND CreatedById = :UserInfo.getUserId() LIMIT 1];

        User testUser = new User(LastName = 'test user 1',
                                 Username = 'testcra.user.1@example.com',
                                 Email = 'testcra.1@example.com',
                                 Alias = 'testu1',
                                 TimeZoneSidKey = 'GMT',
                                 LocaleSidKey = 'en_GB',
                                 EmailEncodingKey = 'ISO-8859-1',
                                 ProfileId = testProfile.Id,
                                 LanguageLocaleKey = 'en_US');
        Insert testUser;
        
        VTR5_CRA_Task__c newT = new VTR5_CRA_Task__c();
        newT.OwnerId = queue.QueueId;
        insert newT;
        
        Test.startTest();
        System.runAs(testUser) {
            
            VT_R5_CRATaskPoolController.claimTasksRemote(new List<Id>{newT.Id});
            list<VTR5_CRA_Task__c> CRATask = [SELECT Id,OwnerId FROM VTR5_CRA_Task__c WHERE Id =: newT.Id];
            System.assert(!CRATask.isEmpty());
        }
        Test.stopTest();
    }

/**********************************************************************************************
 	 * Story  : SH-14821
     * Method : getCRATasksFromPoolTest
     * param  : N/A
     * description : This mothod is used to cover the code coverage getCRATasksFromPool method.
***********************************************************************************************/    
    @isTest
    public static void getCRATasksFromPoolTest() {
        Map<String, Object> params = new Map<String, Object>();
        params.put('sObjectName', 'VTR5_CRA_Task__c');
        params.put('offset', '10');
        params.put('limit', '100');
        
        
        Test.StartTest();
        VT_D1_TestUtils.loadProfiles();
        VT_D1_TestUtils.initUserRole();
        User u = VT_D1_TestUtils.createUserByProfile('CRA', 'crauser');
        insert u;
        
        System.runAs(u) {
            string msg = VT_R5_CRATaskPoolController.getCRATasksFromPool(null, JSON.serialize(params));
            string msg2 = VT_R5_CRATaskPoolController.getCRATasksFromPool('Document Review', JSON.serialize(params));
            System.assertNotEquals('', msg);
            System.assertNotEquals('', msg);
        }
        Test.StopTest();
    }
}