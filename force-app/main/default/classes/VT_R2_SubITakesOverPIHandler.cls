/**
 * Created by Alexander Komarov on 18.02.2019.
 */

public without sharing class VT_R2_SubITakesOverPIHandler {

    private Map<Id, Map<Id, Study_Team_Member__c>> studySTMsMap;
    private Map<Id, Study_Team_Member__c> stmToUpdate = new Map<Id, Study_Team_Member__c>();

    public void handleChangeComplete(List<Virtual_Site__c> sites, Map<Id, Virtual_Site__c> oldMap) {
        List<Virtual_Site__c> sitesToProcess = new List<Virtual_Site__c>();

        //before update, if flag PI_Change_Complete becomes true and New_Backup_PI filled, process completed changes
        for (Virtual_Site__c site : sites) {
            Virtual_Site__c old = oldMap.get(site.Id);
            if (site.VTR2_PI_Change_Complete__c && !old.VTR2_PI_Change_Complete__c) {
                if (site.VTR2_New_Backup_PI__c != null) {
                    sitesToProcess.add(site);
                } else {
                    site.addError(System.Label.VTR2_NewBackup_Pi_Error);
                }
            }
        }
        if (!sitesToProcess.isEmpty()) {
            processCompletedChanges(sitesToProcess);
        }
    }

    private void processCompletedChanges(List<Virtual_Site__c> sites) {
        Map<Id, Map<Id, Id>> studySTMIdsToUserIdsMap = new Map<Id, Map<Id, Id>>();

        //get map with study_id to user_id to study_team_member (Backup PI)
        this.studySTMsMap = getStudySTMs(sites);

        for(Id studyId : this.studySTMsMap.keySet()){
            for(Id userId : this.studySTMsMap.get(studyId).keySet()) {
                if (!studySTMIdsToUserIdsMap.containsKey(studyId)) {
                    studySTMIdsToUserIdsMap.put(studyId, new Map<Id, Id>());
                }
                studySTMIdsToUserIdsMap.get(studyId).put(this.studySTMsMap.get(studyId).get(userId).Id, userId);
            }
        }

        updateSiteMembers(sites);
        updateStudyMembers(sites);

        //reassign fields
        for (Virtual_Site__c site : sites) {
            site.VTD1_Study_Team_Member__c = site.VTR2_Backup_PI_STM__c;
            site.VTR2_Backup_PI_STM__c = this.studySTMsMap.get(site.VTD1_Study__c).get(site.VTR2_New_Backup_PI__c).Id;
            site.VTD1_PI_User__c  = studySTMIdsToUserIdsMap.get(site.VTD1_Study__c).get(site.VTD1_Study_Team_Member__c);
            site.VTR2_New_Backup_PI__c = null;
        }
    }

    // map with study_id to user_id to study_team_member
    private Map<Id, Map<Id, Study_Team_Member__c>> getStudySTMs(List<Virtual_Site__c> sites) {
        //form basic map
        Map<Id, Map<Id, Study_Team_Member__c>> studySTMsMap = new Map<Id, Map<Id, Study_Team_Member__c>>();
        List<Id> studiesOfSites = new List<Id>();
        List<Id> backupUserIds = new List<Id>();
        List<Id> stmIds = new List<Id>();
        for (Virtual_Site__c site : sites) {
            studiesOfSites.add(site.VTD1_Study__c);
            backupUserIds.add(site.VTR2_New_Backup_PI__c);
            stmIds.add(site.VTD1_Study_Team_Member__c);
            stmIds.add(site.VTR2_Backup_PI_STM__c);
        }
        for (Study_Team_Member__c stm : [
                SELECT Id,Study__c,user__c, VTD1_Active__c, VTD1_Ready_For_Assignment__c
                FROM Study_Team_Member__c
                WHERE (Study__c IN :studiesOfSites and user__c IN :backupUserIds)
                OR Id IN :stmIds
        ]) {
            if (!studySTMsMap.containsKey(stm.Study__c)) {
                studySTMsMap.put(stm.Study__c, new Map<Id, Study_Team_Member__c>());
            }
            studySTMsMap.get(stm.Study__c).put(stm.user__c, stm);
        }

        //check if site.VTR2_New_Backup_PI__c is in map, create new stm if not, form list to insert
        List<Study_Team_Member__c> stmToInsert = new List<Study_Team_Member__c>();

        for (Virtual_Site__c site : sites) {
            if (!studySTMsMap.containsKey(site.VTD1_Study__c)) {
                studySTMsMap.put(site.VTD1_Study__c, new Map<Id, Study_Team_Member__c>());
            }
            if (!studySTMsMap.get(site.VTD1_Study__c).containsKey(site.VTR2_New_Backup_PI__c)) {
                Study_Team_Member__c newSTM = new Study_Team_Member__c(
                        RecordTypeId = VT_R4_ConstantsHelper_ProfilesSTM.RECORD_TYPE_ID_STUDY_TEAM_MEMBER_PI,
                        User__c = site.VTR2_New_Backup_PI__c,
                        Study__c = site.VTD1_Study__c,
                        VTD1_Active__c = true,
                        VTD1_Ready_For_Assignment__c = true,
                        Study_Team_Member__c = site.VTR2_Backup_PI_STM__c
                );
                stmToInsert.add(newSTM);
            } else {
                Study_Team_Member__c existingMember = studySTMsMap.get(site.VTD1_Study__c).get(site.VTR2_New_Backup_PI__c);
                existingMember.VTD1_Active__c = true;
                existingMember.VTD1_Ready_For_Assignment__c = true;
                existingMember.Study_Team_Member__c = site.VTR2_Backup_PI_STM__c;
                this.stmToUpdate.put(existingMember.Id, existingMember);
            }

        }
        //insert!
        //add inserted users to map
        if (!stmToInsert.isEmpty()) {
            insert stmToInsert;
            for (Study_Team_Member__c stm : stmToInsert) {
                studySTMsMap.get(stm.Study__c).put(stm.User__c, stm);
            }
        }

        return studySTMsMap;
    }



    public void reassignPatientsToNewPI(List<Virtual_Site__c> sites, Map<Id, Virtual_Site__c> oldMap) {
        //after update, if site got new PI, reassign patients
        List <Virtual_Site__c> sitesToProcess = new List<Virtual_Site__c>();
        for (Virtual_Site__c site : sites) {
            Virtual_Site__c old = oldMap.get(site.Id);
            if (site.VTD1_Study_Team_Member__c != old.VTD1_Study_Team_Member__c) {
                sitesToProcess.add(site);
            }
        }
        if (!sitesToProcess.isEmpty()) {
            reassignPatients(sitesToProcess);
        }
    }

    private void reassignPatients(List<Virtual_Site__c> sites) {
        Map<Id, Virtual_Site__c> mapIdToSite = new Map<Id, Virtual_Site__c>();
        //form map with some strange object containing necessary data site_ID -> site_object_with_data
        for (Virtual_Site__c site : [
                SELECT id,VTD1_Study_Team_Member__c, VTD1_Study_Team_Member__r.User__c, VTD1_Study_Team_Member__r.VTD1_UserContact__c,VTR2_Backup_PI_STM__c, VTR2_Backup_PI_STM__r.User__c, VTR2_Backup_PI_STM__r.VTD1_UserContact__c
                FROM Virtual_Site__c
                WHERE Id in :sites
        ]) {
            mapIdToSite.put(site.Id, site);
        }

        //set with case statuses not for reconsent needed after pi change
        Set<String> patientStatusesNotForReconsent = new Set<String>{
                VT_R4_ConstantsHelper_AccountContactCase.CASE_PRE_CONSENT,
                VT_R4_ConstantsHelper_AccountContactCase.CASE_DROPPED,
                VT_R4_ConstantsHelper_AccountContactCase.CASE_COMPLETED,
                VT_R4_ConstantsHelper_AccountContactCase.CASE_CLOSED
        };

        //set with case statuses not for PI Reassignment, strange to write such comment to so self-described name -_-
        Set<String> patientStatusesNotForPIReassignment = new Set<String>{
                VT_R4_ConstantsHelper_AccountContactCase.CASE_DROPPED,
                VT_R4_ConstantsHelper_AccountContactCase.CASE_COMPLETED,
                VT_R4_ConstantsHelper_AccountContactCase.CASE_CLOSED
        };

        //loop through relevant cases, set reconsent flag if needed, reassign patients in needed
        List<Case> casesToBeUpdated = new List<Case>();
        for (Case cas : [
                SELECT Id,VTD2_Patient_Status__c,VTD1_Virtual_Site__c
                FROM Case
                WHERE VTD1_Virtual_Site__c IN :sites FOR UPDATE
        ]) {
            Boolean isUpdated = false;
            if (!patientStatusesNotForReconsent.contains(cas.VTD2_Patient_Status__c)) {
                cas.VTD1_Reconsent_Needed__c = true;
                isUpdated = true;
            }

            if (!patientStatusesNotForPIReassignment.contains(cas.VTD2_Patient_Status__c)) {
                Virtual_Site__c currentSite = mapIdToSite.get(cas.VTD1_Virtual_Site__c);
                //cas.Previous_PI_Lookup__c = cas.VTD1_PI_contact__c; dunno if its need \ probably it is
                cas.VTD1_PI_user__c = currentSite.VTD1_Study_Team_Member__r.User__c;
                cas.VTD1_Backup_PI_User__c = currentSite.VTR2_Backup_PI_STM__r.User__c;
                cas.VTD1_PI_contact__c = currentSite.VTD1_Study_Team_Member__r.VTD1_UserContact__c;
                cas.VTD1_Backup_PI_Contact__c = currentSite.VTR2_Backup_PI_STM__r.VTD1_UserContact__c;
                isUpdated = true;
            }

            if (isUpdated) {
                casesToBeUpdated.add(cas);
            }
        }

        if (casesToBeUpdated.size() <= 5) {
            update casesToBeUpdated;
        } else if(!casesToBeUpdated.isEmpty()) {
            //System.enqueueJob(new VT_D1_AsyncDML(casesToBeUpdated, 'UPDATE', true, 5)); 
            //@author:      IQVIA Strikers
            //@description: Commented the Apex Queable job and replaced with Apex Batch. To avoid multiple jobs in queue.
            Database.executeBatch(new VT_R5_CaseUpdateBatch(casesToBeUpdated), 5);
        }
    }

    private void updateSiteMembers(List<Virtual_Site__c> sites) {
        Map<Id, Id> oldToNewSTMIds = new Map<Id, Id>();
        for (Virtual_Site__c item : sites) {
            oldToNewSTMIds.put(item.VTD1_Study_Team_Member__c, item.VTR2_Backup_PI_STM__c);
        }

        List<Study_Site_Team_Member__c> siteMembers = [
                SELECT Id, VTD1_Associated_PI__c, VTR2_Associated_PI__c
                FROM Study_Site_Team_Member__c
                WHERE VTD1_Associated_PI__c IN :oldToNewSTMIds.keySet() OR VTR2_Associated_PI__c IN :oldToNewSTMIds.keySet()
        ];

        if (! siteMembers.isEmpty()) {
            for (Study_Site_Team_Member__c item : siteMembers) {
                if (oldToNewSTMIds.containsKey(item.VTD1_Associated_PI__c)) {
                    item.VTD1_Associated_PI__c = oldToNewSTMIds.get(item.VTD1_Associated_PI__c);
                }
                if (oldToNewSTMIds.containsKey(item.VTR2_Associated_PI__c)) {
                    item.VTR2_Associated_PI__c = oldToNewSTMIds.get(item.VTR2_Associated_PI__c);
                }
            }
            //update siteMembers;
        }
    }

    private void updateStudyMembers(List<Virtual_Site__c> sites) {
        List<Id> oldPriMemberIds = new List<Id>();
        for (Virtual_Site__c item : sites) {
            oldPriMemberIds.add(item.VTD1_Study_Team_Member__c);
        }
        Map<Id, Study_Team_Member__c> oldMemberMap = new Map<Id, Study_Team_Member__c>([
                SELECT Id, VTD1_PIsAssignedPG__c
            //START RAJESH SH-17440
            //, VTR2_Remote_CRA__c, VTR2_Onsite_CRA__c
            //END:SH-17440
                FROM Study_Team_Member__c
                WHERE Id IN :oldPriMemberIds
        ]);

        for (Virtual_Site__c vs : sites) {
            if (vs.VTD1_Study_Team_Member__c == null) continue;

            this.stmToUpdate.put(vs.VTD1_Study_Team_Member__c, new Study_Team_Member__c(
                    Id = vs.VTD1_Study_Team_Member__c,
                    VTD1_Active__c = false,
                    VTD1_Ready_For_Assignment__c = false,
                    VTD1_VirtualSite__c = null
            ));

            Study_Team_Member__c oldStm = oldMemberMap.get(vs.VTD1_Study_Team_Member__c);
            if (oldStm != null && vs.VTR2_Backup_PI_STM__c != null) {
                this.stmToUpdate.put(vs.VTR2_Backup_PI_STM__c, new Study_Team_Member__c(
                        Id = vs.VTR2_Backup_PI_STM__c,
                        Study_Team_Member__c = null,
                        VTD1_PIsAssignedPG__c = oldStm.VTD1_PIsAssignedPG__c,
                    //START RAJESH SH-17440
                        //VTR2_Remote_CRA__c = oldStm.VTR2_Remote_CRA__c,
                        //VTR2_Onsite_CRA__c = oldStm.VTR2_Onsite_CRA__c,
                    //END:SH-17440
                        VTD1_VirtualSite__c = vs.Id
                ));
            }

        }

        update this.stmToUpdate.values();
    }
}