/**
 * Created by Yuliya Yakushenkova on 10/13/2020.
 */

public class VT_R5_PreRandomizationHomepage implements VT_R5_RandomizationHomepage {

    public String userName;
    public String version;
    public Visit upNext;
    public VT_R5_RandomizationHomepageService.StudyInformation studyInformation;
    public List<VT_R5_RandomizationHomepageService.Teammate> myStudyTeammates;
    public VT_R5_RandomizationHomepageService.NewsFeedSection newsFeed;

    public VT_R5_PreRandomizationHomepage() {
        this.userName = UserInfo.getFirstName();
        this.version = 'PreRandomizedHomepage';
    }

    public VT_R5_RandomizationHomepage getPage(Case caseId) {
        this.upNext = getUpNextSection();
        return this;
    }

    public VT_R5_RandomizationHomepage setTeammateSection(List<VT_R5_RandomizationHomepageService.Teammate> teammates) {
        this.myStudyTeammates = teammates;
        return this;
    }

    public VT_R5_RandomizationHomepage setStudyInformation(VT_R5_RandomizationHomepageService.StudyInformation studyInformation) {
        this.studyInformation = studyInformation;
        return this;
    }

    public VT_R5_RandomizationHomepage addNewsFeed(VT_R5_RandomizationHomepageService.NewsFeedSection news) {
        this.newsFeed = news;
        return this;
    }

    private Visit getUpNextSection() {
        VT_D1_UpNextVisit.ActualVisitWrapper actualVisitWrapper = VT_D1_UpNextVisit.getVisit();
        return new Visit(actualVisitWrapper.actualVisit, actualVisitWrapper.visitName);
    }


    private class Visit {
        public String id;
        public String name;
        public String type;
        public Boolean scheduled = false;
        public Long dateAndTime;

        public Visit(VTD1_Actual_Visit__c actualVisit, String visitName) {
            this.id = actualVisit.Id;
            this.name = visitName;
            this.type = actualVisit.VTD1_Protocol_Visit__r.VTD1_VisitType__c;
            if (actualVisit.VTD1_Scheduled_Date_Time__c != null) {
                this.scheduled = true;
                this.dateAndTime = actualVisit.VTD1_Scheduled_Date_Time__c.getTime() +
                        UserInfo.getTimeZone().getOffset(System.now());
            }
        }
    }
}