/**
 * Created by Rishat Shamratov on 04.10.2019.
 */

@RestResource(UrlMapping='/Patient/CustomLabels/Translate')
global without sharing class VT_R3_RestPatientTranslatedCustomLabels {

    // HERE YOU CAN EDIT THE MAP WITH CUSTOM LABEL MASTER LABELS AND THEIR MASKS FOR TOOLING API REQUEST
    private static Map<String, String> validMasterLabels = new Map<String, String>{
            'PatientCommunity' => '%27%2525PatientCommunity%2525%27',
            'Mobile' => '%27%2525Mobile%2525%27',
            'Login' => '%27%2525Login%2525%27'
    };

    private static Map<String, String> labelsApiNamesAndTranslatedValuesMap = new Map<String, String>();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();

    /**
     * Process request for getting custom labels translation
     *
     * @return Map of strings "API name" to strings "Translation"
     */
    @HttpGet
    global static String getTranslatedCustomLabels() {
        RestResponse response = RestContext.response;
        RestRequest request = RestContext.request;

        // Get specified language from request
        String requestedLanguage = request.params.get('lang');

        // Get all available system languages
        Set<String> validLanguages = new Set<String>();
        List<Schema.PicklistEntry> picklistLanguages = Contact.VTR2_Primary_Language__c.getDescribe().getPicklistValues();
        for (Schema.PicklistEntry picklistValue : picklistLanguages) {
            validLanguages.add(picklistValue.getValue());
        }

        // Get specified custom label master label (Short description)
        String requestedMasterLabel = request.params.get('masterLabel');

        // Get specified mask for custom label master label from a map, that was set
        String masterLabelMask;
        if (validMasterLabels.get(requestedMasterLabel) != null) {
            masterLabelMask = validMasterLabels.get(requestedMasterLabel);
        }

        // Set responses for all cases
        try {
            if (validLanguages.contains(requestedLanguage) && validMasterLabels.containsKey(requestedMasterLabel)) {
                labelsApiNamesAndTranslatedValuesMap = VT_R3_CustomLabelsRenderPageController.getLabelsString(requestedLanguage, masterLabelMask);
            } else {
                if (!validLanguages.contains(requestedLanguage) && !validMasterLabels.containsKey(requestedMasterLabel)) {
                    response.statusCode = 400;
                    cr.buildResponse('Available languages: ' + validLanguages + ' ' + 'Available master labels: ' + validMasterLabels.keySet() + ' ' + requestedLanguage + ' ' + requestedMasterLabel);
                    return JSON.serialize(cr, true);
                } else if (!validLanguages.contains(requestedLanguage)) {
                    response.statusCode = 400;
                    cr.buildResponse('Please, specify one of the valid languages: ' + validLanguages + ' ' + requestedLanguage);
                    return JSON.serialize(cr, true);
                } else if (!validMasterLabels.containsKey(requestedMasterLabel)) {
                    response.statusCode = 400;
                    cr.buildResponse('Please, specify one of the valid master labels: ' + validMasterLabels.keySet() + ' ' + requestedMasterLabel);
                    return JSON.serialize(cr, true);
                }
            }
        } catch (Exception e) {
            response.statusCode = 500;
            return 'SERVER FAILURE. ' + e.getMessage() + ' ' + e.getStackTraceString();
        }

        cr.buildResponse(labelsApiNamesAndTranslatedValuesMap);
        return JSON.serialize(cr, true);
    }
}