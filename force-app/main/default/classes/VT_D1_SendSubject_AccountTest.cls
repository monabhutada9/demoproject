/**
 * Created by User on 19/05/21.
 */
@isTest
private with sharing class VT_D1_SendSubject_AccountTest {

    @TestSetup
    static void setup(){
        DomainObjects.User_t user = new DomainObjects.User_t()
                .setCreatedDate(Date.today().addDays(-5));
        DomainObjects.Account_t acc = new  DomainObjects.Account_t()
                .setVTD1_LegalFirstName('Test1')
                .setRecordTypeByName('Sponsor');
        acc.persist();
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(acc);
        DomainObjects.Contact_t con = new DomainObjects.Contact_t()
                .addAccount(acc)
                .setAccountId(acc.Id);
        Case oldCase = (Case) new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .addStudy(study)
                .addUser(user)
                .addContact(con)
                .setStatus('Consented')
                .record;
        insert oldCase;
        //new DomainObjects.Case_t()
        //        .setRecordTypeByName('VTD1_PCF')
        //        .addStudy(study)
        //        .addUser(user)
        //        .addContact(con)
        //        .setStatus('Consented')
        //        .persist();
        HealthCloudGA__CandidatePatient__c candidatePatient = (HealthCloudGA__CandidatePatient__c) new DomainObjects.HealthCloudGA_CandidatePatient_t()
                .setRr_Email('test@email.com').record;
        candidatePatient.VTR4_CaseForTransfer__c = oldCase.Id;
        insert candidatePatient;
        Account currentAccount = (Account) acc.record;
        currentAccount.Candidate_Patient__c = candidatePatient.Id;
        update currentAccount;
    }

    @isTest
    private static void doTest(){
        Test.startTest();
        List<Account> accountObj = [SELECT Id,VTD1_LegalFirstName__c FROM Account LIMIT 1];
        accountObj[0].VTD1_LegalFirstName__c = 'Test2';
        update accountObj;
        Test.stopTest();
    }
}