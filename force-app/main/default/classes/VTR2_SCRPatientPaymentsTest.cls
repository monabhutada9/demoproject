@IsTest
private class VTR2_SCRPatientPaymentsTest {

    private static final String SEARCH_STRING = '{"sortingParams": null,"filterParams": [],"searchParams": null,"entriesOnPage": 10,"currentPage": 1}';

    @TestSetup
    static void testDataSetup() {
        HealthCloudGA__CarePlanTemplate__c study = new HealthCloudGA__CarePlanTemplate__c(
                Name = 'TestName'
        );
        insert study;
        Case cas = new Case(
                VTD1_Study__r = study
        );
        insert cas;
        List<VTD1_Patient_Payment__c> patientPayments = new List<VTD1_Patient_Payment__c>{
                new VTD1_Patient_Payment__c(
                        Name = 'TestPayment1',
                        VTD1_Amount__c = 5000.00,
                        VTD1_Payment_Issued__c = false,
                        VTD1_PG_Approved__c = false,
                        VTD1_Activity_Complete__c = false,
                        VTD1_Clinical_Study_Membership__c = cas.Id,
                        VTD1_Clinical_Study_Membership__r = cas
                ),
                new VTD1_Patient_Payment__c(
                        Name = 'TestPayment2',
                        VTD1_Amount__c = 500.00,
                        VTD1_Payment_Issued__c = false,
                        VTD1_PG_Approved__c = false,
                        VTD1_Activity_Complete__c = false,
                        VTD1_Clinical_Study_Membership__c = cas.Id,
                        VTD1_Clinical_Study_Membership__r = cas
                )
        };
        insert patientPayments;
        List<Task> tasks = new List<Task>{
                new Task(
                        WhatId = patientPayments[0].Id,
                        OwnerId = UserInfo.getUserId(),
                        Status = 'Open',
                        VTD2_Task_Unique_Code__c = '400'
                ),
                new Task(
                        WhatId = patientPayments[1].Id,
                        OwnerId = UserInfo.getUserId(),
                        Status = 'Open',
                        VTD2_Task_Unique_Code__c = '400'
                )
        };
        insert tasks;
    }
 
    @IsTest
    static void getPatientPayments_Positive() {
        Id caseId = [SELECT Id FROM Case LIMIT 1].Id;
        Test.startTest();
        VTR2_SCRPatientPayments.PatientPaymentsWrapper patientPayments = VTR2_SCRPatientPayments.getPatientPayments(caseId, SEARCH_STRING);
        Test.stopTest();
        System.debug('getPatientPayments_Positive');
        System.debug(JSON.serializePretty(patientPayments));
        System.assertEquals(2, patientPayments.paymentsList.size(), 'Incorrect Size of Returned Records');
        for (VTD1_Patient_Payment__c p : patientPayments.paymentsList) {
            System.assertEquals(caseId, p.VTD1_Clinical_Study_Membership__c, 'Incorrect caseId');
        }

    }

    @IsTest
    static void getPatientPayments_Negative() {
        Id caseId = null;
        VTR2_SCRPatientPayments.PatientPaymentsWrapper patientPayments = null;
        Test.startTest();
        try {
            patientPayments = VTR2_SCRPatientPayments.getPatientPayments(caseId, null);
        } catch (AuraHandledException e) {
            System.assertEquals(null, patientPayments);
        }
        Test.stopTest();
    }

    @IsTest
    static void reviewPayment_Positive() {
        List<VTD1_Patient_Payment__c> paymentsList = [SELECT Id FROM VTD1_Patient_Payment__c ORDER BY Name ASC];
        Test.startTest();
        VTR2_SCRPatientPayments.reviewPayment(paymentsList[0].Id, true);
        VTR2_SCRPatientPayments.reviewPayment(paymentsList[1].Id, false);
        Test.stopTest();
        List<VTD1_Patient_Payment__c> paymentsList2 = [SELECT Id, VTD1_PG_Approved__c, VTD1_Activity_Complete__c FROM VTD1_Patient_Payment__c ORDER BY Name ASC];
        System.assertEquals(true, paymentsList2[0].VTD1_PG_Approved__c);
        System.assertEquals(false, paymentsList2[1].VTD1_Activity_Complete__c);
        List<Task> tasks = [SELECT Id, Status FROM Task WHERE VTD2_Task_Unique_Code__c = '400'];
        for (Task t : tasks) {
            System.assertEquals('Completed', t.Status);
        }
    }

    @IsTest
    static void reviewPayment_Negative() {
        Test.startTest();
        try {
            VTR2_SCRPatientPayments.reviewPayment(null, true);
        } catch (AuraHandledException e) {

        }
        Test.stopTest();
    }

}