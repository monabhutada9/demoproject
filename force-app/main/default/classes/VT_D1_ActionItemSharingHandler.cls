/**
 * Created by Leonid Bartenev
 */

public class VT_D1_ActionItemSharingHandler {
    public static void rebuildSharingForActionItems(Set<Id> aiIdList){
        VT_R3_GlobalSharing.doSharing(new List<Id>(aiIdList));
    }
}