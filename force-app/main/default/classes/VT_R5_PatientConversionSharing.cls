/**
* @author: Carl Judge
* @date: 12-Oct-20
* @description: Create group shares for patient Case/User/Account, plus study shares for the patients themselves
**/

public without sharing class VT_R5_PatientConversionSharing {
    public static final String READ_ACCESS = 'Read';
    public static final String EDIT_ACCESS = 'Edit';

    public static void createConversionShares(Set<Id> carePlanIds) {
        List<Case> carePlans = [
            SELECT Id,
                VTD1_Patient_User__c,
                Contact.AccountId,
                VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c,
                VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c,
                VTD1_Study__c
            FROM Case
            WHERE Id IN :carePlanIds AND VTD1_Virtual_Site__c != NULL
        ];

        List<CaseShare> caseShares = new List<CaseShare>();
        List<AccountShare> accShares = new List<AccountShare>();
        List<UserShare> userShares = new List<UserShare>();
        List<HealthCloudGA__CarePlanTemplate__Share> studyShares = new List<HealthCloudGA__CarePlanTemplate__Share>();

        for (Case carePlan : carePlans) {
            caseShares.addAll(getGroupCaseShares(carePlan));
            accShares.addAll(getGroupAccountShares(carePlan));
            accShares.add(getAccountShare(carePlan.Contact.AccountId, carePlan.VTD1_Patient_User__c, EDIT_ACCESS));
            userShares.addAll(getGroupUserShares(carePlan));
            studyShares.add(getPatientStudyShare(carePlan));
        }

        insertShares(caseShares);
        insertShares(accShares);
        insertShares(userShares);
        insertShares(studyShares);
    }

    private static List<CaseShare> getGroupCaseShares(Case carePlan) {
        return new List<CaseShare>{
            getCaseShare(carePlan.Id, carePlan.VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c, READ_ACCESS),
            getCaseShare(carePlan.Id, carePlan.VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c, EDIT_ACCESS)
        };
    }

    private static CaseShare getCaseShare(Id caseId, Id userOrGroupId, String accessLevel) {
        return new CaseShare(
            CaseId = caseId,
            CaseAccessLevel = accessLevel,
            UserOrGroupId = userOrGroupId
        );
    }

    private static List<AccountShare> getGroupAccountShares(Case carePlan) {
        return new List<AccountShare>{
            getAccountShare(carePlan.Contact.AccountId, carePlan.VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c, READ_ACCESS),
            getAccountShare(carePlan.Contact.AccountId, carePlan.VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c, EDIT_ACCESS)
        };
    }

    private static AccountShare getAccountShare(Id accountId, Id userOrGroupId, String accessLevel) {
        return new AccountShare(
            AccountId = accountId,
            UserOrGroupId = userOrGroupId,
            AccountAccessLevel = accessLevel,
            ContactAccessLevel = accessLevel,
            OpportunityAccessLevel = accessLevel
        );
    }

    private static List<UserShare> getGroupUserShares(Case carePlan) {
        return new List<UserShare>{
            getUserShare(carePlan.VTD1_Patient_User__c, carePlan.VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c, READ_ACCESS),
            getUserShare(carePlan.VTD1_Patient_User__c, carePlan.VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c, EDIT_ACCESS)
        };
    }
    
    private static UserShare getUserShare(Id userId, Id userOrGroupId, String accessLevel) {
        return new UserShare(
            UserId = userId,
            UserAccessLevel = accessLevel,
            UserOrGroupId = userOrGroupId
        );
    }

    private static HealthCloudGA__CarePlanTemplate__Share getPatientStudyShare(Case carePlan) {
        return new HealthCloudGA__CarePlanTemplate__Share(
            ParentId = carePlan.VTD1_Study__c,
            RowCause = 'GlobalSharing__c',
            AccessLevel = 'Read',
            UserOrGroupId = carePlan.VTD1_Patient_User__c
        );
    }

    private static void insertShares(List<SObject> sharesToInsert) {
        if (!sharesToInsert.isEmpty()) {
            List<Database.SaveResult> results = Database.insert(sharesToInsert, false);
            processSaveResults(VT_R5_DatabaseResultWrapper.wrapResults(results), sharesToInsert);
        }
    }

    private static void processSaveResults(List<VT_R5_DatabaseResultWrapper> results, List<SObject> shares) {
        List<SObject> sharesToRetry = new List<SObject>();
        List<VT_R5_DatabaseResultWrapper> resultsToLog = new List<VT_R5_DatabaseResultWrapper>();
        List<SObject> sharesToLog = new List<SObject>();

        for (Integer i = 0; i < results.size(); i++) {
            if (!results[i].isSuccess() && VT_D1_AsyncDML.isRetryDML(results[i].getErrors(), VT_R3_AbstractGlobalSharingLogic.RETRY_ERRORS).isRetry) {
                sharesToRetry.add(shares[i]);
            } else if (!results[i].isSuccess() || Test.isRunningTest()) {
                resultsToLog.add(results[i]);
                sharesToLog.add(shares[i]);
            }
        }

        if (!sharesToRetry.isEmpty()) {
            VT_D1_AsyncDML asyncDML = new VT_D1_AsyncDML(sharesToRetry, DMLOperation.INS);
            asyncDML.setAllOrNothing(false);
            asyncDML.setBatchSize(2000);
            asyncDML.setRetryErrors(VT_R3_AbstractGlobalSharingLogic.RETRY_ERRORS);
            asyncDML.setLoggingEnabled(true); // SH-21968 INC10511409 - Study Level Sharing Access for Patients
            asyncDML.setLogFullRecord(true);
            asyncDML.enqueue();
        }

        if (!resultsToLog.isEmpty() && !sharesToLog.isEmpty()) {
            logErrors(resultsToLog, sharesToLog); // SH-21968
        }
    }

    private static void logErrors(List<VT_R5_DatabaseResultWrapper> resultsToLog, List<SObject> recordsToLog) {
        try {
            VT_D1_AsyncDML.PlatformEventErrorLogger logger = new VT_D1_AsyncDML.PlatformEventErrorLogger(
                    resultsToLog, recordsToLog, 'VT_R5_PatientConversionSharing', true);
            logger.publish();
        } catch (Exception ex) {
            System.debug('Unable to log errors: ' + ex.getMessage());
        }
    }
}