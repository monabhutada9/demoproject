@IsTest
public with sharing class VT_R2_PIMyPatientGuidesControllerTest {

    public static void testSuccessGetMyPatientGuides() {
        User patientGuide = [SELECT Id FROM User WHERE VTD1_Profile_Name__c = 'Primary Investigator' ORDER BY CreatedDate DESC LIMIT 1];
        System.runAs(patientGuide) {
            Test.startTest();
            String result = VT_D2_PIMyPatientGuidesController.getMyPatientGuides(patientGuide.Id);
            Test.stopTest();
            System.assert(String.isNotEmpty(result));
        }
    }

    public static void testFailedGetMyPatientGuides() {
        try {
            VT_D2_PIMyPatientGuidesController.getMyPatientGuides(null);
        } catch (Exception ex) {
        }
    }
}