/**
* @author: Olga Baranova
* @date: 23-01-2020
* @description:
**/
public without sharing class VT_R4_eProCreatorFromCase extends VT_R4_AbstractEproCreator {
    private List<Case> statusChangedCases = new List<Case>();
    private Map<Id, Map<String, List<VTD1_Protocol_ePRO__c>>> studyStatusEpros = new Map<Id, Map<String, List<VTD1_Protocol_ePRO__c>>>();
    public void createEpros(List<Case> cases, Map<Id, Case> oldMap) {
        queryCases(cases, oldMap);
        if (!statusChangedCases.isEmpty()) {
            getCaregiverMap();
            getStudyStatusEpros();
            initialiseProtocolAmendmentVersioning();
            generateEpros();
        }
    }
    private void queryCases(List<Case> cases, Map<Id, Case> oldMap) {
        Set<Id> statusChangedCasesIds = new Set<Id>();
        for (Case item : cases) {
            if (oldMap == null) {
                if (item.Status != null) {
                    statusChangedCasesIds.add(item.Id);
                }
            } else {
                Case old = oldMap.get(item.Id);
                if (item.Status != old.Status) {
                    statusChangedCasesIds.add(item.Id);
                }
            }
        }
        if (!statusChangedCasesIds.isEmpty()) {
            for (Case item : [
                    SELECT Id,
                            Status,
                            VTD1_ePro_can_be_completed_by_Caregiver__c,
                            VTD1_Patient_User__r.Contact.AccountId,
                            VTD1_Patient_User__c,
                            VTD1_Patient_User__r.VTD2_UserTimezone__c,
                            VTD1_Study__c,
                            VTD1_Virtual_Site__c,
                            VTD1_Primary_PG__c,
                            VTR2_SiteCoordinator__c,
                            VTR5_SubsetSafety__c,
                            VTR5_SubsetImmuno__c
                    FROM Case
                    WHERE Id IN :statusChangedCasesIds
                    AND VTD1_Patient_User__c != NULL
            ]) {
                statusChangedCases.add(item);
            }
        }
    }
    private void getCaregiverMap() {
        List<Id> patientAccIds = new List<Id>();
        for (Case item : statusChangedCases) {
            if (item.VTD1_Patient_User__r.Contact.AccountId != null) {
                patientAccIds.add(item.VTD1_Patient_User__r.Contact.AccountId);
            }
        }
        if (!patientAccIds.isEmpty()) {
            for (User item : [
                    SELECT Id, Contact.AccountId
                    FROM User
                    WHERE Contact.AccountId IN :patientAccIds
                    AND Contact.RecordType.DeveloperName = 'Caregiver'
                    ORDER BY Contact.VTD1_Primary_CG__c ASC
            ]) {
                caregiverMap.put(item.Contact.AccountId, item);
            }
        }
    }
    private void getStudyStatusEpros() {
        List<String> milestones = new List<String>();
        List<Id> studyIds = new List<Id>();
        for (Case item : statusChangedCases) {
            milestones.add(item.Status);
            studyIds.add(item.VTD1_Study__c);
        }
        for (VTD1_Protocol_ePRO__c item : [
                SELECT Id,
                        Name,
                        VTD1_Response_Window__c,
                        VTR4_RootProtocolId__c,
                        VTD1_Protocol_Amendment__c,
                        VTR4_RemoveFromProtocol__c,
                        VTR4_EventOffset__c,
                        VTD1_Reminder_Window__c,
                        VTD1_Caregiver_on_behalf_of_Patient__c,
                        RecordType.DeveloperName,
                        VTR5_eCOAEventName__c,
                        VTD1_Study__c,
                        VTD1_Subject__c,
                        VTR2_Protocol_Reviewer__c,
                        VTR2_eDiary_delay__c,
                        VTR4_EventOffsetPeriod__c,
                        VTR4_EventTime__c,
                        VTR4_EventType__c,
                        VTR4_PatientMilestone__c,
                        VTR5_SubsetSafetyID__c,
                        VTR5_SubsetImmunoID__c
                FROM VTD1_Protocol_ePRO__c
                WHERE VTD1_Study__c IN :studyIds
                    AND VTD1_Protocol_ePRO__c.VTR4_PatientMilestone__c IN :milestones AND VTD1_isArchivedVersion__c = FALSE
        ]) {
            if (!studyStatusEpros.containsKey(item.VTD1_Study__c)) {
                studyStatusEpros.put(item.VTD1_Study__c, new Map<String, List<VTD1_Protocol_ePRO__c>>());
            }
            if (!studyStatusEpros.get(item.VTD1_Study__c).containsKey(item.VTR4_PatientMilestone__c)) {
                studyStatusEpros.get(item.VTD1_Study__c).put(item.VTR4_PatientMilestone__c, new List<VTD1_Protocol_ePRO__c>());
            }
            studyStatusEpros.get(item.VTD1_Study__c).get(item.VTR4_PatientMilestone__c).add(item);
        }
    }
    private void initialiseProtocolAmendmentVersioning() {
        List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper> wrappedRecords = new List<VT_R4_ProtocolAmendmentVersionHelper.RecordWrapper>();
        for (Map<String, List<VTD1_Protocol_ePRO__c>> statusEprosMap : studyStatusEpros.values()) {
            wrappedRecords.addAll(wrapListOfLists(statusEprosMap.values()));
        }
        paVersionHelper = new VT_R4_ProtocolAmendmentVersionHelper(wrappedRecords);
    }
    private void generateEpros() {
        for (Case cas : statusChangedCases) {
            if (!(studyStatusEpros.containsKey(cas.VTD1_Study__c) && studyStatusEpros.get(cas.VTD1_Study__c).containsKey(cas.Status))) {
                continue;
            }
            List<VTD1_Protocol_ePRO__c> unVersionedTemplates = studyStatusEpros.get(cas.VTD1_Study__c).get(cas.Status);
            List<VTD1_Protocol_ePRO__c> correctVersionProtocols = paVersionHelper.getCorrectVersions(cas.VTD1_Virtual_Site__c, unVersionedTemplates);
            for (VTD1_Protocol_ePRO__c template : correctVersionProtocols) {
                if (isCaregiverSubjectWithNoCaregiver(cas, template)) {
                    continue;
                }
                if (template.RecordType.DeveloperName != 'VTR5_External') {
                    String patientTimeZone = cas.VTD1_Patient_User__r.VTD2_UserTimezone__c;
                    ePros.add(getNewEpro(cas, template, getEventDateTime(template, patientTimeZone))); //create SH Diaries
                } else if (isECoaEventNeeded(template, cas)) {
                    Id subjectId = isSubjectCaregiver(template) ? getCaregiverId(cas, template) : cas.VTD1_Patient_User__c;
                    addEvent(subjectId, template.VTR5_eCOAEventName__c); //send date to eCoa event to trigger the diary
                }
            }
        }
        if (!ePros.isEmpty()) {
            insert ePros;
        }
        sendEvents();
    }

    private VTD1_Survey__c getNewEpro(Case cas, VTD1_Protocol_ePRO__c template, Datetime availableDate) {
        if (template.VTR2_eDiary_delay__c != null && template.VTR2_eDiary_delay__c > 0) {
            availableDate = availableDate.addDays(template.VTR2_eDiary_delay__c.intValue());
        }
        VTD1_Survey__c newSurvey = new VTD1_Survey__c(
                Name = template.Name,
                VTD1_CSM__c = cas.Id,
                //VTD1_Status__c = VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_NOT_STARTED, // use the default picklist value for the specific RT
                VTD1_Date_Available__c = availableDate,
                VTD1_Protocol_ePRO__c = template.Id,
                VTD1_Patient_User_Id__c = cas.VTD1_Patient_User__c,
                VTD1_Caregiver_User_Id__c = getCaregiverId(cas, template),
                RecordTypeId = surveyRecTypeMap.get(template.RecordType.DeveloperName).getRecordTypeId(),
                VTD1_Due_Date__c = template.VTD1_Response_Window__c != null ?
                        availableDate.addMinutes(Math.round(template.VTD1_Response_Window__c * 60)) : availableDate,
                VTR2_Reviewer_User__c = getReviewerForEDiary(cas.VTD1_Primary_PG__c, cas.VTR2_SiteCoordinator__c, template.VTR2_Protocol_Reviewer__c)
        );
        newSurvey.VTD1_Reminder_Due_Date__c = getReminderDate(newSurvey.VTD1_Due_Date__c, template.VTD1_Reminder_Window__c);
        return newSurvey;
    }

    private Boolean isCaregiverSubjectWithNoCaregiver(Case cas, VTD1_Protocol_ePRO__c template) {
        return
                isSubjectCaregiver(template) &&
                        !caregiverMap.containsKey(cas.VTD1_Patient_User__r.Contact.AccountId);
    }
}