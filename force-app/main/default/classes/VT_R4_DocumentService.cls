/**
 * Created by Maksim Fedarenka on 20-May-20.
 */
public with sharing class VT_R4_DocumentService {
	/**
	 * @description Populates the VTR4_AuditStudy__c, VTR4_AuditSite__c document fields, using the following scheme:
	 * document->case->site->study or document.oldSite->study or document->oldStudy
	 * @jira SH-11423 Create Study & Virtual Site Direct Link on Patient-Level Documents
	 * @author Maksim Fedarenka
	*/
	public static void populateStudyAndVirtualSite(List<VTD1_Document__c> records) {
		for (VTD1_Document__c document : records) {
			Case clinicalStudyMembership = document.VTD1_Clinical_Study_Membership__r;
			Virtual_Site__c site = document.VTD1_Site__r;
			Boolean isCaseFilled = (clinicalStudyMembership != null);
			Boolean isSiteFilled = (site != null);
			document.VTR4_AuditStudy__c = (isCaseFilled) ? clinicalStudyMembership.VTD1_Virtual_Site__r.VTD1_Study__c
				: (isSiteFilled) ? site.VTD1_Study__c
				: document.VTD1_Study__c;
			document.VTR4_AuditSite__c = (isCaseFilled) ? clinicalStudyMembership.VTD1_Virtual_Site__c
				: (isSiteFilled) ? site.Id
				: document.VTD2_Eligibility_Assesment_For__c;
		}
	}
	/**
	 * @description Queries and populates document VTD1_Clinical_Study_Membership__r, VTD1_Site__r data
	 * @author Maksim Fedarenka
	 */
	public static List<VTD1_Document__c> queryDocumentCaseAndSiteRecords(List<VTD1_Document__c> records) {
		Set<Id> uniqCaseIds = collectCaseIds(records);
		Map<Id, Case> caseIdToCase = new Map<Id, Case>(selectCases(uniqCaseIds));
		Set<Id> uniqSiteIds = collectSiteIds(records);
		Map<Id, Virtual_Site__c> siteIdToSite = new Map<Id, Virtual_Site__c>(selectSites(uniqSiteIds));
		for (VTD1_Document__c document : records) {
			Id documentCaseId = document.VTD1_Clinical_Study_Membership__c;
			Id documentSiteId = document.VTD1_Site__c;
			if (documentCaseId != null && caseIdToCase.get(documentCaseId) != null) {
				document.VTD1_Clinical_Study_Membership__r = caseIdToCase.get(documentCaseId);
			}
			if (documentSiteId != null && siteIdToSite.get(documentSiteId) != null) {
				document.VTD1_Site__r = siteIdToSite.get(documentSiteId);
			}
		}
		return records;
	}
	public static Set<Id> collectCaseIds(List<VTD1_Document__c> documents) {
		Set<Id> uniqCaseIds = new Set<Id>();
		for (VTD1_Document__c document : documents) {
			if (document.VTD1_Clinical_Study_Membership__c != null) {
				uniqCaseIds.add(document.VTD1_Clinical_Study_Membership__c);
			}
		}
		return uniqCaseIds;
	}
	public static Set<Id> collectSiteIds(List<VTD1_Document__c> documents) {
		Set<Id> uniqSiteIds = new Set<Id>();
		for (VTD1_Document__c document : documents) {
			if (document.VTD1_Site__c != null) {       
				uniqSiteIds.add(document.VTD1_Site__c);
			}
		}
		return uniqSiteIds;
	}
	public static List<Case> selectCases(Set<Id> caseIds) {
		return (caseIds.isEmpty()) ? new List<Case>() : [
			SELECT Id, VTD1_Virtual_Site__r.VTD1_Study__c
			FROM Case
			WHERE Id IN :caseIds
		];
	}
	public static List<Virtual_Site__c> selectSites(Set<Id> siteIds) {
		return (siteIds.isEmpty()) ? new List<Virtual_Site__c>() : [
			SELECT Id, VTD1_Study__c
			FROM Virtual_Site__c
			WHERE Id IN :siteIds
		];
	}
}