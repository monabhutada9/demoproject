/**
* @author: Carl Judge
* @date: 27-Aug-18
* @description: Verify updates made to protocol ePRO are allowed
**/

public without sharing class VT_D1_Protocol_ePRO_Update_Verifier {

    public static final String ERROR_MESSAGE = 'Cannot update this Protocol ePRO because ePROs are already in progress. Clone a new version instead.';
    public static Boolean ignoreValidation = false;

    public static Set<Id> getInprogressIds(List<VTD1_Protocol_ePRO__c> recs) {
        Map<Id, VTD1_Protocol_ePRO__c> recMap = new Map<Id, VTD1_Protocol_ePRO__c>(recs);
        Set<Id> inProgressIds = new Set<Id>();
        if (! ignoreValidation) {
            for (VTD1_Survey__c item : [
                    SELECT VTD1_Protocol_ePRO__c
                    FROM VTD1_Survey__c
                    WHERE VTD1_Protocol_ePRO__c IN :recMap.keySet()
                        AND VTD1_Status__c != :VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_NOT_STARTED
                        AND VTD1_Protocol_ePRO__r.VTR4_Ignore_Edit_Validation__c = false
            ]) {
                if(!recMap.get(item.VTD1_Protocol_ePRO__c).VTR4_Ignore_Edit_Validation__c) {
                    inProgressIds.add(item.VTD1_Protocol_ePRO__c);
                }
            }
        }
        return inProgressIds;
    }
}