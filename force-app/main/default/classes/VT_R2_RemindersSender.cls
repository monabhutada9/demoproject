/**
 * Created by shume on 06.05.2019.
 */
global without sharing class VT_R2_RemindersSender {
    public static List<VTR2_Reminders__mdt> reminders;
    public static Integer CHUNK_SIZE = 50;
    public class ParamsHolder {
        @InvocableVariable(Label = 'Actual Visit Id' Required = true)
        public Id actualVisitId;
        @InvocableVariable(Label = 'Hours Before' Required = false)
        public Integer hoursBefore;
        public ParamsHolder(Id actualVisitId, Integer hoursBefore) {
            this.actualVisitId = actualVisitId;
            this.hoursBefore = hoursBefore;
        }
        public ParamsHolder() {
        }
    }
    class EmailsCreator implements Queueable {
        final List <VT_R3_EmailsSender.ParamsHolder> paramsList;
        final Integer index;
        public EmailsCreator(List <VT_R3_EmailsSender.ParamsHolder> paramsList, Integer index) {
            this.paramsList = paramsList;
            this.index = index;
        }
        public void execute(QueueableContext queueableContext) {
            Integer endIndex = index + CHUNK_SIZE;
            if (endIndex > paramsList.size()) {
                endIndex = paramsList.size();
            }
            List <VT_R3_EmailsSender.ParamsHolder> chunk = new List<VT_R3_EmailsSender.ParamsHolder>();
            for (Integer i = index; i < endIndex; i ++) {
                chunk.add(paramsList[i]);
            }
            VT_R3_EmailsSender.sendEmails(chunk);
            if (endIndex != paramsList.size()) {
                System.enqueueJob(new EmailsCreator(paramsList, index + CHUNK_SIZE));
            }
        }
    }
    static Map <String, String> conditionsMap = new Map<String, String>{
            'Onboarding_Type__c' => 'Onboarding_Type_Condition__c',
            'Sub_Type__c' => 'Sub_Type_Condition__c',
            'Visit_Record_Type__c' => 'Visit_Record_Type_Condition__c',
            'Visit_Type__c' => 'Visit_Type_Condition__c'
    };
    @InvocableMethod
    public static void sendReminders(List <ParamsHolder> params) {
        Set <Integer> hours = new Set<Integer>();
        for (ParamsHolder holder : params) {
            hours.add(holder.hoursBefore);
        }
        if (reminders == null) {
            reminders = [
                    SELECT Id,
                            Email_Template__c,
                            Hours__c,
                            //Post_to_chatter__c,
                            Profile_Name__c,
                            //TN_Cataloge_Task__c,
                            TN_Cataloge_Notification__c,
                            Visit_Record_Type__c,
                            Visit_Type__c,
                            Sub_Type__c,
                            Onboarding_Type__c,
                            Onboarding_Type_Condition__c,
                            Sub_Type_Condition__c,
                            Visit_Record_Type_Condition__c,
                            Visit_Type_Condition__c
                    FROM VTR2_Reminders__mdt
                    WHERE VTR3_ReminderType__c = 'Visit' AND Hours__c IN :hours
            ];
            System.debug('MDT ** ' + reminders);
            if (reminders.isEmpty()) return;
        }
        Datetime dt = System.now();
        Set <Id> visitIds = new Set<Id>();
        for (ParamsHolder param : params) {
            visitIds.add(param.actualVisitId);
        }
        // notifications
        List <String> tnCatalogNotificationCodes = new List<String>();
        List <Id> sourceNotificationIds = new List<Id>();
        Map <String, String> subjectParams = new Map<String, String>();
        Map <String, String> linkToRelatedEventMap = new Map<String, String>();
        // emails
        Map<Visit_Member__c, List <String>> mapVMemberEmails = new Map<Visit_Member__c, List <String>>();
        List <String> templateNamesAll = new List<String>();
        Set<String> visitEmailTemplates = new Set<String>();
        for (EmailTemplate emailTemplate : [SELECT DeveloperName FROM EmailTemplate WHERE Folder.Name = 'Visit emails']){
            visitEmailTemplates.add(emailTemplate.DeveloperName);
        }
        List<Id> videoConfVisitMembersIds = new List<Id>();
        List <VTD1_Actual_Visit__c> visits = [
                SELECT Id, VTD1_Status__c, toLabel(VTD1_Status__c) statusLabel, VTD1_Scheduled_Date_Time__c, RecordType.DeveloperName,
                        VTD2_Common_Visit_Type__c, VTD1_VisitType_SubType__c, VTD1_Onboarding_Type__c, VTD1_Contact__c, VTR2_Televisit__c, (
                        SELECT Id, VTD1_External_Participant_Type__c, VTD1_External_Participant_Email__c, VTD1_Participant_User__c,
                                VTD1_Participant_User__r.ContactId
                        FROM Visit_Members__r
                )
                FROM VTD1_Actual_Visit__c
                WHERE Id IN :visitIds AND
                VTD1_Status__c IN (:VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_SCHEDULED, :VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_RESCHEDULED)
        ];
        Map <Id, VTD1_Actual_Visit__c> vmIdToVisitMap = new Map<Id, VTD1_Actual_Visit__c>();
        for (VTD1_Actual_Visit__c visit : visits) {
            for (VTR2_Reminders__mdt reminder : reminders) {
                System.debug('r = ' + reminder);
                Datetime bottomTimeBound = dt.addHours((Integer) reminder.Hours__c).addMinutes(-5);
                Datetime topTimeBound = dt.addHours((Integer) reminder.Hours__c).addMinutes(5);
                System.debug('times = ' + visit.VTD1_Scheduled_Date_Time__c + ' ' + bottomTimeBound + ' ' + topTimeBound);
                Boolean inTimeInterval = visit.VTD1_Scheduled_Date_Time__c > bottomTimeBound &&
                        visit.VTD1_Scheduled_Date_Time__c <= topTimeBound;
                System.debug('inTimeInterval = ' + inTimeInterval);
                if (!inTimeInterval) {
                    continue;
                }
                Boolean checkInConditions = checkInConditions(reminder, visit);
                System.debug('checkInConditions = ' + checkInConditions);
                if (!checkInConditions) {
                    continue;
                }
                for (Visit_Member__c vm : visit.Visit_Members__r) {
                    vmIdToVisitMap.put(vm.Id, visit);
                    if (reminder.Profile_Name__c.indexOf(vm.VTD1_External_Participant_Type__c) >= 0 || (reminder.Profile_Name__c == 'Other External' && vm.VTD1_Participant_User__c == null)) {
                        if (reminder.TN_Cataloge_Notification__c != null && vm.VTD1_External_Participant_Type__c != 'Caregiver') {
                            tnCatalogNotificationCodes.add(reminder.TN_Cataloge_Notification__c);
                            sourceNotificationIds.add(vm.Id);
                            linkToRelatedEventMap.put(reminder.TN_Cataloge_Notification__c + visit.Id,
                                    visit.Id);
                        }
                        System.debug('email add ' + reminder.Email_Template__c + ' for ' + vm.Id);
                        if (reminder.Email_Template__c != null) {
                            List <String> templateNames = mapVMemberEmails.get(vm);
                            if (templateNames == null) {
                                templateNames = new List<String>();
                                mapVMemberEmails.put(vm, templateNames);
                            }
                            //mapVMemberEmails.put(vm, r.Email_Template__c);
                            //TODO
                            if(visit.VTR2_Televisit__c && visitEmailTemplates.contains(reminder.Email_Template__c + '_TV')){
                                templateNames.add(reminder.Email_Template__c + '_TV');
                            } else {
                                templateNames.add(reminder.Email_Template__c);
                            }
                            templateNamesAll.add(reminder.Email_Template__c);
                            if (reminder.Email_Template__c.contains('videoConf')) {
                                videoConfVisitMembersIds.add(vm.Id);
                            }
                        }
                    }
                }
            }
        }
        System.debug('VT_R2_RemindersSender.generateNotifications');
        System.debug('tnCatalogNotificationCodes = ' + tnCatalogNotificationCodes);
        System.debug('sourceNotificationIds = ' + sourceNotificationIds);
        if (!tnCatalogNotificationCodes.isEmpty() && !Test.isRunningTest()) {
            VT_D2_TNCatalogNotifications.generateNotifications(new List<String>(tnCatalogNotificationCodes), sourceNotificationIds, subjectParams, linkToRelatedEventMap, null);
        }
        if (!mapVMemberEmails.isEmpty()) {
            //List <OrgWideEmailAddress> owEmailAddresses = [SELECT Id FROM OrgWideEmailAddress ORDER BY CreatedDate LIMIT 1];
            Map <String, String> templates = new Map <String, String> ();
            for (EmailTemplate et : [
                    SELECT Id, DeveloperName
                    FROM EmailTemplate
                    WHERE DeveloperName IN :templateNamesAll
            ]) {
                templates.put(et.DeveloperName, et.Id);
            }
            if (!templates.isEmpty()) {
                //List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                Map<Id, Id> vmVideoConfMemberMap = new Map<Id, Id>();
                if (!videoConfVisitMembersIds.isEmpty()) {
                    for (VTD1_Conference_Member__c cm : [
                            SELECT Id, VTD1_Visit_Member__c
                            FROM VTD1_Conference_Member__c
                            WHERE VTD1_Visit_Member__c IN :videoConfVisitMembersIds
                    ]) {
                        vmVideoConfMemberMap.put(cm.VTD1_Visit_Member__c, cm.Id);
                    }
                }
                List<VT_R3_EmailsSender.ParamsHolder> mailParams = new List<VT_R3_EmailsSender.ParamsHolder>();
                for (Visit_Member__c vm : mapVMemberEmails.keySet()) {
                    List <String> templateNames = mapVMemberEmails.get(vm);
                    System.debug('RemindersSender templateNames ' + templateNames);
                    for (String templateName : templateNames) {
                        mailParams.add(new VT_R3_EmailsSender.ParamsHolder (
                                vmIdToVisitMap.get(vm.Id).VTD1_Contact__c,
                                (templateName.contains('videoConf') && vmVideoConfMemberMap.get(vm.Id) != null) ? (String) vmVideoConfMemberMap.get(vm.Id) : (String) vm.Id,
                                templateName,
                                vm.VTD1_External_Participant_Email__c,
                                true
                        ));
                        /*
                        String tId = templates.get(templateName);
                        if (tId != null) {
                            System.debug('tId = ' + tId + ' for ' + vm);
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            List<String> toAddresses = new List<String>();
                            mail.setTemplateId(tId);
                            Id videoConfMember = vmVideoConfMemberMap.get(vm.Id);
                            if (templateName.contains('videoConf') && videoConfMember!=null) {
                                System.debug('videoConfMember ' + videoConfMember);
                                mail.setWhatId(videoConfMember);
                            } else {
                                mail.setWhatId(vm.Id);
                            }
                            mail.setTargetObjectId(vmIdToVisitMap.get(vm.Id).VTD1_Contact__c);
                            mail.setTreatTargetObjectAsRecipient(false);
                            toAddresses.add(vm.VTD1_External_Participant_Email__c);
                            mail.setToAddresses(toAddresses);
                            mail.setUseSignature(false);
                            mail.setBccSender(false);
                            mail.setSaveAsActivity(false);
                            if (!owEmailAddresses.isEmpty()) {
                                mail.setOrgWideEmailAddressId(owEmailAddresses[0].Id);
                            }
                            mails.add(mail);
                            System.debug('mail = ' + mail);
                        }
                        */
                    }
                }
                if (mailParams.size() < 50) {
                    VT_R3_EmailsSender.sendEmails(mailParams);
                } else {
                    System.enqueueJob(new EmailsCreator(mailParams, 0));
                }
                /*
                System.debug('total mails = ' + mails.size());
                List<Messaging.SingleEmailMessage> mailsPack = new List<Messaging.SingleEmailMessage>();
                for (Messaging.SingleEmailMessage m : mails) {
                    mailsPack.add(m);
                    if (mailsPack.size() == 10) {
                        System.debug('sendEmail 1 ' + mailsPack.size());
                        if (!Test.isRunningTest()) {
                            List<Messaging.SendEmailResult> results = Messaging.sendEmail((List<Messaging.Email>) mailsPack, false);
                            for (Messaging.SendEmailResult result : results) {
                                System.debug('result = ' + result.isSuccess());
                            }
                        }
                        mailsPack.clear();
                    }
                }
                if (!mailsPack.isEmpty() && !Test.isRunningTest()) {
                    System.debug('sendEmail 2 ' + mailsPack.size());
                    List<Messaging.SendEmailResult> results = Messaging.sendEmail((List<Messaging.Email>) mailsPack, false);
                    for (Messaging.SendEmailResult result : results) {
                        System.debug('result = ' + result.isSuccess());
                    }
                }
                */
            }
        }
    }
    private static Boolean checkInConditions(VTR2_Reminders__mdt r, VTD1_Actual_Visit__c visit) {
        Boolean result = true;
        for (String fieldName : conditionsMap.keySet()) {
            String leftValue = (String) r.get(fieldName);
            if (String.isEmpty(leftValue)) {
                continue;
            }
            Set <String> valueSet = new Set<String>(leftValue.split(','));
            String rightValue = fieldName == 'Visit_Record_Type__c' ? visit.RecordType.DeveloperName :
                    fieldName == 'Onboarding_Type__c' ? visit.VTD1_Onboarding_Type__c :
                            fieldName == 'Visit_Type__c' ? visit.VTD2_Common_Visit_Type__c :
                                    fieldName == 'Sub_Type__c' ? visit.VTD1_VisitType_SubType__c : '';
            //System.debug('operands ' + leftValue + ' ' + rightValue);
            String condition = (String) r.get(conditionsMap.get(fieldName));
            Boolean nextResult = (condition == 'not equals' || condition == 'not contains') ? false : true;
            System.debug('nextResult ' + nextResult);
            System.debug(leftValue + ' ' + rightValue + ' ' + valueSet.contains(rightValue));
            result = valueSet.contains(rightValue) == nextResult;
            if (!result) {
                break;
            }
        }
        return result;
    }
}