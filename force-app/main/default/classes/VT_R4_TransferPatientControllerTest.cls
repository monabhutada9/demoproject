@IsTest
public without sharing class VT_R4_TransferPatientControllerTest {

    public static void createCandidatePatientTest() {
        VT_R4_TransferPatientController.CandidatePatient patientInfo = new VT_R4_TransferPatientController.CandidatePatient();
        patientInfo.firstName = 'First';
        patientInfo.lastName = 'Last';
        patientInfo.country = 'US';
        patientInfo.preferredLanguage = 'en_US';
        patientInfo.addressLine1 = 'addressLine1';
        patientInfo.addressLine2= 'addressLine2';
        patientInfo.stateProvinceTerritory = 'NY';
        patientInfo.zipPostal = '123456';
        patientInfo.email = VT_D1_TestUtils.generateUniqueUserName();
        patientInfo.phoneNumber = '1234567890';
        patientInfo.phoneNumberType = 'Mobile';
        VT_R4_TransferPatientController.createCandidatePatient(patientInfo);
    }

    public static void testGetAllStudies() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id, Name FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        Case caseObj = [SELECT Id FROM Case WHERE RecordType.Name = 'CarePlan' LIMIT 1];

        Test.startTest();
        VT_R4_TransferPatientController.StudiesInfo studiesInfo = VT_R4_TransferPatientController.getAllStudies(caseObj.Id);
        Test.stopTest();
        System.assertEquals(study.Id, studiesInfo.allStudies[0].Id);
        System.assertEquals(study.Name, studiesInfo.currentStudy);
    }

    public static void testFindAvailablePIs() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id, Name FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        User user = [
                SELECT Id, FirstName, LastName
                FROM User
                WHERE Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME
                ORDER BY CreatedDate DESC
                LIMIT 1
        ];
        Test.startTest();
        VT_R4_TransferPatientController.PrimaryInvestigator primaryInvestigator = VT_R4_TransferPatientController.findAvailablePIs(study.Id)[0];
        Test.stopTest();
        System.assertEquals(user.Id, primaryInvestigator.userId);
        System.assertEquals(user.FirstName + ' ' + user.LastName, primaryInvestigator.name);
    }

    public static void testPrimaryInvestigatorEquality() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id, Name FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        User user = [
                SELECT Id, FirstName, LastName
                FROM User
                WHERE Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME
                ORDER BY CreatedDate DESC
                LIMIT 1
        ];
        Study_Team_Member__c studyTeamMember = [SELECT Id, VTD1_PI_License__c, VTD1_VirtualSite__c FROM Study_Team_Member__c LIMIT 1];
        VT_R4_TransferPatientController.PrimaryInvestigator pi = new VT_R4_TransferPatientController.PrimaryInvestigator(
                user.Id, user.FirstName + ' ' + user.LastName, studyTeamMember.VTD1_PI_License__c, studyTeamMember.VTD1_VirtualSite__c
        );
        Test.startTest();
        VT_R4_TransferPatientController.PrimaryInvestigator queriedPi = VT_R4_TransferPatientController.findAvailablePIs(study.Id)[0];
        Test.stopTest();
        System.assert(queriedPi.equals(pi));
    }

    public static void testFindCriteria() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        VTR2_Pre_Screening_Criteria__c preScreenedCriteria = (VTR2_Pre_Screening_Criteria__c) new DomainObjects.VTR2_Pre_Screening_Criteria_t()
                .setVTR2_Criteria('Test Criteria')
                .setStudy(study)
                .persist();
        Test.startTest();
        VTR2_Pre_Screening_Criteria__c queriedCriteria = VT_R4_TransferPatientController.findCriteria(study.Id)[0];
        Test.stopTest();
        System.assertEquals(preScreenedCriteria.VTR2_Criteria__c, queriedCriteria.VTR2_Criteria__c);
    }

    public static void testGetContact() {
        Case caseObj = [SELECT Id FROM Case WHERE RecordType.Name = 'CarePlan' LIMIT 1];

        Test.startTest();
        Contact queriedContact = VT_R4_TransferPatientController.getContact(caseObj.Id);
        Test.stopTest();
        System.assertNotEquals(null, queriedContact.Id);
        System.assert(!queriedContact.Addresses__r.isEmpty());
    }

    public static void testSaveCandidatePatient() {
        Test.startTest();
        HealthCloudGA__CandidatePatient__c queriedCandidatePatient = [
                SELECT Id, rr_firstName__c, Study__c FROM HealthCloudGA__CandidatePatient__c
        ];
        System.assert(queriedCandidatePatient.rr_firstName__c != null);
        System.assert(queriedCandidatePatient.Study__c != null);
        Test.stopTest();
    }


    public static void testDoPost() {
        HttpCalloutMock cMock = new CalloutMock();
        Test.setMock(HttpCalloutMock.class, cMock);
        HealthCloudGA__CandidatePatient__c queriedCandidatePatient = [
                SELECT Id, rr_firstName__c, Study__c FROM HealthCloudGA__CandidatePatient__c
        ];
    }

    public with sharing class CalloutMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/xml');
            res.setStatusCode(200);
            return res;
        }
    }
}