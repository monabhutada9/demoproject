/**
 * Created by user on 18-May-20.
 * Aleksandr Mazan
 */

public with sharing class VT_R4_DeletedFilesHistoryController {
    public static final String NONE = 'none';
    public static final String DATETIME_FORMAT = 'MM/dd/yyyy hh:mm a';
    public static final String GMT = 'GMT';
    public static final String VTD1_DOCUMENT_HISTORY = 'VTD1_Document__History';
    public static final String VTR4_DELETION_FILE_NAME = 'vtr4_deletionfilename__c';
    public static final String VT_R4_DELETION_FILE_NAME = 'vtr4_deletionfilename__c';
    public static final String VTD1_DOCUMENT_DELETION_REASON = 'vtr4_deletionreason__c';
    public static final String VTR4_DELETION_USER_NAME = 'vtr4_deletionusername__c';
    public static final String VT_R4_DELETION_USER_NAME = 'vtr4_deletionusername__c';
    public static final String VTD1_DOCUMENT_DATE_DELETED = 'vtd1_deletion_date__c';
    public static final String VTD1_DOCUMENT = 'VTD1_Document__c';

    public static Map<String, String> fieldLabels;
    public static Map<String, Schema.SObjectField> docFields;
    public static Set<String> fieldsApi;
    public static String oldStudyName;

    public static String oldStudyId;
    public static String oldSiteName;
    public static String oldSiteId;
    public static String oldDocName;
    public static String oldDocId;

    public static String userTimeZone = String.valueOf(UserInfo.getTimeZone());



    public with sharing class FileInformation {
        @AuraEnabled public String studyName;
        @AuraEnabled public String siteName;
        @AuraEnabled public String docName;
        @AuraEnabled public String editDate;
        @AuraEnabled public String fieldOrEvent;
        @AuraEnabled public String oldValue;
        @AuraEnabled public String newValue;
        @AuraEnabled public String editedBy;
        @AuraEnabled public String error;
        @AuraEnabled public Integer totalPages;
    }


    @AuraEnabled
    public static List<FileInformation> getDocumentHistory(String querySettingsString) {
        oldStudyName = VT_R4_DeletedFilesHistoryController.NONE;
        oldSiteName = VT_R4_DeletedFilesHistoryController.NONE;
        oldDocName = VT_R4_DeletedFilesHistoryController.NONE;
        if (fieldsApi == null) {
            SObjectType docType = Schema.getGlobalDescribe().get(VT_R4_DeletedFilesHistoryController.VTD1_DOCUMENT);
            docFields = docType.getDescribe().fields.getMap();
            fieldsApi = new Set<String>{


                    VT_R4_DeletedFilesHistoryController.VTD1_DOCUMENT_DELETION_REASON, VT_R4_DeletedFilesHistoryController.VTR4_DELETION_FILE_NAME,
                    VT_R4_DeletedFilesHistoryController.VTR4_DELETION_USER_NAME, VT_R4_DeletedFilesHistoryController.VTD1_DOCUMENT_DATE_DELETED


            };
            fieldLabels = new Map<String, String>();
            for (String key : fieldsApi) {
                fieldLabels.put(key, docFields.get(key).getDescribe().getLabel());
            }
        }
        VT_TableHelper.TableQuerySettings querySettings = VT_TableHelper.parseQuerySettings(querySettingsString);
        String queryOrder = ((querySettings.queryOrder != null && querySettings.queryOrder != '') ? ' ORDER BY ' +

                querySettings.queryOrder : ' ORDER BY CreatedDate DESC');

        String queryOffset = (querySettings.queryOffset != null && querySettings.queryOffset != '') ? ' ' + querySettings.queryOffset : '';
        String queryLimit = (querySettings.queryLimit != null && querySettings.queryLimit != '') ? ' ' + querySettings.queryLimit : '';
        List<FileInformation> docsInfo = new List<FileInformation>();
        List<VTD1_Document__History> historyFields = new List<VTD1_Document__History>();
        try {

            String query = 'SELECT ParentId, Parent.VTD1_Document_Name__c, OldValue, NewValue, Field, CreatedById, CreatedBy.Name, CreatedDate, Parent.VTR4_AuditSite__c, ' +
                    'Parent.VTR4_AuditSite__r.Name, Parent.VTR4_AuditStudy__r.Name, Parent.VTR4_AuditStudy__c FROM ' + VT_R4_DeletedFilesHistoryController.VTD1_DOCUMENT_HISTORY +
                    ' WHERE Field IN :fieldsApi ' + queryOrder + ' NULLS LAST, Parent.VTR4_AuditStudy__c, Parent.VTR4_AuditSite__r.Name ASC NULLS LAST,  Parent.VTR4_AuditSite__c ASC, ' +
                    'Parent.VTD1_Document_Name__c ASC, ParentId ASC, CreatedDate DESC' +
                    queryLimit + queryOffset;

            historyFields = Database.query(query);
        } catch (Exception e) {
            System.debug(e.getMessage());
            FileInformation error = new FileInformation();
            error.error = e.getMessage();
            return new List<FileInformation>{
                    error
            };
        }
        for (VTD1_Document__History docHistory : historyFields) {


            try {
                Id sobjOldId = (ID) docHistory.OldValue;
                Id sobjNewid = (ID) docHistory.NewValue;
                if (Test.isRunningTest()) {
                    throw new DmlException('test');
                }
            } catch (Exception e) {

            FileInformation newFileInfo = new FileInformation();
                if (docHistory.Parent.VTR4_AuditStudy__r.Name != null && (oldStudyName != docHistory.Parent.VTR4_AuditStudy__r.Name || oldStudyId != docHistory.Parent.VTR4_AuditStudy__c)) {
                    oldStudyId = docHistory.Parent.VTR4_AuditStudy__c;
                    oldStudyName = docHistory.Parent.VTR4_AuditStudy__r.Name;
                    newFileInfo.studyName = docHistory.Parent.VTR4_AuditStudy__r.Name;
                    oldSiteName = VT_R4_DeletedFilesHistoryController.NONE;
                    oldSiteId = VT_R4_DeletedFilesHistoryController.NONE;
                    oldDocName = VT_R4_DeletedFilesHistoryController.NONE;
                    oldDocId = VT_R4_DeletedFilesHistoryController.NONE;
                }
                if (docHistory.Parent.VTR4_AuditSite__r.Name != null && (oldSiteName != docHistory.Parent.VTR4_AuditSite__r.Name || oldSiteId != docHistory.Parent.VTR4_AuditSite__c)) {
                    oldSiteId = docHistory.Parent.VTR4_AuditSite__c;
                    oldSiteName = docHistory.Parent.VTR4_AuditSite__r.Name;
                    newFileInfo.siteName = docHistory.Parent.VTR4_AuditSite__r.Name;
                    oldDocName = VT_R4_DeletedFilesHistoryController.NONE;
                    oldDocId = VT_R4_DeletedFilesHistoryController.NONE;
                }
                if (oldDocName != docHistory.Parent.VTD1_Document_Name__c || oldDocId != docHistory.ParentId) {
                    oldDocId = docHistory.ParentId;
                oldDocName = docHistory.Parent.VTD1_Document_Name__c;
                newFileInfo.docName = docHistory.Parent.VTD1_Document_Name__c;
            }


            newFileInfo.editDate = docHistory.CreatedDate.format(VT_R4_DeletedFilesHistoryController.DATETIME_FORMAT, userTimeZone);

            newFileInfo.fieldOrEvent = fieldLabels.get(docHistory.Field.toLowerCase());
            if(docHistory.Field.toLowerCase() == VT_R4_DeletedFilesHistoryController.VTD1_DOCUMENT_DATE_DELETED) {
                Datetime oldDataValue = (Datetime)docHistory.OldValue;
                Datetime newDataValue = (Datetime)docHistory.NewValue;
                if(oldDataValue != null) {




                   newFileInfo.oldValue = oldDataValue.format(VT_R4_DeletedFilesHistoryController.DATETIME_FORMAT, userTimeZone);
               }
               if (newDataValue != null) {
                   newFileInfo.newValue = newDataValue.format(VT_R4_DeletedFilesHistoryController.DATETIME_FORMAT, userTimeZone);

                }
            } else {
                newFileInfo.oldValue = (String) docHistory.OldValue;
                newFileInfo.newValue = (String) docHistory.NewValue;
            }
            newFileInfo.editedBy = docHistory.CreatedBy.Name;
            docsInfo.add(newFileInfo);


        }


        }
        if (docsInfo[0] != null) {
            docsInfo[0].totalPages = VT_R4_DeletedFilesHistoryController.getTotalPages(querySettings);
        }
        return docsInfo;
    }


    private static Integer getTotalPages(VT_TableHelper.TableQuerySettings querySettings) {
        String query = 'SELECT Count(Id) FROM ' + VT_R4_DeletedFilesHistoryController.VTD1_DOCUMENT_HISTORY + ' WHERE Field IN: fieldsApi';
        AggregateResult[] ar = Database.query(query);
        Integer totalPages = (Integer) Math.ceil((Decimal) ar[0].get('expr0') / querySettings.entriesOnPage);
        return totalPages;
    }
}