public class VTD1_SC_Task_Extention {
	ApexPages.StandardSetController setCon;
	public VTD1_SC_Task__c sctask { get; set; }
	public VTD1_SC_Task_Extention(ApexPages.StandardSetController controller) {
		list<VTD1_SC_Task__c> sctasks = (list<VTD1_SC_Task__c>) controller.getSelected();
		for (VTD1_SC_Task__c sctsk : sctasks) {
			sctsk.OwnerId = UserInfo.getUserId();
		}
   }
}