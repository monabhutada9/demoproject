@isTest
public with sharing class VT_R3_RestPatientTrainingMaterialsTest {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();

    public static void getTrainingMaterialsTest() {
        String responseString;
        Map<String, Object>responseMap = new Map<String, Object>();
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/Patient/TrainingMaterials/';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = response;

        Knowledge__kav knowledge = new Knowledge__kav(Title = 'knowledge Title',
                UrlName = 'UrlName',
                VTD2_Type__c = 'Training_Materials',
                VTD2_Study__c = null,
                RecordTypeId = '0121N000001YmjXQAS');
        insert knowledge;
        knowledge = [SELECT KnowledgeArticleId FROM Knowledge__kav WHERE Id = :knowledge.Id];
        List<User> usersList = [SELECT Id, ContactId FROM User WHERE UserPermissionsKnowledgeUser = true AND IsActive = true AND Profile.Name = 'System Administrator'];

        if (!usersList.isEmpty()) {
            User user = usersList.get(0);
            System.runAs(user) {
                KbManagement.PublishingService.publishArticle(knowledge.KnowledgeArticleId, true);
            }

            VT_R3_RestPatientTrainingMaterials.getTrainingMaterials();

            RestContext.request.requestURI = '/Patient/TrainingMaterials/' + knowledge.Id;
            responseString = VT_R3_RestPatientTrainingMaterials.getTrainingMaterials();
            responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
            System.debug('here map ' + responseMap);
//		List<Object> knowledgeList = (List<Object>)responseMap.get('serviceResponse');
//		System.assertEquals('knowledge Title', ((Map<String,Object>)knowledgeList[0]).get('title'));

            RestContext.request.requestURI = '/Patient/TrainingMaterials/0051N000005cleYQAQ';
            responseString = VT_R3_RestPatientTrainingMaterials.getTrainingMaterials();
            responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
            System.assertEquals(helper.forAnswerForIncorrectInput(), (String) responseMap.get('serviceResponse'));
        }
    }

    public static void getTrainingMaterialsTest2() {
        Knowledge__kav knowledge = new Knowledge__kav(Title = 'knowledge Title',
                UrlName = 'UrlName',
                VTD2_Type__c = 'Training_Materials',
                VTD2_Study__c = null,
                RecordTypeId = '0121N000001YmjXQAS');
        insert knowledge;
        knowledge = [SELECT KnowledgeArticleId FROM Knowledge__kav WHERE Id = :knowledge.Id];
        List<User> usersList = [SELECT Id, ContactId FROM User WHERE UserPermissionsKnowledgeUser = true AND IsActive = true AND Profile.Name = 'System Administrator'];

        if (!usersList.isEmpty()) {
            User userToPub = usersList.get(0);
            System.runAs(userToPub) {
                KbManagement.PublishingService.publishArticle(knowledge.KnowledgeArticleId, true);
            }
        }
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor')
                );
        new DomainObjects.VTD2_Study_Geography_t(study).persist();

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t();
        DomainObjects.User_t userPatient = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        User user = (User) userPatient.persist();
        System.runAs(user) {
            String responseString;
            RestRequest request = new RestRequest();
            RestResponse response = new RestResponse();
            request.requestURI = '/Patient/TrainingMaterials/';
            request.httpMethod = 'GET';
            RestContext.request = request;
            RestContext.response = response;
            responseString = VT_R3_RestPatientTrainingMaterials.getTrainingMaterials();

            RestContext.request.requestURI = '/Patient/TrainingMaterials/' + knowledge.Id;
            responseString = VT_R3_RestPatientTrainingMaterials.getTrainingMaterials();
        }
    }
}