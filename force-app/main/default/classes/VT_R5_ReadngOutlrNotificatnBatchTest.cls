/************************************************************************
 * Name  : VT_R5_ReadngOutlrNotificatnBatchTest
 * Desc  : Test class for Batch which is scheduled for fetching the (point in time) PatientDevices whose readings(public$readings) lies in 
 * outliers specified at study level studyMeasurementConfigureations.
 * And call the NOtification & Task handlers.
 *
 *
 * Modification Log:
 * ----------------------------------------------------------------------
 * Developer                Date                description
 * ----------------------------------------------------------------------
 * Aarohi S		            11/06/2020           Original 
 * 
 *************************************************************************/
@isTest(seeAllData = false)
public class VT_R5_ReadngOutlrNotificatnBatchTest {

    @testSetup
    static void setupTestData(){
		try{
            Test.startTest();
             VT_D1_TestUtils.createConnectedDevicesConfigNotification();
            List<case> listOfCases = new List<case>();
            HealthCloudGA__CarePlanTemplate__c study=VT_D1_TestUtils.prepareStudy(1);            
            System.debug('*** Line 19'+study.id);
            List<User> userIds = [SELECT Id FROM User WHERE Profile.Name = 'Patient' LIMIT 2];
            List<Contact> lstContacts = new List<Contact>();
            for(Integer i = 0;i<2;i++){
                lstContacts.add(new Contact(
                    FirstName = 'Test' + i,
                    LastName = 'Patient',
                    RecordTypeId = VT_D1_ConstantsHelper.RECORD_TYPE_ID_CONTACT_PATIENT,
                    VTD1_UserId__c = userIds.get(i).Id
                ));
            }
            List<User> lstPIUser = [SELECT Id FROM User WHERE Profile.Name = 'Primary Investigator' LIMIT 1];
            List<User> lstSCRUser = [SELECT Id FROM User WHERE Profile.Name = 'Site Coordinator' LIMIT 1];
            List<User> lstPGUser = [SELECT Id FROM User WHERE Profile.Name = 'Patient Guide' LIMIT 1];
            lstContacts.add(new Contact(
                    FirstName = 'Test',
                    LastName = 'PI',
                    RecordTypeId = VT_D1_ConstantsHelper.RECORD_TYPE_ID_CONTACT_PI,
                    VTD1_UserId__c = lstPIUser.get(0).Id
                ));
            insert lstContacts;

            Case caseRecordSCR = new Case(ContactId = lstContacts.get(0).Id, VTD1_Study__c = study.Id,VTD1_PI_contact__c=lstContacts.get(2).Id,
                                        VTR2_SiteCoordinator__c = lstSCRUser.get(0).Id);
            Case caseRecordPG = new Case (ContactId = lstContacts.get(1).Id, VTD1_Study__c = study.Id, VTD1_PI_contact__c=lstContacts.get(2).Id,
                                        VTD1_Primary_PG__c = lstPGUser.get(0).Id);
            listOfCases.add(caseRecordSCR);
            listOfCases.add(caseRecordPG);
            insert listOfCases;

            List<Account> lstAct = new List<Account>();
            for(Contact con : lstContacts){
                lstAct.add(new Account(Name = 'Test Patient ' + con.FirstName, VTD2_Patient_s_Contact__c = con.Id));
            }
            insert lstAct;

            lstContacts.get(0).VTD1_Clinical_Study_Membership__c = caseRecordSCR.Id;
            lstContacts.get(0).AccountId = lstAct.get(0).Id;
            lstContacts.get(1).VTD1_Clinical_Study_Membership__c = caseRecordPG.Id;
            lstContacts.get(1).AccountId = lstAct.get(1).Id;
            update lstContacts;

            List<VTD1_Protocol_Kit__c> lstPRotocolKit =[Select Id  from VTD1_Protocol_Kit__c LIMIT 1];
            HealthCloudGA__EhrDevice__c ehrDevice = VT_D1_TestUtils.createEherDeviceConfig(lstPRotocolKit.get(0).id);
            VT_D1_TestUtils.studyMeasurmentConfig(study.id);
            VT_D1_TestUtils.createpatientDeviceConfig(ehrDevice.id,caseRecordSCR.id,'SVTAppleWatch123');
       		VT_D1_TestUtils.createpatientDeviceConfig(ehrDevice.id,caseRecordPG.id,'SVTAppleWatch124');
            System.debug('Contacts : ' + lstContacts);            
        }catch(Exception e){
            system.debug('****Line 24'+e.getStackTraceString());
        }
        Test.stopTest();
    }
     /* 
     * method : testBatchDevices()
     * param : none
     * description : This method is used to test batchexecution of VT_R5_ReadingOutlierNotificationBatch.
     * return : void
     */
    


 
    /* 
     * method : testBatchDevicesTask()
     * param : none
     * description : This method is used to test task notifications for patient devices.
     * return : void
     */
    
    @isTest
    static void testBatchDevicesTask(){
        Test.startTest();
        User sysAdmin = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];
        Boolean isSuccess = false;
        System.runAs(sysAdmin){
            try{
                Task t = new Task(
                    VTR5_Email_Subject__c ='ABC');
                insert t;
               // VT_R5_ReadingOutlierNotificationBatch.sendPatientDeviceTaskAlert(new List<Task>{t});
                isSuccess = true;
            }catch(Exception e){
                isSuccess = false;
            }
        }
        Test.stopTest();
        //System.assertEquals(isSuccess, true);
    }
    
     /* 
     * method : testBatchDevicesScheduleBatch()
     * param : none
     * description : This method is used to test schedullar job.
     *               It schedules the job at interval of every 15 min.
     * return : void
     */
    @isTest

    static void testBatchClass(){
        Test.startTest();
        User sysAdmin = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];
        System.runAs(sysAdmin){
            VT_R5_ReadingOutlierNotificationBatch batch = new VT_R5_ReadingOutlierNotificationBatch();
            Database.executeBatch(batch);

        }    
        Test.stopTest();
        System.assert([SELECT Id FROM AsyncApexJob WHERE ApexClass.Name = 'VT_R5_ReadingOutlierNotificationBatch' ] != null);
    }
    
    @isTest
    static void testBatchDevicesCreateHtml(){
        Boolean isSuccess = false;
        Test.startTest();
        User sysAdmin = [SELECT Id, ContactId FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];
        System.runAs(sysAdmin){
            try{
                Task t = new Task(
                    VTR5_Email_Subject__c ='ABC');
                insert t;
                system.debug('*********Line 87'+t.id);
                //VT_R5_ReadingOutlierNotificationBatch.CreateHTMLBody(T, [Select id, LanguageLocaleKey, lastname, Profile.Name from User where 
                                                                    //Profile.Name = 'Patient Guide'][0]);                                              
                isSuccess = true;
            }catch(Exception e){
                isSuccess = false;
            }
        }
        Test.stopTest();
    }
    
	public static List<VTR5_publicreadings__x> createReadingForTest(){
		return new List<VTR5_publicreadings__x>{
                    new VTR5_publicreadings__x(
                        VTR5_date__c= system.now(),
                        VTR5_measurement_id__c='7777777-1568183726010',
                        VTR5_serial_number__c='SVTAppleWatch123',
                        VTR5_device_key_id__c='SVTAppleWatch123',
                        VTR5_type__c='pressure_sys',
                        VTR5_value__c=370.0
                    ),new VTR5_publicreadings__x(
                        VTR5_date__c= system.now(),
                        VTR5_measurement_id__c='7777777-1568183726010',
                        VTR5_serial_number__c='SVTAppleWatch124',
                        VTR5_device_key_id__c='SVTAppleWatch124',
                        VTR5_type__c='pressure_sys',
                        VTR5_value__c=370.0
                    ),
                    new VTR5_publicreadings__x(
                        VTR5_date__c= system.now(),
                        VTR5_measurement_id__c='7777777-1568183726010',
                        VTR5_serial_number__c='SVTAppleWatch124',
                        VTR5_type__c='pressure_sys',
                        VTR5_value__c=370.0
                    )
                };
	} 
}