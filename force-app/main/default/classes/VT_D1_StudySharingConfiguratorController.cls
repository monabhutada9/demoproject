public with sharing class VT_D1_StudySharingConfiguratorController {

    public class ShareConfigItem implements Comparable {
        public VTD1_Study_Sharing_Configuration__c config;
        public String userName;
        public String patientName;
        public String profileName;
        public Boolean hasOpenTasks;

        public Integer compareTo(Object compareTo) {
            ShareConfigItem compareToItem = (ShareConfigItem)compareTo;
            if (this.userName != compareToItem.userName) {
                return this.userName < compareToItem.userName ? -1 : 1;
            }
            return 0;
        }
    }

    public class WrapperRecords {
        //copado
        @AuraEnabled public List<ShareConfigItem> shareConfigItems = new List<ShareConfigItem>();
        @AuraEnabled public Integer totalRecords;
    }

    @AuraEnabled(cacheable=true)
    public static String getConfigs(Id studyId, String tabId, String jsonParams) {
        return new VT_D1_StudySharingQueryHelper().getConfigs(studyId, tabId, jsonParams);
    }

    @AuraEnabled
    public static void saveConfigs(String configsJSON) {
        List<VTD1_Study_Sharing_Configuration__c> configs =
            (List<VTD1_Study_Sharing_Configuration__c>)JSON.deserialize(configsJSON, List<VTD1_Study_Sharing_Configuration__c>.class);
        for (VTD1_Study_Sharing_Configuration__c item : configs) {
            item.VTD1_Locked__c = true;
        }
        upsert configs;
    }

    @AuraEnabled
    public static String getSessionId() {
        return UserInfo.getSessionId();
    }

    @AuraEnabled
    public static Boolean getStudyIsClosed(Id studyId) {
        HealthCloudGA__CarePlanTemplate__c study = [
            SELECT VTD1_StudyStatus__c
            FROM HealthCloudGA__CarePlanTemplate__c
            WHERE Id = :studyId
        ];
        return study.VTD1_StudyStatus__c != null && study.VTD1_StudyStatus__c.equalsIgnoreCase('Completed');
    }
}