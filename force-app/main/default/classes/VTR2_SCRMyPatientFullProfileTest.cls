/**
 * Created by User on 19/04/25.
 */
@IsTest
public with sharing class VTR2_SCRMyPatientFullProfileTest {

    public static void getFullProfileTest() {
        Case ptCase = [SELECT Id, ContactId FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        Contact ptContact = [SELECT Id, VTD1_Clinical_Study_Membership__c FROM Contact WHERE Id =: ptCase.ContactId];
        Test.startTest();
        try {
            String res = VTR2_SCRMyPatientFullProfile.getFullProfile(ptContact.VTD1_Clinical_Study_Membership__c);
            System.assertNotEquals(null, res);
            VTR2_SCRMyPatientFullProfile.PatientFullProfile patientFullProfile = (VTR2_SCRMyPatientFullProfile.PatientFullProfile) JSON.deserialize(res, VTR2_SCRMyPatientFullProfile.PatientFullProfile.class);
            System.assert(patientFullProfile.userTimeZoneSelectRatioElementList.size() > 0);
            System.assert(patientFullProfile.contactGenderOptionsForRadioGroup.size() > 0);
            System.assert(patientFullProfile.contactEmergencyPhoneTypeSelectRatioElementList.size() > 0);
            System.assert(patientFullProfile.contactPreferredContactMethodOptionsForRadioGroup.size() > 0);
            System.assert(patientFullProfile.addressStateSelectRatioElementList.size() > 0);
            System.assert(patientFullProfile.addressCountrySelectRatioElementList.size() > 0);
            System.assert(patientFullProfile.addressAddressTypeSelectRatioElementList.size() > 0);
            System.assert(patientFullProfile.newPhoneTypeInputSelectRatioElementList.size() > 0);
            System.assert(patientFullProfile.newPhysicianStateInputSelectRatioElementList.size() > 0);
            System.assert(patientFullProfile.newPhysicianCountryInputSelectRatioElementList.size() > 0);
        } catch (Exception ex) {
            System.assertNotEquals(null, ex.getMessage());
        }
        Test.stopTest();
    }

    public static void updateProfileSuccessTest() {
        Case ptCase = [SELECT Id, ContactId FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        Contact ptContact = [SELECT Id, VTD1_Clinical_Study_Membership__c FROM Contact WHERE Id =: ptCase.ContactId];
        Test.startTest();
        String res = VTR2_SCRMyPatientFullProfile.getFullProfile(ptContact.VTD1_Clinical_Study_Membership__c);

        VTR2_SCRMyPatientFullProfile.PatientFullProfile patientFullProfile =  (VTR2_SCRMyPatientFullProfile.PatientFullProfile) JSON.deserialize(res, VTR2_SCRMyPatientFullProfile.PatientFullProfile.class);

        Address__c address = new Address__c();
        address.City__c = 'City';
        address.State__c  = 'TN';
        address.VTD1_Contact__c = ptContact.Id;
        address.VTD1_Primary__c  = false;
        patientFullProfile.newAddressList = new List<Address__c>{address};


        List<VTR2_SCRMyPatientFullProfile.NewPhone> newContactPhoneList = new List<VTR2_SCRMyPatientFullProfile.NewPhone>();
        VTR2_SCRMyPatientFullProfile.NewPhone newContactPhone = new VTR2_SCRMyPatientFullProfile.NewPhone();
        newContactPhone.phoneNumber = '55555';
        newContactPhone.phoneType = 'Home';
        newContactPhone.primary = false;
        newContactPhoneList.add(newContactPhone);
        patientFullProfile.newContactPhoneList = newContactPhoneList;


        Boolean hasException = false;
        try {
            VTR2_SCRMyPatientFullProfile.updateProfile(JSON.serialize(patientFullProfile));
        }catch (Exception ex){
            hasException = true;
        }

//        System.assertEquals(false,hasException);
        Test.stopTest();
    }

    public static void saveCaregiverInDBTest() {
        Case ptCase = [SELECT Id, ContactId FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        Contact ptContact = [SELECT Id,VTD1_Clinical_Study_Membership__c FROM Contact WHERE Id =: ptCase.ContactId];
        Test.startTest();
        String res = VTR2_SCRMyPatientFullProfile.getFullProfile(ptContact.VTD1_Clinical_Study_Membership__c);
        VTR2_SCRMyPatientFullProfile.PatientFullProfile patientFullProfile = (VTR2_SCRMyPatientFullProfile.PatientFullProfile) JSON.deserialize(res, VTR2_SCRMyPatientFullProfile.PatientFullProfile.class);


        patientFullProfile.caregiverList[0].lastName = 'Test';
        patientFullProfile.caregiverList[0].firstName = 'Test';
        patientFullProfile.caregiverList[0].email = 'test@test.com';
        patientFullProfile.caregiverList[0].relationship = 'Child';
        patientFullProfile.caregiverList[0].phone = '65465464';

        try {
            VTR2_SCRMyPatientFullProfile.saveCaregiverInDB(JSON.serialize(patientFullProfile.caregiverList[0]));
        }catch (Exception ex){
            System.assertNotEquals(null,ex.getMessage());
        }

        Test.stopTest();
    }

    public static void addNewPhysicianTest() {
        Case ptCase = [SELECT Id, ContactId FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        Contact ptContact = [SELECT Id FROM Contact WHERE Id =: ptCase.ContactId];
        Test.startTest();

        VTR2_SCRMyPatientFullProfile.NewPhysician newPhysician = new VTR2_SCRMyPatientFullProfile.NewPhysician();
        newPhysician.lastName = 'Test3';
        newPhysician.firstName = 'Test';
        newPhysician.office = 'office';
        newPhysician.email = 'test3@test.com';
        newPhysician.phone = '434543';

        try {
            VTR2_SCRMyPatientFullProfile.addNewPhysician(JSON.serialize(newPhysician), ptContact.VTD1_Clinical_Study_Membership__c);
        } catch (Exception ex) {
            System.assertNotEquals(null, ex.getMessage());
        }
        Test.stopTest();
    }

    public static void checkSuccessEditableFields() {
        Case ptCase = [SELECT Id, ContactId FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        Contact ptContact = [SELECT Id FROM Contact WHERE Id =: ptCase.ContactId];
        Test.startTest();

        try{
            VTR2_SCRMyPatientFullProfile.getIsEditableFields(ptContact.Id);
        }catch (Exception ex){
            System.assertNotEquals(null,ex.getMessage());
        }

        Test.stopTest();
    }


}