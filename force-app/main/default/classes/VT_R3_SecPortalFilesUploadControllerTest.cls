/**
 * Created by Rishat Shamratov on 05.11.2019.
 */

@IsTest
public class VT_R3_SecPortalFilesUploadControllerTest {

    public class DocWrapper {
        Id fileId;
        String fileName;
        String category;
        String nickname;
        String comments;
        Id recId;
    }

    public class RecordObjWrapper {
        Id userId;
        String patient;
        String phone;
        String visit;
        String study;
        Boolean linkIsValid;
    }

    public with sharing class MyCalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setBody('test');
            res.setStatus('OK');
            res.setStatusCode(201);
            return res;
        }
    }

    @TestSetup
    private static void setupMethod() {
        VT_D1_TestUtils.prepareStudy(1);
        VT_D1_TestUtils.createTestPatientCandidate(1);
    }

    @IsTest
    static String getVisitObjectTest() {
        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Range__c = 4;
        protocolVisit.VTD1_VisitOffset__c = 2;
        protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient; Site Coordinator';
        insert protocolVisit;

        Case cas = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];

        VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
        actualVisit.VTD1_Protocol_Visit__c = protocolVisit.Id;
        actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
        actualVisit.VTD1_Case__c = cas.Id;
        insert actualVisit;

        String token = generateLinkTest(actualVisit.Id);
        String visitObj = VT_R3_SecurePortalFilesUploadController.getVisitObject(actualVisit.Id, token);
        RecordObjWrapper visitObjDeserialize = (RecordObjWrapper) JSON.deserialize(visitObj, RecordObjWrapper.class);

        return visitObj;
    }

    @IsTest
    static String getCaseObjectTest() {
        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Range__c = 4;
        protocolVisit.VTD1_VisitOffset__c = 2;
        protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient; Site Coordinator';
        insert protocolVisit;

        Case c = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];

        VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
        actualVisit.VTD1_Protocol_Visit__c = protocolVisit.Id;
        actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
        actualVisit.VTD1_Case__c = c.Id;
        insert actualVisit;

        Case cas = [
                SELECT Id,
                        VTD1_Patient_User__r.Name,
                        VTD1_Study__r.VTD1_Protocol_Nickname__c,
                        VTD1_Virtual_Site__r.VTR3_Study_Geography__c,
                        VTR3_UploadLinkTokens__c
                FROM Case
        ];
        String token = generateLinkTest(cas.Id);
        String caseObj = VT_R3_SecurePortalFilesUploadController.getCaseObject(cas.Id, token);
        RecordObjWrapper caseObjDeserialize = (RecordObjWrapper) JSON.deserialize(caseObj, RecordObjWrapper.class);

        return caseObj;
    }

    @IsTest
    static void uploadDocsVisitTest() {
        VTD1_ProtocolVisit__c protocolVisit = new VTD1_ProtocolVisit__c();
        protocolVisit.VTD1_Range__c = 4;
        protocolVisit.VTD1_VisitOffset__c = 2;
        protocolVisit.VTD1_VisitOffsetUnit__c = 'days';
        protocolVisit.VTD1_VisitDuration__c = '30 minutes';
        protocolVisit.VTD1_Onboarding_Type__c = 'N/A';
        protocolVisit.VTR2_Visit_Participants__c = 'PI; Patient Guide; Patient; Site Coordinator';
        insert protocolVisit;

        Case cas = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];

        VTD1_Actual_Visit__c actualVisit = new VTD1_Actual_Visit__c();
        actualVisit.VTD1_Protocol_Visit__c = protocolVisit.Id;
        actualVisit.VTD1_Status__c = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
        actualVisit.VTD1_Case__c = cas.Id;
        insert actualVisit;

        ContentVersion contentDoc = new ContentVersion();
        contentDoc.Title = 'TestTitle';
        contentDoc.PathOnClient = '/TestDoc.txt';
        contentDoc.VersionData = Blob.valueOf('/TestDoc.txt');
        insert contentDoc;

        ContentVersion cv = [
                SELECT Id,
                        ContentDocumentId
                FROM ContentVersion
        ];

        DocWrapper dw = new DocWrapper();
        dw.fileId = cv.ContentDocumentId;
        dw.fileName = 'TestDoc.txt';
        dw.category = 'Other';
        dw.nickname = 'TestDoc';
        dw.comments = 'Doc for Test';
        //dw.recId = actualVisit.Id;

        List<DocWrapper> dwList = new List<DocWrapper>();
        dwList.add(dw);

        String token = generateLinkTest(actualVisit.Id);

        VT_R3_SecurePortalFilesUploadController.CalloutWrapper cw = new VT_R3_SecurePortalFilesUploadController.CalloutWrapper();
        cw.token = token;
        cw.recId = actualVisit.Id;
        cw.dwListJSON = JSON.serialize(dwList);

        Test.startTest();
        HttpCalloutMock cOutMock = new MyCalloutMock();
        Test.setMock(HttpCalloutMock.class, cOutMock);
        VT_R3_SecurePortalFilesUploadController.uploadDocs(JSON.serialize(dwList), actualVisit.Id, token);
        VT_R3_SecurePortalFilesUploadController.insertDocs(cw);
        Test.stopTest();
    }

    @IsTest
    static void uploadDocsCaseTest() {
        Case cas = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];

        ContentVersion contentDoc = new ContentVersion();
        contentDoc.Title = 'TestTitle';
        contentDoc.PathOnClient = '/TestDoc.txt';
        contentDoc.VersionData = Blob.valueOf('/TestDoc.txt');
        insert contentDoc;

        ContentVersion cv = [
                SELECT Id,
                        ContentDocumentId
                FROM ContentVersion
        ];

        DocWrapper dw = new DocWrapper();
        dw.fileId = cv.ContentDocumentId;
        dw.fileName = 'TestDoc.txt';
        dw.category = 'Other';
        dw.nickname = 'TestDoc';
        dw.comments = 'Doc for Test';
        //dw.recId = cas.Id;

        List<DocWrapper> dwList = new List<DocWrapper>();
        dwList.add(dw);

        String token = generateLinkTest(cas.Id);

        VT_R3_SecurePortalFilesUploadController.CalloutWrapper cw = new VT_R3_SecurePortalFilesUploadController.CalloutWrapper();
        cw.token = token;
        cw.recId = cas.Id;
        cw.dwListJSON = JSON.serialize(dwList);

        Test.startTest();
        HttpCalloutMock cOutMock = new MyCalloutMock();
        Test.setMock(HttpCalloutMock.class, cOutMock);
        VT_R3_SecurePortalFilesUploadController.uploadDocs(JSON.serialize(dwList), cas.Id, token);
        VT_R3_SecurePortalFilesUploadController.insertDocs(cw);
        Test.stopTest();
    }

    static String generateLinkTest(Id recId) {
        List<String> link = VT_R3_SecurePortalFilesUploadController.generateLink(recId, 8).split('&token=');
/*
        VTD1_RTId__c cs = new VTD1_RTId__c();
        cs.VTD2_Patient_Community_URL__c = 'https://someOrg.force.com/patient/s';
        insert cs;
        System.debug('CS: ' + VTD1_RTId__c.getInstance().VTD2_Patient_Community_URL__c);
*/
        return link[1];
    }
}