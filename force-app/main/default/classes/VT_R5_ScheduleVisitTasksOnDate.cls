/**
 * Created by Alexey Mezentsev on 8/19/2020.
 * SH-14596 PB Name: "Schedule Visit Tasks on Date"
 * Test class: VT_R5_ActualVisitProcessesHandlerTest.scheduleVisitTasksOnDateBatchTest()
*/

public without sharing class VT_R5_ScheduleVisitTasksOnDate implements Database.Batchable<SObject>, Database.Stateful {
    private List<VTR4_Conversion_Log__c> batchErrors = new List<VTR4_Conversion_Log__c>();

    public Iterable<SObject> start(Database.BatchableContext context) {
        List<VTD1_Actual_Visit__c> aVisits = [
                SELECT VTD1_Status__c,
                        VTD1_Visit_Type__c,
                        VTD2_Common_Visit_Type__c,
                        VTD1_Schedule_Visit_Task_Date__c,
                        VTR2_ScheduleVisitTaskPatient__c
                FROM VTD1_Actual_Visit__c
                WHERE VTD1_Onboarding_Type__c = 'N/A'
                AND VTD1_Scheduled_Date_Time__c = NULL
                AND VTR3_LTFUStudyVisit__c = TRUE
                AND VTD1_Status__c IN ('Future Visit', 'To Be Scheduled')
                AND (VTD1_Schedule_Visit_Task_Date__c = TODAY OR VTR2_ScheduleVisitTaskPatient__c = TODAY)
        ];
        return aVisits;
    }

    public void execute(Database.BatchableContext context, List<VTD1_Actual_Visit__c> aVisits) {
        try {
            List<VT_D2_TNCatalogTasks.ParamsHolder> userTasks = new List<VT_D2_TNCatalogTasks.ParamsHolder>();
            List<VT_D2_TNCatalogNotifications.ParamsHolder> notifications = new List<VT_D2_TNCatalogNotifications.ParamsHolder>();
            List<VTD1_Actual_Visit__c> aVisitsToUpdate = new List<VTD1_Actual_Visit__c>();
            for (VTD1_Actual_Visit__c aVisit : aVisits) {
                if (VT_R5_ActualVisitProcessesHandler.setToBeScheduledStatusAndGenerateTasks(aVisit, userTasks, notifications)) {
                    aVisitsToUpdate.add(new VTD1_Actual_Visit__c(Id = aVisit.Id, VTD1_Status__c = aVisit.VTD1_Status__c));
                }
            }
            if (!aVisitsToUpdate.isEmpty()) {
                Database.SaveResult[] srList = Database.update(aVisits, false);
                for (Integer i = 0; i < aVisitsToUpdate.size(); i++) {
                    Database.SaveResult sr = srList[i];
                    if (!sr.isSuccess()) {
                        batchErrors.add(new VTR4_Conversion_Log__c(
                                VTR5_RecordId__c = aVisitsToUpdate[i].Id,
                                VTR5_ClassName__c = VT_R5_ScheduleVisitTasksOnDate.class.getName(),
                                VTR4_Error_Message__c = sr.getErrors()[0].getStatusCode() + ' ' + sr.getErrors()[0].getMessage()
                        ));
                    }
                }
            }
            if (!userTasks.isEmpty()) {
                VT_D2_TNCatalogTasks.generateTask(userTasks);
            }
            if (!notifications.isEmpty()) {
                VT_D2_TNCatalogNotifications.generateNotification(notifications);
            }
        } catch (Exception e) {
            batchErrors.add(new VTR4_Conversion_Log__c(
                    VTR5_ClassName__c = VT_R5_ScheduleVisitTasksOnDate.class.getName(),
                    VTR4_Error_Message__c = e.getMessage() + ' ' + e.getStackTraceString()
            ));
        }
    }

    public void finish(Database.BatchableContext context) {
        if (!batchErrors.isEmpty()) {
            insert batchErrors;
        }
    }
}