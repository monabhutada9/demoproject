/**
 * @author: Alexander Komarov
 * @date: 09.07.2020
 * @description: Test class for VT_Stubber
 */

@IsTest
private class VT_StubberTest {

    @IsTest
    static void testResultStub() {
        List<User> users = getUsers('User_Query');
        System.assert(users.size() == 10);

        VT_Stubber.applyStub(
                'User_Query',
                new List<User>{
                        new User(
                                FirstName = 'TestUserName'
                        ),
                        new User(
                                FirstName = 'TestUserName2'
                        )
                });

        users = getUsers('User_Query');
        System.assert(users.size() == 2);
        System.assertEquals(users[0].FirstName, 'TestUserName');
        System.assertEquals(users[1].FirstName, 'TestUserName2');

        VT_Stubber.applyStub(
                'Second_Users_Query',
                new List<User>{
                        new User(
                                FirstName = 'TestUserName3'
                        )
                });

        users = getUsers('Second_Users_Query');
        System.assert(users.size() == 1);
        System.assertEquals(users[0].FirstName, 'TestUserName3');

        VT_Stubber.removeStub('User_Query');

        users = getUsers('User_Query');
        System.assert(users.size() == 10);

        VT_Stubber.clearAllStub();

        users = getUsers('Second_Users_Query');
        System.assert(users.size() == 10);

    }

    private static List<User> getUsers(String queryName) {
        return (List<User>) new VT_Stubber.ResultStub(
                queryName,
        [SELECT Id FROM User LIMIT 10]
        ).getResult();
    }

    @IsTest
    static void testDMLStub() {
        List<Database.SaveResult> dbsr = insertContacts('Insert');
        System.assert(dbsr[0].isSuccess());
        System.assertNotEquals(dbsr[0].getId(), null);
        System.assertEquals(dbsr[0].getErrors().size(), 0);

        VT_Stubber.applyStub('Insert', (List<Database.SaveResult>) JSON.deserialize(
                '[{"success":true,"id":"0031000000abcde"}]',
                List<Database.SaveResult>.class));

        dbsr = insertContacts('Insert');
        System.assert(dbsr[0].isSuccess());
        System.assertEquals(dbsr[0].getId(), '0031000000abcde');
        System.assertEquals(dbsr[0].getErrors().size(), 0);

        Contact contactToUpdate = [SELECT Id, FirstName FROM Contact LIMIT 1];
        contactToUpdate.FirstName += ' jr.';

        dbsr = updateContacts('Update', contactToUpdate);
        System.assert(dbsr[0].isSuccess());
        System.assertNotEquals(dbsr[0].getId(), null);
        System.assertEquals(dbsr[0].getErrors().size(), 0);

        VT_Stubber.applyStub('Update', (List<Database.SaveResult>) JSON.deserialize(
                '[{"success":true,"id":"0031000000bcdef"}]',
                List<Database.SaveResult>.class));
        dbsr = updateContacts('Update', contactToUpdate);
        System.assert(dbsr[0].isSuccess());
        System.assertEquals(dbsr[0].getId(), '0031000000bcdef');
        System.assertEquals(dbsr[0].getErrors().size(), 0);

        Contact contactToUpsert = [SELECT Id, FirstName FROM Contact LIMIT 1];
        contactToUpsert.FirstName = 'mr. ' + contactToUpsert.FirstName;
        List<Database.UpsertResult> dbur = upsertContacts('Upsert', contactToUpsert);
        System.assert(dbur[0].isSuccess());
        System.assertEquals(dbur[0].isCreated(), false);
        System.assertNotEquals(dbur[0].getId(), null);
        System.assertEquals(dbur[0].getErrors().size(), 0);

        VT_Stubber.applyStub('Upsert', (List<Database.UpsertResult>) JSON.deserialize(
                '[{"success":true,"id":"0031000000cdefg","created":true}]',
                List<Database.UpsertResult>.class));
        dbur = upsertContacts('Upsert', contactToUpsert);
        System.assert(dbur[0].isSuccess());
        System.assertEquals(dbur[0].isCreated(), true);
        System.assertEquals(dbur[0].getId(), '0031000000cdefg');
        System.assertEquals(dbur[0].getErrors().size(), 0);
    }

    @IsTest
    static void testDMLStubDeletion() {
        Contact contactToWork = new Contact(FirstName = 'Fn TEMP', LastName = 'Ln TEMP');
        insert contactToWork;
        Id contactId = contactToWork.Id;

        List<Database.DeleteResult> dbdr = deleteContact('Delete', contactToWork);
        System.assert(dbdr[0].isSuccess());
        System.assertEquals(dbdr[0].getId(), contactId);
        System.assertEquals(dbdr[0].getErrors().size(), 0);

        VT_Stubber.applyStub('Delete', (List<Database.DeleteResult>) JSON.deserialize(
                '[{"success":false,"id":"0031000000defgh","errors":[{"message":"EXCEPTION","statusCode":"FIELD_CUSTOM_VALIDATION_EXCEPTION"}]}]',
                List<Database.DeleteResult>.class));

        dbdr = deleteContact('Delete', contactToWork);
        System.assert(!dbdr[0].isSuccess());
        System.assertEquals(dbdr[0].getId(), '0031000000defgh');
        System.assertEquals(dbdr[0].getErrors()[0].message, 'EXCEPTION');
        System.assertEquals(dbdr[0].getErrors()[0].statusCode, System.StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION);

        Contact toUndelete = [SELECT Id FROM Contact WHERE Id = :contactId ALL ROWS];
        List<Database.UndeleteResult> dbur = undeleteContact('Undelete', toUndelete);
        System.assert(dbur[0].isSuccess());
        System.assertEquals(dbur[0].getId(), contactId);

        VT_Stubber.applyStub('Undelete', (List<Database.UndeleteResult>) JSON.deserialize(
                '[{"success":true,"id":"0031000000efghi"}]',
                List<Database.UndeleteResult>.class));

        dbur = undeleteContact('Undelete', toUndelete);
        System.assert(dbur[0].isSuccess());
        System.assertEquals(dbur[0].getId(), '0031000000efghi');
    }

    private static List<Database.UndeleteResult> undeleteContact(String queryName, Contact contactToUndelete) {
        return (List<Database.UndeleteResult>) new VT_Stubber.DMLStub(
                queryName,
                contactToUndelete,
                DMLOperation.UND).perform().getResult();
    }
    private static List<Database.DeleteResult> deleteContact(String queryName, Contact contactToDelete) {
        return (List<Database.DeleteResult>) new VT_Stubber.DMLStub(
                queryName,
                contactToDelete,
                DMLOperation.DEL).perform().getResult();
    }

    private static List<Database.UpsertResult> upsertContacts(String queryName, Contact contactToUpsert) {
        return (List<Database.UpsertResult>) new VT_Stubber.DMLStub(
                queryName,
                contactToUpsert,
                DMLOperation.UPS).perform().getResult();
    }

    private static List<Database.SaveResult> updateContacts(String queryName, Contact contactToUpdate) {
        return (List<Database.SaveResult>) new VT_Stubber.DMLStub(
                queryName,
                contactToUpdate,
                DMLOperation.UPD).perform().getResult();
    }

    private static List<Database.SaveResult> insertContacts(String queryName) {
        return (List<Database.SaveResult>) new VT_Stubber.DMLStub(
                queryName,
                new List<Contact>{
                        new Contact(FirstName = 'TestFN', LastName = 'TestLN')
                },
                DMLOperation.INS,
                true).perform().getResult();
    }
}