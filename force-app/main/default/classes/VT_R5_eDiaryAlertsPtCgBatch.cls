/**
 * Created by dmitry on 30.10.2020.
 */

public with sharing class VT_R5_eDiaryAlertsPtCgBatch extends VT_R5_AbstractEDiaryAlertsBatch {

    private static final String NOTIFICATION_TITLE_FIELD = 'VTR5_Notification_Title__c';
    private static final String NOTIFICATION_MESSAGE_FIELD = 'VTR5_NotificationMessage__c';
    private static final String LINK_TO_RELATED_EVENT_OR_OBJECT = 'my-diary?banner=#message';
    private static final String EMAIL_TEMPLATE_NAME = 'VT_R5_eCOA_Notification';
    private static final Set<String> ALLOWED_CASE_STATUSES = getAllowedCaseStatuses();

    private static final Set<String> DIARY_STATUSES = new Set<String> {
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_DUE_SOON,
            VT_R4_ConstantsHelper_Protocol_ePRO.SURVEY_READY_TO_START
    };

    private Map<Id, User> pushNotificationsUserMap;
    private List<VTD1_NotificationC__c> notificationsForInsert;

    public VT_R5_eDiaryAlertsPtCgBatch(Map<Id, VT_R5_eDiaryAlertsService.AlertRequestInfo> alertsMap,String Recipient_Type) {   // 5.7 Epic SH-20625
        super(alertsMap,Recipient_Type); //5.7 EPic:SH-20625
        this.batchScopeSize = 500;
        this.scopeItemIdFieldName = 'VTD1_CSM__c';
        this.notificationsForInsert = new List<VTD1_NotificationC__c>();
        this.pushNotificationsUserMap = new Map<Id, User>();
    }

    protected override Iterable<SObject> getScope() {
        return Database.getQueryLocator([
                SELECT VTD1_Study__c, VTD1_Study__r.VTR5_eDiaryTool__c, VTD1_Patient_User__r.TimeZoneSidKey,
                    (SELECT VTD1_UserId__r.Profile.Name,
                            VTD1_UserId__r.TimeZoneSidKey, VTD1_Primary_CG__c
                    FROM Contacts__r
                    WHERE VTD1_UserId__r.IsActive = TRUE
                        AND VTD1_UserId__r.Profile.Name = :VT_R4_ConstantsHelper_Profiles.CAREGIVER_PROFILE_NAME
                        AND VTD1_Primary_CG__c = TRUE
                    LIMIT 1)
                FROM Case
                WHERE VTD1_Patient_User__r.IsActive = TRUE
                    AND VTD1_Study__c IN :this.studyIdToBuildersMap.keySet()
                    AND Status IN :ALLOWED_CASE_STATUSES
        ]);
    }

    protected override List<AggregateResult> getAggregatedSurveys(Set<Id> caseIds) {

        return [
                SELECT COUNT(VTD1_Status__c), VTD1_CSM__c, VTR5_eCoaAlertsKey__c, RecordTypeId
                FROM VTD1_Survey__c
                WHERE VTD1_CSM__c IN :caseIds
                    AND VTR5_DiaryStudyKey__c IN :this.studyEDiaryKeys
                    AND VTR5_eCoaAlertsKey__c != NULL
                    AND VTD1_Status__c IN :DIARY_STATUSES
                    AND VTD1_Due_Date__c != NULL
                GROUP BY VTR5_eCoaAlertsKey__c, VTD1_CSM__c, RecordTypeId
        ];
    }

    protected override Set<Id> getScopeIds(List<SObject> scope) {
        Set<Id> scopeIds = new Map<Id, SObject>(scope).keySet();
        this.pushNotificationsUserMap.putAll(getPushNotificationsUserMap(scopeIds));

        return scopeIds;
    }

    protected override void prepareDailyAlerts(SObject scopeItem, VT_R5_eDiaryAlertsService.AlertRequestInfo alertRequestInfo, VTR5_eDiaryNotificationBuilder__c builder) {
        Case cas = (Case) scopeItem;
        Set<String> diaryStudyKeys = this.scopeItemIdToDiaryStudyKeyMap.get(cas.Id);

        if (alertRequestInfo.diaryBased && (diaryStudyKeys == null || !diaryStudyKeys.contains(builder.VTR5_DiaryStudyKey__c))) { return; }

        if (builder.VTR5_NotificationRecipient__c.contains(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME)
                && isTimezoneMatched(alertRequestInfo.timezoneOffset, cas.VTD1_Patient_User__r)) {
            addNotificationForInsert(cas.Id, cas.VTD1_Patient_User__r, builder.Id);

        }
        if (!cas.Contacts__r.isEmpty() && builder.VTR5_NotificationRecipient__c.contains(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME)
                && isTimezoneMatched(alertRequestInfo.timezoneOffset, cas.Contacts__r[0].VTD1_UserId__r)) {
            addNotificationForInsert(cas.Id, cas.Contacts__r[0].VTD1_UserId__r, builder.Id);

        }
    }

    protected override void prepareExpirationAlerts(SObject scopeItem, VT_R5_eDiaryAlertsService.AlertRequestInfo alertRequestInfo, VTR5_eDiaryNotificationBuilder__c builder) {
        Case cas = (Case) scopeItem;
        if (cas.VTD1_Study__r.VTR5_eDiaryTool__c == 'eCOA') { return; }

        Set<String> diaryAlertKeys = this.scopeItemIdToAlertsKeyMap.get(cas.Id);
        String alertKey = getAlertKey(alertRequestInfo, builder);

        if (diaryAlertKeys == null || !diaryAlertKeys.contains(alertKey)) { return; }

        if (builder.VTR5_NotificationRecipient__c.contains(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME)) {
            addNotificationForInsert(cas.Id, cas.VTD1_Patient_User__r, builder.Id);
        }
        if (!cas.Contacts__r.isEmpty() && builder.VTR5_NotificationRecipient__c.contains(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME)) {
            addNotificationForInsert(cas.Id, cas.Contacts__r[0].VTD1_UserId__r, builder.Id);
        }
    }

    protected override VT_R5_eDiaryAlertsBulkInsertRestService.RequestPayload getAlerts() {
        if (this.notificationsForInsert.isEmpty()) { return null; }

        Set<Id> usersForPush = new Set<Id>();
        Set<Id> usersForEmail = new Set<Id>();

        VT_D1_TranslateHelper.translate(this.notificationsForInsert, true);

        for (VTD1_NotificationC__c n : this.notificationsForInsert) {
            usersForEmail.add(n.OwnerId);

            User u = this.pushNotificationsUserMap.get(n.OwnerId);
            if (u != null && !this.pushNotificationsUserMap.get(n.OwnerId).MobilePushRegistrations__r.isEmpty()) {
                usersForPush.add(n.OwnerId);
            }

            n.VTR5_OriginalTitle__c = n.Title__c;
            n.VTR5_OriginalMessage__c = n.Message__c;
            n.Message__c = '#1';
            n.Title__c = '#1';
        }

        return new VT_R5_eDiaryAlertsBulkInsertRestService.RequestPayload(
                EMAIL_TEMPLATE_NAME,
                usersForEmail,
                this.notificationsForInsert,
                usersForPush
        );
    }

    private static Set<String> getAllowedCaseStatuses() {
        Set<String> caseStatusesForExclude = new Set<String>{
                'Dropped',
                'Dropped Treatment,Compl F/Up',
                'Completed',
                'Compl Treatment,Dropped F/Up',
                'Closed'
        };
        Set<String> allowedCaseStatuses = new Set<String>();
        for(Schema.PicklistEntry entry : Case.Status.getDescribe().getPicklistValues()) {
            if (!caseStatusesForExclude.contains(entry.value)) {
                allowedCaseStatuses.add(entry.value);
            }
        }
        return allowedCaseStatuses;
    }

    private Map<Id, User> getPushNotificationsUserMap(Set<Id> caseIds) {

        return new Map<Id, User>([
                SELECT Id, (SELECT VTR5_ConnectionToken__c, VTR5_ServiceType__c, VTR5_User__c FROM MobilePushRegistrations__r ORDER BY CreatedDate DESC LIMIT 5)
                FROM User
                WHERE Contact.VTD1_Clinical_Study_Membership__c IN :caseIds
        ]);
    }

    private void addNotificationForInsert(Id caseId, User u, Id builderId) {
        this.notificationsForInsert.add(new VTD1_NotificationC__c(
                VTD1_Receivers__c = u.Id,
                OwnerId = u.Id,
                VTR4_CurrentPatientCase__c = caseId,
                Message__c = '#1',
                Title__c = '#1',
                VTD2_Title_Parameters__c = builderId + NOTIFICATION_TITLE_FIELD,
                VTD1_Parameters__c = builderId + NOTIFICATION_MESSAGE_FIELD,
                VTR3_Notification_Type__c = NOTIFICATION_TYPE,
                VTR5_eDiaryNotificationBuilder__c = builderId,
                Link_to_related_event_or_object__c = LINK_TO_RELATED_EVENT_OR_OBJECT,
                HasDirectLink__c = true
        ));
    }

}