public class VT_D1_PatientContactDeliveryController {
	@AuraEnabled 
  	public static User getPatientContactUser() {
  		User u = [SELECT Id, ContactId FROM User WHERE Id =: userInfo.getUserId()];
  		return u;
  	}
}