/**
* @author: Carl Judge
* @date: 19-Nov-19
* @description: Performs a DML operation on the records passed in using a separate context via platform event
**/

public without sharing class VT_R3_AsyncDML extends VT_R3_AbstractPublishedAction {
    private List<SObject> recs;
    private String operation;
    private Boolean allOrNothing;

    public VT_R3_AsyncDML(List<SObject> recs, String operation) {
        this.recs = recs;
        this.operation = operation;
        this.allOrNothing = true;
    }

    public VT_R3_AsyncDML(List<SObject> recs, String operation, Boolean allOrNothing) {
        this.recs = recs;
        this.operation = operation;
        this.allOrNothing = allOrNothing;
    }

    public override Type getType() {
        return VT_R3_AsyncDML.class;
    }

    public override void execute() {
        switch on operation.toUpperCase() {
            when 'INSERT' {
                Database.insert(recs, allOrNothing);
            }
            when 'UPDATE' {
                Database.update(recs, allOrNothing);
            }
            when 'UPSERT' {
                Database.upsert(recs, allOrNothing);
            }
            when 'DELETE' {
                Database.delete(recs, allOrNothing);
            }
            when 'UNDELETE' {
                Database.undelete(recs, allOrNothing);
            }
        }
    }

    public static void doBatchedDML(List<SObject> recs, String operation, Integer batchSize) {
        doBatchedDML(recs, operation, true, batchSize);
    }
    public static void doBatchedDML(List<SObject> recs, String operation, Boolean allOrNothing, Integer batchSize) {
        List<VTR3_PublishedAction__e> actions = new List<VTR3_PublishedAction__e>();
        List<SObject> currentBatch = new List<SObject>();
        while (!recs.isEmpty()) {
            currentBatch.add(recs.remove(0));
            if (currentBatch.size() == batchSize) {
                actions.add(getBatchedAction(currentBatch, operation, allOrNothing));
                currentBatch = new List<SObject>();
            }
        }
        if (! currentBatch.isEmpty()) {
            actions.add(getBatchedAction(currentBatch, operation, allOrNothing));
        }
        if (! actions.isEmpty()) {
            EventBus.publish(actions);
        }
    }
    private static VTR3_PublishedAction__e getBatchedAction(List<SObject> placeHolders, String operation, Boolean allOrNothing) {
        VT_R3_AsyncDML asyncDmlClass = new VT_R3_AsyncDML(placeHolders, operation, allOrNothing);
        return new VTR3_PublishedAction__e(
            VTR3_ActionJSON__c = JSON.serialize(asyncDmlClass),
            VTR3_ActionType__c = asyncDmlClass.getType().getName()
        );
    }
}