@isTest
public with sharing class VT_R3_RestPatientCalendarTest {

    public static void calendarTest() {
        HealthCloudGA__CarePlanTemplate__c study = [SELECT id FROM HealthCloudGA__CarePlanTemplate__c LIMIT 1];
        Case carePlan = [SELECT Id, ContactId, VTD1_PI_user__c FROM Case WHERE Subject = 'VT_R5_allTest'];
        VTD1_ProtocolVisit__c pVisit = new VTD1_ProtocolVisit__c(
                VTD1_Visit_Checklist__c = 'TEST',
                VTD1_EDC_Name__c = 'PVName',
                VTD1_Study__c = study.Id,
                VTR2_Visit_Participants__c = VTD1_ProtocolVisit__c.VTR2_Visit_Participants__c.getDescribe().getPicklistValues()[0].getValue()
        );
        insert pVisit;
        VTD1_Actual_Visit__c actVisit = new VTD1_Actual_Visit__c(
                VTD1_Case__c = carePlan.Id,
                VTD1_Protocol_Visit__c = pVisit.Id,
                VTD1_Reason_for_Request__c = 'TestVideoPanel',
                VTD1_Additional_Visit_Checklist__c = 'TEST',
                VTD1_Scheduled_Date_Time__c = Datetime.now() + 1,
                Name = 'SomeTestVisitName',
                VTD1_Status_To_Be_Scheduled_Set_Date__c = Date.today() + 1,
                VTR2_Televisit__c = false
        );
        insert actVisit;
        insert new Visit_Member__c(VTD1_Actual_Visit__c = actVisit.Id, VTD1_Participant_User__c = carePlan.VTD1_PI_user__c);

        String responseString;
        Map<String, Object> responseMap = new Map<String, Object>();
        Contact patietnCont = [SELECT Id, VTD1_Clinical_Study_Membership__c FROM Contact WHERE Id =: carePlan.ContactId];
        patietnCont.VTD1_Clinical_Study_Membership__c = carePlan.Id;
        update patietnCont;
        User patientUser = [SELECT Id FROM User WHERE ContactId =: carePlan.ContactId];

        System.runAs(patientUser) {
            RestRequest request = new RestRequest();
            RestResponse response = new RestResponse();
            request.httpMethod = 'GET';
            request.requestURI = '/Patient/Calendar/*';
            RestContext.request = request;
            RestContext.response = response;

            VTD1_Actual_Visit__c visit = [
                    SELECT Id,
                            VTD1_Formula_Name__c,
                            VTD1_Scheduled_Date_Time__c,
                            VTD1_Scheduled_Visit_End_Date_Time__c,
                            Unscheduled_Visits__c,
                            VTD1_Status__c,
                            Type__c
                    FROM VTD1_Actual_Visit__c
                    LIMIT 1
            ];
            System.debug('VISIT **** ' + visit);
            Test.startTest();
            responseString = VT_R3_RestPatientCalendar.getSpecificVisit();

            request.params.put('visitId', '0052a000001bDcvAAE');
            responseString = VT_R3_RestPatientCalendar.getSpecificVisit();

            request.params.put('visitId', 'a1P2a0000002CrbEAE');
            responseString = VT_R3_RestPatientCalendar.getSpecificVisit();
            responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
            System.assertEquals('FAILURE. Server error! Can not find a visit with this ID: a1P2a0000002CrbEAE.', responseMap.get('serviceResponse'));

            HttpCalloutMock cOutMock = new MyCalloutMock();
            Test.setMock(HttpCalloutMock.class, cOutMock);
            request.params.put('visitId', visit.Id);
            responseString = VT_R3_RestPatientCalendar.getSpecificVisit();
            Test.stopTest();
        }
    }

    public static void getCalendarEventsTest() {
        String responseString;
        Map<String, Object> responseMap = new Map<String, Object>();
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.httpMethod = 'GET';
        request.requestURI = '/Patient/Calendar/*';
        RestContext.request = request;
        RestContext.response = response;

        Datetime startDateTime = DateTime.now().addHours(1) ;
        Datetime endDateTime = startDateTime.addDays(1).addHours(5);
        Datetime wrongStDatetime = DateTime.now() + 3;
        Case carePlan = [SELECT Id, ContactId, VTD1_PI_user__c FROM Case WHERE Subject = 'VT_R5_allTest'];
        Contact patietnCont = [SELECT Id, VTD1_Clinical_Study_Membership__c FROM Contact WHERE Id =: carePlan.ContactId];
        patietnCont.VTD1_Clinical_Study_Membership__c = carePlan.Id;
        update patietnCont;
        User patientUser = [SELECT Id FROM User WHERE ContactId =: carePlan.ContactId];
        Test.startTest();
        System.runAs(patientUser) {
            responseString = VT_R3_RestPatientCalendar.getCalendarEvents(wrongStDatetime.getTime(), endDateTime.getTime(), '');
            responseString = VT_R3_RestPatientCalendar.getCalendarEvents(startDateTime.getTime(), endDateTime.getTime(), '');
        }
        Test.stopTest();
    }


    public static void getCalendarEventsTest2() {
        String responseString;
        Map<String, Object> responseMap = new Map<String, Object>();
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.httpMethod = 'GET';
        request.requestURI = '/Patient/Calendar/*';
        RestContext.request = request;
        RestContext.response = response;

        Datetime startDateTime = DateTime.now().addHours(1) ;
        Datetime endDateTime = startDateTime.addDays(1).addHours(5);
        Datetime wrongStDatetime = DateTime.now() + 3;

        DomainObjects.Contact_t patientCaregiver = new DomainObjects.Contact_t();
        DomainObjects.User_t userCaregiver = new DomainObjects.User_t()
                .addContact(patientCaregiver)
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME);
        userCaregiver.persist();

        Case carePlan = [SELECT Id, ContactId, VTD1_PI_user__c FROM Case WHERE Subject = 'VT_R5_allTest'];
        Contact cContact = (Contact) patientCaregiver.record;
        cContact.VTD1_Clinical_Study_Membership__c = carePlan.Id;
        update cContact;
        User caregiver = (User) userCaregiver.record;

          Test.startTest();
        System.runAs(caregiver) {
            responseString = VT_R3_RestPatientCalendar.getCalendarEvents(startDateTime.getTime(), endDateTime.getTime(), '');
        }
        Test.stopTest();
    }

    public static void getCalendarEventsTest3() {
        String responseString;
        Map<String, Object> responseMap = new Map<String, Object>();
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.httpMethod = 'GET';
        request.requestURI = '/Patient/Calendar/*';
        RestContext.request = request;
        RestContext.response = response;

        Datetime startDateTime = DateTime.now().addHours(1) ;
        Datetime endDateTime = startDateTime.addDays(1).addHours(5);
        Datetime wrongStDatetime = DateTime.now() + 3;

        Test.startTest();
        Case carePlan = [SELECT Id, ContactId, VTD1_PI_user__c FROM Case WHERE Subject = 'VT_R5_allTest'];
        Contact patietnCont = [SELECT Id, VTD1_Clinical_Study_Membership__c FROM Contact WHERE Id =: carePlan.ContactId];
        patietnCont.VTD1_Clinical_Study_Membership__c = carePlan.Id;
        update patietnCont;
        User patientUser = [SELECT Id FROM User WHERE ContactId =: carePlan.ContactId];

        System.runAs(patientUser) {
            responseString = VT_R3_RestPatientCalendar.getCalendarEvents(startDateTime.getTime(), endDateTime.getTime(), '');
        }
    }


    public static void rescheduleRequestTest() {
        String responseString;
        Map<String, Object> responseMap = new Map<String, Object>();
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.httpMethod = 'GET';
        request.requestURI = '/Patient/Calendar/Reschedule/*';
        RestContext.request = request;
        RestContext.response = response;

        Datetime startDateTime = DateTime.now().addHours(1) ;
        Datetime endDateTime = startDateTime.addDays(1).addHours(5);
        Datetime wrongStDatetime = DateTime.now() + 3;
        Test.startTest();
        VTD1_Actual_Visit__c visit = [
                SELECT Id,
                        VTD1_Formula_Name__c,
                        VTD1_Scheduled_Date_Time__c,
                        VTD1_Scheduled_Visit_End_Date_Time__c,
                        VTD1_Status_To_Be_Scheduled_Set_Date__c,
                        Unscheduled_Visits__c,
                        VTD1_Status__c,
                        Type__c,
                        VTR2_Modality__c,
                        VTR2_Loc_Name__c
                FROM VTD1_Actual_Visit__c
                LIMIT 1
        ];
        visit.VTD1_Scheduled_Date_Time__c = null;
        update visit;

        System.debug('here11' + visit);

        String bigString = 'qqqqqqqqqq';
        while (bigString.length() < 500) {
            bigString = bigString + bigString;
        }

        responseString = VT_R3_RestPatientCalendarReschedule.rescheduleRequest(UserInfo.getUserId(), startDateTime.getTime(), endDateTime.getTime(), 'reason');

        responseString = VT_R3_RestPatientCalendarReschedule.rescheduleRequest(visit.Id, startDateTime.getTime(), endDateTime.getTime(), 'reason');
        responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
        System.debug('here1 =' + responseString);
        System.assertEquals(System.Label.VTR2_SchedulingVisitSuccess, responseMap.get('serviceMessage'));

        responseString = VT_R3_RestPatientCalendarReschedule.rescheduleRequest(visit.Id, startDateTime.getTime(), endDateTime.getTime(), bigString);
        responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
        System.assertEquals(System.Label.VT_D1_ErrorRequestReschedule, responseMap.get('serviceMessage'));

        Test.stopTest();
    }



    public static void cancelCalendarTest() {
        String responseString;
        Map<String, Object> responseMap = new Map<String, Object>();
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.httpMethod = 'GET';
        request.requestURI = '/Patient/Calendar/Cancel/*';
        RestContext.request = request;
        RestContext.response = response;

        VTD1_Actual_Visit__c visit = [
                SELECT Id,
                        VTD1_Formula_Name__c,
                        VTD1_Scheduled_Date_Time__c,
                        VTD1_Scheduled_Visit_End_Date_Time__c,
                        Unscheduled_Visits__c,
                        VTD1_Status__c,
                        Type__c
                FROM VTD1_Actual_Visit__c
                LIMIT 1
        ];

        responseString = VT_R3_RestPatientCalendarCancel.cancelRequest(UserInfo.getUserId(), 'reason');
        responseString = VT_R3_RestPatientCalendarCancel.cancelRequest(visit.Id, 'reason');

        String bigString = 'qqqqqqqqqq';
        while (bigString.length() < 500) {
            bigString = bigString + bigString;
        }
        responseString = VT_R3_RestPatientCalendarCancel.cancelRequest(visit.Id, bigString);
        responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
        System.assertEquals(System.Label.VT_D1_ErrorCancellingVisit, responseMap.get('serviceMessage'));

    }

    public with sharing class MyCalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setBody('test');
            res.setStatus('OK');
            res.setStatusCode(201);
            return res;
        }
    }
}