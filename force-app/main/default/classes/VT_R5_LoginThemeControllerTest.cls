/**
 * Created by user on 7/29/2020.
 */

@IsTest
public with sharing class VT_R5_LoginThemeControllerTest {

    public static void getPickListValuesForIMBTest(){
        List<String> langList = VT_R5_LoginThemeController.getPickListValuesForIMB();
        System.assert(langList.size() > 0);
        if (langList.size() > 0) {
            System.assertNotEquals(null, langList.get(1));
            System.assert(langList.contains('en_US'));
        }
    }

    public static void changeLangTest(){
        System.assertEquals('en_US', VT_R5_LoginThemeController.changeLang('en'));
        System.assertEquals('en_US', VT_R5_LoginThemeController.changeLang('en_US'));
    }
}