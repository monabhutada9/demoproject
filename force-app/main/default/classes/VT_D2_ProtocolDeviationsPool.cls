public with sharing class VT_D2_ProtocolDeviationsPool {
	@AuraEnabled
	public static List<VTD1_Protocol_Deviation__c> getProtocolDeviations() {
		// get user groups
		Id userId = UserInfo.getUserId();
		List<GroupMember> groups = [SELECT GroupId FROM GroupMember WHERE UserOrGroupId = :userId AND Group.Type != 'Queue'];
		List<Id> groupIds = new List<Id>();
		for (GroupMember gm : groups) {
			groupIds.add(gm.GroupId);
		}

		system.debug(groupIds);

		// get queues for user or his groups
		List<Id> queueIds = new List<Id>();
		List <GroupMember> queues = [
				SELECT GroupId
				FROM GroupMember
				WHERE (UserOrGroupId=:userId OR UserOrGroupId IN :groupIds) AND Group.Type = 'Queue'
		];
		for (GroupMember q : queues) {
			queueIds.add(q.GroupId);
		}

		// get protocol deviations for queues
		List<VTD1_Protocol_Deviation__c> deviations = [
				SELECT
				Id, VTD1_Subject_ID__c, VTD1_StudyId__c, VTD2_Date_Documented_Date__c, VTD1_Deviation_Severity__c,
					VTD1_Deviation_Description__c, VTD1_StudyId__r.Name, toLabel(VTD1_Site_Subject_Level__c), VTD1_PD_Status__c,
					VTD1_Subject_ID__r.VTD1_Subject_ID__c
				FROM VTD1_Protocol_Deviation__c
				WHERE OwnerId IN :queueIds
				ORDER BY CreatedDate DESC
		];
		return deviations;
	}

	@AuraEnabled
	public static void claimProtocolDeviations(List<Id> deviationIds) {
		List<VTD1_Protocol_Deviation__c> deviationsToUpdate = [
				SELECT Id
				FROM VTD1_Protocol_Deviation__c
				WHERE Id IN :deviationIds
				FOR UPDATE
		];
		for (VTD1_Protocol_Deviation__c d : deviationsToUpdate) {
			d.OwnerId = UserInfo.getUserId();
			d.AssignedCMId__c = UserInfo.getUserId();
			d.VTD2_Assigned_to_CM__c = true;
		}

		try {
			update deviationsToUpdate;
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
		}
	}

	@AuraEnabled(cacheable=true)
	public static List <String> getDeviationCategories() {
		List <String> allOpts = new list <String> ();
		Schema.sObjectType objType = VTD1_Protocol_Deviation__c.getSObjectType();

		// Describe the SObject using its object type.
		Schema.DescribeSObjectResult objDescribe = objType.getDescribe();

		// Get a map of fields for the SObject
		map <String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();

		// Get the list of picklist values for this field.
		list <Schema.PicklistEntry> values =
				fieldMap.get('VTD1_Deviation_Severity__c').getDescribe().getPickListValues();

		// Add these values to the selectoption list.
		for (Schema.PicklistEntry a: values) {
			allOpts.add(a.getValue());
		}
		// allOpts.sort();
		return allOpts;
	}
}