@isTest
public with sharing class VT_R3_RestVisitTimeslotsTest {

	public static void getTimeSlotsTest() {
		Case cs = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'CarePlan'];
		VTD1_Actual_Visit__c visit = new VTD1_Actual_Visit__c(VTR5_PatientCaseId__c=cs.Id);
		insert visit;
		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();
		request.requestURI = '/Visit/Timeslots';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response  = response;
		Test.startTest();
		String responseString = VT_R3_RestVisitTimeslots.getRescheduleTimeSlots();
		System.assert(!String.isEmpty(responseString));

		RestContext.request.params.put('visitId', visit.Id);
		responseString = VT_R3_RestVisitTimeslots.getRescheduleTimeSlots();
		System.assert(!String.isEmpty(responseString));
		Test.stopTest();
	}
}