/**
* @author: Carl Judge
* @date: 30-Apr-20
* @description: Class to send API requests to eCOA
**/
public class VT_R5_eCoaApiRequest {

    public static final String ORG_GUID = eCOA_IntegrationDetails__c.getInstance().eCOA_OrgGuid__c;
    public static Boolean usePointToPoint = eCOA_IntegrationDetails__c.getInstance().UsePointToPoint__c;


    private String studyGuid = '';
    private String subjectGuid = '';
    private String endpoint;
    private String method;
    private String action;
    private Boolean skipLogging = false;
    private VT_D1_RequestBuilder requestBuilder;


    private Map<String, String> headerMap = new Map<String, String>{
        'content-type' => 'application/json'
    };




    public String getStudyGuid() {
        return this.studyGuid;
    }
    public String getSubjectGuid() {
        return this.subjectGuid;
    }
    public String getEndpoint() {
        return this.endpoint;
    }
    public String getMethod() {
        return this.method;
    }
    public String getAction() {
        return this.action;
    }
    public Boolean getSkipLogging() {
        return this.skipLogging;
    }
    public VT_D1_RequestBuilder getBody() {
        return this.requestBuilder;
    }
    public Map<String, String> getHeaders() {
        return this.headerMap;
    }



    public VT_R5_eCoaApiRequest setStudyGuid(String studyGuid) {
        this.studyGuid = studyGuid;
        return this;
    }
    public VT_R5_eCoaApiRequest setSubjectGuid(String subjectGuid) {
        this.subjectGuid = subjectGuid;
        return this;
    }
    public VT_R5_eCoaApiRequest setEndpoint(String endpoint) {
        this.endpoint = endpoint;
        return this;
    }
    public VT_R5_eCoaApiRequest setMethod(String method) {
        this.method = method;
        return this;
    }
    public VT_R5_eCoaApiRequest setAction(String action) {
        this.action = action;
        return this;
    }
    public VT_R5_eCoaApiRequest setBody(VT_D1_RequestBuilder requestBuilder) {
        this.requestBuilder = requestBuilder;
        return this;
    }
    public VT_R5_eCoaApiRequest setSkipLogging(Boolean skipLogging) {
        this.skipLogging = skipLogging;
        return this;
    }

    public VT_R5_eCoaApiRequest setHeaders(Map<String, String> headerMap) {
        this.headerMap = headerMap;
        return this;
    }
    public VT_D1_HTTPConnectionHandler.Result send() {

        return usePointToPoint
            ? VT_R5_eCoaPointToPointHelper.sendRequest(this)
            : VT_D1_HTTPConnectionHandler.send(method, getMergedEndpoint(), requestBuilder, action, headerMap, skipLogging);

    }
    public String getMergedEndpoint() {
        String endpoint = this.endpoint;
        if (ORG_GUID != null) { endpoint = endpoint.replace(':orgGuid', ORG_GUID); }
        if (studyGuid != null) { endpoint = endpoint.replace(':studyGuid', studyGuid); }
        if (subjectGuid != null) { endpoint = endpoint.replace(':subjectGuid', subjectGuid); }
        return endpoint;
    }


}