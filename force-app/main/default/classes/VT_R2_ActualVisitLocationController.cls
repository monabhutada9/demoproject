public with sharing class VT_R2_ActualVisitLocationController {
    public class VisitLocation{
        public String Id;
        public String Name;
        public Boolean Primary;
        public String LocationName;
        public String AddressLine;
        public String City;
        public String ZIP;
        public String LocationType;
        public String Phone;
    }

    @AuraEnabled(Cacheable=true)
    public static String getSiteAddressName(Id siteAddressId) {
        try {
            return [SELECT Name FROM VTR2_Site_Address__c WHERE Id=:siteAddressId].Name;
        } catch (Exception e){
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled(Cacheable=true)
    public static String getVisitLocations(Id caseId) {
//        Id siteId = [SELECT Id, VTD1_Virtual_Site__c FROM Case WHERE Id =: caseId].VTD1_Virtual_Site__c;
        List<VTR2_Site_Address__c> siteAddressesList = [SELECT Id,
                                                               Name,
                                                               VTR2_Primary__c,
                                                               VTR2_LocationName__c,
                                                               VTR2_AddressLine1__c,
                                                               VTR2_City__c,
                                                               VTR2_ZIP__c,
                                                               VTR2_LocationType__c,
                                                               VTR2_PhoneNumber__c
                                                        FROM VTR2_Site_Address__c
                                                        WHERE VTR2_VirtualSiteLookup__c	IN (SELECT VTD1_Virtual_Site__c FROM Case WHERE Id = :caseId)];
        List<VisitLocation> visitLocationList = new List<VisitLocation>();
        for (VTR2_Site_Address__c siteAddress : siteAddressesList) {
            VisitLocation visitLocation = new VisitLocation();
            visitLocation.Id = siteAddress.Id;
            visitLocation.Name = siteAddress.Name;
            visitLocation.Primary = siteAddress.VTR2_Primary__c;
            visitLocation.LocationName = siteAddress.VTR2_LocationName__c;
            visitLocation.AddressLine = siteAddress.VTR2_AddressLine1__c;
            visitLocation.City = siteAddress.VTR2_City__c;
            visitLocation.ZIP = siteAddress.VTR2_ZIP__c;
            visitLocation.LocationType = siteAddress.VTR2_LocationType__c;
            visitLocation.Phone = siteAddress.VTR2_PhoneNumber__c;
            visitLocationList.add(visitLocation);
        }
        System.debug(visitLocationList);
        return JSON.serialize(visitLocationList);
    }
}