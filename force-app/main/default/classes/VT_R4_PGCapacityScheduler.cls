/**
 * Created by user on 6/29/2020.
 */

public with sharing class VT_R4_PGCapacityScheduler implements Schedulable {
    private Set<Id> pgUserIds = new Set<Id>();

    public VT_R4_PGCapacityScheduler(Set<Id> pgUserIds) {
        this.pgUserIds = pgUserIds;
    }

    public void execute(SchedulableContext param1) {
        List<VTD1_Patient_Guide_Capacity__c> pgCapacityToInsert = new List<VTD1_Patient_Guide_Capacity__c>();
        for (Id userId : pgUserIds) {
            pgCapacityToInsert.add(new VTD1_Patient_Guide_Capacity__c(User__c = userId, VTD1_Overall_Capacity__c = 15));
        }
        insert pgCapacityToInsert;
        System.abortJob(param1.getTriggerId());
    }
}