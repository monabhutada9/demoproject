/**
 * @author Ruslan Mullayanov
 * @created 11.06.2020
 * @see VT_D1_UserProfileMenuController
 */
@IsTest
public with sharing class VT_D1_UserProfileMenuControllerTest {
	
 	public static void userProfileMenuTest(){
		User usr = (User) new DomainObjects.User_t().toObject();

		Test.startTest();
		System.runAs(usr) {
			User user = VT_D1_UserProfileMenuController.getUser();
			System.assertNotEquals(null, user);
			System.assertEquals(false, String.isEmpty(user.FullPhotoUrl));

			VT_D1_UserProfileMenuController.getBaseUrl();
		}
		Test.stopTest();
	}
}