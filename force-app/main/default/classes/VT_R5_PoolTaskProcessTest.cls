@IsTest 
public with sharing class VT_R5_PoolTaskProcessTest {

    @TestSetup
    static void setup() {
        Test.startTest();
        //VT_D1_TestUtils.prepareStudy(1);
        DomainObjects.Study_t study = new DomainObjects.Study_t();

        DomainObjects.VirtualSite_t virtualSite = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .setStudySiteNumber('12345');
        virtualSite.persist();



		 DomainObjects.User_t craUser = new DomainObjects.User_t()
        .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.CRA_PROFILE_NAME)
        .setEmail('test.cra@test.cra');
        craUser.persist();
 
        DomainObjects.StudyTeamMember_t craSTM = new DomainObjects.StudyTeamMember_t()
        .addStudy(study)
        .addUser(craUser);
        craSTM.persist(); 
        

        DomainObjects.StudyTeamMember_t stm = new DomainObjects.StudyTeamMember_t()
            .setRecordTypeByName('SCR')
            .addUser(new DomainObjects.User_t()
            .addContact(new DomainObjects.Contact_t())
            .setProfile('Site Coordinator'))
           // .setOnsiteCraId(craSTM.id)
           // .setRemoteCraId(craSTM.id)
            .addStudy(study);
 
		


        DomainObjects.User_t mrrUser = new DomainObjects.User_t().
                setProfile(VT_R4_ConstantsHelper_ProfilesSTM.MRR_PROFILE_NAME);
        mrrUser.persist();
        VTD1_RTId__c rtId = new VTD1_RTId__c(VTD1_Task_SimpleTask__c = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('SimpleTask').getRecordTypeId());
        insert rtId;

		/*@Vijendra Hire
         * @Story : SH-17437 
         * @Description : Removed onsite CRA field from SOQL
         */
        List <Virtual_Site__c> vsites = new List<Virtual_Site__c>();
        vsites=[Select Id, VTD1_Study_Team_Member__c from Virtual_Site__c limit 1];
        vsites[0].VTD1_Study_Team_Member__c=   craSTM.id;
        update vsites;
		
        Study_Site_Team_Member__c sstm1 = new Study_Site_Team_Member__c();
        sstm1.VTR5_Associated_CRA__c = craSTM.Id;
        sstm1.VTD1_SiteID__c = vsites[0].Id;
        insert sstm1;
        Test.stopTest();
        
		Study_Team_Member__c craSTM1 =   [SELECT	Id, 
                                            User__c,
                                            User__r.FirstName, 
                                            User__r.Profile.Name,
                                            Study_Team_Member__c                                           
                                    FROM Study_Team_Member__c
                                    WHERE Study__c = :study.Id AND User__r.Profile.Name='CRA' LIMIT 1];
        HealthCloudGA__CarePlanTemplate__Share objVirtualShare = new HealthCloudGA__CarePlanTemplate__Share();
        objVirtualShare.ParentId = Study.Id;
        objVirtualShare.UserOrGroupId = craSTM1.User__c;
        objVirtualShare.AccessLevel = 'Edit';
        insert objVirtualShare;
        
        
        VTD1_Monitoring_Visit__c monVisit = new VTD1_Monitoring_Visit__c();
        monVisit.VTD1_Study__c = study.id;
        monVisit.VTD1_Virtual_Site__c = vsites[0].id;
        monVisit.VTR5_MRRSignedMVReport__c = mrrUser.id;
        
        /*@Vijendra Hire
         * @Story : SH-17437 
         * @Description : Removed onsite CRA fields from here
         */
      // //monVisit.VTR2_Remote_Onsite__c = 'Remote'; 
        System.runAs(new User(Id = craSTM1.User__c)){
            insert monVisit;
        }


        

            //createFile('test', monVisit.Id, 'I', 'AllUsers');

            insert new VTD2_TN_Catalog_Code__c(
              
                VTD2_T_Task_Unique_Code__c = 'T613',
                VTD2_T_Category__c = 'Document Review',
                VTD2_T_Task_Record_Type_ID__c = '$Setup.VTD1_RTId__c.VTR5_Pool_Task_MRR__c',
                VTD2_T_Due_Date__c = 'TODAY()',
                VTD2_T_SObject_Name__c = 'VTD1_Monitoring_Visit__c',
                RecordTypeId = VTD2_TN_Catalog_Code__c.getSObjectType().getDescribe().getRecordTypeInfosByName().get('PoolTask').getRecordTypeId(),
                VTD2_T_Patient__c = 'VTD1_Document__c.VTD1_Clinical_Study_Membership__c',
                VTD2_T_Assigned_To_ID__c = 'VTD1_Monitoring_Visit__c.VTD1_Virtual_Site__r.VTD1_Study__r.VTR5_MRR_Queue_ID__c',
                VTR3_T_Name__c = 'Approve or reject visit report',
		        VTD2_T_Related_To_Id__c='VTD1_Monitoring_Visit__c.Id',
		        VTD2_T_Care_Plan_Template__c='VTD1_Monitoring_Visit__c.VTD1_Virtual_Site__r.VTD1_Study__r.Id',
                VTR5_T_MonitoringVisit__c='VTD1_Monitoring_Visit__c.Id',
                VTD2_T_Subject__c = 'Approve or reject " & VTD1_Monitoring_Visit__c.VTD2_Virtual_Site_Name__c & " - " & VTD1_Monitoring_Visit__c.Name &" visit report for " & VTD1_Monitoring_Visit__c.VTD2_VirtualSitePIFullName__c'
        );

	


}
    @IsTest public static void testsendDocusignEmail() {
        VTD1_Monitoring_Visit__c mVisit=[Select Id from VTD1_Monitoring_Visit__c limit 1];
        HealthCloudGA__CarePlanTemplate__c study =[Select Id from HealthCloudGA__CarePlanTemplate__c limit 1];
        VT_D2_TNCatalogTasks.generateTasks(
            new List<String>{
                    'T613' 
            }, new List<String>{
                mVisit.Id
            }, null, null);

           
            DomainObjects.User_t user = new DomainObjects.User_t()
            //.addContact(con)
            .setProfile('Monitoring Report Reviewer');
            //List<User>mrrs = [Select Id,Email From User where Profile.Name='Monitoring Report Reviewer' limit 3];
            //system.debug('mrrs '+mrrs);
         
           

            Group SCQueue = new Group();
            SCQueue.Name='Mrr_Test';
            insert SCQueue;
            study.VTR5_MRR_Queue_Id__c=SCQueue.Id;
            update study;
  //      test.startTest();
        List<User>mrrs = [Select Id,Email From User where Profile.Name='Monitoring Report Reviewer' and isActive=true limit 3];
        List <GroupMember> gms = new List<GroupMember>();
        User useradmin=[Select Id from User where Id=:UserInfo.getUserId() limit 1];
        system.runas(useradmin){
       // Group SCQueue=[Select Id from Group where Name='Mrr_Test' limit 1];
        for(User us:mrrs){
            GroupMember gm1 = new GroupMember();
            gm1.GroupId=SCQueue.Id;
            gm1.UserOrGroupId=us.Id;
            gms.add(gm1);

        }
        
        insert gms;
        test.startTest();
        system.runas(mrrs[1]){
        VTR5_Pool_Task__c pt = [Select Id, Name,RecordTypeId, OwnerId from VTR5_Pool_Task__c limit 1];
        pt.OwnerId=mrrs[1].Id;
        pt.VTR5_TypeForBackEndLogic__c='Monitoring Visit Report Sent';
        system.debug('PT '+pt);
        update pt;
        test.stopTest();
        }
        system.debug('usrs '+mrrs); 
      //  pt.OwnerId=mrrs[1].Id;
       // update pt;
        }
        //Test.stoptest();

    }    
    private static ContentVersion createFile(String title, Id parentId, String shareType, String visibility) {
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.PathOnClient = 'test.txt';
        contentVersion.Title = title;
        contentVersion.VersionData = Blob.valueOf('Test Data');
        insert contentVersion;

        ContentVersion contentVersion1 = [
                SELECT ContentDocumentId
                FROM ContentVersion
                WHERE Id = :contentVersion.Id
        ];

        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = parentId;
        contentDocumentLink.ContentDocumentId = contentVersion1.ContentDocumentId;
        contentDocumentLink.ShareType = shareType;
        contentDocumentLink.Visibility = visibility;
        insert contentDocumentLink;

        return contentVersion1;
    }
}