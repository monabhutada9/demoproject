@IsTest
public with sharing class VT_R2_NewSiteHandlerTest {

    public static void testBehavior() {
        Case patentCase = [SELECT Id, VTD1_Study__c, VTD1_Virtual_Site__c, VTR2_New_Virtual_Site__c FROM Case WHERE Subject = 'VT_R5_allTest' LIMIT 1];
        Test.startTest();
        VT_R2_NewSiteHandler handler = new VT_R2_NewSiteHandler(new List<Case>{patentCase});
        handler.execute();
        Test.stopTest();

        System.assertEquals(2, [SELECT Id FROM VTD1_Document__c].size());
    }

}