@IsTest
private with sharing class VT_R5_AllTests_Part2 {

    @TestSetup
    static void setup() {
        VT_R3_GlobalSharing.disableForTest = true;
        DomainObjects.Study_t study = new DomainObjects.Study_t()
                .addAccount(new DomainObjects.Account_t().setRecordTypeByName('Sponsor'))
                .setOriginalName('tN')
                .setStudyAdminId(UserInfo.getUserId());

        DomainObjects.User_t piUser = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile('Primary Investigator')
                .setEmail(DomainObjects.RANDOM.getEmail());

        DomainObjects.Contact_t scrCon = new DomainObjects.Contact_t()
                .setLastName('Con1');
        DomainObjects.User_t scrUser = new DomainObjects.User_t()
                .addContact(scrCon)
                .setProfile('Site Coordinator');

        DomainObjects.Phone_t phone = new DomainObjects.Phone_t(new DomainObjects.Account_t()
                .setRecordTypeByName('Sponsor'), '345-6789')
                .setType('Home')
                .setPrimaryForPhone(false)
                .addContact(scrCon);
        phone.persist();
        DomainObjects.VTR2_Study_Geography_t studyGeo = new DomainObjects.VTR2_Study_Geography_t()
                .addVTD2_Study(study)
                .setGeographicalRegion('Country')
                .setCountry('US')
                .setDateOfBirthRestriction('Year Only');
        DomainObjects.VTR2_StudyPhoneNumber_t studyPhone = new DomainObjects.VTR2_StudyPhoneNumber_t()
                .addVTR2_Study_Geography(studyGeo);

        DomainObjects.Contact_t patientContact = new DomainObjects.Contact_t();
        DomainObjects.User_t ptUser = new DomainObjects.User_t()
                .addContact(patientContact)
                .setProfile('Patient');
        DomainObjects.Case_t patientCase = new DomainObjects.Case_t()
                .setRecordTypeByName('CarePlan')
                .setSubject('VT_R5_allTest')
                .addPIUser(piUser)
                .addVTD1_Patient_User(ptUser)
                .addStudy(study)
                .addUser(ptUser)
                .addContact(patientContact)
                .addStudyGeography(studyGeo);
        patientCase.persist();
        Test.startTest();
        DomainObjects.VirtualSite_t virtualSite = new DomainObjects.VirtualSite_t()
                .addStudy(study)
                .setStudySiteNumber('12345');

        DomainObjects.StudyTeamMember_t stmCra = new DomainObjects.StudyTeamMember_t()
                .addUser(new DomainObjects.User_t().setProfile('CRA'))
                .addStudy(study)
                .setRecordTypeByName('CRA');

        DomainObjects.StudyTeamMember_t stmPI = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addUser(piUser)
                .addVirtualSite(virtualSite)
                .setRecordTypeByName('PI');
        //START RAJESH SH-17440
        //.setOnsiteCraId(stmCra.id)
        //.setRemoteCraId(stmCra.id);
        //END:SH-17440
        DomainObjects.StudyTeamMember_t stmSCR = new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addVirtualSite(virtualSite)
                .setUserId(UserInfo.getUserId())
                .setRecordTypeByName('SCR');
        new DomainObjects.StudyTeamMember_t()
                .addStudy(study)
                .addVirtualSite(virtualSite)
                .setUserId(UserInfo.getUserId())
                .setRecordTypeByName('SCR')
                .persist();

        new DomainObjects.Study_Site_Team_Member_t()
                .addVTR2_Associated_SCr(stmSCR)
                .addVTR2_Associated_PI(stmPI)
                .addVTD1_Associated_PI_t(stmPI)
                .persist();
        Test.stopTest();
        DomainObjects.StudySiteTeamMember_t sstm = new DomainObjects.StudySiteTeamMember_t()
                .setAssociatedPI2(stmPI)
                .setAssociatedScr(stmSCR);
        sstm.persist();

        DomainObjects.Case_t cas = new DomainObjects.Case_t()
                .setRecordTypeByName('VTD1_PCF')
                .setSubject('CaseControllerRemoteTest')
                .setVTD1_Clinical_Study_Membership(patientCase.id)
                .addStudy(study)
                .addPIUser(piUser)
                .addVirtualSite(virtualSite)
                .addContact(scrCon);

        DomainObjects.VTR2_Site_Address_t address = new DomainObjects.VTR2_Site_Address_t()
                .setVirtualSiteId(virtualSite.toObject().Id)
                .setPrimary(true);
        address.persist();

        new DomainObjects.VTD1_Patient_Stratification_t()
                .addCase(cas)
                .addStratification(new DomainObjects.VTD1_Study_Stratification_t()
                .addStudy(study))
                .addValue('TestValue')
                .persist();
        DomainObjects.VTD1_Actual_Visit_t visit = new DomainObjects.VTD1_Actual_Visit_t()
                .setVTD1_Case(patientCase.id)
                .setVTD1_Status('To Be Scheduled')
                .setScheduledDateTime(Datetime.newInstance(2020, 2, 17))
                .setRecordTypeByName('VTD1_Unscheduled');

        new DomainObjects.Visit_Member_t()
                .setVTD1_Participant_UserId(ptUser.Id)
                .setVTD1_External_Participant_Type('Patient')
                .addVTD1_Actual_Visit(visit)
                .persist();
        new DomainObjects.Visit_Member_t()
                .setVTD1_External_Participant_Type('HHN')
                .addVTD1_Actual_Visit(visit)
                .persist();

        new DomainObjects.VTD1_Regulatory_Binder_t()
                .addStudy(study)
                .addRegDocument(new DomainObjects.VTD1_Regulatory_Document_t()
                .setDocumentType(VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_SOURCE_NOTE_OF_TRANSFER))
                .persist();
        new DomainObjects.VTD1_Regulatory_Binder_t()
                .addStudy(study)
                .addRegDocument(new DomainObjects.VTD1_Regulatory_Document_t()
                .setDocumentType(VT_R4_ConstantsHelper_Documents.REGULATORY_DOCUMENT_TYPE_TARGET_NOTE_OF_TRANSFER))
                .persist();
    }
    @IsTest
    static void VT_D2_StudyTeamByStudyControllerTest1() {
        VT_D2_StudyTeamByStudyControllerTest.getMyStudiesTest();
    }
    @IsTest
    static void VT_D2_StudyTeamByStudyControllerTest2() {
        VT_D2_StudyTeamByStudyControllerTest.getMyStudiesExceptionTest();
    }
    @IsTest
    static void VT_D2_StudyTeamByStudyControllerTest3() {
        VT_D2_StudyTeamByStudyControllerTest.getStudyTeamTest();
    }
    @IsTest
    static void VT_D2_StudyTeamByStudyControllerTest4() {
        VT_D2_StudyTeamByStudyControllerTest.getStudyTeamExceptionTest();
    }
    @IsTest
    static void VT_D2_StudyTeamByStudyControllerTest5() {
        VT_D2_StudyTeamByStudyControllerTest.getOtherRolesMapTest();
    }
    @IsTest
    static void VT_R3_InputDobControllerTest1() {
        VT_R3_InputDobControllerTest.testGetContactSuccess();
    }
    @IsTest
    static void VT_R3_InputDobControllerTest2() {
        VT_R3_InputDobControllerTest.testGetStudyGeographySuccess();
    }
    @IsTest
    static void VT_R2_ActualVisitLocationControllerTest() {
        VT_R2_ActualVisitLocationControllerTest.test1();
    }
//    @IsTest
//    static void VT_R5_ActualVisitProcessesHandlerTest1() {
//        VT_R5_ActualVisitProcessesHandlerTest.populatingStatusToBeScheduledSetDateTest();
//    }
//    @IsTest
//    static void VT_R5_ActualVisitProcessesHandlerTest2() {
//        VT_R5_ActualVisitProcessesHandlerTest.SCTasksForHCPAndIRVisitsTest();
//    }
//    @IsTest
//    static void VT_R5_ActualVisitProcessesHandlerTest3() {
//        VT_R5_ActualVisitProcessesHandlerTest.populatingContactFieldOnUnscheduledTest();
//    }
//    @IsTest
//    static void VT_R5_ActualVisitProcessesHandlerTest4() {
//        VT_R5_ActualVisitProcessesHandlerTest.handleCaseUpdateOnVisitCompletedTest();
//    }
//    @IsTest
//    static void VT_R5_ActualVisitProcessesHandlerTest5() {
//        VT_R5_ActualVisitProcessesHandlerTest.generateVisitTasksOnDateBatchTest();
//    }
//    @IsTest
//    static void VT_R5_ActualVisitProcessesHandlerTest6() {
//        VT_R5_ActualVisitProcessesHandlerTest.generateVisitTasksOnDateTest();
//    }
//    @IsTest
//    static void VT_R5_ActualVisitProcessesHandlerTest7() {
//        VT_R5_ActualVisitProcessesHandlerTest.scheduleJobForGenerateVisitTasksOnDateTest();
//    }
//    @IsTest
//    static void VT_R5_ActualVisitProcessesHandlerTest8() {
//        VT_R5_ActualVisitProcessesHandlerTest.changeInstructionsOnTasksTest();
//    }
//    @IsTest
//    static void VT_R5_ActualVisitProcessesHandlerTest9() {
//        VT_R5_ActualVisitProcessesHandlerTest.rejectTimeSlotsWhenScheduledTest();
//    }
    @IsTest
    static void VT_R2_NewProtocolDeviationFormCntrlTest1() {
        VT_R2_NewProtocolDeviationFormCntrlTest.getPDDataTest();
    }
    @IsTest
    static void VT_R2_NewProtocolDeviationFormCntrlTest2() {
        VT_R2_NewProtocolDeviationFormCntrlTest.createNewPDDraftRecordTest();
    }
    @IsTest
    static void VT_R2_SCRPatientVisitDetailTest1() {
        VT_R2_SCRPatientVisitDetailTest.cancelVisitRemoteTest();
    }
    @IsTest
    static void VT_R2_SCRPatientVisitDetailTest2() {
        VT_R2_SCRPatientVisitDetailTest.getVisitDataTest();
    }
    @IsTest
    static void VT_R2_SCRPatientVisitDetailTest3() {
        VT_R2_SCRPatientVisitDetailTest.reasonOfBackdatingErrorTest();
    }
    @IsTest
    static void VT_R2_SCRPatientVisitDetailTest4() {
        VT_R2_SCRPatientVisitDetailTest.reasonOfBackdatingWithoutErrorTest();
    }
    @IsTest
    static void VT_R3_RestPatientCalendarTest1() {
        VT_R3_RestPatientCalendarTest.calendarTest();
    }
    @IsTest
    static void VT_R3_RestPatientCalendarTest2() {
        VT_R3_RestPatientCalendarTest.getCalendarEventsTest();
    }
    @IsTest
    static void VT_R3_RestPatientCalendarTest3() {
        VT_R3_RestPatientCalendarTest.getCalendarEventsTest2();
    }
    @IsTest
    static void VT_R3_RestPatientCalendarTest4() {
        VT_R3_RestPatientCalendarTest.getCalendarEventsTest3();
    }
    @IsTest
    static void VT_R3_RestPatientCalendarTest5() {
        VT_R3_RestPatientCalendarTest.rescheduleRequestTest();
    }
    @IsTest
    static void VT_R3_RestPatientCalendarTest6() {
        VT_R3_RestPatientCalendarTest.cancelCalendarTest();
    }
//    @IsTest
//    static void VT_R2_SCRSiteSwitcherRemoteTest() {
//        VT_R2_SCRSiteSwitcherRemoteTest.testBehavior();
//    }
    @IsTest
    static void VT_R4_DocsAuditDataPopulationBatchTest() {
        VT_R4_DocsAuditDataPopulationBatchTest.testBehavior();
    }
    @IsTest
    static void VT_R2_NewSiteHandlerTest() {
        VT_R2_NewSiteHandlerTest.testBehavior();
    }
    @IsTest
    static void VT_D1_PIStudySwitcherRemoteTest() {
        VT_D1_PIStudySwitcherRemoteTest.testBehavior();
    }
    @IsTest
    static void VT_D1_PatientCalendar_Test1() {
        VT_D1_PatientCalendar_Test.getEventsTest();
    }
    @IsTest
    static void VT_D1_PatientCalendar_Test2() {
        VT_D1_PatientCalendar_Test.cancelVisitRemoteTest();
    }
    @IsTest
    static void VT_D1_PatientCalendar_Test3() {
        VT_D1_PatientCalendar_Test.getDatesTimesTest();
    }
    @IsTest
    static void VT_D1_PatientCalendar_Test4() {
        VT_D1_PatientCalendar_Test.updateActualVisitTest();
    }
    @IsTest
    static void VT_D1_PatientCalendar_Test5() {
        VT_D1_PatientCalendar_Test.getUserInfoTest();
    }
    @IsTest
    static void VT_D1_PatientCalendar_Test6() {
        VT_D1_PatientCalendar_Test.getProtocolVisitLabelTranslatedTest();
    }
    @IsTest
    static void VT_D1_PatientCalendar_Test7() {
        VT_D1_PatientCalendar_Test.getVisitDescriptionTest();
    }
    @IsTest
    static void VT_R2_PIMyPatientGuidesControllerTest1() {
        VT_R2_PIMyPatientGuidesControllerTest.testSuccessGetMyPatientGuides();
    }
    @IsTest
    static void VT_R2_PIMyPatientGuidesControllerTest2() {
        VT_R2_PIMyPatientGuidesControllerTest.testFailedGetMyPatientGuides();
    }
    @IsTest
    static void VT_R2_AddNewPatientControllerTest1() {
        VT_R2_AddNewPatientControllerTest.createCandidatePatientTest();
    }
    @IsTest
    static void VT_R2_AddNewPatientControllerTest2() {
        VT_R2_AddNewPatientControllerTest.preScreeningTest();
    }
    @IsTest
    static void VT_R2_AddNewPatientControllerTest3() {
        VT_R2_AddNewPatientControllerTest.getPIAndStudyInfoListTest();
    }
    @IsTest
    static void VT_R2_AddNewPatientControllerTest4() {
        VT_R2_AddNewPatientControllerTest.getVirtualSiteRemoteTest();
    }
    @IsTest
    static void VT_R2_AddNewPatientControllerTest5() {
        VT_R2_AddNewPatientControllerTest.isEmailUsedTest();
    }
    @IsTest
    static void VT_R2_AddNewPatientControllerTest6() {
        VT_R2_AddNewPatientControllerTest.isMobileUsedTest();
    }
}