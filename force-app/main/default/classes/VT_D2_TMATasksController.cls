/**
 * Created by shume on 23/11/2018.
 */

public with sharing class VT_D2_TMATasksController {
    @AuraEnabled
    public static List<VTD2_TMA_Task__c> getTMATasksFromPool() {
        return getTasks();
    }

    @AuraEnabled
    public static List<VTD2_TMA_Task__c> claimTasksRemote (List<Id> taskIds) {
        List<VTD2_TMA_Task__c> tasksToUpdate = new List<VTD2_TMA_Task__c>();
        List<VTD2_TMA_Task__c> tasks =  [SELECT Id, OwnerId
                                        FROM VTD2_TMA_Task__c
                                        WHERE Id IN :taskIds AND OwnerId!=:UserInfo.getUserId()
                                        FOR UPDATE ];
        for (VTD2_TMA_Task__c t : tasks) {
            if (t.OwnerId.getSobjectType().getDescribe().getName() == 'Group') {
                tasksToUpdate.add(t);
            }
        }
        for (VTD2_TMA_Task__c t : tasksToUpdate) {
            t.OwnerId = UserInfo.getUserId();
        }
        try {
            update tasks;
            return getTasks();
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
        }
    }

    private static List <VTD2_TMA_Task__c> getTasks() {
        // user groups
        List<Id> groupIds = new List<Id>();
        for (GroupMember gm : [SELECT GroupId
        FROM GroupMember
        WHERE UserOrGroupId = :UserInfo.getUserId() AND Group.Type != 'Queue']) {
            groupIds.add(gm.Id);
        }
        return [SELECT Id,
                VTD2_Subject__c,
                VTD2_PCF__r.VTD1_Study__c,
                VTD2_PCF__r.VTD1_Study__r.Name,
                VTD2_Protocol_Deviation__r.VTD1_StudyId__c,
                VTD2_Protocol_Deviation__r.VTD1_StudyId__r.Name,
                VTD2_Document__r.VTD1_Study__c,
                VTD2_Document__r.VTD1_Study__r.Name,
                CreatedDate
        FROM VTD2_TMA_Task__c
        WHERE OwnerId IN (SELECT GroupId
        FROM GroupMember
        WHERE ((UserOrGroupId = :UserInfo.getUserId() OR UserOrGroupId IN :groupIds)
        AND Group.Type = 'Queue'))
        ORDER BY CreatedDate DESC];
    }
}