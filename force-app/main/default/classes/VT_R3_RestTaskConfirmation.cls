/**
 * Created by Alexander Komarov on 30.05.2019.
 */

@RestResource(UrlMapping='/Task/Confirmation/*')
global with sharing class VT_R3_RestTaskConfirmation {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();

    @HttpGet
    global static String closeTask() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        String temp = request.requestURI.substringAfterLast('/');


        if (String.isBlank(temp) || !helper.isIdCorrectType(temp, 'Task')) {
            response.statusCode = 400;
            cr.buildResponse(helper.forAnswerForIncorrectInput());
            return JSON.serialize(cr, true);
        }

        try {
            Id taskId = temp;
            VT_D1_TaskListController.closeTask(taskId);
        } catch (Exception e) {
            response.statusCode = 500;
            cr.buildResponse('FAILURE. Server error! Stacktrace: ' + e.getStackTraceString() + ' Message: ' + e.getMessage() + ' Type: ' + e.getTypeName(),
                    'Some text for NOT-success-banner "TASK CONFIRMATION" action');
            return JSON.serialize(cr, true);
        }
        cr.buildResponse('SUCCESS');
        return JSON.serialize(cr, true);
    }
}