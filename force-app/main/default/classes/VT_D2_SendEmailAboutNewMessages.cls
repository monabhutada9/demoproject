/*
 * Created by shume on 08/10/2018.
*/

public class VT_D2_SendEmailAboutNewMessages implements Database.Batchable<sObject>{

    public static final String PT_CG_TEMPLATE_NAME = 'VT_D2_New_Messages_Notification_Template';
    public static final String PI_TEMPLATE_NAME = 'VTD2_New_Messages_Email_Template_PI';
    private List<EmailTemplate> emailTemplates;
    private List <OrgWideEmailAddress> owEmailAddresses;

    public VT_D2_SendEmailAboutNewMessages() {
        this.emailTemplates = [
                SELECT Id, DeveloperName
                FROM EmailTemplate
                WHERE DeveloperName IN (:PT_CG_TEMPLATE_NAME, :PI_TEMPLATE_NAME)
        ];
        this.owEmailAddresses = [
                SELECT Id
                FROM OrgWideEmailAddress
                ORDER BY CreatedDate
                LIMIT 1
        ];
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
        List<String> profilesForSendingEmail = new List<String>();
        profilesForSendingEmail.add(VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME);
        profilesForSendingEmail.add(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);
        profilesForSendingEmail.add(VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME);
        String query = 'SELECT Id, OwnerId, Owner.Email, Owner.Name, Owner.Profile.Name, Number_of_unread_messages__c ' +
                'FROM VTD1_NotificationC__c WHERE Number_of_unread_messages__c > 0 AND Type__c = \'Messages\' ' +
                'AND OwnerId IN (SELECT Id FROM User WHERE Contact.VTD2_Send_notifications_as_emails__c = TRUE ' +
                'AND Email != NULL AND Profile.Name IN :profilesForSendingEmail)';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<VTD1_NotificationC__c> scope) {
        Map<String, EmailTemplate> nameToTemplateMap = new Map<String, EmailTemplate>();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        for (EmailTemplate item : this.emailTemplates) {
            nameToTemplateMap.put(item.DeveloperName, item);
        }
        if (!nameToTemplateMap.isEmpty()) {

            for (VTD1_NotificationC__c n : scope) {
                EmailTemplate template = nameToTemplateMap.get(
                        n.Owner.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME ?
                                PI_TEMPLATE_NAME : PT_CG_TEMPLATE_NAME);

                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                //mail.setSenderDisplayName('Iqvia');
                mail.setTargetObjectId(n.OwnerId);
                mail.setUseSignature(false);
                mail.setBccSender(false);
                mail.setSaveAsActivity(false);
                mail.setTemplateId(template.Id);
                mail.setWhatId(n.Id);
                if (!this.owEmailAddresses.isEmpty())
                    mail.setOrgWideEmailAddressId(this.owEmailAddresses[0].Id);
                mails.add(mail);
            }
            if (!mails.isEmpty()) {
                Messaging.sendEmail(mails);
            }
        }
    }

    public void finish(Database.BatchableContext BC) {
        System.debug('VT_D2_SendEmailAboutNewMessages butch executed.');
    }
}