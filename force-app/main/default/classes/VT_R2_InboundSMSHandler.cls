/**
 * Created by MPlatonov on 14.02.2019.
 */

public with sharing class VT_R2_InboundSMSHandler {

    private static final String SUBSCRIBE_YES = 'YES';
    private static final String SUBSCRIBE_STOP = 'STOP';
    private static final String SUBSCRIBE_HELP = 'HELP';

    public static void onInsertAfter(List <VTR2_InboundSMS__c> inboundSMSList) {
        Map <String, String> phoneNumberToUnrecozniedOptCodeMap = new Map<String, String>();
        Map <String, Id> phoneNumberToContactIdMap = new Map<String, Id>();

        Map <String, VTR2_InboundSMS__c> inboundSMSMap = new Map<String, VTR2_InboundSMS__c>();
        Set <String> uniqueCode = new Set<String>();
        for (VTR2_InboundSMS__c inboundSMS : inboundSMSList) {
            inboundSMSMap.put(inboundSMS.VTR2_PhoneNumber__c + '_' + inboundSMS.VTR2_MessageText__c, inboundSMS);
        }

        Map <String, VT_D1_Phone__c> phoneNumbersMap = new Map<String, VT_D1_Phone__c>();
        for (VTR2_InboundSMS__c inboundSMS : inboundSMSMap.values()) {
            phoneNumbersMap.put(inboundSMS.VTR2_PhoneNumber__c, null);
        }

        // R5.6 Changes
		//System.debug('wewewwe: ' + [SELECT Id, VTR2_NumberWithoutDigits__c, VTR2_OptInFlag__c, VTR2_SMS_Opt_In_Status__c FROM VT_D1_Phone__c]);
		//System.debug('wewewwe__ phoneNumbersMap.keySet(): ' + phoneNumbersMap.keySet());
        List <VT_D1_Phone__c> phones = [select Id, PhoneNumber__c, VTD1_Contact__c, VTR2_SMS_Opt_In_Status__c,
                                        VTR2_OptInFlag__c, VTR2_NumberWithoutDigits__c
                                        from VT_D1_Phone__c
                                        where VTR2_NumberWithoutDigits__c in : phoneNumbersMap.keySet()
                                        AND VTR2_OptInFlag__c = true AND (VTR2_SMS_Opt_In_Status__c = 'Pending' OR VTR2_SMS_Opt_In_Status__c='Active')];
		//System.debug('phones size : ' + phones.size());
		if (phones.isEmpty())
            return;

        for (VT_D1_Phone__c phone : phones) {
            phoneNumbersMap.put(phone.VTR2_NumberWithoutDigits__c, phone);
        }

        List <VT_D1_Phone__c> phonesToUpdate = new List<VT_D1_Phone__c>();

        for (VTR2_InboundSMS__c inboundSMS : inboundSMSMap.values()) {
           // System.debug('inboundSMS = ' + inboundSMS);
            //System.debug('phoneNumbersMap = ' + phoneNumbersMap.get(inboundSMS.VTR2_PhoneNumber__c));
            VT_D1_Phone__c phone = phoneNumbersMap.get(inboundSMS.VTR2_PhoneNumber__c);
            if (phone != null) {
				//System.debug('34343434344 : ' + phone.VTR2_OptInFlag__c);
				//System.debug('34343434344 : ' + phone.VTR2_SMS_Opt_In_Status__c);
				//System.debug('34343434344 : ' + inboundSMS.VTR2_MessageText__c.toUpperCase());
                if ((!phone.VTR2_OptInFlag__c || phone.VTR2_SMS_Opt_In_Status__c != 'Active') && inboundSMS.VTR2_MessageText__c.toUpperCase() == SUBSCRIBE_YES) {
                    phone.VTR2_OptInFlag__c = true;
                    phone.VTR2_SMS_Opt_In_Status__c = 'Active';
                    phonesToUpdate.add(phone);
                } else if ((phone.VTR2_OptInFlag__c || phone.VTR2_SMS_Opt_In_Status__c == 'Active') && inboundSMS.VTR2_MessageText__c.toUpperCase()  == SUBSCRIBE_STOP) {
                    phone.VTR2_OptInFlag__c = false;
                    phone.VTR2_SMS_Opt_In_Status__c = 'Inactive';
                    phonesToUpdate.add(phone);
                } else {
                    phoneNumberToUnrecozniedOptCodeMap.put(phone.PhoneNumber__c, inboundSMS.VTR2_MessageText__c);
                    phoneNumberToContactIdMap.put(phone.PhoneNumber__c, phone.VTD1_Contact__c);
                }
            }
        }

        if (!phonesToUpdate.isEmpty()) {
            update phonesToUpdate;
        }

        if (!phoneNumberToUnrecozniedOptCodeMap.isEmpty()) {
            sendResponseSMS(phoneNumberToUnrecozniedOptCodeMap, phoneNumberToContactIdMap);
        }
    }

    private static void sendResponseSMS(Map <String, String> phoneNumberToUnrecozniedOptCodeMap, Map <String, Id> phoneNumberToContactIdMap) {
        System.debug('sendResponseSMS ' + phoneNumberToUnrecozniedOptCodeMap);
        Map <String, List <String>> messageToPhoneNumbersMap = new Map<String, List<String>>();
        for (String phoneNumber : phoneNumberToUnrecozniedOptCodeMap.keySet()) {
            String optCode = phoneNumberToUnrecozniedOptCodeMap.get(phoneNumber);
            List <String> phoneNumbers = messageToPhoneNumbersMap.get(optCode);
            if (phoneNumbers == null) {
                phoneNumbers = new List<String>();
                messageToPhoneNumbersMap.put(optCode, phoneNumbers);
            }
            phoneNumbers.add(phoneNumber);
        }

        Long currentTime = System.now().getTime();

        for (String message : messageToPhoneNumbersMap.keySet()) {
            List <String> phoneNumbers = messageToPhoneNumbersMap.get(message);
            //System.debug('send message ' + message);
            //System.debug('to phoneNumbers ' + phoneNumbers);
            Map <String, Id> phoneToContactMap = new Map<String, Id>();
            for (String phoneNumber : phoneNumbers) {
                phoneToContactMap.put(phoneNumber, phoneNumberToContactIdMap.get(phoneNumber));
            }
            if (message.toUpperCase() == SUBSCRIBE_HELP) {
                message = Label.VTR2_SMSSubscribeHelp;
            } else {
                message = Label.VTR2_SMSUnrecognized + ' ' + message;
            }
            //System.debug('send response ' + message + ' ' + phoneToContactMap);
            sendResponseSMSFuture(phoneToContactMap, message, '' + (currentTime ++));
        }
    }

    @future(callout=true)
    public static void sendResponseSMSFuture(Map <String, Id> phoneToContactMap, String message, String notId) {
        VT_R2_McloudBroadcaster broadcaster = new VT_R2_McloudBroadcaster();
        broadcaster.sendMessage(phoneToContactMap, message, notId, null);
    }

    public static void checkOptsChanged(List <VT_D1_Phone__c> phones, Map <Id, VT_D1_Phone__c> oldMap) {
        Map <String, List <VT_D1_Phone__c>> changeOptSubscribeToPhoneMap = new Map<String, List <VT_D1_Phone__c>>();
        Map <String, String> messages = new Map<String, String>{
                'Pending' => Label.VTR2_SMSSubscribeVerification,
                'Active' => Label.VTR2_SMSSubscribeEnabled,
                'Inactive' => Label.VTR2_SMSSubscribeDisabled
        };
        for (VT_D1_Phone__c phone : phones) {
            if (phone.VTR2_SMS_Opt_In_Status__c == oldMap.get(phone.Id).VTR2_SMS_Opt_In_Status__c)
                continue;
            String message = messages.get(phone.VTR2_SMS_Opt_In_Status__c);
            if (message != null) {
                List <VT_D1_Phone__c> phonesByMessage = changeOptSubscribeToPhoneMap.get(message);
                if (phonesByMessage == null) {
                    phonesByMessage = new List<VT_D1_Phone__c>();
                    changeOptSubscribeToPhoneMap.put(message, phonesByMessage);
                }
                phonesByMessage.add(phone);
            }
        }

        Long currentTime = System.now().getTime();
        for (String message : changeOptSubscribeToPhoneMap.keySet()) {
            List <VT_D1_Phone__c> phonesByMessage = changeOptSubscribeToPhoneMap.get(message);
            Map <String, Id> phoneToContactMap = new Map<String, Id>();
            for (VT_D1_Phone__c phone : phonesByMessage) {
                phoneToContactMap.put(phone.PhoneNumber__c, phone.VTD1_Contact__c);
            }
            //System.debug('send message ' + message + ' to ' + phoneToContactMap);
            sendResponseSMSFuture(phoneToContactMap, message, '' + (currentTime ++));
        }
    }
}