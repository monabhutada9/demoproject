/**
 * @author: Aliaksandr Vabishchevich
 * @date: 27-Aug-2020
 * @description: Test for VT_R5_SignantJsonProcessingService
 */

@IsTest
public with sharing class VT_R5_SignantJsonProcessingServiceTest {
    public static void processSignantRequest() {
        VTR5_Signant_File__c sf = (VTR5_Signant_File__c) new DomainObjects.VTR5_Signant_File_t()
                .setNameByAttr('1001')
                .setXmlContentByAttrs('10001', '10001', null)
                .toObject();

        String requestString = '{"subjects": [{' +
                '"fileName": "' + sf.Name + '", ' +
                '"createdDate": "' + Datetime.now().format('yyyy-MM-dd\'T\'HH:mm:ss') + '", ' +
                '"content": "' + sf.VTR5_XMLContent__c.replace('"', '\\"') + '"' +
                '}]}';

        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/SignantJsonProcessingService/';
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = response;
        RestContext.request.requestBody = Blob.valueOf(requestString);

        VT_R5_SignantJsonProcessingService.processJson();
        Test.stopTest();

        List<VTR5_Signant_File__c> sgFiles = [
                SELECT Name, VTR5_XMLContent__c, VTR5_DateReceived__c, VTR5_IsProcessed__c, VTR5_DateProcessed__c
                FROM VTR5_Signant_File__c
                WHERE Name = :sf.Name
        ];
        System.assertEquals(1, sgFiles.size());

        VTR5_Signant_File__c insertedSf = sgFiles[0];
        System.assertEquals(insertedSf.Name, sf.Name);
        System.assertEquals(insertedSf.VTR5_XMLContent__c, sf.VTR5_XMLContent__c);
        System.assertNotEquals(insertedSf.VTR5_DateReceived__c, null);

        System.assertEquals(insertedSf.VTR5_IsProcessed__c, false);

    }
}