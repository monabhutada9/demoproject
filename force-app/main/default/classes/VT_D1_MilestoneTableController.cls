public with sharing class VT_D1_MilestoneTableController {

    @AuraEnabled
    public static String getParentType(Id parentId) {
        return parentId.getSobjectType().getDescribe().getLabel();
    }

    @AuraEnabled
    public static Map<String, VTD1_Milestone_Component_Config__mdt> getMdtMap(Id parentId) {
        Map<String, VTD1_Milestone_Component_Config__mdt> mdtMap = new Map<String, VTD1_Milestone_Component_Config__mdt>();

        //Boolean isPatientGuide = [SELECT Name FROM Profile WHERE Id = :userInfo.getProfileId()].Name.equals('Patient Guide');
        Boolean isPatientGuide = VTD1_RTId__c.getOrgDefaults().VTD2_Profile_PG__c == String.valueOf(UserInfo.getProfileId()).left(15);

        for (VTD1_Milestone_Component_Config__mdt item : [
                SELECT MasterLabel, VTD1_Actual_Date_Available__c, VTD1_Contracted_Date_Available__c,VTD1_Projected_Date_Available__c
                FROM VTD1_Milestone_Component_Config__mdt
                WHERE VTD1_Parent_Type__c = :parentId.getSobjectType().getDescribe().getLabel()
        ]) {
            if (isPatientGuide) {
                item.VTD1_Actual_Date_Available__c=item.VTD1_Contracted_Date_Available__c=item.VTD1_Projected_Date_Available__c = false;
            }
            mdtMap.put(item.MasterLabel, item);
        }

        return mdtMap;
    }

    @AuraEnabled
    public static List<sObject> getMilestones(Id parentId, Map<String, VTD1_Milestone_Component_Config__mdt> mdtMap) {
        return new VT_D1_MilestoneTableQueryHelper().getMilestones(parentId, mdtMap);
    }

    @AuraEnabled
    public static SObject doSave(String milestone) {
        System.Type recordClass = milestone.contains(VT_R4_ConstantsHelper_AccountContactCase.STUDY_MILESTONE_PARENT_FIELD) ?
                VTD1_Study_Milestone__c.class : VTD1_Site_Milestone__c.class;

        sObject rec = (sObject)JSON.deserialize(milestone, recordClass);
        upsert rec;

        return rec;
    }
}