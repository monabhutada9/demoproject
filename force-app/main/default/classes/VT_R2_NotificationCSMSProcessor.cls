/**
 * Created by MPlatonov on 08.02.2019.
 */

public without sharing class VT_R2_NotificationCSMSProcessor {
    public static final Integer TYPE_EDIARY = 1;
    public static final Integer TYPE_MESSAGE = 2;
    public static final Integer TYPE_VISIT = 3;

    public static void onAfterUpdate(List <VTD1_NotificationC__c> notifications, Map <Id, VTD1_NotificationC__c> oldMap) {
        List <VTD1_NotificationC__c> notificationsFiltered = new List<VTD1_NotificationC__c>();
        for (VTD1_NotificationC__c notification : notifications) {
            if (notification.Number_of_unread_messages__c > oldMap.get(notification.Id).Number_of_unread_messages__c) {
                notificationsFiltered.add(notification);
            }
        }
        if (!notificationsFiltered.isEmpty()) {
            onAfterInsert(notificationsFiltered);
        }
    }

    public static void onAfterInsert(List <VTD1_NotificationC__c> notifications) {
        List <String> profileNames = new List<String>{
            VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME, VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME, VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME, VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME
        };

        Map <Id, Integer> notificationTypeMap = new Map<Id, Integer>();
        Set <Id> userIds = new Set<Id>();
        System.debug('notifications = ' + notifications.size());
        for (VTD1_NotificationC__c notification : notifications) {
//            System.debug('notification = ' + notification);
            if (notification.Title__c != null && notification.Title__c.toLowerCase().indexOf('ediary') >= 0) {
                notificationTypeMap.put(notification.Id, TYPE_EDIARY);
            } else if (notification.Title__c != null && notification.Title__c.toLowerCase().indexOf('message') >= 0) {
                notificationTypeMap.put(notification.Id, TYPE_MESSAGE);
            } else if (notification.Title__c != null && notification.Title__c.toLowerCase().indexOf('visit') >= 0) {
                notificationTypeMap.put(notification.Id, TYPE_VISIT);
            }
            if (notificationTypeMap.containsKey(notification.Id)) {
                userIds.add(notification.OwnerId);
            }
        }
        //List <VTD1_NotificationC__c> notificationsToSend = new List<VTD1_NotificationC__c>();
        Map <Id, Id> notificationIdToContactIdMap = new Map<Id, Id>();
        //Map <Id, String> notificationIdToOptCodeMap = new Map<Id, String>();
        System.debug('userIds = ' + userIds);
        Map <Id, User> userMap = new Map<Id, User>([
            select Id, Profile.Name, ContactId,
                Contact.Account.HealthCloudGA__CarePlan__r.VTD1_Study__r.VTR2_Send_eDiary_Notifications__c,
                Contact.Account.HealthCloudGA__CarePlan__r.VTD1_Study__r.VTR2_Send_New_Messages__c,
                Contact.Account.HealthCloudGA__CarePlan__r.VTD1_Study__r.VTD2_Send_Overdue_Notifications__c,
                Contact.Account.HealthCloudGA__CarePlan__r.VTD1_Study__r.VDT2_Send_Visit_Reminders__c,
                Contact.Account.HealthCloudGA__CarePlan__r.VTR2_Receive_SMS_eDiary_Notifications__c,
                Contact.Account.HealthCloudGA__CarePlan__r.VTR2_Receive_SMS_New_Notifications__c,
                Contact.Account.HealthCloudGA__CarePlan__r.VTR2_Receive_SMS_Visit_Reminders__c
            from User where Id in :userIds and Profile.Name in :profileNames
        ]);

        Set <Id> casesIds = new Set<Id>();
        Set <Id> PIUserIds = new Set<Id>();

        System.debug('userMap = ' + userMap);

        for (VTD1_NotificationC__c notification : notifications) {
            User user = userMap.get(notification.OwnerId);
            if (user != null && (user.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME || user.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME)) {
                Integer notificationType = notificationTypeMap.get(notification.Id);
                Boolean sendEdiaryNotifications_Study = user.Contact.Account.HealthCloudGA__CarePlan__r.VTD1_Study__r.VTR2_Send_eDiary_Notifications__c;
                Boolean sendNewMessages_Study = user.Contact.Account.HealthCloudGA__CarePlan__r.VTD1_Study__r.VTR2_Send_New_Messages__c;
                Boolean sendOverdueNotifications_Study = user.Contact.Account.HealthCloudGA__CarePlan__r.VTD1_Study__r.VTD2_Send_Overdue_Notifications__c;
                Boolean sendVisitReminders_Study = user.Contact.Account.HealthCloudGA__CarePlan__r.VTD1_Study__r.VDT2_Send_Visit_Reminders__c;
//                System.debug('study notifications flags = ' + sendEdiaryNotifications_Study + ' ' + sendNewMessages_Study + ' ' + sendOverdueNotifications_Study + ' ' + sendVisitReminders_Study);

                Boolean sendEdiaryNotifications = user.Contact.Account.HealthCloudGA__CarePlan__r.VTR2_Receive_SMS_eDiary_Notifications__c;
                Boolean sendNewMessages = user.Contact.Account.HealthCloudGA__CarePlan__r.VTR2_Receive_SMS_New_Notifications__c;
                Boolean sendVisitReminders = user.Contact.Account.HealthCloudGA__CarePlan__r.VTR2_Receive_SMS_Visit_Reminders__c;
//                System.debug('STM flag = ' + sendEdiaryNotifications + ' ' + sendNewMessages + ' ' + sendVisitReminders);
                sendEdiaryNotifications = sendEdiaryNotifications_Study && sendEdiaryNotifications;
                sendNewMessages = sendNewMessages_Study && sendNewMessages;
                sendVisitReminders = sendVisitReminders_Study && sendVisitReminders;

                if (notificationType == TYPE_EDIARY && sendEdiaryNotifications) {
                    notificationIdToContactIdMap.put(notification.Id, user.ContactId);
                } else if (notificationType == TYPE_MESSAGE && sendNewMessages) {
                    notificationIdToContactIdMap.put(notification.Id, user.ContactId);
                } else if (notificationType == TYPE_VISIT && sendVisitReminders) {
                    notificationIdToContactIdMap.put(notification.Id, user.ContactId);
                }

            } else if (user != null && (user.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME || user.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME) && notification.VTD1_CSM__c != null) {
                casesIds.add(notification.VTD1_CSM__c);
                PIUserIds.add(notification.OwnerId);
                System.debug('added PI or SCR ' + notification.OwnerId + ' ' + user.Profile.Name);
            }
        }

        if (!casesIds.isEmpty()) {
            System.debug('casesIds = ' + casesIds);
            Map <Id, Case> casesForPI = new Map<Id, Case>([select Id, VTD1_Study__c,
                VTD1_Study__r.VTR2_Send_eDiary_Notifications__c,
                VTD1_Study__r.VTR2_Send_New_Messages__c,
                VTD1_Study__r.VTD2_Send_Overdue_Notifications__c,
                VTD1_Study__r.VDT2_Send_Visit_Reminders__c from Case where Id in : casesIds]);

            List <Study_Team_Member__c> studyTeamMembers = [select Id, Study__c, User__c,
                VTR2_Receive_SMS_Visit_Reminders__c, VTR2_Receive_SMS_New_Message__c
            from Study_Team_Member__c where User__c in :PIUserIds];

            Map <String, Study_Team_Member__c> userPIIdStudyIdToSTMMap = new Map<String, Study_Team_Member__c>();
            for (Study_Team_Member__c studyTeamMember : studyTeamMembers) {
                userPIIdStudyIdToSTMMap.put(studyTeamMember.User__c + '_' + studyTeamMember.Study__c, studyTeamMember);
            }

            for (VTD1_NotificationC__c notification : notifications) {
                Case caseObj = notification.VTD1_CSM__c != null ? casesForPI.get(notification.VTD1_CSM__c) : null;
                if (caseObj == null)
                    continue;
                Boolean sendEdiaryNotifications_Study = caseObj.VTD1_Study__r.VTR2_Send_eDiary_Notifications__c;
                Boolean sendNewMessages_Study = caseObj.VTD1_Study__r.VTR2_Send_New_Messages__c;
                Boolean sendOverdueNotifications_Study = caseObj.VTD1_Study__r.VTD2_Send_Overdue_Notifications__c;
                Boolean sendVisitReminders_Study = caseObj.VTD1_Study__r.VDT2_Send_Visit_Reminders__c;

                System.debug('caseObj for PI = ' + caseObj);
                Study_Team_Member__c studyTeamMember = userPIIdStudyIdToSTMMap.get(notification.OwnerId + '_' + caseObj.VTD1_Study__c);
                System.debug('studyTeamMember for PI = ' + studyTeamMember);
                Boolean sendNewMessages =studyTeamMember != null && studyTeamMember.VTR2_Receive_SMS_New_Message__c;
                Boolean sendVisitReminders = studyTeamMember!=null && studyTeamMember.VTR2_Receive_SMS_Visit_Reminders__c;

                Integer notificationType = notificationTypeMap.get(notification.Id);

                sendNewMessages = sendNewMessages_Study && sendNewMessages;
                sendVisitReminders = sendVisitReminders_Study && sendVisitReminders;

                User user = userMap.get(notification.OwnerId);
                if (notificationType == TYPE_MESSAGE && sendNewMessages) {
                    notificationIdToContactIdMap.put(notification.Id, user.ContactId);
                } else if (notificationType == TYPE_VISIT && sendVisitReminders) {
                    notificationIdToContactIdMap.put(notification.Id, user.ContactId);
                }
            }
        }

        if (!notificationIdToContactIdMap.isEmpty()) {
            sendSMS(notificationIdToContactIdMap);
        }
    }

    public static void sendSMS(Map <Id, Id> notificationIdToContactIdMap) {
        System.debug('sendSMS ' + notificationIdToContactIdMap);
        List <VTD1_NotificationC__c> notifications = [select Id, Message__c,Title__c,VTD2_Title_Parameters__c,VTD1_Parameters__c
        from VTD1_NotificationC__c where Id in : notificationIdToContactIdMap.keySet()];

        List <VT_D1_Phone__c> allPhones = [select Id, VTD1_Contact__c, PhoneNumber__c
        from VT_D1_Phone__c where VTD1_Contact__c in : notificationIdToContactIdMap.values() and Type__c = 'Mobile' and VTR2_OptInFlag__c = true and VTR2_SMS_Opt_In_Status__c = 'Active'];

        System.debug('allPhones = ' + allPhones);

        Map <Id, List <VT_D1_Phone__c>> contactIdToPhonesMap = new Map<Id, List<VT_D1_Phone__c>>();

        for (VT_D1_Phone__c phone : allPhones) {
            List <VT_D1_Phone__c> phones = contactIdToPhonesMap.get(phone.VTD1_Contact__c);
            if (phones == null) {
                phones = new List<VT_D1_Phone__c>();
                contactIdToPhonesMap.put(phone.VTD1_Contact__c, phones);
            }
            phones.add(phone);
        }
        VT_D1_TranslateHelper.translate(notifications);
        for (VTD1_NotificationC__c notification : notifications) {
            Id contactId = notificationIdToContactIdMap.get(notification.Id);
            List <VT_D1_Phone__c> phones = contactIdToPhonesMap.get(contactId);
            if (phones == null)
                continue;
            Map<String, Id> phoneToContactMap = new Map<String, Id>();
            for (VT_D1_Phone__c phone : phones) {
                phoneToContactMap.put(phone.PhoneNumber__c, contactId);
            }
            sendSMSFuture(phoneToContactMap, JSON.serialize(notification), notification.Id);
        }
    }

    @future(callout=true)
    public static void sendSMSFuture(Map<String, Id>phoneToContactMap, String jSONNotification, Id notificationId) {
        VTD1_NotificationC__c notification = (VTD1_NotificationC__c)JSON.deserialize(jSONNotification, VTD1_NotificationC__c.class);
        VT_D1_TranslateHelper.translate(new List<VTD1_NotificationC__c>{notification});
        String message = notification.Message__c + '\n' + Label.VTR2_SMSUnsubscribe;
        VT_R2_McloudBroadcaster broadcaster = new VT_R2_McloudBroadcaster();
        broadcaster.sendMessage(phoneToContactMap, message, notificationId, notificationId);
    }

    public static void optInRemote (String phoneId) {
        try {
            System.debug('phoneId ' + phoneId);
            VT_D1_Phone__c phone  = new VT_D1_Phone__c(Id=phoneId, VTR2_OptInFlag__c = true);
            update phone;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
        }
    }

    public static void resendOptInRemote (String phoneId) {
        System.debug('phoneId ' + phoneId);
        if (phoneId == null) return;
        try {
            VT_D1_Phone__c phone = [SELECT PhoneNumber__c, VTD1_Contact__c FROM VT_D1_Phone__c WHERE Id=:phoneId];
            if (phone==null) return;
            Map <String, Id> phoneToContactMap = new Map<String, Id>();
            phoneToContactMap.put(phone.PhoneNumber__c, phone.VTD1_Contact__c);
            VT_R2_McloudBroadcaster broadcaster = new VT_R2_McloudBroadcaster();
            broadcaster.sendMessage(phoneToContactMap, Label.VTR2_SMSSubscribeVerification, '' + System.now().getTime(), null);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
        }
    }

    public static void optOutRemote (String phoneId) {
        try {
            System.debug('phoneId ' + phoneId);
            VT_D1_Phone__c phone  = new VT_D1_Phone__c(Id=phoneId, VTR2_OptInFlag__c = false,
                    VTR2_SMS_Opt_In_Status__c = 'Inactive');
            update phone;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
        }
    }

    public static void sendContactsRemote(String phone) {
        Map<string, id > phoneToCont = new Map<String, Id>();

        VT_D1_Phone__c phoneDataObj = [
                select Name, PhoneNumber__c, VTD1_Contact__c,
                        VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTR2_StudyPhoneNumber__r.Name,
                        VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_Primary_PG__r.VTD2_Phone_Formula__c,
                        VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_Secondary_PG__r.VTD2_Phone_Formula__c,
                        VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_Primary_PG__r.Name,
                        VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_Secondary_PG__r.Name,
                        VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_PI_contact__c,
                        VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_PI_contact__r.Name,
                        VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_PI_contact__r.Phone,
                        VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTR2_SiteCoordinator__r.Name,
                        VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTR2_SiteCoordinator__r.ContactId,
                        VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_Patient__r.VTD1_Contact__r.Phone
                from VT_D1_Phone__c
                where id = :phone
        ];
         if (phoneDataObj != null) {
            String message = 'Study Delivery Number: ';

            if (phoneDataObj.Name != null) {
                phoneToCont.put(phoneDataObj.Name, phone);
            }

            if (phoneDataObj.VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTR2_StudyPhoneNumber__c != null) {
                message += phoneDataObj.VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTR2_StudyPhoneNumber__r.Name;
            }

            if (phoneDataObj.VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTR2_SiteCoordinator__c != null) {
                VT_D1_Phone__c[] phoneSiteCoordWork = [
                        select PhoneNumber__c, VTD1_Contact__r.Name
                        from VT_D1_Phone__c
                        where Type__c = 'Work'
                        and IsPrimaryForPhone__c = true
                        and VTD1_Contact__c = :phoneDataObj.VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTR2_SiteCoordinator__r.ContactId
                ];
                if(phoneSiteCoordWork.size() > 0) {
                    message += ';\n Site coordinator ' + phoneSiteCoordWork[0].VTD1_Contact__r.Name
                            + ': ' + phoneSiteCoordWork[0].PhoneNumber__c;
                }
            }

            if (phoneDataObj.VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_Primary_PG__c != null &&
                    phoneDataObj.VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_Primary_PG__r.VTD2_Phone_Formula__c != null) {
                message += ';\nPrimary PG ' + phoneDataObj.VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_Primary_PG__r.Name
                        + ': ' + phoneDataObj.VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_Primary_PG__r.VTD2_Phone_Formula__c;
            }

            if (phoneDataObj.VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_Secondary_PG__c != null &&
                    phoneDataObj.VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_Secondary_PG__r.VTD2_Phone_Formula__c != null) {
                message += ';\nSecondary PG ' + phoneDataObj.VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_Secondary_PG__r.Name
                        + ': ' + phoneDataObj.VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_Secondary_PG__r.VTD2_Phone_Formula__c;
            }

            if (phoneDataObj.VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_PI_contact__c != null &&
                    phoneDataObj.VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_PI_contact__r.Name != null) {
                VT_D1_Phone__c[] phonePiWork = [
                        select VTD1_Contact__r.Name, PhoneNumber__c
                        from VT_D1_Phone__c
                        where Type__c = 'Work'
                        and VTD1_Contact__c = :phoneDataObj.VTD1_Contact__r.Account.HealthCloudGA__CarePlan__r.VTD1_PI_contact__c
                ];
                if(phonePiWork.size()>0) {
                    message += ';\n PI '
                            + phonePiWork[0].VTD1_Contact__r.Name
                            + ': ' + phonePiWork[0].PhoneNumber__c + ';';
                }
            }
            Map<String, Id> phoneToContactMap = new Map<String, Id>();
            phoneToContactMap.put(phoneDataObj.PhoneNumber__c, phoneDataObj.VTD1_Contact__c);
            new VT_R2_McloudBroadcaster().sendMessage(phoneToContactMap, message, '' + System.now().getTime(), null);
        }
    }
}