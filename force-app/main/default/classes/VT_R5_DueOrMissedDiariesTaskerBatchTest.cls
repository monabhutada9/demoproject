/**
 * Created by Alexander Medentsov on 8/14/2020.
 */

@IsTest
public with sharing class VT_R5_DueOrMissedDiariesTaskerBatchTest {
    private class HerokuCalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            //System.debug(request.getBody());
            return new HttpResponse();
        }
    }
    private static final Id STUDY_ID = generateId(HealthCloudGA__CarePlanTemplate__c.SObjectType);
    private static final Datetime DUE_TIME1 = Datetime.now().addMinutes(-30);
    private static final Datetime DUE_TIME2 = Datetime.now().addMinutes(20);
    private static final Datetime DUE_TIME3 = Datetime.now().addMinutes(30);
    private static final Datetime DUE_TIME4 = Datetime.now().addMinutes(5);
    private static final Integer Offset1 = 15;
    private static final Integer Offset2 = 10;
    private static final Integer Offset3 = 12;
    private static final Integer Offset4 = 5;


    public static void testBatch(){
        applyBuildersStub();
        applyQueryLocator();

        Test.setMock(HttpCalloutMock.class, new HerokuCalloutMock());
        Test.startTest();
        {
            System.schedule(Crypto.getRandomInteger() + 'VT_R5_DueOrMissedDiariesTaskerBatch', '0 0 * * * ?',  new VT_R5_DueOrMissedDiariesTaskerBatch());
        }
        Test.stopTest();
    }

    private static void applyBuildersStub() {
        VTR5_eDiaryNotificationBuilder__c test1 = createBuilder('BEFORE the Expiration Date', 'test1', Offset1);
        VTR5_eDiaryNotificationBuilder__c test2 = createBuilder('BEFORE the Expiration Date', 'test2', Offset2);
        VTR5_eDiaryNotificationBuilder__c test3 = createBuilder('AFTER the Expiration Date', 'test3', Offset3);
        VTR5_eDiaryNotificationBuilder__c test4 = createBuilder('AFTER the Expiration Date', 'test4', Offset4);
        VT_Stubber.applyStub(
                'Notifications_Builder',
                new Map<Id, VTR5_eDiaryNotificationBuilder__c>{
                        test1.Id => test1,
                        test2.Id => test2,
                        test3.Id => test3,
                        test4.Id => test4
                }
        );
    }

    private static void applyQueryLocator(){
        List<VTD1_Survey__c> ediaries = new List<VTD1_Survey__c>();
        ediaries.add(createSurveys(DUE_TIME1, 'test1'));
        ediaries.add(createSurveys(DUE_TIME2, 'test2'));
        ediaries.add(createSurveys(DUE_TIME3, 'test3'));
        ediaries.add(createSurveys(DUE_TIME4, 'test4'));

        System.debug('RESULT EDIARY' + ediaries.get(0).VTR5_DiaryStudyKey__c);
        VT_Stubber.applyStub('eDiaryBuilder_QueryLocator', ediaries);
    }

    private static VTD1_Survey__c createSurveys(Datetime dueDate, String name){
                Map<String, Object> ediariesBuilder = new Map<String, Object> {
                        'VTR5_eCoaAlertsKey__c' => name + STUDY_ID + '*-*' + String.valueOf(dueDate)
                };
        Object builder = JSON.deserialize(JSON.serialize(ediariesBuilder), VTD1_Survey__c.class);
        return (VTD1_Survey__c) builder;
    }

    private static VTR5_eDiaryNotificationBuilder__c createBuilder(String builderType, String eDiaryName, Integer offset) {
        Map<String, Object> eDiaryNotificationBuilderMap = new Map<String, Object> {
                'Id' => generateId(VTR5_eDiaryNotificationBuilder__c.SObjectType),
                'VTR5_Study__c' => STUDY_ID,
                'VTR5_eDiaryName__c' => eDiaryName,
                'VTR5_Active__c' => true,
                'VTR5_Offset__c' => offset,
                'VTR5_RecipientType__c' => 'Site Staff',
                'VTR5_OperationType__c' => builderType,
                'VTR5_NotificationRecipient__c' => 'Site Coordinator',
                'VTR5_DiaryStudyKey__c' => eDiaryName + STUDY_ID

        };
        Object builder = JSON.deserialize(JSON.serialize(eDiaryNotificationBuilderMap), VTR5_eDiaryNotificationBuilder__c.class);
        return (VTR5_eDiaryNotificationBuilder__c) builder;
    }

    private static Id generateId(SObjectType sot) {
        return fflib_IDGenerator.generate(sot);
    }

}