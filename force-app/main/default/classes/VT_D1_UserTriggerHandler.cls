public with sharing class VT_D1_UserTriggerHandler extends Handler {
    private static Boolean hasBeforeRanOnce = false;
    private static Boolean hasAfterRanOnce = false;
    private static Boolean hasAfterSentEmail = false;
    private static Set<String> COMMUNITYPROFILE = new Set<String>{
            'Patient', 'Caregiver', 'Primary Investigator', 'Site Coordinator'
    };
    /*Added by IQVIA Strikers SH-10041 */
    private static final String STUDY_CONCIERGE = 'Study Concierge';
    private static final String ENGLISH = 'en_US';
    public static Set<Id> powerPartnersForWelcomeEmail = new Set<Id>();


    public override void onBeforeUpdate(TriggerContext context) {
        system.debug('ONBEFOREUPDATE');
        VT_D2_UserTriggerHelper.fillDeactivateFields((List<User>) context.newList, (Map<Id, User>) context.oldMap);
    }



    public static void processBefore(Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, List<User> oldList, List<User> newList, Map<Id, User> oldMap, Map<Id, User> newMap) {
        if (hasBeforeRanOnce) return;
        hasBeforeRanOnce = true;

        if (isInsert || isUpdate) {
            populateTextTimeZoneField(newList);
            handleActivatedPowerPartners(isUpdate, newList, oldMap);
            VT_D2_UserTriggerHelper.uncheckSendWelcomeEmail(newList);
        }
        if (isUpdate) {
            VT_D2_UserTriggerHelper.updateStudyPhoneNumberOnCase(newList, oldMap);
            // on VTR5_Create_Subject_in_eCOA check - populate eCOA GUIDs
            setCreateSubjectInEcoa(newList, oldMap);
            setClinroAndEproUserFields(newList,oldMap);
        }

        if (isInsert){
            /*Added by IQVIA Strikers SH-10041 */
            if (!VT_R4_PatientConversion.patientConversionInProgress) {
                updateScPrimaryLanguage(newList);
            }
        }


    }



    public static void processAfter(Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, List<User> oldList, List<User> newList, Map<Id, User> oldMap, Map<Id, User> newMap) {
        if (VT_R4_PatientConversion.patientConversionInProgress) {
            if (isInsert) {
                assignPermissionSets(newList);
                createUserAuditForEveryNewUser(newList);
            }
        } else {
        if (!hasAfterRanOnce) {
            hasAfterRanOnce = true;
            if (isUpdate) {
                VT_D2_UserTriggerHelper.updateVisitAndConferenceMembers(newList, oldMap);
                if(!VT_D1_ContactTriggerHandler.isLanguageUpdate){
                    updateContactLanguage(oldMap, newMap);
                }
            } else if (isInsert) {
                VT_D2_UserTriggerHelper.createSharesForPIUsers(newList);
                moveUsersToGroup(newList);
                List <Id> SCRUserIds = new List<Id>();
                for (User user : newList) {
                    if (user.VTD1_Profile_Name__c  == VT_R4_ConstantsHelper_ProfilesSTM.SCR_PROFILE_NAME) {
                        SCRUserIds.add(user.Id);
                    }
                }
                if (!SCRUserIds.isEmpty()) {
                    addSCRUsersToGroup(SCRUserIds);
                }
            }
        }

        if (isInsert) {
            assignPermissionSets(newList);
            /*To avoid code execution when called from batch class*/
            if(!System.isBatch()){
                VT_R5_SerResSkillAssignForSC.createServiceResourse(newList);
            }
            processNewCaregiversContacts(newList);
            welcomeEmailsForCommunityUsers(newList, null);
            sendWelcomeAfterCreateUser(newList, null);
            createUserAuditForEveryNewUser(newList);
//            insertUser(newList);
        }
        if (isUpdate) {
            /*To avoid code execution when called from batch class*/
            if(!System.isBatch()){
                VT_R5_SerResSkillAssignForSC.afterUpdateSerResSkillAssignment(newList,oldMap);
            }
            processChangedEmailOrProfile(newList, oldMap);
            if(!hasAfterSentEmail){
                welcomeEmailForRescueStudyUsers(newList,oldMap,newMap);
            }

            sendWelcomeAfterCreateUser(newList, oldMap);

            // processes creation and update for eCOA users
            VT_R5_eCoaUserUpdateHelper.processEcoaUpdate(newList, oldMap);

        }
    }
    }

    /**
     * @author Aliaksandr Vabishchevich
     * @date 18-Aug-2020
     * @description User becomes Activated (from mobile) and Study.VTR5_WhenToCreate_eCoaSubject="Activation" ->
     *   -> set for patient user VTR5_Create_Subject_in_eCOA__c = true
     * Called in UserTrigger.beforeUpdate. SH-14359
     * @param usersList - trigger.new
     * @param oldUsersMap - trigger.oldMap
     */
    private static void setCreateSubjectInEcoa(List<User> usersList, Map<Id, User> oldUsersMap) {
        System.debug('sf: in UserTrigger.setCreateSubjectInEcoa');
        Set<Id> updatedUserIds = new Set<Id>();
        
        for (User u : usersList) {
            if (u.VTR4_Is_Activated__c && !oldUsersMap.get(u.Id).VTR4_Is_Activated__c) {
                updatedUserIds.add(u.Id);
            }
        }
        System.debug('sf: updatedUserIds: ' + updatedUserIds);
        if (updatedUserIds.isEmpty()) return;

        List<Id> userIdsToProcess = new List<Id>(new Map<Id, User>([
                SELECT Id
                FROM User
                WHERE Id IN :updatedUserIds
                AND Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_WhenToCreate_eCoaSubject__c  = 'User Activation'
                AND Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.VTR5_eDiaryTool__c = 'eCOA'
                AND Profile.Name = :VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME

        ]).keySet());

        System.debug('sf: userIdsToProcess:' + userIdsToProcess);

        for (User u : usersList) {
            if (!userIdsToProcess.contains(u.Id)) continue;
            u.VTR5_Create_Subject_in_eCOA__c = true;
        }
    }

    /**
     * @author Aliaksandr Vabishchevich
     * @date 18-Aug-2020
     * @description When User.VTR5_Create_Subject_in_eCOA becomes true ->
     *   -> calculate GUIDs and Schedule Names from Study Rulesets and set them on User.
     * Called in UserTrigger.beforeUpdate. SH-14329
     * @param usersList - trigger.new
     * @param oldUsersMap - trigger.oldMap
     */
    @TestVisible
   private static void setClinroAndEproUserFields(List<User> usersList, Map<Id,User> oldUsersMap) {
        System.debug('sf: in UserTrigger.setClinroAndEproUserFields');
        Set<Id> updatedUserIds = new Set<Id>();
        Set<Id> subjectContactId = new Set<Id>();
        for (User u : usersList) {
            if (u.VTR5_Create_Subject_in_eCOA__c && !oldUsersMap.get(u.Id).VTR5_Create_Subject_in_eCOA__c) {
                updatedUserIds.add(u.Id);
                subjectContactId.add(u.contactId);
            }
        }
        System.debug('sf: updatedUserIds:' + updatedUserIds);
        if (updatedUserIds.isEmpty()) return;

        Map<Id, Case> casesByUserMap = new Map<Id, Case>();
        List<Case> cases = [
                SELECT Id, VTD1_Study__c,VTR5_SubsetImmuno__c, VTR5_SubsetSafety__c, VTD1_Patient_User__c
                FROM Case
                WHERE VTD1_Patient_User__c IN :updatedUserIds AND VTD1_Study__c != NULL
        ];
        Map<Id, List<VTR5_ePROClinRORuleset__c>> rulesetsByStudyMap = new Map<Id, List<VTR5_ePROClinRORuleset__c>>();
       
        for (Case c : cases) {
            casesByUserMap.put(c.VTD1_Patient_User__c, c);
            rulesetsByStudyMap.put(c.VTD1_Study__c, new List<VTR5_ePROClinRORuleset__c>());
        }
        
        List<VTR5_ePROClinRORuleset__c> rulesets = [
                SELECT VTR5_ProtocolNumber__c, VTR5_SubsetImmuno__c, VTR5_SubsetSafety__c,
                        VTR5_ePRO_GUID__c, VTR5_ClinRO_GUID__c, VTR5_ePRO_ScheduleName__c,
                         VTR5_ClinRO_ScheduleName__c,VTR5_State__c,VTR5_ProtocolNumber__r.VTR5_eCOA_Ruleset_Criteria__c
                FROM VTR5_ePROClinRORuleset__c
                WHERE VTR5_ProtocolNumber__c IN :rulesetsByStudyMap.keySet()
        ];
        Boolean isStateSpecificRulesets = false;
        for (VTR5_ePROClinRORuleset__c r : rulesets) {
            rulesetsByStudyMap.get(r.VTR5_ProtocolNumber__c).add(r);
            if(String.isNotBlank(r.VTR5_State__c)){
               isStateSpecificRulesets = true; 
            }
        }
        System.debug('sf: rulesetsByStudyMap:' + rulesetsByStudyMap);
        Map<String,String> userIdToStateMap = new Map<String,String>();
        if(isStateSpecificRulesets && !subjectContactId.isEmpty()){
            for( Address__c addr: [Select id ,State__c,VTD1_Contact__r.VTD1_UserId__c from Address__c 
                                   where VTD1_Contact__c =: subjectContactId]){
                    userIdToStateMap.put(addr.VTD1_Contact__r.VTD1_UserId__c,addr.State__c);
               }
        }
        for (User u: usersList) {
            Case c = casesByUserMap.get(u.Id);
            if (c == null) continue;

            for (VTR5_ePROClinRORuleset__c r : rulesetsByStudyMap.get(c.VTD1_Study__c)) {


                if(r.VTR5_ProtocolNumber__r.VTR5_eCOA_Ruleset_Criteria__c != null){
                    if(r.VTR5_ProtocolNumber__r.VTR5_eCOA_Ruleset_Criteria__c == 'Subset Immuno/Safety') {


                    if (c.VTR5_SubsetImmuno__c == r.VTR5_SubsetImmuno__c && c.VTR5_SubsetSafety__c == r.VTR5_SubsetSafety__c) {
                        u.VTR5_ePRO_GUID__c = r.VTR5_ePRO_GUID__c;
                        u.VTR5_ClinRO_GUID__c = r.VTR5_ClinRO_GUID__c;
                        u.VTR5_ePRO_ScheduleName__c = r.VTR5_ePRO_ScheduleName__c;
                        u.VTR5_ClinRO_ScheduleName__c = r.VTR5_ClinRO_ScheduleName__c;

                        System.debug('sf: ---\n\n\n SET GUIDS FOR U.ID ' + u.Id + '\n\n');
                    }
                }



                    else if(r.VTR5_ProtocolNumber__r.VTR5_eCOA_Ruleset_Criteria__c == 'States Address'  && userIdToStateMap.containsKey(u.Id) && r.VTR5_State__c != null  ){ 
                        if(r.VTR5_State__c == userIdToStateMap.get(u.Id)){
                            u.VTR5_ePRO_GUID__c = r.VTR5_ePRO_GUID__c;
                            u.VTR5_ePRO_ScheduleName__c = r.VTR5_ePRO_ScheduleName__c;
                        }
                    }
                }
            }
    }

    }
    //SH-4224
    private static void updateContactLanguage(Map<Id, User> oldMap, Map<Id, User> newMap) {
        VTD1_RTId__c rtIds = VTD1_RTId__c.getInstance();
        Map<Id, User> contactMap = new Map<Id, User>();
        for (Id userId : newMap.keySet()) {
            if (oldMap.get(userId).LanguageLocaleKey != newMap.get(userId).LanguageLocaleKey ||
                    ((newMap.get(userId).ProfileId == rtIds.VTD2_Profile_Caregiver__c || newMap.get(userId).ProfileId == rtIds.VTD2_Profile_Patient__c) &&

                            (oldMap.get(userId).VT_R5_Secondary_Preferred_Language__c != newMap.get(userId).VT_R5_Secondary_Preferred_Language__c  || oldMap.get(userId).VT_R5_Tertiary_Preferred_Language__c != newMap.get(userId).VT_R5_Tertiary_Preferred_Language__c))){
                contactMap.put(newMap.get(userId).contactId, newMap.get(userId));
            }
        }
        List<Contact> contactList = [SELECT Id, VTR2_Primary_Language__c, VTD1_UserId__c, VT_R5_Secondary_Preferred_Language__c, VT_R5_Tertiary_Preferred_Language__c FROM Contact WHERE Id IN :contactMap.keySet()];
        if (contactList != null && !contactList .isEmpty()){
            for (Contact con : contactList){
                con.VTR2_Primary_Language__c = processBadLanguage(contactMap.get(con.id).LanguageLocaleKey);
                // SH-9185 - Start - reflect Secondary and Tertiary Preferred Language on contact record. 
                con.VT_R5_Secondary_Preferred_Language__c = processBadLanguage(contactMap.get(con.id).VT_R5_Secondary_Preferred_Language__c);
                con.VT_R5_Tertiary_Preferred_Language__c = processBadLanguage(contactMap.get(con.id).VT_R5_Tertiary_Preferred_Language__c);
                // SH-9185 - End
            }
            VT_D1_ContactTriggerHandler.isLanguageUpdate = true;
            update contactList;
        }
    }

    /**
     * @author Vlad Tyazhov
     * @date 20-Aug-2020
     * @description SH-14355 - dirty custom solution to avoid errors while updating User language to Zulu, Xhosa:
     * INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST, Preferred Language: bad value for restricted picklist field: 208
     * @param rawLang - Contact.LanguageLocaleKey
     * @return - correct language
     */
    private static String processBadLanguage(String rawLang) {
        Map<String, String> langIdToCode = new Map<String, String>{
                '206' => 'af',
                '207' => 'zu',
                '208' => 'xh'
        };
        return langIdToCode.containsKey(rawLang) ? langIdToCode.get(rawLang) : rawLang;
    }

    public static void populateTextTimeZoneField(List<User> users) {
        for (User u : users) {
            u.VTD2_UserTimezone__c = u.TimeZoneSidKey;
        }
    }

    // SH-9185 - mapping additional Language fields to reflect Secondary and Tertiary Preferred Language on user record.       

    private static void processNewCaregiversContacts(List<User> users) {
        List<Id> conIds = new List<Id>();
        Id contactProfileId = VTD1_RTId__c.getInstance().VTD2_Profile_Caregiver__c;
        for (User item : users) {
            if (item.ContactId != null && item.ProfileId == contactProfileId) {
                conIds.add(item.ContactId);
            }
        }
        if (!conIds.isEmpty()) {
            Map <Id, User> userMap = new Map<Id, User>([select Id, LanguageLocaleKey, VT_R5_Secondary_Preferred_Language__c, VT_R5_Tertiary_Preferred_Language__c from User where Id in : users]);
            Map <Id, Contact> contactMap = new Map<Id, Contact>([select Id, VTR2_Primary_Language__c, VT_R5_Secondary_Preferred_Language__c, VT_R5_Tertiary_Preferred_Language__c from Contact where Id in : conIds]);
            System.debug('contactMap = ' + contactMap);
            List<User> usersToUpdate = new List<User>();
            Boolean isLangDiff;
            for(User user : users){
                User userCopy = userMap.get(user.Id);
                Contact contact = contactMap.get(user.ContactId);
                isLangDiff = false;
                if (contact != null){
                    if(userCopy.LanguageLocaleKey != contact.VTR2_Primary_Language__c) {
                        userCopy.LanguageLocaleKey = contact.VTR2_Primary_Language__c;
                        isLangDiff = true;
                    }
                    // SH-9185 -- start
                    if(userCopy.VT_R5_Secondary_Preferred_Language__c != contact.VT_R5_Secondary_Preferred_Language__c) {
                        userCopy.VT_R5_Secondary_Preferred_Language__c = contact.VT_R5_Secondary_Preferred_Language__c;
                        isLangDiff = true;
                    }
                    if(userCopy.VT_R5_Tertiary_Preferred_Language__c != contact.VT_R5_Tertiary_Preferred_Language__c ) {
                        userCopy.VT_R5_Tertiary_Preferred_Language__c  = contact.VT_R5_Tertiary_Preferred_Language__c ;
                        isLangDiff = true;
                    }
                    // SH-9185 -- end
                    if(isLangDiff){
                        usersToUpdate.add(userCopy);
                    }
                }

            }

            if (!usersToUpdate.isEmpty()) {
                update usersToUpdate;
            }

            MemberCreator memberCreator = new MemberCreator(conIds);
            if (Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()) {
                System.enqueueJob(memberCreator);
            } else {
                memberCreator.execute(null);
            }



        }
    }

    public class MemberCreator implements Queueable {
        private List<Id> conIds;

        public MemberCreator(List<Id> conIds) {
            this.conIds = conIds;
        }

        public void execute(QueueableContext context) {
            VT_R2_ProcessNewPrimaryCGContacts.processContacts([
                    SELECT Id, RecordTypeId, VTD1_Primary_CG__c
                    FROM Contact
                    WHERE Id IN :conIds
                    AND RecordType.DeveloperName = 'Caregiver'
                    AND VTD1_Primary_CG__c = true
            ], null);
        }
    }

    private static void processChangedEmailOrProfile(List<User> users, Map<Id, User> oldMap) {
        List<Id> userIds = new List<Id>();
        for (User item : users) {
            if (item.Email != oldMap.get(item.Id).Email || item.ProfileId != oldMap.get(item.Id).ProfileId) {
                userIds.add(item.Id);
            }
        }

        if (!userIds.isEmpty()) {
            VisitMemberUpdater visitMemberUpdater = new VisitMemberUpdater(userIds);
            if (Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()) {
                System.enqueueJob(visitMemberUpdater);
            } else {
                visitMemberUpdater.execute(null);


            }

        }


    }


    public class VisitMemberUpdater implements Queueable {
        private List<Id> userIds;

        public VisitMemberUpdater(List<Id> userIds) {
            this.userIds = userIds;
        }

        public void execute(QueueableContext context) {
            List<Visit_Member__c> members = [
                    SELECT Id, VTD1_Participant_User__c
                    FROM Visit_Member__c
                    WHERE VTD1_Participant_User__c IN :this.userIds
            ];
            new VT_R2_VisitMemberTriggerHandler().setEmailsAndParticipantTypes(members, null);
            update members;
        }
    }

    /**
     * @author Aliaksandr Vabishchevich
     * @description - Create new User Audit record for new user, except user profile Patient and Caregiver
     * @param newUsers
     */
    public static void createUserAuditForEveryNewUser(List<User> newUsers) {
        List<Id> userIds = new List<Id>();

        Set<Id> patientCaregiverProfilesIds = new Set<Id> {VTD1_RTId__c.getInstance().VTD2_Profile_Caregiver__c, VTD1_RTId__c.getInstance().VTD2_Profile_Patient__c};
        System.debug(patientCaregiverProfilesIds);
        for (User u : newUsers) {
            System.debug('User Audit ' + u.Id + ' Profile ' + u.ProfileId);
            System.debug('User Audit condition ' + (u.IsActive && !VT_R4_ConstantsHelper_ProfilesSTM.HIPAA_PROFILES.contains(u.Profile.Name)));
            if (u.IsActive && !patientCaregiverProfilesIds.contains(u.ProfileId)) {
                userIds.add(u.Id);
            }
        }
        System.debug(userIds);

        if (!userIds.isEmpty()) {
            UserAuditCreation ua = new UserAuditCreation(userIds);
            if (Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()) {
                System.enqueueJob(ua);
            } else {
                ua.execute(null);
            }
        }
    }

    public class UserAuditCreation implements Queueable {
        private List<Id> userIds;
        public UserAuditCreation(List<Id> usersIds) {
            this.userIds = usersIds;
        }
        public void execute(QueueableContext context) {
            List<VTR5_UserAudit__c> userAuditsToCreate = new List<VTR5_UserAudit__c>();
            for (Id userId : userIds) {
                    VTR5_UserAudit__c ua = new VTR5_UserAudit__c();
                    ua.User__c = userId;
                    userAuditsToCreate.add(ua);
                }
            insert userAuditsToCreate;
        }
    }

    @future
    public static void addSCRUsersToGroup(List <Id> SCRUserIds) {
        System.debug('add SCR User to group ' + SCRUserIds);
        List <Group> SCRGroups = [select Id from Group where Name = 'SCR_Users' limit 1];
        if (SCRGroups.isEmpty()) {
            System.debug('There is no Group with name SCR_Users !');
            return;
        }
        List <GroupMember> groupMembers = new List<GroupMember>();
        for (Id userId : SCRUserIds) {
            GroupMember groupMember = new GroupMember(GroupId = SCRGroups[0].Id, UserOrGroupId = userId);
            groupMembers.add(groupMember);
        }
        insert groupMembers;
    }

    //    Bobich Viktar 05.02.2020
    //    Test coverage in VT_R4_MoveUsersToGroupBatch_Test
    public static void moveUsersToGroup(List<User> userList){
        List <GroupMember> groupMemberList = new List<GroupMember>();
        Map<String, Id> groupNameIdMap = new Map<String, Id>();
        String groupPrefix = ' Users';
        List<Group> groupList = new List<Group>();
        String profileName;
        Boolean hasPortalUser = false;
        for(User user : userList){
            if(user.VTR4_Profile_UserType__c == 'Standard'){
                hasPortalUser = true;
                break;
            }
        }
        if(hasPortalUser) {
            groupList = [SELECT Id, Name FROM Group WHERE Type = 'Regular'];
            for (Group gr : groupList) {
                groupNameIdMap.put(gr.Name, gr.Id);
            }
            for (User user : userList) {
                if (user.VTR4_Profile_UserType__c == 'Standard') {
                    profileName = user.VTD1_Profile_Name__c == 'PT1' ? 'System Administrator' : user.VTD1_Profile_Name__c;
                    if (groupNameIdMap.keySet().contains(profileName + groupPrefix)) {
                        groupMemberList.add(new GroupMember(GroupId = groupNameIdMap.get(profileName + groupPrefix), UserOrGroupId = user.Id));
                    }
                }
            }
            if(!Test.isRunningTest()){
                insert groupMemberList;
            }
        }
    }

    public static void sendWelcomeAfterCreateUser(List<User> users, map<Id, User> oldMap) {
        List<User> usersToProcess = new List<User>();
        for (User u : users) {
            if(u.IsActive && u.VTR2_WelcomeEmailSent__c){
                if(u.UserType == 'Standard' && (oldMap == null || !oldMap.get(u.Id).IsActive)){
                    usersToProcess.add(u.clone(true, true, false, false));
                }
            }
        }
        if (!usersToProcess.isEmpty()) {
            System.debug('inside');
            List<EmailTemplate> emailTemplateList = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'VT_D2_Registration_Welcome_Template' LIMIT 1];
            if (!emailTemplateList.isEmpty()) {
                List<OrgWideEmailAddress> orgWideEmailAddresses = [SELECT Id From OrgWideEmailAddress ORDER BY CreatedDate ASC LIMIT 1];
                List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                for (User u : usersToProcess) {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(u.Id);
                    mail.setUseSignature(false);
                    if (!orgWideEmailAddresses.isEmpty()) {
                        mail.setOrgWideEmailAddressId(orgWideEmailAddresses[0].Id);
                    }
                    mail.setBccSender(false);
                    mail.setSaveAsActivity(false);
                    mail.setTemplateId(emailTemplateList[0].Id);
                    mail.setWhatId(u.Id);
                    mails.add(mail);
                    u.VTR2_WelcomeEmailSent__c = false;
                    if(oldMap != null && u.IsActive && oldMap.get(u.Id).IsActive != u.IsActive ){
                        system.resetPassword(u.Id, true);
                    }
                }
                Messaging.sendEmail(mails);


            }
            update usersToProcess;
        }
    }

    //    Olya Baranova 08.11.2019
    public static void welcomeEmailsForCommunityUsers(List<User> users, Map<Id, User> oldMap) {
        List<Id> communityUserIds = new List<Id>();
        for (User usr : users) {
            User oldUser;
            if(oldMap !=null){
                oldUser = oldMap.get(usr.Id);
            }

            if (usr.UserType != 'Standard' && usr.UserType != 'PowerPartner' && (!usr.VTR4_Delay_activation_emails_for_PT_CG__c || (oldMap != null && oldUser != null && usr.IsActive && usr.IsActive != oldUser.IsActive))) {
                System.debug('inside in active');

                communityUserIds.add(usr.Id);
            }
        }
        if(!powerPartnersForWelcomeEmail.isEmpty()){
            communityUserIds.addAll(powerPartnersForWelcomeEmail);
            powerPartnersForWelcomeEmail = new Set<Id>();
        }

        if (!communityUserIds.isEmpty()) {
            new VT_R3_PublishedAction_ResetCommunityPass(communityUserIds).publish();
        }
    }


    public static void handleActivatedPowerPartners(Boolean isUpdate, List<User> users, Map<Id, User> oldMap) {
        if(isUpdate){ //needs to be run before "uncheckSendWelcomeEmail"
            Set<Id> usersToCheckSTM = new Set<Id>();
            Map<Id, User> userMap = new Map<Id, User>(users);
            for (User u : users) {
                if (!u.isActive && u.VTR5_ActivationEmailSent__c){
                    u.VTR5_ActivationEmailSent__c = false;
                }
                if(u.UserType == 'PowerPartner' && u.isActive && oldMap != null && (!oldMap.get(u.Id).isActive || u.VTR2_WelcomeEmailSent__c)) {
                    usersToCheckSTM.add(u.Id);
                }
            }
            if(!usersToCheckSTM.isEmpty()){
                for(Study_Team_Member__c stm : [Select User__c from Study_Team_Member__c where User__c in: usersToCheckSTM and VTD1_Active__c = true]){
                    powerPartnersForWelcomeEmail.add(stm.User__c);
                    userMap.get(stm.User__c).VTR5_ActivationEmailSent__c = true;
                }
            }
        }
    }


    public static void welcomeEmailForRescueStudyUsers(List<User> lstNewUsers, Map<Id, User> oldMap, Map<Id, User> newMap){
        //set<Id> setUserId = new set<Id>();
        List<User> lstUser = new List<User>();
        //set<Id> setAllowdRecordTyeId = new set<Id>();
        for(User user: lstNewUsers){
            User oldUser = oldMap.get(user.Id);
            if(user.IsActive != oldUser.IsActive && user.IsActive){
                //setUserId.add(user.Id);
                lstUser.add(user);

            }

        }
        /*if(!setUserId.isEmpty()){
            Schema.DescribeSObjectResult d = Schema.SObjectType.Study_Team_Member__c; 
            Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
            setAllowdRecordTyeId.add(rtMapByName.get('PI').getRecordTypeId());
            setAllowdRecordTyeId.add(rtMapByName.get('SCR').getRecordTypeId());
            for(Study_Team_Member__c teamMemer: [SELECT Id, User__c, Study__r.VT_R4_RescueTelVisitStudy__c FROM Study_Team_member__c WHERE User__c IN : setUserId AND RecordType.ID IN : setAllowdRecordTyeId]){
                if(teamMemer.Study__r != null && teamMemer.Study__r.VT_R4_RescueTelVisitStudy__c){
                    lstUser.add(newMap.get(teamMemer.User__c));
                    system.resetPassword(teamMemer.User__c, true);
                }
            }            
        }*/
        hasAfterSentEmail = true;
        if(!lstUser.isEmpty() || !powerPartnersForWelcomeEmail.isEmpty()){
            welcomeEmailsForCommunityUsers(lstUser, oldMap);
        }
    }



    /*Added by IQVIA Strikers SH-10041 for update primary chat language */
    public static void updateScPrimaryLanguage(List<User> listOfUser){
        for(User u : listOfUser){
            if(STUDY_CONCIERGE.equals(u.VTD1_Profile_Name__c) && String.isBlank(u.VT_R5_Primary_Preferred_Language__c)){
                u.VT_R5_Primary_Preferred_Language__c = ENGLISH;
            }
        }
    }
    /*Added by IQVIA Strikers SH-10041 to by few pass methods of trigger */
    public static void byPassUserTrigger(){
        hasAfterRanOnce = true;
        hasBeforeRanOnce = true;
    }

    public static void assignPermissionSets(List<User> newList) {
        Set<Id> userIds = new Set<Id>();
        for (User u : newList) {
            userIds.add(u.Id);
        }
        if (!userIds.isEmpty()) {
            new VT_R5_AssignApexClassesAccessPermission(userIds).publish();
        }
    }

}