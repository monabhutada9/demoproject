/**
* @author: Olga Baranova
* @date: 07-feb-2020
* need to delete this class
**/

global without sharing class VT_R4_PIISharingRemovalBatch implements Database.Batchable<SObject>, Database.Stateful  {
    public static final List<String> SHARE_NAMES = new List<String>{
            'AccountShare',
            'CaseShare',
            'UserShare'
    };
    public List<String> shareNames;

    global Database.QueryLocator start(Database.BatchableContext bc) {
        if (shareNames == null) { shareNames = SHARE_NAMES.clone(); }
        String shareName = shareNames.remove(0);
        List<String> profiles = new List<String>{
                'Monitoring Report Reviewer',
                'Project Lead',
                'Virtual Trial Study Lead'};
        List<String> docRecordTypes = new List<String>{
                'VTR2_Note_of_Transfer',
                'VTD1_Regulatory_Document',
                'VTD1_Regulatory_Document_Locked',
                'Regulatory_Document_Rejected',
                'VTR2_Source_Note_of_Transfer',
                'VTR2_Target_Note_of_Transfer'};

        String query = 'SELECT Id FROM ' + shareName + ' WHERE (' +
                'UserOrGroup.Profile.Name = \'Regulatory Specialist\'';
        if (shareName != 'VTD1_Protocol_Deviation__Share') {
            query += ' OR UserOrGroup.Profile.Name = \'Project Management Analyst\'';
            if (shareName != 'VTD1_Monitoring_Visit__Share') {
                query += ' OR UserOrGroup.Profile.Name IN : profiles';
            }
        }   query += ') ';

        if (shareName == 'VTD1_Document__Share') {
            query += 'AND Parent.RecordType.DeveloperName NOT IN : docRecordTypes';
        }
        else if (shareName == 'UserShare') {
            query += 'AND (User.Profile.Name = \'Patient\' OR User.Profile.Name = \'Caregiver\')';
        }
        else if (shareName == 'AccountShare') {
            query += 'AND (Account.VTD2_Patient_s_Contact__r.VTD1_UserId__r.Profile.Name = \'Patient\' OR Account.VTD2_Patient_s_Contact__r.VTD1_UserId__r.Profile.Name = \'Caregiver\')';
        }
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<SObject> scope){
        Database.delete(scope, false);
    }

    global void finish(Database.BatchableContext bc){
        if (! shareNames.isEmpty()) {
            launchNextBatch();
        }

    }

    private void launchNextBatch() {
        VT_R4_PIISharingRemovalBatch nextIteration = new VT_R4_PIISharingRemovalBatch();
        nextIteration.shareNames = shareNames;
        Database.executeBatch(nextIteration);
    }

}