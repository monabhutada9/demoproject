/**
 * Created by shume on 23/10/2018.
 */

@IsTest
public class VT_D1_Test_PatientCaregiverBound {

    public static void test() {
        Case patientCase = [SELECT Id, VTD1_Patient_User__c, AccountId FROM Case WHERE RecordType.DeveloperName = 'CarePlan' LIMIT 1];
        User patientUser = [SELECT Id, ContactId FROM User WHERE id =: patientCase.VTD1_Patient_User__c LIMIT 1];
        User caregiverUser = [SELECT Id, AccountId FROM User WHERE AccountId =:patientCase.AccountId AND VTD1_Profile_Name__c = 'Caregiver' LIMIT 1];
        Test.startTest();
        System.runAs((User) caregiverUser) {
            VT_D1_PatientCaregiverBound.getCaseForUser();

            VT_D1_PatientCaregiverBound.getPatientId();
        }


        System.runAs(patientUser) {
            System.assertNotEquals(null, VT_D1_PatientCaregiverBound.getPatientId());
            VT_D1_PatientCaregiverBound.getCaseForUser();

        }

        System.assertEquals('Patient', VT_D1_PatientCaregiverBound.getUserProfileName(patientUser.id));
        System.assertNotEquals(null, VT_D1_PatientCaregiverBound.findPatientOfCaregiver(caregiverUser.Id));
        System.assertNotEquals(null, VT_D1_PatientCaregiverBound.findCaregiverOfPatient(patientUser.id));
        VT_D1_PatientCaregiverBound.getPatientCase(patientUser.id);

        DomainObjects.User_t piUser = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PI_PROFILE_NAME);
        piUser.persist();
        VT_D1_PatientCaregiverBound.getPatientCase(piUser.id);


        Test.stopTest();
    }
}