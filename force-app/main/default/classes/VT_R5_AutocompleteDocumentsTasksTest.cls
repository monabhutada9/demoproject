/**
 * Created by Denis Belkovskii on 9/7/2020.
 */ 

@IsTest
public class VT_R5_AutocompleteDocumentsTasksTest{

    @TestSetup
    static void setup() {
        /*Close related tasks - PI Closeout Submiss ion complete ---- prepare test data*/
        DomainObjects.VirtualSite_t domainVirtualSite = new DomainObjects.VirtualSite_t()
                .addStudy(new DomainObjects.Study_t());
        Virtual_Site__c testVc = (Virtual_Site__c) domainVirtualSite.persist();
        DomainObjects.VTD1_Document_t domainDocumentT = new DomainObjects.VTD1_Document_t()
                .setRecordTypeId(Schema.SobjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD1_Regulatory_Document').getRecordTypeId())
                .setVirtualSite(testVc.Id) 
                .addRegBinder(new DomainObjects.VTD1_Regulatory_Binder_t()
                        .addStudy(new DomainObjects.Study_t())
                        .addRegDocument(new DomainObjects.VTD1_Regulatory_Document_t()
                                .setDocumentType('PI Closeout IRB Submission')))
                .setStatus('Pending Certification')
                .setVTR3_Category_add('Uncategorized');
        VTD1_Document__c doc = (VTD1_Document__c) domainDocumentT.persist();
        new DomainObjects.Task_t()
                .setVirtualSiteId(testVc.Id)
                .setStatus('Open')
                .setVTD2_Task_Unique_Code('006')
                .persist();
        /*Certification signed --- prepare test data*/
        new DomainObjects.Task_t()
                .setWhatId(doc.Id)
                .setStatus('Open')
                .setVTD2_Task_Unique_Code('T534')
                .persist();
        /*Regulatory Document is Certified --- prepare test data*/
        DomainObjects.VTD1_Document_t domainRegDocCertifiedDoc = new DomainObjects.VTD1_Document_t();
        domainRegDocCertifiedDoc.setStatus('Pending Certification')
                .setRecordTypeId(Schema.SobjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('VTD2_Certification').getRecordTypeId())
                .setVTD1_Current_Workflow('Medical Records flow')
                .setVTD2_Associated_Medical_Record_id(doc.Id);
        VTD1_Document__c regDocCertifiedDoc = (VTD1_Document__c) domainRegDocCertifiedDoc.persist();
        new DomainObjects.Task_t()
                .setWhatId(doc.Id)
                .setStatus('Open') 
                .setVTD2_Task_Unique_Code('T612')
                .persist();
        /*Change categories --- prepare test data*/
        new DomainObjects.Task_t()
                .setWhatId(doc.Id)
                .setStatus('Open')
                .setVTD2_Task_Unique_Code('T533')
                .persist();
        /*Medical Record is Approved --- prepare test data*/
        DomainObjects.VTD1_Document_t domainMedicalRecordApprovedDoc = new DomainObjects.VTD1_Document_t();
        domainMedicalRecordApprovedDoc
                .setRecordTypeId(Schema.SObjectType.VTD1_Document__c.getRecordTypeInfosByDeveloperName().get('Medical_Record_Release_Form').getRecordTypeId())
                .setVTD1_Current_Workflow('Medical Records flow');
        VTD1_Document__c medicalRecordApprovedDoc = (VTD1_Document__c) domainMedicalRecordApprovedDoc.persist();
        new DomainObjects.Task_t()
                .setWhatId(medicalRecordApprovedDoc.Id)
                .setStatus('Open')
                .setVTD2_Task_Unique_Code('001')
                .persist();
    }

    @isTest
    static void testProcess() {
        /*Close related tasks - PI Closeout Submission complete ---- test handler/flow*/
        VTD1_Document__c doc = [
                SELECT VTD1_Status__c, RecordTypeId FROM VTD1_Document__c
                WHERE VTD1_Regulatory_Document_Type__c = 'PI Closeout IRB Submission'
        ];
        doc.VTD1_Status__c =  'TMF Started';
        doc.VTD1_Files_have_not_been_edited__c = true;
        doc.RecordTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT_LOCKED;
        update doc;
        Task tsk = [SELECT Status, WhatId FROM Task WHERE VTD2_Task_Unique_Code__c = '006'];
        System.assertEquals('Completed', tsk.Status);
        /*Certification signed --- test handler/flow*/
        Task tsk1 = [SELECT Status, WhatId FROM Task WHERE VTD2_Task_Unique_Code__c = 'T534'];
        System.assertEquals('Completed', tsk1.Status);
        /*Regulatory Document is Certified --- handler/flow*/
        VTD1_Document__c doc2 = [
                SELECT RecordType.DeveloperName, VTD1_Status__c
                FROM VTD1_Document__c
                WHERE RecordType.DeveloperName = 'VTD2_Certification'
        ];
        doc2.VTD1_Status__c = 'Certified';
        update doc2;
        Task tsk2 = [SELECT Status, WhatId FROM Task WHERE VTD2_Task_Unique_Code__c = 'T612'];
        System.assertEquals('Completed', tsk2.Status);
        /*Change categories --- test handler/flow*/
        Task tsk3 = [SELECT Status, WhatId FROM Task WHERE VTD2_Task_Unique_Code__c = 'T533'];
        System.assertEquals('Completed', tsk3.Status);
        /*Medical Record is Approved --- test handler/flow*/
        VTD1_Document__c doc3 = [SELECT VTD1_Status__c FROM VTD1_Document__c WHERE RecordType.DeveloperName = 'Medical_Record_Release_Form'];
        doc3.VTD1_Status__c = VT_R4_ConstantsHelper_Documents.DOCUMENT_APPROVED;
        update doc3;
        Task tsk4 = [SELECT Status FROM Task WHERE VTD2_Task_Unique_Code__c = '001'];
        System.assertEquals('Completed', tsk4.Status);
    }
}