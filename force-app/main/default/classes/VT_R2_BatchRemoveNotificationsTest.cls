/**
 * @author Ruslan Mullayanov
 * @date 2019-05-20
 * @see VT_R2_BatchRemoveNotifications
 */
@IsTest
private class VT_R2_BatchRemoveNotificationsTest {
    @TestSetup
    static void testSetup(){
        VT_R3_GlobalSharing.disableForTest = true;
        Test.startTest();
        DomainObjects.Account_t account = new DomainObjects.Account_t().setRecordTypeByName('Sponsor');
        DomainObjects.Study_t domainStudy = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(account);
        HealthCloudGA__CarePlanTemplate__c study = (HealthCloudGA__CarePlanTemplate__c) domainStudy.persist();
        study.VTD1_Randomized__c = 'No';
        update study;

        VTR3_Study_Notification__c studyNotification = (VTR3_Study_Notification__c) new DomainObjects.VTR3_Study_Notification_t()
                .setVTR3_Notification_Recipient('Patient')
                .setVTR3_Event_Type(0)
                .setActive(true)
                .setEventTime(null)
                .setVTR3_Event_Type('Patient Life Cycle status')
                .setTarget_Status('Consented')
                .setStudy(study.Id)
                .setNumberOfOccurrences(3)
                .persist();
        List<VTD1_NotificationC__c> ls = new List<VTD1_NotificationC__c>();

        User patientUser = (User) new DomainObjects.User_t()
                .setTimezone(UserInfo.getTimeZone())
                .setUserTimezone(UserInfo.getTimeZone())
                .setProfile('Patient')
                .addContact(new DomainObjects.Contact_t()
                        .addAccount(account)
                )
                .persist();
        String receiverId = String.valueOf(patientUser.Id).left(15);
        insert new VTD1_RTId__c(VTR2_Backend_Logic_User_ID__c = receiverId);

        VTD1_NotificationC__c notification1 = new VTD1_NotificationC__c(
                VTR3_Study_Notification__c = studyNotification.Id,
                VTD1_Receivers__c = receiverId,
                Type__c = 'Messages',
                Number_of_unread_messages__c = 3,
                VTD1_Read__c = true,
                VTD2_New_Message_Added_DateTme__c = Datetime.now(),
                Message__c = 'Test notification message ' + 1,
                VTR3_Recurrence_Number__c = 3
        );
        ls.add(notification1);

        VTD1_NotificationC__c notification2 = new VTD1_NotificationC__c(
                VTR3_Study_Notification__c = studyNotification.Id,
                VTD1_Receivers__c = receiverId,
                Type__c = 'Messages',
                Number_of_unread_messages__c = 2,
                VTD1_Read__c = true,
                VTD2_New_Message_Added_DateTme__c = Datetime.now(),
                Message__c = 'Test notification message ' + 2,
                VTR3_Recurrence_Number__c = 5
        );
        ls.add(notification2);

        VTD1_NotificationC__c notification3 = new VTD1_NotificationC__c(
                VTR3_Study_Notification__c = studyNotification.Id,
                VTD1_Receivers__c = receiverId,
                Type__c = 'Messages',
                Number_of_unread_messages__c = 2,
                VTD1_Read__c = true,
                VTD2_New_Message_Added_DateTme__c = Datetime.now(),
                Message__c = 'Test notification message ' + 3,
                VTR3_Recurrence_Number__c = 1
        );
        ls.add(notification3);
        insert ls;

        Test.stopTest();
    }

    @IsTest
    static void scheduleTest(){
//        configureDependantCustomSettings();

        new DomainObjects.VTD1_NotificationC_t()
                .persist();
        Test.startTest();
        List<CronTrigger> ct = [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name = 'VT_R2_BatchRemoveNotifications'];
        if (!ct.isEmpty()) { System.abortJob(ct[0].Id); }
        VT_R2_BatchRemoveNotifications.scheduleDaily();
        VT_R2_BatchRemoveNotifications.scheduleWeekly();
//        VT_R2_BatchRemoveNotifications b = new VT_R2_BatchRemoveNotifications();
//        System.schedule('VT_R2_BatchRemoveNotifications test', '0 0 0 * * ?', b);
        Test.stopTest();
    }

    @IsTest
    static void testFindTechnicalNotifications() {
        List<VTD1_NotificationC__c> ls = [SELECT Id, VTR3_Study_Notification__c,
                VTR3_Recurrence_Number__c,
                VTR3_Study_Notification__r.VTR3_Number_of_Occurrences__c,
                VTD1_Receivers__c, CreatedDate
        FROM VTD1_NotificationC__c];
        Test.startTest();
        VT_R2_BatchRemoveNotifications.OldStudyNotificationsCleanService service
                = new VT_R2_BatchRemoveNotifications.OldStudyNotificationsCleanService(ls);
        service.findTechnicalNotifications(ls);
        Test.stopTest();
        Boolean checkTest;
        for (VTD1_NotificationC__c obj : service.notificationsToDelete){
            if (obj.VTR3_Recurrence_Number__c == 1){
                checkTest = true;
            }
        }
        System.assertEquals(true, checkTest);
    }

    @IsTest
    static void testCompareTo() {
        List<VTD1_NotificationC__c> ls = [SELECT Id, VTR3_Recurrence_Number__c, VTR3_Study_Notification__r.VTR3_Number_of_Occurrences__c FROM VTD1_NotificationC__c];
        Test.startTest();
        List<VT_R2_BatchRemoveNotifications.NotificationC> sortNotificationWrapper = new List<VT_R2_BatchRemoveNotifications.NotificationC>();
        for (VTD1_NotificationC__c step: ls){
            sortNotificationWrapper.add(new VT_R2_BatchRemoveNotifications.NotificationC(step));
        }
        sortNotificationWrapper.sort();
        Test.stopTest();
        System.assertEquals(5, sortNotificationWrapper.get(0).notification.VTR3_Recurrence_Number__c);
    }

    @IsTest
    static void testBatch() {
//        configureDependantCustomSettings();
        Test.startTest();
        Database.executeBatch(new VT_R2_BatchRemoveNotifications());
        Test.stopTest();
        List<VTD1_NotificationC__c> notificationCS = [SELECT Id, Message__c FROM VTD1_NotificationC__c];
        System.assertEquals(1, notificationCS.size());
        System.assertEquals('Test notification message ' + 2, notificationCS[0].Message__c);
    }

//    private static void configureDependantCustomSettings() {
//        Id userId = VTD1_RTId__c.getInstance().VTR2_Backend_Logic_User_ID__c;
//        if (userId == null) {
//            User user = [SELECT Id FROM User WHERE Id != :UserInfo.getUserId() AND Profile.Name = 'System Administrator' AND IsActive = TRUE LIMIT 1];
//            insert new VTD1_RTId__c(VTR2_Backend_Logic_User_ID__c = String.valueOf(user.Id).left(15));
//        }
//        System.debug('!!!!: ' + userId);
//    }
}