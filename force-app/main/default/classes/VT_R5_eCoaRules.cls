/**
* @author: Carl Judge
* @date: 27-May-20
* @description: 
**/

public class VT_R5_eCoaRules {
    /**
     * Get rule info for a study from eCOA. Needed for other operations
     * @param studyGuid - eCOA guid of the study
     * @return the rule guid for the study
     */
    public static VT_D1_HTTPConnectionHandler.Result getStudyRules(String studyGuid) {
        String endpoint = '/api/v1/orgs/:orgGuid/studies/:studyGuid/rulesets';
        String method = 'GET';
        String action = 'eCOA Get Study Rules';

        return new VT_R5_eCoaApiRequest()
                .setStudyGuid(studyGuid)
                .setEndpoint(endpoint)
                .setMethod(method)
                .setAction(action)
                .setSkipLogging(true)
                .send();
    }
}