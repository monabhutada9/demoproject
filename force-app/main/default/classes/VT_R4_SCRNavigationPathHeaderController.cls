public class VT_R4_SCRNavigationPathHeaderController {
    @AuraEnabled
    (cacheable=true)
    public static VTD1_Document__c getCaseIdByDocumentId(String documentIdParam) {
        if (documentIdParam != null){
           List<VTD1_Document__c> docs = [SELECT VTD1_Clinical_Study_Membership__c, RecordType.DeveloperName
                                          FROM VTD1_Document__c
                                          WHERE Id = :documentIdParam];
           if (!docs.isEmpty()){
               return docs[0];
           }
        }
        return null;
    }
    @AuraEnabled (cacheable=true)
    public static String getCaseIdByTreatmentArmId(String treatmentArmId) {
        String result;
        List<String> urlSplit = treatmentArmId.split('/');
        String documentId = urlSplit.get(urlSplit.size() - 1);
        /*fix SH-16220*/
        List <VTR2_Treatment_Arm__c> tArms=[SELECT VTR2_Case__c FROM VTR2_Treatment_Arm__c WHERE Id = :documentId];
        if (tArms.size()>0){
            result=tArms[0].VTR2_Case__c;
        }
      return result;
    }
    @AuraEnabled (cacheable=true)
    public static String getLabelTranslated(String labelName) {
        return VT_D1_TranslateHelper.getLabelValue(labelName);
    }
}