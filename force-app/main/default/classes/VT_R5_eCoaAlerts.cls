/**
 * Created by Dmitry Yartsev on 10.08.2020.
 */

global with sharing class VT_R5_eCoaAlerts implements VT_R5_HerokuScheduler.HerokuExecutable {

    public void herokuExecute(String payload) {

        VT_R5_eDiaryAlertsService alertsService = new VT_R5_eDiaryAlertsService();
        alertsService.herokuExecute(payload.replaceAll('Site Staff', 'SITE_STAFF').replaceAll('Patient/Caregiver', 'PT_CG'));
    }

}