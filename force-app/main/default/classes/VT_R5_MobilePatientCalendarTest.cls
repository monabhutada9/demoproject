/**
 * Created by Yuliya Yakushenkova on 10/6/2020.
 */

@IsTest
public class VT_R5_MobilePatientCalendarTest {

    private static final String URI = '/patient/v1/calendar';
    static {
        new VT_R5_MobilePatientCalendar().getMinimumVersions();
    }

    public static void firstTest() {
        User usr = fillRestContexts();
        String responseBody = VT_R5_TestRestContext.doTest(
                usr, new VT_R5_TestRestContext.MockRestContext(URI, null)
        );
        System.debug(responseBody);
    }
    public static void secondTest() {
        User usr = fillRestContexts();
        Id caseId = VT_R5_PatientQueryService.getCaseByUserId(usr.Id).Id;
        Id visitId = VT_R5_PatientQueryService.getVisitsAndMembersByCaseId(caseId).get(0).Id;

        String responseBody = VT_R5_TestRestContext.doTest(usr,
                new VT_R5_TestRestContext.MockRestContext(
                        URI, new Map<String, String>{
                                'startDate' => String.valueOf(System.now().addDays(-7).getTime()),
                                'endDate' => String.valueOf(System.now().addDays(7).getTime()),
                                'visitIds' => visitId
                        }
                )
        );
        System.debug(responseBody);
    }

    public static User fillRestContexts() {
        User usr = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.PT_USERNAME LIMIT 1];

        return usr;
    }
}