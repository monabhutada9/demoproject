public class Participant{
    String photoUrl {get; set;}
    String userName {get; set;}
    String profName {get; set;}

    public Participant(String photoUrl, String userName, String profName) {
        this.photoUrl = photoUrl;
        this.userName = userName;
        this.profName = profName;
    }
}