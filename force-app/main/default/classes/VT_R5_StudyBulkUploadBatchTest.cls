/**
 * Created by Denis Belkovskii on 10/16/2020.
 */

@IsTest 
public with sharing class VT_R5_StudyBulkUploadBatchTest {
    @TestSetup
    static void setup() {
        for (Integer i=0; i<5; i++) {
            new DomainObjects.Study_t()
                    .setVTD1_Bulk_Upload_Flag(true)
                    .setVTD1_Next_Bulk_Upload_Date(Date.today())
                    .setVTD1_SC_Tasks_Queue_Id('666')
                    .persist();
        }
    }

    @IsTest
    static void testStudyBulkUpdateScheduler() {
        Test.startTest();

        String jobId = System.schedule('StudyBulkUploadBatchTest', '0 0 0 * * ?', new VT_R5_StudyBulkUploadBatch());

        Test.stopTest();
        System.assertEquals(1, [SELECT COUNT() FROM CronTrigger WHERE Id = :jobId]);
    }

    @IsTest
    static void testStudyBulkUpdateBatch() {
        Test.startTest();
        VT_R5_StudyBulkUploadBatch studyBulkUploadBatch = new VT_R5_StudyBulkUploadBatch();
        Database.executeBatch(studyBulkUploadBatch);
        Test.stopTest();
        Integer numberOfRescheduledJobs = [
                SELECT COUNT() FROM HealthCloudGA__CarePlanTemplate__c

                WHERE VTD1_Bulk_Upload_Flag__c = TRUE AND VTD1_Next_Bulk_Upload_Date__c = :Date.today().addDays(1)

        ];
        System.assertEquals(5, numberOfRescheduledJobs);
    }
}