/**
 * Created by Alexander Komarov on 17.04.2019.
 */

@RestResource(UrlMapping='/Notifications/*')
global without sharing class VT_R3_RestNotifications {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();

    @HttpGet
    global static String getNotifications() {
        List<VTD1_NotificationC__c> notificationCs = VT_D1_CustomNotificationsController.getNotificationsForCurrentUser();
        cr.buildResponse(convertNotifications(notificationCs));
        return JSON.serialize(cr, true);
    }

    @HttpPatch
    global static String updateNotifications() {
        RestResponse response = RestContext.response;
        RestRequest request = RestContext.request;
        String originalRequestBody = request.requestBody.toString();

        List<String> clearIds = new List<String>();
        for (String s : originalRequestBody.split(',')) {
            clearIds.add(s.remove(' ').remove('"').remove('\''));
        }

        if (String.isBlank(originalRequestBody) || !helper.isIdCorrectType(clearIds, 'VTD1_NotificationC__c')) {
            response.statusCode = 400;
            cr.buildResponse(helper.forAnswerForIncorrectInput());
            return JSON.serialize(cr, true);
        }

        VTD1_NotificationC__c[] nots = new List<VTD1_NotificationC__c>();
        for (VTD1_NotificationC__c notification : [SELECT Id, VTD1_Read__c FROM VTD1_NotificationC__c WHERE Id IN :clearIds]) {
            if (!notification.VTD1_Read__c) {
                nots.add(new VTD1_NotificationC__c(
                        Id = notification.Id,
                        VTD1_Read__c = true
                ));
            }
        }
        update nots;
        cr.buildResponse(originalRequestBody);
        return JSON.serialize(cr, true);
    }

    @HttpDelete
    global static String hideNotifications() {
        RestResponse response = RestContext.response;
        RestRequest request = RestContext.request;
        String originalRequestBody = request.requestBody.toString();

        Set<String> splittedBody = new Set<String>();
        if (originalRequestBody.contains(',')) {
            splittedBody.addAll(originalRequestBody.split(','));
        } else {
            splittedBody.add(originalRequestBody);
        }

        List<VTD1_NotificationC__c> toHide = new List<VTD1_NotificationC__c>();
        List<VTD1_NotificationC__c> notificationCs = VT_D1_CustomNotificationsController.getNotificationsForCurrentUser();
        for (VTD1_NotificationC__c notificationC : notificationCs) {
            if (splittedBody.contains(String.valueOf(notificationC.Id))) {
                notificationC.VTR5_HideNotification__c = true;
                toHide.add(notificationC);
            }
        }

        try {
            if (!toHide.isEmpty()) update toHide;
        } catch (Exception e) {
            response.statusCode = 500;
            cr.buildResponse('FAILURE', 'Notification already removed.');
            return JSON.serialize(cr, true);
        }
//        List<String> clearIds = new List<String>();
//        for (String s : splittedBody) {
//            clearIds.add(s.remove(' ').remove('"'));
//        }
//
//        List<Id> superClearIds = new List<Id>();
//        for (String s : clearIds) {
//            if (s.length() > 14) {
//                superClearIds.add(Id.valueOf(s));
//            }
//        }

//        try {
//            List<VTD1_NotificationC__c> notesForHide = [SELECT Id,VTR5_HideNotification__c FROM VTD1_NotificationC__c WHERE Id IN :superClearIds];
//            if (!notesForHide.isEmpty()) {
//                for (VTD1_NotificationC__c notification : notesForHide) {
//                    notification.VTR5_HideNotification__c = true;
//                }
//                update notesForHide;
//            }
//        } catch (Exception e) {
//            response.statusCode = 500;
//            cr.buildResponse('FAILURE', 'Notification already removed.');
//            return JSON.serialize(cr, true);
//        }

        cr.buildResponse(originalRequestBody);
        return JSON.serialize(cr, true);

    }

    private static List<NotificationObj> convertNotifications(List<VTD1_NotificationC__c> notis) {
        List<NotificationObj> result = new List<NotificationObj>();
        if (notis.isEmpty()) return result;

        Set<Id> dubbedNotifications = new Set<Id>();
        Set<Id> notificationsIds = new Set<Id>();

        for (VTD1_NotificationC__c noti : notis) {
            notificationsIds.add(noti.Id);
            if (String.isNotBlank(noti.VTR2_PatientNotificationId__c)) {
                dubbedNotifications.add(noti.Id);
            }
        }

        Map<Id, Id> actualVisitToNotification = new Map<Id, Id>();
        for (VTD1_NotificationC__c noti : [
                SELECT Id, VTD2_VisitID__c
                FROM VTD1_NotificationC__c
                WHERE Id IN :notificationsIds
        ]) {
            if (String.isNotBlank(noti.VTD2_VisitID__c) && helper.isIdCorrectType(noti.VTD2_VisitID__c, 'VTD1_Actual_Visit__c')) {
                actualVisitToNotification.put(noti.VTD2_VisitID__c, noti.Id);
            }
        }

        Map<Id, String> notiIdToVideoSession = new Map<Id, String>();
        for (Video_Conference__c vc : [
                SELECT Id, SessionId__c, VTD1_Actual_Visit__c
                FROM Video_Conference__c
                WHERE VTD1_Actual_Visit__c IN :actualVisitToNotification.keySet()
        ]) {
            if (String.isNotBlank(vc.SessionId__c)) {
                notiIdToVideoSession.put(actualVisitToNotification.get(vc.VTD1_Actual_Visit__c), vc.SessionId__c);
            }
        }
        String patientName;
        if (dubbedNotifications.size() > 0) {
            Id patientAccount = [SELECT Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].Contact.AccountId;
            patientName = [SELECT Name FROM Contact WHERE Id IN (SELECT ContactId FROM Case WHERE AccountId = :patientAccount) LIMIT 1].Name;
        }
        for (VTD1_NotificationC__c n : notis) {
            NotificationObj convertedNotification = new NotificationObj(
                    n.Id,
                    n.Title__c,
                    n.Message__c,
                    n.Link_to_related_event_or_object__c,
                    n.CreatedDate,
                    n.VTD1_Read__c
            );
            if (String.isNotBlank(n.VTD2_VisitID__c)) {
                convertedNotification.visitId = n.VTD2_VisitID__c;
            }
            if (n.Type__c != null) {
                if (n.Type__c.equals('Messages')) {
                    convertedNotification.relatedObject = 'messages';
                    if (n.Number_of_unread_messages__c > 1) {
                        convertedNotification.message = System.Label.VT_D1_YouHaveMessages + ' ' + String.valueOf(n.Number_of_unread_messages__c) + ' ' + System.Label.VT_D1_NewMessages;
                    } else {
                        convertedNotification.message = System.Label.VT_D1_OneNewMessage;
                    }
                    if (!n.VTD1_Read__c) {
                        convertedNotification.notificationDate = n.VTD2_New_Message_Added_DateTme__c.getTime();

                    }

                }
            }
            if (n.VTR3_Notification_Type__c.equals('eCOA Notification')) {
                convertedNotification.relatedObject = 'my-diary';
                convertedNotification.successBannerNeed = true;
                convertedNotification.successBannerText = n.Title__c;
            }
            if (convertedNotification.relatedObject != null) {
                if (convertedNotification.relatedObject.equals('detail/device-list')) {
                    convertedNotification.relatedObject = 'device-list';
                }
                if (convertedNotification.relatedObject.contains('device?deviceId=')) {
                    convertedNotification.deviceId = convertedNotification.relatedObject.substringAfter('device?deviceId=');
                    convertedNotification.relatedObject = 'device';
                }
            }

            if (dubbedNotifications.contains(n.Id)) {
                convertedNotification.patientName = patientName;
            }
            if (notiIdToVideoSession.containsKey(n.Id)) {
                convertedNotification.videoSessionId = notiIdToVideoSession.get(n.Id);
            }
            result.add(convertedNotification);
        }
        return result;
    }

    private class NotificationObj {
        public String id;
        public String title;
        public String message;
        public String relatedObject;
        public Long notificationDate;
        public Boolean read;
        public String visitId;
        public String deviceId;
        public String videoSessionId;
        public String patientName;
        public Boolean successBannerNeed;
        public String successBannerText;


        public NotificationObj(String i, String t, String m, String ro, Datetime d, Boolean r) {
            this.id = i;
            this.title = t;
            this.message = m;
            this.relatedObject = ro;
            this.notificationDate = d.getTime();
            this.read = r;
        }
    }
}