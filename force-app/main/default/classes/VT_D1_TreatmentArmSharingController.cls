public with sharing class VT_D1_TreatmentArmSharingController {
	@AuraEnabled
	public static List<StudyMemberObj> getRows(Id studyId) {
		List<Study_Team_Member__c> studyMembers = [
				SELECT Id, User__r.Id, User__r.Profile.Name, User__r.Name
				FROM Study_Team_Member__c
				WHERE Study__c = :studyId
				AND User__r.IsActive = TRUE
		];

		List<StudyMemberObj> rows = new List<StudyMemberObj>();
		for (Study_Team_Member__c s: studyMembers) {
			StudyMemberObj row = new StudyMemberObj();
			row.studyTeamMemberId = s.Id;
			row.userId = s.User__r.Id;
			row.accessLevel = 'Read';
			row.userName = String.valueOf(s.User__r.Name);
			row.profileName = String.valueOf(s.User__r.Profile.Name);
			rows.add(row);
		}
		return rows;
	}

	public class StudyMemberObj {
		@AuraEnabled public Id studyTeamMemberId;
		@AuraEnabled public Id userId;
		@AuraEnabled public String userName;
		@AuraEnabled public String profileName;
		@AuraEnabled public String accessLevel;
	}
}