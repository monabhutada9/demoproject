public with sharing class CustomThemeLayoutController {
    @AuraEnabled
    public static String getSessionIdUserId() {
        return UserInfo.getSessionId() + ',' + UserInfo.getUserId();
    }
    @AuraEnabled
    public static Id getPatientCaregiverCase() {
        List<User> users = getPatientCaregiverUsers();
        if (users.isEmpty()) return null;

        Case c = VT_D1_PatientCaregiverBound.getPatientCase(users[0].Id);
        if (c == null) return null;


        return c.Id;
    }

    @AuraEnabled
    public static List<User> getPatientCaregiverUsers() {
        return [
                SELECT Id
                FROM User
                WHERE (Profile.Name = :VT_D1_ConstantsHelper.PATIENT_PROFILE_NAME
                OR Profile.Name = :VT_D1_ConstantsHelper.CAREGIVER_PROFILE_NAME)
                AND Id = :UserInfo.getUserId()
        ];
    }


    @AuraEnabled
    public static String getWebSocketUrl() {
        String urlParamsTemplate = '/{0}?userId={1}&ts={2}&token={3}';
        String url = VTR4_HerokuNotificationService__c.getInstance().VTR4_AppUrl__c + urlParamsTemplate;
        List<String> urlParams = new List<String>();
        Long timestamp = Datetime.now().getTime();
        urlParams.add(UserInfo.getOrganizationId());
        urlParams.add(UserInfo.getUserId());
        urlParams.add(String.valueOf(timestamp));
        urlParams.add(getNotificationServiceToken(timestamp));
        return String.format(url, urlParams);
    }

    public static String getNotificationServiceToken(Long timestamp) {
        String algorithm = 'AES256';
        String encoding = 'UTF-8';
        Blob secretKey = EncodingUtil.base64Decode(VTR4_HerokuNotificationService__c.getInstance().VTR4_AesKey__c);
        Blob clearData = Blob.valueOf(UserInfo.getUserId() + timestamp + VTR4_HerokuNotificationService__c.getInstance().VTR4_ApiKey__c);
        String token = JSON.serialize(Crypto.encryptWithManagedIV(algorithm, secretKey, clearData)).replaceAll('"', '');
        return EncodingUtil.urlEncode(token, encoding);
    }

    @AuraEnabled
    public static String getCurrentVideoId() {
        List<Video_Conference__c> videoConferences = [
                SELECT Id
                FROM Video_Conference__c
                WHERE Id
                        IN (
                                SELECT VTR4_Video_Conference__c
                                FROM VTR4_Scheduler_Video_Conference__c
                                WHERE VTR4_ReadyToStart__c = TRUE
                                AND VTR4_ReadyToEnd__c = FALSE
                        )
                AND VTD1_Actual_Visit__c IN (
                        SELECT VTD1_Actual_Visit__c
                        FROM Visit_Member__c
                        WHERE VTD1_Participant_User__c = :UserInfo.getUserId()
                )
                ORDER BY Scheduled_For__c
                LIMIT 1
        ];
        return (!videoConferences.isEmpty()
                ? (String) videoConferences[0].Id
                : [SELECT Id, Televisit_Buffer__c FROM User WHERE Id = :UserInfo.getUserId()].Televisit_Buffer__c);
    }

    @AuraEnabled
    public static String translateNotificationMessage(Id notId) {
        if (notId == null) {
            return null;
        }
        String message;
        List <VTD1_NotificationC__c> nots = [
                SELECT Id, Title__c,
                        VTD2_Title_Parameters__c,
                        Link_to_related_event_or_object__c,
                        FORMAT(LastModifiedDate) LastModifiedDate,
                        Message__c,
                        VTD1_Read__c,
                        Number_of_unread_messages__c,
                        Type__c,
                        HasDirectLink__c,
                        isActualVisitLink__c,
                        VTD1_Parameters__c
                FROM VTD1_NotificationC__c
                WHERE Id = :notId
        ];
        try {
            VT_D1_TranslateHelper.translate(nots);
            message = nots[0].Message__c;
        } catch (Exception e) {
            System.debug('Error' + e.getMessage());
        }
        return message;
    }

    @AuraEnabled(Cacheable=true)
    public static UserMetricsData getUserMetricsData() {
        try {
            User u = [
                    SELECT Id,
                            Country,
                            City,
                            Profile.Name,
                            ContactId,
                            AccountId,
                            Contact.VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c,
                            Contact.VTD1_Clinical_Study_Membership__r.VTR5_Internal_Subject_Id__c,
                            Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.Name
                    FROM User
                    WHERE Id = :UserInfo.getUserId()
            ];
            UserMetricsData um = new UserMetricsData();
            um.Id = u.Id;
            um.profileName = u.Profile.Name;
            if (Test.isRunningTest()) um.profileName = VT_R4_ConstantsHelper_Profiles.PATIENT_PROFILE_NAME;

            if (VT_R4_ConstantsHelper_Profiles.HIPAA_PROFILES.contains(um.profileName)) {
                um.subjectId = u.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Subject_ID__c;
                um.internalSubjectId = u.Contact.VTD1_Clinical_Study_Membership__r.VTR5_Internal_Subject_Id__c;
                um.studyName = u.Contact.VTD1_Clinical_Study_Membership__r.VTD1_Study__r.Name;

                List<Address__c> addresses = [
                        SELECT Id, Country__c, City__c
                        FROM Address__c
                        WHERE VTD1_Patient__c = :u.AccountId AND VTD1_Primary__c = TRUE
                ];
                if (addresses.size() > 0) {
                    Address__c a = addresses[0];
                    um.country = a.Country__c;
                    um.city = a.City__c;
                }

            } else {  // for study team roles
                um.country = u.Country;
                um.city = u.City;
				um.studyName = null;  // default for PI/SCR - null, filled in VT_D1_PIStudySwitcher
			}

            if(!Test.isRunningTest()) {
            um.ip = Auth.SessionManagement.getCurrentSession().get('SourceIp');
            if (um.ip == '::') um.ip = null;
            }



            String networkId = Network.getNetworkId();
            if (networkId != null && ConnectApi.Communities.getCommunity(networkId) != null) {
                um.community = ConnectApi.Communities.getCommunity(networkId).name;
            }


            List<VTD1_General_Settings__mdt> customMetaData = [
                    SELECT VTR5_Browser_Metrics_Heroku_App_URL__c
                    FROM VTD1_General_Settings__mdt LIMIT 1
            ];
            if (!customMetaData.isEmpty()) {
                um.herokuUrl = customMetaData[0].VTR5_Browser_Metrics_Heroku_App_URL__c;
            }

            return um;

        } catch (Exception e) {
            return null;
        }
    }

    public class UserMetricsData {
        @AuraEnabled public String Id { get; set; }
        @AuraEnabled public String subjectId { get; set; }
        @AuraEnabled public String internalSubjectId { get; set; }
        @AuraEnabled public String studyName { get; set; }

        @AuraEnabled public String community { get; set; }
		@AuraEnabled public String profileName { get; set; }

		@AuraEnabled public String ip { get; set; }
        @AuraEnabled public String country { get; set; }
        @AuraEnabled public String city { get; set; }


        @AuraEnabled public String herokuUrl { get; set; }
        @AuraEnabled public String herokuKey { get; set; }

    }
}