/**
 * @author: Alexander Komarov
 * @date: 21.05.2020
 * @description:
 */

public with sharing class VT_R5_MobileInitial extends VT_R5_MobileRestResponse implements VT_R5_MobileRestRouter.Routable{


    public List<String> getMapping() {
        return new List<String>{
                '/patient/{version}/initial/'
        };
    }

    public void versionRoute(Map<String, String> parameters)  {
        initService();
        String requestVersion = parameters.get('version');
        switch on this.request.httpMethod {
            when 'GET' {
                switch on requestVersion {
                    when 'v1' {
                        get_v1();
                        return;
                    }
                }

            }
        }
        VT_R5_MobileRestRouter.unsupportedVersionResponse();
        this.addError('unsupported version');
        this.sendResponse();
    }

    private void get_v1() {
        try {
            this.buildResponse(new InitialData());
        } catch (Exception e) {
            this.buildResponse(e);
            return;
        }
        this.sendResponse();
    }

    public void initService() {
    }

    private static final Map<String, String> profileToCommunities = new Map<String, String>{
            'Patient' => 'VTD2_PatientCGCommunity',
            'Caregiver' => 'VTD2_PatientCGCommunity',
            'Primary Investigator' => 'VTD2_PICommunity',
            'Site Coordinator' => 'VTR2_SCRCommunity'
    };

    private class InitialData {
        private String videoAPIKey;
        private String userProfile;
        private String subjectId;
        private String studyId;
        private String patientStatus;
        private String virtualSiteId;

        private String readingsInsertEndpoint;
        private String readingsInsertAuth;
        private String readingsInsertStorageName;
        private String readingsReceiveEndpoint;
        private String readingsReceiveAuth;
        private String readingsReceiveStorageName;

        private String scribeAppLinkIos;
        private String scribeAppLinkAndroid;
        private Boolean eCoaStudy = false;
        private String internalSubjectId;
        private String communityName;

        private String userCity;
        private String userCounty;
        private Boolean isStudyDevicesEnabled = false;

//        private Map<String, Boolean> enabledFeatures;

        private final String userFirstName = UserInfo.getFirstName();
        private final String salesforceId = UserInfo.getUserId();
        private final String userLastName = UserInfo.getLastName();
        private final String userLanguage = UserInfo.getLanguage();
        private final String username = UserInfo.getUserName();
        private final String organizationId = UserInfo.getOrganizationId();
        private final String communityURL = Url.getCurrentRequestUrl().getHost();
        private Boolean isTestUser = false;

//        private Map<String, Boolean> setEnabledFeatures(String studySettings) {
//            Map<String, Boolean> result = new Map<String, Boolean>();
//
//            List<String> enabledFeatures = studySettings == null ? new List<String>() : studySettings.split(';');
//
//            SObjectType sObjectType = healthcloudga__CarePlanTemplate__c.getSObjectType();
//            for (Schema.PicklistEntry pe : sObjectType
//                    .getDescribe()
//                    .fields
//                    .getMap()
//                    .get('VTR5_PatientMobileAppEnabledFeatures__c')
//                    .getDescribe()
//                    .getPicklistValues()) {
//                String feature = pe.getValue();
//                Boolean isEnabled = enabledFeatures.contains(feature);
//                result.put(feature, isEnabled);
//            }
//            return result;
//
//        }

        InitialData() {
            List<VTD1_General_Settings__mdt> generalMetadata = [SELECT VTR2_Video_APIKey__c FROM VTD1_General_Settings__mdt];
            if (!generalMetadata.isEmpty()) {
                this.videoAPIKey = generalMetadata[0].VTR2_Video_APIKey__c;
            }

            User currentUser = [SELECT Profile.Name, VTD1_Is_Test__c FROM User WHERE Id = :UserInfo.getUserId()];

            this.userProfile = currentUser.Profile.Name;
            this.isTestUser = currentUser.VTD1_Is_Test__c == null ? false : currentUser.VTD1_Is_Test__c;

            this.communityName = profileToCommunities.get(this.userProfile);

            Case currentUserCase = VT_D1_PatientCaregiverBound.getCaseForUser();

            Case selectedAgainCaseCozCodeDuplication = [
                    SELECT Id,
                            VTD1_Subject_ID__c,
                            VTD1_Study__c,
                            VTD1_Study__r.Name,
                            VTD1_Virtual_Site__r.VTD1_Study__r.VTR5_eDiaryTool__c,
                            VTD1_Virtual_Site__r.Name,
                            VTD1_Virtual_Site__c,
                            VTR5_Internal_Subject_Id__c,
                            Contact.Id,
                            Status
                    FROM Case
                    WHERE Id = :currentUserCase.Id
            ];

            List<Address__c> addresses = [SELECT Id, City__c, Country__c FROM Address__c WHERE VTD1_Contact__c = :selectedAgainCaseCozCodeDuplication.Contact.Id];

            List<VTD1_Patient_Device__c> devices = [SELECT Id FROM VTD1_Patient_Device__c WHERE VTD1_Case__c = :currentUserCase.Id];

            this.isStudyDevicesEnabled = !devices.isEmpty();

            List<VTR5_Mobile_App_Setting__mdt> mobileMetadata = [
                    SELECT
                            VTR5_DeviceReadingsInsertAuthorization__c,
                            VTR5_DeviceReadingsInsertEndpoint__c,
                            VTR5_DeviceReadingsStorageName__c,
                            VTR5_DeviceReadingsReceiveAuthorization__c,
                            VTR5_DeviceReadingsReceiveEndpoint__c,

                            VTR5_DeviceReadingsReceiveStorage__c,
                            VTR5_IqviaScribeAppAndroidLink__c,
                            VTR5_IqviaScribeAppIosLink__c

                    FROM VTR5_Mobile_App_Setting__mdt
            ];
            if (!mobileMetadata.isEmpty()) {
                this.readingsInsertEndpoint = mobileMetadata[0].VTR5_DeviceReadingsInsertEndpoint__c;
                this.readingsInsertAuth = mobileMetadata[0].VTR5_DeviceReadingsInsertAuthorization__c;
                this.readingsInsertStorageName = mobileMetadata[0].VTR5_DeviceReadingsStorageName__c;
                this.readingsReceiveEndpoint = mobileMetadata[0].VTR5_DeviceReadingsReceiveEndpoint__c;
                this.readingsReceiveAuth = mobileMetadata[0].VTR5_DeviceReadingsReceiveAuthorization__c;
                this.readingsReceiveStorageName = mobileMetadata[0].VTR5_DeviceReadingsReceiveStorage__c;

                this.scribeAppLinkAndroid = mobileMetadata[0].VTR5_IqviaScribeAppAndroidLink__c;
                this.scribeAppLinkIos = mobileMetadata[0].VTR5_IqviaScribeAppIosLink__c;
            }

            if (!addresses.isEmpty()) {
                this.userCity = addresses[0].City__c;
                this.userCounty = addresses[0].Country__c;
            }
            this.subjectId = selectedAgainCaseCozCodeDuplication.VTD1_Subject_ID__c;
            this.patientStatus = selectedAgainCaseCozCodeDuplication.Status;
            this.virtualSiteId = selectedAgainCaseCozCodeDuplication.VTD1_Virtual_Site__c;
            this.studyId = selectedAgainCaseCozCodeDuplication.VTD1_Study__r.Name;
            String ediaryTool = selectedAgainCaseCozCodeDuplication.VTD1_Virtual_Site__r.VTD1_Study__r.VTR5_eDiaryTool__c;
            this.eCoaStudy = String.isNotEmpty(ediaryTool) && ediaryTool.equals('eCOA');
            this.internalSubjectId = selectedAgainCaseCozCodeDuplication.VTR5_Internal_Subject_Id__c;
        }
    }
}