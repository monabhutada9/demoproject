public class VT_D1_PatientProfileIntialController {
    public class InputSelectRatioElement {
        public String key;
        public String label;
        public Boolean value;
    }
    public class PatientProfileInitial {
        public Contact contact;
        public List<InputSelectRatioElement> contactPreferredContactMethodSelectRatioElementList;
//        public List<InputSelectRatioElement> contactLanguageSelectRatioElementList;
        public VT_D1_Phone__c phone;
        public String phoneNumberForCreate;
        public Address__c address;
        public String stateForCreate;
        public String zipCodeForCreate;
        public List<InputSelectRatioElement> addressStateSelectRatioElementList;
        public User user;
    }
    @AuraEnabled
    public static String getInitialProfile() {
        try {
            PatientProfileInitial ppi = getPatientProfileInitial();
            return JSON.serialize(ppi);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
    public static PatientProfileInitial getPatientProfileInitial() {
        PatientProfileInitial patientProfileInitial = new PatientProfileInitial();
        VT_R5_PatientProfileService patientService = new VT_R5_PatientProfileService();
        patientProfileInitial.user = patientService.usr;

        patientProfileInitial.phone = patientService.phone;//Comment added for copado to see the difference
        patientProfileInitial.contact = patientService.contact;
        patientProfileInitial.address = patientService.address;//Comment added for copado to see the difference

        patientProfileInitial.contactPreferredContactMethodSelectRatioElementList =
                getContactPreferredContactMethodSelectRatioElementList(patientProfileInitial.contact);
        patientProfileInitial.addressStateSelectRatioElementList =
                getAddressStateSelectRatioElementList(patientProfileInitial.address.State__c);
        System.debug(patientProfileInitial);
        return patientProfileInitial;
/**        User u = [
//                SELECT
//                        Id,
//                        ContactId,
//                        LanguageLocaleKey,
//                        VT_R5_Secondary_Preferred_Language__c,
//                        VT_R5_Tertiary_Preferred_Language__c,
//                        VTD1_Filled_Out_Patient_Profile__c,
//                        Profile.Name
//                FROM User WHERE Id = :UserInfo.getUserId()
//        ];
//        if (u.ContactId != null) {
//            List<Contact> contacts = [
//                    SELECT Id
//                            , FirstName
//                            , VTD1_MiddleName__c
//                            , LastName
//                            , Preferred_Contact_Method__c
//                            , AccountId
//                            , HealthCloudGA__Gender__c
//                            , VTR2_Primary_Language__c
//                            , VT_R5_Secondary_Preferred_Language__c
//                            , VT_R5_Tertiary_Preferred_Language__c
////                                , HealthCloudGA__BirthDate__c
//                    FROM Contact
//                    WHERE Id = :u.ContactId
//            ];
//            if (!contacts.isEmpty()) {
//                Contact contact = contacts[0];
//                patientProfileInitial.contact = contact;
//
//                List<InputSelectRatioElement> contactPreferredContactMethodSelectRatioElementList = new List<InputSelectRatioElement>();
//                Map<String, String> optionsMap = VT_D1_CustomMultiPicklistController.getSelectOptionsWithLabel(contact, 'Preferred_Contact_Method__c');
//                for (String option : optionsMap.keySet()) {
//                    InputSelectRatioElement inputSelectRatioElement = new InputSelectRatioElement();
//                    inputSelectRatioElement.key = option;
//                    inputSelectRatioElement.label = optionsMap.get(option);
////                if (contact.Preferred_Contact_Method__c == option){
////                    inputSelectRatioElement.value = true;
////                } else {
////                    inputSelectRatioElement.value = false;
////                }
//                    contactPreferredContactMethodSelectRatioElementList.add(inputSelectRatioElement);
//                }
//                patientProfileInitial.contactPreferredContactMethodSelectRatioElementList = contactPreferredContactMethodSelectRatioElementList;
//            }
//            /*
//            List<InputSelectRatioElement> contactLanguageSelectRatioElementList = new List<InputSelectRatioElement>();
//            List<String> contactLanguageOptionsList = new List<String>();
//            contactLanguageOptionsList.add('English');
//            contactLanguageOptionsList.add('Spanish');
//            contactLanguageOptionsList.add('Other');
//            for (String option : contactLanguageOptionsList) {
//                InputSelectRatioElement inputSelectRatioElement = new InputSelectRatioElement();
//                inputSelectRatioElement.key = option;
//                if (contact.HealthCloudGA__PrimaryLanguage__c == option){
//                    inputSelectRatioElement.value = true;
//                } else {
//                    inputSelectRatioElement.value = false;
//                }
//                contactLanguageSelectRatioElementList.add(inputSelectRatioElement);
//            }
//            patientProfileInitial.contactLanguageSelectRatioElementList = contactLanguageSelectRatioElementList;
//
//
////            List<VT_D1_Phone__c> contactPhonesList = [SELECT Id, PhoneNumber__c, IsPrimaryForPhone__c FROM VT_D1_Phone__c WHERE VTD1_Contact__c = :u.ContactId];
////            if (contactPhonesList.size() > 0) {
////                patientProfileInitial.phone = contactPhonesList.get(0);
////            }
////
////            List<Address__c> addressList = [
////                    SELECT Id, toLabel(State__c) stateLabel,State__c, ZipCode__c,Country__c
////                    FROM Address__c
////                    WHERE VTD1_Contact__c = :u.ContactId
////            ];
////            if (addressList.size() > 0) {
////                patientProfileInitial.address = addressList.get(0);
////            }
////        }
*/
    }
    public static List<InputSelectRatioElement> getContactPreferredContactMethodSelectRatioElementList(Contact contact) {
        List<InputSelectRatioElement> contactPreferredElements = new List<InputSelectRatioElement>();
        if (contact == null) return null;
        Map<String, String> optionsMap = VT_D1_CustomMultiPicklistController.getSelectOptionsWithLabel(
                contact, 'Preferred_Contact_Method__c'
        );
        for (String option : optionsMap.keySet()) {
            VT_D1_PatientProfileIntialController.InputSelectRatioElement inputSelectRatioElement =
                    new VT_D1_PatientProfileIntialController.InputSelectRatioElement();
            inputSelectRatioElement.key = option;
            inputSelectRatioElement.label = optionsMap.get(option);
            contactPreferredElements.add(inputSelectRatioElement);
        }
        return contactPreferredElements;
    }
    public static List<InputSelectRatioElement> getAddressStateSelectRatioElementList(String state) {
        return getInputSelectRatioList(new Address__c(), 'State__c', state, false);
    }
    public static List<InputSelectRatioElement> getInputSelectRatioList(SObject sObj, String fieldName, String fieldValue, Boolean addNullKeyIfValueEmpty) {
        Boolean inputSelectRatioElementEmpty = true;
        List<InputSelectRatioElement> inputSelectRatioList = new List<InputSelectRatioElement>();
        Map<String, String> optionsMap = VT_D1_CustomMultiPicklistController.getSelectOptionsWithLabel(sObj, fieldName);
        for (String option : optionsMap.keySet()) {
            InputSelectRatioElement inputSelectRatioElement = new InputSelectRatioElement();
            inputSelectRatioElement.key = option;
            inputSelectRatioElement.label = optionsMap.get(option);
            if (fieldValue == option) {
                inputSelectRatioElement.value = true;
                inputSelectRatioElementEmpty = false;
            } else {
                inputSelectRatioElement.value = false;
            }
            inputSelectRatioList.add(inputSelectRatioElement);
        }
        if (addNullKeyIfValueEmpty && inputSelectRatioElementEmpty) {
            InputSelectRatioElement inputSelectRatioElement = new InputSelectRatioElement();
            inputSelectRatioElement.key = null;
            inputSelectRatioElement.value = true;
            inputSelectRatioList.add(inputSelectRatioElement);
        }
        return inputSelectRatioList;
    }
    @AuraEnabled(Cacheable=true)
    public static List<InputSelectOption> getInputSelectOptions(String sObjName, String fieldName) {
        System.debug('TESTING!');
        List<InputSelectOption> options = new List<InputSelectOption>();
        if (!String.isEmpty(sObjName) && !String.isEmpty(fieldName)) {
            Schema.SObjectType objType = Schema.getGlobalDescribe().get(sObjName);
            SObject sObj = objType.newSObject();
            List<List<String>> optionsPairsList = VT_D1_CustomMultiPicklistController.getSortedSelectOptionsWithLabel(sObj, fieldName);
            System.debug(optionsPairsList.size());
            for (List<String> optionPair : optionsPairsList) {
                InputSelectOption option = new InputSelectOption(optionPair[0], optionPair[1]);
                options.add(option);
            }
        }
        return options;
    }
    @AuraEnabled
    public static void updateProfile(String patientProfileInitialString) {
        PatientProfileInitial patientProfileInitial =
                (PatientProfileInitial) JSON.deserialize(patientProfileInitialString, PatientProfileInitial.class);
        Contact contact = patientProfileInitial.contact;
        User user = patientProfileInitial.user;
        Address__c address = patientProfileInitial.address;
        VT_R5_PatientProfileService patientService = new VT_R5_PatientProfileService(
                contact, user, patientProfileInitial.phone, address
        );
        patientService.updatePatientProfile(
                patientProfileInitial.phoneNumberForCreate,
                patientProfileInitial.stateForCreate,
                patientProfileInitial.zipCodeForCreate
        );
        /**
//        system.debug('Contact: ' + contact);
//        update contact;
//
//
//        u.VTD1_Filled_Out_Patient_Profile__c = true;
//        //u.LanguageLocaleKey = UserInfo.getLanguage();
//        update u;
//        VT_D1_Phone__c phone = patientProfileInitial.phone;
//        if (phone != null) {
//            phone.IsPrimaryForPhone__c = true;
//            update phone;
//        } else {
//            VT_D1_Phone__c phoneToInsert = new VT_D1_Phone__c();
//            phoneToInsert.VTD1_Contact__c = contact.Id;
//            phoneToInsert.Account__c = contact.AccountId;
//            phoneToInsert.IsPrimaryForPhone__c = true;
//            phoneToInsert.PhoneNumber__c = patientProfileInitial.phoneNumberForCreate;
//
//            System.debug(phoneToInsert);
//            insert phoneToInsert;
//        }
//        if (address != null) {
//            update address;
//        } else if (patientProfileInitial.user.Profile.Name == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
//            Address__c addressToInsert = new Address__c();
//            addressToInsert.VTD1_Contact__c = contact.Id;
//            addressToInsert.VTD1_Patient__c = contact.AccountId;
//            addressToInsert.State__c = patientProfileInitial.stateForCreate;
//            addressToInsert.ZipCode__c = patientProfileInitial.zipCodeForCreate;
//            System.debug(addressToInsert);
//            insert addressToInsert;
//        }
//        HandlerExecutionPool.getInstance().removeHandlerFromDisabledList('VT_R4_PatientFieldLockService');
*/
    }
    public class InputSelectOption {
        @AuraEnabled public String value;
        @AuraEnabled public String label;
        public InputSelectOption(String value, String label) {
            this.value = value;
            this.label = label;
        }
    }
    @AuraEnabled
    public static Boolean isUserProfileFilledOut() {
        User u = [SELECT VTD1_Filled_Out_Patient_Profile__c FROM User WHERE Id = :UserInfo.getUserId()];
        return u.VTD1_Filled_Out_Patient_Profile__c == false;
    }


    @AuraEnabled
    public static String getPatientProfileInitialWrapper (){
        PatientProfileInitialWrapper wrapper = new PatientProfileInitialWrapper();
        wrapper.isUserProfileFilledOut = isUserProfileFilledOut();
        wrapper.isPreConsentStatus = false;
        if(wrapper.isUserProfileFilledOut){
            wrapper.isPreConsentStatus = VT_D1_PatientCaregiverBound.getCaseForUser().Status == 'Pre-Consent';
        }
        return JSON.serialize(wrapper);
    }


    public class PatientProfileInitialWrapper{
        Boolean isUserProfileFilledOut;
        Boolean isPreConsentStatus;
    }
//    Copado update
}