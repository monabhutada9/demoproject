/**
 * @author Ruslan Mullayanov
 * @description One time use. Transfers Patient Field History records from VTR2_Patient_Field_History__b to VTR5_PatientFieldHistory__b
 */

global without sharing class VT_R5_PatientHistoryTransferBatch implements Database.Batchable<SObject> {

    global Iterable<SObject> start(Database.BatchableContext BC) {
        return [
                SELECT Object_Type__c, Object_Id__c, Field__c, Old_Value__c, New_Value__c, Operation_Type__c, Patient_Id__c, Is_Deleted__c, Date__c, Hash__c, User_Id__c, CreatedById
                FROM VTR2_Patient_Field_History__b
        ];
    }

    global void execute(Database.BatchableContext BC, List<VTR2_Patient_Field_History__b> scope) {
        if (scope == null || scope.isEmpty()) return;
        System.debug('Scope size '+scope.size());
        List<VTR5_PatientFieldHistory__b> pfh = new List<VTR5_PatientFieldHistory__b>();
        List<VTR2_Patient_Field_History__b> pfhToDelete = new List<VTR2_Patient_Field_History__b>();
        for (VTR2_Patient_Field_History__b pfht : scope) {
            pfh.add(new VTR5_PatientFieldHistory__b(
                    VTR5_Object_Type__c = pfht.Object_Type__c,
                    VTR5_Object_Id__c = pfht.Object_Id__c,
                    VTR5_Field__c = pfht.Field__c,
                    VTR5_Old_Value__c = pfht.Old_Value__c,
                    VTR5_New_Value__c = pfht.New_Value__c,
                    VTR5_Operation_Type__c = pfht.Operation_Type__c,
                    VTR5_Patient_Id__c = pfht.Patient_Id__c,
                    VTR5_Is_Deleted__c = String.valueOf(pfht.Is_Deleted__c),
                    VTR5_Date__c = pfht.Date__c,
                    VTR5_Hash__c = pfht.Hash__c,
                    VTR5_User_Id__c = pfht.CreatedById
            ));
            pfhToDelete.add(new VTR2_Patient_Field_History__b(
                    Patient_Id__c = pfht.Patient_Id__c,
                    Date__c = pfht.Date__c,
                    Hash__c = pfht.Hash__c
            ));
        }
        Boolean hasErrors = false;
        try {
            Database.SaveResult[] srList = Database.insertImmediate(pfh);
            System.debug('Inserted ' + pfh.size());
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    hasErrors = true;
                    for (Database.Error err : sr.getErrors()) {
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Fields that affected this error: ' + err.getFields());
                    }
                }
            }
        } catch (Exception e) {
            hasErrors = true;
        }
//        if (!hasErrors) {
//            Database.deleteImmediate(pfhToDelete);
//            System.debug('Deleted '+pfhToDelete.size());
//        }
    }

    global void finish(Database.BatchableContext BC) {
        System.debug('Finished');
    }
}