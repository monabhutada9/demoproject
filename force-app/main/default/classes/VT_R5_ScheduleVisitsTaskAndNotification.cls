/**
 * Created by Stanislav Frolov on 8/12/2020.
 */

public with sharing class VT_R5_ScheduleVisitsTaskAndNotification{


    private static List<VTD1_Actual_Visit__c> updateVisits = new List<VTD1_Actual_Visit__c>();

    public static final String NOT_ONBOARDING_VISIT = 'Not Onboarding Visit';
    public static final String BASELINE_VISIT = 'Baseline Visit';
    public static final String PROTOCOL_BASELINE_VISIT = 'Protocol Baseline Visit';
    public static final String PROTOCOL_VISIT = 'Protocol Visit';

    private static final String TO_BE_SCHEDULED = VT_R4_ConstantsHelper_Statuses.ACTUAL_VISIT_STATUS_TO_BE_SCHEDULED;
    private static final String FUTURE_VISIT = VT_R4_ConstantsHelper_Statuses.ACTUAL_VISIT_STATUS_FUTURE_VISIT;
    private static final String CANCELLED = VT_R4_ConstantsHelper_Statuses.ACTUAL_VISIT_STATUS_CANCELLED;
    private static final String HCP = VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_HCP;
    private static final String LABS = VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_LABS;
    private static final String STUDY_TEAM = VT_R4_ConstantsHelper_VisitsEvents.PROTOCOL_VISIT_TYPE_STUDY_TEAM;
    private static final String PSC = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_SUBTYPE_PSC;
    private static final String COMPLETED = VT_R4_ConstantsHelper_VisitsEvents.ACTUAL_VISIT_STATUS_COMPLETED;
    private static final String AT_LOCATION = 'At Location';
    private static final String SCREENING = 'Screening';
    private static final String HHN = 'Home Health Nurse';
    private static final String NA = 'N/A';
    private static final String PHLEBOTOMIST = 'Phlebotomist';


    private static final String BASELINE_TN_CODE = '300';
    private static final String ONBOARDING_TN_CODE = '002';

    // !! commented by Vlad Tyazhov as containsKey caused CaseHandler error (null pointer exception for firstHHNFounds)
    // private static Map<String,Set<Id>> firstPhlebotomistFounds
    // private static Map<String,Set<Id>> firstHHNFounds;
    private static Map<String,Set<Id>> firstPhlebotomistFounds = new Map<String,Set<Id>>();
    private static Map<String,Set<Id>> firstHHNFounds = new Map<String,Set<Id>>();


    private static Map<Id, VTD1_Actual_Visit__c> baselineVisits;
    private static Map<Id, String> screeningVisitsByCaseId;

    /** @description: method to schedule VTD1_Actual_Visit__c and generate task if needed
     * @param caseIdsByVisitType takes visit types: 'Baseline Visit', 'Not Onboarding Visit', 'Protocol visit', 'Protocol Baseline Visit', as a key and
      * related Set<> of Case Ids as a value */
    public static void doVisitSchedule(Map<String, Set<Id>> caseIdsByVisitType) {

        List<Id> allCaseIds = new List<Id>();
        Set<String> visitTypes = new Set<String>();
        for (String visitType : caseIdsByVisitType.keySet()) {
            visitTypes.add(visitType);
            allCaseIds.addAll(caseIdsByVisitType.get(visitType));
        }

        for (VTD1_Actual_Visit__c v : getAllVisitsByCaseId(allCaseIds)) {
            for (String vType : visitTypes) {
                if (caseIdsByVisitType.get(vType).contains(v.VTD1_Case__c)) {
                    if (vType == BASELINE_VISIT) {
                        if (v.VTD1_Status__c == FUTURE_VISIT && v.VTD1_Onboarding_Type__c == BASELINE_VISIT) {
                            v.VTD1_Status__c = TO_BE_SCHEDULED;
                            if (baselineVisits == null) {
                                baselineVisits = new Map<Id, VTD1_Actual_Visit__c>();
                            }
                            baselineVisits.put(v.Id, v);
                            updateVisits.add(v);
                        } else if (v.VTD1_Onboarding_Type__c == SCREENING) {
                            if (screeningVisitsByCaseId == null) {
                                screeningVisitsByCaseId = new Map<Id, String>();
                            }
                            screeningVisitsByCaseId.put(v.VTD1_Case__c, v.VTR2_Modality__c);
                        }
                    } else if (v.VTR3_LTFUStudyVisit__c && v.VTD1_Status__c != CANCELLED) {
                        if (vType == NOT_ONBOARDING_VISIT
                                || (vType == PROTOCOL_BASELINE_VISIT && v.VTD2_Baseline_Visit__c)
                                || (vType == PROTOCOL_VISIT && v.VTD1_Onboarding_Type__c != NA && !v.VTD2_Baseline_Visit__c && v.VTD1_Status__c != COMPLETED)) {
                            scheduleNonBaselineVisits(v, vType);
                        }
                    }
                }
            }
        }

        if (baselineVisits != null) {
            VT_D2_TNCatalogTasks.generateTasks(new List<String>{
                    BASELINE_TN_CODE
            }, new List<Id>(baselineVisits.keySet()), null, calculateTaskAssignee(new List<VTD1_Actual_Visit__c>(baselineVisits.values()), screeningVisitsByCaseId));
        }
        update updateVisits;
    }
    private static List<VTD1_Actual_Visit__c> getAllVisitsByCaseId(List<Id> caseIds){
        return [
                SELECT Id,VTD1_Case__c,VTD1_Status__c,
                        VTD1_Case__r.VTD1_Primary_PG__c,
                        VTD1_Case__r.VTR2_SiteCoordinator__c,
                        VTD1_Case__r.VTD1_Study__r.VTR2_Site_Staff_Responsible_for_Elgbl__c,
                        To_Be_Scheduled_Date__c,
                        VTD2_Schedule_Notification_Time_Formula__c,
                        VTR2_f_ScheduleNotificationPT__c,
                        VTD1_Visit_Type__c,
                        Sub_Type__c,VTD1_Visit_Number__c,
                        VTD2_Baseline_Visit__c,
                        VTR2_Modality__c,
                        VTD1_Schedule_Visit_Task_Date__c,
                        VTD1_Onboarding_Type__c,
                        VTR2_f_OnbordingVisitOrderNumber__c,
                        VTR3_LTFUStudyVisit__c
                FROM VTD1_Actual_Visit__c
                WHERE VTD1_Case__c IN :caseIds
        ];
        //  AND VTD1_Case__r.VTD1_Study__r.VTR5_IRTSystem__c != 'Signant'
    }

    /** @description: method to be called to schedule VTD1_Actual_Visit__c that is in the next visitOrderNumber
       * @param vNumberOrderByCaseId takes an Id of VTD1_Actual_Visit__c as a key and visitOrderNumber of this visit as a value */
    public static void doVisitSchedule(Map<Id,Integer> vNumberOrderByCaseId){
        Map<String, VTD1_Actual_Visit__c> onboardingVisitsId = new Map<String, VTD1_Actual_Visit__c>();
        for(VTD1_Actual_Visit__c v : getAllVisitsByCaseId(new List<Id>(vNumberOrderByCaseId.keySet()))) {
            if ((v.VTD1_Onboarding_Type__c != BASELINE_VISIT && v.VTD1_Onboarding_Type__c != NA)
                    && v.VTD1_Status__c == FUTURE_VISIT
                    && (v.VTR2_f_OnbordingVisitOrderNumber__c != null
                    && v.VTR2_f_OnbordingVisitOrderNumber__c == vNumberOrderByCaseId.get(v.VTD1_Case__c) + 1)) {
                if (onboardingVisitsId == null) {
                    onboardingVisitsId = new Map<String, VTD1_Actual_Visit__c>();
                }
                v.VTD1_Status__c = TO_BE_SCHEDULED;
                onboardingVisitsId.put(v.Id, v);
            }
        }
        if (onboardingVisitsId != null) {
            VT_D2_TNCatalogTasks.generateTasks(new List<String>{
                    ONBOARDING_TN_CODE
            }, new List<String>(onboardingVisitsId.keySet()), null, null);
        }
        update onboardingVisitsId.values();

    }
    public static void scheduleNonBaselineVisits(VTD1_Actual_Visit__c vst, String vType) {
        if ((vType == PROTOCOL_VISIT || vType == PROTOCOL_BASELINE_VISIT)
                && vst.VTD1_Visit_Number__c == 'v1' && vst.VTD2_Baseline_Visit__c) {
            vst.VTD1_Schedule_Visit_Task_Date__c = Date.today();
            updateVisits.add(vst);
        } else if (vst.VTD1_Visit_Type__c == STUDY_TEAM) {

            vst.VTD1_Schedule_Visit_Task_Date__c = (vst.VTD2_Schedule_Notification_Time_Formula__c != null && vst.To_Be_Scheduled_Date__c != null)
                    ? vst.To_Be_Scheduled_Date__c - Integer.valueOf(vst.VTD2_Schedule_Notification_Time_Formula__c)
                    : null;

            vst.VTR2_ScheduleVisitTaskPatient__c = (vst.VTR2_f_ScheduleNotificationPT__c != null && vst.To_Be_Scheduled_Date__c != null)
                    ? vst.To_Be_Scheduled_Date__c - Integer.valueOf(vst.VTR2_f_ScheduleNotificationPT__c) : null;
            updateVisits.add(vst);

        } else if (vst.VTD1_Visit_Type__c == HCP) {
            vst.VTD1_Schedule_Visit_Task_Date__c = Date.today();
            updateVisits.add(vst);

        } else if (vst.Sub_Type__c == PHLEBOTOMIST && vst.VTD1_Visit_Type__c == LABS) {
            if (!firstPhlebotomistFounds.containsKey(vType)) {
                firstPhlebotomistFounds.put(vType, new Set<Id>());
                // firstPhlebotomistFounds = new Map<String,Set<Id>>{vType => new Set<Id>()};  // commented by Vlad Tyazhov as containsKey caused CaseHandler error
            }
            if (vst.To_Be_Scheduled_Date__c != null) {
                vst.VTD1_Schedule_Visit_Task_Date__c = !firstPhlebotomistFounds.get(vType).contains(vst.VTD1_Case__c) ?
                        vst.To_Be_Scheduled_Date__c - 21 : vst.To_Be_Scheduled_Date__c - 14;
            } else {
                vst.VTD1_Schedule_Visit_Task_Date__c = null;
            }
            firstPhlebotomistFounds.get(vType).add(vst.VTD1_Case__c);
            updateVisits.add(vst);

        } else if (vst.Sub_Type__c == HHN && vst.VTD1_Visit_Type__c == LABS) {
            if (!firstHHNFounds.containsKey(vType)) {
                firstHHNFounds.put(vType, new Set<Id>());
                // firstHHNFounds = new Map<String,Set<Id>>{vType => new Set<Id>()};  // commented by Vlad Tyazhov as containsKey caused CaseHandler error
            }
            System.debug('sf: vst: ' + vst);
            System.debug('sf: vst.To_Be_Scheduled_Date__c: ' + vst.To_Be_Scheduled_Date__c);
            System.debug('sf: firstHHNFounds: ' + firstHHNFounds);
            System.debug('sf: firstHHNFounds.get(vType): ' + firstHHNFounds.get(vType));
            System.debug('sf: vst.VTD1_Case__c: ' + vst.VTD1_Case__c);
            if (vst.To_Be_Scheduled_Date__c != null) {
                vst.VTD1_Schedule_Visit_Task_Date__c = !firstHHNFounds.get(vType).contains(vst.VTD1_Case__c) ?
                        vst.To_Be_Scheduled_Date__c - 21 : vst.To_Be_Scheduled_Date__c - 14;
            } else {
                vst.VTD1_Schedule_Visit_Task_Date__c = null;
            }
            firstHHNFounds.get(vType).add(vst.VTD1_Case__c);
            updateVisits.add(vst);

        } else if (vst.Sub_Type__c == PSC && vst.VTD1_Visit_Type__c == LABS) {
            vst.VTD1_Schedule_Visit_Task_Date__c = (vst.To_Be_Scheduled_Date__c != null) ? vst.To_Be_Scheduled_Date__c - 14 : null;
            updateVisits.add(vst);
        }
        System.debug('updateVisits ' + updateVisits);
    }

    private static Map<String, String> calculateTaskAssignee(List<VTD1_Actual_Visit__c> baselineVisits, Map<Id, String> visitModalityOnCaseIds) {
        Map<String, String> assignedTo = new Map<String, String>();
        for (VTD1_Actual_Visit__c v : baselineVisits) {
            String assignKey = BASELINE_TN_CODE + v.Id;
            Id pgId = v.VTD1_Case__r.VTD1_Primary_PG__c;
            Id scrId = v.VTD1_Case__r.VTR2_SiteCoordinator__c;
            if (v.VTD1_Case__r.VTD1_Study__r.VTR2_Site_Staff_Responsible_for_Elgbl__c) {
                assignedTo.put(assignKey, scrId);
            } else if ((pgId != null && scrId != null) && visitModalityOnCaseIds.containsKey(v.VTD1_Case__c)) {
                if (visitModalityOnCaseIds.get(v.VTD1_Case__c) == AT_LOCATION) {
                    assignedTo.put(assignKey, scrId);
                } else {
                    assignedTo.put(assignKey, pgId);
                }
            } else if (pgId != null) {
                assignedTo.put(assignKey, pgId);
            } else {
                assignedTo.put(assignKey, scrId);
            }
        }
        return assignedTo;
    }
}