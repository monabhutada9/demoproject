/**
 * Created by Dmitry Kovalev on 29.08.2020.
 * Release 5.1, Sprint BugFix
 */
public with sharing class VT_R5_GVEmailTemplateController {
    /**
     * Populates from apex:attribute tag, relatedTo.Id value from email template.
     */
    public Id relToId { get; set; }
    /**
     * Populates from apex:attribute tag, recipient.Id value from email template.
     */
    public Id recipId { get; set; }
    /**
     * According to existing logic within 6 email templates, Id refers to 'VTD1_Conference_Member__c'
     * SObjectType only if the email sends about televisit.
     */
    public Boolean isTelevisit {
        get {
            return this.isRelatedToConferenceMember;
        }
    }
    /**
     * Returns true, if 'relatedToId' refers to 'VTD1_Conference_Member__c' SObjectType.
     */
    private Boolean isRelatedToConferenceMember {
        get {
            return this.relToId.getSobjectType().getDescribe().getName() == 'VTD1_Conference_Member__c';
        }
    }
    /**
     * Real email recipient name.
     */
    public String externalMemberName {
        get {
            return generalVisitMember.VTR4_External_Member_Name__c;
        }
    }
    /**
     * Name of the scheduled General Visit.
     */
    public String generalVisitName {
        get {
            return generalVisitMember.VTR4_General_Visit__r.Name;
        }
    }
    /**
     * Reason for participation in the General Visit.
     */
    public String participationReason {
        get {
            return generalVisitMember.VTR4_ReasonForParticipation__c;
        }
    }
    /**
     * By using this URL, external member can be connected to scheduled televisit.
     */
    public String televisitUrl {
        get {
            return ExternalVideoController.getVideoDataForExternal(this.conferenceMember.VTD1_Video_Conference__c, this.conferenceMember.VTD1_Video_Conference__r.SessionId__c, this.conferenceMember.Id);
        }
    }
    /**
     * Day of the week in the recipient timezone of scheduled visit date and time.
     */
    public String scheduledDay {
        get {
            return scheduledDatetime.format('EEEE', recipient.VTD2_UserTimezone__c);
        }
    }
    /**
     * Date in 'MM/dd/yyyy' format in the recipient timezone of scheduled visit date and time.
     */
    public String scheduledDate {
        get {
            return scheduledDatetime.format('MM/dd/yyyy', recipient.VTD2_UserTimezone__c);
        }
    }
    /**
     * Time in 'h:mm a' format in the recipient timezone of scheduled visit date and time.
     */
    public String scheduledTime {
        get {
            return scheduledDatetime.format('h:mm a', recipient.VTD2_UserTimezone__c);
        }
    }
    /**
     * Date and time of scheduled General Visit.
     */
    private Datetime scheduledDatetime {
        get {
            return this.generalVisitMember.VTR4_General_Visit__r.VTR4_Scheduled_Date_Time__c;
        }
    }
    /**
     * If member SObjectType is 'VTR4_General_Visit_Member__c', getter returns SObject of the specified SObjectType.
     * Otherwise getter returns conferenceMember.VTR4_General_Visit_Member__r.
     */
    public VTR4_General_Visit_Member__c generalVisitMember {
        get {
            if (isRelatedToConferenceMember) {
                return conferenceMember.VTR4_General_Visit_Member__r;
            } else {
                return ((List<VTR4_General_Visit_Member__c>) member).get(0);
            }
        }
    }
    /**
     * If member SObjectType is VTD1_Conference_Member__c, getter returns SObject of the specified SObjectType.
     * Otherwise throws System.TypeException: Invalid conversion from runtime type ...
     */
    public VTD1_Conference_Member__c conferenceMember {
        get {
            return ((List<VTD1_Conference_Member__c>) member).get(0);
        }
    }
    /**
     * Returns SObject with necessary fields and appropriate SObjectType, based on relatedTo.Id SObjectType.
     */
    private Object member {
        get {
            if (member == null) {
                if (isRelatedToConferenceMember) {
                    member = new VT_Stubber.ResultStub('VT_R5_GVEmailTemplateController.confMember', [
                        SELECT
                            VTR4_General_Visit_Member__r.VTR4_External_Member_Name__c,
                            VTR4_General_Visit_Member__r.VTR4_General_Visit__r.Name,
                            VTR4_General_Visit_Member__r.VTR4_General_Visit__r.VTR4_Scheduled_Date_Time__c,
                            VTR4_General_Visit_Member__r.VTR4_ReasonForParticipation__c,
                            VTD1_Video_Conference__c, VTD1_Video_Conference__r.SessionId__c
                        FROM VTD1_Conference_Member__c
                        WHERE Id = :this.relToId
                    ]).getResult();
                } else {
                    member = new VT_Stubber.ResultStub('VT_R5_GVEmailTemplateController.gVisitMember', [
                        SELECT
                            VTR4_External_Member_Name__c,
                            VTR4_General_Visit__r.Name,
                            VTR4_General_Visit__r.VTR4_Scheduled_Date_Time__c,
                            VTR4_ReasonForParticipation__c
                        FROM VTR4_General_Visit_Member__c
                        WHERE Id = :this.relToId
                    ]).getResult();
                }
            }
            return member;
        }
        private set;
    }
    /**
     * Since this email is for an external user who may not be registered in Salesforce Org,
     * the "recipient" is not really the recipient of the email, but rather the initiator of the visit.
     */
    public User recipient {
        get {
            if (recipient == null) {
                recipient = [
                    SELECT Id, VTD2_UserTimezone__c, FirstName, LastName, Email
                    FROM User
                    WHERE Id = :this.recipId
                ];
            }
            return recipient;
        }
        private set;
    }
}