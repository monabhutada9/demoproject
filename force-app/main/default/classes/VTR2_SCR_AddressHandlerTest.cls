@IsTest
private class VTR2_SCR_AddressHandlerTest {

    @IsTest
    static void insertFirstAddressSuccess() {
        DomainObjects.Address_t address = new DomainObjects.Address_t()
                .addContact(new DomainObjects.Contact_t())
                .setPrimary(true);

        Test.startTest();
        address.persist();
        Test.stopTest();

        System.assertEquals(1, [SELECT Id FROM Address__c].size());
    }

    @IsTest
    static void insertFirstAddressFail() {
        DomainObjects.Address_t address = new DomainObjects.Address_t()
                .addContact(new DomainObjects.Contact_t())
                .setPrimary(false);

        String dmlMessage;
        Test.startTest();
        try {
            address.persist();
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        Test.stopTest();

        System.assertEquals(0, [SELECT Id FROM Address__c].size());
        System.assertEquals(Label.VTR2_SCR_Patient_Should_Have_Primary_Address, dmlMessage);
    }

    @IsTest
    static void insertSecondAddressSuccess() {
        DomainObjects.Contact_t contact = new DomainObjects.Contact_t();

        new DomainObjects.Address_t()
                .addContact(contact)
                .setPrimary(true)
                .persist();

        DomainObjects.Address_t secondAddress = new DomainObjects.Address_t()
                .addContact(contact)
                .setPrimary(false);


        Test.startTest();
        secondAddress.persist();
        Test.stopTest();

        System.assertEquals(2, [SELECT Id FROM Address__c].size());
    }

    @IsTest
    static void insertSecondAddressFail() {
        DomainObjects.Contact_t contact = new DomainObjects.Contact_t();

        new DomainObjects.Address_t()
                .addContact(contact)
                .setPrimary(true)
                .persist();

        DomainObjects.Address_t secondAddress = new DomainObjects.Address_t()
                .addContact(contact)
                .setPrimary(true);

        String dmlMessage;
        Test.startTest();
        try {
            secondAddress.persist();
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        Test.stopTest();

        System.assertEquals(1, [SELECT Id FROM Address__c].size());
        System.assertEquals(Label.VTR2_SCR_One_Primary_Address_Allowed, dmlMessage);
    }

    @IsTest
    static void updateAddressSuccess() {
        DomainObjects.Address_t address = new DomainObjects.Address_t()
                .addContact(new DomainObjects.Contact_t())
                .setPrimary(true);
        address.persist();

        address.setAddressLine1('New Address Line 1');

        String dmlMessage;
        Test.startTest();
        try {
            update address.toObject();
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        Test.stopTest();

        System.assertEquals(null, dmlMessage);
    }

    @IsTest
    static void updateAddressFail() {
        DomainObjects.Address_t address = new DomainObjects.Address_t()
                .addContact(new DomainObjects.Contact_t())
                .setPrimary(true);
        address.persist();

        address.setPrimary(false);

        String dmlMessage;
        Test.startTest();
        try {
            update address.toObject();
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        Test.stopTest();

        System.assertEquals(Label.VTR2_SCR_Patient_Should_Have_Primary_Address, dmlMessage);
    }

    @IsTest
    static void updateSecondAddressSuccess() {
        DomainObjects.Contact_t contact = new DomainObjects.Contact_t();

        DomainObjects.Address_t firstAddress = new DomainObjects.Address_t()
                .addContact(contact)
                .setPrimary(true);
        firstAddress.persist();

        DomainObjects.Address_t secondAddress = new DomainObjects.Address_t()
                .addContact(contact)
                .setPrimary(false);
        secondAddress.persist();

        secondAddress.setPrimary(true);

        String dmlMessage;
        Test.startTest();
        try {
            update secondAddress.toObject();
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        Test.stopTest();

        System.assertEquals(null, dmlMessage);
        System.assertEquals(1, [SELECT Id FROM Address__c WHERE Id = :secondAddress.id AND VTD1_Primary__c = TRUE].size());
        System.assertEquals(1, [SELECT Id FROM Address__c WHERE Id = :firstAddress.id AND VTD1_Primary__c = FALSE].size());
    }

    @IsTest
    static void updateSecondAddressChangeContactSuccess() {
        DomainObjects.Contact_t firstContact = new DomainObjects.Contact_t();
        DomainObjects.Contact_t secondContact = new DomainObjects.Contact_t();

        DomainObjects.Address_t firstAddress = new DomainObjects.Address_t()
                .addContact(firstContact)
                .setPrimary(true);
                firstAddress.persist();

        DomainObjects.Address_t secondAddress = new DomainObjects.Address_t()
                .addContact(firstContact)
                .setPrimary(false);
                secondAddress.persist();

        secondAddress.setContact(secondContact.id);
        secondAddress.setPrimary(true);

        String dmlMessage;
        Test.startTest();
        try {
            update secondAddress.toObject();
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        Test.stopTest();
        System.assertEquals(null, dmlMessage);
        System.assertEquals(1, [SELECT Id FROM Address__c WHERE VTD1_Contact__c = :firstContact.id].size());
        System.assertEquals(1, [SELECT Id FROM Address__c WHERE VTD1_Contact__c = :secondContact.id].size());
    }

    @IsTest
    static void updateSecondAddressChangeContactFail() {
        DomainObjects.Contact_t firstContact = new DomainObjects.Contact_t();
        DomainObjects.Contact_t secondContact = new DomainObjects.Contact_t();

        DomainObjects.Address_t firstAddress = new DomainObjects.Address_t()
                .addContact(firstContact)
                .setPrimary(true);
        firstAddress.persist();

        DomainObjects.Address_t secondAddress = new DomainObjects.Address_t()
                .addContact(firstContact)
                .setPrimary(false);
        secondAddress.persist();

        secondAddress.setContact(secondContact.id);

        String dmlMessage;
        Test.startTest();
        try {
            update secondAddress.toObject();
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        Test.stopTest();

        System.assertEquals(Label.VTR2_SCR_Patient_Should_Have_Primary_Address, dmlMessage);
        System.assertEquals(2, [SELECT Id FROM Address__c WHERE VTD1_Contact__c = :firstContact.id].size());
        System.assertEquals(0, [SELECT Id FROM Address__c WHERE VTD1_Contact__c = :secondContact.id].size());
    }

    @IsTest
    static void updatePrimaryAddressChangeContactFail() {
        DomainObjects.Contact_t firstContact = new DomainObjects.Contact_t();
        DomainObjects.Contact_t secondContact = new DomainObjects.Contact_t();
        secondContact.persist();

        DomainObjects.Address_t firstAddress = new DomainObjects.Address_t()
                .addContact(firstContact)
                .setPrimary(true);
        firstAddress.persist();

        new DomainObjects.Address_t()
                .addContact(firstContact)
                .setPrimary(false)
                .persist();

        firstAddress.setContact(secondContact.id);

        String dmlMessage;
        Test.startTest();
        try {
            update firstAddress.toObject();
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        Test.stopTest();

        System.assertEquals(Label.VTR2_SCR_Patient_Should_Have_Primary_Address, dmlMessage);
        System.assertEquals(2, [SELECT Id FROM Address__c WHERE VTD1_Contact__c = :firstContact.id].size());
        System.assertEquals(0, [SELECT Id FROM Address__c WHERE VTD1_Contact__c = :secondContact.id].size());
    }

    @IsTest
    static void updatePrimaryAddressChangeContactSuccess() {
        DomainObjects.Contact_t firstContact = new DomainObjects.Contact_t();
        DomainObjects.Contact_t secondContact = new DomainObjects.Contact_t();
        secondContact.persist();

        DomainObjects.Address_t firstAddress = new DomainObjects.Address_t()
                .addContact(firstContact)
                .setPrimary(true);
        firstAddress.persist();

        DomainObjects.Address_t secondAddress = new DomainObjects.Address_t()
                .addContact(firstContact)
                .setPrimary(false);
        secondAddress.persist();

        firstAddress.setContact(secondContact.id);
        secondAddress.setPrimary(true);

        String dmlMessage;
        Test.startTest();
        try {
            update new List<Address__c> {(Address__c) firstAddress.toObject(), (Address__c) secondAddress.toObject()};
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        Test.stopTest();

        System.assertEquals(null, dmlMessage);
        System.assertEquals(1, [SELECT Id FROM Address__c WHERE VTD1_Contact__c = :firstContact.id].size());
        System.assertEquals(1, [SELECT Id FROM Address__c WHERE VTD1_Contact__c = :secondContact.id].size());
    }

    @IsTest
    static void updatePrimaryAddressChangeContactSuccess1() {
        DomainObjects.Contact_t firstContact = new DomainObjects.Contact_t();
        DomainObjects.Contact_t secondContact = new DomainObjects.Contact_t();

        DomainObjects.Address_t firstAddress = new DomainObjects.Address_t()
                .addContact(firstContact)
                .setPrimary(true);
        firstAddress.persist();

        DomainObjects.Address_t secondAddress = new DomainObjects.Address_t()
                .addContact(firstContact)
                .setPrimary(false);
        secondAddress.persist();

        new DomainObjects.Address_t()
                .addContact(secondContact)
                .setPrimary(true)
                .persist();

        secondAddress.setContact(secondContact.id);

        String dmlMessage;
        Test.startTest();
        try {
            update secondAddress.toObject();
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        Test.stopTest();

        System.assertEquals(null, dmlMessage);
        System.assertEquals(1, [SELECT Id FROM Address__c WHERE VTD1_Contact__c = :firstContact.id].size());
        System.assertEquals(2, [SELECT Id FROM Address__c WHERE VTD1_Contact__c = :secondContact.id].size());
    }

    @IsTest
    static void updatePrimaryAddressChangeContactFail1() {
        DomainObjects.Contact_t firstContact = new DomainObjects.Contact_t();
        DomainObjects.Contact_t secondContact = new DomainObjects.Contact_t();

        DomainObjects.Address_t firstAddress = new DomainObjects.Address_t()
                .addContact(firstContact)
                .setPrimary(true);
        firstAddress.persist();

        DomainObjects.Address_t secondAddress = new DomainObjects.Address_t()
                .addContact(firstContact)
                .setPrimary(false);
        secondAddress.persist();

        new DomainObjects.Address_t()
                .addContact(secondContact)
                .setPrimary(true)
                .persist();

        secondAddress.setContact(secondContact.id);
        secondAddress.setPrimary(true);

        String dmlMessage;
        Test.startTest();
        try {
            update secondAddress.toObject();
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        Test.stopTest();

        System.assertEquals(Label.VTR2_SCR_One_Primary_Address_Allowed, dmlMessage);
        System.assertEquals(2, [SELECT Id FROM Address__c WHERE VTD1_Contact__c = :firstContact.id].size());
        System.assertEquals(1, [SELECT Id FROM Address__c WHERE VTD1_Contact__c = :secondContact.id].size());
    }

    @IsTest
    static void deleteAddressSuccess() {
        DomainObjects.Contact_t contact = new DomainObjects.Contact_t();

        new DomainObjects.Address_t()
                .addContact(contact)
                .setPrimary(true)
                .persist();

        DomainObjects.Address_t address = new DomainObjects.Address_t()
                .addContact(contact)
                .setPrimary(false);
        address.persist();

        String dmlMessage;
        Test.startTest();
        try {
            delete address.toObject();
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        Test.stopTest();

        System.assertEquals(null, dmlMessage);
        System.assertEquals(1, [SELECT Id FROM Address__c].size());
    }

    @IsTest
    static void deleteAddressFail() {
        DomainObjects.Contact_t contact = new DomainObjects.Contact_t();

        DomainObjects.Address_t address = new DomainObjects.Address_t()
                .addContact(contact)
                .setPrimary(true);
        address.persist();

        new DomainObjects.Address_t()
                .addContact(contact)
                .setPrimary(false)
                .persist();

        String dmlMessage;
        Test.startTest();
        try {
            delete address.toObject();
        } catch (DmlException ex) {
            dmlMessage = ex.getDmlMessage(0);
        }
        Test.stopTest();

        System.assertEquals(Label.VTR2_SCR_Patient_Should_Have_Primary_Address, dmlMessage);
        System.assertEquals(2, [SELECT Id FROM Address__c].size());
    }

}