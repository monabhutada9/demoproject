/*
 * Author: Sunil BP.
 * Date: 19 Aug 2020 
 * Jira Issue: SH-14661
 * Desription: This batch class is used to update the Preferred Language on Conact(CAREGIVER, PATIENT) from Virtual site. One time Batch.
*/
public class VT_R5_UpdatePreferredLangOnContactBatch implements Database.Batchable<sObject>, Database.Stateful{
    map<Id, String> mapFailedContact = new map<Id,String>();
    public Database.QueryLocator start(Database.BatchableContext BC){
        set<String> setRecordType = new set<String>{VT_R4_ConstantsHelper_RecordTypes.RECORD_TYPE_ID_CONTACT_PATIENT, VT_R4_ConstantsHelper_RecordTypes.RECORD_TYPE_ID_CONTACT_CAREGIVER};
            String query = 'SELECT Id, VTR2_Primary_Language__c, VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c, VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_DefaultIRBLanguage__c FROM Contact WHERE RecordTypeId IN: setRecordType AND VTD1_Clinical_Study_Membership__c != null';
        System.debug('query'+ query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC,  List<sObject> scope){
        List<Contact> lstContactToUpdate = new List<Contact>();
        try{
            for(Contact contact : (List<Contact>)scope){
                if(isValidForLanguageUpdate(contact) ){
                    contact.VTR2_Primary_Language__c = contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_DefaultIRBLanguage__c;
                    lstContactToUpdate.add(contact);
                }
            }
            system.debug('lstContactToUpdate.size'+lstContactToUpdate.size());
            if(lstContactToUpdate.size() > 0){
                Database.SaveResult[] srList = Database.update(lstContactToUpdate, false);
                List<Database.Error> errors = new List<Database.Error>();
                Integer recordId = 0;
                for (Database.SaveResult sr : srList) {
                    if (!sr.isSuccess()) {
                        for(Database.Error err : sr.getErrors()) {
                            mapFailedContact.put(lstContactToUpdate[recordId].Id, err.getMessage());
                            errors.add(err);
                        }
                    }
                    recordId++;
                }
                system.debug('errorserrors'+errors);
                if(!errors.isEmpty()){
                    ErrorLogUtility.logErrorsInBulk(srList,lstContactToUpdate, null, 'VTR5_UpdatePreferredLangOnContactBatch');
                }
            }
        }Catch(Exception ex){
            System.debug('StackTraceString : '+ex.getStackTraceString());
            ErrorLogUtility.logException(ex, null, 'VTR5_UpdatePreferredLangOnContactBatch');
        }
    }
    /*
     * Description: This method is used o confirm that the contact is eligible for Language update or not.
     * Params:  Contact: Conact record to check the eligiblity.
     * Return Type: Boolean.
	*/
    private Boolean isValidForLanguageUpdate(Contact contact){
        if(contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__c != null && contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c != null && contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_DefaultIRBLanguage__c != null){
            List<String> lstPiclistValues = contact.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__r.VTR5_IRBApprovedLanguages__c.split(';');
            set<String> setIRBApprovedLanguages = new set<String>();
            setIRBApprovedLanguages.addAll(lstPiclistValues);
            if(!setIRBApprovedLanguages.contains(contact.VTR2_Primary_Language__c)){
                return true;
            }                
        }
        return false;
    }
    /*
     * Description: This method is used to send the failed record details over email.
    */
    public void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        string[] to = new string[] {Userinfo.getUserEmail()};
            email.setToAddresses(to);
        email.setSubject('Failed contact preferred language update details');
        email.setHtmlBody(getTableBody(mapFailedContact));
        if(mapFailedContact.keyset().size()> 0 && !Test.isRunningTest()){
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }
        
        
    }
     /*
     * Description: This method is used to create the email body
     * Params:  mapFailedContact: Contact record to its excetion message.
     * Return Type: String.
	*/
    
    private static string getTableBody(Map<Id, String> mapFailedContact){
        String htmlBody = '';
        htmlBody = '<table border="1" style="border-collapse: collapse"><caption>Contact Preferred language update summary </caption><tr><th>Contact Id</th><th>Error Message</th></tr>';
        for(Id contactId : mapFailedContact.keySet()){
            htmlBody += '<tr><td>' + '<a href='+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+contactId+'>'+contactId+'</a>' + '</td><td>' + mapFailedContact.get(contactId) + '</td></tr>';
        }
        htmlBody += '</table>';
        return htmlBody;
    }
    
}