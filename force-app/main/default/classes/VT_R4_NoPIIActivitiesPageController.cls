/**
 * @author: Dmitry Yartsev
 * @date: 05.02.2020
 * @description: Controller for vt_r4_no_pii_activities_page
 */
public without sharing class VT_R4_NoPIIActivitiesPageController {
    /**
     * Task SObject type
     *
     * @type {SObjectType}
     *
     * @see SObjectType
     * @see Task
     */
    private static final SObjectType TASK_SOBJECT_TYPE = Task.getSObjectType();
    /**
     * Event SObject type
     *
     * @type {SObjectType}
     *
     * @see SObjectType
     * @see Event
     */
    private static final SObjectType EVENT_SOBJECT_TYPE = Event.getSObjectType();
    /**
     * A set of profiles whose usernames should be hidden
     *
     * @type {Set<String>}
     */
    private static final Set<String> PROFILES_FOR_HIDE_PII = new Set<String>{
            'Patient', 'Caregiver'
    };
    /**
     * Get a list of profile IDs whose usernames should be hidden
     *
     * @return List<Id>
     */
    @AuraEnabled(Cacheable = true)
    public static List<Id> getPtCgProfileIds() {
        return new List<Id>(new Map<Id, Profile>([
                SELECT Id
                FROM Profile
                WHERE Name IN :PROFILES_FOR_HIDE_PII
        ]).keySet());
    }
    /**
     * Get data for Activity record
     *
     * @param recordId - Activity record ID
     *
     * @return {String} - Serialized Map<String, Object> with the found Activity record data
     * @return {null} - Returned if recordId is not Task/Event ID
     *
     * @see getTask
     * @see getEvent
     */
    @AuraEnabled
    public static String getActivityRecord(Id recordId) {
        SObjectType recSObjType = recordId.getSobjectType();
        if (recSObjType == TASK_SOBJECT_TYPE) {
            return JSON.serialize(getTask(recordId));
        } else if (recSObjType == EVENT_SOBJECT_TYPE) {
            return JSON.serialize(getEvent(recordId));
        }
        return null;
    }
    /**
     * Update Activity record
     *
     * @param record - A serialized Activity record
     *
     * @return {SObject} - Updated record
     *
     * @see SObject
     */
    @AuraEnabled
    public static SObject updateActivity(String record) {
        SObject recordToUpdate = (SObject) JSON.deserialize(record, SObject.class);
        update recordToUpdate;
        return getSObjectWithoutOwnerPII(recordToUpdate);
    }
    /**
     * Get Task data
     *
     * @param taskId - Task record ID
     *
     * @return Map<String, Object> with the the found Task record data
     */
    private static Map<String, Object> getTask(Id taskId) {
        Task record = [
                SELECT Subject,
                        Status,
                        Priority,
                        Category__c,
                        What.Name,
                        WhatId,
                        Document__r.Name,
                        ActivityDate,
                        VTR5_VirtualSite__r.Name,
                        HealthCloudGA__CarePlanTemplate__r.VTD1_Protocol_Nickname__c,
                         HealthCloudGA__CarePlanTemplate__r.Id,
                        Owner.Id,
                        Owner.Name,
                        Owner.Profile.Name
                FROM Task
                WHERE Id = :taskId
        ];
        return new Map<String, Object>{
                'record' => getSObjectWithoutOwnerPII(record),
                'statusOptions' => getTaskStatuses()
              //'available Owners' => getAvailableOwners()
        };
    }
    /**
     * Get Event data
     *
     * @param eventId - Event record ID
     *
     * @return Map<String, Object> with the the found Event record data
     *
     * @see Event
     */
    private static Map<String, Object> getEvent(Id eventId) {
        Event record = [
                SELECT Subject,
                        VTR2_Monitoring_Visit_del__r.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__r.VTR2_AddressLine1__c,
                        VTR2_Monitoring_Visit_del__r.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__r.VTR3_AddressLine_2__c,
                        VTR2_Monitoring_Visit_del__r.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__r.VTR3_AddressLine_3__c,
                        VTR2_Monitoring_Visit_del__r.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__r.VTR2_City__c,
                        VTR2_Monitoring_Visit_del__r.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__r.VTR2_State__c,
                        VTR2_Monitoring_Visit_del__r.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__r.VTR2_ZIP__c,
                        VTR2_Monitoring_Visit_del__r.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__r.VTR3_Country__c,
                        VTR2_Redirect_Status__c,
                        FORMAT(StartDateTime) startDate,
                        FORMAT(EndDateTime) endDate,
                        Owner.Name,
                        Owner.Profile.Name
                FROM Event
                WHERE Id = :eventId
        ];
        return new Map<String, Object>{
                'record' => getSObjectWithoutOwnerPII(record),
                'siteAddress' => getSiteAddress(record)
        };
    }
    /**
     * Remove Owner PII
     *
     * @param record - instance of SObject
     *
     * @return {SObject} - instance of SObject with removed PII form Owner
     *
     * @see User
     */
    private static SObject getSObjectWithoutOwnerPII(SObject record) {
        if (record.getSObject('Owner') != null && PROFILES_FOR_HIDE_PII.contains(String.valueOf(record.getSObject('Owner').getSObject('Profile').get('Name')))) {
            Map<String, Object> ownerMap = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(record.getSObject('Owner')));
            ownerMap.put('Name', record.getSObject('Owner').getSObject('Profile').get('Name'));
            record.putSObject('Owner', (SObject) JSON.deserialize(JSON.serialize(ownerMap), User.class));
        }
        return record;
    }
    /**
     * Get picklist values for Task Status field
     *
     * @return {List<Map<String, String>>} - A List of Map<String, String> with label and value for each picklist value
     *
     * @see Task.Status
     */
    private static List<Map<String, String>> getTaskStatuses() {
        List<Map<String, String>> taskStatuses = new List<Map<String, String>>();
        Map<String, String> taskStatusesMap = VT_D1_CustomMultiPicklistController.getSelectOptionsWithLabel(new Task(), 'Status');
        for (String value : taskStatusesMap.keySet()) {
            taskStatuses.add(
                    new Map<String, String>{
                            'label' => taskStatusesMap.get(value),
                            'value' => value
                    }
            );
        }
        return taskStatuses;
    }
    /**
     * Get formatted Site Address for display in <c-lightning-formatted-address> component
     *
     * @param record - Event record
     *
     * @return {Map<String, String>} - formatted map that contains street, city, country, province and zipCode
     *
     * @see VTR2_Site_Address__c
     */
    private static Map<String, String> getSiteAddress(Event record) {
        String street = record.VTR2_Monitoring_Visit_del__r.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__r.VTR2_AddressLine1__c;
        if (record.VTR2_Monitoring_Visit_del__r.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__r.VTR3_AddressLine_2__c != null) {
            street += ', ' + record.VTR2_Monitoring_Visit_del__r.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__r.VTR3_AddressLine_2__c;
        }
        if (record.VTR2_Monitoring_Visit_del__r.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__r.VTR3_AddressLine_3__c != null) {
            street += ', ' + record.VTR2_Monitoring_Visit_del__r.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__r.VTR3_AddressLine_3__c;
        }
        return new Map<String, String>{
                'street' => street,
                'city' => record.VTR2_Monitoring_Visit_del__r.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__r.VTR2_City__c,
                'country' => record.VTR2_Monitoring_Visit_del__r.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__r.VTR3_Country__c,
                'province' => record.VTR2_Monitoring_Visit_del__r.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__r.VTR2_State__c,
                'zipCode' => record.VTR2_Monitoring_Visit_del__r.VTD1_Virtual_Site__r.VTR2_PrimarySiteAddress__r.VTR2_ZIP__c
        };
    }
}