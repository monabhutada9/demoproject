/**
 * @author: Alexander Komarov
 * @date: 06.07.2020
 * @description:
 */

@IsTest
public class VT_R5_MobileInitialTest {
    public static void firstTest() {
        Test.startTest();
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_AllTestsMobilePatient@test.com' LIMIT 1];
        System.runAs(toRun) {
            setRestContext('/patient/v1/initial/', 'GET');
            VT_R5_MobileRestRouter.doGET();
        }
  Test.stopTest();
    }
    public static void secondTest() {
        Test.startTest();
        User toRun = [SELECT Id FROM User WHERE Username = 'VT_R5_AllTestsMobilePatient@test.com' LIMIT 1];
        System.runAs(toRun) {
            setRestContext('/patient/v0/initial/', 'GET');
            VT_R5_MobileRestRouter.doGET();
        }
        Test.stopTest();
    }
    public static void thirdTest() {
        Test.startTest();
        setRestContext('/patient/v1/initial/', 'GET');
        VT_R5_MobileRestRouter.doGET();
        Test.stopTest();
    }
    private static void setRestContext(String URI, String method) {
        RestContext.request = new RestRequest();
        RestContext.response = new RestResponse();
        RestContext.request.requestURI = URI;
        RestContext.request.httpMethod = method;
    }
}