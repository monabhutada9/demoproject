/**
 * @author: Alexander Komarov
 * @date: 25.11.2020
 * @description:
 */

@IsTest
public with sharing class VT_R5_MobilePatientPhoneSMSTest {

    private static User user;

    static {
        user = [SELECT Id FROM User WHERE Username = :VT_R5_AllTestsMobile.PT_USERNAME LIMIT 1];
   }

    public static void firstTest() {
        Test.startTest();
        System.runAs(user) {
            VT_D1_Phone__c phone = [SELECT Id FROM VT_D1_Phone__c WHERE Type__c = 'Mobile'];
            VT_R5_AllTestsMobile.setRestContext('/patient/v1/phone/sms', 'POST');
            RestContext.request.params.put('action', 'optIn');
            RestContext.request.requestBody = Blob.valueOf('{"phoneId":"'+phone.Id+'"}');
            VT_R5_MobileRestRouter.doPOST();
        }
        Test.stopTest();
    }

    public static void secondTest() {
        Test.startTest();
        System.runAs(user) {
            VT_D1_Phone__c phone = [SELECT Id FROM VT_D1_Phone__c WHERE Type__c = 'Mobile'];
            phone.VTR2_SMS_Opt_In_Status__c = 'Active';
            update phone;
            VT_R5_AllTestsMobile.setRestContext('/patient/v1/phone/sms', 'POST');
            RestContext.request.params.put('action', 'optOut');
            RestContext.request.requestBody = Blob.valueOf('{"phoneId":"'+phone.Id+'"}');
            VT_R5_MobileRestRouter.doPOST();
        }
        Test.stopTest();
    }
    public static void thirdTest() {
        Test.startTest();
        System.runAs(user) {
            VT_D1_Phone__c phone = [SELECT Id FROM VT_D1_Phone__c WHERE Type__c = 'Mobile'];
            phone.VTR2_SMS_Opt_In_Status__c = 'Pending';
            update phone;
            VT_R5_AllTestsMobile.setRestContext('/patient/v1/phone/sms', 'POST');
            RestContext.request.params.put('action', 'resendOptIn');
            RestContext.request.requestBody = Blob.valueOf('{"phoneId":"'+phone.Id+'"}');
            VT_R5_MobileRestRouter.doPOST();
        }
        Test.stopTest();
    }
    public static void fourthTest() {
        Test.startTest();
        System.runAs(user) {
            VT_D1_Phone__c phone = [SELECT Id FROM VT_D1_Phone__c WHERE Type__c = 'Mobile'];
            phone.VTR2_SMS_Opt_In_Status__c = 'Active';
            update phone;
            VT_R5_AllTestsMobile.setRestContext('/patient/v1/phone/sms', 'POST');
            RestContext.request.params.put('action', 'sendContacts');
            RestContext.request.requestBody = Blob.valueOf('{"phoneId":"'+phone.Id+'"}');
            VT_R5_MobileRestRouter.doPOST();
        }
        Test.stopTest();
    }


}
//actions: optIn, optOut, resendOptIn, sendContacts
//phoneId
//action