/**
* @author: Carl Judge
* @date: 16-Nov-18
* @description: Builds request body for DS access token
**/

public class VT_D2_RequestBuilder_DocuSignAccessToken implements VT_D1_RequestBuilder {

    public String buildRequestBody() {
        VTD2_Docusign_API_Settings__c dsSettings = VTD2_Docusign_API_Settings__c.getInstance();

        Auth.JWT jwt = new Auth.JWT();
        jwt.setIss(dsSettings.VTD2_Integrator_Key__c);
        jwt.setSub(dsSettings.VTD2_DocuSign_User_ID__c);
        jwt.setAud(dsSettings.VTD2_Auth_Domain__c);
        jwt.setAdditionalClaims(new Map<String, String>{'scope' => 'signature impersonation'});

        if (! Test.isRunningTest()) {
            Auth.JWS jws = new Auth.JWS(jwt, dsSettings.VTD2_Certificate_Name__c);
            return 'grant_type=urn:ietf:params:oauth:grant-type:jwt-bearer&assertion=' + jws.getCompactSerialization();
        } else {
            return '';
        }
    }
}