/**
 * Created by Eugen Kharchuk on 01-Jul-19.
 */

public with sharing class VT_R2_PISignatureRequiredFieldSetter extends Handler {

    public override void onBeforeInsert(Handler.TriggerContext context) {
        new VT_R2_PISignatureRequiredFieldSetter.LogicHandler(context).execute();
    }

    public inherited sharing class LogicHandler {
        private Handler.TriggerContext context;
        private Map<Id, VTD1_Regulatory_Binder__c> regBindersByIds;

        public LogicHandler(Handler.TriggerContext context) {
            this.context = context;
        }

        public void execute() {
            if (userHasNoCRUDAccessToRequiredFields()) {
                return;
            }

            this.regBindersByIds = getRegBindersByIds();
            for (VTD1_Document__c doc : (List<VTD1_Document__c>) context.newList) {
                if (regBindersByIds.containsKey(doc.VTD1_Regulatory_Binder__c)) {
                    setPiSignatureRequiredField(doc);
                }
            }
        }

        private Map<Id, VTD1_Regulatory_Binder__c> getRegBindersByIds() {
            Map<Id, VTD1_Regulatory_Binder__c> result = new Map<Id, VTD1_Regulatory_Binder__c>();
            new QueryBuilder(VTD1_Regulatory_Binder__c.SObjectType)
                    .namedQueryRegister('VT_R2_PISignatureRequiredFieldSetter_RegBinders')
                    .addField('VTD1_Signature_Required__c')
                    .addConditions()
                    .add(new QueryBuilder.InCondition('Id').inCollection((List<Id>) getRegBinderIds()))
                    .endConditions()
                    .toMap(result);
            return result;
        }

        private List<Id> getRegBinderIds() {
            List<Id> regBinderIds = new List<Id>();
            for (VTD1_Document__c doc : (List<VTD1_Document__c>) context.newList) {
                if (doc.isSet('VTD1_Regulatory_Binder__c')) {
                    regBinderIds.add(doc.VTD1_Regulatory_Binder__c);
                }
            }
            return regBinderIds;
        }

        private void setPiSignatureRequiredField(VTD1_Document__c doc) {
            doc.VTD2_PI_Signature_required__c = this.regBindersByIds
                    .get(doc.VTD1_Regulatory_Binder__c).VTD1_Signature_Required__c;
        }

        private Boolean userHasNoCRUDAccessToRequiredFields() {
            return !Schema.SObjectType.VTD1_Document__c.fields.VTD2_PI_Signature_required__c.isUpdateable()
                    || !Schema.SObjectType.VTD1_Document__c.fields.VTD1_Regulatory_Binder__c.isAccessible()
                    || !Schema.SObjectType.VTD1_Regulatory_Binder__c.fields.VTD1_Signature_Required__c.isAccessible()
                    || !Schema.SObjectType.VTD1_Regulatory_Document__c.fields.VTD1_Signature_Required__c.isAccessible();
        }
    }
}