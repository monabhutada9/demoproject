/**
 * Created by Alexander Komarov on 08.07.2019.
 */

@RestResource(UrlMapping='/Patient/StudyDetails/')
global with sharing class VT_R3_RestPatientStudyDetails {
    static VT_R3_RestHelper helper = new VT_R3_RestHelper();
    static VT_R3_RestHelper.CustomResponse cr = new VT_R3_RestHelper.CustomResponse();
    static final String FALLBACK_LANGUAGE = 'en_US';

    @HttpGet
    global static String getStudyDetails() {
        RestResponse response = RestContext.response;

        StudyDetailsObj pn;
        try {
            User currentUser = [SELECT Id, Contact.Account.Candidate_Patient__r.Study__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
            Knowledge__kav article;
            String userLang = UserInfo.getLanguage();

            for (Knowledge__kav item : [
                    SELECT VTD1_Content__c, Language, Title,Summary
                    FROM Knowledge__kav
                    WHERE IsLatestVersion = TRUE
                    AND PublishStatus = 'Online'
                    AND VTD2_Type__c = 'Study_Details'
                    AND VTD2_Study__c = :currentUser.Contact.Account.Candidate_Patient__r.Study__c
            ]) {
                if (item.Language == userLang || (item.Language == FALLBACK_LANGUAGE && article == null)) {
                    article = item;
                }
            }
            pn = new StudyDetailsObj(article.Id, article.Summary, article.Title, article.VTD1_Content__c);
        } catch (Exception e) {
            System.debug('EXCEPTION *** ' + e);
            response.statusCode = 500;
            cr.buildResponse('Something went wrong.');
            return JSON.serialize(cr, true);
        }
        cr.buildResponse(pn);
        return JSON.serialize(cr, true);
    }

    private class StudyDetailsObj {
        public String id;
        public String summary;
        public String title;
        public String content;

        public StudyDetailsObj(String i, String s, String t, String c) {
            this.id = i;
            this.summary = s;
            this.title = t;
            this.content = c;

        }
    }
}