/**
* @author Ruslan Mullayanov
* @description UserTrigger methods to run without sharing
*/
public without sharing class VT_D2_UserTriggerHelper {
    public static void updateVisitAndConferenceMembers(List<User> userList, Map<Id, User> userOldMap) {
        try {
            List<User> usersToProcess = new List<User>();
            for (User user : userList) {
                if (user.Email != userOldMap.get(user.Id).Email) {
                    usersToProcess.add(user);
                }
            }
            System.debug('usersToProcess ' + usersToProcess);
            if (usersToProcess.isEmpty()) return;
            List<Visit_Member__c> visitMemberList = [SELECT Id, VTD1_External_Participant_Email__c, VTD1_Participant_User__c FROM Visit_Member__c WHERE VTD1_Participant_User__c IN :usersToProcess];
            List<VTD1_Conference_Member__c> conferenceMemberList = [SELECT Id, VTD1_External_Participant_Email__c, VTD1_User__c FROM VTD1_Conference_Member__c WHERE VTD1_User__c IN :usersToProcess];
            System.debug('visitMemberList ' + visitMemberList);
            System.debug('conferenceMemberList ' + conferenceMemberList);
            if (visitMemberList.isEmpty() && conferenceMemberList.isEmpty()) return;
            Map<Id, List<Visit_Member__c>> userIdToVisitMembersMap = new Map<Id, List<Visit_Member__c>>();
            for (Visit_Member__c member : visitMemberList) {
                Id memberUserId = member.VTD1_Participant_User__c;
                if (!userIdToVisitMembersMap.containsKey(memberUserId)) {
                    userIdToVisitMembersMap.put(memberUserId, new List<Visit_Member__c>());
                }
                userIdToVisitMembersMap.get(memberUserId).add(member);
            }
            Map<Id, List<VTD1_Conference_Member__c>> userIdToConferenceMembersMap = new Map<Id, List<VTD1_Conference_Member__c>>();
            for (VTD1_Conference_Member__c member : conferenceMemberList) {
                Id memberUserId = member.VTD1_User__c;
                if (!userIdToConferenceMembersMap.containsKey(memberUserId)) {
                    userIdToConferenceMembersMap.put(memberUserId, new List<VTD1_Conference_Member__c>());
                }
                userIdToConferenceMembersMap.get(memberUserId).add(member);
            }
            List<SObject> membersToUpdate = new List<SObject>();
            for (User user : usersToProcess) {
                if (userIdToVisitMembersMap.containsKey(user.Id)) {
                    for (Visit_Member__c member : userIdToVisitMembersMap.get(user.Id)) {
                        member.VTD1_External_Participant_Email__c = user.Email;
                        membersToUpdate.add(member);
                    }
                }
                if (userIdToConferenceMembersMap.containsKey(user.Id)) {
                    for (VTD1_Conference_Member__c member : userIdToConferenceMembersMap.get(user.Id)) {
                        member.VTD1_External_Participant_Email__c = user.Email;
                        membersToUpdate.add(member);
                    }
                }
            }
            if (!membersToUpdate.isEmpty()) {
                System.debug('membersToUpdate ' + membersToUpdate);
                update membersToUpdate;
            }
        } catch (Exception e) {
            System.debug(e.getMessage() + ' ' + e.getStackTraceString());
        }
    }

    public static void updateStudyPhoneNumberOnCase(List<User> userList, Map<Id, User> userOldMap) {
        try {
            Id patientProfileId = VT_D1_HelperClass.getProfileIdByName('Patient');
            List<Id> contactsToProcess = new List<Id>();
            Map<Id, String> userLangMap = new Map<Id, String>();
            for (User user : userList) {
                if (user.ProfileId == patientProfileId && user.LanguageLocaleKey != userOldMap.get(user.Id).LanguageLocaleKey) {
                    contactsToProcess.add(user.ContactId);
                    userLangMap.put(user.Id, user.LanguageLocaleKey);
                }
            }
            if (contactsToProcess.size()!=0) {
                Map <Id, Case> caseMap = new Map<Id, Case>();
                List<VT_R4_PatientConversionHelper.ContactCandidateWrapper> patients = new List<VT_R4_PatientConversionHelper.ContactCandidateWrapper>();
                List<Case> cases = [
                        SELECT Id, RecordTypeId, AccountId, ContactId,
                        VTR2_StudyPhoneNumber__c, VTD1_Study__c, VTD1_Patient_User__c,
                        VTR4_CandidatePatient__r.VTD2_Language__c,
                        VTR4_CandidatePatient__r.HealthCloudGA__Address1Country__c,
                        VTR4_CandidatePatient__r.HealthCloudGA__Address1State__c
                        FROM Case
                        WHERE ContactId IN :contactsToProcess AND VTR4_CandidatePatient__c != NULL
                        AND RecordType.DeveloperName = :VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN
                ];
                List<HealthCloudGA__CandidatePatient__c> cpList = new List<HealthCloudGA__CandidatePatient__c>();
                for (Case c : cases) {
                    patients.add(new VT_R4_PatientConversionHelper.ContactCandidateWrapper(c, c.VTR4_CandidatePatient__r));
                }
                List<Case> casesToUpdate = VT_D1_CaseHandler.setStudyPhoneNumberAndConsentPacket(patients, userLangMap);
                for (Case caseObj : casesToUpdate) {
                    caseMap.put(caseObj.Id, caseObj);
                }
                if (!caseMap.isEmpty()) {
                    update caseMap.values();
                }
            }
        } catch (Exception e) {
            System.debug(e.getMessage() + ' ' + e.getStackTraceString());
        }
    }

    // SH-899
    public static void createSharesForPIUsers(List <User> users) {
        Set <Id> userIds = new Set<Id>();
        for (User user : users) {
            userIds.add(user.Id);
        }
        if (System.isFuture() || System.isBatch()) {
            createSharesForPIUserIds(userIds);
        } else {
            createSharesForPIUserIdsFuture(userIds);
        }
    }

    @Future
    public static void createSharesForPIUserIdsFuture(Set<Id> piUserIds) {
        createSharesForPIUserIds(piUserIds);
    }

    public static void createSharesForPIUserIds(Set<Id> piUserIds) {
        List<User> users = [SELECT Id, ContactId, Contact.AccountId FROM User WHERE Id IN :piUserIds AND IsActive = TRUE AND Profile.Name = 'Primary Investigator'];
        List <ContactShare> contactShares = new List<ContactShare>();
        List <AccountShare> accountShares = new List<AccountShare>();
        for (User user : users) {
            contactShares.add(new ContactShare(UserOrGroupId = user.Id, ContactId = user.ContactId, ContactAccessLevel = 'Edit'));
            if (user.Contact.AccountId != null) {
                accountShares.add(new AccountShare(UserOrGroupId = user.Id, AccountId = user.Contact.AccountId, AccountAccessLevel = 'Edit', OpportunityAccessLevel = 'Edit'));
            }
        }
        if (!contactShares.isEmpty()) {
            insert contactShares;
        }
        if (!accountShares.isEmpty()) {
            insert accountShares;
        }
    }

    public static void uncheckSendWelcomeEmail(List<User> users) {

        for (User user : users) {
            if (!(user.IsActive && user.UserType == 'Standard') && user.VTR2_WelcomeEmailSent__c) {
                user.VTR2_WelcomeEmailSent__c = false;

            }
            if (!user.IsActive) {
                user.VTR2_WelcomeEmailSent__c = false;
            }
        }
    }

    public static void fillDeactivateFields(List<User> newUserList, Map<Id, User> oldUserMap) {
        Set<Id> newPTUserIdSet = new Set<Id>();
        for (User newUser : newUserList) {
            if (newUser.VTD1_Profile_Name__c == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME) {
                newPTUserIdSet.add(newUser.Id);
            }
        }

        Map<Id, User> lastModifiedByList = new Map<Id, User>([SELECT Id, LastModifiedBy.Name FROM User WHERE Id IN :newUserList]);
        List<Case> caseList = [
                SELECT Id,
                        VTD1_Patient_User__c,
                        Caregiver_s_Name__c,
                        LastModifiedById,
                        LastModifiedBy.Name
                FROM Case
                WHERE RecordType.Name = :VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN
                AND VTD1_Patient_User__c IN :newPTUserIdSet
        ];

        for (User newUser : newUserList) {

            if (newUser.IsActive != oldUserMap.get(newUser.Id).IsActive) {
                if(newUser.IsActive) {
                    newUser.VTR4_DeactivatedBy__c = null;
                    newUser.VTR4_DeactivatedDate__c = null;
                } else {
                    newUser.VTR4_DeactivatedDate__c = System.now();
                    if (VT_R4_ConstantsHelper_ProfilesSTM.INTERNAL_PROFILES.contains(newUser.VTD1_Profile_Name__c)) {
                        newUser.VTR4_DeactivatedBy__c = UserInfo.getUserId();
                    } else if (newUser.VTD1_Profile_Name__c == VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME || newUser.VTD1_Profile_Name__c == VT_R4_ConstantsHelper_ProfilesSTM.CAREGIVER_PROFILE_NAME) {
                        for (Case carePlan : caseList) {
                            if (newUser.Id == carePlan.VTD1_Patient_User__c || newUser.VTD1_Full_Name__c == carePlan.Caregiver_s_Name__c) {
                                newUser.VTR4_DeactivatedBy__c = UserInfo.getName() != 'API User' ? newUser.LastModifiedById : carePlan.LastModifiedById;
                            }
                        }
                    }

                }
            }
        }
    } 
}