public with sharing class VT_D1_DocumentController {

    public static final Set<Id> DOCUMENT_REC_TYPE_IDS_SET = new Set<Id>{
            VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT,
            VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_NOTE_OF_TRANSFER
    };

    public class FileWrapper {
        @AuraEnabled public String Id { get; set; }
        @AuraEnabled public String FileName { get; set; }
        @AuraEnabled public Date CreatedDate { get; set; }
        @AuraEnabled public String OwnerName { get; set; }
        @AuraEnabled public String SiteName { get; set; }
        @AuraEnabled public String FileURL { get; set; }
        @AuraEnabled public String Study { get; set; }

        public FileWrapper(ContentDocumentLink file) {
            this.Id = file.Id;
            this.FileName = file.ContentDocument.title;
            this.CreatedDate = date.newinstance(file.ContentDocument.CreatedDate.year(), file.ContentDocument.CreatedDate.month(), file.ContentDocument.CreatedDate.day());
            this.OwnerName = file.ContentDocument.CreatedBy.Name;
            this.SiteName = file.LinkedEntity.name;
            this.FileURL = !Test.isRunningTest() ? ConnectApi.Communities.getCommunity(Network.getNetworkId()).siteUrl + '/sfc/servlet.shepherd/document/download/' + file.contentDocument.Id
                                                 : '/sfc/servlet.shepherd/document/download/' + file.contentDocument.Id;
        }
    }

    public class DocumentWrapper {
        @AuraEnabled public String Id { get; set; }
        @AuraEnabled public String Name { get; set; }
        @AuraEnabled public String DocumentName { get; set; }
        @AuraEnabled public Boolean RSU { get; set; }
        @AuraEnabled public Boolean ISF { get; set; }
        @AuraEnabled public Boolean TMF { get; set; }
        @AuraEnabled public String Version { get; set; }
        @AuraEnabled public String LifecycleStatus { get; set; }
        @AuraEnabled public String WorkflowStatus { get; set; }
        @AuraEnabled public String StudyId { get; set; }
        @AuraEnabled public String StudyName { get; set; }
        @AuraEnabled public String StudyStatus { get; set; }
        @AuraEnabled public Boolean CurrentVersion { get; set; }
        @AuraEnabled public String ContentDocumentId { get; set; }
        @AuraEnabled public List<DocumentWrapper> Versions { get; set; }
        @AuraEnabled public String RegulatoryDocumentType { get; set; }
        @AuraEnabled public String DocuSignStatus { get; set; }
        @AuraEnabled public String RejectionComment { get; set; }
        @AuraEnabled public String SiteName { get; set; }
        @AuraEnabled public String FileName { get; set; }


        public DocumentWrapper(VTD1_Document__c d, List<ContentVersion> cvList) {
            this.Id = d.Id;
            this.Name = d.Name;
            this.DocumentName = d.VTD1_Document_Name__c;
            this.RSU = d.VTD1_RSU__c;
            this.ISF = d.VTD1_ISF__c;
            this.TMF = d.VTD1_TMF__c;
            this.Version = d.VTD1_Version__c;
            this.LifecycleStatus = d.VTD1_Lifecycle_State__c;
            this.WorkflowStatus = d.VTD1_Status__c;
            this.StudyId = d.VTD1_Study__c;
            this.StudyName = d.VTD1_Study__r.Name;
            this.StudyStatus = d.VTD1_Study__r.VTD1_StudyStatus__c;
            this.Versions = new List<DocumentWrapper>();
            this.RegulatoryDocumentType = d.VTD1_Regulatory_Document_Type__c;
            if (d.DocuSign_Status__r.size() > 0) {
                this.DocuSignStatus = d.DocuSign_Status__r[0].dsfs__Envelope_Status__c;
                this.RejectionComment = d.DocuSign_Status__r[0].dsfs__Declined_Reason__c;
            }
            if (cvList != null) {
                for (ContentVersion cv : cvList) {
                    this.Versions.add(new DocumentWrapper(d, cv));
                }
            }
            this.SiteName = d.VTD1_Site__r.Name;
            this.FileName = d.VTD1_FileNames__c;
        }
        public DocumentWrapper(VTD1_Document__c d, ContentVersion cv) {
            this.Id = cv.Id;
            this.Name = d.Name;
            this.DocumentName = cv.Title;
            this.RSU = d.VTD1_RSU__c;
            this.ISF = d.VTD1_ISF__c;
            this.TMF = d.VTD1_TMF__c;
            this.Version = cv.VTD1_CompoundVersionNumber__c;
            this.LifecycleStatus = d.VTD1_Lifecycle_State__c;
            this.WorkflowStatus = '';
            this.CurrentVersion = cv.VTD1_Current_Version__c;
            this.ContentDocumentId = cv.ContentDocumentId;
            this.SiteName = d.VTD1_Site__r.Name;
        }
    }

    public class DocumentWrapperSCR {
        @AuraEnabled public List<DocumentWrapper> docWrapperList { get; set; }
        @AuraEnabled public Integer totalPages { get; set; }
        @AuraEnabled public Id userId;
        @AuraEnabled public List<Virtual_Site__c> virtualSites;
    }

    public class DocumentWrapperSCRTBD {
        @AuraEnabled public List<FileWrapper> FilesWrapper { get; set; }
        @AuraEnabled public Integer totalPages { get; set; }
    }

    @AuraEnabled
    public static List<DocumentWrapper> getDocuments(Id studyId) {
        List<DocumentWrapper> docWrapperList = new List<DocumentWrapper>();
        try {
            Map<Id, VTD1_Document__c> documentsMap = getDocumentsMap (studyId);
            return getDocWrapperList(docWrapperList, documentsMap);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    //TODO Create Helper The same logic in VT_R4_VSiteFilesUploaderController

    public static Set<Id> getAvailableSiteIds(Id currentUserId) {
        Set<Id> availableSiteIds = new Set<Id>();
        for (Study_Site_Team_Member__c studySiteTeamMember : [
                SELECT VTR2_Associated_PI3__r.VTD1_VirtualSite__c, VTR2_Associated_PI__r.VTD1_VirtualSite__c
                FROM Study_Site_Team_Member__c
                WHERE VTR2_Associated_SubI__r.User__c = :currentUserId
                OR VTR2_Associated_SCr__r.User__c = :currentUserId
        ]) {
            availableSiteIds.add(studySiteTeamMember.VTR2_Associated_PI3__r.VTD1_VirtualSite__c);
            availableSiteIds.add(studySiteTeamMember.VTR2_Associated_PI__r.VTD1_VirtualSite__c);
        }
        availableSiteIds.remove(null);
        return availableSiteIds;
    }

    @AuraEnabled
    public static DocumentWrapperSCRTBD getContentDocumentLinks(Id studyId, String querySettingsString, Boolean areLimitsIgnored) {
        Id currentUserId = UserInfo.getUserId();
        VT_TableHelper.TableQuerySettings querySettings = VT_TableHelper.parseQuerySettings(querySettingsString);
        List<Id> myStudyIds = VT_D1_PIStudySwitcherRemote.getMyStudyIds(studyId);
        List<FileWrapper> files = new List<FileWrapper>();
        Set<Id> availableSiteIds = VT_D1_DocumentController.getAvailableSiteIds(currentUserId);
        Map <Id, Virtual_Site__c> sites = new Map<Id, Virtual_Site__c>([
                SELECT Id, Name, VTD1_Study__r.Name, VTD1_Study__c
                FROM Virtual_Site__c
                WHERE VTD1_Study__c IN :myStudyIds
                AND (
                        Id IN :availableSiteIds
                        OR VTD1_Study_Team_Member__r.User__c = :currentUserId
                        OR VTR2_Backup_PI_STM__r.User__c = :currentUserId
                )
        ]);
        Map<Id, ContentDocumentLink> contentDocumentLink = new Map<Id, ContentDocumentLink>();
        DocumentWrapperSCRTBD documents = new DocumentWrapperSCRTBD();
        try {
            if (sites.keySet().isEmpty()) {
                DocumentWrapperSCRTBD wrapper = new DocumentWrapperSCRTBD();
                wrapper.FilesWrapper = new List<FileWrapper>();
                return wrapper;
            }
            contentDocumentLink = getContentDocumentsMap (sites.keySet(), querySettings, areLimitsIgnored);
            documents.totalPages = getTotalPagesAddFileTab(sites.keySet(), querySettings);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }

        String study;
        String selectedStudy = getStudyName(studyId, sites.values());
        for (Id documentId : contentDocumentLink.keySet()) {
            study = sites.get(contentDocumentLink.get(documentId).LinkedEntityId).VTD1_Study__r.Name;
            FileWrapper fileData = new FileWrapper(contentDocumentLink.get(documentId));
            fileData.Study = study;
            files.add(fileData);
        }
        documents.FilesWrapper = files;
        return documents;
    }

    @AuraEnabled
    public static DocumentWrapperSCR getDocumentsSCR(Id studyId, String querySettingsString) {
        VT_TableHelper.TableQuerySettings querySettings = VT_TableHelper.parseQuerySettings(querySettingsString);
        List<Id> myStudyIds = VT_D1_PIStudySwitcherRemote.getMyStudyIds(studyId);
        List<DocumentWrapper> docWrapperList = new List<DocumentWrapper>();
        DocumentWrapperSCR docWrapperSCR = new DocumentWrapperSCR();
        docWrapperSCR.userId = UserInfo.getUserId();
        try {
            Map<Id, VTD1_Document__c> documentsMap = getDocumentsMap (myStudyIds, querySettings);
            docWrapperSCR.docWrapperList = getDocWrapperList(docWrapperList, documentsMap);
            docWrapperSCR.totalPages = getTotalPages(myStudyIds, querySettings);

            Set<Id> availableSiteIds = VT_D1_DocumentController.getAvailableSiteIds(docWrapperSCR.userId);
            docWrapperSCR.virtualSites = getVirtualSites(availableSiteIds, docWrapperSCR.userId, myStudyIds);
            return docWrapperSCR;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    public static List<Virtual_Site__c> getVirtualSites(Set<Id> siteIds, Id userId, List<Id> myStudyIds){
        return [Select Id, Name, VTD1_Study__c from Virtual_Site__c where VTD1_Study__c in: myStudyIds and (Id in: siteIds OR VTR2_Primary_SCR__c = :userId) order by Name asc];
    }

    @AuraEnabled
    public static Id uploadFile(String docId, String fileName, String base64Data) {
        List<ContentDocumentLink> cdlList = [SELECT id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :docId];
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        ContentVersion cv;
        try {
            if (cdlList.size() > 0) {
                ContentDocumentLink cdl = cdlList[0];
                cv = new ContentVersion(
                        Title = fileName,
                        PathOnClient = '/' + fileName,
                        VersionData = EncodingUtil.base64Decode(base64Data),
                        IsMajorVersion = true,
                        ContentDocumentId = cdl.ContentDocumentId
                );
                insert cv;
                return cv.Id;
            } else {
                cv = new ContentVersion(
                        Title = fileName,
                        PathOnClient = '/' + fileName,
                        VersionData = EncodingUtil.base64Decode(base64Data),
                        IsMajorVersion = true
                );
                insert cv;
                ContentDocumentLink cdl = new ContentDocumentLink(LinkedEntityId = docId,
                        ContentDocumentId = [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :cv.Id].Id,
                        ShareType = 'V',
                        Visibility = 'AllUsers');
                insert cdl;
                return cv.Id;
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    @AuraEnabled
    public static Boolean isSandbox() {
        return String.isNotEmpty(VT_D1_HelperClass.getSandboxPrefix());
    }

    @AuraEnabled
    public static Boolean isPi() {
        return UserInfo.getProfileId() == [SELECT Id FROM Profile WHERE Name = 'Primary Investigator'][0].Id;
    }

    @AuraEnabled
    public static String convertToNewVersion(Id docId, Id contentDocId) {
        List<ContentDocumentLink> cdlList = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: docId];
        ContentVersion uploadedVersion = [Select Id, Title, PathOnClient, VersionData, ContentDocumentId, ContentDocument.Id from ContentVersion where ContentDocumentId =: contentDocId limit 1];
        Id finalDocumentId = !cdlList.isEmpty() ? cdlList.get(0).ContentDocumentId : null;
        ContentVersion finalVersion = new ContentVersion(
                            Title = uploadedVersion.Title,
                            PathOnClient = uploadedVersion.PathOnClient,
                            VersionData = uploadedVersion.VersionData,
                            IsMajorVersion = true,
                            ContentDocumentId = finalDocumentId
                        );
        insert finalVersion;
        delete uploadedVersion.ContentDocument;

        if (cdlList.isEmpty()) {
            if(finalDocumentId == null){
                finalDocumentId = [Select ContentDocumentId from ContentVersion where Id =: finalVersion.Id].ContentDocumentId;
            }
            ContentDocumentLink cdl = new ContentDocumentLink(LinkedEntityId = docId,
                    ContentDocumentId = finalDocumentId,
                    ShareType = 'V',
                    Visibility = 'AllUsers');
            insert cdl;

        }

        return finalDocumentId;
    }

    private static Integer getTotalPages(List<Id> studyIds, VT_TableHelper.TableQuerySettings querySettings) {
        String queryFilter = (querySettings.queryFilter != null && querySettings.queryFilter != '') ? ' AND ' + querySettings.queryFilter : '';

        String query = 'SELECT Count(Id) FROM VTD1_Document__c  WHERE RecordTypeId = :DOCUMENT_REC_TYPE_IDS_SET' +
                ' AND VTD1_Document_Name__c != \'\'' +
                ' AND VTD1_Study__c IN :studyIds ' + queryFilter;
        AggregateResult[] ar = Database.query(query);
        Integer totalPages = (Integer) Math.ceil((Decimal) ar[0].get('expr0') / querySettings.entriesOnPage);
        return totalPages;
    }

    private static Integer getTotalPagesAddFileTab(Set<Id> siteIds, VT_TableHelper.TableQuerySettings querySettings) {
        String queryFilter = (querySettings.queryFilter != null && querySettings.queryFilter != '') ? ' AND ' + querySettings.queryFilter : '';

        String query = 'SELECT Count(Id) FROM ContentDocumentLink WHERE LinkedEntityId IN :siteIds' + queryFilter;
        AggregateResult[] ar = Database.query(query);
        Integer totalPages = (Integer) Math.ceil((Decimal) ar[0].get('expr0') / querySettings.entriesOnPage);
        return totalPages;
    }

    private static Map<Id, VTD1_Document__c> getDocumentsMap(List<Id> studyIds, VT_TableHelper.TableQuerySettings querySettings) {
        String queryFilter = (querySettings.queryFilter != null && querySettings.queryFilter != '') ? ' AND ' + querySettings.queryFilter : '';
        String queryOrder = ((querySettings.queryOrder != null && querySettings.queryOrder != '') ? ' ORDER BY ' + querySettings.queryOrder : ' ORDER BY VTD1_Regulatory_Document_Type__c') + ', VTD1_Version__c DESC NULLS LAST';
        String queryOffset = (querySettings.queryOffset != null && querySettings.queryOffset != '') ? ' ' + querySettings.queryOffset : '';
        String queryLimit = (querySettings.queryLimit != null && querySettings.queryLimit != '') ? ' ' + querySettings.queryLimit : '';

        String query = 'SELECT Id, Name, VTD1_Document_Name__c, VTD1_RSU__c, VTD1_ISF__c, VTD1_TMF__c, VTD1_Version__c,' +
                ' VTD1_Lifecycle_State__c, VTD1_Status__c, VTD1_Study__c, VTD1_Study__r.Name, VTD1_Study__r.VTD1_StudyStatus__c, VTD1_FileNames__c, VTD1_Regulatory_Document_Type__c,' +
                ' VTD1_Site__r.Name, (select dsfs__Envelope_Status__c, dsfs__Declined_Reason__c from DocuSign_Status__r ORDER BY LastModifiedDate DESC LIMIT 1)' +
                ' FROM VTD1_Document__c WHERE RecordTypeId = :DOCUMENT_REC_TYPE_IDS_SET' +
                ' AND VTD1_Document_Name__c != \'\'' +
                ' AND VTD1_Study__c IN :studyIds ' + queryFilter + queryOrder + queryLimit + queryOffset;

        System.debug('query = ' + query);
        Map<Id, VTD1_Document__c> documentsMap = new Map<Id, VTD1_Document__c>((List<VTD1_Document__c>) Database.query(query));
        return documentsMap;
    }

    private static Map<Id, ContentDocumentLink> getContentDocumentsMap(Set<Id> siteIds, VT_TableHelper.TableQuerySettings querySettings, Boolean areLimitsIgnored) {
        String queryFilter = (querySettings.queryFilter != null && querySettings.queryFilter != '') ? ' AND ' + querySettings.queryFilter : '';
        String queryOrder = ((querySettings.queryOrder != null && querySettings.queryOrder != '') ? ' ORDER BY ' + querySettings.queryOrder : ' ORDER BY ContentDocument.CreatedDate DESC') + ', ContentDocument.title NULLS LAST';
        String queryOffset = (!areLimitsIgnored && querySettings.queryOffset != null && querySettings.queryOffset != '') ? ' ' + querySettings.queryOffset : '';
        String queryLimit = (!areLimitsIgnored && querySettings.queryLimit != null && querySettings.queryLimit != '') ? ' ' + querySettings.queryLimit : '';
        String query = 'SELECT Id, LinkedEntityId, LinkedEntity.name, ContentDocument.title, ContentDocument.CreatedBy.Name, ContentDocument.CreatedDate ' +
                'FROM ContentDocumentLink WHERE LinkedEntityId IN :siteIds' + queryFilter + queryOrder + queryLimit + queryOffset;

        System.debug('query = ' + query);
        Map<Id, ContentDocumentLink> documentsMap = new Map<Id, ContentDocumentLink>((List<ContentDocumentLink>) Database.query(query));
        return documentsMap;
    }

    private static Map<Id, VTD1_Document__c> getDocumentsMap(Id studyId) {
        List<Id> myStudyIds = VT_D1_PIStudySwitcherRemote.getMyStudyIds(studyId);

        Id regulatoryDocumentRecTypeId = VT_R4_ConstantsHelper_Documents.RECORD_TYPE_ID_DOCUMENT_REGULATORY_DOCUMENT;

        Map<Id, VTD1_Document__c> documentsMap = new Map<Id, VTD1_Document__c>([
                SELECT
                        Id,
                        Name,
                        VTD1_Document_Name__c,
                        VTD1_RSU__c,
                        VTD1_ISF__c,
                        VTD1_TMF__c,
                        VTD1_Version__c,
                        VTD1_Lifecycle_State__c,
                        VTD1_Status__c,
                        VTD1_Study__c,
                        VTD1_Study__r.Name,
                        VTD1_Regulatory_Document_Type__c,
                        VTD1_Site__r.Name,
                        VTD1_FileNames__c, (select dsfs__Envelope_Status__c, dsfs__Declined_Reason__c from DocuSign_Status__r ORDER BY LastModifiedDate DESC LIMIT 1)
                FROM VTD1_Document__c
                WHERE RecordTypeId = :regulatoryDocumentRecTypeId
                AND VTD1_Document_Name__c != ''
                AND VTD1_Study__c IN :myStudyIds
                ORDER BY VTD1_Document_Name__c ASC, VTD1_Version__c DESC
        ]);
        return documentsMap;
    }

    private static List<DocumentWrapper> getDocWrapperList(List<DocumentWrapper> docWrapperList, Map<Id, VTD1_Document__c> documentsMap) {
        Set<Id> docIdsSet = new Set<Id>(documentsMap.keySet());
        if (!docIdsSet.isEmpty()) {
            List<ContentDocumentLink> contentDocumentLinks = [
                    SELECT Id, ContentDocumentId, LinkedEntityId
                    FROM ContentDocumentLink
                    WHERE LinkedEntityId IN :docIdsSet
            ];

            Map<Id, Id> contentDocumentLinksMap = new Map<Id, Id>();
            for (ContentDocumentLink link : contentDocumentLinks) {
                contentDocumentLinksMap.put(link.ContentDocumentId, link.LinkedEntityId);
            }

            List<ContentVersion> contentVersions = [
                    SELECT
                            Id,
                            Title,
                            ContentDocumentId,
                            VTD1_Lifecycle_State__c,
                            VTD1_CompoundVersionNumber__c,
                            VTD1_Current_Version__c
                    FROM ContentVersion
                    WHERE ContentDocumentId IN :contentDocumentLinksMap.keySet()
                    //AND VTD1_Current_Version__c = FALSE
                    ORDER BY ContentDocumentId ASC, VTD1_CompoundVersionNumber__c DESC
            ];

            Map<Id, ContentVersion[]> docsToVersMap = new Map<Id, ContentVersion[]>();
            for (ContentVersion version : contentVersions) {
                Id documentId = contentDocumentLinksMap.get(version.ContentDocumentId);
                if (docsToVersMap.containsKey(documentId)) {
                    docsToVersMap.get(documentId).add(version);
                } else {
                    docsToVersMap.put(documentId, new List<ContentVersion>{
                            version
                    });
                }
            }

            for (VTD1_Document__c document : documentsMap.values()) {
                docWrapperList.add(new DocumentWrapper(document, docsToVersMap.get(document.Id)));
            }
        }
        return docWrapperList;
    }

    private static String getStudyName(Id studyId, List<Virtual_Site__c> sites) {
        for (Virtual_Site__c site : sites) {
            if (site.VTD1_Study__c == studyId) {
                return site.VTD1_Study__r.name;
            }
        }
        return null;
    }
}