/**
* @author: Carl Judge
* @date: 29-Aug-19
**/

public without sharing class VT_R3_GlobalSharingLogic_VideoConference extends VT_R3_GlobalSharingLogic_PatientRelated {
    public override Type getType() {
        return VT_R3_GlobalSharingLogic_VideoConference.class;
    }

    public override VTR3_GlobalSharingConfig__mdt getConfig() {
        return getConfigForObjectType(Video_Conference__c.getSObjectType().getDescribe().getName());
    }

    protected override void fillRecordToCasesIdMap() {
        for (Video_Conference__c conf : [
            SELECT VTD1_Clinical_Study_Membership__c, VTD1_Actual_Visit__r.Unscheduled_Visits__c, VTD1_Actual_Visit__r.VTD1_Case__c
            FROM Video_Conference__c
            WHERE Id IN :recordIds
        ]) {
            Id caseId;
            if (conf.VTD1_Clinical_Study_Membership__c != null) {
                caseId = conf.VTD1_Clinical_Study_Membership__c;
            } else if (conf.VTD1_Actual_Visit__r.Unscheduled_Visits__c != null) {
                caseId = conf.VTD1_Actual_Visit__r.Unscheduled_Visits__c;
            } else if (conf.VTD1_Actual_Visit__r.VTD1_Case__c != null) {
                caseId = conf.VTD1_Actual_Visit__r.VTD1_Case__c;
            }
            if (caseId != null) {
                addToRecordToCaseIdsMap(conf.Id, caseId);
            }
        }
    }

    protected override Set<Id> getRecordIdsFromCarePlanIds(Set<Id> caseIds) {
        return new Map<Id, Video_Conference__c>([
            SELECT Id
            FROM Video_Conference__c
            WHERE VTD1_Clinical_Study_Membership__c IN :caseIds
            OR VTD1_Actual_Visit__r.Unscheduled_Visits__c IN :caseIds
            OR VTD1_Actual_Visit__r.VTD1_Case__c IN :caseIds
            LIMIT :Limits.getLimitQueryRows() - Limits.getQueryRows()
        ]).keySet();
    }

    protected override String getUserQuery() {
        String query =
            'SELECT Id,' +
                'VTD1_Actual_Visit__r.VTR5_PatientCaseId__r.Id,' +
                'VTD1_Actual_Visit__r.VTR5_PatientCaseId__r.VTD1_Patient_User__c,' +
                'VTD1_Actual_Visit__r.VTR5_PatientCaseId__r.VTD1_Study__c,' +
                'VTD1_Clinical_Study_Membership__r.Id,' +
                'VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c,' +
                'VTD1_Clinical_Study_Membership__r.VTD1_Study__c,' +
                'VTD1_Actual_Visit__r.VTR5_PatientCaseId__c' +
            ' FROM Video_Conference__c' +
            ' WHERE (VTD1_Actual_Visit__r.VTR5_PatientCaseId__r.VTD1_Patient_User__c IN :patientIds' +
                ' OR VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c IN :patientIds' +
                ' OR VTD1_Actual_Visit__r.VTR5_PatientCaseId__r.VTD1_Patient_User__c IN :cgPatientIds' +
                ' OR VTD1_Clinical_Study_Membership__r.VTD1_Patient_User__c IN :cgPatientIds' +
            ')';
        String scopeCondition = getScopeCondition();
        if (scopeCondition != null) {
            query += ' AND ' + scopeCondition;
        }
        System.debug(query);
        return query;
    }

    protected override String getRemoveQuery() {
        String removeQuery = 'SELECT Id FROM Video_Conference__Share WHERE (';
        if (scopedCaseIds != null) {
            removeQuery += 'Parent.VTD1_Actual_Visit__r.VTR5_PatientCaseId__c IN :scopedCaseIds' +
                ' OR Parent.VTD1_Clinical_Study_Membership__c IN :scopedCaseIds';
        } else if (scopedSiteIds != null) {
            removeQuery += 'Parent.VTD1_Actual_Visit__r.VTR5_PatientCaseId__r.VTD1_Virtual_Site__c IN :scopedSiteIds' +
                ' OR Parent.VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__c IN :scopedSiteIds';
        } else if (scopedStudyIds != null) {
            removeQuery += 'Parent.VTD1_Actual_Visit__r.VTR5_PatientCaseId__r.VTD1_Study__c IN :scopedStudyIds' +
                ' OR Parent.VTD1_Clinical_Study_Membership__r.VTD1_Study__c IN :scopedStudyIds';
        }
        removeQuery += ') AND UserOrGroupId IN :includedUserIds';
        return removeQuery;
    }

    private String getScopeCondition() {
        String scopeCondition;
        if (scopedCaseIds != null) {
            scopeCondition = '(VTD1_Actual_Visit__r.VTR5_PatientCaseId__c IN :scopedCaseIds' +
                ' OR VTD1_Clinical_Study_Membership__c IN :scopedCaseIds)';
        } else if (scopedSiteIds != null) {
            scopeCondition = '(VTD1_Actual_Visit__r.VTR5_PatientCaseId__r.VTD1_Virtual_Site__c IN :scopedSiteIds' +
                ' OR VTD1_Clinical_Study_Membership__r.VTD1_Virtual_Site__c IN :scopedSiteIds)';
        } else if (scopedStudyIds != null) {
            scopeCondition = '(VTD1_Actual_Visit__r.VTR5_PatientCaseId__r.VTD1_Study__c IN :scopedStudyIds' +
                ' OR VTD1_Clinical_Study_Membership__r.VTD1_Study__c IN :scopedStudyIds)';
        }
        return scopeCondition;
    }

    protected override void createUserShareDetailsFromRecords(List<SObject> objs) {
        List<Video_Conference__c> records = (List<Video_Conference__c>)objs;
        for (Video_Conference__c rec : records) {
            Case cas;
            if (rec.VTD1_Actual_Visit__r.VTR5_PatientCaseId__c != null) {
                cas = rec.VTD1_Actual_Visit__r.VTR5_PatientCaseId__r;
            } else {
                cas = rec.VTD1_Clinical_Study_Membership__r;
            }
            addUserShareDetailsForCase(rec.Id, cas);
        }
    }

    protected override void getShareDetailsByCaseId() {
        for (Case cas : cases) {
            addCaseShareDetail(cas, cas.VTD1_Patient_User__c);
            addCaseShareDetail(cas, cas.VTD1_Virtual_Site__r.VTR5_ReadShareGroupId__c);
            addCaseShareDetail(cas, cas.VTD1_Virtual_Site__r.VTR5_EditShareGroupId__c);
            if (patientToCaregiverIds.containsKey(cas.VTD1_Patient_User__c)) {
                for (Id cgId : patientToCaregiverIds.get(cas.VTD1_Patient_User__c)) { addCaseShareDetail(cas, cgId); }
            }
        }
    }
}