/**
 * Created by Yuliya Yakushenkova on 10/13/2020.
 */

public class VT_R5_RandomizationHomepageService {

    public static VT_R5_RandomizationHomepage getHomePage(Case patientCase) {
        List<Teammate> teammates = getStudyTeammates(patientCase);
        StudyInformation studyInformation = getStudyInformation(patientCase);
        NewsFeedSection newsFeedSection = getNewsFeedSection(patientCase);

        if (patientCase.Contact.VTD1_FullHomePage__c) {
            return new VT_R5_PostRandomizationHomepage()
                    .setStudyInformation(studyInformation)
                    .setTeammateSection(teammates)
                    .addNewsFeed(newsFeedSection)
                    .getPage(patientCase);
        }
        return new VT_R5_PreRandomizationHomepage()
                .setStudyInformation(
                        studyInformation.setHeaderAndWelcomeText(
                                patientCase.VTD1_Study__r.VTD1_Protocol_Nickname__c,
                                patientCase.VTD1_Study__r.Name
                        )
                )
                .addNewsFeed(newsFeedSection)
                .setTeammateSection(teammates)
                .getPage(patientCase);
    }



    private static List<Teammate> getStudyTeammates(Case patientCase) {
        List<Teammate> team = new List<Teammate>();

        if (String.isNotBlank(patientCase.VTD1_Primary_PG__r.Name)) {
            team.add(new Teammate(
                    patientCase.VTD1_Primary_PG__r.Name,
                    'VT_D1_TeamPatientGuide',
                    patientCase.VTD1_Primary_PG__c
            ));
        }
        if (String.isNotBlank(patientCase.VTR2_SiteCoordinator__r.Name)) {
            team.add(new Teammate(
                    patientCase.VTR2_SiteCoordinator__r.Name,
                    'VTR2_ProfileSiteCoordinator',
                    patientCase.VTR2_SiteCoordinator__c
            ));
        }
        if (String.isNotBlank(patientCase.VTD1_PI_contact__r.Name)) {
            team.add(new Teammate(
                    patientCase.VTD1_PI_contact__r.Name,
                    'VT_D1_TeamStudyDoctor',
                    patientCase.VTD1_PI_user__c
            ));
        }
        return team;
    }

    private static StudyInformation getStudyInformation(Case patientCase) {
        return new StudyInformation(patientCase.VTD1_Study__r.VTD1_Study_Logo__c, patientCase.VTR2_StudyPhoneNumber__r.Name);
    }

    private static NewsFeedSection getNewsFeedSection(Case patientCase) {
        return new NewsFeedSection(patientCase);
    }

    public class Teammate {
        public String name;
        public String photo;
        public String role;

        public Teammate(String name, String role, Id memberId) {
            this.name = name;
            this.role = VT_D1_TranslateHelper.getLabelValue(role);
            setMemberPhoto(memberId);
        }

        private void setMemberPhoto(Id memberId) {
            String photo = '';
            if (!Test.isRunningTest()) photo = ConnectApi.UserProfiles.getPhoto(null, memberId).standardEmailPhotoUrl;
            if (!photo.contains('default_profile')) this.photo = photo;
        }
    }

    public class StudyInformation {
        private String logo;
        public String header;
        public String welcomeText;
        private Link callSupportLink;
        private String startChatLabel;
        private Link studyDetailsLink;
        private Link requestReplacementLink;

        private StudyInformation(String logo, String studyPhoneNumberName) {
            if (String.isNotBlank(logo)) {
                this.logo = logo.substring(
                        logo.indexOf('src=') + 5,
                        logo.indexOf('"></img>')
                ).replace('amp;', '');
            }
            if (String.isNotBlank(studyPhoneNumberName)) {
                this.callSupportLink = new Link('VTD1_CallSupportCenter', studyPhoneNumberName);
            }
            startChatLabel = VT_D1_TranslateHelper.getLabelValue('VTD1_StartChat');
            studyDetailsLink = new Link('VT_D1_ViewStudyDetails', 'StudyDetails');
            requestReplacementLink = new Link('VTD1_UploadMedicalRecords', 'MedicalRecords');
        }

        private StudyInformation setHeaderAndWelcomeText(String protocolNickname, String studyName) {
            if (UserInfo.getLanguage() == 'ja') {
                this.header = protocolNickname + ' ' + Label.VT_D1_Welcome;
                this.welcomeText = System.Label.VT_D1_YourStudyNumber.substringBefore('{') + ' ' + studyName + 'です。 ' + System.Label.VT_D1_StudyHubIs.replace('\n', '');
            } else {
                this.header = System.Label.VT_D1_Welcome + ' ' + System.Label.VT_D1_CommunityHomeWelcome_The + ' ' + protocolNickname + ' ' + System.Label.VT_D1_Study;
                this.welcomeText = System.Label.VT_D1_YourStudyNumber.substringBefore('{') + ' ' + studyName + '. ' + System.Label.VT_D1_StudyHubIs.replace('\n', '');
            }
            return this;
        }
    }

    private class Link {
        public String label;
        public String destination;

        public Link(String label, String destination) {
            this.label = VT_D1_TranslateHelper.getLabelValue(label);
            this.destination = destination;
        }
    }

    public class NewsFeedSection {
        Integer totalNews;
        List<VT_R5_MobileNewsFeedService.NewsFeedItem> newsItems;

        NewsFeedSection (Case patientCase) {
            VT_R5_MobileNewsFeedService newsFeedService = new VT_R5_MobileNewsFeedService();
            newsFeedService.setListQueryParams(3, 0, UserInfo.getLanguage(), patientCase.VTD1_Study__c, patientCase.Status);
            this.newsItems = newsFeedService.getNewsFeedList();
            this.totalNews = newsFeedService.countArticles();
        }
    }

}