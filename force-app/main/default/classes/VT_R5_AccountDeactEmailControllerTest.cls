/**
 * @author:         Rishat Shamratov
 * @date:           17.07.2020
 * @description:    Test class for VT_R5_AccountDeactivatedEmailController
 */

@IsTest
public with sharing class VT_R5_AccountDeactEmailControllerTest {

    public static void renderStoredEmailTemplateTest() {
        DomainObjects.Study_t domainStudy = new DomainObjects.Study_t()
                .setMaximumDaysWithoutLoggingIn(1)
                .addAccount(new DomainObjects.Account_t()
                        .setRecordTypeByName('Sponsor'));
        domainStudy.persist();

        HealthCloudGA__CarePlanTemplate__c study = [SELECT Id FROM HealthCloudGA__CarePlanTemplate__c WHERE Id = :domainStudy.Id];
        study.VTD1_Protocol_Nickname__c = 'Test Study';
        study.VTR5_Completion_or_Deactivation_Message__c = 'Account Deactivated.';
        study.VTR5_Sponsor_Logo__c = 'Sponsor Logo Image';
        study.VTR5_Sponsor_Name__c = 'Sponsor';
        update study;

        DomainObjects.VTD1_Patient_t patient_t = new DomainObjects.VTD1_Patient_t()
                .addVTD1_Contact(new DomainObjects.Contact_t());

        DomainObjects.User_t user_t = new DomainObjects.User_t()
                .addContact(new DomainObjects.Contact_t())
                .setProfile(VT_R4_ConstantsHelper_ProfilesSTM.PATIENT_PROFILE_NAME);

        DomainObjects.Case_t domainCarePlan = new DomainObjects.Case_t()
                .addStudy(domainStudy)
                .addVTD1_Patient(patient_t)
                .setRecordTypeByName(VT_R4_ConstantsHelper_AccountContactCase.RT_CAREPLAN)
                .addVTD1_Patient_User(user_t);
        domainCarePlan.persist();

        EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'VT_R5_Account_Deactivated'];

        Test.startTest();
        Messaging.renderStoredEmailTemplate(template.Id, patient_t.Id, domainCarePlan.Id);
        Test.stopTest();
    }
}