/**
 * Created by Aleksandr Mazan on 17-Aug-20.
 */
//The class contains all scheduled actions from eDiary PB

global with sharing class VT_R5_eDiaryScheduledActionsHeroku extends VT_R5_HerokuScheduler.AbstractScheduler implements VT_R5_HerokuScheduler.HerokuExecutable {

    public static Id eProRecordTypeId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('ePRO').getRecordTypeId();
    public static Id satisfactionSurveyRecordTypeId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('SatisfactionSurvey').getRecordTypeId();
    public static Id externalRecordTypeId = Schema.SObjectType.VTD1_Survey__c.getRecordTypeInfosByDeveloperName().get('VTR5_External').getRecordTypeId();

    global VT_R5_eDiaryScheduledActionsHeroku() {
    }

    public class FlowInput {
        public String surveyId;
        public DateTime scheduledTime;
        public DateTime reminderDueDate;
        public String reviewerUser;
        public DateTime reviewDueDate;
        public DateTime surveyDueDate;
        public DateTime dateAvailable;
        public String surveyStatus;  //the field uses in VT_R5_HerokuScheduler.doPost() for run butch
        public Id surveyRecordTypeId;
        public DateTime scoringDueDate;

        public  FlowInput(VTD1_Survey__c item, Datetime scheduledTime) {
            this.scheduledTime = scheduledTime;
            this.surveyId = item.Id;
            this.reviewerUser = item.VTR2_Reviewer_User__c;
            this.dateAvailable = item.VTD1_Date_Available__c;
            this.reminderDueDate = item.VTD1_Reminder_Due_Date__c;
            this.scoringDueDate = item.VTD1_Scoring_Due_Date__c;
            this.surveyDueDate = item.VTD1_Due_Date__c;
            this.surveyRecordTypeId = item.RecordTypeId;
            this.surveyStatus = item.VTD1_Status__c;
            this.reviewDueDate = item.VTR2_Review_Due_Date__c;
        }
    }


    public static Map<String, EmailTemplate> emailTemplateByDevName = new Map<String, EmailTemplate>();
    public static Map<Id, Contact> contactByStudyMemberId = new Map<Id, Contact>();
    public static final String EDIARY_RECORD_FLAG = 'surveyStatus';
    public static final String STATUS_NOT_STARTED = 'Not Started';
    public static final String EDIARY_SCHEDULE_ACTION_HEROKU = 'VT_R5_eDiaryScheduledActionsHeroku';
    public static final String STATUS_READY_TO_START = 'Ready to Start';
    public static final String STATUS_EDIARY_MISSED = 'eDiary Missed';
    public static final String STATUS_REVIEW_REQUIRED = 'Review Required';
    public static final String STATUS_IN_PROGRESS = 'In Progress';
    public static final String USER_PATIENT_GUIDE = 'Patient Guide';
    public static final String USER_SITE_COORDINATOR = 'Site Coordinator';
    public static final String PATIENT = 'Patient';
    public static final String EPRO = 'ePro';
    public static final String VT_R3_EPRO_AVAILABLE_TASK_PATIENT_VF = 'VT_R3_ePro_Available_Task_Patient_VF';
    public static final String VT_R3_EPRO_AVAILABLE_TASK_CAREGIVER_VF = 'VT_R3_ePro_Available_Task_Caregiver_VF';
    public static final String VT_R3_EPRO_IS_ALMOST_DUE_PATIENT_VF = 'VT_R3_ePRO_is_almost_due_Patient_VF';
    public static final String VT_R3_EPRO_IS_ALMOST_DUE_CAREGIVER_VF = 'VT_R3_ePRO_is_almost_due_Caregiver_VF';
    public static final String SATISFACTIONSURVEY = 'SatisfactionSurvey';
    public static final String VTR5_EXTERNAL = 'VTR5_External';
    public static final String TNCODE_N004 = 'N004';
    public static final String TNCODE_N532 = 'N532';
    public static final String TNCODE_N533 = 'N533';
    public static final String TNCODE_N534 = 'N534';
    public static final String TNCODE_N076 = 'N076';
    public static final String dateTimeNow = DateTime.now().format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'');
    public static String query = 'SELECT Id, Name, VTD1_Reminder_Due_Date__c, VTR2_Reviewer_User__c, VTD1_CSM__c, VTD1_Due_Date__c, VTD1_CSM__r.VTD1_Study__r.VTD1_Protocol_Nickname__c, ' +
            'VTD1_Date_Available__c, VTD1_Status__c, RecordTypeId, RecordType.DeveloperName, VTD1_Protocol_ePRO__c, VTD2_Study_Nickname__c, ' +
            'VTD1_Protocol_ePRO__r.VTD1_Subject__c, VTD1_Patient_User_Id__c, VTD1_Patient_User_Id__r.Email, VTD1_Caregiver_User_Id__c, VTD1_Caregiver_User_Id__r.Email, VTD1_CSM__r.VTD1_Study__c, ' +
            'VTR2_Number_of_Answered_Answers__c, VTD1_Number_of_Answers__c, VTR2_Review_Due_Date__c, VTD1_Scoring_Due_Date__c ' +
            'FROM VTD1_Survey__c ' +
            'WHERE VTD1_Patient_User_Id__r.IsActive = true AND';


    public static void scheduleActions(List<FlowInput> surveysData) {
        List<VT_R5_HerokuScheduler.HerokuSchedulerPayload> notifications = new List<VT_R5_HerokuScheduler.HerokuSchedulerPayload>();
        for (FlowInput surveyData : surveysData) {
            if (surveyData.scheduledTime != null) {
                notifications.add(new VT_R5_HerokuScheduler.HerokuSchedulerPayload(
                        surveyData.scheduledTime,
                        EDIARY_SCHEDULE_ACTION_HEROKU,
                        JSON.serialize(
                                surveyData
                        )));
            }
        }
        if (notifications.size() > 0) {
            new VT_R5_asincHeroku(notifications).enqueue();
        }
    }

    public void herokuExecute(String payload) {
        if (payload.countMatches(EDIARY_RECORD_FLAG) > 50) {
            Database.executeBatch(new VT_R5_eDiaryScheduledActionsHerokuBatch(payload), 50);
        } else {
        List<FlowInput> alerts = (List<FlowInput>) JSON.deserialize(payload, List<FlowInput>.class);
        Map<Id, FlowInput> alertsById = new Map<Id, FlowInput>();
        for (FlowInput oldSurvey : alerts) {
            alertsById.put(oldSurvey.surveyId, oldSurvey);
        }
        if (!alertsById.isEmpty()) {
            processAlerts(alertsById);
        }
    }
    }

    public void processAlerts(Map<Id, FlowInput> alertsById) {
        List<VT_R3_EmailsSender.ParamsHolder> emailParams = new List<VT_R3_EmailsSender.ParamsHolder>();
        Map<Id, VTD1_Survey__c> currentSurveys = new Map<Id, VTD1_Survey__c>();
        List<Id> surveyIds = new List<Id>(alertsById.keySet());
        query += ' Id IN :surveyIds';
        currentSurveys = new Map<Id, VTD1_Survey__c>((List<VTD1_Survey__c>) Database.query(query));
        List<Id> caseIds = new List<Id>();
        for (VTD1_Survey__c survey : currentSurveys.values()) {
            caseIds.add(survey.VTD1_CSM__c);
        }
        for (Contact contact : [SELECT Id, VTD1_UserId__c, Email, VTD1_Clinical_Study_Membership__c FROM Contact WHERE VTD1_Primary_CG__c = true AND VTD1_Clinical_Study_Membership__c IN :caseIds]) {
            contactByStudyMemberId.put(contact.VTD1_Clinical_Study_Membership__c, contact);
        }
        List<VTD1_Survey__c> surveysForUpdate = new List<VTD1_Survey__c>();
        List<VT_D1_ePROSchedulingTaskHelper.TaskTemplate> taskTemplates = new List<VT_D1_ePROSchedulingTaskHelper.TaskTemplate>();
        List<VT_D1_ePROSchedulingNotificationHelper.NotificationTemplate> notificationTemplates = new List<VT_D1_ePROSchedulingNotificationHelper.NotificationTemplate>();
        List <VT_D2_TNCatalogNotifications.ParamsHolder> paramHolders = new List<VT_D2_TNCatalogNotifications.ParamsHolder>();
        Boolean isNeedUpdateSurvey = false;
        FlowInput oldSurvey;
        VTD1_Survey__c currentSurvey;
        if (emailTemplateByDevName.keySet().isEmpty()) {
            List<String> templateNames = new List<String>{
                    VT_R3_EPRO_AVAILABLE_TASK_PATIENT_VF, VT_R3_EPRO_AVAILABLE_TASK_CAREGIVER_VF, VT_R3_EPRO_IS_ALMOST_DUE_PATIENT_VF, VT_R3_EPRO_IS_ALMOST_DUE_CAREGIVER_VF
            };
            for (EmailTemplate emailTemplate : [SELECT Id,Subject,Description,HtmlValue,DeveloperName,Body FROM EmailTemplate WHERE DeveloperName IN :templateNames]) {
                emailTemplateByDevName.put(emailTemplate.DeveloperName, emailTemplate);
            }
        }
        for (Id surveyId : alertsById.keySet()) {
            try {
                if (currentSurveys.keySet().contains(surveyId)) {
                    oldSurvey = alertsById.get(surveyId);
                    currentSurvey = currentSurveys.get(surveyId);
                    isNeedUpdateSurvey = false;
                    if (oldSurvey != null) {
                        if (Test.isRunningTest() || oldSurvey.dateAvailable == currentSurvey.VTD1_Date_Available__c &&
                                oldSurvey.reminderDueDate == currentSurvey.VTD1_Reminder_Due_Date__c &&
                                oldSurvey.reviewDueDate == currentSurvey.VTR2_Review_Due_Date__c &&
                                oldSurvey.reviewerUser == currentSurvey.VTR2_Reviewer_User__c &&
                                oldSurvey.scoringDueDate == currentSurvey.VTD1_Scoring_Due_Date__c &&
                                oldSurvey.surveyDueDate == currentSurvey.VTD1_Due_Date__c &&
                                oldSurvey.surveyRecordTypeId == currentSurvey.RecordTypeId &&
                                oldSurvey.surveyStatus == currentSurvey.VTD1_Status__c
                                ) {
                            if (currentSurvey.VTD1_Date_Available__c != null &&
                                    currentSurvey.VTD1_Status__c == STATUS_NOT_STARTED &&
                                    (currentSurvey.RecordType.DeveloperName == EPRO ||
                                            currentSurvey.RecordType.DeveloperName == SATISFACTIONSURVEY)) {
                                if (currentSurvey.VTD1_Date_Available__c < Datetime.now()) {

                                    currentSurvey.VTD1_Status__c = STATUS_READY_TO_START;

                                    VT_D1_ePROSchedulingTaskHelper.TaskTemplate newTaskTemplate = new VT_D1_ePROSchedulingTaskHelper.TaskTemplate();
                                    newTaskTemplate.category = 'Other Tasks';
                                    newTaskTemplate.dueDate = currentSurvey.VTD1_Due_Date__c.date();
                                    newTaskTemplate.recordTypeDeveloperName = 'SimpleTask';
                                    newTaskTemplate.redirect = 'my-diary';
                                    newTaskTemplate.subject = 'VTD2_CompleteeDiary';
                                    newTaskTemplate.subjectParameters = (String) (Id) currentSurvey.VTD1_Protocol_ePRO__c + 'Name';
                                    newTaskTemplate.surveyId = currentSurvey.Id;
                                    newTaskTemplate.uniqueCode = '010';
                                    taskTemplates.add(newTaskTemplate);

                                    emailParams.addAll(prepareEmails(currentSurvey, true, currentSurvey.VTD1_Protocol_ePRO__r.VTD1_Subject__c));

                                    VT_D1_ePROSchedulingNotificationHelper.NotificationTemplate newNotificationTemplate = new VT_D1_ePROSchedulingNotificationHelper.NotificationTemplate();
                                    if (currentSurvey.VTD1_Protocol_ePRO__r.VTD1_Subject__c == 'Caregiver') {
                                        newNotificationTemplate.receiverId = currentSurvey.VTD1_Caregiver_User_Id__c;
                                        newNotificationTemplate.ownerId = currentSurvey.VTD1_Caregiver_User_Id__c;
                                    } else {
                                        newNotificationTemplate.receiverId = currentSurvey.VTD1_Patient_User_Id__c;
                                        newNotificationTemplate.ownerId = currentSurvey.VTD1_Patient_User_Id__c;
                                    }
                                    newNotificationTemplate.hasDirectLink = true;
                                    newNotificationTemplate.linkToRelatedEventOrObject = 'my-diary';
                                    newNotificationTemplate.messageLabel = 'VTR3_eDiary_Entry_Available_Notification';
                                    newNotificationTemplate.title = 'eDiary';
                                    newNotificationTemplate.studyId = currentSurvey.VTD1_CSM__r.VTD1_Study__c;
                                    newNotificationTemplate.surveyId = currentSurvey.Id;
                                    notificationTemplates.add(newNotificationTemplate);
                                    isNeedUpdateSurvey = true;
                                }
                            } else if (currentSurvey.VTD1_Status__c == STATUS_READY_TO_START &&
                                    (currentSurvey.VTR2_Reviewer_User__c == USER_PATIENT_GUIDE ||
                                            currentSurvey.VTR2_Reviewer_User__c == USER_SITE_COORDINATOR) &&
                                    (currentSurvey.RecordType.DeveloperName == EPRO ||
                                            currentSurvey.RecordType.DeveloperName == SATISFACTIONSURVEY)) {
                                if (currentSurvey.VTD1_Reminder_Due_Date__c <= Datetime.now() && currentSurvey.VTD1_Due_Date__c <= Datetime.now()) {
                                    isNeedUpdateSurvey = true;
                                }
                                if (currentSurvey.VTD1_Reminder_Due_Date__c <= Datetime.now()) {
                                    paramHolders.add(createParamsHolder(currentSurvey.Id, TNCODE_N004));
                                    emailParams.addAll(prepareEmails(currentSurvey, false, currentSurvey.VTD1_Protocol_ePRO__r.VTD1_Subject__c));
                                }
                                if (currentSurvey.VTD1_Due_Date__c <= Datetime.now()) {
                                    currentSurvey.VTD1_Status__c = STATUS_EDIARY_MISSED;
                                    currentSurvey.VTD1_ePRO_Missed__c = true;
                                    isNeedUpdateSurvey = true;
                                    if (currentSurvey.VTR2_Reviewer_User__c == USER_PATIENT_GUIDE) {
                                        paramHolders.add(createParamsHolder(currentSurvey.Id, TNCODE_N532));
                                    } else {
                                        paramHolders.add(createParamsHolder(currentSurvey.Id, TNCODE_N076));
                                    }
                                }
                            } else if (currentSurvey.VTD1_Status__c == STATUS_IN_PROGRESS &&
                                    currentSurvey.VTR2_Number_of_Answered_Answers__c > 0 &&
                                    currentSurvey.VTR2_Number_of_Answered_Answers__c <= currentSurvey.VTD1_Number_of_Answers__c &&
                                    (currentSurvey.RecordType.DeveloperName == EPRO ||
                                            currentSurvey.RecordType.DeveloperName == SATISFACTIONSURVEY)) {
                                if (currentSurvey.VTD1_Reminder_Due_Date__c <= Datetime.now() && currentSurvey.VTD1_Due_Date__c <= Datetime.now()) {
                                    isNeedUpdateSurvey = true;
                                }
                                if (currentSurvey.VTD1_Reminder_Due_Date__c <= Datetime.now()) {
                                    paramHolders.add(createParamsHolder(currentSurvey.Id, TNCODE_N004));
                                }
                                if (currentSurvey.VTD1_Due_Date__c <= Datetime.now()) {
                                    currentSurvey.VTD1_Status__c = STATUS_REVIEW_REQUIRED;
                                    isNeedUpdateSurvey = true;
                                }
                            } else if (currentSurvey.VTD1_Status__c == STATUS_REVIEW_REQUIRED &&
                                    currentSurvey.VTR2_Review_Due_Date__c != null &&
                                    (currentSurvey.VTR2_Reviewer_User__c == USER_PATIENT_GUIDE ||
                                            currentSurvey.VTR2_Reviewer_User__c == USER_SITE_COORDINATOR) &&
                                    (currentSurvey.RecordType.DeveloperName == EPRO ||
                                            currentSurvey.RecordType.DeveloperName == VTR5_EXTERNAL ||
                                            currentSurvey.RecordType.DeveloperName == SATISFACTIONSURVEY)) {
                                if (currentSurvey.VTR2_Review_Due_Date__c <= Datetime.now()) {
                                    paramHolders.add(createParamsHolder(currentSurvey.Id, TNCODE_N533));
                                    isNeedUpdateSurvey = true;
                                }
                            } else if (currentSurvey.VTD1_Status__c == STATUS_REVIEW_REQUIRED &&
                                    currentSurvey.VTD1_Scoring_Due_Date__c != null &&
                                    (currentSurvey.VTR2_Reviewer_User__c == USER_PATIENT_GUIDE ||
                                            currentSurvey.VTR2_Reviewer_User__c == USER_SITE_COORDINATOR) &&
                                    currentSurvey.RecordType.DeveloperName == EPRO) {
                                if (currentSurvey.VTD1_Scoring_Due_Date__c <= Datetime.now()) {
                                    paramHolders.add(createParamsHolder(currentSurvey.Id, TNCODE_N534));
                                    isNeedUpdateSurvey = true;
                                }
                            }
                            if (isNeedUpdateSurvey) {
                                surveysForUpdate.add(currentSurvey);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                System.debug(e.getMessage());
            }
        }
        if (!paramHolders.isEmpty()) {
            VT_D2_TNCatalogNotifications.generateNotification(paramHolders);
        }
        if (!emailParams.isEmpty()) {
            VT_R3_EmailsSender.sendEmails(emailParams);
        }
        if (!taskTemplates.isEmpty()) {
            VT_D1_ePROSchedulingTaskHelper.sendPatientCaregiverTasks(taskTemplates);
        }
        if (!notificationTemplates.isEmpty()) {
            VT_D1_ePROSchedulingNotificationHelper.sendPatientCaregiverNotifications(notificationTemplates);
        }
        if (!surveysForUpdate.isEmpty()) {
            update surveysForUpdate;
        }
    }

    public static List<VT_R3_EmailsSender.ParamsHolder> prepareEmails(VTD1_Survey__c vareDiaryInput, Boolean eDiaryFlag, String varProtocolSubject) {
        List<VT_R3_EmailsSender.ParamsHolder> params = new List<VT_R3_EmailsSender.ParamsHolder>();
        if (eDiaryFlag) {
            if (vareDiaryInput.VTD1_Patient_User_Id__c != null && varProtocolSubject == PATIENT) {
                params.add(new VT_R3_EmailsSender.ParamsHolder(vareDiaryInput.VTD1_Patient_User_Id__c, vareDiaryInput.Id, VT_R3_EPRO_AVAILABLE_TASK_PATIENT_VF));
            }
            if (contactByStudyMemberId.keySet().contains(vareDiaryInput.VTD1_CSM__c)) {
                params.add(new VT_R3_EmailsSender.ParamsHolder(contactByStudyMemberId.get(vareDiaryInput.VTD1_CSM__c).VTD1_UserId__c, vareDiaryInput.Id, VT_R3_EPRO_AVAILABLE_TASK_CAREGIVER_VF));
            }
        } else if (eDiaryFlag == false && vareDiaryInput.VTD1_Status__c == STATUS_IN_PROGRESS) {
            if (vareDiaryInput.VTD1_Patient_User_Id__c != null && varProtocolSubject == PATIENT) {
                params.add(new VT_R3_EmailsSender.ParamsHolder(vareDiaryInput.VTD1_Patient_User_Id__c, vareDiaryInput.Id, VT_R3_EPRO_IS_ALMOST_DUE_PATIENT_VF));
            }
            if (contactByStudyMemberId.keySet().contains(vareDiaryInput.VTD1_CSM__c)) {
                params.add(new VT_R3_EmailsSender.ParamsHolder(contactByStudyMemberId.get(vareDiaryInput.VTD1_CSM__c).VTD1_UserId__c, vareDiaryInput.Id, VT_R3_EPRO_IS_ALMOST_DUE_CAREGIVER_VF));
            }
        }
        return params;
    }

    public static VT_D2_TNCatalogNotifications.ParamsHolder createParamsHolder(Id sourceId, String tnCode) {
        VT_D2_TNCatalogNotifications.ParamsHolder paramHolder = new VT_D2_TNCatalogNotifications.ParamsHolder();
        paramHolder.sourceId = sourceId;
        paramHolder.tnCode = tnCode;
        return paramHolder;
    }

    global with sharing class VT_R5_asincHeroku extends VT_R3_AbstractChainableQueueable implements Database.AllowsCallouts{
        private List<VT_R5_HerokuScheduler.HerokuSchedulerPayload> notifications;

        public VT_R5_asincHeroku(List<VT_R5_HerokuScheduler.HerokuSchedulerPayload> notifications) {
            this.notifications = new List<VT_R5_HerokuScheduler.HerokuSchedulerPayload>(notifications);
        }

        public override Type getType() {
            return VT_R5_asincHeroku.class;
        }

        public override void executeLogic() {
            new VT_R5_eDiaryScheduledActionsHeroku().schedule(notifications);
        }
    }

    public static void isNeedScheduledAction(List<VTD1_Survey__c> newSurveys, List<VTD1_Survey__c> oldSurveys) {
        List<VTD1_Survey__c> surveysNeedUpdate = new List<VTD1_Survey__c>();
        for (Integer i = 0; i < newSurveys.size(); i++) {
            if ((newSurveys[i].VTD1_Date_Available__c != null &&
                    newSurveys[i].VTD1_Status__c == STATUS_NOT_STARTED) ||
                    (newSurveys[i].VTD1_Status__c == STATUS_READY_TO_START &&
                            oldSurveys[i].VTD1_Status__c != STATUS_READY_TO_START &&
                            ((newSurveys[i].VTR2_Reviewer_User__c == USER_PATIENT_GUIDE &&
                                    oldSurveys[i].VTR2_Reviewer_User__c == USER_PATIENT_GUIDE) ||
                                    (newSurveys[i].VTR2_Reviewer_User__c == USER_SITE_COORDINATOR &&
                                            oldSurveys[i].VTR2_Reviewer_User__c == USER_SITE_COORDINATOR))) ||
                    (newSurveys[i].VTD1_Status__c == STATUS_IN_PROGRESS &&
                            oldSurveys[i].VTD1_Status__c != STATUS_IN_PROGRESS &&
                            newSurveys[i].VTR2_Number_of_Answered_Answers__c > 0 &&
                            newSurveys[i].VTR2_Number_of_Answered_Answers__c <= newSurveys[i].VTD1_Number_of_Answers__c) ||
                    (newSurveys[i].VTD1_Status__c == STATUS_REVIEW_REQUIRED &&
                            oldSurveys[i].VTD1_Status__c != STATUS_REVIEW_REQUIRED &&
                            newSurveys[i].VTR2_Review_Due_Date__c != null &&
                            (newSurveys[i].VTR2_Reviewer_User__c == USER_PATIENT_GUIDE &&
                                    oldSurveys[i].VTR2_Reviewer_User__c == USER_PATIENT_GUIDE ||
                                    newSurveys[i].VTR2_Reviewer_User__c == USER_SITE_COORDINATOR &&
                                    oldSurveys[i].VTR2_Reviewer_User__c == USER_SITE_COORDINATOR)) ||
                    (newSurveys[i].VTD1_Status__c == STATUS_REVIEW_REQUIRED &&
                            oldSurveys[i].VTD1_Status__c != STATUS_REVIEW_REQUIRED &&
                            newSurveys[i].VTD1_Scoring_Due_Date__c != null &&
                            (newSurveys[i].VTR2_Reviewer_User__c == USER_PATIENT_GUIDE &&
                                    oldSurveys[i].VTR2_Reviewer_User__c == USER_PATIENT_GUIDE ||
                                    newSurveys[i].VTR2_Reviewer_User__c == USER_SITE_COORDINATOR &&
                                    oldSurveys[i].VTR2_Reviewer_User__c == USER_SITE_COORDINATOR))) {
                surveysNeedUpdate.add(newSurveys[i]);
            }
        }
        if (!surveysNeedUpdate.isEmpty()) {
            scheduleNotifications(surveysNeedUpdate);
        }
    }

    public static void scheduleNotifications(List<VTD1_Survey__c> surveys) {
        List<FlowInput> inputData = new List<FlowInput>();
        for (VTD1_Survey__c item : surveys) {
            if (item.VTD1_Date_Available__c != null &&
                    item.VTD1_Status__c == STATUS_NOT_STARTED &&
                    (item.RecordTypeId == eProRecordTypeId ||
                    item.RecordTypeId == satisfactionSurveyRecordTypeId)) {
                inputData.add(new FlowInput(item, item.VTD1_Date_Available__c));
            } else if (item.VTD1_Status__c == STATUS_READY_TO_START &&
                    (item.VTR2_Reviewer_User__c == USER_PATIENT_GUIDE ||
                            item.VTR2_Reviewer_User__c == USER_SITE_COORDINATOR) &&
                    (item.RecordTypeId == eProRecordTypeId ||
                    item.RecordTypeId == satisfactionSurveyRecordTypeId)) {
                inputData.add(new FlowInput(item, item.VTD1_Reminder_Due_Date__c));
                inputData.add(new FlowInput(item, item.VTD1_Due_Date__c));
            } else if (item.VTD1_Status__c == STATUS_IN_PROGRESS &&
                    item.VTR2_Number_of_Answered_Answers__c > 0 &&
                    item.VTR2_Number_of_Answered_Answers__c <= item.VTD1_Number_of_Answers__c &&
                    (item.RecordTypeId == eProRecordTypeId ||
                    item.RecordTypeId == satisfactionSurveyRecordTypeId)) {
                inputData.add(new FlowInput(item, item.VTD1_Reminder_Due_Date__c));
                inputData.add(new FlowInput(item, item.VTD1_Due_Date__c));
            } else if (item.VTD1_Status__c == STATUS_REVIEW_REQUIRED &&
                    item.VTR2_Review_Due_Date__c != null &&
                    (item.VTR2_Reviewer_User__c == USER_PATIENT_GUIDE ||
                            item.VTR2_Reviewer_User__c == USER_SITE_COORDINATOR) &&
                    (item.RecordTypeId == eProRecordTypeId ||
                            item.RecordTypeId == externalRecordTypeId ||
                            item.RecordTypeId == satisfactionSurveyRecordTypeId)) {
                inputData.add(new FlowInput(item, item.VTR2_Review_Due_Date__c));
            } else if (item.VTD1_Status__c == STATUS_REVIEW_REQUIRED &&
                    item.VTD1_Scoring_Due_Date__c != null &&
                    (item.VTR2_Reviewer_User__c == USER_PATIENT_GUIDE ||
                            item.VTR2_Reviewer_User__c == USER_SITE_COORDINATOR) &&
                    item.RecordTypeId == eProRecordTypeId) {
                inputData.add(new FlowInput(item, item.VTD1_Scoring_Due_Date__c));
            }
        }
        if (!inputData.isEmpty()) {
            scheduleActions(inputData);
        }
    }

}