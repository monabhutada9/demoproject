public with sharing class VT_R4_LastGeneralVisitsController {

    @AuraEnabled
    public static List<VTR4_General_Visit__c> getRecentlyVieweds() {
        return [
                SELECT Id, Name
                FROM VTR4_General_Visit__c
                WHERE createdById =: UserInfo.getUserId()
                ORDER BY CreatedDate DESC
                LIMIT 5
        ];
    }
}